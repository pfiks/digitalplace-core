<%@ include file="../init.jsp"%>

<c:set var="hasError" value="${not empty errorMessage}"/>

<div class="govuk-form-group ${hasError ? 'govuk-form-group--error' : ''}" id="${portletNamespace}_field_${fieldName}">
	<fieldset class="govuk-fieldset">
		
		<%@ include file="../fieldLegendLabel.jspf" %>
		<%@ include file="../fieldHintMessage.jspf" %>
		<%@ include file="../fieldErrorMessage.jspf" %>
		
		<div class="govuk-checkboxes">
			<c:forEach items="${fieldOptions}" var="availableOption" varStatus="loop">
				<div class="govuk-checkboxes__item">
					<c:set var="isSelected" value="${availableOption.key.equals(fieldValue) || fieldSelectedValues.contains(availableOption.key)}"/>
					<input class="govuk-checkboxes__input" id="${portletNamespace}${fieldName}-${loop.index}" name="${portletNamespace}${fieldName}" type="checkbox" value="${availableOption.key}"
						${isSelected ? 'checked' : ''}
					/>
					<label class="govuk-label govuk-checkboxes__label" for="${portletNamespace}${fieldName}-${loop.index}">
						<liferay-ui:message key="${availableOption.value}"/>
					</label>
				</div>
			</c:forEach>
		</div>
	</fieldset>
</div>