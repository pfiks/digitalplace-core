package com.placecube.digitalplace.ddl.web.portlet.display.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration")
public interface DDLDisplayPortletInstanceConfiguration {

	@Meta.AD(required = false)
	long ddmStructureId();

	@Meta.AD(required = false, deflt = "false")
	boolean enableTranslations();

	@Meta.AD(required = false, deflt = "")
	String[] mandatoryCategoryIds();

	@Meta.AD(required = false, deflt = "")
	String[] mandatoryVocabularyIds();

	@Meta.AD(required = false, deflt = "true")
	boolean showLoginOption();

}
