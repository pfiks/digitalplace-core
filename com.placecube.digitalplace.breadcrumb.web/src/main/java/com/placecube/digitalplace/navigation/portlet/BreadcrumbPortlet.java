package com.placecube.digitalplace.navigation.portlet;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.liferay.fragment.processor.PortletRegistry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.site.navigation.taglib.servlet.taglib.util.BreadcrumbUtil;
import com.placecube.digitalplace.navigation.configuration.BreadcrumbPortletInstanceConfiguration;
import com.placecube.digitalplace.navigation.constants.ConfigurationConstants;
import com.placecube.digitalplace.navigation.constants.PortletKeys;
import com.placecube.digitalplace.navigation.constants.PortletPreferenceKeys;
import com.placecube.digitalplace.navigation.model.BreadcrumbServiceContext;
import com.placecube.digitalplace.navigation.service.BreadcrumbService;

@Component(immediate = true, configurationPid = ConfigurationConstants.BREADCRUMB_PID, property = {
		"com.liferay.portlet.add-default-resource=true", "com.liferay.portlet.css-class-wrapper=navigation-breadcrumb", "com.liferay.portlet.display-category=category.cms",
		"com.liferay.portlet.instanceable=true", "com.liferay.portlet.layout-cacheable=true", "com.liferay.portlet.preferences-owned-by-group=true",
		"com.liferay.portlet.preferences-unique-per-layout=false", "com.liferay.portlet.private-request-attributes=false", "com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=1", "com.liferay.portlet.use-default-template=true", "javax.portlet.expiration-cache=0", "javax.portlet.init-param.template-path=/META-INF/resources/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.init-param.config-template=/configuration.jsp", "javax.portlet.name=" + PortletKeys.BREADCRUMB,
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=guest,power-user,user" }, service = Portlet.class)
public class BreadcrumbPortlet extends MVCPortlet {

	@Reference
	private BreadcrumbService breadcrumbService;

	@Reference
	private Portal portal;

	@Reference
	private PortletRegistry portletRegistry;

	private volatile BreadcrumbPortletInstanceConfiguration breadcrumbConfiguration;

	private static final String ALIAS = "navigation-breadcrumb";

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {

		breadcrumbConfiguration = ConfigurableUtil.createConfigurable(BreadcrumbPortletInstanceConfiguration.class, properties);

		if (breadcrumbConfiguration == null) {
			throw new IllegalStateException("Breadcrumb configuration missing - PID: " + ConfigurationConstants.BREADCRUMB_PID);
		}

		portletRegistry.registerAlias(ALIAS, PortletKeys.BREADCRUMB);

	}

	@Deactivate
	protected void deactivate() {
		portletRegistry.unregisterAlias(ALIAS);
	}

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		PortletPreferences preferences = renderRequest.getPreferences();
		try {

			BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(renderRequest);
			ThemeDisplay themeDisplay = breadcrumbServiceContext.getThemeDisplay();
			Layout layout = breadcrumbServiceContext.getLayout();
			String categoryNavRootEntryName = getCategoryNavRootEntryName(preferences);
			Optional<BreadcrumbEntry> siteLayoutBreadcrumbEntry = breadcrumbService.getSiteLayoutBreadcrumbEntry(themeDisplay, categoryNavRootEntryName);

			String displayStyle = preferences.getValue(PortletPreferenceKeys.DISPLAY_STYLE, StringPool.BLANK);
			long displayStyleGroupId = Long.valueOf(preferences.getValue(PortletPreferenceKeys.DISPLAY_STYLE_GROUP_ID, "0"));

			if (LayoutConstants.TYPE_ASSET_DISPLAY.equals(layout.getType())) {

				BreadcrumbEntry displayPageBreadcrumbEntry = breadcrumbService.getDisplayPageBreadcrumbEntry(breadcrumbServiceContext);
				List<List<BreadcrumbEntry>> categoryBreadcrumbEntries = breadcrumbService.getCategoryBreadcrumbEntries(breadcrumbServiceContext, siteLayoutBreadcrumbEntry, displayPageBreadcrumbEntry);

				breadcrumbService.applyCurrentLayoutPreferenceFilterToBreadcrumbTree(categoryBreadcrumbEntries, renderRequest);

				renderRequest.setAttribute("categoryBreadcrumbEntries", categoryBreadcrumbEntries);

			} else {
				HttpServletRequest httpServletRequest = portal.getHttpServletRequest(renderRequest);
				List<BreadcrumbEntry> layoutBreadcrumbEntries = BreadcrumbUtil.getLayoutBreadcrumbEntries(httpServletRequest, themeDisplay);

				breadcrumbService.addRootEntryIfNecessary(layoutBreadcrumbEntries, renderRequest, siteLayoutBreadcrumbEntry);
				breadcrumbService.applyCurrentLayoutPreferenceFilter(layoutBreadcrumbEntries, renderRequest);

				renderRequest.setAttribute("layoutBreadcrumbEntries", layoutBreadcrumbEntries);

			}

			renderRequest.setAttribute(PortletPreferenceKeys.DISPLAY_STYLE, displayStyle);
			renderRequest.setAttribute(PortletPreferenceKeys.DISPLAY_STYLE_GROUP_ID, displayStyleGroupId);
			super.render(renderRequest, renderResponse);

		} catch (Exception e) {

			throw new PortletException(e);

		}

	}

	private String getCategoryNavRootEntryName(PortletPreferences preferences) {

		return preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME, breadcrumbConfiguration.categoryNavRootEntryName());

	}

}
