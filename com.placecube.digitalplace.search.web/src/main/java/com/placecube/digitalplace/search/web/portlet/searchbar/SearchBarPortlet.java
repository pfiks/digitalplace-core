package com.placecube.digitalplace.search.web.portlet.searchbar;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.searchbar.configuration.SearchBarPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.searchbar.service.SearchBarService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-search-searchbar", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=dp-search-search-bar dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=true", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.restore-current-view=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/search-bar/view.jsp", //
		"javax.portlet.init-param.config-template=/search-bar/configuration.jsp", //
		"javax.portlet.name=" + PortletKeys.SEARCH_BAR, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class SearchBarPortlet extends MVCPortlet {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private SearchBarService searchBarService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {

			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			SearchBarPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(SearchBarPortletInstanceConfiguration.class, themeDisplay);

			if (!Validator.isBlank(configuration.destinationPage())) {
				boolean invalidConfigurationForCustomLayout = true;

				Optional<Layout> destinationLayout = searchBarService.getDestinationLayout(themeDisplay, configuration);

				if (destinationLayout.isPresent()) {
					String destinationSearchBarPortletId = searchBarService.getDestinationSearchBarPortletIdOnLayout(destinationLayout.get());
					if (Validator.isNotNull(destinationSearchBarPortletId)) {
						renderRequest.setAttribute("destinationPlid", destinationLayout.get().getPlid());
						renderRequest.setAttribute("destinationSearchBarPortletId", destinationSearchBarPortletId);
						invalidConfigurationForCustomLayout = false;
					}
				}

				renderRequest.setAttribute("invalidConfiguration", invalidConfigurationForCustomLayout);

			} else {

				SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

				renderRequest.setAttribute("redirectURL", searchResponse.getEscapedSearchResultsIteratorURL());
				renderRequest.setAttribute("keywords", searchResponse.getKeywords());
				renderRequest.setAttribute("totalResults", searchResponse.getTotalResults());

			}

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}