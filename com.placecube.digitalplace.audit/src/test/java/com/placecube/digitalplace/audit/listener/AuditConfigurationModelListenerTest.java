package com.placecube.digitalplace.audit.listener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Dictionary;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.configuration.persistence.listener.ConfigurationModelListener;
import com.liferay.portal.security.audit.event.generators.constants.EventTypes;
import com.placecube.digitalplace.audit.service.AuditService;

public class AuditConfigurationModelListenerTest extends PowerMockito {

	@InjectMocks
	private AuditConfigurationModelListener auditConfigurationModelListener;

	@Mock
	private Map<String, String> mockAdditionalInformationMap;

	@Mock
	private AuditService mockAuditService;

	@Mock
	private Dictionary<String, Object> mockProperties;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void onAfterDelete_WhenNoError_ThenAddsAuditLogEntry() {
		String pid = "myPid";
		when(mockAuditService.getConfigurationAuditInfo(pid)).thenReturn(mockAdditionalInformationMap);

		auditConfigurationModelListener.onAfterDelete(pid);

		verify(mockAuditService, times(1)).addAuditLogEntry(ConfigurationModelListener.class.getName(), 0l, EventTypes.DELETE, mockAdditionalInformationMap);

	}

	@Test
	public void onAfterSave_WhenNoError_ThenAddsAuditLogEntry() {
		String pid = "myPid";
		when(mockAuditService.getConfigurationAuditInfo(pid, mockProperties)).thenReturn(mockAdditionalInformationMap);

		auditConfigurationModelListener.onAfterSave(pid, mockProperties);

		verify(mockAuditService, times(1)).addAuditLogEntry(ConfigurationModelListener.class.getName(), 0l, EventTypes.UPDATE, mockAdditionalInformationMap);
	}

}
