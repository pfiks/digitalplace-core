/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.dataconsumer.service.impl;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.aop.AopService;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.service.base.DataConsumerLocalServiceBaseImpl;

/**
 * The implementation of the data consumer local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.dataconsumer.service.DataConsumerLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DataConsumerLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.dataconsumer.model.DataConsumer", service = AopService.class)
public class DataConsumerLocalServiceImpl extends DataConsumerLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.dataconsumer.service.
	 * DataConsumerLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.dataconsumer.service.
	 * DataConsumerLocalServiceUtil</code>.
	 */

	@Override
	public DataConsumer createDataConsumer() {
		String className = DataConsumer.class.getName();
		long nextId = counterLocalService.increment(className, 1);
		return dataConsumerLocalService.createDataConsumer(nextId);
	}

	@Override
	public <T> T dslQuery(DSLQuery dslQuery) {
		return null;
	}

	@Override
	public int dslQueryCount(DSLQuery dslQuery) {
		return 0;
	}

	@Override
	public List<DataConsumer> getDataConsumersByCompanyIdAndGroupId(long companyId, long groupId) {
		return dataConsumerPersistence.findByCompanyIdAndGroupId(companyId, groupId);
	}

	@Override
	public List<DataConsumer> getDataConsumersByDDMFormInstanceId(long ddmFormInstanceId) {
		return dataConsumerPersistence.findByDDMFormInstanceId(ddmFormInstanceId);
	}
}