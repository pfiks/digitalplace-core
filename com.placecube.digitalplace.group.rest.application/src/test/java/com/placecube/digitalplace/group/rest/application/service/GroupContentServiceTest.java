package com.placecube.digitalplace.group.rest.application.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.dynamic.data.lists.constants.DDLRecordConstants;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordService;
import com.liferay.dynamic.data.lists.service.DDLRecordSetService;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.service.permission.ModelPermissions;
import com.liferay.portal.search.constants.SearchContextAttributes;
import com.pfiks.common.permission.PermissionUtil;
import com.placecube.digitalplace.ddl.constants.DDLIndexerField;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;
import com.placecube.digitalplace.group.rest.application.model.FileAttachment;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ FileAttachment.class, RestrictionsFactoryUtil.class, PermissionThreadLocal.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class GroupContentServiceTest extends PowerMockito {

	@InjectMocks
	private GroupContentService groupContentService;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private Company mockCompany;

	@Mock
	private CompanyLocalService mockCompanyLocalService;

	@Mock
	private Criterion mockCriterion;

	@Mock
	private DDLContentParsingService mockDDLContentParsingService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordService mockDDLRecordService;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private List<DDLRecordSet> mockDDLRecordSets;

	@Mock
	private DDLRecordSetService mockDDLRecordSetService;

	@Mock
	private DDMForm mockDDMForm;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DLFileEntry mockDLFileEntry;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private FileAttachment mockFileAttachment1;

	@Mock
	private Set<FileAttachment> mockFileAttachments;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupContentSearchHelper mockGroupContentSearchHelper;

	@Mock
	private GroupFileService mockGroupFileService;

	@Mock
	private Hits mockHits;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private ModelPermissions mockModelPermissions;

	@Mock
	private ModelResourcePermission<DDLRecordSet> mockModelResourcePermission;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private PermissionUtil mockPermissionUtil;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServiceContext mockServiceContextCloned;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetUp() {
		mockStatic(RestrictionsFactoryUtil.class, FileAttachment.class, PermissionThreadLocal.class);
	}

	@Test(expected = PortalException.class)
	public void addDDLRecord_WhenException_ThenRollsBackAttachmentsCreationAndThrowsException() throws Exception {
		long userId = 1L;
		long groupId = 2L;
		String ddlRecordJSON = "myDdlRecordJson";
		long[] categoryIds = { 11, 22, 33 };
		String[] tagNames = { "tag1", "tag2", "tag3" };

		when(mockDDLRecordSet.getDDMStructure()).thenThrow(new PortalException());

		groupContentService.addDDLRecord(userId, groupId, mockDDLRecordSet, ddlRecordJSON, mockFileAttachments, categoryIds, tagNames, mockServiceContext);

		verify(mockGroupFileService, times(1)).rollbackAttachmentsCreation(mockFileAttachments);
	}

	@Test
	public void addDDLRecord_WhenNoError_ThenReturnsTheNewDDLRecord() throws Exception {
		long userId = 1L;
		long groupId = 2L;
		long ddlRecordSetId = 4L;
		String ddlRecordJSON = "myDdlRecordJson";
		long[] categoryIds = { 11, 22, 33 };
		String[] tagNames = { "tag1", "tag2", "tag3" };
		String jsonWithIds = "myjsonWithIds";
		String jsonToUse = "myjsonToUse";

		when(mockDDLRecordSet.getRecordSetId()).thenReturn(ddlRecordSetId);
		when(mockDDLRecordSet.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getFullHierarchyDDMForm()).thenReturn(mockDDMForm);
		when(mockDDLContentParsingService.addInstanceIdsToDDLRecordJSON(ddlRecordJSON)).thenReturn(jsonWithIds);
		when(mockDDLContentParsingService.getContentWithAttachmentDetails(jsonWithIds, mockFileAttachments)).thenReturn(jsonToUse);
		when(mockDDLContentParsingService.deserializeDDLRecordJSON(jsonToUse, mockDDMForm)).thenReturn(mockDDMFormValues);
		when(mockDDLRecordService.addRecord(groupId, ddlRecordSetId, DDLRecordConstants.DISPLAY_INDEX_DEFAULT, mockDDMFormValues, mockServiceContext)).thenReturn(mockDDLRecord);

		DDLRecord result = groupContentService.addDDLRecord(userId, groupId, mockDDLRecordSet, ddlRecordJSON, mockFileAttachments, categoryIds, tagNames, mockServiceContext);

		assertThat(result, sameInstance(mockDDLRecord));
		InOrder inOrder = inOrder(mockServiceContext, mockDDLRecordService);
		inOrder.verify(mockServiceContext, times(1)).setAssetCategoryIds(categoryIds);
		inOrder.verify(mockServiceContext, times(1)).setAssetTagNames(tagNames);
		inOrder.verify(mockDDLRecordService, times(1)).addRecord(groupId, ddlRecordSetId, DDLRecordConstants.DISPLAY_INDEX_DEFAULT, mockDDMFormValues, mockServiceContext);
		verifyZeroInteractions(mockGroupFileService);
	}

	@Test
	public void configureSearchContextWithCreatorUserId_WhenUserIdGreaterThanZero_ThenFilterByUserIdIsAddedToSearchContext() {
		Long userId = 1l;

		groupContentService.configureSearchContextWithCreatorUserId(mockSearchContext, userId);

		verify(mockGroupContentSearchHelper, times(1)).addBooleanQuery(mockSearchContext, Field.USER_ID, String.valueOf(userId), BooleanClauseOccur.MUST);
	}

	@Test
	public void configureSearchContextWithCreatorUserId_WhenUserIdNotGreaterThanZero_ThenNothingIsAddedToSearchContext() {
		groupContentService.configureSearchContextWithCreatorUserId(mockSearchContext, 0l);

		verifyZeroInteractions(mockSearchContext, mockGroupContentSearchHelper);
	}

	@Test
	public void configureSearchContextWithCreatorUserId_WhenUserIdNull_ThenNothingIsAddedToSearchContext() {
		groupContentService.configureSearchContextWithCreatorUserId(mockSearchContext, null);

		verifyZeroInteractions(mockSearchContext, mockGroupContentSearchHelper);
	}

	@Test
	public void executeSearch_WhenNoError_ThenExecutesTheSearchWithTheEvaluatedStartResults() throws SearchException {
		int resultsPerPage = 12;
		int startPage = 34;
		Integer start = 44;
		when(mockGroupContentSearchHelper.evaluateStartResultBasedOnCurrentPageAndDelta(startPage, resultsPerPage)).thenReturn(start);
		when(mockGroupContentSearchHelper.executeSearch(mockSearchContext, start, resultsPerPage)).thenReturn(mockHits);

		Hits result = groupContentService.executeSearch(mockSearchContext, startPage, resultsPerPage);

		assertThat(result, sameInstance(mockHits));
	}

	@Test
	public void fetchUser_WhenNoUserFound_ThenReturnsNull() {
		long companyId = 1;
		String email = "myEmail";
		when(mockGroup.getCompanyId()).thenReturn(companyId);
		when(mockUserLocalService.fetchUserByEmailAddress(companyId, email)).thenReturn(null);

		User result = groupContentService.fetchUser(mockGroup, email);

		assertNull(result);
	}

	@Test
	public void getDDLRecordSet_WhenNoError_ThenReturnsTheRecordSet() throws PortalException {
		long id = 1;
		when(mockDDLRecordSetService.getRecordSet(id)).thenReturn(mockDDLRecordSet);

		DDLRecordSet result = groupContentService.getDDLRecordSet(id);

		assertThat(result, sameInstance(mockDDLRecordSet));
	}

	@Test
	public void getDDLRecordSets_WhenNoError_ThenReturnsTheRecordSetsForTheGroup() {
		long id = 123l;
		when(mockGroup.getGroupId()).thenReturn(id);
		when(mockDDLRecordSetService.getRecordSets(new long[] { id })).thenReturn(mockDDLRecordSets);

		List<DDLRecordSet> results = groupContentService.getDDLRecordSets(mockGroup);

		assertThat(results, sameInstance(mockDDLRecordSets));
	}

	@Test
	public void getGroupCategories_WhenNoError_ThenConfiguresTheDynamicQuery() {
		long groupId = 123L;
		when(mockAssetCategoryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(RestrictionsFactoryUtil.eq("groupId", groupId)).thenReturn(mockCriterion);

		groupContentService.getGroupCategories(mockGroup);

		InOrder inOrder = inOrder(mockAssetCategoryLocalService, mockDynamicQuery);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterion);
		inOrder.verify(mockAssetCategoryLocalService, times(1)).dynamicQuery(mockDynamicQuery);
	}

	@Test
	public void getGroupCategories_WhenNoError_ThenReturnsListOfCategories() {
		when(mockAssetCategoryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		List<Object> expected = new ArrayList<>();
		expected.add(mockAssetCategory);
		when(mockAssetCategoryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(expected);

		List<AssetCategory> results = groupContentService.getGroupCategories(mockGroup);

		assertThat(results, sameInstance(expected));
	}

	@Test
	public void getPortalURL_WhenNoError_ThenReturnsTheCompanyPortalURL() throws PortalException {
		String expected = "expected";
		long companyId = 123L;
		long groupId = 444L;
		when(mockGroup.getCompanyId()).thenReturn(companyId);
		when(mockCompany.getGroupId()).thenReturn(groupId);
		when(mockCompanyLocalService.getCompany(companyId)).thenReturn(mockCompany);
		when(mockCompany.getPortalURL(groupId)).thenReturn(expected);

		String result = groupContentService.getPortalURL(mockGroup);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSearchContextForAdminDynamicDataListSearch_WhenStructureIdGreaterThanZero_ThenReturnsTheSearchContextWithTheStructureIdFilter() {
		long userId = 123;
		long structureId = 456;
		when(mockGroupContentSearchHelper.getBaseSearchContext(mockGroup, userId)).thenReturn(mockSearchContext);

		SearchContext result = groupContentService.getSearchContextForAdminDynamicDataListSearch(userId, mockGroup, structureId);

		assertThat(result, sameInstance(mockSearchContext));
		verify(mockSearchContext, times(1)).setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, true);
		verify(mockGroupContentSearchHelper, times(1)).addBooleanQuery(mockSearchContext, DDLIndexerField.DDM_STRUCTURE_ID, String.valueOf(structureId), BooleanClauseOccur.MUST);
	}

	@Test
	public void getSearchContextForAdminDynamicDataListSearch_WhenStructureIdNotGreaterThanZero_ThenReturnsTheSearchContextWithoutTheStructureIdFilter() {
		long userId = 123;
		when(mockGroupContentSearchHelper.getBaseSearchContext(mockGroup, userId)).thenReturn(mockSearchContext);

		SearchContext result = groupContentService.getSearchContextForAdminDynamicDataListSearch(userId, mockGroup, 0);

		assertThat(result, sameInstance(mockSearchContext));
		verify(mockSearchContext, times(1)).setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, true);
	}

	@Test
	public void getServiceContextAndRunAsUser_WhenNoError_ThenRunsAsUserAndReturnsTheNewServiceContext() throws Exception {
		long userId = 1;
		long groupId = 2l;
		long companyId = 3;
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockGroup.getCompanyId()).thenReturn(companyId);

		ServiceContext result = groupContentService.getServiceContextAndRunAsUser(mockGroup, userId);

		assertThat(result.getScopeGroupId(), equalTo(groupId));
		assertThat(result.getUserId(), equalTo(userId));
		assertThat(result.getCompanyId(), equalTo(companyId));
		verify(mockPermissionUtil, times(1)).runAsUser(userId);
	}

	@Test
	public void getUser_WhenUserFound_ThenReturnsTheUser() {
		long companyId = 1;
		String email = "myEmail";
		when(mockGroup.getCompanyId()).thenReturn(companyId);
		when(mockUserLocalService.fetchUserByEmailAddress(companyId, email)).thenReturn(mockUser);

		User result = groupContentService.fetchUser(mockGroup, email);

		assertThat(result, sameInstance(mockUser));

	}

	@Test(expected = PortalException.class)
	public void updateDDLRecordAssetEntries_WhenUserIsNotTheOwnerAndDoesNotHaveUpdatePermissions_ThenThrowsException() throws Exception {
		long userId = 100L;
		long recordSetId = 400l;
		long[] categoryIds = { 11L, 22L, 33L };
		String[] tagNames = { "Tag 1", "Tag 2", "Tag 3" };
		when(mockDDLRecord.getUserId()).thenReturn(89898989l);
		when(PermissionThreadLocal.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);
		doThrow(new PortalException()).when(mockModelResourcePermission).check(mockPermissionChecker, recordSetId, ActionKeys.UPDATE);

		groupContentService.updateDDLRecordAssetEntries(userId, mockDDLRecord, categoryIds, tagNames);

		verify(mockPermissionUtil, times(1)).runAsUser(userId);
	}

	@Test
	public void updateDDLRecordAssetEntries_WhenUserIsNotTheOwnerButHasUpdatePermissionOnTheDDLRecordSet_ThenUpdateDDLRecordCategoriesAndTags() throws Exception {
		long userId = 100L;
		long groupId = 200L;
		long ddlRecordId = 300L;
		long recordSetId = 400l;
		String className = DDLRecord.class.getName();
		long[] categoryIds = { 11L, 22L, 33L };
		String[] tagNames = { "Tag 1", "Tag 2", "Tag 3" };
		when(mockDDLRecord.getUserId()).thenReturn(898989l);
		when(PermissionThreadLocal.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);
		when(mockDDLRecord.getGroupId()).thenReturn(groupId);
		when(mockDDLRecord.getRecordId()).thenReturn(ddlRecordId);

		groupContentService.updateDDLRecordAssetEntries(userId, mockDDLRecord, categoryIds, tagNames);

		InOrder inOrder = inOrder(mockPermissionUtil, mockAssetEntryLocalService, mockModelResourcePermission);
		inOrder.verify(mockPermissionUtil, times(1)).runAsUser(userId);
		inOrder.verify(mockModelResourcePermission, times(1)).check(mockPermissionChecker, recordSetId, ActionKeys.UPDATE);
		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(userId, groupId, className, ddlRecordId, categoryIds, tagNames);
	}

	@Test
	public void updateDDLRecordAssetEntries_WhenUserIsTheOwner_ThenUpdateDDLRecordCategoriesAndTags() throws Exception {
		long userId = 100L;
		long groupId = 200L;
		long ddlRecordId = 300L;
		String className = DDLRecord.class.getName();
		long[] categoryIds = { 11L, 22L, 33L };
		String[] tagNames = { "Tag 1", "Tag 2", "Tag 3" };

		when(mockDDLRecord.getUserId()).thenReturn(userId);
		when(mockDDLRecord.getGroupId()).thenReturn(groupId);
		when(mockDDLRecord.getRecordId()).thenReturn(ddlRecordId);

		groupContentService.updateDDLRecordAssetEntries(userId, mockDDLRecord, categoryIds, tagNames);

		InOrder inOrder = inOrder(mockPermissionUtil, mockAssetEntryLocalService);
		inOrder.verify(mockPermissionUtil, times(1)).runAsUser(userId);
		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(userId, groupId, className, ddlRecordId, categoryIds, tagNames);
	}

	@Test
	public void uploadDDLAttachments_WhenNoError_ThenReturnsAllUploadedAttachments() throws Exception {
		String attachmentFiles = "myattachmentFiles";
		when(mockDDLContentParsingService.getSafeJSONArray(attachmentFiles)).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject);
		when(mockServiceContext.clone()).thenReturn(mockServiceContextCloned);
		when(mockGroupFileService.createDLFileEntry(mockJSONObject, mockServiceContextCloned)).thenReturn(mockDLFileEntry);
		when(mockGroupFileService.getModelPermissions(mockDDLRecordSet)).thenReturn(mockModelPermissions);
		when(mockJSONObject.getString("placeholderKey")).thenReturn("placeholderKeyValue");
		when(mockDDLContentParsingService.getJSONObjectForAttachment(mockDLFileEntry)).thenReturn("myAttachmentJsonStringValue");
		when(FileAttachment.init("placeholderKeyValue", mockDLFileEntry, "myAttachmentJsonStringValue")).thenReturn(mockFileAttachment1);

		Set<FileAttachment> results = groupContentService.uploadDDLAttachments(mockGroup, attachmentFiles, mockDDLRecordSet, mockServiceContext);

		assertThat(results, contains(mockFileAttachment1));
		InOrder inOrder = inOrder(mockServiceContextCloned, mockGroupFileService);
		inOrder.verify(mockServiceContextCloned, times(1)).setModelPermissions(mockModelPermissions);
		inOrder.verify(mockServiceContextCloned, times(1)).setAttribute("actionKey", ActionKeys.ADD_ATTACHMENT);
		inOrder.verify(mockGroupFileService, times(1)).createDLFileEntry(mockJSONObject, mockServiceContextCloned);
	}

	@Test(expected = GroupRestSecurityException.class)
	public void validateParameters_WhenStartPageIsLessThanZero_ThenThrowsGroupRestSecurityException() {
		groupContentService.validateParameters(-1, 2, 3);
	}

	@Test
	public void validateParameters_WhenStartPageIsNullAndResultsPerPageIsValidAndTrucateDescriptionIsNull_ThenNoExceptionIsThrown() {
		try {
			groupContentService.validateParameters(null, 1, null);
		} catch (Exception e) {
			fail();
		}
	}

	@Test(expected = GroupRestSecurityException.class)
	@Parameters({ "0,-1", "1,0" })
	public void validateParameters_WhenStartPageIsValidAndResultsPerPageIsNotGreaterThanZero_ThenThrowsGroupRestSecurityException(Integer startPage, Integer resultsPerPage) {
		groupContentService.validateParameters(startPage, resultsPerPage, 3);
	}

	@Test
	@Parameters({ "0,2,1", "1,1,3" })
	public void validateParameters_WhenStartPageIsValidAndResultsPerPageIsValidAndTrucateDescriptionIsGreaterThanZero_ThenNoExceptionIsThrown(Integer startPage, Integer resultsPerPage,
			Integer truncateDescription) {
		try {
			groupContentService.validateParameters(startPage, resultsPerPage, truncateDescription);
		} catch (Exception e) {
			fail();
		}
	}

	@Test(expected = GroupRestSecurityException.class)
	@Parameters({ "0,1,-1", "1,1,0" })
	public void validateParameters_WhenStartPageIsValidAndResultsPerPageIsValidAndTrucateDescriptionIsNotGreaterThanZero_ThenThrowsGroupRestSecurityException(Integer startPage, Integer resultsPerPage,
			Integer truncateDescription) {
		groupContentService.validateParameters(startPage, resultsPerPage, truncateDescription);
	}

}
