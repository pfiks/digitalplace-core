package com.placecube.digitalplace.search.internal.indexer;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.search.spi.settings.IndexSettingsContributor;
import com.liferay.portal.search.spi.settings.TypeMappingsHelper;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
public class GeoLocationIndexSettingsContributorTest extends PowerMockito {

	private static final String INDEX_NAME = "index=name";

	private GeoLocationIndexSettingsContributor geoLocationIndexSettingsContributor = new GeoLocationIndexSettingsContributor();

	@Mock
	private IndexSettingsContributor mockIndexSettingsContributor;

	@Mock
	private TypeMappingsHelper mockTypeMappingsHelper;

//	@Test
//	public void compareTo_WhenContribbutorClassNameIsNotTheSame_ThenReturnsNonZero() {
//
//		int result = geoLocationIndexSettingsContributor.compareTo(mockIndexSettingsContributor);
//
//		assertThat(result, not(0));
//
//	}

//	@Test
//	public void compareTo_WhenContributorClassNameIsTheSame_ThenReturnsZero() {
//
//		int result = geoLocationIndexSettingsContributor.compareTo(new GeoLocationIndexSettingsContributor());
//
//		assertThat(result, equalTo(0));
//
//	}

	@Test
	public void contribute_WhenErrorReadingMappingsFile_ThenNotSetsMappingConfigOnTypeMappingsHelper() throws IOException {
		when(StringUtil.read(getClass().getClassLoader(), "META-INF/resources/mappings/index-mappings.json")).thenThrow(new IOException());

		geoLocationIndexSettingsContributor.contribute(INDEX_NAME, mockTypeMappingsHelper);

		verifyZeroInteractions(mockTypeMappingsHelper);
	}

	@Test
	public void contribute_WhenNoErrorReadingMappingsFile_ThenSetsMappingConfigOnTypeMappingsHelper() throws IOException {
		String mappingsConfig = "{'mapping':'geolocation-settings}')";
		when(StringUtil.read(getClass().getClassLoader(), "META-INF/resources/mappings/index-mappings.json")).thenReturn(mappingsConfig);

		geoLocationIndexSettingsContributor.contribute(INDEX_NAME, mockTypeMappingsHelper);

		verify(mockTypeMappingsHelper, times(1)).addTypeMappings(INDEX_NAME, mappingsConfig);
	}

//	@Test
//	public void getPriority_WhenCalled_ThenReturnsZero() {
//
//		int result = geoLocationIndexSettingsContributor.getPriority();
//
//		assertThat(result, equalTo(0));
//
//	}

	@Before
	public void setUp() {

		mockStatic(StringUtil.class);
	}

}
