<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.petra.string.StringUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ include file="init.jsp"%>


<c:choose>
	<c:when test = "${ accountCreationDisabled }">
		<div class="alert alert-danger">
			<liferay-ui:message key="account-creation-is-not-currently-enabled" />
		</div>
	</c:when>

	 <c:otherwise>
		<c:set var="progressBarValue" value="25"/>
		<%@ include file="/fragments/progress-bar.jspf" %>
		
		<div class="row">
			<%@ include file="/fragments/side-bar.jspf" %>
			<div class="col-md-8"> 
				<portlet:actionURL name="<%=MVCCommandKeys.PERSONAL_DETAILS%>" var="personalDetailsURL" />
				<gds-forms-ui:errorSummary portletNamespace="${portletNamespace}" errors="${accountContext.getErrors()}"/>
				<aui:form action="${personalDetailsURL}" method="post" name="createAccountForm">
					<%@ include file="/fragments/personal-fields.jspf" %>
					<aui:button-row cssClass="pull-right">
						<aui:button type="submit" cssClass="btn btn-primary" value="next" icon="glyphicon glyphicon-chevron-right" iconAlign="right"/>
					</aui:button-row>
				</aui:form>
				
			</div>
		</div>
	</c:otherwise>
</c:choose>