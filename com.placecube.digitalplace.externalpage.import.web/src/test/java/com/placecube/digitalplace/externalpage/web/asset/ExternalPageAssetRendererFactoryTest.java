package com.placecube.digitalplace.externalpage.web.asset;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.asset.kernel.model.BaseAssetRenderer" })
public class ExternalPageAssetRendererFactoryTest extends PowerMockito {

	@InjectMocks
	private ExternalPageAssetRendererFactory externalPageAssetRendererFactory;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ExternalPage mockExternalPage;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void getAssetRenderer_WhenExceptionRetrievingPage_ThenThrowsPortalException() throws PortalException {
		long externalPageId = 123;
		int type = 77;
		when(mockExternalPageLocalService.getExternalPage(externalPageId)).thenThrow(new PortalException());

		externalPageAssetRendererFactory.getAssetRenderer(externalPageId, type);
	}

	@Test
	public void getAssetRenderer_WhenNoError_ThenReturnsTheExternalPageAssetRenderer() throws PortalException {
		long externalPageId = 123;
		int type = 77;
		when(mockExternalPageLocalService.getExternalPage(externalPageId)).thenReturn(mockExternalPage);

		AssetRenderer<ExternalPage> result = externalPageAssetRendererFactory.getAssetRenderer(externalPageId, type);

		assertThat(result.getClass().getName(), equalTo(ExternalPageAssetRenderer.class.getName()));
		assertThat(result.getAssetObject(), sameInstance(mockExternalPage));
	}

	@Test
	public void getClassName_WhenNoError_ThenReturnsTheClassName() {
		String result = externalPageAssetRendererFactory.getClassName();

		assertThat(result, equalTo(ExternalPage.class.getName()));
	}

	@Test
	public void getPortletId_WhenNoError_ThenReturnsThePortletId() {
		String result = externalPageAssetRendererFactory.getPortletId();

		assertThat(result, equalTo(PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET));
	}

	@Test
	public void getType_WhenNoError_ThenReturnsTheClassName() {
		String result = externalPageAssetRendererFactory.getType();

		assertThat(result, equalTo(ExternalPage.class.getName()));
	}

	@Test
	public void hasPermission_WhenNoError_ThenReturnsTrue() throws Exception {
		boolean result = externalPageAssetRendererFactory.hasPermission(mockPermissionChecker, 123, "actionId");

		assertTrue(result);
		verifyZeroInteractions(mockPermissionChecker);
	}

	@Test
	public void isActive_WhenNoError_ThenReturnsTrue() throws Exception {
		boolean result = externalPageAssetRendererFactory.isActive(456);

		assertTrue(result);
	}

	@Test
	public void isSearchable_WhenNoError_ThenReturnsTrue() throws Exception {
		boolean result = externalPageAssetRendererFactory.isSearchable();

		assertTrue(result);
	}

}
