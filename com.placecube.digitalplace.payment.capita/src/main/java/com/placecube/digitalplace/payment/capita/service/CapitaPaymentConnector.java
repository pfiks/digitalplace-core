package com.placecube.digitalplace.payment.capita.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.payment.capita.service.internal.OneOffPaymentOrderFactory;
import com.placecube.digitalplace.payment.capita.service.internal.OneOffPaymentOrderFileService;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.service.PaymentService;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;
import com.placecube.digitalplace.payment.service.PaymentConnector;

@Component(immediate = true, service = PaymentConnector.class)
public class CapitaPaymentConnector implements PaymentConnector {

	private static final Log LOG = LogFactoryUtil.getLog(CapitaPaymentConnector.class);

	@Reference
	private CapitaPaymentService capitaPaymentService;

	@Reference
	private OneOffPaymentOrderFactory oneOffPaymentOrderFactory;

	@Reference
	private OneOffPaymentOrderFileService oneOffPaymentOrderFileService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return capitaPaymentService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Optional<String> getBackURL(String paymentReference, ServiceContext serviceContext) {
		Optional<OneOffPaymentOrder> oneOffPaymentOrderOpt = getOneOffPaymentOrderFile(paymentReference, serviceContext);

		Optional<String> backUrlOpt = Optional.empty();

		if (oneOffPaymentOrderOpt.isPresent()) {

			backUrlOpt = Optional.ofNullable(oneOffPaymentOrderOpt.get().getBackURL());
		}

		return backUrlOpt;
	}

	@Override
	public Optional<String> getDefaultAdvancedConfiguration(long companyId) {
		try {
			return capitaPaymentService.getDefaultAdvancedConfiguration(companyId);
		} catch (ConfigurationException e) {
			LOG.error(e);
			return Optional.empty();
		}
	}

	@Override
	public PaymentStatus getPaymentStatus(String paymentReference, ServiceContext serviceContext) {

		PaymentStatus paymentStatus = PaymentStatus.success();

		Optional<OneOffPaymentOrder> oneOffPaymentOrderOpt = getOneOffPaymentOrderFile(paymentReference, serviceContext);

		if (oneOffPaymentOrderOpt.isPresent()) {

			// Query Capita for the current status of the transaction
			OneOffPaymentOrder oneOffPaymentOrder = oneOffPaymentOrderOpt.get();
			PaymentService paymentService = capitaPaymentService.getPaymentService(serviceContext);
			boolean successful = paymentService.invokeQueryServiceOneOffPaymentOrder(oneOffPaymentOrder);

			if (successful) {
				paymentStatus.setPaymentUniqueReferenceNumber(String.valueOf(oneOffPaymentOrder.getContinuousAuditNumber()));
			} else {
				LOG.error("Error getting payment information from Capita: Transaction Reference Number(" + oneOffPaymentOrder.getTransactionReferenceNumber() + "), Payment Status ("
						+ oneOffPaymentOrder.getStatus() + ")");
				paymentStatus = PaymentStatus.fromValue(oneOffPaymentOrder.getStatus());
			}

		} else {
			paymentStatus = PaymentStatus.appError();
		}

		return paymentStatus;
	}

	@Override
	public PaymentResponse preparePayment(PaymentContext paymentContext, ServiceContext serviceContext) {

		try {

			PaymentService paymentService = capitaPaymentService.getPaymentService(serviceContext);
			OneOffPaymentOrder oneOffOrder = oneOffPaymentOrderFactory.createOneOffPaymentOrder(paymentContext);

			LOG.debug(oneOffOrder);

			Optional<ThemeDisplay> themeDisplayOpt = Optional.ofNullable(serviceContext.getThemeDisplay());
			String setUpPaymentSuccessful = paymentService.invokePaymentService(oneOffOrder, paymentContext.getRealUserId(themeDisplayOpt), paymentContext.isImpersonated(themeDisplayOpt));

			LOG.debug("[setUpPayment] PAYMENT SETUP WAS: " + setUpPaymentSuccessful);

			if (PaymentStatus.paymentError().getValue().equals(setUpPaymentSuccessful)) {
				return new PaymentResponse(StringPool.BLANK, PaymentStatus.paymentError());
			}
			oneOffPaymentOrderFileService.uploadPaymentTempFile(oneOffOrder, serviceContext);
			return new PaymentResponse(oneOffOrder.getRedirectUrl(), PaymentStatus.success());

		} catch (Exception e) {
			LOG.error(e);
			return new PaymentResponse(StringPool.BLANK, PaymentStatus.appError());
		}
	}

	private Optional<OneOffPaymentOrder> getOneOffPaymentOrderFile(String paymentReference, ServiceContext serviceContext) {
		try {
			return oneOffPaymentOrderFileService.loadPaymentTempFile(paymentReference, serviceContext);
		} catch (Exception e) {
			LOG.error(e);
			return Optional.empty();
		}
	}

}
