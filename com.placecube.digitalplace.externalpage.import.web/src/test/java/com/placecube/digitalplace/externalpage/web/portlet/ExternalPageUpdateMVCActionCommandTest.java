package com.placecube.digitalplace.externalpage.web.portlet;

import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ SessionErrors.class, PropsUtil.class, ParamUtil.class, ExternalPageUpdateMVCActionCommandTest.class, ExternalPage.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.util.ParamUtil")
public class ExternalPageUpdateMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	ExternalPageUpdateMVCActionCommand externalPageUpdateMVCActionCommand = new ExternalPageUpdateMVCActionCommand();

	@Mock
	ActionRequest mockActionRequest;

	@Mock
	ActionResponse mockActionResponse;

	@Mock
	ExternalPage mockExternalPage;

	@Mock
	ExternalPageLocalService mockExternalPageLocalService;

	@Before
	public void activeSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, SessionErrors.class);
	}

	@Test
	public void doProcessAction_WhenExternalPageIdIsGreaterThanZero_ThenUpdateExternalPage() throws Exception {
		long externalPageId = 123;
		String title = "abc";
		String content = "xyz";
		when(ParamUtil.getLong(mockActionRequest, "externalPageId")).thenReturn(externalPageId);
		when(ParamUtil.getString(mockActionRequest, "title")).thenReturn(title);
		when(ParamUtil.getString(mockActionRequest, "content")).thenReturn(content);
		when(mockExternalPageLocalService.fetchExternalPage(externalPageId)).thenReturn(mockExternalPage);
		externalPageUpdateMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
		verify(mockExternalPage, times(1)).setContent(content);
		verify(mockExternalPage, times(1)).setTitle(title);
		verify(mockExternalPageLocalService, times(1)).updateExternalPage(mockExternalPage);
	}

	@Test(expected = PortalException.class)
	@Parameters({"-1", "0"})
	public void doProcessAction_WhenExternalPageIdIsLessThanOrEqualToZero_ThenThrowPortalException(long externalPageId) throws Exception {
		when(ParamUtil.getLong(mockActionRequest, "externalPageId")).thenReturn(externalPageId);
		
		externalPageUpdateMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
		
		verifyZeroInteractions(mockExternalPageLocalService);
	}
}
