package com.placecube.digitalplace.group.rest.application.filters;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.group.rest.application.GroupRestApplication;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;
import com.placecube.digitalplace.group.rest.application.service.RequestFiltersService;

/**
 * Filter to validate the Secure requests Headers Required Headers are:
 * X-Api-Key, X-Date, X-Signature.
 * <p>
 * If any of the required headers is missing, response status is set to
 * SC_BAD_REQUEST (400)
 */
@Provider
@PreMatching
public class HeaderCheckerFilter implements ContainerRequestFilter {

	private static final Log LOG = LogFactoryUtil.getLog(HeaderCheckerFilter.class);

	private RequestFiltersService filterCheckersService;

	public HeaderCheckerFilter(GroupRestApplication restApplication) {
		filterCheckersService = restApplication.getRequestFiltersService();
	}

	@Override
	public void filter(ContainerRequestContext requestContext) {
		String acceptHeader = filterCheckersService.getAcceptHeader(requestContext);
		String apiKeyHeader = filterCheckersService.getApiKeyHeader(requestContext);
		String dateHeader = filterCheckersService.getDateHeader(requestContext);
		String signatureHeader = filterCheckersService.getSignatureHeader(requestContext);

		if (Validator.isNull(apiKeyHeader) || Validator.isNull(dateHeader) || Validator.isNull(signatureHeader) || invalidAcceptHeader(acceptHeader)) {
			LOG.debug("Missing header. acceptHeader: " + acceptHeader + ", apiKeyHeader: " + apiKeyHeader + ", dateHeader: " + dateHeader + ", signatureHeader: " + signatureHeader);
			throw new GroupRestSecurityException(ErrorCodes.MISSING_HEADERS, Response.Status.BAD_REQUEST);
		}
	}

	private boolean invalidAcceptHeader(String acceptHeader) {
		return Validator.isNull(acceptHeader) || !MediaType.APPLICATION_JSON.equals(acceptHeader);
	}

}
