package com.placecube.digitalplace.fragmentcollectioncontributor;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.fragment.contributor.BaseFragmentCollectionContributor;
import com.liferay.fragment.contributor.FragmentCollectionContributor;

@Component(property = "fragment.collection.key=DIGITAL_PLACE", service = FragmentCollectionContributor.class)
public class DigitalPlaceFragmentContributor extends BaseFragmentCollectionContributor {

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.fragmentcollectioncontributor)")
	private ServletContext servletContext;

	@Override
	public String getFragmentCollectionKey() {
		return "DIGITAL_PLACE";
	}

	@Override
	public ServletContext getServletContext() {
		return servletContext;
	}

}
