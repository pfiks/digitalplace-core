package com.placecube.digitalplace.template.contributor.theme;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

public class ThemeTemplateContextContributorTest extends PowerMockito {

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Layout mockLayout;

	private Map<String, Object> contextObjects;

	private ThemeTemplateContextContributor themeTemplateContextContributor;

	@Before
	public void setUp() {

		initMocks(this);

		when(mockHttpServletRequest.getAttribute(WebKeys.LAYOUT)).thenReturn(mockLayout);

		contextObjects = new HashMap<>();
		themeTemplateContextContributor = new ThemeTemplateContextContributor();

	}

	@Test
	public void prepare_WhenLayoutTypeIsAssetDisplay_ThenAddsAssetDisplayPageBodyCssClassToContextObjects() {

		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);

		themeTemplateContextContributor.prepare(contextObjects, mockHttpServletRequest);

		String bodyCssClass = (String) contextObjects.get("body_css_class");
		assertThat(bodyCssClass, equalTo("asset-display-page"));

	}

	@Test
	public void prepare_WhenLayoutTypeIsAssetDisplay_ThenAddsEmptyContentCssClassToContextObjects() {

		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);

		themeTemplateContextContributor.prepare(contextObjects, mockHttpServletRequest);

		String contentCssClass = (String) contextObjects.get("content_css_class");
		assertThat(contentCssClass, isEmptyString());

	}

	@Test
	public void prepare_WhenLayoutTypeIsContent_ThenAddsContentPageBodyCssClassToContextObjects() {

		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_CONTENT);

		themeTemplateContextContributor.prepare(contextObjects, mockHttpServletRequest);

		String contentCssClass = (String) contextObjects.get("body_css_class");
		assertThat(contentCssClass, equalTo("content-page"));

	}

	@Test
	public void prepare_WhenLayoutTypeIsContent_ThenAddsEmptyContentCssClassToContextObjects() {

		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_CONTENT);

		themeTemplateContextContributor.prepare(contextObjects, mockHttpServletRequest);

		String contentCssClass = (String) contextObjects.get("content_css_class");
		assertThat(contentCssClass, isEmptyString());

	}

	@Test
	public void prepare_WhenLayoutTypeIsPortlet_ThenAddsPortletPageBodyCssClassToContextObjects() {

		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_PORTLET);

		themeTemplateContextContributor.prepare(contextObjects, mockHttpServletRequest);

		String contentCssClass = (String) contextObjects.get("body_css_class");
		assertThat(contentCssClass, equalTo("portlet-page"));

	}

	@Test
	public void prepare_WhenLayoutTypeIsPortlet_ThenAddsPortletPageContentCssClassToContextObjects() {

		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_PORTLET);

		themeTemplateContextContributor.prepare(contextObjects, mockHttpServletRequest);

		String contentCssClass = (String) contextObjects.get("content_css_class");
		assertThat(contentCssClass, equalTo("container"));

	}

	@Test
	public void prepare_WhenLayoutTypeIsNotContentOrPortletOrAssetDisplay_ThenAddsEmptyBodyCssClassToContextObjects() {

		when(mockLayout.getType()).thenReturn(StringPool.BLANK);

		themeTemplateContextContributor.prepare(contextObjects, mockHttpServletRequest);

		String bodyCssClass = (String) contextObjects.get("body_css_class");
		assertThat(bodyCssClass, isEmptyString());

	}

	@Test
	public void prepare_WhenLayoutTypeIsNotContentOrPortletOrAssetDisplay_ThenAddsEmptyContentCssClassToContextObjects() {

		when(mockLayout.getType()).thenReturn(StringPool.BLANK);

		themeTemplateContextContributor.prepare(contextObjects, mockHttpServletRequest);

		String contentCssClass = (String) contextObjects.get("content_css_class");
		assertThat(contentCssClass, isEmptyString());

	}

}
