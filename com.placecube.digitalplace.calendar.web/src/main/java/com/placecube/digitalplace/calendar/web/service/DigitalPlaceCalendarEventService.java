package com.placecube.digitalplace.calendar.web.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarConnector;
import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarEvent;
import com.placecube.digitalplace.calendar.web.constants.DigitalPlaceCalendarConfigurationConstants;
import com.placecube.digitalplace.calendar.web.portlet.configuration.DigitalPlaceCalendarInstanceConfiguration;
import com.placecube.digitalplace.calendar.web.registry.DigitalPlaceCalendarConnectorRegistry;

@Component(immediate = true, service = DigitalPlaceCalendarEventService.class)
public class DigitalPlaceCalendarEventService {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceCalendarEventService.class);

	@Reference
	private DigitalPlaceCalendarConnectorRegistry digitalPlaceCalendarConnectorRegistry;

	@Reference
	private DigitalPlaceCalendarConfigurationService digitalPlaceCalendarConfigurationService;

	public List<DigitalPlaceCalendarEvent> getCalendarEvents(long userId, long[] groupIds, LocalDateTime start, LocalDateTime end, ThemeDisplay themeDisplay) throws PortalException {
		List<DigitalPlaceCalendarEvent> result = new ArrayList<>();

		List<DigitalPlaceCalendarConnector> calendarConnectors = digitalPlaceCalendarConnectorRegistry.getCalendarConnectors();
		for (DigitalPlaceCalendarConnector calendarConnector : calendarConnectors) {
			result.addAll(calendarConnector.getCalendarEvents(userId, groupIds, start, end, themeDisplay));
		}
		return result;
	}

	public long[] getCalendarScopeGroupIds(ThemeDisplay themeDisplay) {
		try {
			DigitalPlaceCalendarInstanceConfiguration configuration = digitalPlaceCalendarConfigurationService.getDigitalPlaceCalendarConfiguration(
					themeDisplay);
			if (DigitalPlaceCalendarConfigurationConstants.SCOPE_GROUP.equals(configuration.scope())) {
				return new long[] { themeDisplay.getScopeGroupId() };
			}

		} catch (ConfigurationException e) {
			LOG.error("Cannot get configuration for groupId " + themeDisplay.getScopeGroupId() + " (" + e.getMessage() + ")");
			LOG.debug(e);
		}

		return  new long[0];
	}

	public DateTimeFormatter getCalendarEventsDateFormatter (ThemeDisplay themeDisplay) {
		if (themeDisplay.getTimeZone().equals(TimeZone.getTimeZone("UTC"))) {
			return DateTimeFormatter.ISO_DATE_TIME;
		} else {
			return DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		}
	}

	public LocalDateTime parseDate (String date, DateTimeFormatter dateTimeFormatter) {
		return LocalDateTime.parse(date, dateTimeFormatter);
	}

}
