package com.placecube.digitalplace.initialiser.service.util;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.initialiser.service.constants.ConfigurationKeys;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.LayoutPageTemplateCollectionInitializer;
import com.placecube.initializer.service.ModelCreationFactory;

@Component(immediate = true, service = PageCreationUtil.class)
public class PageCreationUtil {

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private LayoutContextUtil layoutContextUtil;

	@Reference
	private LayoutPageTemplateCollectionInitializer layoutPageTemplateCollectionInitializer;

	@Reference
	private ModelCreationFactory modelCreationFactory;

	@Reference
	private ParserUtil parserUtil;

	@Reference
	private Portal portal;

	public Map<Integer, Set<PortletContext>> getColumns(JSONArray columnsJSONArray, Map<String, String> prefsPlaceholderValues, ConfigurationKeys configurationKeys) {
		Map<Integer, Set<PortletContext>> columnsConfigs = new HashMap<>();
		if (Validator.isNotNull(columnsJSONArray)) {
			for (int i = 0; i < columnsJSONArray.length(); i++) {
				Set<PortletContext> columnPortlets = new LinkedHashSet<>();
				JSONObject columnJSONObject = columnsJSONArray.getJSONObject(i);
				JSONArray portletsJSONArray = columnJSONObject.getJSONArray("portlets");
				if (Validator.isNotNull(portletsJSONArray)) {
					for (int j = 0; j < portletsJSONArray.length(); j++) {
						JSONObject portletJSONObject = portletsJSONArray.getJSONObject(j);
						Map<String, String> portletPreferences = parserUtil.getPortletPreferencesWithPlaceholders(portletJSONObject, prefsPlaceholderValues, configurationKeys);
						PortletContext portletWithPreferences = parserUtil.getPortletWithPreferences(portletJSONObject, portletPreferences, configurationKeys);
						parserUtil.configureMultivaluedPreferences(portletWithPreferences, portletJSONObject, configurationKeys);
						columnPortlets.add(portletWithPreferences);
					}
				}
				columnsConfigs.put(columnJSONObject.getInt("columnId"), columnPortlets);
			}
		}
		return columnsConfigs;
	}

	public LayoutContext getContentPageToCreate(Class clazz, JSONObject pageJSONObject, Set<FragmentContext> fragments, Set<PortletContext> portlets, Map<String, String> pageTypeSettings) {
		LayoutContext layoutContext = modelCreationFactory.createContentPageLayoutContext(pageJSONObject.getString("pageName"), pageJSONObject.getString("pageFriendlyURL"),
				pageJSONObject.getBoolean("private"));
		layoutContextUtil.addCommonPageDetails(clazz, pageJSONObject, layoutContext);
		layoutContext.setFragmentContexts(fragments);
		layoutContext.setPortletContexts(portlets);
		layoutContext.setTypeSettings(pageTypeSettings);
		layoutContext.setStructureContent(pageJSONObject.getString("contentStructureDefinition"));
		return layoutContext;
	}

	public Set<FragmentContext> getFragments(JSONArray fragmentsJSONArray) {
		Set<FragmentContext> fragments = new LinkedHashSet<>();
		if (Validator.isNotNull(fragmentsJSONArray)) {
			for (int i = 0; i < fragmentsJSONArray.length(); i++) {
				fragments.add(parserUtil.getFragment(fragmentsJSONArray.getJSONObject(i)));
			}
		}
		return fragments;
	}

	@SuppressWarnings("rawtypes")
	public JSONObject getJSONObject(Class clazz, String path) throws PortalException {
		try {
			return jsonFactory.createJSONObject(StringUtil.read(clazz.getClassLoader(), path));
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public LayoutPageTemplateCollection getLayoutPageTemplateCollection(JSONObject pageTemplateJSONObject, ServiceContext serviceContext) throws PortalException {
		String collectionName = pageTemplateJSONObject.getString("collectionName");
		if (Validator.isNotNull(collectionName)) {
			return layoutPageTemplateCollectionInitializer.getOrCreateLayoutPageTemplateCollection(collectionName, serviceContext);
		}
		return null;
	}

	public LayoutContext getLinkToLayoutPageToCreate(Class clazz, JSONObject pageJSONObject) {
		LayoutContext layoutContext = modelCreationFactory.createLayoutContext(pageJSONObject.getString("pageName"), pageJSONObject.getString("pageFriendlyURL"), pageJSONObject.getBoolean("private"));
		layoutContextUtil.addCommonPageDetails(clazz, pageJSONObject, layoutContext);
		layoutContext.setLinkToLayoutFriendlyURL(pageJSONObject.getString("linkToPageURL"));
		return layoutContext;
	}

	public LayoutContext getPageFromPageTemplateToCreate(String pageTitle, String friendlyURL, boolean isPrivate, LayoutPageTemplateEntry layoutPageTemplateEntry) {
		LayoutContext layoutContext = modelCreationFactory.createContentPageLayoutContext(pageTitle, friendlyURL, isPrivate);
		layoutContext.setClassPK(layoutPageTemplateEntry.getLayoutPageTemplateEntryId());
		layoutContext.setClassNameId(portal.getClassNameId(LayoutPageTemplateEntry.class));
		return layoutContext;
	}

	public LayoutContext getPageTemplateToCreate(JSONObject pageJSONObject) {
		return modelCreationFactory.createPageTemplateLayoutContext(pageJSONObject.getString("pageTemplateName"), pageJSONObject.getString("layoutTemplateId"));
	}

	public Set<PortletContext> getPortlets(long groupId, JSONArray portletsJSONArray, JSONArray contentSetPlaceholders, Map<String, String> prefsPlaceholderValues,
			ConfigurationKeys configurationKeys) {
		Set<PortletContext> portlets = new LinkedHashSet<>();
		Map<String, String> contentSets = parserUtil.getContentSets(groupId, contentSetPlaceholders);
		for (int i = 0; i < portletsJSONArray.length(); i++) {
			JSONObject portletJSONObject = portletsJSONArray.getJSONObject(i);
			PortletContext portlet = parserUtil.getPortletWithPlaceholder(portletJSONObject, configurationKeys);
			Map<String, String> portletPreferences = parserUtil.getPortletPreferencesWithPlaceholders(portletJSONObject, prefsPlaceholderValues, configurationKeys);
			parserUtil.configurePortletPreferences(portlet, portletPreferences, contentSets);
			parserUtil.configureMultivaluedPreferences(portlet, portletJSONObject, configurationKeys);
			portlets.add(portlet);
		}
		return portlets;
	}

	public LayoutContext getWidgetPageToCreate(Class clazz, JSONObject pageJSONObject, Map<Integer, Set<PortletContext>> columnsConfigs) {
		LayoutContext layoutContext = modelCreationFactory.createWidgetPageLayoutContext(pageJSONObject.getString("pageName"), pageJSONObject.getString("pageFriendlyURL"),
				pageJSONObject.getBoolean("private"), pageJSONObject.getString("layoutTemplateId"));

		layoutContextUtil.addCommonPageDetails(clazz, pageJSONObject, layoutContext);
		layoutContext.setColumnPortletContexts(columnsConfigs);
		return layoutContext;
	}

}