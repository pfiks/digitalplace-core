package com.placecube.digitalplace.addresslookup.mock.local.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.mock.local.configuration.MockAddressLocalLookupCompanyConfiguration;

@Component(immediate = true, service = LocalAddressRetrievalService.class)
public class LocalAddressRetrievalService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private LocalResponseParserService localResponseParserService;

	public AddressContext getAddressContext(String uprn) {

		if (uprn.length() > 3) {
			String digit = uprn.substring(3, 4);
			String postcode = uprn.replace(digit, StringPool.SPACE);
			return getAddressLookup(localResponseParserService.getDPA(Integer.valueOf(digit), postcode));
		} else {

			try {
				return getAddressLookup(localResponseParserService.getDPA(Integer.valueOf(uprn), uprn));
			} catch (NumberFormatException e) {
				return getAddressLookup(localResponseParserService.getDPA(1, "1"));
			}

		}

	}

	public AddressContext getAddressLookup(JSONObject jsonObject) {
		JSONObject entryJsonObject = jsonObject.getJSONObject("DPA");

		String buildingDetails = localResponseParserService.getBuildingDetails(entryJsonObject);
		String buildingNumber = entryJsonObject.getString("BUILDING_NUMBER", StringPool.BLANK);
		String streetName = localResponseParserService.getStreeetName(entryJsonObject);

		String addressLine1 = localResponseParserService.getAddressLine1(buildingDetails, buildingNumber);
		String addressLine2 = localResponseParserService.getAddressLine2(buildingDetails, buildingNumber, streetName);
		String addressLine3 = localResponseParserService.getAddressLine3(entryJsonObject);

		return localResponseParserService.getAddressContext(entryJsonObject, addressLine1, addressLine2, addressLine3);
	}

	public JSONArray getResults(String postcode) throws PortalException {
		try {
			return localResponseParserService.getResults(postcode);
		} catch (JSONException e) {
			throw new PortalException(e);
		}
	}

	public Integer getWeight(long companyId) throws ConfigurationException {
		MockAddressLocalLookupCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockAddressLocalLookupCompanyConfiguration.class, companyId);
		return configuration.weight();
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		MockAddressLocalLookupCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockAddressLocalLookupCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}

	public boolean isNational(long companyId) throws ConfigurationException {
		MockAddressLocalLookupCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockAddressLocalLookupCompanyConfiguration.class, companyId);
		return configuration.national();
	}

}
