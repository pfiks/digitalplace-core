package com.placecube.digitalplace.pageimportadmin.model;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collections;

import org.junit.Test;

public class RowImportModelTest {

	@Test
	public void addCategoryId_WhenNoError_ThenAddsTheCategoryId() {
		Long expectedId = 123465l;
		RowImportModel rowImportModel = new RowImportModel(1l, 2, "title", "url", Collections.emptyList());
		rowImportModel.addCategoryId(expectedId);

		assertThat(rowImportModel.getCategoryIds(), contains(expectedId));
	}

	@Test
	public void addWarningMessage_WhenNoError_ThenAddsTheWarningMessage() {
		RowImportModel rowImportModel = new RowImportModel(1l, 2, "title", "url", Collections.emptyList());
		String expected = "expectedMessage";
		rowImportModel.addErrorMessage("message");
		rowImportModel.addWarningMessage(expected);

		assertThat(rowImportModel.getWarningMessages(), contains(expected));
	}

	@Test
	public void addErrorMessage_WhenNoError_ThenAddsTheErrorMessage() {
		RowImportModel rowImportModel = new RowImportModel(1l, 2, "title", "url", Collections.emptyList());
		String expected = "expectedMessage";
		rowImportModel.addWarningMessage("message");
		rowImportModel.addErrorMessage(expected);

		assertThat(rowImportModel.getErrorMessages(), contains(expected));
	}

	@Test
	public void isValid_WhenThereAreNoErrorMessages_ThenReturnsTrue() {
		RowImportModel rowImportModel = new RowImportModel(1l, 2, "title", "url", Collections.emptyList());
		rowImportModel.addWarningMessage("message");

		assertTrue(rowImportModel.isValid());
	}

	@Test
	public void isValid_WhenThereAreErrorMessages_ThenReturnsFalse() {
		RowImportModel rowImportModel = new RowImportModel(1l, 2, "title", "url", Collections.emptyList());
		rowImportModel.addErrorMessage("message");

		assertFalse(rowImportModel.isValid());
	}

	@Test
	public void isWebContent_WhenIsInstanceOfWebContentImportModel_ThenReturnsTrue() {
		RowImportModel rowImportModel = new WebContentImportModel(1l, 2, "title", "url", Collections.emptyList());

		assertTrue(rowImportModel.isWebContent());
	}

	@Test
	public void isWebContent_WhenIsNotInstanceOfWebContentImportModel_ThenReturnsFalse() {
		RowImportModel rowImportModel = new RowImportModel(1l, 2, "title", "url", Collections.emptyList());
		rowImportModel.addErrorMessage("message");

		assertFalse(rowImportModel.isWebContent());
	}

	@Test
	public void isPageImport_WhenIsInstanceOfPageImportModel_ThenReturnsTrue() {
		RowImportModel rowImportModel = new PageImportModel(1l, 2, "title", "url", Collections.emptyList());

		assertTrue(rowImportModel.isPageImport());
	}

	@Test
	public void isPageImport_WhenIsNotInstanceOfPageImportModel_ThenReturnsFalse() {
		RowImportModel rowImportModel = new RowImportModel(1l, 2, "title", "url", Collections.emptyList());
		rowImportModel.addErrorMessage("message");

		assertFalse(rowImportModel.isPageImport());
	}
}
