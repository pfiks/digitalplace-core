package com.placecube.digitalplace.group.rest.internal.constants;

public final class RequestHeaders {

	public static final String ACCEPT = "Accept";

	public static final String X_API_KEY = "X-Api-Key";

	public static final String X_DATE = "X-Date";

	public static final String X_SIGNATURE = "X-Signature";

	public static final String X_EVENT = "X-Event";

	private RequestHeaders() {
	}

}
