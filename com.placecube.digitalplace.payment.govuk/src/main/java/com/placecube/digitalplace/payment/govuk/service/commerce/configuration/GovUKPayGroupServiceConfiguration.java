package com.placecube.digitalplace.payment.govuk.service.commerce.configuration;

import aQute.bnd.annotation.metatype.Meta;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

@ExtendedObjectClassDefinition(
	category = "payment", scope = ExtendedObjectClassDefinition.Scope.GROUP
)
@Meta.OCD(
	id = "com.placecube.digitalplace.payment.govuk.service.commerce.configuration.GovUKGroupServiceConfiguration",
	localization = "content/Language",
	name = "commerce-payment-method-govuk-group-service-configuration-name"
)
public interface GovUKPayGroupServiceConfiguration {

	@Meta.AD(name = "key-name", required = false)
	public String keyName();

}