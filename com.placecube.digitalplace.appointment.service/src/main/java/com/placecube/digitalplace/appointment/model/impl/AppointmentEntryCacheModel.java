/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.appointment.model.AppointmentEntry;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing AppointmentEntry in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AppointmentEntryCacheModel
	implements CacheModel<AppointmentEntry>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AppointmentEntryCacheModel)) {
			return false;
		}

		AppointmentEntryCacheModel appointmentEntryCacheModel =
			(AppointmentEntryCacheModel)object;

		if (appointmentEntryId ==
				appointmentEntryCacheModel.appointmentEntryId) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, appointmentEntryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", appointmentEntryId=");
		sb.append(appointmentEntryId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", entryClassPK=");
		sb.append(entryClassPK);
		sb.append(", entryClassNameId=");
		sb.append(entryClassNameId);
		sb.append(", appointmentId=");
		sb.append(appointmentId);
		sb.append(", serviceId=");
		sb.append(serviceId);
		sb.append(", connectorClassName=");
		sb.append(connectorClassName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", appointmentStartDate=");
		sb.append(appointmentStartDate);
		sb.append(", appointmentEndDate=");
		sb.append(appointmentEndDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AppointmentEntry toEntityModel() {
		AppointmentEntryImpl appointmentEntryImpl = new AppointmentEntryImpl();

		if (uuid == null) {
			appointmentEntryImpl.setUuid("");
		}
		else {
			appointmentEntryImpl.setUuid(uuid);
		}

		appointmentEntryImpl.setAppointmentEntryId(appointmentEntryId);
		appointmentEntryImpl.setCompanyId(companyId);
		appointmentEntryImpl.setEntryClassPK(entryClassPK);
		appointmentEntryImpl.setEntryClassNameId(entryClassNameId);

		if (appointmentId == null) {
			appointmentEntryImpl.setAppointmentId("");
		}
		else {
			appointmentEntryImpl.setAppointmentId(appointmentId);
		}

		if (serviceId == null) {
			appointmentEntryImpl.setServiceId("");
		}
		else {
			appointmentEntryImpl.setServiceId(serviceId);
		}

		if (connectorClassName == null) {
			appointmentEntryImpl.setConnectorClassName("");
		}
		else {
			appointmentEntryImpl.setConnectorClassName(connectorClassName);
		}

		if (createDate == Long.MIN_VALUE) {
			appointmentEntryImpl.setCreateDate(null);
		}
		else {
			appointmentEntryImpl.setCreateDate(new Date(createDate));
		}

		if (appointmentStartDate == Long.MIN_VALUE) {
			appointmentEntryImpl.setAppointmentStartDate(null);
		}
		else {
			appointmentEntryImpl.setAppointmentStartDate(
				new Date(appointmentStartDate));
		}

		if (appointmentEndDate == Long.MIN_VALUE) {
			appointmentEntryImpl.setAppointmentEndDate(null);
		}
		else {
			appointmentEntryImpl.setAppointmentEndDate(
				new Date(appointmentEndDate));
		}

		appointmentEntryImpl.resetOriginalValues();

		return appointmentEntryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		appointmentEntryId = objectInput.readLong();

		companyId = objectInput.readLong();

		entryClassPK = objectInput.readLong();

		entryClassNameId = objectInput.readLong();
		appointmentId = (String)objectInput.readObject();
		serviceId = objectInput.readUTF();
		connectorClassName = objectInput.readUTF();
		createDate = objectInput.readLong();
		appointmentStartDate = objectInput.readLong();
		appointmentEndDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(appointmentEntryId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(entryClassPK);

		objectOutput.writeLong(entryClassNameId);

		if (appointmentId == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(appointmentId);
		}

		if (serviceId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(serviceId);
		}

		if (connectorClassName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(connectorClassName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(appointmentStartDate);
		objectOutput.writeLong(appointmentEndDate);
	}

	public String uuid;
	public long appointmentEntryId;
	public long companyId;
	public long entryClassPK;
	public long entryClassNameId;
	public String appointmentId;
	public String serviceId;
	public String connectorClassName;
	public long createDate;
	public long appointmentStartDate;
	public long appointmentEndDate;

}