package com.placecube.digitalplace.formadmin.web.service;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = { SearchContainerService.class })
public class SearchContainerService {

	public SearchContainer getSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse, String noResultsFoundMessage) {
		SearchContainer searchContainer = new SearchContainer<>(renderRequest, getSearchContainerPortletURL(renderResponse, renderRequest), null, noResultsFoundMessage);
		searchContainer.setDeltaConfigurable(true);
		return searchContainer;
	}

	private PortletURL getSearchContainerPortletURL(RenderResponse renderResponse, RenderRequest renderRequest) {
		PortletURL portletURL = renderResponse.createRenderURL();

		String currentTab = ParamUtil.getString(renderRequest, "currentTab");
		if (Validator.isNotNull(currentTab)) {
			portletURL.getRenderParameters().setValue("currentTab", currentTab);
		}
		String mvcRenderCommandName = ParamUtil.getString(renderRequest, "mvcRenderCommandName");
		if (Validator.isNotNull(currentTab)) {
			portletURL.getRenderParameters().setValue("mvcRenderCommandName", mvcRenderCommandName);
		}
		return portletURL;
	}
}
