package com.placecube.digitalplace.gds.taglib.tag;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.gds.taglib.context.ServletContextUtil;

public class InputTypeSelectTag extends IncludeTag {

	private boolean required = false;
	private boolean showEmptyOption = false;
	private String errorMessage;
	private String fieldLabel;
	private String fieldName;
	private Map<String, String> fieldOptions;
	private String fieldValue;
	private String placeholder;
	private String placeholderValue;
	private String portletNamespace;

	@Override
	public int doStartTag() {
		setAttributeNamespace("gds-forms-ui:input-select");
		return EVAL_BODY_INCLUDE;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setFieldOptions(Map<String, String> fieldOptions) {
		this.fieldOptions = fieldOptions;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);
		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

	public void setPlaceholderValue(String placeholderValue) {
		this.placeholderValue = placeholderValue;
	}

	public void setPortletNamespace(String portletNamespace) {
		this.portletNamespace = portletNamespace;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public void setShowEmptyOption(boolean showEmptyOption) {
		this.showEmptyOption = showEmptyOption;
	}

	@Override
	protected void cleanUp() {
		super.cleanUp();
		portletNamespace = null;
		fieldOptions = new HashMap<>();
		fieldName = null;
		fieldLabel = null;
		fieldValue = null;
		errorMessage = null;
		placeholder = null;
		placeholderValue = null;
		required = false;
		showEmptyOption = false;
	}

	@Override
	protected String getPage() {
		return "/META-INF/taglibs/gds-forms-ui/select/view.jsp";
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute("portletNamespace", portletNamespace);
		request.setAttribute("fieldName", fieldName);
		request.setAttribute("fieldLabel", fieldLabel);
		request.setAttribute("fieldValue", fieldValue);
		request.setAttribute("errorMessage", errorMessage);
		request.setAttribute("fieldOptions", fieldOptions);
		request.setAttribute("placeholder", placeholder);
		request.setAttribute("placeholderValue", placeholderValue);
		request.setAttribute("showEmptyOption", showEmptyOption);
		request.setAttribute("required", required);
	}

}
