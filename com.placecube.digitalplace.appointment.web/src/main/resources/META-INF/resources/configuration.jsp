<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ page import="com.placecube.digitalplace.appointment.web.constants.PortletPreferenceKeys" %>

<%@ include file="/init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="<%= true %>" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="<%= true %>" var="configurationRenderURL" />

<div class="container-fluid container-fluid-max-xl">

	<aui:form action="<%= configurationActionURL %>" method="post" name="fm">

		<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
		<aui:input name="redirect" type="hidden" value="<%= configurationRenderURL %>" />

		<aui:fieldset>
			<aui:select
				label="dpappointment-service-id"
				showEmptyOption="true"
				name="<%= PortletPreferenceKeys.SERVICE_ID %>"
			>
				<c:forEach var="serviceId" items="${ availableServiceIds }">
					<aui:option value="${ serviceId }" selected="${ serviceId eq configuredServiceId }" >${ serviceId }</aui:option>
				</c:forEach>
			</aui:select>
			<aui:select
				label="dpappointment-configure-object-definition-id"
				showEmptyOption="true"
				name="<%= PortletPreferenceKeys.OBJECT_DEFINTION_ID %>"
			>
					<c:forEach var="objectDefinition" items="${ availableObjectDefinitionIds }">
						<aui:option value="${ objectDefinition.objectDefinitionId }" selected="${ objectDefinition.objectDefinitionId == configuredObjectDefinitionId }" >${ objectDefinition.label }</aui:option>
					</c:forEach>
				</aui:select>
			<aui:select
				label="dpappointment-display-page-template"
				showEmptyOption="true"
				name="<%= PortletPreferenceKeys.DISPLAY_PAGE_TEMPLATE_ENTRY_ID %>"
			>
				<c:forEach var="dpt" items="${ availableDPTs }">
					<aui:option value="${ dpt.getLayoutPageTemplateEntryId() }" selected="${ dpt.getLayoutPageTemplateEntryId() eq configuredDPT }" >${ dpt.getName() }</aui:option>
				</c:forEach>
			</aui:select>

			<aui:input name="<%= PortletPreferenceKeys.ID_COLUMN_LABEL %>" type="text" value="${configuredIdColumnLabel}" label="dpappointment-id-column-label" />
			
			<aui:input name="<%= PortletPreferenceKeys.FILTER_COLUMN_LABEL %>" type="text" value="${configuredFilterColumnLabel}" label="dpappointment-filter-column-label" />

			<aui:input name="<%= PortletPreferenceKeys.SUMMARY_COLUMN_LABEL %>" type="text" value="${configuredSummaryColumnLabel}" label="dpappointment-summary-column-label" />

			<aui:input name="<%= PortletPreferenceKeys.STARTDATE_COLUMN_LABEL %>" type="text" value="${configuredStartDateColumnLabel}" label="dpappointment-start-date-column-label"/>

			<aui:input name="<%= PortletPreferenceKeys.SUMMARY_FIELD_EXPRESSION %>" type="text" value="${configuredSummaryFieldExpression}" label="dpappointment-summary-field-expression" helpMessage="dpappointment-summary-field-expression-help" />

			<aui:input name="<%= PortletPreferenceKeys.ODATA_FILTER %>" type="text" value="${configuredODataFilter}" label="dpappointment-odata-filter" helpMessage="dpappointment-odata-filter-help" />

			<aui:button-row>
				<aui:button type="submit" />
			</aui:button-row>
		</aui:fieldset>
	</aui:form>

</div>