package com.placecube.digitalplace.ddl.web.upgrade;

import com.liferay.portal.kernel.security.permission.ResourceActionsUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;

public class Upgrade_1_0_0_AddResourceActions extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {
		String[] paths = PropsUtil.getArray(PropsKeys.RESOURCE_ACTIONS_CONFIGS);
		ResourceActionsUtil.populateModelResources(Upgrade_1_0_0_AddResourceActions.class.getClassLoader(), paths, true);
	}
}