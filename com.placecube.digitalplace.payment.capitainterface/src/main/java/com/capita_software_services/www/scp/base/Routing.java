/**
 * Routing.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class Routing  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.ReturnUrl returnUrl;

    private org.apache.axis.types.Token backUrl;

    private org.apache.axis.types.Token errorUrl;

    private int siteId;

    private int scpId;

    public Routing() {
    }

    public Routing(
           com.capita_software_services.www.scp.base.ReturnUrl returnUrl,
           org.apache.axis.types.Token backUrl,
           org.apache.axis.types.Token errorUrl,
           int siteId,
           int scpId) {
           this.returnUrl = returnUrl;
           this.backUrl = backUrl;
           this.errorUrl = errorUrl;
           this.siteId = siteId;
           this.scpId = scpId;
    }


    /**
     * Gets the returnUrl value for this Routing.
     *
     * @return returnUrl
     */
    public com.capita_software_services.www.scp.base.ReturnUrl getReturnUrl() {
        return returnUrl;
    }


    /**
     * Sets the returnUrl value for this Routing.
     *
     * @param returnUrl
     */
    public void setReturnUrl(com.capita_software_services.www.scp.base.ReturnUrl returnUrl) {
        this.returnUrl = returnUrl;
    }


    /**
     * Gets the backUrl value for this Routing.
     *
     * @return backUrl
     */
    public org.apache.axis.types.Token getBackUrl() {
        return backUrl;
    }


    /**
     * Sets the backUrl value for this Routing.
     *
     * @param backUrl
     */
    public void setBackUrl(org.apache.axis.types.Token backUrl) {
        this.backUrl = backUrl;
    }


    /**
     * Gets the errorUrl value for this Routing.
     *
     * @return errorUrl
     */
    public org.apache.axis.types.Token getErrorUrl() {
        return errorUrl;
    }


    /**
     * Sets the errorUrl value for this Routing.
     *
     * @param errorUrl
     */
    public void setErrorUrl(org.apache.axis.types.Token errorUrl) {
        this.errorUrl = errorUrl;
    }


    /**
     * Gets the siteId value for this Routing.
     *
     * @return siteId
     */
    public int getSiteId() {
        return siteId;
    }


    /**
     * Sets the siteId value for this Routing.
     *
     * @param siteId
     */
    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }


    /**
     * Gets the scpId value for this Routing.
     *
     * @return scpId
     */
    public int getScpId() {
        return scpId;
    }


    /**
     * Sets the scpId value for this Routing.
     *
     * @param scpId
     */
    public void setScpId(int scpId) {
        this.scpId = scpId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Routing)) return false;
        Routing other = (Routing) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.returnUrl==null && other.getReturnUrl()==null) ||
             (this.returnUrl!=null &&
              this.returnUrl.equals(other.getReturnUrl()))) &&
            ((this.backUrl==null && other.getBackUrl()==null) ||
             (this.backUrl!=null &&
              this.backUrl.equals(other.getBackUrl()))) &&
            ((this.errorUrl==null && other.getErrorUrl()==null) ||
             (this.errorUrl!=null &&
              this.errorUrl.equals(other.getErrorUrl()))) &&
            this.siteId == other.getSiteId() &&
            this.scpId == other.getScpId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReturnUrl() != null) {
            _hashCode += getReturnUrl().hashCode();
        }
        if (getBackUrl() != null) {
            _hashCode += getBackUrl().hashCode();
        }
        if (getErrorUrl() != null) {
            _hashCode += getErrorUrl().hashCode();
        }
        _hashCode += getSiteId();
        _hashCode += getScpId();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Routing.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "routing"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "returnUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "returnUrl"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("backUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "backUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "token"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "errorUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "token"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "siteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scpId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "scpId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
