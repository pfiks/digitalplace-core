package com.placecube.digitalplace.search.sortby.mostactive.triggers;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.social.kernel.model.SocialActivity;
import com.placecube.digitalplace.search.sortby.mostactive.service.MostActiveService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SocialActivityListenerTest extends PowerMockito {

	private static final long COUNT = 456;
	private static final long GROUP_COUNT = 700;
	private static final long GROUP_ID = 13;
	private static final long USER_ID = 12;

	@Mock
	private Group mockGroup;

	@Mock
	private Indexer<Group> mockGroupIndexer;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private IndexerRegistry mockIndexerRegistry;

	@Mock
	private MostActiveService mockMostActiveService;

	@Mock
	private SocialActivity mockSocialActivity;

	@Mock
	private User mockUser;

	@Mock
	private Indexer<User> mockUserIndexer;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private SocialActivityListener socialActivityListener;

	@Test
	public void onAfterCreate_WhenExceptionRetrievingUserAndExceptionRetrievingTheGroup_ThenNoErrorIsThrownAndNoChangesAreMadeToTheIndex() throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenThrow(new PortalException());
		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenThrow(new PortalException());

		try {
			socialActivityListener.onAfterCreate(mockSocialActivity);
		} catch (Exception e) {
			fail();
		}

		verifyZeroInteractions(mockMostActiveService);
	}

	@Test
	@Parameters({ "true", "false" })
	public void onAfterCreate_WhenExceptionRetrievingUserAndNoErrorRetrievingTheGroup_ThenNoErrorIsThrownAndReindexGroupSocialActivityCount(boolean isCommitImmediately) throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenThrow(new PortalException());

		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockIndexerRegistry.getIndexer(Group.class)).thenReturn(mockGroupIndexer);
		when(mockGroupIndexer.isCommitImmediately()).thenReturn(isCommitImmediately);
		when(mockMostActiveService.getActivitiesCount(mockGroup)).thenReturn(GROUP_COUNT);

		try {
			socialActivityListener.onAfterCreate(mockSocialActivity);
		} catch (Exception e) {
			fail();
		}

		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockGroup, GROUP_COUNT, isCommitImmediately);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void onAfterCreate_WhenNoErrorAndGroupIsNotOrganization_ThenReindexTheUserSocialActivityCountAndGroupSocialActivityCount(boolean isCommitImmediatelyGroup, boolean isCommitImmediatelyUser)
			throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockMostActiveService.getActivitiesCount(mockUser)).thenReturn(COUNT);
		when(mockIndexerRegistry.getIndexer(User.class)).thenReturn(mockUserIndexer);
		when(mockUserIndexer.isCommitImmediately()).thenReturn(isCommitImmediatelyUser);

		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockIndexerRegistry.getIndexer(Group.class)).thenReturn(mockGroupIndexer);
		when(mockGroupIndexer.isCommitImmediately()).thenReturn(isCommitImmediatelyGroup);
		when(mockMostActiveService.getActivitiesCount(mockGroup)).thenReturn(GROUP_COUNT);

		socialActivityListener.onAfterCreate(mockSocialActivity);

		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockUser, COUNT, isCommitImmediatelyUser);
		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockGroup, GROUP_COUNT, isCommitImmediatelyGroup);
	}

	@Test
	public void onAfterCreate_WhenNoErrorAndGroupIsNotOrganizationAndGroupIndexerIsNotAvailable_ThenDoNotReindexGroupSocialActivityCount() throws PortalException {
		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockIndexerRegistry.getIndexer(Group.class)).thenReturn(null);

		socialActivityListener.onAfterCreate(mockSocialActivity);

		verify(mockMostActiveService, never()).reindexSocialActivityCount(eq(mockGroup), anyLong(), anyBoolean());
	}

	@Test
	@Parameters({ "true", "false" })
	public void onAfterCreate_WhenNoErrorAndGroupIsOrganization_ThenReindexTheUserSocialActivityCount(Boolean isCommitImmediatelyUser) throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockMostActiveService.getActivitiesCount(mockUser)).thenReturn(COUNT);
		when(mockIndexerRegistry.getIndexer(User.class)).thenReturn(mockUserIndexer);
		when(mockUserIndexer.isCommitImmediately()).thenReturn(isCommitImmediatelyUser);

		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.isOrganization()).thenReturn(true);

		socialActivityListener.onAfterCreate(mockSocialActivity);

		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockUser, COUNT, isCommitImmediatelyUser);
		verify(mockMostActiveService, never()).reindexSocialActivityCount(any(Group.class), anyLong(), anyBoolean());
	}

	@Test
	@Parameters({ "true", "false" })
	public void onAfterCreate_WhenNoErrorRetrievingUserAndExceptionRetrievingTheGroup_ThenNoErrorIsThrownAndReindexTheUserSocialActivityCount(Boolean isCommitImmediatelyUser) throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockMostActiveService.getActivitiesCount(mockUser)).thenReturn(COUNT);
		when(mockIndexerRegistry.getIndexer(User.class)).thenReturn(mockUserIndexer);
		when(mockUserIndexer.isCommitImmediately()).thenReturn(isCommitImmediatelyUser);

		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenThrow(new PortalException());

		try {
			socialActivityListener.onAfterCreate(mockSocialActivity);
		} catch (Exception e) {
			fail();
		}

		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockUser, COUNT, isCommitImmediatelyUser);
	}

	@Test
	public void onAfterRemove_WhenExceptionRetrievingUserAndExceptionRetrievingTheGroup_ThenNoErrorIsThrownAndNoChangesAreMadeToTheIndex() throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenThrow(new PortalException());
		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenThrow(new PortalException());

		try {
			socialActivityListener.onAfterRemove(mockSocialActivity);
		} catch (Exception e) {
			fail();
		}

		verifyZeroInteractions(mockMostActiveService);
	}

	@Test
	@Parameters({ "true", "false" })
	public void onAfterRemove_WhenExceptionRetrievingUserAndNoErrorRetrievingTheGroup_ThenNoErrorIsThrownAndReindexGroupSocialActivityCount(Boolean isCommitImmediatelyGroup) throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenThrow(new PortalException());

		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockIndexerRegistry.getIndexer(Group.class)).thenReturn(mockGroupIndexer);
		when(mockGroupIndexer.isCommitImmediately()).thenReturn(isCommitImmediatelyGroup);
		when(mockMostActiveService.getActivitiesCount(mockGroup)).thenReturn(GROUP_COUNT);

		try {
			socialActivityListener.onAfterRemove(mockSocialActivity);
		} catch (Exception e) {
			fail();
		}

		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockGroup, GROUP_COUNT, isCommitImmediatelyGroup);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,true", "false,false" })
	public void onAfterRemove_WhenNoErrorAndGroupIsNotOrganization_ThenReindexTheUserSocialActivityCountAndGroupSocialActivityCount(Boolean isCommitImmediatelyUser, boolean isCommitImmediatelyGroup)
			throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockMostActiveService.getActivitiesCount(mockUser)).thenReturn(COUNT);
		when(mockIndexerRegistry.getIndexer(User.class)).thenReturn(mockUserIndexer);
		when(mockUserIndexer.isCommitImmediately()).thenReturn(isCommitImmediatelyUser);

		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockIndexerRegistry.getIndexer(Group.class)).thenReturn(mockGroupIndexer);
		when(mockGroupIndexer.isCommitImmediately()).thenReturn(isCommitImmediatelyGroup);
		when(mockMostActiveService.getActivitiesCount(mockGroup)).thenReturn(GROUP_COUNT);

		socialActivityListener.onAfterRemove(mockSocialActivity);

		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockUser, COUNT, isCommitImmediatelyUser);
		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockGroup, GROUP_COUNT, isCommitImmediatelyGroup);
	}

	@Test
	public void onAfterRemove_WhenNoErrorAndGroupIsNotOrganizationAndGroupIndexerIsNotAvailable_ThenDoNotReindexGroupSocialActivityCount() throws PortalException {
		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockIndexerRegistry.getIndexer(Group.class)).thenReturn(null);

		socialActivityListener.onAfterRemove(mockSocialActivity);

		verify(mockMostActiveService, never()).reindexSocialActivityCount(eq(mockGroup), anyLong(), anyBoolean());
	}

	@Test
	@Parameters({ "true", "false" })
	public void onAfterRemove_WhenNoErrorAndGroupIsOrganization_ThenReindexTheUserSocialActivityCount(Boolean isCommitImmediatelyUser) throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockMostActiveService.getActivitiesCount(mockUser)).thenReturn(COUNT);
		when(mockIndexerRegistry.getIndexer(User.class)).thenReturn(mockUserIndexer);
		when(mockUserIndexer.isCommitImmediately()).thenReturn(isCommitImmediatelyUser);

		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.isOrganization()).thenReturn(true);

		socialActivityListener.onAfterRemove(mockSocialActivity);

		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockUser, COUNT, isCommitImmediatelyUser);
		verify(mockMostActiveService, never()).reindexSocialActivityCount(any(Group.class), anyLong(), anyBoolean());
	}

	@Test
	@Parameters({ "true", "false" })
	public void onAfterRemove_WhenNoErrorRetrievingUserAndExceptionRetrievingTheGroup_ThenNoErrorIsThrownAndReindexTheUserSocialActivityCount(Boolean isCommitImmediatelyUser) throws PortalException {
		when(mockSocialActivity.getUserId()).thenReturn(USER_ID);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockMostActiveService.getActivitiesCount(mockUser)).thenReturn(COUNT);
		when(mockIndexerRegistry.getIndexer(User.class)).thenReturn(mockUserIndexer);
		when(mockUserIndexer.isCommitImmediately()).thenReturn(isCommitImmediatelyUser);

		when(mockSocialActivity.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenThrow(new PortalException());

		try {
			socialActivityListener.onAfterRemove(mockSocialActivity);
		} catch (Exception e) {
			fail();
		}

		verify(mockMostActiveService, times(1)).reindexSocialActivityCount(mockUser, COUNT, isCommitImmediatelyUser);
	}
}
