package com.placecube.digitalplace.siteinitializeradmin.portlet;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.site.initializer.SiteInitializer;
import com.liferay.site.initializer.SiteInitializerRegistry;
import com.placecube.digitalplace.siteinitializeradmin.constants.PortletKeys;
import com.placecube.digitalplace.siteinitializeradmin.constants.PortletRequestKeys;
import com.placecube.digitalplace.siteinitializeradmin.constants.ViewKeys;

@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true", "com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false", "com.liferay.portlet.private-session-attributes=false", "com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true", "javax.portlet.expiration-cache=0", "javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=" + ViewKeys.VIEW,
		"javax.portlet.name=" + PortletKeys.SITE_INITIALIZER_ADMIN, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html" }, service = Portlet.class)
public class SiteInitializerAdminPortlet extends MVCPortlet {

	@Reference
	Portal portal;

	@Reference
	SiteInitializerRegistry siteInitializerRegistry;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		List<SiteInitializer> siteInitializers = siteInitializerRegistry.getSiteInitializers(portal.getCompanyId(renderRequest), true);

		Locale locale = portal.getLocale(renderRequest);

		Collections.sort(siteInitializers, new Comparator<SiteInitializer>() {

			@Override
			public int compare(SiteInitializer o1, SiteInitializer o2) {
				return o1.getName(locale).compareTo(o2.getName(locale));
			}
		});

		renderRequest.setAttribute(PortletRequestKeys.SITE_INITIALIZERS, siteInitializers);

		super.render(renderRequest, renderResponse);
	}
}