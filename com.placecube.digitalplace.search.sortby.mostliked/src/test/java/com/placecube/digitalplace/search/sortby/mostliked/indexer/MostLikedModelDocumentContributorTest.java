package com.placecube.digitalplace.search.sortby.mostliked.indexer;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.search.sortby.mostliked.constants.MostLikedConstants;
import com.placecube.digitalplace.search.sortby.mostliked.service.MostLikedService;

public class MostLikedModelDocumentContributorTest extends PowerMockito {

	@Mock
	private Document mockDocument;

	@Mock
	private MostLikedService mockMostLikedService;

	@Mock
	private BaseModel mockBaseModel;

	@InjectMocks
	private MostLikedModelDocumentContributor mostLikedModelDocumentContributor;

	@Before
	public void actiavateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenNoError_ThenAddsTheScoreFieldToTheIndexedDocument() {
		double score = 5.6;
		String className = "classNameVal";
		long classPK = 12;
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(className);
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(classPK));
		when(mockMostLikedService.getScore(className, classPK)).thenReturn(score);

		mostLikedModelDocumentContributor.contribute(mockDocument, mockBaseModel);

		verify(mockDocument, times(1)).addKeyword(MostLikedConstants.COUNTER_FIELD_NAME, score);
	}

}
