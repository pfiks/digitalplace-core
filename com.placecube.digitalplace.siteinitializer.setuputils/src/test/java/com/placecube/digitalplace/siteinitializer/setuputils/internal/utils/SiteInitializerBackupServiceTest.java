package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;

public class SiteInitializerBackupServiceTest extends PowerMockito {

	@Mock
	private SiteInitializerBackupUtils mockSiteInitializerBackupUtils;

	@InjectMocks
	private SiteInitializerBackupService siteInitializerBackupService;

	private static final  long CURRENT_GROUP_ID = 1L;

	private static final  long BACK_UP_GROUP_ID = 2L;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void backupContentInTemporaryGroup_WhenHasContentToBackup_ThenBacksUpTheContentAndReturnsTheTemporaryGroupId() throws PortalException {
		when(mockSiteInitializerBackupUtils.hasContentToBackUp(CURRENT_GROUP_ID)).thenReturn(true);
		when(mockSiteInitializerBackupUtils.createBackupGroup(CURRENT_GROUP_ID)).thenReturn(BACK_UP_GROUP_ID);

		long result = siteInitializerBackupService.backupContentInTemporaryGroup(CURRENT_GROUP_ID);

		assertThat(result, equalTo(BACK_UP_GROUP_ID));
		verify(mockSiteInitializerBackupUtils, times(1)).moveSiteNavigationMenus(CURRENT_GROUP_ID, BACK_UP_GROUP_ID);
	}

	@Test(expected = PortalException.class)
	public void backupContentInTemporaryGroup_WhenHasContentToBackupAndExceptionCreatingTheTemporaryGroup_ThenThrowsPortalException() throws PortalException {
		when(mockSiteInitializerBackupUtils.hasContentToBackUp(CURRENT_GROUP_ID)).thenReturn(true);
		when(mockSiteInitializerBackupUtils.createBackupGroup(CURRENT_GROUP_ID)).thenThrow(new PortalException());

		siteInitializerBackupService.backupContentInTemporaryGroup(CURRENT_GROUP_ID);
	}

	@Test
	public void backupContentInTemporaryGroup_WhenThereIsNoContentToBackup_ThenDoesNotDoAnyActionAndReturnsZero() throws PortalException {
		when(mockSiteInitializerBackupUtils.hasContentToBackUp(CURRENT_GROUP_ID)).thenReturn(false);

		long result = siteInitializerBackupService.backupContentInTemporaryGroup(CURRENT_GROUP_ID);

		assertThat(result, equalTo(0L));
		verify(mockSiteInitializerBackupUtils, never()).createBackupGroup(anyLong());
		verify(mockSiteInitializerBackupUtils, never()).moveSiteNavigationMenus(anyLong(), anyLong());
	}

	@Test
	public void importBackedUpContentAndRemoveTemporaryGroup_WhenInvalidBackupGroupId_ThenNoActionIsPerformed() {
		long backupGroupId = 0;

		siteInitializerBackupService.importBackedUpContentAndRemoveTemporaryGroup(CURRENT_GROUP_ID, backupGroupId);

		verifyNoInteractions(mockSiteInitializerBackupUtils);
	}

	@Test
	public void importBackedUpContentAndRemoveTemporaryGroup_WhenValidBackupGroupId_ThenMovesTheSiteNavigationMenusAndDeletesTheBackupGroup() {
		siteInitializerBackupService.importBackedUpContentAndRemoveTemporaryGroup(CURRENT_GROUP_ID, BACK_UP_GROUP_ID);

		InOrder inOrder = inOrder(mockSiteInitializerBackupUtils);
		inOrder.verify(mockSiteInitializerBackupUtils, times(1)).moveSiteNavigationMenus(BACK_UP_GROUP_ID, CURRENT_GROUP_ID);
		inOrder.verify(mockSiteInitializerBackupUtils, times(1)).removeBackupGroup(BACK_UP_GROUP_ID);
	}

	@Test(expected = Exception.class)
	public void importBackedUpContentAndRemoveTemporaryGroup_WhenValidBackupGroupId_ThenMovesTheSiteNavigationMenusAndDeletesTheBackupGroupa() {
		Mockito.doAnswer(invocation -> {
			throw new Exception();
		}).when(mockSiteInitializerBackupUtils).moveSiteNavigationMenus(BACK_UP_GROUP_ID, CURRENT_GROUP_ID);
		try {
			siteInitializerBackupService.importBackedUpContentAndRemoveTemporaryGroup(CURRENT_GROUP_ID, BACK_UP_GROUP_ID);
		} finally {
			verify(mockSiteInitializerBackupUtils, times(1)).removeBackupGroup(BACK_UP_GROUP_ID);
		}
	}

}
