package com.placecube.digitalplace.editor.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "editor", scope = ExtendedObjectClassDefinition.Scope.GROUP)
@Meta.OCD(id = "com.placecube.digitalplace.editor.configuration.CKEditorGuidelineHelpGroupConfiguration", localization = "content/Language", name = "ckeditor-help-content-group-configuration-name")
public interface CKEditorGuidelineHelpGroupConfiguration {

	@Meta.AD(deflt = "", type = Meta.Type.String, name = "article-id", required = false)
	String helpContentArticleId();

	@Meta.AD(deflt = "0", type = Meta.Type.Long, name = "help-content-group-id", required = false)
	long helpContentGroupId();
}
