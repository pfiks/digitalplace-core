package com.placecube.digitalplace.externalpage.web.backgroundtask;

import com.liferay.portal.kernel.backgroundtask.BackgroundTask;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskConstants;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskResult;
import com.liferay.portal.kernel.backgroundtask.BaseBackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.display.BackgroundTaskDisplay;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.pfiks.mail.service.MailService;
import com.placecube.digitalplace.externalpage.web.constants.BackgroundTaskKeys;
import com.placecube.digitalplace.externalpage.web.crawler.ExternalPageCrawler;
import com.placecube.digitalplace.externalpage.web.service.CrawlModelBuilder;
import com.placecube.digitalplace.externalpage.web.service.CrawlUtil;

import java.io.Serializable;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

@Component(property = "background.task.executor.class.name=com.placecube.digitalplace.externalpage.web.backgroundtask.ExternalPageImportBackgroundTaskExecutor", service = BackgroundTaskExecutor.class)
public class ExternalPageImportBackgroundTaskExecutor extends BaseBackgroundTaskExecutor {

	private static final Log LOG = LogFactoryUtil.getLog(ExternalPageImportBackgroundTaskExecutor.class);

	@Reference
	private CrawlModelBuilder crawlModelBuilder;

	@Reference
	private MailService mailService;

	@Override
	public BackgroundTaskExecutor clone() {
		return this;
	}

	@Override
	public BackgroundTaskResult execute(BackgroundTask backgroundTask) throws Exception {
		int COUNCURRENT_THREAD = 7;
		Map<String, Serializable> taskContextMap = backgroundTask.getTaskContextMap();

		String url = GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.URL));
		int maxPagesToFetch = GetterUtil.getInteger(taskContextMap.get(BackgroundTaskKeys.MAX_PAGES_TO_FETCH));
		String xpath = GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.X_PATH));
		String indexArray = GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.INDEX_ARRAY));

		long companyId = GetterUtil.getLong(taskContextMap.get(BackgroundTaskKeys.COMPANY_ID));
		long userId = GetterUtil.getLong(taskContextMap.get(BackgroundTaskKeys.USER_ID));
		long scopeGroupId = GetterUtil.getLong(taskContextMap.get(BackgroundTaskKeys.SCOPE_GROUP_ID));
		String emailAddress = GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.USER_EMAIL_ADDRESS));
		String userName = GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.USER_NAME));

		int totalURLCount = GetterUtil.getInteger(taskContextMap.get(BackgroundTaskKeys.TOTAL_URL_COUNT));
		int total = Integer.valueOf(totalURLCount) + maxPagesToFetch;

		CrawlConfig crawlConfig = crawlModelBuilder.getCrawlConfig();

		if (maxPagesToFetch > 0) {
			crawlConfig.setMaxPagesToFetch(maxPagesToFetch);
		}
		PageFetcher pageFetcher = crawlModelBuilder.getPageFetcher(crawlConfig);
		RobotstxtConfig robotstxtConfig = crawlModelBuilder.getRobotstxtConfig();
		RobotstxtServer robotstxtServer = crawlModelBuilder.getRobotstxtServer(robotstxtConfig, pageFetcher);

		CrawlController crawlController = crawlModelBuilder.getCrawlController(crawlConfig, pageFetcher, robotstxtServer);
		crawlController.setCustomData(CrawlUtil.generateCustomData(xpath, indexArray, companyId, scopeGroupId, userId));
		crawlController.addSeed(url);
		crawlController.start(ExternalPageCrawler.class, COUNCURRENT_THREAD);

		mailService.sendEmail(emailAddress, userName, "Site Crawler Status", " Total URLs spidered:" + maxPagesToFetch + "- total new URLs added to external pages results list:" + total);

		return BackgroundTaskResult.SUCCESS;

	}

	@Override
	public BackgroundTaskDisplay getBackgroundTaskDisplay(BackgroundTask backgroundTask) {
		return null;
	}

	@Override
	public int getIsolationLevel() {
		return BackgroundTaskConstants.ISOLATION_LEVEL_TASK_NAME;
	}

}