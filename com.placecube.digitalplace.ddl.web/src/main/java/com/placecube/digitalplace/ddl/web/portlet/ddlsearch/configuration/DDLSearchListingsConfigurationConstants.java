package com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration;

public final class DDLSearchListingsConfigurationConstants {

	public static final String DEFAULT_DISPLAY_STYLE = "defaultDisplayStyle";

	public static final String DESTINATION_PAGE = "destinationPage";

	public static final String GEOLOCATION_FIELD = "geoLocationField";

	public static final String GRID_DISPLAY_STYLE_CSS = "gridDisplayStyleCss";

	public static final String HIDE_PAGINATION = "hidePagination";

	public static final String NUMBER_RESULTS_PER_PAGE = "numberOfResultsPerPage";

	public static final String STRUCTURE_ID = "ddmStructureId";

	private DDLSearchListingsConfigurationConstants() {
	}

}
