/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.authentication.twofactor.service.impl;

import java.util.List;
import java.util.Optional;

import org.joda.time.DateTime;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.authentication.twofactor.checker.EnforceTwoFactorAuthenticationChecker;
import com.placecube.digitalplace.authentication.twofactor.checker.registry.EnforceTwoFactorAuthenticationCheckerRegistry;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.internal.TwoFactorUtils;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.base.UserTwoFactorAuthenticationLocalServiceBaseImpl;
import com.placecube.digitalplace.authentication.twofactor.service.persistence.UserTwoFactorAuthenticationPK;

/**
 * The implementation of the user two factor authentication local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTwoFactorAuthenticationLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication", service = AopService.class)
public class UserTwoFactorAuthenticationLocalServiceImpl extends UserTwoFactorAuthenticationLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.authentication.twofactor.service.
	 * UserTwoFactorAuthenticationLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.authentication.twofactor.service.
	 * UserTwoFactorAuthenticationLocalServiceUtil</code>.
	 */

	@Reference
	private EnforceTwoFactorAuthenticationCheckerRegistry enforceTwoFactorAuthenticationCheckerRegistry;

	@Reference(service = TwoFactorUtils.class)
	private TwoFactorUtils twoFactorUtils;

	@Override
	public void configureTwoFactorAuthenticationForUser(User user, String secretKey) {
		Optional<String> encrypt = twoFactorUtils.encrypt(secretKey, user.getUserUuid());
		if (encrypt.isPresent()) {
			UserTwoFactorAuthenticationPK primaryKey = twoFactorUtils.getPrimaryKey(user);
			UserTwoFactorAuthentication userTwoFactorAuthentication = userTwoFactorAuthenticationPersistence.fetchByPrimaryKey(primaryKey);
			if (Validator.isNull(userTwoFactorAuthentication)) {
				userTwoFactorAuthentication = userTwoFactorAuthenticationLocalService.createUserTwoFactorAuthentication(primaryKey);
			}
			userTwoFactorAuthentication.setSecretKey(encrypt.get());
			userTwoFactorAuthentication.setEnabled(true);
			userTwoFactorAuthentication.setResetKey(false);
			userTwoFactorAuthentication.setModifiedDate(DateTime.now().toDate());
			userTwoFactorAuthenticationLocalService.updateUserTwoFactorAuthentication(userTwoFactorAuthentication);
		}
	}

	@Override
	public void disableTwoFactorAuthenticationForUser(User user) {
		UserTwoFactorAuthenticationPK primaryKey = twoFactorUtils.getPrimaryKey(user);
		UserTwoFactorAuthentication userTwoFactorAuthentication = userTwoFactorAuthenticationPersistence.fetchByPrimaryKey(primaryKey);
		if (Validator.isNotNull(userTwoFactorAuthentication)) {
			resetUserTwoFactorFields(userTwoFactorAuthentication, false);
			userTwoFactorAuthenticationLocalService.updateUserTwoFactorAuthentication(userTwoFactorAuthentication);
		}
	}

	@Override
	public UserTwoFactorAuthentication enableAndGetOrCreateTwoFactorAuthenticationForUser(User user, boolean enabled) {
		UserTwoFactorAuthenticationPK primaryKey = twoFactorUtils.getPrimaryKey(user);
		UserTwoFactorAuthentication userTwoFactorAuthentication = userTwoFactorAuthenticationPersistence.fetchByPrimaryKey(primaryKey);

		if (Validator.isNull(userTwoFactorAuthentication)) {
			userTwoFactorAuthentication = userTwoFactorAuthenticationPersistence.create(primaryKey);
		}

		resetUserTwoFactorFields(userTwoFactorAuthentication, enabled);

		return userTwoFactorAuthenticationPersistence.update(userTwoFactorAuthentication);
	}

	/**
	 * Generates a new random security key
	 *
	 * @return the generated security key
	 */
	@Override
	public String generateSecretKey() {
		return twoFactorUtils.generateRandomSecretKey();
	}

	/**
	 * Gets the QR bar code URL.
	 *
	 * @param companyId the company id
	 * @param emailAddress the email address of the user
	 * @param secretKey the secret key associated to the user
	 * @return the QR bar code URL
	 * @throws PortalException any exception retrieving the qr bar code url
	 */
	@Override
	public String getQRBarCodeURL(long companyId, String emailAddress, String secretKey) throws PortalException {
		AuthenticationTwoFactorCompanyConfiguration authConfiguration = twoFactorUtils.getConfiguration(companyId);
		return twoFactorUtils.getOTPUrl(authConfiguration, emailAddress, secretKey);
	}

	@Override
	public Optional<UserTwoFactorAuthentication> getUserTwoFactorAuthentication(User user) {
		UserTwoFactorAuthenticationPK primaryKey = twoFactorUtils.getPrimaryKey(user);
		UserTwoFactorAuthentication userTwoFactorAuthentication = userTwoFactorAuthenticationPersistence.fetchByPrimaryKey(primaryKey);
		return Optional.ofNullable(userTwoFactorAuthentication);
	}

	@Override
	public boolean isTwoFactorAuthenticationEnforcedForUser(User user) {
		List<EnforceTwoFactorAuthenticationChecker> checkers = enforceTwoFactorAuthenticationCheckerRegistry.getEnforceTwoFactorAuthenticationCheckers();

		for (EnforceTwoFactorAuthenticationChecker enforceTwoFactorAuthenticationChecker : checkers) {
			if (enforceTwoFactorAuthenticationChecker.checkIfTwoFactorAuthenticationShouldBeEnforced(user)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the Two-Factor authentication code is valid for the user.
	 *
	 * @param user the user
	 * @param authenticationCode the authenticationCode to check
	 * @return true if the code is valid or if the feature is disabled for the
	 *         user. Returns false if the feature is enabled and the code is not
	 *         valid
	 * @throws PortalException any exception while checking the two factor
	 *             authentication code
	 */
	@Override
	public boolean isValidTwoFactorAuthentication(User user, String authenticationCode) throws PortalException {

		Optional<UserTwoFactorAuthentication> userTwoFactorAuthentication = userTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(user);
		if (userTwoFactorAuthentication.isPresent()) {
			String secretKeyForUser = userTwoFactorAuthentication.get().getSecretKey();
			return twoFactorUtils.decrypt(secretKeyForUser, user.getUserUuid()).map(secretKey -> twoFactorUtils.verifyUserOneTimeCode(secretKey, authenticationCode))
					.orElse(Boolean.FALSE);
		} else {
			return false;
		}
	}

	@Override
	public void resetUserTwoFactorAuthentication(long companyId, long userId) {
		UserTwoFactorAuthenticationPK primaryKey = twoFactorUtils.getPrimaryKey(companyId, userId);
		UserTwoFactorAuthentication userTwoFactorAuthentication = userTwoFactorAuthenticationPersistence.fetchByPrimaryKey(primaryKey);
		if (Validator.isNotNull(userTwoFactorAuthentication)) {
			resetUserTwoFactorFields(userTwoFactorAuthentication, userTwoFactorAuthentication.getEnabled());
			userTwoFactorAuthenticationLocalService.updateUserTwoFactorAuthentication(userTwoFactorAuthentication);
		}
	}

	@Override
	public boolean verifyAndEnableUserTwoFactorAuthentication(User user, String authenticationCode, String secretKey) throws PortalException {

		if (twoFactorUtils.verifyUserOneTimeCode(secretKey, authenticationCode)) {
			userTwoFactorAuthenticationLocalService.configureTwoFactorAuthenticationForUser(user, secretKey);
			return true;
		} else {
			return false;
		}
	}

	private void resetUserTwoFactorFields(UserTwoFactorAuthentication userTwoFactorAuthentication, boolean enabled) {
		userTwoFactorAuthentication.setEnabled(enabled);
		userTwoFactorAuthentication.setSecretKey(StringPool.BLANK);
		userTwoFactorAuthentication.setResetKey(true);
		userTwoFactorAuthentication.setModifiedDate(DateTime.now().toDate());

	}
}