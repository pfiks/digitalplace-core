<%@ include file="../init.jsp"%>

<c:set var="hasError" value="${not empty errorMessage}"/>

<div class="govuk-form-group ${hasError ? 'govuk-form-group--error' : ''}" id="${portletNamespace}_field_${fieldName}">
	<fieldset class="govuk-fieldset" role="group">
		
		<%@ include file="../fieldLegendLabel.jspf" %>
		<%@ include file="../fieldHintMessage.jspf" %>
		<%@ include file="../fieldErrorMessage.jspf" %>

		<div class="govuk-date-input" id="${portletNamespace}_field_${fieldName}">
			<c:choose>
				<c:when test="${hideDay}">
					<input class="hide" id="${portletNamespace}${fieldName}_day" name="${portletNamespace}${fieldName}_day" type="hidden" value="1"/>
				</c:when>
				<c:otherwise>
					<div class="govuk-date-input__item">
						<div class="govuk-form-group">
							<label class="govuk-label govuk-date-input__label" for="${portletNamespace}${fieldName}_day">
								<liferay-ui:message key="${dayLabel}"/>
							</label>
							<input class="govuk-input govuk-date-input__input govuk-input--width-2 ${hasError ? 'govuk-input--error' : ''}" 
								id="${portletNamespace}${fieldName}_day" 
								name="${portletNamespace}${fieldName}_day" 
								type="number" pattern="[0-9]*"
								value="${dayValue}"
							/>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			
			<c:choose>
				<c:when test="${hideMonth}">
					<input class="hide" id="${portletNamespace}${fieldName}_month" name="${portletNamespace}${fieldName}_month" type="hidden" value="1"/>
				</c:when>
				<c:otherwise>
					<div class="govuk-date-input__item">
						<div class="govuk-form-group">
							<label class="govuk-label govuk-date-input__label" for="${portletNamespace}${fieldName}_month">
								<liferay-ui:message key="${monthLabel}"/>
							</label>
							<input class="govuk-input govuk-date-input__input govuk-input--width-2 ${hasError ? 'govuk-input--error' : ''}" 
								id="${portletNamespace}${fieldName}_month" 
								name="${portletNamespace}${fieldName}_month" 
								type="number" 
								pattern="[0-9]*"
								value="${monthValue}"
							/>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			
			<div class="govuk-date-input__item">
				<div class="govuk-form-group">
					<label class="govuk-label govuk-date-input__label" for="${portletNamespace}${fieldName}_year">
						<liferay-ui:message key="${yearLabel}"/>
					</label>
					<input class="govuk-input govuk-date-input__input govuk-input--width-4 ${hasError ? 'govuk-input--error' : ''}" 
						id="${portletNamespace}${fieldName}_year" 
						name="${portletNamespace}${fieldName}_year" 
						type="number" 
						pattern="[0-9]*"
						value="${yearValue}"
					/>
				</div>
			</div>

		</div>
	</fieldset>
</div>