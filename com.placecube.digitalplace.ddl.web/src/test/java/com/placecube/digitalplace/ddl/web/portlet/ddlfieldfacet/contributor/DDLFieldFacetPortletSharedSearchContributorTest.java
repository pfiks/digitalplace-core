package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.contributor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration.DDLFieldFacetPortletConfigurationConstants;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetParserService;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetRequestService;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;

public class DDLFieldFacetPortletSharedSearchContributorTest extends PowerMockito {

	private static final String DDM_FIELD_NAME = "FieldOne";
	private static final long DDM_STRUCTURE_ID = 1l;
	private static final String INDEXED_FIELD_NAME = "IndexedFieldOne";

	@InjectMocks
	private DDLFieldFacetPortletSharedSearchContributor ddlFieldFacetPortletSharedSearchContributor;

	@Mock
	private DDLFieldFacetParserService mockDDLFieldFacetParserService;

	@Mock
	private DDLFieldFacetRequestService mockDDLFieldFacetRequestService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private FacetConfiguration mockFacetConfiguration;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenNoError_ThenAddsSelectedFilterValuesToSearchQuery() throws ConfigurationException {
		mockFacetInit();

		List<SharedActiveFilter> activeCategoryFilters = new ArrayList<>();
		activeCategoryFilters.add(new SharedActiveFilter("n", "11", "l", "c"));

		when(mockSharedSearchContributorSettings.getActiveFiltersForField(INDEXED_FIELD_NAME)).thenReturn(activeCategoryFilters);

		ddlFieldFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQuery(INDEXED_FIELD_NAME, "11", BooleanClauseOccur.MUST);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheFacetInTheContributorSettings() throws ConfigurationException {
		mockFacetInit();

		when(mockSharedSearchContributorSettings.getActiveFiltersForField(INDEXED_FIELD_NAME)).thenReturn(Collections.emptyList());

		ddlFieldFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockFacetConfiguration, mockJSONObject, mockSharedSearchContributorSettings);
		inOrder.verify(mockFacetConfiguration, times(1)).setOrder("OrderHitsDesc");
		inOrder.verify(mockFacetConfiguration, times(1)).setStatic(false);
		inOrder.verify(mockFacetConfiguration, times(1)).setWeight(1.3d);
		inOrder.verify(mockJSONObject, times(1)).put("frequencyThreshold", 1);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).addFacet(INDEXED_FIELD_NAME, Optional.of(mockFacetConfiguration));
	}

	@Test(expected = SystemException.class)
	public void contribute_WhenNoPreferencesFound_ThenThrowsSystemException() throws Exception {
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.empty());

		ddlFieldFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);
	}

	@Test
	public void contribute_WhenPreSelectedValueDoesNotExist_ThenDoesNotAddPreSelectedValueToSearchQuery() throws ConfigurationException {
		mockFacetInit();

		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, StringPool.BLANK)).thenReturn(null);

		ddlFieldFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(0)).addBooleanQuery(INDEXED_FIELD_NAME, StringPool.BLANK, BooleanClauseOccur.MUST);
		verify(mockSharedSearchContributorSettings, times(0)).addBooleanQuery(INDEXED_FIELD_NAME, null, BooleanClauseOccur.MUST);
	}

	@Test
	public void contribute_WhenPreSelectedValueExists_ThenAddsPreSelectedValueToSearchQuery() throws ConfigurationException {
		mockFacetInit();

		String preSelectedValue = "Preselected";
		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, StringPool.BLANK)).thenReturn(preSelectedValue);

		ddlFieldFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQuery(INDEXED_FIELD_NAME, preSelectedValue, BooleanClauseOccur.MUST);
	}

	@Test
	public void contribute_WhenStructureIdIsNotGreaterThanZero_ThenDoesNotModifyTheSearchQuery() throws ConfigurationException {
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.STRUCTURE_ID, "")).thenReturn("0");

		ddlFieldFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, never()).addBooleanQuery(anyString(), anyString(), any(BooleanClauseOccur.class));
		verify(mockSharedSearchContributorSettings, never()).addFacet(anyString(), any());
	}

	private void mockFacetInit() throws ConfigurationException {
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.STRUCTURE_ID, "")).thenReturn(String.valueOf(DDM_STRUCTURE_ID));
		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.FIELD_NAME, "")).thenReturn(DDM_FIELD_NAME);

		when(mockSharedSearchContributorSettings.createFacetConfiguration(INDEXED_FIELD_NAME)).thenReturn(mockFacetConfiguration);
		when(mockFacetConfiguration.getData()).thenReturn(mockJSONObject);
		when(mockDDMStructureLocalService.fetchDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);
	}

}
