package com.placecube.digitalplace.payment.govuk.service.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.commerce.model.CommerceOrder;
import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.pfiks.expando.creator.ExpandoColumnCreatorInputStreamService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class GovUKPayLifecycleListener extends BasePortalInstanceLifecycleListener {

	@Reference
	private ExpandoColumnCreatorInputStreamService expandoColumnCreatorInputStreamService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		createExpandoFields(company);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		//implementation not needed
	}

	private void createExpandoFields(Company company) throws ExpandoColumnCreationException {
		ClassLoader classLoader = getClass().getClassLoader();
		expandoColumnCreatorInputStreamService.createExpandoColumn(company, CommerceOrder.class.getName(), classLoader.getResourceAsStream("/dependencies/expandos/agreement-id.xml"));
		expandoColumnCreatorInputStreamService.createExpandoColumn(company, CommerceOrder.class.getName(), classLoader.getResourceAsStream("/dependencies/expandos/payment-idempotency-data.xml"));
	}
}
