package com.placecube.digitalplace.formadmin.web.service;

import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstanceLink;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceLinkLocalService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.exportimport.controller.PortletExportController;
import com.liferay.exportimport.kernel.lar.ExportImportHelper;
import com.liferay.exportimport.kernel.lar.ExportImportPathUtil;
import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.exportimport.kernel.lar.PortletDataContextFactory;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.service.PortletLocalService;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ReleaseInfo;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.kernel.zip.ZipWriter;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LanguageUtil.class, ReleaseInfo.class, Time.class, ExportImportPathUtil.class, PropsUtil.class, SAXReaderUtil.class })
public class FormExportServiceTest extends PowerMockito {

	@InjectMocks
	public FormExportService formExportService;

	@Mock
	private Document mockDocument;

	@Mock
	private DDMDataProviderInstanceLink ddmDataProviderInstanceLink;

	@Mock
	private DDMDataProviderInstanceLinkLocalService mockDDMDataProviderInstanceLinkLocalService;

	@Mock
	public DDMFormInstance mockDDMFormInstance;

	@Mock
	public DDMFormInstanceLocalService mockDDMFormInstanceLocalService;

	@Mock
	public DDMStructure mockDDMStructure;

	@Mock
	public DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private ExportImportHelper mockExportImportHelper;

	@Mock
	private FormExportServiceUtil mockFormExportServiceUtil;

	@Mock
	private Portlet mockPortlet;

	@Mock
	private PortletExportController mockPortletExportController;

	@Mock
	private PortletDataContext mockPortletDataContext;

	@Mock
	private PortletDataContextFactory mockPortletDataContextFactory;

	@Mock
	private PortletLocalService mockPortletLocalService;

	@Mock
	private ZipWriter mockZipWriter;

	@Before
	public void Setup() {
		mockStatic(PropsUtil.class, SAXReaderUtil.class);
	}

	@Test(expected = Exception.class)
	public void exportForm_WhenErrorGettingFormInstance_ThenThrowException() throws Exception {
		long ddmFormInstanceId = 1L;

		when(mockDDMFormInstanceLocalService.getFormInstance(ddmFormInstanceId)).thenThrow(new Exception());

		formExportService.exportForm(mockDDMFormInstance);
	}

	@Test(expected = Exception.class)
	public void exportForm_WhenErrorGettingStructure_ThenThrowException() throws Exception {
		long ddmFormInstanceId = 1L;
		long structureId = 2L;

		when(mockDDMFormInstanceLocalService.getFormInstance(ddmFormInstanceId)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getStructureId()).thenReturn(structureId);
		when(mockDDMStructureLocalService.getStructure(structureId)).thenThrow(new Exception());

		formExportService.exportForm(mockDDMFormInstance);
	}

	@Test(expected = Exception.class)
	public void exportForm_WhenErrorGettingFormattedString_ThenThrowException() throws Exception {
		long ddmFormInstanceId = 1L;
		long structureId = 2L;
		long groupId = 3L;
		long companyId = 4L;
		long scopeGroupId = 3L;
		String portletId = "com_liferay_dynamic_data_mapping_form_web_portlet_DDMFormAdminPortlet";

		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(mockDDMFormInstanceLocalService.getFormInstance(ddmFormInstanceId)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getStructureId()).thenReturn(structureId);
		when(mockDDMStructureLocalService.getStructure(structureId)).thenReturn(mockDDMStructure);
		when(mockDDMDataProviderInstanceLinkLocalService.getDataProviderInstanceLinks(structureId)).thenReturn(ddmDataProviderInstanceLinkList);

		when(mockDDMFormInstance.getGroupId()).thenReturn(groupId);
		when(mockDDMFormInstance.getCompanyId()).thenReturn(companyId);
		when(mockDDMFormInstance.getGroupId()).thenReturn(scopeGroupId);

		when(mockPortletLocalService.getPortletById(companyId, portletId)).thenReturn(mockPortlet);

		when(mockExportImportHelper.getPortletZipWriter(portletId)).thenReturn(mockZipWriter);

		when(mockPortletDataContextFactory.createExportPortletDataContext(companyId, groupId, new HashMap<>(), null, null, mockZipWriter)).thenReturn(mockPortletDataContext);

		doNothing().when(mockFormExportServiceUtil).createManifestXML(mockPortlet, mockPortletDataContext, groupId, companyId, portletId, 1);
		doNothing().when(mockFormExportServiceUtil).createPortletXML(mockPortlet, groupId, mockPortletDataContext);
		doNothing().when(mockFormExportServiceUtil).createPortletDataXml(mockPortlet, mockPortletDataContext, groupId, mockDDMFormInstance, mockDDMStructure, ddmDataProviderInstanceLinkList);
		doNothing().when(mockFormExportServiceUtil).createDDMDataProviderInstanceXML(ddmDataProviderInstanceLinkList, mockPortletDataContext);
		doNothing().when(mockFormExportServiceUtil).createDDMFormLayoutJSON(mockPortletDataContext, mockDDMStructure);
		doNothing().when(mockFormExportServiceUtil).createDDMFormJSON(mockPortletDataContext, mockDDMStructure);
		doNothing().when(mockFormExportServiceUtil).createDDMFormInstanceXML(mockPortletDataContext, mockDDMFormInstance);
		doNothing().when(mockFormExportServiceUtil).createDDMStructureXML(mockPortletDataContext, mockDDMStructure);
		doNothing().when(mockFormExportServiceUtil).createSettingsDDMFormValuesJSON(mockPortletDataContext, mockDDMFormInstance);

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.formattedString()).thenThrow(new Exception());

		formExportService.exportForm(mockDDMFormInstance);
	}

	@Test
	public void exportForm_WhenNoError_ThenExportForm() throws Exception {
		long ddmFormInstanceId = 1L;
		long structureId = 2L;
		long groupId = 3L;
		long companyId = 4L;
		long scopeGroupId = 3L;
		String portletId = "com_liferay_dynamic_data_mapping_form_web_portlet_DDMFormAdminPortlet";

		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(mockDDMFormInstanceLocalService.getFormInstance(ddmFormInstanceId)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstance.getStructureId()).thenReturn(structureId);
		when(mockDDMStructureLocalService.getStructure(structureId)).thenReturn(mockDDMStructure);
		when(mockDDMDataProviderInstanceLinkLocalService.getDataProviderInstanceLinks(structureId)).thenReturn(ddmDataProviderInstanceLinkList);

		when(mockDDMFormInstance.getGroupId()).thenReturn(groupId);
		when(mockDDMFormInstance.getCompanyId()).thenReturn(companyId);
		when(mockDDMFormInstance.getGroupId()).thenReturn(scopeGroupId);

		when(mockPortletLocalService.getPortletById(companyId, portletId)).thenReturn(mockPortlet);

		when(mockExportImportHelper.getPortletZipWriter(portletId)).thenReturn(mockZipWriter);

		when(mockPortletDataContextFactory.createExportPortletDataContext(companyId, groupId, new HashMap<>(), null, null, mockZipWriter)).thenReturn(mockPortletDataContext);

		doNothing().when(mockFormExportServiceUtil).createManifestXML(mockPortlet, mockPortletDataContext, groupId, companyId, portletId, 1);
		doNothing().when(mockFormExportServiceUtil).createPortletXML(mockPortlet, groupId, mockPortletDataContext);
		doNothing().when(mockFormExportServiceUtil).createPortletDataXml(mockPortlet, mockPortletDataContext, groupId, mockDDMFormInstance, mockDDMStructure, ddmDataProviderInstanceLinkList);
		doNothing().when(mockFormExportServiceUtil).createDDMDataProviderInstanceXML(ddmDataProviderInstanceLinkList, mockPortletDataContext);
		doNothing().when(mockFormExportServiceUtil).createDDMFormLayoutJSON(mockPortletDataContext, mockDDMStructure);
		doNothing().when(mockFormExportServiceUtil).createDDMFormJSON(mockPortletDataContext, mockDDMStructure);
		doNothing().when(mockFormExportServiceUtil).createDDMFormInstanceXML(mockPortletDataContext, mockDDMFormInstance);
		doNothing().when(mockFormExportServiceUtil).createDDMStructureXML(mockPortletDataContext, mockDDMStructure);
		doNothing().when(mockFormExportServiceUtil).createSettingsDDMFormValuesJSON(mockPortletDataContext, mockDDMFormInstance);

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		formExportService.exportForm(mockDDMFormInstance);
		InOrder inOrder = Mockito.inOrder(mockFormExportServiceUtil, mockPortletExportController);
		inOrder.verify(mockFormExportServiceUtil, times(1)).createManifestXML(mockPortlet, mockPortletDataContext, groupId, companyId, portletId, 1);
		inOrder.verify(mockFormExportServiceUtil, times(1)).createPortletXML(mockPortlet, groupId, mockPortletDataContext);
		inOrder.verify(mockFormExportServiceUtil, times(1)).createPortletDataXml(mockPortlet, mockPortletDataContext, groupId, mockDDMFormInstance, mockDDMStructure, ddmDataProviderInstanceLinkList);
		inOrder.verify(mockFormExportServiceUtil, times(1)).createDDMDataProviderInstanceXML(ddmDataProviderInstanceLinkList, mockPortletDataContext);
		inOrder.verify(mockFormExportServiceUtil, times(1)).createDDMFormLayoutJSON(mockPortletDataContext, mockDDMStructure);
		inOrder.verify(mockFormExportServiceUtil, times(1)).createDDMFormJSON(mockPortletDataContext, mockDDMStructure);
		inOrder.verify(mockFormExportServiceUtil, times(1)).createDDMFormInstanceXML(mockPortletDataContext, mockDDMFormInstance);
		inOrder.verify(mockFormExportServiceUtil, times(1)).createDDMStructureXML(mockPortletDataContext, mockDDMStructure);
		inOrder.verify(mockFormExportServiceUtil, times(1)).createSettingsDDMFormValuesJSON(mockPortletDataContext, mockDDMFormInstance);

		inOrder.verify(mockPortletExportController, times(1)).exportAssetLinks(mockPortletDataContext);
		inOrder.verify(mockPortletExportController, times(1)).exportLocks(mockPortletDataContext);
	}
}
