package com.placecube.digitalplace.search.shared.constants;

public class SharedSearchAttributeKeys {

	public static final String SEARCH_CONTAINER_DELTA = "searchContainerDelta";

	private SharedSearchAttributeKeys() {

	}
}
