package com.placecube.digitalplace.editor.styles.contributor.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.editor.styles.contributor.constants.ButtonStyle;

@Component(immediate = true, service = CKEditorStyleService.class)
public class CKEditorStyleService {

	@Reference
	private JSONFactory jsonFactory;

	public JSONObject createStyle(ButtonStyle buttonStyle) {
		JSONObject attributesJSONObject = jsonFactory.createJSONObject();
		attributesJSONObject.put("class", buttonStyle.getValue());
		attributesJSONObject.put("data-module", buttonStyle.getData());

		JSONObject styleJSONObject = jsonFactory.createJSONObject();
		styleJSONObject.put("attributes", attributesJSONObject);
		styleJSONObject.put("element", buttonStyle.getElement());
		styleJSONObject.put("name", buttonStyle.getName());

		return styleJSONObject;
	}

	public Optional<JSONArray> getStyleSet(JSONObject jsonObject) {
		return Optional.ofNullable(jsonObject.getJSONArray("stylesSet"));
	}

}
