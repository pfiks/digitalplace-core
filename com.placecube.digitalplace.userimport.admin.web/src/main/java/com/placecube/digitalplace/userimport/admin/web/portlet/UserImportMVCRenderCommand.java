package com.placecube.digitalplace.userimport.admin.web.portlet;

import java.util.List;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.GroupProvider;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.userimport.admin.web.constants.PortletKeys;
import com.placecube.digitalplace.userimport.admin.web.constants.UserProfileFields;
import com.placecube.digitalplace.userimport.admin.web.service.UserImportCategoryService;
import com.placecube.digitalplace.userimport.admin.web.service.UserImportService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.USER_IMPORT, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class UserImportMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(UserImportPortlet.class);

	@Reference
	private GroupProvider groupProvider;

	@Reference
	private Portal portal;

	@Reference
	private UserImportCategoryService userImportCategoryService;

	@Reference
	private UserImportService userImportService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		renderRequest.setAttribute("userProfileFields", UserProfileFields.values());

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		List<ExpandoColumn> expandoFields = userImportService.getUserExpandoFields(serviceContext);
		List<ExpandoColumn> visibleExpandos = expandoFields.stream().filter(e -> !isExpandoHidden(e)).collect(Collectors.toList());
		renderRequest.setAttribute("userExpandoFields", visibleExpandos);

		try {
			List<AssetVocabulary> globalVocabulariesOfUserType = userImportCategoryService.getGlobalVocabulariesOfUserType(serviceContext.getCompanyId());
			renderRequest.setAttribute("vocabularies", globalVocabulariesOfUserType);
		} catch (PortalException e) {
			LOG.error(e);
		}
		return "/user-import/view.jsp";
	}

	private boolean isExpandoHidden(ExpandoColumn expando) {
		String hiddenValue = expando.getTypeSettingsProperties().get("hidden");
		return "1".equals(hiddenValue) || Boolean.parseBoolean(hiddenValue);
	}

}
