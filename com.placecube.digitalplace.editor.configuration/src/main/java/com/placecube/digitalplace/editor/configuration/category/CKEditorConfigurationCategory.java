package com.placecube.digitalplace.editor.configuration.category;

import org.osgi.service.component.annotations.Component;

import com.liferay.configuration.admin.category.ConfigurationCategory;

@Component(service = ConfigurationCategory.class)
public class CKEditorConfigurationCategory implements ConfigurationCategory {

	@Override
	public String getBundleSymbolicName() {
		return "com.placecube.digitalplace.editor.configurations";
	}

	@Override
	public String getCategoryIcon() {
		return "text-editor";
	}

	@Override
	public String getCategoryKey() {
		return "editor";
	}

	@Override
	public String getCategorySection() {
		return "content-and-data";
	}
}
