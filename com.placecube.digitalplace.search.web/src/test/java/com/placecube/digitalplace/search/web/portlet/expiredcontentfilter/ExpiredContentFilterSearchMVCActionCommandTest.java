package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.constants.ExpiredContentFilterSearchConstants;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.service.ExpiredContentFilterSearchService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ExpiredContentFilterSearchMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private ExpiredContentFilterSearchMVCActionCommand expiredContentFilterSearchMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ExpiredContentFilterSearchService mockExpiredContentFilterSearchService;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoError_ThenAddsSharedFilterAndRedirects(boolean includeExpiredContent) throws Exception {
		when(ParamUtil.getBoolean(mockActionRequest, "includeExpiredContent", false)).thenReturn(includeExpiredContent);

		expiredContentFilterSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, ExpiredContentFilterSearchConstants.SHARED_SEARCH_ATTRIBUTE_NAME,
				String.valueOf(includeExpiredContent));
		verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

}
