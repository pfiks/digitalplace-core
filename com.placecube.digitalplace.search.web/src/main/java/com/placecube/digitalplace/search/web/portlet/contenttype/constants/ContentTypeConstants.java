package com.placecube.digitalplace.search.web.portlet.contenttype.constants;

import com.liferay.blogs.model.BlogsEntry;
import com.liferay.calendar.model.CalendarBooking;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.knowledge.base.model.KBArticle;
import com.liferay.message.boards.model.MBMessage;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.wiki.model.WikiPage;

public final class ContentTypeConstants {

	public static final String CALENDAR_BOOKING_CLASS_NAME = CalendarBooking.class.getName();
	public static final String DDL_RECORD_CLASS_NAME = DDLRecord.class.getName();
	public static final String MB_MESSAGE_CLASS_NAME = MBMessage.class.getName();

	private static final String BLOGS_ENTRY_CLASS_NAME = BlogsEntry.class.getName();
	private static final String DL_FILE_ENTRY_CLASS_NAME = DLFileEntry.class.getName();
	private static final String GROUP_CLASS_NAME = Group.class.getName();
	private static final String KB_ARTICLE_CLASS_NAME = KBArticle.class.getName();
	private static final String USER_CLASS_NAME = User.class.getName();
	private static final String WIKI_PAGE_CLASS_NAME = WikiPage.class.getName();

	public static String getMessageKeyForClassName(String className) {
		if (BLOGS_ENTRY_CLASS_NAME.equals(className)) {
			return "content-type-blogs";

		} else if (CALENDAR_BOOKING_CLASS_NAME.equals(className)) {
			return "content-type-events";

		} else if (DL_FILE_ENTRY_CLASS_NAME.equals(className)) {
			return "content-type-documents";

		} else if (GROUP_CLASS_NAME.equals(className)) {
			return "content-type-groups";

		} else if (KB_ARTICLE_CLASS_NAME.equals(className)) {
			return "content-type-kb-articles";

		} else if (MB_MESSAGE_CLASS_NAME.equals(className)) {
			return "content-type-discussions";

		} else if (USER_CLASS_NAME.equals(className)) {
			return "content-type-users";

		} else if (WIKI_PAGE_CLASS_NAME.equals(className)) {
			return "content-type-wiki-pages";

		} else {
			return StringUtil.extractLast(className, StringPool.PERIOD);
		}
	}

	private ContentTypeConstants() {
	}
}
