package com.placecube.digitalplace.search.web.portlet.tag.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.portlet.tag.service.TagConfigurationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class TagFacetPortletConfigurationActionTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private TagConfigurationService mockTagConfigurationService;

	@Mock
	private TagFacetPortletInstanceConfiguration mockTagFacetPortletInstanceConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private TagFacetPortletConfigurationAction tagFacetPortletConfigurationAction;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void include_WhenNoError_ThenSetsTagsQueryAndedAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		boolean expected = true;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(TagFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTagFacetPortletInstanceConfiguration);
		when(mockTagFacetPortletInstanceConfiguration.tagsQueryAnded()).thenReturn(expected);

		tagFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(TagFacetPortletConfigurationConstants.TAGS_QUERY_ANDED, expected);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheFilterColourAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String expected = "filterColourValue";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(TagFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTagFacetPortletInstanceConfiguration);
		when(mockTagFacetPortletInstanceConfiguration.filterColour()).thenReturn(expected);

		tagFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("filterColour", expected);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheMaxShowMoreTagsToShowAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		int expected = 14;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(TagFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTagFacetPortletInstanceConfiguration);
		when(mockTagFacetPortletInstanceConfiguration.maxShowMoreTagsToShow()).thenReturn(expected);

		tagFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("maxShowMoreTagsToShow", expected);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheMaxTagsToShowAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		int expected = 34;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(TagFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTagFacetPortletInstanceConfiguration);
		when(mockTagFacetPortletInstanceConfiguration.maxTagsToShow()).thenReturn(expected);

		tagFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("maxTagsToShow", expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesFilterColourToShowPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String expected = "colorVal";
		when(ParamUtil.getString(mockActionRequest, "filterColour", StringPool.BLANK)).thenReturn(expected);
		tagFacetPortletConfigurationAction = spy(new TagFacetPortletConfigurationAction());

		tagFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(tagFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "filterColour", expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesMaxShowMoreTagsToShowPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		int expected = 14;
		when(ParamUtil.getInteger(mockActionRequest, "maxShowMoreTagsToShow")).thenReturn(expected);
		tagFacetPortletConfigurationAction = spy(new TagFacetPortletConfigurationAction());

		tagFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(tagFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "maxShowMoreTagsToShow", String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesMaxTagsToShowPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		int expected = 34;
		when(ParamUtil.getInteger(mockActionRequest, "maxTagsToShow")).thenReturn(expected);
		tagFacetPortletConfigurationAction = spy(new TagFacetPortletConfigurationAction());

		tagFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(tagFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "maxTagsToShow", String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesTagsQueryAndedPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		boolean expected = true;
		when(ParamUtil.getBoolean(mockActionRequest, TagFacetPortletConfigurationConstants.TAGS_QUERY_ANDED, false)).thenReturn(true);
		tagFacetPortletConfigurationAction = spy(new TagFacetPortletConfigurationAction());

		tagFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(tagFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, TagFacetPortletConfigurationConstants.TAGS_QUERY_ANDED, String.valueOf(expected));
	}

}
