package com.placecube.digitalplace.notifications.smsworksnotificationservice;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.io.IOException;
import java.util.Map;

import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendSmsContext;
import com.placecube.digitalplace.notifications.smsworksnotificationservice.configuration.SmsWorksNotificationServiceConfiguration;
import com.placecube.digitalplace.notifications.smsworksnotificationservice.service.SmsWorksNotificationSmsConnector;
import com.placecube.digitalplace.notifications.smsworksnotificationservice.service.SmsWorksNotificationsService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest(HttpClients.class)
public class SmsWorksNotificationSmsConnectorTest extends PowerMockito {

	private static final long COMPANY_ID = 1l;

	@Mock
	private CloseableHttpClient mockHttpClient;

	@Mock
	private HttpPost mockHttpPost;

	@Mock
	private CloseableHttpResponse mockHttpResponse;

	@Mock
	private Map<String, Object> mockPersonalisation;

	@Mock
	private SendSmsContext mockSendSmsContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SmsWorksNotificationServiceConfiguration mockSmsWorksNotificationServiceConfiguration;

	@Mock
	private SmsWorksNotificationsService mockSmsWorksNotificationsService;

	@Mock
	private StatusLine mockStatusLine;

	@Mock
	private StringEntity mockStringEntity;

	@InjectMocks
	private SmsWorksNotificationSmsConnector smsWorksNotificationSmsConnector;

	@Before
	public void activateSetup() {
		mockStatic(HttpClients.class);
	}

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockSmsWorksNotificationsService.isSmsEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = smsWorksNotificationSmsConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockSmsWorksNotificationsService.isSmsEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = smsWorksNotificationSmsConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = NotificationsException.class)
	public void sendSms_WhenExceptionRetrievingTheConfiguration_ThenThrowsNotificationsException() throws Exception {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSmsWorksNotificationsService.getConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		smsWorksNotificationSmsConnector.sendSms(mockSendSmsContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	public void sendSms_WhenExceptionSendingSms_ThenThrowsNotificationsException() throws Exception {
		String authToken = "authTokenValue";
		String body = "bodyValue";
		String content = "contentValue";
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSmsWorksNotificationsService.getConfiguration(COMPANY_ID)).thenReturn(mockSmsWorksNotificationServiceConfiguration);
		when(mockSmsWorksNotificationsService.getContentWithPlaceholderValues(mockSendSmsContext)).thenReturn(content);
		when(mockSmsWorksNotificationsService.getBody(mockSendSmsContext, content)).thenReturn(body);
		when(mockSmsWorksNotificationsService.getHttpPost(mockSmsWorksNotificationServiceConfiguration)).thenReturn(mockHttpPost);
		when(mockSmsWorksNotificationsService.getStringEntity(body)).thenReturn(mockStringEntity);
		when(mockSmsWorksNotificationServiceConfiguration.authToken()).thenReturn(authToken);
		when(HttpClients.createDefault()).thenReturn(mockHttpClient);
		when(mockHttpClient.execute(mockHttpPost)).thenThrow(new IOException());

		smsWorksNotificationSmsConnector.sendSms(mockSendSmsContext, mockServiceContext);
	}

	@Test
	public void sendSms_WhenNoError_ThenSendsTheSms() throws Exception {
		String authToken = "authTokenValue";
		String body = "bodyValue";
		String content = "contentValue";
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSmsWorksNotificationsService.getConfiguration(COMPANY_ID)).thenReturn(mockSmsWorksNotificationServiceConfiguration);
		when(mockSmsWorksNotificationsService.getContentWithPlaceholderValues(mockSendSmsContext)).thenReturn(content);
		when(mockSmsWorksNotificationsService.getBody(mockSendSmsContext, content)).thenReturn(body);
		when(mockSmsWorksNotificationsService.getHttpPost(mockSmsWorksNotificationServiceConfiguration)).thenReturn(mockHttpPost);
		when(mockSmsWorksNotificationsService.getStringEntity(body)).thenReturn(mockStringEntity);
		when(mockSmsWorksNotificationServiceConfiguration.authToken()).thenReturn(authToken);
		when(HttpClients.createDefault()).thenReturn(mockHttpClient);
		when(mockHttpClient.execute(mockHttpPost)).thenReturn(mockHttpResponse);
		when(mockHttpResponse.getStatusLine()).thenReturn(mockStatusLine);
		when(mockHttpResponse.getEntity()).thenReturn(mockStringEntity);

		smsWorksNotificationSmsConnector.sendSms(mockSendSmsContext, mockServiceContext);

		InOrder inOrder = inOrder(mockHttpPost, mockHttpClient);
		inOrder.verify(mockHttpPost, times(1)).setEntity(mockStringEntity);
		inOrder.verify(mockHttpPost, times(1)).setHeader("Content-type", "application/json");
		inOrder.verify(mockHttpPost, times(1)).addHeader("Authorization", authToken);
		inOrder.verify(mockHttpClient, times(1)).execute(mockHttpPost);
	}

}
