package com.placecube.digitalplace.payment.govuk.service.commerce;

import static com.placecube.digitalplace.payment.constants.PaymentConstants.API_AUTH_KEY;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.commerce.constants.CommerceOrderPaymentConstants;
import com.liferay.commerce.constants.CommercePaymentMethodConstants;
import com.liferay.commerce.model.CommerceAddress;
import com.liferay.commerce.model.CommerceOrder;
import com.liferay.commerce.payment.engine.CommercePaymentEngine;
import com.liferay.commerce.payment.request.CommercePaymentRequest;
import com.liferay.commerce.payment.result.CommercePaymentResult;
import com.liferay.commerce.payment.util.CommercePaymentHttpHelper;
import com.liferay.commerce.service.CommerceOrderLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.PortalSessionThreadLocal;
import com.liferay.portal.kernel.settings.GroupServiceSettingsLocator;
import com.liferay.portal.kernel.settings.SettingsLocatorHelperUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.govuk.service.GovUKPaymentConnector;
import com.placecube.digitalplace.payment.govuk.service.commerce.configuration.GovUKPayGroupServiceConfiguration;
import com.placecube.digitalplace.payment.govuk.service.internal.GovUKPaymentOrderFactory;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceContextThreadLocal.class, PortalSessionThreadLocal.class, ParamUtil.class, SettingsLocatorHelperUtil.class, ResourceBundleUtil.class, LanguageUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil", "com.liferay.portal.kernel.settings.SettingsLocatorHelperUtil" })
public class GovUKCommercePaymentMethodTest extends PowerMockito {

	@Mock
	private CommerceAddress mockCommerceAddress;

	@Mock
	private CommerceOrder mockCommerceOrder;

	@Mock
	private CommerceOrderLocalService mockCommerceOrderLocalService;

	@Mock
	private CommercePaymentHttpHelper mockCommercePaymentHttpHelper;

	@Mock
	private CommercePaymentEngine mockCommercePaymentEngine;

	@Mock
	private CommercePaymentRequest mockCommercePaymentRequest;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private GovUKPayGroupServiceConfiguration mockGovUKPayGroupServiceConfiguration;

	@Mock
	private GovUKPaymentConnector mockGovUKPaymentConnector;

	@Mock
	private GovUKPaymentOrderFactory mockGovUKPaymentOrderFactory;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private HttpSession mockHttpSession;

	@Mock
	private PaymentResponse mockPaymentResponse;

	@Mock
	private ResourceBundle mockResourceBundle;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Captor
	private ArgumentCaptor<PaymentContext> paymentContextArgumentCaptor;

	@InjectMocks
	private GovUKCommercePaymentMethod govUKCommercePaymentMethod;

	@Before
	public void activateSetup() {
		mockStatic(ServiceContextThreadLocal.class, PortalSessionThreadLocal.class, ParamUtil.class, SettingsLocatorHelperUtil.class, ResourceBundleUtil.class, LanguageUtil.class);
	}

	@Test
	public void getDescription_WhenNoError_ThenReturnsDescription() {
		Locale locale = Locale.UK;
		String description = "description";

		when(ResourceBundleUtil.getBundle("content.Language", locale, GovUKCommercePaymentMethod.class)).thenReturn(mockResourceBundle);
		when(LanguageUtil.get(mockResourceBundle, "commerce-payment-govuk-description")).thenReturn(description);

		String result = govUKCommercePaymentMethod.getDescription(locale);

		assertEquals(description, result);
	}

	@Test
	public void getKey_WhenNoError_ThenReturnsKey() {
		String result = govUKCommercePaymentMethod.getKey();

		assertEquals(GovUKCommerceConstants.PAYMENT_ENGINE_KEY, result);
	}

	@Test
	public void getName_WhenNoError_ThenReturnsName() {
		Locale locale = Locale.UK;
		String description = "description";

		when(ResourceBundleUtil.getBundle("content.Language", locale, GovUKCommercePaymentMethod.class)).thenReturn(mockResourceBundle);
		when(LanguageUtil.get(mockResourceBundle, "commerce-payment-govuk-name")).thenReturn(description);

		String result = govUKCommercePaymentMethod.getName(locale);

		assertEquals(description, result);
	}

	@Test
	public void getPaymentType_WhenNoError_ThenReturnsPaymentType() {
		int result = govUKCommercePaymentMethod.getPaymentType();

		assertEquals(CommercePaymentMethodConstants.TYPE_ONLINE_REDIRECT, result);
	}

	@Test
	public void getServletPath_WhenNoError_ThenReturnsServletPath() {
		String result = govUKCommercePaymentMethod.getServletPath();

		assertEquals(GovUKCommerceConstants.SERVLET_PATH, result);
	}

	@Test
	public void isCancelEnabled_WhenNoError_ThenReturnsTrue() {
		boolean result = govUKCommercePaymentMethod.isCancelEnabled();

		assertTrue(result);
	}

	@Test
	public void isCompleteEnabled_WhenNoError_ThenReturnsTrue() {
		boolean result = govUKCommercePaymentMethod.isCompleteEnabled();

		assertTrue(result);
	}

	@Test
	public void isProcessPaymentEnabled_WhenNoError_ThenReturnsTrue() {
		boolean result = govUKCommercePaymentMethod.isProcessPaymentEnabled();

		assertTrue(result);
	}

	@Test
	public void cancelPayment_WhenNoError_ThenReturnsResultWithPaymentStatusCanceled() {
		String transactionId = "id";
		long commerceOrderId = 123;

		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommercePaymentRequest.getTransactionId()).thenReturn(transactionId);

		CommercePaymentResult result = govUKCommercePaymentMethod.cancelPayment(mockCommercePaymentRequest);

		assertThat(result.getNewPaymentStatus(), equalTo(CommerceOrderPaymentConstants.STATUS_CANCELLED));
		assertThat(result.getCommerceOrderId(), equalTo(commerceOrderId));
		assertThat(result.getAuthTransactionId(), equalTo(transactionId));
	}

	@Test
	public void completePayment_WhenNoErrorAndPaymentStatusIsSuccess_ThenReturnsResultWithPaymentStatusCompleted() throws Exception {
		String transactionId = "id";
		long commerceOrderId = 123;
		String paymentId = "paymentId";
		String keyName = "keyName";

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommercePaymentRequest.getTransactionId()).thenReturn(transactionId);
		when(mockCommerceOrderLocalService.getCommerceOrder(commerceOrderId)).thenReturn(mockCommerceOrder);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(GroupServiceSettingsLocator.class))).thenReturn(mockGovUKPayGroupServiceConfiguration);
		when(mockGovUKPayGroupServiceConfiguration.keyName()).thenReturn(keyName);
		when(mockGovUKPaymentConnector.getPaymentStatus("paymentReference", mockServiceContext)).thenReturn(PaymentStatus.success());
		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(paymentId);

		CommercePaymentResult result = govUKCommercePaymentMethod.completePayment(mockCommercePaymentRequest);

		InOrder inOrder = inOrder(mockServiceContext, mockGovUKPaymentConnector);
		inOrder.verify(mockServiceContext, times(1)).setAttribute(API_AUTH_KEY, keyName);
		inOrder.verify(mockGovUKPaymentConnector, times(1)).getPaymentStatus("paymentReference", mockServiceContext);
		assertThat(result.getNewPaymentStatus(), equalTo(CommerceOrderPaymentConstants.STATUS_COMPLETED));
		assertThat(result.getCommerceOrderId(), equalTo(commerceOrderId));
		assertThat(result.getAuthTransactionId(), equalTo(paymentId));
	}

	@Test
	public void completePayment_WhenNoErrorAndPaymentStatusIsNotSuccess_ThenReturnsResultWithPaymentStatusFailed() throws Exception {
		String transactionId = "id";
		long commerceOrderId = 123;
		String keyName = "keyName";
		String errorMessage = "error";

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommercePaymentRequest.getTransactionId()).thenReturn(transactionId);
		when(mockCommerceOrderLocalService.getCommerceOrder(commerceOrderId)).thenReturn(mockCommerceOrder);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(GroupServiceSettingsLocator.class))).thenReturn(mockGovUKPayGroupServiceConfiguration);
		when(mockGovUKPayGroupServiceConfiguration.keyName()).thenReturn(keyName);
		when(mockGovUKPaymentConnector.getPaymentStatus("paymentReference", mockServiceContext)).thenReturn(PaymentStatus.paymentError("123", errorMessage));

		CommercePaymentResult result = govUKCommercePaymentMethod.completePayment(mockCommercePaymentRequest);

		InOrder inOrder = inOrder(mockServiceContext, mockGovUKPaymentConnector);
		inOrder.verify(mockServiceContext, times(1)).setAttribute(API_AUTH_KEY, keyName);
		inOrder.verify(mockGovUKPaymentConnector, times(1)).getPaymentStatus("paymentReference", mockServiceContext);
		assertThat(result.getNewPaymentStatus(), equalTo(CommerceOrderPaymentConstants.STATUS_FAILED));
		assertThat(result.getCommerceOrderId(), equalTo(commerceOrderId));
		assertThat(result.getAuthTransactionId(), equalTo(transactionId));
		assertThat(result.getResultMessages().size(), equalTo(1));
		assertThat(result.getResultMessages().get(0), equalTo(errorMessage));
	}

	@Test(expected = PortalException.class)
	public void completePayment_WhenExceptionGettingCommerceOrder_ThenThrowsException() throws Exception {
		String transactionId = "id";
		long commerceOrderId = 123;

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommercePaymentRequest.getTransactionId()).thenReturn(transactionId);
		when(mockCommerceOrderLocalService.getCommerceOrder(commerceOrderId)).thenThrow(PortalException.class);

		govUKCommercePaymentMethod.completePayment(mockCommercePaymentRequest);
	}

	@Test(expected = ConfigurationException.class)
	public void completePayment_WhenExceptionGettingConfiguration_ThenThrowsException() throws Exception {
		String transactionId = "id";
		long commerceOrderId = 123;

		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommercePaymentRequest.getTransactionId()).thenReturn(transactionId);
		when(mockCommerceOrderLocalService.getCommerceOrder(commerceOrderId)).thenReturn(mockCommerceOrder);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(GroupServiceSettingsLocator.class))).thenThrow(ConfigurationException.class);

		govUKCommercePaymentMethod.completePayment(mockCommercePaymentRequest);
	}

	@Test
	public void processPayment_WhenNoError_ThenPreparesThePaymentAndReturnsResultWithPaymentStatusAuthorised() throws Exception {
		String transactionId = "id";
		long commerceOrderId = 123;
		String keyName = "keyName";
		long userId = 321;
		String email = "email";
		String userName = "userName";
		String street1 = "1";
		String street2 = "2";
		String street3 = "3";
		String city = "city";
		String zip = "zip";
		String externalRefCode = "code";
		BigDecimal amount = BigDecimal.ONE;
		String returnUrl = "url";
		String redirectUrl = "redirectUrl";
		String paymentId = "paymentId";

		when(mockCommercePaymentRequest.getReturnUrl()).thenReturn(returnUrl);
		when(mockCommercePaymentRequest.getCancelUrl()).thenReturn(returnUrl);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommercePaymentRequest.getTransactionId()).thenReturn(transactionId);
		when(mockCommercePaymentRequest.getAmount()).thenReturn(amount);
		when(mockCommerceOrderLocalService.getCommerceOrder(commerceOrderId)).thenReturn(mockCommerceOrder);
		when(mockCommerceOrder.getBillingAddress()).thenReturn(mockCommerceAddress);
		when(mockCommerceAddress.getStreet1()).thenReturn(street1);
		when(mockCommerceAddress.getStreet2()).thenReturn(street2);
		when(mockCommerceAddress.getStreet3()).thenReturn(street3);
		when(mockCommerceAddress.getCity()).thenReturn(city);
		when(mockCommerceAddress.getZip()).thenReturn(zip);
		when(mockCommerceOrder.getUserId()).thenReturn(userId);
		when(mockUserLocalService.getUser(userId)).thenReturn(mockUser);
		when(mockUser.getEmailAddress()).thenReturn(email);
		when(mockCommerceOrder.getUserName()).thenReturn(userName);
		when(mockCommerceOrder.getExternalReferenceCode()).thenReturn(externalRefCode);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(GroupServiceSettingsLocator.class))).thenReturn(mockGovUKPayGroupServiceConfiguration);
		when(mockGovUKPayGroupServiceConfiguration.keyName()).thenReturn(keyName);
		when(mockGovUKPaymentConnector.preparePayment(any(PaymentContext.class), eq(mockServiceContext))).thenReturn(mockPaymentResponse);
		when(mockPaymentResponse.getRedirectURL()).thenReturn(redirectUrl);
		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(paymentId);

		CommercePaymentResult result = govUKCommercePaymentMethod.processPayment(mockCommercePaymentRequest);

		InOrder inOrder = inOrder(mockServiceContext, mockGovUKPaymentConnector);
		inOrder.verify(mockServiceContext, times(1)).setAttribute(API_AUTH_KEY, keyName);
		inOrder.verify(mockGovUKPaymentConnector, times(1)).preparePayment(paymentContextArgumentCaptor.capture(), eq(mockServiceContext));
		verifyPaymentContextSentToPreparePayment(email, amount, userName, externalRefCode, returnUrl, street1, street2, street3, city, zip);

		assertThat(result.getNewPaymentStatus(), equalTo(CommerceOrderPaymentConstants.STATUS_AUTHORIZED));
		assertThat(result.getCommerceOrderId(), equalTo(commerceOrderId));
		assertThat(result.getAuthTransactionId(), equalTo(paymentId));
		assertThat(result.isOnlineRedirect(), equalTo(true));
		assertThat(result.getRedirectUrl(), equalTo(redirectUrl));
	}

	@Test
	public void processPayment_WhenExceptionGettingUser_ThenPreparesThePaymentWithoutPrefillingEmailAndReturnsResultWithPaymentStatusAuthorised() throws Exception {
		String transactionId = "id";
		long commerceOrderId = 123;
		String keyName = "keyName";
		long userId = 321;
		String userName = "userName";
		String street1 = "1";
		String street2 = "2";
		String street3 = "3";
		String city = "city";
		String zip = "zip";
		String externalRefCode = "code";
		BigDecimal amount = BigDecimal.ONE;
		String returnUrl = "url";
		String redirectUrl = "redirectUrl";
		String paymentId = "paymentId";

		when(mockCommercePaymentRequest.getReturnUrl()).thenReturn(returnUrl);
		when(mockCommercePaymentRequest.getCancelUrl()).thenReturn(returnUrl);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommercePaymentRequest.getTransactionId()).thenReturn(transactionId);
		when(mockCommercePaymentRequest.getAmount()).thenReturn(amount);
		when(mockCommerceOrderLocalService.getCommerceOrder(commerceOrderId)).thenReturn(mockCommerceOrder);
		when(mockCommerceOrder.getBillingAddress()).thenReturn(mockCommerceAddress);
		when(mockCommerceAddress.getStreet1()).thenReturn(street1);
		when(mockCommerceAddress.getStreet2()).thenReturn(street2);
		when(mockCommerceAddress.getStreet3()).thenReturn(street3);
		when(mockCommerceAddress.getCity()).thenReturn(city);
		when(mockCommerceAddress.getZip()).thenReturn(zip);
		when(mockCommerceOrder.getUserId()).thenReturn(userId);
		when(mockUserLocalService.getUser(userId)).thenThrow(PortalException.class);
		when(mockCommerceOrder.getUserName()).thenReturn(userName);
		when(mockCommerceOrder.getExternalReferenceCode()).thenReturn(externalRefCode);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(GroupServiceSettingsLocator.class))).thenReturn(mockGovUKPayGroupServiceConfiguration);
		when(mockGovUKPayGroupServiceConfiguration.keyName()).thenReturn(keyName);
		when(mockGovUKPaymentConnector.preparePayment(any(PaymentContext.class), eq(mockServiceContext))).thenReturn(mockPaymentResponse);
		when(mockPaymentResponse.getRedirectURL()).thenReturn(redirectUrl);
		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(paymentId);

		CommercePaymentResult result = govUKCommercePaymentMethod.processPayment(mockCommercePaymentRequest);

		InOrder inOrder = inOrder(mockServiceContext, mockGovUKPaymentConnector);
		inOrder.verify(mockServiceContext, times(1)).setAttribute(API_AUTH_KEY, keyName);
		inOrder.verify(mockGovUKPaymentConnector, times(1)).preparePayment(paymentContextArgumentCaptor.capture(), eq(mockServiceContext));
		verifyPaymentContextSentToPreparePayment(StringPool.BLANK, amount, userName, externalRefCode, returnUrl, street1, street2, street3, city, zip);

		assertThat(result.getNewPaymentStatus(), equalTo(CommerceOrderPaymentConstants.STATUS_AUTHORIZED));
		assertThat(result.getCommerceOrderId(), equalTo(commerceOrderId));
		assertThat(result.getAuthTransactionId(), equalTo(paymentId));
		assertThat(result.isOnlineRedirect(), equalTo(true));
		assertThat(result.getRedirectUrl(), equalTo(redirectUrl));
	}

	@Test(expected = PortalException.class)
	public void processPayment_WhenExceptionGettingOrder_ThenThrowsExceptionAndDoesNotPreparePayment() throws Exception {
		long commerceOrderId = 123;

		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommerceOrderLocalService.getCommerceOrder(commerceOrderId)).thenThrow(PortalException.class);

		govUKCommercePaymentMethod.processPayment(mockCommercePaymentRequest);

		verifyZeroInteractions(mockGovUKPaymentConnector);
	}

	@Test(expected = PortalException.class)
	public void processPayment_WhenExceptionGettingOrderBillingAddress_ThenThrowsExceptionAndDoesNotPreparePayment() throws Exception {
		long commerceOrderId = 123;

		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommerceOrderLocalService.getCommerceOrder(commerceOrderId)).thenReturn(mockCommerceOrder);
		when(mockCommerceOrder.getBillingAddress()).thenThrow(PortalException.class);

		govUKCommercePaymentMethod.processPayment(mockCommercePaymentRequest);

		verifyZeroInteractions(mockGovUKPaymentConnector);
	}

	@Test(expected = ConfigurationException.class)
	public void processPayment_WhenExceptionGettingConfiguration_ThenThrowsExceptionAndDoesNotPreparePayment() throws Exception {
		String transactionId = "id";
		long commerceOrderId = 123;
		long userId = 321;
		String userName = "userName";
		String street1 = "1";
		String street2 = "2";
		String street3 = "3";
		String city = "city";
		String zip = "zip";
		String externalRefCode = "code";
		BigDecimal amount = BigDecimal.ONE;
		String returnUrl = "url";

		when(mockCommercePaymentRequest.getReturnUrl()).thenReturn(returnUrl);
		when(mockCommercePaymentRequest.getCancelUrl()).thenReturn(returnUrl);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockCommercePaymentRequest.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(mockCommercePaymentRequest.getTransactionId()).thenReturn(transactionId);
		when(mockCommercePaymentRequest.getAmount()).thenReturn(amount);
		when(mockCommerceOrderLocalService.getCommerceOrder(commerceOrderId)).thenReturn(mockCommerceOrder);
		when(mockCommerceOrder.getBillingAddress()).thenReturn(mockCommerceAddress);
		when(mockCommerceAddress.getStreet1()).thenReturn(street1);
		when(mockCommerceAddress.getStreet2()).thenReturn(street2);
		when(mockCommerceAddress.getStreet3()).thenReturn(street3);
		when(mockCommerceAddress.getCity()).thenReturn(city);
		when(mockCommerceAddress.getZip()).thenReturn(zip);
		when(mockCommerceOrder.getUserId()).thenReturn(userId);
		when(mockUserLocalService.getUser(userId)).thenThrow(PortalException.class);
		when(mockCommerceOrder.getUserName()).thenReturn(userName);
		when(mockCommerceOrder.getExternalReferenceCode()).thenReturn(externalRefCode);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(GroupServiceSettingsLocator.class))).thenThrow(ConfigurationException.class);

		govUKCommercePaymentMethod.processPayment(mockCommercePaymentRequest);

		verifyZeroInteractions(mockGovUKPaymentConnector);
	}

	private void verifyPaymentContextSentToPreparePayment(String email, BigDecimal amount, String userName, String externalRefCode, String returnUrl, String street1, String street2, String street3, String city, String zip) {
		assertThat(paymentContextArgumentCaptor.getAllValues().size(), equalTo(1));
		PaymentContext argumentPaymentContext = paymentContextArgumentCaptor.getAllValues().get(0);
		assertThat(argumentPaymentContext.getEmailAddress(), equalTo(email));
		assertThat(argumentPaymentContext.getAmount(), equalTo(amount));
		assertThat(argumentPaymentContext.getFirstName(), equalTo(userName));
		assertThat(argumentPaymentContext.getItemReference(), equalTo(externalRefCode));
		assertThat(argumentPaymentContext.getReturnURL(), equalTo(returnUrl));
		assertThat(argumentPaymentContext.getSalesReference(), equalTo(StringPool.BLANK));
		assertThat(argumentPaymentContext.getSalesDescription(), equalTo("Order reference code: " + externalRefCode));
		assertThat(argumentPaymentContext.getAddress().getAddressLine1(), equalTo(street1));
		assertThat(argumentPaymentContext.getAddress().getAddressLine2(), equalTo(street2));
		assertThat(argumentPaymentContext.getAddress().getAddressLine3(), equalTo(street3));
		assertThat(argumentPaymentContext.getAddress().getCity(), equalTo(city));
		assertThat(argumentPaymentContext.getAddress().getPostcode(), equalTo(zip));
	}

}
