package com.placecube.digitalplace.search.web.portlet.contenttype.indexer;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.DocumentContributor;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.search.web.portlet.contenttype.constants.ContentTypeConstants;
import com.placecube.digitalplace.search.web.portlet.contenttype.service.ContentTypeSearchService;

@SuppressWarnings("rawtypes")
@Component(immediate = true, service = DocumentContributor.class)
public class ContentTypeModelDocumentContributor implements DocumentContributor {

	private static final Log LOG = LogFactoryUtil.getLog(ContentTypeModelDocumentContributor.class);

	@Reference
	private ContentTypeSearchService contentTypeSearchService;

	@Override
	public void contribute(Document document, BaseModel baseModel) {
		String className = document.get(Field.ENTRY_CLASS_NAME);

		if (ContentTypeConstants.DDL_RECORD_CLASS_NAME.equalsIgnoreCase(className)) {
			try {
				className = contentTypeSearchService.getDDLRecordContentTypeValue(contentTypeSearchService.getDDLStructureIdFromDocument(document));
			} catch (PortalException e) {
				LOG.debug(e);
			}
		}

		document.add(contentTypeSearchService.getContentTypeSearchField(className));
	}

}