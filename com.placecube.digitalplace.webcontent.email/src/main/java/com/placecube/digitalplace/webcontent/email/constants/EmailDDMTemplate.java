package com.placecube.digitalplace.webcontent.email.constants;

public enum EmailDDMTemplate {

	EMAIL("GLOBAL-EMAIL", "Global Email");

	private final String key;
	private final String name;

	private EmailDDMTemplate(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

}
