package com.placecube.digitalplace.layout.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.theme.NavItem;

public interface LayoutRetrievalService {

	/**
	 * Retrieves a list of all the portlets on the layout
	 *
	 * @param layout the layout
	 * @return list of portlets
	 */
	List<Portlet> getAllPortletsOnLayout(Layout layout);

	/**
	 * Retrieves the layouts found with the given portlet id
	 *
	 * @param groupId the groupId
	 * @param portletId the portletId
	 * @param privateLayout if the layout should be searched in the private or
	 *            public layout set of the group
	 * @return a list with layouts found, or empty list if none is found
	 */
	List<Layout> getLayoutsWithPortletId(long groupId, String portletId, boolean privateLayout);

	/**
	 * Retrieves the first layout found with the given portlet id
	 *
	 * @param groupId the groupId
	 * @param portletId the portletId
	 * @param privateLayout if the layout should be searched in the private or
	 *            public layout set of the group
	 * @return the layout found, or empty optional if none is found
	 */
	Optional<Layout> getLayoutWithPortletId(long groupId, String portletId, boolean privateLayout);

	/**
	 * Returns the NavItem for the specified layout.
	 *
	 * @param httpServletRequest the request
	 * @param groupId the groupId to retrieve the layout from
	 * @param friendlyURL the layout friendly url
	 * @param privateLayout if the layout is private or not
	 * @return the navItem
	 * @throws PortalException any exception retrieving the layout
	 */
	NavItem getNavItemForLayout(HttpServletRequest httpServletRequest, long groupId, String friendlyURL, boolean privateLayout) throws PortalException;

	/**
	 * Returns the nav items list for the specified layouts list.
	 *
	 * @param httpServletRequest the request
	 * @param layouts layouts list
	 * @return list of navItems
	 */
	LinkedList<NavItem> getNavItemsForLayouts(HttpServletRequest httpServletRequest, List<Layout> layouts);

	/**
	 * Returns optional with the NavItem for the specified layout if the layout
	 * is found, empty optional otherwise.
	 *
	 * @param httpServletRequest the request
	 * @param groupId the groupId to retrieve the layout from
	 * @param friendlyURL the layout friendly url
	 * @param privateLayout if the layout is private or not
	 * @return the optional with the navItem
	 */
	Optional<NavItem> getOptionalNavItemForLayout(HttpServletRequest httpServletRequest, long groupId, String friendlyURL, boolean privateLayout);

	/**
	 * Retrieves the first layout found with the given portlet id
	 *
	 * @param groupId the groupId
	 * @param portletId the portletId
	 * @param preferenceOnPrivateLayouts if true then the layout is first
	 *            searched in private layouts, then in public ones. Reverse is
	 *            true as well
	 * @return the layout found, or empty optional if none is found
	 */
	Optional<Layout> getPreferredLayoutWithPortletId(long groupId, String portletId, boolean preferenceOnPrivateLayouts);

	/**
	 * Retrieves the first layout found with the given portlet id
	 *
	 * @param groupId the groupId
	 * @param portletId the portletId
	 * @param preferenceOnPrivateLayouts if true then the layout is first
	 *            searched in private layouts, then in public ones. Reverse is
	 *            true as well
	 * @param prefsToMatch map of portlet preferences to match - portlet on the
	 *            layout will have to have them set
	 * @return the layout found, or empty optional if none is found
	 */
	Optional<Layout> getPreferredLayoutWithPortletId(long groupId, String portletId, boolean preferenceOnPrivateLayouts, Map<String, String> prefsToMatch);

	/**
	 * If the current nav items are public pages, it'll return a list of
	 * navItems with the public and private pages. If the current nav items are
	 * private pages, it'll return a list of navItems with the public and
	 * private pages.
	 *
	 * @param request the request
	 * @param currentNavItems the current nav items
	 * @return list of public and private pages
	 */
	List<NavItem> getPublicAndPrivateNavItems(HttpServletRequest request, List<NavItem> currentNavItems);

	/**
	 * For all the layouts specified, it will remove the VIEW permissions for
	 * the roles found and it will set the layout as hidden
	 *
	 * @param companyId the companyId
	 * @param layouts the layouts to update
	 * @param roleNames the roles to remove VIEW permissions from
	 * @throws PortalException any error whilst updating the layout and its
	 *             permissions
	 */
	void hidePagesFromRoles(long companyId, List<Layout> layouts, Set<String> roleNames) throws PortalException;

	/**
	 * For all the layouts specified, it will add the VIEW permissions for the
	 * roles found and it will set the layout as visible
	 *
	 * @param companyId the companyId
	 * @param layouts the layouts to update
	 * @param roleNames the roles to add VIEW permissions from
	 * @throws PortalException any error whilst updating the layout and its
	 *             permissions
	 */
	void showPagesToRoles(long companyId, List<Layout> layouts, Set<String> roleNames) throws PortalException;

}
