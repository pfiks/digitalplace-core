package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLLocationSearchPortletInstanceConfiguration")
public interface DDLLocationSearchPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "0")
	long ddmStructureId();

	@Meta.AD(required = false)
	String destinationPage();

	@Meta.AD(deflt = "location", name = "geoLocationField", required = false)
	String geoLocationField();

}