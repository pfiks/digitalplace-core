package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.display.context;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;

import com.liferay.frontend.taglib.clay.servlet.taglib.display.context.SearchContainerManagementToolbarDisplayContext;
import com.liferay.frontend.taglib.clay.servlet.taglib.util.DropdownItem;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.portal.kernel.util.WebKeys;

public class JournalArticleItemSelectorViewManagementToolbarDisplayContext extends SearchContainerManagementToolbarDisplayContext {

	private final JournalArticleItemSelectorViewDisplayContext journalArticleItemSelectorViewDisplayContext;
	private final ThemeDisplay themeDisplay;

	public JournalArticleItemSelectorViewManagementToolbarDisplayContext(HttpServletRequest httpServletRequest, LiferayPortletRequest liferayPortletRequest,
			LiferayPortletResponse liferayPortletResponse, JournalArticleItemSelectorViewDisplayContext journalArticleItemSelectorViewDisplayContext1) throws PortalException, PortletException {

		super(liferayPortletRequest, liferayPortletResponse, httpServletRequest, journalArticleItemSelectorViewDisplayContext1.getSearchContainer());

		journalArticleItemSelectorViewDisplayContext = journalArticleItemSelectorViewDisplayContext1;

		themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public String getClearResultsURL() {
		PortletURL clearResultsURL = getPortletURL();

		clearResultsURL.setParameter("keywords", StringPool.BLANK);

		return clearResultsURL.toString();
	}

	@Override
	public String getSearchActionURL() {
		PortletURL searchActionURL = getPortletURL();

		return searchActionURL.toString();
	}

	@Override
	public String getSortingOrder() {
		if (Objects.equals(getOrderByCol(), "relevance")) {
			return null;
		}

		return super.getSortingOrder();
	}

	@Override
	public Boolean isSelectable() {
		return false;
	}

	@Override
	protected String getDefaultDisplayStyle() {
		return "descriptive";
	}

	@Override
	protected String[] getDisplayViews() {
		return new String[] { "list", "descriptive", "icon" };
	}

	@Override
	protected List<DropdownItem> getDropdownItems(Map<String, String> entriesMap, PortletURL entryURL, String parameterName, String parameterValue) {

		if (entriesMap == null || entriesMap.isEmpty()) {
			return null;
		}

		ResourceBundle resourceBundle = ResourceBundleUtil.getBundle("content.Language", themeDisplay.getLocale(), getClass());
		List<DropdownItem> dropdownItemList = new ArrayList<>();

		for (Map.Entry<String, String> entry : entriesMap.entrySet()) {
			DropdownItem dropdownItem = new DropdownItem();
			if (parameterValue != null) {
				dropdownItem.setActive(parameterValue.equals(entry.getValue()));
			}
			dropdownItem.setHref(entryURL, parameterName, entry.getValue());
			dropdownItem.setLabel(LanguageUtil.get(resourceBundle, entry.getKey()));
			dropdownItemList.add(dropdownItem);
		}

		return dropdownItemList;
	}

	@Override
	protected String[] getOrderByKeys() {
		String[] orderColumns = { "modified-date", "title" };

		if (journalArticleItemSelectorViewDisplayContext.isSearch()) {
			orderColumns = ArrayUtil.append(orderColumns, "relevance");
		}

		if (journalArticleItemSelectorViewDisplayContext.showArticleId()) {
			orderColumns = ArrayUtil.append(orderColumns, "id");
		}

		return orderColumns;
	}

}