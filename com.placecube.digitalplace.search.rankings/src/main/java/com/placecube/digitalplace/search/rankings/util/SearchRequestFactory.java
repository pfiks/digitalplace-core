package com.placecube.digitalplace.search.rankings.util;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.search.engine.adapter.search.SearchSearchRequest;

@Component(immediate = true, service = { SearchRequestFactory.class })
public class SearchRequestFactory {

	public SearchSearchRequest getSearchSearchRequest() {
		return new SearchSearchRequest();
	}
}
