package com.placecube.digitalplace.frontend.taglib.servlet.taglib.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.InfoItemItemSelectorReturnType;
import com.liferay.item.selector.criteria.info.item.criterion.InfoItemItemSelectorCriterion;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.PortletProvider;
import com.liferay.portal.kernel.portlet.PortletProviderUtil;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.url.builder.PortletURLBuilder;
import com.liferay.portal.kernel.portlet.url.builder.PortletURLBuilder.PortletURLStep;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.taglib.aui.AUIUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AUIUtil.class, PortletProviderUtil.class, PortalUtil.class, AssetCategoryLocalServiceUtil.class, AssetsUtil.class, AssetCategoryUtil.class, ListUtil.class, ItemSelectorUtil.class,
		RequestBackedPortletURLFactoryUtil.class, PortletURLBuilder.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.portlet.PortletProviderUtil" })
public class AssetCategoriesSelectorTagUtilTest extends PowerMockito {

	private static final String EVENT_NAME = "event name";
	private static final long SCOPE_GROUP_ID = 673232L;

	@Captor
	private ArgumentCaptor<InfoItemItemSelectorCriterion> infoItemItemSelectorCriterionArgumentCaptor;

	@Mock
	private AssetVocabulary mockAssetVocabulary1;

	@Mock
	private AssetVocabulary mockAssetVocabulary2;

	@Mock
	private Group mockGroup;

	@Mock
	private ItemSelector mockItemSelector;

	@Mock
	private PortletURL mockItemSelectorPortletURL;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletResponse mockPortletResponse;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private PortletURLStep mockPortletURLStep;

	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	private RequestBackedPortletURLFactory mockRequestBackedPortletURLFactory;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(AUIUtil.class, PortletProviderUtil.class, PortalUtil.class, AssetCategoryLocalServiceUtil.class, AssetsUtil.class, AssetCategoryUtil.class, ItemSelectorUtil.class,
				RequestBackedPortletURLFactoryUtil.class, PortletURLBuilder.class);
	}

	@Test
	public void getAssetCategoriesSelectorId_WhenNoError_ThenReturnsAssetCategoriesSelector() {
		String namespace = "namespace";
		String selectorId = AssetCategoriesSelectorTagUtil.getAssetCategoriesSelectorId(namespace);

		assertThat(selectorId, equalTo(namespace + "assetCategoriesSelector"));
	}

	@Test
	public void getCategoryIds_WhenDoNotIgnoreRequestValueAndHiddenInputValueIsEmpty_ThenReturnsEmptyArray() {
		long vocabularyId = 123;
		boolean ignoreRequestValue = false;
		String hiddenInput = "assetCategories";

		when(mockRequest.getParameter(hiddenInput + StringPool.UNDERLINE + vocabularyId)).thenReturn(StringPool.BLANK);

		long[] categoryIds = AssetCategoriesSelectorTagUtil.getCategoryIds(vocabularyId, ignoreRequestValue, hiddenInput, mockRequest);

		assertThat(categoryIds.length, equalTo(0));

	}

	@Test
	public void getCategoryIds_WhenDoNotIgnoreRequestValueAndHiddenInputValueIsNotEmpty_ThenReturnsArrayOfRequestedCategoryIds() {
		long vocabularyId = 123;
		boolean ignoreRequestValue = false;
		String hiddenInput = "assetCategories";
		long catId1 = 456;
		long catId2 = 789;
		String expectedCategoryIds = catId1 + StringPool.COMMA + catId2;

		when(mockRequest.getParameter(hiddenInput + StringPool.UNDERLINE + vocabularyId)).thenReturn(expectedCategoryIds);

		long[] categoryIds = AssetCategoriesSelectorTagUtil.getCategoryIds(vocabularyId, ignoreRequestValue, hiddenInput, mockRequest);

		assertThat(categoryIds.length, equalTo(2));
		assertThat(categoryIds[0], equalTo(catId1));
		assertThat(categoryIds[1], equalTo(catId2));

	}

	@Test
	public void getCategoryIds_WhenIgnoreRequestValue_ThenReturnsEmptyArray() {
		long vocabularyId = 123;
		boolean ignoreRequestValue = true;
		String hiddenInput = "assetCategories";

		long[] categoryIds = AssetCategoriesSelectorTagUtil.getCategoryIds(vocabularyId, ignoreRequestValue, hiddenInput, mockRequest);

		assertThat(categoryIds.length, equalTo(0));
	}

	@Test
	public void getEventName_WhenNoError_ThenSelectCategoryEventName() {
		String namespace = "namespace";
		String portletId = "abc";

		when(PortletProviderUtil.getPortletId(AssetCategory.class.getName(), PortletProvider.Action.BROWSE)).thenReturn(portletId);
		when(PortalUtil.getPortletNamespace(portletId)).thenReturn(namespace);

		String eventName = AssetCategoriesSelectorTagUtil.getEventName();

		assertThat(eventName, equalTo(namespace + "selectCategory"));
	}

	@Test
	public void getGroupIds_WhenRequestGroupIdsDoesNotExist_ThenReturnCurrentAndAncestorGroupIdsFromScopeGroupId() throws PortalException {
		long[] requestGroupIds = new long[] {};
		long scopeGroupId = 123L;
		long[] expectedGroupIds = new long[] { 456, 789 };
		List<Long> expectedGroupIdsList = new ArrayList<>();
		expectedGroupIdsList.add(456L);
		expectedGroupIdsList.add(789L);
		mockStatic(ListUtil.class);
		when(ListUtil.fromArray(expectedGroupIds)).thenReturn(expectedGroupIdsList);

		when(PortalUtil.getCurrentAndAncestorSiteGroupIds(scopeGroupId)).thenReturn(expectedGroupIds);

		List<Long> groupIds = AssetCategoriesSelectorTagUtil.getGroupIds(requestGroupIds, scopeGroupId);

		assertThat(groupIds.size(), equalTo(expectedGroupIds.length));
		assertEquals(expectedGroupIds[0], (long) groupIds.get(0));
		assertEquals(expectedGroupIds[1], (long) groupIds.get(1));
	}

	@Test
	public void getGroupIds_WhenRequestGroupIdsExist_ThenReturnCurrentAndAncestorGroupIdsFromRequestGroupIds() throws PortalException {
		long[] requestGroupIds = new long[] { 11, 12 };
		long scopeGroupId = 123L;
		long[] expectedGroupIds = new long[] { 456, 789 };
		List<Long> expectedGroupIdsList = new ArrayList<>();
		expectedGroupIdsList.add(456L);
		expectedGroupIdsList.add(789L);
		mockStatic(ListUtil.class);
		when(ListUtil.fromArray(expectedGroupIds)).thenReturn(expectedGroupIdsList);

		when(PortalUtil.getCurrentAndAncestorSiteGroupIds(requestGroupIds)).thenReturn(expectedGroupIds);

		List<Long> groupIds = AssetCategoriesSelectorTagUtil.getGroupIds(requestGroupIds, scopeGroupId);

		assertThat(groupIds.size(), equalTo(expectedGroupIds.length));
		assertEquals(expectedGroupIds[0], (long) groupIds.get(0));
		assertEquals(expectedGroupIds[1], (long) groupIds.get(1));
	}

	@Test
	public void getHiddenInputName_WhenNoError_ThenReturnsInputName() {
		String namespace = "namespace";
		String hiddenInput = "hidden_input";

		String inputName = AssetCategoriesSelectorTagUtil.getHiddenInputName(namespace, hiddenInput);

		assertThat(inputName, equalTo(namespace + hiddenInput + StringPool.UNDERLINE));
	}

	@Test
	public void getNamespace_WhenPortletRequestAndPortletResponseExist_ThenReturnsNamespaceFromPortletRequest() {
		String expectedNamespace = "namespace";

		when(mockRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockRequest.getAttribute(JavaConstants.JAVAX_PORTLET_RESPONSE)).thenReturn(mockPortletResponse);
		when(AUIUtil.getNamespace(mockPortletRequest, mockPortletResponse)).thenReturn(expectedNamespace);

		String namespace = AssetCategoriesSelectorTagUtil.getNamespace(mockRequest);

		assertThat(namespace, equalTo(expectedNamespace));
	}

	@Test
	public void getNamespace_WhenPortletRequestOrPortletResponseDoesNotExist_ThenReturnNamespaceFromRequest() {
		String expectedNamespace = "namespace";

		when(mockRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockRequest.getAttribute(JavaConstants.JAVAX_PORTLET_RESPONSE)).thenReturn(null);
		when(AUIUtil.getNamespace(mockRequest)).thenReturn(expectedNamespace);

		String namespace = AssetCategoriesSelectorTagUtil.getNamespace(mockRequest);

		assertThat(namespace, equalTo(expectedNamespace));
	}

	@Test
	public void getPortletURL_WhenNoErrors_ThenConfiguresItemSelectorToReturnAssetCategories() {
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ItemSelectorUtil.getItemSelector()).thenReturn(mockItemSelector);

		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);

		when(RequestBackedPortletURLFactoryUtil.create(mockRequest)).thenReturn(mockRequestBackedPortletURLFactory);
		when(mockItemSelector.getItemSelectorURL(eq(mockRequestBackedPortletURLFactory), eq(mockGroup), eq(SCOPE_GROUP_ID), eq(EVENT_NAME), any(InfoItemItemSelectorCriterion.class)))
				.thenReturn(mockItemSelectorPortletURL);
		when(PortletURLBuilder.create(mockItemSelectorPortletURL)).thenReturn(mockPortletURLStep);
		when(mockPortletURLStep.buildPortletURL()).thenReturn(mockPortletURL);

		AssetCategoriesSelectorTagUtil.getPortletURL(EVENT_NAME, mockRequest);

		verify(mockItemSelector, times(1)).getItemSelectorURL(eq(mockRequestBackedPortletURLFactory), eq(mockGroup), eq(SCOPE_GROUP_ID), eq(EVENT_NAME),
				infoItemItemSelectorCriterionArgumentCaptor.capture());

		InfoItemItemSelectorCriterion capturedInfoItemItemSelectorCriterion = infoItemItemSelectorCriterionArgumentCaptor.getValue();

		assertThat(capturedInfoItemItemSelectorCriterion.getItemType(), equalTo(AssetCategory.class.getName()));
		assertThat(capturedInfoItemItemSelectorCriterion.isMultiSelection(), equalTo(true));

		List<ItemSelectorReturnType> returnTypes = capturedInfoItemItemSelectorCriterion.getDesiredItemSelectorReturnTypes();

		assertThat(returnTypes.size(), equalTo(1));
		assertThat(returnTypes.get(0), instanceOf(InfoItemItemSelectorReturnType.class));

	}

	@Test
	public void getPortletURL_WhenNoErrors_ThenReturnsItemSelectorURL() {
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ItemSelectorUtil.getItemSelector()).thenReturn(mockItemSelector);

		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);

		when(RequestBackedPortletURLFactoryUtil.create(mockRequest)).thenReturn(mockRequestBackedPortletURLFactory);
		when(mockItemSelector.getItemSelectorURL(eq(mockRequestBackedPortletURLFactory), eq(mockGroup), eq(SCOPE_GROUP_ID), eq(EVENT_NAME), any(InfoItemItemSelectorCriterion.class)))
				.thenReturn(mockItemSelectorPortletURL);
		when(PortletURLBuilder.create(mockItemSelectorPortletURL)).thenReturn(mockPortletURLStep);
		when(mockPortletURLStep.buildPortletURL()).thenReturn(mockPortletURL);

		PortletURL result = AssetCategoriesSelectorTagUtil.getPortletURL(EVENT_NAME, mockRequest);

		assertThat(result, sameInstance(mockPortletURL));
	}

	@Test
	public void getThemeDisplay_WhenNoError_ThenThemeDisplayFromRequest() {
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		ThemeDisplay themeDisplay = AssetCategoriesSelectorTagUtil.getThemeDisplay(mockRequest);

		assertThat(themeDisplay, equalTo(mockThemeDisplay));
	}

	@Test
	public void getVocabulariesDescriptors_WhenClassNameIsNotNullAndIsNotIgnoreRequestValue_ThenReturnVocabularyDescriptorsWithIdAndTitleAndGroupAndSingleSelectParametersSet() throws PortalException {
		long vocabularyId1 = 11;
		long vocabularyId2 = 12;
		long classNameId = 13;
		long classTypePK = 14;
		long[] vocabularyIds = new long[] { vocabularyId1, vocabularyId2 };
		String className = "className";
		String hiddenInput = "hiddenInput";
		List<AssetVocabulary> nonEmptyVocabularies = Arrays.asList(mockAssetVocabulary1, mockAssetVocabulary2);
		List<AssetVocabulary> filteredVocabularies = Arrays.asList(mockAssetVocabulary2);
		boolean showRequiredLabel = true;
		boolean isSingleSelect = true;
		boolean ignoreRequestValue = false;
		boolean isRequired = true;
		long scopeGroupId = 15;
		Locale locale = Locale.CANADA;
		String vocabularyTitle = "vocabularyTitle";
		int visibilityType = 16;

		long[] assetCategoryIds = new long[] { 111, 112 };
		String[] categoryIdsParam = ArrayUtil.toStringArray(assetCategoryIds);
		String[] categoryIdsTitle = new String[] { "title1", "title2" };
		List<Map<String, Object>> selectedItems = new ArrayList<>();
		Map<String, Object> selectedItemsMap = new HashMap<>();
		selectedItemsMap.put("label", categoryIdsTitle[1]);
		selectedItemsMap.put("value", categoryIdsTitle[0]);
		selectedItems.add(selectedItemsMap);

		mockStatic(ListUtil.class);
		when(mockAssetVocabulary1.getVocabularyId()).thenReturn(vocabularyId1);
		when(mockAssetVocabulary2.getVocabularyId()).thenReturn(vocabularyId2);

		when(AssetsUtil.getNonEmptyVocabulariesByIds(vocabularyIds)).thenReturn(nonEmptyVocabularies);
		when(PortalUtil.getClassNameId(className)).thenReturn(classNameId);
		when(ListUtil.filter(eq(nonEmptyVocabularies), any())).thenReturn(filteredVocabularies);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockRequest.getParameterValues(hiddenInput + StringPool.UNDERLINE + vocabularyId2)).thenReturn(categoryIdsParam);
		when(AssetCategoryUtil.getCategoryIdsTitles(assetCategoryIds, vocabularyId2, mockThemeDisplay)).thenReturn(categoryIdsTitle);
		when(mockAssetVocabulary2.isRequired(classNameId, classTypePK) && showRequiredLabel).thenReturn(isRequired);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockAssetVocabulary2.getUnambiguousTitle(filteredVocabularies, scopeGroupId, locale)).thenReturn(vocabularyTitle);
		when(mockAssetVocabulary2.getVisibilityType()).thenReturn(visibilityType);

		List<Map<String, Object>> vocabularyDescriptors = AssetCategoriesSelectorTagUtil.getVocabulariesDescriptors(vocabularyIds, assetCategoryIds, className, classTypePK, showRequiredLabel,
				isSingleSelect, ignoreRequestValue, hiddenInput, mockThemeDisplay, mockRequest);

		assertThat(vocabularyDescriptors.size(), equalTo(1));
		Map<String, Object> vocabularyDescriptor = vocabularyDescriptors.get(0);
		assertThat(vocabularyDescriptor.get("id"), equalTo(vocabularyId2));
		assertThat(vocabularyDescriptor.get("title"), equalTo(vocabularyTitle));
		assertThat(vocabularyDescriptor.get("required"), equalTo(isRequired));
		assertThat(vocabularyDescriptor.get("singleSelect"), equalTo(isSingleSelect));
		assertThat(vocabularyDescriptor.get("selectedCategories"), equalTo(categoryIdsTitle[0]));
		assertThat(vocabularyDescriptor.get("selectedItems"), equalTo(selectedItems));
		assertThat(vocabularyDescriptor.get("visibilityType"), equalTo(visibilityType));
	}

	@Test
	public void getVocabulariesDescriptors_WhenVocabulariesAreNonEmptyAndSelectedCategoryIdsDoesNotExist_ThenReturnVocabularyDescriptorsWithoutSelectedItemsParameterSet() throws PortalException {
		long vocabularyId1 = 11;
		long vocabularyId2 = 12;
		long classNameId = 13;
		long classTypePK = 14;
		long[] vocabularyIds = new long[] { vocabularyId1, vocabularyId2 };
		String className = "className";
		String hiddenInput = "hiddenInput";
		List<AssetVocabulary> nonEmptyVocabularies = Arrays.asList(mockAssetVocabulary1, mockAssetVocabulary2);
		List<AssetVocabulary> filteredVocabularies = Arrays.asList(mockAssetVocabulary2);
		boolean showRequiredLabel = true;
		boolean isSingleSelect = true;
		boolean ignoreRequestValue = false;
		boolean isRequired = true;
		long scopeGroupId = 15;
		Locale locale = Locale.CANADA;
		String vocabularyTitle = "vocabularyTitle";
		int visibilityType = 16;

		long[] assetCategoryIds = new long[] { 111, 112 };
		String[] categoryIdsParam = ArrayUtil.toStringArray(assetCategoryIds);
		String[] categoryIdsTitle = new String[] { "", "" };

		mockStatic(ListUtil.class);
		when(mockAssetVocabulary1.getVocabularyId()).thenReturn(vocabularyId1);
		when(mockAssetVocabulary2.getVocabularyId()).thenReturn(vocabularyId2);
		when(AssetsUtil.getNonEmptyVocabulariesByIds(vocabularyIds)).thenReturn(nonEmptyVocabularies);
		when(PortalUtil.getClassNameId(className)).thenReturn(classNameId);
		when(ListUtil.filter(eq(nonEmptyVocabularies), any())).thenReturn(filteredVocabularies);
		when(mockRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockRequest.getParameterValues(hiddenInput + StringPool.UNDERLINE + vocabularyId2)).thenReturn(categoryIdsParam);
		when(AssetCategoryUtil.getCategoryIdsTitles(assetCategoryIds, vocabularyId2, mockThemeDisplay)).thenReturn(categoryIdsTitle);
		when(mockAssetVocabulary2.isRequired(classNameId, classTypePK) && showRequiredLabel).thenReturn(isRequired);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockAssetVocabulary2.getUnambiguousTitle(filteredVocabularies, scopeGroupId, locale)).thenReturn(vocabularyTitle);
		when(mockAssetVocabulary2.getVisibilityType()).thenReturn(visibilityType);

		List<Map<String, Object>> vocabularyDescriptors = AssetCategoriesSelectorTagUtil.getVocabulariesDescriptors(vocabularyIds, assetCategoryIds, className, classTypePK, showRequiredLabel,
				isSingleSelect, ignoreRequestValue, hiddenInput, mockThemeDisplay, mockRequest);

		assertThat(vocabularyDescriptors.size(), equalTo(1));
		Map<String, Object> vocabularyDescriptor = vocabularyDescriptors.get(0);
		assertThat(vocabularyDescriptor.get("id"), equalTo(vocabularyId2));
		assertThat(vocabularyDescriptor.get("title"), equalTo(vocabularyTitle));
		assertThat(vocabularyDescriptor.get("required"), equalTo(isRequired));
		assertThat(vocabularyDescriptor.get("singleSelect"), equalTo(isSingleSelect));
		assertThat(vocabularyDescriptor.get("selectedCategories"), equalTo(StringPool.BLANK));
		List<Map<String, Object>> selectedItems = (List<Map<String, Object>>) vocabularyDescriptor.get("selectedItems");
		assertThat(selectedItems.size(), equalTo(0));
		assertThat(vocabularyDescriptor.get("visibilityType"), equalTo(visibilityType));
	}
}
