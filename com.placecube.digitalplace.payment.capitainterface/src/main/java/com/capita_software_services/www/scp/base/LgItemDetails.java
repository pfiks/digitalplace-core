/**
 * LgItemDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class LgItemDetails  implements java.io.Serializable {
    private java.lang.String fundCode;

    private java.lang.Boolean isFundItem;

    private java.lang.String additionalReference;

    private java.lang.String narrative;

    private com.capita_software_services.www.scp.base.ThreePartName accountName;

    private com.capita_software_services.www.scp.base.Address accountAddress;

    private com.capita_software_services.www.scp.base.Contact contact;

    public LgItemDetails() {
    }

    public LgItemDetails(
           java.lang.String fundCode,
           java.lang.Boolean isFundItem,
           java.lang.String additionalReference,
           java.lang.String narrative,
           com.capita_software_services.www.scp.base.ThreePartName accountName,
           com.capita_software_services.www.scp.base.Address accountAddress,
           com.capita_software_services.www.scp.base.Contact contact) {
           this.fundCode = fundCode;
           this.isFundItem = isFundItem;
           this.additionalReference = additionalReference;
           this.narrative = narrative;
           this.accountName = accountName;
           this.accountAddress = accountAddress;
           this.contact = contact;
    }


    /**
     * Gets the fundCode value for this LgItemDetails.
     *
     * @return fundCode
     */
    public java.lang.String getFundCode() {
        return fundCode;
    }


    /**
     * Sets the fundCode value for this LgItemDetails.
     *
     * @param fundCode
     */
    public void setFundCode(java.lang.String fundCode) {
        this.fundCode = fundCode;
    }


    /**
     * Gets the isFundItem value for this LgItemDetails.
     *
     * @return isFundItem
     */
    public java.lang.Boolean getIsFundItem() {
        return isFundItem;
    }


    /**
     * Sets the isFundItem value for this LgItemDetails.
     *
     * @param isFundItem
     */
    public void setIsFundItem(java.lang.Boolean isFundItem) {
        this.isFundItem = isFundItem;
    }


    /**
     * Gets the additionalReference value for this LgItemDetails.
     *
     * @return additionalReference
     */
    public java.lang.String getAdditionalReference() {
        return additionalReference;
    }


    /**
     * Sets the additionalReference value for this LgItemDetails.
     *
     * @param additionalReference
     */
    public void setAdditionalReference(java.lang.String additionalReference) {
        this.additionalReference = additionalReference;
    }


    /**
     * Gets the narrative value for this LgItemDetails.
     *
     * @return narrative
     */
    public java.lang.String getNarrative() {
        return narrative;
    }


    /**
     * Sets the narrative value for this LgItemDetails.
     *
     * @param narrative
     */
    public void setNarrative(java.lang.String narrative) {
        this.narrative = narrative;
    }


    /**
     * Gets the accountName value for this LgItemDetails.
     *
     * @return accountName
     */
    public com.capita_software_services.www.scp.base.ThreePartName getAccountName() {
        return accountName;
    }


    /**
     * Sets the accountName value for this LgItemDetails.
     *
     * @param accountName
     */
    public void setAccountName(com.capita_software_services.www.scp.base.ThreePartName accountName) {
        this.accountName = accountName;
    }


    /**
     * Gets the accountAddress value for this LgItemDetails.
     *
     * @return accountAddress
     */
    public com.capita_software_services.www.scp.base.Address getAccountAddress() {
        return accountAddress;
    }


    /**
     * Sets the accountAddress value for this LgItemDetails.
     *
     * @param accountAddress
     */
    public void setAccountAddress(com.capita_software_services.www.scp.base.Address accountAddress) {
        this.accountAddress = accountAddress;
    }


    /**
     * Gets the contact value for this LgItemDetails.
     *
     * @return contact
     */
    public com.capita_software_services.www.scp.base.Contact getContact() {
        return contact;
    }


    /**
     * Sets the contact value for this LgItemDetails.
     *
     * @param contact
     */
    public void setContact(com.capita_software_services.www.scp.base.Contact contact) {
        this.contact = contact;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LgItemDetails)) return false;
        LgItemDetails other = (LgItemDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.fundCode==null && other.getFundCode()==null) ||
             (this.fundCode!=null &&
              this.fundCode.equals(other.getFundCode()))) &&
            ((this.isFundItem==null && other.getIsFundItem()==null) ||
             (this.isFundItem!=null &&
              this.isFundItem.equals(other.getIsFundItem()))) &&
            ((this.additionalReference==null && other.getAdditionalReference()==null) ||
             (this.additionalReference!=null &&
              this.additionalReference.equals(other.getAdditionalReference()))) &&
            ((this.narrative==null && other.getNarrative()==null) ||
             (this.narrative!=null &&
              this.narrative.equals(other.getNarrative()))) &&
            ((this.accountName==null && other.getAccountName()==null) ||
             (this.accountName!=null &&
              this.accountName.equals(other.getAccountName()))) &&
            ((this.accountAddress==null && other.getAccountAddress()==null) ||
             (this.accountAddress!=null &&
              this.accountAddress.equals(other.getAccountAddress()))) &&
            ((this.contact==null && other.getContact()==null) ||
             (this.contact!=null &&
              this.contact.equals(other.getContact())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFundCode() != null) {
            _hashCode += getFundCode().hashCode();
        }
        if (getIsFundItem() != null) {
            _hashCode += getIsFundItem().hashCode();
        }
        if (getAdditionalReference() != null) {
            _hashCode += getAdditionalReference().hashCode();
        }
        if (getNarrative() != null) {
            _hashCode += getNarrative().hashCode();
        }
        if (getAccountName() != null) {
            _hashCode += getAccountName().hashCode();
        }
        if (getAccountAddress() != null) {
            _hashCode += getAccountAddress().hashCode();
        }
        if (getContact() != null) {
            _hashCode += getContact().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LgItemDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lgItemDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "fundCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isFundItem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "isFundItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "additionalReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("narrative");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "narrative"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "accountName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "threePartName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "accountAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "address"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contact");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "contact"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "contact"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
