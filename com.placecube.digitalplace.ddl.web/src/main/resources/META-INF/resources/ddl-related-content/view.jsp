<%@ include file="../init.jsp" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dp-ddl" uri="http://placecube.com/digitalplace/tld/ddl" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<div class="ddl-taglib ddl-taglib-related-content">
	<div class="container">
		<h2><liferay-ui:message key="related-content"/>:</h2>
		<%@ include file="../ddl-search/view.jsp" %>
	</div>
</div>