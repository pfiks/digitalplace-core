package com.placecube.digitalplace.ddl.web.constants;

public final class LocationSearchParameters {

	public static final String FORMATTED_ADDRESS = "formattedAddress";

	public static final String GEO_LOCATION_FIELD = "geoLocationField";

	public static final String SEARCH_LATITUDE = "latitude";

	public static final String SEARCH_LONGITUDE = "longitude";

	private LocationSearchParameters() {
	}

}
