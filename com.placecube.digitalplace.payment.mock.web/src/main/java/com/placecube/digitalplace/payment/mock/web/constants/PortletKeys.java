package com.placecube.digitalplace.payment.mock.web.constants;

public final class PortletKeys {

	public static final String MOCK_PAYMENT_PORTLET = "com_placecube_digitalplace_payment_mock_MockPaymentPortlet";

	private PortletKeys() {
	}

}
