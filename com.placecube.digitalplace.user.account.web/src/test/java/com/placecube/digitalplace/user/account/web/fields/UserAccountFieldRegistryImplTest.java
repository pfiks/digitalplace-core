package com.placecube.digitalplace.user.account.web.fields;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;

public class UserAccountFieldRegistryImplTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;

	@Mock
	private UserAccountField mockCreateUserAccountField1;

	@Mock
	private UserAccountField mockCreateUserAccountField2;

	@InjectMocks
	private UserAccountFieldRegistryImpl userAccountFieldRegistryImpl;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getCreateUserAccountFields_WhenNoError_ThenFieldsSortedByDisplayOrderHighestToLow() {
		mockBasicSetup();
		when(mockCreateUserAccountField1.getDisplayOrder()).thenReturn(200);
		when(mockCreateUserAccountField2.getDisplayOrder()).thenReturn(100);

		List<UserAccountField> createUserAccountFields = userAccountFieldRegistryImpl.getUserAccountFields(COMPANY_ID);

		assertThat(createUserAccountFields.get(0), is(mockCreateUserAccountField1));
		assertThat(createUserAccountFields.get(1), is(mockCreateUserAccountField2));
	}

	@Test
	public void getCreateUserAccountFields_WhenNoError_ThenOnlyEnabledFieldsReturned() {
		when(mockCreateUserAccountField1.isEnabled(COMPANY_ID)).thenReturn(false);
		when(mockCreateUserAccountField2.isEnabled(COMPANY_ID)).thenReturn(true);
		addCreateUserAccountField(mockCreateUserAccountField1);
		addCreateUserAccountField(mockCreateUserAccountField2);

		List<UserAccountField> createUserAccountFields = userAccountFieldRegistryImpl.getUserAccountFields(COMPANY_ID);

		assertThat(createUserAccountFields.size(), equalTo(1));
		assertThat(createUserAccountFields.get(0).isEnabled(COMPANY_ID), is(true));
	}

	@Test
	public void getCreateUserAccountFieldsByStep_WhenCreationStepAdditionalDetails_ThenOnlyFieldsWithThisStepReturned() {
		mockBasicSetup();
		when(mockCreateUserAccountField1.getAccountCreationStep()).thenReturn(AccountCreationStep.ADDITIONAL_DETAILS);
		when(mockCreateUserAccountField2.getAccountCreationStep()).thenReturn(AccountCreationStep.PERSONAL_DETAILS);

		List<UserAccountField> createUserAccountFields = userAccountFieldRegistryImpl.getUserAccountFieldsByStep(AccountCreationStep.ADDITIONAL_DETAILS, COMPANY_ID);

		assertThat(createUserAccountFields.get(0).getAccountCreationStep(), is(AccountCreationStep.ADDITIONAL_DETAILS));
	}

	@Test
	public void getCreateUserAccountFieldsByStep_WhenCreationStepPersonalDetails_ThenOnlyFieldsWithThisStepReturned() {
		mockBasicSetup();
		when(mockCreateUserAccountField1.getAccountCreationStep()).thenReturn(AccountCreationStep.ADDITIONAL_DETAILS);
		when(mockCreateUserAccountField2.getAccountCreationStep()).thenReturn(AccountCreationStep.PERSONAL_DETAILS);

		List<UserAccountField> createUserAccountFields = userAccountFieldRegistryImpl.getUserAccountFieldsByStep(AccountCreationStep.PERSONAL_DETAILS, COMPANY_ID);

		assertThat(createUserAccountFields.get(0).getAccountCreationStep(), is(AccountCreationStep.PERSONAL_DETAILS));
	}

	@Test
	public void unsetCreateUserAccountField_WhenNoErrors_ThenFieldIsRemoved() {
		when(mockCreateUserAccountField1.isEnabled(COMPANY_ID)).thenReturn(true);
		when(mockCreateUserAccountField2.isEnabled(COMPANY_ID)).thenReturn(true);
		addCreateUserAccountField(mockCreateUserAccountField1);
		addCreateUserAccountField(mockCreateUserAccountField2);

		userAccountFieldRegistryImpl.unsetUserAccountField(mockCreateUserAccountField1);

		List<UserAccountField> createUserAccountFields = userAccountFieldRegistryImpl.getUserAccountFields(COMPANY_ID);
		assertThat(createUserAccountFields.size(), equalTo(1));
	}

	private void addCreateUserAccountField(UserAccountField createUserAccountField) {
		userAccountFieldRegistryImpl.setUserAccountField(createUserAccountField);
	}

	private void mockBasicSetup() {
		when(mockCreateUserAccountField1.isEnabled(COMPANY_ID)).thenReturn(true);
		when(mockCreateUserAccountField2.isEnabled(COMPANY_ID)).thenReturn(true);
		addCreateUserAccountField(mockCreateUserAccountField1);
		addCreateUserAccountField(mockCreateUserAccountField2);
	}
}
