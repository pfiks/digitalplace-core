package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetParserService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.DDL_FIELD_FACET_PORTLET, service = ConfigurationAction.class)
public class DDLFieldFacetPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Reference
	private DDLFieldFacetParserService ddlFieldFacetParserService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		DDLFieldFacetPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(DDLFieldFacetPortletInstanceConfiguration.class, themeDisplay);
		List<DDMStructure> availableDDMStructures = ddlConfigurationService.getAvailableDDMStructures(themeDisplay.getScopeGroup());

		httpServletRequest.setAttribute("configuration", configuration);
		httpServletRequest.setAttribute("availableStructures", availableDDMStructures);

		long ddmStructureId = configuration.ddmStructureId();

		if (ddmStructureId > 0) {
			DDMStructure ddmStructure = ddmStructureLocalService.fetchDDMStructure(ddmStructureId);
			httpServletRequest.setAttribute("availableFields", ddlFieldFacetParserService.parseToFilterFields(ddmStructure.getDDMForm().getDDMFormFields()));
		}

		long ddmLinkStructureId = configuration.ddmLinkStructureId();

		if (ddmLinkStructureId > 0) {
			DDMStructure ddmStructure = ddmStructureLocalService.fetchDDMStructure(ddmLinkStructureId);
			httpServletRequest.setAttribute("availableLinkFields", ddlFieldFacetParserService.parseToFilterFields(ddmStructure.getDDMForm().getDDMFormFields()));
		}

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		boolean alphaSort = ParamUtil.getBoolean(actionRequest, DDLFieldFacetPortletConfigurationConstants.ALPHA_SORT);
		long ddmStructureId = ParamUtil.getLong(actionRequest, DDLFieldFacetPortletConfigurationConstants.STRUCTURE_ID, 0);
		long ddmLinkStructureId = ParamUtil.getLong(actionRequest, DDLFieldFacetPortletConfigurationConstants.DDM_LINK_STRUCTURE_ID, 0);
		String displayStyle = ParamUtil.getString(actionRequest, DDLFieldFacetPortletConfigurationConstants.DISPLAY_STYLE, StringPool.BLANK);
		String filterField = ParamUtil.getString(actionRequest, "filterField", StringPool.BLANK);
		String filterColour = ParamUtil.getString(actionRequest, DDLFieldFacetPortletConfigurationConstants.FILTER_COLOUR, StringPool.BLANK);
		String ddlRecordLinkFieldName = ParamUtil.getString(actionRequest, DDLFieldFacetPortletConfigurationConstants.DDL_RECORD_LINK_FIELD_NAME, StringPool.BLANK);
		boolean invisible = ParamUtil.getBoolean(actionRequest, DDLFieldFacetPortletConfigurationConstants.INVISIBLE);
		boolean isFieldNameDDLRecordLink = ParamUtil.getBoolean(actionRequest, DDLFieldFacetPortletConfigurationConstants.IS_FIELD_NAME_DDL_RECORD_LINK);
		String preselectedValue = ParamUtil.getString(actionRequest, DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, StringPool.BLANK);
		String customHeading = ParamUtil.getString(actionRequest, DDLFieldFacetPortletConfigurationConstants.CUSTOM_HEADING, StringPool.BLANK);
		boolean showHeadingAsFirstDropdownOption = ParamUtil.getBoolean(actionRequest, DDLFieldFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION);

		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.ALPHA_SORT, String.valueOf(alphaSort));
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.CUSTOM_HEADING, customHeading);
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.DISPLAY_STYLE, displayStyle);
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.DDM_LINK_STRUCTURE_ID, String.valueOf(ddmLinkStructureId));
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.STRUCTURE_ID, String.valueOf(ddmStructureId));
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.FIELD_NAME, filterField);
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.FILTER_COLOUR, filterColour);
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.INVISIBLE, String.valueOf(invisible));
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.DDL_RECORD_LINK_FIELD_NAME, String.valueOf(ddlRecordLinkFieldName));
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.IS_FIELD_NAME_DDL_RECORD_LINK, String.valueOf(isFieldNameDDLRecordLink));
		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION, String.valueOf(showHeadingAsFirstDropdownOption));

		setPreference(actionRequest, DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, preselectedValue);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
