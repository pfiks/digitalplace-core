package com.placecube.digitalplace.payment.capitapaymentinterface.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Calendar;

import javax.xml.rpc.ServiceException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;
import com.placecube.digitalplace.payment.capitainterface.service.BuildCredentialsService;

import uk.co.capita_software.support.selfservice.Credentials;
import uk.co.capita_software.support.selfservice.RequestIdentification;
import uk.co.capita_software.support.selfservice.Signature;
import uk.co.capita_software.support.selfservice.Subject;
import uk.co.capita_software.support.selfservice.SystemCode;

public class BuildCredentialsServiceTest {

	private BuildCredentialsService credentialsService;

	@Mock
	private PaymentProperties mockPaymentProperties;

	@Before
	public void setUp() throws Exception {

		initMocks(this);
		credentialsService = new BuildCredentialsService(mockPaymentProperties);
	}

	@Test
	public void testBuildCredentials() {

		when(mockPaymentProperties.getHmacKey()).thenReturn("hmackey");
		int scpid = 8389789;
		when(mockPaymentProperties.getScpId()).thenReturn(scpid);

		Credentials cred = credentialsService.buildCredentials("christine@mailinator.com");
		assertNotNull(cred);
		assertNotNull(cred.getRequestIdentification());
		assertNotNull(cred.getRequestIdentification().getTimeStamp());
		assertNotNull(cred.getRequestIdentification().getUniqueReference());

		assertNotNull(cred.getSignature());
		assertNotNull(cred.getSignature().getDigest());
		assertNotNull(cred.getSignature().getAlgorithm());
		assertNotNull(cred.getSignature().getHmacKeyID());

		assertNotNull(cred.getSubject());
		assertTrue(cred.getSubject().getIdentifier() > 0);
		assertNotNull(cred.getSubject().getSubjectType());
		assertNotNull(cred.getSubject().getSystemCode());

		System.out.println(cred);
	}

	@Test
	public void testBuildRequestIdentification() {

		when(mockPaymentProperties.getHmacKey()).thenReturn("hmackey");

		RequestIdentification reqId1 = credentialsService.buildRequestIdentification("christine@mailinator.com");
		RequestIdentification reqId2 = credentialsService.buildRequestIdentification("christine@mailinator.com");

		assertNotEquals(reqId1.getUniqueReference(), reqId2.getUniqueReference());

		Calendar testCal = Calendar.getInstance();
		assertTrue(reqId1.getTimeStamp().startsWith(Integer.toString(testCal.get(Calendar.YEAR))));
		assertTrue(reqId2.getTimeStamp().startsWith(Integer.toString(testCal.get(Calendar.YEAR))));
	}

	@Test
	public void testBuildSignature() {

		when(mockPaymentProperties.getHmacKey()).thenReturn("hmackey");

		RequestIdentification reqId1 = credentialsService.buildRequestIdentification("christine@mailinator.com");
		Signature sig = credentialsService.buildSignature(reqId1);

		assertNotNull(sig);
		assertNotNull(sig.getDigest());
		assertNotNull(sig.getAlgorithm());
		assertNotNull(sig.getHmacKeyID());

		reqId1 = null;
		sig = credentialsService.buildSignature(reqId1);

		assertNull(sig);

		reqId1 = credentialsService.buildRequestIdentification("christine@mailinator.com");
		reqId1.setTimeStamp("");
		sig = credentialsService.buildSignature(reqId1);
		assertNull(sig);

		reqId1 = credentialsService.buildRequestIdentification("christine@mailinator.com");
		reqId1.setUniqueReference("");
		sig = credentialsService.buildSignature(reqId1);
		assertNull(sig);

	}

	@Test
	public void testBuildSubject() throws ServiceException {

		int scpid = 8389789;
		when(mockPaymentProperties.getScpId()).thenReturn(scpid);
		Subject subject = credentialsService.buildSubject();

		assertThat(subject.getIdentifier(), equalTo(scpid));
		assertEquals(subject.getSubjectType(), CapitaConstants.SUBJECT_TYPE);
		assertEquals(subject.getSystemCode(), SystemCode.SCP);

	}

	@Test
	public void testGenerateTimeStamp() {
		String timestamp = credentialsService.generateTimeStamp();

		final Calendar cal = Calendar.getInstance();
		assertTrue(timestamp.length() == 14);
		assertTrue(timestamp.startsWith(Integer.toString(cal.get(Calendar.YEAR))));
		assertTrue(Integer.parseInt(timestamp.substring(4, 6)) == cal.get(Calendar.MONTH) + 1);
		assertTrue(Integer.parseInt(timestamp.substring(6, 8)) == cal.get(Calendar.DAY_OF_MONTH));
	}

}
