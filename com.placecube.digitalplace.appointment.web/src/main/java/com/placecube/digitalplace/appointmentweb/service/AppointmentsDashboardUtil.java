package com.placecube.digitalplace.appointmentweb.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.object.model.ObjectEntry;
import com.liferay.object.service.ObjectEntryLocalService;
import com.liferay.petra.sql.dsl.DSLQueryFactoryUtil;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.common.time.model.DateTimeSelection;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.model.AppointmentEntryTable;
import com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService;
import com.placecube.digitalplace.appointment.web.model.AppointmentDashboardModel;

@Component(immediate = true, service = AppointmentsDashboardUtil.class)
public class AppointmentsDashboardUtil {

	@Reference
	private AppointmentEntryLocalService appointmentEntryLocalService;

	@Reference
	private ObjectEntryLocalService objectEntryLocalService;

	@Reference
	private SummaryService summaryUtil;

	public List<AppointmentDashboardModel> convertAppointmentEntriresToAppointmentDashboardModels(List<AppointmentEntry> appointmentEntries, String baseUrl, String summaryExpression) {
		List<AppointmentDashboardModel> appointmentDashboardModels = new ArrayList<>();

		boolean buildSummary = Validator.isNotNull(summaryExpression);
		for (AppointmentEntry appointmentEntry : appointmentEntries) {
			String summary = StringPool.BLANK;
			if (buildSummary) {
				ObjectEntry objectEntry = objectEntryLocalService.fetchObjectEntry(appointmentEntry.getEntryClassPK());
				if (Validator.isNotNull(objectEntry)) {
					summary = summaryUtil.replacePlaceholders(summaryExpression, objectEntry.getValues());
				}
			}

			AppointmentDashboardModel appointmentDashboardModel = new AppointmentDashboardModel(appointmentEntry.getEntryClassPK(), appointmentEntry.getAppointmentStartDate(), summary,
					getEntryUrl(baseUrl, appointmentEntry));

			appointmentDashboardModels.add(appointmentDashboardModel);
		}

		return appointmentDashboardModels;
	}

	public List<String> getAvailableServiceIds(long companyId) {
		DSLQuery dslQuery = DSLQueryFactoryUtil.selectDistinct(AppointmentEntryTable.INSTANCE.serviceId)//
				.from(AppointmentEntryTable.INSTANCE)//
				.where(AppointmentEntryTable.INSTANCE.companyId.eq(companyId));
		return appointmentEntryLocalService.dslQuery(dslQuery);
	}

	public Calendar getEndOfDayCalendar(String dateValue) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date parsedDate = dateFormat.parse(dateValue);
		Calendar dateCalendar = CalendarFactoryUtil.getCalendar();
		dateCalendar.setTime(parsedDate);
		dateCalendar.set(Calendar.HOUR_OF_DAY, 23);
		dateCalendar.set(Calendar.MINUTE, 59);
		dateCalendar.set(Calendar.SECOND, 59);
		dateCalendar.set(Calendar.MILLISECOND, 999);

		return dateCalendar;
	}

	public SearchContainer<AppointmentDashboardModel> getSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse) {
		SearchContainer<AppointmentDashboardModel> searchContainer = new SearchContainer<>(renderRequest, null, null, "cur", 20, renderResponse.createRenderURL(), null, "no-appointments-found");
		searchContainer.setDeltaConfigurable(true);
		searchContainer.setId("appointmentSearchContainer");

		return searchContainer;
	}

	public DateTimeSelection getEmptyDateTimeSelection() {
		DateTimeSelection dateTimeSelection = new DateTimeSelection();
		dateTimeSelection.setDay(0);
		dateTimeSelection.setMonth(-1);
		dateTimeSelection.setYear(0);
		return dateTimeSelection;
	}

	public Calendar getStartOfDayCalendar(String dateValue) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date parsedDate = dateFormat.parse(dateValue);
		Calendar dateCalendar = CalendarFactoryUtil.getCalendar();
		dateCalendar.setTime(parsedDate);
		dateCalendar.set(Calendar.HOUR_OF_DAY, 0);
		dateCalendar.set(Calendar.MINUTE, 0);
		dateCalendar.set(Calendar.SECOND, 0);
		dateCalendar.set(Calendar.MILLISECOND, 0);

		return dateCalendar;
	}

	private String getEntryUrl(String baseUrl, AppointmentEntry appointmentEntry) {
		return baseUrl + "/" + appointmentEntry.getEntryClassNameId() + "/" + appointmentEntry.getEntryClassPK();
	}

}
