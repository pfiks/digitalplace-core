package com.placecube.digitalplace.template.contributor.internal.util;

import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_ADDRESS_LOOKUP_SERVICE;
import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_CATEGORY_RETRIEVAL_SERVICE;
import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_CONSTANTS_VIEW;
import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_DYNAMIC_DATA_LIST_SERVICE;
import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_DL_URL_HELPER;
import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_JOURNAL_ARTICLE_RETRIEVAL_SERVICE;
import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_LANGUAGE;
import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_SERVICE_CONTEXT;
import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_WORKFLOW_STATUS_ANY;
import static com.placecube.digitalplace.template.contributor.freemarker.DigitalPlaceFreemarkerTemplateContextContributor.DIGITALPLACE_WORKFLOW_STATUS_APPROVED;

import java.io.File;
import java.io.IOException;

import com.liferay.document.library.util.DLURLHelper;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.category.service.CategoryRetrievalService;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.journal.service.JournalArticleRetrievalService;

public class UpgradeHelper {

	private UpgradeHelper() {

	}

	public static boolean hasComponent(String script) {
		return script.contains(CategoryRetrievalService.class.getName()) || script.contains(DynamicDataListService.class.getName()) || script.contains(JournalArticleRetrievalService.class.getName())
				|| script.contains(LanguageUtil.class.getName()) || script.contains(WorkflowConstants.class.getName()) || script.contains(ServiceContextThreadLocal.class.getName())
				|| script.contains(Constants.class.getName()) || script.contains(DLURLHelper.class.getName());

	}

	public static String upgradeScript(String script) {
		String upgradedScript = replaceServiceLocatorCallByReference(script, CategoryRetrievalService.class, DIGITALPLACE_CATEGORY_RETRIEVAL_SERVICE);
		upgradedScript = replaceServiceLocatorCallByReference(upgradedScript, DynamicDataListService.class, DIGITALPLACE_DYNAMIC_DATA_LIST_SERVICE);
		upgradedScript = replaceServiceLocatorCallByReference(upgradedScript, DLURLHelper.class, DIGITALPLACE_DL_URL_HELPER);
		upgradedScript = replaceServiceLocatorCallByReference(upgradedScript, JournalArticleRetrievalService.class, DIGITALPLACE_JOURNAL_ARTICLE_RETRIEVAL_SERVICE);
		upgradedScript = replaceServiceContextReferences(upgradedScript);
		upgradedScript = replaceWorkflowConstantsReferences(upgradedScript);
		upgradedScript = replaceConstantsReferences(upgradedScript);
		upgradedScript = replaceLanguageReferences(upgradedScript);
		return removeEmptyAssigns(upgradedScript);
	}

	public static boolean hasAddressLookupServiceComponent(String script) {
		return Validator.isNotNull(script) && script.contains(AddressLookupService.class.getName());
	}

	public static String replaceAddressLookupServiceInScript(String script) {
		return UpgradeHelper.replaceServiceLocatorCallByReference(script, AddressLookupService.class, DIGITALPLACE_ADDRESS_LOOKUP_SERVICE);
	}

	public static File getOutputFile(UpgradeProcess upgradeProcess) {
		return new File(System.getProperty("liferay.home") + "/upgrade/" + upgradeProcess.getClass().getName() + ".txt");
	}

	public static void writeOutput(File file, String output) throws IOException {
		FileUtil.write(file, output, false, true);
	}

	private static String removeEmptyAssigns(String script) {
		return script.replaceAll("<\\s?#assign\\s*/?>", StringPool.BLANK);
	}

	private static String replaceServiceLocatorCallByReference(String script, Class clazz, String reference) {
		return script.replace("serviceLocator.findService(\"" + clazz.getName() + "\")", reference);
	}

	private static String replaceWorkflowConstantsReferences(String script) {
		return script.replaceAll("WorkflowConstants\\s*=\\s*staticUtil\\[\"com\\.liferay\\.portal\\.kernel\\.workflow\\.WorkflowConstants\"]", StringPool.BLANK).//
				replace("WorkflowConstants.STATUS_APPROVED", DIGITALPLACE_WORKFLOW_STATUS_APPROVED).//
				replace("WorkflowConstants.STATUS_ANY", DIGITALPLACE_WORKFLOW_STATUS_ANY);
	}

	private static String replaceConstantsReferences(String script) {
		return script.replaceAll("Constants\\s*=\\s*staticUtil\\[\"com\\.liferay\\.portal\\.kernel\\.util\\.Constants\"]", StringPool.BLANK).//
				replace("Constants.VIEW", DIGITALPLACE_CONSTANTS_VIEW).//
				replace("digitalplace_workflowConstantsView", DIGITALPLACE_CONSTANTS_VIEW);
	}

	private static String replaceLanguageReferences(String script) {

		return script.replaceAll("LanguageUtil\\s*=\\s*staticUtil\\[\"com\\.liferay\\.portal\\.kernel\\.language\\.LanguageUtil\"]", StringPool.BLANK).//
				replaceAll("\\bLanguageUtil\\b", DIGITALPLACE_LANGUAGE);
	}

	private static String replaceServiceContextReferences(String script) {
		return script.replaceAll("serviceContext\\s*=\\s*staticUtil\\[\"com\\.liferay\\.portal\\.kernel\\.service\\.ServiceContextThreadLocal\"]\\.getServiceContext\\(\\)", StringPool.BLANK)
				.replaceAll("\\bserviceContext\\b", DIGITALPLACE_SERVICE_CONTEXT);
	}

}
