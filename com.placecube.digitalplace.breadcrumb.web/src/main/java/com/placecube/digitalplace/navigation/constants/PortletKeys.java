package com.placecube.digitalplace.navigation.constants;

public final class PortletKeys {

	public static final String BREADCRUMB = "com_placecube_digitalplace_navigation_portlet_BreadcrumbPortlet";

	private PortletKeys() {
		return;
	}

}
