package com.placecube.digitalplace.twitterfeed.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;
import aQute.bnd.annotation.metatype.Meta.Type;

@ExtendedObjectClassDefinition(scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE)
@Meta.OCD(id = "com.placecube.digitalplace.twitterfeed.configuration.TwitterFeedPortletInstanceConfiguration", name = "digitalplace-twitterfeed-twitterFeedPortletInstanceConfiguration", localization = "content/Language")
public interface TwitterFeedPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "5", type = Type.Integer, name = "number-of-items-to-display")
	int feedsToShow();

	@Meta.AD(required = false, deflt = "", type = Type.String, name = "embedded")
	String twitterEmbedded();

	@Meta.AD(required = false, deflt = "", type = Type.String, name = "screen-name")
	String twitterUsername();

}
