package com.placecube.digitalplace.search.shared.model;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.Document;

/**
 * Represents a shared search response
 *
 */
public interface SharedSearchResponse {

	List<SharedActiveFilter> getActiveFilters();

	Set<String> getActiveValuesForFilter(String filterName);

	String getEscapedSearchResultsIteratorURL();

	/**
	 * For the specified facet field name returns a map containing the facet
	 * values and their frequencies. If no facet is found then an empty map is
	 * returned.
	 *
	 * @param fieldName the facet field name
	 * @return facetValue-facetValueFrequency
	 */
	Map<String, Integer> getFacetValues(String fieldName);

	String getIteratorURL();

	String getKeywords();

	SearchContainer getSearchContainerForResults(String searchContainerId, List results, String noResultsMessage);

	Document[] getSearchResults();

	String getSharedSearchSessionSingleValuedAttribute(String attributeName);

	int getTotalResults();

}
