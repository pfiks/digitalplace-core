package com.placecube.digitalplace.group.rest.internal.constants;

public final class ResponseEntry {

	public static final String EVENT = "event";

	public static final String GROUP_ID = "groupId";

	public static final String ENTRY_NAME = "name";

	public static final String ENTRY_ID = "id";

	public static final String ENTRY_DATE = "date";

	public static final String USER_ID = "userId";

	private ResponseEntry() {
	}

}
