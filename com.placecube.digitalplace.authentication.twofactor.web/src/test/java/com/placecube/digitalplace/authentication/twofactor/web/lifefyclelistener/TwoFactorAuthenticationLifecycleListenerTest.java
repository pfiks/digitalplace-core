package com.placecube.digitalplace.authentication.twofactor.web.lifefyclelistener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.authentication.twofactor.web.constants.WebContentArticles;

public class TwoFactorAuthenticationLifecycleListenerTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@Mock
	private Company mockCompany;

	@Mock
	private Group mockGroup;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private TwoFactorAuthenticationSetupService mockTwoFactorAuthenticationSetupService;

	@InjectMocks
	private TwoFactorAuthenticationLifecycleListener twoFactorAuthenticationLifecycleListener;

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenExceptionAddingFolder_ThenThrowsPortalException() throws Exception {
		mockServiceContextDetails();
		when(mockTwoFactorAuthenticationSetupService.addFolder(mockServiceContext)).thenThrow(new PortalException());

		twoFactorAuthenticationLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = Exception.class)
	public void portalInstanceRegistered_WhenExceptionAddingSetupArticle_ThenThrowsException() throws Exception {
		mockServiceContextDetails();
		when(mockTwoFactorAuthenticationSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);
		doThrow(new Exception()).when(mockTwoFactorAuthenticationSetupService).addArticle(WebContentArticles.TWO_FACTOR_SETUP, mockJournalFolder, mockServiceContext);

		twoFactorAuthenticationLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = Exception.class)
	public void portalInstanceRegistered_WhenExceptionAddingVerifyCodeArticle_ThenThrowsException() throws Exception {
		mockServiceContextDetails();
		when(mockTwoFactorAuthenticationSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);
		doThrow(new Exception()).when(mockTwoFactorAuthenticationSetupService).addArticle(WebContentArticles.TWO_FACTOR_VERIFY_CODE, mockJournalFolder, mockServiceContext);

		twoFactorAuthenticationLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenExceptionRetrievingTheGuestGroup_ThenThrowsPortalException() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenThrow(new PortalException());

		twoFactorAuthenticationLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesSetupArticle() throws Exception {
		mockServiceContextDetails();
		when(mockTwoFactorAuthenticationSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		twoFactorAuthenticationLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockTwoFactorAuthenticationSetupService, times(1)).addArticle(WebContentArticles.TWO_FACTOR_SETUP, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesVerifyCodeArticle() throws Exception {
		mockServiceContextDetails();
		when(mockTwoFactorAuthenticationSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		twoFactorAuthenticationLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockTwoFactorAuthenticationSetupService, times(1)).addArticle(WebContentArticles.TWO_FACTOR_VERIFY_CODE, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesAuthenticatorCodeResetArticle() throws Exception {
		mockServiceContextDetails();
		when(mockTwoFactorAuthenticationSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		twoFactorAuthenticationLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockTwoFactorAuthenticationSetupService, times(1)).addArticle(WebContentArticles.TWO_FACTOR_USER_RESET, mockJournalFolder, mockServiceContext);
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

	private void mockServiceContextDetails() throws PortalException {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockTwoFactorAuthenticationSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
	}

}
