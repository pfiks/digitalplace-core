package com.placecube.digitalplace.payment.mock.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.payment.mock.web.constants.ParamKeys;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor("com.liferay.portal.kernel.util.ParamUtil")
public class RedirectMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private RedirectMVCActionCommand redirectMVCActionCommand;

	@Test
	public void doProcessAction_WhenCardNumberNotStartsWith99AndPaymentSuccess_ThenSendRedirectWithPaymentSuccessTrueParameter() throws Exception {
		String cardNumber = "123456789";
		String returnUrl = "http://redirect/after-confirmation";
		String fullUrl = returnUrl + "&paymentSuccess=true";

		when(ParamUtil.getString(mockActionRequest, "cardNumber", StringPool.BLANK)).thenReturn(cardNumber);
		when(ParamUtil.getString(mockActionRequest, ParamKeys.RETURN_URL, StringPool.BLANK)).thenReturn(returnUrl);
		when(mockPortal.getHttpServletResponse(mockActionResponse)).thenReturn(mockHttpServletResponse);

		redirectMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockHttpServletResponse, times(1)).sendRedirect(fullUrl);
	}

	@Test
	public void doProcessAction_WhenCardNumberStartsWith99_ThenSendRedirectWithPaymentSuccessFalseParameter() throws Exception {
		String cardNumber = "993456789";
		String returnUrl = "http://redirect/after-confirmation";
		String fullUrl = returnUrl + "&paymentSuccess=false";

		when(ParamUtil.getString(mockActionRequest, "cardNumber", StringPool.BLANK)).thenReturn(cardNumber);
		when(ParamUtil.getString(mockActionRequest, ParamKeys.RETURN_URL, StringPool.BLANK)).thenReturn(returnUrl);
		when(mockPortal.getHttpServletResponse(mockActionResponse)).thenReturn(mockHttpServletResponse);

		redirectMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockHttpServletResponse, times(1)).sendRedirect(fullUrl);
	}

}
