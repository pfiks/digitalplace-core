package com.placecube.digitalplace.user.serviceoverride.internal.util;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = ServiceContextDetailsUtil.class)
public class ServiceContextDetailsUtil {

	private static final Log LOG = LogFactoryUtil.getLog(ServiceContextDetailsUtil.class);

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private Portal portal;

	public void populateMissingServiceContextDetails(long companyId, ServiceContext serviceContext) {

		if (serviceContext == null) {
			serviceContext = ServiceContextThreadLocal.getServiceContext();
		}

		configurePathMain(serviceContext);
		configurePortalURL(companyId, serviceContext);
	}

	private void configurePathMain(ServiceContext serviceContext) {
		if (Validator.isNull(serviceContext.getPathMain())) {
			serviceContext.setPathMain(portal.getPathMain());
		}
	}

	private void configurePortalURL(long companyId, ServiceContext serviceContext) {
		try {
			if (Validator.isNull(serviceContext.getPortalURL())) {
				Company company = companyLocalService.getCompanyById(companyId);
				String portalURL = company.getPortalURL(0l);
				serviceContext.setPortalURL(portalURL);
			}
		} catch (Exception e) {
			LOG.warn("Exception configuring portal URL in service context", e);
		}
	}
}
