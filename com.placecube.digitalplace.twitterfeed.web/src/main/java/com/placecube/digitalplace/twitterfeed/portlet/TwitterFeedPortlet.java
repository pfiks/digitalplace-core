package com.placecube.digitalplace.twitterfeed.portlet;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.twitterfeed.configuration.TwitterFeedConfigurationUtil;
import com.placecube.digitalplace.twitterfeed.configuration.TwitterFeedPortletInstanceConfiguration;
import com.placecube.digitalplace.twitterfeed.constants.PortletKeys;
import com.placecube.digitalplace.twitterfeed.constants.PortletRequestKeys;
import com.placecube.digitalplace.twitterfeed.constants.ViewKeys;

@Component(immediate = true, property = {
		"com.liferay.fragment.entry.processor.portlet.alias=dp-twitter-feed",
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.css-class-wrapper=portlet-twitter-feed",
		"com.liferay.portlet.display-category=category.social",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/META-INF/resources/",
		"javax.portlet.init-param.view-template=" + ViewKeys.VIEW_JSP,
		"javax.portlet.name=" + PortletKeys.TWITTER,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html",
		"javax.portlet.version=3.0"
}, service = Portlet.class)
public class TwitterFeedPortlet extends MVCPortlet {

	@Reference
	private TwitterFeedConfigurationUtil twitterFeedConfigurationUtil;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		Optional<TwitterFeedPortletInstanceConfiguration> configuration = twitterFeedConfigurationUtil.getValidConfiguration(themeDisplay);
		if (configuration.isPresent()) {
			renderRequest.setAttribute(PortletRequestKeys.CONFIGURATION, configuration.get());
		} else {
			renderRequest.setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, true);
		}

		super.render(renderRequest, renderResponse);
	}
}
