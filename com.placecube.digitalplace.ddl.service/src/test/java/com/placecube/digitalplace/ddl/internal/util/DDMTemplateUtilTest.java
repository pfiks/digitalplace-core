package com.placecube.digitalplace.ddl.internal.util;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;

public class DDMTemplateUtilTest extends PowerMockito {

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getTemplateById_WhenStructureHasNoTemplates_ThenReturnsEmptyOptional() {
		long templateId = 1;
		when(mockDDMStructure.getTemplates()).thenReturn(Collections.emptyList());

		Optional<DDMTemplate> result = DDMTemplateUtil.getTemplateById(mockDDMStructure, templateId);

		assertFalse(result.isPresent());
	}

	@Test
	public void getTemplateById_WhenTemplateIdNotGreaterThanZero_ThenReturnsEmptyOptional() {
		Optional<DDMTemplate> result = DDMTemplateUtil.getTemplateById(mockDDMStructure, 0l);

		assertFalse(result.isPresent());
	}

	@Test
	public void getTemplateById_WhenTemplateOfTypeDisplayIsFoundButTemplateIdDoesNotMatch_ThenReturnsEmptyOptional() {
		long templateId = 1;
		when(mockDDMStructure.getTemplates()).thenReturn(Collections.singletonList(mockDDMTemplate));
		when(mockDDMTemplate.getType()).thenReturn(DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY);
		when(mockDDMTemplate.getTemplateId()).thenReturn(989898l);

		Optional<DDMTemplate> result = DDMTemplateUtil.getTemplateById(mockDDMStructure, templateId);

		assertFalse(result.isPresent());
	}

	@Test
	public void getTemplateById_WhenTemplateOfTypeDisplayIsFoundWithTheMatchingId_ThenReturnsOptionalWithTheTemplate() {
		long templateId = 1;
		when(mockDDMStructure.getTemplates()).thenReturn(Collections.singletonList(mockDDMTemplate));
		when(mockDDMTemplate.getType()).thenReturn(DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY);
		when(mockDDMTemplate.getTemplateId()).thenReturn(templateId);

		Optional<DDMTemplate> result = DDMTemplateUtil.getTemplateById(mockDDMStructure, templateId);

		assertThat(result.get(), sameInstance(mockDDMTemplate));
	}

	@Test
	public void getTemplateById_WhenTemplateOfTypeDisplayIsNotFound_ThenReturnsEmptyOptional() {
		long templateId = 1;
		when(mockDDMStructure.getTemplates()).thenReturn(Collections.singletonList(mockDDMTemplate));
		when(mockDDMTemplate.getType()).thenReturn(DDMTemplateConstants.TEMPLATE_TYPE_FORM);
		when(mockDDMTemplate.getTemplateId()).thenReturn(templateId);

		Optional<DDMTemplate> result = DDMTemplateUtil.getTemplateById(mockDDMStructure, templateId);

		assertFalse(result.isPresent());
	}
}
