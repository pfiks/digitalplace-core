package com.placecube.digitalplace.user.account.configuration;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "user-authentication", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.user.account.configuration.UserAuthenticationCompanyConfiguration", localization = "content/Language", name = "password-policy")
public interface UserAuthenticationCompanyConfiguration {

	@Meta.AD(required = false, deflt = StringPool.BLANK, name = "password-policy-article-id", description = "password-policy-article-id-help")
	String passwordPolicyArticleId();

	@Meta.AD(required = false, deflt = "0", name = "password-policy-group-id", description = "password-policy-group-id-help")
	long passwordPolicyGroupId();

}
