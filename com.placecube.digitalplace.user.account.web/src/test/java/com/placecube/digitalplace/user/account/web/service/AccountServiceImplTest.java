package com.placecube.digitalplace.user.account.web.service;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.entry.rel.service.AssetEntryAssetCategoryRelLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserConstants;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.common.permission.PermissionUtil;
import com.placecube.digitalplace.address.constants.AddressType;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.address.service.AddressTypeConfigurationService;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.account.constants.PhoneType;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountDateOfBirthService;
import com.placecube.digitalplace.user.account.service.AccountEmailAddressService;
import com.placecube.digitalplace.user.account.service.AccountPhoneService;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PropsUtil.class, ServiceContextThreadLocal.class, SessionErrors.class })
public class AccountServiceImplTest extends PowerMockito {

	private static final Long COMPANY_ID = 11L;
	private static final int dobDay = 1;
	private static final int dobMonth = 1;
	private static final int dobYear = 1800;
	private static final String EMAIL = "emailValue";
	private static final String FIRSTNAME = "firstNameValue";
	private static final Long GROUP_ID = 12L;
	private static final String JOB_TITLE = "jobTitleValue";
	private static final String LASTNAME = "lastNameValue";
	private static final Locale LOCALE = Locale.CANADA;
	private static final String PASSWORD1 = "password1Value";
	private static final String PASSWORD2 = "password2Value";
	private static final String POSTCODE = "postcode";
	private static final String UPRN = "uprn";
	private static final long[] USER_GROUP_IDS = new long[0];
	private static final long USER_ID = 123L;

	@InjectMocks
	private AccountServiceImpl accountServiceImpl;

	private Map<String, Serializable> customFields;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private AccountDateOfBirthService mockAccountDateOfBirthService;

	@Mock
	private AccountEmailAddressService mockAccountEmailAddressService;

	@Mock
	private AccountPhoneService mockAccountPhoneService;

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private AddressLookupService mockAddressLookupService;

	@Mock
	private AddressTypeConfigurationService mockAddressTypeConfigurationService;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetEntryAssetCategoryRelLocalService mockAssetCategoryRelLocalService;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private Company mockCompany;

	@Mock
	private Contact mockContact;

	@Mock
	private UserAccountField mockCreateUserAccountField;

	@Mock
	private ExpandoBridge mockExpandoBridge;

	@Mock
	private PermissionUtil mockPermissionUtil;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserAccountFieldRegistry mockUserAccountFieldRegistry;

	@Mock
	private UserGroupRoleLocalService mockUserGroupRoleLocalService;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	private final List<UserGroupRole> userGroupRoles = new ArrayList<>();

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ServiceContextThreadLocal.class, SessionErrors.class);
	}

	@Test(expected = PortletException.class)
	public void checkAccountCreationEnabled_WhenCompanyIsStrangersIsFalse_ThenThrowsPortletException() throws PortletException {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockCompany.isStrangers()).thenReturn(false);

		accountServiceImpl.checkAccountCreationEnabled(mockPortletRequest);
	}

	@Test
	public void checkAccountCreationEnabled_WhenCompanyIsStrangersIsTrue_ThenNoExceptionIsThrown() {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockCompany.isStrangers()).thenReturn(true);

		try {
			accountServiceImpl.checkAccountCreationEnabled(mockPortletRequest);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenAddressContextDoesNotExist_ThenDoesNotCreateTheAddress(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenReturn(Optional.empty());

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAddressLookupService, never()).saveAddress(any(User.class), any(AddressContext.class), any(ServiceContext.class));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenAddressContextExists_ThenCreatesTheAddress(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenReturn(Optional.of(mockAddressContext));

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAddressLookupService, times(1)).saveAddress(mockUser, mockAddressContext, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenAddressContextExistsAndExceptionCreatingTheAddress_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenReturn(Optional.of(mockAddressContext));
		when(mockAddressLookupService.saveAddress(mockUser, mockAddressContext, mockServiceContext)).thenThrow(new PortalException());

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenAUserIsCreated_ThenAddCustomFieldsToContext(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		customFields = mockServiceContext.getExpandoBridgeAttributes();
		customFields.putAll(mockAccountContext.getExpandoFields());

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockServiceContext, times(1)).setExpandoBridgeAttributes(customFields);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenAUserIsCreated_ThenAssignCompanyAdminRole(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockPermissionUtil, times(1)).runAsCompanyAdmin(mockServiceContext.getCompanyId());
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenBusinessPhoneNumberNotSpecified_ThenDoesNotCreateTheBusinessPhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, never()).addPhone(any(User.class), anyString(), eq(PhoneType.BUSINESS), any(ServiceContext.class));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenBusinessPhoneNumberSpecified_ThenCreatesTheBusinessPhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		String businessPhone = "businessPhoneValue";
		when(mockAccountContext.getBusinessPhoneNumber()).thenReturn(businessPhone);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, businessPhone, PhoneType.BUSINESS, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenBusinessPhoneNumberSpecifiedAndExceptionCreatingThePhoneNuber_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		String businessPhone = "businessPhoneValue";
		when(mockAccountContext.getBusinessPhoneNumber()).thenReturn(businessPhone);
		doThrow(new PortalException()).when(mockAccountPhoneService).addPhone(mockUser, businessPhone, PhoneType.BUSINESS, mockServiceContext);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenCreateUserAccountFieldsArePresent_ThenExpandoValuesAreSetOnUser(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		String fieldName = "fieldName";
		String fieldValue = "fieldValue";
		long userId = 1L;
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn(fieldName);
		when(mockAccountContext.getExpandoValue(fieldName)).thenReturn(fieldValue);
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);
		when(mockUserAccountFieldRegistry.getUserAccountFields(COMPANY_ID)).thenReturn(createUserAccountFields);
		when(mockServiceContext.getUserId()).thenReturn(userId);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockExpandoBridge, times(1)).setAttribute(fieldName, fieldValue, false);
		verify(mockPermissionUtil, times(1)).runAsUser(userId);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenEmailVerifiedPlidIsNotPresent_ThenSetsDefaultLayoutPlidInServiceContext(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAccountEmailAddressService.getEmailAddressVerifiedLayoutPlid(mockAccountContext)).thenReturn(Optional.empty());

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockServiceContext, times(1)).setPlid(LayoutConstants.DEFAULT_PLID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenEmailVerifiedPlidIsPresent_ThenSetsEmailAddressVerifiedPlidInServiceContext(boolean fallbackToNationalLookup) throws Exception {
		final long plid = 4541L;
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAccountEmailAddressService.getEmailAddressVerifiedLayoutPlid(mockAccountContext)).thenReturn(Optional.of(plid));

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockServiceContext, times(1)).setPlid(plid);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenEmailVerifiedPlidRetrievalFails_ThenDoesNotConfigureAutoGeneratedEmailDetails(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAccountEmailAddressService.getEmailAddressVerifiedLayoutPlid(mockAccountContext)).thenThrow(new UserAccountException());

		try {
			accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);
			fail("Exception expected");
		} catch (UserAccountException ignored) {
		}

		verify(mockAccountContextService, never()).configureAutoGeneratedEmailDetails(any(AccountContext.class));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenEmailVerifiedPlidRetrievalFails_ThenDoesNotCreateAccount(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAccountEmailAddressService.getEmailAddressVerifiedLayoutPlid(mockAccountContext)).thenThrow(new UserAccountException());

		try {
			accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);
			fail("Exception expected");

		} catch (UserAccountException ignored) {
		}

		verify(mockUserLocalService, never()).addUserWithWorkflow(anyLong(), anyLong(), anyBoolean(), anyString(), anyString(), anyBoolean(), anyString(), anyString(), any(Locale.class), anyString(),
				anyString(), anyString(), anyLong(), anyLong(), anyBoolean(), anyInt(), anyInt(), anyInt(), anyString(), anyInt(), any(long[].class), any(long[].class), any(long[].class),
				any(long[].class), anyBoolean(), eq(mockServiceContext));

	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenEmailVerifiedPlidRetrievalFails_ThenDoesPerformPostCreateUpdateTasks(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAccountEmailAddressService.getEmailAddressVerifiedLayoutPlid(mockAccountContext)).thenThrow(new UserAccountException());

		try {
			accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

			fail("Exception expected");

		} catch (UserAccountException ignored) {
		}

		verify(mockAddressLookupService, never()).saveAddress(any(User.class), any(AddressContext.class), any(ServiceContext.class));
		verify(mockAccountPhoneService, never()).addPhone(any(User.class), anyString(), any(PhoneType.class), any(ServiceContext.class));
		verify(mockUserTwoFactorAuthenticationLocalService, never()).enableAndGetOrCreateTwoFactorAuthenticationForUser(any(User.class), anyBoolean());
		verify(mockExpandoBridge, never()).setAttribute(anyString(), anyString(), anyBoolean());
	}

	@Test(expected = UserAccountException.class)
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenEmailVerifiedPlidRetrievalFailsWithPortalException_ThenThrowsPortalException(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAccountEmailAddressService.getEmailAddressVerifiedLayoutPlid(mockAccountContext)).thenThrow(new UserAccountException());

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);
	}

	@Test(expected = UserAccountException.class)
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenEmailVerifiedPlidRetrievalFailsWithPortalException_ThenThrowsPortletException(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAccountEmailAddressService.getEmailAddressVerifiedLayoutPlid(mockAccountContext)).thenThrow(new UserAccountException());

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);
	}

	@Test(expected = UserAccountException.class)
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenExceptionCreatingTheAccount_ThenThrowsPortalException(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockUserLocalService.addUserWithWorkflow(0, COMPANY_ID, false, PASSWORD1, PASSWORD2, true, null, EMAIL, LOCALE, FIRSTNAME, null, LASTNAME, 0, 0, true, dobMonth - 1, dobDay, dobYear,
				JOB_TITLE, UserConstants.TYPE_REGULAR, new long[0], new long[0], new long[0], new long[0], true, mockServiceContext)).thenThrow(new PortalException());

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenExceptionGettingAddressContext_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenThrow(new PortalException());

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenHomePhoneNumberNotSpecified_ThenDoesNotCreateThePersonalPhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, never()).addPhone(any(User.class), anyString(), eq(PhoneType.PERSONAL), any(ServiceContext.class));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenHomePhoneNumberSpecified_ThenCreatesThePersonalPhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		String homePhone = "HomePhoneValue";
		when(mockAccountContext.getHomePhoneNumber()).thenReturn(homePhone);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, homePhone, PhoneType.PERSONAL, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenHomePhoneNumberSpecifiedAndExceptionCreatingThePhoneNumber_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		String homePhone = "HomePhoneValue";
		when(mockAccountContext.getHomePhoneNumber()).thenReturn(homePhone);
		doThrow(new PortalException()).when(mockAccountPhoneService).addPhone(mockUser, homePhone, PhoneType.PERSONAL, mockServiceContext);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenMobilePhoneNumberNotSpecified_ThenDoesNotCreateTheMobilePhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, never()).addPhone(any(User.class), anyString(), eq(PhoneType.MOBILE), any(ServiceContext.class));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenMobilePhoneNumberSpecified_ThenCreatesTheMobilePhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		String mobilePhone = "mobilePhoneValue";
		when(mockAccountContext.getMobilePhoneNumber()).thenReturn(mobilePhone);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, mobilePhone, PhoneType.MOBILE, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenMobilePhoneNumberSpecifiedAndExceptionCreatingThePhoneNumber_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		String mobilePhone = "mobilePhoneValue";
		when(mockAccountContext.getMobilePhoneNumber()).thenReturn(mobilePhone);
		doThrow(new PortalException()).when(mockAccountPhoneService).addPhone(mockUser, mobilePhone, PhoneType.MOBILE, mockServiceContext);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test
	public void createUserAccount_WhenNoError_ThenConfiguresAssetCategoriesInServiceContext() throws Exception {
		mockUserCreation(true, false);
		long[] categoryIds = { 123L, 456L };

		when(mockAccountContext.getCategoryIds()).thenReturn(categoryIds);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, false);

		verify(mockServiceContext, times(1)).setAssetCategoryIds(categoryIds);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,true", "false,false" })
	public void createUserAccount_WhenNoErrors_ThenCreatesUserAfterConfiguringServiceContextAndEmailInContext(boolean sendEmail, boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(sendEmail, fallbackToNationalLookup);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		InOrder inOrder = inOrder(mockUserLocalService, mockAccountContextService, mockServiceContext);
		inOrder.verify(mockServiceContext, times(1)).setPlid(0L);
		inOrder.verify(mockAccountContextService, times(1)).configureAutoGeneratedEmailDetails(mockAccountContext);
		inOrder.verify(mockUserLocalService, times(1)).addUserWithWorkflow(0, COMPANY_ID, false, PASSWORD1, PASSWORD2, true, null, EMAIL, LOCALE, FIRSTNAME, null, LASTNAME, 0, 0, true, dobMonth - 1,
				dobDay, dobYear, JOB_TITLE, UserConstants.TYPE_REGULAR, new long[0], new long[0], new long[0], new long[0], sendEmail, mockServiceContext);

	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenNoFixedAbodeTrueAndCurrentAddressIsNotNoCurrentAddress_ThenCreatesTheAddress(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenReturn(Optional.empty());

		when(GetterUtil.getString(mockAccountContext.getExpandoValue("address-info-status"), StringPool.BLANK)).thenReturn("no-fixed-abode");
		when(mockAccountContext.getFullAddress()).thenReturn("currentAddress");
		when(mockAddressLookupService.createAddressContext("", AccountConstants.NO_CURRENT_ADDRESS, "", "", AccountConstants.NO_CURRENT_ADDRESS, "", "")).thenReturn(mockAddressContext);
		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAddressLookupService, times(1)).saveAddress(mockUser, mockAddressContext, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void createUserAccount_WhenSettingCreateUserAccountFieldsCausesError_ThenExceptionIsThrown(boolean fallbackToNationalLookup) throws Exception {
		mockUserCreation(true, fallbackToNationalLookup);
		String fieldName = "fieldName";
		String fieldValue = "fieldValue";
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn(fieldName);
		when(mockAccountContext.getExpandoValue(fieldName)).thenReturn(fieldValue);
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);
		when(mockUserAccountFieldRegistry.getUserAccountFields(COMPANY_ID)).thenReturn(createUserAccountFields);
		when(mockUser.getExpandoBridge()).thenReturn(null);

		accountServiceImpl.createUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");

	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenAddressContextDoesNotExist_ThenDoesNotCreateTheAddress(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenReturn(Optional.empty());

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAddressLookupService, never()).saveAddress(any(User.class), any(AddressContext.class), any(ServiceContext.class));
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenAddressContextExists_ThenUpdatesTheContextWithADefaultAddressType(boolean fallbackToNationalLookup) throws Exception {
		AddressType defaultAddressType = AddressType.PRIMARY;
		mockUserUpdate(fallbackToNationalLookup);
		when(mockAddressTypeConfigurationService.getDefaultAddressType(COMPANY_ID)).thenReturn(defaultAddressType);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenReturn(Optional.of(mockAddressContext));

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAddressContext, times(1)).setAddressType(defaultAddressType);

	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenAddressContextExistsAndExceptionCreatingTheAddress_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenReturn(Optional.of(mockAddressContext));
		when(mockAddressLookupService.saveAddress(mockUser, mockAddressContext, mockServiceContext)).thenThrow(new PortalException());

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test
	public void updateUserAccount_WhenAssetEntryIsNull_ThenDoNotDeleteAnyAssociatedCategories() throws Exception {
		mockUserUpdate(false);
		long[] userCategoryIds = null;

		when(mockAccountContext.isUpdateUserCategories()).thenReturn(true);
		when(mockAssetEntryLocalService.fetchEntry(User.class.getName(), USER_ID)).thenReturn(null);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, false);

		verify(mockAssetCategoryRelLocalService, times(0)).deleteAssetEntryAssetCategoryRel(123L, 321L);
		verify(mockServiceContext, times(1)).setAssetCategoryIds(userCategoryIds);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenBusinessPhoneNumberNotSpecified_ThenDoesNotCreateTheBusinessPhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, null, PhoneType.BUSINESS, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenBusinessPhoneNumberSpecified_ThenCreatesTheBusinessPhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		String businessPhone = "businessPhoneValue";
		when(mockAccountContext.getBusinessPhoneNumber()).thenReturn(businessPhone);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, businessPhone, PhoneType.BUSINESS, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenBusinessPhoneNumberSpecifiedAndExceptionCreatingThePhoneNuber_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		String businessPhone = "businessPhoneValue";
		when(mockAccountContext.getBusinessPhoneNumber()).thenReturn(businessPhone);
		doThrow(new PortalException()).when(mockAccountPhoneService).addPhone(mockUser, businessPhone, PhoneType.BUSINESS, mockServiceContext);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenEmailAddressNotSpecified_ThenGeneratesEmailAddress(boolean fallbackToNationalLookup) throws Exception {
		boolean isEmailBlank = true;
		mockUserUpdate(fallbackToNationalLookup, isEmailBlank);
		when(mockAccountContext.getEmailAddress()).thenReturn(StringPool.BLANK);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountContextService, times(1)).configureAutoGeneratedEmailDetails(mockAccountContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenExceptionGettingAddressContext_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenThrow(new PortalException());

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test(expected = UserAccountException.class)
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenExceptionUpdatingTheAccount_ThenThrowsPortalException(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		when(mockUserLocalService.updateUser(USER_ID, "oldPassword", PASSWORD1, PASSWORD2, mockUser.getPasswordReset(), mockUser.getReminderQueryQuestion(), mockUser.getReminderQueryAnswer(),
				mockUser.getScreenName(), EMAIL, false, null, mockUser.getLanguageId(), mockUser.getTimeZoneId(), mockUser.getGreeting(), mockUser.getComments(), FIRSTNAME, mockUser.getMiddleName(),
				LASTNAME, mockContact.getPrefixListTypeId(), mockContact.getSuffixListTypeId(), mockUser.getMale(), dobMonth - 1, dobDay, dobYear, mockContact.getSmsSn(), mockContact.getFacebookSn(),
				mockContact.getJabberSn(), mockContact.getSkypeSn(), mockContact.getTwitterSn(), JOB_TITLE, mockUser.getGroupIds(), mockUser.getOrganizationIds(), mockUser.getRoleIds(),
				userGroupRoles, mockUser.getUserGroupIds(), mockServiceContext)).thenThrow(new PortalException());

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenHomePhoneNumberNotSpecified_ThenDoesNotCreateThePersonalPhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, null, PhoneType.PERSONAL, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenHomePhoneNumberSpecified_ThenCreatesThePersonalPhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		String homePhone = "HomePhoneValue";
		when(mockAccountContext.getHomePhoneNumber()).thenReturn(homePhone);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, homePhone, PhoneType.PERSONAL, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenHomePhoneNumberSpecifiedAndExceptionCreatingThePhoneNumber_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		String homePhone = "HomePhoneValue";
		when(mockAccountContext.getHomePhoneNumber()).thenReturn(homePhone);
		doThrow(new PortalException()).when(mockAccountPhoneService).addPhone(mockUser, homePhone, PhoneType.PERSONAL, mockServiceContext);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenMobilePhoneNumberNotSpecified_ThenDoesNotCreateTheMobilePhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, null, PhoneType.MOBILE, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenMobilePhoneNumberSpecified_ThenCreatesTheMobilePhoneNumber(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		String mobilePhone = "mobilePhoneValue";
		when(mockAccountContext.getMobilePhoneNumber()).thenReturn(mobilePhone);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockAccountPhoneService, times(1)).addPhone(mockUser, mobilePhone, PhoneType.MOBILE, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenMobilePhoneNumberSpecifiedAndExceptionCreatingThePhoneNuber_ThenAddsSessionError(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		String mobilePhone = "mobilePhoneValue";
		when(mockAccountContext.getMobilePhoneNumber()).thenReturn(mobilePhone);
		doThrow(new PortalException()).when(mockAccountPhoneService).addPhone(mockUser, mobilePhone, PhoneType.MOBILE, mockServiceContext);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");
	}

	@Test
	public void updateUserAccount_WhenNoError_ThenConfiguresAssetCategoriesInServiceContext() throws Exception {
		mockUserUpdate(false);
		long[] userCategoryIds = { 123L, 456L };
		List<AssetCategory> assetCategories = new ArrayList<>();
		assetCategories.add(mockAssetCategory);

		when(mockAccountContext.isUpdateUserCategories()).thenReturn(true);
		when(mockAccountContext.getCategoryIds()).thenReturn(userCategoryIds);
		when(mockAssetEntryLocalService.fetchEntry(User.class.getName(), USER_ID)).thenReturn(mockAssetEntry);
		when(mockAssetEntry.getEntryId()).thenReturn(123L);
		when(mockAssetCategory.getCategoryId()).thenReturn(321L);
		when(mockAssetEntry.getCategories()).thenReturn(assetCategories);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, false);

		verify(mockAssetCategoryRelLocalService, times(1)).deleteAssetEntryAssetCategoryRel(123L, 321L);
		verify(mockServiceContext, times(1)).setAssetCategoryIds(userCategoryIds);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenSettingCreateUserAccountFieldsCausesError_ThenExceptionIsThrown(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		String fieldName = "fieldName";
		String fieldValue = "fieldValue";
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn(fieldName);
		when(mockAccountContext.getExpandoValue(fieldName)).thenReturn(fieldValue);
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);
		when(mockUserAccountFieldRegistry.getUserAccountFields(COMPANY_ID)).thenReturn(createUserAccountFields);
		when(mockUser.getExpandoBridge()).thenReturn(null);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, "accountCreatedWithErrors");

	}

	@Test
	@Parameters({ "true", "false" })
	public void updateUserAccount_WhenUpdateUserAccountFieldsArePresent_ThenExpandoValuesAreSetOnUser(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup);
		String fieldName = "fieldName";
		String fieldValue = "fieldValue";
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn(fieldName);
		when(mockAccountContext.getExpandoValue(fieldName)).thenReturn(fieldValue);
		List<UserAccountField> updateUserAccountFields = new ArrayList<>();
		updateUserAccountFields.add(mockCreateUserAccountField);
		when(mockUserAccountFieldRegistry.getUserAccountFields(COMPANY_ID)).thenReturn(updateUserAccountFields);

		accountServiceImpl.updateUserAccount(mockPortletRequest, mockAccountContext, fallbackToNationalLookup);

		verify(mockExpandoBridge, times(1)).setAttribute(fieldName, fieldValue, false);

	}

	private void mockBasic(boolean fallbackToNationalLookup) throws Exception {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
		when(mockAccountContext.getExpandoFields()).thenReturn(Collections.emptyMap());
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], fallbackToNationalLookup)).thenReturn(Optional.empty());
		when(mockAccountContext.getPassword1()).thenReturn(PASSWORD1);
		when(mockAccountContext.getPassword2()).thenReturn(PASSWORD2);
		when(mockAccountContext.getEmailAddress()).thenReturn(EMAIL);
		when(mockAccountContext.getFirstName()).thenReturn(FIRSTNAME);
		when(mockAccountContext.getLastName()).thenReturn(LASTNAME);
		when(mockAccountContext.getJobTitle()).thenReturn(JOB_TITLE);
		when(mockAccountContext.getPostcode()).thenReturn(POSTCODE);
		when(mockAccountContext.getUprn()).thenReturn(UPRN);
		when(mockAccountContext.getUserGroupIds()).thenReturn(USER_GROUP_IDS);
		when(mockAccountDateOfBirthService.getDateOfBirthFromContext(mockAccountContext)).thenReturn(LocalDate.of(dobYear, dobMonth, dobDay));
	}

	private void mockUserCreation(boolean sendMail, boolean fallbackToNationalLookup) throws Exception {
		mockBasic(fallbackToNationalLookup);
		when(mockAccountContext.sendEmail()).thenReturn(sendMail);
		when(mockUserLocalService.addUserWithWorkflow(0, COMPANY_ID, false, PASSWORD1, PASSWORD2, true, null, EMAIL, LOCALE, FIRSTNAME, null, LASTNAME, 0, 0, true, dobMonth - 1, dobDay, dobYear,
				JOB_TITLE, UserConstants.TYPE_REGULAR, new long[0], new long[0], new long[0], mockAccountContext.getUserGroupIds(), sendMail, mockServiceContext)).thenReturn(mockUser);

		when(mockUser.getUserId()).thenReturn(USER_ID);
	}

	private void mockUserUpdate(boolean fallbackToNationalLookup) throws Exception {
		mockUserUpdate(fallbackToNationalLookup, false);
	}

	private void mockUserUpdate(boolean fallbackToNationalLookup, boolean isEmailBlank) throws Exception {
		mockBasic(fallbackToNationalLookup);

		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getPassword()).thenReturn("oldPassword");
		when(mockUser.getContact()).thenReturn(mockContact);

		String email = isEmailBlank ? StringPool.BLANK : EMAIL;

		when(mockUserLocalService.updateUser(USER_ID, "oldPassword", PASSWORD1, PASSWORD2, mockUser.getPasswordReset(), mockUser.getReminderQueryQuestion(), mockUser.getReminderQueryAnswer(),
				mockUser.getScreenName(), email, false, null, mockUser.getLanguageId(), mockUser.getTimeZoneId(), mockUser.getGreeting(), mockUser.getComments(), FIRSTNAME, mockUser.getMiddleName(),
				LASTNAME, mockContact.getPrefixListTypeId(), mockContact.getSuffixListTypeId(), mockUser.getMale(), dobMonth - 1, dobDay, dobYear, mockContact.getSmsSn(), mockContact.getFacebookSn(),
				mockContact.getJabberSn(), mockContact.getSkypeSn(), mockContact.getTwitterSn(), JOB_TITLE, mockUser.getGroupIds(), mockUser.getOrganizationIds(), mockUser.getRoleIds(),
				userGroupRoles, mockUser.getUserGroupIds(), mockServiceContext)).thenReturn(mockUser);

		when(mockAccountContext.isUpdate()).thenReturn(true);
		when(mockUserLocalService.getUserById(mockAccountContext.getUserId())).thenReturn(mockUser);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(USER_ID)).thenReturn(userGroupRoles);
	}
}
