package com.placecube.digitalplace.admin.emailverify.useradmins;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.admin.emailverify.service.EmailVerifyTicketService;
import com.placecube.digitalplace.admin.emailverify.service.EmailVerifyUrlService;

public class EmailVerifyUserActionContributorTest extends PowerMockito {

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletResponse mockPortletResponse;

	@Mock
	private User mockSelectedUser;

	@Mock
	private EmailVerifyTicketService mockEmailVerifyTicketService;

	@Mock
	private EmailVerifyUrlService mockEmailVerifyUrlService;

	@Mock
	private User mockUser;

	@InjectMocks
	private EmailVerifyUserActionContributor emailVerifyUserActionContributor;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getURL_WhenNoError_ThenReturnsTheVerifyEmailURL() {
		String expected = "expectedValue";
		when(mockEmailVerifyUrlService.getVerifyEmailURL(mockPortletResponse, mockSelectedUser)).thenReturn(expected);

		String result = emailVerifyUserActionContributor.getURL(mockPortletRequest, mockPortletResponse, mockUser, mockSelectedUser);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void isShow_WhenUserHasPendingTickets_ThenReturnsTrue() {
		long companyId = 1;
		long userId = 2;
		when(mockSelectedUser.getCompanyId()).thenReturn(companyId);
		when(mockSelectedUser.getUserId()).thenReturn(userId);
		when(mockEmailVerifyTicketService.doesUserHavePendingTickets(companyId, userId)).thenReturn(true);

		boolean result = emailVerifyUserActionContributor.isShow(mockPortletRequest, mockUser, mockSelectedUser);

		assertTrue(result);
	}

	@Test
	public void isShow_WhenUserDoesNotHavePendingTickets_ThenReturnsFalse() {
		long companyId = 1;
		long userId = 2;
		when(mockSelectedUser.getCompanyId()).thenReturn(companyId);
		when(mockSelectedUser.getUserId()).thenReturn(userId);
		when(mockEmailVerifyTicketService.doesUserHavePendingTickets(companyId, userId)).thenReturn(false);

		boolean result = emailVerifyUserActionContributor.isShow(mockPortletRequest, mockUser, mockSelectedUser);

		assertFalse(result);
	}

	@Test
	public void isShowConfirmationMessage_WhenNoError_ThenReturnsTrue() {
		boolean result = emailVerifyUserActionContributor.isShowConfirmationMessage(mockSelectedUser);

		assertTrue(result);
	}

}
