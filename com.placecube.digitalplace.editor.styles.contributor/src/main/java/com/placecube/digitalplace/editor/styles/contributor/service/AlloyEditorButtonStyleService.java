package com.placecube.digitalplace.editor.styles.contributor.service;

import java.util.Objects;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.editor.styles.contributor.constants.ButtonStyle;

@Component(immediate = true, service = AlloyEditorButtonStyleService.class)
public class AlloyEditorButtonStyleService {

	private static final Log LOG = LogFactoryUtil.getLog(AlloyEditorButtonStyleService.class);

	@Reference
	private JSONFactory jsonFactory;

	public JSONObject createButtonStyle(ButtonStyle buttonStyle) {
		JSONObject attributesJSONObject = jsonFactory.createJSONObject();
		attributesJSONObject.put("class", buttonStyle.getValue());

		JSONObject styleJSONObject = jsonFactory.createJSONObject();
		styleJSONObject.put("attributes", attributesJSONObject);
		styleJSONObject.put("element", buttonStyle.getElement());

		JSONObject styleConfiguration = jsonFactory.createJSONObject();
		styleConfiguration.put("name", buttonStyle.getName());
		styleConfiguration.put("style", styleJSONObject);
		return styleConfiguration;
	}

	public Optional<JSONArray> getTextButtonStyles(JSONObject jsonObject) {
		try {
			JSONObject toolbarsJSONObject = jsonObject.getJSONObject("toolbars");

			JSONObject stylesToolbar = toolbarsJSONObject.getJSONObject("styles");

			JSONArray selectionsJSONArray = stylesToolbar.getJSONArray("selections");

			for (int i = 0; i < selectionsJSONArray.length(); i++) {
				JSONObject selection = selectionsJSONArray.getJSONObject(i);

				if (Objects.equals(selection.get("name"), "text")) {
					JSONArray buttons = selection.getJSONArray("buttons");
					JSONObject button = buttons.getJSONObject(0);
					JSONObject cfg = button.getJSONObject("cfg");
					JSONArray styles = cfg.getJSONArray("styles");
					return Optional.of(styles);
				}
			}
		} catch (Exception e) {
			LOG.debug(e); 
			LOG.error("Error during get Text Button Styles. " + e.getMessage());
		}

		return Optional.empty();
	}

}
