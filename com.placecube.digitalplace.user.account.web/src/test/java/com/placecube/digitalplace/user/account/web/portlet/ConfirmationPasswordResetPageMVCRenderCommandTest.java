package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class ConfirmationPasswordResetPageMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private ConfirmationPasswordResetPageMVCRenderCommand confirmationPasswordResetPageMVCRenderCommand;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Test
	public void render_WhenNoError_ThenReturnsPasswordResetJSP() {

		String result = confirmationPasswordResetPageMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/confirmation-password-reset-page.jsp"));
	}

}
