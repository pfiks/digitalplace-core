package com.placecube.digitalplace.appointment.office365.model;

import com.liferay.portal.kernel.json.JSONObject;

public class AppointmentConfiguredService {

	private final String calendarId;
	private final int numberOfDatesToReturn;
	private final int onlyDatesWithLessThanXappoinments;
	private final String serviceId;
	private final String userId;

	public AppointmentConfiguredService(JSONObject json) {
		serviceId = json.getString("serviceId");
		calendarId = json.getString("calendarId");
		userId = json.getString("userId");
		numberOfDatesToReturn = json.getInt("numberOfDatesToReturn", 10);
		onlyDatesWithLessThanXappoinments = json.getInt("onlyDatesWithLessThanXappoinments", 10);
	}

	public String getCalendarId() {
		return calendarId;
	}

	public int getNumberOfDatesToReturn() {
		return numberOfDatesToReturn;
	}

	public int getOnlyDatesWithLessThanXappoinments() {
		return onlyDatesWithLessThanXappoinments;
	}

	public String getServiceId() {
		return serviceId;
	}

	public String getUserId() {
		return userId;
	}

}
