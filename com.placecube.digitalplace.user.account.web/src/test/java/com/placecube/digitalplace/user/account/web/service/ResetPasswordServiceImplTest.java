package com.placecube.digitalplace.user.account.web.service;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Locale;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.journal.service.JournalArticleRetrievalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, ServiceContextFactory.class })
public class ResetPasswordServiceImplTest extends PowerMockito {

	private static final Long COMPANY_ID = 1L;

	private static final Long GROUP_ID = 2L;

	private static final Locale LOCALE_UK = Locale.UK;

	private static final String RESET_PASSWORD_ARTICLE_ID = "RESET_PASSWORD_ARTICLE_ID";

	private static final String USER_EMAIL_ADDRESS_TO_SEND = "USER_EMAIL_ADDRESS_TO_SEND";

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private JournalArticleRetrievalService mockJournalArticleRetrievalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private ResetPasswordServiceImpl resetPasswordServiceImpl;

	@Before
	public void activateSetup() throws PortalException {
		mockStatic(PropsUtil.class, ParamUtil.class, ServiceContextFactory.class);
		when(mockPortal.getCompanyId(mockPortletRequest)).thenReturn(COMPANY_ID);
		when(mockPortal.getScopeGroupId(mockPortletRequest)).thenReturn(GROUP_ID);
		when(mockPortal.getLocale(mockPortletRequest)).thenReturn(LOCALE_UK);
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(ParamUtil.getString(mockPortletRequest, RequestKeys.USER_EMAIL_ADDRESS_TO_SEND_RESET_PASSWORD)).thenReturn(USER_EMAIL_ADDRESS_TO_SEND);
	}

	@Test(expected = PortletException.class)
	public void resetPassword_WhenErrorDuringGetInstance_ThenThrowsPortletException() throws PortletException, PortalException {
		when(ServiceContextFactory.getInstance(Mockito.any(PortletRequest.class))).thenThrow(new PortalException());

		resetPasswordServiceImpl.resetPassword(mockPortletRequest);
	}

	@Test(expected = PortletException.class)
	public void resetPassword_WhenErrorGettingAccountCompanyConfiguration_ThenThrowsPortletException() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockPortletRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenThrow(new UserAccountException());
		resetPasswordServiceImpl.resetPassword(mockPortletRequest);
	}

	@Test
	public void resetPassword_WhenResetPasswordArticleNotExist_ThenSendPasswordWithDefaultEmail() throws PortletException, PortalException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockPortletRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockCreateAccountConfiguration.resetPasswordArticleId()).thenReturn(RESET_PASSWORD_ARTICLE_ID);
		when(mockJournalArticleLocalService.fetchLatestArticle(GROUP_ID, RESET_PASSWORD_ARTICLE_ID, WorkflowConstants.STATUS_APPROVED)).thenReturn(null);
		when(mockJournalArticleRetrievalService.getFieldValue(null, "Body", LOCALE_UK)).thenReturn(Optional.empty());
		when(mockJournalArticleRetrievalService.getFieldValue(null, "Subject", LOCALE_UK)).thenReturn(Optional.empty());
		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);

		resetPasswordServiceImpl.resetPassword(mockPortletRequest);

		InOrder inOrder = inOrder(mockUserLocalService, mockServiceContext);
		inOrder.verify(mockServiceContext, times(1)).setPlid(0);
		inOrder.verify(mockUserLocalService, times(1)).sendPassword(COMPANY_ID, USER_EMAIL_ADDRESS_TO_SEND, null, null, null, null, mockServiceContext);
	}

	@Test
	public void resetPassword_WhenResetPasswordArticleExist_ThenSendPasswordWithProvidedJournalArticleEail() throws PortalException, PortletException, UserAccountException {
		String body = "Body_mockJournalArticle";
		String subject = "Subject_mockJournalArticle";

		when(mockAccountContextService.getOrCreateContext(mockPortletRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.resetPasswordArticleId()).thenReturn(RESET_PASSWORD_ARTICLE_ID);
		when(mockJournalArticleLocalService.fetchLatestArticle(GROUP_ID, RESET_PASSWORD_ARTICLE_ID, WorkflowConstants.STATUS_APPROVED)).thenReturn(mockJournalArticle);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, "Body", LOCALE_UK)).thenReturn(Optional.of(body));
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, "Subject", LOCALE_UK)).thenReturn(Optional.of(subject));
		when(ServiceContextFactory.getInstance(mockPortletRequest)).thenReturn(mockServiceContext);

		resetPasswordServiceImpl.resetPassword(mockPortletRequest);

		InOrder inOrder = inOrder(mockUserLocalService, mockServiceContext, mockAccountContextService);
		inOrder.verify(mockServiceContext, times(1)).setPlid(0);
		inOrder.verify(mockUserLocalService, times(1)).sendPassword(COMPANY_ID, USER_EMAIL_ADDRESS_TO_SEND, null, null, subject, body, mockServiceContext);
		inOrder.verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockPortletRequest);
	}
}
