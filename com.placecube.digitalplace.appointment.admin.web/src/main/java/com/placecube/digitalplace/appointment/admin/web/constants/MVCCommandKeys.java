package com.placecube.digitalplace.appointment.admin.web.constants;

public final class MVCCommandKeys {

	public static final String CANCEL_BOOKED_APPOINTMENT = "/appointmentManager/cancel-booked-appointment";

	public static final String DELETE_SLOT = "/appointmentManager/delete-slot";

	public static final String UPDATE_SLOT = "/appointmentManager/update-slot";

	public static final String VIEW_ALL_SLOTS = "/appointmentManager/view-all-slots";

	public static final String VIEW_BOOKED_APPOINTMENTS = "/appointmentManager/view-booked-appointments";

	private MVCCommandKeys() {
	}
}