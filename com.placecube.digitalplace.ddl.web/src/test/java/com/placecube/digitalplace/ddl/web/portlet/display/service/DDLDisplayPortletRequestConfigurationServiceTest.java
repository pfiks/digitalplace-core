package com.placecube.digitalplace.ddl.web.portlet.display.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.RenderRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class DDLDisplayPortletRequestConfigurationServiceTest {

	@Mock
	private DDLDisplayPortletInstanceConfiguration mockConfiguration;

	@Mock
	private DDLEditorService mockDDLEditorService;

	@Mock
	private DDLPermissionService mockDDLPermissionService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordService mockDDLRecordService;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private DDLDisplayPortletRequestConfigurationService ddlDisplayPortletRequestConfigurationService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void setRenderRequestAttributesUsingConfiguration_WhenNoException_ThenSetsAttributes() throws PortalException {
		long recordSetId = 3;
		boolean hasPermission = true;
		boolean showLoginOption = true;

		when(mockDDLEditorService.getServiceContextForScopeGroup(mockThemeDisplay)).thenReturn(mockServiceContext);
		when(mockDDLEditorService.getOrCreateDDLRecordSet(mockDDMStructure, mockServiceContext)).thenReturn(mockDDLRecordSet);
		when(mockDDLPermissionService.hasAddPermission(mockThemeDisplay, mockDDLRecordSet)).thenReturn(hasPermission);
		when(mockDDLRecordSet.getRecordSetId()).thenReturn(recordSetId);
		when(mockConfiguration.showLoginOption()).thenReturn(showLoginOption);

		ddlDisplayPortletRequestConfigurationService.setRenderRequestAttributesUsingConfiguration(mockRenderRequest, mockThemeDisplay, mockConfiguration, mockDDMStructure);

		verify(mockRenderRequest, times(1)).setAttribute("hasAddPermission", hasPermission);
		verify(mockRenderRequest, times(1)).setAttribute("ddmStructure", mockDDMStructure);
		verify(mockRenderRequest, times(1)).setAttribute("ddlRecordSetId", recordSetId);
		verify(mockRenderRequest, times(1)).setAttribute("showLoginOption", showLoginOption);
	}

	@Test(expected = PortalException.class)
	public void setRenderRequestAttributesUsingConfiguration_WhenExceptionGettingRecordSet_ThenThrowsPortalException() throws PortalException {
		when(mockDDLEditorService.getServiceContextForScopeGroup(mockThemeDisplay)).thenReturn(mockServiceContext);
		when(mockDDLEditorService.getOrCreateDDLRecordSet(mockDDMStructure, mockServiceContext)).thenThrow(new PortalException());

		ddlDisplayPortletRequestConfigurationService.setRenderRequestAttributesUsingConfiguration(mockRenderRequest, mockThemeDisplay, mockConfiguration, mockDDMStructure);
	}

	@Test
	public void setRenderRequestAttributesUsingDDLRecord_WhenNoException_ThenSetsRenderRequestAttributes() throws PortalException {
		long ddlRecordId = 1;
		long recordSetId = 2;
		boolean hasUpdatePermission = true;

		when(mockDDLRecordService.getRecord(ddlRecordId)).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDDLPermissionService.hasAddPermission(mockThemeDisplay, mockDDLRecordSet)).thenReturn(hasUpdatePermission);
		when(mockDDLRecordSet.getRecordSetId()).thenReturn(recordSetId);

		ddlDisplayPortletRequestConfigurationService.setRenderRequestAttributesUsingDDLRecord(mockRenderRequest, mockThemeDisplay, ddlRecordId);

		verify(mockRenderRequest, times(1)).setAttribute("hasAddPermission", hasUpdatePermission);
		verify(mockRenderRequest, times(1)).setAttribute("ddmStructure", mockDDMStructure);
		verify(mockRenderRequest, times(1)).setAttribute("ddlRecordSetId", recordSetId);
		verify(mockRenderRequest, times(1)).setAttribute("showLoginOption", true);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "getRecord", "getRecordSet", "getDDMStructure" })
	public void setRenderRequestAttributesUsingDDLRecord_WhenException_ThenThrowsPortalException(String exception) throws PortalException {
		long ddlRecordId = 1;

		switch (exception) {
		case "getRecord":
			when(mockDDLRecordService.getRecord(ddlRecordId)).thenThrow(new PortalException());
		case "getRecordSet":
			when(mockDDLRecordService.getRecord(ddlRecordId)).thenReturn(mockDDLRecord);
			when(mockDDLRecord.getRecordSet()).thenThrow(new PortalException());
		case "getDDMStructure":
			when(mockDDLRecordService.getRecord(ddlRecordId)).thenReturn(mockDDLRecord);
			when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
			when(mockDDLRecordSet.getDDMStructure()).thenThrow(new PortalException());
		}

		ddlDisplayPortletRequestConfigurationService.setRenderRequestAttributesUsingDDLRecord(mockRenderRequest, mockThemeDisplay, ddlRecordId);
	}

}