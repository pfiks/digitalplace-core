/**
 * ScpServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.simple;

public class ScpServiceLocator extends org.apache.axis.client.Service implements com.capita_software_services.www.scp.simple.ScpService {

    public ScpServiceLocator() {
    }


    public ScpServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ScpServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for scpSoap11
    private java.lang.String scpSoap11_address = "https://sbsctest.e-paycapita.com:443/scp/scpws";
    //private java.lang.String scpSoap11_address = "https://localhost:9001/scp/scpws";

    public java.lang.String getscpSoap11Address() {
        return scpSoap11_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String scpSoap11WSDDServiceName = "scpSoap11";

    public java.lang.String getscpSoap11WSDDServiceName() {
        return scpSoap11WSDDServiceName;
    }

    public void setscpSoap11WSDDServiceName(java.lang.String name) {
        scpSoap11WSDDServiceName = name;
    }

    public com.capita_software_services.www.scp.simple.Scp getscpSoap11() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(scpSoap11_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getscpSoap11(endpoint);
    }

    public com.capita_software_services.www.scp.simple.Scp getscpSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.capita_software_services.www.scp.simple.ScpSoap11Stub _stub = new com.capita_software_services.www.scp.simple.ScpSoap11Stub(portAddress, this);
            _stub.setPortName(getscpSoap11WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setscpSoap11EndpointAddress(java.lang.String address) {
        scpSoap11_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.capita_software_services.www.scp.simple.Scp.class.isAssignableFrom(serviceEndpointInterface)) {
                com.capita_software_services.www.scp.simple.ScpSoap11Stub _stub = new com.capita_software_services.www.scp.simple.ScpSoap11Stub(new java.net.URL(scpSoap11_address), this);
                _stub.setPortName(getscpSoap11WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("scpSoap11".equals(inputPortName)) {
            return getscpSoap11();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "scpService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "scpSoap11"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {

if ("scpSoap11".equals(portName)) {
            setscpSoap11EndpointAddress(address);
        }
        else
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
