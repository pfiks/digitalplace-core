/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.appointment.model.AppointmentSlot;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for AppointmentSlot. This utility wraps
 * <code>com.placecube.digitalplace.appointment.service.impl.AppointmentSlotLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentSlotLocalService
 * @generated
 */
public class AppointmentSlotLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.appointment.service.impl.AppointmentSlotLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the appointment slot to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentSlotLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentSlot the appointment slot
	 * @return the appointment slot that was added
	 */
	public static AppointmentSlot addAppointmentSlot(
		AppointmentSlot appointmentSlot) {

		return getService().addAppointmentSlot(appointmentSlot);
	}

	public static int countAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(
		long companyId, String slotType, String serviceId) {

		return getService().
			countAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(
				companyId, slotType, serviceId);
	}

	/**
	 * Creates a new appointment slot with the primary key. Does not add the appointment slot to the database.
	 *
	 * @param appointmentSlotId the primary key for the new appointment slot
	 * @return the new appointment slot
	 */
	public static AppointmentSlot createAppointmentSlot(
		long appointmentSlotId) {

		return getService().createAppointmentSlot(appointmentSlotId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the appointment slot from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentSlotLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentSlot the appointment slot
	 * @return the appointment slot that was removed
	 */
	public static AppointmentSlot deleteAppointmentSlot(
		AppointmentSlot appointmentSlot) {

		return getService().deleteAppointmentSlot(appointmentSlot);
	}

	/**
	 * Deletes the appointment slot with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentSlotLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot that was removed
	 * @throws PortalException if a appointment slot with the primary key could not be found
	 */
	public static AppointmentSlot deleteAppointmentSlot(long appointmentSlotId)
		throws PortalException {

		return getService().deleteAppointmentSlot(appointmentSlotId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static AppointmentSlot fetchAppointmentSlot(long appointmentSlotId) {
		return getService().fetchAppointmentSlot(appointmentSlotId);
	}

	/**
	 * Returns the appointment slot with the matching UUID and company.
	 *
	 * @param uuid the appointment slot's UUID
	 * @param companyId the primary key of the company
	 * @return the matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public static AppointmentSlot fetchAppointmentSlotByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().fetchAppointmentSlotByUuidAndCompanyId(
			uuid, companyId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static List
		<com.placecube.digitalplace.appointment.booking.model.
			AppointmentBookingSlot> getAppointmentBookingSlots(
				AppointmentSlot appointmentSlot, java.util.Date fromDate,
				java.util.Date toDate) {

		return getService().getAppointmentBookingSlots(
			appointmentSlot, fromDate, toDate);
	}

	/**
	 * Returns the appointment slot with the primary key.
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot
	 * @throws PortalException if a appointment slot with the primary key could not be found
	 */
	public static AppointmentSlot getAppointmentSlot(long appointmentSlotId)
		throws PortalException {

		return getService().getAppointmentSlot(appointmentSlotId);
	}

	/**
	 * Returns the appointment slot with the matching UUID and company.
	 *
	 * @param uuid the appointment slot's UUID
	 * @param companyId the primary key of the company
	 * @return the matching appointment slot
	 * @throws PortalException if a matching appointment slot could not be found
	 */
	public static AppointmentSlot getAppointmentSlotByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException {

		return getService().getAppointmentSlotByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of all the appointment slots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of appointment slots
	 */
	public static List<AppointmentSlot> getAppointmentSlots(
		int start, int end) {

		return getService().getAppointmentSlots(start, end);
	}

	public static List<AppointmentSlot>
		getAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(
			long companyId, String slotType, String serviceId, int start,
			int end) {

		return getService().
			getAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(
				companyId, slotType, serviceId, start, end);
	}

	/**
	 * Returns the number of appointment slots.
	 *
	 * @return the number of appointment slots
	 */
	public static int getAppointmentSlotsCount() {
		return getService().getAppointmentSlotsCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the appointment slot in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentSlotLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentSlot the appointment slot
	 * @return the appointment slot that was updated
	 */
	public static AppointmentSlot updateAppointmentSlot(
		AppointmentSlot appointmentSlot) {

		return getService().updateAppointmentSlot(appointmentSlot);
	}

	public static AppointmentSlotLocalService getService() {
		return _service;
	}

	public static void setService(AppointmentSlotLocalService service) {
		_service = service;
	}

	private static volatile AppointmentSlotLocalService _service;

}