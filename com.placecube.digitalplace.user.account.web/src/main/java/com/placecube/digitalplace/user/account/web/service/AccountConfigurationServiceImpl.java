package com.placecube.digitalplace.user.account.web.service;

import java.util.Set;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.captcha.configuration.CaptchaConfiguration;
import com.liferay.captcha.util.CaptchaUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfigurationFactory;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;
import com.placecube.digitalplace.userprofile.service.UserProfileFieldsService;

@Component(immediate = true, service = AccountConfigurationService.class)
public class AccountConfigurationServiceImpl implements AccountConfigurationService {

	private static final Log LOG = LogFactoryUtil.getLog(AccountConfigurationServiceImpl.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private CreateAccountConfigurationFactory createAccountConfigurationFactory;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private UserProfileFieldsService userProfileFieldsService;

	@Override
	public CreateAccountConfiguration getAccountConfiguration(AccountContext accountContext) throws UserAccountException {
		try {
			return createAccountConfigurationFactory.getCreateAccountConfiguration(groupLocalService.getGroup(accountContext.getGroupId()));
		} catch (PortalException e) {
			throw new UserAccountException(e);
		}
	}

	@Override
	public CreateAccountConfiguration getAccountConfiguration(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException {

		try {
			if (Validator.isNull(accountContext.getAccountConnectorModelId())) {
				return createAccountConfigurationFactory.getCreateAccountConfiguration(portletRequest);
			}
			return createAccountConfigurationFactory.getCreateAccountConfiguration(groupLocalService.getGroup(accountContext.getGroupId()));
		} catch (PortalException e) {
			throw new UserAccountException(e);
		}
	}

	@Override
	public String getAdditionalDetailsPreviousStep(UserProfileFieldsConfiguration configuration) {
		if (configuration.addressEnabled()) {
			return MVCCommandKeys.ADDRESS_DETAILS;

		} else {
			return MVCCommandKeys.PERSONAL_DETAILS;
		}
	}

	@Override
	public String getAddressNextStep(AccountContext accountContext) throws UserAccountException {
		UserProfileFieldsConfiguration configuration = getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());

		if (additionalDetailsStepsAvailable(accountContext, configuration)) {
			return MVCCommandKeys.ADDITIONAL_DETAILS;
		} else if (!accountContext.isSkipPasswordStep()) {
			return MVCCommandKeys.PASSWORD_DETAILS;
		} else if (isAccountSecurityStepEnabled(accountContext)) {
			return MVCCommandKeys.ACCOUNT_SECURITY;
		}
		return StringPool.BLANK;
	}

	@Override
	public UserProfileFieldsConfiguration getCompanyUserProfileFieldsConfiguration(long companyId) throws UserAccountException {
		try {

			return userProfileFieldsService.getCompanyUserProfileFieldsConfiguration(companyId);
		} catch (PortalException e) {
			throw new UserAccountException(e);
		}
	}

	@Override
	public Set<ExpandoField> getConfiguredExpandoFields(AccountContext accountContext, UserProfileFieldsConfiguration configuration) {
		return userProfileFieldsService.getConfiguredExpandoFields(accountContext.getCompanyId(), configuration);
	}

	@Override
	public boolean getEmailVerificationEnabled(AccountContext accountContext) {
		PortletPreferences preferences = PrefsPropsUtil.getPreferences(accountContext.getCompanyId());

		String securityStrangersVerifyString = preferences.getValue(PropsKeys.COMPANY_SECURITY_STRANGERS_VERIFY, null);

		return GetterUtil.getBoolean(securityStrangersVerifyString);
	}

	@Override
	public String getPasswordPreviousStep(AccountContext accountContext, UserProfileFieldsConfiguration configuration) {

		if (additionalDetailsStepsAvailable(accountContext, configuration)) {
			return MVCCommandKeys.ADDITIONAL_DETAILS;

		} else if (configuration.addressEnabled()) {
			return MVCCommandKeys.ADDRESS_DETAILS;

		} else {
			return MVCCommandKeys.PERSONAL_DETAILS;
		}
	}

	@Override
	public String getPersonalDetailsNextStep(AccountContext accountContext) throws UserAccountException {
		UserProfileFieldsConfiguration configuration = getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());

		if (configuration.addressEnabled()) {
			return MVCCommandKeys.ADDRESS_DETAILS;
		} else if (additionalDetailsStepsAvailable(accountContext, configuration)) {
			return MVCCommandKeys.ADDITIONAL_DETAILS;
		} else if (!accountContext.isSkipPasswordStep()) {
			return MVCCommandKeys.PASSWORD_DETAILS;
		} else if (isAccountSecurityStepEnabled(accountContext)) {
			return MVCCommandKeys.ACCOUNT_SECURITY;
		}
		return StringPool.BLANK;
	}

	@Override

	public boolean isAccountSecurityStepEnabled(AccountContext accountContext) throws UserAccountException {
		UserProfileFieldsConfiguration userProfileFieldsConfiguration = getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());

		return userProfileFieldsConfiguration.twoFactorAuthenticationPromptEnabled();
	}

	@Override
	public boolean isCaptchaEnabled(PortletRequest portletRequest) {
		try {
			if (CaptchaUtil.isEnabled(portletRequest)) {
				CaptchaConfiguration captchaConfiguration = configurationProvider.getSystemConfiguration(CaptchaConfiguration.class);

				return captchaConfiguration.createAccountCaptchaEnabled();
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return false;
	}

	private boolean additionalDetailsStepsAvailable(AccountContext accountContext, UserProfileFieldsConfiguration configuration) {
		return configuration.homePhoneNumberEnabled() || configuration.mobilePhoneNumberEnabled() || configuration.jobTitleEnabled()
				|| !getConfiguredExpandoFields(accountContext, configuration).isEmpty();
	}

}
