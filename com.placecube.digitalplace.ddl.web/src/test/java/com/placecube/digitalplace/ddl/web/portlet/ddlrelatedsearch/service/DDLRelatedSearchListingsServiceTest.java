package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.service;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.search.document.Document;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor("com.liferay.portal.kernel.dao.search.SearchContainer")
public class DDLRelatedSearchListingsServiceTest {

	private static final int MAX_ITEMS = 3;

	@InjectMocks
	private DDLRelatedSearchListingsService ddlRelatedSearchListingsService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRelatedSearchService mockDDLRelatedSearchService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private List<Document> mockResultsDocuments;

	@Mock
	private List<Object> mockResultsList;

	@Mock
	private SearchContainer mockSearchContainer;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void configureRequestAttributes_WhenExceptionGettingSearchHits_ThenDoesNothing() throws PortalException {

		when(mockDDLRelatedSearchService.getSearchResultDocuments(mockHttpServletRequest, mockDDLRecord, MAX_ITEMS)).thenThrow(PortalException.class);

		ddlRelatedSearchListingsService.configureRequestAttributes(mockHttpServletRequest, mockDDLRecord, MAX_ITEMS);

		verifyNoInteractions(mockSearchContainer);
		verify(mockHttpServletRequest, never()).setAttribute("hidePagination", true);
		verify(mockHttpServletRequest, never()).setAttribute("searchContainer", mockSearchContainer);
		verify(mockHttpServletRequest, never()).setAttribute("displayStyleList", false);
	}

	@Test
	public void configureRequestAttributes_WhenNoException_ThenSetsRequestAttributes() throws PortalException {

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockHttpServletRequest.getAttribute("javax.portlet.request")).thenReturn(mockRenderRequest);
		when(mockHttpServletRequest.getAttribute("javax.portlet.response")).thenReturn(mockRenderResponse);

		when(mockDDLRelatedSearchService.getSearchResultDocuments(mockHttpServletRequest, mockDDLRecord, MAX_ITEMS)).thenReturn(mockResultsDocuments);

		when(mockDDLRelatedSearchService.getDDLDisplayEntryResults(mockResultsDocuments, mockThemeDisplay, mockHttpServletRequest)).thenReturn(mockResultsList);
		when(mockDDLRelatedSearchService.getSearchContainer(mockRenderRequest, mockRenderResponse, mockResultsList)).thenReturn(mockSearchContainer);
		when(mockResultsList.size()).thenReturn(MAX_ITEMS);

		ddlRelatedSearchListingsService.configureRequestAttributes(mockHttpServletRequest, mockDDLRecord, MAX_ITEMS);

		verify(mockSearchContainer, times(1)).setResultsAndTotal(mockResultsList);
		verify(mockHttpServletRequest, times(1)).setAttribute("hidePagination", true);
		verify(mockHttpServletRequest, times(1)).setAttribute("searchContainer", mockSearchContainer);
		verify(mockHttpServletRequest, times(1)).setAttribute("displayStyleList", false);
	}

}
