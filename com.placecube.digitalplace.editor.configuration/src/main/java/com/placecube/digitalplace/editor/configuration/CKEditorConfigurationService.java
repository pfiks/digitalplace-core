package com.placecube.digitalplace.editor.configuration;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;

@Component(immediate = true, service = CKEditorConfigurationService.class)
public class CKEditorConfigurationService {

	private static final Log LOG = LogFactoryUtil.getLog(CKEditorConfigurationService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JSONFactory jsonFactory;

	public Optional<CKEditorGuidelineHelpGroupConfiguration> getGuidanceHelpConfiguration(long groupId) {
		try {
			return Optional.of(configurationProvider.getGroupConfiguration(CKEditorGuidelineHelpGroupConfiguration.class, groupId));
		} catch (ConfigurationException e) {
			LOG.warn("Unable to retrieve CKEditorGuidelineHelpGroupConfiguration for groupID: " + groupId);
			return Optional.empty();
		}
	}

	public Optional<JSONObject> getPluginAndRolesJsonConfiguration(long companyId) {

		try {
			return Optional.of(jsonFactory.createJSONObject(configurationProvider.getCompanyConfiguration(CKEditorCompanyConfiguration.class, companyId).pluginAndRolesJsonConfiguration()));
		} catch (Exception e) {
			LOG.error("Unable to retrieve CKEditorCompanyConfiguration for companyId: " + companyId);
			return Optional.empty();
		}
	}

	public Optional<JSONObject> getPluginJsonConfiguration(long companyId) {

		try {
			return Optional.of(jsonFactory.createJSONObject(configurationProvider.getCompanyConfiguration(CKEditorCompanyConfiguration.class, companyId).pluginJsonConfiguration()));
		} catch (Exception e) {
			LOG.error("Unable to retrieve CKEditorCompanyConfiguration for companyId: " + companyId);
			return Optional.empty();
		}
	}
}
