package com.placecube.digitalplace.search.web.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.web.portlet.contenttype.service.ContentTypeSearchService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(FacetResult.class)
public class FacetParsingServiceTest extends PowerMockito {

	@InjectMocks
	private FacetParsingService facetParsingService;

	@Mock
	private AssetCategory mockAssetCategory1;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private ContentTypeSearchService mockContentTypeSearchService;

	@Mock
	private FacetResult mockFacetResult1;

	@Mock
	private FacetResult mockFacetResult2;

	@Mock
	private FacetResult mockFacetResult3;

	@Mock
	private Map<Locale, String> mockLabelsMap1;

	@Mock
	private Map<Locale, String> mockLabelsMap2;

	@Mock
	private Map<Locale, String> mockLabelsMap3;

	@Mock
	private Map<Locale, String> mockTitleMap1;

	@Mock
	private Map<Locale, String> mockTitleMap2;

	@Before
	public void activateSetup() {
		mockStatic(FacetResult.class);
	}

	@Test
	public void parseAllCategories_WhenAssetCategoryCannotBeRetrieved_ThenDoesNotReturnsFacetResultForMissingCategory() throws Exception {
		String categoryId1 = "11";
		String categoryId2 = "22";

		Set<Long> categories = new HashSet<>();
		categories.add(Long.parseLong(categoryId1));
		categories.add(Long.parseLong(categoryId2));

		Set<String> selectedFacets = new HashSet<>();
		selectedFacets.add(categoryId1);

		Map<String, Integer> facetValues = new LinkedHashMap<>();
		facetValues.put(categoryId1, 1111);

		when(mockAssetCategoryLocalService.getAssetCategory(Long.parseLong(categoryId1))).thenReturn(mockAssetCategory1);
		when(mockAssetCategoryLocalService.getAssetCategory(Long.parseLong(categoryId2))).thenThrow(new PortalException());

		when(mockAssetCategory1.getTitleMap()).thenReturn(mockTitleMap1);

		when(FacetResult.init(categoryId1, 1111, true, mockTitleMap1)).thenReturn(mockFacetResult1);

		List<FacetResult> results = facetParsingService.parseAllCategories(facetValues, categories, selectedFacets);

		assertThat(results.size(), equalTo(1));
		assertThat(results.contains(mockFacetResult1), equalTo(true));
	}

	@Test
	public void parseAllCategories_WhenFacetForCategoryDoesNotExist_ThenReturnsFacetResultWithZeroValue() throws Exception {
		String categoryId1 = "11";
		String categoryId2 = "22";

		Set<Long> categories = new HashSet<>();
		categories.add(Long.parseLong(categoryId1));

		when(mockAssetCategoryLocalService.getAssetCategory(Long.parseLong(categoryId1))).thenReturn(mockAssetCategory1);
		when(mockAssetCategoryLocalService.getAssetCategory(Long.parseLong(categoryId2))).thenReturn(mockAssetCategory2);

		when(mockAssetCategory1.getTitleMap()).thenReturn(mockTitleMap1);

		when(FacetResult.init(categoryId1, 0, false, mockTitleMap1)).thenReturn(mockFacetResult1);

		List<FacetResult> results = facetParsingService.parseAllCategories(new HashMap<>(), categories, new HashSet());

		assertThat(results.size(), equalTo(1));
		assertThat(results.contains(mockFacetResult1), equalTo(true));
	}

	@Test
	public void parseAllCategories_WhenNoErrors_ThenReturnsFacetResultsForAllTheArgumentCategories() throws Exception {
		String categoryId1 = "11";
		String categoryId2 = "22";

		Set<Long> categories = new HashSet<>();
		categories.add(Long.parseLong(categoryId1));
		categories.add(Long.parseLong(categoryId2));

		Set<String> selectedFacets = new HashSet<>();
		selectedFacets.add(categoryId1);

		Map<String, Integer> facetValues = new LinkedHashMap<>();
		facetValues.put(categoryId1, 1111);
		facetValues.put(categoryId2, 2222);

		when(mockAssetCategoryLocalService.getAssetCategory(Long.parseLong(categoryId1))).thenReturn(mockAssetCategory1);
		when(mockAssetCategoryLocalService.getAssetCategory(Long.parseLong(categoryId2))).thenReturn(mockAssetCategory2);

		when(mockAssetCategory1.getTitleMap()).thenReturn(mockTitleMap1);
		when(mockAssetCategory2.getTitleMap()).thenReturn(mockTitleMap2);

		when(FacetResult.init(categoryId1, 1111, true, mockTitleMap1)).thenReturn(mockFacetResult1);
		when(FacetResult.init(categoryId2, 2222, false, mockTitleMap2)).thenReturn(mockFacetResult2);

		List<FacetResult> results = facetParsingService.parseAllCategories(facetValues, categories, selectedFacets);

		assertThat(results.size(), equalTo(2));
		assertThat(results.contains(mockFacetResult1), equalTo(true));
		assertThat(results.contains(mockFacetResult2), equalTo(true));
	}

	@Test
	public void parseCategoryFacets_WhenNoError_ThenReturnsAlistOfValidFacetResultsForAssetCategories() throws Exception {
		String categoryId1 = "11";
		String categoryId2 = "22";
		String categoryId3 = "33";
		Set<String> selectedFacets = new HashSet<>();
		selectedFacets.add(categoryId1);
		selectedFacets.add("someOtherSelectedValue");

		Map<String, Integer> facetValues = new LinkedHashMap<>();
		facetValues.put(categoryId1, 1111);
		facetValues.put(categoryId2, 2222);
		facetValues.put(categoryId3, 3333);

		when(mockAssetCategoryLocalService.getAssetCategory(Long.valueOf(categoryId1))).thenReturn(mockAssetCategory1);
		when(mockAssetCategory1.getTitleMap()).thenReturn(mockTitleMap1);
		when(FacetResult.init(categoryId1, 1111, true, mockTitleMap1)).thenReturn(mockFacetResult1);

		when(mockAssetCategoryLocalService.getAssetCategory(Long.valueOf(categoryId2))).thenThrow(new PortalException());

		when(mockAssetCategoryLocalService.getAssetCategory(Long.valueOf(categoryId3))).thenReturn(mockAssetCategory2);
		when(mockAssetCategory2.getTitleMap()).thenReturn(mockTitleMap2);
		when(FacetResult.init(categoryId3, 3333, false, mockTitleMap2)).thenReturn(mockFacetResult2);

		List<FacetResult> results = facetParsingService.parseCategoryFacets(facetValues, selectedFacets);

		assertThat(results, contains(mockFacetResult1, mockFacetResult2));
	}

	@Test
	public void parseContentTypes_WhenNoErrorAndJoinFiltersIsFalse_ThenReturnsAListOfFacetResultsForTheContentTypes() {
		Locale locale = Locale.CANADA_FRENCH;
		String contentTypeValue1 = "contentTypeVal1";
		String contentTypeValue2 = "contentTypeVal2";
		String contentTypeValue3 = "contentTypeVal3";

		Map<String, Integer> facetValues = new LinkedHashMap<>();
		facetValues.put(contentTypeValue1, 1111);
		facetValues.put(contentTypeValue2, 2222);
		facetValues.put(contentTypeValue3, 3333);

		Set<String> selectedFacets = new HashSet<>();
		selectedFacets.add(contentTypeValue2);

		when(mockContentTypeSearchService.getLabelMapForContentType(locale, contentTypeValue1)).thenReturn(mockLabelsMap1);
		when(FacetResult.init(contentTypeValue1, 1111, false, mockLabelsMap1)).thenReturn(mockFacetResult1);
		when(mockFacetResult1.getLabel(locale)).thenReturn("Beta");

		when(mockContentTypeSearchService.getLabelMapForContentType(locale, contentTypeValue2)).thenReturn(mockLabelsMap2);
		when(FacetResult.init(contentTypeValue2, 2222, true, mockLabelsMap2)).thenReturn(mockFacetResult2);
		when(mockFacetResult2.getLabel(locale)).thenReturn("Beta");

		when(mockContentTypeSearchService.getLabelMapForContentType(locale, contentTypeValue3)).thenReturn(mockLabelsMap3);
		when(FacetResult.init(contentTypeValue3, 3333, false, mockLabelsMap3)).thenReturn(mockFacetResult3);
		when(mockFacetResult3.getLabel(locale)).thenReturn("Alpha");

		List<FacetResult> results = facetParsingService.parseContentTypes(facetValues, selectedFacets, locale, false);

		assertThat(results.size(), equalTo(3));
		assertThat(results, contains(mockFacetResult3, mockFacetResult1, mockFacetResult2));

	}

	@Test
	public void parseContentTypes_WhenNoErrorAndJoinFiltersIsTrue_ThenReturnsAListOfGroupedFacetResultsForTheContentTypes() {
		Locale locale = Locale.CANADA_FRENCH;
		String contentTypeValue1 = "contentTypeVal1";
		String contentTypeValue2 = "contentTypeVal2";
		String contentTypeValue3 = "contentTypeVal3";

		Map<String, Integer> facetValues = new LinkedHashMap<>();
		facetValues.put(contentTypeValue1, 1111);
		facetValues.put(contentTypeValue2, 2222);
		facetValues.put(contentTypeValue3, 3333);

		Set<String> selectedFacets = new HashSet<>();
		selectedFacets.add(contentTypeValue1 + StringPool.COMMA + contentTypeValue2);

		when(mockContentTypeSearchService.getLabelMapForContentType(locale, contentTypeValue1)).thenReturn(mockLabelsMap1);
		when(FacetResult.init(contentTypeValue1, 1111, false, mockLabelsMap1)).thenReturn(mockFacetResult1);
		when(mockLabelsMap1.get(locale)).thenReturn("Beta");
		when(mockFacetResult1.getLabel(locale)).thenReturn("Beta");
		when(mockFacetResult1.getValue()).thenReturn(contentTypeValue1 + StringPool.COMMA + contentTypeValue2);

		when(mockContentTypeSearchService.getLabelMapForContentType(locale, contentTypeValue2)).thenReturn(mockLabelsMap2);
		when(mockLabelsMap2.get(locale)).thenReturn("Beta");

		when(mockContentTypeSearchService.getLabelMapForContentType(locale, contentTypeValue3)).thenReturn(mockLabelsMap3);
		when(FacetResult.init(contentTypeValue3, 3333, false, mockLabelsMap3)).thenReturn(mockFacetResult2);
		when(mockLabelsMap3.get(locale)).thenReturn("Alpha");
		when(mockFacetResult2.getLabel(locale)).thenReturn("Alpha");
		when(mockFacetResult2.getValue()).thenReturn(contentTypeValue3);

		List<FacetResult> results = facetParsingService.parseContentTypes(facetValues, selectedFacets, locale, true);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get(0), sameInstance(mockFacetResult2));
		assertThat(results.get(1), sameInstance(mockFacetResult1));

		verify(mockFacetResult1, times(1)).appendValue(contentTypeValue2);
		verify(mockFacetResult1, times(1)).setSelected(true);
		verify(mockFacetResult2, times(1)).setSelected(false);
	}

	@Test
	public void parseFacets_WhenNoError_ThenReturnsAlistOfFacetResults() {
		Set<String> selectedFacets = new HashSet<>();
		selectedFacets.add("valueA");
		selectedFacets.add("valueD");

		Map<String, Integer> facetValues = new LinkedHashMap<>();
		facetValues.put("valueA", 11);
		facetValues.put("valueB", 22);

		when(FacetResult.init("valueA", 11, true)).thenReturn(mockFacetResult1);
		when(FacetResult.init("valueB", 22, false)).thenReturn(mockFacetResult2);

		List<FacetResult> results = facetParsingService.parseFacets(facetValues, selectedFacets);

		assertThat(results, contains(mockFacetResult1, mockFacetResult2));
	}

}
