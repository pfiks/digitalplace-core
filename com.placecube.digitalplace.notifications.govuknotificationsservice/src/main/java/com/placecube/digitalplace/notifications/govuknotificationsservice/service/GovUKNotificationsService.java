package com.placecube.digitalplace.notifications.govuknotificationsservice.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.notifications.govuknotificationsservice.configuration.GovUKNotificationServiceConfiguration;

import uk.gov.service.notify.NotificationClient;

@Component(immediate = true, service = GovUKNotificationsService.class)
public class GovUKNotificationsService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public String getApplicationKey(long companyId, String apiKeyName) throws ConfigurationException {
		GovUKNotificationServiceConfiguration configuration = configurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, companyId);
		for (String apiKey : configuration.additionalApplicationKeys()) {
			String[] additionalKey = apiKey.split(StringPool.COLON);
			if (additionalKey.length == 2 && additionalKey[0].equals(apiKeyName)) {
				return additionalKey[1];
			}
		}
		return configuration.applicationKey();
	}

	public NotificationClient getClient(String applicationKey) {
		return new NotificationClient(applicationKey);
	}

	public boolean isEmailEnabled(long companyId) throws ConfigurationException {
		GovUKNotificationServiceConfiguration configuration = configurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, companyId);
		return configuration.emailEnabled();
	}

	public boolean isSmsEnabled(long companyId) throws ConfigurationException {
		GovUKNotificationServiceConfiguration configuration = configurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, companyId);
		return configuration.smsEnabled();
	}
}
