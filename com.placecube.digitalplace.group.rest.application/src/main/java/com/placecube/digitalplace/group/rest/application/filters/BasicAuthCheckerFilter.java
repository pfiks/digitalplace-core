package com.placecube.digitalplace.group.rest.application.filters;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.group.rest.application.BasicAuthGroupRestApplication;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.service.GroupRestRetrievalService;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;

@Provider
@PreMatching
public class BasicAuthCheckerFilter implements ContainerRequestFilter {

	private GroupRestRetrievalService groupRestRetrievalService;

	public BasicAuthCheckerFilter(BasicAuthGroupRestApplication restApplication) {
		groupRestRetrievalService = restApplication.getGroupRestRetrievalService();
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		SecurityContext securityContext = requestContext.getSecurityContext();

		if (isNotBasicAuth(securityContext) || groupRestRetrievalService.isGuestUser(getUserId(securityContext))) {
			throw new GroupRestSecurityException(ErrorCodes.INVALID_AUTHENTICATION, Response.Status.FORBIDDEN);
		}
	}

	private long getUserId(SecurityContext securityContext) {
		return GetterUtil.getLong(securityContext.getUserPrincipal().getName());
	}

	private boolean isNotBasicAuth(SecurityContext securityContext) {
		String authenticationScheme = securityContext.getAuthenticationScheme();
		return Validator.isNull(authenticationScheme) || !SecurityContext.BASIC_AUTH.equals(authenticationScheme);
	}

}
