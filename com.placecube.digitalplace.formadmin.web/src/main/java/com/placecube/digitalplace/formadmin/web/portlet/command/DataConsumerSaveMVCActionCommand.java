package com.placecube.digitalplace.formadmin.web.portlet.command;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.formadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.service.DataConsumerListService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.FORMS_MANAGER, "mvc.command.name=" + MVCCommandKeys.DATA_CONSUMER_SAVE }, service = MVCActionCommand.class)
public class DataConsumerSaveMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private DataConsumerListService dataConsumerListService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long dataConsumerId = ParamUtil.getLong(actionRequest, PortletRequestKeys.DATA_CONSUMER_ID);

		String headersJson = dataConsumerListService.extractHeadersAsJson(actionRequest);
		String parameterMapping = dataConsumerListService.extractParameterMappingAsJson(actionRequest);

		if (dataConsumerId > 0) {
			dataConsumerListService.updateDataConsumerDetails(dataConsumerId, headersJson, parameterMapping, actionRequest);
		} else {

			dataConsumerListService.createDataConsumer(headersJson, parameterMapping, actionRequest);
		}

		actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.SELECT_DATA_PROVIDER);
	}

}
