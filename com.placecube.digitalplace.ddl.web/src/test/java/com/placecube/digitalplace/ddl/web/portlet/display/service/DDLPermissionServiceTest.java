package com.placecube.digitalplace.ddl.web.portlet.display.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.lists.constants.DDLActionKeys;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.role.RoleConstants;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.permission.ModelPermissions;
import com.liferay.portal.kernel.theme.ThemeDisplay;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class DDLPermissionServiceTest extends PowerMockito {

	@InjectMocks
	private DDLPermissionService ddlPermissionService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private ModelPermissions mockModelPermissions;

	@Mock
	private ModelResourcePermission<DDLRecordSet> mockModelResourcePermission;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void configureDefaultDDLRecordSetPermissionsInServiceContext_WhenNoError_ThenConfiguresTheServiceContextWithTheDefaultPermissions() {
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		ddlPermissionService.configureDefaultDDLRecordSetPermissionsInServiceContext(mockServiceContext);

		InOrder inOrder = inOrder(mockModelPermissions, mockServiceContext);
		inOrder.verify(mockModelPermissions, times(1)).setResourceName(DDLRecordSet.class.getName());
		inOrder.verify(mockModelPermissions, times(1)).addRolePermissions(RoleConstants.OWNER, DDLActionKeys.ADD_RECORD, ActionKeys.VIEW);
		inOrder.verify(mockModelPermissions, times(1)).addRolePermissions(RoleConstants.SITE_MEMBER, DDLActionKeys.ADD_RECORD, ActionKeys.VIEW);
		inOrder.verify(mockServiceContext, times(1)).setModelPermissions(mockModelPermissions);
		inOrder.verify(mockServiceContext, times(1)).setAttribute("addRecordSetResources", true);
	}

	@Test
	public void hasAddPermission_WhenUserDoesNotHaveAddPermissionOnTheRecordSet_ThenReturnsFalse() throws PortalException {
		long recordSetId = 1;
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockDDLRecordSet.getRecordSetId()).thenReturn(recordSetId);
		doThrow(new PortalException()).when(mockModelResourcePermission).check(mockPermissionChecker, recordSetId, DDLActionKeys.ADD_RECORD);

		boolean result = ddlPermissionService.hasAddPermission(mockThemeDisplay, mockDDLRecordSet);

		assertFalse(result);
	}

	@Test
	public void hasAddPermission_WhenUserHasAddPermissionOnTheRecordSet_ThenReturnsTrue() throws PortalException {
		long recordSetId = 1;
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockDDLRecordSet.getRecordSetId()).thenReturn(recordSetId);

		boolean result = ddlPermissionService.hasAddPermission(mockThemeDisplay, mockDDLRecordSet);

		assertTrue(result);
		verify(mockModelResourcePermission, times(1)).check(mockPermissionChecker, recordSetId, DDLActionKeys.ADD_RECORD);
	}

	@Test
	public void hasDeletePermission_WhenUserIsNotRecordCreatorAndDoesNotHaveDeletePermissionOnTheRecordSet_ThenReturnsFalse() throws PortalException {
		long userId = 123;
		long recordUserId = 456;
		long recordSetId = 1;
		when(mockPermissionChecker.getUserId()).thenReturn(userId);
		when(mockDDLRecord.getUserId()).thenReturn(recordUserId);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);
		doThrow(new PortalException()).when(mockModelResourcePermission).check(mockPermissionChecker, recordSetId, ActionKeys.DELETE);

		boolean result = ddlPermissionService.hasDeletePermission(mockPermissionChecker, mockDDLRecord);

		assertFalse(result);
	}

	@Test
	public void hasDeletePermission_WhenUserIsNotRecordCreatorAndHasDeletePermissionOnTheRecordSet_ThenReturnsTrue() throws PortalException {
		long userId = 123;
		long recordUserId = 456;
		long recordSetId = 1;
		when(mockPermissionChecker.getUserId()).thenReturn(userId);
		when(mockDDLRecord.getUserId()).thenReturn(recordUserId);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);

		boolean result = ddlPermissionService.hasDeletePermission(mockPermissionChecker, mockDDLRecord);

		assertTrue(result);
		verify(mockModelResourcePermission, times(1)).check(mockPermissionChecker, recordSetId, ActionKeys.DELETE);
	}

	@Test
	public void hasDeletePermission_WhenUserIsRecordCreator_ThenReturnsTrue() throws PortalException {
		long userId = 123;
		when(mockPermissionChecker.getUserId()).thenReturn(userId);
		when(mockDDLRecord.getUserId()).thenReturn(userId);

		boolean result = ddlPermissionService.hasDeletePermission(mockPermissionChecker, mockDDLRecord);

		assertTrue(result);
		verifyZeroInteractions(mockModelResourcePermission);
	}

	@Test
	public void hasUpdatePermission_WhenUserIsNotRecordCreatorAndDoesNotHaveUpdatePermissionOnTheRecordSet_ThenReturnsFalse() throws PortalException {
		long userId = 123;
		long recordUserId = 456;
		long recordSetId = 1;
		when(mockPermissionChecker.getUserId()).thenReturn(userId);
		when(mockDDLRecord.getUserId()).thenReturn(recordUserId);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);
		doThrow(new PortalException()).when(mockModelResourcePermission).check(mockPermissionChecker, recordSetId, ActionKeys.UPDATE);

		boolean result = ddlPermissionService.hasUpdatePermission(mockPermissionChecker, mockDDLRecord);

		assertFalse(result);
	}

	@Test
	public void hasUpdatePermission_WhenUserIsNotRecordCreatorAndHasUpdatePermissionOnTheRecordSet_ThenReturnsTrue() throws PortalException {
		long userId = 123;
		long recordUserId = 456;
		long recordSetId = 1;
		when(mockPermissionChecker.getUserId()).thenReturn(userId);
		when(mockDDLRecord.getUserId()).thenReturn(recordUserId);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);

		boolean result = ddlPermissionService.hasUpdatePermission(mockPermissionChecker, mockDDLRecord);

		assertTrue(result);
		verify(mockModelResourcePermission, times(1)).check(mockPermissionChecker, recordSetId, ActionKeys.UPDATE);
	}

	@Test
	public void hasUpdatePermission_WhenUserIsRecordCreator_ThenReturnsTrue() throws PortalException {
		long userId = 123;
		when(mockPermissionChecker.getUserId()).thenReturn(userId);
		when(mockDDLRecord.getUserId()).thenReturn(userId);

		boolean result = ddlPermissionService.hasUpdatePermission(mockPermissionChecker, mockDDLRecord);

		assertTrue(result);
		verifyZeroInteractions(mockModelResourcePermission);
	}

	@Test
	@Parameters({ "1,1,false", "1,2,true" })
	public void validateDeletePermission_WhenHasPermissionOrIsOwner_ThenThrowsNoException(long permissionCheckerUserId, long recordUserId, boolean hasPermissionOnDDLRecord) throws PortalException {
		long recordSetId = 12;
		long ddlRecordId = 22;

		when(mockDDLRecordLocalService.getDDLRecord(ddlRecordId)).thenReturn(mockDDLRecord);
		when(mockPermissionChecker.getUserId()).thenReturn(permissionCheckerUserId);
		when(mockDDLRecord.getUserId()).thenReturn(recordUserId);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);
		if (!hasPermissionOnDDLRecord) {
			doThrow(new PortalException()).when(mockModelResourcePermission).check(mockPermissionChecker, recordSetId, ActionKeys.DELETE);
		}

		ddlPermissionService.validateDeletePermission(mockPermissionChecker, ddlRecordId);
	}

	@Test(expected = PortalException.class)
	public void validateDeletePermission_WhenNoPermissionAndIsNotOwner_ThenThrowsPortalException() throws PortalException {
		long recordSetId = 12;
		long ddlRecordId = 22;

		when(mockDDLRecordLocalService.getDDLRecord(ddlRecordId)).thenReturn(mockDDLRecord);
		when(mockPermissionChecker.getUserId()).thenReturn(1L);
		when(mockDDLRecord.getUserId()).thenReturn(2L);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);
		doThrow(new PortalException()).when(mockModelResourcePermission).check(mockPermissionChecker, recordSetId, ActionKeys.DELETE);

		ddlPermissionService.validateDeletePermission(mockPermissionChecker, ddlRecordId);
	}

	@Test
	@Parameters({ "1,1,false", "1,2,true" })
	public void validateUpdatePermission_WhenHasPermissionOrIsOwner_ThenThrowsNoException(long permissionCheckerUserId, long recordUserId, boolean hasPermissionOnDDLRecord) throws PortalException {
		long recordSetId = 12;
		long ddlRecordId = 22;

		when(mockDDLRecordLocalService.getDDLRecord(ddlRecordId)).thenReturn(mockDDLRecord);
		when(mockPermissionChecker.getUserId()).thenReturn(permissionCheckerUserId);
		when(mockDDLRecord.getUserId()).thenReturn(recordUserId);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);
		if (!hasPermissionOnDDLRecord) {
			doThrow(new PortalException()).when(mockModelResourcePermission).check(mockPermissionChecker, recordSetId, ActionKeys.UPDATE);
		}

		ddlPermissionService.validateUpdatePermission(mockPermissionChecker, ddlRecordId);
	}

	@Test(expected = PortalException.class)
	public void validateUpdatePermission_WhenNoPermissionAndIsNotOwner_ThenThrowsPortalException() throws PortalException {
		long recordSetId = 12;
		long ddlRecordId = 22;

		when(mockDDLRecordLocalService.getDDLRecord(ddlRecordId)).thenReturn(mockDDLRecord);
		when(mockPermissionChecker.getUserId()).thenReturn(1L);
		when(mockDDLRecord.getUserId()).thenReturn(2L);
		when(mockDDLRecord.getRecordSetId()).thenReturn(recordSetId);
		doThrow(new PortalException()).when(mockModelResourcePermission).check(mockPermissionChecker, recordSetId, ActionKeys.UPDATE);

		ddlPermissionService.validateUpdatePermission(mockPermissionChecker, ddlRecordId);
	}
}
