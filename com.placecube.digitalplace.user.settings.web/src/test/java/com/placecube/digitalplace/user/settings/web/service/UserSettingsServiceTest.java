package com.placecube.digitalplace.user.settings.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;
import com.placecube.digitalplace.user.settings.web.constants.PortletKeys;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.initializer.service.ServiceContextInitializer;
import com.placecube.journal.service.JournalArticleCreationService;

@SuppressWarnings("rawtypes")
@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
public class UserSettingsServiceTest extends PowerMockito {

	private static final String ARTICLE_CONTENT = "contentValue";

	private static final String ARTICLE_ID = "articleIdValue";

	private static final String ARTICLE_PATH = "articlePathValue";

	private static final String ARTICLE_TITLE = "articleTitleValue";

	@Mock
	private Company mockCompany;

	@Mock
	private Group mockGroup;

	@Mock
	private JournalArticleCreationService mockJournalArticleCreationService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private LiferayPortletURL mockLiferayPortletURL;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletURLFactory mockPortletURLFactory;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServiceContextInitializer mockServiceContextInitializer;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UserSettingsService userSettingsService;

	@Before
	public void activateSetup() {
		mockStatic(StringUtil.class);
	}

	@Test(expected = PortalException.class)
	public void configureUserSettingsWebContentArticle_WhenExceptionCreatingTheArticle_ThenThrowsPortalException() throws Exception {
		Class clazz = Group.class;
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockServiceContextInitializer.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		when(mockJournalArticleCreationService.getOrCreateJournalFolder("User Settings Portlet", mockServiceContext)).thenReturn(mockJournalFolder);
		when(StringUtil.read(clazz.getClassLoader(), ARTICLE_PATH)).thenReturn(ARTICLE_CONTENT);
		when(mockJournalArticleCreationService.getOrCreateBasicWebContentArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, mockJournalFolder, mockServiceContext)).thenThrow(new PortalException());

		userSettingsService.configureUserSettingsWebContentArticle(mockCompany, clazz, ARTICLE_PATH, ARTICLE_ID, ARTICLE_TITLE);
	}

	@Test(expected = PortalException.class)
	public void configureUserSettingsWebContentArticle_WhenExceptionCreatingTheFolder_ThenThrowsPortalException() throws Exception {
		Class clazz = Group.class;
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockServiceContextInitializer.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		when(mockJournalArticleCreationService.getOrCreateJournalFolder("User Settings Portlet", mockServiceContext)).thenThrow(new PortalException());

		userSettingsService.configureUserSettingsWebContentArticle(mockCompany, clazz, ARTICLE_PATH, ARTICLE_ID, ARTICLE_TITLE);
	}

	@Test(expected = PortalException.class)
	public void configureUserSettingsWebContentArticle_WhenExceptionGettingTheCompanyGroup_ThenThrowsPortalException() throws Exception {
		Class clazz = Group.class;
		when(mockCompany.getGroup()).thenThrow(new PortalException());

		userSettingsService.configureUserSettingsWebContentArticle(mockCompany, clazz, ARTICLE_PATH, ARTICLE_ID, ARTICLE_TITLE);
	}

	@Test(expected = PortalException.class)
	public void configureUserSettingsWebContentArticle_WhenExceptionReadingTheArticleContent_ThenThrowsPortalException() throws Exception {
		Class clazz = Group.class;
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockServiceContextInitializer.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		when(StringUtil.read(clazz.getClassLoader(), ARTICLE_PATH)).thenThrow(new IOException());

		userSettingsService.configureUserSettingsWebContentArticle(mockCompany, clazz, ARTICLE_PATH, ARTICLE_ID, ARTICLE_TITLE);
	}

	@Test
	public void configureUserSettingsWebContentArticle_WhenNoError_ThenCreatesTheArticle() throws Exception {
		Class clazz = Group.class;
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockServiceContextInitializer.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
		when(mockJournalArticleCreationService.getOrCreateJournalFolder("User Settings Portlet", mockServiceContext)).thenReturn(mockJournalFolder);
		when(StringUtil.read(clazz.getClassLoader(), ARTICLE_PATH)).thenReturn(ARTICLE_CONTENT);

		userSettingsService.configureUserSettingsWebContentArticle(mockCompany, clazz, ARTICLE_PATH, ARTICLE_ID, ARTICLE_TITLE);

		verify(mockJournalArticleCreationService, times(1)).getOrCreateBasicWebContentArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void getUserSettingsURL_WhenNoError_ThenReturnsALiferayURLForTheUserSettingsPorltet() {
		long plid = 123;
		when(mockThemeDisplay.getPlid()).thenReturn(plid);
		when(mockPortletURLFactory.create(mockPortletRequest, PortletKeys.USER_SETTINGS, plid, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		LiferayPortletURL result = userSettingsService.getUserSettingsURL(mockPortletRequest, mockThemeDisplay, DisplaySection.NOTIFICATIONS);

		assertThat(result, sameInstance(mockLiferayPortletURL));
		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.CURRENT_SECTION_ID, DisplaySection.NOTIFICATIONS.getKey());
	}

}
