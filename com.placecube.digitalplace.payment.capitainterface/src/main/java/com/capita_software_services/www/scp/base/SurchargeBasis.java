/**
 * SurchargeBasis.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;


/**
 * At least one of surchargeFixed and/or surchargeRate
 * 				is required
 */
public class SurchargeBasis  implements java.io.Serializable {
    private org.apache.axis.types.PositiveInteger surchargeFixed;

    private java.math.BigDecimal surchargeRate;

    public SurchargeBasis() {
    }

    public SurchargeBasis(
           org.apache.axis.types.PositiveInteger surchargeFixed,
           java.math.BigDecimal surchargeRate) {
           this.surchargeFixed = surchargeFixed;
           this.surchargeRate = surchargeRate;
    }


    /**
     * Gets the surchargeFixed value for this SurchargeBasis.
     *
     * @return surchargeFixed
     */
    public org.apache.axis.types.PositiveInteger getSurchargeFixed() {
        return surchargeFixed;
    }


    /**
     * Sets the surchargeFixed value for this SurchargeBasis.
     *
     * @param surchargeFixed
     */
    public void setSurchargeFixed(org.apache.axis.types.PositiveInteger surchargeFixed) {
        this.surchargeFixed = surchargeFixed;
    }


    /**
     * Gets the surchargeRate value for this SurchargeBasis.
     *
     * @return surchargeRate
     */
    public java.math.BigDecimal getSurchargeRate() {
        return surchargeRate;
    }


    /**
     * Sets the surchargeRate value for this SurchargeBasis.
     *
     * @param surchargeRate
     */
    public void setSurchargeRate(java.math.BigDecimal surchargeRate) {
        this.surchargeRate = surchargeRate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SurchargeBasis)) return false;
        SurchargeBasis other = (SurchargeBasis) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.surchargeFixed==null && other.getSurchargeFixed()==null) ||
             (this.surchargeFixed!=null &&
              this.surchargeFixed.equals(other.getSurchargeFixed()))) &&
            ((this.surchargeRate==null && other.getSurchargeRate()==null) ||
             (this.surchargeRate!=null &&
              this.surchargeRate.equals(other.getSurchargeRate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSurchargeFixed() != null) {
            _hashCode += getSurchargeFixed().hashCode();
        }
        if (getSurchargeRate() != null) {
            _hashCode += getSurchargeRate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SurchargeBasis.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeBasis"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeFixed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeFixed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "positiveInteger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/portal-api", "generalDecimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
