package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.ADDRESS_DETAILS }, service = MVCActionCommand.class)

public class AddressDetailsMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private AccountService accountService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		AccountContext accountContext = accountContextService.getOrCreateContext(actionRequest);
		accountContextService.configureAddressDetails(accountContext, actionRequest);
		accountContextService.configureSkipPasswordStep(accountContext);

		String mvcCommandKey = MVCCommandKeys.ADDRESS_DETAILS;

		if (!accountContext.hasErrors()) {
			String nextStep = accountConfigurationService.getAddressNextStep(accountContext);
			if (Validator.isNotNull(nextStep)) {
				mvcCommandKey = nextStep;
			} else {
				accountService.createUserAccount(actionRequest, accountContext, accountConfigurationService.getAccountConfiguration(accountContext, actionRequest).addressOutOfLocalArea());
				mvcCommandKey = MVCCommandKeys.CONFIRMATION_PAGE;
			}
		}
		actionResponse.getRenderParameters().setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, mvcCommandKey);
		accountContextService.updateContextInSession(accountContext, actionRequest);
	}

}
