package com.placecube.digitalplace.appointment.admin.web.portlet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.TimeZone;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.admin.web.model.AppointmentSlotForm;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;

@RunWith(MockitoJUnitRunner.class)
public class UpdateAppointmentSlotMVCRenderCommandTest {

	@Mock
	private AppointmentSlotForm mockAppointmentSlotForm;

	@Mock
	private AppointmentsManagerService mockAppointmentsManagerService;

	private MockedStatic<ParamUtil> mockParamUtil;

	private MockedStatic<PropsUtil> mockPropsUtil;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TimeZone mockTimeZone;

	@InjectMocks
	private UpdateAppointmentSlotMVCRenderCommand updateAppointmentSlotMVCRenderCommand;

	@Before
	public void activateSetUp() {
		mockPropsUtil = mockStatic(PropsUtil.class);
		mockParamUtil = mockStatic(ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenAppointmentSlotIdGreaterThanZeroAndExceptionRetrievingTheAppointmentSlotForm_ThenThrowsPortletException() throws Exception {
		long appointmentSlotId = 11;
		mockParamUtil.when(() -> ParamUtil.getLong(mockRenderRequest, "appointmentSlotId", 0l)).thenReturn(appointmentSlotId);
		when(mockAppointmentsManagerService.getAppointmentSlotForm(appointmentSlotId)).thenThrow(new PortalException());

		updateAppointmentSlotMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoErrorAndAppointmentSlotIdGreaterThanZero_ThenSetsAppointmentSlotFormAsAttribute() throws Exception {
		long appointmentSlotId = 11;
		mockParamUtil.when(() -> ParamUtil.getLong(mockRenderRequest, "appointmentSlotId", 0l)).thenReturn(appointmentSlotId);
		when(mockAppointmentsManagerService.getAppointmentSlotForm(appointmentSlotId)).thenReturn(mockAppointmentSlotForm);

		String result = updateAppointmentSlotMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/update-slot.jsp"));

		verify(mockRenderRequest, times(1)).setAttribute("appointmentSlotForm", mockAppointmentSlotForm);
	}

	@Test
	public void render_WhenNoErrorAndAppointmentSlotIdNotGreaterThanZero_ThenSetsAppointmentSlotFormAndGroupsAsAttribute() throws Exception {
		mockParamUtil.when(() -> ParamUtil.getLong(mockRenderRequest, "appointmentSlotId", 0l)).thenReturn(0l);
		mockParamUtil.when(() -> ParamUtil.getString(mockRenderRequest, "slotType")).thenReturn("slotTypeValue");
		mockParamUtil.when(() -> ParamUtil.getString(mockRenderRequest, "serviceId")).thenReturn("serviceIdValue");
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getTimeZone()).thenReturn(mockTimeZone);
		when(mockAppointmentsManagerService.getBlankAppointmentSlotForm(mockTimeZone, "slotTypeValue", "serviceIdValue")).thenReturn(mockAppointmentSlotForm);

		String result = updateAppointmentSlotMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/update-slot.jsp"));

		verify(mockRenderRequest, times(1)).setAttribute("appointmentSlotForm", mockAppointmentSlotForm);
	}

	@After
	public void teardown() {
		mockPropsUtil.close();
		mockParamUtil.close();
	}

}
