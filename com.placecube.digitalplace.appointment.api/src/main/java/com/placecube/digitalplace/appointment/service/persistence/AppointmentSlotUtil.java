/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.appointment.model.AppointmentSlot;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the appointment slot service. This utility wraps <code>com.placecube.digitalplace.appointment.service.persistence.impl.AppointmentSlotPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentSlotPersistence
 * @generated
 */
public class AppointmentSlotUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(AppointmentSlot appointmentSlot) {
		getPersistence().clearCache(appointmentSlot);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, AppointmentSlot> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AppointmentSlot> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AppointmentSlot> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AppointmentSlot> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static AppointmentSlot update(AppointmentSlot appointmentSlot) {
		return getPersistence().update(appointmentSlot);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static AppointmentSlot update(
		AppointmentSlot appointmentSlot, ServiceContext serviceContext) {

		return getPersistence().update(appointmentSlot, serviceContext);
	}

	/**
	 * Returns all the appointment slots where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching appointment slots
	 */
	public static List<AppointmentSlot> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the appointment slots where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of matching appointment slots
	 */
	public static List<AppointmentSlot> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the appointment slots where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment slots
	 */
	public static List<AppointmentSlot> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the appointment slots where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment slots
	 */
	public static List<AppointmentSlot> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppointmentSlot> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public static AppointmentSlot findByUuid_First(
			String uuid, OrderByComparator<AppointmentSlot> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public static AppointmentSlot fetchByUuid_First(
		String uuid, OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public static AppointmentSlot findByUuid_Last(
			String uuid, OrderByComparator<AppointmentSlot> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public static AppointmentSlot fetchByUuid_Last(
		String uuid, OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the appointment slots before and after the current appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param appointmentSlotId the primary key of the current appointment slot
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment slot
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public static AppointmentSlot[] findByUuid_PrevAndNext(
			long appointmentSlotId, String uuid,
			OrderByComparator<AppointmentSlot> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByUuid_PrevAndNext(
			appointmentSlotId, uuid, orderByComparator);
	}

	/**
	 * Removes all the appointment slots where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of appointment slots where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching appointment slots
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns all the appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching appointment slots
	 */
	public static List<AppointmentSlot> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of matching appointment slots
	 */
	public static List<AppointmentSlot> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment slots
	 */
	public static List<AppointmentSlot> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment slots
	 */
	public static List<AppointmentSlot> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppointmentSlot> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public static AppointmentSlot findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<AppointmentSlot> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public static AppointmentSlot fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public static AppointmentSlot findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<AppointmentSlot> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public static AppointmentSlot fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the appointment slots before and after the current appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param appointmentSlotId the primary key of the current appointment slot
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment slot
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public static AppointmentSlot[] findByUuid_C_PrevAndNext(
			long appointmentSlotId, String uuid, long companyId,
			OrderByComparator<AppointmentSlot> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByUuid_C_PrevAndNext(
			appointmentSlotId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the appointment slots where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching appointment slots
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @return the matching appointment slots
	 */
	public static List<AppointmentSlot> findByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId) {

		return getPersistence().findByCompanyId_SlotType_ServiceId(
			companyId, slotType, serviceId);
	}

	/**
	 * Returns a range of all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of matching appointment slots
	 */
	public static List<AppointmentSlot> findByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId, int start, int end) {

		return getPersistence().findByCompanyId_SlotType_ServiceId(
			companyId, slotType, serviceId, start, end);
	}

	/**
	 * Returns an ordered range of all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment slots
	 */
	public static List<AppointmentSlot> findByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId, int start, int end,
		OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().findByCompanyId_SlotType_ServiceId(
			companyId, slotType, serviceId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment slots
	 */
	public static List<AppointmentSlot> findByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId, int start, int end,
		OrderByComparator<AppointmentSlot> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByCompanyId_SlotType_ServiceId(
			companyId, slotType, serviceId, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public static AppointmentSlot findByCompanyId_SlotType_ServiceId_First(
			long companyId, String slotType, String serviceId,
			OrderByComparator<AppointmentSlot> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByCompanyId_SlotType_ServiceId_First(
			companyId, slotType, serviceId, orderByComparator);
	}

	/**
	 * Returns the first appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public static AppointmentSlot fetchByCompanyId_SlotType_ServiceId_First(
		long companyId, String slotType, String serviceId,
		OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().fetchByCompanyId_SlotType_ServiceId_First(
			companyId, slotType, serviceId, orderByComparator);
	}

	/**
	 * Returns the last appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public static AppointmentSlot findByCompanyId_SlotType_ServiceId_Last(
			long companyId, String slotType, String serviceId,
			OrderByComparator<AppointmentSlot> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByCompanyId_SlotType_ServiceId_Last(
			companyId, slotType, serviceId, orderByComparator);
	}

	/**
	 * Returns the last appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public static AppointmentSlot fetchByCompanyId_SlotType_ServiceId_Last(
		long companyId, String slotType, String serviceId,
		OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().fetchByCompanyId_SlotType_ServiceId_Last(
			companyId, slotType, serviceId, orderByComparator);
	}

	/**
	 * Returns the appointment slots before and after the current appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param appointmentSlotId the primary key of the current appointment slot
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment slot
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public static AppointmentSlot[]
			findByCompanyId_SlotType_ServiceId_PrevAndNext(
				long appointmentSlotId, long companyId, String slotType,
				String serviceId,
				OrderByComparator<AppointmentSlot> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByCompanyId_SlotType_ServiceId_PrevAndNext(
			appointmentSlotId, companyId, slotType, serviceId,
			orderByComparator);
	}

	/**
	 * Removes all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 */
	public static void removeByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId) {

		getPersistence().removeByCompanyId_SlotType_ServiceId(
			companyId, slotType, serviceId);
	}

	/**
	 * Returns the number of appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @return the number of matching appointment slots
	 */
	public static int countByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId) {

		return getPersistence().countByCompanyId_SlotType_ServiceId(
			companyId, slotType, serviceId);
	}

	/**
	 * Caches the appointment slot in the entity cache if it is enabled.
	 *
	 * @param appointmentSlot the appointment slot
	 */
	public static void cacheResult(AppointmentSlot appointmentSlot) {
		getPersistence().cacheResult(appointmentSlot);
	}

	/**
	 * Caches the appointment slots in the entity cache if it is enabled.
	 *
	 * @param appointmentSlots the appointment slots
	 */
	public static void cacheResult(List<AppointmentSlot> appointmentSlots) {
		getPersistence().cacheResult(appointmentSlots);
	}

	/**
	 * Creates a new appointment slot with the primary key. Does not add the appointment slot to the database.
	 *
	 * @param appointmentSlotId the primary key for the new appointment slot
	 * @return the new appointment slot
	 */
	public static AppointmentSlot create(long appointmentSlotId) {
		return getPersistence().create(appointmentSlotId);
	}

	/**
	 * Removes the appointment slot with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot that was removed
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public static AppointmentSlot remove(long appointmentSlotId)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().remove(appointmentSlotId);
	}

	public static AppointmentSlot updateImpl(AppointmentSlot appointmentSlot) {
		return getPersistence().updateImpl(appointmentSlot);
	}

	/**
	 * Returns the appointment slot with the primary key or throws a <code>NoSuchAppointmentSlotException</code> if it could not be found.
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public static AppointmentSlot findByPrimaryKey(long appointmentSlotId)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentSlotException {

		return getPersistence().findByPrimaryKey(appointmentSlotId);
	}

	/**
	 * Returns the appointment slot with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot, or <code>null</code> if a appointment slot with the primary key could not be found
	 */
	public static AppointmentSlot fetchByPrimaryKey(long appointmentSlotId) {
		return getPersistence().fetchByPrimaryKey(appointmentSlotId);
	}

	/**
	 * Returns all the appointment slots.
	 *
	 * @return the appointment slots
	 */
	public static List<AppointmentSlot> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the appointment slots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of appointment slots
	 */
	public static List<AppointmentSlot> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the appointment slots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of appointment slots
	 */
	public static List<AppointmentSlot> findAll(
		int start, int end,
		OrderByComparator<AppointmentSlot> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the appointment slots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of appointment slots
	 */
	public static List<AppointmentSlot> findAll(
		int start, int end,
		OrderByComparator<AppointmentSlot> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the appointment slots from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of appointment slots.
	 *
	 * @return the number of appointment slots
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AppointmentSlotPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(AppointmentSlotPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile AppointmentSlotPersistence _persistence;

}