/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model.impl;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringUtil;

import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.model.AppointmentEntryModel;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.sql.Blob;
import java.sql.Types;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * The base model implementation for the AppointmentEntry service. Represents a row in the &quot;Placecube_Appointment_AppointmentEntry&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface <code>AppointmentEntryModel</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link AppointmentEntryImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentEntryImpl
 * @generated
 */
public class AppointmentEntryModelImpl
	extends BaseModelImpl<AppointmentEntry> implements AppointmentEntryModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a appointment entry model instance should use the <code>AppointmentEntry</code> interface instead.
	 */
	public static final String TABLE_NAME =
		"Placecube_Appointment_AppointmentEntry";

	public static final Object[][] TABLE_COLUMNS = {
		{"uuid_", Types.VARCHAR}, {"appointmentEntryId", Types.BIGINT},
		{"companyId", Types.BIGINT}, {"entryClassPK", Types.BIGINT},
		{"entryClassNameId", Types.BIGINT}, {"appointmentId", Types.CLOB},
		{"serviceId", Types.VARCHAR}, {"connectorClassName", Types.VARCHAR},
		{"createDate", Types.TIMESTAMP},
		{"appointmentStartDate", Types.TIMESTAMP},
		{"appointmentEndDate", Types.TIMESTAMP}
	};

	public static final Map<String, Integer> TABLE_COLUMNS_MAP =
		new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("uuid_", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("appointmentEntryId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("companyId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("entryClassPK", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("entryClassNameId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("appointmentId", Types.CLOB);
		TABLE_COLUMNS_MAP.put("serviceId", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("connectorClassName", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("createDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("appointmentStartDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("appointmentEndDate", Types.TIMESTAMP);
	}

	public static final String TABLE_SQL_CREATE =
		"create table Placecube_Appointment_AppointmentEntry (uuid_ VARCHAR(75) null,appointmentEntryId LONG not null primary key,companyId LONG,entryClassPK LONG,entryClassNameId LONG,appointmentId TEXT null,serviceId STRING null,connectorClassName STRING null,createDate DATE null,appointmentStartDate DATE null,appointmentEndDate DATE null)";

	public static final String TABLE_SQL_DROP =
		"drop table Placecube_Appointment_AppointmentEntry";

	public static final String ORDER_BY_JPQL =
		" ORDER BY appointmentEntry.appointmentEntryId ASC";

	public static final String ORDER_BY_SQL =
		" ORDER BY Placecube_Appointment_AppointmentEntry.appointmentEntryId ASC";

	public static final String DATA_SOURCE = "liferayDataSource";

	public static final String SESSION_FACTORY = "liferaySessionFactory";

	public static final String TX_MANAGER = "liferayTransactionManager";

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long APPOINTMENTID_COLUMN_BITMASK = 1L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long COMPANYID_COLUMN_BITMASK = 2L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long CONNECTORCLASSNAME_COLUMN_BITMASK = 4L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long ENTRYCLASSNAMEID_COLUMN_BITMASK = 8L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long ENTRYCLASSPK_COLUMN_BITMASK = 16L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long SERVICEID_COLUMN_BITMASK = 32L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long UUID_COLUMN_BITMASK = 64L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *		#getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long APPOINTMENTENTRYID_COLUMN_BITMASK = 128L;

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setEntityCacheEnabled(boolean entityCacheEnabled) {
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setFinderCacheEnabled(boolean finderCacheEnabled) {
	}

	public AppointmentEntryModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _appointmentEntryId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setAppointmentEntryId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _appointmentEntryId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return AppointmentEntry.class;
	}

	@Override
	public String getModelClassName() {
		return AppointmentEntry.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		Map<String, Function<AppointmentEntry, Object>>
			attributeGetterFunctions = getAttributeGetterFunctions();

		for (Map.Entry<String, Function<AppointmentEntry, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<AppointmentEntry, Object> attributeGetterFunction =
				entry.getValue();

			attributes.put(
				attributeName,
				attributeGetterFunction.apply((AppointmentEntry)this));
		}

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Map<String, BiConsumer<AppointmentEntry, Object>>
			attributeSetterBiConsumers = getAttributeSetterBiConsumers();

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			String attributeName = entry.getKey();

			BiConsumer<AppointmentEntry, Object> attributeSetterBiConsumer =
				attributeSetterBiConsumers.get(attributeName);

			if (attributeSetterBiConsumer != null) {
				attributeSetterBiConsumer.accept(
					(AppointmentEntry)this, entry.getValue());
			}
		}
	}

	public Map<String, Function<AppointmentEntry, Object>>
		getAttributeGetterFunctions() {

		return AttributeGetterFunctionsHolder._attributeGetterFunctions;
	}

	public Map<String, BiConsumer<AppointmentEntry, Object>>
		getAttributeSetterBiConsumers() {

		return AttributeSetterBiConsumersHolder._attributeSetterBiConsumers;
	}

	private static class AttributeGetterFunctionsHolder {

		private static final Map<String, Function<AppointmentEntry, Object>>
			_attributeGetterFunctions;

		static {
			Map<String, Function<AppointmentEntry, Object>>
				attributeGetterFunctions =
					new LinkedHashMap
						<String, Function<AppointmentEntry, Object>>();

			attributeGetterFunctions.put("uuid", AppointmentEntry::getUuid);
			attributeGetterFunctions.put(
				"appointmentEntryId", AppointmentEntry::getAppointmentEntryId);
			attributeGetterFunctions.put(
				"companyId", AppointmentEntry::getCompanyId);
			attributeGetterFunctions.put(
				"entryClassPK", AppointmentEntry::getEntryClassPK);
			attributeGetterFunctions.put(
				"entryClassNameId", AppointmentEntry::getEntryClassNameId);
			attributeGetterFunctions.put(
				"appointmentId", AppointmentEntry::getAppointmentId);
			attributeGetterFunctions.put(
				"serviceId", AppointmentEntry::getServiceId);
			attributeGetterFunctions.put(
				"connectorClassName", AppointmentEntry::getConnectorClassName);
			attributeGetterFunctions.put(
				"createDate", AppointmentEntry::getCreateDate);
			attributeGetterFunctions.put(
				"appointmentStartDate",
				AppointmentEntry::getAppointmentStartDate);
			attributeGetterFunctions.put(
				"appointmentEndDate", AppointmentEntry::getAppointmentEndDate);

			_attributeGetterFunctions = Collections.unmodifiableMap(
				attributeGetterFunctions);
		}

	}

	private static class AttributeSetterBiConsumersHolder {

		private static final Map<String, BiConsumer<AppointmentEntry, Object>>
			_attributeSetterBiConsumers;

		static {
			Map<String, BiConsumer<AppointmentEntry, ?>>
				attributeSetterBiConsumers =
					new LinkedHashMap
						<String, BiConsumer<AppointmentEntry, ?>>();

			attributeSetterBiConsumers.put(
				"uuid",
				(BiConsumer<AppointmentEntry, String>)
					AppointmentEntry::setUuid);
			attributeSetterBiConsumers.put(
				"appointmentEntryId",
				(BiConsumer<AppointmentEntry, Long>)
					AppointmentEntry::setAppointmentEntryId);
			attributeSetterBiConsumers.put(
				"companyId",
				(BiConsumer<AppointmentEntry, Long>)
					AppointmentEntry::setCompanyId);
			attributeSetterBiConsumers.put(
				"entryClassPK",
				(BiConsumer<AppointmentEntry, Long>)
					AppointmentEntry::setEntryClassPK);
			attributeSetterBiConsumers.put(
				"entryClassNameId",
				(BiConsumer<AppointmentEntry, Long>)
					AppointmentEntry::setEntryClassNameId);
			attributeSetterBiConsumers.put(
				"appointmentId",
				(BiConsumer<AppointmentEntry, String>)
					AppointmentEntry::setAppointmentId);
			attributeSetterBiConsumers.put(
				"serviceId",
				(BiConsumer<AppointmentEntry, String>)
					AppointmentEntry::setServiceId);
			attributeSetterBiConsumers.put(
				"connectorClassName",
				(BiConsumer<AppointmentEntry, String>)
					AppointmentEntry::setConnectorClassName);
			attributeSetterBiConsumers.put(
				"createDate",
				(BiConsumer<AppointmentEntry, Date>)
					AppointmentEntry::setCreateDate);
			attributeSetterBiConsumers.put(
				"appointmentStartDate",
				(BiConsumer<AppointmentEntry, Date>)
					AppointmentEntry::setAppointmentStartDate);
			attributeSetterBiConsumers.put(
				"appointmentEndDate",
				(BiConsumer<AppointmentEntry, Date>)
					AppointmentEntry::setAppointmentEndDate);

			_attributeSetterBiConsumers = Collections.unmodifiableMap(
				(Map)attributeSetterBiConsumers);
		}

	}

	@Override
	public String getUuid() {
		if (_uuid == null) {
			return "";
		}
		else {
			return _uuid;
		}
	}

	@Override
	public void setUuid(String uuid) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_uuid = uuid;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalUuid() {
		return getColumnOriginalValue("uuid_");
	}

	@Override
	public long getAppointmentEntryId() {
		return _appointmentEntryId;
	}

	@Override
	public void setAppointmentEntryId(long appointmentEntryId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_appointmentEntryId = appointmentEntryId;
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_companyId = companyId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalCompanyId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("companyId"));
	}

	@Override
	public long getEntryClassPK() {
		return _entryClassPK;
	}

	@Override
	public void setEntryClassPK(long entryClassPK) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_entryClassPK = entryClassPK;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalEntryClassPK() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("entryClassPK"));
	}

	@Override
	public long getEntryClassNameId() {
		return _entryClassNameId;
	}

	@Override
	public void setEntryClassNameId(long entryClassNameId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_entryClassNameId = entryClassNameId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalEntryClassNameId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("entryClassNameId"));
	}

	@Override
	public String getAppointmentId() {
		if (_appointmentId == null) {
			return "";
		}
		else {
			return _appointmentId;
		}
	}

	@Override
	public void setAppointmentId(String appointmentId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_appointmentId = appointmentId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalAppointmentId() {
		return getColumnOriginalValue("appointmentId");
	}

	@Override
	public String getServiceId() {
		if (_serviceId == null) {
			return "";
		}
		else {
			return _serviceId;
		}
	}

	@Override
	public void setServiceId(String serviceId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_serviceId = serviceId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalServiceId() {
		return getColumnOriginalValue("serviceId");
	}

	@Override
	public String getConnectorClassName() {
		if (_connectorClassName == null) {
			return "";
		}
		else {
			return _connectorClassName;
		}
	}

	@Override
	public void setConnectorClassName(String connectorClassName) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_connectorClassName = connectorClassName;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalConnectorClassName() {
		return getColumnOriginalValue("connectorClassName");
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_createDate = createDate;
	}

	@Override
	public Date getAppointmentStartDate() {
		return _appointmentStartDate;
	}

	@Override
	public void setAppointmentStartDate(Date appointmentStartDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_appointmentStartDate = appointmentStartDate;
	}

	@Override
	public Date getAppointmentEndDate() {
		return _appointmentEndDate;
	}

	@Override
	public void setAppointmentEndDate(Date appointmentEndDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_appointmentEndDate = appointmentEndDate;
	}

	public long getColumnBitmask() {
		if (_columnBitmask > 0) {
			return _columnBitmask;
		}

		if ((_columnOriginalValues == null) ||
			(_columnOriginalValues == Collections.EMPTY_MAP)) {

			return 0;
		}

		for (Map.Entry<String, Object> entry :
				_columnOriginalValues.entrySet()) {

			if (!Objects.equals(
					entry.getValue(), getColumnValue(entry.getKey()))) {

				_columnBitmask |= _columnBitmasks.get(entry.getKey());
			}
		}

		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(
			getCompanyId(), AppointmentEntry.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public AppointmentEntry toEscapedModel() {
		if (_escapedModel == null) {
			Function<InvocationHandler, AppointmentEntry>
				escapedModelProxyProviderFunction =
					EscapedModelProxyProviderFunctionHolder.
						_escapedModelProxyProviderFunction;

			_escapedModel = escapedModelProxyProviderFunction.apply(
				new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		AppointmentEntryImpl appointmentEntryImpl = new AppointmentEntryImpl();

		appointmentEntryImpl.setUuid(getUuid());
		appointmentEntryImpl.setAppointmentEntryId(getAppointmentEntryId());
		appointmentEntryImpl.setCompanyId(getCompanyId());
		appointmentEntryImpl.setEntryClassPK(getEntryClassPK());
		appointmentEntryImpl.setEntryClassNameId(getEntryClassNameId());
		appointmentEntryImpl.setAppointmentId(getAppointmentId());
		appointmentEntryImpl.setServiceId(getServiceId());
		appointmentEntryImpl.setConnectorClassName(getConnectorClassName());
		appointmentEntryImpl.setCreateDate(getCreateDate());
		appointmentEntryImpl.setAppointmentStartDate(getAppointmentStartDate());
		appointmentEntryImpl.setAppointmentEndDate(getAppointmentEndDate());

		appointmentEntryImpl.resetOriginalValues();

		return appointmentEntryImpl;
	}

	@Override
	public AppointmentEntry cloneWithOriginalValues() {
		AppointmentEntryImpl appointmentEntryImpl = new AppointmentEntryImpl();

		appointmentEntryImpl.setUuid(
			this.<String>getColumnOriginalValue("uuid_"));
		appointmentEntryImpl.setAppointmentEntryId(
			this.<Long>getColumnOriginalValue("appointmentEntryId"));
		appointmentEntryImpl.setCompanyId(
			this.<Long>getColumnOriginalValue("companyId"));
		appointmentEntryImpl.setEntryClassPK(
			this.<Long>getColumnOriginalValue("entryClassPK"));
		appointmentEntryImpl.setEntryClassNameId(
			this.<Long>getColumnOriginalValue("entryClassNameId"));
		appointmentEntryImpl.setAppointmentId(
			this.<String>getColumnOriginalValue("appointmentId"));
		appointmentEntryImpl.setServiceId(
			this.<String>getColumnOriginalValue("serviceId"));
		appointmentEntryImpl.setConnectorClassName(
			this.<String>getColumnOriginalValue("connectorClassName"));
		appointmentEntryImpl.setCreateDate(
			this.<Date>getColumnOriginalValue("createDate"));
		appointmentEntryImpl.setAppointmentStartDate(
			this.<Date>getColumnOriginalValue("appointmentStartDate"));
		appointmentEntryImpl.setAppointmentEndDate(
			this.<Date>getColumnOriginalValue("appointmentEndDate"));

		return appointmentEntryImpl;
	}

	@Override
	public int compareTo(AppointmentEntry appointmentEntry) {
		long primaryKey = appointmentEntry.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AppointmentEntry)) {
			return false;
		}

		AppointmentEntry appointmentEntry = (AppointmentEntry)object;

		long primaryKey = appointmentEntry.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isEntityCacheEnabled() {
		return true;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isFinderCacheEnabled() {
		return true;
	}

	@Override
	public void resetOriginalValues() {
		_columnOriginalValues = Collections.emptyMap();

		_columnBitmask = 0;
	}

	@Override
	public CacheModel<AppointmentEntry> toCacheModel() {
		AppointmentEntryCacheModel appointmentEntryCacheModel =
			new AppointmentEntryCacheModel();

		appointmentEntryCacheModel.uuid = getUuid();

		String uuid = appointmentEntryCacheModel.uuid;

		if ((uuid != null) && (uuid.length() == 0)) {
			appointmentEntryCacheModel.uuid = null;
		}

		appointmentEntryCacheModel.appointmentEntryId = getAppointmentEntryId();

		appointmentEntryCacheModel.companyId = getCompanyId();

		appointmentEntryCacheModel.entryClassPK = getEntryClassPK();

		appointmentEntryCacheModel.entryClassNameId = getEntryClassNameId();

		appointmentEntryCacheModel.appointmentId = getAppointmentId();

		String appointmentId = appointmentEntryCacheModel.appointmentId;

		if ((appointmentId != null) && (appointmentId.length() == 0)) {
			appointmentEntryCacheModel.appointmentId = null;
		}

		appointmentEntryCacheModel.serviceId = getServiceId();

		String serviceId = appointmentEntryCacheModel.serviceId;

		if ((serviceId != null) && (serviceId.length() == 0)) {
			appointmentEntryCacheModel.serviceId = null;
		}

		appointmentEntryCacheModel.connectorClassName = getConnectorClassName();

		String connectorClassName =
			appointmentEntryCacheModel.connectorClassName;

		if ((connectorClassName != null) &&
			(connectorClassName.length() == 0)) {

			appointmentEntryCacheModel.connectorClassName = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			appointmentEntryCacheModel.createDate = createDate.getTime();
		}
		else {
			appointmentEntryCacheModel.createDate = Long.MIN_VALUE;
		}

		Date appointmentStartDate = getAppointmentStartDate();

		if (appointmentStartDate != null) {
			appointmentEntryCacheModel.appointmentStartDate =
				appointmentStartDate.getTime();
		}
		else {
			appointmentEntryCacheModel.appointmentStartDate = Long.MIN_VALUE;
		}

		Date appointmentEndDate = getAppointmentEndDate();

		if (appointmentEndDate != null) {
			appointmentEntryCacheModel.appointmentEndDate =
				appointmentEndDate.getTime();
		}
		else {
			appointmentEntryCacheModel.appointmentEndDate = Long.MIN_VALUE;
		}

		return appointmentEntryCacheModel;
	}

	@Override
	public String toString() {
		Map<String, Function<AppointmentEntry, Object>>
			attributeGetterFunctions = getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			(5 * attributeGetterFunctions.size()) + 2);

		sb.append("{");

		for (Map.Entry<String, Function<AppointmentEntry, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<AppointmentEntry, Object> attributeGetterFunction =
				entry.getValue();

			sb.append("\"");
			sb.append(attributeName);
			sb.append("\": ");

			Object value = attributeGetterFunction.apply(
				(AppointmentEntry)this);

			if (value == null) {
				sb.append("null");
			}
			else if (value instanceof Blob || value instanceof Date ||
					 value instanceof Map || value instanceof String) {

				sb.append(
					"\"" + StringUtil.replace(value.toString(), "\"", "'") +
						"\"");
			}
			else {
				sb.append(value);
			}

			sb.append(", ");
		}

		if (sb.index() > 1) {
			sb.setIndex(sb.index() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	private static class EscapedModelProxyProviderFunctionHolder {

		private static final Function<InvocationHandler, AppointmentEntry>
			_escapedModelProxyProviderFunction =
				ProxyUtil.getProxyProviderFunction(
					AppointmentEntry.class, ModelWrapper.class);

	}

	private String _uuid;
	private long _appointmentEntryId;
	private long _companyId;
	private long _entryClassPK;
	private long _entryClassNameId;
	private String _appointmentId;
	private String _serviceId;
	private String _connectorClassName;
	private Date _createDate;
	private Date _appointmentStartDate;
	private Date _appointmentEndDate;

	public <T> T getColumnValue(String columnName) {
		columnName = _attributeNames.getOrDefault(columnName, columnName);

		Function<AppointmentEntry, Object> function =
			AttributeGetterFunctionsHolder._attributeGetterFunctions.get(
				columnName);

		if (function == null) {
			throw new IllegalArgumentException(
				"No attribute getter function found for " + columnName);
		}

		return (T)function.apply((AppointmentEntry)this);
	}

	public <T> T getColumnOriginalValue(String columnName) {
		if (_columnOriginalValues == null) {
			return null;
		}

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		return (T)_columnOriginalValues.get(columnName);
	}

	private void _setColumnOriginalValues() {
		_columnOriginalValues = new HashMap<String, Object>();

		_columnOriginalValues.put("uuid_", _uuid);
		_columnOriginalValues.put("appointmentEntryId", _appointmentEntryId);
		_columnOriginalValues.put("companyId", _companyId);
		_columnOriginalValues.put("entryClassPK", _entryClassPK);
		_columnOriginalValues.put("entryClassNameId", _entryClassNameId);
		_columnOriginalValues.put("appointmentId", _appointmentId);
		_columnOriginalValues.put("serviceId", _serviceId);
		_columnOriginalValues.put("connectorClassName", _connectorClassName);
		_columnOriginalValues.put("createDate", _createDate);
		_columnOriginalValues.put(
			"appointmentStartDate", _appointmentStartDate);
		_columnOriginalValues.put("appointmentEndDate", _appointmentEndDate);
	}

	private static final Map<String, String> _attributeNames;

	static {
		Map<String, String> attributeNames = new HashMap<>();

		attributeNames.put("uuid_", "uuid");

		_attributeNames = Collections.unmodifiableMap(attributeNames);
	}

	private transient Map<String, Object> _columnOriginalValues;

	public static long getColumnBitmask(String columnName) {
		return _columnBitmasks.get(columnName);
	}

	private static final Map<String, Long> _columnBitmasks;

	static {
		Map<String, Long> columnBitmasks = new HashMap<>();

		columnBitmasks.put("uuid_", 1L);

		columnBitmasks.put("appointmentEntryId", 2L);

		columnBitmasks.put("companyId", 4L);

		columnBitmasks.put("entryClassPK", 8L);

		columnBitmasks.put("entryClassNameId", 16L);

		columnBitmasks.put("appointmentId", 32L);

		columnBitmasks.put("serviceId", 64L);

		columnBitmasks.put("connectorClassName", 128L);

		columnBitmasks.put("createDate", 256L);

		columnBitmasks.put("appointmentStartDate", 512L);

		columnBitmasks.put("appointmentEndDate", 1024L);

		_columnBitmasks = Collections.unmodifiableMap(columnBitmasks);
	}

	private long _columnBitmask;
	private AppointmentEntry _escapedModel;

}