package com.placecube.digitalplace.payment.heycentric.service.internal.arp;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.payment.heycentric.service.internal.HeycentricHashingService;

@RunWith(MockitoJUnitRunner.class)
public class HeycentricARPSecurityUtilTest {

	@Mock
	private HeycentricHashingService mockHeycentricHashingService;

	@InjectMocks
	private HeycentricARPSecurityUtil heycentricARPSecurityUtil;

	private static final String RETURN_URL = "http://return.url";
	private static final long USER_ID = 12345L;
	private static final String SECRET = "secret";
	private static final String EXTERNAL_REFERENCE_CODE = "externalReferenceCode";
	private static final String DIGEST = "digest";

	@Test
	public void generateDigest_WhenNoErrors_ThenReturnsDigestFromReturlUrlUserIdAndSecret() throws GeneralSecurityException {
		when(mockHeycentricHashingService.rawHashToSHA512(RETURN_URL + "+" + USER_ID + "+" + SECRET)).thenReturn(DIGEST);

		String result = heycentricARPSecurityUtil.generateDigest(RETURN_URL, USER_ID, SECRET);
		assertThat(result, equalTo(DIGEST));
	}

	@Test
	public void isValidData_WhenERCIsEqualsSidAndDigestIsEqualsPayloadDigest_ThenReturnsTrue() throws NoSuchAlgorithmException {
		when(mockHeycentricHashingService.rawHashToSHA512(EXTERNAL_REFERENCE_CODE + "+" + USER_ID + "+" + SECRET)).thenReturn(DIGEST);
		boolean result = heycentricARPSecurityUtil.isValidData(EXTERNAL_REFERENCE_CODE, EXTERNAL_REFERENCE_CODE, String.valueOf(USER_ID), SECRET, DIGEST);
		assertThat(result, equalTo(true));
	}

	@Test
	public void isValidData_WhenERCIsNotEqualsSid_ThenReturnsFalse() throws NoSuchAlgorithmException {
		boolean result = heycentricARPSecurityUtil.isValidData(EXTERNAL_REFERENCE_CODE, "another sid", String.valueOf(USER_ID), SECRET, DIGEST);
		assertThat(result, equalTo(false));

	}

	@Test
	public void isValidData_WhenERCIsEqualsSidAndPayloadDigestIsEmpty_ThenReturnsFalse() throws NoSuchAlgorithmException {
		boolean result = heycentricARPSecurityUtil.isValidData(EXTERNAL_REFERENCE_CODE, EXTERNAL_REFERENCE_CODE, String.valueOf(USER_ID), SECRET, StringPool.BLANK);
		assertThat(result, equalTo(false));
	}

	@Test
	public void isValidData_WhenERCIsEqualsSidAndDigestIsNotEqualsPayloadDigest_ThenReturnsFalse() throws NoSuchAlgorithmException {
		when(mockHeycentricHashingService.rawHashToSHA512(EXTERNAL_REFERENCE_CODE + "+" + USER_ID + "+" + SECRET)).thenReturn("another digest");
		boolean result = heycentricARPSecurityUtil.isValidData(EXTERNAL_REFERENCE_CODE, EXTERNAL_REFERENCE_CODE, String.valueOf(USER_ID), SECRET, DIGEST);
		assertThat(result, equalTo(false));
	}

	@Test(expected = NoSuchAlgorithmException.class)
	public void isValidData_WhenWhenHashGenerationFails_ThenThrowsNoSuchAlgorithmException() throws NoSuchAlgorithmException {
		when(mockHeycentricHashingService.rawHashToSHA512(EXTERNAL_REFERENCE_CODE + "+" + USER_ID + "+" + SECRET)).thenThrow(new NoSuchAlgorithmException());
		heycentricARPSecurityUtil.isValidData(EXTERNAL_REFERENCE_CODE, EXTERNAL_REFERENCE_CODE, String.valueOf(USER_ID), SECRET, DIGEST);
	}
}