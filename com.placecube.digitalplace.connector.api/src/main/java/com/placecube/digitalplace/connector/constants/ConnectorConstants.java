package com.placecube.digitalplace.connector.constants;

public class ConnectorConstants {

	public static final String CONNECTOR_DISPLAY_PRIORITY = "connector.display.priority";

	public static final String CONNECTOR_NAME = "connector.name";

	public static final String CONNECTOR_TYPE = "connector.type";

	private ConnectorConstants() {

	}
}