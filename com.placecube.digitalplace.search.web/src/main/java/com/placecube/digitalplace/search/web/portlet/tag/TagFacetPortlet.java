package com.placecube.digitalplace.search.web.portlet.tag;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.tag.configuration.TagFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.tag.service.TagConfigurationService;
import com.placecube.digitalplace.search.web.service.FacetParsingService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-search-tagfacet", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=dp-search-facet-tag dp-search-facet dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.always-display-default-configuration-icons=true", //
		"javax.portlet.init-param.config-template=/tag-filter/configuration.jsp", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/tag-filter/view.jsp", //
		"javax.portlet.name=" + PortletKeys.TAG_FILTER, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class TagFacetPortlet extends MVCPortlet {

	@Reference
	private FacetParsingService facetParsingService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Reference
	private TagConfigurationService tagConfigurationService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			Optional<TagFacetPortletInstanceConfiguration> configuration = tagConfigurationService.getValidConfiguration(themeDisplay);

			if (configuration.isPresent()) {
				TagFacetPortletInstanceConfiguration tagConfiguration = configuration.get();
				SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);
				Map<String, Integer> facetValues = searchResponse.getFacetValues(SearchFacetConstants.ASSET_TAG_NAMES);
				Set<String> selectedValues = searchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_TAGS);

				int maxTagsToShow = tagConfiguration.maxTagsToShow();
				int maxShowMoreTagsToShow = tagConfiguration.maxShowMoreTagsToShow();

				List<FacetResult> availableTags = facetParsingService.parseFacets(facetValues, selectedValues);

				renderRequest.setAttribute("availableTags", ListUtil.subList(availableTags, 0, maxTagsToShow));
				renderRequest.setAttribute("availableShowMoreTags", ListUtil.subList(availableTags, maxTagsToShow, maxTagsToShow + maxShowMoreTagsToShow));
				renderRequest.setAttribute("redirectURL", searchResponse.getEscapedSearchResultsIteratorURL());
				renderRequest.setAttribute("filterColour", tagConfiguration.filterColour());
			} else {
				renderRequest.setAttribute("invalidConfiguration", true);
			}

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}