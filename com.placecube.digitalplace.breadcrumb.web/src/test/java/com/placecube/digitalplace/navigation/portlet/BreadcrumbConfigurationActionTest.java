package com.placecube.digitalplace.navigation.portlet;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.portlet.BaseJSPSettingsConfigurationAction;
import com.liferay.portal.kernel.portlet.SettingsConfigurationAction;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.navigation.configuration.BreadcrumbPortletInstanceConfiguration;
import com.placecube.digitalplace.navigation.constants.ParamKeys;
import com.placecube.digitalplace.navigation.constants.PortletPreferenceKeys;
import com.placecube.digitalplace.navigation.service.AssetCategoryService;
import com.placecube.digitalplace.navigation.service.AssetVocabularyService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BaseJSPSettingsConfigurationAction.class, ConfigurableUtil.class, ParamUtil.class, PortalUtil.class, PropsUtil.class, SettingsConfigurationAction.class })
public class BreadcrumbConfigurationActionTest extends PowerMockito {

	private static final long GLOBAL_GROUP_ID = 2L;

	private static final long GROUP_ID = 1L;

	private static final String CATEGORY_NAV_ROOT_ENTRY_NAME = "categoryNavRootEntryName";

	private static final String CATEGORY_URL_PROPERTY_NAME = "categoryUrlPropertyName";

	private static final String BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME = "buildCategoryiesHierarchyUrlPropertyName";

	private static final String DISPLAY_STYLE = "displayStyle";

	private static final String DISPLAY_STYLE_GROUP_ID = "displayStyleGroupId";

	private static final Map<Object, Object> MOCK_ACTIVATION_PROPERTIES = Collections.emptyMap();

	private static final boolean SHOW_CURRENT_LAYOUT = true;

	private static final boolean USE_LAYOUT_URL = true;

	private static final boolean DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB = false;

	private static final boolean FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS = true;

	private static final long VOCABULARY_ID_LONG = 2L;

	private static final long VOCABULARY_ID2_LONG = 22L;

	private static final String VOCABULARY_ID_STRING = String.valueOf(VOCABULARY_ID_LONG);

	private static final String VOCABULARY_ID2_STRING = String.valueOf(VOCABULARY_ID2_LONG);

	private BreadcrumbConfigurationAction breadcrumbConfigurationAction;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private AssetCategoryService mockAssetCategoryService;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private AssetVocabularyService mockAssetVocabularyService;

	@Mock
	private BreadcrumbPortletInstanceConfiguration mockBreadcrumbConfiguration;

	@Mock
	private Company mockCompany;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletPreferences mockPreferences;

	@Before
	public void setUp() {

		mockStatic(PropsUtil.class);
		mockStatic(ConfigurableUtil.class, ParamUtil.class, PortalUtil.class);

		breadcrumbConfigurationAction = spy(new BreadcrumbConfigurationAction());
		breadcrumbConfigurationAction.assetCategoryService = mockAssetCategoryService;
		breadcrumbConfigurationAction.assetVocabularyLocalService = mockAssetVocabularyLocalService;
		breadcrumbConfigurationAction.assetVocabularyService = mockAssetVocabularyService;

	}

	@Test
	public void activate_WhenBreadcrumbConfigurationFound_ThenDoesNotThrowException() {

		when(ConfigurableUtil.createConfigurable(BreadcrumbPortletInstanceConfiguration.class, MOCK_ACTIVATION_PROPERTIES)).thenReturn(mockBreadcrumbConfiguration);

		breadcrumbConfigurationAction.activate(MOCK_ACTIVATION_PROPERTIES);

		assertTrue(true);

	}

	@Test(expected = IllegalStateException.class)
	public void activate_WhenBreadcrumbConfigurationNotFound_ThenThrowsException() {

		when(ConfigurableUtil.createConfigurable(BreadcrumbPortletInstanceConfiguration.class, MOCK_ACTIVATION_PROPERTIES)).thenReturn(null);

		breadcrumbConfigurationAction.activate(MOCK_ACTIVATION_PROPERTIES);

	}

	@Test
	public void include_WhenNoErrors_ThenAddsCategoryNavUrlCategoryPropertyNamePreferenceToRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME, mockBreadcrumbConfiguration.categoryNavUrlCategoryPropertyName()))
				.thenReturn(CATEGORY_URL_PROPERTY_NAME);

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME, CATEGORY_URL_PROPERTY_NAME);

	}

	@Test
	public void include_WhenNoErrors_ThenAddsCategoryNavBuildCategoriesHierarchyUrlPropertyNamePreferenceToRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME,
				mockBreadcrumbConfiguration.categoryNavBuildCategoriesHierarchyUrlPropertyName())).thenReturn(BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME);

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME);

	}

	@Test
	public void include_WhenNoErrors_ThenAddsCategoryNavFilterVocabIdToRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, VOCABULARY_ID_STRING)).thenReturn(VOCABULARY_ID_STRING);
		when(mockAssetVocabularyService.getVocabularyId(GROUP_ID, VOCABULARY_ID_STRING)).thenReturn(VOCABULARY_ID_LONG);

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, VOCABULARY_ID_STRING);

	}

	@Test
	public void include_WhenCategoryNavFilterVocabNameIsNotNull_ThenAddsCategoryNavUrlFilterVocabIdToRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, VOCABULARY_ID_STRING)).thenReturn(VOCABULARY_ID_STRING);
		when(mockAssetVocabularyService.getVocabularyId(GROUP_ID, VOCABULARY_ID_STRING)).thenReturn(VOCABULARY_ID2_LONG);

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, VOCABULARY_ID2_STRING);

	}

	@Test
	public void include_WhenNoErrors_ThenAddsCategoryNavRootEntryNameToRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME, CATEGORY_NAV_ROOT_ENTRY_NAME)).thenReturn(CATEGORY_NAV_ROOT_ENTRY_NAME);

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME, CATEGORY_NAV_ROOT_ENTRY_NAME);

	}

	@Test
	public void include_WhenNoErrors_ThenAddsCategoryNavShowCurrentLayoutToRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, String.valueOf(SHOW_CURRENT_LAYOUT))).thenReturn(String.valueOf(SHOW_CURRENT_LAYOUT));

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, SHOW_CURRENT_LAYOUT);

	}

	@Test
	public void include_WhenNoErrors_ThenAddsForceRootForLayoutBreadcrumbsToRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS,
				String.valueOf(FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS))).thenReturn(String.valueOf(SHOW_CURRENT_LAYOUT));

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(
				PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS,
				FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS);

	}

	@Test
	public void include_WhenNoErrors_ThenCopiesInheritedPreferencesIntoRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.DISPLAY_STYLE, StringPool.BLANK)).thenReturn(DISPLAY_STYLE);
		when(mockPreferences.getValue(PortletPreferenceKeys.DISPLAY_STYLE_GROUP_ID, StringPool.BLANK)).thenReturn(DISPLAY_STYLE_GROUP_ID);

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.DISPLAY_STYLE, DISPLAY_STYLE);
		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.DISPLAY_STYLE_GROUP_ID, DISPLAY_STYLE_GROUP_ID);

	}

	@Test
	public void include_WhenNoErrors_ThenAddsUseLayoutUrlToRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.USE_LAYOUT_URL, String.valueOf(USE_LAYOUT_URL))).thenReturn(String.valueOf(USE_LAYOUT_URL));

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.USE_LAYOUT_URL, USE_LAYOUT_URL);

	}

	@Test
	public void include_WhenNoErrors_ThenAddsDisplayOnlyOneAssetCategoryBreadcrumbToRequest() throws Exception {

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockPreferences.getValue(PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB, String.valueOf(DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB))).thenReturn(String.valueOf(DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB));

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB, DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB);

	}

	@Test
	public void include_WhenNoExceptionRetrievingGroupVocabularies_ThenAddsGroupVocabulariesToRequest() throws Exception {

		List<AssetVocabulary> mockVocabularies = Collections.emptyList();

		mockRequests();
		mockBreadcrumbConfiguration();
		suppressSuperClassIncludeMethod();

		when(mockAssetVocabularyLocalService.getGroupVocabularies(new long[]{GROUP_ID, GLOBAL_GROUP_ID})).thenReturn(mockVocabularies);

		breadcrumbConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(ParamKeys.CATEGORY_NAV_FILTER_VOCABS, mockVocabularies);

	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsCategoryNavFilterVocabIdPreference() throws Exception {

		suppressSuperClassProcessActionMethod();

		when(ParamUtil.getString(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID)).thenReturn(VOCABULARY_ID_STRING);

		breadcrumbConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(breadcrumbConfigurationAction, times(1)).setPreference(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, VOCABULARY_ID_STRING);

	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsCategoryRootNavEntryNamePreference() throws Exception {

		suppressSuperClassProcessActionMethod();

		when(ParamUtil.getString(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME)).thenReturn(CATEGORY_NAV_ROOT_ENTRY_NAME);

		breadcrumbConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(breadcrumbConfigurationAction, times(1)).setPreference(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME, CATEGORY_NAV_ROOT_ENTRY_NAME);

	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsCategoryNavUrlCategoryPropertyNamePreference() throws Exception {

		suppressSuperClassProcessActionMethod();

		when(ParamUtil.getString(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME)).thenReturn(CATEGORY_URL_PROPERTY_NAME);

		breadcrumbConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(breadcrumbConfigurationAction, times(1)).setPreference(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME, CATEGORY_URL_PROPERTY_NAME);

	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsCategoryNavBuildCategoriesHierarchyUrlPropertyNamePreference() throws Exception {

		suppressSuperClassProcessActionMethod();

		when(ParamUtil.getString(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME)).thenReturn(BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME);

		breadcrumbConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(breadcrumbConfigurationAction, times(1)).setPreference(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME,
				BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME);

	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsCategoryNavShowCurrentLayoutPreference() throws Exception {

		suppressSuperClassProcessActionMethod();

		when(ParamUtil.getBoolean(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT)).thenReturn(SHOW_CURRENT_LAYOUT);

		breadcrumbConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(breadcrumbConfigurationAction, times(1)).setPreference(mockActionRequest, PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, String.valueOf(SHOW_CURRENT_LAYOUT));

	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsUseLayoutUrlPreference() throws Exception {

		suppressSuperClassProcessActionMethod();

		when(ParamUtil.getBoolean(mockActionRequest, PortletPreferenceKeys.USE_LAYOUT_URL)).thenReturn(USE_LAYOUT_URL);

		breadcrumbConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(breadcrumbConfigurationAction, times(1)).setPreference(mockActionRequest, PortletPreferenceKeys.USE_LAYOUT_URL, String.valueOf(USE_LAYOUT_URL));

	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsForceRootForLayoutBreadcrumbsPreference() throws Exception {

		suppressSuperClassProcessActionMethod();

		when(ParamUtil.getBoolean(mockActionRequest,
				PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS))
						.thenReturn(FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS);

		breadcrumbConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(breadcrumbConfigurationAction, times(1)).setPreference(mockActionRequest,
				PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS,
				String.valueOf(FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS));

	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsDisplayOnlyOneAssetCategoryBreadcrumb() throws Exception {

		suppressSuperClassProcessActionMethod();

		when(ParamUtil.getBoolean(mockActionRequest, PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB)).thenReturn(DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB);

		breadcrumbConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(breadcrumbConfigurationAction, times(1)).setPreference(mockActionRequest, PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB, String.valueOf(DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB));

	}

	private void mockBreadcrumbConfiguration() {

		when(ConfigurableUtil.createConfigurable(BreadcrumbPortletInstanceConfiguration.class, MOCK_ACTIVATION_PROPERTIES)).thenReturn(mockBreadcrumbConfiguration);
		when(mockBreadcrumbConfiguration.categoryNavFilterVocabularyId()).thenReturn(VOCABULARY_ID_LONG);
		when(mockBreadcrumbConfiguration.categoryNavRootEntryName()).thenReturn(CATEGORY_NAV_ROOT_ENTRY_NAME);
		when(mockBreadcrumbConfiguration.categoryNavUrlCategoryPropertyName()).thenReturn(CATEGORY_URL_PROPERTY_NAME);
		when(mockBreadcrumbConfiguration.categoryNavBuildCategoriesHierarchyUrlPropertyName()).thenReturn(BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME);
		when(mockBreadcrumbConfiguration.categoryNavShowCurrentLayout()).thenReturn(SHOW_CURRENT_LAYOUT);
		when(mockBreadcrumbConfiguration.useLayoutUrl()).thenReturn(USE_LAYOUT_URL);
		when(mockBreadcrumbConfiguration.forceRootForLayoutBreadcrumbs()).thenReturn(FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS);
		when(mockBreadcrumbConfiguration.displayOnlyOneAssetCategoryBreadcrumb()).thenReturn(DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB);

		breadcrumbConfigurationAction.activate(MOCK_ACTIVATION_PROPERTIES);

	}

	private void mockRequests() throws PortalException {

		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockPortletRequest.getPreferences()).thenReturn(mockPreferences);
		when(PortalUtil.getScopeGroupId(mockHttpServletRequest)).thenReturn(GROUP_ID);
		when(PortalUtil.getCompany(mockHttpServletRequest)).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(GLOBAL_GROUP_ID);

	}

	@SuppressWarnings("rawtypes")
	private void suppressSuperClassIncludeMethod() throws Exception {

		Class[] cArg = new Class[3];
		cArg[0] = PortletConfig.class;
		cArg[1] = HttpServletRequest.class;
		cArg[2] = HttpServletResponse.class;
		Method superRenderMethod = BaseJSPSettingsConfigurationAction.class.getMethod("include", cArg);
		MemberModifier.suppress(superRenderMethod);

	}

	@SuppressWarnings("rawtypes")
	private void suppressSuperClassProcessActionMethod() throws Exception {

		Class[] cArg = new Class[3];
		cArg[0] = PortletConfig.class;
		cArg[1] = ActionRequest.class;
		cArg[2] = ActionResponse.class;
		Method superRenderMethod = SettingsConfigurationAction.class.getMethod("processAction", cArg);
		MemberModifier.suppress(superRenderMethod);

	}

}