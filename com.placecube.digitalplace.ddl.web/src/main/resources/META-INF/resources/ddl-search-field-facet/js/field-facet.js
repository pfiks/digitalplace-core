;(function () {

	Liferay.provide(window, 'updateFieldFacetSelection', function (namespace, element, baseRemoveFilterUrl, baseUpdateFilterSelectionUrl, redirectUrl) {
		var A = AUI();
		
		var id = A.one(element).get('id');
		var oldField = A.one("#" + id + "_old");
		
		var optionSelectedValue = A.one(element).val();
		
		var selectedElement = A.one(element).one("option[value=" + optionSelectedValue + "]");
		var value = selectedElement.get('value');
		var label = selectedElement.get('label');

		var updateFilterSelectionUrl = baseUpdateFilterSelectionUrl + "&" + namespace + "filterValue=" + value + "&" + namespace + "filterLabel=" + label;
   		var removeFilterUrl = baseRemoveFilterUrl + "&" + namespace + "filterValue=" + oldField.val();

		$.ajax({
				type: "POST",
				url: removeFilterUrl,
				success : function(data) {					
					$.ajax({
						type: "POST",
						url: updateFilterSelectionUrl,
						success : function(data) {
							oldField.val(optionSelectedValue);
							window.location.href = redirectUrl;
						},
						error: function(jqXHR, textStatus, errorThrown){
					   		console.info("Update filter selection call failed: " + textStatus);
						}
				  	});			
  				},
		  		error: function(jqXHR, textStatus, errorThrown){
					console.info("Clear all filters call failed: " + textStatus);
				}
		  
		});
	});
	
})()