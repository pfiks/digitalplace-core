<%@page import="com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.constants.DDLFieldFacetDisplayConstants"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="/init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<c:set var="dropDownDisplayStyle" value="<%=DDLFieldFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN %>" />
<c:set var="checkboxDisplayStyle" value="<%=DDLFieldFacetDisplayConstants.FACET_DISPLAY_STYLE_CHECKBOX %>" />
	
<liferay-frontend:edit-form action="${configurationActionURL}" method="post" name="ddlSearchListingForm" >
	
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />

	<liferay-frontend:edit-form-body>
		<div class="sheet">
			<div class="panel-group panel-group-flush">
			
				<liferay-frontend:fieldset>
					<%@ include file ="../configuration-structure-select.jspf" %>
					
					<aui:input
						label="filter-colour-label"
						name="filterColour"
						type="text"
						value="${ configuration.filterColour() }"
					/>
					
					<aui:input
						label="custom-heading"
						name="customHeading"
						type="text"
						value="${ configuration.customHeading() }"
					/>
					
					<c:choose>
						<c:when test="${not empty availableFields}">
	
							<div id="<portlet:namespace/>filterStructure_${configuration.ddmStructureId()}">
								<aui:select name="filterField" id="filterField" showEmptyOption="true" required="false" multiple="false" label="dynamic-content-listing-filterField">
									<c:forEach items="${availableFields}" var="filter">
										<aui:option label="${filter.fieldLabel}" value="${filter.facetFieldName}"
												selected="${configuration.ddmFieldName() == filter.facetFieldName}"/>
										<c:if test="${ configuration.ddmFieldName() == filter.facetFieldName }">
											<c:set var="selectedFilter" value="${ filter }" />
										</c:if>
									</c:forEach>
								</aui:select>
							</div>
					
							<c:if test="${ not empty selectedFilter }">
	
								<div id="<portlet:namespace/>preselectedValueMessage" class="alert alert-info d-none">
									<liferay-ui:message key="no-preselect-values-available-for-field"/>
								</div>
								<div id="<portlet:namespace/>preselectedValueHolder">
									<c:choose>
										<c:when test="${ selectedFilter.getType().equals('boolean') }">
											<aui:select name="preselectedValue" id="preselectedValue" showEmptyOption="true" required="false" multiple="false" label="dynamic-content-listing-preselected-value">										
												<aui:option label="true" value="true" selected="${ configuration.preselectedValue() == 'true' }"/>
												<aui:option label="false" value="false" selected="${ configuration.preselectedValue() == 'false' }"/>
											</aui:select>
										</c:when>
										<c:when test="${empty selectedFilter.getOptions()}">
											<aui:input name="preselectedValue" type="text" value="${configuration.preselectedValue() }" label="dynamic-content-listing-preselected-value" required="false" />
										</c:when>								
										<c:otherwise>
											<aui:select name="preselectedValue" id="preselectedValue" showEmptyOption="true" required="false" multiple="false" label="dynamic-content-listing-preselected-value">
												<c:forEach items="${ selectedFilter.getOptions() }" var="option">
													<aui:option label="${ option.getValue().getString(locale) }" value="${ option.getKey() }"
															selected="${ configuration.preselectedValue() == option.getKey() }"/>
												</c:forEach>
											</aui:select>
										</c:otherwise>
									</c:choose>
	
									<aui:input name="invisible" label="invisible" type="toggle-switch" value="${ configuration.invisible() }" />
																	
									<aui:input name="alphaSort" label="alpha-sort" type="toggle-switch" value="${ configuration.alphaSort() }" />
					
									<aui:select name="displayStyle" label="display-style" value="${configuration.displayStyle() }" >
										<aui:option label="${checkboxDisplayStyle }" selected="${configuration.displayStyle() eq checkboxDisplayStyle }" value="${checkboxDisplayStyle }"/>
										<aui:option label="${dropDownDisplayStyle }" selected="${configuration.displayStyle() eq dropDownDisplayStyle }" value="${dropDownDisplayStyle }"/>
									</aui:select>
									
									<aui:input name="showHeadingAsFirstDropdownOption" label="show-heading-as-first-dropdown-option" type="toggle-switch" value="${ configuration.showHeadingAsFirstDropdownOption() }" />
					
									<aui:input name="isFieldNameDDLRecordLink" label="is-field-name-ddl-record-link" type="toggle-switch" value="${ configuration.isFieldNameDDLRecordLink() }" />
									
									<c:if test="${ configuration.isFieldNameDDLRecordLink() }">
										<aui:select label="structure" name="ddmLinkStructureId" showEmptyOption="false" required="true">
											<c:forEach items="${availableStructures}" var="availableStructure">
												<aui:option value="${availableStructure.getStructureId()}" selected="${availableStructure.getStructureId() == configuration.ddmLinkStructureId()}">
													${availableStructure.getName(locale)}
												</aui:option>
											</c:forEach>
										</aui:select>
										
										<c:choose>
											<c:when test="${not empty availableLinkFields}">
												<div id="<portlet:namespace/>filterLinkStructure_${configuration.ddmLinkStructureId()}">
													<aui:select name="ddlRecordLinkFieldName" id="ddlRecordLinkFieldName" showEmptyOption="true" required="false" multiple="false" label="ddl-record-link-field-name">
														<c:forEach items="${availableLinkFields}" var="filter">
															<aui:option label="${filter.fieldLabel}" value="${filter.facetFieldName}"
																	selected="${configuration.ddlRecordLinkFieldName() == filter.facetFieldName}"/>
															<c:if test="${ configuration.ddlRecordLinkFieldName() == filter.facetFieldName }">
																<c:set var="selectedFilter" value="${ filter }" />
															</c:if>
														</c:forEach>
													</aui:select>
												</div>
											</c:when>
											<c:otherwise>
												<div class="alert alert-info">
													<liferay-ui:message key="no-filter-fields-available-for-structure"/>
												</div>										
											</c:otherwise>
										</c:choose>
									</c:if>
								</div>
							</c:if>
	
						</c:when>
						<c:otherwise>
							<div class="alert alert-info">
								<liferay-ui:message key="no-filter-fields-available-for-structure"/>
							</div>
						</c:otherwise>
					</c:choose>
	
				</liferay-frontend:fieldset>
			</div>
		</div>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" value="${not empty availableFields ? 'save' : 'refresh'}" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>
	
</liferay-frontend:edit-form>

<script>
	$('#<portlet:namespace/>filterField').change(function() {
		$('#<portlet:namespace/>preselectedValueMessage').removeClass('d-none');
		$('#<portlet:namespace/>preselectedValue').val('');
		$('#<portlet:namespace/>preselectedValueHolder').hide();
	});
</script>
