package com.placecube.digitalplace.connector.model;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class ConnectorContextTest {

	@Test
	public void new_WhenCalledWithCompanyIdAndConnectorName_ThenSetsCompanyIdAndConnectorNameProperties() {
		final long companyId = 322L;
		final String connectorName = "connectorName";

		ConnectorContext context = new ConnectorContext(companyId, connectorName);

		assertThat(context.getCompanyId(), equalTo(companyId));
		assertThat(context.getConnectorName(), equalTo(connectorName));

	}
}
