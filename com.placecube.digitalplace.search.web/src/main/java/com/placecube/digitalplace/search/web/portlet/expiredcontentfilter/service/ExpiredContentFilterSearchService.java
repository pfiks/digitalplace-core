package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.settings.LocalizedValuesMap;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.configuration.ExpiredContentFilterSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.constants.ExpiredContentFilterSearchConstants;

@Component(immediate = true, service = ExpiredContentFilterSearchService.class)
public class ExpiredContentFilterSearchService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public boolean getIncludeExpiredContentValue(SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<String> includeExpiredContentValue = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(ExpiredContentFilterSearchConstants.SHARED_SEARCH_ATTRIBUTE_NAME);
		if (includeExpiredContentValue.isPresent()) {
			return GetterUtil.getBoolean(includeExpiredContentValue.get(), false);
		}
		return false;
	}

	public String getTitle(ThemeDisplay themeDisplay, LocalizedValuesMap titleLocalizedValuesMap) {
		String title = titleLocalizedValuesMap.get(themeDisplay.getLocale());
		if (Validator.isNull(title)) {
			title = titleLocalizedValuesMap.get(themeDisplay.getSiteDefaultLocale());
		}
		return title;
	}

	public Optional<ExpiredContentFilterSearchPortletInstanceConfiguration> getValidConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		ExpiredContentFilterSearchPortletInstanceConfiguration portletInstanceConfiguration = configurationProvider
				.getPortletInstanceConfiguration(ExpiredContentFilterSearchPortletInstanceConfiguration.class, themeDisplay);

		return Validator.isNotNull(portletInstanceConfiguration.title().get(themeDisplay.getSiteDefaultLocale())) && Validator.isNotNull(portletInstanceConfiguration.dateParameterName())
				? Optional.of(portletInstanceConfiguration)
				: Optional.empty();
	}

}
