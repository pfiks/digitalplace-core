package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.constants.ExpiredContentFilterSearchConstants;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.EXPIRED_CONTENT_FILTER_SEARCH, service = ConfigurationAction.class)
public class ExpiredContentFilterSearchPortletConfigurationAction extends DefaultConfigurationAction {

	private static final String TITLE = "title";

	@Reference
	private ConfigurationProvider configurationProvider;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		ExpiredContentFilterSearchPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(ExpiredContentFilterSearchPortletInstanceConfiguration.class,
				themeDisplay);

		String titleXML = LocalizationUtil.updateLocalization(configuration.title().getValues(), StringPool.BLANK, TITLE, LocaleUtil.toLanguageId(LocaleUtil.getSiteDefault()));
		httpServletRequest.setAttribute(TITLE, titleXML);

		httpServletRequest.setAttribute(ExpiredContentFilterSearchConstants.CONFIG_DATE_PARAMETER_NAME, configuration.dateParameterName());

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		PortletPreferences preferences = actionRequest.getPreferences();

		LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, TITLE);

		String searchFields = ParamUtil.getString(actionRequest, ExpiredContentFilterSearchConstants.CONFIG_DATE_PARAMETER_NAME);
		setPreference(actionRequest, ExpiredContentFilterSearchConstants.CONFIG_DATE_PARAMETER_NAME, searchFields);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}
}
