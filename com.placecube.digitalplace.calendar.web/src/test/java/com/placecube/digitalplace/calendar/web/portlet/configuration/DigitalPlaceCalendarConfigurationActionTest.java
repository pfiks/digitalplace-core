package com.placecube.digitalplace.calendar.web.portlet.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.calendar.web.constants.DigitalPlaceCalendarConfigurationConstants;
import com.placecube.digitalplace.calendar.web.service.DigitalPlaceCalendarConfigurationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class, PropsUtil.class })
@SuppressStaticInitializationFor("com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil")
public class DigitalPlaceCalendarConfigurationActionTest extends PowerMockito {


	@InjectMocks
	private DigitalPlaceCalendarConfigurationAction digitalPlaceCalendarConfigurationAction;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private DigitalPlaceCalendarConfigurationService mockDigitalPlaceCalendarConfigurationService;

	@Mock
	private DigitalPlaceCalendarInstanceConfiguration mockDigitalPlaceCalendarInstanceConfiguration;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletPreferences mockPreferences;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void include_WhenNoError_ThenAddsConfigurationAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDigitalPlaceCalendarConfigurationService.getDigitalPlaceCalendarConfiguration(mockThemeDisplay)).thenReturn(mockDigitalPlaceCalendarInstanceConfiguration);

		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockPortletRequest.getPreferences()).thenReturn(mockPreferences);

		digitalPlaceCalendarConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("configuration", mockDigitalPlaceCalendarInstanceConfiguration);
	}

	@Test
	public void processAction_WhenNoError_ThenSetsScopePreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		digitalPlaceCalendarConfigurationAction = Mockito.spy(DigitalPlaceCalendarConfigurationAction.class);

		String scope = "scope";

		when(ParamUtil.getString(mockActionRequest, "scope", DigitalPlaceCalendarConfigurationConstants.SCOPE_GROUP)).thenReturn(scope);

		digitalPlaceCalendarConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(digitalPlaceCalendarConfigurationAction, times(1)).setPreference(mockActionRequest, "scope", scope);
	}




}
