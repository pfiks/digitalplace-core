package com.placecube.digitalplace.ddl.web.portlet.display;

import java.util.Collections;
import java.util.List;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLCategorisationService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@Component(property = { "javax.portlet.name=" + PortletKeys.DDL_DISPLAY_PORTLET, "mvc.command.name=" + MVCCommandKeys.VALIDATE_ENTRY }, service = MVCResourceCommand.class)
public class ValidateEditDDLEntryMVCResourceCommand extends BaseMVCResourceCommand {

	@Reference
	private DDLCategorisationService ddlCategorisationService;

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		DDLDisplayPortletInstanceConfiguration configuration = ddlConfigurationService.getDDLDisplayPortletInstanceConfiguration(themeDisplay);

		boolean validCategorisation = true;

		List<Long> requiredRootCategoryIds = ddlConfigurationService.getLongValuesAsList(configuration.mandatoryCategoryIds());
		List<Long> requiredVocabularyIds = ddlConfigurationService.getLongValuesAsList(configuration.mandatoryVocabularyIds());

		if (!requiredRootCategoryIds.isEmpty() || !requiredVocabularyIds.isEmpty()) {
			List<Long> selectedAssetCategoryIds = ddlConfigurationService.getValuesAsList(ParamUtil.getLongValues(resourceRequest, "selectedAssetCategoryIds", new long[0]));
			validCategorisation = !Collections.disjoint(requiredRootCategoryIds, selectedAssetCategoryIds)
					|| ddlCategorisationService.areChildCategoriesSelected(requiredVocabularyIds, requiredRootCategoryIds, selectedAssetCategoryIds);
		}

		JSONObject jsonObject = jsonFactory.createJSONObject();
		jsonObject.put("success", String.valueOf(validCategorisation));
		resourceResponse.getWriter().print(jsonObject);
	}

}
