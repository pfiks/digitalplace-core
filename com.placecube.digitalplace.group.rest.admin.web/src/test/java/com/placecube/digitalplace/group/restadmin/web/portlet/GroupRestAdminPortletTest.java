package com.placecube.digitalplace.group.restadmin.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberMatcher.methods;
import static org.powermock.api.support.membermodification.MemberModifier.suppress;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletRequestKeys;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class GroupRestAdminPortletTest {

	@InjectMocks
	private GroupRestAdminPortlet groupRestAdminPortlet;

	@Mock
	private GroupApiKeyLocalService mockGroupApiKeyLocalService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test
	public void render_WhenNoError_ThenSetsGroupApplicationKeyAndGroupApplicationSecretAsAttributes() throws IOException, PortletException {
		long groupId = 123;
		long companyId = 432;
		String applicationKey = "applicationKeyValue";
		String applicationSecret = "applicationSecretValue";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroupApiKeyLocalService.getGroupApplicationKey(companyId, groupId)).thenReturn(applicationKey);
		when(mockGroupApiKeyLocalService.getGroupApplicationSecret(companyId, groupId)).thenReturn(applicationSecret);

		groupRestAdminPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.APPLICATION_KEY, applicationKey);
		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.APPLICATION_SECRET, applicationSecret);
	}

}
