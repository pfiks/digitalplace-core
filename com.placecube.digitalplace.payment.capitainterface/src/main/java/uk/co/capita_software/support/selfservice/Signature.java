/**
 * Signature.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uk.co.capita_software.support.selfservice;

public class Signature  implements java.io.Serializable {
    private uk.co.capita_software.support.selfservice.Algorithm algorithm;

    private int hmacKeyID;

    private java.lang.String digest;

    public Signature() {
    }

    public Signature(
           uk.co.capita_software.support.selfservice.Algorithm algorithm,
           int hmacKeyID,
           java.lang.String digest) {
           this.algorithm = algorithm;
           this.hmacKeyID = hmacKeyID;
           this.digest = digest;
    }


    /**
     * Gets the algorithm value for this Signature.
     *
     * @return algorithm
     */
    public uk.co.capita_software.support.selfservice.Algorithm getAlgorithm() {
        return algorithm;
    }


    /**
     * Sets the algorithm value for this Signature.
     *
     * @param algorithm
     */
    public void setAlgorithm(uk.co.capita_software.support.selfservice.Algorithm algorithm) {
        this.algorithm = algorithm;
    }


    /**
     * Gets the hmacKeyID value for this Signature.
     *
     * @return hmacKeyID
     */
    public int getHmacKeyID() {
        return hmacKeyID;
    }


    /**
     * Sets the hmacKeyID value for this Signature.
     *
     * @param hmacKeyID
     */
    public void setHmacKeyID(int hmacKeyID) {
        this.hmacKeyID = hmacKeyID;
    }


    /**
     * Gets the digest value for this Signature.
     *
     * @return digest
     */
    public java.lang.String getDigest() {
        return digest;
    }


    /**
     * Sets the digest value for this Signature.
     *
     * @param digest
     */
    public void setDigest(java.lang.String digest) {
        this.digest = digest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Signature)) return false;
        Signature other = (Signature) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.algorithm==null && other.getAlgorithm()==null) ||
             (this.algorithm!=null &&
              this.algorithm.equals(other.getAlgorithm()))) &&
            this.hmacKeyID == other.getHmacKeyID() &&
            ((this.digest==null && other.getDigest()==null) ||
             (this.digest!=null &&
              this.digest.equals(other.getDigest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAlgorithm() != null) {
            _hashCode += getAlgorithm().hashCode();
        }
        _hashCode += getHmacKeyID();
        if (getDigest() != null) {
            _hashCode += getDigest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Signature.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "signature"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("algorithm");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "algorithm"));
        elemField.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "algorithm"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hmacKeyID");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "hmacKeyID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("digest");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "digest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

    @Override
    public String toString() {
        return "Signature [algorithm=" + algorithm + ", hmacKeyID=" + hmacKeyID
                + ", digest=" + digest + "]";
    }



}
