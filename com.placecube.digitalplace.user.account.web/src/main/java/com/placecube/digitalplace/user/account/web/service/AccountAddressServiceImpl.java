package com.placecube.digitalplace.user.account.web.service;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountAddressService;
import com.placecube.digitalplace.user.account.web.util.AccountAddressUtils;

@Component(immediate = true, service = AccountAddressService.class)
public class AccountAddressServiceImpl implements AccountAddressService {

	@Override
	public void populateContextWithExistingPrimaryAddress(AccountContext accountContext, User user) {
		List<Address> addressList = user.getAddresses();
		for (Address address : addressList) {
			if (address.isPrimary()) {
				accountContext.setFullAddress(AccountAddressUtils.getFullAddress(address, "<br>"));
			}
		}
	}
}
