package com.placecube.digitalplace.userprofile.model.impl;

import java.util.Locale;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

public class ExpandoFieldImpl implements ExpandoField {

	private final long columnId;
	private final ExpandoColumn expandoColumn;
	private final String expandoFieldKey;
	private final boolean required;

	public ExpandoFieldImpl(ExpandoColumn expandoColumn, boolean required) {
		this.expandoColumn = expandoColumn;
		expandoFieldKey = expandoColumn.getName();
		this.required = required;
		columnId = expandoColumn.getColumnId();
	}

	@Override
	public long getExpandoColumnId() {
		return columnId;
	}

	@Override
	public String getExpandoFieldKey() {
		return expandoFieldKey;
	}

	@Override
	public String getLabel(Locale locale) {
		return expandoColumn.getDisplayName(locale);
	}

	@Override
	public boolean isRequired() {
		return required;
	}

}
