package com.placecube.digitalplace.authentication.twofactor.web.service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.service.TicketLocalService;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.service.MailService;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorAuditMessage;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorAuthenticatonType;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorSessionKeys;
import com.placecube.digitalplace.authentication.twofactor.web.model.ModelFactoryBuilder;

@Component(immediate = true, service = TwoFactorEmailService.class)
public class TwoFactorEmailService {

	@Reference
	private ClassNameLocalService classNameLocalService;

	@Reference
	private CounterLocalService counterLocalService;

	@Reference
	private MailService mailService;

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	@Reference
	private TicketLocalService ticketLocalService;

	@Reference
	private TwoFactorAuditMessageService twoFactorAuditMessageService;

	@Reference
	private TwoFactorService twoFactorService;

	public Ticket addTicket(String code, User user, long expirySeconds) {
		Ticket ticket = ticketLocalService.createTicket(counterLocalService.increment());
		ticket.setKey(String.valueOf(code));
		Instant now = Instant.now().plus(expirySeconds, ChronoUnit.SECONDS);
		Date expiredDate = Date.from(now);
		ticket.setExpirationDate(expiredDate);
		ticket.setClassPK(user.getUserId());
		ticket.setCreateDate(DateUtil.newDate());
		ticket.setExtraInfo(user.getEmailAddress());
		ticket.setClassName(UserTwoFactorAuthentication.class.getName());
		ticket.setType(TwoFactorAuthenticatonType.EMAIL);
		return ticketLocalService.addTicket(ticket);
	}

	public void deleteTicket(String twoFactorAuthCode) throws PortalException {
		Ticket ticket = ticketLocalService.getTicket(twoFactorAuthCode);
		ticketLocalService.deleteTicket(ticket);
	}

	public String genereateCode() throws NoSuchAlgorithmException {
		SecureRandom sr = SecureRandom.getInstanceStrong();
		String randomCode = String.valueOf(sr.nextInt(900000) + 100000);
		if (ticketLocalService.fetchTicket(randomCode) != null) {
			genereateCode();
		}
		return randomCode;
	}

	public long getExpirySeconds(long companyId) {
		Optional<AuthenticationTwoFactorCompanyConfiguration> configuration = twoFactorService.getEnabledConfiguration(companyId);
		if (configuration.isPresent()) {
			return configuration.get().resendEmailTimeout();
		}
		return 90;
	}

	public String getMaskedEmailAddress(String email) {
		final String mask = "*****";
		final int at = email.indexOf("@");
		if (at > 2) {
			final int maskLen = Math.min(Math.max(at / 2, 2), 4);
			final int start = (at - maskLen) / 2;
			return email.substring(0, start) + mask.substring(0, maskLen) + email.substring(start + maskLen);
		}
		return email;
	}

	public boolean isValidTwoFactorEmailCode(User user, String twoFactorEmailAuthCode, HttpServletRequest httpServletRequest) throws PortalException {
		if (Validator.isNull(twoFactorEmailAuthCode)) {
			return false;
		}

		Ticket ticket = ticketLocalService.fetchTicket(twoFactorEmailAuthCode);

		if (Validator.isNotNull(ticket) && !ticket.isExpired() && ticket.getClassPK() == user.getUserId()) {
			HttpSession httpSession = httpServletRequest.getSession();
			httpSession.setAttribute(TwoFactorSessionKeys.TWO_FACTOR_VALIDATED_USER_ID, user.getUserId());
			return true;

		}
		twoFactorAuditMessageService.addAuditMessage(user, TwoFactorAuditMessage.INVALID_AUTH_CODE);
		return false;

	}

	public void removePreviousTickets(User user) {
		long classNameId = classNameLocalService.getClassNameId(UserTwoFactorAuthentication.class);
		DynamicQuery dynamicQuery = ticketLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", user.getCompanyId()));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("classNameId", classNameId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("type", TwoFactorAuthenticatonType.EMAIL));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("classPK", user.getUserId()));

		List<Ticket> tickets = ticketLocalService.dynamicQuery(dynamicQuery, QueryUtil.ALL_POS, QueryUtil.ALL_POS);

		for (Ticket ticket : tickets) {
			ticketLocalService.deleteTicket(ticket);
		}

	}

	public void sendEmail(String emailAddress, long companyId, String oneTimePassword, String toName, String portalUrl) throws PortalException, MailException {

		Optional<AuthenticationTwoFactorCompanyConfiguration> configuration = twoFactorService.getEnabledConfiguration(companyId);

		if (configuration.isPresent()) {
			String bodyText = configuration.get().authenticationEmailBody();
			Map<String, String> personalisation = new HashMap<>();
			personalisation.put("[$PORTAL_URL$]", portalUrl);
			personalisation.put("[$TO_NAME$]", toName);
			personalisation.put("[$ONE_TIME_PASSWORD$]", oneTimePassword);

			bodyText = substituteVariables(bodyText, personalisation);

			String subjectText = configuration.get().authenticationEmailSubject();
			subjectText = substituteVariables(subjectText, personalisation);
			mailService.sendEmail(companyId, emailAddress, toName, subjectText, bodyText);
		}
	}

	private String substituteVariables(String text, Map<String, String> variables) {
		for (Map.Entry<String, String> variable : variables.entrySet()) {
			text = text.replace(variable.getKey(), String.valueOf(variable.getValue()));
		}

		return text;
	}

}
