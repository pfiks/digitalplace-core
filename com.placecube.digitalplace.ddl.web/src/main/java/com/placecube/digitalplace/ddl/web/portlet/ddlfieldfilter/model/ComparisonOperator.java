package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model;

import java.util.HashMap;
import java.util.Map;

public enum ComparisonOperator {

	GTE("gte"),
	GT("gt"),
	LT("lt"),
	LTE("lte");

	private static final Map<String, ComparisonOperator> COMPARISON_OPERATORS = new HashMap<>();

	static {
		for (ComparisonOperator comparisonOperator : ComparisonOperator.values()) {
			COMPARISON_OPERATORS.put(comparisonOperator.getKey(), comparisonOperator);
		}
	}

	private String key;

	private ComparisonOperator(String key) {
		this.key = key;
	}

	public static ComparisonOperator fromKey(String key) {
		return COMPARISON_OPERATORS.get(key);
	}

	public String getKey() {
		return key;
	}
}
