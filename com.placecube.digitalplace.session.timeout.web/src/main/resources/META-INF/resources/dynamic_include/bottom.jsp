<%@page import="com.liferay.taglib.aui.ScriptTag" %>
<%@taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<aui:script sandbox="true">
	var customSessionLengthInMins = ${customSessionLengthInMins};
	var systemTimeout = ${systemTimeout};
	var isSignedIn = ${isSignedIn};

	if (customSessionLengthInMins > 0 && isSignedIn) {
		var autoExtend = false;
		var sessionTimeout = customSessionLengthInMins;
		if (customSessionLengthInMins > systemTimeout) {
			autoExtend = true;
			setTimeout(expireSession, customSessionLengthInMins * 60 * 1000);
			
			sessionTimeout = systemTimeout;
		}

		AUI().ready('liferay-session', function() {
			if (Liferay.Session) {
				Liferay.Session.set("autoExtend", autoExtend);
				Liferay.Session.set("sessionLength", sessionTimeout * 60);
				Liferay.Session.resetInterval();

				if (Liferay.Session.display) {
					Liferay.Session.display.destructor();
					Liferay.Session.display.initializer();
				}
			}
		});
	}

	function expireSession() {
		if (Liferay.Session) {
			Liferay.Session.expire();
		}
	}
</aui:script>

<%
	ScriptTag.flushScriptData(pageContext);
%>



