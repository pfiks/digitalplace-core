/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.appointment.exception.NoSuchAppointmentSlotException;
import com.placecube.digitalplace.appointment.model.AppointmentSlot;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the appointment slot service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentSlotUtil
 * @generated
 */
@ProviderType
public interface AppointmentSlotPersistence
	extends BasePersistence<AppointmentSlot> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AppointmentSlotUtil} to access the appointment slot persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the appointment slots where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByUuid(String uuid);

	/**
	 * Returns a range of all the appointment slots where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the appointment slots where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns an ordered range of all the appointment slots where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public AppointmentSlot findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
				orderByComparator)
		throws NoSuchAppointmentSlotException;

	/**
	 * Returns the first appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public AppointmentSlot fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns the last appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public AppointmentSlot findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
				orderByComparator)
		throws NoSuchAppointmentSlotException;

	/**
	 * Returns the last appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public AppointmentSlot fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns the appointment slots before and after the current appointment slot in the ordered set where uuid = &#63;.
	 *
	 * @param appointmentSlotId the primary key of the current appointment slot
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment slot
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public AppointmentSlot[] findByUuid_PrevAndNext(
			long appointmentSlotId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
				orderByComparator)
		throws NoSuchAppointmentSlotException;

	/**
	 * Removes all the appointment slots where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of appointment slots where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching appointment slots
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns an ordered range of all the appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public AppointmentSlot findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
				orderByComparator)
		throws NoSuchAppointmentSlotException;

	/**
	 * Returns the first appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public AppointmentSlot fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns the last appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public AppointmentSlot findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
				orderByComparator)
		throws NoSuchAppointmentSlotException;

	/**
	 * Returns the last appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public AppointmentSlot fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns the appointment slots before and after the current appointment slot in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param appointmentSlotId the primary key of the current appointment slot
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment slot
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public AppointmentSlot[] findByUuid_C_PrevAndNext(
			long appointmentSlotId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
				orderByComparator)
		throws NoSuchAppointmentSlotException;

	/**
	 * Removes all the appointment slots where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of appointment slots where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching appointment slots
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @return the matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId);

	/**
	 * Returns a range of all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId, int start, int end);

	/**
	 * Returns an ordered range of all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns an ordered range of all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment slots
	 */
	public java.util.List<AppointmentSlot> findByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public AppointmentSlot findByCompanyId_SlotType_ServiceId_First(
			long companyId, String slotType, String serviceId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
				orderByComparator)
		throws NoSuchAppointmentSlotException;

	/**
	 * Returns the first appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public AppointmentSlot fetchByCompanyId_SlotType_ServiceId_First(
		long companyId, String slotType, String serviceId,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns the last appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot
	 * @throws NoSuchAppointmentSlotException if a matching appointment slot could not be found
	 */
	public AppointmentSlot findByCompanyId_SlotType_ServiceId_Last(
			long companyId, String slotType, String serviceId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
				orderByComparator)
		throws NoSuchAppointmentSlotException;

	/**
	 * Returns the last appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	public AppointmentSlot fetchByCompanyId_SlotType_ServiceId_Last(
		long companyId, String slotType, String serviceId,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns the appointment slots before and after the current appointment slot in the ordered set where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param appointmentSlotId the primary key of the current appointment slot
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment slot
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public AppointmentSlot[] findByCompanyId_SlotType_ServiceId_PrevAndNext(
			long appointmentSlotId, long companyId, String slotType,
			String serviceId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
				orderByComparator)
		throws NoSuchAppointmentSlotException;

	/**
	 * Removes all the appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 */
	public void removeByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId);

	/**
	 * Returns the number of appointment slots where companyId = &#63; and slotType = &#63; and serviceId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param slotType the slot type
	 * @param serviceId the service ID
	 * @return the number of matching appointment slots
	 */
	public int countByCompanyId_SlotType_ServiceId(
		long companyId, String slotType, String serviceId);

	/**
	 * Caches the appointment slot in the entity cache if it is enabled.
	 *
	 * @param appointmentSlot the appointment slot
	 */
	public void cacheResult(AppointmentSlot appointmentSlot);

	/**
	 * Caches the appointment slots in the entity cache if it is enabled.
	 *
	 * @param appointmentSlots the appointment slots
	 */
	public void cacheResult(java.util.List<AppointmentSlot> appointmentSlots);

	/**
	 * Creates a new appointment slot with the primary key. Does not add the appointment slot to the database.
	 *
	 * @param appointmentSlotId the primary key for the new appointment slot
	 * @return the new appointment slot
	 */
	public AppointmentSlot create(long appointmentSlotId);

	/**
	 * Removes the appointment slot with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot that was removed
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public AppointmentSlot remove(long appointmentSlotId)
		throws NoSuchAppointmentSlotException;

	public AppointmentSlot updateImpl(AppointmentSlot appointmentSlot);

	/**
	 * Returns the appointment slot with the primary key or throws a <code>NoSuchAppointmentSlotException</code> if it could not be found.
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot
	 * @throws NoSuchAppointmentSlotException if a appointment slot with the primary key could not be found
	 */
	public AppointmentSlot findByPrimaryKey(long appointmentSlotId)
		throws NoSuchAppointmentSlotException;

	/**
	 * Returns the appointment slot with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot, or <code>null</code> if a appointment slot with the primary key could not be found
	 */
	public AppointmentSlot fetchByPrimaryKey(long appointmentSlotId);

	/**
	 * Returns all the appointment slots.
	 *
	 * @return the appointment slots
	 */
	public java.util.List<AppointmentSlot> findAll();

	/**
	 * Returns a range of all the appointment slots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of appointment slots
	 */
	public java.util.List<AppointmentSlot> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the appointment slots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of appointment slots
	 */
	public java.util.List<AppointmentSlot> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator);

	/**
	 * Returns an ordered range of all the appointment slots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of appointment slots
	 */
	public java.util.List<AppointmentSlot> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentSlot>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the appointment slots from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of appointment slots.
	 *
	 * @return the number of appointment slots
	 */
	public int countAll();

}