package com.placecube.digitalplace.search.web.portlet.sortby.model.impl;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.model.SortByOption;

@Component(immediate = true, service = { SortByCreateDate.class, SortByOption.class })
public class SortByCreateDate implements SortByOption {

	private static boolean DEFAULT_SORT_DIRECTION_REVERSE = true;

	@Override
	public String getId() {
		return "sortByNewest";
	}

	@Override
	public String getLabel(Locale locale) {
		return AggregatedResourceBundleUtil.get("newest", locale, "com.placecube.digitalplace.search.web");
	}

	@Override
	public Sort[] getSorts(SharedSearchContributorSettings sharedSearchContributorSettings, boolean useOppositeSortDirection) {
		boolean sortDirection = useOppositeSortDirection ? !DEFAULT_SORT_DIRECTION_REVERSE : DEFAULT_SORT_DIRECTION_REVERSE;
		return new Sort[] { SortFactoryUtil.create("createDate_sortable", sortDirection) };
	}

}
