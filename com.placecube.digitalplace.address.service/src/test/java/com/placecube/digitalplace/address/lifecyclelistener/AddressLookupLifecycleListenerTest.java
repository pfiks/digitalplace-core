package com.placecube.digitalplace.address.lifecyclelistener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Company;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.placecube.digitalplace.address.expando.UserUPRNExpandoColumnCreator;

public class AddressLookupLifecycleListenerTest extends PowerMockito {

	@InjectMocks
	private AddressLookupLifecycleListener addressLookupLifecycleListener;

	@Mock
	private UserUPRNExpandoColumnCreator mockUserUPRNExpandoColumnCreator;

	@Mock
	private Company mockCompany;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesTheExpandoColumn() throws Exception {
		addressLookupLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockUserUPRNExpandoColumnCreator, times(1)).create(mockCompany);
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void portalInstanceRegistered_WhenExceptionCreatingColumn_ThenThrowsExpandoColumnCreationException() throws Exception {
		when(mockUserUPRNExpandoColumnCreator.create(mockCompany)).thenThrow(new ExpandoColumnCreationException("msg"));

		addressLookupLifecycleListener.portalInstanceRegistered(mockCompany);

	}
}
