package com.placecube.digitalplace.override.widgettemplate.navigationmenu.lifecyclelistener;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.placecube.digitalplace.override.widgettemplate.navigationmenu.service.WidgetTemplateService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class NavigationMenuTemplatesOverrideLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(NavigationMenuTemplatesOverrideLifecycleListener.class);

	@Reference
	private WidgetTemplateService widgetTemplateService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		LOG.debug("Running NavigationMenuTemplatesOverrideLifecycleListener for companyId: " + company.getCompanyId());

		List<DDMTemplate> navigationMenuTemplates = widgetTemplateService.getNavigationMenuTemplates(company.getGroupId());

		for (DDMTemplate ddmTemplate : navigationMenuTemplates) {
			DDMTemplate templateToUpdate = (DDMTemplate) ddmTemplate.clone();
			widgetTemplateService.removeNoItemsFoundAlert(templateToUpdate);
		}
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		return;
	}
}
