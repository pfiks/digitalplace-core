package com.placecube.digitalplace.payment.govuk.service;

import static com.placecube.digitalplace.payment.constants.PaymentConstants.API_AUTH_KEY;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.payment.govuk.configuration.GovUKCompanyConfiguration;
import com.placecube.digitalplace.payment.govuk.service.internal.models.GovUKPaymentProperties;

@Component(immediate = true, service = GovUKPaymentService.class)
public class GovUKPaymentService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private GovUKAuthKeyService govUKAuthKeyService;

	public GovUKPaymentProperties getGovUKPaymentProperties(ServiceContext serviceContext) throws ConfigurationException {
		GovUKCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(GovUKCompanyConfiguration.class, serviceContext.getCompanyId());

		GovUKPaymentProperties govUKPaymentProperties = new GovUKPaymentProperties();
		govUKPaymentProperties.setAgreementCancelURL(configuration.getAgreementCancelURL());
		govUKPaymentProperties.setAgreementStatusURL(configuration.getAgreementStatusURL());
		govUKPaymentProperties.setApiAuthKey(govUKAuthKeyService.getApiAuthKey(configuration, (String) serviceContext.getAttribute(API_AUTH_KEY)));
		govUKPaymentProperties.setBaseURL(configuration.baseURL());
		govUKPaymentProperties.setCreateAgreementURL(configuration.createAgreementURL());
		govUKPaymentProperties.setCreatePaymentURL(configuration.createPaymentURL());
		govUKPaymentProperties.setPaymentStatusURL(configuration.getPaymentStatusURL());
		return govUKPaymentProperties;
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		GovUKCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(GovUKCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}
}
