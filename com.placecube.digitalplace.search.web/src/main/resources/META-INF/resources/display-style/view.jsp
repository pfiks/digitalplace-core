<%@ include file="/init.jsp"%>

<div class="row search-display-styles">
	<div class="col-md-12">
		<a href="${displayStyleListURL}" class="btn ${isDisplayListSelected ? 'display-style-selected': 'text-muted'}">
			<liferay-ui:icon icon="list" markupView="lexicon" message="list"/>
		</a>
	
		<a href="${displayStyleGridURL}" class="btn ${isDisplayListSelected ? 'text-muted': 'display-style-selected'}">
			<liferay-ui:icon icon="cards2" markupView="lexicon" message="grid"/>
		</a>
	</div>
</div>

