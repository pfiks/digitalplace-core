package com.placecube.digitalplace.search.internal.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.Optional;
import java.util.stream.Stream;

import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;
import com.placecube.digitalplace.search.internal.util.RequestUtil;
import com.placecube.digitalplace.search.internal.util.RetrievalUtil;
import com.placecube.digitalplace.search.internal.util.SearchUtil;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.model.impl.SharedSearchContributorSettingsImpl;
import com.placecube.digitalplace.search.shared.model.impl.SharedSearchResponseImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SharedSearchResponseImpl.class, SharedSearchContributorSettingsImpl.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.dao.search.SearchContainer")
public class SharedSearchServiceTest {

	private static final long COMPANY_ID = 12;

	@Mock
	private Hits mockHits;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Layout mockLayout;

	@Mock
	private Portlet mockPortlet1;

	@Mock
	private Portlet mockPortlet2;

	@Mock
	private Portlet mockPortlet3;

	@Mock
	private Optional<PortletPreferences> mockPortletPrefs1;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RequestUtil mockRequestUtil;

	@Mock
	private RetrievalUtil mockRetrievalUtil;

	@Mock
	private SearchContainer<Object> mockSearchContainer;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchUtil mockSearchUtil;

	@Mock
	private SharedSearchContributor mockSharedSearchContributor1;

	@Mock
	private SharedSearchContributor mockSharedSearchContributor2;

	@Mock
	private ServiceTrackerMap<String, SharedSearchContributor> mockSharedSearchContributors;

	@Mock
	private SharedSearchContributorSettingsImpl mockSharedSearchContributorSettingsImpl;

	@Mock
	private SharedSearchResponseImpl mockSharedSearchResponse;

	@Mock
	private SharedSearchSession mockSharedSearchSession;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SharedSearchService sharedSearchService;

	@Before
	public void activateSetup() {
		mockStatic(SharedSearchResponseImpl.class, SharedSearchContributorSettingsImpl.class);
	}

	@Test
	public void doSearch_WhenLayoutIsNotOfTypePortlet_ThenPerformsASharedSearchForContributorsOfThePortletsAndTheInstanceablePortletsAndReturnsTheSharedSearchResponse() throws SearchException {
		mockCommonSearchDetails();

		Stream<Portlet> portletsOnPage = Stream.of(mockPortlet1, mockPortlet2);
		when(mockRetrievalUtil.getAllPortletsOnLayout(mockLayout)).thenReturn(portletsOnPage);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);
		when(mockRetrievalUtil.getValidPortletsOnLayout(mockLayout, COMPANY_ID)).thenReturn(Stream.of(mockPortlet1, mockPortlet3));
		when(mockPortlet1.getPortletName()).thenReturn("portletName1");
		when(mockSharedSearchContributors.getService("portletName1")).thenReturn(mockSharedSearchContributor1);
		when(mockPortlet2.getPortletName()).thenReturn("portletName2");
		when(mockSharedSearchContributors.getService("portletName2")).thenReturn(null);
		when(mockPortlet3.getPortletName()).thenReturn("portletName3");
		when(mockSharedSearchContributors.getService("portletName3")).thenReturn(mockSharedSearchContributor2);

		SharedSearchResponse result = sharedSearchService.doSearch(mockRenderRequest, mockSharedSearchContributors);

		assertThat(result, sameInstance(mockSharedSearchResponse));

		InOrder inOrder = inOrder(mockSearchUtil, mockSharedSearchContributor1, mockSharedSearchContributor2);
		inOrder.verify(mockSharedSearchContributor1, times(1)).contribute(mockSharedSearchContributorSettingsImpl);
		inOrder.verify(mockSharedSearchContributor2, times(1)).contribute(mockSharedSearchContributorSettingsImpl);
		inOrder.verify(mockSearchUtil, times(1)).executeSearch(mockSearchContext, mockSearchContainer);
	}

	@Test
	public void doSearch_WhenLayoutIsOfTypePortlet_ThenPerformsASharedSearchForContributorsOfThePortletsAndReturnsTheSharedSearchResponse() throws SearchException {
		mockCommonSearchDetails();

		Stream<Portlet> portletsOnPage = Stream.of(mockPortlet1, mockPortlet2);
		when(mockRetrievalUtil.getAllPortletsOnLayout(mockLayout)).thenReturn(portletsOnPage);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_PORTLET);
		when(mockPortlet1.getPortletName()).thenReturn("portletName1");
		when(mockSharedSearchContributors.getService("portletName1")).thenReturn(mockSharedSearchContributor1);
		when(mockPortlet2.getPortletName()).thenReturn("portletName2");
		when(mockSharedSearchContributors.getService("portletName2")).thenReturn(null);

		SharedSearchResponse result = sharedSearchService.doSearch(mockRenderRequest, mockSharedSearchContributors);

		assertThat(result, sameInstance(mockSharedSearchResponse));

		InOrder inOrder = inOrder(mockSearchUtil, mockSharedSearchContributor1, mockSharedSearchContributorSettingsImpl);
		inOrder.verify(mockSharedSearchContributorSettingsImpl, times(1)).setPortletPreferences(mockPortletPrefs1);
		inOrder.verify(mockSharedSearchContributor1, times(1)).contribute(mockSharedSearchContributorSettingsImpl);
		inOrder.verify(mockSearchUtil, times(1)).executeSearch(mockSearchContext, mockSearchContainer);
	}

	private void mockCommonSearchDetails() throws SearchException {
		when(mockRequestUtil.getThemeDisplay(mockRenderRequest)).thenReturn(mockThemeDisplay);
		when(mockSearchUtil.getBaseSearchContext(mockThemeDisplay)).thenReturn(mockSearchContext);
		when(mockRequestUtil.getOriginalHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockRequestUtil.getOrCreateSharedSearchSession(mockHttpServletRequest, mockThemeDisplay)).thenReturn(mockSharedSearchSession);
		when(mockSearchUtil.getBaseSearchContainer(mockRenderRequest, mockHttpServletRequest, mockSharedSearchContributorSettingsImpl)).thenReturn(mockSearchContainer);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSearchUtil.executeSearch(mockSearchContext, mockSearchContainer)).thenReturn(mockHits);
		when(SharedSearchResponseImpl.init(mockHits, mockSearchContext, mockSearchContainer, mockSharedSearchSession)).thenReturn(mockSharedSearchResponse);
		when(mockRetrievalUtil.getPortletPreferences(mockPortlet1, mockThemeDisplay)).thenReturn(mockPortletPrefs1);
		when(SharedSearchContributorSettingsImpl.init(mockSearchUtil, mockThemeDisplay, mockSearchContext, mockSharedSearchSession)).thenReturn(mockSharedSearchContributorSettingsImpl);
	}
}
