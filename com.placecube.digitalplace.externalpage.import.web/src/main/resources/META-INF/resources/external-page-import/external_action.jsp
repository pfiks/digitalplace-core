<%@ include file="init.jsp"%>

<c:set var="externalList" value="${SEARCH_CONTAINER_RESULT_ROW.object}" />
	<liferay-ui:icon-menu markupView="lexicon" showWhenSingleIcon="true" >
	<portlet:actionURL name="<%=MVCCommandKeys.EXTERNAL_PAGE_DELETE %>" var="deleteExternalPageURL">
		<portlet:param name="redirect" value="${currentURL}" />
		<portlet:param name="externalPageId" value="${externalList.externalPageId}" />
	</portlet:actionURL>
	
	<portlet:renderURL var="editExternalPageURL">
	    <portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.EXTERNAL_PAGE_EDIT %>" />
		<portlet:param name="externalPageId" value="${externalList.externalPageId}" />
	</portlet:renderURL>

	<liferay-ui:icon message="edit" url="${editExternalPageURL}" />
	
	<portlet:actionURL name="<%=MVCCommandKeys.EXTERNAL_PAGE_STATUS %>" var="publishActionURL">
		<portlet:param name="externalPageId" value="${externalList.externalPageId}" />
		<portlet:param name="<%=PortletRequestKeys.STATUS%>" value="true" />
	</portlet:actionURL>
	
	<portlet:actionURL name="<%=MVCCommandKeys.EXTERNAL_PAGE_STATUS %>" var="unpublishActionURL">
		<portlet:param name="externalPageId" value="${externalList.externalPageId}" />
		<portlet:param name="<%=PortletRequestKeys.STATUS %>" value="false" />
	</portlet:actionURL>

	<c:choose>
		<c:when  test='${externalList.status eq false}'>
			<liferay-ui:icon message="publish" url="${ publishActionURL }" />
		</c:when>
		<c:otherwise>
			<liferay-ui:icon message="unpublish" url="${ unpublishActionURL }" />
		</c:otherwise>
	</c:choose>	
	
	<liferay-ui:icon message="delete" url="${deleteExternalPageURL}" />
</liferay-ui:icon-menu>