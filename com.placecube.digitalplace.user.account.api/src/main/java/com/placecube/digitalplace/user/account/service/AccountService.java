package com.placecube.digitalplace.user.account.service;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;

public interface AccountService {

	void checkAccountCreationEnabled(PortletRequest portletRequest) throws PortletException;

	void createUserAccount(PortletRequest portletRequest, AccountContext accountContext, boolean fallbackToNationalLookup) throws UserAccountException;

	void updateUserAccount(PortletRequest portletRequest, AccountContext accountContext, boolean fallbackToNationalLookup) throws UserAccountException;
}
