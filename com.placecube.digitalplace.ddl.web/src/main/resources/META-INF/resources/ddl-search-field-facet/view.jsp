<%@page import="com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.constants.DDLFieldFacetDisplayConstants"%>
<%@ include file="/init.jsp"%>

<c:choose>
	<c:when test="${invalidConfiguration}">
		<%@ include file="/invalid-configuration.jspf" %>
	</c:when>
	
	<c:when test="${isEditLayoutMode && invisible}">
		<%@ include file="/invisible-portlet-edit-mode.jspf" %>
	</c:when>
	
	<c:when test="${ not invisible }">
		<c:set var="sectionCss" value="category-filter" />
		<c:set var="availableResults" value="${availableFieldValues}" />
		<c:set var="searchKey" value="/search" />

		<c:if test="${not empty availableResults}">
			<c:set var="dropDownDisplayStyle" value="<%=DDLFieldFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN %>" />

			<c:choose>			
				<c:when test="${displayStyle eq dropDownDisplayStyle }">
					<%@ include file="field-facet-dropdown-view.jspf" %>
				</c:when>			
				<c:otherwise>
					<%@ include file="field-facet-checkbox-view.jspf" %>
				</c:otherwise>
			</c:choose>		
		</c:if>
	</c:when>
</c:choose>
