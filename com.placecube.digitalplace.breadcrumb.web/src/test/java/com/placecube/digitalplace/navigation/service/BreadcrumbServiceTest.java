package com.placecube.digitalplace.navigation.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ListMergeable;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.site.navigation.taglib.servlet.taglib.util.BreadcrumbUtil;
import com.placecube.digitalplace.navigation.constants.ConfigurationConstants;
import com.placecube.digitalplace.navigation.constants.PortletPreferenceKeys;
import com.placecube.digitalplace.navigation.model.BreadcrumbServiceContext;
import com.placecube.digitalplace.navigation.service.util.BreadcrumbServiceUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BreadcrumbUtil.class })
public class BreadcrumbServiceTest extends PowerMockito {

	private static final String BREADCRUMB_ENTRY_TITLE_1 = "breadcrumbEntryTitle1";

	private static final String BREADCRUMB_ENTRY_TITLE_2 = "breadcrumbEntryTitle2";

	private static final String BREADCRUMB_ENTRY_URL_1 = "breadcrumbEntryUrl1";

	private static final String BREADCRUMB_ENTRY_URL_2 = "breadcrumbEntryUrl2";

	private static final String CATEGORY_TITLE_1 = "categoryTitle1";

	private static final String CATEGORY_TITLE_2 = "categoryTitle2";

	private static final long CLASS_PK = 2L;

	private static final long GROUP_ID = 3;

	private static final String LAYOUT_TITLE = "layoutTitle";

	private static final String PAGE_TITLE = "pageTitle";

	private static final String BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME = "buildCategoriesHierarchyUrlPropertyName";

	private static final String CATEGORY_URL_PROPERTY_NAME = "categoryUrlPropertyName";

	private static final String SITE_LAYOUT_BREADCRUMB_ENTRY_NAME = "siteLayoutBreadcrumbEntryName";

	private static final String ASSET_CATEGORY_URL_ONE = "/categoryUrlValueOne";

	private static final String ASSET_CATEGORY_URL_TWO = "/categoryUrlValueTwo";

	private static final String LAYOUT_URL_ONE = "/layoutUrlValueOne";

	private static final String LAYOUT_URL_TWO = "/layoutUrlValueTwo";

	private static final long VOCABULARY_ID = 1L;

	@InjectMocks
	private BreadcrumbService breadcrumbService;

	@Mock
	private BreadcrumbServiceUtil mockBreadcrumbServiceUtil;

	@Mock
	private AssetCategory mockAssetCategory1;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private List<AssetCategory> mockAssetCategories;

	@Mock
	private AssetCategoryService mockAssetCategoryService;

	@Mock
	private AssetVocabularyService mockAssetVocabularyService;

	@Mock
	private BreadcrumbEntry mockBreadcrumbEntry1;

	@Mock
	private BreadcrumbEntry mockBreadcrumbEntry2;

	@Mock
	private BreadcrumbServiceContext mockBreadcrumbServiceContext;

	@Mock
	private Group mockGroup;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private Layout mockLayout;

	@Mock
	private Locale mockLocale;

	@Mock
	private ListMergeable<String> mockPageTitleListMergeable;

	@Mock
	private PortletPreferences mockPreferences;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void getCategoryBreadcrumbEntries_WhenLayoutAssetEntryIsEmpty_ThenReturnsDefaultCategoryBreadcrumbEntries() throws PortalException {

		when(mockBreadcrumbServiceContext.getLayoutAssetEntry()).thenReturn(Optional.empty());

		List<List<BreadcrumbEntry>> categoryBreadcrumbEntries = breadcrumbService.getCategoryBreadcrumbEntries(mockBreadcrumbServiceContext, Optional.of(mockBreadcrumbEntry1), mockBreadcrumbEntry2);
		List<BreadcrumbEntry> breadcrumbEntries = categoryBreadcrumbEntries.get(0);

		assertThat(breadcrumbEntries, contains(mockBreadcrumbEntry1, mockBreadcrumbEntry2));

	}

	@Test
	public void getCategoryBreadcrumbEntries_WhenLayoutAssetEntryIsPresent_ThenReturnsDefaultCategoryBreadcrumbEntries() throws PortalException {

		mockBreadcrumbServiceContext(false);
		when(mockAssetCategoryService.getCategories(mockAssetEntry, VOCABULARY_ID)).thenReturn(Collections.emptyList());

		List<List<BreadcrumbEntry>> categoryBreadcrumbEntries = breadcrumbService.getCategoryBreadcrumbEntries(mockBreadcrumbServiceContext, Optional.of(mockBreadcrumbEntry1), mockBreadcrumbEntry2);
		List<BreadcrumbEntry> breadcrumbEntries = categoryBreadcrumbEntries.get(0);

		assertThat(breadcrumbEntries, contains(mockBreadcrumbEntry1, mockBreadcrumbEntry2));

	}

	@Test
	public void getCategoryBreadcrumbEntries_WhenSiteBreadCrumbIsNotPresent_ThenDoesNotReturnSiteBreadcrumbEntry() throws PortalException {

		mockBreadcrumbServiceContext(false);
		when(mockAssetCategoryService.getCategories(mockAssetEntry, VOCABULARY_ID)).thenReturn(Collections.emptyList());

		List<List<BreadcrumbEntry>> categoryBreadcrumbEntries = breadcrumbService.getCategoryBreadcrumbEntries(mockBreadcrumbServiceContext, Optional.empty(), mockBreadcrumbEntry2);
		List<BreadcrumbEntry> breadcrumbEntries = categoryBreadcrumbEntries.get(0);

		assertThat(breadcrumbEntries, contains(mockBreadcrumbEntry2));
		assertThat(breadcrumbEntries.contains(mockBreadcrumbEntry1), equalTo(false));
	}

	@Test
	public void getCategoryBreadcrumbEntries_WhenLayoutAssetEntryWithCategoriesFromFilterVocabIsSetAndUseLayoutUrlIsFalse_ThenReturnsCategoryBreadcrumbEntries() throws PortalException {
		boolean useLayoutUrl = false;
		mockBreadcrumbServiceContext(useLayoutUrl);
		mockThemeDisplay();
		mockBreadcrumbEntries();
		mockAssetCategories();

		List<AssetCategory> mockJournalArticleCategories = new ArrayList<>();
		mockJournalArticleCategories.add(mockAssetCategory1);

		List<AssetCategory> mockCategoryHierarchy = new ArrayList<>();
		mockCategoryHierarchy.add(mockAssetCategory1);
		mockCategoryHierarchy.add(mockAssetCategory2);

		List<List<AssetCategory>> mockHierarchicalCategories = new ArrayList<>();
		mockHierarchicalCategories.add(mockCategoryHierarchy);

		when(mockAssetEntry.getClassPK()).thenReturn(CLASS_PK);
		when(mockBreadcrumbServiceContext.getLayoutAssetEntry()).thenReturn(Optional.of(mockAssetEntry));
		when(mockAssetCategoryService.getAssetCategoryUrl(mockAssetCategory1, CATEGORY_URL_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, useLayoutUrl, GROUP_ID))
				.thenReturn(ASSET_CATEGORY_URL_ONE);
		when(mockAssetCategoryService.getAssetCategoryUrl(mockAssetCategory2, CATEGORY_URL_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, useLayoutUrl, GROUP_ID))
				.thenReturn(ASSET_CATEGORY_URL_TWO);
		when(mockAssetCategoryService.getCategories(mockAssetEntry, VOCABULARY_ID)).thenReturn(mockAssetCategories);
		when(mockAssetCategoryService.getTheDeepestCategories(mockAssetCategories)).thenReturn(mockJournalArticleCategories);
		when(mockAssetVocabularyService.getVocabularyId(GROUP_ID, String.valueOf(VOCABULARY_ID))).thenReturn(VOCABULARY_ID);
		when(mockAssetCategoryService.getHierarchicalCategories(mockJournalArticleCategories, VOCABULARY_ID)).thenReturn(mockHierarchicalCategories);

		List<List<BreadcrumbEntry>> categoryBreadcrumbEntries = breadcrumbService.getCategoryBreadcrumbEntries(mockBreadcrumbServiceContext, Optional.of(mockBreadcrumbEntry1), mockBreadcrumbEntry2);

		assertThat(categoryBreadcrumbEntries.size(), equalTo(1));
		List<BreadcrumbEntry> breadcrumbEntries = categoryBreadcrumbEntries.get(0);

		assertBreadcrumbEntry(breadcrumbEntries.get(0), BREADCRUMB_ENTRY_TITLE_1, BREADCRUMB_ENTRY_URL_1);
		assertBreadcrumbEntry(breadcrumbEntries.get(1), CATEGORY_TITLE_1, ASSET_CATEGORY_URL_ONE);
		assertBreadcrumbEntry(breadcrumbEntries.get(2), CATEGORY_TITLE_2, ASSET_CATEGORY_URL_TWO);
		assertBreadcrumbEntry(breadcrumbEntries.get(3), BREADCRUMB_ENTRY_TITLE_2, BREADCRUMB_ENTRY_URL_2);

	}

	@Test
	public void getCategoryBreadcrumbEntries_WhenLayoutAssetEntryWithCategoriesFromFilterVocabIsSetAndUseLayoutUrlIsTrue_ThenReturnsCategoryLayoutBreadcrumbEntries() throws PortalException {
		boolean useLayoutUrl = true;
		mockBreadcrumbServiceContext(useLayoutUrl);
		mockThemeDisplay();
		mockBreadcrumbEntries();
		mockAssetCategories();

		List<AssetCategory> mockJournalArticleCategories = new ArrayList<>();
		mockJournalArticleCategories.add(mockAssetCategory1);

		List<AssetCategory> mockCategoryHierarchy = new ArrayList<>();
		mockCategoryHierarchy.add(mockAssetCategory1);
		mockCategoryHierarchy.add(mockAssetCategory2);

		List<List<AssetCategory>> mockHierarchicalCategories = new ArrayList<>();
		mockHierarchicalCategories.add(mockCategoryHierarchy);

		when(mockAssetEntry.getClassPK()).thenReturn(CLASS_PK);
		when(mockBreadcrumbServiceContext.getLayoutAssetEntry()).thenReturn(Optional.of(mockAssetEntry));
		when(mockAssetCategoryService.getAssetCategoryUrl(mockAssetCategory1, CATEGORY_URL_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, useLayoutUrl, GROUP_ID))
				.thenReturn(LAYOUT_URL_ONE);
		when(mockAssetCategoryService.getAssetCategoryUrl(mockAssetCategory2, CATEGORY_URL_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, useLayoutUrl, GROUP_ID))
				.thenReturn(LAYOUT_URL_TWO);
		when(mockAssetCategoryService.getCategories(mockAssetEntry, VOCABULARY_ID)).thenReturn(mockAssetCategories);
		when(mockAssetCategoryService.getTheDeepestCategories(mockAssetCategories)).thenReturn(mockJournalArticleCategories);
		when(mockAssetVocabularyService.getVocabularyId(GROUP_ID, String.valueOf(VOCABULARY_ID))).thenReturn(VOCABULARY_ID);
		when(mockAssetCategoryService.getHierarchicalCategories(mockJournalArticleCategories, VOCABULARY_ID)).thenReturn(mockHierarchicalCategories);

		List<List<BreadcrumbEntry>> categoryBreadcrumbEntries = breadcrumbService.getCategoryBreadcrumbEntries(mockBreadcrumbServiceContext, Optional.of(mockBreadcrumbEntry1), mockBreadcrumbEntry2);

		assertThat(categoryBreadcrumbEntries.size(), equalTo(1));
		List<BreadcrumbEntry> breadcrumbEntries = categoryBreadcrumbEntries.get(0);

		assertBreadcrumbEntry(breadcrumbEntries.get(0), BREADCRUMB_ENTRY_TITLE_1, BREADCRUMB_ENTRY_URL_1);
		assertBreadcrumbEntry(breadcrumbEntries.get(1), CATEGORY_TITLE_1, LAYOUT_URL_ONE);
		assertBreadcrumbEntry(breadcrumbEntries.get(2), CATEGORY_TITLE_2, LAYOUT_URL_TWO);
		assertBreadcrumbEntry(breadcrumbEntries.get(3), BREADCRUMB_ENTRY_TITLE_2, BREADCRUMB_ENTRY_URL_2);

	}

	@Test
	public void getCategoryBreadcrumbEntries_WhenDisplayOnlyOneAssetCategoryBreadcrumbIsTrue_ThenReturnsFirstCategoryBreadcrumbEntry() throws PortalException {

		boolean useLayoutUrl = false;
		mockBreadcrumbServiceContext(useLayoutUrl);
		mockThemeDisplay();
		mockBreadcrumbEntries();
		mockAssetCategories();

		List<AssetCategory> mockJournalArticleCategories = new ArrayList<>();
		mockJournalArticleCategories.add(mockAssetCategory1);
		mockJournalArticleCategories.add(mockAssetCategory2);

		List<AssetCategory> mockCategoryHierarchy = new ArrayList<>();
		mockCategoryHierarchy.add(mockAssetCategory1);
		List<AssetCategory> mockCategoryHierarchy2 = new ArrayList<>();
		mockCategoryHierarchy2.add(mockAssetCategory2);

		List<List<AssetCategory>> mockHierarchicalCategories = new ArrayList<>();
		mockHierarchicalCategories.add(mockCategoryHierarchy);
		mockHierarchicalCategories.add(mockCategoryHierarchy2);

		when(mockAssetEntry.getClassPK()).thenReturn(CLASS_PK);
		when(mockBreadcrumbServiceContext.getLayoutAssetEntry()).thenReturn(Optional.of(mockAssetEntry));
		when(mockAssetCategoryService.getAssetCategoryUrl(mockAssetCategory1, CATEGORY_URL_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, useLayoutUrl, GROUP_ID))
				.thenReturn(ASSET_CATEGORY_URL_ONE);
		when(mockAssetCategoryService.getAssetCategoryUrl(mockAssetCategory2, CATEGORY_URL_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, useLayoutUrl, GROUP_ID))
				.thenReturn(ASSET_CATEGORY_URL_TWO);
		when(mockAssetCategoryService.getCategories(mockAssetEntry, VOCABULARY_ID)).thenReturn(mockAssetCategories);
		when(mockAssetCategoryService.getTheDeepestCategories(mockAssetCategories)).thenReturn(mockJournalArticleCategories);
		when(mockAssetVocabularyService.getVocabularyId(GROUP_ID, String.valueOf(VOCABULARY_ID))).thenReturn(VOCABULARY_ID);
		when(mockAssetCategoryService.getHierarchicalCategories(mockJournalArticleCategories, VOCABULARY_ID)).thenReturn(mockHierarchicalCategories);
		when(mockBreadcrumbServiceContext.isDisplayOnlyOneAssetCategoryBreadcrumb()).thenReturn(true);

		List<List<BreadcrumbEntry>> categoryBreadcrumbEntries = breadcrumbService.getCategoryBreadcrumbEntries(mockBreadcrumbServiceContext, Optional.of(mockBreadcrumbEntry1), mockBreadcrumbEntry2);

		assertThat(categoryBreadcrumbEntries.size(), equalTo(1));
		List<BreadcrumbEntry> breadcrumbEntries = categoryBreadcrumbEntries.get(0);

		assertBreadcrumbEntry(breadcrumbEntries.get(0), BREADCRUMB_ENTRY_TITLE_1, BREADCRUMB_ENTRY_URL_1);
		assertBreadcrumbEntry(breadcrumbEntries.get(1), CATEGORY_TITLE_1, ASSET_CATEGORY_URL_ONE);

	}

	@Test
	public void getBreadcrumbServiceContext_WhenLayoutAssetEntryInScope_ThenReturnsBreadcrumbContextWithOptionalContainingJournalArticle() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		Optional<AssetEntry> layoutAssetEntry = breadcrumbServiceContext.getLayoutAssetEntry();

		assertThat(layoutAssetEntry.get(), sameInstance(mockAssetEntry));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenLayoutAssetEntryNotInScope_ThenReturnsBreadcrumbContextWithEmptyOptional() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);
		when(mockRenderRequest.getAttribute(WebKeys.LAYOUT_ASSET_ENTRY)).thenReturn(null);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		Optional<AssetEntry> layoutAssetEntry = breadcrumbServiceContext.getLayoutAssetEntry();

		assertThat(layoutAssetEntry.isPresent(), equalTo(false));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenPageTitleListNotInScope_ThenReturnsBreadcrumbContextWithLayoutPageTitle() {

		mockRenderRequest();
		mockThemeDisplay();
		when(mockLayout.getTitle(mockLocale)).thenReturn(LAYOUT_TITLE);
		when(mockRenderRequest.getAttribute(WebKeys.PAGE_TITLE)).thenReturn(null);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		String pageTitle = breadcrumbServiceContext.getPageTitle();

		assertThat(pageTitle, equalTo(LAYOUT_TITLE));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenPageTitleListIsInScope_ThenReturnsBreadcrumbContextWithPageTitleScopeValue() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		String pageTitle = breadcrumbServiceContext.getPageTitle();

		assertThat(pageTitle, equalTo(PAGE_TITLE));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenCategoryNavFilterVocabularyIdPreferenceIsSet_ThenReturnsBreadcrumbContextWithCategoryNavFilterVocabularyIdPreferenceValue() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID))
				.thenReturn(String.valueOf(VOCABULARY_ID));

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		long categoryNavFilterVocabularyId = breadcrumbServiceContext.getCategoryNavFilterVocabularyId();

		assertThat(categoryNavFilterVocabularyId, equalTo(VOCABULARY_ID));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenCategoryNavFilterVocabularyIdPreferenceNotSet_ThenReturnsBreadcrumbContextWithCategoryNavFilterVocabularyIdDefaultValue() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID))
				.thenReturn(ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		long categoryNavFilterVocabularyId = breadcrumbServiceContext.getCategoryNavFilterVocabularyId();

		assertThat(categoryNavFilterVocabularyId, equalTo(Long.valueOf(ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID)));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenCategoryUrlCategoryPropertyNamePreferenceIsSet_ThenReturnsBreadcrumbContextWithCategoryUrlCategoryPropertyNamePreferenceValue() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME, StringPool.BLANK)).thenReturn(CATEGORY_URL_PROPERTY_NAME);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		String urlCategoryPropertyName = breadcrumbServiceContext.getCategoryNavUrlCategoryPropertyName();

		assertThat(urlCategoryPropertyName, equalTo(CATEGORY_URL_PROPERTY_NAME));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenCategoryUrlCategoryPropertyNamePreferenceNotSet_ThenReturnsBreadcrumbContextWithCategoryUrlCategoryPropertyNameDefaultValue() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME, StringPool.BLANK)).thenReturn(StringPool.BLANK);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		String urlCategoryPropertyName = breadcrumbServiceContext.getCategoryNavUrlCategoryPropertyName();

		assertThat(urlCategoryPropertyName, equalTo(StringPool.BLANK));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenCategoryBuildCategoriesHierarchyUrlPropertyNamePreferenceIsSet_ThenReturnsBreadcrumbContextWithCategoryBuildCategoriesHierarchyUrlPropertyNamePreferenceValue() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, StringPool.BLANK)).thenReturn(BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		String urlCategoryPropertyName = breadcrumbServiceContext.getCategoryNavigationBuildCategoriesHierarchyUrlPropertyName();

		assertThat(urlCategoryPropertyName, equalTo(BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenCategoryBuildCategoriesHierarchyUrlPropertyNamePreferenceNotSet_ThenReturnsBreadcrumbContextWithCategoryBuildCategoriesHierarchyUrlPropertyNameDefaultValue() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, StringPool.BLANK)).thenReturn(StringPool.BLANK);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		String urlCategoryPropertyName = breadcrumbServiceContext.getCategoryNavigationBuildCategoriesHierarchyUrlPropertyName();

		assertThat(urlCategoryPropertyName, equalTo(StringPool.BLANK));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenNoErrors_ThenReturnsBreadcrumbContextWithLayout() {

		mockRenderRequest();
		mockThemeDisplay();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		Layout layout = breadcrumbServiceContext.getLayout();

		assertThat(layout, sameInstance(mockLayout));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenNoErrors_ThenReturnsBreadcrumbContextWithLocale() {

		mockRenderRequest();
		mockThemeDisplay();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		Locale locale = breadcrumbServiceContext.getLocale();

		assertThat(locale, sameInstance(mockLocale));

	}

	@Test
	public void getBreadcrumbServiceContext_WhenNoErrors_ThenReturnsBreadcrumbContextWithThemeDisplay() {

		mockRenderRequest();
		when(mockPageTitleListMergeable.mergeToString(StringPool.BLANK)).thenReturn(PAGE_TITLE);

		BreadcrumbServiceContext breadcrumbServiceContext = breadcrumbService.getBreadcrumbServiceContext(mockRenderRequest);
		ThemeDisplay themeDisplay = breadcrumbServiceContext.getThemeDisplay();

		assertThat(themeDisplay, sameInstance(mockThemeDisplay));

	}

	@Test
	public void getDisplayPageBreadcrumbEntry_WhenNoErrors_ThenReturnsDisplayPageBreadcrumbEntryWithTitleAndEmptyURL() {

		mockBreadcrumbServiceContext(false);
		mockRenderRequest();

		BreadcrumbEntry displayPageBreadcrumbEntry = breadcrumbService.getDisplayPageBreadcrumbEntry(mockBreadcrumbServiceContext);

		assertBreadcrumbEntry(displayPageBreadcrumbEntry, PAGE_TITLE, StringPool.BLANK);

	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsGuestGroup_ThenReturnsGuestGroupBreadcrumbEntry() throws Exception {

		mockBreadcrumbUtil();
		mockRenderRequest();
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(true);

		Optional<BreadcrumbEntry> breadcrumbEntry = breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		assertThat(breadcrumbEntry.isPresent(), equalTo(true));
		assertThat(breadcrumbEntry.get(), sameInstance(mockBreadcrumbEntry1));

	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsGuestGroup_ThenReturnedBreadcrumbEntryTitleIsSetToGivenEntryName() throws Exception {

		mockBreadcrumbUtil();
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(true);

		breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		verify(mockBreadcrumbEntry1, times(1)).setTitle(SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);
	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsGuestGroup_AndBreadcrumbIsNotFound_ThenReturnsEmptyOptional() throws Exception {

		mockBreadcrumbUtil();
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(true);
		when(BreadcrumbUtil.getGuestGroupBreadcrumbEntry(mockThemeDisplay)).thenReturn(null);

		Optional<BreadcrumbEntry> result = breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsGuestGroup_AndBreadcrumbIsNotFound_ThenDoesNotSetBreadcrumbEntryTitle() throws Exception {

		mockBreadcrumbUtil();
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(true);
		when(BreadcrumbUtil.getGuestGroupBreadcrumbEntry(mockThemeDisplay)).thenReturn(null);

		breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		verify(mockBreadcrumbEntry1, never()).setTitle(anyString());
		verify(mockBreadcrumbEntry2, never()).setTitle(anyString());
	}

	@Test(expected = Exception.class)
	public void getSiteLayoutBreadcrumbEntry_WhenIsGuestGroupAndBreadcrumbUtilThrowsException_ThenThrowsException() throws Exception {

		when(mockGroup.isGuest()).thenReturn(true);
		when(BreadcrumbUtil.getGuestGroupBreadcrumbEntry(mockThemeDisplay)).thenThrow(new Exception());

		breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsNotGuestGroup_AndIsGuestStagingGroup_ThenReturnsLayoutSetGroupBreadcrumbEntry() throws Exception {
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockBreadcrumbServiceUtil.isGuestStagingGroup(mockGroup)).thenReturn(true);
		when(mockBreadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay)).thenReturn(Optional.of(mockBreadcrumbEntry1));

		Optional<BreadcrumbEntry> breadcrumbEntry = breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		assertThat(breadcrumbEntry.isPresent(), equalTo(true));
		assertThat(breadcrumbEntry.get(), sameInstance(mockBreadcrumbEntry1));

	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsNotGuestGroup_AndAndIsGuestStagingGroup_ThenReturnedBreadcrumbEntryTitleIsSetToGivenEntryName() throws Exception {
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockBreadcrumbServiceUtil.isGuestStagingGroup(mockGroup)).thenReturn(true);
		when(mockBreadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay)).thenReturn(Optional.of(mockBreadcrumbEntry1));

		breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		verify(mockBreadcrumbEntry1, times(1)).setTitle(SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);
	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsNotGuestGroup_AndAndIsGuestStagingGroup_AndBreadcrumbIsNotFound_ThenDoesNotSetBreadcrumbEntryTitle() throws Exception {
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockBreadcrumbServiceUtil.isGuestStagingGroup(mockGroup)).thenReturn(true);
		when(mockBreadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay)).thenReturn(Optional.empty());

		breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		verify(mockBreadcrumbEntry1, never()).setTitle(anyString());
		verify(mockBreadcrumbEntry2, never()).setTitle(anyString());
	}

	@Test(expected = Exception.class)
	public void getSiteLayoutBreadcrumbEntry_WhenIsNotGuestGroup_AndIsGuestStagingGroup_AndBreadcrumbRetrievalThrowsException_ThenThrowsException() throws Exception {
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockBreadcrumbServiceUtil.isGuestStagingGroup(mockGroup)).thenReturn(true);
		when(mockBreadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay)).thenThrow(new PortalException());

		breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);
	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsNotGuestGroup_AndIsNotGuestStagingGroup_ThenReturnsScopeGroupBreadcrumbEntry() throws Exception {

		mockBreadcrumbUtil();
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockBreadcrumbServiceUtil.isGuestStagingGroup(mockGroup)).thenReturn(false);

		Optional<BreadcrumbEntry> breadcrumbEntry = breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		assertThat(breadcrumbEntry.isPresent(), equalTo(true));
		assertThat(breadcrumbEntry.get(), sameInstance(mockBreadcrumbEntry2));

	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsNotGuestGroup_AndIsNotGuestStagingGroup_ThenReturnedBreadcrumbEntryTitleIsSetToGivenEntryName() throws Exception {

		mockBreadcrumbUtil();
		mockThemeDisplay();
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockBreadcrumbServiceUtil.isGuestStagingGroup(mockGroup)).thenReturn(false);

		breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		verify(mockBreadcrumbEntry2, times(1)).setTitle(SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);
	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsNotGuestGroup_AndIsNotGuestStagingGroup_AndBreadcrumbIsNotFound_ThenReturnsEmptyOptional() throws Exception {

		mockBreadcrumbUtil();
		mockThemeDisplay();
		when(BreadcrumbUtil.getScopeGroupBreadcrumbEntry(mockThemeDisplay)).thenReturn(null);
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockBreadcrumbServiceUtil.isGuestStagingGroup(mockGroup)).thenReturn(false);

		Optional<BreadcrumbEntry> result = breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getSiteLayoutBreadcrumbEntry_WhenIsNotGuestGroup_AndIsNotGuestStagingGroup_AndBreadcrumbIsNotFound_ThenDoesNotSetBreadcrumbEntryTitle() throws Exception {

		mockBreadcrumbUtil();
		mockThemeDisplay();
		when(BreadcrumbUtil.getScopeGroupBreadcrumbEntry(mockThemeDisplay)).thenReturn(null);
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockBreadcrumbServiceUtil.isGuestStagingGroup(mockGroup)).thenReturn(false);

		breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

		verify(mockBreadcrumbEntry1, never()).setTitle(anyString());
		verify(mockBreadcrumbEntry2, never()).setTitle(anyString());
	}

	@Test(expected = Exception.class)
	public void getSiteLayoutBreadcrumbEntry_WhenIsNotGuestGroupAndBreadcrumbUtilThrowsException_ThenThrowsException() throws Exception {

		when(mockGroup.isGuest()).thenReturn(false);
		when(BreadcrumbUtil.getScopeGroupBreadcrumbEntry(mockThemeDisplay)).thenThrow(new Exception());

		breadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, SITE_LAYOUT_BREADCRUMB_ENTRY_NAME);

	}

	@Test
	public void addRootEntryIfNecessary_WhenForceRootEntryEnabledAndSiteLayoutEntryPresent_ThenAddsSiteEntryToBreadCrumbList() {
		List<BreadcrumbEntry> breadcrumbEntries = new ArrayList<>();
		breadcrumbEntries.add(mockBreadcrumbEntry1);
		breadcrumbEntries.add(mockBreadcrumbEntry2);

		when(mockRenderRequest.getPreferences()).thenReturn(mockPreferences);
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS,
				Boolean.FALSE.toString())).thenReturn("true");
		
		BreadcrumbEntry mockRootEntry = mock(BreadcrumbEntry.class);
		Optional<BreadcrumbEntry> entryOption = Optional.of(mockRootEntry);

		List<BreadcrumbEntry> result = breadcrumbService.addRootEntryIfNecessary(breadcrumbEntries, mockRenderRequest, entryOption);

		assertThat(result.size(), equalTo(3));
		assertThat(result.get(0), sameInstance(mockRootEntry));
	}

	@Test
	public void addRootEntryIfNecessary_WhenForceRootEntryDisabledAndSiteLayoutEntryPresent_ThenDoesNotAddSiteEntryToBreadCrumbList() {
		List<BreadcrumbEntry> breadcrumbEntries = new ArrayList<>();
		breadcrumbEntries.add(mockBreadcrumbEntry1);
		breadcrumbEntries.add(mockBreadcrumbEntry2);

		when(mockRenderRequest.getPreferences()).thenReturn(mockPreferences);
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS,
				Boolean.FALSE.toString())).thenReturn("false");
		
		BreadcrumbEntry mockRootEntry = mock(BreadcrumbEntry.class);
		Optional<BreadcrumbEntry> entryOption = Optional.of(mockRootEntry);

		List<BreadcrumbEntry> result = breadcrumbService.addRootEntryIfNecessary(breadcrumbEntries, mockRenderRequest, entryOption);

		assertThat(result.size(), equalTo(2));
		assertThat(result.get(0), sameInstance(mockBreadcrumbEntry1));
	}

	@Test
	public void addRootEntryIfNecessary_WhenForceRootEntryEnabledAndSiteLayoutEntryNotPresent_ThenDoesNotAddSiteEntryToBreadCrumbList() {
		List<BreadcrumbEntry> breadcrumbEntries = new ArrayList<>();
		breadcrumbEntries.add(mockBreadcrumbEntry1);
		breadcrumbEntries.add(mockBreadcrumbEntry2);

		when(mockRenderRequest.getPreferences()).thenReturn(mockPreferences);
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS,
				Boolean.FALSE.toString())).thenReturn("true");

		List<BreadcrumbEntry> result = breadcrumbService.addRootEntryIfNecessary(breadcrumbEntries, mockRenderRequest, Optional.empty());

		assertThat(result.size(), equalTo(2));
		assertThat(result.get(0), sameInstance(mockBreadcrumbEntry1));
	}

	@Test
	public void applyCurrentLayoutPreferenceFilter_WhenShowCurrentLayoutPreferenceIsFalseAndBreadcrumbHasMoreThanOneEntry_ThenRemovesLastEntry() {
		List<BreadcrumbEntry> breadcrumbEntries = new ArrayList<>();
		breadcrumbEntries.add(mockBreadcrumbEntry1);
		breadcrumbEntries.add(mockBreadcrumbEntry2);

		mockRenderRequest();
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, Boolean.TRUE.toString())).thenReturn("false");

		List<BreadcrumbEntry> result = breadcrumbService.applyCurrentLayoutPreferenceFilter(breadcrumbEntries, mockRenderRequest);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(0), sameInstance(mockBreadcrumbEntry1));
	}

	@Test
	public void applyCurrentLayoutPreferenceFilter_WhenShowCurrentLayoutPreferenceIsFalseAndBreadcrumbHasOneEntry_ThenDoesNotRemoveLastEntry() {
		List<BreadcrumbEntry> breadcrumbEntries = new ArrayList<>();
		breadcrumbEntries.add(mockBreadcrumbEntry1);

		mockRenderRequest();
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, Boolean.TRUE.toString())).thenReturn("false");

		List<BreadcrumbEntry> result = breadcrumbService.applyCurrentLayoutPreferenceFilter(breadcrumbEntries, mockRenderRequest);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(0), sameInstance(mockBreadcrumbEntry1));
	}

	@Test
	public void applyCurrentLayoutPreferenceFilter_WhenShowCurrentLayoutPreferenceIstrue_ThenDoesNotRemoveLastEntry() {
		List<BreadcrumbEntry> breadcrumbEntries = new ArrayList<>();
		breadcrumbEntries.add(mockBreadcrumbEntry1);
		breadcrumbEntries.add(mockBreadcrumbEntry2);

		mockRenderRequest();
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, Boolean.TRUE.toString())).thenReturn("true");

		List<BreadcrumbEntry> result = breadcrumbService.applyCurrentLayoutPreferenceFilter(breadcrumbEntries, mockRenderRequest);

		assertThat(result.size(), equalTo(2));
		assertThat(result.get(0), sameInstance(mockBreadcrumbEntry1));
		assertThat(result.get(1), sameInstance(mockBreadcrumbEntry2));
	}

	@Test
	public void applyCurrentLayoutPreferenceFilterToBreadcrumbTree_WhenShowCurrentLayoutPreferenceIsFalseAndBreadcrumbsHaveMoreThanOneEntry_ThenRemovesLastEntryFromEachBreadcrumb() {
		List<BreadcrumbEntry> breadcrumbEntriesOne = new ArrayList<>();
		breadcrumbEntriesOne.add(mockBreadcrumbEntry1);
		breadcrumbEntriesOne.add(mockBreadcrumbEntry2);

		List<BreadcrumbEntry> breadcrumbEntriesTwo = new ArrayList<>();
		breadcrumbEntriesTwo.add(mockBreadcrumbEntry2);
		breadcrumbEntriesTwo.add(mockBreadcrumbEntry1);

		List<List<BreadcrumbEntry>> breadcrumbsList = new ArrayList<>();
		breadcrumbsList.add(breadcrumbEntriesOne);
		breadcrumbsList.add(breadcrumbEntriesTwo);

		mockRenderRequest();
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, Boolean.TRUE.toString())).thenReturn("false");

		List<List<BreadcrumbEntry>> result = breadcrumbService.applyCurrentLayoutPreferenceFilterToBreadcrumbTree(breadcrumbsList, mockRenderRequest);

		assertThat(result.get(0).size(), equalTo(1));
		assertThat(result.get(0).get(0), sameInstance(mockBreadcrumbEntry1));

		assertThat(result.get(1).size(), equalTo(1));
		assertThat(result.get(1).get(0), sameInstance(mockBreadcrumbEntry2));
	}

	@Test
	public void applyCurrentLayoutPreferenceFilterToBreadcrumbTree_WhenShowCurrentLayoutPreferenceIsFalseAndBreadcrumbHasOneEntry_ThenDoesNotRemoveLastEntry() {
		List<BreadcrumbEntry> breadcrumbEntriesOne = new ArrayList<>();
		breadcrumbEntriesOne.add(mockBreadcrumbEntry1);

		List<BreadcrumbEntry> breadcrumbEntriesTwo = new ArrayList<>();
		breadcrumbEntriesTwo.add(mockBreadcrumbEntry2);

		List<List<BreadcrumbEntry>> breadcrumbsList = new ArrayList<>();
		breadcrumbsList.add(breadcrumbEntriesOne);
		breadcrumbsList.add(breadcrumbEntriesTwo);

		mockRenderRequest();
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, Boolean.TRUE.toString())).thenReturn("false");

		List<List<BreadcrumbEntry>> result = breadcrumbService.applyCurrentLayoutPreferenceFilterToBreadcrumbTree(breadcrumbsList, mockRenderRequest);

		assertThat(result.get(0).size(), equalTo(1));
		assertThat(result.get(0).get(0), sameInstance(mockBreadcrumbEntry1));
		assertThat(result.get(1).size(), equalTo(1));
		assertThat(result.get(1).get(0), sameInstance(mockBreadcrumbEntry2));
	}

	@Test
	public void applyCurrentLayoutPreferenceFilterToBreadcrumbTree_WhenShowCurrentLayoutPreferenceIstrue_ThenDoesNotRemoveLastEntry() {
		List<BreadcrumbEntry> breadcrumbEntriesOne = new ArrayList<>();
		breadcrumbEntriesOne.add(mockBreadcrumbEntry1);
		breadcrumbEntriesOne.add(mockBreadcrumbEntry2);

		List<BreadcrumbEntry> breadcrumbEntriesTwo = new ArrayList<>();
		breadcrumbEntriesTwo.add(mockBreadcrumbEntry2);
		breadcrumbEntriesTwo.add(mockBreadcrumbEntry1);

		List<List<BreadcrumbEntry>> breadcrumbsList = new ArrayList<>();
		breadcrumbsList.add(breadcrumbEntriesOne);
		breadcrumbsList.add(breadcrumbEntriesTwo);

		mockRenderRequest();
		when(mockPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, Boolean.TRUE.toString())).thenReturn("true");

		List<List<BreadcrumbEntry>> result = breadcrumbService.applyCurrentLayoutPreferenceFilterToBreadcrumbTree(breadcrumbsList, mockRenderRequest);

		assertThat(result.get(0).size(), equalTo(2));
		assertThat(result.get(0).get(0), sameInstance(mockBreadcrumbEntry1));
		assertThat(result.get(0).get(1), sameInstance(mockBreadcrumbEntry2));

		assertThat(result.get(1).size(), equalTo(2));
		assertThat(result.get(1).get(0), sameInstance(mockBreadcrumbEntry2));
		assertThat(result.get(1).get(1), sameInstance(mockBreadcrumbEntry1));

	}

	private void assertBreadcrumbEntry(BreadcrumbEntry breadcrumbEntry, String expectedTitle, String expectedUrl) {

		assertThat(breadcrumbEntry.getTitle(), equalTo(expectedTitle));
		assertThat(breadcrumbEntry.getURL(), equalTo(expectedUrl));

	}

	private void mockAssetCategories() {

		when(mockAssetCategory1.getTitle(mockLocale)).thenReturn(CATEGORY_TITLE_1);
		when(mockAssetCategory2.getTitle(mockLocale)).thenReturn(CATEGORY_TITLE_2);

	}

	private void mockBreadcrumbEntries() {

		when(mockBreadcrumbEntry1.getTitle()).thenReturn(BREADCRUMB_ENTRY_TITLE_1);
		when(mockBreadcrumbEntry1.getURL()).thenReturn(BREADCRUMB_ENTRY_URL_1);
		when(mockBreadcrumbEntry2.getTitle()).thenReturn(BREADCRUMB_ENTRY_TITLE_2);
		when(mockBreadcrumbEntry2.getURL()).thenReturn(BREADCRUMB_ENTRY_URL_2);

	}

	private void mockBreadcrumbServiceContext(boolean useLayoutUrl) {

		when(mockBreadcrumbServiceContext.getCategoryNavFilterVocabularyId()).thenReturn(VOCABULARY_ID);
		when(mockBreadcrumbServiceContext.getCategoryNavUrlCategoryPropertyName()).thenReturn(CATEGORY_URL_PROPERTY_NAME);
		when(mockBreadcrumbServiceContext.getCategoryNavigationBuildCategoriesHierarchyUrlPropertyName()).thenReturn(BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME);
		when(mockBreadcrumbServiceContext.getLayoutAssetEntry()).thenReturn(Optional.of(mockAssetEntry));
		when(mockBreadcrumbServiceContext.getLocale()).thenReturn(mockLocale);
		when(mockBreadcrumbServiceContext.getPageTitle()).thenReturn(PAGE_TITLE);
		when(mockBreadcrumbServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockBreadcrumbServiceContext.isUseLayoutUrl()).thenReturn(useLayoutUrl);

	}

	private void mockBreadcrumbUtil() throws Exception {

		mockStatic(BreadcrumbUtil.class);

		when(BreadcrumbUtil.getGuestGroupBreadcrumbEntry(mockThemeDisplay)).thenReturn(mockBreadcrumbEntry1);
		when(BreadcrumbUtil.getScopeGroupBreadcrumbEntry(mockThemeDisplay)).thenReturn(mockBreadcrumbEntry2);

	}

	private void mockRenderRequest() {

		when(mockRenderRequest.getAttribute(WebKeys.LAYOUT_ASSET_ENTRY)).thenReturn(mockAssetEntry);
		when(mockRenderRequest.getAttribute(WebKeys.PAGE_TITLE)).thenReturn(mockPageTitleListMergeable);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockRenderRequest.getPreferences()).thenReturn(mockPreferences);

	}

	private void mockThemeDisplay() {

		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockThemeDisplay.getLocale()).thenReturn(mockLocale);

	}

}
