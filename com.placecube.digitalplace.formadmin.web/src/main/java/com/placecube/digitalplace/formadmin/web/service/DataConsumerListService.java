package com.placecube.digitalplace.formadmin.web.service;

import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.model.DataProviderInputParameter;
import com.placecube.digitalplace.dataconsumer.model.DataProviderOutputParameter;
import com.placecube.digitalplace.dataconsumer.service.DataConsumerLocalService;
import com.placecube.digitalplace.dataconsumer.service.DataTransformationService;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = DataConsumerListService.class)
public class DataConsumerListService {

	private static final String PARAM_PREFIX = "p_";
	@Reference
	private DataConsumerLocalService dataConsumerLocalService;

	@Reference
	private DataTransformationService dataTransformationService;

	@Reference
	private DDMDataProviderInstanceService ddmDataProviderInstanceService;

	@Reference
	private DDMFormInstanceService ddmFormInstanceService;

	public void createDataConsumer(String headersJson, String parameterMapping, PortletRequest portletRequest) {

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		DataConsumer dc = dataConsumerLocalService.createDataConsumer();

		dc.setCompanyId(themeDisplay.getCompanyId());
		dc.setGroupId(themeDisplay.getScopeGroupId());
		dc.setDdmFormInstanceId(ParamUtil.getLong(portletRequest, PortletRequestKeys.FORM_INSTANCE_ID));
		dc.setDdmDataProviderInstanceId(ParamUtil.getLong(portletRequest, PortletRequestKeys.DATA_PROVIDER_INSTANCE_ID));
		dc.setMethod(ParamUtil.getString(portletRequest, PortletRequestKeys.METHOD));
		dc.setMapping(parameterMapping);
		dc.setHeaders(headersJson);
		dc.setChain(ParamUtil.getInteger(portletRequest, PortletRequestKeys.CHAIN));
		dc.setSourceDataProviderId(ParamUtil.getLong(portletRequest, PortletRequestKeys.SOURCE_DATA_PROVIDER_ID));
		dc.setBody(ParamUtil.getString(portletRequest, PortletRequestKeys.REQUEST_BODY));

		dataConsumerLocalService.addDataConsumer(dc);
	}

	public String extractHeadersAsJson(ActionRequest actionRequest) {
		String[] headerName = ParamUtil.getStringValues(actionRequest, PortletRequestKeys.HEADER_NAME);
		String[] headerValue = ParamUtil.getStringValues(actionRequest, PortletRequestKeys.HEADER_VALUE);

		Map<String, String> headers = new HashMap<>();
		for (int i = 0; i < headerValue.length; i++) {
			headers.put(headerName[i], headerValue[i]);
		}
		return dataTransformationService.serializeMapping(headers);
	}

	public String extractParameterMappingAsJson(ActionRequest actionRequest) {
		Map<String, String> paramMap = new HashMap<>();

		actionRequest.getActionParameters().getNames().stream().filter(k -> k.startsWith(PARAM_PREFIX))
				.forEach(k -> paramMap.put(k.substring(PARAM_PREFIX.length()), ParamUtil.getString(actionRequest, k)));

		return dataTransformationService.serializeMapping(paramMap);
	}

	public String getDataConsumerBody(Optional<DataConsumer> dataConsumer, List<DataProviderInputParameter> inputFields) {
		return dataConsumer.map(DataConsumer::getBody).orElse(dataTransformationService.getDefaultInputParametersJSONMapping(inputFields));
	}

	public String getFormInstanceName(long formInstanceId, Locale locale) {
		String formInstanceName = StringPool.BLANK;

		try {
			DDMFormInstance ddmFormInstance = ddmFormInstanceService.getFormInstance(formInstanceId);
			formInstanceName = ddmFormInstance.getName(locale);
		} catch (PortalException pe) {
			pe.printStackTrace();
		}

		return formInstanceName;
	}

	public List<DataProviderOutputParameter> getOutputParametersForDataProviderId(long sourceDataProviderId, Locale locale) throws PortalException {
		DDMDataProviderInstance sourceProviderInstance = ddmDataProviderInstanceService.getDataProviderInstance(sourceDataProviderId);
		DDMFormValues sourceProviderFields = dataTransformationService.getDataProviderFormValues(sourceProviderInstance);
		return dataTransformationService.getOutputParameters(sourceProviderFields, locale);
	}

	public void updateDataConsumerDetails(long dataConsumerId, String headersJson, String parameterMapping, PortletRequest portletRequest) throws PortalException {

		DataConsumer dc = dataConsumerLocalService.getDataConsumer(dataConsumerId);

		dc.setHeaders(headersJson);
		dc.setDdmDataProviderInstanceId(ParamUtil.getLong(portletRequest, PortletRequestKeys.DATA_PROVIDER_INSTANCE_ID));
		dc.setMethod(ParamUtil.getString(portletRequest, PortletRequestKeys.METHOD));
		dc.setMapping(parameterMapping);
		dc.setBody(ParamUtil.getString(portletRequest, PortletRequestKeys.REQUEST_BODY));

		dataConsumerLocalService.updateDataConsumer(dc);
	}

}
