package com.placecube.digitalplace.externalpage.web.constants;

public class ViewConstants {

	public static final String SEARCH_CONTAINER_ID = "externalPageEntries";
	public static final String DELETE_ENTRIES_ACTION = "deleteEntries";

	private ViewConstants() {

	}
}
