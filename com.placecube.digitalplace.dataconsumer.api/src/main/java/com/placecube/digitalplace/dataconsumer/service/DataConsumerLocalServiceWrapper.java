/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.dataconsumer.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link DataConsumerLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DataConsumerLocalService
 * @generated
 */
public class DataConsumerLocalServiceWrapper
	implements DataConsumerLocalService,
			   ServiceWrapper<DataConsumerLocalService> {

	public DataConsumerLocalServiceWrapper() {
		this(null);
	}

	public DataConsumerLocalServiceWrapper(
		DataConsumerLocalService dataConsumerLocalService) {

		_dataConsumerLocalService = dataConsumerLocalService;
	}

	/**
	 * Adds the data consumer to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DataConsumerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dataConsumer the data consumer
	 * @return the data consumer that was added
	 */
	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
		addDataConsumer(
			com.placecube.digitalplace.dataconsumer.model.DataConsumer
				dataConsumer) {

		return _dataConsumerLocalService.addDataConsumer(dataConsumer);
	}

	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
		createDataConsumer() {

		return _dataConsumerLocalService.createDataConsumer();
	}

	/**
	 * Creates a new data consumer with the primary key. Does not add the data consumer to the database.
	 *
	 * @param dataConsumerId the primary key for the new data consumer
	 * @return the new data consumer
	 */
	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
		createDataConsumer(long dataConsumerId) {

		return _dataConsumerLocalService.createDataConsumer(dataConsumerId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dataConsumerLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the data consumer from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DataConsumerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dataConsumer the data consumer
	 * @return the data consumer that was removed
	 */
	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
		deleteDataConsumer(
			com.placecube.digitalplace.dataconsumer.model.DataConsumer
				dataConsumer) {

		return _dataConsumerLocalService.deleteDataConsumer(dataConsumer);
	}

	/**
	 * Deletes the data consumer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DataConsumerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer that was removed
	 * @throws PortalException if a data consumer with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
			deleteDataConsumer(long dataConsumerId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dataConsumerLocalService.deleteDataConsumer(dataConsumerId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dataConsumerLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _dataConsumerLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _dataConsumerLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _dataConsumerLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _dataConsumerLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.dataconsumer.model.impl.DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _dataConsumerLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.dataconsumer.model.impl.DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _dataConsumerLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _dataConsumerLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _dataConsumerLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
		fetchDataConsumer(long dataConsumerId) {

		return _dataConsumerLocalService.fetchDataConsumer(dataConsumerId);
	}

	/**
	 * Returns the data consumer matching the UUID and group.
	 *
	 * @param uuid the data consumer's UUID
	 * @param groupId the primary key of the group
	 * @return the matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
		fetchDataConsumerByUuidAndGroupId(String uuid, long groupId) {

		return _dataConsumerLocalService.fetchDataConsumerByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _dataConsumerLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the data consumer with the primary key.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer
	 * @throws PortalException if a data consumer with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
			getDataConsumer(long dataConsumerId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dataConsumerLocalService.getDataConsumer(dataConsumerId);
	}

	/**
	 * Returns the data consumer matching the UUID and group.
	 *
	 * @param uuid the data consumer's UUID
	 * @param groupId the primary key of the group
	 * @return the matching data consumer
	 * @throws PortalException if a matching data consumer could not be found
	 */
	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
			getDataConsumerByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dataConsumerLocalService.getDataConsumerByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.dataconsumer.model.impl.DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of data consumers
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.dataconsumer.model.DataConsumer>
			getDataConsumers(int start, int end) {

		return _dataConsumerLocalService.getDataConsumers(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.dataconsumer.model.DataConsumer>
			getDataConsumersByCompanyIdAndGroupId(
				long companyId, long groupId) {

		return _dataConsumerLocalService.getDataConsumersByCompanyIdAndGroupId(
			companyId, groupId);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.dataconsumer.model.DataConsumer>
			getDataConsumersByDDMFormInstanceId(long ddmFormInstanceId) {

		return _dataConsumerLocalService.getDataConsumersByDDMFormInstanceId(
			ddmFormInstanceId);
	}

	/**
	 * Returns all the data consumers matching the UUID and company.
	 *
	 * @param uuid the UUID of the data consumers
	 * @param companyId the primary key of the company
	 * @return the matching data consumers, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.dataconsumer.model.DataConsumer>
			getDataConsumersByUuidAndCompanyId(String uuid, long companyId) {

		return _dataConsumerLocalService.getDataConsumersByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of data consumers matching the UUID and company.
	 *
	 * @param uuid the UUID of the data consumers
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching data consumers, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.dataconsumer.model.DataConsumer>
			getDataConsumersByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.dataconsumer.model.DataConsumer>
						orderByComparator) {

		return _dataConsumerLocalService.getDataConsumersByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of data consumers.
	 *
	 * @return the number of data consumers
	 */
	@Override
	public int getDataConsumersCount() {
		return _dataConsumerLocalService.getDataConsumersCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _dataConsumerLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _dataConsumerLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _dataConsumerLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dataConsumerLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the data consumer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DataConsumerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dataConsumer the data consumer
	 * @return the data consumer that was updated
	 */
	@Override
	public com.placecube.digitalplace.dataconsumer.model.DataConsumer
		updateDataConsumer(
			com.placecube.digitalplace.dataconsumer.model.DataConsumer
				dataConsumer) {

		return _dataConsumerLocalService.updateDataConsumer(dataConsumer);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _dataConsumerLocalService.getBasePersistence();
	}

	@Override
	public DataConsumerLocalService getWrappedService() {
		return _dataConsumerLocalService;
	}

	@Override
	public void setWrappedService(
		DataConsumerLocalService dataConsumerLocalService) {

		_dataConsumerLocalService = dataConsumerLocalService;
	}

	private DataConsumerLocalService _dataConsumerLocalService;

}