package com.placecube.digitalplace.group.restadmin.web.constants;

public final class ViewKeys {

	public static final String VIEW_GROUP_REST_ADMIN_JSP = "/group-rest-admin/view.jsp";

	public static final String VIEW_GROUP_WEBHOOK_JSP = "/group-webhooks/view.jsp";

	public static final String UPDATE_GROUP_WEBHOOK_JSP = "/group-webhooks/update.jsp";

	private ViewKeys() {
	}

}
