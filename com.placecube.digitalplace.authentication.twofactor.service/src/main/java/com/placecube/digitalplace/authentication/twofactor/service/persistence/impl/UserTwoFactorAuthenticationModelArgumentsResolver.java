/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.authentication.twofactor.service.persistence.impl;

import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.model.BaseModel;

import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthenticationTable;
import com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationImpl;
import com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.service.component.annotations.Component;

/**
 * The arguments resolver class for retrieving value from UserTwoFactorAuthentication.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(
	property = {
		"class.name=com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationImpl",
		"table.name=Placecube_Auth_TwoFactor_UserTwoFactorAuthentication"
	},
	service = ArgumentsResolver.class
)
public class UserTwoFactorAuthenticationModelArgumentsResolver
	implements ArgumentsResolver {

	@Override
	public Object[] getArguments(
		FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
		boolean original) {

		String[] columnNames = finderPath.getColumnNames();

		if ((columnNames == null) || (columnNames.length == 0)) {
			if (baseModel.isNew()) {
				return new Object[0];
			}

			return null;
		}

		UserTwoFactorAuthenticationModelImpl
			userTwoFactorAuthenticationModelImpl =
				(UserTwoFactorAuthenticationModelImpl)baseModel;

		long columnBitmask =
			userTwoFactorAuthenticationModelImpl.getColumnBitmask();

		if (!checkColumn || (columnBitmask == 0)) {
			return _getValue(
				userTwoFactorAuthenticationModelImpl, columnNames, original);
		}

		Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
			finderPath);

		if (finderPathColumnBitmask == null) {
			finderPathColumnBitmask = 0L;

			for (String columnName : columnNames) {
				finderPathColumnBitmask |=
					userTwoFactorAuthenticationModelImpl.getColumnBitmask(
						columnName);
			}

			_finderPathColumnBitmasksCache.put(
				finderPath, finderPathColumnBitmask);
		}

		if ((columnBitmask & finderPathColumnBitmask) != 0) {
			return _getValue(
				userTwoFactorAuthenticationModelImpl, columnNames, original);
		}

		return null;
	}

	@Override
	public String getClassName() {
		return UserTwoFactorAuthenticationImpl.class.getName();
	}

	@Override
	public String getTableName() {
		return UserTwoFactorAuthenticationTable.INSTANCE.getTableName();
	}

	private static Object[] _getValue(
		UserTwoFactorAuthenticationModelImpl
			userTwoFactorAuthenticationModelImpl,
		String[] columnNames, boolean original) {

		Object[] arguments = new Object[columnNames.length];

		for (int i = 0; i < arguments.length; i++) {
			String columnName = columnNames[i];

			if (original) {
				arguments[i] =
					userTwoFactorAuthenticationModelImpl.getColumnOriginalValue(
						columnName);
			}
			else {
				arguments[i] =
					userTwoFactorAuthenticationModelImpl.getColumnValue(
						columnName);
			}
		}

		return arguments;
	}

	private static final Map<FinderPath, Long> _finderPathColumnBitmasksCache =
		new ConcurrentHashMap<>();

}