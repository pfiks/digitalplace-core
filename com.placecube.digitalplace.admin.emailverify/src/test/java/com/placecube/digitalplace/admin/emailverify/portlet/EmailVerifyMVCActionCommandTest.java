package com.placecube.digitalplace.admin.emailverify.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.admin.emailverify.service.EmailVerifyTicketService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SessionErrors.class, PortalUtil.class, SessionMessages.class })
public class EmailVerifyMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private EmailVerifyTicketService mockEmailVerifyTicketService;

	@Mock
	private LiferayPortletResponse mockLiferayPortletResponse;

	@Mock
	private LiferayPortletURL mockLiferayPortletURL;

	@Mock
	private Portal mockPortal;

	@InjectMocks
	private EmailVerifyMVCActionCommand emailVerifyMVCActionCommand;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenVerifiesEmailAddressForTheUser() throws Exception {
		long selectedUserId = 123;
		long companyId = 456;
		when(ParamUtil.getLong(mockActionRequest, "selectedUserId")).thenReturn(selectedUserId);
		when(mockPortal.getCompanyId(mockActionRequest)).thenReturn(companyId);
		when(mockPortal.getLiferayPortletResponse(mockActionResponse)).thenReturn(mockLiferayPortletResponse);
		when(mockLiferayPortletResponse.createLiferayPortletURL(PortletKeys.USERS_ADMIN, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);

		emailVerifyMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockEmailVerifyTicketService, times(1)).verifyUserEmailAddress(companyId, selectedUserId);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenSendsRedirectToTheUsersAdminPortlet() throws Exception {
		long selectedUserId = 123;
		long companyId = 456;
		String redirect = "redirectValue";
		when(ParamUtil.getLong(mockActionRequest, "selectedUserId")).thenReturn(selectedUserId);
		when(mockPortal.getCompanyId(mockActionRequest)).thenReturn(companyId);
		when(mockPortal.getLiferayPortletResponse(mockActionResponse)).thenReturn(mockLiferayPortletResponse);
		when(mockLiferayPortletResponse.createLiferayPortletURL(PortletKeys.USERS_ADMIN, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.toString()).thenReturn(redirect);

		emailVerifyMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionResponse, times(1)).sendRedirect(redirect);
	}
}
