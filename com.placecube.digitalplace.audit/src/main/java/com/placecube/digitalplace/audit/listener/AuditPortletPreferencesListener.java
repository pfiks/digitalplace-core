package com.placecube.digitalplace.audit.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.PortletPreferences;
import com.liferay.portal.security.audit.event.generators.constants.EventTypes;
import com.placecube.digitalplace.audit.service.AuditService;

@Component(immediate = true, service = ModelListener.class)
public class AuditPortletPreferencesListener extends BaseModelListener<PortletPreferences> {

	@Reference
	private AuditService auditService;

	@Override
	public void onAfterCreate(PortletPreferences portletPreferences) throws ModelListenerException {
		addAuditEntry(portletPreferences, EventTypes.ADD);
	}

	@Override
	public void onAfterRemove(PortletPreferences portletPreferences) {
		addAuditEntry(portletPreferences, EventTypes.DELETE);
	}

	@Override
	public void onAfterUpdate(PortletPreferences previousPrefs, PortletPreferences portletPreferences) {
		addAuditEntry(portletPreferences, EventTypes.UPDATE);
	}

	private void addAuditEntry(PortletPreferences portletPreferences, String eventType) {
		auditService.addAuditLogEntry(PortletPreferences.class.getName(), portletPreferences.getPortletPreferencesId(), eventType, auditService.getPortletPreferencesAuditInfo(portletPreferences));
	}

}
