/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.authentication.twofactor.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.persistence.UserTwoFactorAuthenticationPK;

import java.io.Serializable;

import java.util.List;
import java.util.Optional;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for UserTwoFactorAuthentication. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see UserTwoFactorAuthenticationLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface UserTwoFactorAuthenticationLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>com.placecube.digitalplace.authentication.twofactor.service.impl.UserTwoFactorAuthenticationLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the user two factor authentication local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link UserTwoFactorAuthenticationLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the user two factor authentication to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 * @return the user two factor authentication that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public UserTwoFactorAuthentication addUserTwoFactorAuthentication(
		UserTwoFactorAuthentication userTwoFactorAuthentication);

	public void configureTwoFactorAuthenticationForUser(
		User user, String secretKey);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Creates a new user two factor authentication with the primary key. Does not add the user two factor authentication to the database.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key for the new user two factor authentication
	 * @return the new user two factor authentication
	 */
	@Transactional(enabled = false)
	public UserTwoFactorAuthentication createUserTwoFactorAuthentication(
		UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	/**
	 * Deletes the user two factor authentication from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 * @return the user two factor authentication that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public UserTwoFactorAuthentication deleteUserTwoFactorAuthentication(
		UserTwoFactorAuthentication userTwoFactorAuthentication);

	/**
	 * Deletes the user two factor authentication with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication that was removed
	 * @throws PortalException if a user two factor authentication with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public UserTwoFactorAuthentication deleteUserTwoFactorAuthentication(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK)
		throws PortalException;

	public void disableTwoFactorAuthenticationForUser(User user);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> T dslQuery(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int dslQueryCount(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	public UserTwoFactorAuthentication
		enableAndGetOrCreateTwoFactorAuthenticationForUser(
			User user, boolean enabled);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public UserTwoFactorAuthentication fetchUserTwoFactorAuthentication(
		UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK);

	/**
	 * Returns the user two factor authentication with the matching UUID and company.
	 *
	 * @param uuid the user two factor authentication's UUID
	 * @param companyId the primary key of the company
	 * @return the matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public UserTwoFactorAuthentication
		fetchUserTwoFactorAuthenticationByUuidAndCompanyId(
			String uuid, long companyId);

	/**
	 * Generates a new random security key
	 *
	 * @return the generated security key
	 */
	public String generateSecretKey();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Gets the QR bar code URL.
	 *
	 * @param companyId the company id
	 * @param emailAddress the email address of the user
	 * @param secretKey the secret key associated to the user
	 * @return the QR bar code URL
	 * @throws PortalException any exception retrieving the qr bar code url
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public String getQRBarCodeURL(
			long companyId, String emailAddress, String secretKey)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Optional<UserTwoFactorAuthentication> getUserTwoFactorAuthentication(
		User user);

	/**
	 * Returns the user two factor authentication with the primary key.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication
	 * @throws PortalException if a user two factor authentication with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public UserTwoFactorAuthentication getUserTwoFactorAuthentication(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK)
		throws PortalException;

	/**
	 * Returns the user two factor authentication with the matching UUID and company.
	 *
	 * @param uuid the user two factor authentication's UUID
	 * @param companyId the primary key of the company
	 * @return the matching user two factor authentication
	 * @throws PortalException if a matching user two factor authentication could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public UserTwoFactorAuthentication
			getUserTwoFactorAuthenticationByUuidAndCompanyId(
				String uuid, long companyId)
		throws PortalException;

	/**
	 * Returns a range of all the user two factor authentications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @return the range of user two factor authentications
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<UserTwoFactorAuthentication> getUserTwoFactorAuthentications(
		int start, int end);

	/**
	 * Returns the number of user two factor authentications.
	 *
	 * @return the number of user two factor authentications
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getUserTwoFactorAuthenticationsCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isTwoFactorAuthenticationEnforcedForUser(User user);

	/**
	 * Checks if the Two-Factor authentication code is valid for the user.
	 *
	 * @param user the user
	 * @param authenticationCode the authenticationCode to check
	 * @return true if the code is valid or if the feature is disabled for the
	 user. Returns false if the feature is enabled and the code is not
	 valid
	 * @throws PortalException any exception while checking the two factor
	 authentication code
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isValidTwoFactorAuthentication(
			User user, String authenticationCode)
		throws PortalException;

	public void resetUserTwoFactorAuthentication(long companyId, long userId);

	/**
	 * Updates the user two factor authentication in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 * @return the user two factor authentication that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public UserTwoFactorAuthentication updateUserTwoFactorAuthentication(
		UserTwoFactorAuthentication userTwoFactorAuthentication);

	public boolean verifyAndEnableUserTwoFactorAuthentication(
			User user, String authenticationCode, String secretKey)
		throws PortalException;

}