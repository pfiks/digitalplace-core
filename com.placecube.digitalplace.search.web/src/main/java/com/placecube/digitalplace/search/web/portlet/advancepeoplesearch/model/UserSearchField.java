package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.model;

public class UserSearchField {

	private final String field;
	private final String label;

	public UserSearchField(String field, String label) {
		this.field = field;
		this.label = label;
	}

	public String getField() {
		return field;
	}

	public String getLabel() {
		return label;
	}
}
