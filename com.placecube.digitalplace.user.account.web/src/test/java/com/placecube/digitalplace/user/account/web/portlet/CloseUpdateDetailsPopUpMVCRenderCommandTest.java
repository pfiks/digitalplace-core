package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class CloseUpdateDetailsPopUpMVCRenderCommandTest extends PowerMockito {

	private CloseUpdateDetailsPopUpMVCRenderCommand closeUpdateDetailsPopUpMVCRenderCommand = new CloseUpdateDetailsPopUpMVCRenderCommand();

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenNoErrors_ThenSetsRedirectAttributeAsRequestAttribute() throws PortletException {
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL)).thenReturn("redirectUrl");

		closeUpdateDetailsPopUpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.REDIRECT_URL, "redirectUrl");

	}

	@Test
	public void render_WhenNoErrors_ThenReturnsJsp() throws PortletException {
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL)).thenReturn("redirectUrl");

		String result = closeUpdateDetailsPopUpMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/close-update-details-popup.jsp"));

	}
}
