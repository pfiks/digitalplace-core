package com.placecube.digitalplace.search.web.portlet.contenttype.configuration;

public final class ContentTypeFacetPortletConfigurationConstants {

	public static final String FILTER_COLOUR = "filterColour";

	public static final String JOIN_FILTERS_WITH_THE_SAME_NAME = "joinFiltersWithTheSameName";

	private ContentTypeFacetPortletConfigurationConstants() {
	}

}
