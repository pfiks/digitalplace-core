package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.display.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletURL;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.journal.constants.JournalArticleConstants;
import com.liferay.journal.constants.JournalFolderConstants;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalArticleServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.journal.service.JournalFolderServiceUtil;
import com.liferay.journal.util.comparator.FolderArticleArticleIdComparator;
import com.liferay.journal.util.comparator.FolderArticleModifiedDateComparator;
import com.liferay.journal.util.comparator.FolderArticleTitleComparator;
import com.liferay.portal.kernel.bean.BeanParamUtil;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.QueryConfig;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.GroupServiceUtil;
import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HashMapBuilder;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.LinkedHashMapBuilder;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.search.JournalSearcher;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.JournalArticleItemSelectorCriterion;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.JournalArticleItemSelectorView;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.util.JournalPortletUtil;

public class JournalArticleItemSelectorViewDisplayContext {

	private final HttpServletRequest httpServletRequest;
	private final JournalArticleItemSelectorCriterion infoItemItemSelectorCriterion;
	private final String itemSelectedEventName;
	private final JournalArticleItemSelectorView journalArticleItemSelectorView;
	private final PortletRequest portletRequest;
	private final PortletResponse portletResponse;
	private final PortletURL portletURL;
	private final boolean search;
	private final ThemeDisplay themeDisplay;
	private SearchContainer<?> articleSearchContainer;
	private Long ddmStructureId;
	private String ddmStructureKey;
	private String displayStyle;
	private JournalFolder folder;
	private Long folderId;
	private String keywords;
	private String orderByCol;
	private String orderByType;
	private Boolean searchEverywhere;

	public JournalArticleItemSelectorViewDisplayContext(HttpServletRequest httpServletRequest1, JournalArticleItemSelectorCriterion infoItemItemSelectorCriterion1, String itemSelectedEventName1,
			JournalArticleItemSelectorView journalArticleItemSelectorView1, PortletURL portletURL1, boolean search1) {

		httpServletRequest = httpServletRequest1;
		infoItemItemSelectorCriterion = infoItemItemSelectorCriterion1;
		itemSelectedEventName = itemSelectedEventName1;
		journalArticleItemSelectorView = journalArticleItemSelectorView1;
		portletURL = portletURL1;
		search = search1;

		portletRequest = (PortletRequest) httpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST);
		portletResponse = (RenderResponse) httpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_RESPONSE);
		themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public Long getDDMStructureId() {
		if (ddmStructureId != null) {
			return ddmStructureId;
		}

		return ParamUtil.getLong(httpServletRequest, "ddmStructureId");
	}

	public String getDDMStructureKey() {
		if (ddmStructureKey != null) {
			return ddmStructureKey;
		}

		String ddmStructureKeyStr = ParamUtil.getString(httpServletRequest, "ddmStructureKey");

		if (Validator.isNull(ddmStructureKeyStr)) {
			ddmStructureKeyStr = infoItemItemSelectorCriterion.getItemSubtype();
		}

		ddmStructureKey = ddmStructureKeyStr;

		return ddmStructureKey;
	}

	public String getDisplayStyle() {
		if (Validator.isNotNull(displayStyle)) {
			return displayStyle;
		}

		displayStyle = ParamUtil.getString(httpServletRequest, "displayStyle", "descriptive");

		return displayStyle;
	}

	public String getGroupCssIcon(long groupId) throws PortalException {
		Group group = GroupServiceUtil.getGroup(groupId);

		return group.getIconCssClass();
	}

	public String getGroupLabel(long groupId, Locale locale) throws PortalException {

		Group group = GroupServiceUtil.getGroup(groupId);

		return group.getDescriptiveName(locale);
	}

	public String getItemSelectedEventName() {
		return itemSelectedEventName;
	}

	public String getKeywords() {
		if (keywords != null) {
			return keywords;
		}

		keywords = ParamUtil.getString(httpServletRequest, "keywords");

		return keywords;
	}

	public JournalArticle getLatestArticle(JournalArticle journalArticle) {
		JournalArticle latestArticle = JournalArticleLocalServiceUtil.fetchLatestArticle(journalArticle.getGroupId(), journalArticle.getArticleId(), WorkflowConstants.STATUS_ANY);

		if (latestArticle != null) {
			return latestArticle;
		}

		return journalArticle;
	}

	public List<BreadcrumbEntry> getPortletBreadcrumbEntries() throws Exception {

		List<BreadcrumbEntry> breadcrumbEntries = new ArrayList<>();

		breadcrumbEntries.add(_getSiteBreadcrumb());

		breadcrumbEntries.add(_getHomeBreadcrumb());

		JournalFolder folder = _getFolder();

		if (folder == null) {
			return breadcrumbEntries;
		}

		List<JournalFolder> ancestorFolders = folder.getAncestors();

		Collections.reverse(ancestorFolders);

		PortletURL portletURL = getPortletURL();

		portletURL.setParameter("folderId", String.valueOf(JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID));

		for (JournalFolder ancestorFolder : ancestorFolders) {
			BreadcrumbEntry folderBreadcrumbEntry = new BreadcrumbEntry();

			folderBreadcrumbEntry.setTitle(ancestorFolder.getName());

			portletURL.setParameter("folderId", String.valueOf(ancestorFolder.getFolderId()));

			folderBreadcrumbEntry.setURL(portletURL.toString());

			breadcrumbEntries.add(folderBreadcrumbEntry);
		}

		if (folder.getFolderId() != JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) {

			BreadcrumbEntry folderBreadcrumbEntry = new BreadcrumbEntry();

			JournalFolder unescapedFolder = folder.toUnescapedModel();

			folderBreadcrumbEntry.setTitle(unescapedFolder.getName());

			portletURL.setParameter("folderId", String.valueOf(folder.getFolderId()));

			folderBreadcrumbEntry.setURL(portletURL.toString());

			breadcrumbEntries.add(folderBreadcrumbEntry);
		}

		return breadcrumbEntries;
	}

	public PortletURL getPortletURL() throws PortletException {
		PortletURL portletURL2 = PortletURLUtil.clone(portletURL, PortalUtil.getLiferayPortletResponse(portletResponse));

		portletURL2.setParameter("displayStyle", getDisplayStyle());
		portletURL2.setParameter("selectedTab", String.valueOf(_getTitle(httpServletRequest.getLocale())));

		return portletURL2;
	}

	public SearchContainer<?> getSearchContainer() throws PortalException, PortletException {
		if (articleSearchContainer != null) {
			return articleSearchContainer;
		}

		if (getDDMStructureId() > 0) {
			SearchContainer<JournalArticle> articleSearchContainerNew = new SearchContainer<>(portletRequest, getPortletURL(), null, null);

			OrderByComparator<JournalArticle> orderByComparator = JournalPortletUtil.getArticleOrderByComparator(_getOrderByCol(), _getOrderByType());

			articleSearchContainerNew.setOrderByCol(_getOrderByCol());
			articleSearchContainerNew.setOrderByComparator(orderByComparator);
			articleSearchContainerNew.setOrderByType(_getOrderByType());

			int total = JournalArticleServiceUtil.getArticlesCountByStructureId(_getGroupId(), getDDMStructureId(), WorkflowConstants.STATUS_APPROVED);

			List<JournalArticle> results = JournalArticleServiceUtil.getArticlesByStructureId(_getGroupId(), getDDMStructureId(), WorkflowConstants.STATUS_APPROVED,
					articleSearchContainerNew.getStart(), articleSearchContainerNew.getEnd(), orderByComparator);

			articleSearchContainerNew.setResultsAndTotal(() -> results, total);

			articleSearchContainer = articleSearchContainerNew;

			return articleSearchContainer;
		}

		PortletURL portletURL = getPortletURL();

		portletURL.setParameter("folderId", String.valueOf(_getFolderId()));

		SearchContainer<Object> articleAndFolderSearchContainer = new SearchContainer<>(portletRequest, portletURL, null, null);

		articleAndFolderSearchContainer.setOrderByCol(_getOrderByCol());
		articleAndFolderSearchContainer.setOrderByType(_getOrderByType());

		if (isSearch()) {
			List<Long> folderIds = new ArrayList<>(1);

			if (_getFolderId() != JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID) {

				folderIds.add(_getFolderId());
			}

			boolean orderByAsc = false;

			if (Objects.equals(_getOrderByCol(), "asc")) {
				orderByAsc = true;
			}

			Sort sort = null;

			if (Objects.equals(_getOrderByCol(), "id")) {
				sort = new Sort(Field.getSortableFieldName(Field.ARTICLE_ID), Sort.STRING_TYPE, !orderByAsc);
			} else if (Objects.equals(_getOrderByCol(), "modified-date")) {
				sort = new Sort(Field.MODIFIED_DATE, Sort.LONG_TYPE, !orderByAsc);
			} else if (Objects.equals(_getOrderByCol(), "relevance")) {
				sort = new Sort(null, Sort.SCORE_TYPE, false);
			} else if (Objects.equals(_getOrderByCol(), "title")) {
				sort = new Sort(Field.getSortableFieldName("localized_title_" + themeDisplay.getLanguageId()), !orderByAsc);
			}

			Indexer<?> indexer = JournalSearcher.getInstance();

			SearchContext searchContext = buildSearchContext(folderIds, articleAndFolderSearchContainer.getStart(), articleAndFolderSearchContainer.getEnd(), sort);

			Hits hits = indexer.search(searchContext);

			List<Object> results = new ArrayList<>();

			Document[] documents = hits.getDocs();

			for (Document document : documents) {
				String className = document.get(Field.ENTRY_CLASS_NAME);
				long classPK = GetterUtil.getLong(document.get(Field.ENTRY_CLASS_PK));

				if (className.equals(JournalArticle.class.getName())) {
					JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(classPK, WorkflowConstants.STATUS_ANY, false);

					results.add(article);
				} else if (className.equals(JournalFolder.class.getName())) {
					results.add(JournalFolderLocalServiceUtil.getFolder(classPK));
				}
			}

			articleAndFolderSearchContainer.setResultsAndTotal(() -> results, hits.getLength());
		} else {
			int total = JournalFolderServiceUtil.getFoldersAndArticlesCount(_getGroupId(), 0, _getFolderId(), infoItemItemSelectorCriterion.getStatus());

			OrderByComparator<Object> folderOrderByComparator = null;

			boolean orderByAsc = false;

			if (Objects.equals(_getOrderByType(), "asc")) {
				orderByAsc = true;
			}

			if (Objects.equals(_getOrderByCol(), "id")) {
				folderOrderByComparator = new FolderArticleArticleIdComparator(orderByAsc);
			} else if (Objects.equals(_getOrderByCol(), "modified-date")) {
				folderOrderByComparator = new FolderArticleModifiedDateComparator(orderByAsc);
			} else if (Objects.equals(_getOrderByCol(), "title")) {
				folderOrderByComparator = new FolderArticleTitleComparator(orderByAsc);
			}

			List<Object> results = JournalFolderServiceUtil.getFoldersAndArticles(_getGroupId(), 0, _getFolderId(), infoItemItemSelectorCriterion.getStatus(), themeDisplay.getLocale(),
					articleAndFolderSearchContainer.getStart(), articleAndFolderSearchContainer.getEnd(), folderOrderByComparator);

			articleAndFolderSearchContainer.setResultsAndTotal(() -> results, total);
		}

		articleSearchContainer = articleAndFolderSearchContainer;

		return articleSearchContainer;
	}

	public boolean isSearch() {
		if (_isEverywhereScopeFilter()) {
			return true;
		}

		return search;
	}

	public boolean isSearchEverywhere() {
		if (searchEverywhere != null) {
			return searchEverywhere;
		}

		if (Objects.equals(ParamUtil.getString(httpServletRequest, "scope"), "everywhere")) {

			searchEverywhere = true;
		} else {
			searchEverywhere = false;
		}

		return searchEverywhere;
	}

	public boolean showArticleId() {
		return false;
	}

	protected SearchContext buildSearchContext(List<Long> folderIds, int start, int end, Sort sort) throws PortalException {

		SearchContext searchContext = new SearchContext();

		searchContext.setAndSearch(false);
		searchContext.setAttribute("head", Boolean.TRUE);
		searchContext.setAttribute("latest", Boolean.TRUE);

		LinkedHashMap<String, Object> params = LinkedHashMapBuilder.<String, Object>put("expandoAttributes", getKeywords()).put("keywords", getKeywords()).build();

		searchContext.setAttribute("params", params);

		searchContext.setAttribute("showNonindexable", Boolean.TRUE);
		searchContext.setAttributes(HashMapBuilder.<String, Serializable>put(Field.ARTICLE_ID, getKeywords()).put(Field.CLASS_NAME_ID, JournalArticleConstants.CLASSNAME_ID_DEFAULT)
				.put(Field.CONTENT, getKeywords()).put(Field.DESCRIPTION, getKeywords()).put(Field.STATUS, infoItemItemSelectorCriterion.getStatus()).put(Field.TITLE, getKeywords())
				.put("ddmStructureKey", getDDMStructureKey()).put("ddmStructureId", getDDMStructureId()).put("params", params).build());

		searchContext.setCompanyId(themeDisplay.getCompanyId());
		searchContext.setEnd(end);
		searchContext.setFolderIds(folderIds);
		searchContext.setGroupIds(_getGroupIds());
		// searchContext.setIncludeInternalAssetCategories(true);
		searchContext.setKeywords(getKeywords());

		QueryConfig queryConfig = searchContext.getQueryConfig();

		queryConfig.setHighlightEnabled(false);
		queryConfig.setScoreEnabled(false);

		if (sort != null) {
			searchContext.setSorts(sort);
		}

		searchContext.setStart(start);

		return searchContext;
	}

	private JournalFolder _getFolder() {
		if (folder != null) {
			return folder;
		}

		long folderId = ParamUtil.getLong(httpServletRequest, "folderId");

		folder = JournalFolderLocalServiceUtil.fetchFolder(folderId);

		return folder;
	}

	private long _getFolderId() {
		if (folderId != null) {
			return folderId;
		}

		folderId = BeanParamUtil.getLong(_getFolder(), httpServletRequest, "folderId", JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID);

		return folderId;
	}

	private long _getGroupId() {
		return ParamUtil.getLong(portletRequest, "groupId", themeDisplay.getScopeGroupId());
	}

	private long[] _getGroupIds() throws PortalException {
		/*
		 * if (_isEverywhereScopeFilter()) { return SiteConnectedGroupUtil.
		 * getCurrentAndAncestorSiteAndDepotGroupIds(
		 * _themeDisplay.getScopeGroupId()); }
		 */
		return PortalUtil.getCurrentAndAncestorSiteGroupIds(themeDisplay.getScopeGroupId());
	}

	private BreadcrumbEntry _getHomeBreadcrumb() throws Exception {
		BreadcrumbEntry breadcrumbEntry = new BreadcrumbEntry();

		Group group = GroupLocalServiceUtil.getGroup(_getGroupId());

		breadcrumbEntry.setTitle(group.getDescriptiveName(themeDisplay.getLocale()));

		PortletURL portletURL = getPortletURL();

		portletURL.setParameter("folderId", String.valueOf(JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID));

		breadcrumbEntry.setURL(portletURL.toString());

		return breadcrumbEntry;
	}

	private String _getOrderByCol() {
		if (orderByCol != null) {
			return orderByCol;
		}

		String defaultOrderByCol = "modified-date";

		if (isSearch()) {
			defaultOrderByCol = "relevance";
		}

		orderByCol = ParamUtil.getString(httpServletRequest, "orderByCol", defaultOrderByCol);

		return orderByCol;
	}

	private String _getOrderByType() {
		if (orderByType != null) {
			return orderByType;
		}

		if (Objects.equals(_getOrderByCol(), "relevance")) {
			return "desc";
		}

		orderByType = ParamUtil.getString(httpServletRequest, "orderByType", "asc");

		return orderByType;
	}

	private BreadcrumbEntry _getSiteBreadcrumb() throws Exception {
		BreadcrumbEntry breadcrumbEntry = new BreadcrumbEntry();

		ResourceBundle resourceBundle = ResourceBundleUtil.getBundle("content.Language", themeDisplay.getLocale(), getClass());

		breadcrumbEntry.setTitle(LanguageUtil.get(httpServletRequest, resourceBundle.getString("sites-and-libraries")));

		PortletURL portletURL = getPortletURL();

		portletURL.setParameter("groupType", "site");
		portletURL.setParameter("showGroupSelector", Boolean.TRUE.toString());

		breadcrumbEntry.setURL(portletURL.toString());

		return breadcrumbEntry;
	}

	private String _getTitle(Locale locale) {
		return journalArticleItemSelectorView.getTitle(locale);
	}

	private boolean _isEverywhereScopeFilter() {
		if (Objects.equals(ParamUtil.getString(httpServletRequest, "scope"), "everywhere")) {

			return true;
		}

		return false;
	}

}
