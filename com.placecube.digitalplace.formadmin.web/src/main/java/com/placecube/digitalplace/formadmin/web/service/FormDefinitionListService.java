package com.placecube.digitalplace.formadmin.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.digitalplace.form.service.FormDefinitionRegistry;
import com.placecube.digitalplace.form.service.FormDefinitionService;
import com.placecube.digitalplace.formadmin.web.model.FormDefinitionResult;

@Component(immediate = true, service = { FormDefinitionListService.class })
public class FormDefinitionListService {

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private FormDefinitionRegistry formDefinitionRegistry;

	@Reference
	private FormDefinitionService formDefinitionService;

	@Reference
	private Portal portal;

	public List<FormDefinitionResult> getFormDefinitions(RenderRequest renderRequest) {

		List<FormDefinition> formDefinitions = formDefinitionRegistry.getFormNamesAndFormDefinitions().values().stream().collect(Collectors.toList());

		List<FormDefinitionResult> formDefinitionResults = new ArrayList<>();

		if (!formDefinitions.isEmpty()) {

			formDefinitions = formDefinitionService.sort(formDefinitions);

			long classNameId = portal.getClassNameId(DDMFormInstance.class);

			long groupId = getScopeGroupId(renderRequest);

			for (FormDefinition formDefinition : formDefinitions) {

				boolean installed = ddmStructureLocalService.fetchStructure(groupId, classNameId, formDefinition.getKey()) != null;

				formDefinitionResults.add(FormDefinitionResult.create(formDefinition, installed));
			}
		}

		return formDefinitionResults;
	}

	private long getScopeGroupId(RenderRequest renderRequest) {
		return ((ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY)).getScopeGroupId();
	}

}
