package com.placecube.digitalplace.group.rest.service.util;

import com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = GroupApiKeyPKUtil.class)
public class GroupApiKeyPKUtil {

	public GroupApiKeyPK createGroupApiKeyPK(long companyId, long groupId) {
		return new GroupApiKeyPK(companyId, groupId);
	}
}
