package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.ItemSelectorView;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.Language;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.LiferayRenderRequest;
import com.liferay.portal.kernel.portlet.LiferayRenderResponse;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.JavaConstants;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.constants.JournalArticleItemSelectorRequestKeys;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.display.context.JournalArticleItemSelectorViewDisplayContext;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.display.context.JournalArticleItemSelectorViewManagementToolbarDisplayContext;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.util.JournalArticleItemSelectorViewHelper;

@Component(property = "item.selector.view.order:Integer=100", service = ItemSelectorView.class)
public class JournalArticleItemSelectorView implements ItemSelectorView<JournalArticleItemSelectorCriterion> {

	private static final List<ItemSelectorReturnType> supportedItemSelectorReturnTypes = Arrays.asList(new JournalArticleItemSelectorReturnType());

	@Reference
	private Language language;

	@Reference
	private JournalArticleItemSelectorViewHelper journalArticleItemSelectorViewHelper;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.editor.itemselector.contributor)")
	private ServletContext servletContext;

	public String getClassName() {
		return JournalArticle.class.getName();
	}

	@Override
	public Class<JournalArticleItemSelectorCriterion> getItemSelectorCriterionClass() {

		return JournalArticleItemSelectorCriterion.class;
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	@Override
	public List<ItemSelectorReturnType> getSupportedItemSelectorReturnTypes() {
		return supportedItemSelectorReturnTypes;
	}

	@Override
	public String getTitle(Locale locale) {
		return language.get(locale, "web-content");
	}

	@Override
	public boolean isVisible(ThemeDisplay themeDisplay) {
		return true;
	}

	@Override
	public void renderHTML(ServletRequest servletRequest, ServletResponse servletResponse, JournalArticleItemSelectorCriterion infoItemItemSelectorCriterion, PortletURL portletURL,
			String itemSelectedEventName, boolean search) throws IOException, ServletException {

		try {
			HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

			JournalArticleItemSelectorViewDisplayContext journalItemSelectorViewDisplayContext = new JournalArticleItemSelectorViewDisplayContext(httpServletRequest, infoItemItemSelectorCriterion,
					itemSelectedEventName, this, portletURL, search);

			LiferayPortletRequest liferayPortletRequest = (LiferayRenderRequest) servletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST);
			LiferayPortletResponse liferayPortletResponse = (LiferayRenderResponse) servletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_RESPONSE);

			JournalArticleItemSelectorViewManagementToolbarDisplayContext journalArticleItemSelectorViewManagementToolbarDisplayContext = new JournalArticleItemSelectorViewManagementToolbarDisplayContext(
					httpServletRequest, liferayPortletRequest, liferayPortletResponse, journalItemSelectorViewDisplayContext);

			servletRequest.setAttribute(JournalArticleItemSelectorRequestKeys.JOURNAL_ARTICLE_ITEM_SELECTOR_VIEW_DISPLAY_CONTEXT, journalItemSelectorViewDisplayContext);
			servletRequest.setAttribute(JournalArticleItemSelectorRequestKeys.JOURNAL_ARTICLE_ITEM_SELECTOR_VIEW_MANAGEMENT_TOOLBAR_DISPLAY_CONTEXT,
					journalArticleItemSelectorViewManagementToolbarDisplayContext);
			servletRequest.setAttribute(JournalArticleItemSelectorRequestKeys.JOURNAL_ARTICLE_ITEM_SELECTOR_VIEW_HELPER, journalArticleItemSelectorViewHelper);
			servletRequest.setAttribute(JournalArticleItemSelectorRequestKeys.HTML, HtmlUtil.class);

			ServletContext servletContext = getServletContext();

			RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher("/item/selector/select_articles.jsp");

			requestDispatcher.include(servletRequest, servletResponse);
		} catch (PortletException | PortalException e) {
			throw new ServletException(e);
		}
	}

}