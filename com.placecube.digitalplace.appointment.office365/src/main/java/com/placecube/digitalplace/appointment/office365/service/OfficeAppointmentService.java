package com.placecube.digitalplace.appointment.office365.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.appointment.office365.configuration.AppointmentOffice365CompanyConfiguration;
import com.placecube.digitalplace.appointment.office365.model.AppointmentConfiguredService;

@Component(immediate = true, service = OfficeAppointmentService.class)
public class OfficeAppointmentService {

	private static final Log LOG = LogFactoryUtil.getLog(OfficeAppointmentService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private ParsingUtils parsingUtils;

	public Set<AppointmentConfiguredService> getAppointmentConfiguredServices(AppointmentOffice365CompanyConfiguration configuration, String serviceId) {
		Set<AppointmentConfiguredService> results = new HashSet<>();

		String[] serviceIdUserCalendars = configuration.serviceIdUserCalendars();

		for (String configuredService : serviceIdUserCalendars) {
			results.add(parsingUtils.getAppointmentConfiguredServiceFromJsonString(configuredService));
		}
		return results.stream().filter(appointmentConfiguredService -> serviceId.equals(appointmentConfiguredService.getServiceId())).collect(Collectors.toSet());
	}

	public Optional<AppointmentOffice365CompanyConfiguration> getAppointmentOffice365CompanyConfiguration(long companyId) {
		try {
			AppointmentOffice365CompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(AppointmentOffice365CompanyConfiguration.class, companyId);
			return Optional.of(configuration);
		} catch (Exception e) {
			LOG.warn("Unable to retrieve Office365 configuration for companyId: " + companyId + " - " + e.getMessage());
		}
		return Optional.empty();
	}

	public Optional<AppointmentOffice365CompanyConfiguration> getValidAppointmentOffice365CompanyConfiguration(long companyId) {
		Optional<AppointmentOffice365CompanyConfiguration> configuration = getAppointmentOffice365CompanyConfiguration(companyId);
		if (configuration.isPresent()) {
			AppointmentOffice365CompanyConfiguration config = configuration.get();

			if (Validator.isNotNull(config.applicationId()) && Validator.isNotNull(config.applicationSecret()) && Validator.isNotNull(config.tenantId())) {
				return configuration;
			}
		}
		return Optional.empty();
	}

}
