package com.placecube.digitalplace.oauth.dataprovider.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class DataProviderPortalCacheServiceTest {

	private DataProviderPortalCacheService dataProviderPortalCacheService;

	@Before
	public void activateSetup() {
		dataProviderPortalCacheService = new DataProviderPortalCacheService();
	}

	@Test
	public void getPortalCacheKey_WhenParametersMapIsNotEmpty_ThenReturnsPortalCacheKeyFromIdParametersAndUrl() {
		Map<String, String> parameters = new HashMap<>();
		parameters.put("parameter1", "value1");
		parameters.put("parameter2", "value2");

		String result = dataProviderPortalCacheService.getPortalCacheKey("12456", parameters, "localhost:8080");

		assertThat(result, equalTo("12456@localhost:8080?parameter2=value2&parameter1=value1"));
	}

	@Test
	public void getPortalCacheKey_WhenAllParametersMapIsEmpty_ThenReturnsPortalCacheKeyFromIdAndUrl() {
		String result = dataProviderPortalCacheService.getPortalCacheKey("12456", new HashMap<>(), "localhost:8080");

		assertThat(result, equalTo("12456@localhost:8080?"));
	}
}
