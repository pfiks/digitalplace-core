package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorPortletKeys;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorEmailService;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;

@Component(property = { "javax.portlet.name=" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION, "mvc.command.name=" + TwoFactorMVCCommandKeys.SEND_EMAIL_WITH_CODE }, service = MVCActionCommand.class)
public class TwoFactorAuthenticationSendEmailWithCodeMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private Portal portal;

	@Reference
	private TwoFactorEmailService twoFactorEmailService;

	@Reference
	private TwoFactorRequestUtils twoFactorRequestUtils;

	@Reference
	private TwoFactorService twoFactorService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		HttpServletRequest originalServletRequest = twoFactorRequestUtils.getOriginalServletRequest(actionRequest);
		Optional<User> twoFactorUser = twoFactorService.getTwoFactorUser(actionRequest, originalServletRequest);

		if (twoFactorUser.isPresent()) {
			User user = twoFactorUser.get();
			String randomCode = twoFactorEmailService.genereateCode();
			twoFactorEmailService.removePreviousTickets(user);
			Ticket ticket = twoFactorEmailService.addTicket(randomCode, user, twoFactorEmailService.getExpirySeconds(user.getCompanyId()));
			twoFactorEmailService.sendEmail(user.getEmailAddress(), user.getCompanyId(), ticket.getKey(), user.getFullName(), portal.getPortalURL(originalServletRequest));
		}

	}

}
