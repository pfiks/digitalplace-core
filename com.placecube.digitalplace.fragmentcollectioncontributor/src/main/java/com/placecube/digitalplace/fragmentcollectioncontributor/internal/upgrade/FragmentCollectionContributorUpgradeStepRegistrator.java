package com.placecube.digitalplace.fragmentcollectioncontributor.internal.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.fragment.service.FragmentEntryLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.fragmentcollectioncontributor.internal.upgrade.upgrade_1_0_0.GuideFragmentsUpgradeProcess;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class FragmentCollectionContributorUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private FragmentEntryLocalService fragmentEntryLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new GuideFragmentsUpgradeProcess(companyLocalService, fragmentEntryLocalService));
	}
}
