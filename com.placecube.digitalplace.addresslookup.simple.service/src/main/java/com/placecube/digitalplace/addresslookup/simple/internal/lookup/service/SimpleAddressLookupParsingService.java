package com.placecube.digitalplace.addresslookup.simple.internal.lookup.service;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;

@Component(immediate = true, service = SimpleAddressLookupParsingService.class)
public class SimpleAddressLookupParsingService {

	public AddressContext getAddressContext(SimpleAddressLookupEntry simpleAddressLookupEntry) {
		return new AddressContext(simpleAddressLookupEntry.getUprn(), simpleAddressLookupEntry.getAddressLine1(), simpleAddressLookupEntry.getAddressLine2(),
				simpleAddressLookupEntry.getAddressLine3(), StringPool.BLANK, simpleAddressLookupEntry.getCity(), simpleAddressLookupEntry.getPostcode());
	}

	public Set<AddressContext> getAddressContexts(List<SimpleAddressLookupEntry> simpleAddressLookupEntries) {
		return simpleAddressLookupEntries.stream().map(entry -> getAddressContext(entry)).collect(Collectors.toCollection(LinkedHashSet::new));
	}

}
