package com.placecube.digitalplace.appointment.admin.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.appointment.admin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.admin.web.constants.PortletKeys;
import com.placecube.digitalplace.appointment.service.AppointmentSlotLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.APPOINTMENTS_MANAGER, "mvc.command.name=" + MVCCommandKeys.DELETE_SLOT }, service = MVCActionCommand.class)
public class DeleteAppointmentSlotMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(DeleteAppointmentSlotMVCActionCommand.class);

	@Reference
	private AppointmentSlotLocalService appointmentSlotLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try {
			long appointmentSlotId = ParamUtil.getLong(actionRequest, "appointmentSlotId");
			if (appointmentSlotId > 0) {
				appointmentSlotLocalService.deleteAppointmentSlot(appointmentSlotId);
			}

			actionResponse.getRenderParameters().setValue("serviceId", ParamUtil.getString(actionRequest, "serviceId", StringPool.BLANK));
			actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_ALL_SLOTS);
		} catch (Exception e) {
			LOG.error(e);
			throw e;
		}
	}

}
