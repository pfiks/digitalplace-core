package com.placecube.digitalplace.gds.taglib.tag;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.gds.taglib.context.ServletContextUtil;

public class InputTypeRadioTag extends IncludeTag {

	private boolean inline = false;
	private String errorMessage;
	private String fieldHint;
	private String fieldLabel;
	private String fieldLabelSize = "l";
	private String fieldName;
	private Map<String, String> fieldOptions;
	private String fieldValue;
	private String portletNamespace;
	private boolean required = false;

	@Override
	protected void cleanUp() {
		super.cleanUp();
		portletNamespace = null;
		fieldOptions = new HashMap<>();
		fieldName = null;
		fieldHint = null;
		fieldLabel = null;
		fieldLabelSize = null;
		fieldValue = null;
		errorMessage = null;
		required = false;
	}

	@Override
	public int doStartTag() {
		setAttributeNamespace("gds-forms-ui:input-radio");
		return EVAL_BODY_INCLUDE;
	}

	@Override
	protected String getPage() {
		return "/META-INF/taglibs/gds-forms-ui/radio/view.jsp";
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute("inline", inline);
		request.setAttribute("portletNamespace", portletNamespace);
		request.setAttribute("fieldName", fieldName);
		request.setAttribute("fieldHint", fieldHint);
		request.setAttribute("fieldLabel", fieldLabel);
		request.setAttribute("fieldLabelSize", fieldLabelSize);
		request.setAttribute("fieldValue", fieldValue);
		request.setAttribute("errorMessage", errorMessage);
		request.setAttribute("fieldOptions", fieldOptions);
		request.setAttribute("required", required);

	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setFieldHint(String fieldHint) {
		this.fieldHint = fieldHint;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public void setFieldLabelSize(String fieldLabelSize) {
		this.fieldLabelSize = fieldLabelSize;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setFieldOptions(Map<String, String> fieldOptions) {
		this.fieldOptions = fieldOptions;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);
		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setPortletNamespace(String portletNamespace) {
		this.portletNamespace = portletNamespace;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public void setInline(boolean inline) {
		this.inline = inline;
	}
}