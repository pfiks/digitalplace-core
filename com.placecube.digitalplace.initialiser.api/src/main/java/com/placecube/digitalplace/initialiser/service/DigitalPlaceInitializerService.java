package com.placecube.digitalplace.initialiser.service;

import java.util.Locale;
import java.util.Map;

import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.ServiceContext;

@SuppressWarnings("rawtypes")
public interface DigitalPlaceInitializerService {

	/**
	 * Sets the layout priority to zero
	 *
	 * @param layout the layout to update
	 * @return the updated layout
	 * @throws PortalException exception updating the layout priority
	 */
	Layout configurePageAsFirstLayout(Layout layout) throws PortalException;

	/**
	 * Creates the page specified in the json object as a content page, or
	 * retrieves the current page by friendlyURL
	 *
	 * @param clazz the class
	 * @param pageJSONObject JSONObject containing the page details
	 * @param preferencesPlaceholderValues map to hold preferences placeholders
	 *            and the value to use
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	Layout initializeContentPage(Class clazz, JSONObject pageJSONObject, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException;

	/**
	 * Creates the page specified in the json file as a content page, or
	 * retrieves the current page by friendlyURL
	 *
	 * @param clazz the class
	 * @param jsonFilePath the file path
	 * @param preferencesPlaceholderValues map to hold preferences placeholders
	 *            and the value to use
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	Layout initializeContentPage(Class clazz, String jsonFilePath, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException;

	/**
	 * Creates the page specified in the json file as a link to layout page, or
	 * retrieves the current page by friendlyURL
	 *
	 * @param clazz the class
	 * @param jsonFilePath the file path
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	Layout initializeLinkToLayoutPage(Class clazz, String jsonFilePath, ServiceContext serviceContext) throws PortalException;

	/**
	 * Creates a page from a specified page template or retrieves the current
	 * page by friendlyURL Creates a page from a specified page template or
	 * retrieves the current page by friendlyURL
	 *
	 * @param pageTitleMap the localised title map of the created page
	 * @param friendlyURL the friendlyURL of the created page
	 * @param isPrivate whether the page should be private
	 * @param layoutPageTemplateEntry page template from which layout is to be
	 *            created
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	Layout initializePageFromPageTemplate(Map<Locale, String> pageTitleMap, String friendlyURL, boolean isPrivate, LayoutPageTemplateEntry layoutPageTemplateEntry, ServiceContext serviceContext)
			throws PortalException;

	/**
	 * Creates a page from a specified page template or retrieves the current
	 * page by friendlyURL
	 *
	 * @param pageTitle the title of the created page
	 * @param friendlyURL the friendlyURL of the created page
	 * @param isPrivate whether the page should be private
	 * @param layoutPageTemplateEntry page template from which layout is to be
	 *            created
	 * @param rolePermissionsToAdd the role permissions to add to the created
	 *            page
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	Layout initializePageFromPageTemplate(String pageTitle, String friendlyURL, boolean isPrivate, LayoutPageTemplateEntry layoutPageTemplateEntry, Map<String, String[]> rolePermissionsToAdd,
			ServiceContext serviceContext) throws PortalException;

	/**
	 * Creates a page from a specified page template or retrieves the current
	 * page by friendlyURL
	 *
	 * @param pageTitle the title of the created page
	 * @param friendlyURL the friendlyURL of the created page
	 * @param isPrivate whether the page should be private
	 * @param layoutPageTemplateEntry page template from which layout is to be
	 *            created
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	Layout initializePageFromPageTemplate(String pageTitle, String friendlyURL, boolean isPrivate, LayoutPageTemplateEntry layoutPageTemplateEntry, ServiceContext serviceContext)
			throws PortalException;

	/**
	 * Creates a page template specified in the json file, or retrieves the
	 * current page template by template name
	 *
	 * @param clazz the class
	 * @param jsonFilePath the file path
	 * @param preferencesPlaceholderValues map to hold preferences placeholders
	 *            and the value to use
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	LayoutPageTemplateEntry initializePageTemplate(Class clazz, String jsonFilePath, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException;

	/**
	 * Creates the page specified in the json object as a widget page, or
	 * retrieves the current page by friendlyURL
	 *
	 * @param clazz the class
	 * @param pageJSONObject the page json object
	 * @param preferencesPlaceholderValues map to hold preferences placeholders
	 *            and the value to use
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	Layout initializeWidgetPage(Class clazz, JSONObject pageJSONObject, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException;

	/**
	 * Creates the page specified in the json file as a widget page, or
	 * retrieves the current page by friendlyURL
	 *
	 * @param clazz the class
	 * @param jsonFilePath the file path
	 * @param preferencesPlaceholderValues map to hold preferences placeholders
	 *            and the value to use
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	Layout initializeWidgetPage(Class clazz, String jsonFilePath, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException;

	/**
	 * Creates the page specified in the json file as a widget page, or
	 * retrieves the current page by friendlyURL
	 *
	 * @param clazz the class
	 * @param jsonFilePath the file path
	 * @param serviceContext the service context
	 * @return the created page
	 * @throws PortalException any exception creating and configuring the page
	 */
	Layout initializeWidgetPage(Class clazz, String jsonFilePath, ServiceContext serviceContext) throws PortalException;

}
