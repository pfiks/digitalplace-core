package com.placecube.digitalplace.notifications.model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SendMailContext {

	private final String apiKeyName;
	private final List<File> attachments;
	private final String[] bccEmailAddresses;
	private final String emailAddress;
	private final String fromEmailAddress;
	private final String fromName;
	private final long groupId;
	private final Map<String, Object> personalisation;
	private final String reference;
	private final String templateId;

	private SendMailContext(SendMailContextBuilder builder) {
		templateId = builder.templateId;
		emailAddress = builder.emailAddress;
		personalisation = builder.personalisation;
		reference = builder.reference;
		fromEmailAddress = builder.fromEmailAddress;
		attachments = builder.attachments;
		bccEmailAddresses = builder.bccEmailAddresses;
		apiKeyName = builder.apiKeyName;
		groupId = builder.groupId;
		fromName = builder.fromName;
	}

	public String getApiKeyName() {
		return apiKeyName;
	}

	public List<File> getAttachments() {
		return attachments;
	}

	public String[] getBccEmailAddresses() {
		return bccEmailAddresses;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getFromEmailAddress() {
		return fromEmailAddress;
	}

	public long getGroupId() {
		return groupId;
	}

	public Map<String, Object> getPersonalisation() {
		return personalisation;
	}

	public String getReference() {
		return reference;
	}

	public String getTemplateId() {
		return templateId;
	}

	public String getFromName() {
		return fromName;
	}

	public static class SendMailContextBuilder {

		private final String emailAddress;
		private final String fromEmailAddress;
		private final Map<String, Object> personalisation;
		private final String reference;
		private final String templateId;

		private String apiKeyName;
		private List<File> attachments = new ArrayList<>();
		private String[] bccEmailAddresses = new String[] {};
		private String fromName;
		private long groupId = 0;

		public SendMailContextBuilder(String templateId, String emailAddress, Map<String, Object> personalisation, String reference, String fromEmailAddress) {
			this.templateId = templateId;
			this.emailAddress = emailAddress;
			this.personalisation = null != personalisation ? personalisation : new HashMap<>();
			this.reference = reference;
			this.fromEmailAddress = fromEmailAddress;
		}

		public SendMailContextBuilder fromName(String fromName) {
			this.fromName = fromName;
			return this;
		}

		public SendMailContextBuilder attachments(List<File> attachments) {
			if (attachments != null) {
				this.attachments = attachments;
			}

			return this;
		}

		public SendMailContextBuilder bccEmailAddresses(String[] bccEmailAddresses) {
			if (bccEmailAddresses != null) {
				this.bccEmailAddresses = bccEmailAddresses;
			}

			return this;
		}

		public SendMailContextBuilder apiKeyName(String apiKeyName) {
			this.apiKeyName = apiKeyName;
			return this;
		}

		public SendMailContextBuilder groupId(long groupId) {
			this.groupId = groupId;
			return this;
		}

		public SendMailContext build() {
			return new SendMailContext(this);
		}
	}
}
