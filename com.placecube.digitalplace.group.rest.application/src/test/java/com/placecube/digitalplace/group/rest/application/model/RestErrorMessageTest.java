package com.placecube.digitalplace.group.rest.application.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

public class RestErrorMessageTest extends PowerMockito {

	private static final String STACKTRACE = "stacktraceValue";
	private static final String ERROR_CODE = "errorCodeValue";
	private static final int ERROR_STATUS = 5460;

	@Test
	public void init_WhenNoError_ThenReturnsEntryWithErrorStatus() {
		RestErrorMessage result = RestErrorMessage.init(ERROR_STATUS, ERROR_CODE, STACKTRACE);

		assertThat(result.getErrorStatus(), equalTo(ERROR_STATUS));
	}

	@Test
	public void init_WhenNoError_ThenReturnsEntryWithErrorCode() {
		RestErrorMessage result = RestErrorMessage.init(ERROR_STATUS, ERROR_CODE, STACKTRACE);

		assertThat(result.getErrorCode(), equalTo(ERROR_CODE));
	}

	@Test
	public void init_WhenNoError_ThenReturnsEntryWithStacktrace() {
		RestErrorMessage result = RestErrorMessage.init(ERROR_STATUS, ERROR_CODE, STACKTRACE);

		assertThat(result.getStackTrace(), equalTo(STACKTRACE));
	}

}
