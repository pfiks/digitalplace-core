package com.placecube.digitalplace.user.settings.web.twofactor.constants;

/**
 * A constants class that provides keys to request attributes used by the user
 * settings portlet
 *
 * @author Chris Newton
 */
public final class TwoFactorAuthenticationRequestKeys {

	public static final String QR_URL = "qrURL";

	public static final String SECRET_KEY = "secretKey";

	public static final String SECRET_KEY_GENERATED = "secretKeyGenerated";

	public static final String TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM = "twoFactorAuthenticationDisplayForm";

	public static final String TWO_FACTOR_AUTHENTICATION_ENFORCED = "twoFactorAuthenticationEnforced";

	public static final String TWO_FACTOR_VERIFICATION_CODE = "twoFactorVerificationCode";

	private TwoFactorAuthenticationRequestKeys() {
	}
}
