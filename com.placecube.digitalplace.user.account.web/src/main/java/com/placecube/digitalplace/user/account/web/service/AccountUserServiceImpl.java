package com.placecube.digitalplace.user.account.web.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountEmailAddressService;
import com.placecube.digitalplace.user.account.service.AccountUserService;

@Component(immediate = true, service = AccountUserService.class)
public class AccountUserServiceImpl implements AccountUserService {

	@Reference
	private AccountEmailAddressService accountEmailAddressService;

	@Override
	public void populateContextWithExistingUserDetails(AccountContext accountContext, User user) throws UserAccountException {
		accountContext.setUserId(user.getUserId());
		accountContext.setFirstName(user.getFirstName());
		accountContext.setLastName(user.getLastName());
		accountContext.setEmailAddress(user.getEmailAddress());
		accountContext.setConfirmEmailAddress(user.getEmailAddress());
		accountContext.setJobTitle(user.getJobTitle());
		accountContext.setEmailAddressAutoGenerated(accountEmailAddressService.isEmailAutoGenerated(user.getEmailAddress(), accountContext));
	}

}
