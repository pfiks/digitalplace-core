package com.placecube.digitalplace.notifications.smsworksnotificationservice.service;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendSmsContext;
import com.placecube.digitalplace.notifications.service.NotificationSmsConnector;
import com.placecube.digitalplace.notifications.smsworksnotificationservice.configuration.SmsWorksNotificationServiceConfiguration;

@Component(immediate = true, service = NotificationSmsConnector.class)
public class SmsWorksNotificationSmsConnector implements NotificationSmsConnector {

	private static final Log LOG = LogFactoryUtil.getLog(SmsWorksNotificationSmsConnector.class);

	@Reference
	private SmsWorksNotificationsService smsWorksNotificationsService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return smsWorksNotificationsService.isSmsEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public void sendSms(SendSmsContext sendSmsContext, ServiceContext serviceContext) throws NotificationsException, ConfigurationException {
		try (CloseableHttpClient client = HttpClients.createDefault();) {
			SmsWorksNotificationServiceConfiguration configuration = smsWorksNotificationsService.getConfiguration(serviceContext.getCompanyId());

			String content = smsWorksNotificationsService.getContentWithPlaceholderValues(sendSmsContext);
			String body = smsWorksNotificationsService.getBody(sendSmsContext, content);

			HttpPost method = smsWorksNotificationsService.getHttpPost(configuration);
			method.setEntity(smsWorksNotificationsService.getStringEntity(body));
			method.setHeader("Content-type", "application/json");
			method.addHeader("Authorization", configuration.authToken());
			HttpResponse response = client.execute(method);
			LOG.info("SMS Sent - Response code: " + response.getStatusLine().getStatusCode() + " -- " + EntityUtils.toString(response.getEntity()));
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw new NotificationsException(e);
		}
	}
}
