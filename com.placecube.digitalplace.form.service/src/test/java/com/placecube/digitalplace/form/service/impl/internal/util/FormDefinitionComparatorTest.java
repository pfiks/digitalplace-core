package com.placecube.digitalplace.form.service.impl.internal.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.placecube.digitalplace.form.model.FormDefinition;

public class FormDefinitionComparatorTest {

	private static final String CATEGORY_1 = "category1";

	private static final String CATEGORY_2 = "category2";

	private static final String NAME_1 = "name1";

	private static final String NAME_2 = "name2";

	@Mock
	private FormDefinition mockFormDefinition1;

	@Mock
	private FormDefinition mockFormDefinition2;

	@Before
	public void setUp() {

		initMocks(this);

	}

	@Test
	public void getInstance_WheNoError_ThenReturnsInstance() {

		assertNotNull(FormDefinitionComparator.getInstance());

	}

	@Test
	public void compare_WhenCategory1IsLessThanCategory2_ThenReturnsMinusOne() {

		when(mockFormDefinition1.getCategory()).thenReturn(CATEGORY_1);
		when(mockFormDefinition2.getCategory()).thenReturn(CATEGORY_2);

		int result = FormDefinitionComparator.getInstance().compare(mockFormDefinition1, mockFormDefinition2);

		assertTrue(result == -1);

	}

	@Test
	public void compare_WhenCategory1IsGreaterThanCategory2_ThenReturnsOne() {

		when(mockFormDefinition1.getCategory()).thenReturn(CATEGORY_2);
		when(mockFormDefinition2.getCategory()).thenReturn(CATEGORY_1);

		int result = FormDefinitionComparator.getInstance().compare(mockFormDefinition1, mockFormDefinition2);

		assertTrue(result == 1);

	}

	@Test
	public void compare_WhenCategoriesAreTheSameAndName1IsLessThanName2_ThenReturnsMinusOne() {

		when(mockFormDefinition1.getCategory()).thenReturn(CATEGORY_1);
		when(mockFormDefinition1.getName()).thenReturn(NAME_1);
		when(mockFormDefinition2.getCategory()).thenReturn(CATEGORY_1);
		when(mockFormDefinition2.getName()).thenReturn(NAME_2);

		int result = FormDefinitionComparator.getInstance().compare(mockFormDefinition1, mockFormDefinition2);

		assertTrue(result == -1);

	}

	@Test
	public void compare_WhenCategoriesAreTheSameAndName1IsGreaterThanName2_ThenReturnsMinusOne() {

		when(mockFormDefinition1.getCategory()).thenReturn(CATEGORY_1);
		when(mockFormDefinition1.getName()).thenReturn(NAME_2);
		when(mockFormDefinition2.getCategory()).thenReturn(CATEGORY_1);
		when(mockFormDefinition2.getName()).thenReturn(NAME_1);

		int result = FormDefinitionComparator.getInstance().compare(mockFormDefinition1, mockFormDefinition2);

		assertTrue(result == 1);

	}

	@Test
	public void compare_WhenCategoriesAreTheSameAndNamesAreEquals_ThenReturnsZero() {

		when(mockFormDefinition1.getCategory()).thenReturn(CATEGORY_1);
		when(mockFormDefinition1.getName()).thenReturn(NAME_1);
		when(mockFormDefinition2.getCategory()).thenReturn(CATEGORY_1);
		when(mockFormDefinition2.getName()).thenReturn(NAME_1);

		int result = FormDefinitionComparator.getInstance().compare(mockFormDefinition1, mockFormDefinition2);

		assertTrue(result == 0);

	}
}
