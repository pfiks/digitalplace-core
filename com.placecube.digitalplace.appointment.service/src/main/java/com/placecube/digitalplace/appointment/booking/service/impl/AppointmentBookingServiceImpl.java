package com.placecube.digitalplace.appointment.booking.service.impl;

import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.booking.registry.AppointmentsRegistry;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService;

@Component(immediate = true, service = AppointmentBookingService.class)
public class AppointmentBookingServiceImpl implements AppointmentBookingService {

	@Reference
	private AppointmentEntryLocalService appointmentEntryLocalService;

	@Reference
	private AppointmentsRegistry appointmentsRegistry;

	@Override
	public void bookAppointment(long companyId, String serviceId, AppointmentBookingEntry appointmentBookingEntry) throws PortalException {

		if (isAppointmentEntryInvalid(appointmentBookingEntry)) {
			throw new PortalException("Invalid appointment entry. Required details are classPK, classNameId, startDate, endDate, timeZone");
		}

		appointmentsRegistry.getEnabledAppointmentConnectors(companyId).stream().forEach(connector -> {
			List<String> bookedAppointmentIds = connector.bookAppointment(companyId, serviceId, appointmentBookingEntry);
			bookedAppointmentIds.stream().forEach(appointmentId -> {
				if (Validator.isNotNull(appointmentId)) {
					appointmentEntryLocalService.addAppointmentEntry(companyId, serviceId, connector.getClass().getName(), appointmentBookingEntry, appointmentId);
				}
			});
		});

	}

	@Override
	public void cancelAppointment(long companyId, String serviceId, AppointmentBookingEntry appointmentBookingEntry) {
		long classPK = appointmentBookingEntry.getClassPK();
		long classNameId = appointmentBookingEntry.getClassNameId();
		appointmentsRegistry.getEnabledAppointmentConnectors(companyId).stream().forEach(connector -> {
			List<String> cancelledAppointmentIds = connector.cancelAppointment(companyId, serviceId, appointmentBookingEntry.getAppointmentId());
			cancelledAppointmentIds.stream().forEach(appointmentId -> {
				if (Validator.isNotNull(appointmentId)) {
					appointmentEntryLocalService.removeAppointmentEntry(companyId, serviceId, connector.getClass().getName(), classPK, classNameId, appointmentId);
				}
			});
		});
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, AppointmentContext appointmentContext) {
		List<AppointmentBookingEntry> results = new LinkedList<>();
		appointmentsRegistry.getEnabledAppointmentConnectors(companyId).stream().forEach(connector -> results.addAll(connector.getAppointments(companyId, serviceId, appointmentContext)));
		return results;
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, Date startDate, Date endDate, TimeZone timeZone) {
		List<AppointmentBookingEntry> results = new LinkedList<>();
		appointmentsRegistry.getEnabledAppointmentConnectors(companyId).stream().forEach(connector -> results.addAll(connector.getAppointments(companyId, serviceId, startDate, endDate, timeZone)));
		return results;

	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, String appointmentId) {
		List<AppointmentBookingEntry> results = new LinkedList<>();
		appointmentsRegistry.getEnabledAppointmentConnectors(companyId).stream().forEach(connector -> {
			results.addAll(connector.getAppointments(companyId, serviceId, appointmentId));
		});
		return results;
	}

	@Override
	public List<AppointmentBookingSlot> getAppointmentSlots(long companyId, String serviceId, AppointmentContext appointmentContext) throws ParseException {
		List<AppointmentBookingSlot> results = new LinkedList<>();
		appointmentsRegistry.getEnabledAppointmentConnectors(companyId).stream().forEach(connector -> {
			results.addAll(connector.getAppointmentSlots(companyId, serviceId, appointmentContext));
		});
		return results;
	}

	@Override
	public List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int numberOfDatesToReturn, int dayOfWeek, TimeZone timeZone,
			int onlyDatesWithLessThanXappoinments) {
		List<Date> results = new LinkedList<>();
		appointmentsRegistry.getEnabledAppointmentConnectors(companyId).stream().forEach(connector -> {
			results.addAll(connector.getDatesWithAvailableAppoinmentsForDayOfWeek(companyId, serviceId, numberOfDatesToReturn, dayOfWeek, timeZone, onlyDatesWithLessThanXappoinments));
		});
		return results;
	}

	@Override
	public List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int dayOfWeek, TimeZone timeZone) {
		List<Date> results = new LinkedList<>();
		appointmentsRegistry.getEnabledAppointmentConnectors(companyId).stream().forEach(connector -> {
			results.addAll(connector.getDatesWithAvailableAppoinmentsForDayOfWeek(companyId, serviceId, dayOfWeek, timeZone));
		});
		return results;
	}

	private boolean isAppointmentEntryInvalid(AppointmentBookingEntry appointmentEntry) {
		return appointmentEntry.getClassPK() <= 0 || appointmentEntry.getClassNameId() <= 0 || Validator.isNull(appointmentEntry.getStartDate()) || Validator.isNull(appointmentEntry.getEndDate())
				|| Validator.isNull(appointmentEntry.getTimeZone());
	}

}
