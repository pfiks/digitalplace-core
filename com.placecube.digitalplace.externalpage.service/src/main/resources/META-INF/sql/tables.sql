create table Placecube_ExternalPage_ExternalPage (
	uuid_ VARCHAR(75) null,
	externalPageId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	url VARCHAR(200) null,
	title VARCHAR(100) null,
	content STRING null,
	status BOOLEAN
);