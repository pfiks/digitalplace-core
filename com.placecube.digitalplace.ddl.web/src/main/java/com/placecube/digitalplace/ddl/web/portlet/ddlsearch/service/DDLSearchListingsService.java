package com.placecube.digitalplace.ddl.web.portlet.ddlsearch.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.service.DynamicDataListTemplateContentService;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.model.DDLDisplayEntry;

@Component(immediate = true, service = DDLSearchListingsService.class)
public class DDLSearchListingsService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLSearchListingsService.class);

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private DynamicDataListTemplateContentService dynamicDataListTemplateContentService;

	public List<DDLDisplayEntry> getDDLDisplayEntryResults(Document[] searchResults, DDMStructure ddmStructure, Optional<DDMTemplate> ddmTemplate, ThemeDisplay themeDisplay,
			HttpServletRequest httpServletRequest) {
		List<DDLDisplayEntry> results = new LinkedList<>();

		if (ddmTemplate.isPresent()) {
			DDMTemplate displayTemplate = ddmTemplate.get();

			for (Document document : searchResults) {
				try {
					long ddlRecordId = GetterUtil.getLong(document.get(Field.ENTRY_CLASS_PK));
					String templateString = dynamicDataListTemplateContentService.getTransformedTemplate(ddlRecordId, ddmStructure, displayTemplate, themeDisplay,
							httpServletRequest);
					results.add(new DDLDisplayEntry(ddlRecordId, templateString));
				} catch (Exception e) {
					LOG.warn("Unable to retrieve DDLRecord - " + e.getMessage());
				}
			}
		}

		return results;
	}

	public String getTransformedTemplate(Document[] searchResults, DDMStructure ddmStructure, DDMTemplate ddmTemplate, ThemeDisplay themeDisplay, RenderRequest renderRequest)
			throws PortalException {

		List<AssetEntry> ddlRecords = new ArrayList<>();
		for (Document searchResult : searchResults) {
			long recordId = GetterUtil.getLong(searchResult.get("entryClassPK"));
			AssetEntry assetEntry = assetEntryLocalService.fetchEntry(DDLRecord.class.getName(), recordId);
			if (Validator.isNotNull(assetEntry)) {
				ddlRecords.add(assetEntry);
			}
		}

		return dynamicDataListTemplateContentService.getTranformedTemplate(ddlRecords, ddmStructure, ddmTemplate, themeDisplay, renderRequest);
	}

}
