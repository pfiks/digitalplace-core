package com.placecube.digitalplace.ddl.web.portlet.ddlsearch.contributor;

import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.placecube.digitalplace.ddl.constants.DDLIndexerField;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLSearchListingsConfigurationConstants;
import com.placecube.digitalplace.search.shared.constants.SharedSearchAttributeKeys;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.DDL_SEARCH_LISTINGS_PORTLET, service = SharedSearchContributor.class)
public class DDLSearchListingsPortletSharedSearchContributor implements SharedSearchContributor {

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<PortletPreferences> portletPreferencesOpt = sharedSearchContributorSettings.getPortletPreferences();
		if (portletPreferencesOpt.isPresent()) {
			PortletPreferences portletPreferences = portletPreferencesOpt.get();
			configureFilterByDDMStructureId(portletPreferences, sharedSearchContributorSettings);
			configureSearchContainerDelta(portletPreferences, sharedSearchContributorSettings);

		} else {
			throw new SystemException("Invalid portlet configuration");
		}

		sharedSearchContributorSettings.setAllowEmptySearch(true);

		sharedSearchContributorSettings.setEntryClassNames(DDLRecord.class.getName());

		long scopeGroupId = sharedSearchContributorSettings.getThemeDisplay().getScopeGroupId();
		sharedSearchContributorSettings.setSearchGroupIds(new long[] { scopeGroupId });
	}

	private void configureFilterByDDMStructureId(PortletPreferences portletPreferences, SharedSearchContributorSettings sharedSearchContributorSettings) {

		String structureId = portletPreferences.getValue(DDLSearchListingsConfigurationConstants.STRUCTURE_ID, StringPool.BLANK);
		sharedSearchContributorSettings.addBooleanQuery(DDLIndexerField.DDM_STRUCTURE_ID, structureId, BooleanClauseOccur.MUST);

	}

	private void configureSearchContainerDelta(PortletPreferences portletPreferences, SharedSearchContributorSettings sharedSearchContributorSettings) {

		String delta = portletPreferences.getValue(DDLSearchListingsConfigurationConstants.NUMBER_RESULTS_PER_PAGE, StringPool.BLANK);
		sharedSearchContributorSettings.addSharedSearchSessionSingleValuedAttribute(SharedSearchAttributeKeys.SEARCH_CONTAINER_DELTA, delta);

	}

}