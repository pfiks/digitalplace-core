<%@ include file="init.jsp" %>

<div class="container-fluid container-fluid-max-xl">
	<c:set var="currentTab" value="viewAppointments"/>
	<%@ include file="header-tabs.jspf" %>
	
	<c:choose>
		
		<c:when test="${empty availableServiceIds}">
			<div class="alert alert-info">
				<liferay-ui:message key="no-results-were-found" />
			</div>
		</c:when>
		
		<c:otherwise>
		
			<portlet:renderURL var="loadBookedAppointmentsForServiceIdRenderURL">
				<portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.VIEW_BOOKED_APPOINTMENTS%>" />
				<portlet:param name="searchExecuted" value="true" />
			</portlet:renderURL>
			
			<aui:form action="${loadBookedAppointmentsForServiceIdRenderURL}" method="post" name="loadBookedAppointmentsForServiceIdForm" cssClass="mt-2">
				<aui:select  name="serviceId" label="select-service-id" showEmptyOption="true" required="true" ignoreRequestValue="true">
					<c:forEach var="serviceIdEntry" items="${availableServiceIds}">
						<aui:option value="${serviceIdEntry}" selected="${serviceId == serviceIdEntry}">
							${serviceIdEntry}
						</aui:option>
					</c:forEach>
				</aui:select>
				
				<aui:row>
					<aui:col span="6">
						<aui:field-wrapper name="startDateValue" label="start">
							<liferay-ui:input-date name="startDate"
								dayValue="${filterStartDateTime.day}" dayParam="startDay"
								monthValue="${filterStartDateTime.month}" monthParam="startMonth"
								yearValue="${filterStartDateTime.year}" yearParam="startYear" />
						</aui:field-wrapper>
					</aui:col>
					<aui:col span="6">
						<aui:field-wrapper name="endDateValue" label="end">
							<liferay-ui:input-date name="endDate"
								dayValue="${filterEndDateTime.day}" dayParam="endDay"
								monthValue="${filterEndDateTime.month}" monthParam="endMonth"
								yearValue="${filterEndDateTime.year}" yearParam="endYear" />
						</aui:field-wrapper>
					</aui:col>
				</aui:row>
				
				<aui:button-row>
					<aui:button type="submit" value="search"/>

					<portlet:renderURL var="cancelURL">
						<portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.VIEW_BOOKED_APPOINTMENTS%>" />
					</portlet:renderURL>
					<aui:button href="${cancelURL}" type="cancel" value="clear" />
				</aui:button-row>
			</aui:form>
			
			<hr/>
			
			<c:choose>
				<c:when test="${not empty bookedAppointments}">
					<div class="h4 mb-2 mt-2">
						<liferay-ui:message key="booked-appointments-x" arguments="${bookedAppointments.size()}" />
					</div>
					<table class="table">
						<thead>
							<tr>
								<th scope="col"><liferay-ui:message key="start-date"/></th>
								<th scope="col"><liferay-ui:message key="end-date"/></th>
								<th scope="col"><liferay-ui:message key="entry-id"/></th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${bookedAppointments}" var="bookedAppointment">
								<tr>
									<td><fmt:formatDate value="${bookedAppointment.getStartDate()}" pattern="dd/MM/yyyy HH:mm" /></td>
									<td><fmt:formatDate value="${bookedAppointment.getEndDate()}" pattern="dd/MM/yyyy HH:mm" /></td>
									<td>${bookedAppointment.getClassPK()}</td>
									<td>
										<liferay-ui:icon-menu direction="left-side" markupView="lexicon" showWhenSingleIcon="false" showExpanded="true" extended="true">
				
											<portlet:actionURL var="cancelBookedAppointmentUrl" name="<%= MVCCommandKeys.CANCEL_BOOKED_APPOINTMENT %>">
												<portlet:param name="companyId" value="${themeDisplay.getCompanyId()}" />
												<portlet:param name="serviceId" value="${serviceId}" />
												<portlet:param name="appointmentId" value="${bookedAppointment.getAppointmentId()}" />
												<portlet:param name="appointmentEntryId" value="${bookedAppointment.getAppointmentEntryId()}" />
												<portlet:param name="classNameId" value="${bookedAppointment.getClassNameId()}" />
												<portlet:param name="classPK" value="${bookedAppointment.getClassPK()}" />
											</portlet:actionURL>
											<liferay-ui:icon-delete  url="${cancelBookedAppointmentUrl}" label="true" message="cancel" confirmation="are-you-sure-you-want-to-cancel-this-appointment" />
									
										</liferay-ui:icon-menu>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>
					<c:if test="${not empty serviceId}">
						<div class="alert alert-warning">
							<liferay-ui:message key="no-appointments-found" />
						</div>
					</c:if>
				</c:otherwise>
			</c:choose>
						
		</c:otherwise>
	</c:choose>
</div>