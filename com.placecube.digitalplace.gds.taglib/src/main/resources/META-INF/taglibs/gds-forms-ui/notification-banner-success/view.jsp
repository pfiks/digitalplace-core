<%@ include file="../init.jsp"%>

<c:if test="${not empty messageTitleLabel}" >
	<div class="govuk-notification-banner govuk-notification-banner--success" role="alert" aria-labelledby="${portletNamespace}govuk-notification-banner-title" data-module="govuk-notification-banner">
		<div class="govuk-notification-banner__header">
			<h2 class="govuk-notification-banner__title" id="${portletNamespace}govuk-notification-banner-title">
				<liferay-ui:message key="success"/>
			</h2>
		</div>
		<div class="govuk-notification-banner__content">
			<h3 class="govuk-notification-banner__heading">
				<liferay-ui:message key="${messageTitleLabel}"/>
			</h3>
			<c:if test="${not empty messageTextLabel}">
				<p class="govuk-body"><liferay-ui:message key="${messageTextLabel}"/></p>
			</c:if>
		</div>
	</div>
</c:if>