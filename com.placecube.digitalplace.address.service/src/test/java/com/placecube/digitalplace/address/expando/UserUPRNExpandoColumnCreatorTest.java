package com.placecube.digitalplace.address.expando;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.pfiks.expando.creator.ExpandoColumnCreatorInputStreamService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.placecube.digitalplace.address.constants.expando.UserExpandoUPRN;

public class UserUPRNExpandoColumnCreatorTest extends PowerMockito {

	@Mock
	private Company mockCompany;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Mock
	private ExpandoColumnCreatorInputStreamService mockExpandoColumnCreatorInputStreamService;

	@InjectMocks
	private UserUPRNExpandoColumnCreator userUPRNExpandoColumnCreator;

	@Test(expected = ExpandoColumnCreationException.class)
	public void create_WhenExceptionCreatingColumn_ThenThrowsExpandoColumnCreationException() throws ExpandoColumnCreationException {
		when(mockExpandoColumnCreatorInputStreamService.createExpandoColumn(same(mockCompany), same(User.class.getName()), any(InputStream.class)))
				.thenThrow(new ExpandoColumnCreationException("msg"));

		userUPRNExpandoColumnCreator.create(mockCompany);
	}

	@Test
	public void create_WhenNoError_ThenReturnsTheCreatedColumn() throws ExpandoColumnCreationException {
		when(mockExpandoColumnCreatorInputStreamService.createExpandoColumn(same(mockCompany), same(User.class.getName()), any(InputStream.class))).thenReturn(mockExpandoColumn);

		ExpandoColumn result = userUPRNExpandoColumnCreator.create(mockCompany);

		assertThat(result, sameInstance(mockExpandoColumn));
		InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("com/placecube/digitalplace/address/dependencies/expando/" + UserExpandoUPRN.FIELD_NAME + ".xml");
		assertThat(resourceAsStream, is(notNullValue()));
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

}
