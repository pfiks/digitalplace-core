package com.placecube.digitalplace.user.workflowhandler.override.internal.workflow;

import com.liferay.portal.kernel.audit.AuditRequestThreadLocal;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.permission.ResourceActionsUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.transaction.TransactionCommitCallbackUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.workflow.BaseWorkflowHandler;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandler;

import java.io.Serializable;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


@Component(property = { "model.class.name=com.liferay.portal.kernel.model.User", "service.ranking:Integer=100" }, service = WorkflowHandler.class)
public class UserWorkflowHandlerOverride extends BaseWorkflowHandler<User> {

	private static final Log LOG = LogFactoryUtil.getLog(UserWorkflowHandlerOverride.class);

	@Override
	public String getClassName() {
		return User.class.getName();
	}

	@Override
	public String getType(Locale locale) {
		return ResourceActionsUtil.getModelResource(locale, getClassName());
	}

	@Override
	public boolean isScopeable() {
		return false;
	}

	@Override
	public User updateStatus(
			int status, Map<String, Serializable> workflowContext)
		throws PortalException {

		long userId = GetterUtil.getLong(
			(String)workflowContext.get(
				WorkflowConstants.CONTEXT_ENTRY_CLASS_PK));

		User user = _userLocalService.getUser(userId);

		ServiceContext serviceContext = (ServiceContext)workflowContext.get(
			WorkflowConstants.CONTEXT_SERVICE_CONTEXT);

		if (((user.getStatus() == WorkflowConstants.STATUS_DRAFT) ||
			 (user.getStatus() == WorkflowConstants.STATUS_PENDING)) &&
			(status == WorkflowConstants.STATUS_APPROVED)) {

			// Override: Register a call after the transaction to complete the
			// user registration
			TransactionCommitCallbackUtil.registerCallback(() -> {

				_userLocalService.completeUserRegistration(_userLocalService.getUser(userId), serviceContext);

				LOG.debug("User registration completed for userId: " + userId);

				return null;
			});

			_updateAuditRequestThreadLocal(workflowContext);
		}

		return _userLocalService.updateStatus(user.getUserId(), status, serviceContext);
	}

	private void _updateAuditRequestThreadLocal(
		Map<String, Serializable> workflowContext) {

		AuditRequestThreadLocal auditRequestThreadLocal =
			AuditRequestThreadLocal.getAuditThreadLocal();

		ServiceContext serviceContext = (ServiceContext)workflowContext.get(
			WorkflowConstants.CONTEXT_SERVICE_CONTEXT);

		auditRequestThreadLocal.setClientHost(serviceContext.getRemoteHost());
		auditRequestThreadLocal.setClientIP(serviceContext.getRemoteAddr());

		long userId = GetterUtil.getLong(
			(String)workflowContext.get(WorkflowConstants.CONTEXT_USER_ID));

		if (userId != 0) {
			auditRequestThreadLocal.setRealUserId(userId);
		}

		HttpServletRequest httpServletRequest =
			_portal.getOriginalServletRequest(serviceContext.getRequest());

		if (httpServletRequest == null) {
			return;
		}

		auditRequestThreadLocal.setServerName(
			httpServletRequest.getServerName());
		auditRequestThreadLocal.setServerPort(
			httpServletRequest.getServerPort());

		HttpSession httpSession = httpServletRequest.getSession();

		if (httpSession == null) {
			return;
		}

		auditRequestThreadLocal.setSessionID(httpSession.getId());
	}

	@Reference
	private Portal _portal;

	@Reference
	private UserLocalService _userLocalService;


}