package com.placecube.digitalplace.search.web.portlet.category.configuration;

public final class CategoryFacetPortletConfigurationConstants {

	public static final String ALPHA_SORT = "alphaSort";

	public static final String CATEGORIES_QUERY_ANDED = "categoriesQueryAnded";

	public static final String CHECKBOXES_PANEL_COLLAPSED_BY_DEFAULT = "checkboxesPanelCollapsedByDefault";

	public static final String CUSTOM_CSS_CLASS = "customCssClass";

	public static final String CUSTOM_HEADING = "customHeading";

	public static final String DISPLAY_CATEGORY_IDS = "displayCategoryIds";

	public static final String DISPLAY_STYLE = "displayStyle";

	public static final String DISPLAY_VOCABOLARY_ID = "displayVocabularyIds";

	public static final String FILTER_COLOUR = "filterColour";

	public static final String INTERNAL_PORTLET_ID = "internalPortletId";

	public static final String INVISIBLE = "invisible";

	public static final String MAX_CATEGORIES_TO_SHOW = "maxCategoriesToShow";

	public static final String MAX_TREE_VIEW_DEPTH = "maxTreeViewDepth";

	public static final String PRESELECTED_CATEGORY_IDS = "preselectedCategoryIds";

	public static final String PRESELECTED_VOCABOLARY_ID = "preselectedVocabularyIds";

	public static final String SHOW_ALL_CATEGORIES_ALWAYS = "showAllCategoriesAlways";

	public static final String SHOW_FREQUENCIES = "showFrequencies";

	public static final String SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION = "showHeadingAsFirstDropdownOption";

	private CategoryFacetPortletConfigurationConstants() {
	}

}
