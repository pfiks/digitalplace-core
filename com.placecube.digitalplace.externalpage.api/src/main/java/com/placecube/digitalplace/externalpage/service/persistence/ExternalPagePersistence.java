/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.externalpage.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.externalpage.exception.NoSuchExternalPageException;
import com.placecube.digitalplace.externalpage.model.ExternalPage;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the external page service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ExternalPageUtil
 * @generated
 */
@ProviderType
public interface ExternalPagePersistence extends BasePersistence<ExternalPage> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ExternalPageUtil} to access the external page persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the external pages where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching external pages
	 */
	public java.util.List<ExternalPage> findByUuid(String uuid);

	/**
	 * Returns a range of all the external pages where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	public java.util.List<ExternalPage> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	public java.util.List<ExternalPage> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	public java.util.List<ExternalPage> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public ExternalPage findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Returns the first external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns the last external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public ExternalPage findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Returns the last external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns the external pages before and after the current external page in the ordered set where uuid = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public ExternalPage[] findByUuid_PrevAndNext(
			long externalPageId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Removes all the external pages where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of external pages where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching external pages
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the external page where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchExternalPageException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public ExternalPage findByUUID_G(String uuid, long groupId)
		throws NoSuchExternalPageException;

	/**
	 * Returns the external page where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the external page where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the external page where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the external page that was removed
	 */
	public ExternalPage removeByUUID_G(String uuid, long groupId)
		throws NoSuchExternalPageException;

	/**
	 * Returns the number of external pages where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching external pages
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching external pages
	 */
	public java.util.List<ExternalPage> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	public java.util.List<ExternalPage> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	public java.util.List<ExternalPage> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	public java.util.List<ExternalPage> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public ExternalPage findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Returns the first external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns the last external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public ExternalPage findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Returns the last external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns the external pages before and after the current external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public ExternalPage[] findByUuid_C_PrevAndNext(
			long externalPageId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Removes all the external pages where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching external pages
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @return the matching external pages
	 */
	public java.util.List<ExternalPage> findByGroupIdUrl(
		long groupId, String url);

	/**
	 * Returns a range of all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	public java.util.List<ExternalPage> findByGroupIdUrl(
		long groupId, String url, int start, int end);

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	public java.util.List<ExternalPage> findByGroupIdUrl(
		long groupId, String url, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	public java.util.List<ExternalPage> findByGroupIdUrl(
		long groupId, String url, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public ExternalPage findByGroupIdUrl_First(
			long groupId, String url,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Returns the first external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByGroupIdUrl_First(
		long groupId, String url,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns the last external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public ExternalPage findByGroupIdUrl_Last(
			long groupId, String url,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Returns the last external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByGroupIdUrl_Last(
		long groupId, String url,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns the external pages before and after the current external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public ExternalPage[] findByGroupIdUrl_PrevAndNext(
			long externalPageId, long groupId, String url,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Removes all the external pages where groupId = &#63; and url = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 */
	public void removeByGroupIdUrl(long groupId, String url);

	/**
	 * Returns the number of external pages where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @return the number of matching external pages
	 */
	public int countByGroupIdUrl(long groupId, String url);

	/**
	 * Returns all the external pages where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching external pages
	 */
	public java.util.List<ExternalPage> findByGroupId(long groupId);

	/**
	 * Returns a range of all the external pages where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	public java.util.List<ExternalPage> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	public java.util.List<ExternalPage> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	public java.util.List<ExternalPage> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public ExternalPage findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Returns the first external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns the last external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public ExternalPage findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Returns the last external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public ExternalPage fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns the external pages before and after the current external page in the ordered set where groupId = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public ExternalPage[] findByGroupId_PrevAndNext(
			long externalPageId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
				orderByComparator)
		throws NoSuchExternalPageException;

	/**
	 * Removes all the external pages where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of external pages where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching external pages
	 */
	public int countByGroupId(long groupId);

	/**
	 * Caches the external page in the entity cache if it is enabled.
	 *
	 * @param externalPage the external page
	 */
	public void cacheResult(ExternalPage externalPage);

	/**
	 * Caches the external pages in the entity cache if it is enabled.
	 *
	 * @param externalPages the external pages
	 */
	public void cacheResult(java.util.List<ExternalPage> externalPages);

	/**
	 * Creates a new external page with the primary key. Does not add the external page to the database.
	 *
	 * @param externalPageId the primary key for the new external page
	 * @return the new external page
	 */
	public ExternalPage create(long externalPageId);

	/**
	 * Removes the external page with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page that was removed
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public ExternalPage remove(long externalPageId)
		throws NoSuchExternalPageException;

	public ExternalPage updateImpl(ExternalPage externalPage);

	/**
	 * Returns the external page with the primary key or throws a <code>NoSuchExternalPageException</code> if it could not be found.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public ExternalPage findByPrimaryKey(long externalPageId)
		throws NoSuchExternalPageException;

	/**
	 * Returns the external page with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page, or <code>null</code> if a external page with the primary key could not be found
	 */
	public ExternalPage fetchByPrimaryKey(long externalPageId);

	/**
	 * Returns all the external pages.
	 *
	 * @return the external pages
	 */
	public java.util.List<ExternalPage> findAll();

	/**
	 * Returns a range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of external pages
	 */
	public java.util.List<ExternalPage> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of external pages
	 */
	public java.util.List<ExternalPage> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator);

	/**
	 * Returns an ordered range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of external pages
	 */
	public java.util.List<ExternalPage> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ExternalPage>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the external pages from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of external pages.
	 *
	 * @return the number of external pages
	 */
	public int countAll();

}