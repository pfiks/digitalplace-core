package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.service;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.search.document.Document;

@Component(immediate = true, service = DDLRelatedSearchListingsService.class)
public class DDLRelatedSearchListingsService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLRelatedSearchListingsService.class);

	@Reference
	private DDLRelatedSearchService ddlRelatedSearchService;

	public void configureRequestAttributes(HttpServletRequest httpServletRequest, DDLRecord ddlRecord, int maxItems) {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			RenderRequest renderRequest = (RenderRequest) httpServletRequest.getAttribute("javax.portlet.request");
			RenderResponse renderResponse = (RenderResponse) httpServletRequest.getAttribute("javax.portlet.response");

			List<Document> documents = ddlRelatedSearchService.getSearchResultDocuments(httpServletRequest, ddlRecord, maxItems);

			List<Object> results = ddlRelatedSearchService.getDDLDisplayEntryResults(documents, themeDisplay, httpServletRequest);

			SearchContainer<Object> searchContainer = ddlRelatedSearchService.getSearchContainer(renderRequest, renderResponse, results);

			searchContainer.setResultsAndTotal(results);

			httpServletRequest.setAttribute("hidePagination", true);
			httpServletRequest.setAttribute("searchContainer", searchContainer);
			httpServletRequest.setAttribute("displayStyleList", false);

		} catch (PortalException e) {
			LOG.error(e.getMessage());
			LOG.debug(e);
		}
	}

}
