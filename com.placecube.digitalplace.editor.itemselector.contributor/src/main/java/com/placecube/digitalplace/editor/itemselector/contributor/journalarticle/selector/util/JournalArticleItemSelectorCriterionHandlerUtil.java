package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.util;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.liferay.item.selector.ItemSelectorCriterion;
import com.liferay.item.selector.ItemSelectorView;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.JournalArticleItemSelectorCriterion;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.JournalArticleItemSelectorView;

public class JournalArticleItemSelectorCriterionHandlerUtil {

	public static List<ItemSelectorView<JournalArticleItemSelectorCriterion>> getFilteredItemSelectorViews(ItemSelectorCriterion itemSelectorCriterion,
			List<ItemSelectorView<JournalArticleItemSelectorCriterion>> itemSelectorViews) {

		JournalArticleItemSelectorCriterion infoItemItemSelectorCriterion = (JournalArticleItemSelectorCriterion) itemSelectorCriterion;

		if (Validator.isNull(infoItemItemSelectorCriterion.getItemType())) {
			return itemSelectorViews;
		}

		Stream<ItemSelectorView<JournalArticleItemSelectorCriterion>> stream = itemSelectorViews.stream();

		return stream.filter(itemSelectorView -> {
			if (!(itemSelectorView instanceof JournalArticleItemSelectorView)) {
				return false;
			}

			JournalArticleItemSelectorView journalArticleItemSelectorView = (JournalArticleItemSelectorView) itemSelectorView;

			return Objects.equals(journalArticleItemSelectorView.getClassName(), infoItemItemSelectorCriterion.getItemType());
		}).collect(Collectors.toList());
	}

}
