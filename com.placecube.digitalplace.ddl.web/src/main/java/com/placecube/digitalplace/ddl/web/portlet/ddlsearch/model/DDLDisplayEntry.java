package com.placecube.digitalplace.ddl.web.portlet.ddlsearch.model;

public class DDLDisplayEntry {

	private final String content;
	private final long recordId;

	public DDLDisplayEntry(long recordId, String content) {
		this.recordId = recordId;
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public long getRecordId() {
		return recordId;
	}

}
