package com.placecube.digitalplace.externalpage.web.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET,
		"mvc.command.name=" + MVCCommandKeys.EXTERNAL_PAGE_DELETE }, service = MVCActionCommand.class)
public class ExternalPageDeleteMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ExternalPageDeleteMVCActionCommand.class);

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException {

		long externalPageId = ParamUtil.getLong(actionRequest, "externalPageId");
		String[] deleteExternalPageIds = ParamUtil.getStringValues(actionRequest, PortletRequestKeys.ROW_IDS, externalPageId == 0 ? new String[] {} : new String[] { String.valueOf(externalPageId) });

		if (deleteExternalPageIds.length == 0) {
			throw new PortalException("No externalPageId was found in the request");
		}
		try {
			for (String deleteExternalPageId : deleteExternalPageIds) {
				externalPageLocalService.deleteExternalPage(GetterUtil.getLong(deleteExternalPageId));
			}

		} catch (PortalException e) {
			LOG.error("Error while deleting external page Id " + e.getMessage(), e);
			throw e;
		}

	}

}
