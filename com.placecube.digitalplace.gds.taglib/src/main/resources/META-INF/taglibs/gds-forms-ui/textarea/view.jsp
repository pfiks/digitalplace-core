<%@ include file="../init.jsp"%>

<c:set var="hasError" value="${not empty errorMessage}"/>

<div class="govuk-form-group ${hasError ? 'govuk-form-group--error' : ''}" id="${portletNamespace}_field_${fieldName}">
	<label class="govuk-label" for="${portletNamespace}${fieldName}">
		<liferay-ui:message key="${fieldLabel}"/>
	</label>
	
	<%@ include file="../fieldErrorMessage.jspf" %>
	
	<textarea class="govuk-textarea ${hasError ? 'govuk-textarea--error' : ''}" rows="${fieldRows}" id="${portletNamespace}${fieldName}" name="${portletNamespace}${fieldName}">${fieldValue}</textarea>
</div>
