package com.placecube.digitalplace.group.rest.application.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetTag;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestContentEntry.class, HtmlUtil.class, GetterUtil.class })
public class RestContentEntryBuilderTest extends PowerMockito {

	@InjectMocks
	private RestContentEntryBuilder restContentEntryBuilder;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private RestContentEntry mockRestContentEntry;

	@Mock
	private RestListResult mockRestListResult;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private AssetTag mockAssetTag;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private User mockUser;

	@Mock
	private RestContentUser mockRestContentUser;

	@Before
	public void activateSetUp() {
		mockStatic(RestContentEntry.class, HtmlUtil.class);
	}

	@Test(expected = PortalException.class)
	public void getDDLRecord_WhenRecordGroupIdDifferentFromGroupId_ThenThrowsPortalException() throws PortalException {
		long id = 123;
		long groupId = 345;
		when(mockDDLRecordLocalService.getDDLRecord(id)).thenReturn(mockDDLRecord);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockDDLRecord.getGroupId()).thenReturn(898989l);

		restContentEntryBuilder.getDDLRecord(mockGroup, id);
	}

	@Test
	public void getDDLRecord_WhenRecordGroupIdSameAsGroupId_ThenReturnsDDLRecord() throws PortalException {
		long id = 123;
		long groupId = 345;
		when(mockDDLRecordLocalService.getDDLRecord(id)).thenReturn(mockDDLRecord);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockDDLRecord.getGroupId()).thenReturn(groupId);

		DDLRecord result = restContentEntryBuilder.getDDLRecord(mockGroup, id);

		assertThat(result, sameInstance(mockDDLRecord));
	}

	@Test
	public void getRestContentEntryForAssetCategory_WhenNoError_ThenReturnsRestContentEntry() {
		long id = 123l;
		String title = "titleValue";
		String languageId = "languageIdValue";
		when(mockAssetCategory.getCategoryId()).thenReturn(id);
		when(mockAssetCategory.getDefaultLanguageId()).thenReturn(languageId);
		when(mockAssetCategory.getTitle(languageId)).thenReturn(title);
		when(RestContentEntry.init(id, title)).thenReturn(mockRestContentEntry);

		RestContentEntry result = restContentEntryBuilder.getRestContentEntryForAssetCategory(mockAssetCategory);

		assertThat(result, sameInstance(mockRestContentEntry));
	}

	@Test
	public void getRestContentEntryForAssetTag_WhenNoError_ThenReturnsRestContentEntry() {
		long id = 123l;
		String title = "titleValue";
		when(mockAssetTag.getTagId()).thenReturn(id);
		when(mockAssetTag.getName()).thenReturn(title);
		when(RestContentEntry.init(id, title)).thenReturn(mockRestContentEntry);

		RestContentEntry result = restContentEntryBuilder.getRestContentEntryForAssetTag(mockAssetTag);

		assertThat(result, sameInstance(mockRestContentEntry));
	}

	@Test
	public void getRestContentEntryForDDLRecordSet_WhenNoError_ThenReturnsTHeRestContentForTheRecordSet() {
		long id = 123;
		String name = "myName";
		when(mockDDLRecordSet.getRecordSetId()).thenReturn(id);
		when(mockDDLRecordSet.getDefaultLanguageId()).thenReturn("languageIdValue");
		when(mockDDLRecordSet.getName("languageIdValue")).thenReturn(name);
		when(RestContentEntry.init(id, name)).thenReturn(mockRestContentEntry);

		RestContentEntry result = restContentEntryBuilder.getRestContentEntryForDDLRecordSet(mockDDLRecordSet);

		assertThat(result, sameInstance(mockRestContentEntry));
	}

	@Test
	public void getRestContentEntryForDDMStructure_WhenNoError_ThenReturnsRestContentEntry() {
		long id = 123l;
		String title = "titleValue";
		String languageId = "languageIdValue";
		when(mockDDMStructure.getStructureId()).thenReturn(id);
		when(mockDDMStructure.getDefaultLanguageId()).thenReturn(languageId);
		when(mockDDMStructure.getName(languageId)).thenReturn(title);
		when(RestContentEntry.init(id, title)).thenReturn(mockRestContentEntry);

		RestContentEntry result = restContentEntryBuilder.getRestContentEntryForDDMStructure(mockDDMStructure);
		assertThat(result, sameInstance(mockRestContentEntry));
	}

	@Test
	public void getRestContentUser_WhenUserFoundAndIsGroupMember_ThenReturnsEntryWithUserIdAndEmailAndIsGroupMemberTrue() {
		long id = 123;
		long groupId = 456l;
		String email = "myEmail";
		when(mockUser.getUserId()).thenReturn(id);
		when(mockUser.getEmailAddress()).thenReturn(email);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockUser.getGroupIds()).thenReturn(new long[] { 11, 22, groupId });

		RestContentUser result = restContentEntryBuilder.getRestContentUser(mockGroup, mockUser, email);

		assertThat(result.getUserId(), equalTo(id));
		assertThat(result.getEmailAddress(), equalTo(email));
		assertTrue(result.isGroupMember());
	}

	@Test
	public void getRestContentUser_WhenUserFoundAndIsNotGroupMember_ThenReturnsEntryWithUserIdAndEmailAndIsGroupMemberFalse() {
		long id = 123;
		long groupId = 456l;
		String email = "myEmail";
		when(mockUser.getUserId()).thenReturn(id);
		when(mockUser.getEmailAddress()).thenReturn(email);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockUser.getGroupIds()).thenReturn(new long[] { 11, 22 });

		RestContentUser result = restContentEntryBuilder.getRestContentUser(mockGroup, mockUser, email);

		assertThat(result.getUserId(), equalTo(id));
		assertThat(result.getEmailAddress(), equalTo(email));
		assertFalse(result.isGroupMember());
	}

	@Test
	public void getRestContentUser_WhenUserNotFound_ThenReturnsEntryWithZeroAsUserIdAndEmail() {
		String email = "myEmail";

		RestContentUser result = restContentEntryBuilder.getRestContentUser(mockGroup, null, email);

		assertThat(result.getUserId(), equalTo(0l));
		assertThat(result.getEmailAddress(), equalTo(email));
		assertFalse(result.isGroupMember());
	}

	@Test
	public void getSuccessResponse_WithRestContentEntry_WhenNoError_ThenReturnsResponseWithEntityAndStatus200() {
		Response result = restContentEntryBuilder.getSuccessResponse(mockRestContentEntry);

		assertThat(result.getEntity(), sameInstance(mockRestContentEntry));
		assertThat(result.getStatus(), equalTo(200));
	}

	@Test
	public void getSuccessResponse_WithRestContentUser_WhenNoError_ThenReturnsResponseWithEntityAndStatus200() {
		Response result = restContentEntryBuilder.getSuccessResponse(mockRestContentUser);

		assertThat(result.getEntity(), sameInstance(mockRestContentUser));
		assertThat(result.getStatus(), equalTo(200));
	}

	@Test
	public void getSuccessResponse_WithRestListResult_WhenNoError_ThenReturnsResponseWithEntityAndStatus200() {
		Response result = restContentEntryBuilder.getSuccessResponse(mockRestListResult);

		assertThat(result.getEntity(), sameInstance(mockRestListResult));
		assertThat(result.getStatus(), equalTo(200));
	}

}
