package com.placecube.digitalplace.ddl.internal.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.template.TemplateConstants;

public class TemplateUtilTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 5l;
	private static final long COMPANY_GROUP_ID = 3l;
	private static final long COMPANY_ID = 2l;
	private static final long GROUP_ID = 4l;
	private static final long TEMPLATE_ID = 1l;

	@InjectMocks
	private TemplateUtil templateUtil;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getTemplateId_WhenCompanyGroupIdIsZero_ThenUseGroupIdToBuildString() {

		String expectedResult = "2#4#1";
		String result = templateUtil.getTemplateId(TEMPLATE_ID, COMPANY_ID, 0l, GROUP_ID);

		assertEquals(expectedResult, result);
	}

	@Test
	public void getTemplateId_WhenCompanyGroupIdNotZero_ThenUseItToBuildString() {

		String expectedResult = "2#3#1";
		String result = templateUtil.getTemplateId(TEMPLATE_ID, COMPANY_ID, COMPANY_GROUP_ID, GROUP_ID);

		assertEquals(expectedResult, result);
	}

	@Test
	public void getTemplatesPath_WhenMethodCalled_ThenStringConstructedCorrectly() {

		String expectedResult = TemplateConstants.TEMPLATE_SEPARATOR + "/2/4/5";
		String result = templateUtil.getTemplatesPath(COMPANY_ID, GROUP_ID, CLASS_NAME_ID);

		assertEquals(expectedResult, result);
	}
}
