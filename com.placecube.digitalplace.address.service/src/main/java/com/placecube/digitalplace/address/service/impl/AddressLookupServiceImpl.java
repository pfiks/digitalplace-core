package com.placecube.digitalplace.address.service.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.ListTypeConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.AddressLocalService;
import com.liferay.portal.kernel.service.ListTypeLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.constants.expando.UserExpandoUPRN;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookup;
import com.placecube.digitalplace.address.service.AddressLookupService;

@Component(immediate = true, service = AddressLookupService.class)
public class AddressLookupServiceImpl implements AddressLookupService {

	@Reference
	private AddressLocalService addressLocalService;

	@Reference
	private AddressLookupHelper addressLookupHelper;

	private Set<AddressLookup> addressLookups = new HashSet<>();

	@Reference
	private ListTypeLocalService listTypeLocalService;

	@Override
	public AddressContext createAddressContext(String uprn, String addressLine1, String addressLine2, String addressLine3, String addressLine4, String city, String postcode) {
		return new AddressContext(uprn, addressLine1, addressLine2, addressLine3, addressLine4, city, postcode);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<AddressContext> getByKeyword(long companyId, String keyword, String[] paramsAndValues, boolean fallbackToNationalLookup) throws PortalException {

		if (Validator.isNotNull(keyword)) {

			List<AddressLookup> localAddressLookups = getLocalAddressLookup(companyId);
			List<AddressLookup> nationalAddressLookups = fallbackToNationalLookup ? getNationalAddressLookup(companyId) : Collections.emptyList();

			addressLookupHelper.validateLookups(ListUtil.concat(localAddressLookups, nationalAddressLookups));

			return findByKeyword(companyId, keyword, paramsAndValues, localAddressLookups, nationalAddressLookups);
		}

		return Collections.emptySet();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<AddressContext> getByPostcode(long companyId, String postcode, String[] paramsAndValues, boolean fallbackToNationalLookup) throws PortalException {

		if (Validator.isNotNull(postcode)) {

			List<AddressLookup> localAddressLookups = getLocalAddressLookup(companyId);
			List<AddressLookup> nationalAddressLookups = fallbackToNationalLookup ? getNationalAddressLookup(companyId) : Collections.emptyList();

			addressLookupHelper.validateLookups(ListUtil.concat(localAddressLookups, nationalAddressLookups));

			return findByPostcode(companyId, postcode, paramsAndValues, localAddressLookups, nationalAddressLookups);
		}

		return Collections.emptySet();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Optional<AddressContext> getByUprn(long companyId, String uprn, String[] paramsAndValues, boolean fallbackToNationalLookup) throws PortalException {

		if (Validator.isNotNull(uprn)) {

			List<AddressLookup> localAddressLookups = getLocalAddressLookup(companyId);
			List<AddressLookup> nationalAddressLookups = fallbackToNationalLookup ? getNationalAddressLookup(companyId) : Collections.emptyList();

			addressLookupHelper.validateLookups(ListUtil.concat(localAddressLookups, nationalAddressLookups));

			return findByUprn(companyId, uprn, paramsAndValues, localAddressLookups, nationalAddressLookups);

		}
		return Optional.empty();

	}

	@Override
	public Address saveAddress(User user, AddressContext addressContext, ServiceContext serviceContext) throws PortalException {
		ListType listType = listTypeLocalService.getListType(serviceContext.getCompanyId(), addressContext.getAddressType().getName(), ListTypeConstants.CONTACT_ADDRESS);
		Address address = addressLocalService.addAddress(null, user.getUserId(), Contact.class.getName(), user.getContactId(), null, null, addressContext.getAddressLine1(),
				addressContext.getAddressLine2(), addressContext.getAddressLine3(), addressContext.getCity(), addressContext.getPostcode(), 0, 0, listType.getListTypeId(), false, true, null,
				serviceContext);
		user.getExpandoBridge().setAttribute(UserExpandoUPRN.FIELD_NAME, addressContext.getUPRN(), false);
		return address;
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setAddressLookup(AddressLookup addressLookup) {
		addressLookups.add(addressLookup);
	}

	protected void unsetAddressLookup(AddressLookup addressLookup) {
		addressLookups.remove(addressLookup);
	}

	private Set<AddressContext> findByKeyword(long companyId, String keyword, String[] paramsAndValues, List<AddressLookup> localAddressLookups, List<AddressLookup> nationalLookups) {

		Set<AddressContext> addressContexts = addressLookupHelper.fetchKeyword(companyId, keyword, paramsAndValues, localAddressLookups);

		if (addressContexts.isEmpty() && !nationalLookups.isEmpty()) {
			addressContexts = addressLookupHelper.fetchKeyword(companyId, keyword, paramsAndValues, nationalLookups);
		}

		return addressContexts;
	}

	private Set<AddressContext> findByPostcode(long companyId, String postcode, String[] paramsAndValues, List<AddressLookup> localAddressLookups, List<AddressLookup> nationalLookups) {

		Set<AddressContext> addressContexts = addressLookupHelper.fetchPostcode(companyId, postcode, paramsAndValues, localAddressLookups);

		if (addressContexts.isEmpty() && !nationalLookups.isEmpty()) {

			addressContexts = addressLookupHelper.fetchPostcode(companyId, postcode, paramsAndValues, nationalLookups);

		}

		return addressContexts;
	}

	private Optional<AddressContext> findByUprn(long companyId, String uprn, String[] paramsAndValues, List<AddressLookup> localAddressLookups, List<AddressLookup> nationalLookups) {

		Optional<AddressContext> addressContext = addressLookupHelper.fetchUprn(companyId, uprn, paramsAndValues, localAddressLookups);

		if (!addressContext.isPresent() && !nationalLookups.isEmpty()) {

			addressContext = addressLookupHelper.fetchUprn(companyId, uprn, paramsAndValues, nationalLookups);

		}

		return addressContext;
	}

	private List<AddressLookup> getLocalAddressLookup(long companyId) {

		return addressLookupHelper.getLocalAddressLookup(companyId, addressLookups);

	}

	private List<AddressLookup> getNationalAddressLookup(long companyId) {

		return addressLookupHelper.getNationalAddressLookup(companyId, addressLookups);
	}
}
