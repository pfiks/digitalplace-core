package com.placecube.digitalplace.appointment.booking.registry;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;

import com.placecube.digitalplace.appointment.booking.connector.AppointmentBookingConnector;

@Component(immediate = true, service = AppointmentsRegistry.class)
public class AppointmentsRegistry {

	private Set<AppointmentBookingConnector> appointmentConnectors = new LinkedHashSet<>();

	public Set<AppointmentBookingConnector> getEnabledAppointmentConnectors(long companyId) {
		return appointmentConnectors.stream().filter(appointmentConnector -> appointmentConnector.isEnabled(companyId)).collect(Collectors.toCollection(LinkedHashSet::new));
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC, policyOption = ReferencePolicyOption.GREEDY)
	protected void setAppointmentConnector(AppointmentBookingConnector appointmentConnector) {
		appointmentConnectors.add(appointmentConnector);
	}

	protected void unsetAppointmentConnector(AppointmentBookingConnector appointmentConnector) {
		appointmentConnectors.remove(appointmentConnector);
	}

}
