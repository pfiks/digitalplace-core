package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch.sortby;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.GeoDistanceSort;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.geolocation.GeoLocationPoint;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.ddl.web.constants.LocationSearchParameters;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.model.SortByOption;

@Component(immediate = true, service = { SortByGeoSpatial.class, SortByOption.class })
public class SortByGeoSpatial implements SortByOption {

	@Override
	public String getId() {
		return "sortByGeoSpatial";
	}

	@Override
	public String getLabel(Locale locale) {
		return AggregatedResourceBundleUtil.get("sort-by-geo-spatial", locale, "com.placecube.digitalplace.ddl.web");
	}

	@Override
	public Sort[] getSorts(SharedSearchContributorSettings sharedSearchContributorSettings, boolean useOppositeSortDirection) {

		String latitude = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(LocationSearchParameters.SEARCH_LATITUDE).orElse(null);
		String longitude = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(LocationSearchParameters.SEARCH_LONGITUDE).orElse(null);
		String searchField = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(LocationSearchParameters.GEO_LOCATION_FIELD).orElse(null);

		if (Validator.isNotNull(latitude) && Validator.isNotNull(longitude) && Validator.isNotNull(searchField)) {
			GeoLocationPoint geoLocationPinPoint = new GeoLocationPoint(Double.valueOf(latitude), Double.valueOf(longitude));
			GeoDistanceSort geoDistanceSort = new GeoDistanceSort();
			geoDistanceSort.setType(Sort.GEO_DISTANCE_TYPE);
			geoDistanceSort.setFieldName(searchField);
			geoDistanceSort.addGeoLocationPoint(geoLocationPinPoint);
			return new Sort[] { geoDistanceSort };
		}

		return new Sort[0];

	}

}
