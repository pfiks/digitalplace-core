package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.configuration;

public final class DDLFieldFilterPortletConfigurationConstants {

	public static final String HEADER = "header";

	public static final String INTERNAL_PORTLET_ID = "internalPortletId";

	public static final String INVISIBLE = "invisible";

	public static final String JSON_FILTER_CONFIG = "jsonFilterConfig";

	public static final String STRUCTURE_ID = "ddmStructureId";

	public static final String SHOW_GROUP_NAME_AS_FIRST_OPTION = "showGroupNameAsFirstOption";

	private DDLFieldFilterPortletConfigurationConstants() {
	}

}
