<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.util.PropsValues"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<liferay-theme:defineObjects />

<liferay-util:buffer var="html">
	<liferay-util:include page="/html/common/themes/top_head.portal.jsp" servletContext="<%= application %>" />
</liferay-util:buffer>

<%
	String customCompanyCSS = GetterUtil.getString(request.getAttribute("customCompanyCSS"), "");

	String customFaviconURL = GetterUtil.getString(request.getAttribute("customCompanyFaviconURL"), "");

	if (Validator.isNotNull(customFaviconURL)) {
		String valueToMatch = "<link href=\""+ themeDisplay.getPathThemeImages() + "/" + PropsValues.THEME_SHORTCUT_ICON +"\" rel=\"icon\" />";
		String newFavicon = "<link href=\""+ customFaviconURL +"\" rel=\"icon\" />";
		html = StringUtil.replace(html, valueToMatch, newFavicon);
	}
	
	String customCanonicalURL = GetterUtil.getString(request.getAttribute("customCanonicalURL"));
	
	if (!themeDisplay.isSignedIn() && layout.isPublicLayout() && Validator.isNotNull(customCanonicalURL)) {
		
		String completeURL = PortalUtil.getCurrentCompleteURL(request);
		String canonicalURL = PortalUtil.getCanonicalURL(completeURL, themeDisplay, layout, false, false);
		
		String canonicalURLLink = "<link data-senna-track=\"temporary\" href=\"" + canonicalURL + "\" rel=\"canonical\" />";
		String newCanonicalURLLink = "<link data-senna-track=\"temporary\" href=\"" + customCanonicalURL + "\" rel=\"canonical\" />";
		
		html = StringUtil.replace(html, canonicalURLLink, newCanonicalURLLink);
	}
%>	

<%= html %>

<%= customCompanyCSS %>
