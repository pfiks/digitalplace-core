package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorPortletKeys;

@Component(immediate = true, property = { "javax.portlet.version=3.0", "com.liferay.portlet.css-class-wrapper=portlet-two-factor-authentication",
		"com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.instanceable=false", "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,administrator", "com.liferay.portlet.add-default-resource=true", "javax.portlet.name=" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION,
		"portlet.add.default.resource.check.whitelist=" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION }, service = Portlet.class)
public class TwoFactorAuthenticationPortlet extends MVCPortlet {

}
