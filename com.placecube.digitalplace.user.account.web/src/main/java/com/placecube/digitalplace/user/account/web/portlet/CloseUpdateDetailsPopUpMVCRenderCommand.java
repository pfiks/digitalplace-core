package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.EDIT_ACCOUNT, "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.CLOSE_POPUP }, service = MVCRenderCommand.class)
public class CloseUpdateDetailsPopUpMVCRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		renderRequest.setAttribute(AccountConnectorConstant.MODEL_ID, ParamUtil.getString(renderRequest, AccountConnectorConstant.MODEL_ID));
		renderRequest.setAttribute(RequestKeys.REDIRECT_URL, ParamUtil.getString(renderRequest, RequestKeys.REDIRECT_URL));
		return "/close-update-details-popup.jsp";
	}
}
