package com.placecube.digitalplace.initialiser.service.util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.initialiser.service.constants.ConfigurationKeys;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.LayoutPageTemplateEntryInitializer;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CompanyThreadLocal.class })
public class PageTemplateCreationUtilTest extends PowerMockito {

	private static final long CLASS_NAME_ID = 123;
	private static final long GLOBAL_GROUP_ID = 3535L;
	private static final long GLOBAL_STRUCTURE_ID = 101112;
	private static final String GLOBAL_STRUCTURE_KEY = "ddmStructureGlobal";
	private static final long GROUP_ID = 456;
	private static final String STRUCTURE_DEFINITION = "structureDefinition";
	private static final long STRUCTURE_ID = 789;
	private static final String STRUCTURE_KEY = "ddmStructure";

	@Mock
	private Map<Integer, Set<PortletContext>> mockColumnsConfigs;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private Set<FragmentContext> mockFragments;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private LayoutContext mockLayoutContext;

	@Mock
	private LayoutPageTemplateCollection mockLayoutPageTemplateCollection;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Mock
	private LayoutPageTemplateEntryInitializer mockLayoutPageTemplateEntryInitializer;

	@Mock
	private PageCreationUtil mockPageCreationUtil;

	@Mock
	private Portal mockPortal;

	@Mock
	private Set<PortletContext> mockPortlets;

	@Mock
	private Map<String, String> mockPreferencesPlaceholderValues;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private JSONArray mockTemplateJSONArray1;

	@Mock
	private JSONArray mockTemplateJSONArray2;

	@Mock
	private JSONObject mockTemplateJSONContentStructureDefinition;

	@Mock
	private JSONObject mockTemplateJSONObject;

	@InjectMocks
	private PageTemplateCreationUtil pageTemplateCreationUtil;

	@Test(expected = PortalException.class)
	public void createContentPageTemplate_WhenErrorCreatingCollection_ThenThrowsPortalException() throws PortalException {
		mockLayoutContextConfiguration();
		mockTemplateCollection(true);

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);

		pageTemplateCreationUtil.createContentPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);
	}

	@Test()
	public void createContentPageTemplate_WhenNoError_ThenConfiguresLayoutContext() throws PortalException {
		mockLayoutContextConfiguration();
		mockTemplateCollection(false);

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);

		pageTemplateCreationUtil.createContentPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);

		verify(mockLayoutContext, times(1)).setPortletContexts(mockPortlets);
		verify(mockLayoutContext, times(1)).setFragmentContexts(mockFragments);
		verify(mockLayoutContext, times(1)).setStructureContent(STRUCTURE_DEFINITION);
	}

	@Test()
	public void createContentPageTemplate_WhenNoError_ThenReturnsCreatedPageTemplate() throws PortalException {
		mockLayoutContextConfiguration();
		mockTemplateCollection(false);

		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);

		when(mockLayoutPageTemplateEntryInitializer.createContentPageTemplate(mockLayoutPageTemplateCollection, mockLayoutContext, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = pageTemplateCreationUtil.createContentPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);

		assertThat(result, equalTo(mockLayoutPageTemplateEntry));
	}

	@Test(expected = PortalException.class)
	public void createDisplayPageTemplate_WhenErrorGettingStructure_ThenThrowsPortalException() throws PortalException {
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockTemplateJSONObject.getString("mappingWebContentStructure")).thenReturn(STRUCTURE_KEY);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, CLASS_NAME_ID, STRUCTURE_KEY)).thenThrow(new PortalException(""));

		pageTemplateCreationUtil.createDisplayPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);
	}

	@Test()
	public void createDisplayPageTemplate_WhenNoError_ThenConfiguresLayoutContext() throws PortalException {
		mockLayoutContextConfiguration();

		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockTemplateJSONObject.getString("mappingWebContentStructure")).thenReturn(STRUCTURE_KEY);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, CLASS_NAME_ID, STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);

		pageTemplateCreationUtil.createDisplayPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);

		verify(mockLayoutContext, times(1)).setPortletContexts(mockPortlets);
		verify(mockLayoutContext, times(1)).setFragmentContexts(mockFragments);
		verify(mockLayoutContext, times(1)).setStructureContent(STRUCTURE_DEFINITION);
	}

	@Test()
	public void createDisplayPageTemplate_WhenNoErrorAndGlobalStructureIsFalse_ThenReturnsCreatedPageTemplateWithSiteStructure() throws PortalException {
		mockLayoutContextConfiguration();

		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockTemplateJSONObject.getBoolean("globalStructure")).thenReturn(false);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockTemplateJSONObject.getString("mappingWebContentStructure")).thenReturn(STRUCTURE_KEY);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, CLASS_NAME_ID, STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockLayoutPageTemplateEntryInitializer.createDisplayPageTemplate(mockLayoutContext, CLASS_NAME_ID, STRUCTURE_ID, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = pageTemplateCreationUtil.createDisplayPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);

		assertThat(result, equalTo(mockLayoutPageTemplateEntry));
	}

	@Test()
	public void createDisplayPageTemplate_WhenNoErrorAndGlobalStructureIsTrue_ThenReturnsCreatedPageTemplateWithGlobalStructure() throws PortalException {
		mockLayoutContextConfiguration();

		when(CompanyThreadLocal.getCompanyId()).thenReturn(GLOBAL_GROUP_ID);
		when(mockGroup.getGroupId()).thenReturn(GLOBAL_GROUP_ID);
		when(mockGroupLocalService.getCompanyGroup(CompanyThreadLocal.getCompanyId())).thenReturn(mockGroup);

		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockTemplateJSONObject.getBoolean("globalStructure")).thenReturn(true);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GLOBAL_GROUP_ID);
		when(mockTemplateJSONObject.getString("mappingWebContentStructure")).thenReturn(GLOBAL_STRUCTURE_KEY);
		when(mockDDMStructureLocalService.getStructure(GLOBAL_GROUP_ID, CLASS_NAME_ID, GLOBAL_STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(GLOBAL_STRUCTURE_ID);
		when(mockLayoutPageTemplateEntryInitializer.createDisplayPageTemplate(mockLayoutContext, CLASS_NAME_ID, GLOBAL_STRUCTURE_ID, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = pageTemplateCreationUtil.createDisplayPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);

		assertThat(result, equalTo(mockLayoutPageTemplateEntry));
	}

	@Test(expected = PortalException.class)
	public void createWidgetPageTemplate_WhenErrorCreatingCollection_ThenThrowsPortalException() throws PortalException {
		mockTemplateCollection(true);

		when(mockTemplateJSONObject.getJSONArray("columns")).thenReturn(mockTemplateJSONArray1);
		when(mockPageCreationUtil.getColumns(mockTemplateJSONArray1, mockPreferencesPlaceholderValues, ConfigurationKeys.DEFAULT)).thenReturn(mockColumnsConfigs);

		pageTemplateCreationUtil.createWidgetPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);
	}

	@Test()
	public void createWidgetPageTemplate_WhenNoError_ThenConfiguresLayoutContext() throws PortalException {
		mockTemplateCollection(false);

		when(mockTemplateJSONObject.getJSONArray("columns")).thenReturn(mockTemplateJSONArray1);
		when(mockPageCreationUtil.getColumns(mockTemplateJSONArray1, mockPreferencesPlaceholderValues, ConfigurationKeys.DEFAULT)).thenReturn(mockColumnsConfigs);

		pageTemplateCreationUtil.createWidgetPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);

		verify(mockLayoutContext, times(1)).setColumnPortletContexts(mockColumnsConfigs);
	}

	@Test()
	public void createWidgetPageTemplate_WhenNoError_ThenReturnsCreatedPageTemplate() throws PortalException {
		mockTemplateCollection(false);

		when(mockTemplateJSONObject.getJSONArray("columns")).thenReturn(mockTemplateJSONArray1);
		when(mockPageCreationUtil.getColumns(mockTemplateJSONArray1, mockPreferencesPlaceholderValues, ConfigurationKeys.DEFAULT)).thenReturn(mockColumnsConfigs);
		when(mockLayoutPageTemplateEntryInitializer.createWidgetPageTemplate(mockLayoutPageTemplateCollection, mockLayoutContext, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = pageTemplateCreationUtil.createWidgetPageTemplate(mockLayoutContext, mockTemplateJSONObject, mockPreferencesPlaceholderValues, mockServiceContext);

		assertThat(result, equalTo(mockLayoutPageTemplateEntry));
	}

	@Before
	public void setUp() {
		mockStatic(CompanyThreadLocal.class);
	}

	private void mockLayoutContextConfiguration() {
		when(mockTemplateJSONObject.getJSONArray("fragments")).thenReturn(mockTemplateJSONArray1);
		when(mockPageCreationUtil.getFragments(mockTemplateJSONArray1)).thenReturn(mockFragments);
		when(mockTemplateJSONObject.getJSONArray("portlets")).thenReturn(mockTemplateJSONArray2);
		when(mockPageCreationUtil.getPortlets(GROUP_ID, mockTemplateJSONArray2, null, mockPreferencesPlaceholderValues, ConfigurationKeys.CONTENT_OR_DISPLAY_PAGE_TEMPLATE)).thenReturn(mockPortlets);
		when(mockTemplateJSONObject.getJSONObject("contentStructureDefinition")).thenReturn(mockTemplateJSONContentStructureDefinition);
		when(mockTemplateJSONContentStructureDefinition.toJSONString()).thenReturn(STRUCTURE_DEFINITION);
	}

	private void mockTemplateCollection(boolean throwException) throws PortalException {
		if (throwException) {
			when(mockPageCreationUtil.getLayoutPageTemplateCollection(mockTemplateJSONObject, mockServiceContext)).thenThrow(new PortalException(""));
		} else {
			when(mockPageCreationUtil.getLayoutPageTemplateCollection(mockTemplateJSONObject, mockServiceContext)).thenReturn(mockLayoutPageTemplateCollection);
		}
	}
}
