package com.placecube.digitalplace.form.service.impl.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.service.permission.ModelPermissions;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DDMFormInstanceServiceTest {

	private static final long DDM_FORM_INSTANCE_ID = 4;

	private static final long GROUP_ID = 2;

	private static final long USER_ID = 1;

	private static final String UUID = "uuid";

	private static final String VERSION_DEFAULT = "1.0";

	@InjectMocks
	private DDMFormInstanceService ddmFormInstanceService;

	@Mock
	protected CounterLocalService mockCounterLocalService;

	@Mock
	protected DDMFormInstanceLocalService mockDDMFormInstanceLocalService;

	@Mock
	protected DDMFormInstanceVersionService mockDDMFormInstanceVersionService;

	@Mock
	protected FormDefinitionDeserializerService mockFormDefinitionDeserializerService;

	@Mock
	protected UserLocalService mockUserLocalService;

	@Mock
	protected WorkflowDefinitionService mockWorkflowDefinitionService;

	@Mock
	private DDMFormValues mockSettingsDDMFormValues;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private Map<Locale, String> mockDescriptionMap;

	@Mock
	private Map<Locale, String> mockNameMap;

	@Mock
	private DDMForm mockDDMForm;

	@Mock
	private User mockUser;

	@Mock
	private DDMFormInstance mockDDMFormInstance;

	@Mock
	private DDMFormInstance mockDDMFormInstanceUpdated;

	@Mock
	private ModelPermissions mockModelPermissions;

	@Before
	public void setUp() {

		initMocks(this);

	}

	@Test(expected = PortalException.class)
	public void addFormInstance_WhenErrorGettingUser_ThenPortalExceptionIsThrown() throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenThrow(new PortalException());

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void addFormInstance_WhenErrorUpdatingWorkflowDefinitionLink_ThenPortalExceptionIsThrown() throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstance);
		doThrow(new PortalException()).when(mockWorkflowDefinitionService).updateWorkflowDefinitionLink(mockDDMFormInstance, mockSettingsDDMFormValues, mockServiceContext);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "false,true", "true,false", "true,true" })
	public void addFormInstance_WhenErrorAddingFormInstanceResourcesForGroupAndGuest_ThenPortalExceptionIsThrown(boolean isAddGroupPermissions, boolean isAddGuestPermissions) throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstance);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(isAddGroupPermissions);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(isAddGuestPermissions);
		doThrow(new PortalException()).when(mockDDMFormInstanceLocalService).addFormInstanceResources(mockDDMFormInstance, isAddGroupPermissions, isAddGuestPermissions);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void addFormInstance_WhenErrorAddingFormInstanceResources_ThenPortalExceptionIsThrown() throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstance);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(false);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(false);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);
		doThrow(new PortalException()).when(mockDDMFormInstanceLocalService).addFormInstanceResources(mockDDMFormInstance, mockModelPermissions);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void addFormInstance_WhenErrorAddingFormInstanceVersion_ThenPortalExceptionIsThrown() throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstance);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(false);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(false);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);
		doThrow(new PortalException()).when(mockDDMFormInstanceVersionService).addFormInstanceVersion(mockDDMStructure, mockUser, mockDDMFormInstance, VERSION_DEFAULT, mockServiceContext);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);
	}

	@Test
	@Parameters({ "false,true", "true,false", "true,true" })
	public void addFormInstance_WhenNoError_ThenReturnsFormInstanceUpdated(boolean isAddGroupPermissions, boolean isAddGuestPermissions) throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstanceUpdated);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(isAddGroupPermissions);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(isAddGuestPermissions);

		DDMFormInstance result = ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);

		assertThat(result, equalTo(mockDDMFormInstanceUpdated));
	}

	@Test
	public void addFormInstance_WhenNoErrorAndAddModelPermissions_ThenReturnsFormInstanceUpdated() throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstanceUpdated);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(false);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(false);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		DDMFormInstance result = ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);

		assertThat(result, equalTo(mockDDMFormInstanceUpdated));
	}

	@Test
	@Parameters({ "false,true", "true,false", "true,true" })
	public void addFormInstance_WhenNoErrorAndAddGroupPermissionOrAddGuestPermission_ThenSetFormInstanceValues(boolean isAddGroupPermissions, boolean isAddGuestPermissions) throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockServiceContext.getUuid()).thenReturn(UUID);
		long companyId = 1;
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		String fullName = "fullName";
		when(mockUser.getFullName()).thenReturn(fullName);
		long structureId = 33;
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		String settings = "settings";
		when(mockFormDefinitionDeserializerService.serialize(mockSettingsDDMFormValues)).thenReturn(settings);
		// when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(isAddGroupPermissions);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(isAddGuestPermissions);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);

		InOrder inOrder = Mockito.inOrder(mockDDMFormInstance, mockFormDefinitionDeserializerService, mockWorkflowDefinitionService, mockDDMFormInstanceLocalService,
				mockDDMFormInstanceVersionService);
		inOrder.verify(mockDDMFormInstance, times(1)).setUuid(UUID);
		inOrder.verify(mockDDMFormInstance, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockDDMFormInstance, times(1)).setCompanyId(companyId);
		inOrder.verify(mockDDMFormInstance, times(1)).setUserId(USER_ID);
		inOrder.verify(mockDDMFormInstance, times(1)).setUserName(fullName);
		inOrder.verify(mockDDMFormInstance, times(1)).setStructureId(structureId);
		inOrder.verify(mockDDMFormInstance, times(1)).setVersion(VERSION_DEFAULT);
		inOrder.verify(mockDDMFormInstance, times(1)).setNameMap(mockNameMap, Locale.UK);
		inOrder.verify(mockDDMFormInstance, times(1)).setDescriptionMap(mockDescriptionMap, Locale.UK);
		inOrder.verify(mockDDMFormInstance, times(1)).setSettings(settings);
		inOrder.verify(mockWorkflowDefinitionService, times(1)).updateWorkflowDefinitionLink(mockDDMFormInstance, mockSettingsDDMFormValues, mockServiceContext);
		inOrder.verify(mockDDMFormInstanceLocalService, times(1)).addFormInstanceResources(mockDDMFormInstance, isAddGroupPermissions, isAddGuestPermissions);
		inOrder.verify(mockDDMFormInstanceVersionService, times(1)).addFormInstanceVersion(mockDDMStructure, mockUser, mockDDMFormInstance, VERSION_DEFAULT, mockServiceContext);

	}

	@Test
	public void addFormInstance_WhenNoErrorAndAddModelPermissions_ThenSetFormInstanceValues() throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockServiceContext.getUuid()).thenReturn(UUID);
		long companyId = 1;
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		String fullName = "fullName";
		when(mockUser.getFullName()).thenReturn(fullName);
		long structureId = 33;
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		String settings = "settings";
		when(mockFormDefinitionDeserializerService.serialize(mockSettingsDDMFormValues)).thenReturn(settings);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(false);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(false);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);

		InOrder inOrder = Mockito.inOrder(mockDDMFormInstance, mockFormDefinitionDeserializerService, mockWorkflowDefinitionService, mockDDMFormInstanceLocalService,
				mockDDMFormInstanceVersionService);
		inOrder.verify(mockDDMFormInstance, times(1)).setUuid(UUID);
		inOrder.verify(mockDDMFormInstance, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockDDMFormInstance, times(1)).setCompanyId(companyId);
		inOrder.verify(mockDDMFormInstance, times(1)).setUserId(USER_ID);
		inOrder.verify(mockDDMFormInstance, times(1)).setUserName(fullName);
		inOrder.verify(mockDDMFormInstance, times(1)).setStructureId(structureId);
		inOrder.verify(mockDDMFormInstance, times(1)).setVersion(VERSION_DEFAULT);
		inOrder.verify(mockDDMFormInstance, times(1)).setNameMap(mockNameMap, Locale.UK);
		inOrder.verify(mockDDMFormInstance, times(1)).setDescriptionMap(mockDescriptionMap, Locale.UK);
		inOrder.verify(mockDDMFormInstance, times(1)).setSettings(settings);
		inOrder.verify(mockWorkflowDefinitionService, times(1)).updateWorkflowDefinitionLink(mockDDMFormInstance, mockSettingsDDMFormValues, mockServiceContext);
		inOrder.verify(mockDDMFormInstanceLocalService, times(1)).addFormInstanceResources(mockDDMFormInstance, mockModelPermissions);
		inOrder.verify(mockDDMFormInstanceVersionService, times(1)).addFormInstanceVersion(mockDDMStructure, mockUser, mockDDMFormInstance, VERSION_DEFAULT, mockServiceContext);

	}

	@Test
	public void addFormInstance_WhenNoError_ThenUpdateWorkflowDefinitionLink() throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstanceUpdated);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(false);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(false);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);

		verify(mockWorkflowDefinitionService, times(1)).updateWorkflowDefinitionLink(mockDDMFormInstance, mockSettingsDDMFormValues, mockServiceContext);
	}

	@Test
	@Parameters({ "true,true", "false,true", "true,false" })
	public void addFormInstance_WhenNoError_ThenAddFormInstanceResourcesForGuestAndGroup(boolean isAddGroupPermissions, boolean isAddGuestPermissions) throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstanceUpdated);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(isAddGroupPermissions);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(isAddGuestPermissions);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);

		verify(mockDDMFormInstanceLocalService, times(1)).addFormInstanceResources(mockDDMFormInstance, isAddGroupPermissions, isAddGuestPermissions);
	}

	@Test
	public void addFormInstance_WhenNoError_ThenAddFormInstanceResources() throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstanceUpdated);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(false);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(false);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);

		verify(mockDDMFormInstanceLocalService, times(1)).addFormInstanceResources(mockDDMFormInstance, mockModelPermissions);
	}

	@Test
	public void addFormInstance_WhenNoError_ThenAddFormInstanceVersion() throws Exception {

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_ID);
		when(mockDDMFormInstanceLocalService.createDDMFormInstance(DDM_FORM_INSTANCE_ID)).thenReturn(mockDDMFormInstance);
		when(mockDDMFormInstanceLocalService.updateDDMFormInstance(mockDDMFormInstance)).thenReturn(mockDDMFormInstanceUpdated);
		when(mockServiceContext.isAddGroupPermissions()).thenReturn(false);
		when(mockServiceContext.isAddGuestPermissions()).thenReturn(false);
		when(mockServiceContext.getModelPermissions()).thenReturn(mockModelPermissions);

		ddmFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, mockNameMap, mockDescriptionMap, mockSettingsDDMFormValues, mockServiceContext);

		verify(mockDDMFormInstanceVersionService, times(1)).addFormInstanceVersion(mockDDMStructure, mockUser, mockDDMFormInstance, VERSION_DEFAULT, mockServiceContext);
	}

}
