package com.placecube.digitalplace.pageimportadmin.model;

import java.util.List;
import java.util.Optional;

import com.liferay.dynamic.data.mapping.model.DDMStructure;

public class WebContentImportModel extends RowImportModel {

	private Optional<DDMStructure> webContentStructure;

	public WebContentImportModel(long groupId, int index, String title, String url, List<String> categoriesNames) {
		super(groupId, index, title, url, categoriesNames);
	}

	public Optional<DDMStructure> getWebContentStructure() {
		return webContentStructure;
	}

	public void setWebContentStructure(Optional<DDMStructure> webContentStructure) {
		this.webContentStructure = webContentStructure;
	}

}
