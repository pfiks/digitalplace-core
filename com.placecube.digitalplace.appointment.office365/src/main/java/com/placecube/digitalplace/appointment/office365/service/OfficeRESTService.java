package com.placecube.digitalplace.appointment.office365.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TimeZone;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.office365.configuration.AppointmentOffice365CompanyConfiguration;
import com.placecube.digitalplace.appointment.office365.constants.Office365Constants;
import com.placecube.digitalplace.appointment.office365.model.AppointmentConfiguredService;

@Component(immediate = true, service = OfficeRESTService.class)
public class OfficeRESTService {

	private static final Log LOG = LogFactoryUtil.getLog(OfficeRESTService.class);

	@Reference
	private ParsingUtils parsingUtils;

	@Reference
	private RestCallUtil restCallUtil;

	public String cancelEvent(AppointmentConfiguredService appointmentConfiguredService, String appointmentId, String authToken) {
		try {
			String createEventURL = Office365Constants.getCancelEventURL(appointmentConfiguredService.getUserId(), appointmentConfiguredService.getCalendarId(), appointmentId);
			String responseString = restCallUtil.executePostCallWithAuthenticationBearer(createEventURL, StringPool.BLANK, authToken);
			if (Validator.isNull(responseString)) {
				LOG.info("Event with id: " + appointmentId + " has been cancelled from O365 calendar: " + appointmentConfiguredService.getCalendarId());
				return appointmentId;
			}
		} catch (Exception e) {
			LOG.error("Unable to cancel event", e);
		}
		return StringPool.BLANK;
	}

	public String createEvent(AppointmentConfiguredService appointmentConfiguredService, AppointmentBookingEntry appointmentEntry, String authToken) {
		try {
			String createEventURL = Office365Constants.getCreateEventURL(appointmentConfiguredService.getUserId(), appointmentConfiguredService.getCalendarId());
			String requestBody = parsingUtils.getJsonStringFromAppointmentEntry(appointmentEntry);
			String responseString = restCallUtil.executePostCallWithAuthenticationBearer(createEventURL, requestBody, authToken);
			String appointmentId = parsingUtils.getAppointmentIdFromResponse(responseString);
			LOG.info("Event with id: " + appointmentId + " has been created in O365 calendar: " + appointmentConfiguredService.getCalendarId());
			return appointmentId;
		} catch (Exception e) {
			LOG.error("Unable to create event", e);
			return StringPool.BLANK;
		}
	}

	public String getAuthenticationToken(AppointmentOffice365CompanyConfiguration configuration) throws PortalException {
		String tokenRequestURL = Office365Constants.getTokenRequestURL(configuration.tenantId());
		String tokenRequestBody = Office365Constants.getTokenRequestBody(configuration.applicationId(), configuration.applicationSecret());
		String responseString = restCallUtil.executePostCallWithFormEncodedBody(tokenRequestURL, tokenRequestBody);

		return parsingUtils.getAccessTokenFromResponse(responseString);
	}

	public List<AppointmentBookingEntry> getCalendarEvents(AppointmentConfiguredService appointmentConfiguredService, String formattedStartDateTime, String formattedEndDateTime, TimeZone timeZone,
			String authToken) {
		List<AppointmentBookingEntry> results = new LinkedList<>();
		try {
			String searchEventsURL = Office365Constants.getSearchEventsURL(appointmentConfiguredService.getUserId(), appointmentConfiguredService.getCalendarId(), formattedStartDateTime,
					formattedEndDateTime, 200, "id,subject,bodyPreview,start,end");

			String responseString = restCallUtil.executeGetCall(searchEventsURL, authToken);

			JSONArray calendarEvents = parsingUtils.getValuesJsonArrayFromResponse(responseString);
			for (int i = 0; i < calendarEvents.length(); i++) {
				JSONObject eventJson = calendarEvents.getJSONObject(i);
				results.add(parsingUtils.getAppointmentEntry(eventJson));
			}

		} catch (Exception e) {
			LOG.error("Unable to retrieve calendar events", e);
		}
		return results;
	}

	public List<LocalDateTime> getDatesWithAvailableAppointmentsForMatchingDates(AppointmentConfiguredService appointmentConfiguredService, String formattedStartDateTime,
			String formattedEndDateTime, List<LocalDateTime> availableDatesToMatch, int onlyDatesWithLessThanXappoinments, String authToken) {
		try {

			int entriesToReturn = availableDatesToMatch.size() * (onlyDatesWithLessThanXappoinments + 3) * 7;
			String searchEventsURL = Office365Constants.getSearchEventsURL(appointmentConfiguredService.getUserId(), appointmentConfiguredService.getCalendarId(), formattedStartDateTime,
					formattedEndDateTime, entriesToReturn, "start");
			String responseString = restCallUtil.executeGetCall(searchEventsURL, authToken);

			JSONArray calendarEvents = parsingUtils.getValuesJsonArrayFromResponse(responseString);

			Map<LocalDateTime, Integer> appointmentsOnDates = new HashMap<>();

			for (int i = 0; i < calendarEvents.length(); i++) {
				JSONObject eventJson = calendarEvents.getJSONObject(i);

				LocalDateTime startDateFromAppointmentEntry = parsingUtils.getStartDateFromAppointmentEntryTruncatedToDays(eventJson);
				if (availableDatesToMatch.contains(startDateFromAppointmentEntry)) {
					appointmentsOnDates.put(startDateFromAppointmentEntry, appointmentsOnDates.getOrDefault(startDateFromAppointmentEntry, 0) + 1);
				}
			}

			for (Entry<LocalDateTime, Integer> entry : appointmentsOnDates.entrySet()) {
				if (entry.getValue() >= onlyDatesWithLessThanXappoinments) {
					availableDatesToMatch.remove(entry.getKey());
				}
			}

		} catch (Exception e) {
			LOG.error("Unable to retrieve dates for available appointments", e);
		}
		return availableDatesToMatch;
	}

	public Optional<AppointmentBookingEntry> getEvent(AppointmentConfiguredService appointmentConfiguredService, String appointmentId, String authToken) {
		try {
			String getEventURL = Office365Constants.getRetrieveEventURL(appointmentConfiguredService.getUserId(), appointmentConfiguredService.getCalendarId(), appointmentId);
			String responseString = restCallUtil.executeGetCall(getEventURL, authToken);
			return Optional.ofNullable(parsingUtils.getAppointmentEntryFromResponse(responseString));
		} catch (Exception e) {
			LOG.error("Unable to retrieve event", e);
			return Optional.empty();
		}
	}

}
