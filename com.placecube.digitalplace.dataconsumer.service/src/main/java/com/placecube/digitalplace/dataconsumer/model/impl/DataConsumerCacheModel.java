/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.dataconsumer.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.dataconsumer.model.DataConsumer;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing DataConsumer in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class DataConsumerCacheModel
	implements CacheModel<DataConsumer>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DataConsumerCacheModel)) {
			return false;
		}

		DataConsumerCacheModel dataConsumerCacheModel =
			(DataConsumerCacheModel)object;

		if (dataConsumerId == dataConsumerCacheModel.dataConsumerId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, dataConsumerId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", dataConsumerId=");
		sb.append(dataConsumerId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", ddmFormInstanceId=");
		sb.append(ddmFormInstanceId);
		sb.append(", ddmDataProviderInstanceId=");
		sb.append(ddmDataProviderInstanceId);
		sb.append(", sourceDataProviderId=");
		sb.append(sourceDataProviderId);
		sb.append(", method=");
		sb.append(method);
		sb.append(", mapping=");
		sb.append(mapping);
		sb.append(", headers=");
		sb.append(headers);
		sb.append(", chain=");
		sb.append(chain);
		sb.append(", body=");
		sb.append(body);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DataConsumer toEntityModel() {
		DataConsumerImpl dataConsumerImpl = new DataConsumerImpl();

		if (uuid == null) {
			dataConsumerImpl.setUuid("");
		}
		else {
			dataConsumerImpl.setUuid(uuid);
		}

		dataConsumerImpl.setDataConsumerId(dataConsumerId);
		dataConsumerImpl.setGroupId(groupId);
		dataConsumerImpl.setCompanyId(companyId);
		dataConsumerImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			dataConsumerImpl.setCreateDate(null);
		}
		else {
			dataConsumerImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			dataConsumerImpl.setModifiedDate(null);
		}
		else {
			dataConsumerImpl.setModifiedDate(new Date(modifiedDate));
		}

		dataConsumerImpl.setDdmFormInstanceId(ddmFormInstanceId);
		dataConsumerImpl.setDdmDataProviderInstanceId(
			ddmDataProviderInstanceId);
		dataConsumerImpl.setSourceDataProviderId(sourceDataProviderId);

		if (method == null) {
			dataConsumerImpl.setMethod("");
		}
		else {
			dataConsumerImpl.setMethod(method);
		}

		if (mapping == null) {
			dataConsumerImpl.setMapping("");
		}
		else {
			dataConsumerImpl.setMapping(mapping);
		}

		if (headers == null) {
			dataConsumerImpl.setHeaders("");
		}
		else {
			dataConsumerImpl.setHeaders(headers);
		}

		dataConsumerImpl.setChain(chain);

		if (body == null) {
			dataConsumerImpl.setBody("");
		}
		else {
			dataConsumerImpl.setBody(body);
		}

		dataConsumerImpl.resetOriginalValues();

		return dataConsumerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		dataConsumerId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		ddmFormInstanceId = objectInput.readLong();

		ddmDataProviderInstanceId = objectInput.readLong();

		sourceDataProviderId = objectInput.readLong();
		method = objectInput.readUTF();
		mapping = objectInput.readUTF();
		headers = objectInput.readUTF();

		chain = objectInput.readInt();
		body = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(dataConsumerId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(ddmFormInstanceId);

		objectOutput.writeLong(ddmDataProviderInstanceId);

		objectOutput.writeLong(sourceDataProviderId);

		if (method == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(method);
		}

		if (mapping == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(mapping);
		}

		if (headers == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(headers);
		}

		objectOutput.writeInt(chain);

		if (body == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(body);
		}
	}

	public String uuid;
	public long dataConsumerId;
	public long groupId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long ddmFormInstanceId;
	public long ddmDataProviderInstanceId;
	public long sourceDataProviderId;
	public String method;
	public String mapping;
	public String headers;
	public int chain;
	public String body;

}