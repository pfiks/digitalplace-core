/**
 * ScpResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class ScpResponse  implements java.io.Serializable {
    private org.apache.axis.types.Token requestId;

    private java.lang.String scpReference;

    private com.capita_software_services.www.scp.base.TransactionState transactionState;

    public ScpResponse() {
    }

    public ScpResponse(
           org.apache.axis.types.Token requestId,
           java.lang.String scpReference,
           com.capita_software_services.www.scp.base.TransactionState transactionState) {
           this.requestId = requestId;
           this.scpReference = scpReference;
           this.transactionState = transactionState;
    }


    /**
     * Gets the requestId value for this ScpResponse.
     *
     * @return requestId
     */
    public org.apache.axis.types.Token getRequestId() {
        return requestId;
    }


    /**
     * Sets the requestId value for this ScpResponse.
     *
     * @param requestId
     */
    public void setRequestId(org.apache.axis.types.Token requestId) {
        this.requestId = requestId;
    }


    /**
     * Gets the scpReference value for this ScpResponse.
     *
     * @return scpReference
     */
    public java.lang.String getScpReference() {
        return scpReference;
    }


    /**
     * Sets the scpReference value for this ScpResponse.
     *
     * @param scpReference
     */
    public void setScpReference(java.lang.String scpReference) {
        this.scpReference = scpReference;
    }


    /**
     * Gets the transactionState value for this ScpResponse.
     *
     * @return transactionState
     */
    public com.capita_software_services.www.scp.base.TransactionState getTransactionState() {
        return transactionState;
    }


    /**
     * Sets the transactionState value for this ScpResponse.
     *
     * @param transactionState
     */
    public void setTransactionState(com.capita_software_services.www.scp.base.TransactionState transactionState) {
        this.transactionState = transactionState;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScpResponse)) return false;
        ScpResponse other = (ScpResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.requestId==null && other.getRequestId()==null) ||
             (this.requestId!=null &&
              this.requestId.equals(other.getRequestId()))) &&
            ((this.scpReference==null && other.getScpReference()==null) ||
             (this.scpReference!=null &&
              this.scpReference.equals(other.getScpReference()))) &&
            ((this.transactionState==null && other.getTransactionState()==null) ||
             (this.transactionState!=null &&
              this.transactionState.equals(other.getTransactionState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRequestId() != null) {
            _hashCode += getRequestId().hashCode();
        }
        if (getScpReference() != null) {
            _hashCode += getScpReference().hashCode();
        }
        if (getTransactionState() != null) {
            _hashCode += getTransactionState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScpResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "scpResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "requestId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "token"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scpReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "scpReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "transactionState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "transactionState"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
