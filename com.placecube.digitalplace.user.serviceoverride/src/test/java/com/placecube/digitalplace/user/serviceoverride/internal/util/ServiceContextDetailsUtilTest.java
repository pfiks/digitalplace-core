package com.placecube.digitalplace.user.serviceoverride.internal.util;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.Portal;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceContextThreadLocal.class)
public class ServiceContextDetailsUtilTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@Mock
	private Company mockCompany;

	@Mock
	private CompanyLocalService mockCompanyLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private ServiceContextDetailsUtil serviceContextDetailsUtil;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void populateMissingServiceContextDetails_WhenServiceContextHasNoValueForPathMain_ThenPathMainValueIsSetWithPortalUtilValue() {
		String portalUtilPathMain = "portalUtilPathMain";
		when(mockPortal.getPathMain()).thenReturn(portalUtilPathMain);
		when(mockServiceContext.getPathMain()).thenReturn(" ");
		when(mockServiceContext.getPortalURL()).thenReturn("myPortalURL");

		serviceContextDetailsUtil.populateMissingServiceContextDetails(COMPANY_ID, mockServiceContext);

		verify(mockServiceContext, times(1)).setPathMain(portalUtilPathMain);
	}

	@Test
	public void populateMissingServiceContextDetails_WhenServiceContextHasNoValueForPortalURLAndExceptionRetrievingCompanyPortalURL_ThenNoExceptionIsThrowsByTheMethod() throws Exception {
		when(mockServiceContext.getPathMain()).thenReturn("myPathMainValue");
		when(mockServiceContext.getPortalURL()).thenReturn(" ");
		when(mockCompanyLocalService.getCompanyById(COMPANY_ID)).thenReturn(null);

		try {
			serviceContextDetailsUtil.populateMissingServiceContextDetails(COMPANY_ID, mockServiceContext);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void populateMissingServiceContextDetails_WhenServiceContextHasNoValueForPortalURLAndNoErrorRetrievingCompanyPortalURL_ThenPortalURLValueIsSetWithCompanyPortalURL() throws Exception {
		String companyPortalURL = "companyPortalURL";
		when(mockServiceContext.getPathMain()).thenReturn("myPathMainValue");
		when(mockServiceContext.getPortalURL()).thenReturn(" ");
		when(mockCompanyLocalService.getCompanyById(COMPANY_ID)).thenReturn(mockCompany);
		when(mockCompany.getPortalURL(0l)).thenReturn(companyPortalURL);

		serviceContextDetailsUtil.populateMissingServiceContextDetails(COMPANY_ID, mockServiceContext);

		verify(mockServiceContext, times(1)).setPortalURL(companyPortalURL);
	}

	@Test
	public void populateMissingServiceContextDetails_WhenServiceContextHasValueForPathMain_ThenPathMainValueIsNotUpdated() {
		when(mockServiceContext.getPathMain()).thenReturn("myPathMainValue");
		when(mockServiceContext.getPortalURL()).thenReturn("myPortalURL");

		serviceContextDetailsUtil.populateMissingServiceContextDetails(COMPANY_ID, mockServiceContext);

		verify(mockServiceContext, never()).setPathMain(anyString());
	}

	@Test
	public void populateMissingServiceContextDetails_WhenServiceContextHasValueForPortalURL_ThenPortalURLValueIsNotUpdated() {
		when(mockServiceContext.getPathMain()).thenReturn("myPathMainValue");
		when(mockServiceContext.getPortalURL()).thenReturn("myPortalURL");

		serviceContextDetailsUtil.populateMissingServiceContextDetails(COMPANY_ID, mockServiceContext);

		verify(mockServiceContext, never()).setPortalURL(anyString());
	}

	@Test
	public void populateMissingServiceContextDetails_WhenServiceContextIsNull_ThenServiceContextIsCreated() {
		mockStatic(ServiceContextThreadLocal.class);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);

		serviceContextDetailsUtil.populateMissingServiceContextDetails(COMPANY_ID, null);

		verifyStatic(ServiceContextThreadLocal.class);
		ServiceContextThreadLocal.getServiceContext();
	}

}
