/**
 * Surchargeable.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.simple;

public class Surchargeable  implements java.io.Serializable {
    private java.lang.Object applyScpConfig;

    private com.capita_software_services.www.scp.base.SurchargeItemDetails surcharge;

    public Surchargeable() {
    }

    public Surchargeable(
           java.lang.Object applyScpConfig,
           com.capita_software_services.www.scp.base.SurchargeItemDetails surcharge) {
           this.applyScpConfig = applyScpConfig;
           this.surcharge = surcharge;
    }


    /**
     * Gets the applyScpConfig value for this Surchargeable.
     *
     * @return applyScpConfig
     */
    public java.lang.Object getApplyScpConfig() {
        return applyScpConfig;
    }


    /**
     * Sets the applyScpConfig value for this Surchargeable.
     *
     * @param applyScpConfig
     */
    public void setApplyScpConfig(java.lang.Object applyScpConfig) {
        this.applyScpConfig = applyScpConfig;
    }


    /**
     * Gets the surcharge value for this Surchargeable.
     *
     * @return surcharge
     */
    public com.capita_software_services.www.scp.base.SurchargeItemDetails getSurcharge() {
        return surcharge;
    }


    /**
     * Sets the surcharge value for this Surchargeable.
     *
     * @param surcharge
     */
    public void setSurcharge(com.capita_software_services.www.scp.base.SurchargeItemDetails surcharge) {
        this.surcharge = surcharge;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Surchargeable)) return false;
        Surchargeable other = (Surchargeable) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.applyScpConfig==null && other.getApplyScpConfig()==null) ||
             (this.applyScpConfig!=null &&
              this.applyScpConfig.equals(other.getApplyScpConfig()))) &&
            ((this.surcharge==null && other.getSurcharge()==null) ||
             (this.surcharge!=null &&
              this.surcharge.equals(other.getSurcharge())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApplyScpConfig() != null) {
            _hashCode += getApplyScpConfig().hashCode();
        }
        if (getSurcharge() != null) {
            _hashCode += getSurcharge().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Surchargeable.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "surchargeable"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("applyScpConfig");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "applyScpConfig"));
        //elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "string"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surcharge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "surcharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeItemDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
