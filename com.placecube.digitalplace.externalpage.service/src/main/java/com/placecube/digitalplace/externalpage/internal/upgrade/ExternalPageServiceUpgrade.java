package com.placecube.digitalplace.externalpage.internal.upgrade;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.externalpage.internal.upgrade.upgrade_1_1_1.ExternalPageTableUpdate;
import com.placecube.digitalplace.externalpage.internal.upgrade.upgrade_1_1_2.ExternalPageIndexUpdate;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class ExternalPageServiceUpgrade implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("1.1.0", "1.1.1", new ExternalPageTableUpdate());
		registry.register("1.1.1", "1.1.2", new ExternalPageIndexUpdate());
	}

}
