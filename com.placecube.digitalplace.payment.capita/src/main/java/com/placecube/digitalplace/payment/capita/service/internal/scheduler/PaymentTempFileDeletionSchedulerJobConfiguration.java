package com.placecube.digitalplace.payment.capita.service.internal.scheduler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.petra.function.UnsafeRunnable;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.scheduler.SchedulerJobConfiguration;
import com.liferay.portal.kernel.scheduler.TriggerConfiguration;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.placecube.digitalplace.payment.capita.service.internal.constants.OneOffPaymentOrderConstants;

@Component(immediate = true, service = SchedulerJobConfiguration.class)
public class PaymentTempFileDeletionSchedulerJobConfiguration implements SchedulerJobConfiguration {

	private static final Log LOG = LogFactoryUtil.getLog(PaymentTempFileDeletionSchedulerJobConfiguration.class);

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DLFileEntryLocalService dlFileEntryLocalService;

	@Reference
	private DLFolderLocalService dlFolderLocalService;

	@Override
	public UnsafeConsumer<Long, Exception> getCompanyJobExecutorUnsafeConsumer() {
		return companyId -> runJobForCompany(companyId);
	}

	@Override
	public UnsafeRunnable<Exception> getJobExecutorUnsafeRunnable() {
		return () -> companyLocalService.forEachCompanyId(companyId -> runJobForCompany(companyId));
	}

	@Override
	public TriggerConfiguration getTriggerConfiguration() {
		TriggerConfiguration triggerConfiguration = TriggerConfiguration.createTriggerConfiguration(OneOffPaymentOrderConstants.EVERY_HOUR_CRON_EXPRESSION);
		triggerConfiguration.setStartDate(new Date());

		return triggerConfiguration;
	}

	private void runJobForCompany(long companyId) {
		CompanyThreadLocal.setCompanyId(companyId);

		DynamicQuery dlFolderDynamicQuery = dlFolderLocalService.dynamicQuery();
		Criterion folderNameCriterion = RestrictionsFactoryUtil.eq(Field.NAME, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME);
		Criterion folderCompanyIdCriterion = RestrictionsFactoryUtil.eq(Field.COMPANY_ID, companyId);

		List<Long> dlFoldersIds = dlFolderLocalService.dynamicQuery(dlFolderDynamicQuery.setProjection(PropertyFactoryUtil.forName("folderId")).add(folderNameCriterion).add(folderCompanyIdCriterion));

		if (!dlFoldersIds.isEmpty()) {
			DynamicQuery dynamicQuery = dlFileEntryLocalService.dynamicQuery();
			Criterion folderIdCriterion = RestrictionsFactoryUtil.in(Field.FOLDER_ID, dlFoldersIds);
			Criterion dateCriterion = RestrictionsFactoryUtil.and(folderIdCriterion, RestrictionsFactoryUtil.lt(Field.CREATE_DATE, getCurrentDateMinusThirtyMinutes()));

			List<DLFileEntry> fileEntries = dlFileEntryLocalService.dynamicQuery(dynamicQuery.add(dateCriterion));

			for (DLFileEntry fileEntry : fileEntries) {
				LOG.debug("Removing fileEntry with fileEntryId: " + fileEntry.getFileEntryId() + " from Company with companyId: " + companyId);
				dlFileEntryLocalService.deleteDLFileEntry(fileEntry);
			}
		}

	}

	private Date getCurrentDateMinusThirtyMinutes() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, -30);
		return calendar.getTime();
	}

}
