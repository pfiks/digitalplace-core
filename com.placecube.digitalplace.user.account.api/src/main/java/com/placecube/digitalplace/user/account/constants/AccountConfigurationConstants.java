package com.placecube.digitalplace.user.account.constants;

public final class AccountConfigurationConstants {

	public static final String PAGE_ADDITIONAL_DETAILS = "additional-details";

	public static final String PAGE_PERSONAL_DETAILS = "personal-details";

	private AccountConfigurationConstants() {
	}

}
