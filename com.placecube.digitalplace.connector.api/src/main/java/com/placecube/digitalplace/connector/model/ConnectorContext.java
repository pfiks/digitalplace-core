package com.placecube.digitalplace.connector.model;

public class ConnectorContext {

	private final long companyId;
	private final String connectorName;

	public ConnectorContext(long companyId, String connectorName) {
		this.companyId = companyId;
		this.connectorName = connectorName;
	}

	public long getCompanyId() {
		return companyId;
	}

	public String getConnectorName() {
		return connectorName;
	}
}
