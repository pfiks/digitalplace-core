package com.placecube.digitalplace.search.web.portlet.sortby;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.common.portlet.PortletURLService;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.model.SortByOption;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.configuration.SortByPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.sortby.service.SortByService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class, SortFactoryUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.search.SortFactoryUtil", "com.placecube.digitalplace.search.web.portlet.sortby.service.SortByService" })
public class SortByPortletTest extends PowerMockito {

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private PortletURLService mockPortletURLService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private SortByOption mockSortByOption1;

	@Mock
	private SortByOption mockSortByOption2;

	@Mock
	private SortByPortletInstanceConfiguration mockSortByPortletInstanceConfiguration;

	@Mock
	private SortByService mockSortByService;
	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SortByPortlet sortByPortlet;

	@Before
	public void activateSetup() {
		suppress(methods(MVCPortlet.class, "render"));
		mockStatic(SortFactoryUtil.class);
	}

	@Test
	public void render_WhenConfigurationHasInvalidValuesConfigured_ThenSetsInvalidConfigurationTrueAndDoesNotPerformAsearch() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(new String[] { "" });

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verifyZeroInteractions(mockSharedSearchRequestService);
	}

	@Test
	public void render_WhenConfigurationHasNoValuesConfigured_ThenSetsInvalidConfigurationTrueAndDoesNotPerformAsearch() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(null);

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verifyZeroInteractions(mockSharedSearchRequestService);
	}

	@Test
	public void render_WhenConfigurationHasOneValueOfFalseConfigured_ThenSetsInvalidConfigurationTrueAndDoesNotPerformAsearch() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(new String[] { "false" });

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verifyZeroInteractions(mockSharedSearchRequestService);
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionRetrievingConfiguration_ThenThrowsPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoAvailableSortByOptions_ThenSetsInvalidConfigurationTrue() throws Exception {
		String searchKeyword = "searchKeywordVal";
		String[] enabledSortByOptions = new String[] { "abc" };

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(searchKeyword);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(Collections.emptyList());

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresDisplayPortletAsRequestAttribute() throws Exception {
		String searchKeyword = "searchKeywordVal";
		String[] enabledSortByOptions = new String[] { "abc" };

		List<SortByOption> sortByOptions = new LinkedList<>();
		sortByOptions.add(mockSortByOption1);
		sortByOptions.add(mockSortByOption2);

		boolean displayPortlet = true;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(searchKeyword);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSortByPortletInstanceConfiguration.displayPortlet()).thenReturn(displayPortlet);

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("displayPortlet", displayPortlet);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresTheIteratorUrlAsRequestAttribute() throws Exception {
		String searchKeyword = "searchKeywordVal";
		String iteratorURL = "iteratorURLVal";
		String[] enabledSortByOptions = new String[] { "abc" };

		List<SortByOption> sortByOptions = new LinkedList<>();
		sortByOptions.add(mockSortByOption1);
		sortByOptions.add(mockSortByOption2);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(searchKeyword);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSharedSearchResponse.getEscapedSearchResultsIteratorURL()).thenReturn(iteratorURL);

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", iteratorURL);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresTheSortByOptionsAsRequestAttribute() throws Exception {
		String searchKeyword = "searchKeywordVal";
		String[] enabledSortByOptions = new String[] { "abc" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		sortByOptions.add(mockSortByOption1);
		sortByOptions.add(mockSortByOption2);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(searchKeyword);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("sortByOptions", sortByOptions);
	}

	@Test
	public void render_WhenSelectedSortByIsNotSpecified_ThenConfiguresTheFirstAvailableSortOptionAsSelectedSortByRequestAttribute() throws Exception {
		String searchKeyword = "searchKeywordVal";
		String firstSortBy = "selectedSortBy";
		String[] enabledSortByOptions = new String[] { "abc" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		sortByOptions.add(mockSortByOption1);
		sortByOptions.add(mockSortByOption2);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(searchKeyword);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSharedSearchResponse.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(StringPool.BLANK);
		when(mockSortByOption1.getId()).thenReturn(firstSortBy);

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("selectedSortBy", firstSortBy);
	}

	@Test
	public void render_WhenSelectedSortByIsSpecified_ThenConfiguresTheSelectedSortByAsRequestAttribute() throws Exception {
		String searchKeyword = "searchKeywordVal";
		String selectedSortBy = "selectedSortBy";
		String[] enabledSortByOptions = new String[] { "abc" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		sortByOptions.add(mockSortByOption1);
		sortByOptions.add(mockSortByOption2);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(searchKeyword);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSharedSearchResponse.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(selectedSortBy);

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("selectedSortBy", selectedSortBy);
	}

	@Test
	public void render_WhenSelectedSortByIsSpecified_ThenSetsIsEditLayoutModeAttributeInRequest() throws Exception {
		String searchKeyword = "searchKeywordVal";
		String[] enabledSortByOptions = new String[] { "abc" };
		List<SortByOption> sortByOptions = Arrays.asList(mockSortByOption1);

		when(mockPortletURLService.isEditLayoutMode(mockRenderRequest)).thenReturn(true);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(searchKeyword);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("isEditLayoutMode", true);
	}

	@Test
	public void render_WhenSortDirectionChangeableIsFalse_ThenDoesNotSetSortDirectionDetailsAsRequestAttributes() throws Exception {
		String searchKeyword = "searchKeywordVal";
		String[] enabledSortByOptions = new String[] { "abc" };
		List<SortByOption> sortByOptions = Arrays.asList(mockSortByOption1);

		when(mockPortletURLService.isEditLayoutMode(mockRenderRequest)).thenReturn(true);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);
		when(mockSortByPortletInstanceConfiguration.sortDirectionChangeable()).thenReturn(false);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(searchKeyword);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq("sortDirectionChangeable"), any());
		verify(mockRenderRequest, never()).setAttribute(eq("currentSelectedSortByDirection"), any());
	}

	@Test
	public void render_WhenSortDirectionChangeableIsTrue_ThenSetsSortDirectionDetailsAsRequestAttributes() throws Exception {
		String searchKeyword = "searchKeywordVal";
		String[] enabledSortByOptions = new String[] { "abc" };
		List<SortByOption> sortByOptions = Arrays.asList(mockSortByOption1);

		when(mockPortletURLService.isEditLayoutMode(mockRenderRequest)).thenReturn(true);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);
		when(mockSortByPortletInstanceConfiguration.sortDirectionChangeable()).thenReturn(true);
		when(mockSharedSearchResponse.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY_DIRECTION)).thenReturn("selectedSortDirectionValue");
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(searchKeyword);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("sortDirectionChangeable", true);
		verify(mockRenderRequest, times(1)).setAttribute("currentSelectedSortByDirection", "selectedSortDirectionValue");
	}

	@Test(expected = PortletException.class)
	public void render_WhenValidConfigurationAndExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockSortByPortletInstanceConfiguration);
		when(mockSortByPortletInstanceConfiguration.enabledSortByOptions()).thenReturn(new String[] { "123" });
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		sortByPortlet.render(mockRenderRequest, mockRenderResponse);
	}

}
