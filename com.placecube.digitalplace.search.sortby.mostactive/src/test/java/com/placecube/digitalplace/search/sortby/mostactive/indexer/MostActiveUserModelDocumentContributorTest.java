package com.placecube.digitalplace.search.sortby.mostactive.indexer;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.placecube.digitalplace.search.sortby.mostactive.constants.MostActiveConstants;
import com.placecube.digitalplace.search.sortby.mostactive.service.MostActiveService;

public class MostActiveUserModelDocumentContributorTest extends PowerMockito {

	@Mock
	private Date mockDate;

	@Mock
	private Document mockDocument;

	@Mock
	private MostActiveService mockMostActiveService;

	@Mock
	private User mockUser;

	@InjectMocks
	private MostActiveUserModelDocumentContributor mostActiveUserModelDocumentContributor;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenUserIsNotTheDefaultUser_ThenAddsTheLastLoginDateToTheIndexedDocument() {
		when(mockUser.isGuestUser()).thenReturn(false);
		when(mockUser.getLastLoginDate()).thenReturn(mockDate);

		mostActiveUserModelDocumentContributor.contribute(mockDocument, mockUser);

		verify(mockDocument, times(1)).addDate(MostActiveConstants.LAST_LOGIN_DATE_FIELD_NAME, mockDate);
	}

	@Test
	public void contribute_WhenUserIsNotTheDefaultUser_ThenAddsTheMostActiveFieldToTheIndexedDocument() {
		long count = 56;
		when(mockUser.isGuestUser()).thenReturn(false);
		when(mockMostActiveService.getActivitiesCount(mockUser)).thenReturn(count);

		mostActiveUserModelDocumentContributor.contribute(mockDocument, mockUser);

		verify(mockDocument, times(1)).addKeyword(MostActiveConstants.COUNTER_FIELD_NAME, count);
	}

	@Test
	public void contribute_WhenUserIsTheDefaultUser_ThenNoChangesAreMadeToTheIndexedDocument() {
		when(mockUser.isGuestUser()).thenReturn(true);

		mostActiveUserModelDocumentContributor.contribute(mockDocument, mockUser);

		verifyZeroInteractions(mockDocument);
	}
}
