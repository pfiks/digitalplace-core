package com.placecube.digitalplace.address.service.impl;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookup;

public class AddressLookupHelperTest extends PowerMockito {

	private static final Long COMPANY_ID = 11l;

	private static final String[] PARAMS_AND_VALUES = new String[] { "one", "two" };

	private static final String POSTCODE = "postcodeValue";

	private static final String UPRN = "uprnValue";

	private AddressLookupHelper addressLookupHelper = new AddressLookupHelper();

	private List<AddressLookup> addressLookupList = new ArrayList<>();

	private Set<AddressLookup> addressLookups = new HashSet<>();

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private Set<AddressContext> mockAddressContexts;

	@Mock
	private AddressLookup mockAddressLookup1;

	@Mock
	private AddressLookup mockAddressLookup2;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void fetchKeyword_WhenKeywordIsFoundInFirstAddressLookup_ThenDoesNotUseTheNextOne() {

		addressLookupList.add(mockAddressLookup1);
		addressLookupList.add(mockAddressLookup2);
		when(mockAddressLookup1.findByKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(mockAddressContexts);

		Set<AddressContext> result = addressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, addressLookupList);

		assertThat(result, sameInstance(mockAddressContexts));
		verifyZeroInteractions(mockAddressLookup2);

	}

	@Test
	public void fetchKeyword_WhenKeywordIsNoFoundInFirstAddressLookup_ThenUseTheNextOne() {

		addressLookupList.add(mockAddressLookup1);
		addressLookupList.add(mockAddressLookup2);
		when(mockAddressLookup1.findByKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(Collections.emptySet());
		when(mockAddressLookup2.findByKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(mockAddressContexts);

		Set<AddressContext> result = addressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, addressLookupList);

		assertThat(result, sameInstance(mockAddressContexts));

	}

	@Test
	public void fetchKeyword_WhenKeywordIsNotFound_ThenReturnEmptyOptional() {

		addressLookupList.add(mockAddressLookup1);
		addressLookupList.add(mockAddressLookup2);
		when(mockAddressLookup1.findByKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(Collections.emptySet());
		when(mockAddressLookup2.findByKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(Collections.emptySet());

		Set<AddressContext> result = addressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, addressLookupList);

		assertTrue(result.isEmpty());

	}

	@Test
	public void fetchPostcode_WhenPostcodeIsFoundInFirstAddressLookup_ThenDoesNotUseTheNextOne() {

		addressLookupList.add(mockAddressLookup1);
		addressLookupList.add(mockAddressLookup2);
		when(mockAddressLookup1.findByPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(mockAddressContexts);

		Set<AddressContext> result = addressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, addressLookupList);

		assertThat(result, sameInstance(mockAddressContexts));
		verifyZeroInteractions(mockAddressLookup2);

	}

	@Test
	public void fetchPostcode_WhenPostcodeIsNoFoundInFirstAddressLookup_ThenUseTheNextOne() {

		addressLookupList.add(mockAddressLookup1);
		addressLookupList.add(mockAddressLookup2);
		when(mockAddressLookup1.findByPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(Collections.emptySet());
		when(mockAddressLookup2.findByPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(mockAddressContexts);

		Set<AddressContext> result = addressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, addressLookupList);

		assertThat(result, sameInstance(mockAddressContexts));

	}

	@Test
	public void fetchPostcode_WhenPostcodeIsNotFound_ThenReturnEmptyOptional() {

		addressLookupList.add(mockAddressLookup1);
		addressLookupList.add(mockAddressLookup2);
		when(mockAddressLookup1.findByPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(Collections.emptySet());
		when(mockAddressLookup2.findByPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES)).thenReturn(Collections.emptySet());

		Set<AddressContext> result = addressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, addressLookupList);

		assertTrue(result.isEmpty());

	}

	@Test
	public void fetchUprn_WhenUprnIsFoundInFirstAddressLookup_ThenDoesNotUseTheNextOne() {

		addressLookupList.add(mockAddressLookup1);
		addressLookupList.add(mockAddressLookup2);
		when(mockAddressLookup1.findByUprn(COMPANY_ID, UPRN, PARAMS_AND_VALUES)).thenReturn(Optional.of(mockAddressContext));

		Optional<AddressContext> result = addressLookupHelper.fetchUprn(COMPANY_ID, UPRN, PARAMS_AND_VALUES, addressLookupList);

		assertThat(result.get(), sameInstance(mockAddressContext));
		verifyZeroInteractions(mockAddressLookup2);

	}

	@Test
	public void fetchUprn_WhenUprnIsNoFoundInFirstAddressLookup_ThenUseTheNextOne() {

		addressLookupList.add(mockAddressLookup1);
		addressLookupList.add(mockAddressLookup2);
		when(mockAddressLookup1.findByUprn(COMPANY_ID, UPRN, PARAMS_AND_VALUES)).thenReturn(Optional.empty());
		when(mockAddressLookup2.findByUprn(COMPANY_ID, UPRN, PARAMS_AND_VALUES)).thenReturn(Optional.of(mockAddressContext));

		Optional<AddressContext> result = addressLookupHelper.fetchUprn(COMPANY_ID, UPRN, PARAMS_AND_VALUES, addressLookupList);

		assertThat(result.get(), sameInstance(mockAddressContext));

	}

	@Test
	public void fetchUprn_WhenUprnIsNotFound_ThenReturnEmptyOptional() {

		addressLookupList.add(mockAddressLookup1);
		addressLookupList.add(mockAddressLookup2);
		when(mockAddressLookup1.findByUprn(COMPANY_ID, UPRN, PARAMS_AND_VALUES)).thenReturn(Optional.empty());
		when(mockAddressLookup2.findByUprn(COMPANY_ID, UPRN, PARAMS_AND_VALUES)).thenReturn(Optional.empty());

		Optional<AddressContext> result = addressLookupHelper.fetchUprn(COMPANY_ID, UPRN, PARAMS_AND_VALUES, addressLookupList);

		assertFalse(result.isPresent());

	}

	@Test
	public void getLocalAddressLookup_WhenThereAreAddressLookupsEnabled_ThenReturnedSortedListByWeight() {

		when(mockAddressLookup1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockAddressLookup1.getWeight(COMPANY_ID)).thenReturn(2);
		addressLookups.add(mockAddressLookup1);
		when(mockAddressLookup2.enabled(COMPANY_ID)).thenReturn(true);
		when(mockAddressLookup2.getWeight(COMPANY_ID)).thenReturn(1);
		addressLookups.add(mockAddressLookup2);

		List<AddressLookup> result = addressLookupHelper.getLocalAddressLookup(COMPANY_ID, addressLookups);

		assertThat(result, contains(mockAddressLookup2, mockAddressLookup1));
	}

	@Test
	public void getLocalAddressLookup_WhenThereIsNoAddressLookup_ThenReturnEmptyList() {

		List<AddressLookup> result = addressLookupHelper.getLocalAddressLookup(COMPANY_ID, addressLookups);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getLocalAddressLookup_WhenThereIsNoLocalAddressLookupEnabled_ThenReturnEmptyList() {

		when(mockAddressLookup1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockAddressLookup1.national(COMPANY_ID)).thenReturn(true);
		addressLookups.add(mockAddressLookup1);

		when(mockAddressLookup2.enabled(COMPANY_ID)).thenReturn(false);
		when(mockAddressLookup2.national(COMPANY_ID)).thenReturn(false);
		addressLookups.add(mockAddressLookup2);

		List<AddressLookup> result = addressLookupHelper.getLocalAddressLookup(COMPANY_ID, addressLookups);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getNationalAddressLookup_WhenThereAreAddressLookupsEnabled_ThenReturnedSortedListByWeight() {

		when(mockAddressLookup1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockAddressLookup1.national(COMPANY_ID)).thenReturn(true);
		when(mockAddressLookup1.getWeight(COMPANY_ID)).thenReturn(2);
		addressLookups.add(mockAddressLookup1);

		when(mockAddressLookup2.enabled(COMPANY_ID)).thenReturn(true);
		when(mockAddressLookup2.national(COMPANY_ID)).thenReturn(true);
		when(mockAddressLookup2.getWeight(COMPANY_ID)).thenReturn(1);
		addressLookups.add(mockAddressLookup2);

		List<AddressLookup> result = addressLookupHelper.getNationalAddressLookup(COMPANY_ID, addressLookups);

		assertThat(result, contains(mockAddressLookup2, mockAddressLookup1));
	}

	@Test
	public void getNationalAddressLookup_WhenThereIsNoAddressLookup_ThenReturnEmptyList() {

		List<AddressLookup> result = addressLookupHelper.getNationalAddressLookup(COMPANY_ID, addressLookups);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getNationalAddressLookup_WhenThereIsNoNationalAddressLookupEnabled_ThenReturnEmptyList() {

		when(mockAddressLookup1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockAddressLookup1.national(COMPANY_ID)).thenReturn(true);
		addressLookups.add(mockAddressLookup1);

		when(mockAddressLookup2.enabled(COMPANY_ID)).thenReturn(true);
		when(mockAddressLookup2.national(COMPANY_ID)).thenReturn(false);
		addressLookups.add(mockAddressLookup2);

		List<AddressLookup> result = addressLookupHelper.getNationalAddressLookup(COMPANY_ID, addressLookups);

		assertTrue(result.isEmpty());
	}

	@Test(expected = PortalException.class)
	public void validateLookups_WhenAddressLookupsListIsEmpty_ThenPortalExceptionIsThrown() throws PortalException {

		addressLookupHelper.validateLookups(Collections.emptyList());

	}

	@Test
	public void validateLookups_WhenAddressLookupsListIsNotEmpty_ThenNoExceptionIsThrown() {

		addressLookupList.add(mockAddressLookup1);
		try {
			addressLookupHelper.validateLookups(addressLookupList);
		} catch (Exception e) {
			fail();
		}

	}

}
