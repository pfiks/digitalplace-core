package com.placecube.digitalplace.user.settings.web.constants;

public final class PortletKeys {

	public static final String USER_SETTINGS = "com_placecube_digitalplace_user_settings_UserSettingsPortlet";

	private PortletKeys() {
		return;
	}
}
