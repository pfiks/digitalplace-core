package com.placecube.digitalplace.user.account.web.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SessionMessages.class, SessionErrors.class, PortalUtil.class })
public class PersonalDetailsMVCActionCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	private static final String EMAIL = "emailValue";

	private static final long GROUP_ID = 11;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private AccountService mockAccountService;

	@Mock
	private AccountValidationService mockAccountValidationService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private PersonalDetailsMVCActionCommand personalDetailsMVCActionCommand;

	@Before
	public void activateSetUp() {
		mockStatic(SessionMessages.class, SessionErrors.class, PortalUtil.class);
	}

	@Test
	public void doProcessAction_WhenNoErrors_ThenSetsTheNextStepAsRenderCommandName() throws Exception {
		String nextStep = "nextStep";
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		when(mockAccountConfigurationService.getPersonalDetailsNextStep(mockAccountContext)).thenReturn(nextStep);
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, EMAIL)).thenReturn(null);
		when(mockAccountContext.hasErrors()).thenReturn(false);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		personalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountValidationService, times(1)).validateCaptcha(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration, AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, nextStep);
	}

	@Test
	public void doProcessAction_WhenNoErrorsAndModelIdIsNotNull_ThenUseTheAccountGroupConfiguration() throws Exception {
		String nextStep = "nextStep";
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		when(mockAccountConfigurationService.getPersonalDetailsNextStep(mockAccountContext)).thenReturn(nextStep);
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, EMAIL)).thenReturn(null);
		when(mockAccountContext.hasErrors()).thenReturn(false);
		when(mockAccountContext.getAccountConnectorModelId()).thenReturn("modelId");
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		personalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountConfigurationService, times(1)).getAccountConfiguration(mockAccountContext, mockActionRequest);
	}

	@Test
	public void doProcessAction_WhenThereAreErrors_ThenSetsTheNextStepAsPersonalDetails() throws Exception {
		String nextStep = "nextStep";
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		when(mockAccountConfigurationService.getPersonalDetailsNextStep(mockAccountContext)).thenReturn(nextStep);
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, EMAIL)).thenReturn(null);
		when(mockAccountContext.hasErrors()).thenReturn(true);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		personalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountValidationService, times(1)).validateCaptcha(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration, AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.PERSONAL_DETAILS);
	}

	@Test
	public void doProcessAction_WhenThereAreNoErrorsAndNextStepNotNull_ThenSetsTheNextStepAsNextStepReturned() throws Exception {
		String nextStep = "nextStep";
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, EMAIL)).thenReturn(null);
		when(mockAccountContext.hasErrors()).thenReturn(false);
		when(mockAccountConfigurationService.getPersonalDetailsNextStep(mockAccountContext)).thenReturn(nextStep);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
		personalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountValidationService, times(1)).validateCaptcha(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration, AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", nextStep);
	}

	@Test
	public void doProcessAction_WhenThereAreNoErrorsAndNextStepNull_ThenCreatesUserAndSetsTheNextStepAsConfirmationPage() throws Exception {
		String nextStep = "";
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, EMAIL)).thenReturn(null);
		when(mockAccountContext.hasErrors()).thenReturn(false);
		when(mockAccountConfigurationService.getPersonalDetailsNextStep(mockAccountContext)).thenReturn(nextStep);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(true);

		personalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountService, mockMutableRenderParameters, mockAccountContextService);
		inOrder.verify(mockAccountService, times(1)).createUserAccount(mockActionRequest, mockAccountContext, true);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.CONFIRMATION_PAGE);
		inOrder.verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockActionRequest);
	}

}
