package com.placecube.digitalplace.address.service;

import com.placecube.digitalplace.address.constants.AddressType;

public interface AddressTypeConfigurationService {

	AddressType getDefaultAddressType(long companyId);
}
