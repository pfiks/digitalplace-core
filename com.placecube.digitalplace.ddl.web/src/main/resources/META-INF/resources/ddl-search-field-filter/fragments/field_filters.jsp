<c:if test="${not empty filterFields}">
	<liferay-frontend:fieldset label="filter-groups">
			<div id="<portlet:namespace/>filterGroups"></div>

			<aui:button name="addFilterGroupBtn" value="add-filter-group" type="button"/>
	</liferay-frontend:fieldset>

	<div id="<portlet:namespace/>filter-group-template" style="display: none;">
		<liferay-frontend:edit-form-body>
			<aui:a cssClass="remove-group" href="#" >
				<liferay-ui:message key="delete"/>
			</aui:a>

			<aui:input
				label="filter-group-name"
				name="filterGroupName"
				type="text"
				value=""
			/>

			<aui:input
				label="preselected-option-name"
				name="filterPreselectedOptionName"
				type="text"
				value=""
			/>
			<div class="filter-options"></div>

			<aui:button cssClass="add-option" name="addFilterOptionBtn" value="add-filter-option" type="button" />
		</liferay-frontend:edit-form-body>

	</div>

	<div id="<portlet:namespace/>filter-option-template" style="display: none;">
		<liferay-frontend:edit-form-body>
			<aui:a cssClass="remove-option" href="#">
				<liferay-ui:message key="delete"/>
			</aui:a>

			<aui:input
				label="filter-option-name"
				name="filterOptionName"
				type="text"
				value=""
			/>
			
			<div class="mb-2">
				<div class="filters"></div>
				<aui:button cssClass="add-filter" value="add-filter" type="button" />
			</div>

		</liferay-frontend:edit-form-body>
	</div>

	<div id="<portlet:namespace/>field-filter-template" style="display: none;">
		<table>
			<tbody>
				<tr>
					<td>
						<aui:select cssClass="filter-field-selector" name="fieldName" required="false" multiple="false" label="field-name" showEmptyOption="true">
							<c:forEach items="${filterFields}" var="filterField">
								<aui:option label="${filterField.fieldLabel}" value="${filterField.facetFieldName}" data="${fieldsAttributes[filterField.facetFieldName]}"/>
							</c:forEach>
						</aui:select>
					</td>
					<td>
						<aui:select name="filterOperator" label="filter-operator" >
							<aui:option value="gte"><liferay-ui:message key="greater-than-equal-to"/></aui:option>
							<aui:option value="lte"><liferay-ui:message key="less-than-equal-to"/></aui:option>
							<aui:option value="gt"><liferay-ui:message key="greater-than"/></aui:option>
							<aui:option value="lt"><liferay-ui:message key="less-than"/></aui:option>
						</aui:select>
					</td>
					
					<td>
						<div class="date-field-wrapper" style="display: none;">
							<aui:input name="fieldValue" label="date-value" cssClass="form-control aui-input-date" type="text" helpMessage="select-date-or-leave-empty-for-today" placeholder="today" value="" />
						</div>
						
						<div class="number-field-wrapper" style="display: none;">
							<aui:input name="fieldValue" label="numeric-value" cssClass="form-control" type="text" placeholder="0" value="" />
						</div>
					</td>
					
					<td class="num-days-cell" style="display: none;">
						<aui:input name="numDays" type="text" value="" label="number-of-days" helpMessage="by-days"/>
					</td>
					
					<td>
						<aui:a cssClass="remove-filter" href="#" >
							<liferay-ui:message key="delete"/>
						</aui:a>
					</td>
					<td>
						<aui:input cssClass="field-type" name="fieldType" type="hidden" value="" />
					</td>
					
				</tr>
			</tbody>
		</table>
	</div>

</c:if>

<aui:script use="com-placecube-field-filter-builder, aui-datepicker">

	A.FieldFilterBuilder.init({
		jsonFilterConfig: '${jsonFilterConfig}',
		groupTemplate: $('#<portlet:namespace/>filter-group-template'),
		optionTemplate: $('#<portlet:namespace/>filter-option-template'),
		hiddenFormField: $('#<portlet:namespace/>jsonFilterConfig'),
		ns: '<portlet:namespace/>'
	});

	$('#<portlet:namespace/>ddlFieldFilterForm').submit(function( event ) {
		A.FieldFilterBuilder.formToJson();
	});

	new A.DatePicker({
		mask: '%d/%m/%Y',
		trigger: '.aui-input-date',
		popover: {
			zIndex: 1000
		}
	});

</aui:script>