package com.placecube.digitalplace.payment.capitainterface.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import com.placecube.digitalplace.address.model.AddressContext;

public class OneOffPaymentOrder implements Serializable {

	public enum CardRequestType {
		PAY_AND_STORE_CARD, PAY_ONLY;
	}

	/**
	 * Used for Java serialization
	 */
	private static final long serialVersionUID = 9127974957856947691L;

	/**
	 * formatted address to pass to capita to pre-populate the form
	 */
	private AddressContext address;

	/**
	 * This is the auth code field returned by the Capita query call
	 */
	private String authCode;

	/**
	 * The URL which the payment gateway should redirect to if the back button
	 * is pressed
	 */
	private String backURL;

	/**
	 * Stored card details returned by the Capita query call - the card token
	 * number
	 */
	private String cardToken;

	private int continuousAuditNumber;

	/**
	 * The customers email address
	 */
	private String emailAddress;

	/**
	 * The customers full name
	 */
	private String fullName;

	/**
	 * This is the reference number we generate and pass in the invoke call to
	 * Capita
	 */
	private String generatedReferenceNumber;

	/**
	 * Reference for internal use: DDMFormInstanceRecordId
	 */
	private String internalReference;

	/**
	 * Stored card details returned by the Capita query call - the last 4 digits
	 * of the card
	 */
	private String last4Digits;

	/**
	 * This is the unique payment amount charged, returned by the Capita query
	 * call
	 */
	private BigDecimal paymentValueProcessed;

	/**
	 * This is the Capita URL, generated by and returned by the Capita invoke
	 * call
	 */
	private String redirectUrl;

	/**
	 * the type of the request, e.g. payOnly, or payAndStore - generally used to
	 * control whether the users card details should be stored
	 */
	private CardRequestType requestType;

	/**
	 * The URL which the payment gateway should redirect to after card
	 * processing is complete
	 */
	private String returnURL;

	/**
	 * the total sale amount for the sale, in pence
	 */
	private int saleAmount;

	/**
	 * THe overall sale description, which is displayed on the payment gateway
	 * pages
	 */
	private String saleDescription;

	/**
	 * the reference for the overall sale, as displayed to the user on the
	 * Capita site
	 */
	private String saleReferenceCode;

	/**
	 * The simple sale configuration
	 */
	private SimpleSaleConfiguration simpleSaleConfiguration;

	/**
	 * used to hold the status of the last call we made to Capita for this order
	 */
	private String status;

	/**
	 * This is the one generated by and returned by the Capita invoke call
	 */
	private String transactionReferenceNumber;

	/**
	 * This is the unique payment reference number returned by the Capita query
	 * call
	 */
	private String uniqueTranId;

	/**
	 * @return the address
	 */
	public AddressContext getAddress() {
		return address;
	}

	public String getAuthCode() {
		return authCode;
	}

	public String getBackURL() {
		return backURL;
	}

	public String getBccGeneratedReferenceNumber() {
		return generatedReferenceNumber;
	}

	public String getCardToken() {
		return cardToken;
	}

	public int getContinuousAuditNumber() {
		return continuousAuditNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getFullName() {
		return fullName;
	}

	public String getInternalReference() {
		return internalReference;
	}

	public String getLast4Digits() {
		return last4Digits;
	}

	public BigDecimal getPaymentValueProcessed() {
		return paymentValueProcessed;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	/**
	 * @return the requestType
	 */
	public CardRequestType getRequestType() {
		return requestType;
	}

	public String getReturnURL() {
		return returnURL;
	}

	public int getSaleAmount() {
		return saleAmount;
	}

	public String getSaleDescription() {
		return saleDescription;
	}

	/**
	 * @return the saleReferenceCode
	 */
	public String getSaleReferenceCode() {
		return saleReferenceCode;
	}

	public SimpleSaleConfiguration getSimpleSaleConfiguration() {
		return simpleSaleConfiguration;
	}

	public String getStatus() {
		return status;
	}

	public String getTransactionReferenceNumber() {
		return transactionReferenceNumber;
	}

	public String getUniqueTranId() {
		return uniqueTranId;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(AddressContext address) {
		this.address = address;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public void setBackURL(String backURL) {
		this.backURL = backURL;
	}

	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}

	public void setContinuousAuditNumber(int continuousAuditNumber) {
		this.continuousAuditNumber = continuousAuditNumber;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setGeneratedReferenceNumber(String generatedReferenceNumber) {
		this.generatedReferenceNumber = generatedReferenceNumber;
	}

	public void setInternalReference(String internalReference) {
		this.internalReference = internalReference;
	}

	public void setLast4Digits(String last4Digits) {
		this.last4Digits = last4Digits;
	}

	public void setPaymentValueProcessed(BigDecimal paymentValueProcessed) {
		this.paymentValueProcessed = paymentValueProcessed;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(CardRequestType requestType) {
		this.requestType = requestType;
	}

	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

	public void setSaleAmount(int saleAmount) {
		this.saleAmount = saleAmount;
	}

	public void setSaleDescription(String saleDescription) {
		this.saleDescription = saleDescription;
	}

	/**
	 * @param saleReferenceCode the saleReferenceCode to set
	 */
	public void setSaleReferenceCode(String saleReferenceCode) {
		this.saleReferenceCode = saleReferenceCode;
	}

	public void setSimpleSaleConfiguration(SimpleSaleConfiguration simpleSaleConfiguration) {
		this.simpleSaleConfiguration = simpleSaleConfiguration;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTransactionReferenceNumber(String transactionReferenceNumber) {
		this.transactionReferenceNumber = transactionReferenceNumber;
	}

	public void setUniqueTranId(String uniqueTranId) {
		this.uniqueTranId = uniqueTranId;
	}

	@Override
	public String toString() {
		return "OneOffPaymentOrder [generatedReferenceNumber=" + generatedReferenceNumber + ", requestType=" + requestType + ", saleDescription=" + saleDescription + ", saleAmount=" + saleAmount
				+ ", saleReferenceCode=" + saleReferenceCode + ", simpleSaleConfiguration=" + simpleSaleConfiguration + ", transactionReferenceNumber=" + transactionReferenceNumber + ", status="
				+ status + ", uniqueTranId=" + uniqueTranId + ", paymentValueProcessed=" + paymentValueProcessed + ", emailAddress=" + emailAddress + ", fullName=" + fullName + ", last4Digits="
				+ last4Digits + ", cardToken=" + cardToken + ", authCode=" + authCode + ", address=" + address + ", redirectUrl=" + redirectUrl + ", returnURL=" + returnURL + ", backURL=" + backURL
				+ "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (obj.getClass() != this.getClass()) {
			return false;
		}

		final OneOffPaymentOrder other = (OneOffPaymentOrder) obj;

		return Objects.equals(address, other.getAddress()) //
				&& Objects.equals(authCode, other.getAuthCode()) //
				&& Objects.equals(backURL, other.getBackURL()) //
				&& Objects.equals(cardToken, other.getCardToken()) //
				&& Objects.equals(emailAddress, other.getEmailAddress()) //
				&& Objects.equals(fullName, other.getFullName()) //
				&& Objects.equals(generatedReferenceNumber, other.getBccGeneratedReferenceNumber()) //
				&& Objects.equals(internalReference, other.getInternalReference()) //
				&& Objects.equals(last4Digits, other.getLast4Digits()) //
				&& Objects.equals(paymentValueProcessed, other.getPaymentValueProcessed()) //
				&& Objects.equals(redirectUrl, other.getRedirectUrl()) //
				&& Objects.equals(requestType, other.getRequestType()) //
				&& Objects.equals(returnURL, other.getReturnURL()) //
				&& Objects.equals(saleDescription, other.getSaleDescription()) //
				&& Objects.equals(saleReferenceCode, other.getSaleReferenceCode()) //
				&& Objects.equals(simpleSaleConfiguration, other.getSimpleSaleConfiguration()) //
				&& Objects.equals(status, other.getStatus()) //
				&& Objects.equals(transactionReferenceNumber, other.getTransactionReferenceNumber()) //
				&& Objects.equals(uniqueTranId, other.getUniqueTranId()) //
				&& continuousAuditNumber == other.getContinuousAuditNumber() //
				&& saleAmount == other.getSaleAmount();
	}

	@Override
	public int hashCode() {
		return Objects.hash(address, authCode, backURL, cardToken, emailAddress, fullName, generatedReferenceNumber, internalReference, last4Digits, paymentValueProcessed, redirectUrl, requestType,
				returnURL, saleDescription, saleReferenceCode, simpleSaleConfiguration, status, transactionReferenceNumber, uniqueTranId, saleAmount, continuousAuditNumber);
	}
}
