<%@ include file="../init.jsp"%>

<c:if test="${not empty activeFilters}">
	<div class="row active-filters-entries">
		<div class="col-md-12">
			<span class="active-filters-label">
				<liferay-ui:message key="filters" />
			</span>
			
			<c:forEach items="${activeFilters}" var="activeFilter">
						
				<portlet:actionURL name="<%= MVCCommandKeys.SEARCH %>" var="removeActiveFilterURL">
					<portlet:param name="cmd" value="remove"/>
					<portlet:param name="redirectURL" value="${redirectURL}" />
					<portlet:param name="filterName" value="${activeFilter.getName()}" />
					<portlet:param name="filterValue" value="${activeFilter.getValue()}" />
				</portlet:actionURL>
				
				<aui:button href="${removeActiveFilterURL}" value="${activeFilter.getLabel()}"
					cssClass="btn btn-active-filter" 
					icon="icon-remove" iconAlign="right" style="background-color: ${activeFilter.getColour()}"/>
					
			</c:forEach>
				
		</div>
	</div>
</c:if>


