package com.placecube.digitalplace.connector.service.impl.util;

import org.osgi.service.component.annotations.Component;

import com.liferay.osgi.service.tracker.collections.map.ServiceReferenceMapper;
import com.placecube.digitalplace.connector.constants.ConnectorConstants;
import com.placecube.digitalplace.connector.model.Connector;

@Component(immediate = true, service = ConnectorRegistryUtil.class)
public class ConnectorRegistryUtil {

	public ServiceReferenceMapper<String, Connector> getInterfaceServiceReferenceMapper() {
		return (serviceReference, emitter) -> emitter.emit((String) serviceReference.getProperty(ConnectorConstants.CONNECTOR_TYPE));
	}

}
