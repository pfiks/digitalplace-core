package com.placecube.digitalplace.appointment.admin.web.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.pfiks.common.time.model.DateTimeSelection;
import com.placecube.digitalplace.appointment.admin.web.model.AppointmentSlotForm;
import com.placecube.digitalplace.appointment.admin.web.model.AppointmentSlotModelBuilder;
import com.placecube.digitalplace.appointment.booking.constants.AppointmentSlotType;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.appointment.model.AppointmentSlot;
import com.placecube.digitalplace.appointment.service.AppointmentSlotLocalService;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentsManagerServiceTest {

	@InjectMocks
	private AppointmentsManagerService appointmentsManagerService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private List<AppointmentBookingEntry> mockAppointmentBookingEntryList;

	@Mock
	private AppointmentBookingService mockAppointmentBookingService;

	@Mock
	private AppointmentContext mockAppointmentContext;

	@Mock
	private AppointmentSlot mockAppointmentSlot1;

	@Mock
	private AppointmentSlot mockAppointmentSlot2;

	@Mock
	private AppointmentSlotForm mockAppointmentSlotForm1;

	@Mock
	private AppointmentSlotForm mockAppointmentSlotForm2;

	@Mock
	private AppointmentSlotLocalService mockAppointmentSlotLocalService;

	@Mock
	private AppointmentSlotModelBuilder mockAppointmentSlotModelBuilder;

	@Mock
	private AppointmentsManagerUtils mockAppointmentsManagerUtils;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private Date mockDate1;

	@Mock
	private Date mockDate2;

	@Mock
	private DateTimeSelection mockDateTimeSelection1;

	@Mock
	private DateTimeSelection mockDateTimeSelection2;

	private MockedStatic<ParamUtil> mockParamUtil;

	private MockedStatic<PropsUtil> mockPropsUtil;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private TimeZone mockTimeZone;

	@Mock
	private User mockUser;

	@Before
	public void activateSetUp() {
		mockPropsUtil = mockStatic(PropsUtil.class);
		mockParamUtil = mockStatic(ParamUtil.class);
	}

	@Test
	public void convertToAppointmentSlotForm_WhenNoError_ThenConvertsTheAppointmentSlotsIntoAppointmentSlotForms() {
		when(mockAppointmentSlotModelBuilder.getAppointmentSlotForm(mockAppointmentSlot1)).thenReturn(mockAppointmentSlotForm1);
		when(mockAppointmentSlotModelBuilder.getAppointmentSlotForm(mockAppointmentSlot2)).thenReturn(mockAppointmentSlotForm2);

		List<AppointmentSlotForm> results = appointmentsManagerService.convertToAppointmentSlotForm(List.of(mockAppointmentSlot1, mockAppointmentSlot2));

		assertThat(results, containsInAnyOrder(mockAppointmentSlotForm1, mockAppointmentSlotForm2));
	}

	@Test
	public void createNewAppointmentSlot_WhenNoErrorAndTypeIsConfiguration_ThenCreatesTheAppointmentSlotWithTheConfigurationExtraFields() {
		long nextId = 1;
		long companyId = 2;
		long userId = 3;
		String userName = "userNameValue";
		String slotType = AppointmentSlotType.CONFIGURATION.name();
		String serviceId = "serviceIdValue";
		String name = "nameValue";
		long daysOfWeek = 4;
		int maxAppointment = 5;
		int duration = 6;
		int startHour = 7;
		int startMinute = 8;
		int endHour = 9;
		int endMinute = 10;
		when(mockCounterLocalService.increment(AppointmentSlot.class.getName())).thenReturn(nextId);
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getFullName()).thenReturn(userName);
		when(mockAppointmentSlotForm1.getSlotType()).thenReturn(slotType);
		when(mockAppointmentSlotForm1.getServiceId()).thenReturn(serviceId);
		when(mockAppointmentSlotLocalService.createAppointmentSlot(nextId)).thenReturn(mockAppointmentSlot1);
		when(mockUser.getTimeZone()).thenReturn(mockTimeZone);
		when(mockAppointmentSlotForm1.getName()).thenReturn(name);
		when(mockAppointmentSlotForm1.getDaysOfWeekBitwiseValue()).thenReturn(daysOfWeek);
		when(mockAppointmentSlotForm1.getMaxAppointments()).thenReturn(maxAppointment);
		when(mockAppointmentSlotForm1.getAppointmentDuration()).thenReturn(duration);
		when(mockAppointmentSlotForm1.getSlotStartDate(mockTimeZone)).thenReturn(mockDate1);
		when(mockAppointmentSlotForm1.getSlotStartTimeHour()).thenReturn(startHour);
		when(mockAppointmentSlotForm1.getSlotStartTimeMinute()).thenReturn(startMinute);
		when(mockAppointmentSlotForm1.getSlotEndDate(mockTimeZone)).thenReturn(mockDate2);
		when(mockAppointmentSlotForm1.getSlotEndTimeHour()).thenReturn(endHour);
		when(mockAppointmentSlotForm1.getSlotEndTimeMinute()).thenReturn(endMinute);

		appointmentsManagerService.createNewAppointmentSlot(mockAppointmentSlotForm1, mockUser);

		InOrder inOrder = inOrder(mockAppointmentSlotLocalService, mockAppointmentSlot1);
		inOrder.verify(mockAppointmentSlot1, times(1)).setCompanyId(companyId);
		inOrder.verify(mockAppointmentSlot1, times(1)).setUserId(userId);
		inOrder.verify(mockAppointmentSlot1, times(1)).setUserName(userName);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotType(slotType);
		inOrder.verify(mockAppointmentSlot1, times(1)).setServiceId(serviceId);
		inOrder.verify(mockAppointmentSlot1, times(1)).setName(name);
		inOrder.verify(mockAppointmentSlot1, times(1)).setDaysOfWeekBitwise(daysOfWeek);
		inOrder.verify(mockAppointmentSlot1, times(1)).setMaxAppointments(maxAppointment);
		inOrder.verify(mockAppointmentSlot1, times(1)).setAppointmentDuration(duration);
		inOrder.verify(mockAppointmentSlot1, times(1)).setStartDate(mockDate1);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotStartTimeHour(startHour);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotStartTimeMinute(startMinute);
		inOrder.verify(mockAppointmentSlot1, times(1)).setEndDate(mockDate2);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotEndTimeHour(endHour);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotEndTimeMinute(endMinute);
		inOrder.verify(mockAppointmentSlotLocalService, times(1)).addAppointmentSlot(mockAppointmentSlot1);
	}

	@Test
	public void createNewAppointmentSlot_WhenNoErrorAndTypeIsNotConfiguration_ThenCreatesTheAppointmentSlotWithoutTheExtraFields() {
		long nextId = 1;
		long companyId = 2;
		long userId = 3;
		String userName = "userNameValue";
		String slotType = "somethingElseValue";
		String serviceId = "serviceIdValue";
		String name = "nameValue";
		long daysOfWeek = 4;
		int startHour = 7;
		int startMinute = 8;
		int endHour = 9;
		int endMinute = 10;
		when(mockCounterLocalService.increment(AppointmentSlot.class.getName())).thenReturn(nextId);
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getFullName()).thenReturn(userName);
		when(mockAppointmentSlotForm1.getSlotType()).thenReturn(slotType);
		when(mockAppointmentSlotForm1.getServiceId()).thenReturn(serviceId);
		when(mockAppointmentSlotLocalService.createAppointmentSlot(nextId)).thenReturn(mockAppointmentSlot1);
		when(mockUser.getTimeZone()).thenReturn(mockTimeZone);
		when(mockAppointmentSlotForm1.getName()).thenReturn(name);
		when(mockAppointmentSlotForm1.getDaysOfWeekBitwiseValue()).thenReturn(daysOfWeek);
		when(mockAppointmentSlotForm1.getSlotStartDate(mockTimeZone)).thenReturn(mockDate1);
		when(mockAppointmentSlotForm1.getSlotStartTimeHour()).thenReturn(startHour);
		when(mockAppointmentSlotForm1.getSlotStartTimeMinute()).thenReturn(startMinute);
		when(mockAppointmentSlotForm1.getSlotEndDate(mockTimeZone)).thenReturn(mockDate2);
		when(mockAppointmentSlotForm1.getSlotEndTimeHour()).thenReturn(endHour);
		when(mockAppointmentSlotForm1.getSlotEndTimeMinute()).thenReturn(endMinute);

		appointmentsManagerService.createNewAppointmentSlot(mockAppointmentSlotForm1, mockUser);

		InOrder inOrder = inOrder(mockAppointmentSlotLocalService, mockAppointmentSlot1);
		inOrder.verify(mockAppointmentSlot1, times(1)).setCompanyId(companyId);
		inOrder.verify(mockAppointmentSlot1, times(1)).setUserId(userId);
		inOrder.verify(mockAppointmentSlot1, times(1)).setUserName(userName);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotType(slotType);
		inOrder.verify(mockAppointmentSlot1, times(1)).setServiceId(serviceId);
		inOrder.verify(mockAppointmentSlot1, times(1)).setName(name);
		inOrder.verify(mockAppointmentSlot1, times(1)).setDaysOfWeekBitwise(daysOfWeek);
		inOrder.verify(mockAppointmentSlot1, times(1)).setStartDate(mockDate1);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotStartTimeHour(startHour);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotStartTimeMinute(startMinute);
		inOrder.verify(mockAppointmentSlot1, times(1)).setEndDate(mockDate2);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotEndTimeHour(endHour);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotEndTimeMinute(endMinute);
		inOrder.verify(mockAppointmentSlotLocalService, times(1)).addAppointmentSlot(mockAppointmentSlot1);

		verify(mockAppointmentSlot1, never()).setMaxAppointments(anyInt());
		verify(mockAppointmentSlot1, never()).setAppointmentDuration(anyInt());
	}

	@Test
	public void getAppointmentBookingEntryFromRequest_WhenNoError_ThenReturnsTheEntry() {
		mockParamUtil.when(() -> ParamUtil.getLong(mockActionRequest, "appointmentEntryId")).thenReturn(11l);
		mockParamUtil.when(() -> ParamUtil.getString(mockActionRequest, "appointmentId")).thenReturn("myAppointmentIdValue");
		mockParamUtil.when(() -> ParamUtil.getLong(mockActionRequest, "classNameId")).thenReturn(22l);
		mockParamUtil.when(() -> ParamUtil.getLong(mockActionRequest, "classPK")).thenReturn(33l);

		AppointmentBookingEntry result = appointmentsManagerService.getAppointmentBookingEntryFromRequest(mockActionRequest);

		assertThat(result.getAppointmentEntryId(), equalTo(11l));
		assertThat(result.getAppointmentId(), equalTo("myAppointmentIdValue"));
		assertThat(result.getClassNameId(), equalTo(22l));
		assertThat(result.getClassPK(), equalTo(33l));

	}

	@Test
	public void getAppointmentSlotForm_WhenNoError_ThenReturnsTheAppointmentSlotForm() throws PortalException {
		long id = 3;
		when(mockAppointmentSlotLocalService.getAppointmentSlot(id)).thenReturn(mockAppointmentSlot1);
		when(mockAppointmentSlotModelBuilder.getAppointmentSlotForm(mockAppointmentSlot1)).thenReturn(mockAppointmentSlotForm1);

		AppointmentSlotForm result = appointmentsManagerService.getAppointmentSlotForm(id);

		assertThat(result, sameInstance(mockAppointmentSlotForm1));
	}

	@Test
	public void getAppointmentSlotFormFromRequest_WhenIdGreaterThanZero_ThenReturnsTheAppointmentSlotFormFromRequestAndTheExistingAppointmentSlot() throws PortalException {
		long id = 1;
		String slotType = "somethingElseValue";
		String serviceId = "serviceIdValue";
		String name = "nameValue";
		int duration = 2;
		int maxAppointments = 3;
		long daysOfWeekBitwise = 4;
		int[] daysOfWeek = new int[] { 1, 2, 3, 4, 5, 6 };
		when(mockAppointmentsManagerUtils.getStartDateTimeFromRequest(mockActionRequest)).thenReturn(mockDateTimeSelection1);
		when(mockAppointmentsManagerUtils.getEndDateTimeFromRequest(mockActionRequest)).thenReturn(mockDateTimeSelection2);
		mockParamUtil.when(() -> ParamUtil.getLong(mockActionRequest, "appointmentSlotId", 0l)).thenReturn(id);
		when(mockAppointmentSlotLocalService.getAppointmentSlot(id)).thenReturn(mockAppointmentSlot1);
		when(mockAppointmentSlot1.getAppointmentSlotId()).thenReturn(id);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockActionRequest, "appointmentDuration")).thenReturn(duration);
		mockParamUtil.when(() -> ParamUtil.getString(mockActionRequest, "name")).thenReturn(name);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockActionRequest, "maxAppointments", 1)).thenReturn(maxAppointments);
		mockParamUtil.when(() -> ParamUtil.getIntegerValues(mockActionRequest, "daysOfWeek", new int[0])).thenReturn(daysOfWeek);
		when(mockAppointmentsManagerUtils.getDaysOfWeekBitwiseValue(daysOfWeek)).thenReturn(daysOfWeekBitwise);
		when(mockAppointmentSlot1.getServiceId()).thenReturn(serviceId);
		when(mockAppointmentSlot1.getSlotType()).thenReturn(slotType);

		AppointmentSlotForm result = appointmentsManagerService.getAppointmentSlotFormFromRequest(mockActionRequest);

		assertThat(result.getAppointmentSlotId(), equalTo(id));
		assertThat(result.getStartDateTime(), equalTo(mockDateTimeSelection1));
		assertThat(result.getEndDateTime(), equalTo(mockDateTimeSelection2));
		assertThat(result.getServiceId(), equalTo(serviceId));
		assertThat(result.getSlotType(), equalTo(slotType));
		assertThat(result.getAppointmentDuration(), equalTo(duration));
		assertThat(result.getName(), equalTo(name));
		assertThat(result.getMaxAppointments(), equalTo(maxAppointments));
		assertThat(result.getDaysOfWeekBitwiseValue(), equalTo(daysOfWeekBitwise));
	}

	@Test
	public void getAppointmentSlotFormFromRequest_WhenIdLessThanZero_ThenReturnsTheAppointmentSlotFormFromRequest() throws PortalException {
		long id = 0;
		String slotType = "somethingElseValue";
		String serviceId = "serviceIdValue";
		String name = "nameValue";
		int duration = 2;
		int maxAppointments = 3;
		long daysOfWeekBitwise = 4;
		int[] daysOfWeek = new int[] { 1, 2, 3, 4, 5, 6 };
		when(mockAppointmentsManagerUtils.getStartDateTimeFromRequest(mockActionRequest)).thenReturn(mockDateTimeSelection1);
		when(mockAppointmentsManagerUtils.getEndDateTimeFromRequest(mockActionRequest)).thenReturn(mockDateTimeSelection2);
		mockParamUtil.when(() -> ParamUtil.getLong(mockActionRequest, "appointmentSlotId", 0l)).thenReturn(id);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockActionRequest, "appointmentDuration")).thenReturn(duration);
		mockParamUtil.when(() -> ParamUtil.getString(mockActionRequest, "slotType")).thenReturn(slotType);
		mockParamUtil.when(() -> ParamUtil.getString(mockActionRequest, "serviceId")).thenReturn(serviceId);
		mockParamUtil.when(() -> ParamUtil.getString(mockActionRequest, "name")).thenReturn(name);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockActionRequest, "maxAppointments", 1)).thenReturn(maxAppointments);
		mockParamUtil.when(() -> ParamUtil.getIntegerValues(mockActionRequest, "daysOfWeek", new int[0])).thenReturn(daysOfWeek);
		when(mockAppointmentsManagerUtils.getDaysOfWeekBitwiseValue(daysOfWeek)).thenReturn(daysOfWeekBitwise);

		AppointmentSlotForm result = appointmentsManagerService.getAppointmentSlotFormFromRequest(mockActionRequest);

		assertThat(result.getAppointmentSlotId(), equalTo(id));
		assertThat(result.getStartDateTime(), equalTo(mockDateTimeSelection1));
		assertThat(result.getEndDateTime(), equalTo(mockDateTimeSelection2));
		assertThat(result.getServiceId(), equalTo(serviceId));
		assertThat(result.getSlotType(), equalTo(slotType));
		assertThat(result.getAppointmentDuration(), equalTo(duration));
		assertThat(result.getName(), equalTo(name));
		assertThat(result.getMaxAppointments(), equalTo(maxAppointments));
		assertThat(result.getDaysOfWeekBitwiseValue(), equalTo(daysOfWeekBitwise));
	}

	@Test
	public void getBlankAppointmentSlotForm_WhenNoError_ThenReturnsAblankSlotWithBasicValuesSet() {
		String slotType = "somethingElseValue";
		String serviceId = "serviceIdValue";
		when(mockAppointmentsManagerUtils.getStartDateTimeSelection(mockTimeZone)).thenReturn(mockDateTimeSelection1);
		when(mockAppointmentsManagerUtils.getEndDateTimeSelection(mockTimeZone)).thenReturn(mockDateTimeSelection2);

		AppointmentSlotForm result = appointmentsManagerService.getBlankAppointmentSlotForm(mockTimeZone, slotType, serviceId);

		assertThat(result.getAppointmentSlotId(), equalTo(0l));
		assertThat(result.getStartDateTime(), equalTo(mockDateTimeSelection1));
		assertThat(result.getEndDateTime(), equalTo(mockDateTimeSelection2));
		assertThat(result.getServiceId(), equalTo(serviceId));
		assertThat(result.getSlotType(), equalTo(slotType));
		assertThat(result.getAppointmentDuration(), equalTo(0));
		assertThat(result.getName(), equalTo(null));
		assertThat(result.getMaxAppointments(), equalTo(0));
		assertThat(result.getDaysOfWeekBitwiseValue(), equalTo(0l));
	}

	@Test
	public void getBookedAppointments_WhenNoError_ThenReturnsTheBookedAppointments() {
		long companyId = 2;
		String serviceId = "serviceIdValue";
		when(mockAppointmentsManagerUtils.getAppointmentContext(mockDateTimeSelection1, mockDateTimeSelection2)).thenReturn(mockAppointmentContext);
		when(mockAppointmentBookingService.getAppointments(companyId, serviceId, mockAppointmentContext)).thenReturn(mockAppointmentBookingEntryList);

		List<AppointmentBookingEntry> results = appointmentsManagerService.getBookedAppointments(companyId, serviceId, mockDateTimeSelection1, mockDateTimeSelection2);

		assertThat(results, sameInstance(mockAppointmentBookingEntryList));
	}

	@Test
	public void getFilterEndDate_WhenSearchExecutedIsFalse_ThenReturnsDefaultValue() {
		when(mockAppointmentsManagerUtils.getEndDateTimeSelection(mockTimeZone)).thenReturn(mockDateTimeSelection1);

		DateTimeSelection result = appointmentsManagerService.getFilterEndDate(false, mockRenderRequest, mockTimeZone);

		assertThat(result, sameInstance(mockDateTimeSelection1));
	}

	@Test
	public void getFilterEndDate_WhenSearchExecutedIsTrueAndDateFromRequestHasValidYear_ThenReturnsTheValueFromRequest() {
		when(mockAppointmentsManagerUtils.getEndDateTimeFromRequest(mockRenderRequest)).thenReturn(mockDateTimeSelection2);
		when(mockDateTimeSelection2.getYear()).thenReturn(1);

		DateTimeSelection result = appointmentsManagerService.getFilterEndDate(true, mockRenderRequest, mockTimeZone);

		assertThat(result, sameInstance(mockDateTimeSelection2));
	}

	@Test
	public void getFilterEndDate_WhenSearchExecutedIsTrueButDateFromRequestHasYearInvalid_ThenReturnsDefaultValue() {
		when(mockAppointmentsManagerUtils.getEndDateTimeFromRequest(mockRenderRequest)).thenReturn(mockDateTimeSelection2);
		when(mockDateTimeSelection2.getYear()).thenReturn(0);
		when(mockAppointmentsManagerUtils.getEndDateTimeSelection(mockTimeZone)).thenReturn(mockDateTimeSelection1);

		DateTimeSelection result = appointmentsManagerService.getFilterEndDate(true, mockRenderRequest, mockTimeZone);

		assertThat(result, sameInstance(mockDateTimeSelection1));
	}

	@Test
	public void getFilterStartDate_WhenSearchExecutedIsFalse_ThenReturnsDefaultValue() {
		when(mockAppointmentsManagerUtils.getStartDateTimeSelection(mockTimeZone)).thenReturn(mockDateTimeSelection1);

		DateTimeSelection result = appointmentsManagerService.getFilterStartDate(false, mockRenderRequest, mockTimeZone);

		assertThat(result, sameInstance(mockDateTimeSelection1));
	}

	@Test
	public void getFilterStartDate_WhenSearchExecutedIsTrueAndDateFromRequestHasValidYear_ThenReturnsTheValueFromRequest() {
		when(mockAppointmentsManagerUtils.getStartDateTimeFromRequest(mockRenderRequest)).thenReturn(mockDateTimeSelection2);
		when(mockDateTimeSelection2.getYear()).thenReturn(1);

		DateTimeSelection result = appointmentsManagerService.getFilterStartDate(true, mockRenderRequest, mockTimeZone);

		assertThat(result, sameInstance(mockDateTimeSelection2));
	}

	@Test
	public void getFilterStartDate_WhenSearchExecutedIsTrueButDateFromRequestHasYearInvalid_ThenReturnsDefaultValue() {
		when(mockAppointmentsManagerUtils.getStartDateTimeFromRequest(mockRenderRequest)).thenReturn(mockDateTimeSelection2);
		when(mockDateTimeSelection2.getYear()).thenReturn(0);
		when(mockAppointmentsManagerUtils.getStartDateTimeSelection(mockTimeZone)).thenReturn(mockDateTimeSelection1);

		DateTimeSelection result = appointmentsManagerService.getFilterStartDate(true, mockRenderRequest, mockTimeZone);

		assertThat(result, sameInstance(mockDateTimeSelection1));
	}

	@After
	public void teardown() {
		mockPropsUtil.close();
		mockParamUtil.close();
	}

	@Test
	public void updateAppointmentSlot_WhenNoErrorAndTypeIsConfiguration_ThenUpdatesTheAppointmentSlotWithTheConfigurationExtraFields() throws PortalException {
		long id = 1;
		String slotType = AppointmentSlotType.CONFIGURATION.name();
		String name = "nameValue";
		long daysOfWeek = 4;
		int maxAppointment = 5;
		int duration = 6;
		int startHour = 7;
		int startMinute = 8;
		int endHour = 9;
		int endMinute = 10;
		when(mockAppointmentSlotForm1.getAppointmentSlotId()).thenReturn(id);
		when(mockAppointmentSlotLocalService.getAppointmentSlot(id)).thenReturn(mockAppointmentSlot1);
		when(mockAppointmentSlotForm1.getSlotType()).thenReturn(slotType);
		when(mockAppointmentSlotForm1.getName()).thenReturn(name);
		when(mockAppointmentSlotForm1.getDaysOfWeekBitwiseValue()).thenReturn(daysOfWeek);
		when(mockAppointmentSlotForm1.getMaxAppointments()).thenReturn(maxAppointment);
		when(mockAppointmentSlotForm1.getAppointmentDuration()).thenReturn(duration);
		when(mockAppointmentSlotForm1.getSlotStartDate(mockTimeZone)).thenReturn(mockDate1);
		when(mockAppointmentSlotForm1.getSlotStartTimeHour()).thenReturn(startHour);
		when(mockAppointmentSlotForm1.getSlotStartTimeMinute()).thenReturn(startMinute);
		when(mockAppointmentSlotForm1.getSlotEndDate(mockTimeZone)).thenReturn(mockDate2);
		when(mockAppointmentSlotForm1.getSlotEndTimeHour()).thenReturn(endHour);
		when(mockAppointmentSlotForm1.getSlotEndTimeMinute()).thenReturn(endMinute);

		appointmentsManagerService.updateAppointmentSlot(mockAppointmentSlotForm1, mockTimeZone);

		InOrder inOrder = inOrder(mockAppointmentSlotLocalService, mockAppointmentSlot1);
		inOrder.verify(mockAppointmentSlot1, times(1)).setName(name);
		inOrder.verify(mockAppointmentSlot1, times(1)).setDaysOfWeekBitwise(daysOfWeek);
		inOrder.verify(mockAppointmentSlot1, times(1)).setMaxAppointments(maxAppointment);
		inOrder.verify(mockAppointmentSlot1, times(1)).setAppointmentDuration(duration);
		inOrder.verify(mockAppointmentSlot1, times(1)).setStartDate(mockDate1);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotStartTimeHour(startHour);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotStartTimeMinute(startMinute);
		inOrder.verify(mockAppointmentSlot1, times(1)).setEndDate(mockDate2);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotEndTimeHour(endHour);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotEndTimeMinute(endMinute);
		inOrder.verify(mockAppointmentSlotLocalService, times(1)).updateAppointmentSlot(mockAppointmentSlot1);
	}

	@Test
	public void updateAppointmentSlot_WhenNoErrorAndTypeIsNotConfiguration_ThenUpdatesTheAppointmentSlotWithoutTheExtraFields() throws PortalException {
		long id = 1;
		String slotType = "somethingElseValue";
		String name = "nameValue";
		long daysOfWeek = 4;
		int startHour = 7;
		int startMinute = 8;
		int endHour = 9;
		int endMinute = 10;
		when(mockAppointmentSlotForm1.getAppointmentSlotId()).thenReturn(id);
		when(mockAppointmentSlotLocalService.getAppointmentSlot(id)).thenReturn(mockAppointmentSlot1);
		when(mockAppointmentSlotForm1.getSlotType()).thenReturn(slotType);
		when(mockAppointmentSlotForm1.getName()).thenReturn(name);
		when(mockAppointmentSlotForm1.getDaysOfWeekBitwiseValue()).thenReturn(daysOfWeek);
		when(mockAppointmentSlotForm1.getSlotStartDate(mockTimeZone)).thenReturn(mockDate1);
		when(mockAppointmentSlotForm1.getSlotStartTimeHour()).thenReturn(startHour);
		when(mockAppointmentSlotForm1.getSlotStartTimeMinute()).thenReturn(startMinute);
		when(mockAppointmentSlotForm1.getSlotEndDate(mockTimeZone)).thenReturn(mockDate2);
		when(mockAppointmentSlotForm1.getSlotEndTimeHour()).thenReturn(endHour);
		when(mockAppointmentSlotForm1.getSlotEndTimeMinute()).thenReturn(endMinute);

		appointmentsManagerService.updateAppointmentSlot(mockAppointmentSlotForm1, mockTimeZone);

		InOrder inOrder = inOrder(mockAppointmentSlotLocalService, mockAppointmentSlot1);
		inOrder.verify(mockAppointmentSlot1, times(1)).setName(name);
		inOrder.verify(mockAppointmentSlot1, times(1)).setDaysOfWeekBitwise(daysOfWeek);
		inOrder.verify(mockAppointmentSlot1, times(1)).setStartDate(mockDate1);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotStartTimeHour(startHour);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotStartTimeMinute(startMinute);
		inOrder.verify(mockAppointmentSlot1, times(1)).setEndDate(mockDate2);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotEndTimeHour(endHour);
		inOrder.verify(mockAppointmentSlot1, times(1)).setSlotEndTimeMinute(endMinute);
		inOrder.verify(mockAppointmentSlotLocalService, times(1)).updateAppointmentSlot(mockAppointmentSlot1);

		verify(mockAppointmentSlot1, never()).setMaxAppointments(anyInt());
		verify(mockAppointmentSlot1, never()).setAppointmentDuration(anyInt());
	}
}
