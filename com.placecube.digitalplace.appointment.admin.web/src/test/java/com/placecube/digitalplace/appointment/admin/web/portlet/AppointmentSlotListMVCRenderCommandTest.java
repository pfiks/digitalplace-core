package com.placecube.digitalplace.appointment.admin.web.portlet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.appointment.admin.web.model.AppointmentSlotForm;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;
import com.placecube.digitalplace.appointment.booking.constants.AppointmentSlotType;
import com.placecube.digitalplace.appointment.service.AppointmentSlotLocalService;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentSlotListMVCRenderCommandTest {

	@InjectMocks
	private AppointmentSlotListMVCRenderCommand appointmentSlotListMVCRenderCommand;

	@Mock
	private AppointmentSlotLocalService mockAppointmentSlotLocalService;

	@Mock
	private AppointmentsManagerService mockAppointmentsManagerService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	private MockedStatic<ParamUtil> mockParamUtil;

	@Mock
	private Portal mockPortal;

	private MockedStatic<PropsUtil> mockPropsUtil;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	private SearchContainer<AppointmentSlotForm> mockSearchContainer1;

	private SearchContainer<AppointmentSlotForm> mockSearchContainer2;

	@Mock
	private List<String> mockServiceIds;

	@SuppressWarnings("unchecked")
	@Before
	public void activateSetUp() {
		mockPropsUtil = mockStatic(PropsUtil.class);
		mockParamUtil = mockStatic(ParamUtil.class);
		mockSearchContainer1 = mock(SearchContainer.class);
		mockSearchContainer2 = mock(SearchContainer.class);
	}

	@Test
	public void render_WhenNoErrorAndServiceIdInvalid_ThenReturnsViewJSPAndsSetsRequestAttributesApartFromSearchContainers() throws PortletException {
		long companyId = 122;
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getCompanyId(mockHttpServletRequest)).thenReturn(companyId);
		mockParamUtil.when(() -> ParamUtil.getString(mockRenderRequest, "serviceId", "")).thenReturn("");
		when(mockAppointmentsManagerService.getAvailableServiceIds(companyId)).thenReturn(mockServiceIds);

		String result = appointmentSlotListMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/view-slots.jsp"));
		verify(mockRenderRequest, never()).setAttribute(eq("searchContainer"), any());
		verify(mockRenderRequest, times(1)).setAttribute("availableServiceIds", mockServiceIds);
		verify(mockRenderRequest, times(1)).setAttribute("serviceId", "");
	}

	@Test
	public void render_WhenNoErrorAndServiceIdSpecified_ThenSetsTheSearchContainersAndCreationMenuAsAttributesAndReturnsViewJSP() throws PortletException {
		long companyId = 122;
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getCompanyId(mockHttpServletRequest)).thenReturn(companyId);
		mockParamUtil.when(() -> ParamUtil.getString(mockRenderRequest, "serviceId", "")).thenReturn("myServiceIdValue");
		when(mockAppointmentsManagerService.getAvailableServiceIds(companyId)).thenReturn(mockServiceIds);
		when(mockAppointmentsManagerService.getSearchContainer(mockRenderRequest, mockRenderResponse, AppointmentSlotType.CONFIGURATION.name())).thenReturn(mockSearchContainer1);
		when(mockAppointmentsManagerService.getSearchContainer(mockRenderRequest, mockRenderResponse, AppointmentSlotType.EXCLUSION.name())).thenReturn(mockSearchContainer2);

		String result = appointmentSlotListMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/view-slots.jsp"));
		verify(mockRenderRequest, times(1)).setAttribute("searchContainerSlotConfiguration", mockSearchContainer1);
		verify(mockRenderRequest, times(1)).setAttribute("searchContainerSlotExclusion", mockSearchContainer2);
		verify(mockRenderRequest, times(1)).setAttribute("availableServiceIds", mockServiceIds);
		verify(mockRenderRequest, times(1)).setAttribute("serviceId", "myServiceIdValue");
		verify(mockAppointmentSlotLocalService, times(1)).countAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(companyId, "CONFIGURATION", "myServiceIdValue");
		verify(mockAppointmentSlotLocalService, times(1)).countAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(companyId, "EXCLUSION", "myServiceIdValue");
	}

	@After
	public void teardown() {
		mockPropsUtil.close();
		mockParamUtil.close();
	}

}
