package com.placecube.digitalplace.group.restadmin.web.model;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.group.rest.model.GroupWebhook;

public class WebhookFormModel {

	private final GroupWebhook webhook;
	private final String className;
	private final long webhookId;
	private String name;
	private boolean enabled;
	private long classPK;
	private String payloadURL;

	public WebhookFormModel(GroupWebhook webhook) {
		this.webhook = webhook;
		className = webhook.getClassName();
		webhookId = webhook.getWebhookId();
		name = webhook.getName();
		payloadURL = webhook.getPayloadURL();
		enabled = webhook.isEnabled();
		classPK = webhook.getClassPK();
	}

	public WebhookFormModel(String className) {
		webhook = null;
		this.className = className;
		webhookId = 0L;
		name = StringPool.BLANK;
		payloadURL = StringPool.BLANK;
		enabled = true;
		classPK = 0L;
	}

	public long getClassPK() {
		return classPK;
	}

	public boolean isActionSelected(String eventActionName) {
		return Validator.isNull(webhook) ? false : webhook.getEvents().contains(eventActionName);
	}

	public String getClassName() {
		return className;
	}

	public long getWebhookId() {
		return webhookId;
	}

	public String getPayloadURL() {
		return payloadURL;
	}

	public String getName() {
		return name;
	}

	public boolean isEnabled() {
		return enabled;
	}

}
