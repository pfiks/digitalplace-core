package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model;

import java.util.ArrayList;
import java.util.List;

public class DDLFilterGroup {

	private String groupName;

	private List<String> options = new ArrayList<>();

	public static DDLFilterGroup newInstance(String groupName) {
		return new DDLFilterGroup(groupName);
	}

	private DDLFilterGroup(String groupName) {
		this.groupName = groupName;
	}

	public void addOption(String option) {
		options.add(option);
	}

	public String getGroupName() {
		return groupName;
	}

	public List<String> getOptions() {
		return options;
	}

}
