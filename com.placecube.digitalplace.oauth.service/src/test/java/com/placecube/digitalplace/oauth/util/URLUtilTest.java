package com.placecube.digitalplace.oauth.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpHeaders;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.util.SystemProperties;

@RunWith(MockitoJUnitRunner.class)
public class URLUtilTest {

	@Mock
	private HttpURLConnection mockHttpURLConnection;

	@Mock
	private OutputStream mockOutputStream;

	private MockedStatic<SystemProperties> mockSystemProperties;

	@Mock
	private URL mockURL;

	@InjectMocks
	private URLUtil urlUtil;

	@Before
	public void activateSetup() {
		mockSystemProperties = mockStatic(SystemProperties.class);
	}

	@Test
	public void buildURL_WhenNoErrors_ThenReturnsURLForUrlString() throws Exception {
		URL result = urlUtil.buildURL("http://localhost:8080");
		assertThat(result, notNullValue());
	}

	@Test(expected = MalformedURLException.class)
	public void buildURL_WhenURIFailsToBuild_ThenThrowsException() throws Exception {
		urlUtil.buildURL("htp:\\test::123");
	}

	@Test(expected = IOException.class)
	public void getConnection_WhenConnectionCreationFails_ThenThrowsIOException() throws IOException {
		when(mockURL.openConnection()).thenThrow(new IOException());

		urlUtil.getConnection(mockURL, 20);
	}

	@Test
	public void getConnection_WhenNoErrors_ThenReturnsConnectionAndSetsProperties() throws IOException {
		when(mockURL.openConnection()).thenReturn(mockHttpURLConnection);

		HttpURLConnection result = urlUtil.getConnection(mockURL, 20);

		assertThat(result, sameInstance(mockHttpURLConnection));

		verify(mockHttpURLConnection, times(1)).setRequestMethod("POST");
		verify(mockHttpURLConnection, times(1)).setDoOutput(true);
		verify(mockHttpURLConnection, times(1)).setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
		verify(mockHttpURLConnection, times(1)).setRequestProperty(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());
		verify(mockHttpURLConnection, times(1)).setRequestProperty(HttpHeaders.CONTENT_LENGTH, Integer.toString(20));
		verify(mockHttpURLConnection, times(1)).setUseCaches(false);
	}

	@Test
	public void getResponse_WhenNoErrors_ThenReturnsConnectionResponse() throws Exception {
		final String response = "{response}";
		when(mockHttpURLConnection.getInputStream()).thenReturn(new ByteArrayInputStream(response.getBytes()));

		String result = urlUtil.getResponse(mockHttpURLConnection);

		assertThat(result, equalTo(response));
	}

	@Test(expected = IOException.class)
	public void getResponse_WhenResponseCannotBeRetrieved_ThenThrowsException() throws Exception {
		when(mockHttpURLConnection.getInputStream()).thenThrow(new IOException());
		urlUtil.getResponse(mockHttpURLConnection);
	}

	@After
	public void teardown() {
		mockSystemProperties.close();
	}

	@Test(expected = IOException.class)
	public void writePostData_WhenDataCannotBeWritten_ThenThrowsIOException() throws IOException {
		when(mockHttpURLConnection.getOutputStream()).thenThrow(new IOException());

		urlUtil.writePostData(mockHttpURLConnection, "data".getBytes());
	}

	@Test
	public void writePostData_WhenNoErrors_ThenWritesDataToOutputStream() throws IOException {
		byte[] data = "data".getBytes();
		when(mockHttpURLConnection.getOutputStream()).thenReturn(mockOutputStream);

		urlUtil.writePostData(mockHttpURLConnection, data);

		verify(mockOutputStream, times(1)).write(data, 0, data.length);
	}

}
