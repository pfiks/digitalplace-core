package com.placecube.digitalplace.ddl.service;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public interface DynamicDataListTemplateContentService {

	String getTranformedTemplate(List<AssetEntry> ddlRecords, DDMStructure ddmStructure, DDMTemplate ddmTemplate, ThemeDisplay themeDisplay, RenderRequest renderRequest) throws PortalException;

	String getTransformedTemplate(DDLRecord ddlRecord, DDMStructure ddmStructure, DDMTemplate ddmTemplate, ThemeDisplay themeDisplay, HttpServletRequest httpServletRequest) throws PortalException;

	String getTransformedTemplate(DDLRecord ddlRecord, long ddlTemplateId, ThemeDisplay themeDisplay, HttpServletRequest httpServletRequest) throws PortalException;

	String getTransformedTemplate(long recordId, DDMStructure ddmStructure, DDMTemplate ddmTemplate, ThemeDisplay themeDisplay, HttpServletRequest httpServletRequest) throws PortalException;

}
