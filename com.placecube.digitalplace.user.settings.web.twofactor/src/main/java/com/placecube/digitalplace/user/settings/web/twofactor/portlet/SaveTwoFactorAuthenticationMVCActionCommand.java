package com.placecube.digitalplace.user.settings.web.twofactor.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.constants.PortletKeys;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.USER_SETTINGS,
		"mvc.command.name=" + TwoFactorAuthenticationKeys.SAVE_TWO_FACTOR_AUTHENTICATION }, service = MVCActionCommand.class)
public class SaveTwoFactorAuthenticationMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(SaveTwoFactorAuthenticationMVCActionCommand.class);

	@Reference
	private UserTwoFactorAuthenticationLocalService userTwoFactorAuthenticationLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String verificationCode = ParamUtil.getString(actionRequest, TwoFactorAuthenticationRequestKeys.TWO_FACTOR_VERIFICATION_CODE);
		String secretKey = ParamUtil.getString(actionRequest, TwoFactorAuthenticationRequestKeys.SECRET_KEY);

		try {
			boolean verified = userTwoFactorAuthenticationLocalService.verifyAndEnableUserTwoFactorAuthentication(themeDisplay.getUser(), verificationCode, secretKey);
			if (verified) {
				actionRequest.setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, Boolean.FALSE);
			} else {
				actionRequest.setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, Boolean.TRUE);
				hideDefaultErrorMessage(actionRequest);
				SessionErrors.add(actionRequest, "twoFactorVerificationFailed");
			}
		} catch (PortalException exception) {
			LOG.error("Exception verifying the one time code " + exception.getMessage());
			LOG.debug(exception);
			actionRequest.setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, Boolean.TRUE);
			hideDefaultErrorMessage(actionRequest);
			SessionErrors.add(actionRequest, "twoFactorVerificationFailed");
		}

		actionRequest.setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, Boolean.TRUE);
		actionRequest.setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY, secretKey);
		actionResponse.getRenderParameters().setValue(PortletRequestKeys.CURRENT_SECTION_ID, TwoFactorAuthenticationKeys.BUNDLE_ID);
	}

}
