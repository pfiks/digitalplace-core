package com.placecube.digitalplace.payment.capitapaymentinterface.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.capita_software_services.www.scp.base.BillingDetails;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleConfiguration;
import com.placecube.digitalplace.payment.capitainterface.service.PaymentService;

@Ignore("Disabled as tests are not compatible with builds on JDK11")
public class PaymentServiceIntegrationTest extends PowerMockito {

	private PaymentService instance;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SimpleSaleConfiguration mockSimpleSaleConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void setUp() throws Exception {

		initMocks(this);

		PaymentProperties properties = new PaymentProperties();
		properties.setEndpoint("https://sbsctest.e-paycapita.com:443/scp/scpws");
		properties.setHmacKey("0kvRVpMjRq+anPskP8cGmDtPR6PjpIZfLtHo8KR6BIHpsGmrRH64PswYxUGNI+KyBMNpWNUi+EZ2CE4T7OT2Ag==");
		properties.setHmacKeyId(456);
		properties.setScpId(8389789);
		properties.setSiteId(84);

		instance = new PaymentService(properties);

		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);

	}

	@After
	public void tearDown() throws Exception {
		instance = null;
	}

	@Test
	public void testCreateBillingDetails() {
		OneOffPaymentOrder order = new OneOffPaymentOrder();
		order.setCardToken("abc123");
		order.setLast4Digits("1234");
		order.setEmailAddress("christine@yahoo.com");
		order.setFullName("Full Name");
		BillingDetails billing = instance.createBillingDetails(order);

		assertEquals(order.getCardToken(), billing.getCard().getStoredCardKey().getToken());
		assertEquals(order.getLast4Digits(), billing.getCard().getStoredCardKey().getLastFourDigits());
		assertEquals(order.getEmailAddress(), billing.getCardHolderDetails().getContact().getEmail());
		assertEquals(order.getFullName(), billing.getCardHolderDetails().getCardHolderName());

		order.setCardToken("");
		order.setLast4Digits("");
		order.setEmailAddress("");
		billing = instance.createBillingDetails(order);

		assertNull(billing.getCard());
		assertEquals(order.getEmailAddress(), billing.getCardHolderDetails().getContact().getEmail());

		order.setCardToken(null);
		order.setLast4Digits(null);
		order.setEmailAddress("");
		billing = instance.createBillingDetails(order);

		assertNull(billing.getCard());
		assertEquals(order.getEmailAddress(), billing.getCardHolderDetails().getContact().getEmail());

	}

	@Test
	public void testInvokePaymentService() {
		OneOffPaymentOrder order = new OneOffPaymentOrder();
		order.setEmailAddress("Christine@mailiantor.com");

		ArrayList<Map<String, String>> listOfItemDetails = new ArrayList<>();
		Map<String, String> summary = new HashMap<>();
		summary.put("amountInMinorUnits", "10000");
		summary.put("saleDescription", "item1");
		summary.put("reference", "123");

		listOfItemDetails.add(summary);

		AddressContext address = new AddressContext("uprn", "44A High Street", "City Centre", "", "", "Bristol", "BS1 2AT");

		order.setAddress(address);
		order.setSimpleSaleConfiguration(mockSimpleSaleConfiguration);
		order.setRequestType(OneOffPaymentOrder.CardRequestType.PAY_ONLY);
		order.setReturnURL("scp:close");
		order.setBackURL("scp:close");
		order.setSaleAmount(10000);
		order.setSaleDescription("test sale");
		order.setSaleReferenceCode(getTransactionID());

		String result = instance.invokePaymentService(order, 11l, true);

		// assertEquals("PAYMENT_SETUP_SUCCESSFUL", result);
		// assertTrue(Validator.isNotNull(order.getBccGeneratedReferenceNumber()));
		// assertTrue(Validator.isNotNull(order.getRedirectUrl()));
		// assertTrue(Validator.isNotNull(order.getTransactionReferenceNumber()));

		// // Set an invalid return URL
		// order.setReturnURL("");
		// result = instance.invokePaymentService(order);

		// assertNotEquals("PAYMENT_SETUP_SUCCESSFUL", result);

		// // Invalid total cost
		// order.setSaleAmount(20000);
		// order.setReturnURL("scp:close");
		// result = instance.invokePaymentService(order);

		// assertNotEquals("PAYMENT_SETUP_SUCCESSFUL", result);

	}

	@Test
	public void testInvokeQueryServiceOneOffPaymentOrder() {

		// Empty order, should fail
		OneOffPaymentOrder order = new OneOffPaymentOrder();
		boolean success = instance.invokeQueryServiceOneOffPaymentOrder(order);
		assertFalse(success);

		// Valid case, calling actual service so will only get an INPROGRESS
		// response as we need to mock 'complete' response from Capita
		order.setEmailAddress("Christine@mailiantor.com");

		ArrayList<Map<String, String>> listOfItemDetails = new ArrayList<>();
		Map<String, String> summary = new HashMap<>();
		summary.put("amountInMinorUnits", "10000");
		summary.put("saleDescription", "item1");
		summary.put("reference", "123");

		listOfItemDetails.add(summary);

		order.setSimpleSaleConfiguration(mockSimpleSaleConfiguration);
		order.setRequestType(OneOffPaymentOrder.CardRequestType.PAY_ONLY);
		order.setReturnURL("scp:close");
		order.setSaleAmount(10000);
		order.setSaleDescription("test sale");

		success = instance.invokeQueryServiceOneOffPaymentOrder(order);
		assertFalse(success);

	}

	private String getTransactionID() {

		UUID uuid = UUID.randomUUID();

		String id = "MG" + uuid.toString();

		if (id.length() > 12) {
			id = id.substring(0, 12);
		}

		return id;
	}

}
