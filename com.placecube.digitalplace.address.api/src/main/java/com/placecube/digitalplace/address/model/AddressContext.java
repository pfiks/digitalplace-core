package com.placecube.digitalplace.address.model;

import java.io.Serializable;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.address.constants.AddressType;

public class AddressContext implements Serializable {

	public static class AddressContextBuilder {

		private String addressLine1;
		private String addressLine2;
		private String addressLine3;
		private String addressLine4;
		private AddressType addressType = AddressType.PERSONAL;
		private String city;
		private boolean current;
		private String postcode;
		private String source;
		private String uprn;

		public AddressContextBuilder() {
		}

		public AddressContext build() {
			return new AddressContext(this);
		}

		public AddressContextBuilder setAddressLine1(String addressLine1) {
			this.addressLine1 = addressLine1;
			return this;
		}

		public AddressContextBuilder setAddressLine2(String addressLine2) {
			this.addressLine2 = addressLine2;
			return this;
		}

		public AddressContextBuilder setAddressLine3(String addressLine3) {
			this.addressLine3 = addressLine3;
			return this;
		}

		public AddressContextBuilder setAddressLine4(String addressLine4) {
			this.addressLine4 = addressLine4;
			return this;
		}

		public AddressContextBuilder setAddressType(AddressType addressType) {
			this.addressType = addressType;
			return this;
		}

		public AddressContextBuilder setCity(String city) {
			this.city = city;
			return this;
		}

		public AddressContextBuilder setCurrent(boolean current) {
			this.current = current;
			return this;
		}

		public AddressContextBuilder setPostcode(String postcode) {
			this.postcode = postcode;
			return this;
		}

		public AddressContextBuilder setSource(String source) {
			this.source = source;
			return this;
		}

		public AddressContextBuilder setUprn(String uprn) {
			this.uprn = uprn;
			return this;
		}
	}

	private static final long serialVersionUID = -1343767519367370398L;
	private final String addressLine1;
	private final String addressLine2;
	private final String addressLine3;
	private final String addressLine4;
	private transient AddressType addressType;
	private final String city;
	private boolean current;
	private final String postcode;
	private final String source;
	private final String uprn;

	public AddressContext(AddressContextBuilder builder) {
		addressType = builder.addressType;
		addressLine1 = builder.addressLine1;
		addressLine2 = builder.addressLine2;
		addressLine3 = builder.addressLine3;
		addressLine4 = builder.addressLine4;
		city = builder.city;
		postcode = builder.postcode;
		uprn = builder.uprn;
		source = builder.source;
		current = builder.current;
	}

	public AddressContext(String uprn, String addressLine1, String addressLine2, String addressLine3, String addressLine4, String city, String postcode) {
		this.uprn = uprn;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.addressLine3 = addressLine3;
		this.addressLine4 = addressLine4;
		addressType = AddressType.PERSONAL;
		this.city = city;
		this.postcode = postcode;
		current = true;
		source = StringPool.BLANK;
	}

	public String getAddressLine1() {
		return GetterUtil.getString(addressLine1);
	}

	public String getAddressLine2() {
		return GetterUtil.getString(addressLine2);
	}

	public String getAddressLine3() {
		return GetterUtil.getString(addressLine3);
	}

	public String getAddressLine4() {
		return GetterUtil.getString(addressLine4);
	}

	public AddressType getAddressType() {
		return addressType;
	}

	public String getCity() {
		return GetterUtil.getString(city);
	}

	public String getFullAddress() {
		return String.join(StringPool.SPACE, getAddressLine1(), getAddressLine2(), getAddressLine3(), getAddressLine4(), getCity(), getPostcode()).replaceAll("\\s+", " ");
	}

	public String getPostcode() {
		return GetterUtil.getString(postcode);
	}

	public String getSource() {
		return GetterUtil.getString(source);
	}

	public String getUPRN() {
		return uprn;
	}

	public boolean isCurrent() {
		return current;
	}

	public void setAddressType(AddressType addressType) {
		this.addressType = addressType;
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}

	@Override
	public String toString() {
		return "AddressContext [addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", addressLine3=" + addressLine3 + ", addressLine4=" + addressLine4 + ", city=" + city
				+ ", postcode=" + postcode + ", uprn=" + uprn + ", source=" + source + ", current=" + current + "]";
	}
}
