package com.placecube.digitalplace.user.account.web.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.user.account.constants.WebContentArticles;
import com.placecube.digitalplace.user.account.service.AccountSetupService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class AccountLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(AccountLifecycleListener.class);

	@Reference
	private AccountSetupService accountSetupService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		long companyId = company.getCompanyId();
		LOG.debug("Initialising User account portlets for companyId: " + companyId);

		Group globalGroup = company.getGroup();
		ServiceContext serviceContext = accountSetupService.getServiceContext(globalGroup);

		JournalFolder journalFolder = accountSetupService.addFolder(serviceContext);
		accountSetupService.addArticle(WebContentArticles.USER_ACCOUNT_CONFIRMATION_CREATED_BY_ADMIN, journalFolder, serviceContext);
		accountSetupService.addArticle(WebContentArticles.USER_ACCOUNT_CONFIRMATION_RESET_PASSWORD_INVOKED_BY_ADMIN, journalFolder, serviceContext);
		accountSetupService.addArticle(WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITH_EMAIL_VERIFICATION, journalFolder, serviceContext);
		accountSetupService.addArticle(WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITHOUT_EMAIL_VERIFICATION, journalFolder, serviceContext);
		accountSetupService.addArticle(WebContentArticles.USER_ACCOUNT_SIDE_PANEL_INFO, journalFolder, serviceContext);
		accountSetupService.addArticle(WebContentArticles.USER_ACCOUNT_TWO_FACTOR_AUTHENTICATION, journalFolder, serviceContext);

		LOG.debug("Configuration finished for User account portlets for companyId: " + companyId);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {

	}

}
