package com.placecube.digitalplace.editor.styles.contributor.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.editor.styles.contributor.constants.ButtonStyle;

@RunWith(PowerMockRunner.class)
public class CKEditorStyleServiceTest extends PowerMockito {

	@InjectMocks
	private CKEditorStyleService ckeditorStyleService;

	@Mock
	private JSONArray mockJsonArrayStyles;

	@Mock
	private JSONObject mockJsonCKEditorConfig;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJsonObjectAttributes;

	@Mock
	private JSONObject mockJsonObjectStyle;

	@Test
	public void createStyle_WhenCalledWithButtonStyleType_ThenReturnCreatedStyleJsonObject() {
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonObjectAttributes, mockJsonObjectStyle);

		JSONObject result = ckeditorStyleService.createStyle(ButtonStyle.BASE);

		verify(mockJsonObjectAttributes, times(1)).put("class", ButtonStyle.BASE.getValue());

		verify(mockJsonObjectStyle, times(1)).put("attributes", mockJsonObjectAttributes);
		verify(mockJsonObjectStyle, times(1)).put("element", ButtonStyle.BASE.getElement());
		verify(mockJsonObjectStyle, times(1)).put("name", ButtonStyle.BASE.getName());

		assertThat(result, equalTo(mockJsonObjectStyle));

	}

	@Test
	public void getStyleSet_WhenStyleSetExistsInEditorConfig_ThenReturnConfigStylesSetJSONArray() {
		when(mockJsonCKEditorConfig.getJSONArray("stylesSet")).thenReturn(mockJsonArrayStyles);

		Optional<JSONArray> result = ckeditorStyleService.getStyleSet(mockJsonCKEditorConfig);

		assertThat(result, Matchers.equalTo(Optional.of(mockJsonArrayStyles)));
	}

	@Test
	public void getStyleSet_WhenStyleSetNotExistsInEditorConfig_ThenReturnEmptyOptional() {
		when(mockJsonCKEditorConfig.getJSONArray("stylesSet")).thenReturn(null);

		Optional<JSONArray> result = ckeditorStyleService.getStyleSet(mockJsonCKEditorConfig);

		assertThat(result, Matchers.equalTo(Optional.empty()));
	}

}
