package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector;

import com.liferay.item.selector.BaseItemSelectorCriterionHandler;
import com.liferay.item.selector.ItemSelectorCriterion;
import com.liferay.item.selector.ItemSelectorCriterionHandler;
import com.liferay.item.selector.ItemSelectorView;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.util.JournalArticleItemSelectorCriterionHandlerUtil;

import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

@Component(service = ItemSelectorCriterionHandler.class)
public class JournalArticleItemSelectorCriterionHandler
	extends BaseItemSelectorCriterionHandler<JournalArticleItemSelectorCriterion> {

	@Override
	public Class<JournalArticleItemSelectorCriterion>
		getItemSelectorCriterionClass() {

		return JournalArticleItemSelectorCriterion.class;
	}

	@Override
	public List<ItemSelectorView<JournalArticleItemSelectorCriterion>>
		getItemSelectorViews(ItemSelectorCriterion itemSelectorCriterion) {

		return JournalArticleItemSelectorCriterionHandlerUtil.
			getFilteredItemSelectorViews(
				itemSelectorCriterion,
				super.getItemSelectorViews(itemSelectorCriterion));
	}

	@Activate
	@Override
	protected void activate(BundleContext bundleContext) {
		super.activate(bundleContext);
	}

}