package com.placecube.digitalplace.search.web.portlet.category.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = CategoryLocalService.class)
public class CategoryLocalService {

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;

	public Set<Long> getAggregateCategoryLists(ThemeDisplay themeDisplay, String[] vocabolaries, String[] categories) {
		Set<Long> result = new HashSet<>();

		List<AssetVocabulary> availableVocabularies = getAvailableVocabularies(themeDisplay);
		List<Long> selectedVocabolaries = getLongValueList(vocabolaries);
		List<Long> selectedCategories = getLongValueList(categories);

		for (AssetVocabulary vocabolary : availableVocabularies) {
			if (selectedVocabolaries.contains(vocabolary.getVocabularyId())) {
				vocabolary.getCategories().stream().map(AssetCategory::getCategoryId).forEach(id -> result.add(id));
			} else {
				vocabolary.getCategories().stream().filter(c -> selectedCategories.contains(c.getCategoryId())).map(AssetCategory::getCategoryId).forEach(id -> result.add(id));
			}
		}
		return result;
	}
	
	public List<AssetVocabulary> getAvailableVocabularies(ThemeDisplay themeDisplay) {
		List<AssetVocabulary> vocabularies = assetVocabularyLocalService.getGroupsVocabularies(new long[] { themeDisplay.getScopeGroupId(), themeDisplay.getCompanyGroupId() });
		return vocabularies.stream().filter(assetVocabulary -> assetVocabulary.getCategoriesCount() > 0).collect(Collectors.toList());
	}

	public List<Long> getLongValueList(String[] values) {
		List<Long> results = new ArrayList<>();
		if (null != values) {
			for (String value : values) {
				Long id = GetterUtil.getLong(value, 0l);
				if (id > 0) {
					results.add(id);
				}
			}
		}
		return results;
	}
}