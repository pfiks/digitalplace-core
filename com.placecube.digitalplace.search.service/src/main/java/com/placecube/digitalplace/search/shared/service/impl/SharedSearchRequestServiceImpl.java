package com.placecube.digitalplace.search.shared.service.impl;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;
import com.placecube.digitalplace.search.internal.service.SharedSearchService;
import com.placecube.digitalplace.search.internal.task.SharedSearchTaskExecutor;
import com.placecube.digitalplace.search.internal.util.RequestUtil;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@Component(service = SharedSearchRequestService.class)
public class SharedSearchRequestServiceImpl implements SharedSearchRequestService {

	@Reference
	private RequestUtil requestUtil;

	@Reference
	private Portal portal;

	private ServiceTrackerMap<String, SharedSearchContributor> sharedSearchContributors;

	@Reference
	private SharedSearchService sharedSearchService;

	@Reference
	private SharedSearchTaskExecutor sharedSearchTaskExecutor;

	@Override
	public void addSharedActiveFilter(PortletRequest portletRequest, String attributeName, String attributeValue, String filterLabel, String filterColour) {
		ThemeDisplay themeDisplay = requestUtil.getThemeDisplay(portletRequest);

		addSharedActiveFilter(themeDisplay, attributeName, attributeValue, filterLabel, filterColour);
	}

	@Override
	public void addSharedActiveFilter(ThemeDisplay themeDisplay, String attributeName, String attributeValue, String filterLabel, String filterColour) {
		HttpServletRequest httpServletRequest = portal.getOriginalServletRequest(themeDisplay.getRequest());

		SharedSearchSession sharedSearchSession = requestUtil.getOrCreateSharedSearchSession(httpServletRequest, themeDisplay);

		sharedSearchSession.addActiveFilter(attributeName, attributeValue, filterLabel, filterColour);

		requestUtil.updateSharedSearchSession(httpServletRequest, sharedSearchSession);
	}

	@Override
	public void clearSharedSearchSession(PortletRequest portletRequest) {
		ThemeDisplay themeDisplay = requestUtil.getThemeDisplay(portletRequest);
		HttpServletRequest httpServletRequest = requestUtil.getOriginalHttpServletRequest(portletRequest);

		requestUtil.removeSharedSearchSession(httpServletRequest, themeDisplay.getPlid());
	}

	@Override
	public void removeSharedActiveFilterValue(PortletRequest portletRequest, String filterName, String filterValue) {
		ThemeDisplay themeDisplay = requestUtil.getThemeDisplay(portletRequest);
		HttpServletRequest httpServletRequest = requestUtil.getOriginalHttpServletRequest(portletRequest);

		SharedSearchSession sharedSearchSession = requestUtil.getOrCreateSharedSearchSession(httpServletRequest, themeDisplay);
		sharedSearchSession.removeActiveFilterValue(filterName, filterValue);
		requestUtil.updateSharedSearchSession(httpServletRequest, sharedSearchSession);
	}

	@Override
	public void removeSharedSearchSessionAttribute(PortletRequest portletRequest, String attributeName) {
		ThemeDisplay themeDisplay = requestUtil.getThemeDisplay(portletRequest);
		HttpServletRequest httpServletRequest = requestUtil.getOriginalHttpServletRequest(portletRequest);

		SharedSearchSession sharedSearchSession = requestUtil.getOrCreateSharedSearchSession(httpServletRequest, themeDisplay);
		sharedSearchSession.removeAttribute(attributeName);
		requestUtil.updateSharedSearchSession(httpServletRequest, sharedSearchSession);
	}

	@Override
	public SharedSearchResponse search(RenderRequest renderRequest) throws SearchException {
		return sharedSearchTaskExecutor.executeOnlyOnce(() -> sharedSearchService.doSearch(renderRequest, sharedSearchContributors), SharedSearchResponse.class.getSimpleName(), renderRequest);
	}

	@Override
	public void sendRedirectFromParameter(ActionRequest actionRequest, ActionResponse actionResponse, String redirectParameterName) throws IOException {
		String redirectURL = ParamUtil.getString(actionRequest, redirectParameterName);
		if (Validator.isNotNull(redirectURL)) {
			actionResponse.sendRedirect(redirectURL);
		}
	}

	@Override
	public void setSharedSearchSessionSingleValuedAttribute(PortletRequest portletRequest, String attributeName, String attributeValue) {
		ThemeDisplay themeDisplay = requestUtil.getThemeDisplay(portletRequest);
		HttpServletRequest httpServletRequest = requestUtil.getOriginalHttpServletRequest(portletRequest);
		SharedSearchSession sharedSearchSession = requestUtil.getOrCreateSharedSearchSession(httpServletRequest, themeDisplay);
		sharedSearchSession.addSingleValuedAttribute(attributeName, attributeValue);
		requestUtil.updateSharedSearchSession(httpServletRequest, sharedSearchSession);
	}

	@Activate
	protected void activate(BundleContext bundleContext) {
		sharedSearchContributors = ServiceTrackerMapFactory.openSingleValueMap(bundleContext, SharedSearchContributor.class, "javax.portlet.name");
	}

	@Deactivate
	protected void deactivate() {
		sharedSearchContributors.close();
	}
}
