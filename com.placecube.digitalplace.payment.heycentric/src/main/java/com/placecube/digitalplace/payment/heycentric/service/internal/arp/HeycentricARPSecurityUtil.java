package com.placecube.digitalplace.payment.heycentric.service.internal.arp;

import java.security.NoSuchAlgorithmException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.heycentric.service.internal.HeycentricHashingService;

@Component(immediate = true, service = HeycentricARPSecurityUtil.class)
public class HeycentricARPSecurityUtil {

	@Reference
	private HeycentricHashingService heycentricHashingService;

	public String generateDigest(String returnUrl, long userId, String secret) throws NoSuchAlgorithmException {
		String rawHashString = returnUrl + StringPool.PLUS + userId + StringPool.PLUS + secret;
		return heycentricHashingService.rawHashToSHA512(rawHashString);
	}

	public boolean isValidData(String externalReferenceCode, String sid, String agentId, String secret, String digest) throws NoSuchAlgorithmException {
		return externalReferenceCode.equals(sid) && isValidDigest(sid, agentId, secret, digest);
	}

	private boolean isValidDigest(String sid, String agentId, String secret, String payloadDigest) throws NoSuchAlgorithmException {
		if (Validator.isNotNull(payloadDigest)) {
			String rawHashString = sid + StringPool.PLUS + agentId + StringPool.PLUS + secret;
			String calculatedHash = heycentricHashingService.rawHashToSHA512(rawHashString);
			return payloadDigest.equals(calculatedHash);
		}

		return false;
	}
}
