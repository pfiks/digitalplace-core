package com.placecube.digitalplace.formadmin.web.portlet.command;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.form.exception.FormDefinitionException;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.digitalplace.form.service.FormDefinitionRegistry;
import com.placecube.digitalplace.form.service.FormDefinitionService;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, ServiceContextFactory.class })
public class InstallFormDefinitionMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private InstallFormDefinitionMVCActionCommand installFormDefinitionMVCActionCommand;

	@Mock
	private FormDefinition mockFormDefinition;

	@Mock
	protected FormDefinitionRegistry mockFormDefinitionRegistry;

	@Mock
	protected FormDefinitionService mockFormDefinitionService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, ServiceContextFactory.class);

	}

	@Test
	public void doProcessAction_WhenNoError_ThenAddsFormDefinition() throws Exception {

		when(ServiceContextFactory.getInstance(DDMFormInstance.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		String formDefinitionKey = "key";
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.FORM_DEFINITION_KEY)).thenReturn(formDefinitionKey);
		when(mockFormDefinitionRegistry.getFormDefinitionByKey(formDefinitionKey)).thenReturn(mockFormDefinition);

		installFormDefinitionMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockFormDefinitionService, times(1)).addFormDefinition(mockFormDefinition, mockServiceContext);
	}

	@Test(expected = PortletException.class)
	public void doProcessAction_WhenErrorGettingServiceContext_ThenPortletExceptionIsThrown() throws Exception {

		when(ServiceContextFactory.getInstance(DDMFormInstance.class.getName(), mockActionRequest)).thenThrow(new PortalException());

		installFormDefinitionMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test(expected = PortletException.class)
	public void doProcessAction_WhenErrorAddingFormDefinition_ThenPortletExceptionIsThrown() throws Exception {

		when(ServiceContextFactory.getInstance(DDMFormInstance.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		String formDefinitionKey = "key";
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.FORM_DEFINITION_KEY)).thenReturn(formDefinitionKey);
		when(mockFormDefinitionRegistry.getFormDefinitionByKey(formDefinitionKey)).thenReturn(mockFormDefinition);

		doThrow(new FormDefinitionException("exception")).when(mockFormDefinitionService).addFormDefinition(mockFormDefinition, mockServiceContext);

		installFormDefinitionMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

}
