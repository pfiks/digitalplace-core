package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.net.URLEncoder;
import java.util.Optional;

import org.apache.commons.codec.binary.Base32;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.security.SecureRandom;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.converters.Nullable;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class TOTPServiceTest extends PowerMockito {

	private static final String ALGORITHM = "algorithmValue";
	private static final int AUTHENTICATOR_CODE_DURATION_IN_SECS = 30;
	private static final int AUTHENTICATOR_CODE_LENGTH = 5;
	private static final String EMAIL_ADDRESS = "emailValue";
	private static final String ISSUER = "issuerValue";
	private static final String QR_BARCODE_URL = "urlValue%swithSomePlaceholders";
	private static final String SECRET_KEY = "secretValue";

	@Mock
	private Base32 mockBase32;

	@Mock
	private SecureRandom mockSecureRandom;

	@Mock
	private TOTPCodeService mockTotpCodeService;

	@InjectMocks
	private TOTPService totpService;

	@Before
	public void activateSetup() {
		initMocks(this);

	}

	@Test
	public void getOTPUrl_WhenNoError_ThenBuildsAndReturnsTheEncodedOTPUrl() throws Exception {
		String optUrl = "this is my url";
		String expectedEncoded = URLEncoder.encode(optUrl, "UTF-8");
		String expected = String.format(QR_BARCODE_URL, expectedEncoded);
		when(mockTotpCodeService.buildOTPUrl(ISSUER, EMAIL_ADDRESS, SECRET_KEY, ALGORITHM, AUTHENTICATOR_CODE_LENGTH, AUTHENTICATOR_CODE_DURATION_IN_SECS)).thenReturn(optUrl);

		Optional<String> result = totpService.getOTPUrl(QR_BARCODE_URL, ISSUER, EMAIL_ADDRESS, SECRET_KEY, ALGORITHM, AUTHENTICATOR_CODE_LENGTH, AUTHENTICATOR_CODE_DURATION_IN_SECS);

		assertThat(result.get(), equalTo(expected));
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters({ "null,null,null,null", "null,null,null,not null", "null,null,not null,null", "null,null,not null,not null", "null,not null,null,null", "null,not null,null,not null",
		"null,not null,not null,null", "null,not null,not null,not null", "not null,null,null,null", "not null,null,null,not null", "not null,null,not null,null",
		"not null,null,not null,not null", "not null,not null,null,null", "not null,not null,null,not null", "not null,not null,not null,null" })
	public void getOTPUrl_WhenNotAllMandatoryParametersArePassedIn_ThenThrowsIllegalArgumentException(@Nullable String qrBarcodeUrl, @Nullable String issuer, @Nullable String emailAddress,
			@Nullable String secretKey) throws Exception {
		totpService.getOTPUrl(qrBarcodeUrl, issuer, emailAddress, secretKey, ALGORITHM, AUTHENTICATOR_CODE_LENGTH, AUTHENTICATOR_CODE_DURATION_IN_SECS);
	}

	@Test
	public void verifyUserOneTimeCode_WhenOneTimeCodeIsEmpty_ThenReturnsFalse() throws Exception {
		when(mockTotpCodeService.generateTopCode(SECRET_KEY)).thenReturn(Optional.empty());

		boolean result = totpService.verifyUserOneTimeCode(SECRET_KEY, "myCode");

		assertFalse(result);
	}

	@Test
	public void verifyUserOneTimeCode_WhenOneTimeCodeIsNotEmptyAndDoesNotMatchWithGeneratedCode_ThenReturnsFalse() throws Exception {
		String generatedCode = "generatedCodeValue";
		when(mockTotpCodeService.generateTopCode(SECRET_KEY)).thenReturn(Optional.of(generatedCode));

		boolean result = totpService.verifyUserOneTimeCode(SECRET_KEY, "myCode");

		assertFalse(result);
	}

	@Test
	public void verifyUserOneTimeCode_WhenOneTimeCodeIsNotEmptyAndMatchesWithGeneratedCode_ThenReturnsTrue() throws Exception {
		String generatedCode = "correctOneTimeCode";
		when(mockTotpCodeService.generateTopCode(SECRET_KEY)).thenReturn(Optional.of(generatedCode));

		boolean result = totpService.verifyUserOneTimeCode(SECRET_KEY, generatedCode);

		assertTrue(result);
	}

}
