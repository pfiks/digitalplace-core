package com.placecube.digitalplace.search.web.portlet.sortby.indexer;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.search.web.portlet.contenttype.constants.ContentTypeConstants;
import com.placecube.digitalplace.search.web.portlet.sortby.service.SortByService;

public class SortableTitleModelDocumentContributorTest extends PowerMockito {

	@Mock
	private BaseModel mockBaseModel;

	@Mock
	private Document mockDocument;

	@Mock
	private SortByService mockSortByService;

	@InjectMocks
	private SortableTitleModelDocumentContributor sortableTitleModelDocumentContributor;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenSortableFieldTitleNotPresentAndClassNameIsCalendarBooking_ThenAddsTitleKeywordToTheDocument() throws PortalException {
		when(mockDocument.get(Field.getSortableFieldName(Field.TITLE))).thenReturn(null);
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(ContentTypeConstants.CALENDAR_BOOKING_CLASS_NAME);
		when(mockSortByService.getTitleForCalendarBooking(mockDocument)).thenReturn("titleValue");

		sortableTitleModelDocumentContributor.contribute(mockDocument, mockBaseModel);

		verify(mockDocument, times(1)).addKeyword(Field.TITLE, "titleValue");
	}

	@Test
	public void contribute_WhenSortableFieldTitleNotPresentAndClassNameIsMBMessage_ThenAddsTitleKeywordToTheDocument() throws PortalException {
		when(mockDocument.get(Field.getSortableFieldName(Field.TITLE))).thenReturn(null);
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(ContentTypeConstants.MB_MESSAGE_CLASS_NAME);
		when(mockSortByService.getTitleForMBMessage(mockDocument)).thenReturn("titleValue");

		sortableTitleModelDocumentContributor.contribute(mockDocument, mockBaseModel);

		verify(mockDocument, times(1)).addKeyword(Field.TITLE, "titleValue");
	}

	@Test
	public void contribute_WhenSortableFieldTitleNotPresentAndClassNameIsNotCalendarBookingNorMBMessage_ThenDoesNotAddTitleKeywordToTheDocument() {
		when(mockDocument.get(Field.getSortableFieldName(Field.TITLE))).thenReturn(null);
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn("someClassName");

		sortableTitleModelDocumentContributor.contribute(mockDocument, mockBaseModel);

		verify(mockDocument, never()).addKeyword(anyString(), anyString());
	}

	@Test
	public void contribute_WhenSortableTitleFieldPresent_ThenDoesNotAddTitleKeywordToTheDocument() {
		when(mockDocument.get(Field.getSortableFieldName(Field.TITLE))).thenReturn("someValue");

		sortableTitleModelDocumentContributor.contribute(mockDocument, mockBaseModel);

		verify(mockDocument, never()).addKeyword(anyString(), anyString());
	}
}
