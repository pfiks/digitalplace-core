package com.placecube.digitalplace.dataconsumer.utils;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldTypeServicesRegistry;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldValueRenderer;
import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceService;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.oauth2.provider.model.OAuth2Application;
import com.liferay.oauth2.provider.service.OAuth2ApplicationLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.model.DataProviderInputParameter;
import com.placecube.digitalplace.dataconsumer.service.DataTransformationService;
import com.placecube.digitalplace.oauth.service.DigitalPlaceOAuth2Service;

@Component(immediate = true, service = DataConsumerUtils.class)
public class DataConsumerUtils {

	protected static final String EDITED_SUFFED = "_edited";

	protected static final String FORM_FIELD_GROUP_PREFIX = "ddm$$FieldsGroup";

	protected static final String FORM_FIELD_GROUP_VALUE_PREFIX = "#";

	protected static final String FORM_VALUE_PREFIX = "ddm$$";

	protected static final String FORM_VALUE_SUFFIX = StringPool.DOLLAR;

	private static final Log LOG = LogFactoryUtil.getLog(DataConsumerUtils.class);

	@Reference
	private DataTransformationService dataTransformationService;

	@Reference
	private DDMDataProviderInstanceService ddmDataProviderInstanceService;

	@Reference
	private DDMFormFieldTypeServicesRegistry ddmFormFieldTypeServicesTracker;

	@Reference
	private OAuth2ApplicationLocalService oAuth2ApplicationLocalService;

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private DigitalPlaceOAuth2Service digitalPlaceOAuth2Service;

	public DDMFormValues getDDMFormValues(DataConsumer dataconsumer) throws PortalException {
		DDMDataProviderInstance dataProviderInstance = ddmDataProviderInstanceService.getDataProviderInstance(dataconsumer.getDdmDataProviderInstanceId());
		return dataTransformationService.getDataProviderFormValues(dataProviderInstance);
	}

	public Map<String, String> getHeaders(DataConsumer dataconsumer, Locale locale, DDMFormValues dataProviderFields) throws PortalException {
		Map<String, String> headers = dataTransformationService.deserializeMapping(dataconsumer.getHeaders());
		addAuthHeaderIfNecessary(headers, dataProviderFields, locale, dataconsumer.getCompanyId());
		return headers;
	}

	public Map<String, String> getInputParametersMapping(DataConsumer dataConsumer, Locale locale, DDMFormValues dataProviderFields) throws JSONException {
		Map<String, String> confirmedMapping = new HashMap<>();

		List<DataProviderInputParameter> inputFields = dataTransformationService.getInputParameters(dataProviderFields, locale);
		Map<String, String> dataConsumerMappedFields = dataTransformationService.deserializeMapping(dataConsumer.getMapping());

		inputFields.forEach(f -> confirmedMapping.put(f.getName(), dataConsumerMappedFields.get(f.getName())));

		return confirmedMapping;
	}

	public Map<String, String> mapDataProviderFieldsToFormValues(Map<String, String> confirmedMapping, ActionRequest actionRequest, DDMStructure ddmStructure) {

		Map<String, String> mappedValues = new HashMap<>();

		Set<String> generatedFormFieldNames = actionRequest.getParameterMap().keySet().stream().filter(k -> k.startsWith(FORM_VALUE_PREFIX)).collect(Collectors.toSet());

		confirmedMapping.entrySet().forEach(f -> {

			for (String fieldName : generatedFormFieldNames) {
				if (isMappedFieldMatchedWithRequestField(f.getValue(), fieldName)) {
					String value = ParamUtil.getString(actionRequest, fieldName);
					if (value != null) {
						value = getValueOrOptionReference(ddmStructure, f.getValue(), value);
						mappedValues.put(f.getKey(), value);
					}
				}
			}

		});

		return mappedValues;
	}

	public Map<String, String> mapDataProviderFieldsToFormValues(Map<String, String> confirmedMapping, Locale locale, DDMFormValues ddmFormValues) {
		LOG.debug("mapDataProviderFieldsToFormValues : confirmedMapping : " + confirmedMapping);
		Map<String, String> mappedValues = new HashMap<>();

		List<DDMFormFieldValue> flattenedDDMFormValues = getFlattenedDDMFormValues(ddmFormValues.getDDMFormFieldValues());

		confirmedMapping.entrySet().forEach(f -> {

			for (DDMFormFieldValue formFieldValue : flattenedDDMFormValues) {

				if (formFieldValue.getName().equals(f.getValue())) {
					DDMFormFieldValueRenderer ddmFormFieldValueRenderer = ddmFormFieldTypeServicesTracker.getDDMFormFieldValueRenderer(formFieldValue.getType());
					String value = ddmFormFieldValueRenderer.render(formFieldValue, locale);

					if (Validator.isNotNull(value) && isOptionsField(formFieldValue.getType())) {
						value = selectOptionValueToDelimitedString(value);
					}

					mappedValues.put(f.getKey(), value);
				}
			}
		});

		LOG.debug("mapDataProviderFieldsToFormValues : mappedValues : " + mappedValues);
		return mappedValues;
	}

	public Map<String, String> mapDataProviderFieldsToResponseValues(Map<String, String> confirmedMapping, String jsonResponse, ActionRequest actionRequest) throws JSONException {
		Map<String, String> mappedValues = new HashMap<>();

		Map<String, String> responseMap = dataTransformationService.deserializeMapping(jsonResponse);

		confirmedMapping.entrySet().forEach(f -> {
			if (responseMap.get(f.getValue()) != null) {
				mappedValues.put(f.getKey(), responseMap.get(f.getValue()));
			}
		});

		Set<String> generatedFormFieldNames = actionRequest.getParameterMap().keySet().stream().filter(k -> k.startsWith(FORM_VALUE_PREFIX)).collect(Collectors.toSet());

		confirmedMapping.entrySet().forEach(f -> {

			for (String fieldName : generatedFormFieldNames) {
				if (isMappedFieldMatchedWithRequestField(f.getValue(), fieldName)) {
					String value = ParamUtil.getString(actionRequest, fieldName);
					if (value != null) {
						mappedValues.put(f.getKey(), value);
					}
				}
			}

		});

		return mappedValues;
	}

	public Map<String, String> mapDataProviderFieldsToResponseValues(Map<String, String> confirmedMapping, String jsonResponse, Locale locale, DDMFormValues ddmFormValues) throws JSONException {
		Map<String, String> mappedValues = new HashMap<>();

		Map<String, String> responseMap = dataTransformationService.deserializeMapping(jsonResponse);

		confirmedMapping.entrySet().forEach(f -> {
			if (responseMap.get(f.getValue()) != null) {
				mappedValues.put(f.getKey(), responseMap.get(f.getValue()));
			}
		});

		List<DDMFormFieldValue> flattenedDDMFormValues = getFlattenedDDMFormValues(ddmFormValues.getDDMFormFieldValues());
		confirmedMapping.entrySet().forEach(f -> {
			flattenedDDMFormValues.stream().filter(formFieldValue -> formFieldValue.getName().equals(f.getValue())).forEach(formFieldValue -> {
				String value = formFieldValue.getValue().getString(locale);
				if (Validator.isNotNull(value)) {
					if (DDMFormFieldTypeConstants.SELECT.equalsIgnoreCase(formFieldValue.getType())) {
						value = selectValueToDelimitedString(value);
					}
					mappedValues.put(f.getKey(), value);
				}
			});
		});

		return mappedValues;
	}

	public String replaceBodyPlaceholdersWithDataProviderFieldValues(Map<String, String> dataProviderFieldValues, String body) {

		if (Validator.isNotNull(body)) {
			for (Entry<String, String> entry : dataProviderFieldValues.entrySet()) {
				String valueToReplace = entry.getValue();
				body = body.replaceAll("\\$" + entry.getKey() + "\\$", Validator.isNotNull(valueToReplace) ? valueToReplace : StringPool.BLANK);
			}
		}

		return body;
	}

	private void addAuthHeaderIfNecessary(Map<String, String> headers, DDMFormValues dataProviderFields, Locale locale, long companyId) throws PortalException {
		Map<String, List<DDMFormFieldValue>> dataProviderFieldValuesMap = dataProviderFields.getDDMFormFieldValuesMap(false);

		if (!headers.containsKey("Authorization")) {
			String username = getFormFieldValue(dataProviderFieldValuesMap, "username", locale);
			String oauthApplicationExternalReferenceCode = getFormFieldValue(dataProviderFieldValuesMap, "oauthApplicationExternalReferenceCode", locale);

			if (Validator.isNotNull(username)) {
				addBasicAuthorizationHeaders(headers, dataProviderFieldValuesMap, username, locale);
			} else if (Validator.isNotNull(oauthApplicationExternalReferenceCode)) {
				addOAuthApplicationAuthorizationHeaders(headers, oauthApplicationExternalReferenceCode, companyId);
			}
		}
	}

	private void addOAuthApplicationAuthorizationHeaders(Map<String, String> headers, String oauthApplicationExternalReferenceCode, long companyId) throws PortalException {
		OAuth2Application oAuth2Application = oAuth2ApplicationLocalService.getOAuth2ApplicationByExternalReferenceCode(oauthApplicationExternalReferenceCode, companyId);
		String token = digitalPlaceOAuth2Service.getOAuth2Token(companyId, oAuth2Application.getClientId(), oAuth2Application.getClientSecret());

		headers.put(HttpHeaders.AUTHORIZATION, "Bearer " + token);
	}

	private void addBasicAuthorizationHeaders(Map<String, String> headers, Map<String, List<DDMFormFieldValue>> dataProviderFieldValuesMap, String username, Locale locale) {
		String password = getFormFieldValue(dataProviderFieldValuesMap, "password", locale);
		String basicAuth = Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));
		headers.put("Authorization", "Basic " + basicAuth);
	}

	private String getFormFieldValue(Map<String, List<DDMFormFieldValue>> dataProviderFieldValuesMap, String fieldName, Locale locale) {
		List<DDMFormFieldValue> fieldFormFieldValues = dataProviderFieldValuesMap.get(fieldName);
		return fieldFormFieldValues != null && !fieldFormFieldValues.isEmpty() ? fieldFormFieldValues.get(0).getValue().getString(locale) : StringPool.BLANK;
	}

	private void addDDMFormValueToList(List<DDMFormFieldValue> allDDMFormFieldValues, DDMFormFieldValue ddmFormFieldValue) {
		allDDMFormFieldValues.add(ddmFormFieldValue);
		if (DDMFormFieldTypeConstants.FIELDSET.equals(ddmFormFieldValue.getType())) {
			List<DDMFormFieldValue> nestedDDMFormFields = ddmFormFieldValue.getNestedDDMFormFieldValues();
			if (Validator.isNotNull(nestedDDMFormFields) || !nestedDDMFormFields.isEmpty()) {
				for (DDMFormFieldValue nestedDDMFormFieldValue : nestedDDMFormFields) {
					addDDMFormValueToList(allDDMFormFieldValues, nestedDDMFormFieldValue);
				}
			}
		}
	}

	private List<DDMFormFieldValue> getFlattenedDDMFormValues(List<DDMFormFieldValue> ddmFormFieldsValues) {
		List<DDMFormFieldValue> allDDMFormFieldValues = new ArrayList<>();

		for (DDMFormFieldValue ddmFormField : ddmFormFieldsValues) {
			addDDMFormValueToList(allDDMFormFieldValues, ddmFormField);
		}

		return allDDMFormFieldValues;
	}

	private String getValueOrOptionReference(DDMStructure ddmStructure, String fieldName, String value) {
		try {
			DDMFormField ddmFormField = ddmStructure.getDDMFormField(fieldName);
			if (isOptionsField(ddmFormField.getType())) {
				DDMFormFieldOptions ddmFormFieldOptions = ddmFormField.getDDMFormFieldOptions();
				String optionValue = ddmFormFieldOptions.getOptionReference(value);

				if (optionValue != null) {
					return optionValue;
				}
			}
		} catch (PortalException e) {
			LOG.error(e);
		}
		return value;
	}

	private boolean isMappedFieldMatchedWithRequestField(String confirmedMappingFieldName, String requestFieldName) {
		return requestFieldName.startsWith(FORM_VALUE_PREFIX + confirmedMappingFieldName + FORM_VALUE_SUFFIX) && !requestFieldName.endsWith(EDITED_SUFFED)
				|| requestFieldName.startsWith(FORM_FIELD_GROUP_PREFIX) && requestFieldName.contains(FORM_FIELD_GROUP_VALUE_PREFIX + confirmedMappingFieldName + FORM_VALUE_SUFFIX)
						&& !requestFieldName.endsWith(EDITED_SUFFED);
	}

	private boolean isOptionsField(String fieldType) {
		return DDMFormFieldTypeConstants.SELECT.equalsIgnoreCase(fieldType) || DDMFormFieldTypeConstants.RADIO.equalsIgnoreCase(fieldType)
				|| DDMFormFieldTypeConstants.CHECKBOX.equalsIgnoreCase(fieldType) || DDMFormFieldTypeConstants.CHECKBOX_MULTIPLE.equalsIgnoreCase(fieldType);
	}

	private String selectOptionValueToDelimitedString(String value) {
		return value.replace(StringPool.COMMA, StringPool.SEMICOLON);
	}

	private String selectValueToDelimitedString(String fieldValue) {
		try {
			JSONArray values = jsonFactory.createJSONArray(fieldValue);
			return values.join(StringPool.SEMICOLON).replace(StringPool.QUOTE, StringPool.BLANK);
		} catch (JSONException e) {
			LOG.debug(String.format("Unable to parse json for DDMFormFieldType:%s Value:%s", DDMFormFieldTypeConstants.SELECT, fieldValue), e);
		}
		return fieldValue;
	}
}
