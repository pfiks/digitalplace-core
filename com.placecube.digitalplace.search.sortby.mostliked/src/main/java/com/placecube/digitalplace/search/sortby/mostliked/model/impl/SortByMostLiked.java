package com.placecube.digitalplace.search.sortby.mostliked.model.impl;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.model.SortByOption;
import com.placecube.digitalplace.search.sortby.mostliked.constants.MostLikedConstants;

@Component(immediate = true, service = { SortByMostLiked.class, SortByOption.class })
public class SortByMostLiked implements SortByOption {

	private static boolean DEFAULT_SORT_DIRECTION_REVERSE = true;

	@Override
	public String getId() {
		return "sortByMostLiked";
	}

	@Override
	public String getLabel(Locale locale) {
		return AggregatedResourceBundleUtil.get("most-liked", locale, "com.placecube.digitalplace.search.sortby.mostliked");
	}

	@Override
	public Sort[] getSorts(SharedSearchContributorSettings sharedSearchContributorSettings, boolean useOppositeSortDirection) {
		boolean sortDirection = useOppositeSortDirection ? !DEFAULT_SORT_DIRECTION_REVERSE : DEFAULT_SORT_DIRECTION_REVERSE;
		Sort sortByScore = SortFactoryUtil.create(MostLikedConstants.COUNTER_FIELD_NAME, sortDirection);
		Sort sortByDate = SortFactoryUtil.create("createDate_sortable", sortDirection);
		return new Sort[] { sortByScore, sortByDate };
	}

}
