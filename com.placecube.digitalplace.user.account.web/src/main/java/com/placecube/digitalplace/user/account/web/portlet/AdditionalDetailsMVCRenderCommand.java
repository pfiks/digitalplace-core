package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.ADDITIONAL_DETAILS }, service = MVCRenderCommand.class)
public class AdditionalDetailsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private UserAccountFieldRegistry userAccountFieldRegistry;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(renderRequest);
			UserProfileFieldsConfiguration userFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());
			CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, renderRequest);

			renderRequest.setAttribute("additionalDetailsPreviousStep", accountConfigurationService.getAdditionalDetailsPreviousStep(userFieldsConfiguration));

			boolean showEmailAddress = AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS.equals(accountConfiguration.emailAddressPageLocation());
			renderRequest.setAttribute(RequestKeys.SHOW_EMAIL_ADDRESS_IN_ADDITIONAL_DETAILS, showEmailAddress);

			if (showEmailAddress) {
				renderRequest.setAttribute(RequestKeys.CAPTCHA_ENABLED, accountContextService.isShowCaptchaOnEmailStep(accountContext, renderRequest));
			}

			renderRequest.setAttribute(RequestKeys.ADDITIONAL_DETAILS_EXPANDO_FIELDS, accountConfigurationService.getConfiguredExpandoFields(accountContext, userFieldsConfiguration));
			renderRequest.setAttribute(RequestKeys.ACCOUNT_CONTEXT, accountContext);
			renderRequest.setAttribute(RequestKeys.ACCOUNT_FIELDS, userAccountFieldRegistry.getUserAccountFieldsByStep(AccountCreationStep.ADDITIONAL_DETAILS, accountContext.getCompanyId()));
			renderRequest.setAttribute(RequestKeys.USER_PROFILE_CONFIG, userFieldsConfiguration);
			renderRequest.setAttribute(RequestKeys.ACCOUNT_CONFIG, accountConfiguration);
			accountContextService.updateContextInSession(accountContext, renderRequest);
		} catch (UserAccountException e) {
			throw new PortletException(e);
		}

		return "/additional-details.jsp";
	}

}
