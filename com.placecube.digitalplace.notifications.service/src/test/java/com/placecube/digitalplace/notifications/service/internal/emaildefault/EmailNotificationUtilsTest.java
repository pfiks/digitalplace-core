package com.placecube.digitalplace.notifications.service.internal.emaildefault;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.notifications.exception.NotificationsException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.InternetAddress;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class EmailNotificationUtilsTest extends PowerMockito {

	@InjectMocks
	private EmailNotificationUtils emailNotificationUtils;

	@Mock
	private JSONArray mockJsonArrayDate;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObjectAddress;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void createMailMessage_WhenNoError_ThenReturnsAnewMailMessage() {
		MailMessage result = emailNotificationUtils.createMailMessage();

		assertNotNull(result);
	}

	@Test
	public void getBodyTextField_WhenNoError_ThenReturnsTheBodyTextWithTheReplacedPlaceholdersWrappedInTheHtmlCode() throws JSONException {
		String givenDate = "Wed 21 Apr";
		String jsonStringArrayDate = "[\"" + givenDate + "\"]";
		String postCodeKey = "postcode";
		String postCodeValue = "M22 8LF";
		String fullAddressKey = "fullAddress";
		String fullAddressValue = "fullAddressValue";
		String jsonStringAddress = "{'" + postCodeKey + "':'M22 8LF','" + fullAddressKey + "':'" + fullAddressValue + "'}";
		Map<String, Object> variables = new HashMap<>();
		variables.put("ONE", "1a");
		variables.put("two", "2a");
		variables.put("Address", jsonStringAddress);
		variables.put("Given date", jsonStringArrayDate);
		String text = "This is my $ONE$ text ONE with some $two$ placeholder two values and address like $Address_postcode$ $Address_fullAddress$ with given date $Given date$";
		String expected = "This is my 1a text ONE with some 2a placeholder two values and address like " + postCodeValue + " " + fullAddressValue + " with given date " + givenDate;

		when(mockJsonFactory.createJSONArray(jsonStringArrayDate)).thenReturn(mockJsonArrayDate);
		when(mockJsonFactory.createJSONObject(jsonStringArrayDate)).thenThrow(new JSONException());
		Iterator<Object> mockIteratorDateArray = mock(Iterator.class);
		when(mockJsonArrayDate.iterator()).thenReturn(mockIteratorDateArray);
		when(mockIteratorDateArray.hasNext()).thenReturn(true, false);
		when(mockIteratorDateArray.next()).thenReturn(givenDate);

		when(mockJsonFactory.createJSONObject(jsonStringAddress)).thenReturn(mockJsonObjectAddress);
		Set<String> addressKeySet = new HashSet<>();
		addressKeySet.add(postCodeKey);
		addressKeySet.add(fullAddressKey);
		when(mockJsonObjectAddress.keySet()).thenReturn(addressKeySet);
		when(mockJsonObjectAddress.getString(postCodeKey)).thenReturn(postCodeValue);
		when(mockJsonObjectAddress.getString(fullAddressKey)).thenReturn(fullAddressValue);

		String result = emailNotificationUtils.getBodyTextField(text, variables);

		assertThat(result, equalTo(EmailNotificationUtils.BODY_TEXT_PREFIX + expected + EmailNotificationUtils.BODY_TEXT_SUFFIX));
	}

	@Test
	public void getBodyTextField_WhenNoError_ThenReturnsTheTextWithTheReplacedPlaceholders() throws JSONException {
		String givenDate = "Wed 21 Apr";
		String jsonStringArrayDate = "[\"" + givenDate + "\"]";
		String postCodeKey = "postcode";
		String postCodeValue = "M22 8LF";
		String fullAddressKey = "fullAddress";
		String fullAddressValue = "fullAddressValue";
		String jsonStringAddress = "{'" + postCodeKey + "':'M22 8LF','" + fullAddressKey + "':'" + fullAddressValue + "'}";
		Map<String, Object> variables = new HashMap<>();
		variables.put("ONE", "1a");
		variables.put("two", "2a");
		variables.put("Address", jsonStringAddress);
		variables.put("Given date", jsonStringArrayDate);
		variables.put("emptyField", "");
		variables.put("emptyArray", "[]");

		String text = "This is my $ONE$ text ONE with some $two$ placeholder two values and address like $Address_postcode$ $Address_fullAddress$ with given date $Given date$ $emptyField$ $emptyArray$";
		String expected = "This is my 1a text ONE with some 2a placeholder two values and address like " + postCodeValue + " " + fullAddressValue + " with given date " + givenDate + "  ";

		when(mockJsonFactory.createJSONArray(jsonStringArrayDate)).thenReturn(mockJsonArrayDate);
		when(mockJsonFactory.createJSONObject(jsonStringArrayDate)).thenThrow(new JSONException());
		Iterator<Object> mockIteratorDateArray = mock(Iterator.class);
		when(mockJsonArrayDate.iterator()).thenReturn(mockIteratorDateArray);
		when(mockIteratorDateArray.hasNext()).thenReturn(true, false);
		when(mockIteratorDateArray.next()).thenReturn(givenDate);

		when(mockJsonFactory.createJSONObject(jsonStringAddress)).thenReturn(mockJsonObjectAddress);
		Set<String> addressKeySet = new HashSet<>();
		addressKeySet.add(postCodeKey);
		addressKeySet.add(fullAddressKey);
		when(mockJsonObjectAddress.keySet()).thenReturn(addressKeySet);
		when(mockJsonObjectAddress.getString(postCodeKey)).thenReturn(postCodeValue);
		when(mockJsonObjectAddress.getString(fullAddressKey)).thenReturn(fullAddressValue);

		String result = emailNotificationUtils.getBodyTextField(text, variables);

		assertThat(result, equalTo(EmailNotificationUtils.BODY_TEXT_PREFIX + expected + EmailNotificationUtils.BODY_TEXT_SUFFIX));
	}

	@Test(expected = NotificationsException.class)
	public void getInternetAddress_WhenException_ThenThrowsNotificationsException() throws NotificationsException {
		emailNotificationUtils.getInternetAddress(StringPool.BLANK, StringPool.BLANK);
	}

	@Test
	public void getInternetAddress_WhenNameIsNotEmpty_ThenReturnsTheInternetAddressWithName() throws NotificationsException {
		String address = "test@test.com";
		String name = "Test";

		InternetAddress result = emailNotificationUtils.getInternetAddress(address, name);

		assertThat(result.getAddress(), equalTo(address));
		assertThat(result.getPersonal(), equalTo(name));
	}

	@Test
	public void getInternetAddress_WhenNameIsEmpty_ThenReturnsTheInternetAddressWithNullName() throws NotificationsException {
		String address = "test@test.com";

		InternetAddress result = emailNotificationUtils.getInternetAddress(address, null);

		assertThat(result.getAddress(), equalTo(address));
		assertThat(result.getPersonal(), nullValue());
	}

	@Test
	public void getInternetAddresses_WhenAddressAreNull_ThenReturnsEmptyInternetAddresses() throws NotificationsException {

		String[] addresses = null;

		InternetAddress[] result = emailNotificationUtils.getInternetAddress(addresses);

		assertTrue(result.length == 0);

	}

	@Test
	public void getInternetAddresses_WhenNoError_ThenReturnsTheValidEmailsInternetAddresses() throws NotificationsException {
		String validEmailAddressOne = "test1@test.com";
		String validEmailAddressTwo = "test2@test.com";
		String notValidEmailAddress = "test";
		String notValidEmailAddressOne = null;
		String notValidEmailAddressTwo = StringPool.BLANK;
		String[] addresses = { validEmailAddressOne, validEmailAddressTwo, notValidEmailAddress, notValidEmailAddressOne, notValidEmailAddressTwo };

		InternetAddress[] result = emailNotificationUtils.getInternetAddress(addresses);

		assertTrue(result.length == 2);
		assertThat(result[0].getAddress(), equalTo(validEmailAddressOne));
		assertThat(result[1].getAddress(), equalTo(validEmailAddressTwo));
	}

	@Test
	public void getSubjectTextField_WhenNoError_ThenReturnsTheTextWithTheReplacedPlaceholders() throws JSONException {
		String givenDate = "Wed 21 Apr";
		String jsonStringArrayDate = "[\"" + givenDate + "\"]";
		String postCodeKey = "postcode";
		String postCodeValue = "M22 8LF";
		String fullAddressKey = "fullAddress";
		String fullAddressValue = "fullAddressValue";
		String jsonStringAddress = "{'" + postCodeKey + "':'M22 8LF','" + fullAddressKey + "':'" + fullAddressValue + "'}";
		Map<String, Object> variables = new HashMap<>();
		variables.put("ONE", "1a");
		variables.put("two", "2a");
		variables.put("Address", jsonStringAddress);
		variables.put("Given date", jsonStringArrayDate);
		String text = "This is my $ONE$ text ONE with some $two$ placeholder two values and address like $Address_postcode$ $Address_fullAddress$ with given date $Given date$";
		String expected = "This is my 1a text ONE with some 2a placeholder two values and address like " + postCodeValue + " " + fullAddressValue + " with given date " + givenDate;

		when(mockJsonFactory.createJSONArray(jsonStringArrayDate)).thenReturn(mockJsonArrayDate);
		when(mockJsonFactory.createJSONObject(jsonStringArrayDate)).thenThrow(new JSONException());
		Iterator<Object> mockIteratorDateArray = mock(Iterator.class);
		when(mockJsonArrayDate.iterator()).thenReturn(mockIteratorDateArray);
		when(mockIteratorDateArray.hasNext()).thenReturn(true, false);
		when(mockIteratorDateArray.next()).thenReturn(givenDate);

		when(mockJsonFactory.createJSONObject(jsonStringAddress)).thenReturn(mockJsonObjectAddress);
		Set<String> addressKeySet = new HashSet<>();
		addressKeySet.add(postCodeKey);
		addressKeySet.add(fullAddressKey);
		when(mockJsonObjectAddress.keySet()).thenReturn(addressKeySet);
		when(mockJsonObjectAddress.getString(postCodeKey)).thenReturn(postCodeValue);
		when(mockJsonObjectAddress.getString(fullAddressKey)).thenReturn(fullAddressValue);

		String result = emailNotificationUtils.getSubjectTextField(text, variables);

		assertThat(result, equalTo(expected));
	}
}
