package com.placecube.digitalplace.layout.util;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactory;
import com.liferay.portal.kernel.service.LayoutService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LayoutRetrievalUtilTest extends PowerMockito {

	@InjectMocks
	private LayoutRetrievalUtil layoutRetrievalUtil;

	@Mock
	private Layout mockLayoutOne;

	@Mock
	private LayoutService mockLayoutService;

	@Mock
	private Layout mockLayoutThree;

	@Mock
	private Layout mockLayoutTwo;

	@Mock
	private LayoutWithPortletsRetrievalHelperUtil mockLayoutWithPortletsRetrievalHelperUtil;

	@Mock
	private Layout mockLayoutWithPortletId;

	@Mock
	private List<Portlet> mockPortletList1;

	@Mock
	private List<Portlet> mockPortletList2;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletPreferences mockPortletPreferences2;

	@Mock
	private PortletPreferencesFactory mockPortletPreferencesFactory;

	@Mock
	private Layout mockUnmatchingLayout;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLayoutsWithPortletId_WhenLayoutExistWithThePortletId_ThenReturnsListWithTheLayoutFound(boolean privateLayout) {
		long groupId = 123;
		String portletId = "portletIdToMatchValue";
		List<Layout> groupLayouts = new ArrayList<>();
		when(mockLayoutService.getLayouts(groupId, privateLayout)).thenReturn(groupLayouts);
		groupLayouts.add(mockLayoutWithPortletId);
		groupLayouts.add(mockUnmatchingLayout);

		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutWithPortletId)).thenReturn(mockPortletList1);
		when(mockLayoutWithPortletsRetrievalHelperUtil.hasPortletId(mockPortletList1, portletId)).thenReturn(true);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockUnmatchingLayout)).thenReturn(mockPortletList2);
		when(mockLayoutWithPortletsRetrievalHelperUtil.hasPortletId(mockPortletList2, portletId)).thenReturn(false);

		List<Layout> result = layoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, privateLayout);

		assertThat(result, containsInAnyOrder(mockLayoutWithPortletId));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLayoutsWithPortletId_WhenLayoutWithThePortletIdIsNotFound_ThenReturnsEmptyList(boolean privateLayout) {
		long groupId = 123;
		String portletId = "portletIdToMatchValue";
		List<Layout> groupLayouts = new ArrayList<>();
		when(mockLayoutService.getLayouts(groupId, privateLayout)).thenReturn(groupLayouts);
		groupLayouts.add(mockUnmatchingLayout);

		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockUnmatchingLayout)).thenReturn(mockPortletList1);
		when(mockLayoutWithPortletsRetrievalHelperUtil.hasPortletId(mockPortletList1, portletId)).thenReturn(false);

		List<Layout> result = layoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, privateLayout);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getFirstNonHiddenLayout_WhenNoLayoutsAreFound_ThenReturnsEmptyOptional() {
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		when(mockLayoutOne.isHidden()).thenReturn(true);
		when(mockLayoutTwo.isHidden()).thenReturn(true);

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayout(layouts);

		assertFalse(result.isPresent());
	}

	@Test
	public void getFirstNonHiddenLayout_WhenNoErrors_ThenReturnsFirstLayout() {
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		when(mockLayoutOne.isHidden()).thenReturn(false);
		when(mockLayoutTwo.isHidden()).thenReturn(false);

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayout(layouts);

		assertTrue(result.isPresent());
		assertThat(result.get(), sameInstance(mockLayoutOne));
	}

	@Test
	public void getFirstNonHiddenLayout_WhenSomeLayoutsAreHidden_ThenReturnsFirstNonHiddenLayout() {
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		when(mockLayoutOne.isHidden()).thenReturn(true);
		when(mockLayoutTwo.isHidden()).thenReturn(false);

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayout(layouts);

		assertTrue(result.isPresent());
		assertThat(result.get(), sameInstance(mockLayoutTwo));
	}

	@Test
	public void getFirstNonHiddenLayoutWithCorrectPrefs_WhenNoLayoutsAreFound_ThenReturnsEmptyOptional() {
		long groupId = 123;
		boolean privateLayout = true;
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		String portletId = "id";
		Map<String, String> prefs = Collections.singletonMap("key", "value");
		when(mockLayoutOne.isHidden()).thenReturn(true);
		when(mockLayoutTwo.isHidden()).thenReturn(true);
		when(mockLayoutService.getLayouts(groupId, privateLayout)).thenReturn(layouts);

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, privateLayout, prefs, portletId);

		assertFalse(result.isPresent());
	}

	@Test
	public void getFirstNonHiddenLayoutWithCorrectPrefs_WhenNoErrors_ThenReturnsFirstLayout() throws PortalException {
		long groupId = 123;
		boolean privateLayout = true;
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		String portletId = "id";
		List<String> portletIdsOne = Arrays.asList("id_2", "guacamole_3");
		List<String> portletIdsTwo = Arrays.asList("guacamole_2", "guacamole_3");
		Map<String, String> prefs = Collections.singletonMap("key", "value");
		when(mockLayoutService.getLayouts(groupId, privateLayout)).thenReturn(layouts);
		when(mockLayoutOne.isHidden()).thenReturn(false);
		when(mockLayoutTwo.isHidden()).thenReturn(false);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne)).thenReturn(mockPortletList1);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutTwo)).thenReturn(mockPortletList2);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList1, portletId)).thenReturn(portletIdsOne);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList2, portletId)).thenReturn(portletIdsTwo);
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutOne, "id_2")).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue("key", StringPool.BLANK)).thenReturn("value");

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, privateLayout, prefs, portletId);

		assertTrue(result.isPresent());
		assertThat(result.get(), sameInstance(mockLayoutOne));
	}

	@Test
	public void getFirstNonHiddenLayoutWithCorrectPrefs_WhenSomeLayoutsAreHiddenOrHaveTheWrongPrefs_ThenReturnsFirstValidLayout() throws PortalException {
		long groupId = 123;
		boolean privateLayout = true;
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo, mockLayoutThree);
		String portletId = "id";
		Map<String, String> prefs = Collections.singletonMap("key", "value");
		List<String> portletIdsTwo = Collections.singletonList("id_3");
		List<String> portletIdsThree = Collections.singletonList("id_2");
		when(mockLayoutService.getLayouts(groupId, privateLayout)).thenReturn(layouts);
		when(mockLayoutOne.isHidden()).thenReturn(true);
		when(mockLayoutTwo.isHidden()).thenReturn(false);
		when(mockLayoutThree.isHidden()).thenReturn(false);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutTwo)).thenReturn(mockPortletList1);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutThree)).thenReturn(mockPortletList2);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList1, portletId)).thenReturn(portletIdsTwo);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList2, portletId)).thenReturn(portletIdsThree);

		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutTwo, "id_3")).thenReturn(mockPortletPreferences2);
		when(mockPortletPreferences2.getValue("key", StringPool.BLANK)).thenReturn("bad-value");
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutThree, "id_2")).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue("key", StringPool.BLANK)).thenReturn("value");

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, privateLayout, prefs, portletId);

		assertTrue(result.isPresent());
		assertThat(result.get(), sameInstance(mockLayoutThree));
	}

	@Test
	public void getFirstNonHiddenLayoutWithCorrectPrefs_WhenThereIsMoreThanOnePref_ThenReturnsTheValidLayout() throws PortalException {
		long groupId = 123;
		boolean privateLayout = true;
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		String portletId = "id";
		Map<String, String> prefs = Stream.of(new String[][] { { "key", "value" }, { "key2", "value2" }, }).collect(Collectors.toMap(data -> data[0], data -> data[1]));
		when(mockLayoutOne.isHidden()).thenReturn(false);
		when(mockLayoutTwo.isHidden()).thenReturn(false);
		when(mockLayoutService.getLayouts(groupId, privateLayout)).thenReturn(layouts);
		List<String> portletIdsTwo = Collections.singletonList("id_3");
		List<String> portletIdsOne = Collections.singletonList("id_2");

		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne)).thenReturn(mockPortletList1);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutTwo)).thenReturn(mockPortletList2);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList1, portletId)).thenReturn(portletIdsOne);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList2, portletId)).thenReturn(portletIdsTwo);
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutOne, "id_2")).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue("key", StringPool.BLANK)).thenReturn("value");
		when(mockPortletPreferences.getValue("key2", StringPool.BLANK)).thenReturn("bad-value");
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutTwo, "id_3")).thenReturn(mockPortletPreferences2);
		when(mockPortletPreferences2.getValue("key", StringPool.BLANK)).thenReturn("value");
		when(mockPortletPreferences2.getValue("key2", StringPool.BLANK)).thenReturn("value2");

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, privateLayout, prefs, portletId);

		assertTrue(result.isPresent());
		assertThat(result.get(), sameInstance(mockLayoutTwo));
	}

	@Test
	public void getFirstNonHiddenLayoutWithCorrectPrefs_WhenThereIsPortalExceptionGettingOneOfThePreferences_ThenFindsTheNextTheValidLayout() throws PortalException {
		long groupId = 123;
		boolean privateLayout = true;
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		String portletId = "id";
		Map<String, String> prefs = Collections.singletonMap("key", "value");
		when(mockLayoutOne.isHidden()).thenReturn(false);
		when(mockLayoutTwo.isHidden()).thenReturn(false);
		List<String> portletIdsTwo = Collections.singletonList("id_3");
		List<String> portletIdsOne = Collections.singletonList("id_2");
		when(mockLayoutService.getLayouts(groupId, privateLayout)).thenReturn(layouts);

		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne)).thenReturn(mockPortletList1);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutTwo)).thenReturn(mockPortletList2);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList1, portletId)).thenReturn(portletIdsOne);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList2, portletId)).thenReturn(portletIdsTwo);
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutOne, "id_2")).thenThrow(PortalException.class);
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutTwo, "id_3")).thenReturn(mockPortletPreferences2);
		when(mockPortletPreferences2.getValue("key", StringPool.BLANK)).thenReturn("value");

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, privateLayout, prefs, portletId);

		assertTrue(result.isPresent());
		assertThat(result.get(), sameInstance(mockLayoutTwo));
	}

	@Test
	public void getFirstNonHiddenLayoutWithCorrectPrefs_WhenThereIsPortalExceptionGettingOneOfThePreferencesFromTheListOfPortletIds_ThenFindsTheNextTheValidPreferences() throws PortalException {
		long groupId = 123;
		boolean privateLayout = true;
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		String portletId = "id";
		Map<String, String> prefs = Collections.singletonMap("key", "value");
		when(mockLayoutOne.isHidden()).thenReturn(false);
		when(mockLayoutTwo.isHidden()).thenReturn(false);
		List<String> portletIdsTwo = Arrays.asList("guacamole", "id_3");
		List<String> portletIdsOne = Collections.singletonList("id_2");
		when(mockLayoutService.getLayouts(groupId, privateLayout)).thenReturn(layouts);

		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne)).thenReturn(mockPortletList1);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutTwo)).thenReturn(mockPortletList2);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList1, portletId)).thenReturn(portletIdsOne);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList2, portletId)).thenReturn(portletIdsTwo);
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutOne, "id_2")).thenThrow(PortalException.class);
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutTwo, "guacamole")).thenThrow(PortalException.class);
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutTwo, "id_3")).thenReturn(mockPortletPreferences2);
		when(mockPortletPreferences2.getValue("key", StringPool.BLANK)).thenReturn("value");

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, privateLayout, prefs, portletId);

		assertTrue(result.isPresent());
		assertThat(result.get(), sameInstance(mockLayoutTwo));
	}

	@Test
	public void getFirstNonHiddenLayoutWithCorrectPrefs_WhenOneOfThePreferencesAreNull_ThenFindsTheNextTheValidLayout() throws PortalException {
		long groupId = 123;
		boolean privateLayout = true;
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		String portletId = "id";
		Map<String, String> prefs = Collections.singletonMap("key", "value");
		when(mockLayoutOne.isHidden()).thenReturn(false);
		when(mockLayoutTwo.isHidden()).thenReturn(false);
		List<String> portletIdsTwo = Collections.singletonList("id_3");
		List<String> portletIdsOne = Collections.singletonList("id_2");
		when(mockLayoutService.getLayouts(groupId, privateLayout)).thenReturn(layouts);

		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne)).thenReturn(mockPortletList1);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutTwo)).thenReturn(mockPortletList2);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList1, portletId)).thenReturn(portletIdsOne);
		when(mockLayoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(mockPortletList2, portletId)).thenReturn(portletIdsTwo);
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutOne, "id_2")).thenReturn(null);
		when(mockPortletPreferencesFactory.getExistingPortletSetup(mockLayoutTwo, "id_3")).thenReturn(mockPortletPreferences2);
		when(mockPortletPreferences2.getValue("key", StringPool.BLANK)).thenReturn("value");

		Optional<Layout> result = layoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, privateLayout, prefs, portletId);

		assertTrue(result.isPresent());
		assertThat(result.get(), sameInstance(mockLayoutTwo));
	}

}
