package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle;

import java.util.Map;

import javax.portlet.PortletURL;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.editor.alloyeditor.web.internal.constants.AlloyEditorConstants;
import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorCriterion;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.file.criterion.FileItemSelectorCriterion;
import com.liferay.layout.item.selector.criterion.LayoutItemSelectorCriterion;
import com.liferay.portal.kernel.editor.configuration.BaseEditorConfigContributor;
import com.liferay.portal.kernel.editor.configuration.EditorConfigContributor;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.JournalArticleItemSelectorCriterion;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.JournalArticleItemSelectorReturnType;

@Component(property = { "editor.name=alloyeditor", "service.ranking:Integer=100" }, service = EditorConfigContributor.class)
public class JournalArticleItemSelectorConfigContributor extends BaseEditorConfigContributor {

	private ItemSelector itemSelector;

	@Override
	public void populateConfigJSONObject(JSONObject jsonObject, Map<String, Object> inputEditorTaglibAttributes, ThemeDisplay themeDisplay,
			RequestBackedPortletURLFactory requestBackedPortletURLFactory) {

		String namespace = GetterUtil.getString(inputEditorTaglibAttributes.get(AlloyEditorConstants.ATTRIBUTE_NAMESPACE + ":namespace"));

		String name = GetterUtil.getString(inputEditorTaglibAttributes.get(AlloyEditorConstants.ATTRIBUTE_NAMESPACE + ":name"));

		name = namespace + name;

		populateFileBrowserURL(jsonObject, requestBackedPortletURLFactory, name + "selectDocument");
	}

	protected void populateFileBrowserURL(JSONObject jsonObject, RequestBackedPortletURLFactory requestBackedPortletURLFactory, String eventName) {

		ItemSelectorCriterion fileItemSelectorCriterion = new FileItemSelectorCriterion();

		fileItemSelectorCriterion.setDesiredItemSelectorReturnTypes(new URLItemSelectorReturnType());

		LayoutItemSelectorCriterion layoutItemSelectorCriterion = new LayoutItemSelectorCriterion();

		layoutItemSelectorCriterion.setDesiredItemSelectorReturnTypes(new URLItemSelectorReturnType());

		JournalArticleItemSelectorCriterion journalArticleInfoItemItemSelectorCriterion = new JournalArticleItemSelectorCriterion();
		journalArticleInfoItemItemSelectorCriterion.setDesiredItemSelectorReturnTypes(new JournalArticleItemSelectorReturnType());

		PortletURL itemSelectorURL = itemSelector.getItemSelectorURL(requestBackedPortletURLFactory, eventName, fileItemSelectorCriterion, layoutItemSelectorCriterion,
				journalArticleInfoItemItemSelectorCriterion);

		jsonObject.put("documentBrowseLinkUrl", itemSelectorURL.toString());
	}

	@Reference(unbind = "-")
	protected void setItemSelector(ItemSelector itemSelector1) {
		itemSelector = itemSelector1;
	}

}
