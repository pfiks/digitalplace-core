package com.placecube.digitalplace.addresslookup.ordnancesurvey.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.ordnancesurvey.constants.OrdnanceSurveyConstants;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ResponseParserUtilTest extends PowerMockito {

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject;

	@InjectMocks
	private ResponseParserUtil responseParserUtil;

	@Test
	@Parameters({ "subBuildingNameValue,buildingNameValue,organizationNameValue", ",buildingNameValue,organizationNameValue", "subBuildingNameValue,,organizationNameValue",
			"subBuildingNameValue,buildingNameValue,", ",,organizationNameValue", "subBuildingNameValue,,", ",buildingNameValue,", ",," })
	public void getAddressContextResultForDPA_WhenNoError_ThenReturnsAddressContextConfigured(String subBuildingName, String buildingName, String organisationName) {
		when(mockJSONObject.getString(OrdnanceSurveyConstants.SUB_BUILDING_NAME, StringPool.BLANK)).thenReturn(subBuildingName);
		when(mockJSONObject.getString(OrdnanceSurveyConstants.BUILDING_NAME, StringPool.BLANK)).thenReturn(buildingName);
		when(mockJSONObject.getString(OrdnanceSurveyConstants.BUILDING_NUMBER, StringPool.BLANK)).thenReturn("valueForBUILDING_NUMBER");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.ORGANISATION_NAME, StringPool.BLANK)).thenReturn(organisationName);
		when(mockJSONObject.getString(OrdnanceSurveyConstants.DEPENDENT_LOCALITY, StringPool.BLANK)).thenReturn("valueForDEPENDENT_LOCALITY");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.DOUBLE_DEPENDENT_LOCALITY, StringPool.BLANK)).thenReturn("valueForDOUBLE_DEPENDENT_LOCALITY");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.THOROUGHFARE_NAME, StringPool.BLANK)).thenReturn("valueForTHOROUGHFARE_NAME");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.DEPENDENT_THOROUGHFARE_NAME, StringPool.BLANK)).thenReturn("valueForDEPENDENT_THOROUGHFARE_NAME");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.POST_TOWN, StringPool.BLANK)).thenReturn("valueForPOST_TOWN");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.POSTCODE, StringPool.BLANK)).thenReturn("valueForPOSTCODE");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.UPRN, StringPool.BLANK)).thenReturn("valueForUPRN");

		String buildingDetails = String.join(StringPool.SPACE, subBuildingName, buildingName);
		String addressLine1 = (Validator.isNotNull(organisationName) ? organisationName + StringPool.SPACE : StringPool.BLANK)
				+ (Validator.isNotNull(buildingDetails) ? buildingDetails : "valueForBUILDING_NUMBER");
		String addressLine2 = Validator.isNotNull(buildingDetails) ? "valueForBUILDING_NUMBER" + StringPool.SPACE + "valueForDEPENDENT_THOROUGHFARE_NAME valueForTHOROUGHFARE_NAME"
				: "valueForDEPENDENT_THOROUGHFARE_NAME valueForTHOROUGHFARE_NAME";

		AddressContext result = responseParserUtil.getAddressContextResultForDPA(mockJSONObject);

		assertThat(result.getAddressLine1(), equalTo(addressLine1.replace("  ", " ").trim()));
		assertThat(result.getAddressLine2(), equalTo(addressLine2.replace("  ", " ").trim()));
		assertThat(result.getAddressLine3(), equalTo("valueForDEPENDENT_LOCALITY valueForDOUBLE_DEPENDENT_LOCALITY"));
		assertThat(result.getAddressLine4(), equalTo(""));
		assertThat(result.getCity(), equalTo("valueForPOST_TOWN"));
		assertThat(result.getPostcode(), equalTo("valueForPOSTCODE"));
		assertThat(result.getUPRN(), equalTo("valueForUPRN"));
		assertThat(result.getSource(), equalTo(OrdnanceSurveyConstants.DPA));
	}

	@Test
	public void getAddressContextResultForLPI_WhenNoError_ThenReturnsAddressContextConfigured() {

		when(mockJSONObject.getString(OrdnanceSurveyConstants.ORGANISATION, StringPool.BLANK)).thenReturn("valueForORGANISATION");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.SAO_TEXT, StringPool.BLANK)).thenReturn("valueForSAO_TEXT");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.SAO_START_NUMBER, StringPool.BLANK)).thenReturn("valueForSAO_START_NUMBER");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.SAO_START_SUFFIX, StringPool.BLANK)).thenReturn("valueForSAO_START_SUFFIX");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.SAO_END_NUMBER, StringPool.BLANK)).thenReturn("valueForSAO_END_NUMBER");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.SAO_END_SUFFIX, StringPool.BLANK)).thenReturn("valueForSAO_END_SUFFIX");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.PAO_TEXT, StringPool.BLANK)).thenReturn("valueForPAO_TEXT");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.PAO_START_NUMBER, StringPool.BLANK)).thenReturn("valueForPAO_START_NUMBER");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.PAO_START_SUFFIX, StringPool.BLANK)).thenReturn("valueForPAO_START_SUFFIX");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.PAO_END_NUMBER, StringPool.BLANK)).thenReturn("valueForPAO_END_NUMBER");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.PAO_END_SUFFIX, StringPool.BLANK)).thenReturn("valueForPAO_END_SUFFIX");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.STREET_DESCRIPTION, StringPool.BLANK)).thenReturn("valueForSTREET_DESCRIPTION");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.LOCALITY_NAME, StringPool.BLANK)).thenReturn("valueForLOCALITY_NAME");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.TOWN_NAME, StringPool.BLANK)).thenReturn("valueForTOWN_NAME");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.POSTCODE_LOCATOR, StringPool.BLANK)).thenReturn("valueForPOSTCODE_LOCATOR");
		when(mockJSONObject.getString(OrdnanceSurveyConstants.UPRN, StringPool.BLANK)).thenReturn("valueForUPRN");

		AddressContext result = responseParserUtil.getAddressContextResultForLPI(mockJSONObject);

		assertThat(result.getAddressLine1(), equalTo(
				"valueForORGANISATION valueForSAO_TEXT valueForSAO_START_NUMBERvalueForSAO_START_SUFFIX valueForSAO_END_NUMBERvalueForSAO_END_SUFFIX valueForPAO_TEXT valueForPAO_START_NUMBERvalueForPAO_START_SUFFIX valueForPAO_END_NUMBERvalueForPAO_END_SUFFIX"));
		assertThat(result.getAddressLine2(), equalTo("valueForSTREET_DESCRIPTION"));
		assertThat(result.getAddressLine3(), equalTo("valueForLOCALITY_NAME"));
		assertThat(result.getAddressLine4(), equalTo(""));
		assertThat(result.getCity(), equalTo("valueForTOWN_NAME"));
		assertThat(result.getPostcode(), equalTo("valueForPOSTCODE_LOCATOR"));
		assertThat(result.getUPRN(), equalTo("valueForUPRN"));
		assertThat(result.getSource(), equalTo(OrdnanceSurveyConstants.LPI));
	}

	@Test
	public void getResults_WhenNoError_ThenReturnsJsonObjectForResults() throws JSONException {
		when(mockJSONFactory.createJSONObject("myResponseValue")).thenReturn(mockJSONObject);
		when(mockJSONObject.getJSONArray("results")).thenReturn(mockJSONArray);

		JSONArray result = responseParserUtil.getResults("myResponseValue");

		assertThat(result, sameInstance(mockJSONArray));
	}
}
