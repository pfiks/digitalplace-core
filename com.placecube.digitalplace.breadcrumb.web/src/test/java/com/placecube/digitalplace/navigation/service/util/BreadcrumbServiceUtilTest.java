package com.placecube.digitalplace.navigation.service.util;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class })
public class BreadcrumbServiceUtilTest {

	private static final String SESSION_ID = "efdss32";
	private static final String FRIENDLY_URL = "/layout";
	private static final String FRIENDLY_URL_WITH_SESSION = "/layout?session=2323";
	private static final String GROUP_NAME = "group";
	private static final int PAGE_COUNT = 23;
	private static final Locale LOCALE = Locale.UK;

	@InjectMocks
	private BreadcrumbServiceUtil breadcrumbServiceUtil;

	@Mock
	private BreadcrumbEntryFactory mockBreadcrumbEntryFactory;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutSet mockLayoutSet;

	@Mock
	private Group mockGroup;

	@Mock
	private Group mockLiveGroup;

	@Mock
	private BreadcrumbEntry mockBreadcrumbEntry;

	@Before
	public void setUp() {
		mockStatic(PortalUtil.class);
	}

	@Test
	public void getLayoutSetGroupBreadcrumbEntry_WhenGroupLayoutSetIsPrivate_AndThereAreLayouts_ThenReturnsOptionalBreadcrumbEntry() throws Exception {
		mockGroupWithPrivateLayoutSet();

		Optional<BreadcrumbEntry> result = breadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockBreadcrumbEntry));
	}

	@Test
	public void getLayoutSetGroupBreadcrumbEntry_WhenGroupLayoutSetIsPublic_AndThereAreLayouts_ThenReturnsOptionalBreadcrumbEntry() throws Exception {
		mockGroupWithPrivateLayoutSet();

		when(mockLayoutSet.isPrivateLayout()).thenReturn(false);
		when(mockGroup.getPublicLayoutsPageCount()).thenReturn(PAGE_COUNT);

		Optional<BreadcrumbEntry> result = breadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockBreadcrumbEntry));
	}

	@Test
	public void getLayoutSetGroupBreadcrumbEntry_WhenThereAreNoLayouts_ThenReturnsEmptyOptional() throws Exception {
		mockGroupWithPrivateLayoutSet();
		when(mockGroup.getPrivateLayoutsPageCount()).thenReturn(0);

		Optional<BreadcrumbEntry> result = breadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getLayoutSetGroupBreadcrumbEntry_WhenSessionIdIsNotAddedToUrl_ThenCreatesBreadcrumbWithFriendlyUrl() throws Exception {
		mockGroupWithPrivateLayoutSet();
		when(mockThemeDisplay.isAddSessionIdToURL()).thenReturn(false);

		when(mockBreadcrumbEntryFactory.createBreadcrumbEntry(GROUP_NAME, FRIENDLY_URL)).thenReturn(mockBreadcrumbEntry);

		Optional<BreadcrumbEntry> result = breadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockBreadcrumbEntry));
	}

	@Test(expected = PortalException.class)
	public void getLayoutSetGroupBreadcrumbEntry_WhenLayoutSetGroupCannotBeRetrieved_ThenThrowsPortalException() throws Exception {
		mockGroupWithPrivateLayoutSet();

		when(mockLayoutSet.getGroup()).thenThrow(new PortalException());

		breadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay);
	}

	@Test(expected = PortalException.class)
	public void getLayoutSetGroupBreadcrumbEntry_WhenLayoutSetFriendlyUrlRetrievalFails_ThenThrowsPortalException() throws Exception {
		mockGroupWithPrivateLayoutSet();

		when(PortalUtil.getLayoutSetFriendlyURL(mockLayoutSet, mockThemeDisplay)).thenThrow(new PortalException());

		breadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay);
	}

	@Test(expected = PortalException.class)
	public void getLayoutSetGroupBreadcrumbEntry_WhenGroupDescriptiveNameRetrievalFails_ThenThrowsPortalException() throws Exception {
		mockGroupWithPrivateLayoutSet();

		when(mockGroup.getDescriptiveName(LOCALE)).thenThrow(new PortalException());

		breadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(mockThemeDisplay);
	}

	@Test
	public void isGuestStagingGroup_WhenGroupIsGuest_ThenReturnsFalse() {
		when(mockGroup.isGuest()).thenReturn(true);

		assertThat(breadcrumbServiceUtil.isGuestStagingGroup(mockGroup), equalTo(false));
	}

	@Test
	public void isGuestStagingGroup_WhenGroupIsNotStaged_ThenReturnsFalse() {
		when(mockGroup.isStaged()).thenReturn(false);

		assertThat(breadcrumbServiceUtil.isGuestStagingGroup(mockGroup), equalTo(false));
	}

	@Test
	public void isGuestStagingGroup_WhenLiveGroupIsNull_ThenReturnsFalse() {
		when(mockGroup.isStaged()).thenReturn(true);
		when(mockGroup.getLiveGroup()).thenReturn(null);

		assertThat(breadcrumbServiceUtil.isGuestStagingGroup(mockGroup), equalTo(false));
	}

	@Test
	public void isGuestStagingGroup_WhenLiveGroupKeyIsGuest_ThenReturnsTrue() {
		when(mockGroup.isStaged()).thenReturn(true);
		when(mockGroup.getLiveGroup()).thenReturn(mockLiveGroup);
		when(mockLiveGroup.getGroupKey()).thenReturn(GroupConstants.GUEST);

		assertThat(breadcrumbServiceUtil.isGuestStagingGroup(mockGroup), equalTo(true));
	}

	@Test
	public void isGuestStagingGroup_WhenLiveGroupKeyIsNotGuest_ThenReturnsFalse() {
		when(mockGroup.isStaged()).thenReturn(true);
		when(mockGroup.getLiveGroup()).thenReturn(mockLiveGroup);
		when(mockLiveGroup.getGroupKey()).thenReturn("another group");

		assertThat(breadcrumbServiceUtil.isGuestStagingGroup(mockGroup), equalTo(false));
	}

	private void mockGroupWithPrivateLayoutSet() throws Exception {
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.getLayoutSet()).thenReturn(mockLayoutSet);
		when(mockLayoutSet.getGroup()).thenReturn(mockGroup);

		when(mockLayoutSet.isPrivateLayout()).thenReturn(true);
		when(mockGroup.getPrivateLayoutsPageCount()).thenReturn(PAGE_COUNT);
		when(mockThemeDisplay.getSessionId()).thenReturn(SESSION_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(LOCALE);
		when(mockGroup.getDescriptiveName(LOCALE)).thenReturn(GROUP_NAME);

		when(mockThemeDisplay.isAddSessionIdToURL()).thenReturn(true);
		when(PortalUtil.getLayoutSetFriendlyURL(mockLayoutSet, mockThemeDisplay)).thenReturn(FRIENDLY_URL);
		when(PortalUtil.getURLWithSessionId(FRIENDLY_URL, SESSION_ID)).thenReturn(FRIENDLY_URL_WITH_SESSION);

		when(mockBreadcrumbEntryFactory.createBreadcrumbEntry(GROUP_NAME, FRIENDLY_URL_WITH_SESSION)).thenReturn(mockBreadcrumbEntry);

	}
}
