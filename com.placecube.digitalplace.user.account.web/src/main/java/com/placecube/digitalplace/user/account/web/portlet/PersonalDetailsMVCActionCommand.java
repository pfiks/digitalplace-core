package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.PERSONAL_DETAILS }, service = MVCActionCommand.class)
public class PersonalDetailsMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private AccountService accountService;

	@Reference
	private AccountValidationService accountValidationService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		AccountContext accountContext = accountContextService.getOrCreateContext(actionRequest);
		accountContextService.configurePersonalDetails(accountContext, actionRequest);
		accountContextService.configureSkipPasswordStep(accountContext);

		UserProfileFieldsConfiguration userFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());
		CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, actionRequest);

		accountContext.clearErrors();
		accountValidationService.validatePersonalDetails(accountContext, actionRequest, accountConfiguration, userFieldsConfiguration);
		accountValidationService.validateCaptcha(accountContext, actionRequest, accountConfiguration, AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		String mvcCommandKey = MVCCommandKeys.PERSONAL_DETAILS;

		if (!accountContext.hasErrors()) {
			String nextStep = accountConfigurationService.getPersonalDetailsNextStep(accountContext);
			if (Validator.isNotNull(nextStep)) {
				mvcCommandKey = nextStep;
			} else {
				accountService.createUserAccount(actionRequest, accountContext, accountConfiguration.addressOutOfLocalArea());
				mvcCommandKey = MVCCommandKeys.CONFIRMATION_PAGE;
			}
		}
		actionResponse.getRenderParameters().setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, mvcCommandKey);
		accountContextService.updateContextInSession(accountContext, actionRequest);
	}

}
