package com.placecube.digitalplace.form.exception;

import com.liferay.portal.kernel.exception.PortalException;

@SuppressWarnings("serial")
public class FormDefinitionException extends PortalException {

	public FormDefinitionException(String msg) {
		super(msg);
	}

	public FormDefinitionException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public FormDefinitionException(Throwable cause) {
		super(cause);
	}
}
