package com.placecube.digitalplace.payment.capita.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.payment.capita.configuration.CapitaCompanyConfiguration", localization = "content/Language", name = "payment-capita")
public interface CapitaCompanyConfiguration {

	@Meta.AD(required = false, deflt = "https://sbsctest.e-paycapita.com/scp/scpws", name = "capita-endpoint-url")
	String capitaEndpointURL();

	@Meta.AD(required = false, deflt = "", name = "capita-hmackey")
	String capitaHMACKey();

	@Meta.AD(required = false, deflt = "", name = "capita-hmackeyid")
	String capitaHMACKeyId();

	@Meta.AD(required = false, deflt = "", name = "capita-scpid")
	String capitaSCPId();

	@Meta.AD(required = false, deflt = "", name = "capita-siteid")
	String capitaSiteId();

	@Meta.AD(required = false, deflt = "", name = "default-advanced-payment-configuration", description = "default-advanced-payment-configuration-help")
	String defaultAdvancedConfiguration();

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();
}
