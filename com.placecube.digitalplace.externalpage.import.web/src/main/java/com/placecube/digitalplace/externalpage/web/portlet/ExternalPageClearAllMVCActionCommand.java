package com.placecube.digitalplace.externalpage.web.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;

import java.util.Calendar;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET,
		"mvc.command.name=" + MVCCommandKeys.EXTERNAL_PAGE_CLEAR_ALL }, service = MVCActionCommand.class)
public class ExternalPageClearAllMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ExternalPageClearAllMVCActionCommand.class);

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long groupId = ((ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY)).getScopeGroupId();
		try {
			LOG.info("Clearing External Pages By GroupId: " + Calendar.getInstance().getTime() + ", GroupID(" + groupId + ")");
			externalPageLocalService.removeExternalPagesByGroupId(groupId);
		} catch (Exception e) {
			LOG.error("Error Clearing External Pages By GroupId: " + Calendar.getInstance().getTime() + ", GroupID(" + groupId + ")" + e.getMessage(), e);
			SessionErrors.add(actionRequest, "externalPageImportError");
		}
	}

}
