package com.placecube.digitalplace.address.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.ListTypeConstants;
import com.liferay.portal.kernel.service.ListTypeLocalService;
import com.placecube.digitalplace.address.constants.AddressType;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class AddressTypeListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(AddressTypeListener.class);

	@Reference
	private ListTypeLocalService listTypeLocalService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		ListType primaryAddressType = listTypeLocalService.getListType(company.getCompanyId(), AddressType.PRIMARY.getName(), ListTypeConstants.CONTACT_ADDRESS);
		if (primaryAddressType == null) {
			listTypeLocalService.addListType(company.getCompanyId(), AddressType.PRIMARY.getName(), ListTypeConstants.CONTACT_ADDRESS);
			LOG.debug("Adding a new address type: " + AddressType.PRIMARY.getName());
		}
	}
}