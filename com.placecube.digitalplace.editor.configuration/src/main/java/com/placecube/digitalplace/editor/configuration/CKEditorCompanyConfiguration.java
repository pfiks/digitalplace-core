package com.placecube.digitalplace.editor.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "editor", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.editor.configuration.CKEditorCompanyConfiguration", localization = "content/Language", name = "ck-editor")
public interface CKEditorCompanyConfiguration {

	@Meta.AD(required = false, deflt = "", name = "plugin-roles-json-configuration", description = "plugin-roles-json-configuration-description")
	String pluginAndRolesJsonConfiguration();

	@Meta.AD(required = false, deflt = "", name = "plugin-json-configuration", description = "plugin-json-configuration-description")
	String pluginJsonConfiguration();
}
