package com.placecube.digitalplace.addresslookup.statmap.service.utils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.statmap.constants.StatMapConstants;

@Component(immediate = true, service = AddressRetrievalService.class)
public class AddressRetrievalService {

	private static final Log LOG = LogFactoryUtil.getLog(AddressRetrievalService.class);

	@Reference
	private AddressRetrivalUtil addressRetrivalUtil;

	@Reference
	private JSONFactory jsonFactory;

	public String executeCall(String urlToCall) {
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		try {

			LOG.debug("Retrieving addresses for url: " + urlToCall);

			HttpGet method = addressRetrivalUtil.getHttpGetMethod(urlToCall);
			client = HttpClients.createDefault();
			response = client.execute(method);

			String responseString = EntityUtils.toString(response.getEntity());

			LOG.debug("Response: " + responseString);

			return responseString;

		} catch (Exception e) {
			LOG.error(e);
			return StringPool.BLANK;
		} finally {
			addressRetrivalUtil.quietlyCloseResources(client, response);
		}
	}

	public AddressContext getAddressContextFromResponse(String response) throws JSONException {
		JSONObject responseJSONObject = jsonFactory.createJSONObject(response);
		String recordValue = responseJSONObject.getString(StatMapConstants.JSON_FIELD_RECORD);
		JSONObject recordJSONObject = jsonFactory.createJSONObject(recordValue);

		String uprn = recordJSONObject.getString(StatMapConstants.JSON_FIELD_UPRN);
		String addressLine1 = getAddressLine1(recordJSONObject);
		String addressLine2 = getAddressLine2(recordJSONObject);
		String addressLine3 = StringPool.BLANK;
		String addressLine4 = StringPool.BLANK;
		String city = recordJSONObject.getString(StatMapConstants.JSON_FIELD_TOWN_NAME);
		String postcode = recordJSONObject.getString(StatMapConstants.JSON_FIELD_POSTCODE);

		return new AddressContext(uprn, addressLine1, addressLine2, addressLine3, addressLine4, city, postcode);
	}

	public Set<String> getAddressIdsFromResponse(String response) throws JSONException {
		Set<String> results = new LinkedHashSet<>();
		if (Validator.isNotNull(response)) {
			JSONObject responseJSONObject = jsonFactory.createJSONObject(response);
			JSONArray idsJSONArray = responseJSONObject.getJSONArray(StatMapConstants.JSON_FIELD_IDS);
			if (null != idsJSONArray) {
				for (int i = 0; i < idsJSONArray.length(); i++) {
					results.add(idsJSONArray.getString(i));
				}
			}
		}
		return results;
	}

	public String getRecordURL(String recordEndpoint, String token, String recordId) {
		List<NameValuePair> nvps = new ArrayList<>();

		String url = recordEndpoint.endsWith("/") ? recordEndpoint.concat(recordId) : recordEndpoint.concat("/").concat(recordId);

		return addressRetrivalUtil.getURL(url, nvps, token);
	}

	public String getSearchURL(String baseSearchEndpoint, String token, String searchParameterName, String searchTerm) {
		List<NameValuePair> nvps = new ArrayList<>();
		nvps.add(new BasicNameValuePair(searchParameterName, searchTerm));

		return addressRetrivalUtil.getURL(baseSearchEndpoint, nvps, token);
	}

	private String getAddressLine1(JSONObject recordJSONObject) {
		String organisation = recordJSONObject.getString(StatMapConstants.JSON_FIELD_ORGANISATION);
		String saoText = recordJSONObject.getString(StatMapConstants.JSON_FIELD_SAO_TEXT);
		String saoStart = addressRetrivalUtil.getJoinedValuesFromFieldNamesWithNoSpace(recordJSONObject, StatMapConstants.JSON_FIELD_SAO_START_NUMBER, StatMapConstants.JSON_FIELD_SAO_START_SUFFIX);
		String saoEnd = addressRetrivalUtil.getJoinedValuesFromFieldNamesWithNoSpace(recordJSONObject, StatMapConstants.JSON_FIELD_SAO_END_NUMBER, StatMapConstants.JSON_FIELD_SAO_END_SUFFIX);
		String paoText = recordJSONObject.getString(StatMapConstants.JSON_FIELD_PAO_TEXT);
		String paoStart = addressRetrivalUtil.getJoinedValuesFromFieldNamesWithNoSpace(recordJSONObject, StatMapConstants.JSON_FIELD_PAO_START_NUMBER, StatMapConstants.JSON_FIELD_PAO_START_SUFFIX);
		String paoEnd = addressRetrivalUtil.getJoinedValuesFromFieldNamesWithNoSpace(recordJSONObject, StatMapConstants.JSON_FIELD_PAO_END_NUMBER, StatMapConstants.JSON_FIELD_PAO_END_SUFFIX);

		return addressRetrivalUtil.getJoinedValuesWithSpace(organisation, saoText, saoStart, saoEnd, paoText, paoStart, paoEnd);
	}

	private String getAddressLine2(JSONObject recordJSONObject) {
		return recordJSONObject.getString(StatMapConstants.JSON_FIELD_STREET_DESCRIPTOR);
	}
}
