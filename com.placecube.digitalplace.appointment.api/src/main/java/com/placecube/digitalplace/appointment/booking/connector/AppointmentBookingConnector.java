package com.placecube.digitalplace.appointment.booking.connector;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;

public interface AppointmentBookingConnector {

	/**
	 * Books the appointment in all the configured calendars for the serviceId.
	 * Returns a list of appointment ids booked. The ids are the ids generated
	 * by the connector.
	 *
	 * @param companyId the companyId
	 * @param serviceId the serviceId
	 * @param appointmentBookingEntry the appointment to book
	 * @return list of booked appointment ids
	 */
	List<String> bookAppointment(long companyId, String serviceId, AppointmentBookingEntry appointmentBookingEntry);

	/**
	 * Cancels the appointment in all the configured calendars for the
	 * serviceId. Returns a list of appointment ids that have successfully been
	 * cancelled. The ids are the ids generated used by the connector.
	 *
	 * @param companyId the companyId
	 * @param serviceId the serviceId
	 * @param appointmentId the appointment to cancel
	 * @return list of cancelled appointment ids
	 */
	List<String> cancelAppointment(long companyId, String serviceId, String appointmentId);

	List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, AppointmentContext appointmentContext);

	List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, Date startDate, Date endDate, TimeZone timeZone);

	List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, String appointmentId);

	List<AppointmentBookingSlot> getAppointmentSlots(long companyId, String serviceId, AppointmentContext appointmentContext);

	List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int numberOfDatesToReturn, int dayOfWeek, TimeZone timeZone, int onlyDatesWithLessThanXappoinments);

	List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int dayOfWeek, TimeZone timeZone);

	boolean isEnabled(long companyId);

}
