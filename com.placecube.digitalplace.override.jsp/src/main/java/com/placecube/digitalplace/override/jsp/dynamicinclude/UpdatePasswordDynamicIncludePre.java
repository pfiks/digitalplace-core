package com.placecube.digitalplace.override.jsp.dynamicinclude;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.taglib.BaseDynamicInclude;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude;
import com.placecube.digitalplace.user.account.service.UserAuthenticationService;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = DynamicInclude.class)
public class UpdatePasswordDynamicIncludePre extends BaseDynamicInclude {

	private static final String PASSWORD_POLICY = "passwordPolicy";

	private static final Log LOG = LogFactoryUtil.getLog(UpdatePasswordDynamicIncludePre.class);

	@Reference
	private UserAuthenticationService userAuthenticationService;

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String key) throws IOException {

		try {
			httpServletRequest.setAttribute(PASSWORD_POLICY, userAuthenticationService.getUserAccountPasswordPolicyHtml(httpServletRequest));
		} catch (PortletException e) {
			LOG.error(e);
		}
	}

	@Override
	public void register(DynamicIncludeRegistry dynamicIncludeRegistry) {
		dynamicIncludeRegistry.register("/html/portal/update_password.jsp#pre");
	}
}
