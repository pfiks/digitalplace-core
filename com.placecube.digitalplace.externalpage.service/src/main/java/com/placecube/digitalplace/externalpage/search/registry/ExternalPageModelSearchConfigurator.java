package com.placecube.digitalplace.externalpage.search.registry;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.registrar.ModelSearchConfigurator;
import com.liferay.portal.search.spi.model.result.contributor.ModelSummaryContributor;
import com.liferay.portal.search.spi.model.result.contributor.ModelVisibilityContributor;
import com.placecube.digitalplace.externalpage.model.ExternalPage;

@Component(immediate = true, service = ModelSearchConfigurator.class)
public class ExternalPageModelSearchConfigurator implements ModelSearchConfigurator<ExternalPage> {

	@Reference(target = "(indexer.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage)")
	private ModelIndexerWriterContributor<ExternalPage> modelIndexWriterContributor;
	
	@Reference(target = "(indexer.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage)")
	private ModelSummaryContributor modelSummaryContributor;

	@Reference(target = "(indexer.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage)")
	private ModelVisibilityContributor modelVisibilityContributor;

	
	@Override
	public String getClassName() {
		return ExternalPage.class.getName();
	}
	
	@Override
	public String[] getDefaultSelectedFieldNames() {
		return new String[] {Field.ENTRY_CLASS_PK, Field.ENTRY_CLASS_NAME, Field.COMPANY_ID, Field.CONTENT, Field.GROUP_ID, Field.TITLE, Field.UID, Field.URL};
	}
	
	@Override
	public ModelIndexerWriterContributor<ExternalPage> getModelIndexerWriterContributor() {
		return modelIndexWriterContributor;
	}

	@Override
	public ModelVisibilityContributor getModelVisibilityContributor() {
		return modelVisibilityContributor;
	}
	
	@Override
	public ModelSummaryContributor getModelSummaryContributor() {
		return modelSummaryContributor;
	}
}
