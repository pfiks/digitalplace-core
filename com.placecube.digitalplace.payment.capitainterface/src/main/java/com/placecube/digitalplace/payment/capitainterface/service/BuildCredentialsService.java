package com.placecube.digitalplace.payment.capitainterface.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.ErrorCode;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.LoggingBean;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.Service;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.SourceSystem;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.service.LoggingFormatService;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;

import uk.co.capita_software.support.selfservice.Credentials;
import uk.co.capita_software.support.selfservice.RequestIdentification;
import uk.co.capita_software.support.selfservice.Signature;
import uk.co.capita_software.support.selfservice.Subject;
import uk.co.capita_software.support.selfservice.SystemCode;

/**
 * Service/facade class onto the Capita payment gateway, for building the
 * Credentials
 *
 */
public class BuildCredentialsService {

	private static final Log LOG = LogFactoryUtil.getLog(BuildCredentialsService.class);
	private static final int RANDOM_MULTIPLER = 10000;
	private static Subject subject = null;

	private static long uniqueReference = (long) (Math.random() * RANDOM_MULTIPLER);

	private final PaymentProperties properties;

	public BuildCredentialsService() {
		properties = new PaymentProperties();
	}

	public BuildCredentialsService(PaymentProperties properties) {
		this.properties = properties;
	}

	/**
	 * Build the credentials object to be passed in any of the Capita web
	 * service calls
	 *
	 * @param customerReference - unique customer reference to generate a unique
	 *            reference for the request
	 * @return the credentials
	 */
	public Credentials buildCredentials(String customerReference) {

		RequestIdentification requestIdentification = buildRequestIdentification(customerReference);
		Signature signature = buildSignature(requestIdentification);
		Subject credSubject = buildSubject();

		Credentials credentials = new Credentials();
		credentials.setRequestIdentification(requestIdentification);
		credentials.setSignature(signature);
		credentials.setSubject(credSubject);

		return credentials;
	}

	/**
	 * Build a RequestIdentification object required for the Credentials element
	 * of any Capita call
	 *
	 * @param customerReference unique customer reference to generate a unique
	 *            reference for the request
	 * @return a unique RequestIdentification object
	 */
	public RequestIdentification buildRequestIdentification(String customerReference) {
		RequestIdentification requestIdentification = new RequestIdentification();

		requestIdentification.setTimeStamp(generateTimeStamp());
		requestIdentification.setUniqueReference(getUniqueReference(customerReference));

		return requestIdentification;
	}

	/**
	 * Build a Signature object required for the Credentials element of any
	 * Capita call
	 *
	 * @param requestIdentification uses this to build the digest
	 * @return a unique Signature
	 */
	public Signature buildSignature(RequestIdentification requestIdentification) {

		if (requestIdentification == null || Validator.isNull(requestIdentification.getTimeStamp()) || Validator.isNull(requestIdentification.getUniqueReference())) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0059, "")));

			return null;
		}

		Signature signature = new Signature();

		// The original algorithm
		signature.setAlgorithm(CapitaConstants.ALGORITHM);
		signature.setDigest(createDigest(requestIdentification));
		signature.setHmacKeyID(properties.getHmacKeyId());

		return signature;
	}

	/**
	 * Builds the Subject to be used in the Credentials object
	 *
	 * @return the Subject to be used in the Credentials object
	 */
	public Subject buildSubject() {

		if (subject == null) {
			subject = new Subject();
			subject.setSubjectType(CapitaConstants.SUBJECT_TYPE);
			subject.setSystemCode(SystemCode.SCP);
		}

		subject.setIdentifier(properties.getScpId());

		return subject;
	}

	/**
	 * Creates a digest specifically for use in the Credentials element.
	 *
	 * @param requestIdentification contains timestamp and unique reference
	 *            which are used in the creation of the digest
	 * @return the digest for use in the Credentials element
	 */
	public String createDigest(RequestIdentification requestIdentification) {

		/**
		 *
		 * The <digest> element contains a keyed hash of the other data under
		 * the <credentials> element. The key will be provided by Capita
		 * Software Services. There will be a separate key for each SCP instance
		 * (identified by scpId). Therefore a client that interfaces with more
		 * than one SCP instance must support multiple keys. A client must also
		 * provide facilities to change the hmacKeyID and key for a particular
		 * scpId if requested by Capita Software Services.
		 *
		 * The digest is calculated as follows: Concatenate subjectType,
		 * identifier, uniqueReference, timestamp, algorithm and hmacKeyID into
		 * a single string, with a '!' inserted between each concatenated value.
		 * E.g. the result might be:
		 * CapitaPortal!37!X326736B!20110203201814!Original!2 Convert this
		 * string to a sequence of bytes, using UTF-8 encoding. Generate an
		 * HMAC/SHA256 keyed hash of the resulting byte sequence, using the key
		 * supplied by Capita Software Services. Base-64 encode the hash.
		 */

		if (requestIdentification == null || Validator.isNull(requestIdentification.getTimeStamp()) || Validator.isNull(requestIdentification.getTimeStamp())) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0059, "")));

			return null;
		}
		// Concatenate subjectType, identifier, uniqueReference, timestamp,
		// algorithm and hmacKeyID
		// into a single string, with a '!' inserted between each concatenated
		// value
		String toDigest = CapitaConstants.SUBJECT_TYPE.getValue() + CapitaConstants.DIGEST_SEPARATOR + properties.getScpId() + CapitaConstants.DIGEST_SEPARATOR
				+ requestIdentification.getUniqueReference() + CapitaConstants.DIGEST_SEPARATOR + requestIdentification.getTimeStamp() + CapitaConstants.DIGEST_SEPARATOR
				+ CapitaConstants.ALGORITHM.getValue() + CapitaConstants.DIGEST_SEPARATOR + properties.getHmacKeyId();

		return createDigest(toDigest);

	}

	/**
	 * Generates a timestamp in the format required for the credentials element
	 *
	 * @return the timestamp as a String
	 */
	public String generateTimeStamp() {
		final Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		cal.setTime(new Date());
		final String result = String.format("%04d%02d%02d%02d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY),
				cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

		return result;

	}

	/**
	 * Generate a unique reference number to be used in the capita invoke call
	 *
	 * @param customerReference unique reference for the customer
	 * @return the unique generated reference
	 */
	public String getUniqueReference(String customerReference) {

		// a hash of the customerReference plus a timestamp
		long timestamp = Calendar.getInstance().getTimeInMillis();
		String uniqueString = timestamp + customerReference + uniqueReference++;

		return createDigest(uniqueString);
	}

	/**
	 * Create a sha256HMAC digest of stringToEncode
	 *
	 * @param stringToEncode
	 * @return the digest
	 */
	private String createDigest(String stringToEncode) {
		try {
			// Convert this string to a sequence of bytes, using UTF-8 encoding.
			byte[] digestBytes = stringToEncode.getBytes(CapitaConstants.ENCODING_TYPE);

			// Generate an HMAC/SHA256 keyed hash of the resulting byte
			// sequence, using the key supplied by Capita Software Services.
			Mac sha256HMAC = Mac.getInstance(CapitaConstants.HASH_ALGORITHM);
			byte[] secretKey = Base64.decodeBase64(properties.getHmacKey().getBytes(CapitaConstants.ENCODING_TYPE));

			SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, CapitaConstants.HASH_ALGORITHM);
			sha256HMAC.init(secretKeySpec);

			byte[] hash = sha256HMAC.doFinal(digestBytes);

			// Base-64 encode the hash.
			hash = Base64.encodeBase64(hash);
			String hashAsString = new String(hash, CapitaConstants.ENCODING_TYPE);

			return hashAsString;

		} catch (UnsupportedEncodingException e) {
			LOG.trace(e.getStackTrace());
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0059, "")), e);

		} catch (InvalidKeyException e) {
			LOG.trace(e.getStackTrace());
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0059, "")), e);

		} catch (NoSuchAlgorithmException e) {
			LOG.trace(e.getStackTrace());
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0059, "")), e);

		}

		return "";
	}

}
