/**
 * PaymentBase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class PaymentBase  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.PaymentHeader paymentHeader;

    private com.capita_software_services.www.scp.base.AuthDetails authDetails;

    public PaymentBase() {
    }

    public PaymentBase(
           com.capita_software_services.www.scp.base.PaymentHeader paymentHeader,
           com.capita_software_services.www.scp.base.AuthDetails authDetails) {
           this.paymentHeader = paymentHeader;
           this.authDetails = authDetails;
    }


    /**
     * Gets the paymentHeader value for this PaymentBase.
     *
     * @return paymentHeader
     */
    public com.capita_software_services.www.scp.base.PaymentHeader getPaymentHeader() {
        return paymentHeader;
    }


    /**
     * Sets the paymentHeader value for this PaymentBase.
     *
     * @param paymentHeader
     */
    public void setPaymentHeader(com.capita_software_services.www.scp.base.PaymentHeader paymentHeader) {
        this.paymentHeader = paymentHeader;
    }


    /**
     * Gets the authDetails value for this PaymentBase.
     *
     * @return authDetails
     */
    public com.capita_software_services.www.scp.base.AuthDetails getAuthDetails() {
        return authDetails;
    }


    /**
     * Sets the authDetails value for this PaymentBase.
     *
     * @param authDetails
     */
    public void setAuthDetails(com.capita_software_services.www.scp.base.AuthDetails authDetails) {
        this.authDetails = authDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentBase)) return false;
        PaymentBase other = (PaymentBase) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.paymentHeader==null && other.getPaymentHeader()==null) ||
             (this.paymentHeader!=null &&
              this.paymentHeader.equals(other.getPaymentHeader()))) &&
            ((this.authDetails==null && other.getAuthDetails()==null) ||
             (this.authDetails!=null &&
              this.authDetails.equals(other.getAuthDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentHeader() != null) {
            _hashCode += getPaymentHeader().hashCode();
        }
        if (getAuthDetails() != null) {
            _hashCode += getAuthDetails().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentBase.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "paymentBase"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "paymentHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "paymentHeader"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "authDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "authDetails"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
