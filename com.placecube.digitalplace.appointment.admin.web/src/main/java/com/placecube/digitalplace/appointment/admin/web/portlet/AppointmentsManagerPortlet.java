package com.placecube.digitalplace.appointment.admin.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.appointment.admin.web.constants.DigitalServicesPanelCategoryConstants;
import com.placecube.digitalplace.appointment.admin.web.constants.PortletKeys;

@Component(immediate = true, property = { //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=portlet-appointmentsmanager", //
		"com.liferay.portlet.display-category=" + DigitalServicesPanelCategoryConstants.FRONT_END_PANEL_CATEGORY_DIGITAL_SERVICES, //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.render-weight=10", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.template-path=/", //
		"javax.portlet.init-param.view-template=/view.jsp", //
		"javax.portlet.name=" + PortletKeys.APPOINTMENTS_MANAGER, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=power-user,user", //
		"javax.portlet.supports.mime-type=text/html", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class AppointmentsManagerPortlet extends MVCPortlet {

}