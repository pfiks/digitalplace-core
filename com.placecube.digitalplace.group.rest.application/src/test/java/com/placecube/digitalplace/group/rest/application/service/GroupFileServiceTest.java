package com.placecube.digitalplace.group.rest.application.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.permission.ModelPermissions;
import com.liferay.portal.kernel.service.permission.ModelPermissionsFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ModelPermissionsFactory.class })
public class GroupFileServiceTest extends PowerMockito {

	@InjectMocks
	private GroupFileService groupFileService;

	@Mock
	private ModelPermissions mockModelPermissions;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private ResourcePermissionLocalService mockResourcePermissionLocalService;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private ResourcePermission mockResourcePermission2;

	@Mock
	private ResourcePermission mockResourcePermission3;

	@Mock
	private ResourcePermission mockResourcePermission1;

	@Mock
	private Role mockRole1;

	@Before
	public void activateSetup() {
		mockStatic(ModelPermissionsFactory.class);
	}

	@Test
	public void getModelPermissions_WhenNoError_ThenReturnsTheModelPermissionsConfiguredWithTheValidRolesAndViewActionKeys() throws PortalException {
		long companyId = 1;
		long recordSetId = 2;
		long roleId1 = 3;
		long roleId2 = 4;
		String roleName1 = "roleName1";
		when(ModelPermissionsFactory.create(new String[0], new String[0], DLFileEntry.class.getName())).thenReturn(mockModelPermissions);
		when(mockDDLRecordSet.getCompanyId()).thenReturn(companyId);
		when(mockDDLRecordSet.getRecordSetId()).thenReturn(recordSetId);
		when(mockResourcePermissionLocalService.getResourcePermissions(companyId, DDLRecordSet.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(recordSetId)))
				.thenReturn(Arrays.asList(mockResourcePermission1, mockResourcePermission2, mockResourcePermission3));

		when(mockResourcePermission1.isViewActionId()).thenReturn(true);
		when(mockResourcePermission1.getRoleId()).thenReturn(roleId1);
		when(mockRoleLocalService.getRole(roleId1)).thenThrow(new PortalException());

		when(mockResourcePermission2.isViewActionId()).thenReturn(true);
		when(mockResourcePermission2.getRoleId()).thenReturn(roleId2);
		when(mockRoleLocalService.getRole(roleId2)).thenReturn(mockRole1);
		when(mockRole1.getName()).thenReturn(roleName1);

		when(mockResourcePermission3.isViewActionId()).thenReturn(false);

		ModelPermissions result = groupFileService.getModelPermissions(mockDDLRecordSet);

		assertThat(result, sameInstance(mockModelPermissions));
		verify(mockModelPermissions, times(1)).addRolePermissions(roleName1, new String[] { ActionKeys.VIEW });
	}

}
