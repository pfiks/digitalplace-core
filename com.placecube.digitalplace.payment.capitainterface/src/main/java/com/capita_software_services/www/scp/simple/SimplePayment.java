/**
 * SimplePayment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.simple;

public class SimplePayment  extends com.capita_software_services.www.scp.base.PaymentBase  implements java.io.Serializable {
    private com.capita_software_services.www.scp.simple.SimpleSaleSummary saleSummary;

    private com.capita_software_services.www.scp.base.SurchargeDetails surchargeDetails;

    public SimplePayment() {
    }

    public SimplePayment(
           com.capita_software_services.www.scp.base.PaymentHeader paymentHeader,
           com.capita_software_services.www.scp.base.AuthDetails authDetails,
           com.capita_software_services.www.scp.simple.SimpleSaleSummary saleSummary,
           com.capita_software_services.www.scp.base.SurchargeDetails surchargeDetails) {
        super(
            paymentHeader,
            authDetails);
        this.saleSummary = saleSummary;
        this.surchargeDetails = surchargeDetails;
    }


    /**
     * Gets the saleSummary value for this SimplePayment.
     *
     * @return saleSummary
     */
    public com.capita_software_services.www.scp.simple.SimpleSaleSummary getSaleSummary() {
        return saleSummary;
    }


    /**
     * Sets the saleSummary value for this SimplePayment.
     *
     * @param saleSummary
     */
    public void setSaleSummary(com.capita_software_services.www.scp.simple.SimpleSaleSummary saleSummary) {
        this.saleSummary = saleSummary;
    }


    /**
     * Gets the surchargeDetails value for this SimplePayment.
     *
     * @return surchargeDetails
     */
    public com.capita_software_services.www.scp.base.SurchargeDetails getSurchargeDetails() {
        return surchargeDetails;
    }


    /**
     * Sets the surchargeDetails value for this SimplePayment.
     *
     * @param surchargeDetails
     */
    public void setSurchargeDetails(com.capita_software_services.www.scp.base.SurchargeDetails surchargeDetails) {
        this.surchargeDetails = surchargeDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SimplePayment)) return false;
        SimplePayment other = (SimplePayment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.saleSummary==null && other.getSaleSummary()==null) ||
             (this.saleSummary!=null &&
              this.saleSummary.equals(other.getSaleSummary()))) &&
            ((this.surchargeDetails==null && other.getSurchargeDetails()==null) ||
             (this.surchargeDetails!=null &&
              this.surchargeDetails.equals(other.getSurchargeDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSaleSummary() != null) {
            _hashCode += getSaleSummary().hashCode();
        }
        if (getSurchargeDetails() != null) {
            _hashCode += getSurchargeDetails().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SimplePayment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "simplePayment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "saleSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "simpleSaleSummary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "surchargeDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
