package com.placecube.digitalplace.form.extender.internal;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.HashMapDictionary;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.form.extender.FormDefinitionExtender;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.digitalplace.form.service.FormDefinitionService;

public class FormDefinitionExtenderServiceTest {

	private static String FORM_CATEGORY = "Category";
	private static String FORM_DESCRIPTION = "Description";
	private static String FORM_NAME = "Name";
	private static String FORM_KEY = "Key";
	private static String FORM_JOURNAL_ARTICLE_FOLDER_NAME = "FolderName";

	@InjectMocks
	private FormDefinitionExtenderService formDefinitionExtenderService;

	@Mock
	private BundleTrackerService mockBundleTrackerService;

	@Mock
	private FormDefinitionService mockFormDefinitionService;

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private FormDefinition mockFormDefinition;

	@Mock
	private ServiceRegistration<FormDefinition> mockServiceRegistration;

	@Mock
	private RegistrationService mockRegistrationService;

	@Mock
	private FormDefinitionExtender mockFormDefinitionExtender;

	@Mock
	private Dictionary<String, String> mockHeaders;

	@Mock
	private Bundle mockBundle;

	@Mock
	private HttpURLConnection mockConnection;

	@Mock
	private JournalArticleURLService mockJournalArticleURLService;

	@Mock
	private DataProviderURLService mockDataProviderURLService;

	@Before
	public void setup() {
		initMocks(this);
	}

	@Test
	public void createFormDefinition_WhenNoError_ThenCreateFormDefinition() throws Exception {

		when(mockHeaders.get("Digital-Place-Form-Key")).thenReturn(FORM_KEY);
		when(mockHeaders.get("Digital-Place-Form-Category")).thenReturn(FORM_CATEGORY);
		when(mockHeaders.get("Digital-Place-Form-Name")).thenReturn(FORM_NAME);
		when(mockHeaders.get("Digital-Place-Form-Description")).thenReturn(FORM_DESCRIPTION);
		when(mockHeaders.get("Digital-Place-Journal-Article-Folder-Name")).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);

		URL ddmForm = new URL(null, "file://ddm-form.json", createURLStreamHandler());
		URL ddmFormLayout = new URL(null, "file://ddm-form-layout.json", createURLStreamHandler());
		URL settingsDDMFormValues = new URL(null, "file://settings-ddm-form-values.json", createURLStreamHandler());

		List<URL> journalArticleURLs = new ArrayList<URL>();
		List<URL> dataProviderDDMFormValuesURLs = new ArrayList<URL>();

		when(mockBundle.getEntry("ddm-form.json")).thenReturn(ddmForm);
		when(mockBundle.getEntry("ddm-form-layout.json")).thenReturn(ddmFormLayout);
		when(mockBundle.getEntry("settings-ddm-form-values.json")).thenReturn(settingsDDMFormValues);
		when(mockJournalArticleURLService.getJournalArticleURLs(mockBundle)).thenReturn(journalArticleURLs);
		when(mockDataProviderURLService.getDataProviderDDMFormValuesURLs(mockBundle)).thenReturn(dataProviderDDMFormValuesURLs);

		when(mockFormDefinitionService.create(FORM_NAME, FORM_DESCRIPTION, FORM_CATEGORY, FORM_KEY, FORM_JOURNAL_ARTICLE_FOLDER_NAME, ddmForm, ddmFormLayout, settingsDDMFormValues, journalArticleURLs,
				dataProviderDDMFormValuesURLs)).thenReturn(mockFormDefinition);

		FormDefinition result = formDefinitionExtenderService.createFormDefinition(mockHeaders, mockBundle);

		assertThat(result, sameInstance(mockFormDefinition));

	}

	@Test
	public void isValidFormDefinition_WhenFormDescriptionIsNull_ThenReturnFalse() {

		when(mockHeaders.get("Digital-Place-Form-Category")).thenReturn(FORM_CATEGORY);
		when(mockHeaders.get("Digital-Place-Form-Name")).thenReturn(FORM_NAME);

		when(mockHeaders.get("Digital-Place-Form-Description")).thenReturn(null);

		assertFalse(formDefinitionExtenderService.isValidFormDefinition(mockHeaders));
	}

	@Test
	public void isValidFormDefinition_WhenFormNameIsNull_ThenReturnFalse() {

		when(mockHeaders.get("Digital-Place-Form-Category")).thenReturn(FORM_CATEGORY);
		when(mockHeaders.get("Digital-Place-Form-Description")).thenReturn(FORM_DESCRIPTION);

		when(mockHeaders.get("Digital-Place-Form-Name")).thenReturn(null);

		assertFalse(formDefinitionExtenderService.isValidFormDefinition(mockHeaders));

	}

	@Test
	public void isValidFormDefinition_WhenFormCategoryIsNull_ThenReturnFalse() {

		when(mockHeaders.get("Digital-Place-Form-Name")).thenReturn(FORM_NAME);
		when(mockHeaders.get("Digital-Place-Form-Description")).thenReturn(FORM_DESCRIPTION);

		when(mockHeaders.get("Digital-Place-Form-Category")).thenReturn(null);

		assertFalse(formDefinitionExtenderService.isValidFormDefinition(mockHeaders));

	}

	@Test
	public void isValidFormDefinition_WhenIsValid_ThenReturnTrue() {
		when(mockHeaders.get("Digital-Place-Form-Category")).thenReturn("Category");
		when(mockHeaders.get("Digital-Place-Form-Name")).thenReturn("Name");
		when(mockHeaders.get("Digital-Place-Form-Description")).thenReturn("Description");

		assertTrue(formDefinitionExtenderService.isValidFormDefinition(mockHeaders));

	}

	@Test
	public void register_WhenNoError_ThenRegisterService() {

		when(mockBundleContext.registerService(eq(FormDefinition.class), eq(mockFormDefinition), any(HashMapDictionary.class))).thenReturn(mockServiceRegistration);

		formDefinitionExtenderService.register(mockFormDefinition, mockBundleContext);

		verify(mockRegistrationService, times(1)).registerService(mockFormDefinition, mockServiceRegistration);

	}

	@Test
	public void unregister_WhenNoError_ThenUnregisterService() {

		formDefinitionExtenderService.unregister(mockFormDefinition);

		verify(mockRegistrationService, times(1)).unregisterService(mockFormDefinition);

	}

	@Test
	public void hasValidHeaders_WhenLengthIsGreaterThan75_ReturnsFalse() {

		when(mockHeaders.get("Digital-Place-Form-Category")).thenReturn(FORM_CATEGORY);
		when(mockHeaders.get("Digital-Place-Form-Name")).thenReturn(FORM_NAME);
		when(mockHeaders.get("Digital-Place-Form-Description")).thenReturn(FORM_DESCRIPTION);

		when(mockHeaders.get("Digital-Place-Form-Key")).thenReturn("form-key-greater-than-75----------------------------------------------------");

		assertFalse(formDefinitionExtenderService.hasValidHeaders(mockHeaders));
	}

	@Test
	public void hasValidHeaders_WhenLengthIsLessThan75_ReturnsFalse() {

		when(mockHeaders.get("Digital-Place-Form-Category")).thenReturn(FORM_CATEGORY);
		when(mockHeaders.get("Digital-Place-Form-Name")).thenReturn(FORM_NAME);
		when(mockHeaders.get("Digital-Place-Form-Description")).thenReturn(FORM_DESCRIPTION);

		when(mockHeaders.get("Digital-Place-Form-Key")).thenReturn("form-key-less-than-75----------------------------------------------------");

		assertTrue(formDefinitionExtenderService.hasValidHeaders(mockHeaders));
	}

	@Test
	public void openBundleTracker_WhenNoError_ThenOpenBundleTracker() {

		formDefinitionExtenderService.openBundleTracker(mockBundleContext, mockFormDefinitionExtender);

		verify(mockBundleTrackerService, times(1)).openBundleTracker(mockBundleContext, mockFormDefinitionExtender);

	}

	@Test
	public void closeBundleTracker_WhenNoError_ThenCloseBundleTracker() {

		formDefinitionExtenderService.closeBundleTracker();

		verify(mockBundleTrackerService, times(1)).closeBundleTracker();

	}

	private URLStreamHandler createURLStreamHandler() throws IOException {

		URLStreamHandler stubURLStreamHandler = new URLStreamHandler() {

			@Override
			protected URLConnection openConnection(URL u) throws IOException {
				return mockConnection;
			}
		};

		return stubURLStreamHandler;
	}

}
