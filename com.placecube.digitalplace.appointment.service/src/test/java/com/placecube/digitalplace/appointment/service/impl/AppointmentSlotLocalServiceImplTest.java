package com.placecube.digitalplace.appointment.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.placecube.digitalplace.appointment.model.AppointmentSlot;
import com.placecube.digitalplace.appointment.service.persistence.AppointmentSlotPersistence;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentSlotLocalServiceImplTest {

	@InjectMocks
	private AppointmentSlotLocalServiceImpl appointmentSlotLocalServiceImpl;

	@Mock
	private List<AppointmentSlot> mockAppointmentSlotList;

	@Mock
	private AppointmentSlotPersistence mockAppointmentSlotPersistence;

	@Test
	public void countByCompanyId_SlotType_ServiceId_WhenNoError_ThenReturnsTheCount() {
		long companyId = 1;
		String slotType = "mySlotType";
		String serviceId = "myServiceId";
		int count = 2;
		when(mockAppointmentSlotPersistence.countByCompanyId_SlotType_ServiceId(companyId, slotType, serviceId)).thenReturn(count);

		int result = appointmentSlotLocalServiceImpl.countAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(companyId, slotType, serviceId);

		assertThat(result, equalTo(count));
	}

	@Test
	public void getAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId_WhenNoError_ThenReturnsTheSlotsFound() {
		long companyId = 1;
		String slotType = "mySlotType";
		String serviceId = "myServiceId";
		int start = 2;
		int end = 4;
		when(mockAppointmentSlotPersistence.findByCompanyId_SlotType_ServiceId(companyId, slotType, serviceId, start, end)).thenReturn(mockAppointmentSlotList);

		List<AppointmentSlot> results = appointmentSlotLocalServiceImpl.getAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(companyId, slotType, serviceId, start, end);

		assertThat(results, sameInstance(mockAppointmentSlotList));
	}

}
