package com.placecube.digitalplace.payment.mock.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Optional;

import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.mock.configuration.MockCompanyConfiguration;
import com.placecube.digitalplace.payment.mock.web.constants.PortletKeys;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PortletURLFactoryUtil.class, ParamUtil.class, PropsUtil.class })
public class MockPaymentConnectorTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LiferayPortletRequest mockLiferayPortletRequest;

	@Mock
	private MockCompanyConfiguration mockMockCompanyConfiguration;

	@InjectMocks
	private MockPaymentConnector mockPaymentConnector;

	@Mock
	private PaymentContext mockPaymentContext;

	@Mock
	private PaymentResponse mockPaymentResponse;

	@Mock
	private Portal mockPortal;

	@Mock
	private LiferayPortletURL mockPortletURL;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetup() {
		mockStatic(PortletURLFactoryUtil.class, PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(MockCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = mockPaymentConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(MockCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockMockCompanyConfiguration);
		when(mockMockCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = mockPaymentConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getBackURL_WhenRequestRedirectAttributeIsPresent_ThenReturnRedirectWithParamValue() {
		String redirectAttributeValue = "redirect-value";
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(GetterUtil.getString(mockServiceContext.getAttribute("redirect"))).thenReturn(redirectAttributeValue);

		Optional<String> result = mockPaymentConnector.getBackURL(redirectAttributeValue, mockServiceContext);

		assertEquals(redirectAttributeValue, result.get());

	}

	@Test
	public void getBackURL_WhenRequestRedirectAttributeNotPresent_ThenReturnRedirectWithNoParamValue() {
		String redirectAttributeValue = "redirect-value";
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(GetterUtil.getString(mockServiceContext.getAttribute("redirect"))).thenReturn(StringPool.BLANK);

		Optional<String> result = mockPaymentConnector.getBackURL(redirectAttributeValue, mockServiceContext);

		assertEquals(StringPool.BLANK, result.get());

	}

	@Test
	public void getPaymentStatus_WhenPaymentSuccessParameterIsFalse_ThenReturnsPaymentErrorStatus() {
		String paymentReference = "anything";
		String queryString = "paymentSuccess=false";
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getQueryString()).thenReturn(queryString);

		PaymentStatus result = mockPaymentConnector.getPaymentStatus(paymentReference, mockServiceContext);

		assertEquals(PaymentStatus.paymentError(), result);
	}

	@Test
	public void getPaymentStatus_WhenPaymentSuccessParameterIsTrue_ThenReturnsPaymentSuccessStatus() {
		String paymentReference = "anything";
		String queryString = "paymentSuccess=true";
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getQueryString()).thenReturn(queryString);

		PaymentStatus result = mockPaymentConnector.getPaymentStatus(paymentReference, mockServiceContext);

		assertEquals(PaymentStatus.success(), result);
		assertThat(result.getPaymentUniqueReferenceNumber(), equalTo("UniquePRN-" + paymentReference));
	}

	@Test
	public void preparePayment_WhenAccountIdIsTheFailPreparePayment_ThenPaymentResponseIsAppErrorAndPaymentContextUrlIsSetAsReturnUrl() {
		String returnUrl = "http://localhost";
		when(mockPaymentContext.getAccountId()).thenReturn("failPreparePaymentAccountId");
		when(mockPaymentContext.getReturnURL()).thenReturn(returnUrl);

		PaymentResponse result = mockPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		assertEquals(PaymentStatus.appError(), result.getStatus());
		assertEquals(returnUrl, result.getRedirectURL());

	}

	@SuppressWarnings("deprecation")
	@Test
	public void preparePayment_WhenNoErrors_ThenParametersAreSetOnPaymentResponse() throws WindowStateException, PortletModeException {

		BigDecimal amount = new BigDecimal(new BigInteger("25"));
		String errorParams = "param1=25";
		String returnUrl = "http://localhost?" + errorParams;

		when(mockPaymentContext.getReturnURL()).thenReturn(returnUrl);
		when(mockServiceContext.getLiferayPortletRequest()).thenReturn(mockLiferayPortletRequest);
		when(mockPortal.getHttpServletRequest(mockLiferayPortletRequest)).thenReturn(mockHttpServletRequest);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, PortletKeys.MOCK_PAYMENT_PORTLET, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletURL);
		when(mockPaymentContext.getAmount()).thenReturn(amount);

		mockPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		InOrder inOrder = inOrder(mockPortletURL);
		inOrder.verify(mockPortletURL, times(1)).setWindowState(LiferayWindowState.EXCLUSIVE);
		inOrder.verify(mockPortletURL, times(1)).setPortletMode(PortletMode.VIEW);
		inOrder.verify(mockPortletURL, times(1)).setParameter("amount", amount.toString());
		inOrder.verify(mockPortletURL, times(1)).setParameter("returnUrl", returnUrl);

	}

	@Test
	public void preparePayment_WhenPaymentContextRedirectAttributeIsPresentAndNoErrors_ThenPaymentResponseHasPaymentReturnUrlAndSuccessStatus() {
		String returnUrl = "http://localhost";
		BigDecimal amount = new BigDecimal(new BigInteger("25"));

		when(mockPaymentContext.getReturnURL()).thenReturn(returnUrl);
		when(mockServiceContext.getLiferayPortletRequest()).thenReturn(mockLiferayPortletRequest);
		when(mockPortal.getHttpServletRequest(mockLiferayPortletRequest)).thenReturn(mockHttpServletRequest);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, PortletKeys.MOCK_PAYMENT_PORTLET, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletURL);
		when(mockPaymentContext.getAmount()).thenReturn(amount);
		when(mockPortletURL.toString()).thenReturn(returnUrl);

		PaymentResponse result = mockPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		assertEquals(returnUrl, result.getRedirectURL());
		assertEquals(PaymentStatus.success(), result.getStatus());
	}

	@Test
	public void preparePayment_WhenPaymentContextReturnUrlNotPresent_ThenPaymentResponseHasNullRedirectUrlAndAppErrorStatus() {
		when(mockPaymentContext.getReturnURL()).thenReturn(null);

		PaymentResponse result = mockPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		assertEquals(null, result.getRedirectURL());
		assertEquals(PaymentStatus.appError(), result.getStatus());

	}

	@Test
	public void preparePayment_WhenSettingPortletModeOnNewUrlHasError_ThenPaymentResponseIsAppErrorAndPaymentContextUrlIsSetAsReturnUrl() throws PortletModeException {
		String returnUrl = "http://localhost";
		BigDecimal amount = new BigDecimal(new BigInteger("25"));

		when(mockPaymentContext.getReturnURL()).thenReturn(returnUrl);
		when(mockServiceContext.getLiferayPortletRequest()).thenReturn(mockLiferayPortletRequest);
		when(mockPortal.getHttpServletRequest(mockLiferayPortletRequest)).thenReturn(mockHttpServletRequest);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, PortletKeys.MOCK_PAYMENT_PORTLET, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletURL);
		when(mockPaymentContext.getAmount()).thenReturn(amount);
		when(mockPortletURL.toString()).thenReturn(returnUrl);
		doThrow(new PortletModeException("", null)).when(mockPortletURL).setPortletMode(PortletMode.VIEW);

		PaymentResponse result = mockPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		assertEquals(PaymentStatus.appError(), result.getStatus());
		assertEquals(returnUrl, result.getRedirectURL());

	}

	@Test
	public void preparePayment_WhenSettingWindowStateOnNewUrlHasError_ThenPaymentResponseIsAppErrorAndPaymentContextUrlIsSetAsReturnUrl() throws WindowStateException {
		String returnUrl = "http://localhost";
		BigDecimal amount = new BigDecimal(new BigInteger("25"));

		when(mockPaymentContext.getReturnURL()).thenReturn(returnUrl);
		when(mockServiceContext.getLiferayPortletRequest()).thenReturn(mockLiferayPortletRequest);
		when(mockPortal.getHttpServletRequest(mockLiferayPortletRequest)).thenReturn(mockHttpServletRequest);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, PortletKeys.MOCK_PAYMENT_PORTLET, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletURL);
		when(mockPaymentContext.getAmount()).thenReturn(amount);
		when(mockPortletURL.toString()).thenReturn(returnUrl);
		doThrow(new WindowStateException("", null)).when(mockPortletURL).setWindowState(LiferayWindowState.EXCLUSIVE);

		PaymentResponse result = mockPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		assertEquals(PaymentStatus.appError(), result.getStatus());
		assertEquals(returnUrl, result.getRedirectURL());
	}
}
