package com.placecube.digitalplace.notifications.service.internal.emaildefault;

import java.io.File;
import java.util.Locale;
import java.util.Optional;

import javax.mail.internet.InternetAddress;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendMailContext;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = DefaultEmailNotificationService.class)
public class DefaultEmailNotificationService {

	private static final Log LOG = LogFactoryUtil.getLog(DefaultEmailNotificationService.class);

	@Reference
	private EmailNotificationUtils emailNotificationUtils;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private MailService mailService;

	public void sendMail(SendMailContext sendMailContext, ServiceContext serviceContext) throws NotificationsException {

		long groupId = sendMailContext.getGroupId() == 0 ? serviceContext.getScopeGroupId() : sendMailContext.getGroupId();

		String articleId = sendMailContext.getTemplateId();
		JournalArticle journalArticle = journalArticleLocalService.fetchLatestArticle(groupId, articleId, WorkflowConstants.STATUS_APPROVED);

		if (Validator.isNotNull(journalArticle)) {
			Locale locale = serviceContext.getLocale();
			Optional<String> subjectField = journalArticleRetrievalService.getFieldValue(journalArticle, "Subject", locale);
			Optional<String> bodyField = journalArticleRetrievalService.getFieldValue(journalArticle, "Body", locale);

			if (subjectField.isPresent() && bodyField.isPresent()) {
				String subjectText = emailNotificationUtils.getSubjectTextField(subjectField.get(), sendMailContext.getPersonalisation());
				String bodyText = emailNotificationUtils.getBodyTextField(getJournalArticleDisplayContent(groupId, articleId, bodyField.get(), serviceContext), sendMailContext.getPersonalisation());

				InternetAddress toAddress = emailNotificationUtils.getInternetAddress(sendMailContext.getEmailAddress(), null);
				InternetAddress[] bccEmailAddresses = emailNotificationUtils.getInternetAddress(sendMailContext.getBccEmailAddresses());
				InternetAddress fromAddress = emailNotificationUtils.getInternetAddress(sendMailContext.getFromEmailAddress(), sendMailContext.getFromName());

				MailMessage mailMessage = emailNotificationUtils.createMailMessage();
				mailMessage.setTo(toAddress);
				mailMessage.setBCC(bccEmailAddresses);
				mailMessage.setFrom(fromAddress);
				mailMessage.setSubject(subjectText);
				mailMessage.setBody(bodyText);
				mailMessage.setHTMLFormat(true);

				for (File file : sendMailContext.getAttachments()) {
					mailMessage.addFileAttachment(file);
				}

				mailService.sendEmail(mailMessage);

			} else {
				throw new NotificationsException("Template missing subject or body text: " + articleId);
			}
		} else {
			throw new NotificationsException("Template article can not be found: " + articleId);
		}
	}

	private String getJournalArticleDisplayContent(long groupId, String articleId, String body, ServiceContext serviceContext) {
		try {
			return journalArticleLocalService.getArticleDisplay(groupId, articleId, null, null, serviceContext.getThemeDisplay()).getContent();
		} catch (Exception e) {
			LOG.warn("Unable to retrieve JournalArticleDisplay for articleId: " + articleId + " due to " + e.getMessage());
			LOG.warn("Using article's body for basic placeholder replacement for articleId: " + articleId);
			return body;
		}

	}

}
