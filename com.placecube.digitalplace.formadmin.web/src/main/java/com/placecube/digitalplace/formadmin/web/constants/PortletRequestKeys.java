package com.placecube.digitalplace.formadmin.web.constants;

public final class PortletRequestKeys {

	public static final String BODY_SUPPORTED_METHODS = "bodySupportedMethods";

	public static final String CHAIN = "chain";

	public static final String CURRENT_TAB = "currentTab";

	public static final String DATA_CONSUMER_ID = "dataConsumerId";

	public static final String DATA_CONSUMERS = "dataConsumers";

	public static final String DATA_PROVIDER_INSTANCE_ID = "dataProviderInstanceId";

	public static final String DATA_PROVIDER_NAME = "dataProviderName";

	public static final String DATA_PROVIDERS = "dataProviders";

	public static final String DDM_FORM_FIELDS = "ddmFormFields";

	public static final String FORM = "form";

	public static final String FORM_DEFINITION_KEY = "formDefinitionKey";

	public static final String FORM_INSTANCE_ID = "formInstanceId";

	public static final String FORM_INSTANCE_NAME = "formInstanceName";

	public static final String FORM_SEARCH_CONTAINER = "formSearchContainer";

	public static final String HEADER_NAME = "headerName";

	public static final String HEADER_VALUE = "headerValue";

	public static final String INPUT_FIELDS = "inputFields";

	public static final String MAPPER = "mapper";

	public static final String METHOD = "method";

	public static final String METHODS = "methods";

	public static final String NAVIGATION_ITEMS = "navigationItems";

	public static final String OUTPUT_FIELDS = "outputFields";

	public static final String REQUEST_BODY = "requestBody";

	public static final String SEARCH_CONTAINER = "searchContainer";

	public static final String SELECTED_METHOD = "selectedMethod";

	public static final String SERVICE_HEADERS = "serviceHeaders";

	public static final String SOURCE_DATA_PROVIDER_ID = "sourceDataProviderId";

	public static final String SOURCE_OUTPUT_FIELDS = "sourceOutputFields";

	public static final String URL = "url";

	private PortletRequestKeys() {
		return;
	}
}