package com.placecube.digitalplace.search.shared.service;

import java.util.List;

import com.placecube.digitalplace.search.shared.model.SortByOption;

public interface SortByOptionsRegistry {

	/**
	 * Returns all the registered SortByOptions ordered by displayOrder
	 * ascending.
	 *
	 * @return list of sort by options
	 */
	List<SortByOption> getAllAvailableSortByOptions();

	/**
	 * Returns all the registered SortByOptions ordered by displayOrder
	 * ascending that have the id included in the list of idsToMatch
	 *
	 * @param idsToMatch the sortByOption ids to match
	 * @return list of sort by options that have the specified ids
	 */
	List<SortByOption> getAvailableSortByOptionsForIds(String[] idsToMatch);

}
