create table Placecube_Appointment_AppointmentEntry (
	uuid_ VARCHAR(75) null,
	appointmentEntryId LONG not null primary key,
	companyId LONG,
	entryClassPK LONG,
	entryClassNameId LONG,
	appointmentId TEXT null,
	serviceId STRING null,
	connectorClassName STRING null,
	createDate DATE null,
	appointmentStartDate DATE null,
	appointmentEndDate DATE null
);

create table Placecube_Appointment_AppointmentSlot (
	uuid_ VARCHAR(75) null,
	appointmentSlotId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	slotType VARCHAR(75) null,
	name STRING null,
	serviceId STRING null,
	startDate DATE null,
	endDate DATE null,
	slotStartTimeHour INTEGER,
	slotStartTimeMinute INTEGER,
	slotEndTimeHour INTEGER,
	slotEndTimeMinute INTEGER,
	appointmentDuration INTEGER,
	daysOfWeekBitwise LONG,
	maxAppointments INTEGER
);