package com.placecube.digitalplace.companyinitializeradmin.constants;

public final class ViewKeys {

	public static final String VIEW = "/view.jsp";

	private ViewKeys() {
	}
}