package com.placecube.digitalplace.payment.govuk.service.commerce.servlet;

import static com.placecube.digitalplace.payment.constants.PaymentConstants.API_AUTH_KEY;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.commerce.model.CommerceOrder;
import com.liferay.commerce.payment.engine.CommercePaymentEngine;
import com.liferay.commerce.payment.util.CommercePaymentHttpHelper;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.servlet.PortalSessionThreadLocal;
import com.liferay.portal.kernel.settings.GroupServiceSettingsLocator;
import com.liferay.portal.kernel.settings.SettingsLocatorHelperUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.govuk.service.GovUKPaymentConnector;
import com.placecube.digitalplace.payment.govuk.service.commerce.GovUKCommerceConstants;
import com.placecube.digitalplace.payment.govuk.service.commerce.configuration.GovUKPayGroupServiceConfiguration;
import com.placecube.digitalplace.payment.govuk.service.internal.GovUKPaymentOrderFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceContextThreadLocal.class, PortalSessionThreadLocal.class, ParamUtil.class, SettingsLocatorHelperUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil", "com.liferay.portal.kernel.settings.SettingsLocatorHelperUtil" })
public class GovUKPayCommercePaymentMethodServletTest extends PowerMockito {

	@Mock
	private CommerceOrder mockCommerceOrder;

	@Mock
	private CommercePaymentHttpHelper mockCommercePaymentHttpHelper;

	@Mock
	private CommercePaymentEngine mockCommercePaymentEngine;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private GovUKPayGroupServiceConfiguration mockGovUKPayGroupServiceConfiguration;

	@Mock
	private GovUKPaymentConnector mockGovUKPaymentConnector;

	@Mock
	private GovUKPaymentOrderFactory mockGovUKPaymentOrderFactory;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private HttpSession mockHttpSession;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private GovUKPayCommercePaymentMethodServlet govUKPayCommercePaymentMethodServlet;

	@Before
	public void activateSetup() {
		mockStatic(ServiceContextThreadLocal.class, PortalSessionThreadLocal.class, ParamUtil.class, SettingsLocatorHelperUtil.class);
	}

	@Test
	public void doGet_WhenExceptionGettingCommerceOrder_ThenDoNotInvokePaymentEngine() throws Exception {
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockCommercePaymentHttpHelper.getCommerceOrder(mockHttpServletRequest)).thenThrow(Exception.class);

		govUKPayCommercePaymentMethodServlet.doGet(mockHttpServletRequest, mockHttpServletResponse);

		verifyZeroInteractions(mockCommercePaymentEngine);
	}

	@Test
	public void doGet_WhenNoExceptionPaymentStatusIsFailedAndPaymentStatusCodeIsCancel_ThenCancelPayment() throws Exception {
		String paymentId = "paymentId";
		String keyName = "key";
		long commerceOrderId = 123;
		String redirect = "redirect";

		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockCommercePaymentHttpHelper.getCommerceOrder(mockHttpServletRequest)).thenReturn(mockCommerceOrder);
		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(paymentId);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(GroupServiceSettingsLocator.class))).thenReturn(mockGovUKPayGroupServiceConfiguration);
		when(mockGovUKPayGroupServiceConfiguration.keyName()).thenReturn(keyName);
		when(mockGovUKPaymentConnector.getPaymentStatus("paymentReference", mockServiceContext)).thenReturn(PaymentStatus.paymentError(GovUKCommerceConstants.CANCEL_PAYMENT_RESULT_CODE, "message"));
		when(mockCommerceOrder.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(ParamUtil.getString(mockHttpServletRequest, "redirect")).thenReturn(redirect);

		govUKPayCommercePaymentMethodServlet.doGet(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = Mockito.inOrder(mockServiceContext, mockGovUKPaymentConnector, mockCommercePaymentEngine, mockHttpServletResponse);
		inOrder.verify(mockServiceContext, times(1)).setRequest(mockHttpServletRequest);
		inOrder.verify(mockServiceContext, times(1)).setAttribute(API_AUTH_KEY, keyName);
		inOrder.verify(mockGovUKPaymentConnector, times(1)).getPaymentStatus("paymentReference", mockServiceContext);
		inOrder.verify(mockCommercePaymentEngine, times(1)).cancelPayment(commerceOrderId, StringPool.BLANK, mockHttpServletRequest);
		inOrder.verify(mockHttpServletResponse, times(1)).sendRedirect(redirect);
		verifyStatic(PortalSessionThreadLocal.class);
		PortalSessionThreadLocal.setHttpSession(mockHttpSession);
	}

	@Test
	public void doGet_WhenNoExceptionPaymentStatusIsFailedAndPaymentStatusCodeIsNotCancel_ThenCompletePayment() throws Exception {
		String paymentId = "paymentId";
		String keyName = "key";
		long commerceOrderId = 123;
		String redirect = "redirect";

		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockCommercePaymentHttpHelper.getCommerceOrder(mockHttpServletRequest)).thenReturn(mockCommerceOrder);
		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(paymentId);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(GroupServiceSettingsLocator.class))).thenReturn(mockGovUKPayGroupServiceConfiguration);
		when(mockGovUKPayGroupServiceConfiguration.keyName()).thenReturn(keyName);
		when(mockGovUKPaymentConnector.getPaymentStatus("paymentReference", mockServiceContext)).thenReturn(PaymentStatus.paymentError());
		when(mockCommerceOrder.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(ParamUtil.getString(mockHttpServletRequest, "orderType")).thenReturn("normal");
		when(ParamUtil.getString(mockHttpServletRequest, "redirect")).thenReturn(redirect);

		govUKPayCommercePaymentMethodServlet.doGet(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = Mockito.inOrder(mockServiceContext, mockGovUKPaymentConnector, mockCommercePaymentEngine, mockHttpServletResponse);
		inOrder.verify(mockServiceContext, times(1)).setRequest(mockHttpServletRequest);
		inOrder.verify(mockServiceContext, times(1)).setAttribute(API_AUTH_KEY, keyName);
		inOrder.verify(mockGovUKPaymentConnector, times(1)).getPaymentStatus("paymentReference", mockServiceContext);
		inOrder.verify(mockCommercePaymentEngine, times(1)).completePayment(commerceOrderId, paymentId, mockHttpServletRequest);
		inOrder.verify(mockHttpServletResponse, times(1)).sendRedirect(redirect);
		verifyStatic(PortalSessionThreadLocal.class);
		PortalSessionThreadLocal.setHttpSession(mockHttpSession);
	}

	@Test
	public void doGet_WhenNoExceptionPaymentStatusIsSuccess_ThenCompletePayment() throws Exception {
		String paymentId = "paymentId";
		String keyName = "key";
		long commerceOrderId = 123;
		String redirect = "redirect";

		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockCommercePaymentHttpHelper.getCommerceOrder(mockHttpServletRequest)).thenReturn(mockCommerceOrder);
		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(paymentId);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(GroupServiceSettingsLocator.class))).thenReturn(mockGovUKPayGroupServiceConfiguration);
		when(mockGovUKPayGroupServiceConfiguration.keyName()).thenReturn(keyName);
		when(mockGovUKPaymentConnector.getPaymentStatus("paymentReference", mockServiceContext)).thenReturn(PaymentStatus.success());
		when(mockCommerceOrder.getCommerceOrderId()).thenReturn(commerceOrderId);
		when(ParamUtil.getString(mockHttpServletRequest, "orderType")).thenReturn("normal");
		when(ParamUtil.getString(mockHttpServletRequest, "redirect")).thenReturn(redirect);

		govUKPayCommercePaymentMethodServlet.doGet(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = Mockito.inOrder(mockServiceContext, mockGovUKPaymentConnector, mockCommercePaymentEngine, mockHttpServletResponse);
		inOrder.verify(mockServiceContext, times(1)).setRequest(mockHttpServletRequest);
		inOrder.verify(mockServiceContext, times(1)).setAttribute(API_AUTH_KEY, keyName);
		inOrder.verify(mockGovUKPaymentConnector, times(1)).getPaymentStatus("paymentReference", mockServiceContext);
		inOrder.verify(mockCommercePaymentEngine, times(1)).completePayment(commerceOrderId, paymentId, mockHttpServletRequest);
		inOrder.verify(mockHttpServletResponse, times(1)).sendRedirect(redirect);
		verifyStatic(PortalSessionThreadLocal.class);
		PortalSessionThreadLocal.setHttpSession(mockHttpSession);
	}


}
