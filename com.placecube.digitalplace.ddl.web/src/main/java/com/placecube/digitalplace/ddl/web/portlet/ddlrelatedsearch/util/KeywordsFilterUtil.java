package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class KeywordsFilterUtil {

	private static final List<String> STOP_WORDS = Arrays.asList( "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into", "is", "it", "no", "not", "of", "on", "or", "such",
			"that", "the", "their", "then", "there", "these", "they", "this", "to", "was", "will", "with" );

	public static List<String> removeStopWords(String[] words) {
		return Arrays.stream(words).filter(word->!STOP_WORDS.contains(word)).collect(Collectors.toList());
	}

	private KeywordsFilterUtil() {

	}
}