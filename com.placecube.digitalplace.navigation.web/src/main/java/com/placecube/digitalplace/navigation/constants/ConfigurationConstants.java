package com.placecube.digitalplace.navigation.constants;

public final class ConfigurationConstants {

	public static final String NAVIGATION_MENU_GROUP_ID = "navigationMenuGroupId";

	public static final String NAVIGATION_MENU_ID = "navigationMenuId";

	public static final String NAVIGATION_PID = "com.placecube.digitalplace.navigation.configuration.NavigationPortletInstanceConfiguration";

	public static final String NAVIGATION_TYPE = "navigationType";

	public static final String WIDGET_TEMPLATE_GROUP_ID = "widgetTemplateGroupId";

	public static final String WIDGET_TEMPLATE_KEY = "widgetTemplateKey";

	private ConfigurationConstants() {
		return;
	}

}
