package com.placecube.digitalplace.group.rest.application.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;
import com.placecube.digitalplace.group.rest.exception.GroupRestKeyException;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;

public class SignatureBuilderServiceTest extends PowerMockito {

	@InjectMocks
	private SignatureBuilderService signatureBuilderService;

	private static final String APPLICATION_KEY = "applicationKey";
	private static final String DATE_HEADER = "dateHeader";
	private static final String CONTEXT_PATH = "contextPath";
	private static final String PATH = "path";
	private static final String METHOD = "method";
	private static final String BODY = "body";

	@Mock
	private GroupApiKeyLocalService mockGroupApiKeyLocalService;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void getEvaluatedSignature_WhenException_ThenThrowsGroupRestSecurityExceptionWithBadRequestStatus() throws Exception {
		String textToEncode = String.join(StringPool.NEW_LINE, APPLICATION_KEY, DATE_HEADER, CONTEXT_PATH, PATH, METHOD, BODY);
		when(mockGroupApiKeyLocalService.getEncodedTextWithKey(APPLICATION_KEY, textToEncode)).thenThrow(new GroupRestKeyException());

		try {
			signatureBuilderService.getEvaluatedSignature(APPLICATION_KEY, DATE_HEADER, CONTEXT_PATH, PATH, METHOD, BODY);
		} catch (GroupRestSecurityException e) {
			assertThat(e.getResponse().getStatusInfo(), equalTo(Response.Status.BAD_REQUEST));
		}
	}

	@Test
	public void getEvaluatedSignature_WhenException_ThenThrowsGroupRestSecurityExceptionWithInvalidSignatureMessage() throws Exception {
		String textToEncode = String.join(StringPool.NEW_LINE, APPLICATION_KEY, DATE_HEADER, CONTEXT_PATH, PATH, METHOD, BODY);
		when(mockGroupApiKeyLocalService.getEncodedTextWithKey(APPLICATION_KEY, textToEncode)).thenThrow(new GroupRestKeyException());

		try {
			signatureBuilderService.getEvaluatedSignature(APPLICATION_KEY, DATE_HEADER, CONTEXT_PATH, PATH, METHOD, BODY);
		} catch (GroupRestSecurityException e) {
			assertThat(e.getMessage(), equalTo(ErrorCodes.INVALID_SIGNATURE.getCode()));
		}
	}

	@Test
	public void filter_WhenNoError_ThenReturnsTheEvaluatedSignature() throws Exception {
		String textToEncode = String.join(StringPool.NEW_LINE, APPLICATION_KEY, DATE_HEADER, CONTEXT_PATH, PATH, METHOD, BODY);
		String signature = "signatureEvaluated";
		when(mockGroupApiKeyLocalService.getEncodedTextWithKey(APPLICATION_KEY, textToEncode)).thenReturn(signature);

		String result = signatureBuilderService.getEvaluatedSignature(APPLICATION_KEY, DATE_HEADER, CONTEXT_PATH, PATH, METHOD, BODY);

		assertThat(result, equalTo(signature));
	}

	@Test
	public void validateSignatureMatch_WhenValuesAreTheSame_ThenNoExceptionIsThrown() {
		String signature = "signatureEvaluated";
		try {
			signatureBuilderService.validateSignatureMatch(signature, signature);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void validateSignatureMatch_WhenValuesAreNotTheSame_ThenThrowsGroupRestSecurityExceptionWithForbiddenStatus() {
		try {
			signatureBuilderService.validateSignatureMatch("signatureOne", "differentValue");
		} catch (GroupRestSecurityException e) {
			assertThat(e.getResponse().getStatusInfo(), equalTo(Response.Status.FORBIDDEN));
		}
	}

	@Test
	public void validateSignatureMatch_WhenValuesAreNotTheSame_ThenThrowsGroupRestSecurityExceptionWithInvalidSignatureMessage() {
		try {
			signatureBuilderService.validateSignatureMatch("signatureOne", "differentValue");
		} catch (GroupRestSecurityException e) {
			assertThat(e.getMessage(), equalTo(ErrorCodes.INVALID_SIGNATURE.getCode()));
		}
	}
}
