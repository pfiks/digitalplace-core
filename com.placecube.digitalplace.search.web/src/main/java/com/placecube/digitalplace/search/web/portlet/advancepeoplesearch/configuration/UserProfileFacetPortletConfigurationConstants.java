package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration;

public class UserProfileFacetPortletConfigurationConstants {

	public static final String APPLY_PRIVACY = "applyPrivacy";

	public static final String FILTER_COLOUR = "filterColour";

	public static final String SEARCH_FIELDS = "searchFields";

	public static final String TITLE = "title";

	private UserProfileFacetPortletConfigurationConstants() {
	}
}
