<%@ include file="/init.jsp" %>

<div class="container-fluid container-fluid-max-xl sidenav-container pl-4" >

	<c:choose>
		<c:when test="${disableImportStaging}">

			<div class="alert alert-info mt-5">
				<liferay-ui:message key="import-disabled-staging"/>
			</div>

		</c:when>
		<c:otherwise>

			<div class="alert alert-info mt-5">
				<liferay-ui:message key="import-page-structure-info"/>
			</div>

			<portlet:actionURL name="<%= MVCCommandKeys.IMPORT_PAGES %>" var="importPagesURL"/>

			<aui:form action="${importPagesURL}" name="importPages" enctype="multipart/form-data" method="post" >

				<liferay-ui:error key="PageImportError" message="${ errorMessage }" />

				<aui:input name="importAsPrivate" type="toggle-switch" label="import-as-private-page" helpMessage="import-as-private-page-help" />

				<c:if test="${not empty vocabularies}">
					<aui:select name="assetVocabularyId" required="false" label="vocabulary" showEmptyOption="true" helpMessage="import-vocabulary-help" >
						<c:forEach var="vocabulary" items="${vocabularies}">
							<aui:option value="${vocabulary.getVocabularyId()}">${vocabulary.getName()}</aui:option>
						</c:forEach>
					</aui:select>
				</c:if>

				<aui:input name="file" type="file" required="true">
					<aui:validator name="acceptFiles">
						'xlsx'
					</aui:validator>
				</aui:input>

				<aui:button type="submit" value="import"/>

			</aui:form>

			<c:if test="${not empty rowImportModels}">
				<div class="table-responsive mt-5">
					<table class="table table-hover">
						<thead>
							<tr>
								<th scope="col"><liferay-ui:message key="row-index"/></th>
								<th scope="col"><liferay-ui:message key="type"/></th>
								<th scope="col"><liferay-ui:message key="import-outcome"/></th>
								<th scope="col"><liferay-ui:message key="errors"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${rowImportModels}" var="rowImportModel">
								<tr>
									<td>
										${rowImportModel.index}
									</td>
									<td>
										<liferay-ui:message key="${rowImportModel.isWebContent() ? 'web-content' : 'page'}"/>
									</td>
									<td>
										<c:choose>
											<c:when test="${empty rowImportModel.errorMessages}">
												<aui:icon cssClass="icon-monospaced text-success" image="check-circle" markupView="lexicon" />
											</c:when>
											<c:otherwise>
												<aui:icon cssClass="icon-monospaced text-danger" image="exclamation-circle" markupView="lexicon" />
											</c:otherwise>
										</c:choose>
									</td>
									<td>
										<c:choose>
											<c:when test="${not empty rowImportModel.errorMessages}">
												<c:forEach items="${rowImportModel.errorMessages}" var="errorMessage">
													<p class="text-danger">${errorMessage}</p>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<c:forEach items="${rowImportModel.warningMessages}" var="warningMessage">
													<p class="text-warning">${warningMessage}</p>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:if>

		</c:otherwise>
	</c:choose>

</div>
