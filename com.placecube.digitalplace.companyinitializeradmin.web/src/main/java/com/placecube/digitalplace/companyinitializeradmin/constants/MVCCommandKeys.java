package com.placecube.digitalplace.companyinitializeradmin.constants;

public final class MVCCommandKeys {

	public static final String RUN_COMPANY_INITIALIZER = "run-company-initializer";

	private MVCCommandKeys() {
	}
}