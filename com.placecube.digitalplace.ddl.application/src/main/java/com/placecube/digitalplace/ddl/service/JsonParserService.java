package com.placecube.digitalplace.ddl.service;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.osgi.service.component.annotations.Component;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component(immediate = true, service = JsonParserService.class)
public class JsonParserService {

	private static final Set<String> EXCLUDE_FIELDS = new HashSet<>();
	private static Gson gson = getGson();

	private static ExclusionStrategy getExclusionStrategy() {
		return new ExclusionStrategy() {

			@Override
			public boolean shouldSkipClass(Class<?> clazz) {
				return false;
			}

			@Override
			public boolean shouldSkipField(FieldAttributes field) {
				return EXCLUDE_FIELDS.contains(field.getName());
			}
		};
	}

	private static Gson getGson() {
		EXCLUDE_FIELDS.add("__hashCodeCalc");
		EXCLUDE_FIELDS.add("__equalsCalc");
		EXCLUDE_FIELDS.add("_timeZone");

		final GsonBuilder builder = new GsonBuilder();
		builder.setDateFormat("dd/MM/yy");
		builder.setPrettyPrinting();
		builder.setExclusionStrategies(getExclusionStrategy());
		builder.setFieldNamingStrategy(f -> {
			String fieldName = f.getName();
			if (fieldName.startsWith("_") && fieldName.length() > 1) {
				fieldName = fieldName.substring(1, 2).toLowerCase(Locale.ROOT) + fieldName.substring(2);
			}
			return fieldName;
		});

		return builder.create();
	}

	public String toJson(Object object) {
		return gson.toJson(object);
	}

}