package com.placecube.digitalplace.address.service;

import java.util.Optional;
import java.util.Set;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.address.model.AddressContext;

public interface AddressLookupService {

	AddressContext createAddressContext(String uprn, String addressLine1, String addressLine2, String addressLine3, String addressLine4, String city, String postcode);

	Set<AddressContext> getByKeyword(long companyId, String keyword, String[] paramsAndValues, boolean fallbackToNationalLookup) throws PortalException;

	Set<AddressContext> getByPostcode(long companyId, String postcode, String[] paramsAndValues, boolean fallbackToNationalLookup) throws PortalException;

	Optional<AddressContext> getByUprn(long companyId, String uprn, String[] paramsAndValues, boolean fallbackToNationalLookup) throws PortalException;

	Address saveAddress(User user, AddressContext addressContext, ServiceContext serviceContext) throws PortalException;
}
