package com.placecube.digitalplace.authentication.twofactor.web.lifefyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.authentication.twofactor.web.constants.WebContentArticles;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class TwoFactorAuthenticationLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(TwoFactorAuthenticationLifecycleListener.class);

	@Reference
	private TwoFactorAuthenticationSetupService twoFactorSetupService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		long companyId = company.getCompanyId();
		LOG.debug("Initialising Two factor authentication for companyId: " + companyId);

		Group globalGroup = company.getGroup();
		ServiceContext serviceContext = twoFactorSetupService.getServiceContext(globalGroup);

		JournalFolder journalFolder = twoFactorSetupService.addFolder(serviceContext);
		twoFactorSetupService.addArticle(WebContentArticles.TWO_FACTOR_SETUP, journalFolder, serviceContext);
		twoFactorSetupService.addArticle(WebContentArticles.TWO_FACTOR_VERIFY_CODE, journalFolder, serviceContext);
		twoFactorSetupService.addArticle(WebContentArticles.TWO_FACTOR_USER_RESET, journalFolder, serviceContext);

		LOG.debug("Configuration finished for Two factor authentication for companyId: " + companyId);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Implementation not required
	}

}
