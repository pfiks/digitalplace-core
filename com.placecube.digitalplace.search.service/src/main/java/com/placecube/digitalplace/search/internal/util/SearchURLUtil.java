package com.placecube.digitalplace.search.internal.util;

import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Http;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;

/*
 * This class is a copy of the Liferay portal-search-web com.liferay.portal.search.web.internal.portlet.shared.task.SearchHttpUtil
 * except the method getNullPortletURL which was added as utility method
 */
@Component(immediate = true, service = { SearchURLUtil.class })
public class SearchURLUtil {

	private static final Log LOG = LogFactoryUtil.getLog(SearchURLUtil.class);

	@Reference
	private Portal portal;

	public String getCompleteOriginalURL(HttpServletRequest httpServletRequest) {

		boolean forwarded = false;

		Object requestURLObject = httpServletRequest.getAttribute(JavaConstants.JAVAX_SERVLET_FORWARD_REQUEST_URI);

		if (requestURLObject != null) {
			forwarded = true;
		}

		String requestURL = null;
		String queryString = null;

		if (forwarded) {
			requestURL = portal.getAbsoluteURL(httpServletRequest, (String) httpServletRequest.getAttribute(JavaConstants.JAVAX_SERVLET_FORWARD_REQUEST_URI));

			queryString = (String) httpServletRequest.getAttribute(JavaConstants.JAVAX_SERVLET_FORWARD_QUERY_STRING);
		} else {
			requestURL = String.valueOf(httpServletRequest.getRequestURL());

			queryString = httpServletRequest.getQueryString();
		}

		StringBuffer sb = new StringBuffer();

		sb.append(requestURL);

		if (queryString != null) {
			sb.append(StringPool.QUESTION);
			sb.append(queryString);
		}

		String proxyPath = portal.getPathProxy();

		if (Validator.isNotNull(proxyPath)) {
			int x = sb.indexOf(Http.PROTOCOL_DELIMITER) + Http.PROTOCOL_DELIMITER.length();

			int y = sb.indexOf(StringPool.SLASH, x);

			sb.insert(y, proxyPath);
		}

		String completeURL = sb.toString();

		if (httpServletRequest.isRequestedSessionIdFromURL()) {
			HttpSession session = httpServletRequest.getSession();

			String sessionId = session.getId();

			completeURL = portal.getURLWithSessionId(completeURL, sessionId);
		}

		if (LOG.isWarnEnabled() && completeURL.contains("?&")) {
			LOG.warn("Invalid URL " + completeURL);
		}

		return completeURL;
	}

	public PortletURL getNullPortletURL(String urlString) {
		return new NullPortletURL() {

			@Override
			public String toString() {
				return urlString;
			}

		};
	}

}