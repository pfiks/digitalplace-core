/**
 * AuthDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class AuthDetails  implements java.io.Serializable {
    private java.lang.String authCode;

    private int amountInMinorUnits;

    private java.lang.String maskedCardNumber;

    private com.capita_software_services.www.portal_api.CardDescription cardDescription;

    private com.capita_software_services.www.scp.base.CardType cardType;

    private java.lang.String merchantNumber;

    private java.lang.String expiryDate;

    private org.apache.axis.types.PositiveInteger continuousAuditNumber;

    public AuthDetails() {
    }

    public AuthDetails(
           java.lang.String authCode,
           int amountInMinorUnits,
           java.lang.String maskedCardNumber,
           com.capita_software_services.www.portal_api.CardDescription cardDescription,
           com.capita_software_services.www.scp.base.CardType cardType,
           java.lang.String merchantNumber,
           java.lang.String expiryDate,
           org.apache.axis.types.PositiveInteger continuousAuditNumber) {
           this.authCode = authCode;
           this.amountInMinorUnits = amountInMinorUnits;
           this.maskedCardNumber = maskedCardNumber;
           this.cardDescription = cardDescription;
           this.cardType = cardType;
           this.merchantNumber = merchantNumber;
           this.expiryDate = expiryDate;
           this.continuousAuditNumber = continuousAuditNumber;
    }


    /**
     * Gets the authCode value for this AuthDetails.
     *
     * @return authCode
     */
    public java.lang.String getAuthCode() {
        return authCode;
    }


    /**
     * Sets the authCode value for this AuthDetails.
     *
     * @param authCode
     */
    public void setAuthCode(java.lang.String authCode) {
        this.authCode = authCode;
    }


    /**
     * Gets the amountInMinorUnits value for this AuthDetails.
     *
     * @return amountInMinorUnits
     */
    public int getAmountInMinorUnits() {
        return amountInMinorUnits;
    }


    /**
     * Sets the amountInMinorUnits value for this AuthDetails.
     *
     * @param amountInMinorUnits
     */
    public void setAmountInMinorUnits(int amountInMinorUnits) {
        this.amountInMinorUnits = amountInMinorUnits;
    }


    /**
     * Gets the maskedCardNumber value for this AuthDetails.
     *
     * @return maskedCardNumber
     */
    public java.lang.String getMaskedCardNumber() {
        return maskedCardNumber;
    }


    /**
     * Sets the maskedCardNumber value for this AuthDetails.
     *
     * @param maskedCardNumber
     */
    public void setMaskedCardNumber(java.lang.String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }


    /**
     * Gets the cardDescription value for this AuthDetails.
     *
     * @return cardDescription
     */
    public com.capita_software_services.www.portal_api.CardDescription getCardDescription() {
        return cardDescription;
    }


    /**
     * Sets the cardDescription value for this AuthDetails.
     *
     * @param cardDescription
     */
    public void setCardDescription(com.capita_software_services.www.portal_api.CardDescription cardDescription) {
        this.cardDescription = cardDescription;
    }


    /**
     * Gets the cardType value for this AuthDetails.
     *
     * @return cardType
     */
    public com.capita_software_services.www.scp.base.CardType getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this AuthDetails.
     *
     * @param cardType
     */
    public void setCardType(com.capita_software_services.www.scp.base.CardType cardType) {
        this.cardType = cardType;
    }


    /**
     * Gets the merchantNumber value for this AuthDetails.
     *
     * @return merchantNumber
     */
    public java.lang.String getMerchantNumber() {
        return merchantNumber;
    }


    /**
     * Sets the merchantNumber value for this AuthDetails.
     *
     * @param merchantNumber
     */
    public void setMerchantNumber(java.lang.String merchantNumber) {
        this.merchantNumber = merchantNumber;
    }


    /**
     * Gets the expiryDate value for this AuthDetails.
     *
     * @return expiryDate
     */
    public java.lang.String getExpiryDate() {
        return expiryDate;
    }


    /**
     * Sets the expiryDate value for this AuthDetails.
     *
     * @param expiryDate
     */
    public void setExpiryDate(java.lang.String expiryDate) {
        this.expiryDate = expiryDate;
    }


    /**
     * Gets the continuousAuditNumber value for this AuthDetails.
     *
     * @return continuousAuditNumber
     */
    public org.apache.axis.types.PositiveInteger getContinuousAuditNumber() {
        return continuousAuditNumber;
    }


    /**
     * Sets the continuousAuditNumber value for this AuthDetails.
     *
     * @param continuousAuditNumber
     */
    public void setContinuousAuditNumber(org.apache.axis.types.PositiveInteger continuousAuditNumber) {
        this.continuousAuditNumber = continuousAuditNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthDetails)) return false;
        AuthDetails other = (AuthDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.authCode==null && other.getAuthCode()==null) ||
             (this.authCode!=null &&
              this.authCode.equals(other.getAuthCode()))) &&
            this.amountInMinorUnits == other.getAmountInMinorUnits() &&
            ((this.maskedCardNumber==null && other.getMaskedCardNumber()==null) ||
             (this.maskedCardNumber!=null &&
              this.maskedCardNumber.equals(other.getMaskedCardNumber()))) &&
            ((this.cardDescription==null && other.getCardDescription()==null) ||
             (this.cardDescription!=null &&
              this.cardDescription.equals(other.getCardDescription()))) &&
            ((this.cardType==null && other.getCardType()==null) ||
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType()))) &&
            ((this.merchantNumber==null && other.getMerchantNumber()==null) ||
             (this.merchantNumber!=null &&
              this.merchantNumber.equals(other.getMerchantNumber()))) &&
            ((this.expiryDate==null && other.getExpiryDate()==null) ||
             (this.expiryDate!=null &&
              this.expiryDate.equals(other.getExpiryDate()))) &&
            ((this.continuousAuditNumber==null && other.getContinuousAuditNumber()==null) ||
             (this.continuousAuditNumber!=null &&
              this.continuousAuditNumber.equals(other.getContinuousAuditNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuthCode() != null) {
            _hashCode += getAuthCode().hashCode();
        }
        _hashCode += getAmountInMinorUnits();
        if (getMaskedCardNumber() != null) {
            _hashCode += getMaskedCardNumber().hashCode();
        }
        if (getCardDescription() != null) {
            _hashCode += getCardDescription().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        if (getMerchantNumber() != null) {
            _hashCode += getMerchantNumber().hashCode();
        }
        if (getExpiryDate() != null) {
            _hashCode += getExpiryDate().hashCode();
        }
        if (getContinuousAuditNumber() != null) {
            _hashCode += getContinuousAuditNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AuthDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "authDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "authCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amountInMinorUnits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "amountInMinorUnits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maskedCardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "maskedCardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/portal-api", "cardDescription"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchantNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "merchantNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expiryDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "expiryDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("continuousAuditNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "continuousAuditNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "positiveInteger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
