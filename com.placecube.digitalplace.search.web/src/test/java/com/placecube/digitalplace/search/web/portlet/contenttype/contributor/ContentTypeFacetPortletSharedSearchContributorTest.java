package com.placecube.digitalplace.search.web.portlet.contenttype.contributor;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;

public class ContentTypeFacetPortletSharedSearchContributorTest extends PowerMockito {

	@InjectMocks
	private ContentTypeFacetPortletSharedSearchContributor contentTypeFacetPortletSharedSearchContributor;

	@Mock
	private FacetConfiguration mockFacetConfiguration;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private SharedActiveFilter mockSharedActiveFilter1;

	@Mock
	private SharedActiveFilter mockSharedActiveFilter2;

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheContentTypeFacetInTheContributorSettings() throws ConfigurationException {
		when(mockSharedSearchContributorSettings.createFacetConfiguration(SearchFacetConstants.CONTENT_TYPE)).thenReturn(mockFacetConfiguration);
		when(mockFacetConfiguration.getData()).thenReturn(mockJSONObject);

		contentTypeFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockFacetConfiguration, mockJSONObject, mockSharedSearchContributorSettings);
		inOrder.verify(mockFacetConfiguration, times(1)).setOrder("OrderHitsDesc");
		inOrder.verify(mockFacetConfiguration, times(1)).setStatic(false);
		inOrder.verify(mockFacetConfiguration, times(1)).setWeight(1.4d);
		inOrder.verify(mockJSONObject, times(1)).put("maxTerms", 100);
		inOrder.verify(mockJSONObject, times(1)).put("frequencyThreshold", 1);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).addFacet(SearchFacetConstants.CONTENT_TYPE, Optional.of(mockFacetConfiguration));
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheSelectedAssetContentTypesSplitByCommaInTheContributorSettings() throws ConfigurationException {
		when(mockSharedSearchContributorSettings.createFacetConfiguration(SearchFacetConstants.CONTENT_TYPE)).thenReturn(mockFacetConfiguration);
		when(mockFacetConfiguration.getData()).thenReturn(mockJSONObject);
		List<SharedActiveFilter> filters = new LinkedList<>();
		filters.add(mockSharedActiveFilter1);
		filters.add(mockSharedActiveFilter2);
		when(mockSharedActiveFilter1.getValue()).thenReturn("value1,value2");
		when(mockSharedActiveFilter2.getValue()).thenReturn("value3");
		when(mockSharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_CONTENT_TYPES)).thenReturn(filters);
		List<String> selectedContentTypes = new LinkedList<>();
		selectedContentTypes.add("value1");
		selectedContentTypes.add("value2");
		selectedContentTypes.add("value3");

		contentTypeFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQueryForMultipleValues(SearchFacetConstants.CONTENT_TYPE, selectedContentTypes, BooleanClauseOccur.MUST);
	}

}
