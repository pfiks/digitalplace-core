package com.placecube.digitalplace.externalpage.web.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.portlet.PortletRequest;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.externalpage.web.cleaner.HtmlCleanerFactory;
import com.placecube.digitalplace.externalpage.web.constants.CrawlConstants;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;

import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;

public class CrawlUtil {

	private static final String CUSTOM_DATA_SEPARATOR = StringPool.SEMICOLON;
	private static final Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg|pdf|png|mp3|mp4|zip|gz))$");
	private static final Log LOG = LogFactoryUtil.getLog(CrawlUtil.class);

	private CrawlUtil() {
		return;
	}

	public static String extractContent(String content, String xpath) {
		HtmlCleaner cleaner = HtmlCleanerFactory.createHtmlCleaner();

		CleanerProperties properties = cleaner.getProperties();
		properties.setCharset("utf-8");
		properties.setPruneTags("script,style");
		properties.setOmitComments(true);

		TagNode tagNode = cleaner.clean(content);

		StringBuilder stringBuilder = new StringBuilder();

		addLeadingSpaceToContentNodes(tagNode);

		if (Validator.isNotNull(xpath)) {

			try {
				Object results[] = tagNode.evaluateXPath(xpath);

				for (Object result : results) {

					if (result instanceof TagNode) {
						stringBuilder.append(((TagNode) result).getText());
					} else {
						stringBuilder.append(String.valueOf(result));
					}
				}

			} catch (Exception e) {
				LOG.error("Error applying xpath: " + xpath + " to content", e);
			}

		} else {
			stringBuilder.append(tagNode.getText());
		}

		return trimResultText(stringBuilder.toString());
	}

	public static String generateCustomData(PortletRequest portletRequest) {
		String xpath = ParamUtil.getString(portletRequest, PortletRequestKeys.XPATH);
		String indexArray[] = ParamUtil.getStringValues(portletRequest, PortletRequestKeys.INDEX_CONTENT);

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		StringBuilder customData = new StringBuilder();
		customData.append(CrawlConstants.CUSTOM_DATA_COMPANY_ID);
		customData.append(themeDisplay.getCompanyId());
		customData.append(CUSTOM_DATA_SEPARATOR);
		customData.append(CrawlConstants.CUSTOM_DATA_GROUP_ID);
		customData.append(themeDisplay.getScopeGroupId());
		customData.append(CUSTOM_DATA_SEPARATOR);
		customData.append(CrawlConstants.CUSTOM_DATA_USER_ID);
		customData.append(themeDisplay.getUserId());
		customData.append(CUSTOM_DATA_SEPARATOR);
		customData.append(CrawlConstants.CUSTOM_DATA_XPATH);
		customData.append(xpath);
		customData.append(CUSTOM_DATA_SEPARATOR);
		customData.append(CrawlConstants.CUSTOM_DATA_INDEX_CONTENT);
		customData.append(indexArray[0]);
		customData.append(CUSTOM_DATA_SEPARATOR);
		return customData.toString();
	}

	public static String generateCustomData(String xPath, String indexArr, long companyId, long scopeGroupId, long userId) {

		StringBuilder customData = new StringBuilder();
		customData.append(CrawlConstants.CUSTOM_DATA_COMPANY_ID);
		customData.append(companyId);
		customData.append(CUSTOM_DATA_SEPARATOR);
		customData.append(CrawlConstants.CUSTOM_DATA_GROUP_ID);
		customData.append(scopeGroupId);
		customData.append(CUSTOM_DATA_SEPARATOR);
		customData.append(CrawlConstants.CUSTOM_DATA_USER_ID);
		customData.append(userId);
		customData.append(CUSTOM_DATA_SEPARATOR);
		customData.append(CrawlConstants.CUSTOM_DATA_XPATH);
		customData.append(xPath);
		customData.append(CUSTOM_DATA_SEPARATOR);
		customData.append(CrawlConstants.CUSTOM_DATA_INDEX_CONTENT);
		customData.append(indexArr);
		customData.append(CUSTOM_DATA_SEPARATOR);
		return customData.toString();
	}

	public static boolean getBooleanFromCustomData(List<String> customData, String valuePrefix) throws PortalException {
		Optional<String> entryFound = customData.stream().filter(entry -> entry.startsWith(valuePrefix)).findFirst();
		if (entryFound.isPresent()) {
			return GetterUtil.getBoolean(com.liferay.portal.kernel.util.StringUtil.removeSubstring(entryFound.get(), valuePrefix));
		}
		throw new PortalException("Unable to retrieve " + valuePrefix + " from custom data");
	}

	public static List<String> getCustomData(CrawlController crawlController) {
		Object customData = crawlController.getCustomData();
		String data = GetterUtil.getString(customData);
		return Arrays.asList(data.split(CUSTOM_DATA_SEPARATOR));
	}

	public static String getDomain(String urlToCheck) throws URISyntaxException {
		URI uri = new URI(urlToCheck);
		return uri.getHost();
	}

	public static long getIdFromCustomData(List<String> customData, String valuePrefix) throws PortalException {
		Optional<String> entryFound = customData.stream().filter(entry -> entry.startsWith(valuePrefix)).findFirst();
		if (entryFound.isPresent()) {
			return GetterUtil.getLong(com.liferay.portal.kernel.util.StringUtil.removeSubstring(entryFound.get(), valuePrefix));
		}
		throw new PortalException("Unable to retrieve " + valuePrefix + " from custom data");
	}

	public static String getPageURL(Page page) {
		return page.getWebURL().getURL();
	}

	public static String getStringFromCustomData(List<String> customData, String valuePrefix) throws PortalException {
		Optional<String> entryFound = customData.stream().filter(entry -> entry.startsWith(valuePrefix)).findFirst();
		if (entryFound.isPresent()) {
			return GetterUtil.getString(com.liferay.portal.kernel.util.StringUtil.removeSubstring(entryFound.get(), valuePrefix));
		}
		throw new PortalException("Unable to retrieve " + valuePrefix + " from custom data");
	}

	public static boolean isValidURL(String url) {
		return !FILTERS.matcher(url).matches();
	}

	private static void addLeadingSpaceToContentNodes(TagNode node) {
		node.traverse(HtmlCleanerFactory.createLeadingSpaceTagNodeVisitor());
	}

	private static String trimResultText(String text) {
		return text.replaceAll("\\s+", " ").trim();
	}
}
