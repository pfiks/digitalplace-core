# com.placecube.digitalplace.ddl.web

## Friendly URL Mapper
The module displays a DDLRecord using the following friendly URL mappings

Display the DDLRecord on a maximised view using the recordId and template
${FULL_PAGE_URL}/-/ddl_display/ddl/${ddlRecordId}/${ddlTemplateId}

## Configuration
The portlet must be added to the whitelist. Edit portal-ext.properties and add 'com_placecube_digitalplace_ddl_portlet_DDLDisplayPortlet' to property 'portlet.add.default.resource.check.whitelist'
