package com.placecube.digitalplace.announcementsbanner.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.announcementsbanner.web.service.AnnouncementsBannerService;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class DismissAnnouncementMVCResourceCommandTest extends PowerMockito {

	@InjectMocks
	private DismissAnnouncementMVCResourceCommand dismissAnnouncementMVCResourceCommand;

	@Mock
	private AnnouncementsBannerService mockAnnouncementsBannerService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void doServeResource_WhenNoError_ThenMarksAnnouncementAsRead() {
		Long announcementsEntryId = 11l;
		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getHttpServletResponse(mockResourceResponse)).thenReturn(mockHttpServletResponse);
		when(ParamUtil.getLong(mockResourceRequest, "announcementsEntryId")).thenReturn(announcementsEntryId);

		dismissAnnouncementMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockAnnouncementsBannerService, times(1)).markAnnouncementAsRead(mockHttpServletRequest, mockHttpServletResponse, mockUser, announcementsEntryId);
	}

}
