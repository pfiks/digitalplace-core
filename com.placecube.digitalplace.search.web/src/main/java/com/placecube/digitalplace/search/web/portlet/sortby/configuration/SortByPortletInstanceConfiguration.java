package com.placecube.digitalplace.search.web.portlet.sortby.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.search.web.portlet.sortby.configuration.SortByPortletInstanceConfiguration")
public interface SortByPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "false")
	boolean chainSortByOptions();

	@Meta.AD(required = false, deflt = "true")
	boolean displayPortlet();

	@Meta.AD(required = false)
	String[] enabledSortByOptions();

	@Meta.AD(required = false, deflt = "false")
	boolean sortDirectionChangeable();
}
