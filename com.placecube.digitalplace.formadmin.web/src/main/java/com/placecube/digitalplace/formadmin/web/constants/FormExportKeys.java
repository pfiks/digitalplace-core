package com.placecube.digitalplace.formadmin.web.constants;

public final class FormExportKeys {

	public static final String ADDITION_COUNT = "addition-count";

	public static final String ASSET_ENTRY_PRIORITY = "asset-entry-priority";

	public static final String ATTACHED_CLASS_NAME = "attached-class-name";

	public static final String AVAILABLE_LOCALES = "available-locales";

	public static final String BUILD_NUMBER = "build-number";

	public static final String CLASS_NAME = "class-name";

	public static final String CLASS_PK = "class-pk";

	public static final String COMPANY_ID = "company-id";

	public static final String COMPANY_GROUP_ID = "company-group-id";

	public static final String DDM_DATA_PROVIDER_INSTANCE = "DDMDataProviderInstance";

	public static final String DDM_DATA_PROVIDER_INSTANCE_IDS = "ddm-data-provider-instance-ids";

	public static final String DDM_FORM_ADMIN_PORTLET_DATA_HANDLER = "DDMFormAdminPortletDataHandler";

	public static final String DDM_FORM_INSTANCE = "DDMFormInstance";

	public static final String DDM_FORM_LAYOUT_PATH = "ddm-form-layout-path";

	public static final String DDM_FORM_PATH = "ddm-form-path";

	public static final String DDM_STRUCTURE = "DDMStructure";

	public static final String EXPORT_DATE = "export-date";

	public static final String GROUP = "group";

	public static final String GROUP_ID = "group-id";

	public static final String GROUP_KEY = "group-key";

	public static final String HEADER = "header";

	public static final String LAYOUT_ID = "layout-id";

	public static final String LIVE_GROUP_ID = "live-group-id";

	public static final String MANIFEST_SUMMARY = "manifest-summary";

	public static final String MANIFEST_SUMMARY_KEY = "manifest-summary-key";

	public static final String MISSING = "missing";

	public static final String MISSING_REFERENCES = "missing-references";

	public static final String PATH = "path";

	public static final String PORTLET = "portlet";

	public static final String PORTLET_CONFIGURATION = "portlet-configuration";

	public static final String PORTLET_DATA = "portlet-data";

	public static final String PORTLET_ID = "portlet-id";

	public static final String PRELOADED = "preloaded";

	public static final String PRIVATE_LAYOUT = "private-layout";

	public static final String REFERENCE = "reference";

	public static final String REFERENCED_CLASS_NAME = "referenced-class-name";

	public static final String REFERENCES = "references";

	public static final String ROOT = "root";

	public static final String ROOT_PORTLET_ID = "root-portlet-id";

	public static final String SCHEMA_VERSION = "schema-version";

	public static final String SCOPE_GROUP_ID = "scope-group-id";

	public static final String SCOPE_LAYOUT_TYPE = "scope-layout-type";

	public static final String SCOPE_LAYOUT_UUID = "scope-layout-uuid";

	public static final String SELF_PATH = "self-path";

	public static final String SETTINGS_DDM_FORM_VALUES_PATH = "settings-ddm-form-values-path";

	public static final String STAGED_MODEL = "staged-model";

	public static final String STRUCTURE_KEY = "structure-key";

	public static final String TYPE = "type";

	public static final String USER_PERSONAL_SITE_GROUP_ID = "user-personal-site-group-id";

	public static final String USER_UUID = "user-uuid";

	public static final String UUID = "uuid";

	private FormExportKeys() {
	}
}
