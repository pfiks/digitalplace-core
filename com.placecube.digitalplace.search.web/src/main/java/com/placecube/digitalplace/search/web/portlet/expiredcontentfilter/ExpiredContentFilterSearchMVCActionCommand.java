package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.constants.ExpiredContentFilterSearchConstants;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.service.ExpiredContentFilterSearchService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.EXPIRED_CONTENT_FILTER_SEARCH, "mvc.command.name=" + MVCCommandKeys.SEARCH }, service = MVCActionCommand.class)
public class ExpiredContentFilterSearchMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private ExpiredContentFilterSearchService expiredContentFilterSearchService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		boolean includeExpiredContent = ParamUtil.getBoolean(actionRequest, "includeExpiredContent", false);

		sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, ExpiredContentFilterSearchConstants.SHARED_SEARCH_ATTRIBUTE_NAME, String.valueOf(includeExpiredContent));

		sharedSearchRequestService.sendRedirectFromParameter(actionRequest, actionResponse, "redirectURL");
	}
}
