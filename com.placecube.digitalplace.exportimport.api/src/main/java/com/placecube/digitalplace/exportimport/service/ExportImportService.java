package com.placecube.digitalplace.exportimport.service;

import com.liferay.portal.kernel.exception.PortalException;

public interface ExportImportService {

	void disableValidateLayoutReferences(long companyId) throws PortalException;

	void enableValidateLayoutReferences(long companyId) throws PortalException;

}
