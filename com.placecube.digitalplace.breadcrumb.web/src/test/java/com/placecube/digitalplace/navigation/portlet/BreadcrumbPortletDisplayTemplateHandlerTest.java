package com.placecube.digitalplace.navigation.portlet;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;
import com.placecube.digitalplace.navigation.constants.PortletKeys;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

public class BreadcrumbPortletDisplayTemplateHandlerTest {
	
	private static final String TEMPLATE_HANDLER_NAME = "Category Breadcrumbs";

	private BreadcrumbPortletDisplayTemplateHandler breadcrumbPortletDisplayTemplateHandler;

	@Mock
	private Locale mockLocale;

	@Before
	public void before() {
		breadcrumbPortletDisplayTemplateHandler = new BreadcrumbPortletDisplayTemplateHandler();
	}

	@Test
	public void getClassName_WhenNoError_ThenReturnsClassName() { 
		String expected = BreadcrumbEntry.class.getName();
		String result = breadcrumbPortletDisplayTemplateHandler.getClassName();

		assertEquals(expected, result);
	}

	@Test
	public void getName_WhenNoError_ThenReturnsResourceName() { 
		String result = breadcrumbPortletDisplayTemplateHandler.getName(mockLocale);

		assertEquals(TEMPLATE_HANDLER_NAME, result);
	}

	@Test
	public void getResourceName_WhenNoError_ThenReturnsResourceName() { 
		String expected = PortletKeys.BREADCRUMB;
		String result = breadcrumbPortletDisplayTemplateHandler.getResourceName();

		assertEquals(expected, result);
	}
}
