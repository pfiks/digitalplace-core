package com.placecube.digitalplace.appointment.office365.connector;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.appointment.booking.connector.AppointmentBookingConnector;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.office365.configuration.AppointmentOffice365CompanyConfiguration;
import com.placecube.digitalplace.appointment.office365.model.AppointmentConfiguredService;
import com.placecube.digitalplace.appointment.office365.service.OfficeAppointmentService;
import com.placecube.digitalplace.appointment.office365.service.OfficeRESTService;
import com.placecube.digitalplace.appointment.office365.service.ParsingUtils;

@Component(immediate = true, service = AppointmentBookingConnector.class)
public class OfficeAppointmentBookingConnector implements AppointmentBookingConnector {

	private static final Log LOG = LogFactoryUtil.getLog(OfficeAppointmentBookingConnector.class);

	@Reference
	private OfficeAppointmentService officeAppointmentService;

	@Reference
	private OfficeRESTService officeRESTService;

	@Reference
	private ParsingUtils parsingUtils;

	@Override
	public List<String> bookAppointment(long companyId, String serviceId, AppointmentBookingEntry appointmentEntry) {
		List<String> results = new ArrayList<>();
		try {
			Optional<AppointmentOffice365CompanyConfiguration> configuration = officeAppointmentService.getValidAppointmentOffice365CompanyConfiguration(companyId);
			if (configuration.isPresent()) {
				Set<AppointmentConfiguredService> appointmentConfiguredServices = officeAppointmentService.getAppointmentConfiguredServices(configuration.get(), serviceId);
				if (!appointmentConfiguredServices.isEmpty()) {
					String authToken = officeRESTService.getAuthenticationToken(configuration.get());

					for (AppointmentConfiguredService appointmentConfiguredService : appointmentConfiguredServices) {
						results.add(officeRESTService.createEvent(appointmentConfiguredService, appointmentEntry, authToken));
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Unable to get authentication token", e);
		}
		return results;
	}

	@Override
	public List<String> cancelAppointment(long companyId, String serviceId, String appointmentId) {
		List<String> results = new ArrayList<>();
		try {
			Optional<AppointmentOffice365CompanyConfiguration> configuration = officeAppointmentService.getValidAppointmentOffice365CompanyConfiguration(companyId);
			if (configuration.isPresent()) {
				Set<AppointmentConfiguredService> appointmentConfiguredServices = officeAppointmentService.getAppointmentConfiguredServices(configuration.get(), serviceId);
				if (!appointmentConfiguredServices.isEmpty()) {

					String authToken = officeRESTService.getAuthenticationToken(configuration.get());

					for (AppointmentConfiguredService appointmentConfiguredService : appointmentConfiguredServices) {
						results.add(officeRESTService.cancelEvent(appointmentConfiguredService, appointmentId, authToken));
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Unable to get authentication token", e);
		}
		return results;
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, AppointmentContext appointmentContext) {
		throw new UnsupportedOperationException("Unsupported method");
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, Date startDate, Date endDate, TimeZone timeZone) {
		List<AppointmentBookingEntry> results = new LinkedList<>();
		try {

			Optional<AppointmentOffice365CompanyConfiguration> configuration = officeAppointmentService.getValidAppointmentOffice365CompanyConfiguration(companyId);
			if (configuration.isPresent()) {
				Set<AppointmentConfiguredService> appointmentConfiguredServices = officeAppointmentService.getAppointmentConfiguredServices(configuration.get(), serviceId);
				if (!appointmentConfiguredServices.isEmpty()) {

					String authToken = officeRESTService.getAuthenticationToken(configuration.get());

					String formattedStartDateTime = parsingUtils.getFormattedDateTime(startDate, timeZone);
					String formattedEndDateTime = parsingUtils.getFormattedDateTime(endDate, timeZone);

					for (AppointmentConfiguredService appointmentConfiguredService : appointmentConfiguredServices) {
						results.addAll(officeRESTService.getCalendarEvents(appointmentConfiguredService, formattedStartDateTime, formattedEndDateTime, timeZone, authToken));
					}

				}
			}
		} catch (Exception e) {
			LOG.error("Unable to get authentication token", e);
		}
		return results;
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, String appointmentId) {
		List<AppointmentBookingEntry> results = new LinkedList<>();
		try {

			Optional<AppointmentOffice365CompanyConfiguration> configuration = officeAppointmentService.getValidAppointmentOffice365CompanyConfiguration(companyId);
			if (configuration.isPresent()) {
				Set<AppointmentConfiguredService> appointmentConfiguredServices = officeAppointmentService.getAppointmentConfiguredServices(configuration.get(), serviceId);
				if (!appointmentConfiguredServices.isEmpty()) {

					String authToken = officeRESTService.getAuthenticationToken(configuration.get());

					for (AppointmentConfiguredService appointmentConfiguredService : appointmentConfiguredServices) {
						Optional<AppointmentBookingEntry> appointment = officeRESTService.getEvent(appointmentConfiguredService, appointmentId, authToken);
						if (appointment.isPresent()) {
							results.add(appointment.get());
						}
					}

				}
			}
		} catch (Exception e) {
			LOG.error("Unable to get authentication token", e);
		}
		return results;
	}

	@Override
	public List<AppointmentBookingSlot> getAppointmentSlots(long companyId, String serviceId, AppointmentContext appointmentContext) {
		throw new UnsupportedOperationException("Unsupported method");
	}

	@Override
	public List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int numberOfDatesToReturn, int dayOfWeek, TimeZone timeZone,
			int onlyDatesWithLessThanXappoinments) {
		List<Date> results = new LinkedList<>();

		try {
			Optional<AppointmentOffice365CompanyConfiguration> configuration = officeAppointmentService.getValidAppointmentOffice365CompanyConfiguration(companyId);
			if (configuration.isPresent()) {
				Set<AppointmentConfiguredService> appointmentConfiguredServices = officeAppointmentService.getAppointmentConfiguredServices(configuration.get(), serviceId);
				if (!appointmentConfiguredServices.isEmpty()) {

					String authToken = officeRESTService.getAuthenticationToken(configuration.get());

					DayOfWeek dayOrWeekToMatch = DayOfWeek.of(dayOfWeek);
					ZoneId zoneId = ZoneId.of(timeZone.getID());

					for (AppointmentConfiguredService appointmentConfiguredService : appointmentConfiguredServices) {
						results.addAll(
								getNextDatesForServiceCalendarToResults(authToken, numberOfDatesToReturn, onlyDatesWithLessThanXappoinments, dayOrWeekToMatch, zoneId, appointmentConfiguredService));
					}

				}
			}
		} catch (Exception e) {
			LOG.error("Unable to get authentication token", e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning available dates for serviceId: " + serviceId + ", for dayOfWeek: " + dayOfWeek + " - datesFound: " + results.size() + " -- ");
			for (Date date : results) {
				LOG.debug(date);
			}
		}

		return results;
	}

	@Override
	public List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int dayOfWeek, TimeZone timeZone) {
		List<Date> results = new LinkedList<>();

		try {
			Optional<AppointmentOffice365CompanyConfiguration> configuration = officeAppointmentService.getValidAppointmentOffice365CompanyConfiguration(companyId);
			if (configuration.isPresent()) {
				Set<AppointmentConfiguredService> appointmentConfiguredServices = officeAppointmentService.getAppointmentConfiguredServices(configuration.get(), serviceId);
				if (!appointmentConfiguredServices.isEmpty()) {

					String authToken = officeRESTService.getAuthenticationToken(configuration.get());

					DayOfWeek dayOrWeekToMatch = DayOfWeek.of(dayOfWeek);
					ZoneId zoneId = ZoneId.of(timeZone.getID());

					for (AppointmentConfiguredService appointmentConfiguredService : appointmentConfiguredServices) {
						results.addAll(getNextDatesForServiceCalendarToResults(authToken, appointmentConfiguredService.getNumberOfDatesToReturn(),
								appointmentConfiguredService.getOnlyDatesWithLessThanXappoinments(), dayOrWeekToMatch, zoneId, appointmentConfiguredService));
					}

				}
			}
		} catch (Exception e) {
			LOG.error("Unable to get authentication token", e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Returning available dates for serviceId: " + serviceId + ", for dayOfWeek: " + dayOfWeek + " - datesFound: " + results.size() + " -- ");
			for (Date date : results) {
				LOG.debug(date);
			}
		}

		return results;
	}

	@Override
	public boolean isEnabled(long companyId) {
		Optional<AppointmentOffice365CompanyConfiguration> configuration = officeAppointmentService.getAppointmentOffice365CompanyConfiguration(companyId);

		return configuration.isPresent() && configuration.get().enabled();
	}

	/*
	 * This method retrieves the next X dates that have less than Y
	 * appointments. The updated logic improves performances by reducing the
	 * amount of calls done to Office365 and just parsing the responses and
	 * working with dates.
	 *
	 * 1. Get the next X dates that we would expect
	 *
	 * 2. Execute 1 call for appointments with start=tomorrow, end=tomorrow + X
	 * weeks
	 *
	 * 3. Parse only the start date from the appointments retrieved
	 *
	 * 4. If the appointment date is in the list of dates from step1 and there
	 * are no more than Y appointments on that date, then the date is valid,
	 * otherwise it is removed from the list at point 1.
	 *
	 * 5. If the valid dates found from this call are less than X, execute
	 * another call from step 1
	 */
	private List<Date> getNextDatesForServiceCalendarToResults(String authToken, int numberOfDatesToReturn, int onlyDatesWithLessThanXappoinments, DayOfWeek dayOrWeekToMatch, ZoneId zoneId,
			AppointmentConfiguredService appointmentConfiguredService) {
		List<Date> results = new LinkedList<>();
		int datesToRetrieve = numberOfDatesToReturn * 2;

		LocalDate startDate = LocalDate.now(zoneId).with(TemporalAdjusters.next(dayOrWeekToMatch));
		String formattedStartDateTime = parsingUtils.getFormattedDateTimeAtStartOfDay(startDate);

		LocalDate endDate = startDate.plusWeeks(datesToRetrieve);
		String formattedEndDateTime = parsingUtils.getFormattedDateTimeAtEndOfDay(endDate);

		while (results.size() < numberOfDatesToReturn) {

			datesToRetrieve = datesToRetrieve - results.size();

			List<LocalDateTime> availableDatesToMatch = parsingUtils.getDatesWithDayOfWeekTruncatedToDays(startDate, datesToRetrieve);

			List<LocalDateTime> validDatesFound = officeRESTService.getDatesWithAvailableAppointmentsForMatchingDates(appointmentConfiguredService, formattedStartDateTime, formattedEndDateTime,
					availableDatesToMatch, onlyDatesWithLessThanXappoinments, authToken);

			LOG.debug("Checking calendar dates with less than " + onlyDatesWithLessThanXappoinments + " appointments - startDate: " + startDate + ", endDate: " + endDate + ", datesFound: "
					+ validDatesFound.size());
			results.addAll(parsingUtils.getDatesFromLocalDateTimes(validDatesFound));

			startDate = startDate.plusWeeks(datesToRetrieve);
			formattedStartDateTime = parsingUtils.getFormattedDateTimeAtStartOfDay(startDate);

			endDate = startDate.plusWeeks(datesToRetrieve);
			formattedEndDateTime = parsingUtils.getFormattedDateTimeAtEndOfDay(endDate);
		}

		return results.subList(0, numberOfDatesToReturn);

	}
}
