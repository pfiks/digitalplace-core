/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link SimpleAddressLookupEntry}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SimpleAddressLookupEntry
 * @generated
 */
public class SimpleAddressLookupEntryWrapper
	extends BaseModelWrapper<SimpleAddressLookupEntry>
	implements ModelWrapper<SimpleAddressLookupEntry>,
			   SimpleAddressLookupEntry {

	public SimpleAddressLookupEntryWrapper(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		super(simpleAddressLookupEntry);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("companyId", getCompanyId());
		attributes.put("uprn", getUprn());
		attributes.put("addressLine1", getAddressLine1());
		attributes.put("addressLine2", getAddressLine2());
		attributes.put("addressLine3", getAddressLine3());
		attributes.put("city", getCity());
		attributes.put("postcode", getPostcode());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String uprn = (String)attributes.get("uprn");

		if (uprn != null) {
			setUprn(uprn);
		}

		String addressLine1 = (String)attributes.get("addressLine1");

		if (addressLine1 != null) {
			setAddressLine1(addressLine1);
		}

		String addressLine2 = (String)attributes.get("addressLine2");

		if (addressLine2 != null) {
			setAddressLine2(addressLine2);
		}

		String addressLine3 = (String)attributes.get("addressLine3");

		if (addressLine3 != null) {
			setAddressLine3(addressLine3);
		}

		String city = (String)attributes.get("city");

		if (city != null) {
			setCity(city);
		}

		String postcode = (String)attributes.get("postcode");

		if (postcode != null) {
			setPostcode(postcode);
		}
	}

	@Override
	public SimpleAddressLookupEntry cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the address line1 of this simple address lookup entry.
	 *
	 * @return the address line1 of this simple address lookup entry
	 */
	@Override
	public String getAddressLine1() {
		return model.getAddressLine1();
	}

	/**
	 * Returns the address line2 of this simple address lookup entry.
	 *
	 * @return the address line2 of this simple address lookup entry
	 */
	@Override
	public String getAddressLine2() {
		return model.getAddressLine2();
	}

	/**
	 * Returns the address line3 of this simple address lookup entry.
	 *
	 * @return the address line3 of this simple address lookup entry
	 */
	@Override
	public String getAddressLine3() {
		return model.getAddressLine3();
	}

	/**
	 * Returns the city of this simple address lookup entry.
	 *
	 * @return the city of this simple address lookup entry
	 */
	@Override
	public String getCity() {
		return model.getCity();
	}

	/**
	 * Returns the company ID of this simple address lookup entry.
	 *
	 * @return the company ID of this simple address lookup entry
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the postcode of this simple address lookup entry.
	 *
	 * @return the postcode of this simple address lookup entry
	 */
	@Override
	public String getPostcode() {
		return model.getPostcode();
	}

	/**
	 * Returns the primary key of this simple address lookup entry.
	 *
	 * @return the primary key of this simple address lookup entry
	 */
	@Override
	public com.placecube.digitalplace.addresslookup.simple.service.persistence.
		SimpleAddressLookupEntryPK getPrimaryKey() {

		return model.getPrimaryKey();
	}

	/**
	 * Returns the uprn of this simple address lookup entry.
	 *
	 * @return the uprn of this simple address lookup entry
	 */
	@Override
	public String getUprn() {
		return model.getUprn();
	}

	/**
	 * Returns the uuid of this simple address lookup entry.
	 *
	 * @return the uuid of this simple address lookup entry
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the address line1 of this simple address lookup entry.
	 *
	 * @param addressLine1 the address line1 of this simple address lookup entry
	 */
	@Override
	public void setAddressLine1(String addressLine1) {
		model.setAddressLine1(addressLine1);
	}

	/**
	 * Sets the address line2 of this simple address lookup entry.
	 *
	 * @param addressLine2 the address line2 of this simple address lookup entry
	 */
	@Override
	public void setAddressLine2(String addressLine2) {
		model.setAddressLine2(addressLine2);
	}

	/**
	 * Sets the address line3 of this simple address lookup entry.
	 *
	 * @param addressLine3 the address line3 of this simple address lookup entry
	 */
	@Override
	public void setAddressLine3(String addressLine3) {
		model.setAddressLine3(addressLine3);
	}

	/**
	 * Sets the city of this simple address lookup entry.
	 *
	 * @param city the city of this simple address lookup entry
	 */
	@Override
	public void setCity(String city) {
		model.setCity(city);
	}

	/**
	 * Sets the company ID of this simple address lookup entry.
	 *
	 * @param companyId the company ID of this simple address lookup entry
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the postcode of this simple address lookup entry.
	 *
	 * @param postcode the postcode of this simple address lookup entry
	 */
	@Override
	public void setPostcode(String postcode) {
		model.setPostcode(postcode);
	}

	/**
	 * Sets the primary key of this simple address lookup entry.
	 *
	 * @param primaryKey the primary key of this simple address lookup entry
	 */
	@Override
	public void setPrimaryKey(
		com.placecube.digitalplace.addresslookup.simple.service.persistence.
			SimpleAddressLookupEntryPK primaryKey) {

		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the uprn of this simple address lookup entry.
	 *
	 * @param uprn the uprn of this simple address lookup entry
	 */
	@Override
	public void setUprn(String uprn) {
		model.setUprn(uprn);
	}

	/**
	 * Sets the uuid of this simple address lookup entry.
	 *
	 * @param uuid the uuid of this simple address lookup entry
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	protected SimpleAddressLookupEntryWrapper wrap(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		return new SimpleAddressLookupEntryWrapper(simpleAddressLookupEntry);
	}

}