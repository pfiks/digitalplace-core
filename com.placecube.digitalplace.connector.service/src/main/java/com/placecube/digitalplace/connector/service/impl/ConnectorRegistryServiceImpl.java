package com.placecube.digitalplace.connector.service.impl;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.connector.constants.ConnectorConstants;
import com.placecube.digitalplace.connector.model.Connector;
import com.placecube.digitalplace.connector.model.ConnectorRegistryContext;
import com.placecube.digitalplace.connector.service.ConnectorRegistryService;
import com.placecube.digitalplace.connector.service.impl.util.ConnectorDisplayPriorityComparator;
import com.placecube.digitalplace.connector.service.impl.util.ConnectorRegistryUtil;

@Component(immediate = true, service = ConnectorRegistryService.class)
public class ConnectorRegistryServiceImpl implements ConnectorRegistryService {

	@Reference
	private ConnectorRegistryUtil connectorRegistryUtil;

	private ServiceTrackerMap<String, List<ServiceWrapper<Connector>>> interfaceMapConnectors;

	@Override
	public ConnectorRegistryContext createConnectorRegistryContext(String connectorType, String connectorClassName) {
		ConnectorRegistryContext connectorRegistryContext = new ConnectorRegistryContext();
		connectorRegistryContext.setConnectorType(connectorType);
		connectorRegistryContext.setConnectorClassName(connectorClassName);

		return connectorRegistryContext;
	}

	@Override
	public Optional<ServiceWrapper<Connector>> getConnector(ConnectorRegistryContext connectorRegistryContext) {

		List<ServiceWrapper<Connector>> connectors = interfaceMapConnectors.getService(connectorRegistryContext.getConnectorType());

		if (connectors == null) {
			return Optional.empty();
		}

		return connectors.stream().filter(c -> c.getService().getClass().getName().equals(connectorRegistryContext.getConnectorClassName())).findFirst();
	}

	@Override
	public Map<String, String> getConnectorNames(String interfaceClassName, Locale locale) {
		List<ServiceWrapper<Connector>> connectors = getConnectors(interfaceClassName);

		return connectors.stream().collect(Collectors.toMap(this::getConnectorClassName, x -> getConnectorName(x, locale), (u, v) -> {
			throw new IllegalStateException(String.format("Duplicate key %s", u));
		}, LinkedHashMap::new));
	}

	@Override
	public List<ServiceWrapper<Connector>> getConnectors(String interfaceClassName) {
		List<ServiceWrapper<Connector>> connectors = interfaceMapConnectors.getService(interfaceClassName);

		if (connectors == null) {
			return Collections.emptyList();
		} else {
			List<ServiceWrapper<Connector>> modifiableConnectorsList = ListUtil.toList(connectors);
			Collections.sort(modifiableConnectorsList, new ConnectorDisplayPriorityComparator());
			return modifiableConnectorsList;
		}

	}

	@Activate
	protected void activate(BundleContext bundleContext) {
		interfaceMapConnectors = ServiceTrackerMapFactory.openMultiValueMap(bundleContext, Connector.class, null, connectorRegistryUtil.getInterfaceServiceReferenceMapper(),
				ServiceTrackerCustomizerFactory.<Connector>serviceWrapper(bundleContext));
	}

	@Deactivate
	protected void deactivate() {
		interfaceMapConnectors.close();
	}

	private String getConnectorClassName(ServiceWrapper<Connector> connectorService) {
		return connectorService.getService().getClass().getName();
	}

	private String getConnectorName(ServiceWrapper<Connector> connectorServiceWrapper, Locale locale) {
		String connectorName = (String) connectorServiceWrapper.getProperties().get(ConnectorConstants.CONNECTOR_NAME);

		Class<?> connectorClass = connectorServiceWrapper.getService().getClass();

		if (Validator.isNull(connectorName)) {
			connectorName = connectorClass.getName();
		}

		ResourceBundle resourceBundle = ResourceBundleUtil.getBundle("content/Language", locale, connectorClass);

		return LanguageUtil.get(resourceBundle, connectorName);
	}
}
