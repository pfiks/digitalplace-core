package com.placecube.digitalplace.twitterfeed.constants;

public final class PortletRequestKeys {

	public static final String CONFIGURATION = "configuration";

	public static final String FEEDS_TO_SHOW = "feedsToShow";

	public static final String TWITTER_USERNAME = "twitterUsername";

	public static final String TWITTER_EMBEDDED = "twitterEmbedded";

	private PortletRequestKeys() {
	}
}
