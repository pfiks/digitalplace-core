package com.placecube.digitalplace.notifications.exception;

import com.liferay.portal.kernel.exception.PortalException;

public class NotificationsException extends PortalException {

	private static final long serialVersionUID = 1L;

	public NotificationsException() {
		super();
	}

	public NotificationsException(String msg) {
		super(msg);
	}

	public NotificationsException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public NotificationsException(Throwable cause) {
		super(cause);
	}
}
