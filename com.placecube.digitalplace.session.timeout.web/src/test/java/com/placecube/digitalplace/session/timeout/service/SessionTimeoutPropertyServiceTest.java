package com.placecube.digitalplace.session.timeout.service;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude.DynamicIncludeRegistry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.session.timeout.dynamicinclude.SessionTimeoutDynamicInclude;
import com.placecube.digitalplace.session.timeout.web.config.SessionTimeoutCompanyConfiguration;
import com.placecube.digitalplace.session.timeout.web.service.SessionTimeoutPropertyService;

public class SessionTimeoutPropertyServiceTest extends PowerMockito {

	@InjectMocks
	private SessionTimeoutDynamicInclude sessionTimeoutDynamicInclude;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private DynamicIncludeRegistry mockDynamicIncludeRegistry;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private RequestDispatcher mockRequestDispatcher;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private SessionTimeoutCompanyConfiguration mockSessionTimeoutCompanyConfiguration;

	@Mock
	private SessionTimeoutPropertyService mockSessionTimeoutPropertyService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void include_WhenConfigurationIsThrown_ThenNothingIsAddedToTheRequest() throws IOException, ConfigurationException {
		long companyId = 1234l;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(SessionTimeoutCompanyConfiguration.class, companyId)).thenThrow(ConfigurationException.class);
		when(mockServletContext.getRequestDispatcher(ArgumentMatchers.anyString())).thenReturn(mockRequestDispatcher);

		sessionTimeoutDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, never()).setAttribute(ArgumentMatchers.anyString(), ArgumentMatchers.any());
	}

	@Test
	public void include_WhenNoError_ThenPopulateRequestWithCustomSessionLength() throws Exception {
		long companyId = 123;
		int customTimeout = 3;

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(SessionTimeoutCompanyConfiguration.class, companyId)).thenReturn(mockSessionTimeoutCompanyConfiguration);
		when(mockSessionTimeoutCompanyConfiguration.timeoutInMinutes()).thenReturn(customTimeout);
		when(mockSessionTimeoutPropertyService.getValidTimeout(customTimeout)).thenReturn(customTimeout);
		when(mockServletContext.getRequestDispatcher(ArgumentMatchers.anyString())).thenReturn(mockRequestDispatcher);

		sessionTimeoutDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, times(1)).setAttribute("customSessionLengthInMins", customTimeout);
	}

	@Test
	public void include_WhenNoError_ThenPopulateRequestWithIsSignedIn() throws Exception {
		long companyId = 123;
		boolean isSignedIn = true;

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(SessionTimeoutCompanyConfiguration.class, companyId)).thenReturn(mockSessionTimeoutCompanyConfiguration);
		when(mockThemeDisplay.isSignedIn()).thenReturn(isSignedIn);
		when(mockServletContext.getRequestDispatcher(ArgumentMatchers.anyString())).thenReturn(mockRequestDispatcher);

		sessionTimeoutDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, times(1)).setAttribute("isSignedIn", isSignedIn);
	}

	@Test
	public void include_WhenNoError_ThenPopulateRequestWithSystemSessionLength() throws Exception {
		long companyId = 123;
		int systemTimeout = 15;

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(SessionTimeoutCompanyConfiguration.class, companyId)).thenReturn(mockSessionTimeoutCompanyConfiguration);
		when(mockSessionTimeoutPropertyService.getSystemConfiguredTimeout()).thenReturn(systemTimeout);
		when(mockServletContext.getRequestDispatcher(ArgumentMatchers.anyString())).thenReturn(mockRequestDispatcher);

		sessionTimeoutDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, times(1)).setAttribute("systemTimeout", systemTimeout);
	}

	@Test
	public void include_WhenThemeDisplayIsNull_ThenNothingIsAddedToTheRequest() throws IOException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);
		when(mockServletContext.getRequestDispatcher(ArgumentMatchers.anyString())).thenReturn(mockRequestDispatcher);

		sessionTimeoutDynamicInclude.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, never()).setAttribute(ArgumentMatchers.anyString(), ArgumentMatchers.any());
	}

	@Test
	public void register_WhenNoError_ThenRegistersBottomPostInclude() {
		sessionTimeoutDynamicInclude.register(mockDynamicIncludeRegistry);

		verify(mockDynamicIncludeRegistry, times(1)).register("/html/common/themes/bottom.jsp#post");
	}

}
