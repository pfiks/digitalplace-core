package com.placecube.digitalplace.address.service.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.configuration.AddressTypeCompanyConfiguration;
import com.placecube.digitalplace.address.constants.AddressType;
import com.placecube.digitalplace.address.service.AddressTypeConfigurationService;

@Component(immediate = true, service = AddressTypeConfigurationService.class)
public class AddressTypeConfigurationServiceImpl implements AddressTypeConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	private static final Log LOG = LogFactoryUtil.getLog(AddressTypeConfigurationServiceImpl.class);

	@Override
	public AddressType getDefaultAddressType(long companyId) {
		try {
			AddressTypeCompanyConfiguration companyConfiguration = configurationProvider.getCompanyConfiguration(AddressTypeCompanyConfiguration.class, companyId);

			String defaultAddressTypeName = companyConfiguration.defaultAddressType();

			return Validator.isNull(defaultAddressTypeName) ? AddressType.PERSONAL : AddressType.getAddressTypeByName(defaultAddressTypeName).orElse(AddressType.PERSONAL);

		} catch (ConfigurationException e) {
			LOG.error(e);
		}

		return AddressType.PERSONAL;
	}

}
