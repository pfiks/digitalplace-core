package com.placecube.digitalplace.formadmin.web.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstanceLink;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceLinkLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.exportimport.controller.PortletExportController;
import com.liferay.exportimport.kernel.lar.ExportImportHelper;
import com.liferay.exportimport.kernel.lar.ExportImportPathUtil;
import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.exportimport.kernel.lar.PortletDataContextFactory;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.service.PortletLocalService;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.kernel.zip.ZipWriter;

@Component(immediate = true, service = FormExportService.class)
public class FormExportService {

	@Reference
	private DDMDataProviderInstanceLinkLocalService ddmDataProviderInstanceLinkLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private ExportImportHelper exportImportHelper;

	@Reference
	private FormExportServiceUtil formExportServiceUtil;

	@Reference
	private PortletDataContextFactory portletDataContextFactory;

	@Reference
	private PortletExportController portletExportController;

	@Reference
	private PortletLocalService portletLocalService;

	public File exportForm(DDMFormInstance ddmFormInstance) throws Exception {

		DDMStructure ddmStructure = ddmStructureLocalService.getStructure(ddmFormInstance.getStructureId());
		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = ddmDataProviderInstanceLinkLocalService.getDataProviderInstanceLinks(ddmFormInstance.getStructureId());
		long groupId = ddmFormInstance.getGroupId();
		long companyId = ddmFormInstance.getCompanyId();

		String portletId = "com_liferay_dynamic_data_mapping_form_web_portlet_DDMFormAdminPortlet";

		Portlet portlet = portletLocalService.getPortletById(companyId, portletId);

		ZipWriter zipWriter = exportImportHelper.getPortletZipWriter(portletId);

		PortletDataContext portletDataContext = portletDataContextFactory.createExportPortletDataContext(companyId, groupId, new HashMap<>(), null, null, zipWriter);
		portletDataContext.setPortletId(portletId);

		formExportServiceUtil.createManifestXML(portlet, portletDataContext, groupId, companyId, portletId, ddmDataProviderInstanceLinkList.size());
		formExportServiceUtil.createPortletXML(portlet, groupId, portletDataContext);
		formExportServiceUtil.createPortletDataXml(portlet, portletDataContext, groupId, ddmFormInstance, ddmStructure, ddmDataProviderInstanceLinkList);
		formExportServiceUtil.createDDMDataProviderInstanceXML(ddmDataProviderInstanceLinkList, portletDataContext);
		formExportServiceUtil.createDDMFormLayoutJSON(portletDataContext, ddmStructure);
		formExportServiceUtil.createDDMFormJSON(portletDataContext, ddmStructure);
		formExportServiceUtil.createDDMFormInstanceXML(portletDataContext, ddmFormInstance);
		formExportServiceUtil.createDDMStructureXML(portletDataContext, ddmStructure);
		formExportServiceUtil.createSettingsDDMFormValuesJSON(portletDataContext, ddmFormInstance);

		portletExportController.exportAssetLinks(portletDataContext);
		portletExportController.exportLocks(portletDataContext);
		exportDeletionSystemEvents(portletDataContext);

		return zipWriter.getFile();
	}

	public FileInputStream getFileInputStream(File file) throws FileNotFoundException {
		return new FileInputStream(file);
	}

	private void exportDeletionSystemEvents(PortletDataContext portletDataContext) throws IOException {
		Document document = SAXReaderUtil.createDocument();

		document.addElement("deletion-system-events");

		portletDataContext.addZipEntry(ExportImportPathUtil.getRootPath(portletDataContext) + "/deletion-system-events.xml", document.formattedString());
	}
}