package com.placecube.digitalplace.addresslookup.statmap.service.utils;

import java.util.LinkedHashSet;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.address.model.AddressContext;

@Component(immediate = true, service = StatMapAddressLookupService.class)
public class StatMapAddressLookupService {

	private static final Log LOG = LogFactoryUtil.getLog(StatMapAddressLookupService.class);

	@Reference
	private AddressRetrievalService addressRetrievalService;

	public Set<AddressContext> getAddressContexts(String searchUrl, String recordUrl, String token, String searchParameterName, String searchValue) {
		Set<AddressContext> addressContextsResults = new LinkedHashSet<>();

		try {
			String searchURL = addressRetrievalService.getSearchURL(searchUrl, token, searchParameterName, searchValue);
			String searchResponse = addressRetrievalService.executeCall(searchURL);

			Set<String> addressIds = addressRetrievalService.getAddressIdsFromResponse(searchResponse);

			for (String addressId : addressIds) {
				addAddressToResults(addressContextsResults, recordUrl, token, addressId);
			}

		} catch (Exception e) {
			LOG.error(e);
		}

		return addressContextsResults;
	}

	private void addAddressToResults(Set<AddressContext> addressContextsResults, String recordUrl, String token, String addressId) {
		try {
			String recordURL = addressRetrievalService.getRecordURL(recordUrl, token, addressId);
			String response = addressRetrievalService.executeCall(recordURL);

			addressContextsResults.add(addressRetrievalService.getAddressContextFromResponse(response));
		} catch (Exception e) {
			LOG.error("Unable to retrieve address with id: " + addressId, e);
		}
	}
}
