/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Clob;
import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_Appointment_AppointmentEntry&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentEntry
 * @generated
 */
public class AppointmentEntryTable extends BaseTable<AppointmentEntryTable> {

	public static final AppointmentEntryTable INSTANCE =
		new AppointmentEntryTable();

	public final Column<AppointmentEntryTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<AppointmentEntryTable, Long> appointmentEntryId =
		createColumn(
			"appointmentEntryId", Long.class, Types.BIGINT,
			Column.FLAG_PRIMARY);
	public final Column<AppointmentEntryTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<AppointmentEntryTable, Long> entryClassPK =
		createColumn(
			"entryClassPK", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<AppointmentEntryTable, Long> entryClassNameId =
		createColumn(
			"entryClassNameId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<AppointmentEntryTable, Clob> appointmentId =
		createColumn(
			"appointmentId", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<AppointmentEntryTable, String> serviceId = createColumn(
		"serviceId", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<AppointmentEntryTable, String> connectorClassName =
		createColumn(
			"connectorClassName", String.class, Types.VARCHAR,
			Column.FLAG_DEFAULT);
	public final Column<AppointmentEntryTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<AppointmentEntryTable, Date> appointmentStartDate =
		createColumn(
			"appointmentStartDate", Date.class, Types.TIMESTAMP,
			Column.FLAG_DEFAULT);
	public final Column<AppointmentEntryTable, Date> appointmentEndDate =
		createColumn(
			"appointmentEndDate", Date.class, Types.TIMESTAMP,
			Column.FLAG_DEFAULT);

	private AppointmentEntryTable() {
		super(
			"Placecube_Appointment_AppointmentEntry",
			AppointmentEntryTable::new);
	}

}