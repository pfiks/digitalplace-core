package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.service;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.constants.JsonFilterConfigKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model.DDLFilterGroup;

@Component(immediate = true, service = DDLFieldFilterGroupService.class)
public class DDLFieldFilterGroupService {

	public List<DDLFilterGroup> getFilterGroups(JSONArray groupsConfig) {
		List<DDLFilterGroup> ddlFilterGroups = new ArrayList<>();

		for (int i = 0; i < groupsConfig.length(); i++) {
			ddlFilterGroups.add(getFilterGroup(groupsConfig.getJSONObject(i)));
		}

		return ddlFilterGroups;
	}

	public DDLFilterGroup getFilterGroup(JSONObject groupConfig) {
		DDLFilterGroup ddlFilterGroup = DDLFilterGroup.newInstance(groupConfig.getString("filterGroupName"));
		JSONArray options = groupConfig.getJSONArray("options");

		for (int i = 0; i < options.length(); i++) {
			JSONObject option = options.getJSONObject(i);
			ddlFilterGroup.addOption(option.getString(JsonFilterConfigKeys.FILTER_OPTION_NAME));
		}

		return ddlFilterGroup;
	}

}
