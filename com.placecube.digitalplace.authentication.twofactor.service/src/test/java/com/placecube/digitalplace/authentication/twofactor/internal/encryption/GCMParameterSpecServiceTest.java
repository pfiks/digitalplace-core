package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.crypto.spec.GCMParameterSpec;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

public class GCMParameterSpecServiceTest extends PowerMockito {

	private static final String UUID = "237e8c17-6f94-56c5-f15a-fa46ed5f53f3";

	@InjectMocks
	private GCMParameterSpecService gcmParameterSpecService;

	private byte[] gcmParamterBytes = new byte[GCMParameterSpecService.IV_LENGTH + GCMParameterSpecService.IV_UUID_PADDING];

	private byte[] gcmParamterBytesWrongLength = new byte[GCMParameterSpecService.IV_LENGTH + GCMParameterSpecService.IV_UUID_PADDING + 1];

	private byte[] ivBytes = new byte[GCMParameterSpecService.IV_LENGTH];

	@Mock
	private ByteArrayOutputStream mockByteArrayOutputStream;
	@Mock
	private GCMParameterSpec mockGCMParameterSpec;
	@Mock
	private ModelFactoryBuilder mockModelFactoryBuilder;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = IOException.class)
	public void getGCMParameterSpec_WhenErrorOnCombiningKeys_ThenThrowsIOException() throws IOException {
		mockByteArrayOutputStream();
		doThrow(new IOException()).when(mockByteArrayOutputStream).write(any());

		gcmParameterSpecService.getGCMParameterSpec(UUID);
	}

	@Test(expected = IOException.class)
	public void getGCMParameterSpec_WhenErrorOnReadingBytesFromPath_ThenThrowsIOException() throws IOException {
		when(mockModelFactoryBuilder.getBytesFromFile(GCMParameterSpecService.TWO_FACTOR_IV_FILE_PATH)).thenThrow(new IOException());

		gcmParameterSpecService.getGCMParameterSpec(UUID);
	}

	@Test(expected = IllegalStateException.class)
	public void getGCMParameterSpec_WhenIVHasWrongLength_ThenThrowsIllegalStateException() throws IOException {
		mockByteArrayOutputStream();
		when(mockByteArrayOutputStream.toByteArray()).thenReturn(gcmParamterBytesWrongLength);

		gcmParameterSpecService.getGCMParameterSpec(UUID);
	}

	@Test
	public void getGCMParameterSpec_WhenNoError_ThenCombinesIVAndLastCharactersOfUUID() throws IOException {
		mockByteArrayOutputStream();
		when(mockByteArrayOutputStream.toByteArray()).thenReturn(gcmParamterBytes);

		gcmParameterSpecService.getGCMParameterSpec(UUID);

		InOrder inOrder = Mockito.inOrder(mockByteArrayOutputStream);
		inOrder.verify(mockByteArrayOutputStream, times(1)).write(ivBytes);
		inOrder.verify(mockByteArrayOutputStream, times(1)).write(UUID.substring(UUID.length() - GCMParameterSpecService.IV_UUID_PADDING).getBytes());
	}

	@Test
	public void getGCMParameterSpec_WhenNoError_ThenReturnsGCMParameterSpec() throws IOException {
		mockByteArrayOutputStream();
		when(mockByteArrayOutputStream.toByteArray()).thenReturn(gcmParamterBytes);
		when(mockModelFactoryBuilder.getGCMParameterSpecInstance((GCMParameterSpecService.IV_LENGTH + GCMParameterSpecService.IV_UUID_PADDING) * 8, mockByteArrayOutputStream.toByteArray()))
				.thenReturn(mockGCMParameterSpec);

		GCMParameterSpec result = gcmParameterSpecService.getGCMParameterSpec(UUID);

		assertThat(result, sameInstance(mockGCMParameterSpec));
	}

	private void mockByteArrayOutputStream() throws IOException {
		when(mockModelFactoryBuilder.getBytesFromFile(GCMParameterSpecService.TWO_FACTOR_IV_FILE_PATH)).thenReturn(ivBytes);
		when(mockModelFactoryBuilder.getByteArrayOutputStreamInstance()).thenReturn(mockByteArrayOutputStream);
	}

}
