package com.placecube.digitalplace.navigation.constants;

public final class ParamKeys {

	public static final String CATEGORY_NAV_FILTER_VOCABS = "categoryNavFilterVocabs";

	public static final String GLOBAL_GROUP_ID = "globalGroupId";

	private ParamKeys() {
	}

}
