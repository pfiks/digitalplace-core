package com.placecube.digitalplace.user.settings.web.internal.action;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.struts.StrutsAction;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.layout.service.LayoutRetrievalService;
import com.placecube.digitalplace.user.settings.web.constants.PortletKeys;

@Component(immediate = true, property = { "path=/my-settings", }, service = StrutsAction.class)
public class MySettingsAction implements StrutsAction {

	private static final Log LOG = LogFactoryUtil.getLog(MySettingsAction.class);

	@Reference
	private LayoutRetrievalService layoutRetrievalService;

	@Reference
	private Portal portal;

	@Reference
	private PortletURLFactory portletURLFactory;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			User user = portal.getUser(request);

			if (Validator.isNotNull(user)) {
				if (!user.isGuestUser()) {
					Optional<Layout> settingsLayout = layoutRetrievalService.getLayoutWithPortletId(user.getGroupId(), PortletKeys.USER_SETTINGS, true);

					if (settingsLayout.isPresent()) {
						LiferayPortletURL viewSettingsURL = portletURLFactory.create(request, PortletKeys.USER_SETTINGS, settingsLayout.get(), PortletRequest.RENDER_PHASE);
						String redirect = viewSettingsURL.toString();
						LOG.debug("MySettingsAction - redirecting user to: " + redirect);
						response.sendRedirect(redirect);
					} else {
						LOG.warn("MySettingsAction - MySettings layout was not found");
					}
				} else {
					LOG.warn("MySettingsAction - Not performing any action for a default user");
				}

			} else {

				String redirect = portal.getPathMain() + "/portal/login?redirect=" + URLEncoder.encode(portal.getCurrentURL(request), StandardCharsets.UTF_8.name());
				LOG.debug("MySettingsAction - redirecting guest user to: " + redirect);
				response.sendRedirect(redirect);

			}
		} catch (Exception e) {
			LOG.warn("Exception executing redirect to my settings page: " + e.getMessage());
			LOG.debug(e);
		}
		return null;
	}
}
