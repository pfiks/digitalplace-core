package com.placecube.digitalplace.payment.service;

import java.util.Optional;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.AgreementStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;

public interface PaymentConnector {

	String PAYMENT_CONNECTOR_KEY = "payment.connector.key";

	default boolean cancelAgreement(String agreementId, ServiceContext serviceContext) {
		throw new UnsupportedOperationException();
	}

	default AgreementStatus createAgreement(PaymentContext paymentContext, ServiceContext serviceContext) {
		throw new UnsupportedOperationException();
	}

	default Optional<String> getDefaultAdvancedConfiguration(long companyId) {
		return Optional.empty();
	}

	boolean enabled(long companyId);

	default AgreementStatus getAgreementStatus(String agreementId, ServiceContext serviceContext) {
		throw new UnsupportedOperationException();
	}

	Optional<String> getBackURL(String paymentReference, ServiceContext serviceContext);

	PaymentStatus getPaymentStatus(String paymentReference, ServiceContext serviceContext);

	default PaymentStatus getRecurringPaymentStatus(String paymentId, ServiceContext serviceContext) {
		throw new UnsupportedOperationException();
	}

	PaymentResponse preparePayment(PaymentContext paymentContext, ServiceContext serviceContext);

	default PaymentResponse prepareRecurringPayment(PaymentContext paymentContext, ServiceContext serviceContext) {
		throw new UnsupportedOperationException();
	}

	default PaymentResponse takeRecurringPayment(PaymentContext paymentContext, String agreementId, String idempotencyKey, ServiceContext serviceContext) {
		throw new UnsupportedOperationException();
	}

	default String getAgentReferredPaymentPayload(long companyId, AgentReferredPaymentContext agentReferredPaymentContext) throws PortalException {
		return StringPool.BLANK;
	}

	default PaymentResponse processAgentReferredPaymentResponse(String externalReferenceCode, ServiceContext serviceContext) {
		throw new UnsupportedOperationException();
	}
}
