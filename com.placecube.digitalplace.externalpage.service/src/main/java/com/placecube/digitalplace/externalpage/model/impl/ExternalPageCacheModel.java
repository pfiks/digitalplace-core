/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.externalpage.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.externalpage.model.ExternalPage;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ExternalPage in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ExternalPageCacheModel
	implements CacheModel<ExternalPage>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ExternalPageCacheModel)) {
			return false;
		}

		ExternalPageCacheModel externalPageCacheModel =
			(ExternalPageCacheModel)object;

		if (externalPageId == externalPageCacheModel.externalPageId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, externalPageId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", externalPageId=");
		sb.append(externalPageId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", url=");
		sb.append(url);
		sb.append(", title=");
		sb.append(title);
		sb.append(", content=");
		sb.append(content);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ExternalPage toEntityModel() {
		ExternalPageImpl externalPageImpl = new ExternalPageImpl();

		if (uuid == null) {
			externalPageImpl.setUuid("");
		}
		else {
			externalPageImpl.setUuid(uuid);
		}

		externalPageImpl.setExternalPageId(externalPageId);
		externalPageImpl.setGroupId(groupId);
		externalPageImpl.setCompanyId(companyId);
		externalPageImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			externalPageImpl.setCreateDate(null);
		}
		else {
			externalPageImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			externalPageImpl.setModifiedDate(null);
		}
		else {
			externalPageImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (url == null) {
			externalPageImpl.setUrl("");
		}
		else {
			externalPageImpl.setUrl(url);
		}

		if (title == null) {
			externalPageImpl.setTitle("");
		}
		else {
			externalPageImpl.setTitle(title);
		}

		if (content == null) {
			externalPageImpl.setContent("");
		}
		else {
			externalPageImpl.setContent(content);
		}

		externalPageImpl.setStatus(status);

		externalPageImpl.resetOriginalValues();

		return externalPageImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		externalPageId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		url = objectInput.readUTF();
		title = objectInput.readUTF();
		content = objectInput.readUTF();

		status = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(externalPageId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (url == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(url);
		}

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (content == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(content);
		}

		objectOutput.writeBoolean(status);
	}

	public String uuid;
	public long externalPageId;
	public long groupId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public String url;
	public String title;
	public String content;
	public boolean status;

}