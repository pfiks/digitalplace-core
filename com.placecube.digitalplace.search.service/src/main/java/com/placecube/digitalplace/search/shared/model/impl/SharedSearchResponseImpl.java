package com.placecube.digitalplace.search.shared.model.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;
import com.placecube.digitalplace.search.internal.util.SearchContainerFactory;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;

public class SharedSearchResponseImpl implements SharedSearchResponse {

	private final String escapedIteratorURL;
	private final Hits hits;
	private final String iteratorURL;
	private final SearchContainer<Object> searchContainer;
	private final SearchContext searchContext;
	private final SharedSearchSession sharedSearchSession;

	public static SharedSearchResponseImpl init(Hits hits, SearchContext searchContext, SearchContainer<Object> searchContainer, SharedSearchSession sharedSearchSession) {
		return new SharedSearchResponseImpl(hits, searchContext, searchContainer, sharedSearchSession);
	}

	private SharedSearchResponseImpl(Hits hits, SearchContext searchContext, SearchContainer<Object> searchContainer, SharedSearchSession sharedSearchSession) {
		this.hits = hits;
		this.searchContext = searchContext;
		this.searchContainer = searchContainer;
		this.sharedSearchSession = sharedSearchSession;
		iteratorURL = searchContainer.getIteratorURL().toString();
		escapedIteratorURL = HtmlUtil.escape(iteratorURL);
	}

	@Override
	public List<SharedActiveFilter> getActiveFilters() {
		return sharedSearchSession.getActiveFilters();
	}

	@Override
	public Set<String> getActiveValuesForFilter(String filterName) {
		return sharedSearchSession.getActiveFilters().stream().filter(f -> f.getName().equals(filterName)).map(SharedActiveFilter::getValue).collect(Collectors.toSet());
	}

	@Override
	public String getEscapedSearchResultsIteratorURL() {
		return escapedIteratorURL;
	}

	@Override
	public Map<String, Integer> getFacetValues(String fieldName) {
		Map<String, Integer> facetValues = new LinkedHashMap<>();

		Facet facet = searchContext.getFacet(fieldName);

		if (Validator.isNotNull(facet)) {
			facet.getFacetCollector().getTermCollectors().stream().forEach(termCollector -> {
				String term = termCollector.getTerm();
				if (Validator.isNotNull(term)) {
					facetValues.put(term, termCollector.getFrequency());
				}
			});
		}

		return facetValues;
	}

	@Override
	public String getIteratorURL() {
		return iteratorURL;
	}

	@Override
	public String getKeywords() {
		return searchContext.getKeywords();
	}

	@Override
	public SearchContainer getSearchContainerForResults(String searchContainerId, List results, String noResultsMessage) {
		SearchContainer<?> searchContainerUpdated = SearchContainerFactory.createSearchContainer(searchContainer.getPortletRequest(), searchContainer.getDisplayTerms(),
				searchContainer.getSearchTerms(), searchContainer.getCurParam(), searchContainer.getCur(), searchContainer.getDelta(), searchContainer.getIteratorURL(),
				searchContainer.getHeaderNames(), noResultsMessage);

		searchContainerUpdated.setId(searchContainerId);
		searchContainerUpdated.setResultsAndTotal(() -> results, getTotalResults());

		return searchContainerUpdated;
	}

	@Override
	public Document[] getSearchResults() {
		return hits.getDocs();
	}

	@Override
	public String getSharedSearchSessionSingleValuedAttribute(String attributeName) {
		return sharedSearchSession.getSingleValuedAttribute(attributeName);
	}

	@Override
	public int getTotalResults() {
		return hits.getLength();
	}

}
