package com.placecube.digitalplace.gds.taglib.tag;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.gds.taglib.context.ServletContextUtil;

public class ErrorSummaryTag extends IncludeTag {

	private String portletNamespace;
	private Map<String, String> errors;

	@Override
	public int doStartTag() {
		setAttributeNamespace("gds-forms-ui:errorSummary");
		return EVAL_BODY_INCLUDE;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);
		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setPortletNamespace(String portletNamespace) {
		this.portletNamespace = portletNamespace;
	}

	@Override
	protected void cleanUp() {
		super.cleanUp();
		portletNamespace = null;
		errors = new HashMap<>();
	}

	@Override
	protected String getPage() {
		return "/META-INF/taglibs/gds-forms-ui/error-summary/view.jsp";
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute("portletNamespace", portletNamespace);
		request.setAttribute("errors", errors);
	}

}
