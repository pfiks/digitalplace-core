package com.placecube.digitalplace.search.shared.model;

import java.io.Serializable;

public class SharedActiveFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String name;
	private final String value;
	private final String label;
	private final String colour;

	public SharedActiveFilter(String name, String value, String label, String colour) {
		this.name = name;
		this.value = value;
		this.label = label;
		this.colour = colour;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public String getLabel() {
		return label;
	}

	public String getColour() {
		return colour;
	}
}
