package com.placecube.digitalplace.authentication.twofactor.internal.upgrade;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.authentication.twofactor.internal.upgrade.steps.Upgrade_1_1_1_UserTwoFactorAuthenticationTableIndexesUpdate;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class TwoFactorAuthenticationServiceUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("1.1.0", "1.1.1", new Upgrade_1_1_1_UserTwoFactorAuthenticationTableIndexesUpdate());
	}

}