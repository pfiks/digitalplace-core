/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.dataconsumer.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link DataConsumer}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DataConsumer
 * @generated
 */
public class DataConsumerWrapper
	extends BaseModelWrapper<DataConsumer>
	implements DataConsumer, ModelWrapper<DataConsumer> {

	public DataConsumerWrapper(DataConsumer dataConsumer) {
		super(dataConsumer);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("dataConsumerId", getDataConsumerId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("ddmFormInstanceId", getDdmFormInstanceId());
		attributes.put(
			"ddmDataProviderInstanceId", getDdmDataProviderInstanceId());
		attributes.put("sourceDataProviderId", getSourceDataProviderId());
		attributes.put("method", getMethod());
		attributes.put("mapping", getMapping());
		attributes.put("headers", getHeaders());
		attributes.put("chain", getChain());
		attributes.put("body", getBody());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long dataConsumerId = (Long)attributes.get("dataConsumerId");

		if (dataConsumerId != null) {
			setDataConsumerId(dataConsumerId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long ddmFormInstanceId = (Long)attributes.get("ddmFormInstanceId");

		if (ddmFormInstanceId != null) {
			setDdmFormInstanceId(ddmFormInstanceId);
		}

		Long ddmDataProviderInstanceId = (Long)attributes.get(
			"ddmDataProviderInstanceId");

		if (ddmDataProviderInstanceId != null) {
			setDdmDataProviderInstanceId(ddmDataProviderInstanceId);
		}

		Long sourceDataProviderId = (Long)attributes.get(
			"sourceDataProviderId");

		if (sourceDataProviderId != null) {
			setSourceDataProviderId(sourceDataProviderId);
		}

		String method = (String)attributes.get("method");

		if (method != null) {
			setMethod(method);
		}

		String mapping = (String)attributes.get("mapping");

		if (mapping != null) {
			setMapping(mapping);
		}

		String headers = (String)attributes.get("headers");

		if (headers != null) {
			setHeaders(headers);
		}

		Integer chain = (Integer)attributes.get("chain");

		if (chain != null) {
			setChain(chain);
		}

		String body = (String)attributes.get("body");

		if (body != null) {
			setBody(body);
		}
	}

	@Override
	public DataConsumer cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the body of this data consumer.
	 *
	 * @return the body of this data consumer
	 */
	@Override
	public String getBody() {
		return model.getBody();
	}

	/**
	 * Returns the chain of this data consumer.
	 *
	 * @return the chain of this data consumer
	 */
	@Override
	public Integer getChain() {
		return model.getChain();
	}

	/**
	 * Returns the company ID of this data consumer.
	 *
	 * @return the company ID of this data consumer
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this data consumer.
	 *
	 * @return the create date of this data consumer
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the data consumer ID of this data consumer.
	 *
	 * @return the data consumer ID of this data consumer
	 */
	@Override
	public long getDataConsumerId() {
		return model.getDataConsumerId();
	}

	/**
	 * Returns the ddm data provider instance ID of this data consumer.
	 *
	 * @return the ddm data provider instance ID of this data consumer
	 */
	@Override
	public long getDdmDataProviderInstanceId() {
		return model.getDdmDataProviderInstanceId();
	}

	/**
	 * Returns the ddm form instance ID of this data consumer.
	 *
	 * @return the ddm form instance ID of this data consumer
	 */
	@Override
	public long getDdmFormInstanceId() {
		return model.getDdmFormInstanceId();
	}

	/**
	 * Returns the group ID of this data consumer.
	 *
	 * @return the group ID of this data consumer
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the headers of this data consumer.
	 *
	 * @return the headers of this data consumer
	 */
	@Override
	public String getHeaders() {
		return model.getHeaders();
	}

	/**
	 * Returns the mapping of this data consumer.
	 *
	 * @return the mapping of this data consumer
	 */
	@Override
	public String getMapping() {
		return model.getMapping();
	}

	/**
	 * Returns the method of this data consumer.
	 *
	 * @return the method of this data consumer
	 */
	@Override
	public String getMethod() {
		return model.getMethod();
	}

	/**
	 * Returns the modified date of this data consumer.
	 *
	 * @return the modified date of this data consumer
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this data consumer.
	 *
	 * @return the primary key of this data consumer
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the source data provider ID of this data consumer.
	 *
	 * @return the source data provider ID of this data consumer
	 */
	@Override
	public long getSourceDataProviderId() {
		return model.getSourceDataProviderId();
	}

	/**
	 * Returns the user ID of this data consumer.
	 *
	 * @return the user ID of this data consumer
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user uuid of this data consumer.
	 *
	 * @return the user uuid of this data consumer
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this data consumer.
	 *
	 * @return the uuid of this data consumer
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the body of this data consumer.
	 *
	 * @param body the body of this data consumer
	 */
	@Override
	public void setBody(String body) {
		model.setBody(body);
	}

	/**
	 * Sets the chain of this data consumer.
	 *
	 * @param chain the chain of this data consumer
	 */
	@Override
	public void setChain(Integer chain) {
		model.setChain(chain);
	}

	/**
	 * Sets the company ID of this data consumer.
	 *
	 * @param companyId the company ID of this data consumer
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this data consumer.
	 *
	 * @param createDate the create date of this data consumer
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the data consumer ID of this data consumer.
	 *
	 * @param dataConsumerId the data consumer ID of this data consumer
	 */
	@Override
	public void setDataConsumerId(long dataConsumerId) {
		model.setDataConsumerId(dataConsumerId);
	}

	/**
	 * Sets the ddm data provider instance ID of this data consumer.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID of this data consumer
	 */
	@Override
	public void setDdmDataProviderInstanceId(long ddmDataProviderInstanceId) {
		model.setDdmDataProviderInstanceId(ddmDataProviderInstanceId);
	}

	/**
	 * Sets the ddm form instance ID of this data consumer.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID of this data consumer
	 */
	@Override
	public void setDdmFormInstanceId(long ddmFormInstanceId) {
		model.setDdmFormInstanceId(ddmFormInstanceId);
	}

	/**
	 * Sets the group ID of this data consumer.
	 *
	 * @param groupId the group ID of this data consumer
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the headers of this data consumer.
	 *
	 * @param headers the headers of this data consumer
	 */
	@Override
	public void setHeaders(String headers) {
		model.setHeaders(headers);
	}

	/**
	 * Sets the mapping of this data consumer.
	 *
	 * @param mapping the mapping of this data consumer
	 */
	@Override
	public void setMapping(String mapping) {
		model.setMapping(mapping);
	}

	/**
	 * Sets the method of this data consumer.
	 *
	 * @param method the method of this data consumer
	 */
	@Override
	public void setMethod(String method) {
		model.setMethod(method);
	}

	/**
	 * Sets the modified date of this data consumer.
	 *
	 * @param modifiedDate the modified date of this data consumer
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this data consumer.
	 *
	 * @param primaryKey the primary key of this data consumer
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the source data provider ID of this data consumer.
	 *
	 * @param sourceDataProviderId the source data provider ID of this data consumer
	 */
	@Override
	public void setSourceDataProviderId(long sourceDataProviderId) {
		model.setSourceDataProviderId(sourceDataProviderId);
	}

	/**
	 * Sets the user ID of this data consumer.
	 *
	 * @param userId the user ID of this data consumer
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user uuid of this data consumer.
	 *
	 * @param userUuid the user uuid of this data consumer
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this data consumer.
	 *
	 * @param uuid the uuid of this data consumer
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected DataConsumerWrapper wrap(DataConsumer dataConsumer) {
		return new DataConsumerWrapper(dataConsumer);
	}

}