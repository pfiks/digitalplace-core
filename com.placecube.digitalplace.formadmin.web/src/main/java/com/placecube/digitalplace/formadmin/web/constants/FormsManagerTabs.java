package com.placecube.digitalplace.formadmin.web.constants;

public enum FormsManagerTabs {

	FORMS("forms"),

	DATA_CONSUMERS("data-consumers");

	private final String navigationKey;

	FormsManagerTabs(String navigationKey) {
		this.navigationKey = navigationKey;
	}

	public String getNavigationKey() {
		return navigationKey;
	}
}
