package com.placecube.digitalplace.audit.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.PortalPreferenceValue;
import com.liferay.portal.security.audit.event.generators.constants.EventTypes;
import com.placecube.digitalplace.audit.service.AuditService;

@Component(immediate = true, service = ModelListener.class)
public class AuditPortalPreferenceValueListener extends BaseModelListener<PortalPreferenceValue> {

	@Reference
	private AuditService auditService;

	@Override
	public void onAfterCreate(PortalPreferenceValue portalPreferenceValue) {
		addAuditEntry(portalPreferenceValue, EventTypes.ADD);
	}

	@Override
	public void onAfterRemove(PortalPreferenceValue portalPreferenceValue) {
		addAuditEntry(portalPreferenceValue, EventTypes.DELETE);
	}

	@Override
	public void onAfterUpdate(PortalPreferenceValue previousPortalPreferenceValue, PortalPreferenceValue portalPreferenceValue) {
		addAuditEntry(portalPreferenceValue, EventTypes.UPDATE);
	}

	private void addAuditEntry(PortalPreferenceValue portalPreferenceValue, String eventType) {
		auditService.addAuditLogEntry(PortalPreferenceValue.class.getName(), portalPreferenceValue.getPortalPreferenceValueId(), eventType,
				auditService.getPortalPreferenceValueAuditInfo(portalPreferenceValue));

	}

}
