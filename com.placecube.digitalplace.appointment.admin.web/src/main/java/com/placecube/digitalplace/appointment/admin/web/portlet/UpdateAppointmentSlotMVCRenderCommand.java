package com.placecube.digitalplace.appointment.admin.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.admin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.admin.web.constants.PortletKeys;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.APPOINTMENTS_MANAGER, "mvc.command.name=" + MVCCommandKeys.UPDATE_SLOT }, service = MVCRenderCommand.class)
public class UpdateAppointmentSlotMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(UpdateAppointmentSlotMVCRenderCommand.class);

	@Reference
	private AppointmentsManagerService appointmentsManagerService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {

			long appointmentSlotId = ParamUtil.getLong(renderRequest, "appointmentSlotId", 0l);
			if (appointmentSlotId > 0) {
				renderRequest.setAttribute("appointmentSlotForm", appointmentsManagerService.getAppointmentSlotForm(appointmentSlotId));
			} else {
				ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
				renderRequest.setAttribute("appointmentSlotForm", appointmentsManagerService.getBlankAppointmentSlotForm(themeDisplay.getTimeZone(), ParamUtil.getString(renderRequest, "slotType"),
						ParamUtil.getString(renderRequest, "serviceId")));
			}

			return "/update-slot.jsp";

		} catch (Exception e) {
			LOG.error(e);
			throw new PortletException(e);
		}
	}

}
