package com.placecube.digitalplace.notifications.govuknotificationsservice.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.notifications.govuknotificationsservice.configuration.GovUKNotificationServiceConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class GovUKNotificationsServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 11l;

	private static final String KEY_NAME = "SOME_KEY";

	@InjectMocks
	private GovUKNotificationsService govUKNotificationsService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private GovUKNotificationServiceConfiguration mockGovUKNotificationServiceConfiguration;

	@Test(expected = ConfigurationException.class)
	public void getApplicationKey_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		govUKNotificationsService.getApplicationKey(COMPANY_ID, "any name");
	}

	@Test
	public void getApplicationKey_WhenNoAdditionalKeysDefined_ThenReturnsTheDefaultApplicationKey() throws ConfigurationException {
		String defaultApplicationKey = "applicationKeyValue";
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, COMPANY_ID)).thenReturn(mockGovUKNotificationServiceConfiguration);
		when(mockGovUKNotificationServiceConfiguration.applicationKey()).thenReturn(defaultApplicationKey);
		String[] additionalKeys = {};
		when(mockGovUKNotificationServiceConfiguration.additionalApplicationKeys()).thenReturn(additionalKeys);

		String result = govUKNotificationsService.getApplicationKey(COMPANY_ID, KEY_NAME);

		assertThat(result, equalTo(defaultApplicationKey));
	}

	@Test
	public void getApplicationKey_WhenAdditionalKeysWrongFormat_ThenReturnsTheDefaultApplicationKey() throws ConfigurationException {
		String defaultApplicationKey = "applicationKeyValue";
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, COMPANY_ID)).thenReturn(mockGovUKNotificationServiceConfiguration);
		when(mockGovUKNotificationServiceConfiguration.applicationKey()).thenReturn(defaultApplicationKey);
		String[] additionalKeys = {"key1", "key2"};
		when(mockGovUKNotificationServiceConfiguration.additionalApplicationKeys()).thenReturn(additionalKeys);

		String result = govUKNotificationsService.getApplicationKey(COMPANY_ID, KEY_NAME);

		assertThat(result, equalTo(defaultApplicationKey));
	}

	@Test
	public void getApplicationKey_WhenAdditionalKeyNotFound_ThenReturnsTheDefaultApplicationKey() throws ConfigurationException {
		String defaultApplicationKey = "applicationKeyValue";
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, COMPANY_ID)).thenReturn(mockGovUKNotificationServiceConfiguration);
		when(mockGovUKNotificationServiceConfiguration.applicationKey()).thenReturn(defaultApplicationKey);
		String[] additionalKeys = {"key1:value1", "key2:value2"};
		when(mockGovUKNotificationServiceConfiguration.additionalApplicationKeys()).thenReturn(additionalKeys);

		String result = govUKNotificationsService.getApplicationKey(COMPANY_ID, KEY_NAME);

		assertThat(result, equalTo(defaultApplicationKey));
	}

	@Test
	public void getApplicationKey_WhenAdditionalKeyFound_ThenReturnsValueOfAdditionalKey() throws ConfigurationException {
		String defaultApplicationKey = "applicationKeyValue";
		String keyValue = "Key-Value";
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, COMPANY_ID)).thenReturn(mockGovUKNotificationServiceConfiguration);
		when(mockGovUKNotificationServiceConfiguration.applicationKey()).thenReturn(defaultApplicationKey);
		String[] additionalKeys = {"key1:value1", "key2:value2", KEY_NAME + ":" + keyValue};
		when(mockGovUKNotificationServiceConfiguration.additionalApplicationKeys()).thenReturn(additionalKeys);

		String result = govUKNotificationsService.getApplicationKey(COMPANY_ID, KEY_NAME);

		assertThat(result, equalTo(keyValue));
	}

	@Test(expected = ConfigurationException.class)
	public void isEmailEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		govUKNotificationsService.isEmailEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEmailEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsEmailEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, COMPANY_ID)).thenReturn(mockGovUKNotificationServiceConfiguration);
		when(mockGovUKNotificationServiceConfiguration.emailEnabled()).thenReturn(expected);

		boolean result = govUKNotificationsService.isEmailEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = ConfigurationException.class)
	public void isSmsEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		govUKNotificationsService.isSmsEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isSmsEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsSmsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKNotificationServiceConfiguration.class, COMPANY_ID)).thenReturn(mockGovUKNotificationServiceConfiguration);
		when(mockGovUKNotificationServiceConfiguration.smsEnabled()).thenReturn(expected);

		boolean result = govUKNotificationsService.isSmsEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

}
