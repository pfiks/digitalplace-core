package com.placecube.digitalplace.ddl.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletRequest;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;

public class DDLURLServiceTest extends PowerMockito {

	@InjectMocks
	private DDLURLService ddlURLService;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private DDLRecordSetLocalService mockDDLRecordSetLocalService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LiferayPortletURL mockLiferayPortletURL;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PortletURLFactory mockPortletURLFactory;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void getEditRecordURL_WhenDDLRecordSetFoundAndExceptionSettingWindowState_ThenThrowsPortalException() throws PortalException, WindowStateException {
		long recordSetId = 1;
		String[] params = new String[] { "one=1", "two=2" };
		when(mockDDLRecordSetLocalService.getDDLRecordSet(recordSetId)).thenReturn(mockDDLRecordSet);
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockPortletURLFactory.create(mockHttpServletRequest, PortletKeys.DDL_DISPLAY_PORTLET, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		doThrow(new WindowStateException("", null)).when(mockLiferayPortletURL).setWindowState(LiferayWindowState.MAXIMIZED);

		ddlURLService.getEditRecordURL(mockThemeDisplay, recordSetId, params);
	}

	@Test
	public void getEditRecordURL_WhenDDLRecordSetFoundAndNoError_ThenReturnsTheRenderUrl() throws PortalException, WindowStateException {
		long recordSetId = 1;
		long structureId = 2;
		String[] params = new String[] { "one=1", "two=2" };
		when(mockDDLRecordSetLocalService.getDDLRecordSet(recordSetId)).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(structureId);
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockPortletURLFactory.create(mockHttpServletRequest, PortletKeys.DDL_DISPLAY_PORTLET, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.toString()).thenReturn("expectedUrl");
		when(mockLiferayPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		String result = ddlURLService.getEditRecordURL(mockThemeDisplay, recordSetId, params);

		assertThat(result, equalTo("expectedUrl"));

		verify(mockLiferayPortletURL, times(1)).setWindowState(LiferayWindowState.MAXIMIZED);
		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_ENTRY);
		verify(mockMutableRenderParameters, times(1)).setValue("ddlRecordSetId", String.valueOf(recordSetId));
		verify(mockMutableRenderParameters, times(1)).setValue("ddmStructureId", String.valueOf(structureId));
		verify(mockMutableRenderParameters, times(1)).setValue("ddlAutofillFields", "one=1");
		verify(mockMutableRenderParameters, times(1)).setValue("ddlAutofillFields", "two=2");
	}

	@Test(expected = PortalException.class)
	public void getEditRecordURL_WhenNoDDLRecordSetFound_ThenThrowsPortalException() throws PortalException {
		long recordSetId = 1;
		String[] params = new String[] { "one=1", "two=2" };
		when(mockDDLRecordSetLocalService.getDDLRecordSet(recordSetId)).thenThrow(new PortalException());

		ddlURLService.getEditRecordURL(mockThemeDisplay, recordSetId, params);
	}
}
