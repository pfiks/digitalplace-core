package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.displaycontext;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;

@RunWith(PowerMockRunner.class)
public class AdvancePeopleSearchDisplayContextTest extends PowerMockito {

	private static final String FIELD1 = "field1";

	private static final String VALUE1 = "value1";

	private AdvancePeopleSearchDisplayContext advancePeopleSearchDisplayContext;

	@Before
	public void setup() {
		List<SharedActiveFilter> activeFilters = new ArrayList<>();
		activeFilters.add(new SharedActiveFilter(FIELD1, VALUE1, "label1", "colour1"));
		activeFilters.add(new SharedActiveFilter("field2", "value2", "label2", "colour2"));
		advancePeopleSearchDisplayContext = AdvancePeopleSearchDisplayContext.initDisplayContext(activeFilters);
	}

	@Test
	public void getFilterValue_WhenFilterExists_ThenReturnFilterValue() {
		String result = advancePeopleSearchDisplayContext.getFilterValue(FIELD1);

		assertThat(result, equalTo(VALUE1));
	}

	@Test
	public void getFilterValue_WhenFilterDoesNotExists_ThenReturnEmptyString() {
		String result = advancePeopleSearchDisplayContext.getFilterValue("field");

		assertThat(result, equalTo(StringPool.BLANK));
	}

}
