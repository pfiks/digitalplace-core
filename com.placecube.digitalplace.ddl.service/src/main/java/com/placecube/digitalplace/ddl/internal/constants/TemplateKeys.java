package com.placecube.digitalplace.ddl.internal.constants;

public final class TemplateKeys {

	public static final String COMPANY = "company";

	public static final String COMPANY_ID = "companyId";

	public static final String CUR_RECORD = "cur_record";

	public static final String DEVICE = "device";

	public static final String ENTRIES = "entries";

	public static final String GROUP_ID = "groupId";

	public static final String JOURNAL_TEMPLATES_PATH = "journalTemplatesPath";

	public static final String PERMISSION_CHECKER = "permissionChecker";

	public static final String RANDOM_NAMESPACE = "randomNamespace";

	public static final String SCOPE_GROUP_ID = "scopeGroupId";

	public static final String SITE_GROUP_ID = "siteGroupId";

	public static final String TEMPLATES_PATH = "templatesPath";

	private TemplateKeys() {
	}
}
