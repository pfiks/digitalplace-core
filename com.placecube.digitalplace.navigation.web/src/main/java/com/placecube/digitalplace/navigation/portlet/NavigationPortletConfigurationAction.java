package com.placecube.digitalplace.navigation.portlet;

import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.site.navigation.constants.SiteNavigationConstants;
import com.placecube.digitalplace.navigation.configuration.NavigationPortletInstanceConfiguration;
import com.placecube.digitalplace.navigation.constants.ConfigurationConstants;
import com.placecube.digitalplace.navigation.constants.PortletKeys;
import com.placecube.digitalplace.navigation.service.NavigationService;

@Component(immediate = true, configurationPid = ConfigurationConstants.NAVIGATION_PID, configurationPolicy = ConfigurationPolicy.OPTIONAL, property = "javax.portlet.name="
		+ PortletKeys.NAVIGATION, service = ConfigurationAction.class)
public class NavigationPortletConfigurationAction extends DefaultConfigurationAction {

	private volatile NavigationPortletInstanceConfiguration navigationPortletInstanceConfiguration;

	@Reference
	private NavigationService navigationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		PortletRequest portletRequest = (PortletRequest) httpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST);
		PortletPreferences preferences = portletRequest.getPreferences();

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String navigationMenuGroupId = preferences.getValue(ConfigurationConstants.NAVIGATION_MENU_GROUP_ID, String.valueOf(navigationPortletInstanceConfiguration.navigationMenuGroupId()));
		httpServletRequest.setAttribute(ConfigurationConstants.NAVIGATION_MENU_GROUP_ID, navigationMenuGroupId);

		String navigationType = preferences.getValue(ConfigurationConstants.NAVIGATION_TYPE, String.valueOf(navigationPortletInstanceConfiguration.navigationType()));
		httpServletRequest.setAttribute(ConfigurationConstants.NAVIGATION_TYPE, navigationType);

		String navigationMenuId = preferences.getValue(ConfigurationConstants.NAVIGATION_MENU_ID, String.valueOf(navigationPortletInstanceConfiguration.navigationMenuId()));
		httpServletRequest.setAttribute(ConfigurationConstants.NAVIGATION_MENU_ID, navigationMenuId);

		String widgetTemplateKey = preferences.getValue(ConfigurationConstants.WIDGET_TEMPLATE_KEY, navigationPortletInstanceConfiguration.widgetTemplateKey());
		httpServletRequest.setAttribute(ConfigurationConstants.WIDGET_TEMPLATE_KEY, widgetTemplateKey);

		String widgetTemplateGroupId = preferences.getValue(ConfigurationConstants.WIDGET_TEMPLATE_GROUP_ID, String.valueOf(navigationPortletInstanceConfiguration.widgetTemplateGroupId()));
		httpServletRequest.setAttribute(ConfigurationConstants.WIDGET_TEMPLATE_GROUP_ID, widgetTemplateGroupId);

		Set<Group> availableWidgetTemplateGroups = navigationService.getAvailableGroupsForWidgetTemplateRetrieval(themeDisplay);

		httpServletRequest.setAttribute("availableWidgetTemplateGroups", availableWidgetTemplateGroups);
		httpServletRequest.setAttribute("availableNavigationMenuGroups", navigationService.getGroupsForNavigationMenuRetrieval(availableWidgetTemplateGroups));

		super.include(portletConfig, httpServletRequest, httpServletResponse);

	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long navigationMenuGroupId = ParamUtil.getLong(actionRequest, ConfigurationConstants.NAVIGATION_MENU_GROUP_ID);
		setPreference(actionRequest, ConfigurationConstants.NAVIGATION_MENU_GROUP_ID, String.valueOf(navigationMenuGroupId));

		long navigationMenuId = ParamUtil.getLong(actionRequest, ConfigurationConstants.NAVIGATION_MENU_ID);
		setPreference(actionRequest, ConfigurationConstants.NAVIGATION_MENU_ID, String.valueOf(navigationMenuId));

		int navigationType = ParamUtil.getInteger(actionRequest, ConfigurationConstants.NAVIGATION_TYPE, SiteNavigationConstants.TYPE_PRIMARY);
		setPreference(actionRequest, ConfigurationConstants.NAVIGATION_TYPE, String.valueOf(navigationType));

		String widgetTemplateKey = ParamUtil.getString(actionRequest, ConfigurationConstants.WIDGET_TEMPLATE_KEY);
		setPreference(actionRequest, ConfigurationConstants.WIDGET_TEMPLATE_KEY, widgetTemplateKey);

		long widgetTemplateGroupId = ParamUtil.getLong(actionRequest, ConfigurationConstants.WIDGET_TEMPLATE_GROUP_ID);
		setPreference(actionRequest, ConfigurationConstants.WIDGET_TEMPLATE_GROUP_ID, String.valueOf(widgetTemplateGroupId));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		navigationPortletInstanceConfiguration = ConfigurableUtil.createConfigurable(NavigationPortletInstanceConfiguration.class, properties);

		if (navigationPortletInstanceConfiguration == null) {
			throw new IllegalStateException("Navigation configuration missing - PID: " + ConfigurationConstants.NAVIGATION_PID);
		}

	}

}
