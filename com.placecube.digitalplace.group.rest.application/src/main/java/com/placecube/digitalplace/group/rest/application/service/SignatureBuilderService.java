package com.placecube.digitalplace.group.rest.application.service;

import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;

@Component(immediate = true, service = SignatureBuilderService.class)
public class SignatureBuilderService {

	private static final Log LOG = LogFactoryUtil.getLog(SignatureBuilderService.class);

	@Reference
	private GroupApiKeyLocalService groupApiKeyLocalService;

	public String getEvaluatedSignature(String applicationKey, String dateHeader, String contextPath, String path, String method, String body) {
		try {
			String textToEncode = String.join(StringPool.NEW_LINE, applicationKey, dateHeader, contextPath, path, method, body);
			LOG.debug("Building signature.. Text to encode:");
			LOG.debug(textToEncode);

			String signature = groupApiKeyLocalService.getEncodedTextWithKey(applicationKey, textToEncode);

			LOG.debug("Evaluated signature: " + signature);
			return signature;
		} catch (Exception e) {
			LOG.warn("Unable to build signature", e);
			throw new GroupRestSecurityException(ErrorCodes.INVALID_SIGNATURE, Response.Status.BAD_REQUEST);
		}
	}

	public void validateSignatureMatch(String signatureHeader, String evaluatedSignature) {
		if (!signatureHeader.equals(evaluatedSignature)) {
			LOG.info("Signature is invalid - clientSignature: " + signatureHeader + ", evaluated signature: " + evaluatedSignature);
			throw new GroupRestSecurityException(ErrorCodes.INVALID_SIGNATURE, Response.Status.FORBIDDEN);
		}
	}

}
