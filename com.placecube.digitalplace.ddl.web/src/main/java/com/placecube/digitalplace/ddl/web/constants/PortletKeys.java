package com.placecube.digitalplace.ddl.web.constants;

public final class PortletKeys {

	public static final String DDL_DISPLAY_PORTLET = "com_placecube_digitalplace_ddl_portlet_DDLDisplayPortlet";

	public static final String DDL_FIELD_FACET_PORTLET = "com_placecube_digitalplace_ddl_portlet_DDLFieldFacetPortlet";

	public static final String DDL_FIELD_FILTER_PORTLET = "com_placecube_digitalplace_ddl_portlet_DDLFieldFilterPortlet";

	public static final String DDL_LOCATION_SEARCH_PORTLET = "com_placecube_digitalplace_ddl_portlet_DDLLocationSearchPortlet";

	public static final String DDL_SEARCH_LISTINGS_PORTLET = "com_placecube_digitalplace_ddl_portlet_DDLSearchListingsPortlet";

	private PortletKeys() {
	}

}
