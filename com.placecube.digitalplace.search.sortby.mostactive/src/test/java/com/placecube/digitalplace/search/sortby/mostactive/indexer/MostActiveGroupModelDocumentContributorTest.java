package com.placecube.digitalplace.search.sortby.mostactive.indexer;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.search.Document;
import com.placecube.digitalplace.search.sortby.mostactive.constants.MostActiveConstants;
import com.placecube.digitalplace.search.sortby.mostactive.service.MostActiveService;

public class MostActiveGroupModelDocumentContributorTest extends PowerMockito {

	@Mock
	private Document mockDocument;

	@Mock
	private Group mockGroup;

	@Mock
	private MostActiveService mockMostActiveService;

	@InjectMocks
	private MostActiveGroupModelDocumentContributor mostActiveGroupModelDocumentContributor;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenGroupIsNotOrganization_ThenAddsTheMostActiveFieldToTheIndexedDocument() {
		long count = 56;
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockMostActiveService.getActivitiesCount(mockGroup)).thenReturn(count);

		mostActiveGroupModelDocumentContributor.contribute(mockDocument, mockGroup);

		verify(mockDocument, times(1)).addKeyword(MostActiveConstants.COUNTER_FIELD_NAME, count);
	}

	@Test
	public void contribute_WhenGroupIsOrganization_ThenNoChangesAreMadeToTheIndexedDocument() {
		when(mockGroup.isOrganization()).thenReturn(true);

		mostActiveGroupModelDocumentContributor.contribute(mockDocument, mockGroup);

		verifyZeroInteractions(mockDocument);
	}
}
