package com.placecube.digitalplace.search.web.portlet.tag.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.search.web.portlet.tag.configuration.TagFacetPortletInstanceConfiguration")
public interface TagFacetPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "10")
	int maxShowMoreTagsToShow();

	@Meta.AD(required = false, deflt = "10")
	int maxTagsToShow();

	@Meta.AD(required = false)
	String filterColour();

	@Meta.AD(required = false, deflt = "false")
	boolean tagsQueryAnded();

}
