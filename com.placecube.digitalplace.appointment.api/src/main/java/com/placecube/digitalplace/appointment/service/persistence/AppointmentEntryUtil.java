/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.appointment.model.AppointmentEntry;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the appointment entry service. This utility wraps <code>com.placecube.digitalplace.appointment.service.persistence.impl.AppointmentEntryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentEntryPersistence
 * @generated
 */
public class AppointmentEntryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(AppointmentEntry appointmentEntry) {
		getPersistence().clearCache(appointmentEntry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, AppointmentEntry> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AppointmentEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AppointmentEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AppointmentEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static AppointmentEntry update(AppointmentEntry appointmentEntry) {
		return getPersistence().update(appointmentEntry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static AppointmentEntry update(
		AppointmentEntry appointmentEntry, ServiceContext serviceContext) {

		return getPersistence().update(appointmentEntry, serviceContext);
	}

	/**
	 * Returns all the appointment entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching appointment entries
	 */
	public static List<AppointmentEntry> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the appointment entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	public static List<AppointmentEntry> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	public static List<AppointmentEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	public static List<AppointmentEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public static AppointmentEntry findByUuid_First(
			String uuid, OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public static AppointmentEntry fetchByUuid_First(
		String uuid, OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public static AppointmentEntry findByUuid_Last(
			String uuid, OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public static AppointmentEntry fetchByUuid_Last(
		String uuid, OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public static AppointmentEntry[] findByUuid_PrevAndNext(
			long appointmentEntryId, String uuid,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().findByUuid_PrevAndNext(
			appointmentEntryId, uuid, orderByComparator);
	}

	/**
	 * Removes all the appointment entries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of appointment entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching appointment entries
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching appointment entries
	 */
	public static List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	public static List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	public static List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	public static List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public static AppointmentEntry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public static AppointmentEntry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public static AppointmentEntry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public static AppointmentEntry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public static AppointmentEntry[] findByUuid_C_PrevAndNext(
			long appointmentEntryId, String uuid, long companyId,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().findByUuid_C_PrevAndNext(
			appointmentEntryId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the appointment entries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching appointment entries
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @return the matching appointment entries
	 */
	public static List<AppointmentEntry>
		findByCompanyId_EntryClassPK_EntryClassNameId(
			long companyId, long entryClassPK, long entryClassNameId) {

		return getPersistence().findByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId);
	}

	/**
	 * Returns a range of all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	public static List<AppointmentEntry>
		findByCompanyId_EntryClassPK_EntryClassNameId(
			long companyId, long entryClassPK, long entryClassNameId, int start,
			int end) {

		return getPersistence().findByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId, start, end);
	}

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	public static List<AppointmentEntry>
		findByCompanyId_EntryClassPK_EntryClassNameId(
			long companyId, long entryClassPK, long entryClassNameId, int start,
			int end, OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().findByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId, start, end,
			orderByComparator);
	}

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	public static List<AppointmentEntry>
		findByCompanyId_EntryClassPK_EntryClassNameId(
			long companyId, long entryClassPK, long entryClassNameId, int start,
			int end, OrderByComparator<AppointmentEntry> orderByComparator,
			boolean useFinderCache) {

		return getPersistence().findByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId, start, end,
			orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public static AppointmentEntry
			findByCompanyId_EntryClassPK_EntryClassNameId_First(
				long companyId, long entryClassPK, long entryClassNameId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().
			findByCompanyId_EntryClassPK_EntryClassNameId_First(
				companyId, entryClassPK, entryClassNameId, orderByComparator);
	}

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public static AppointmentEntry
		fetchByCompanyId_EntryClassPK_EntryClassNameId_First(
			long companyId, long entryClassPK, long entryClassNameId,
			OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().
			fetchByCompanyId_EntryClassPK_EntryClassNameId_First(
				companyId, entryClassPK, entryClassNameId, orderByComparator);
	}

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public static AppointmentEntry
			findByCompanyId_EntryClassPK_EntryClassNameId_Last(
				long companyId, long entryClassPK, long entryClassNameId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().
			findByCompanyId_EntryClassPK_EntryClassNameId_Last(
				companyId, entryClassPK, entryClassNameId, orderByComparator);
	}

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public static AppointmentEntry
		fetchByCompanyId_EntryClassPK_EntryClassNameId_Last(
			long companyId, long entryClassPK, long entryClassNameId,
			OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().
			fetchByCompanyId_EntryClassPK_EntryClassNameId_Last(
				companyId, entryClassPK, entryClassNameId, orderByComparator);
	}

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public static AppointmentEntry[]
			findByCompanyId_EntryClassPK_EntryClassNameId_PrevAndNext(
				long appointmentEntryId, long companyId, long entryClassPK,
				long entryClassNameId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().
			findByCompanyId_EntryClassPK_EntryClassNameId_PrevAndNext(
				appointmentEntryId, companyId, entryClassPK, entryClassNameId,
				orderByComparator);
	}

	/**
	 * Removes all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 */
	public static void removeByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId) {

		getPersistence().removeByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId);
	}

	/**
	 * Returns the number of appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @return the number of matching appointment entries
	 */
	public static int countByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId) {

		return getPersistence().countByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId);
	}

	/**
	 * Returns all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @return the matching appointment entries
	 */
	public static List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId) {

		return getPersistence().
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
				companyId, serviceId, connectorClassName, appointmentId);
	}

	/**
	 * Returns a range of all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	public static List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId, int start, int end) {

		return getPersistence().
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
				companyId, serviceId, connectorClassName, appointmentId, start,
				end);
	}

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	public static List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId, int start, int end,
			OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
				companyId, serviceId, connectorClassName, appointmentId, start,
				end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	public static List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId, int start, int end,
			OrderByComparator<AppointmentEntry> orderByComparator,
			boolean useFinderCache) {

		return getPersistence().
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
				companyId, serviceId, connectorClassName, appointmentId, start,
				end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public static AppointmentEntry
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_First(
				long companyId, String serviceId, String connectorClassName,
				String appointmentId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_First(
				companyId, serviceId, connectorClassName, appointmentId,
				orderByComparator);
	}

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public static AppointmentEntry
		fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_First(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId,
			OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().
			fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_First(
				companyId, serviceId, connectorClassName, appointmentId,
				orderByComparator);
	}

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public static AppointmentEntry
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_Last(
				long companyId, String serviceId, String connectorClassName,
				String appointmentId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_Last(
				companyId, serviceId, connectorClassName, appointmentId,
				orderByComparator);
	}

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public static AppointmentEntry
		fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_Last(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId,
			OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().
			fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_Last(
				companyId, serviceId, connectorClassName, appointmentId,
				orderByComparator);
	}

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public static AppointmentEntry[]
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_PrevAndNext(
				long appointmentEntryId, long companyId, String serviceId,
				String connectorClassName, String appointmentId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_PrevAndNext(
				appointmentEntryId, companyId, serviceId, connectorClassName,
				appointmentId, orderByComparator);
	}

	/**
	 * Removes all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 */
	public static void
		removeByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId) {

		getPersistence().
			removeByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
				companyId, serviceId, connectorClassName, appointmentId);
	}

	/**
	 * Returns the number of appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @return the number of matching appointment entries
	 */
	public static int
		countByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId) {

		return getPersistence().
			countByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
				companyId, serviceId, connectorClassName, appointmentId);
	}

	/**
	 * Caches the appointment entry in the entity cache if it is enabled.
	 *
	 * @param appointmentEntry the appointment entry
	 */
	public static void cacheResult(AppointmentEntry appointmentEntry) {
		getPersistence().cacheResult(appointmentEntry);
	}

	/**
	 * Caches the appointment entries in the entity cache if it is enabled.
	 *
	 * @param appointmentEntries the appointment entries
	 */
	public static void cacheResult(List<AppointmentEntry> appointmentEntries) {
		getPersistence().cacheResult(appointmentEntries);
	}

	/**
	 * Creates a new appointment entry with the primary key. Does not add the appointment entry to the database.
	 *
	 * @param appointmentEntryId the primary key for the new appointment entry
	 * @return the new appointment entry
	 */
	public static AppointmentEntry create(long appointmentEntryId) {
		return getPersistence().create(appointmentEntryId);
	}

	/**
	 * Removes the appointment entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry that was removed
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public static AppointmentEntry remove(long appointmentEntryId)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().remove(appointmentEntryId);
	}

	public static AppointmentEntry updateImpl(
		AppointmentEntry appointmentEntry) {

		return getPersistence().updateImpl(appointmentEntry);
	}

	/**
	 * Returns the appointment entry with the primary key or throws a <code>NoSuchAppointmentEntryException</code> if it could not be found.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public static AppointmentEntry findByPrimaryKey(long appointmentEntryId)
		throws com.placecube.digitalplace.appointment.exception.
			NoSuchAppointmentEntryException {

		return getPersistence().findByPrimaryKey(appointmentEntryId);
	}

	/**
	 * Returns the appointment entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry, or <code>null</code> if a appointment entry with the primary key could not be found
	 */
	public static AppointmentEntry fetchByPrimaryKey(long appointmentEntryId) {
		return getPersistence().fetchByPrimaryKey(appointmentEntryId);
	}

	/**
	 * Returns all the appointment entries.
	 *
	 * @return the appointment entries
	 */
	public static List<AppointmentEntry> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of appointment entries
	 */
	public static List<AppointmentEntry> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of appointment entries
	 */
	public static List<AppointmentEntry> findAll(
		int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of appointment entries
	 */
	public static List<AppointmentEntry> findAll(
		int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the appointment entries from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of appointment entries.
	 *
	 * @return the number of appointment entries
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AppointmentEntryPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(AppointmentEntryPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile AppointmentEntryPersistence _persistence;

}