package com.placecube.digitalplace.group.rest.application.filters;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.MockitoAnnotations.initMocks;

import java.security.Principal;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.group.rest.application.BasicAuthGroupRestApplication;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;
import com.placecube.digitalplace.group.rest.application.service.GroupRestRetrievalService;

public class BasicAuthCheckerFilterTest extends PowerMockito {

	private BasicAuthCheckerFilter basicAuthCheckerFilter;

	@Mock
	private BasicAuthGroupRestApplication mockBasicAuthGroupRestApplication;

	@Mock
	private ContainerRequestContext mockContainerRequestContext;

	@Mock
	private GroupRestRetrievalService mockGroupRestRetrievalService;

	@Mock
	private Principal mockPrincipal;

	@Mock
	private SecurityContext mockSecurityContext;

	@Before
	public void activateSetUp() {
		initMocks(this);

		when(mockBasicAuthGroupRestApplication.getGroupRestRetrievalService()).thenReturn(mockGroupRestRetrievalService);
		basicAuthCheckerFilter = new BasicAuthCheckerFilter(mockBasicAuthGroupRestApplication);
	}

	@Test
	public void filter_WhenAuthenticationSchemeIsBasicAndUserIsDefaultUser_ThenThrowsGroupSecurityException() throws Exception {
		when(mockContainerRequestContext.getSecurityContext()).thenReturn(mockSecurityContext);
		when(mockSecurityContext.getAuthenticationScheme()).thenReturn(SecurityContext.BASIC_AUTH);
		when(mockSecurityContext.getUserPrincipal()).thenReturn(mockPrincipal);
		when(mockPrincipal.getName()).thenReturn("123");
		when(mockGroupRestRetrievalService.isGuestUser(123l)).thenReturn(true);

		try {
			basicAuthCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			assertThat(e.getMessage(), equalTo(ErrorCodes.INVALID_AUTHENTICATION.getCode()));
			assertThat(e.getResponse().getStatusInfo(), equalTo(Response.Status.FORBIDDEN));
		}
	}

	@Test
	public void filter_WhenAuthenticationSchemeIsBasicAndUserIsNotDefaultUser_ThenNoErrorIsThrown() throws Exception {
		when(mockContainerRequestContext.getSecurityContext()).thenReturn(mockSecurityContext);
		when(mockSecurityContext.getAuthenticationScheme()).thenReturn(SecurityContext.BASIC_AUTH);
		when(mockSecurityContext.getUserPrincipal()).thenReturn(mockPrincipal);
		when(mockPrincipal.getName()).thenReturn("123");
		when(mockGroupRestRetrievalService.isGuestUser(123l)).thenReturn(false);

		try {
			basicAuthCheckerFilter.filter(mockContainerRequestContext);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void filter_WhenAuthenticationSchemeIsBlank_ThenThrowsGroupSecurityException() throws Exception {
		when(mockContainerRequestContext.getSecurityContext()).thenReturn(mockSecurityContext);
		when(mockSecurityContext.getAuthenticationScheme()).thenReturn("");

		try {
			basicAuthCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			assertThat(e.getMessage(), equalTo(ErrorCodes.INVALID_AUTHENTICATION.getCode()));
			assertThat(e.getResponse().getStatusInfo(), equalTo(Response.Status.FORBIDDEN));
		}
	}

	@Test
	public void filter_WhenAuthenticationSchemeIsNotBasic_ThenThrowsGroupSecurityException() throws Exception {
		when(mockContainerRequestContext.getSecurityContext()).thenReturn(mockSecurityContext);
		when(mockSecurityContext.getAuthenticationScheme()).thenReturn(SecurityContext.CLIENT_CERT_AUTH);

		try {
			basicAuthCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			assertThat(e.getMessage(), equalTo(ErrorCodes.INVALID_AUTHENTICATION.getCode()));
			assertThat(e.getResponse().getStatusInfo(), equalTo(Response.Status.FORBIDDEN));
		}
	}

}
