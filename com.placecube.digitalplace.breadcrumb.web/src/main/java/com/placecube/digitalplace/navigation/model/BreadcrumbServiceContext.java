package com.placecube.digitalplace.navigation.model;

import java.util.Locale;
import java.util.Optional;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public class BreadcrumbServiceContext {

	private String categoryNavigationBuildCategoriesHierarchyUrlPropertyName;

	private long categoryNavigationFilterVocabularyId;

	private String categoryNavigationUrlCategoryPropertyName;

	private boolean displayOnlyOneAssetCategoryBreadcrumb;

	private Locale locale;

	private Layout layout;

	private AssetEntry layoutAssetEntry;

	private String pageTitle;

	private ThemeDisplay themeDisplay;

	private boolean useLayoutUrl;

	public long getCategoryNavFilterVocabularyId() {
		return categoryNavigationFilterVocabularyId;
	}

	public String getCategoryNavigationBuildCategoriesHierarchyUrlPropertyName() {
		return categoryNavigationBuildCategoriesHierarchyUrlPropertyName;
	}

	public String getCategoryNavUrlCategoryPropertyName() {
		return categoryNavigationUrlCategoryPropertyName;
	}

	public Locale getLocale() {
		return locale;
	}

	public Layout getLayout() {
		return layout;
	}

	public Optional<AssetEntry> getLayoutAssetEntry() {
		return Optional.ofNullable(layoutAssetEntry);
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public ThemeDisplay getThemeDisplay() {
		return themeDisplay;
	}

	public boolean isDisplayOnlyOneAssetCategoryBreadcrumb() {
		return displayOnlyOneAssetCategoryBreadcrumb;
	}

	public boolean isUseLayoutUrl() {
		return useLayoutUrl;
	}

	public void setCategoryNavigationBuildCategoriesHierarchyUrlPropertyName(String categoryNavigationBuildCategoriesHierarchyUrlPropertyName) {
		this.categoryNavigationBuildCategoriesHierarchyUrlPropertyName = categoryNavigationBuildCategoriesHierarchyUrlPropertyName;
	}

	public void setCategoryNavigationFilterVocabularyId(long categoryNavigationFilterVocabularyId) {
		this.categoryNavigationFilterVocabularyId = categoryNavigationFilterVocabularyId;
	}

	public void setCategoryNavigationUrlCategoryPropertyName(String categoryNavigationUrlCategoryPropertyName) {
		this.categoryNavigationUrlCategoryPropertyName = categoryNavigationUrlCategoryPropertyName;
	}

	public void setDisplayOnlyOneAssetCategoryBreadcrumb(boolean displayOnlyOneAssetCategoryBreadcrumb) {
		this.displayOnlyOneAssetCategoryBreadcrumb = displayOnlyOneAssetCategoryBreadcrumb;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public void setLayout(Layout layout) {
		this.layout = layout;
	}

	public void setLayoutAssetEntry(AssetEntry layoutAssetEntry) {
		this.layoutAssetEntry = layoutAssetEntry;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public void setThemeDisplay(ThemeDisplay themeDisplay) {
		this.themeDisplay = themeDisplay;
	}

	public void setUseLayoutUrl(boolean useLayoutUrl) {
		this.useLayoutUrl = useLayoutUrl;
	}

}
