package com.placecube.digitalplace.payment.heycentric.service;

import java.security.GeneralSecurityException;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.heycentric.configuration.HeycentricPaymentCompanyConfiguration;
import com.placecube.digitalplace.payment.heycentric.service.arp.HeycentricARPService;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentStatus;
import com.placecube.digitalplace.payment.heycentric.service.internal.HeycentricPaymentOrderFactory;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;
import com.placecube.digitalplace.payment.service.PaymentConnector;

@Component(immediate = true, service = PaymentConnector.class)
public class HeycentricPaymentConnector implements PaymentConnector {

	private static final Log LOG = LogFactoryUtil.getLog(HeycentricPaymentConnector.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private HeycentricPaymentOrderFactory heycentricPaymentOrderFactory;

	@Reference
	private HeycentricARPService heycentricARPService;

	@Reference
	private Portal portal;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	public boolean enabled(long companyId) {
		try {
			HeycentricPaymentCompanyConfiguration heycentricPaymentCompanyConfiguration = configurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, companyId);
			return heycentricPaymentCompanyConfiguration.enabled();
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Optional<String> getBackURL(String paymentReference, ServiceContext serviceContext) {
		String backURL = serviceContext.getPortalURL() + serviceContext.getCurrentURL();
		return Optional.ofNullable(backURL);
	}

	@Override
	public PaymentStatus getPaymentStatus(String paymentReference, ServiceContext serviceContext) {
		String queryString = serviceContext.getRequest().getQueryString();
		if (Validator.isNull(queryString)) {
			LOG.error("Query string is null or empty.");
			return PaymentStatus.paymentError();
		}

		String recNo = StringPool.BLANK;
		String authCode = StringPool.BLANK;
		String returnedStatus = StringPool.BLANK;
		for (String parameter : queryString.split("&")) {
			try {
				String[] paramAndVal = parameter.split("=");
				if (paramAndVal.length == 2) {
					String paramName = paramAndVal[0];
					String paramValue = paramAndVal[1];

					if (HeycentricPaymentConstants.RESPONSE_PAYMENT_AUTH_CODE_PARAMETER.equalsIgnoreCase(paramName)) {
						authCode = paramValue;
					} else if (HeycentricPaymentConstants.RESPONSE_REC_NO_PARAMETER.equalsIgnoreCase(paramName)) {
						recNo = paramValue;
					} else if (HeycentricPaymentConstants.RESPONSE_PAYMENT_STATUS_PARAMETER.equalsIgnoreCase(paramName)) {
						returnedStatus = paramValue;
					}
				}
			} catch (Exception e) {
				LOG.error("Parameter with value is incorect " + parameter);
			}
		}

		if (HeycentricPaymentStatus.SUCCESSFUL.equalsIgnoreCase(returnedStatus)) {
			String paymentRef = authCode + StringPool.SPACE + recNo;
			PaymentStatus paymentStatus = PaymentStatus.success();
			LOG.debug("Payment successful - Payment Ref:" + paymentRef);
			paymentStatus.setPaymentUniqueReferenceNumber(paymentRef);
			return paymentStatus;
		} else {
			return PaymentStatus.paymentError();
		}

	}

	@Override
	public PaymentResponse preparePayment(PaymentContext paymentContext, ServiceContext serviceContext) {
		String paymentURL = StringPool.BLANK;
		PaymentStatus paymentStatus = PaymentStatus.success();
		try {
			HeycentricPaymentCompanyConfiguration heycentricPaymentCompanyConfiguration = configurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class,
					serviceContext.getCompanyId());
			paymentURL = heycentricPaymentOrderFactory.getPaymentURL(heycentricPaymentCompanyConfiguration, paymentContext);
		} catch (ConfigurationException e) {
			LOG.error("Unable to prepare payment", e);
			paymentStatus = PaymentStatus.appError();
		}
		return new PaymentResponse(paymentURL, paymentStatus);
	}

	@Override
	public String getAgentReferredPaymentPayload(long companyId, AgentReferredPaymentContext agentReferredPaymentContext) throws PortalException {
		try {
			HeycentricPaymentCompanyConfiguration heycentricPaymentCompanyConfiguration = configurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, companyId);

			JSONObject arpRequestPayload = heycentricARPService.getAgentReferredPaymentRequestPayload(agentReferredPaymentContext, heycentricPaymentCompanyConfiguration);

			JSONObject fullResponse = jsonFactory.createJSONObject();
			fullResponse.put(HeycentricPaymentConstants.JSON_RESPONSE_ENDPOINT, heycentricPaymentCompanyConfiguration.arpEndpoint());
			fullResponse.put(HeycentricPaymentConstants.JSON_RESPONSE_REQUEST_PAYLOAD, arpRequestPayload.toJSONString());

			return fullResponse.toJSONString();
		} catch (GeneralSecurityException e) {
			throw new PortalException(e);
		}
	}

	@Override
	public PaymentResponse processAgentReferredPaymentResponse(String externalReferenceCode, ServiceContext serviceContext) {
		try {
			return heycentricARPService.processAgentReferredPaymentResponse(serviceContext.getRequest(), externalReferenceCode);
		} catch (Exception e) {
			LOG.error(e);
			return new PaymentResponse(StringPool.BLANK, PaymentStatus.appError());
		}
	}
}