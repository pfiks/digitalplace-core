package com.placecube.digitalplace.formadmin.web.portlet.command;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.digitalplace.form.service.FormDefinitionRegistry;
import com.placecube.digitalplace.form.service.FormDefinitionService;
import com.placecube.digitalplace.formadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.FORMS_MANAGER, "mvc.command.name=" + MVCCommandKeys.INSTALL_FORM_DEFINTION }, service = MVCActionCommand.class)
public class InstallFormDefinitionMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private FormDefinitionRegistry formDefinitionRegistry;

	@Reference
	private FormDefinitionService formDefinitionService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {
		try {

			ServiceContext serviceContext = ServiceContextFactory.getInstance(DDMFormInstance.class.getName(), actionRequest);

			FormDefinition formDefinition = formDefinitionRegistry.getFormDefinitionByKey(ParamUtil.getString(actionRequest, PortletRequestKeys.FORM_DEFINITION_KEY));

			formDefinitionService.addFormDefinition(formDefinition, serviceContext);

		} catch (Exception e) {
			throw new PortletException(e);

		}
	}

}
