package com.placecube.digitalplace.ddl.internal.util;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.ddl.model.DynamicDataListEntry;
import com.placecube.digitalplace.ddl.model.DynamicDataListValue;
import com.placecube.digitalplace.ddl.model.impl.DynamicDataListEntryImpl;
import com.placecube.digitalplace.ddl.model.impl.ModelBuilder;

public class DynamicDataListEntryRetrievalUtilTest extends PowerMockito {

	@InjectMocks
	private DynamicDataListEntryRetrievalUtil dynamicDataListEntryRetrievalUtil;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordParsingUtil mockDDLRecordParsingUtil;

	@Mock
	private DDMFormField mockDDMFormField;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue1;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue2;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DynamicDataListEntryImpl mockDynamicDataListEntryImpl;

	@Mock
	private DynamicDataListValue mockDynamicDataListValue;

	@Mock
	private ModelBuilder mockModelBuilder;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getDynamicDataListEntry_WhenExceptionGettingFormValues_ThenReturnsEmptyOptional() throws PortalException {
		when(mockDDLRecord.getDDMFormValues()).thenThrow(new PortalException());

		Optional<DynamicDataListEntry> result = dynamicDataListEntryRetrievalUtil.getDynamicDataListEntry(mockDDLRecord);

		assertFalse(result.isPresent());
	}

	@Test
	public void getDynamicDataListEntry_WhenNoError_ThenReturnsOptionalWithTheDynamicDataListEntryPopulatedWithAllTheValidFields() throws PortalException {
		when(mockDDLRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(Arrays.asList(mockDDMFormFieldValue1, mockDDMFormFieldValue2));
		when(mockDDMFormFieldValue1.getDDMFormField()).thenReturn(null);
		when(mockDDMFormFieldValue2.getDDMFormField()).thenReturn(mockDDMFormField);
		when(mockDDLRecordParsingUtil.getFieldLabel(mockDDMFormField)).thenReturn("formFieldLabelValue");
		when(mockDDLRecordParsingUtil.getFieldValueAndAdditionalDetails(mockDDMFormField, mockDDMFormFieldValue2)).thenReturn(new String[] { "formFieldValue", "formFieldAdditionalVal" });
		when(mockModelBuilder.initialiseDynamicDataListValue("formFieldLabelValue", "formFieldValue", "formFieldAdditionalVal")).thenReturn(mockDynamicDataListValue);
		when(mockModelBuilder.initialiseDynamicDataListEntry(mockDDLRecord)).thenReturn(mockDynamicDataListEntryImpl);

		Optional<DynamicDataListEntry> result = dynamicDataListEntryRetrievalUtil.getDynamicDataListEntry(mockDDLRecord);

		assertThat(result.get(), sameInstance(mockDynamicDataListEntryImpl));
		verify(mockDynamicDataListEntryImpl, times(1)).addDynamicDataListValue(mockDynamicDataListValue);
	}

}
