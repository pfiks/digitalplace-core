package com.placecube.digitalplace.ddl.web.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;
import com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch.configuration.DDLLocationSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLSearchListingsPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration;

@Component(immediate = true, service = DDLConfigurationService.class)
public class DDLConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DynamicDataListConfigurationService dynamicDataListConfigurationService;

	public List<DDMStructure> getAvailableDDMStructures(Group group) throws PortalException {
		List<DynamicDataListMapping> dynamicDataListMappings = dynamicDataListConfigurationService.getDynamicDataListMappings(group);
		return dynamicDataListMappings.stream().map(DynamicDataListMapping::getDDMStructure).collect(Collectors.toList());
	}

	public DDLDisplayPortletInstanceConfiguration getDDLDisplayPortletInstanceConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		return configurationProvider.getPortletInstanceConfiguration(DDLDisplayPortletInstanceConfiguration.class, themeDisplay);
	}

	public DDLLocationSearchPortletInstanceConfiguration getDDLLocationSearchPortletInstanceConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		return configurationProvider.getPortletInstanceConfiguration(DDLLocationSearchPortletInstanceConfiguration.class, themeDisplay);
	}

	public DDLSearchListingsPortletInstanceConfiguration getDDLSearchListingsPortletInstanceConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		return configurationProvider.getPortletInstanceConfiguration(DDLSearchListingsPortletInstanceConfiguration.class, themeDisplay);
	}

	public Optional<DynamicDataListMapping> getDynamicDataListMappingForConfiguredStructure(long companyId, DDLSearchListingsPortletInstanceConfiguration configuration) {
		long structureId = configuration.ddmStructureId();
		List<DynamicDataListMapping> dynamicDataListMappings = dynamicDataListConfigurationService.getDynamicDataListMappings(companyId);
		return dynamicDataListMappings.stream().filter(mapping -> mapping.getDDMStructureId() == structureId).findFirst();
	}

	public List<Long> getLongValuesAsList(String[] values) {
		List<Long> results = new ArrayList<>();
		if (null != values) {
			for (String value : values) {
				Long id = GetterUtil.getLong(value, 0l);
				if (id > 0) {
					results.add(id);
				}

			}
		}
		return results;
	}

	public List<Long> getValuesAsList(long[] values) {
		if (ArrayUtil.isNotEmpty(values)) {
			return Arrays.asList(ArrayUtil.toArray(values));
		}
		return new ArrayList<>();
	}
}
