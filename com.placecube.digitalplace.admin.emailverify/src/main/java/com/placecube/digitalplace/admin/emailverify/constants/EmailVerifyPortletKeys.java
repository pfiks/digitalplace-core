package com.placecube.digitalplace.admin.emailverify.constants;

public final class EmailVerifyPortletKeys {

	public static final String ADMIN_EMAIL_VERIFY = "com_placecube_digitalplace_admin_emailverify_AdminEmailVerifyPortlet";

	private EmailVerifyPortletKeys() {
	}

}
