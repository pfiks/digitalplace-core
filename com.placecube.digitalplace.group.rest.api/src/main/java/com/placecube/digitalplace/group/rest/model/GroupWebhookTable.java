/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

/**
 * The table class for the &quot;Placecube_GroupRest_GroupWebhook&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see GroupWebhook
 * @generated
 */
public class GroupWebhookTable extends BaseTable<GroupWebhookTable> {

	public static final GroupWebhookTable INSTANCE = new GroupWebhookTable();

	public final Column<GroupWebhookTable, Long> webhookId = createColumn(
		"webhookId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<GroupWebhookTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<GroupWebhookTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<GroupWebhookTable, String> name = createColumn(
		"name", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<GroupWebhookTable, String> payloadURL = createColumn(
		"payloadURL", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<GroupWebhookTable, Boolean> enabled = createColumn(
		"enabled", Boolean.class, Types.BOOLEAN, Column.FLAG_DEFAULT);
	public final Column<GroupWebhookTable, String> events = createColumn(
		"events", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<GroupWebhookTable, String> className = createColumn(
		"className", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<GroupWebhookTable, Long> classPK = createColumn(
		"classPK", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);

	private GroupWebhookTable() {
		super("Placecube_GroupRest_GroupWebhook", GroupWebhookTable::new);
	}

}