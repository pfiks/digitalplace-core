package com.placecube.digitalplace.group.restadmin.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.ViewKeys;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=portlet-group-settings-webhook", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.display-category=category.hidden", //
		"javax.portlet.init-param.template-path=/", //
		"javax.portlet.init-param.view-template=" + ViewKeys.VIEW_GROUP_WEBHOOK_JSP, //
		"javax.portlet.name=" + PortletKeys.GROUP_WEBHOOKS, //
		"javax.portlet.resource-bundle=content.Language" }, service = Portlet.class)
public class GroupSettingsWebhookPortlet extends MVCPortlet {

}
