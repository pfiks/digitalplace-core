package com.placecube.digitalplace.ddl.internal.indexer;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.storage.Field;
import com.liferay.dynamic.data.mapping.storage.Fields;
import com.liferay.dynamic.data.mapping.util.DDMFormValuesToFieldsConverter;
import com.liferay.dynamic.data.mapping.util.DDMIndexer;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.util.HtmlParser;
import com.placecube.digitalplace.ddl.constants.DDLIndexerField;
import com.placecube.digitalplace.ddl.internal.constants.DDLFormFieldTypeConstants;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;

@RunWith(PowerMockRunner.class)
public class DDLRecordModelDocumentContributorTest extends PowerMockito {

	private static final long STRUCTURE_ID = 43433L;

	private static final String FIELD_NAME = "fieldName";
	private static final String FIELD_VALUE = "value";
	private static final String EXTRACTED_FIELD_VALUE = "extractedValue";

	@InjectMocks
	private DDLRecordModelDocumentContributor ddlRecordModelDocumentContributor;

	@Mock
	private DDMFormValuesToFieldsConverter mockDDMFormValuesToFieldsConverter;

	@Mock
	private DynamicDataListConfigurationService mockDynamicDataListConfigurationService;

	@Mock
	private DDMIndexer mockDDMIndexer;

	@Mock
	private Document mockDocument;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private Fields mockFields;

	@Mock
	private Field mockField;

	@Mock
	private DynamicDataListMapping mockDynamicDataListMapping;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private HtmlParser mockHtmlParser;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Test
	public void contribute_WhenNoErrors_ThenAddsStructureAttributesToDocument() throws Exception {
		final String structureKey = "structureKey";

		mockDDLRecord();

		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockDDMStructure.getStructureKey()).thenReturn(structureKey);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, times(1)).addKeyword(DDLIndexerField.DDM_STRUCTURE_ID, STRUCTURE_ID);
		verify(mockDocument, times(1)).addKeyword(DDLIndexerField.DDM_STRUCTURE_KEY, structureKey);
	}

	@Test
	public void contribute_WhenDynamicDataListMappingIsPresentAndTitleIsPresentInMappingConfigurationAndTitleFieldExist_ThenAddsLocalisedAndDefaultTitleFieldToDocument() throws Exception {
		final String indexerTitleFieldName = "title";

		mockDDLRecord();

		when(mockDynamicDataListConfigurationService.getDynamicDataListMapping(mockDDMStructure)).thenReturn(Optional.of(mockDynamicDataListMapping));
		when(mockDynamicDataListMapping.getIndexerTitleFieldName()).thenReturn(indexerTitleFieldName);
		when(mockFields.get(indexerTitleFieldName)).thenReturn(mockField);
		when(mockField.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockField.getValue(Locale.US)).thenReturn(FIELD_VALUE);
		when(mockField.getValue(Locale.UK)).thenReturn(FIELD_VALUE);
		when(mockHtmlParser.extractText(FIELD_VALUE)).thenReturn(EXTRACTED_FIELD_VALUE);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		Map<Locale, String> localisedMap = new HashMap<>();
		localisedMap.put(Locale.US, EXTRACTED_FIELD_VALUE);

		verify(mockDocument, times(1)).addLocalizedText(com.liferay.portal.kernel.search.Field.TITLE, localisedMap);
		verify(mockDocument, times(1)).addText(com.liferay.portal.kernel.search.Field.TITLE, EXTRACTED_FIELD_VALUE);
	}

	@Test
	public void contribute_WhenDynamicDataListMappingIsPresentAndTitleIsPresentInMappingConfigurationAndTitleFieldDoesNotExist_ThenDoesNotAddTitleFieldToDocument() throws Exception {
		final String indexerTitleFieldName = "title";

		mockDDLRecord();

		when(mockDynamicDataListConfigurationService.getDynamicDataListMapping(mockDDMStructure)).thenReturn(Optional.of(mockDynamicDataListMapping));
		when(mockDynamicDataListMapping.getIndexerTitleFieldName()).thenReturn(indexerTitleFieldName);
		when(mockFields.get(indexerTitleFieldName)).thenReturn(null);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, never()).addLocalizedText(anyString(), any(Map.class));
		verify(mockDocument, never()).addText(anyString(), anyString());
	}

	@Test
	public void contribute_WhenDynamicDataListMappingIsPresentAndContentIsPresentInMappingConfigurationAndAndContentFieldExists_ThenAddsLocalisedAndDefaultContentFieldToDocument() throws Exception {
		final String indexerContentFieldName = "content";

		mockDDLRecord();

		when(mockDynamicDataListConfigurationService.getDynamicDataListMapping(mockDDMStructure)).thenReturn(Optional.of(mockDynamicDataListMapping));
		when(mockDynamicDataListMapping.getIndexerContentFieldName()).thenReturn(indexerContentFieldName);
		when(mockFields.get(indexerContentFieldName)).thenReturn(mockField);
		when(mockField.getDefaultLocale()).thenReturn(Locale.UK);
		when(mockField.getValue(Locale.US)).thenReturn(FIELD_VALUE);
		when(mockField.getValue(Locale.UK)).thenReturn(FIELD_VALUE);
		when(mockHtmlParser.extractText(FIELD_VALUE)).thenReturn(EXTRACTED_FIELD_VALUE);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		Map<Locale, String> localisedMap = new HashMap<>();
		localisedMap.put(Locale.US, EXTRACTED_FIELD_VALUE);

		verify(mockDocument, times(1)).addLocalizedText(com.liferay.portal.kernel.search.Field.CONTENT, localisedMap);
		verify(mockDocument, times(1)).addText(com.liferay.portal.kernel.search.Field.CONTENT, EXTRACTED_FIELD_VALUE);

	}

	@Test
	public void contribute_WhenDynamicDataListMappingIsPresentAndContentIsPresentInMappingConfigurationAndContentFieldDoesNotExist_ThenDoesNotAddContentFieldToDocument() throws Exception {
		final String indexerContentFieldName = "content";

		mockDDLRecord();

		when(mockDynamicDataListConfigurationService.getDynamicDataListMapping(mockDDMStructure)).thenReturn(Optional.of(mockDynamicDataListMapping));
		when(mockDynamicDataListMapping.getIndexerTitleFieldName()).thenReturn(indexerContentFieldName);
		when(mockFields.get(indexerContentFieldName)).thenReturn(null);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, never()).addLocalizedText(anyString(), any(Map.class));
		verify(mockDocument, never()).addText(anyString(), anyString());
	}

	@Test
	public void contribute_WhenFieldIsOfDateTypeAndFieldIsNotRepeatableAndValueIsNotEmptyAndValueIsDate_ThenAddsFormattedDateFieldToDocument() throws Exception {
		final LocalDate localDate = LocalDate.now();
		Date date = Date.from(localDate.atStartOfDay(ZoneId.of("UTC")).toInstant());

		mockDDLRecord();

		Fields fields = new Fields();
		fields.put(mockField);

		when(mockField.getName()).thenReturn(FIELD_NAME);
		when(mockField.getType()).thenReturn(DDMFormFieldTypeConstants.DATE);
		when(mockField.isRepeatable()).thenReturn(false);
		when(mockField.getValue()).thenReturn(localDate.toString());

		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(fields);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, times(1)).addDate(FIELD_NAME, date);
	}

	@Test
	public void contribute_WhenFieldIsOfDateTypeAndFieldIsNotRepeatableAndValueIsNotEmptyAndValueIsDateTime_ThenAddsFormattedDateTimeFieldToDocument() throws Exception {
		final LocalDateTime localDateTime = LocalDateTime.now();
		Date date = Date.from(localDateTime.atZone(ZoneId.of("UTC")).toInstant());

		mockDDLRecord();

		Fields fields = new Fields();
		fields.put(mockField);

		when(mockField.getName()).thenReturn(FIELD_NAME);
		when(mockField.getType()).thenReturn(DDMFormFieldTypeConstants.DATE);
		when(mockField.isRepeatable()).thenReturn(false);
		when(mockField.getValue()).thenReturn(localDateTime.toString());

		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(fields);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, times(1)).addDate(FIELD_NAME, date);
	}

	@Test
	public void contribute_WhenFieldIsOfDateTypeAndFieldIsNotRepeatableAndValueIsNotEmptyAndValueIsNotValid_ThenDoesNotAddDateFieldToDocument() throws Exception {
		mockDDLRecord();

		Fields fields = new Fields();
		fields.put(mockField);

		when(mockField.getName()).thenReturn(FIELD_NAME);
		when(mockField.getType()).thenReturn(DDMFormFieldTypeConstants.DATE);
		when(mockField.isRepeatable()).thenReturn(false);
		when(mockField.getValue()).thenReturn("wrong value");

		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(fields);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, never()).addDate(anyString(), any(Date.class));
	}

	@Test
	public void contribute_WhenFieldIsOfDateTypeAndFieldIsNotRepeatableAndValueIsEmpty_ThenDoesNotAddDateFieldToDocument() throws Exception {
		mockDDLRecord();

		Fields fields = new Fields();
		fields.put(mockField);

		when(mockField.getName()).thenReturn(FIELD_NAME);
		when(mockField.getType()).thenReturn(DDMFormFieldTypeConstants.DATE);
		when(mockField.isRepeatable()).thenReturn(false);
		when(mockField.getValue()).thenReturn(StringPool.BLANK);

		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(fields);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, never()).addDate(anyString(), any(Date.class));
	}

	@Test
	public void contribute_WhenFieldIsOfDateTypeAndFieldIsRepeatable_ThenDoesNotAddDateFieldToDocument() throws Exception {
		mockDDLRecord();

		Fields fields = new Fields();
		fields.put(mockField);

		when(mockField.getName()).thenReturn(FIELD_NAME);
		when(mockField.getType()).thenReturn(DDMFormFieldTypeConstants.DATE);
		when(mockField.isRepeatable()).thenReturn(true);
		when(mockField.getValue()).thenReturn(LocalDateTime.now().toString());

		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(fields);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, never()).addDate(anyString(), any(Date.class));
	}

	@Test
	public void contribute_WhenFieldIsOfDDMDateTypeAndFieldIsRepeatable_ThenDoesNotAddDateFieldToDocument() throws Exception {
		mockDDLRecord();

		Fields fields = new Fields();
		fields.put(mockField);

		when(mockField.getName()).thenReturn(FIELD_NAME);
		when(mockField.getType()).thenReturn(DDLFormFieldTypeConstants.DDM_DATE);
		when(mockField.isRepeatable()).thenReturn(true);
		when(mockField.getValue()).thenReturn(LocalDateTime.now().toString());

		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(fields);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, never()).addDate(anyString(), any(Date.class));
	}

	@Test
	public void contribute_WhenFieldIsOfGeolocationType_ThenAddsSortableGeolocationFieldWithLocalisedName() throws Exception {
		final double latitude = 13.343543;
		final double longitude = 2.8732;
		final String json = "{\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\"}";
		final String name = "localisedName";

		mockDDLRecord();

		Fields fields = new Fields();
		fields.put(mockField);

		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockField.getName()).thenReturn(FIELD_NAME);
		when(mockField.getType()).thenReturn(DDMFormFieldTypeConstants.GEOLOCATION);
		when(mockField.getValue(Locale.US)).thenReturn(json);
		when(mockJSONFactory.createJSONObject(json)).thenReturn(mockJSONObject);
		when(mockJSONObject.getDouble("latitude", 0)).thenReturn(latitude);
		when(mockJSONObject.getDouble("longitude", 0)).thenReturn(longitude);
		when(mockDDMIndexer.encodeName(STRUCTURE_ID, FIELD_NAME, Locale.US)).thenReturn(name);

		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(fields);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, times(1)).addGeoLocation(name + "_geolocation_geoLocationsortable", latitude, longitude);
	}

	@Test
	public void contribute_WhenFieldIsNotOfDateOrGeolocationType_ThenDoesNotAddFieldToDocument() throws Exception {
		mockDDLRecord();

		Fields fields = new Fields();
		fields.put(mockField);

		when(mockField.getName()).thenReturn(FIELD_NAME);
		when(mockField.getType()).thenReturn("other");

		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(fields);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, never()).addDate(anyString(), any(Date.class));
		verify(mockDocument, never()).addGeoLocation(anyString(), anyDouble(), anyDouble());
	}

	@Test
	public void contribute_WhenFieldTypeCannotBeObtained_ThenDoesNotAddGeolocationOrDateFields() throws Exception {

		Fields fields = new Fields();
		fields.put(mockField);

		mockDDLRecord();
		when(mockField.getType()).thenThrow(new PortalException());
		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(fields);

		ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);

		verify(mockDocument, never()).addGeoLocation(anyString(), anyDouble(), anyDouble());
		verify(mockDocument, never()).addDate(anyString(), any(Date.class));
	}

	@Test
	public void contribute_WhenDDLRecordPropertiesCannotBeRetrieved_ThenDoesNotThrowException() throws Exception {
		when(mockDDLRecord.getRecordSet()).thenThrow(new PortalException());

		try {
			ddlRecordModelDocumentContributor.contribute(mockDocument, mockDDLRecord);
		} catch (Exception e) {
			fail("No exception expected");
		}
	}

	private void mockDDLRecord() throws PortalException {
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDDLRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMFormValues.getAvailableLocales()).thenReturn(new HashSet<>(Arrays.asList(Locale.US)));

		when(mockDDMFormValuesToFieldsConverter.convert(mockDDMStructure, mockDDMFormValues)).thenReturn(mockFields);
		when(mockDynamicDataListConfigurationService.getDynamicDataListMapping(mockDDMStructure)).thenReturn(Optional.empty());
	}
}
