/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.externalpage.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link ExternalPageLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ExternalPageLocalService
 * @generated
 */
public class ExternalPageLocalServiceWrapper
	implements ExternalPageLocalService,
			   ServiceWrapper<ExternalPageLocalService> {

	public ExternalPageLocalServiceWrapper() {
		this(null);
	}

	public ExternalPageLocalServiceWrapper(
		ExternalPageLocalService externalPageLocalService) {

		_externalPageLocalService = externalPageLocalService;
	}

	/**
	 * Adds the external page to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExternalPageLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param externalPage the external page
	 * @return the external page that was added
	 */
	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
		addExternalPage(
			com.placecube.digitalplace.externalpage.model.ExternalPage
				externalPage) {

		return _externalPageLocalService.addExternalPage(externalPage);
	}

	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
			addOrUpdateExternalPage(
				long companyId, long groupId, long userId, String url,
				String title, String content, boolean indexContent)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _externalPageLocalService.addOrUpdateExternalPage(
			companyId, groupId, userId, url, title, content, indexContent);
	}

	/**
	 * Creates a new external page with the primary key. Does not add the external page to the database.
	 *
	 * @param externalPageId the primary key for the new external page
	 * @return the new external page
	 */
	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
		createExternalPage(long externalPageId) {

		return _externalPageLocalService.createExternalPage(externalPageId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _externalPageLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the external page from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExternalPageLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param externalPage the external page
	 * @return the external page that was removed
	 */
	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
		deleteExternalPage(
			com.placecube.digitalplace.externalpage.model.ExternalPage
				externalPage) {

		return _externalPageLocalService.deleteExternalPage(externalPage);
	}

	/**
	 * Deletes the external page with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExternalPageLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page that was removed
	 * @throws PortalException if a external page with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
			deleteExternalPage(long externalPageId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _externalPageLocalService.deleteExternalPage(externalPageId);
	}

	@Override
	public void deleteExternalPageByPrimaryKey(long externalPageId)
		throws com.liferay.portal.kernel.exception.PortalException {

		_externalPageLocalService.deleteExternalPageByPrimaryKey(
			externalPageId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _externalPageLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _externalPageLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _externalPageLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _externalPageLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _externalPageLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.externalpage.model.impl.ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _externalPageLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.externalpage.model.impl.ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _externalPageLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _externalPageLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _externalPageLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
		fetchExternalPage(long externalPageId) {

		return _externalPageLocalService.fetchExternalPage(externalPageId);
	}

	/**
	 * Returns the external page matching the UUID and group.
	 *
	 * @param uuid the external page's UUID
	 * @param groupId the primary key of the group
	 * @return the matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
		fetchExternalPageByUuidAndGroupId(String uuid, long groupId) {

		return _externalPageLocalService.fetchExternalPageByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _externalPageLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _externalPageLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	/**
	 * Returns the external page with the primary key.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page
	 * @throws PortalException if a external page with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
			getExternalPage(long externalPageId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _externalPageLocalService.getExternalPage(externalPageId);
	}

	/**
	 * Returns the external page matching the UUID and group.
	 *
	 * @param uuid the external page's UUID
	 * @param groupId the primary key of the group
	 * @return the matching external page
	 * @throws PortalException if a matching external page could not be found
	 */
	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
			getExternalPageByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _externalPageLocalService.getExternalPageByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.Optional
		<com.placecube.digitalplace.externalpage.model.ExternalPage>
			getExternalPageImport(long externalPageImportId) {

		return _externalPageLocalService.getExternalPageImport(
			externalPageImportId);
	}

	/**
	 * Returns a range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.externalpage.model.impl.ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of external pages
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.externalpage.model.ExternalPage>
			getExternalPages(int start, int end) {

		return _externalPageLocalService.getExternalPages(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.externalpage.model.ExternalPage>
			getExternalPages(long groupId, int start, int end) {

		return _externalPageLocalService.getExternalPages(groupId, start, end);
	}

	/**
	 * Returns all the external pages matching the UUID and company.
	 *
	 * @param uuid the UUID of the external pages
	 * @param companyId the primary key of the company
	 * @return the matching external pages, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.externalpage.model.ExternalPage>
			getExternalPagesByUuidAndCompanyId(String uuid, long companyId) {

		return _externalPageLocalService.getExternalPagesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of external pages matching the UUID and company.
	 *
	 * @param uuid the UUID of the external pages
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching external pages, or an empty list if no matches were found
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.externalpage.model.ExternalPage>
			getExternalPagesByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.placecube.digitalplace.externalpage.model.ExternalPage>
						orderByComparator) {

		return _externalPageLocalService.getExternalPagesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of external pages.
	 *
	 * @return the number of external pages
	 */
	@Override
	public int getExternalPagesCount() {
		return _externalPageLocalService.getExternalPagesCount();
	}

	@Override
	public int getExternalPagesCount(long groupId) {
		return _externalPageLocalService.getExternalPagesCount(groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _externalPageLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _externalPageLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _externalPageLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
		publishExternalPage(
			com.placecube.digitalplace.externalpage.model.ExternalPage
				externalPage,
			boolean status) {

		return _externalPageLocalService.publishExternalPage(
			externalPage, status);
	}

	@Override
	public void removeExternalPagesByGroupId(long groupId) {
		_externalPageLocalService.removeExternalPagesByGroupId(groupId);
	}

	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
		unPublishExternalPage(
			com.placecube.digitalplace.externalpage.model.ExternalPage
				externalPage,
			boolean status) {

		return _externalPageLocalService.unPublishExternalPage(
			externalPage, status);
	}

	/**
	 * Updates the external page in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExternalPageLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param externalPage the external page
	 * @return the external page that was updated
	 */
	@Override
	public com.placecube.digitalplace.externalpage.model.ExternalPage
		updateExternalPage(
			com.placecube.digitalplace.externalpage.model.ExternalPage
				externalPage) {

		return _externalPageLocalService.updateExternalPage(externalPage);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _externalPageLocalService.getBasePersistence();
	}

	@Override
	public ExternalPageLocalService getWrappedService() {
		return _externalPageLocalService;
	}

	@Override
	public void setWrappedService(
		ExternalPageLocalService externalPageLocalService) {

		_externalPageLocalService = externalPageLocalService;
	}

	private ExternalPageLocalService _externalPageLocalService;

}