package com.placecube.digitalplace.group.rest.application.exceptions;

@SuppressWarnings("serial")
public class GroupRestException extends Exception {

	public GroupRestException(Exception e) {
		super(e);
	}

}
