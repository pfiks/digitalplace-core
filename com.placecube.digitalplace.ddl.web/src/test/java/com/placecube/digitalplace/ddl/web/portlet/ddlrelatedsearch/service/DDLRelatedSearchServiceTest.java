package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.search.document.Document;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.SearchRequestBuilderFactory;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;
import com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util.DDLRelatedSearchQueryUtil;
import com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util.DDLRelatedSearchUtil;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.model.DDLDisplayEntry;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class DDLRelatedSearchServiceTest {

	private static final long COMPANY_ID = 36;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private BooleanQuery mockBooleanQuery;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRelatedSearchQueryUtil mockDDLRelatedSearchQueryUtil;

	@Mock
	private DDLRelatedSearchUtil mockDDLRelatedSearchUtil;

	@Mock
	private Document mockDocument1;

	@Mock
	private Document mockDocument2;

	@Mock
	private DynamicDataListConfigurationService mockDynamicDataListConfigurationService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private List<Document> mockDocumentList;

	@Mock
	private List<DynamicDataListMapping> mockDynamicDataListMappings;

	@Mock
	private Portal mockPortal;

	@Mock
	private Queries mockQueries;

	@Mock
	private SearchRequestBuilder mockSearchRequestBuilder;

	@Mock
	private SearchRequestBuilderFactory mockSearchRequestBuilderFactory;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private DDLRelatedSearchService ddlRelatedSearchService;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void getDDLDisplayEntryResults_WhenNoExceptions_ThenReturnsDDLDisplayEntryList() {
		List<Document> documents = Arrays.asList(mockDocument1, mockDocument2);

		long ddlRecordId1 = 3424;
		long ddlRecordId2 = 432;
		String templateString = "templateString";
		DDLDisplayEntry expectedItem = new DDLDisplayEntry(ddlRecordId1, templateString);

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDynamicDataListConfigurationService.getDynamicDataListMappings(COMPANY_ID)).thenReturn(mockDynamicDataListMappings);

		when(mockDocument1.getLong(Field.ENTRY_CLASS_PK)).thenReturn(ddlRecordId1);
		when(mockDocument2.getLong(Field.ENTRY_CLASS_PK)).thenReturn(ddlRecordId2);

		when(mockDDLRelatedSearchUtil.getDDLRecordTemplateString(ddlRecordId1, mockDynamicDataListMappings, mockThemeDisplay, mockHttpServletRequest)).thenReturn(Optional.of(templateString));
		when(mockDDLRelatedSearchUtil.getDDLRecordTemplateString(ddlRecordId2, mockDynamicDataListMappings, mockThemeDisplay, mockHttpServletRequest)).thenReturn(Optional.empty());

		when(mockDDLRelatedSearchUtil.createDDLDisplayEntry(ddlRecordId1, templateString)).thenReturn(expectedItem);

		List<Object> results = ddlRelatedSearchService.getDDLDisplayEntryResults(documents, mockThemeDisplay, mockHttpServletRequest);

		assertTrue(results.contains(expectedItem));
		assertEquals(1, results.size());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSearchResultDocuments_WhenNoExceptionsAndAssetEntryNotNull_ThenConfiguresSearchAndReturnsDocumentList(boolean isAssetEntry) throws PortalException {
		String ddlRecordTitle = "ddlRecordTitle";
		long primaryKey = 43242;
		int maxItems = 3;

		when(mockPortal.getCompanyId(mockHttpServletRequest)).thenReturn(COMPANY_ID);
		when(mockDDLRelatedSearchUtil.getDDLRecordTitle(mockDDLRecord)).thenReturn(ddlRecordTitle);
		when(mockDDLRecord.getPrimaryKey()).thenReturn(primaryKey);
		when(mockAssetEntryLocalService.fetchEntry(DDLRecord.class.getName(), primaryKey)).thenReturn(isAssetEntry ? mockAssetEntry : null);

		when(mockSearchRequestBuilderFactory.builder()).thenReturn(mockSearchRequestBuilder);

		when(mockSearchRequestBuilder.query(mockBooleanQuery)).thenReturn(mockSearchRequestBuilder);
		when(mockQueries.booleanQuery()).thenReturn(mockBooleanQuery);
		when(mockDDLRelatedSearchQueryUtil.getResultsFromSearchBuilder(mockSearchRequestBuilder, mockBooleanQuery, maxItems)).thenReturn(mockDocumentList);

		List<Document> result = ddlRelatedSearchService.getSearchResultDocuments(mockHttpServletRequest, mockDDLRecord, maxItems);

		assertThat(result, sameInstance(mockDocumentList));
		verify(mockDDLRelatedSearchQueryUtil, times(1)).configureSearchContext(mockSearchRequestBuilder, COMPANY_ID);
		verify(mockDDLRelatedSearchQueryUtil, times(1)).configureRecordAndStructureId(mockBooleanQuery, mockDDLRecord);
		verify(mockDDLRelatedSearchQueryUtil, times(1)).configureKeywordsFromTitle(mockBooleanQuery, ddlRecordTitle);
		if (isAssetEntry) {
			verify(mockDDLRelatedSearchQueryUtil, times(1)).configureAssetTagIds(mockBooleanQuery, mockAssetEntry);
			verify(mockDDLRelatedSearchQueryUtil, times(1)).configureCategoryIds(mockBooleanQuery, mockAssetEntry);
		}
	}

	@Test (expected = PortalException.class)
	public void getSearchResultDocuments_WhenExceptionConfiguringRecordAndStructureId_ThenThrowsPortalException() throws PortalException {
		String ddlRecordTitle = "ddlRecordTitle";
		long primaryKey = 43242;
		int maxItems = 3;

		when(mockPortal.getCompanyId(mockHttpServletRequest)).thenReturn(COMPANY_ID);
		when(mockDDLRelatedSearchUtil.getDDLRecordTitle(mockDDLRecord)).thenReturn(ddlRecordTitle);
		when(mockDDLRecord.getPrimaryKey()).thenReturn(primaryKey);
		when(mockAssetEntryLocalService.fetchEntry(DDLRecord.class.getName(), primaryKey)).thenReturn(mockAssetEntry);

		when(mockSearchRequestBuilderFactory.builder()).thenReturn(mockSearchRequestBuilder);

		when(mockSearchRequestBuilder.query(mockBooleanQuery)).thenReturn(mockSearchRequestBuilder);
		when(mockQueries.booleanQuery()).thenReturn(mockBooleanQuery);
		doThrow(PortalException.class).when(mockDDLRelatedSearchQueryUtil).configureRecordAndStructureId(mockBooleanQuery, mockDDLRecord);

		ddlRelatedSearchService.getSearchResultDocuments(mockHttpServletRequest, mockDDLRecord, maxItems);
	}
}
