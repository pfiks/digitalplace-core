package com.placecube.digitalplace.search.sortby.mostactive.indexer;

import java.util.Date;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.search.sortby.mostactive.constants.MostActiveConstants;
import com.placecube.digitalplace.search.sortby.mostactive.service.MostActiveService;

@Component(immediate = true, property = "indexer.class.name=com.liferay.portal.kernel.model.User", service = ModelDocumentContributor.class)
public class MostActiveUserModelDocumentContributor implements ModelDocumentContributor<User> {

	private static final Log LOG = LogFactoryUtil.getLog(MostActiveUserModelDocumentContributor.class);

	@Reference
	private MostActiveService mostActiveService;

	@Override
	public void contribute(Document document, User user) {
		if (!user.isGuestUser()) {
			document.addKeyword(MostActiveConstants.COUNTER_FIELD_NAME, mostActiveService.getActivitiesCount(user));
			Date lastLoginDate = user.getLastLoginDate();
			document.addDate(MostActiveConstants.LAST_LOGIN_DATE_FIELD_NAME, lastLoginDate);

			LOG.debug("Indexing userId: " + user.getUserId() + " with sort by most active field - " + document.toString());
		}
	}

}