package com.placecube.digitalplace.user.settings.web.twofactor.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.constants.PortletKeys;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.USER_SETTINGS,
		"mvc.command.name=" + TwoFactorAuthenticationKeys.GENERATE_TWO_FACTOR_AUTHENTICATION }, service = MVCActionCommand.class)
public class GenerateTwoFactorAuthenticationMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private UserTwoFactorAuthenticationLocalService userTwoFactorAuthenticationLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {
		String secretKey = userTwoFactorAuthenticationLocalService.generateSecretKey();
		actionRequest.setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY, secretKey);
		actionRequest.setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, Boolean.TRUE);
		actionRequest.setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, Boolean.TRUE);
		hideDefaultSuccessMessage(actionRequest);
		actionResponse.getRenderParameters().setValue(PortletRequestKeys.CURRENT_SECTION_ID, TwoFactorAuthenticationKeys.BUNDLE_ID);
	}

}
