package com.placecube.digitalplace.group.rest.internal.upgrade;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.group.rest.internal.upgrade.upgrade_1_0_1.AddCompanyIdColumnUpdate;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = com.liferay.portal.upgrade.registry.UpgradeStepRegistrator.class)
public class GroupRestUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("1.0.0", "1.0.1", new AddCompanyIdColumnUpdate());
	}
}