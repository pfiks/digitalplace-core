package com.placecube.digitalplace.fileupload.service;

import java.io.InputStream;
import java.util.Optional;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.fileupload.exception.FileUploadException;

public interface FileUploadService {

	/**
	 * Stores the file at the provided location.
	 *
	 * @param name Name of file.
	 * @param path Path of location to store the file.
	 * @param file File inputstream.
	 * @param connectorId ID of connector to use. If null or blank then will use
	 *            default connector
	 * @param serviceContext Provider of additional fields.
	 * @throws FileUploadException If an error occurs storing the file.
	 * @throws ConfigurationException If an error occurs retrieving
	 *             Configuration.
	 */
	public void uploadFile(String name, String path, InputStream file, Optional<String> connectorId, ServiceContext serviceContext) throws FileUploadException, ConfigurationException;

}
