package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Locale;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.NoSuchLayoutException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.LocationSearchParameters;
import com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch.configuration.DDLLocationSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class, PrefsPropsUtil.class, GetterUtil.class, LanguageUtil.class })
public class DDLLocationSearchPortletTest extends PowerMockito {

	private static final long COMPANY_ID = 20L;

	private static final String DESTINATION_FRIENDLY_URL = "/URL";

	private static final String FORMATTED_ADDRESS = "Address";

	private static final String GOOGLE_MAPS_API_KEY_VALUE = "abcde1234";

	private static final long SCOPE_GROUP_ID = 10L;

	@Mock
	private Layout mockDestinationLayout;

	@Mock
	private Layout mockCurrentPageLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private DDLLocationSearchPortletInstanceConfiguration mockConfiguration;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Group mockGroup;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private DDLLocationSearchGoogleAPIKeyHelper mockDDLLocationSearchGoogleAPIKeyHelper;

	@Mock
	private DDLConfigurationService mockDDLConfigurationService;

	@InjectMocks
	private DDLLocationSearchPortlet dDLLocationSearchPortlet;

	private final Locale locale = LocaleUtil.UK;

	@Before
	public void activateSetup() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));
		mockStatic(PrefsPropsUtil.class, GetterUtil.class, LanguageUtil.class);
		initMocks(this);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDDLConfigurationService.getDDLLocationSearchPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.destinationPage()).thenReturn(StringPool.BLANK);
		when(mockConfiguration.geoLocationField()).thenReturn(StringPool.BLANK);

		when(PrefsPropsUtil.getPreferences(COMPANY_ID)).thenReturn(mockPortletPreferences);
		when(mockDDLLocationSearchGoogleAPIKeyHelper.getGoogleMapsAPIKey(COMPANY_ID, mockGroup)).thenReturn(GOOGLE_MAPS_API_KEY_VALUE);

	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresTheIteratorUrlAsRequestAttribute() throws Exception {
		String expected = "expectedVal";
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.getFriendlyURL()).thenReturn(expected);

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", expected);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresGeoLocationFieldAsRequestAttribute() throws Exception {
		String geoLocationField = "location-field";
		long structureId = 99L;
		when(mockConfiguration.geoLocationField()).thenReturn(geoLocationField);
		when(mockConfiguration.ddmStructureId()).thenReturn(structureId);
		when(mockConfiguration.destinationPage()).thenReturn("fakeUrl");
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockLayoutLocalService.getFriendlyURLLayout(anyLong(), anyBoolean(), anyString())).thenReturn(mockCurrentPageLayout);

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(LocationSearchParameters.GEO_LOCATION_FIELD, "ddm__keyword__" + structureId + "__" + geoLocationField + "_geolocation_geoLocationsortable");
	}

	@Test
	public void render_WhenNoError_ThenConfiguresPlaceholderAsRequestAttribute() throws Exception {
		String placeHolder = "place-holder";
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(LanguageUtil.get(locale, "enter-a-location")).thenReturn(placeHolder);
		when(mockConfiguration.destinationPage()).thenReturn("fakeUrl");
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockLayoutLocalService.getFriendlyURLLayout(anyLong(), anyBoolean(), anyString())).thenReturn(mockCurrentPageLayout);

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("placeHolderText", placeHolder);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresFormattedAddressAsRequestAttribute() throws Exception {
		String expected = "Address";
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getSharedSearchSessionSingleValuedAttribute(LocationSearchParameters.FORMATTED_ADDRESS)).thenReturn(FORMATTED_ADDRESS);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockLayoutLocalService.getFriendlyURLLayout(anyLong(), anyBoolean(), anyString())).thenReturn(mockCurrentPageLayout);

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("formattedAddress", expected);
	}

	@Test
	public void render_WhenNoErrorAndGroupIsNotNull_ThenRetrievesGoogleApiKey() throws Exception {
		String expected = GOOGLE_MAPS_API_KEY_VALUE;
		when(mockDDLLocationSearchGoogleAPIKeyHelper.getGoogleMapsAPIKey(COMPANY_ID, mockGroup)).thenReturn(expected);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockLayoutLocalService.getFriendlyURLLayout(anyLong(), anyBoolean(), anyString())).thenReturn(mockCurrentPageLayout);

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("googleApiKey", expected);
	}

	@Test
	public void render_WhenNoErrorAndGroupIsNull_ThenRetrievesGoogleApiKey() throws Exception {
		String expected = GOOGLE_MAPS_API_KEY_VALUE;
		when(mockDDLLocationSearchGoogleAPIKeyHelper.getGoogleMapsAPIKey(COMPANY_ID, null)).thenReturn(expected);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockLayoutLocalService.getFriendlyURLLayout(anyLong(), anyBoolean(), anyString())).thenReturn(mockCurrentPageLayout);

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("googleApiKey", expected);
	}

	@Test
	public void render_WhenInvalidDestinationPageSet_ThenConfiguresInvalidConfigurationAsRequestAttribute() throws Exception {

		when(mockConfiguration.destinationPage()).thenReturn("fakeUrl");
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockLayoutLocalService.getFriendlyURLLayout(anyLong(), anyBoolean(), anyString())).thenThrow(new NoSuchLayoutException());

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
	}

	@Test
	public void render_WhenValidDestinationPageSet_ThenConfiguresDestinationPlidnAsRequestAttribute() throws Exception {
		long expected = 100L;

		when(mockConfiguration.destinationPage()).thenReturn(DESTINATION_FRIENDLY_URL);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(true);

		when(mockLayoutLocalService.getFriendlyURLLayout(SCOPE_GROUP_ID, true, DESTINATION_FRIENDLY_URL)).thenReturn(mockDestinationLayout);
		when(mockDestinationLayout.getPlid()).thenReturn(expected);

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("destinationPlid", expected);
	}

	@Test
	public void render_WhenDestinationPageSet_ThenSearchIsNotExecuted() throws Exception {

		when(mockConfiguration.destinationPage()).thenReturn("fakeUrl");
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockLayoutLocalService.getFriendlyURLLayout(anyLong(), anyBoolean(), anyString())).thenThrow(new NoSuchLayoutException());

		dDLLocationSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockSharedSearchRequestService, times(0)).search(mockRenderRequest);
	}

}