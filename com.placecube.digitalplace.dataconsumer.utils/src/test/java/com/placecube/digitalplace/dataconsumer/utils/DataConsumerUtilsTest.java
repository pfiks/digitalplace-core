package com.placecube.digitalplace.dataconsumer.utils;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldTypeServicesRegistry;
import com.liferay.dynamic.data.mapping.form.field.type.DDMFormFieldValueRenderer;
import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceService;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.oauth2.provider.model.OAuth2Application;
import com.liferay.oauth2.provider.service.OAuth2ApplicationLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.model.DataProviderInputParameter;
import com.placecube.digitalplace.dataconsumer.service.DataTransformationService;
import com.placecube.digitalplace.oauth.service.DigitalPlaceOAuth2Service;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class DataConsumerUtilsTest {

	private static final long COMPANY_ID = 1233L;

	private static final String CLIENT_ID = "43656433";

	private static final String CLIENT_SECRET = "98732847-adsa32142-asd464h";

	private static final String DATA_CONSUMER_FIELD_NAME = "dataConsumerField";

	private static final String DATA_CONSUMER_HEADERS = "headers";

	private static final long DDM_DATA_PROVIDER_INSTANCE_ID = 213L;

	private static final String FORM_FIELD_NAME = "formField";

	private static final String FORM_FIELD_OPTION_REFERENCE = "formFieldOptionReference";

	private static final String FORM_FIELD_VALUE = "formFieldValue";

	private static final Locale LOCALE = Locale.UK;

	private static final String OAUT_APPLICATION_EXTERNAL_REFERENCE_CODE = "43532546";

	private static final String TOKEN = "8665432";

	private static final String USER_EMAIL_ADDRESS = "user@liferay.com";

	private static final String USER_NAME = "user";

	private static final String USER_PASSWORD = "pwd";

	@InjectMocks
	private DataConsumerUtils dataConsumerUtils;

	@Mock
	private JSONArray finalOptionRefJsonArray;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private DataConsumer mockDataConsumer;

	@Mock
	private DataProviderInputParameter mockDataProviderInputParameter;

	@Mock
	private DataTransformationService mockDataTransformationService;

	@Mock
	private DDMDataProviderInstance mockDDMDataProviderInstance;

	@Mock
	private DDMDataProviderInstanceService mockDDMDataProviderInstanceService;

	@Mock
	private DDMFormField mockDDMFormField;

	@Mock
	private DDMFormFieldOptions mockDDMFormFieldOptions;

	@Mock
	private DDMFormFieldTypeServicesRegistry mockDDMFormFieldTypeServicesRegistry;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValueOne;

	@Mock
	private DigitalPlaceOAuth2Service mockDigitalPlaceOAuth2Service;

	@Mock
	private DDMFormFieldValueRenderer mockDDMFormFieldValueRenderer;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValueTwo;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private JSONArray mockJsonArray;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONArray mockOptionRefJsonArray;

	@Mock
	private OAuth2ApplicationLocalService mockOAuth2ApplicationLocalService;

	@Mock
	private OAuth2Application mockOAuth2Application;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private Value mockValueOne;

	@Mock
	private Value mockValueTwo;

	@Test(expected = PortalException.class)
	public void getDDMFormValues_WhenDataProviderInstanceIsNotFound_ThenThrowsPortalException() throws Exception {
		when(mockDataConsumer.getDdmDataProviderInstanceId()).thenReturn(DDM_DATA_PROVIDER_INSTANCE_ID);
		when(mockDDMDataProviderInstanceService.getDataProviderInstance(DDM_DATA_PROVIDER_INSTANCE_ID)).thenThrow(new PortalException());

		dataConsumerUtils.getDDMFormValues(mockDataConsumer);
	}

	@Test
	public void getDDMFormValues_WhenNoErrors_ThenReturnsConsumerDataProviderFormValues() throws Exception {
		when(mockDataConsumer.getDdmDataProviderInstanceId()).thenReturn(DDM_DATA_PROVIDER_INSTANCE_ID);
		when(mockDDMDataProviderInstanceService.getDataProviderInstance(DDM_DATA_PROVIDER_INSTANCE_ID)).thenReturn(mockDDMDataProviderInstance);
		when(mockDataTransformationService.getDataProviderFormValues(mockDDMDataProviderInstance)).thenReturn(mockDDMFormValues);

		DDMFormValues result = dataConsumerUtils.getDDMFormValues(mockDataConsumer);

		assertThat(result, sameInstance(mockDDMFormValues));
	}

	@Test(expected = JSONException.class)
	public void getHeaders_WhenDataConsumerDeserialisationFails_ThenThrowsJSONException() throws PortalException {
		mockDataConsumer();

		when(mockDataTransformationService.deserializeMapping(DATA_CONSUMER_HEADERS)).thenThrow(new JSONException());

		dataConsumerUtils.getHeaders(mockDataConsumer, LOCALE, mockDDMFormValues);
	}

	@Test
	public void getHeaders_WhenDataConsumerHeaderContainsAuthorisation_ThenDoesNotChangeAuthorisationHeader() throws PortalException {
		final String authorizationValue = "Basic";

		mockBasicAuthDataProviderForm(USER_NAME);
		mockDataConsumer();

		Map<String, String> dataConsumerHeadersMap = new HashMap<>();
		dataConsumerHeadersMap.put("Authorization", authorizationValue);
		when(mockDataTransformationService.deserializeMapping(DATA_CONSUMER_HEADERS)).thenReturn(dataConsumerHeadersMap);

		Map<String, String> headersMap = dataConsumerUtils.getHeaders(mockDataConsumer, LOCALE, mockDDMFormValues);

		assertThat(headersMap.get("Authorization"), equalTo(authorizationValue));
	}

	@Test
	public void getHeaders_WhenDataConsumerHeaderDoesNotContainAuthorisationAndDataProviderFieldsContainsUserName_ThenAddsBasicAuthorisationHeader() throws PortalException {
		mockBasicAuthDataProviderForm(USER_NAME);
		mockDataConsumer();

		when(mockDataTransformationService.deserializeMapping(DATA_CONSUMER_HEADERS)).thenReturn(new HashMap<>());

		Map<String, String> headersMap = dataConsumerUtils.getHeaders(mockDataConsumer, LOCALE, mockDDMFormValues);

		String encodedAuthValue = Base64.getEncoder().encodeToString((USER_NAME + ":" + USER_PASSWORD).getBytes(StandardCharsets.UTF_8));

		assertThat(headersMap.get("Authorization"), equalTo("Basic " + encodedAuthValue));
	}

	@Test
	public void getHeaders_WhenDataConsumerHeaderDoesNotContainAuthorisationAndUserNameIsEmptyAndDataProviderContainsOAuthApplicationExternalReferenceCode_ThenAddsOAuthAuthorisationHeader()
			throws PortalException {
		mockOAuth2DataProviderForm(OAUT_APPLICATION_EXTERNAL_REFERENCE_CODE);
		mockDataConsumer();

		when(mockDataTransformationService.deserializeMapping(DATA_CONSUMER_HEADERS)).thenReturn(new HashMap<>());

		Map<String, String> headersMap = dataConsumerUtils.getHeaders(mockDataConsumer, LOCALE, mockDDMFormValues);

		assertThat(headersMap.get("Authorization"), equalTo("Bearer " + TOKEN));
	}

	@Test
	public void getHeaders_WhenDataProviderUserNameIsEmptyAndOAuthAppReferenceCodeIsEmpty_ThenDoesNotAddAuthorisationHeader() throws PortalException {
		mockBasicAuthDataProviderForm(StringPool.BLANK);
		mockDataConsumer();

		when(mockDataTransformationService.deserializeMapping(DATA_CONSUMER_HEADERS)).thenReturn(new HashMap<>());

		Map<String, String> headersMap = dataConsumerUtils.getHeaders(mockDataConsumer, LOCALE, mockDDMFormValues);

		assertThat(headersMap.get("Authorization"), nullValue());
	}

	@Test(expected = JSONException.class)
	public void getInputParametersMapping_WhenMappinsDeserialisationFails_ThenThrowsJSONException() throws JSONException {
		final String dataConsumerMappings = "mappings";

		List<DataProviderInputParameter> inputParameters = new ArrayList<>();
		inputParameters.add(mockDataProviderInputParameter);

		when(mockDataTransformationService.getInputParameters(mockDDMFormValues, LOCALE)).thenReturn(inputParameters);
		when(mockDataConsumer.getMapping()).thenReturn(dataConsumerMappings);
		when(mockDataTransformationService.deserializeMapping(dataConsumerMappings)).thenThrow(new JSONException());

		dataConsumerUtils.getInputParametersMapping(mockDataConsumer, LOCALE, mockDDMFormValues);
	}

	@Test
	public void getInputParametersMapping_WhenNoErrors_ThenGetsDataProviderInputParameterToDataConsumerMappings() throws JSONException {
		final String dataConsumerMappings = "mappings";
		final String inputParameterName = "inputId";
		final String inputParameterMapping = "inputMapping";

		List<DataProviderInputParameter> inputParameters = new ArrayList<>();
		inputParameters.add(mockDataProviderInputParameter);

		Map<String, String> dataConsumerMappedFields = new HashMap<>();
		dataConsumerMappedFields.put(inputParameterName, inputParameterMapping);

		when(mockDataTransformationService.getInputParameters(mockDDMFormValues, LOCALE)).thenReturn(inputParameters);
		when(mockDataConsumer.getMapping()).thenReturn(dataConsumerMappings);
		when(mockDataTransformationService.deserializeMapping(dataConsumerMappings)).thenReturn(dataConsumerMappedFields);
		when(mockDataProviderInputParameter.getName()).thenReturn(inputParameterName);

		Map<String, String> result = dataConsumerUtils.getInputParametersMapping(mockDataConsumer, LOCALE, mockDDMFormValues);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(inputParameterName), equalTo(inputParameterMapping));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenFieldTypeIsSelect_ThenReturnOptionReferenceFromStructure() throws PortalException {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String[]> parametersMap = new HashMap<>();
		parametersMap.put(DataConsumerUtils.FORM_FIELD_GROUP_PREFIX + DataConsumerUtils.FORM_FIELD_GROUP_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX, new String[] {});

		when(mockActionRequest.getParameterMap()).thenReturn(parametersMap);
		when(ParamUtil.getString(mockActionRequest,
				DataConsumerUtils.FORM_FIELD_GROUP_PREFIX + DataConsumerUtils.FORM_FIELD_GROUP_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX)).thenReturn(FORM_FIELD_VALUE);
		when(mockDDMStructure.getDDMFormField(FORM_FIELD_NAME)).thenReturn(mockDDMFormField);
		when(mockDDMFormField.getType()).thenReturn(DDMFormFieldTypeConstants.SELECT);
		when(mockDDMFormField.getDDMFormFieldOptions()).thenReturn(mockDDMFormFieldOptions);
		when(mockDDMFormFieldOptions.getOptionReference(FORM_FIELD_VALUE)).thenReturn(FORM_FIELD_OPTION_REFERENCE);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, mockActionRequest, mockDDMStructure);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(FORM_FIELD_OPTION_REFERENCE));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenFieldTypeHasOptionsAndValueIsEmpty_ThenSetsEmptyFieldValue() throws PortalException {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String[]> parametersMap = new HashMap<>();
		parametersMap.put(DataConsumerUtils.FORM_FIELD_GROUP_PREFIX + DataConsumerUtils.FORM_FIELD_GROUP_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX, new String[] {});

		when(mockActionRequest.getParameterMap()).thenReturn(parametersMap);
		when(ParamUtil.getString(mockActionRequest,
				DataConsumerUtils.FORM_FIELD_GROUP_PREFIX + DataConsumerUtils.FORM_FIELD_GROUP_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX)).thenReturn(StringPool.BLANK);
		when(mockDDMStructure.getDDMFormField(FORM_FIELD_NAME)).thenReturn(mockDDMFormField);
		when(mockDDMFormField.getType()).thenReturn(DDMFormFieldTypeConstants.SELECT);
		when(mockDDMFormField.getDDMFormFieldOptions()).thenReturn(mockDDMFormFieldOptions);
		when(mockDDMFormFieldOptions.getOptionReference(StringPool.BLANK)).thenReturn(null);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, mockActionRequest, mockDDMStructure);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(StringPool.BLANK));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenFormContainsFieldWithConsumerFieldName_ThenAddsFormFieldValueToMap() {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		mockFormFieldValues(FORM_FIELD_VALUE);
		when(mockDDMFormFieldValueOne.getType()).thenReturn(DDMFormFieldTypeConstants.TEXT);
		when(mockDDMFormFieldValueRenderer.render(mockDDMFormFieldValueOne, LOCALE)).thenReturn(FORM_FIELD_VALUE);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, LOCALE, mockDDMFormValues);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(FORM_FIELD_VALUE));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenFormContainsNestedFieldWithConsumerFieldName_ThenAddsFormFieldValueToMap() {
		String nestedFieldName = "Nested";
		String nestedValue = "NestedValue";
		Map<String, String> confirmedMapping = new HashMap<>();
		confirmedMapping.put(nestedFieldName, nestedFieldName);

		List<DDMFormFieldValue> ddmFormFieldValues = new ArrayList<>();
		ddmFormFieldValues.add(mockDDMFormFieldValueOne);
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(ddmFormFieldValues);

		DDMFormFieldValue nestedDDMFormFieldValue = mock(DDMFormFieldValue.class);
		when(nestedDDMFormFieldValue.getName()).thenReturn(nestedFieldName);
		when(nestedDDMFormFieldValue.getValue()).thenReturn(mockValueOne);
		when(mockDDMFormFieldValueOne.getType()).thenReturn(DDMFormFieldTypeConstants.TEXT);
		when(mockDDMFormFieldTypeServicesRegistry.getDDMFormFieldValueRenderer(DDMFormFieldTypeConstants.TEXT)).thenReturn(mockDDMFormFieldValueRenderer);
		when(mockDDMFormFieldValueOne.getName()).thenReturn(nestedFieldName);
		when(mockDDMFormFieldValueRenderer.render(any(), any())).thenReturn(nestedValue);
		when(mockDDMFormFieldValueOne.getNestedDDMFormFieldValues()).thenReturn(Arrays.asList(nestedDDMFormFieldValue));

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, LOCALE, mockDDMFormValues);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(nestedFieldName), equalTo(nestedValue));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenFormDoesNotContainFieldWithConsumerFieldName_ThenDoesNotAddFormFieldValueToMap() {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(new ArrayList<>());

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, LOCALE, mockDDMFormValues);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenFormFieldTypeIsSelectWithMultipleOptionsSelected_ThenConvertValueToDelimitedString() {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();
		String valueOne = "val1";
		String valueTwo = "val2";
		String joinedString = valueOne + "," + valueTwo;
		String expectedString = joinedString.replace(StringPool.COMMA, StringPool.SEMICOLON);
		mockFormFieldValues(joinedString);
		when(mockDDMFormFieldValueOne.getType()).thenReturn(DDMFormFieldTypeConstants.SELECT);
		when(mockDDMFormFieldValueOne.getDDMFormField()).thenReturn(mockDDMFormField);
		when(mockDDMFormFieldTypeServicesRegistry.getDDMFormFieldValueRenderer(DDMFormFieldTypeConstants.SELECT)).thenReturn(mockDDMFormFieldValueRenderer);
		when(mockDDMFormFieldValueRenderer.render(mockDDMFormFieldValueOne, LOCALE)).thenReturn(joinedString);
		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, LOCALE, mockDDMFormValues);
		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(expectedString));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenFormFieldTypeIsSelectWithOneOptionSelected_ThenConvertValueToDelimitedString() {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();
		String valueOne = "val1";
		mockFormFieldValues(valueOne);
		when(mockDDMFormFieldValueOne.getType()).thenReturn(DDMFormFieldTypeConstants.SELECT);
		when(mockDDMFormFieldTypeServicesRegistry.getDDMFormFieldValueRenderer(DDMFormFieldTypeConstants.SELECT)).thenReturn(mockDDMFormFieldValueRenderer);
		when(mockDDMFormFieldValueRenderer.render(mockDDMFormFieldValueOne, LOCALE)).thenReturn(valueOne);
		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, LOCALE, mockDDMFormValues);
		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(valueOne));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenFormFieldValueIsNull_ThenAddsTheFormFieldValueAsEmptyStringInMap() {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		mockFormFieldValues(null);
		when(mockDDMFormFieldValueOne.getType()).thenReturn(DDMFormFieldTypeConstants.TEXT);
		when(mockDDMFormFieldTypeServicesRegistry.getDDMFormFieldValueRenderer(DDMFormFieldTypeConstants.TEXT)).thenReturn(mockDDMFormFieldValueRenderer);
		when(mockDDMFormFieldValueRenderer.render(any(), any())).thenReturn(StringPool.BLANK);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, LOCALE, mockDDMFormValues);

		assertNull(result.get(FORM_FIELD_NAME));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenIssueWhileGettingDDMFormFieldByFieldNameFromStructure_ThenReturnValueFromRequestParameter() throws PortalException {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String[]> parametersMap = new HashMap<>();
		parametersMap.put(DataConsumerUtils.FORM_FIELD_GROUP_PREFIX + DataConsumerUtils.FORM_FIELD_GROUP_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX, new String[] {});

		when(mockActionRequest.getParameterMap()).thenReturn(parametersMap);
		when(ParamUtil.getString(mockActionRequest,
				DataConsumerUtils.FORM_FIELD_GROUP_PREFIX + DataConsumerUtils.FORM_FIELD_GROUP_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX)).thenReturn(FORM_FIELD_VALUE);
		when(mockDDMStructure.getDDMFormField(FORM_FIELD_NAME)).thenThrow(PortalException.class);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, mockActionRequest, mockDDMStructure);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(FORM_FIELD_VALUE));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenRequestContainsFieldGroupFormFieldValueForConsumerFieldName_ThenAddsFormFieldValueToMap() throws PortalException {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String[]> parametersMap = new HashMap<>();
		parametersMap.put(DataConsumerUtils.FORM_FIELD_GROUP_PREFIX + DataConsumerUtils.FORM_FIELD_GROUP_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX, new String[] {});

		when(mockActionRequest.getParameterMap()).thenReturn(parametersMap);
		when(ParamUtil.getString(mockActionRequest,
				DataConsumerUtils.FORM_FIELD_GROUP_PREFIX + DataConsumerUtils.FORM_FIELD_GROUP_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX)).thenReturn(FORM_FIELD_VALUE);
		when(mockDDMStructure.getDDMFormField(FORM_FIELD_NAME)).thenReturn(mockDDMFormField);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, mockActionRequest, mockDDMStructure);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(FORM_FIELD_VALUE));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenRequestContainsFormFieldValueForConsumerFieldName_ThenAddsFormFieldValueToMap() throws PortalException {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String[]> parametersMap = new HashMap<>();
		parametersMap.put(DataConsumerUtils.FORM_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX, new String[] {});

		when(mockActionRequest.getParameterMap()).thenReturn(parametersMap);
		when(ParamUtil.getString(mockActionRequest, DataConsumerUtils.FORM_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX)).thenReturn(FORM_FIELD_VALUE);
		when(mockDDMStructure.getDDMFormField(FORM_FIELD_NAME)).thenReturn(mockDDMFormField);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, mockActionRequest, mockDDMStructure);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(FORM_FIELD_VALUE));
	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenRequestContainsFormFieldValueForConsumerFieldNameAndFormValueInRequestIsNull_ThenNotAddFormFieldValueToMap() {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String[]> parametersMap = new HashMap<>();
		parametersMap.put(DataConsumerUtils.FORM_VALUE_PREFIX + FORM_FIELD_NAME + DataConsumerUtils.FORM_VALUE_SUFFIX, new String[] {});

		when(mockActionRequest.getParameterMap()).thenReturn(parametersMap);
		when(ParamUtil.getString(mockActionRequest, DataConsumerUtils.FORM_VALUE_PREFIX + FORM_FIELD_NAME)).thenReturn(null);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, mockActionRequest, mockDDMStructure);

		assertThat(result.isEmpty(), equalTo(true));

	}

	@Test
	public void mapDataProviderFieldsToFormValues_WhenRequestDoesNotContainFormFieldValueForDataConsumerFieldName_ThenNotAddFormFieldValueToMap() {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		when(mockActionRequest.getParameterMap()).thenReturn(new HashMap<>());

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, mockActionRequest, mockDDMStructure);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void mapDataProviderFieldsToResponseValues_WhenConsumerFieldValueInJsonIsNotNull_ThenReturnsConsumerFieldToJsonValueMapping() throws Exception {
		final String jsonResponse = "{field=value}";

		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String> jsonDeserialisedMapping = new HashMap<>();
		jsonDeserialisedMapping.put(FORM_FIELD_NAME, FORM_FIELD_VALUE);

		when(mockDataTransformationService.deserializeMapping(jsonResponse)).thenReturn(jsonDeserialisedMapping);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToResponseValues(confirmedMapping, jsonResponse, mockActionRequest);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(FORM_FIELD_VALUE));
	}

	@Test
	public void mapDataProviderFieldsToResponseValues_WhenConsumerFieldValueInJsonIsNull_ThenDoesNotReturnConsumerFieldToJsonValueMapping() throws Exception {
		final String jsonResponse = "{field=value}";

		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String> jsonDeserialisedMapping = new HashMap<>();
		jsonDeserialisedMapping.put(FORM_FIELD_NAME, null);

		when(mockDataTransformationService.deserializeMapping(jsonResponse)).thenReturn(jsonDeserialisedMapping);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToResponseValues(confirmedMapping, jsonResponse, mockActionRequest);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void mapDataProviderFieldsToResponseValues_WhenFormFieldTypeIsSelectWithInvalidJSON_ThenTheOriginalValueIsReturned() throws JSONException {
		final String jsonResponse = "{field=value}";

		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String> jsonDeserialisedMapping = new HashMap<>();
		jsonDeserialisedMapping.put(FORM_FIELD_NAME, FORM_FIELD_VALUE);

		String valueOne = "val1";
		String valueOneWithQuotes = "\"" + valueOne + "\"";
		String fieldValueJson = "[" + valueOneWithQuotes + "]";
		mockFormFieldValues(fieldValueJson);
		when(mockJsonFactory.createJSONArray(fieldValueJson)).thenThrow(JSONException.class);
		when(mockDataTransformationService.deserializeMapping(jsonResponse)).thenReturn(jsonDeserialisedMapping);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToResponseValues(confirmedMapping, jsonResponse, LOCALE, mockDDMFormValues);

		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(fieldValueJson));
	}

	@Test
	public void mapDataProviderFieldsToResponseValues_WhenFormFieldTypeIsSelectWithMultipleOptionsSelected_ThenConvertValueToDelimitedString() throws JSONException {
		final String jsonResponse = "{field=value}";

		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String> jsonDeserialisedMapping = new HashMap<>();
		jsonDeserialisedMapping.put(FORM_FIELD_NAME, FORM_FIELD_VALUE);

		String valueOne = "val1";
		String valueOneWithQuotes = "\"" + valueOne + "\"";
		String valueTwo = "val1";
		String valueTwoWithQuotes = "\"" + valueTwo + "\"";
		String fieldValueJson = "[" + valueOneWithQuotes + "," + valueTwoWithQuotes + "]";
		String joinedString = valueOneWithQuotes + "," + valueTwoWithQuotes;
		String unquotedString = valueOne + "," + valueTwo;
		mockFormFieldValues(fieldValueJson);
		when(mockDDMFormFieldValueOne.getType()).thenReturn(DDMFormFieldTypeConstants.SELECT);
		when(mockValueOne.getString(LOCALE)).thenReturn(fieldValueJson);
		when(mockJsonFactory.createJSONArray(fieldValueJson)).thenReturn(mockJsonArray);
		when(mockJsonArray.join(StringPool.SEMICOLON)).thenReturn(joinedString);
		when(mockDataTransformationService.deserializeMapping(jsonResponse)).thenReturn(jsonDeserialisedMapping);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToResponseValues(confirmedMapping, jsonResponse, LOCALE, mockDDMFormValues);

		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(unquotedString));
	}

	@Test
	public void mapDataProviderFieldsToResponseValues_WhenFormFieldTypeIsSelectWithOneOptionSelected_ThenConvertValueToDelimitedString() throws Exception {
		final String jsonResponse = "{field=value}";

		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		Map<String, String> jsonDeserialisedMapping = new HashMap<>();
		jsonDeserialisedMapping.put(FORM_FIELD_NAME, FORM_FIELD_VALUE);

		String valueOne = "val1";
		String valueOneWithQuotes = "\"" + valueOne + "\"";
		String fieldValueJson = "[" + valueOne + "]";
		mockFormFieldValues(fieldValueJson);
		when(mockDDMFormFieldValueOne.getType()).thenReturn(DDMFormFieldTypeConstants.SELECT);
		when(mockValueOne.getString(LOCALE)).thenReturn(fieldValueJson);
		when(mockJsonFactory.createJSONArray(fieldValueJson)).thenReturn(mockJsonArray);
		when(mockJsonArray.join(StringPool.SEMICOLON)).thenReturn(valueOneWithQuotes);
		when(mockDataTransformationService.deserializeMapping(jsonResponse)).thenReturn(jsonDeserialisedMapping);

		Map<String, String> result = dataConsumerUtils.mapDataProviderFieldsToResponseValues(confirmedMapping, jsonResponse, LOCALE, mockDDMFormValues);

		assertThat(result.get(DATA_CONSUMER_FIELD_NAME), equalTo(valueOne));
	}

	@Test(expected = JSONException.class)
	public void mapDataProviderFieldsToResponseValues_WhenJsonDeserialisationFails_ThenThrowsJSonException() throws Exception {
		final String jsonResponse = "{field=value}";

		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();

		when(mockDataTransformationService.deserializeMapping(jsonResponse)).thenThrow(new JSONException());

		dataConsumerUtils.mapDataProviderFieldsToResponseValues(confirmedMapping, jsonResponse, mockActionRequest);
	}

	@Test
	public void replaceBodyPlaceholdersWithDataProviderFieldValues_WhenBodyContainsMapKeysPlaceholders_ThenReplacesPlaceholdersWithMapValues() {
		Map<String, String> confirmedMapping = new HashMap<>();
		confirmedMapping.put(DATA_CONSUMER_FIELD_NAME, FORM_FIELD_NAME);
		confirmedMapping.put("someOtherField", null);
		String result = dataConsumerUtils.replaceBodyPlaceholdersWithDataProviderFieldValues(confirmedMapping,
				"{\"param1\" : \"$" + DATA_CONSUMER_FIELD_NAME + "$\", \"param2\": \"$someOtherField$\"}");

		assertThat(result, equalTo("{\"param1\" : \"" + FORM_FIELD_NAME + "\", \"param2\": \"\"}"));
	}

	@Test
	public void replaceBodyPlaceholdersWithDataProviderFieldValues_WhenBodyIsNull_ThenReturnsNullBody() {
		Map<String, String> confirmedMapping = mockDataConsumerConfirmedMapping();
		String result = dataConsumerUtils.replaceBodyPlaceholdersWithDataProviderFieldValues(confirmedMapping, null);

		assertThat(result, nullValue());
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	private void mockDataConsumer() {
		when(mockDataConsumer.getHeaders()).thenReturn(DATA_CONSUMER_HEADERS);
		when(mockDataConsumer.getCompanyId()).thenReturn(COMPANY_ID);
	}

	private Map<String, String> mockDataConsumerConfirmedMapping() {
		Map<String, String> confirmedMapping = new HashMap<>();
		confirmedMapping.put(DATA_CONSUMER_FIELD_NAME, FORM_FIELD_NAME);

		return confirmedMapping;
	}

	private void mockBasicAuthDataProviderForm(String userName) throws PortalException {
		Map<String, List<DDMFormFieldValue>> dataProviderFormFieldValuesMap = new HashMap<>();
		dataProviderFormFieldValuesMap.put("username", Arrays.asList(mockDDMFormFieldValueOne));
		dataProviderFormFieldValuesMap.put("password", Arrays.asList(mockDDMFormFieldValueTwo));
		when(mockDDMFormFieldValueOne.getValue()).thenReturn(mockValueOne);
		when(mockDDMFormFieldValueTwo.getValue()).thenReturn(mockValueTwo);

		when(mockValueOne.getString(LOCALE)).thenReturn(userName);
		when(mockValueTwo.getString(LOCALE)).thenReturn(USER_PASSWORD);

		when(mockDDMFormValues.getDDMFormFieldValuesMap(false)).thenReturn(dataProviderFormFieldValuesMap);

		when(mockUserLocalService.getUserByScreenName(COMPANY_ID, USER_NAME)).thenReturn(mockUser);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL_ADDRESS);
	}

	private void mockOAuth2DataProviderForm(String oauthApplicationExternalReferenceCode) throws PortalException {
		Map<String, List<DDMFormFieldValue>> dataProviderFormFieldValuesMap = new HashMap<>();
		dataProviderFormFieldValuesMap.put("oauthApplicationExternalReferenceCode", Arrays.asList(mockDDMFormFieldValueOne));
		when(mockDDMFormFieldValueOne.getValue()).thenReturn(mockValueOne);

		when(mockValueOne.getString(LOCALE)).thenReturn(oauthApplicationExternalReferenceCode);

		when(mockDDMFormValues.getDDMFormFieldValuesMap(false)).thenReturn(dataProviderFormFieldValuesMap);

		when(mockOAuth2ApplicationLocalService.getOAuth2ApplicationByExternalReferenceCode(OAUT_APPLICATION_EXTERNAL_REFERENCE_CODE, COMPANY_ID)).thenReturn(mockOAuth2Application);
		when(mockOAuth2Application.getClientId()).thenReturn(CLIENT_ID);
		when(mockOAuth2Application.getClientSecret()).thenReturn(CLIENT_SECRET);
		when(mockDigitalPlaceOAuth2Service.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET)).thenReturn(TOKEN);
	}

	private void mockFormFieldValues(String value) {
		List<DDMFormFieldValue> ddmFormFieldValues = new ArrayList<>();
		ddmFormFieldValues.add(mockDDMFormFieldValueOne);

		when(mockDDMFormFieldValueOne.getName()).thenReturn(FORM_FIELD_NAME);
		when(mockDDMFormFieldValueOne.getValue()).thenReturn(mockValueOne);
		when(mockValueOne.getString(LOCALE)).thenReturn(value);
		when(mockDDMFormValues.getDDMFormFieldValues()).thenReturn(ddmFormFieldValues);
		when(mockDDMFormFieldTypeServicesRegistry.getDDMFormFieldValueRenderer(anyString())).thenReturn(mockDDMFormFieldValueRenderer);
	}

}
