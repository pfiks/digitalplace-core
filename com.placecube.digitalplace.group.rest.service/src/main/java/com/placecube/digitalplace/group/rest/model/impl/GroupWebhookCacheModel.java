/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.group.rest.model.GroupWebhook;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing GroupWebhook in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class GroupWebhookCacheModel
	implements CacheModel<GroupWebhook>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof GroupWebhookCacheModel)) {
			return false;
		}

		GroupWebhookCacheModel groupWebhookCacheModel =
			(GroupWebhookCacheModel)object;

		if (webhookId == groupWebhookCacheModel.webhookId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, webhookId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{webhookId=");
		sb.append(webhookId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", payloadURL=");
		sb.append(payloadURL);
		sb.append(", enabled=");
		sb.append(enabled);
		sb.append(", events=");
		sb.append(events);
		sb.append(", className=");
		sb.append(className);
		sb.append(", classPK=");
		sb.append(classPK);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public GroupWebhook toEntityModel() {
		GroupWebhookImpl groupWebhookImpl = new GroupWebhookImpl();

		groupWebhookImpl.setWebhookId(webhookId);
		groupWebhookImpl.setCompanyId(companyId);
		groupWebhookImpl.setGroupId(groupId);

		if (name == null) {
			groupWebhookImpl.setName("");
		}
		else {
			groupWebhookImpl.setName(name);
		}

		if (payloadURL == null) {
			groupWebhookImpl.setPayloadURL("");
		}
		else {
			groupWebhookImpl.setPayloadURL(payloadURL);
		}

		groupWebhookImpl.setEnabled(enabled);

		if (events == null) {
			groupWebhookImpl.setEvents("");
		}
		else {
			groupWebhookImpl.setEvents(events);
		}

		if (className == null) {
			groupWebhookImpl.setClassName("");
		}
		else {
			groupWebhookImpl.setClassName(className);
		}

		groupWebhookImpl.setClassPK(classPK);

		groupWebhookImpl.resetOriginalValues();

		return groupWebhookImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		webhookId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();
		name = objectInput.readUTF();
		payloadURL = objectInput.readUTF();

		enabled = objectInput.readBoolean();
		events = objectInput.readUTF();
		className = objectInput.readUTF();

		classPK = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(webhookId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (payloadURL == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(payloadURL);
		}

		objectOutput.writeBoolean(enabled);

		if (events == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(events);
		}

		if (className == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(className);
		}

		objectOutput.writeLong(classPK);
	}

	public long webhookId;
	public long companyId;
	public long groupId;
	public String name;
	public String payloadURL;
	public boolean enabled;
	public String events;
	public String className;
	public long classPK;

}