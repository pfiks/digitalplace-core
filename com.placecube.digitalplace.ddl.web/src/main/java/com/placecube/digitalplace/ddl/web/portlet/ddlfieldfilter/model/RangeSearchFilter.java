package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model;

import static com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model.ComparisonOperator.GT;
import static com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model.ComparisonOperator.GTE;
import static com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model.ComparisonOperator.LT;
import static com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model.ComparisonOperator.LTE;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.RangeTermFilter;

public class RangeSearchFilter implements SearchFilter {

	private BooleanFilter booleanFilter = new BooleanFilter();

	private RangeSearchFilter() {
	}

	public static RangeSearchFilter newInstance() {
		return new RangeSearchFilter();
	}

	@Override
	public BooleanClause<Filter> getFilterBooleanClause() {
		return BooleanClauseFactoryUtil.createFilter(booleanFilter, BooleanClauseOccur.MUST);
	}

	public void addUnboundedRange(String fieldName, String value, ComparisonOperator comparisonOperator, BooleanClauseOccur booleanClauseOccur) {

		String upperBoundValue = null;
		String lowerBoundValue = null;
		boolean includeLowerBound = true;
		boolean includeUpperBound = true;

		if (comparisonOperator == GTE || comparisonOperator == GT) {
			lowerBoundValue = value;
			includeLowerBound = comparisonOperator == GTE;
		}

		if (comparisonOperator == LTE || comparisonOperator == LT) {
			upperBoundValue = value;
			includeUpperBound = comparisonOperator == LTE;
		}

		RangeTermFilter rangeFilter = new RangeTermFilter(fieldName, includeLowerBound, includeUpperBound, lowerBoundValue, upperBoundValue);

		booleanFilter.add(rangeFilter, booleanClauseOccur);
	}
}
