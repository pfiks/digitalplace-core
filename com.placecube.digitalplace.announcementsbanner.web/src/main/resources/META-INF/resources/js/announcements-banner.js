
AUI.add(
	'announcements-banner',
	function(A) {

		var AnnouncementsBanner = {
				init: function() {
					var elements = document.getElementsByClassName('announcement-dismiss-button');
					for(var i = 0; i < elements.length; i++){
						elements[i].onclick = function() {
							$.ajax({
								type: 'POST',
								url: $(this).attr("href")
							});
						}
					}
					
				}
		};

		A.AnnouncementsBanner = AnnouncementsBanner;
	},
	'',
	{
		requires: [	
			'aui-base'
		]
	}
);

function displayAnnouncementsBanner() {
	let announcements = $('#p_p_id_com_placecube_digitalplace_announcements_banner_web_AnnouncementsBannerPortlet_').detach();
	if(!Liferay.ThemeDisplay.isControlPanel() && (announcements.length == 0 || !announcements.is(':visible'))){

    	var resourceURL = Liferay.PortletURL.createResourceURL();
    	resourceURL.setPortletId('com_placecube_digitalplace_announcements_banner_web_AnnouncementsBannerPortlet');
    	resourceURL.setResourceId('/announcements-banner/get-configuration');
    	$.get(
    		resourceURL.toString(),
    		{},
    		function(data) {
				announcements.addClass('container');
                let cssSelector = 'body';
                try{
                    var parsedData = JSON.parse(data);
                    var selector = parsedData.announcementsBannerPortletHTMLSelector;
                    if($(selector).length != 0){
                        cssSelector = selector;
                    }
                } catch(error) {}
                $(cssSelector).prepend(announcements);
                announcements.show();
            }
    	);

	}
}

Liferay.on('endNavigate', function(){
	displayAnnouncementsBanner()
});

Liferay.on('allPortletsReady', function(){
	displayAnnouncementsBanner()
});
