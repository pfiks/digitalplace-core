package com.placecube.digitalplace.appointment.booking.digitalplaceconnector;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.sql.dsl.expression.Predicate;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.model.AppointmentEntryTable;

@Component(immediate = true, service = DigitalPlaceAppointmentConnectorQueryUtil.class)
public class DigitalPlaceAppointmentConnectorQueryUtil {

	public boolean doesSlotOverlap(AppointmentBookingSlot toCheck, AppointmentBookingSlot other) {
		return toCheck.getEndDateTime().isAfter(other.getStartDateTime()) && toCheck.getStartDateTime().isBefore(other.getEndDateTime());
	}

	public Predicate getBasePredicateForAppointmentEntry(long companyId, String serviceId) {
		return AppointmentEntryTable.INSTANCE.companyId.eq(companyId)//
				.and(AppointmentEntryTable.INSTANCE.serviceId.eq(serviceId))//
				.and(AppointmentEntryTable.INSTANCE.connectorClassName.eq(DigitalPlaceAppointmentBookingConnector.class.getName()));
	}

}
