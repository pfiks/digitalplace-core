package com.placecube.digitalplace.form.service.impl.internal;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceVersion;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceVersionLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

@Component(immediate = true, service = DDMFormInstanceVersionService.class)
public class DDMFormInstanceVersionService {

	@Reference
	private CounterLocalService counterLocalService;

	@Reference
	private DDMFormInstanceVersionLocalService ddmFormInstanceVersionLocalService;

	public void addFormInstanceVersion(DDMStructure ddmStructure, User user, DDMFormInstance ddmFormInstance, String version, ServiceContext serviceContext) throws PortalException {

		long ddmStructureVersionId = ddmStructure.getStructureVersion().getStructureVersionId();

		long ddmFormInstanceVersionId = counterLocalService.increment();

		DDMFormInstanceVersion ddmFormInstanceVersion = ddmFormInstanceVersionLocalService.createDDMFormInstanceVersion(ddmFormInstanceVersionId);

		ddmFormInstanceVersion.setGroupId(ddmFormInstance.getGroupId());
		ddmFormInstanceVersion.setCompanyId(ddmFormInstance.getCompanyId());
		ddmFormInstanceVersion.setUserId(ddmFormInstance.getUserId());
		ddmFormInstanceVersion.setUserName(ddmFormInstance.getUserName());
		ddmFormInstanceVersion.setCreateDate(ddmFormInstance.getModifiedDate());
		ddmFormInstanceVersion.setFormInstanceId(ddmFormInstance.getFormInstanceId());
		ddmFormInstanceVersion.setStructureVersionId(ddmStructureVersionId);
		ddmFormInstanceVersion.setName(ddmFormInstance.getName());
		ddmFormInstanceVersion.setDescription(ddmFormInstance.getDescription());
		ddmFormInstanceVersion.setVersion(version);

		int status = GetterUtil.getInteger(serviceContext.getAttribute("status"), WorkflowConstants.STATUS_APPROVED);

		ddmFormInstanceVersion.setStatus(status);

		ddmFormInstanceVersion.setStatusByUserId(user.getUserId());
		ddmFormInstanceVersion.setStatusByUserName(user.getFullName());
		ddmFormInstanceVersion.setStatusDate(ddmFormInstance.getModifiedDate());

		ddmFormInstanceVersionLocalService.updateDDMFormInstanceVersion(ddmFormInstanceVersion);

	}
}
