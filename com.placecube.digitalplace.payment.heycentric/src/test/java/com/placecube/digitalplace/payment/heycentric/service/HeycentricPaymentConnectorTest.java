package com.placecube.digitalplace.payment.heycentric.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.openMocks;

import java.security.GeneralSecurityException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.osgi.framework.ServiceException;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.heycentric.configuration.HeycentricPaymentCompanyConfiguration;
import com.placecube.digitalplace.payment.heycentric.service.arp.HeycentricARPService;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentStatus;
import com.placecube.digitalplace.payment.heycentric.service.internal.HeycentricPaymentOrderFactory;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class HeycentricPaymentConnectorTest extends Mockito {

	private static final long COMPANY_ID = 1;

	private static final String ENDPOINT = "http://endpoint";
	private static final String EXTERNAL_REFERENCE_CODE = "erc";
	private static final String PAYLOAD = "payload";
	private static final String PAYMENT_REFERENCE = "paymentReference";

	@InjectMocks
	private HeycentricPaymentConnector heycentricPaymentConnector;

	@Mock
	private AgentReferredPaymentContext mockAgentReferredPaymentContext;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private HeycentricPaymentCompanyConfiguration mockHeycentricPaymentCompanyConfiguration;

	@Mock
	private HeycentricPaymentOrderFactory mockHeycentricPaymentOrderFactory;

	@Mock
	private HeycentricARPService mockHeycentricARPService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private PaymentContext mockPaymentContext;

	@Mock
	private PaymentResponse mockPaymentResponse;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObjectOne;

	@Mock
	private JSONObject mockJSONObjectTwo;

	@Before
	public void activateSetup() {
		openMocks(this);
	}

	@Test
	public void enabled_WhenError_ThenReturnsTheConfigurationEnabledFalse() throws ConfigurationException {

		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenThrow(ConfigurationException.class);

		boolean result = heycentricPaymentConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {

		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockHeycentricPaymentCompanyConfiguration);

		when(mockHeycentricPaymentCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = heycentricPaymentConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getBackURL_WhenNoError_ThenReturnRedirectWithParamValue() {
		String portalURL = "portalURL";
		String currentURL = "currentURL";
		when(mockServiceContext.getPortalURL()).thenReturn(portalURL);
		when(mockServiceContext.getCurrentURL()).thenReturn(currentURL);

		Optional<String> result = heycentricPaymentConnector.getBackURL(PAYMENT_REFERENCE, mockServiceContext);

		String expected = portalURL + currentURL;
		assertEquals(expected, result.get());
	}

	@Test
	@Parameters({ "", "null" })
	public void getPaymentStatus_WithIncorrectQueryString_ThenReturnPaymentErrorStatus(String queryString) {

		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getQueryString()).thenReturn(queryString);

		PaymentStatus result = heycentricPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		assertThat(result, equalTo(PaymentStatus.paymentError()));
	}

	@Test
	public void getPaymentStatus_WithQueryStringNotIncludedPaymentStatusSuccess_ThenReturnPaymentErrorStatus() {
		String queryString = "queryString?a=3&b=&";
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getQueryString()).thenReturn(queryString);

		PaymentStatus result = heycentricPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		assertThat(result, equalTo(PaymentStatus.paymentError()));
	}

	@Test
	public void getPaymentStatus_WithQueryStringPaymentStatusSuccess_ThenReturnPaymentSuccessStatus() {
		String queryString = "queryString?a=3&b=&" + HeycentricPaymentConstants.RESPONSE_PAYMENT_STATUS_PARAMETER + "=" + HeycentricPaymentStatus.SUCCESSFUL;
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getQueryString()).thenReturn(queryString);

		PaymentStatus result = heycentricPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		assertThat(result, equalTo(PaymentStatus.success()));
	}

	@Test
	public void getPaymentStatus_WithQueryStringPaymentStatusSuccess_ThenReturnPaymentSuccessStatusWithUniqueReferenceSetWithAuthCodeAndRecNo() {
		String queryString = "queryString?a=3&b=&" + HeycentricPaymentConstants.RESPONSE_PAYMENT_STATUS_PARAMETER + "=" + HeycentricPaymentStatus.SUCCESSFUL + "&"
				+ HeycentricPaymentConstants.RESPONSE_PAYMENT_AUTH_CODE_PARAMETER + "=auth123" + "&" + HeycentricPaymentConstants.RESPONSE_REC_NO_PARAMETER + "=recno123";
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getQueryString()).thenReturn(queryString);

		PaymentStatus result = heycentricPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		assertThat(result, equalTo(PaymentStatus.success()));
		assertThat(result.getPaymentUniqueReferenceNumber(), equalTo("auth123 recno123"));
	}

	@Test
	public void preparePayment_WhenConfigurationException_ThenReturnPaymentStatusErrorPaymentResponse() throws ServiceException, PortalException {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		PaymentResponse result = heycentricPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = result.getRedirectURL();
		PaymentStatus actualStatus = result.getStatus();

		assertEquals(StringPool.BLANK, actualRedirectURL);
		assertEquals(PaymentStatus.appError(), actualStatus);
	}

	@Test
	public void preparePayment_WhenNoError_ThenReturnPaymentStatusSuccess() throws ServiceException, PortalException {
		String paymentURL = "paymentURL";
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockHeycentricPaymentCompanyConfiguration)
				.thenReturn(mockHeycentricPaymentCompanyConfiguration);
		when(mockHeycentricPaymentOrderFactory.getPaymentURL(mockHeycentricPaymentCompanyConfiguration, mockPaymentContext)).thenReturn(paymentURL);

		PaymentResponse result = heycentricPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = result.getRedirectURL();
		PaymentStatus actualStatus = result.getStatus();

		assertEquals(paymentURL, actualRedirectURL);
		assertEquals(PaymentStatus.success(), actualStatus);
	}

	@Test
	public void getAgentReferredPaymentPayload_WhenNoErrors_ThenReturnsJSONStringWithEndpointAndRequestPayload() throws Exception {
		final String expectedResult = "result";
		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockHeycentricPaymentCompanyConfiguration)
				.thenReturn(mockHeycentricPaymentCompanyConfiguration);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJSONObjectTwo);

		when(mockHeycentricARPService.getAgentReferredPaymentRequestPayload(mockAgentReferredPaymentContext, mockHeycentricPaymentCompanyConfiguration)).thenReturn(mockJSONObjectOne);
		when(mockHeycentricPaymentCompanyConfiguration.arpEndpoint()).thenReturn(ENDPOINT);
		when(mockJSONObjectOne.toJSONString()).thenReturn(PAYLOAD);
		when(mockJSONObjectTwo.toJSONString()).thenReturn(expectedResult);

		String result = heycentricPaymentConnector.getAgentReferredPaymentPayload(COMPANY_ID, mockAgentReferredPaymentContext);

		assertThat(result, equalTo(expectedResult));

		verify(mockJSONObjectTwo, times(1)).put(HeycentricPaymentConstants.JSON_RESPONSE_ENDPOINT, ENDPOINT);
		verify(mockJSONObjectTwo, times(1)).put(HeycentricPaymentConstants.JSON_RESPONSE_REQUEST_PAYLOAD, PAYLOAD);
	}

	@Test(expected = PortalException.class)
	public void getAgentReferredPaymentPayload_WhenPayloadGenerationFails_ThenThrowsPortalException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockHeycentricPaymentCompanyConfiguration)
				.thenReturn(mockHeycentricPaymentCompanyConfiguration);
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJSONObjectTwo);

		when(mockHeycentricARPService.getAgentReferredPaymentRequestPayload(mockAgentReferredPaymentContext, mockHeycentricPaymentCompanyConfiguration)).thenThrow(new GeneralSecurityException());

		heycentricPaymentConnector.getAgentReferredPaymentPayload(COMPANY_ID, mockAgentReferredPaymentContext);
	}

	@Test(expected = ConfigurationException.class)
	public void getAgentReferredPaymentPayload_WhenErrorRetrievingConfiguration_ThenThrowsConfigurationException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		heycentricPaymentConnector.getAgentReferredPaymentPayload(COMPANY_ID, mockAgentReferredPaymentContext);
	}

	@Test
	public void processAgentReferredPaymentResponse_WhenNoErrors_ThenReturnsExpectedResponse() throws Exception {
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHeycentricARPService.processAgentReferredPaymentResponse(mockHttpServletRequest, EXTERNAL_REFERENCE_CODE)).thenReturn(mockPaymentResponse);

		PaymentResponse result = heycentricPaymentConnector.processAgentReferredPaymentResponse(EXTERNAL_REFERENCE_CODE, mockServiceContext);

		assertThat(result, sameInstance(mockPaymentResponse));
	}

	@Test
	public void processAgentReferredPaymentResponse_WhenResponseProcessingFails_ThenReturnsApplicationErrorResponse() throws Exception {
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockHeycentricARPService.processAgentReferredPaymentResponse(mockHttpServletRequest, EXTERNAL_REFERENCE_CODE)).thenThrow(new PortalException());

		PaymentResponse result = heycentricPaymentConnector.processAgentReferredPaymentResponse(EXTERNAL_REFERENCE_CODE, mockServiceContext);

		assertThat(result.getStatus().getValue(), equalTo(PaymentStatus.appError().getValue()));
	}
}
