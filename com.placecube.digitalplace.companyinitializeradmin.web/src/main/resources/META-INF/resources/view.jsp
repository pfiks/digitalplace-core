<%@ include file="/init.jsp" %>

<div class="container-fluid container-fluid-max-xl">
	<c:choose>
		<c:when test="${not empty companyInitializers}">
			
			<div class="alert alert-info">
				<liferay-ui:message key="select-a-company-initializer"/>
			</div>
			
			<portlet:actionURL name="<%= MVCCommandKeys.RUN_COMPANY_INITIALIZER %>" var="runCompanyInitializerURL"/>
			
			<aui:form action="${runCompanyInitializerURL}" name="runCompanyInitializer" onSubmit='<%= "event.preventDefault();" %>'>
				<aui:select name="<%=PortletRequestKeys.SELECTED_COMPANY_INITIALIZER %>" showEmptyOption="true" label="">
					<c:forEach items="${companyInitializers}" var="entry">
						<aui:option value="${entry.getKey()}" label="${entry.getName()}" />
					</c:forEach>
				</aui:select>
				
				<aui:button type="button" value="execute" onClick="checkSaveEntry()"/>
				
			</aui:form>
			
			<aui:script>
				function checkSaveEntry() {
					if(confirm('<liferay-ui:message key="are-you-sure-you-want-to-run-the-company-initializer"/>')){
						submitForm(document.<portlet:namespace />runCompanyInitializer);
					}
				}
			</aui:script>
			
		</c:when>
		<c:otherwise>
			<div class="alert alert-warning">
				<liferay-ui:message key="no-entries-were-found"/>
			</div>
		</c:otherwise>
	</c:choose>
</div>