/**
 * ItemBase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class ItemBase  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.SummaryData itemSummary;

    private com.capita_software_services.www.scp.base.TaxItem tax;

    private java.lang.Short quantity;

    private com.capita_software_services.www.scp.base.NotificationEmails notificationEmails;

    private com.capita_software_services.www.scp.base.LgItemDetails lgItemDetails;

    private com.capita_software_services.www.scp.base.CustomerInfo customerInfo;

    private org.apache.axis.types.Token lineId;

    public ItemBase() {
    }

    public ItemBase(
           com.capita_software_services.www.scp.base.SummaryData itemSummary,
           com.capita_software_services.www.scp.base.TaxItem tax,
           java.lang.Short quantity,
           com.capita_software_services.www.scp.base.NotificationEmails notificationEmails,
           com.capita_software_services.www.scp.base.LgItemDetails lgItemDetails,
           com.capita_software_services.www.scp.base.CustomerInfo customerInfo,
           org.apache.axis.types.Token lineId) {
           this.itemSummary = itemSummary;
           this.tax = tax;
           this.quantity = quantity;
           this.notificationEmails = notificationEmails;
           this.lgItemDetails = lgItemDetails;
           this.customerInfo = customerInfo;
           this.lineId = lineId;
    }


    /**
     * Gets the itemSummary value for this ItemBase.
     *
     * @return itemSummary
     */
    public com.capita_software_services.www.scp.base.SummaryData getItemSummary() {
        return itemSummary;
    }


    /**
     * Sets the itemSummary value for this ItemBase.
     *
     * @param itemSummary
     */
    public void setItemSummary(com.capita_software_services.www.scp.base.SummaryData itemSummary) {
        this.itemSummary = itemSummary;
    }


    /**
     * Gets the tax value for this ItemBase.
     *
     * @return tax
     */
    public com.capita_software_services.www.scp.base.TaxItem getTax() {
        return tax;
    }


    /**
     * Sets the tax value for this ItemBase.
     *
     * @param tax
     */
    public void setTax(com.capita_software_services.www.scp.base.TaxItem tax) {
        this.tax = tax;
    }


    /**
     * Gets the quantity value for this ItemBase.
     *
     * @return quantity
     */
    public java.lang.Short getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this ItemBase.
     *
     * @param quantity
     */
    public void setQuantity(java.lang.Short quantity) {
        this.quantity = quantity;
    }


    /**
     * Gets the notificationEmails value for this ItemBase.
     *
     * @return notificationEmails
     */
    public com.capita_software_services.www.scp.base.NotificationEmails getNotificationEmails() {
        return notificationEmails;
    }


    /**
     * Sets the notificationEmails value for this ItemBase.
     *
     * @param notificationEmails
     */
    public void setNotificationEmails(com.capita_software_services.www.scp.base.NotificationEmails notificationEmails) {
        this.notificationEmails = notificationEmails;
    }


    /**
     * Gets the lgItemDetails value for this ItemBase.
     *
     * @return lgItemDetails
     */
    public com.capita_software_services.www.scp.base.LgItemDetails getLgItemDetails() {
        return lgItemDetails;
    }


    /**
     * Sets the lgItemDetails value for this ItemBase.
     *
     * @param lgItemDetails
     */
    public void setLgItemDetails(com.capita_software_services.www.scp.base.LgItemDetails lgItemDetails) {
        this.lgItemDetails = lgItemDetails;
    }


    /**
     * Gets the customerInfo value for this ItemBase.
     *
     * @return customerInfo
     */
    public com.capita_software_services.www.scp.base.CustomerInfo getCustomerInfo() {
        return customerInfo;
    }


    /**
     * Sets the customerInfo value for this ItemBase.
     *
     * @param customerInfo
     */
    public void setCustomerInfo(com.capita_software_services.www.scp.base.CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }


    /**
     * Gets the lineId value for this ItemBase.
     *
     * @return lineId
     */
    public org.apache.axis.types.Token getLineId() {
        return lineId;
    }


    /**
     * Sets the lineId value for this ItemBase.
     *
     * @param lineId
     */
    public void setLineId(org.apache.axis.types.Token lineId) {
        this.lineId = lineId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ItemBase)) return false;
        ItemBase other = (ItemBase) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.itemSummary==null && other.getItemSummary()==null) ||
             (this.itemSummary!=null &&
              this.itemSummary.equals(other.getItemSummary()))) &&
            ((this.tax==null && other.getTax()==null) ||
             (this.tax!=null &&
              this.tax.equals(other.getTax()))) &&
            ((this.quantity==null && other.getQuantity()==null) ||
             (this.quantity!=null &&
              this.quantity.equals(other.getQuantity()))) &&
            ((this.notificationEmails==null && other.getNotificationEmails()==null) ||
             (this.notificationEmails!=null &&
              this.notificationEmails.equals(other.getNotificationEmails()))) &&
            ((this.lgItemDetails==null && other.getLgItemDetails()==null) ||
             (this.lgItemDetails!=null &&
              this.lgItemDetails.equals(other.getLgItemDetails()))) &&
            ((this.customerInfo==null && other.getCustomerInfo()==null) ||
             (this.customerInfo!=null &&
              this.customerInfo.equals(other.getCustomerInfo()))) &&
            ((this.lineId==null && other.getLineId()==null) ||
             (this.lineId!=null &&
              this.lineId.equals(other.getLineId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getItemSummary() != null) {
            _hashCode += getItemSummary().hashCode();
        }
        if (getTax() != null) {
            _hashCode += getTax().hashCode();
        }
        if (getQuantity() != null) {
            _hashCode += getQuantity().hashCode();
        }
        if (getNotificationEmails() != null) {
            _hashCode += getNotificationEmails().hashCode();
        }
        if (getLgItemDetails() != null) {
            _hashCode += getLgItemDetails().hashCode();
        }
        if (getCustomerInfo() != null) {
            _hashCode += getCustomerInfo().hashCode();
        }
        if (getLineId() != null) {
            _hashCode += getLineId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ItemBase.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "itemBase"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "itemSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "summaryData"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "tax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "taxItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "short"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notificationEmails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "notificationEmails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "notificationEmails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lgItemDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lgItemDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lgItemDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lineId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "token"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
