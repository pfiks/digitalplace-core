package com.placecube.digitalplace.user.account.constants;

public enum WebContentArticles {

	USER_ACCOUNT_CONFIRMATION_CREATED_BY_ADMIN("USER-ACCOUNT-CONFIRMATION-CREATED-BY-ADMIN", "Create account confirmation done by admin"),

	USER_ACCOUNT_CONFIRMATION_RESET_PASSWORD_INVOKED_BY_ADMIN("USER-ACCOUNT-CONFIRMATION-RESET-PASSWORD-INVOKED-BY-ADMIN", "User account confirmation about reset password invoked by admin"),

	USER_ACCOUNT_CONFIRMATION_WITH_EMAIL_VERIFICATION("USER-ACCOUNT-CONFIRMATION-WITH-EMAIL-VERIFICATION", "Create account confirmation with email verification"),

	USER_ACCOUNT_CONFIRMATION_WITHOUT_EMAIL_VERIFICATION("USER-ACCOUNT-CONFIRMATION-WITHOUT-EMAIL-VERIFICATION", "Create account confirmation without email verification"),

	USER_ACCOUNT_SIDE_PANEL_INFO("USER-ACCOUNT-SIDE-PANEL-INFO", "User account side panel info"),

	USER_ACCOUNT_TWO_FACTOR_AUTHENTICATION("USER-ACCOUNT-TWO-FACTOR-AUTHENTICATION", "User account two factor authentication");

	private final String articleId;
	private final String articleTitle;

	private WebContentArticles(String articleId, String articleTitle) {
		this.articleId = articleId;
		this.articleTitle = articleTitle;
	}

	public String getArticleId() {
		return articleId;
	}

	public String getArticleTitle() {
		return articleTitle;
	}

}
