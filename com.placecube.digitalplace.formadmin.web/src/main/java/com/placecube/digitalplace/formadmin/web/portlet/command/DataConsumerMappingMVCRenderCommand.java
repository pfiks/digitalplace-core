package com.placecube.digitalplace.formadmin.web.portlet.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceService;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.model.DataProviderInputParameter;
import com.placecube.digitalplace.dataconsumer.model.DataProviderOutputParameter;
import com.placecube.digitalplace.dataconsumer.service.DataConsumerLocalService;
import com.placecube.digitalplace.dataconsumer.service.DataTransformationService;
import com.placecube.digitalplace.formadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.constants.RequestMethod;
import com.placecube.digitalplace.formadmin.web.service.DataConsumerListService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.FORMS_MANAGER, "mvc.command.name=" + MVCCommandKeys.DATA_CONSUMERS_MAPPING }, service = MVCRenderCommand.class)
public class DataConsumerMappingMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private DataConsumerListService dataConsumerListService;

	@Reference
	private DataConsumerLocalService dataConsumerLocalService;

	@Reference
	private DataTransformationService dataTransformationService;

	@Reference
	private DDMDataProviderInstanceService ddmDataProviderInstanceService;

	@Reference
	private DDMFormInstanceService ddmFormInstanceService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			long formInstanceId = ParamUtil.getLong(renderRequest, PortletRequestKeys.FORM_INSTANCE_ID);
			long dataProviderInstanceId = ParamUtil.getLong(renderRequest, PortletRequestKeys.DATA_PROVIDER_INSTANCE_ID);
			int chain = ParamUtil.getInteger(renderRequest, PortletRequestKeys.CHAIN);

			List<DataConsumer> dataConsumers = dataConsumerLocalService.getDataConsumersByDDMFormInstanceId(formInstanceId);
			Optional<DataConsumer> dataConsumerOption = dataConsumers.stream().filter(c -> c.getChain() == chain).findFirst();
			Locale locale = renderRequest.getLocale();

			DDMFormInstance form = ddmFormInstanceService.getFormInstance(formInstanceId);
			renderRequest.setAttribute(PortletRequestKeys.FORM, form);

			if (chain > 0 && dataConsumerOption.isPresent()) {
				setSourceDataProviderById(dataConsumerOption.get().getSourceDataProviderId(), renderRequest, locale);
			} else {
				Optional<DataConsumer> previousChainLinkConsumerOption = dataConsumers.stream().filter(c -> c.getChain() == chain - 1).findFirst();
				if (previousChainLinkConsumerOption.isPresent()) {
					setSourceDataProviderById(previousChainLinkConsumerOption.get().getDdmDataProviderInstanceId(), renderRequest, locale);
				}
			}

			DDMDataProviderInstance providerInstance = ddmDataProviderInstanceService.getDataProviderInstance(dataProviderInstanceId);
			DDMFormValues dataProviderFields = dataTransformationService.getDataProviderFormValues(providerInstance);
			List<DataProviderInputParameter> inputFields = dataTransformationService.getInputParameters(dataProviderFields, locale);
			List<DataProviderOutputParameter> outputFields = dataTransformationService.getOutputParameters(dataProviderFields, locale);
			String url = dataProviderFields.getDDMFormFieldValuesMap().get(PortletRequestKeys.URL).get(0).getValue().getString(locale);

			renderRequest.setAttribute(PortletRequestKeys.DDM_FORM_FIELDS, getDDMFormFields(form));
			renderRequest.setAttribute(PortletRequestKeys.INPUT_FIELDS, inputFields);
			renderRequest.setAttribute(PortletRequestKeys.OUTPUT_FIELDS, outputFields);
			renderRequest.setAttribute(PortletRequestKeys.URL, url);
			renderRequest.setAttribute(PortletRequestKeys.FORM_INSTANCE_ID, formInstanceId);
			renderRequest.setAttribute(PortletRequestKeys.DATA_PROVIDER_INSTANCE_ID, dataProviderInstanceId);
			renderRequest.setAttribute(PortletRequestKeys.DATA_PROVIDER_NAME, providerInstance.getName(renderRequest.getLocale()));
			renderRequest.setAttribute(PortletRequestKeys.CHAIN, chain);
			renderRequest.setAttribute(PortletRequestKeys.REQUEST_BODY, dataConsumerListService.getDataConsumerBody(dataConsumerOption, inputFields));

			List<String> methods = EnumSet.allOf(RequestMethod.class).stream().map(RequestMethod::name).collect(Collectors.toList());
			renderRequest.setAttribute(PortletRequestKeys.METHODS, methods);

			List<RequestMethod> bodySupportedMethods = Arrays.asList(RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH);
			renderRequest.setAttribute(PortletRequestKeys.BODY_SUPPORTED_METHODS, bodySupportedMethods.stream().map(RequestMethod::name).collect(Collectors.joining(",")));

			setDataConsumerParams(renderRequest, dataProviderInstanceId, dataConsumerOption);

			return "/data-consumer-mapping.jsp";

		} catch (PortalException e) {
			throw new PortletException(e);
		}
	}

	private void addDDMFormFieldToList(List<DDMFormField> allDDMFormFields, DDMFormField ddmFormField) {
		allDDMFormFields.add(ddmFormField);
		if (DDMFormFieldTypeConstants.FIELDSET.equals(ddmFormField.getType())) {
			List<DDMFormField> nestedDDMFormFields = ddmFormField.getNestedDDMFormFields();
			if (Validator.isNotNull(nestedDDMFormFields) || !nestedDDMFormFields.isEmpty()) {
				for (DDMFormField nestedDDMFormField : nestedDDMFormFields) {
					addDDMFormFieldToList(allDDMFormFields, nestedDDMFormField);
				}
			}
		}
	}

	private List<DDMFormField> getDDMFormFields(DDMFormInstance form) throws PortalException {
		List<DDMFormField> allDDMFormFields = new ArrayList<>();

		List<DDMFormField> ddmFormFields = form.getDDMForm().getDDMFormFields();
		for (DDMFormField ddmFormField : ddmFormFields) {
			addDDMFormFieldToList(allDDMFormFields, ddmFormField);
		}

		return allDDMFormFields;
	}

	private void setDataConsumerParams(RenderRequest renderRequest, long dataProviderInstanceId, Optional<DataConsumer> dataConsumerOption) throws JSONException {
		if (dataConsumerOption.isPresent() && dataConsumerOption.get().getDdmDataProviderInstanceId() == dataProviderInstanceId) {
			DataConsumer dataConsumer = dataConsumerOption.get();
			renderRequest.setAttribute(PortletRequestKeys.MAPPER, dataTransformationService.deserializeMapping(dataConsumer.getMapping()));
			renderRequest.setAttribute(PortletRequestKeys.DATA_CONSUMER_ID, dataConsumer.getDataConsumerId());
			renderRequest.setAttribute(PortletRequestKeys.SELECTED_METHOD, dataConsumer.getMethod());
			renderRequest.setAttribute(PortletRequestKeys.SERVICE_HEADERS, dataTransformationService.deserializeMapping(dataConsumer.getHeaders()));
		}
	}

	private void setSourceDataProviderById(long sourceDataProviderId, RenderRequest renderRequest, Locale locale) throws PortalException {
		renderRequest.setAttribute(PortletRequestKeys.SOURCE_OUTPUT_FIELDS, dataConsumerListService.getOutputParametersForDataProviderId(sourceDataProviderId, locale));
		renderRequest.setAttribute(PortletRequestKeys.SOURCE_DATA_PROVIDER_ID, sourceDataProviderId);
	}

}
