package com.placecube.digitalplace.search.web.portlet.category.contributor;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.category.configuration.CategoryFacetPortletConfigurationConstants;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryLocalService;

public class CategoryFacetPortletSharedSearchContributorTest extends PowerMockito {

	private static final String INTERNAL_PORTLET_ID = "sdgsgs";

	@InjectMocks
	private CategoryFacetPortletSharedSearchContributor categoryFacetPortletSharedSearchContributor;

	@Mock
	private CategoryLocalService mockCategoryLocalService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private FacetConfiguration mockFacetConfiguration;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheCategoryFacetInTheContributorSettings() throws ConfigurationException {
		int maxResultsToShow = 2;
		mockFacetInit();
		when(mockPortletPreferences.getValue(CategoryFacetPortletConfigurationConstants.MAX_CATEGORIES_TO_SHOW, "10")).thenReturn(String.valueOf(maxResultsToShow));
		when(mockPortletPreferences.getValue(CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK)).thenReturn(INTERNAL_PORTLET_ID);
		when(mockSharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(Collections.emptyList());

		categoryFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockFacetConfiguration, mockJSONObject, mockSharedSearchContributorSettings);
		inOrder.verify(mockFacetConfiguration, times(1)).setOrder("OrderHitsDesc");
		inOrder.verify(mockFacetConfiguration, times(1)).setStatic(false);
		inOrder.verify(mockFacetConfiguration, times(1)).setWeight(1.3d);
		inOrder.verify(mockJSONObject, times(1)).put("maxTerms", maxResultsToShow);
		inOrder.verify(mockJSONObject, times(1)).put("frequencyThreshold", 1);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).addFacet(SearchFacetConstants.ASSET_CATEGORY_IDS, Optional.of(mockFacetConfiguration));
	}

	@Test
	public void contribute_WhenNoPreselectedCategories_ThenConfiguresTheSelectedAssetCategoryIdsAsMustBooleanQueryInTheContributorSettings() throws ConfigurationException {
		mockFacetInit();
		when(mockPortletPreferences.getValue(CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK)).thenReturn(INTERNAL_PORTLET_ID);

		List<SharedActiveFilter> activeCategoryFilters = new ArrayList<>();

		activeCategoryFilters.add(new SharedActiveFilter("n", "11", "l", "c"));
		activeCategoryFilters.add(new SharedActiveFilter("n", "12", "l", "c"));
		activeCategoryFilters.add(new SharedActiveFilter("n", "13", "l", "c"));

		when(mockSharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(activeCategoryFilters);

		categoryFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQueryForMultipleValues(SearchFacetConstants.ASSET_CATEGORY_IDS, Arrays.asList(new String[] { "11", "12", "13" }),
				BooleanClauseOccur.MUST);
	}

	@Test
	public void contribute_WhenPreselectedCategoriesExistAndCategoriesQueryAndedSettingIsFalse_ThenCombinePreselectedWithSelectedCategoriesAsSingleMustBooleanQueryInTheContributorSettings()
			throws ConfigurationException {
		mockFacetInit();

		String[] vocabularyIds = { "ID", "ID2" };
		String[] categoryIds = { "ID3", "ID4" };

		List<SharedActiveFilter> activeCategoryFilters = new ArrayList<>();
		activeCategoryFilters.add(new SharedActiveFilter("n", "11", "l", "c"));
		activeCategoryFilters.add(new SharedActiveFilter("n", "12", "l", "c"));
		activeCategoryFilters.add(new SharedActiveFilter("n", "13", "l", "c"));

		Set<Long> aggregate = new HashSet<>();
		aggregate.add(13l);
		aggregate.add(14l);

		when(mockPortletPreferences.getValues(CategoryFacetPortletConfigurationConstants.PRESELECTED_VOCABOLARY_ID, new String[0])).thenReturn(vocabularyIds);
		when(mockPortletPreferences.getValues(CategoryFacetPortletConfigurationConstants.PRESELECTED_CATEGORY_IDS, new String[0])).thenReturn(categoryIds);
		when(mockPortletPreferences.getValue(CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK)).thenReturn(INTERNAL_PORTLET_ID);
		when(mockSharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(activeCategoryFilters);
		when(mockPortletPreferences.getValue(CategoryFacetPortletConfigurationConstants.CATEGORIES_QUERY_ANDED, "false")).thenReturn("false");
		when(mockCategoryLocalService.getAggregateCategoryLists(mockThemeDisplay, vocabularyIds, categoryIds)).thenReturn(aggregate);

		categoryFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQueryForMultipleValues(SearchFacetConstants.ASSET_CATEGORY_IDS, Arrays.asList(new String[] { "11", "12", "13", "14" }),
				BooleanClauseOccur.MUST);
	}

	@Test
	public void contribute_WhenCategoriesQueryAndedSettingIsTrue_ThenAddQueriesForEachSelectedCategoryToTheContributorSettings() throws ConfigurationException {
		mockFacetInit();

		List<SharedActiveFilter> activeCategoryFilters = new ArrayList<>();
		activeCategoryFilters.add(new SharedActiveFilter("n", "11", "l", "c"));
		activeCategoryFilters.add(new SharedActiveFilter("n", "12", "l", "c"));
		activeCategoryFilters.add(new SharedActiveFilter("n", "13", "l", "c"));
		when(mockPortletPreferences.getValue(CategoryFacetPortletConfigurationConstants.CATEGORIES_QUERY_ANDED, "false")).thenReturn("true");
		when(mockPortletPreferences.getValue(CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK)).thenReturn(INTERNAL_PORTLET_ID);
		when(mockSharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(activeCategoryFilters);

		categoryFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQuery(SearchFacetConstants.ASSET_CATEGORY_IDS, "11", BooleanClauseOccur.MUST);
		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQuery(SearchFacetConstants.ASSET_CATEGORY_IDS, "12", BooleanClauseOccur.MUST);
		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQuery(SearchFacetConstants.ASSET_CATEGORY_IDS, "13", BooleanClauseOccur.MUST);
	}

	@Test(expected = SystemException.class)
	public void contribute_WhenNoPortletPrefsFound_ThenThrowsSystemException() throws ConfigurationException {
		when(mockSharedSearchContributorSettings.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.empty());

		categoryFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);
	}

	private void mockFacetInit() throws ConfigurationException {
		when(mockSharedSearchContributorSettings.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.createFacetConfiguration(SearchFacetConstants.ASSET_CATEGORY_IDS)).thenReturn(mockFacetConfiguration);
		when(mockFacetConfiguration.getData()).thenReturn(mockJSONObject);
	}

}
