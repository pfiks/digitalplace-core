package com.placecube.digitalplace.form.model.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.form.model.FormDefinition;

public class FormDefinitionImplTest {

	private static final String CATEGORY = "category";
	private static final String NAME = "name";
	private static final String KEY = "form defintion";
	private static final String DESCRIPTION = "description";
	private static String FORM_JOURNAL_ARTICLE_FOLDER_NAME = "FolderName";

	private FormDefinitionImpl formDefinitionImpl;

	private URL ddmForm;

	private URL ddmFormLayout;

	private URL settingsDDMFormValues;

	private List<URL> journalArticleURLs;

	private List<URL> dataProviderURLs;

	@Mock
	private HttpURLConnection mockConnection;

	@Before
	public void setUp() throws Exception {

		initMocks(this);

		ddmForm = new URL(null, "file://ddm-form.json", createURLStreamHandler());
		ddmFormLayout = new URL(null, "file://ddm-form-layout.json", createURLStreamHandler());
		settingsDDMFormValues = new URL(null, "file://settings-ddm-form-values.json", createURLStreamHandler());

		journalArticleURLs = new ArrayList<>();
		URL journalArticle = new URL(null, "file://journal-article.html", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		dataProviderURLs = new ArrayList<>();
		URL dataProviderURL = new URL(null, "file://data-provider.json", createURLStreamHandler());
		dataProviderURLs.add(dataProviderURL);

		formDefinitionImpl = new FormDefinitionImpl(NAME, DESCRIPTION, CATEGORY, KEY, FORM_JOURNAL_ARTICLE_FOLDER_NAME, ddmForm, ddmFormLayout, settingsDDMFormValues, journalArticleURLs,
				dataProviderURLs);

	}

	@Test
	public void getCategory_WhenNoError_ThenReturnsName() {

		assertThat(formDefinitionImpl.getCategory(), equalTo(CATEGORY));

	}

	@Test
	public void getDescription_WhenNoError_ThenReturnsDescription() {

		assertThat(formDefinitionImpl.getDescription(), equalTo(DESCRIPTION));

	}

	@Test
	public void getKey_WhenNoError_ThenReturnsKey() {

		String expected = KEY.replace(StringPool.SPACE, StringPool.DASH).toLowerCase();

		assertThat(formDefinitionImpl.getKey(), equalTo(expected));

	}

	@Test
	public void getName_WhenNoError_ThenReturnsName() {

		assertThat(formDefinitionImpl.getName(), equalTo(NAME));

	}

	@Test
	public void getJournalArticleFolderName_WhenNoError_ThenReturnsJournalArticleFolderName() {

		assertThat(formDefinitionImpl.getJournalArticleFolderName(), equalTo(FORM_JOURNAL_ARTICLE_FOLDER_NAME));

	}

	@Test
	public void getDDMForm_WhenNoError_ThenReturnsDDMForm() {

		assertThat(formDefinitionImpl.getUrlToDDMFormDefinitionFile(), equalTo(ddmForm));

	}

	@Test
	public void getDDMFormLayout_WhenNoError_ThenReturnsDDMFormLayout() {

		assertThat(formDefinitionImpl.getUrlToDDMFormLayoutDefinitionFile(), equalTo(ddmFormLayout));

	}

	@Test
	public void getDDMFormLayout_WhenNoError_ThenReturnsSettingsDDMFormValues() {

		assertThat(formDefinitionImpl.getUrlToSettingsDDMFormValuesDefinitionFile(), equalTo(settingsDDMFormValues));

	}

	@Test
	public void getUrlsToJournalArticles_WhenNoError_ThenReturnsUrlsToJournalArticles() {

		assertThat(formDefinitionImpl.getUrlsToJournalArticles(), equalTo(journalArticleURLs));

	}

	@Test
	public void toString_WhenError_ThenReturnsString() {

		String toString = "FormDefinition [name=name,key=form defintion, category=category]";

		assertThat(formDefinitionImpl.toString(), equalTo(toString));

	}

	@Test
	public void constructorWithNameAndDescriptionAndKey_WhenNoError_ThenReturnsInstanceCreatedWithValuesSet() {
		FormDefinition result = new FormDefinitionImpl(NAME, DESCRIPTION, KEY);

		assertThat(result.getName(), equalTo(NAME));
		assertThat(result.getDescription(), equalTo(DESCRIPTION));
		assertThat(result.getKey(), equalTo(KEY.replace(StringPool.SPACE, StringPool.DASH).toLowerCase()));
	}

	private URLStreamHandler createURLStreamHandler() throws IOException {

		URLStreamHandler stubURLStreamHandler = new URLStreamHandler() {

			@Override
			protected URLConnection openConnection(URL u) throws IOException {
				return mockConnection;
			}
		};

		return stubURLStreamHandler;
	}

}
