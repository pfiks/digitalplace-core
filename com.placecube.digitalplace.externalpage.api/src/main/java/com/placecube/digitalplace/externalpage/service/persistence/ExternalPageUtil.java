/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.externalpage.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.externalpage.model.ExternalPage;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the external page service. This utility wraps <code>com.placecube.digitalplace.externalpage.service.persistence.impl.ExternalPagePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ExternalPagePersistence
 * @generated
 */
public class ExternalPageUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ExternalPage externalPage) {
		getPersistence().clearCache(externalPage);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, ExternalPage> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ExternalPage> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ExternalPage> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ExternalPage> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ExternalPage update(ExternalPage externalPage) {
		return getPersistence().update(externalPage);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ExternalPage update(
		ExternalPage externalPage, ServiceContext serviceContext) {

		return getPersistence().update(externalPage, serviceContext);
	}

	/**
	 * Returns all the external pages where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching external pages
	 */
	public static List<ExternalPage> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the external pages where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	public static List<ExternalPage> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	public static List<ExternalPage> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	public static List<ExternalPage> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public static ExternalPage findByUuid_First(
			String uuid, OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByUuid_First(
		String uuid, OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public static ExternalPage findByUuid_Last(
			String uuid, OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByUuid_Last(
		String uuid, OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the external pages before and after the current external page in the ordered set where uuid = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public static ExternalPage[] findByUuid_PrevAndNext(
			long externalPageId, String uuid,
			OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByUuid_PrevAndNext(
			externalPageId, uuid, orderByComparator);
	}

	/**
	 * Removes all the external pages where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of external pages where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching external pages
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the external page where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchExternalPageException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public static ExternalPage findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the external page where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the external page where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the external page where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the external page that was removed
	 */
	public static ExternalPage removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of external pages where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching external pages
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching external pages
	 */
	public static List<ExternalPage> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	public static List<ExternalPage> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	public static List<ExternalPage> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	public static List<ExternalPage> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public static ExternalPage findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public static ExternalPage findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the external pages before and after the current external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public static ExternalPage[] findByUuid_C_PrevAndNext(
			long externalPageId, String uuid, long companyId,
			OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByUuid_C_PrevAndNext(
			externalPageId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the external pages where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching external pages
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @return the matching external pages
	 */
	public static List<ExternalPage> findByGroupIdUrl(
		long groupId, String url) {

		return getPersistence().findByGroupIdUrl(groupId, url);
	}

	/**
	 * Returns a range of all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	public static List<ExternalPage> findByGroupIdUrl(
		long groupId, String url, int start, int end) {

		return getPersistence().findByGroupIdUrl(groupId, url, start, end);
	}

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	public static List<ExternalPage> findByGroupIdUrl(
		long groupId, String url, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().findByGroupIdUrl(
			groupId, url, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	public static List<ExternalPage> findByGroupIdUrl(
		long groupId, String url, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupIdUrl(
			groupId, url, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public static ExternalPage findByGroupIdUrl_First(
			long groupId, String url,
			OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByGroupIdUrl_First(
			groupId, url, orderByComparator);
	}

	/**
	 * Returns the first external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByGroupIdUrl_First(
		long groupId, String url,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().fetchByGroupIdUrl_First(
			groupId, url, orderByComparator);
	}

	/**
	 * Returns the last external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public static ExternalPage findByGroupIdUrl_Last(
			long groupId, String url,
			OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByGroupIdUrl_Last(
			groupId, url, orderByComparator);
	}

	/**
	 * Returns the last external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByGroupIdUrl_Last(
		long groupId, String url,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().fetchByGroupIdUrl_Last(
			groupId, url, orderByComparator);
	}

	/**
	 * Returns the external pages before and after the current external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public static ExternalPage[] findByGroupIdUrl_PrevAndNext(
			long externalPageId, long groupId, String url,
			OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByGroupIdUrl_PrevAndNext(
			externalPageId, groupId, url, orderByComparator);
	}

	/**
	 * Removes all the external pages where groupId = &#63; and url = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 */
	public static void removeByGroupIdUrl(long groupId, String url) {
		getPersistence().removeByGroupIdUrl(groupId, url);
	}

	/**
	 * Returns the number of external pages where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @return the number of matching external pages
	 */
	public static int countByGroupIdUrl(long groupId, String url) {
		return getPersistence().countByGroupIdUrl(groupId, url);
	}

	/**
	 * Returns all the external pages where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching external pages
	 */
	public static List<ExternalPage> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the external pages where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	public static List<ExternalPage> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	public static List<ExternalPage> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	public static List<ExternalPage> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public static ExternalPage findByGroupId_First(
			long groupId, OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByGroupId_First(
		long groupId, OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	public static ExternalPage findByGroupId_Last(
			long groupId, OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchByGroupId_Last(
		long groupId, OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the external pages before and after the current external page in the ordered set where groupId = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public static ExternalPage[] findByGroupId_PrevAndNext(
			long externalPageId, long groupId,
			OrderByComparator<ExternalPage> orderByComparator)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByGroupId_PrevAndNext(
			externalPageId, groupId, orderByComparator);
	}

	/**
	 * Removes all the external pages where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of external pages where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching external pages
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Caches the external page in the entity cache if it is enabled.
	 *
	 * @param externalPage the external page
	 */
	public static void cacheResult(ExternalPage externalPage) {
		getPersistence().cacheResult(externalPage);
	}

	/**
	 * Caches the external pages in the entity cache if it is enabled.
	 *
	 * @param externalPages the external pages
	 */
	public static void cacheResult(List<ExternalPage> externalPages) {
		getPersistence().cacheResult(externalPages);
	}

	/**
	 * Creates a new external page with the primary key. Does not add the external page to the database.
	 *
	 * @param externalPageId the primary key for the new external page
	 * @return the new external page
	 */
	public static ExternalPage create(long externalPageId) {
		return getPersistence().create(externalPageId);
	}

	/**
	 * Removes the external page with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page that was removed
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public static ExternalPage remove(long externalPageId)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().remove(externalPageId);
	}

	public static ExternalPage updateImpl(ExternalPage externalPage) {
		return getPersistence().updateImpl(externalPage);
	}

	/**
	 * Returns the external page with the primary key or throws a <code>NoSuchExternalPageException</code> if it could not be found.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	public static ExternalPage findByPrimaryKey(long externalPageId)
		throws com.placecube.digitalplace.externalpage.exception.
			NoSuchExternalPageException {

		return getPersistence().findByPrimaryKey(externalPageId);
	}

	/**
	 * Returns the external page with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page, or <code>null</code> if a external page with the primary key could not be found
	 */
	public static ExternalPage fetchByPrimaryKey(long externalPageId) {
		return getPersistence().fetchByPrimaryKey(externalPageId);
	}

	/**
	 * Returns all the external pages.
	 *
	 * @return the external pages
	 */
	public static List<ExternalPage> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of external pages
	 */
	public static List<ExternalPage> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of external pages
	 */
	public static List<ExternalPage> findAll(
		int start, int end, OrderByComparator<ExternalPage> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of external pages
	 */
	public static List<ExternalPage> findAll(
		int start, int end, OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the external pages from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of external pages.
	 *
	 * @return the number of external pages
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ExternalPagePersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(ExternalPagePersistence persistence) {
		_persistence = persistence;
	}

	private static volatile ExternalPagePersistence _persistence;

}