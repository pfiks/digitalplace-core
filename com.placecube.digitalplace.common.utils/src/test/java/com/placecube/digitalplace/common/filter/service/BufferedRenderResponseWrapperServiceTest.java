package com.placecube.digitalplace.common.filter.service;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.placecube.digitalplace.common.filter.model.BufferedRenderResponseWrapper;

public class BufferedRenderResponseWrapperServiceTest {

	private BufferedRenderResponseWrapperService bufferedRenderResponseWrapperService;

	@Mock
	private RenderResponse mockRenderResponse;

	@Test(expected = IllegalArgumentException.class)
	public void getBufferedRenderResponseWrapper_WithoutRenderResponse_ThrowsException() {

		bufferedRenderResponseWrapperService.getBufferedRenderResponseWrapper(null);

	}

	@Test
	public void getBufferedRenderResponseWrapper_WithRenderResponse_ReturnsBufferedRenderResponseWrapper() {

		BufferedRenderResponseWrapper bufferedRenderResponseWrapper = bufferedRenderResponseWrapperService.getBufferedRenderResponseWrapper(mockRenderResponse);

		assertThat(bufferedRenderResponseWrapper, instanceOf(BufferedRenderResponseWrapper.class));

	}

	@Before
	public void setUp() {

		initMocks(this);

		bufferedRenderResponseWrapperService = new BufferedRenderResponseWrapperService();

	}

}