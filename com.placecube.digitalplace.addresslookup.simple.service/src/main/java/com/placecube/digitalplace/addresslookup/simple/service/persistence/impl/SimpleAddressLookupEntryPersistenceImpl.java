/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.addresslookup.simple.exception.NoSuchSimpleAddressLookupEntryException;
import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;
import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntryTable;
import com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryImpl;
import com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl;
import com.placecube.digitalplace.addresslookup.simple.service.persistence.SimpleAddressLookupEntryPK;
import com.placecube.digitalplace.addresslookup.simple.service.persistence.SimpleAddressLookupEntryPersistence;
import com.placecube.digitalplace.addresslookup.simple.service.persistence.SimpleAddressLookupEntryUtil;
import com.placecube.digitalplace.addresslookup.simple.service.persistence.impl.constants.Placecube_AddressLookupSimplePersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the simple address lookup entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = SimpleAddressLookupEntryPersistence.class)
public class SimpleAddressLookupEntryPersistenceImpl
	extends BasePersistenceImpl<SimpleAddressLookupEntry>
	implements SimpleAddressLookupEntryPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>SimpleAddressLookupEntryUtil</code> to access the simple address lookup entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		SimpleAddressLookupEntryImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the simple address lookup entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the simple address lookup entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByUuid(
		String uuid, int start, int end) {

		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<SimpleAddressLookupEntry> list = null;

		if (useFinderCache) {
			list = (List<SimpleAddressLookupEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (SimpleAddressLookupEntry simpleAddressLookupEntry : list) {
					if (!uuid.equals(simpleAddressLookupEntry.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(SimpleAddressLookupEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<SimpleAddressLookupEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry findByUuid_First(
			String uuid,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException {

		SimpleAddressLookupEntry simpleAddressLookupEntry = fetchByUuid_First(
			uuid, orderByComparator);

		if (simpleAddressLookupEntry != null) {
			return simpleAddressLookupEntry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchSimpleAddressLookupEntryException(sb.toString());
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry fetchByUuid_First(
		String uuid,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		List<SimpleAddressLookupEntry> list = findByUuid(
			uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry findByUuid_Last(
			String uuid,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException {

		SimpleAddressLookupEntry simpleAddressLookupEntry = fetchByUuid_Last(
			uuid, orderByComparator);

		if (simpleAddressLookupEntry != null) {
			return simpleAddressLookupEntry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchSimpleAddressLookupEntryException(sb.toString());
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry fetchByUuid_Last(
		String uuid,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<SimpleAddressLookupEntry> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the simple address lookup entries before and after the current simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the current simple address lookup entry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public SimpleAddressLookupEntry[] findByUuid_PrevAndNext(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK, String uuid,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException {

		uuid = Objects.toString(uuid, "");

		SimpleAddressLookupEntry simpleAddressLookupEntry = findByPrimaryKey(
			simpleAddressLookupEntryPK);

		Session session = null;

		try {
			session = openSession();

			SimpleAddressLookupEntry[] array =
				new SimpleAddressLookupEntryImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, simpleAddressLookupEntry, uuid, orderByComparator,
				true);

			array[1] = simpleAddressLookupEntry;

			array[2] = getByUuid_PrevAndNext(
				session, simpleAddressLookupEntry, uuid, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected SimpleAddressLookupEntry getByUuid_PrevAndNext(
		Session session, SimpleAddressLookupEntry simpleAddressLookupEntry,
		String uuid,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(SimpleAddressLookupEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						simpleAddressLookupEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<SimpleAddressLookupEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the simple address lookup entries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (SimpleAddressLookupEntry simpleAddressLookupEntry :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(simpleAddressLookupEntry);
		}
	}

	/**
	 * Returns the number of simple address lookup entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching simple address lookup entries
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_SIMPLEADDRESSLOOKUPENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"simpleAddressLookupEntry.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(simpleAddressLookupEntry.uuid IS NULL OR simpleAddressLookupEntry.uuid = '')";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId) {

		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<SimpleAddressLookupEntry> list = null;

		if (useFinderCache) {
			list = (List<SimpleAddressLookupEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (SimpleAddressLookupEntry simpleAddressLookupEntry : list) {
					if (!uuid.equals(simpleAddressLookupEntry.getUuid()) ||
						(companyId !=
							simpleAddressLookupEntry.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(SimpleAddressLookupEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<SimpleAddressLookupEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException {

		SimpleAddressLookupEntry simpleAddressLookupEntry = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (simpleAddressLookupEntry != null) {
			return simpleAddressLookupEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchSimpleAddressLookupEntryException(sb.toString());
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		List<SimpleAddressLookupEntry> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException {

		SimpleAddressLookupEntry simpleAddressLookupEntry = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (simpleAddressLookupEntry != null) {
			return simpleAddressLookupEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchSimpleAddressLookupEntryException(sb.toString());
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<SimpleAddressLookupEntry> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the simple address lookup entries before and after the current simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the current simple address lookup entry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public SimpleAddressLookupEntry[] findByUuid_C_PrevAndNext(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK, String uuid,
			long companyId,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException {

		uuid = Objects.toString(uuid, "");

		SimpleAddressLookupEntry simpleAddressLookupEntry = findByPrimaryKey(
			simpleAddressLookupEntryPK);

		Session session = null;

		try {
			session = openSession();

			SimpleAddressLookupEntry[] array =
				new SimpleAddressLookupEntryImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, simpleAddressLookupEntry, uuid, companyId,
				orderByComparator, true);

			array[1] = simpleAddressLookupEntry;

			array[2] = getByUuid_C_PrevAndNext(
				session, simpleAddressLookupEntry, uuid, companyId,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected SimpleAddressLookupEntry getByUuid_C_PrevAndNext(
		Session session, SimpleAddressLookupEntry simpleAddressLookupEntry,
		String uuid, long companyId,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(SimpleAddressLookupEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						simpleAddressLookupEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<SimpleAddressLookupEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the simple address lookup entries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (SimpleAddressLookupEntry simpleAddressLookupEntry :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(simpleAddressLookupEntry);
		}
	}

	/**
	 * Returns the number of simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching simple address lookup entries
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_SIMPLEADDRESSLOOKUPENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"simpleAddressLookupEntry.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(simpleAddressLookupEntry.uuid IS NULL OR simpleAddressLookupEntry.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"simpleAddressLookupEntry.id.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByCompanyId_Postcode;
	private FinderPath _finderPathWithoutPaginationFindByCompanyId_Postcode;
	private FinderPath _finderPathCountByCompanyId_Postcode;

	/**
	 * Returns all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @return the matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode) {

		return findByCompanyId_Postcode(
			companyId, postcode, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode, int start, int end) {

		return findByCompanyId_Postcode(companyId, postcode, start, end, null);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return findByCompanyId_Postcode(
			companyId, postcode, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache) {

		postcode = Objects.toString(postcode, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByCompanyId_Postcode;
				finderArgs = new Object[] {companyId, postcode};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByCompanyId_Postcode;
			finderArgs = new Object[] {
				companyId, postcode, start, end, orderByComparator
			};
		}

		List<SimpleAddressLookupEntry> list = null;

		if (useFinderCache) {
			list = (List<SimpleAddressLookupEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (SimpleAddressLookupEntry simpleAddressLookupEntry : list) {
					if ((companyId !=
							simpleAddressLookupEntry.getCompanyId()) ||
						!postcode.equals(
							simpleAddressLookupEntry.getPostcode())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYID_POSTCODE_COMPANYID_2);

			boolean bindPostcode = false;

			if (postcode.isEmpty()) {
				sb.append(_FINDER_COLUMN_COMPANYID_POSTCODE_POSTCODE_3);
			}
			else {
				bindPostcode = true;

				sb.append(_FINDER_COLUMN_COMPANYID_POSTCODE_POSTCODE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(SimpleAddressLookupEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				if (bindPostcode) {
					queryPos.add(postcode);
				}

				list = (List<SimpleAddressLookupEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry findByCompanyId_Postcode_First(
			long companyId, String postcode,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException {

		SimpleAddressLookupEntry simpleAddressLookupEntry =
			fetchByCompanyId_Postcode_First(
				companyId, postcode, orderByComparator);

		if (simpleAddressLookupEntry != null) {
			return simpleAddressLookupEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", postcode=");
		sb.append(postcode);

		sb.append("}");

		throw new NoSuchSimpleAddressLookupEntryException(sb.toString());
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry fetchByCompanyId_Postcode_First(
		long companyId, String postcode,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		List<SimpleAddressLookupEntry> list = findByCompanyId_Postcode(
			companyId, postcode, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry findByCompanyId_Postcode_Last(
			long companyId, String postcode,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException {

		SimpleAddressLookupEntry simpleAddressLookupEntry =
			fetchByCompanyId_Postcode_Last(
				companyId, postcode, orderByComparator);

		if (simpleAddressLookupEntry != null) {
			return simpleAddressLookupEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", postcode=");
		sb.append(postcode);

		sb.append("}");

		throw new NoSuchSimpleAddressLookupEntryException(sb.toString());
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	@Override
	public SimpleAddressLookupEntry fetchByCompanyId_Postcode_Last(
		long companyId, String postcode,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		int count = countByCompanyId_Postcode(companyId, postcode);

		if (count == 0) {
			return null;
		}

		List<SimpleAddressLookupEntry> list = findByCompanyId_Postcode(
			companyId, postcode, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the simple address lookup entries before and after the current simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the current simple address lookup entry
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public SimpleAddressLookupEntry[] findByCompanyId_Postcode_PrevAndNext(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK,
			long companyId, String postcode,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException {

		postcode = Objects.toString(postcode, "");

		SimpleAddressLookupEntry simpleAddressLookupEntry = findByPrimaryKey(
			simpleAddressLookupEntryPK);

		Session session = null;

		try {
			session = openSession();

			SimpleAddressLookupEntry[] array =
				new SimpleAddressLookupEntryImpl[3];

			array[0] = getByCompanyId_Postcode_PrevAndNext(
				session, simpleAddressLookupEntry, companyId, postcode,
				orderByComparator, true);

			array[1] = simpleAddressLookupEntry;

			array[2] = getByCompanyId_Postcode_PrevAndNext(
				session, simpleAddressLookupEntry, companyId, postcode,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected SimpleAddressLookupEntry getByCompanyId_Postcode_PrevAndNext(
		Session session, SimpleAddressLookupEntry simpleAddressLookupEntry,
		long companyId, String postcode,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY_WHERE);

		sb.append(_FINDER_COLUMN_COMPANYID_POSTCODE_COMPANYID_2);

		boolean bindPostcode = false;

		if (postcode.isEmpty()) {
			sb.append(_FINDER_COLUMN_COMPANYID_POSTCODE_POSTCODE_3);
		}
		else {
			bindPostcode = true;

			sb.append(_FINDER_COLUMN_COMPANYID_POSTCODE_POSTCODE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(SimpleAddressLookupEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(companyId);

		if (bindPostcode) {
			queryPos.add(postcode);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						simpleAddressLookupEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<SimpleAddressLookupEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the simple address lookup entries where companyId = &#63; and postcode = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 */
	@Override
	public void removeByCompanyId_Postcode(long companyId, String postcode) {
		for (SimpleAddressLookupEntry simpleAddressLookupEntry :
				findByCompanyId_Postcode(
					companyId, postcode, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(simpleAddressLookupEntry);
		}
	}

	/**
	 * Returns the number of simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @return the number of matching simple address lookup entries
	 */
	@Override
	public int countByCompanyId_Postcode(long companyId, String postcode) {
		postcode = Objects.toString(postcode, "");

		FinderPath finderPath = _finderPathCountByCompanyId_Postcode;

		Object[] finderArgs = new Object[] {companyId, postcode};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_SIMPLEADDRESSLOOKUPENTRY_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYID_POSTCODE_COMPANYID_2);

			boolean bindPostcode = false;

			if (postcode.isEmpty()) {
				sb.append(_FINDER_COLUMN_COMPANYID_POSTCODE_POSTCODE_3);
			}
			else {
				bindPostcode = true;

				sb.append(_FINDER_COLUMN_COMPANYID_POSTCODE_POSTCODE_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				if (bindPostcode) {
					queryPos.add(postcode);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_POSTCODE_COMPANYID_2 =
		"simpleAddressLookupEntry.id.companyId = ? AND ";

	private static final String _FINDER_COLUMN_COMPANYID_POSTCODE_POSTCODE_2 =
		"simpleAddressLookupEntry.postcode = ?";

	private static final String _FINDER_COLUMN_COMPANYID_POSTCODE_POSTCODE_3 =
		"(simpleAddressLookupEntry.postcode IS NULL OR simpleAddressLookupEntry.postcode = '')";

	public SimpleAddressLookupEntryPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(SimpleAddressLookupEntry.class);

		setModelImplClass(SimpleAddressLookupEntryImpl.class);
		setModelPKClass(SimpleAddressLookupEntryPK.class);

		setTable(SimpleAddressLookupEntryTable.INSTANCE);
	}

	/**
	 * Caches the simple address lookup entry in the entity cache if it is enabled.
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 */
	@Override
	public void cacheResult(SimpleAddressLookupEntry simpleAddressLookupEntry) {
		entityCache.putResult(
			SimpleAddressLookupEntryImpl.class,
			simpleAddressLookupEntry.getPrimaryKey(), simpleAddressLookupEntry);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the simple address lookup entries in the entity cache if it is enabled.
	 *
	 * @param simpleAddressLookupEntries the simple address lookup entries
	 */
	@Override
	public void cacheResult(
		List<SimpleAddressLookupEntry> simpleAddressLookupEntries) {

		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (simpleAddressLookupEntries.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (SimpleAddressLookupEntry simpleAddressLookupEntry :
				simpleAddressLookupEntries) {

			if (entityCache.getResult(
					SimpleAddressLookupEntryImpl.class,
					simpleAddressLookupEntry.getPrimaryKey()) == null) {

				cacheResult(simpleAddressLookupEntry);
			}
		}
	}

	/**
	 * Clears the cache for all simple address lookup entries.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(SimpleAddressLookupEntryImpl.class);

		finderCache.clearCache(SimpleAddressLookupEntryImpl.class);
	}

	/**
	 * Clears the cache for the simple address lookup entry.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(SimpleAddressLookupEntry simpleAddressLookupEntry) {
		entityCache.removeResult(
			SimpleAddressLookupEntryImpl.class, simpleAddressLookupEntry);
	}

	@Override
	public void clearCache(
		List<SimpleAddressLookupEntry> simpleAddressLookupEntries) {

		for (SimpleAddressLookupEntry simpleAddressLookupEntry :
				simpleAddressLookupEntries) {

			entityCache.removeResult(
				SimpleAddressLookupEntryImpl.class, simpleAddressLookupEntry);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(SimpleAddressLookupEntryImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				SimpleAddressLookupEntryImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new simple address lookup entry with the primary key. Does not add the simple address lookup entry to the database.
	 *
	 * @param simpleAddressLookupEntryPK the primary key for the new simple address lookup entry
	 * @return the new simple address lookup entry
	 */
	@Override
	public SimpleAddressLookupEntry create(
		SimpleAddressLookupEntryPK simpleAddressLookupEntryPK) {

		SimpleAddressLookupEntry simpleAddressLookupEntry =
			new SimpleAddressLookupEntryImpl();

		simpleAddressLookupEntry.setNew(true);
		simpleAddressLookupEntry.setPrimaryKey(simpleAddressLookupEntryPK);

		String uuid = PortalUUIDUtil.generate();

		simpleAddressLookupEntry.setUuid(uuid);

		simpleAddressLookupEntry.setCompanyId(
			CompanyThreadLocal.getCompanyId());

		return simpleAddressLookupEntry;
	}

	/**
	 * Removes the simple address lookup entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public SimpleAddressLookupEntry remove(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws NoSuchSimpleAddressLookupEntryException {

		return remove((Serializable)simpleAddressLookupEntryPK);
	}

	/**
	 * Removes the simple address lookup entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public SimpleAddressLookupEntry remove(Serializable primaryKey)
		throws NoSuchSimpleAddressLookupEntryException {

		Session session = null;

		try {
			session = openSession();

			SimpleAddressLookupEntry simpleAddressLookupEntry =
				(SimpleAddressLookupEntry)session.get(
					SimpleAddressLookupEntryImpl.class, primaryKey);

			if (simpleAddressLookupEntry == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSimpleAddressLookupEntryException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(simpleAddressLookupEntry);
		}
		catch (NoSuchSimpleAddressLookupEntryException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected SimpleAddressLookupEntry removeImpl(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(simpleAddressLookupEntry)) {
				simpleAddressLookupEntry =
					(SimpleAddressLookupEntry)session.get(
						SimpleAddressLookupEntryImpl.class,
						simpleAddressLookupEntry.getPrimaryKeyObj());
			}

			if (simpleAddressLookupEntry != null) {
				session.delete(simpleAddressLookupEntry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (simpleAddressLookupEntry != null) {
			clearCache(simpleAddressLookupEntry);
		}

		return simpleAddressLookupEntry;
	}

	@Override
	public SimpleAddressLookupEntry updateImpl(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		boolean isNew = simpleAddressLookupEntry.isNew();

		if (!(simpleAddressLookupEntry instanceof
				SimpleAddressLookupEntryModelImpl)) {

			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(simpleAddressLookupEntry.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					simpleAddressLookupEntry);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in simpleAddressLookupEntry proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom SimpleAddressLookupEntry implementation " +
					simpleAddressLookupEntry.getClass());
		}

		SimpleAddressLookupEntryModelImpl simpleAddressLookupEntryModelImpl =
			(SimpleAddressLookupEntryModelImpl)simpleAddressLookupEntry;

		if (Validator.isNull(simpleAddressLookupEntry.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			simpleAddressLookupEntry.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(simpleAddressLookupEntry);
			}
			else {
				simpleAddressLookupEntry =
					(SimpleAddressLookupEntry)session.merge(
						simpleAddressLookupEntry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			SimpleAddressLookupEntryImpl.class,
			simpleAddressLookupEntryModelImpl, false, true);

		if (isNew) {
			simpleAddressLookupEntry.setNew(false);
		}

		simpleAddressLookupEntry.resetOriginalValues();

		return simpleAddressLookupEntry;
	}

	/**
	 * Returns the simple address lookup entry with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the simple address lookup entry
	 * @return the simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public SimpleAddressLookupEntry findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSimpleAddressLookupEntryException {

		SimpleAddressLookupEntry simpleAddressLookupEntry = fetchByPrimaryKey(
			primaryKey);

		if (simpleAddressLookupEntry == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSimpleAddressLookupEntryException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return simpleAddressLookupEntry;
	}

	/**
	 * Returns the simple address lookup entry with the primary key or throws a <code>NoSuchSimpleAddressLookupEntryException</code> if it could not be found.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public SimpleAddressLookupEntry findByPrimaryKey(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws NoSuchSimpleAddressLookupEntryException {

		return findByPrimaryKey((Serializable)simpleAddressLookupEntryPK);
	}

	/**
	 * Returns the simple address lookup entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry, or <code>null</code> if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public SimpleAddressLookupEntry fetchByPrimaryKey(
		SimpleAddressLookupEntryPK simpleAddressLookupEntryPK) {

		return fetchByPrimaryKey((Serializable)simpleAddressLookupEntryPK);
	}

	/**
	 * Returns all the simple address lookup entries.
	 *
	 * @return the simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findAll(
		int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of simple address lookup entries
	 */
	@Override
	public List<SimpleAddressLookupEntry> findAll(
		int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<SimpleAddressLookupEntry> list = null;

		if (useFinderCache) {
			list = (List<SimpleAddressLookupEntry>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY;

				sql = sql.concat(
					SimpleAddressLookupEntryModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<SimpleAddressLookupEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the simple address lookup entries from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (SimpleAddressLookupEntry simpleAddressLookupEntry : findAll()) {
			remove(simpleAddressLookupEntry);
		}
	}

	/**
	 * Returns the number of simple address lookup entries.
	 *
	 * @return the number of simple address lookup entries
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(
					_SQL_COUNT_SIMPLEADDRESSLOOKUPENTRY);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	public Set<String> getCompoundPKColumnNames() {
		return _compoundPKColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "simpleAddressLookupEntryPK";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return SimpleAddressLookupEntryModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the simple address lookup entry persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByCompanyId_Postcode = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId_Postcode",
			new String[] {
				Long.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"companyId", "postcode"}, true);

		_finderPathWithoutPaginationFindByCompanyId_Postcode = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCompanyId_Postcode",
			new String[] {Long.class.getName(), String.class.getName()},
			new String[] {"companyId", "postcode"}, true);

		_finderPathCountByCompanyId_Postcode = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyId_Postcode",
			new String[] {Long.class.getName(), String.class.getName()},
			new String[] {"companyId", "postcode"}, false);

		SimpleAddressLookupEntryUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		SimpleAddressLookupEntryUtil.setPersistence(null);

		entityCache.removeCache(SimpleAddressLookupEntryImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_AddressLookupSimplePersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_AddressLookupSimplePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_AddressLookupSimplePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY =
		"SELECT simpleAddressLookupEntry FROM SimpleAddressLookupEntry simpleAddressLookupEntry";

	private static final String _SQL_SELECT_SIMPLEADDRESSLOOKUPENTRY_WHERE =
		"SELECT simpleAddressLookupEntry FROM SimpleAddressLookupEntry simpleAddressLookupEntry WHERE ";

	private static final String _SQL_COUNT_SIMPLEADDRESSLOOKUPENTRY =
		"SELECT COUNT(simpleAddressLookupEntry) FROM SimpleAddressLookupEntry simpleAddressLookupEntry";

	private static final String _SQL_COUNT_SIMPLEADDRESSLOOKUPENTRY_WHERE =
		"SELECT COUNT(simpleAddressLookupEntry) FROM SimpleAddressLookupEntry simpleAddressLookupEntry WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS =
		"simpleAddressLookupEntry.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No SimpleAddressLookupEntry exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No SimpleAddressLookupEntry exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		SimpleAddressLookupEntryPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});
	private static final Set<String> _compoundPKColumnNames = SetUtil.fromArray(
		new String[] {"companyId", "uprn"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}