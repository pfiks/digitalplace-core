package com.placecube.digitalplace.user.account.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.user.account.service.AccountService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class CreateAccountPortletTest extends PowerMockito {

	@InjectMocks
	private CreateAccountPortlet createAccountPortlet;

	@Mock
	private AccountService mockAccountService;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Test
	public void render_WhenExceptionCheckingAccountCreationDoesNothing_ThenDontSetAccountCreationDisabledAttribute() throws Exception {
		mockCallToSuper();
		doNothing().when(mockAccountService).checkAccountCreationEnabled(mockRenderRequest);

		createAccountPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(0)).setAttribute("accountCreationDisabled", true);
	}

	@Test
	public void render_WhenExceptionCheckingAccountCreationThrowsException_ThenSetAccountCreationDisabledAttribute() throws Exception {
		mockCallToSuper();
		doThrow(new PortletException()).when(mockAccountService).checkAccountCreationEnabled(mockRenderRequest);

		createAccountPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("accountCreationDisabled", true);
	}

	@SuppressWarnings("rawtypes")
	private void mockCallToSuper() throws Exception {
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);
	}
}
