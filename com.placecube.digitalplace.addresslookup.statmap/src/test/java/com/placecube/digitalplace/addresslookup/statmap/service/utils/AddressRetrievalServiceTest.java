package com.placecube.digitalplace.addresslookup.statmap.service.utils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.liferay.petra.string.StringPool;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.statmap.constants.StatMapConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HttpClients.class, EntityUtils.class })
public class AddressRetrievalServiceTest extends PowerMockito {

	@InjectMocks
	private AddressRetrievalService addressRetrievalService;

	@Mock
	private AddressRetrivalUtil mockAddressRetrivalUtil;

	@Mock
	private CloseableHttpClient mockHttpClient;

	@Mock
	private HttpEntity mockHttpEntity;

	@Mock
	private HttpGet mockHttpGet;

	@Mock
	private CloseableHttpResponse mockHttpResponse;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private JSONObject mockJSONObject2;

	@Before
	public void activeSetUp() {
		mockStatic(HttpClients.class, EntityUtils.class);
	}

	@Test
	public void executeCall_WhenException_ThenReturnsEmptyString() throws ClientProtocolException, IOException {
		String url = "urlToCallValue";
		when(mockAddressRetrivalUtil.getHttpGetMethod(url)).thenReturn(mockHttpGet);
		when(HttpClients.createDefault()).thenReturn(mockHttpClient);
		when(HttpClients.createDefault()).thenReturn(mockHttpClient);
		when(mockHttpClient.execute(mockHttpGet)).thenReturn(mockHttpResponse);
		when(mockHttpResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenThrow(new IOException());

		String result = addressRetrievalService.executeCall(url);

		assertThat(result, equalTo(""));
		verify(mockAddressRetrivalUtil, times(1)).quietlyCloseResources(mockHttpClient, mockHttpResponse);
	}

	@Test
	public void executeCall_WhenNoError_ThenReturnsTheResponseEntityString() throws ClientProtocolException, IOException {
		String url = "urlToCallValue";
		when(mockAddressRetrivalUtil.getHttpGetMethod(url)).thenReturn(mockHttpGet);
		when(HttpClients.createDefault()).thenReturn(mockHttpClient);
		when(mockHttpClient.execute(mockHttpGet)).thenReturn(mockHttpResponse);
		when(mockHttpResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn("expectedResponse");

		String result = addressRetrievalService.executeCall(url);

		assertThat(result, equalTo("expectedResponse"));
		verify(mockAddressRetrivalUtil, times(1)).quietlyCloseResources(mockHttpClient, mockHttpResponse);
	}

	@Test
	public void getAddressContextFromResponse_WhenNoError_ThenReturnsTheAddressContext() throws JSONException {
		when(mockJSONFactory.createJSONObject("responseValue")).thenReturn(mockJSONObject);
		when(mockJSONObject.getString(StatMapConstants.JSON_FIELD_RECORD)).thenReturn("recordValue");
		when(mockJSONFactory.createJSONObject("recordValue")).thenReturn(mockJSONObject2);
		when(mockJSONObject2.getString(StatMapConstants.JSON_FIELD_UPRN)).thenReturn("uprnValue");
		when(mockJSONObject2.getString(StatMapConstants.JSON_FIELD_SAO_TEXT)).thenReturn("saoTextValue");
		when(mockJSONObject2.getString(StatMapConstants.JSON_FIELD_PAO_TEXT)).thenReturn("paoTextValue");
		when(mockAddressRetrivalUtil.getJoinedValuesFromFieldNamesWithNoSpace(mockJSONObject2, StatMapConstants.JSON_FIELD_SAO_START_NUMBER, StatMapConstants.JSON_FIELD_SAO_START_SUFFIX))
				.thenReturn("saoStart");
		when(mockAddressRetrivalUtil.getJoinedValuesFromFieldNamesWithNoSpace(mockJSONObject2, StatMapConstants.JSON_FIELD_SAO_END_NUMBER, StatMapConstants.JSON_FIELD_SAO_END_SUFFIX))
				.thenReturn("saoEnd");
		when(mockJSONObject2.getString(StatMapConstants.JSON_FIELD_ORGANISATION)).thenReturn("orgValue");
		when(mockAddressRetrivalUtil.getJoinedValuesWithSpace("orgValue","saoTextValue", "saoStart", "saoEnd", "paoTextValue", "paoStart", "paoEnd")).thenReturn("address1value");
		when(mockAddressRetrivalUtil.getJoinedValuesFromFieldNamesWithNoSpace(mockJSONObject2, StatMapConstants.JSON_FIELD_PAO_START_NUMBER, StatMapConstants.JSON_FIELD_PAO_START_SUFFIX))
				.thenReturn("paoStart");
		when(mockAddressRetrivalUtil.getJoinedValuesFromFieldNamesWithNoSpace(mockJSONObject2, StatMapConstants.JSON_FIELD_PAO_END_NUMBER, StatMapConstants.JSON_FIELD_PAO_END_SUFFIX))
				.thenReturn("paoEnd");
		when(mockJSONObject2.getString(StatMapConstants.JSON_FIELD_STREET_DESCRIPTOR)).thenReturn("streetValue");
		when(mockJSONObject2.getString(StatMapConstants.JSON_FIELD_POSTCODE)).thenReturn("postcodeValue");
		when(mockJSONObject2.getString(StatMapConstants.JSON_FIELD_TOWN_NAME)).thenReturn("cityValue");

		AddressContext result = addressRetrievalService.getAddressContextFromResponse("responseValue");

		assertThat(result.getAddressLine1(), equalTo("address1value"));
		assertThat(result.getAddressLine2(), equalTo("streetValue"));
		assertThat(result.getAddressLine3(), equalTo(StringPool.BLANK));
		assertThat(result.getAddressLine4(), equalTo(StringPool.BLANK));
		assertThat(result.getCity(), equalTo("cityValue"));
		assertThat(result.getPostcode(), equalTo("postcodeValue"));
		assertThat(result.getUPRN(), equalTo("uprnValue"));
	}

	@Test
	public void getAddressIdsFromResponse_WhenResponseHasNoIds_ThenReturnsEmptySet() throws JSONException {
		when(mockJSONFactory.createJSONObject("responseValue")).thenReturn(mockJSONObject);
		when(mockJSONObject.getJSONArray(StatMapConstants.JSON_FIELD_IDS)).thenReturn(null);

		Set<String> results = addressRetrievalService.getAddressIdsFromResponse("responseValue");

		assertTrue(results.isEmpty());
	}

	@Test
	public void getAddressIdsFromResponse_WhenResponseIsBlank_ThenReturnsEmptySet() throws JSONException {
		Set<String> results = addressRetrievalService.getAddressIdsFromResponse(null);

		assertTrue(results.isEmpty());
	}

	@Test
	public void getAddressIdsFromResponse_WhenResponseIsNotBlank_ThenReturnsSetWithTheIds() throws JSONException {
		when(mockJSONFactory.createJSONObject("responseValue")).thenReturn(mockJSONObject);
		when(mockJSONObject.getJSONArray(StatMapConstants.JSON_FIELD_IDS)).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(2);
		when(mockJSONArray.getString(0)).thenReturn("value1");
		when(mockJSONArray.getString(1)).thenReturn("value2");

		Set<String> results = addressRetrievalService.getAddressIdsFromResponse("responseValue");

		assertThat(results.size(), equalTo(2));
		assertThat(results, contains("value1", "value2"));
	}

	@Test
	public void getRecordURL_WhenBaseUrlEndsWithoutSlash_ThenReturnsTheUrlWithTheRecordId() {
		when(mockAddressRetrivalUtil.getURL("baseUrl/recordId", new ArrayList<>(), "tokenValue")).thenReturn("expectedResult");

		String result = addressRetrievalService.getRecordURL("baseUrl", "tokenValue", "recordId");

		assertThat(result, equalTo("expectedResult"));
	}

	@Test
	public void getRecordURL_WhenBaseUrlEndsWithSlash_ThenReturnsTheUrlWithTheRecordId() {
		when(mockAddressRetrivalUtil.getURL("baseUrl/recordId", new ArrayList<>(), "tokenValue")).thenReturn("expectedResult");

		String result = addressRetrievalService.getRecordURL("baseUrl/", "tokenValue","recordId");

		assertThat(result, equalTo("expectedResult"));
	}

	@Test
	public void getSearchURL_WhenNoError_ThenReturnsTheSearchUrlWithTheParameters() {
		List<NameValuePair> nvps = new ArrayList<>();
		nvps.add(new BasicNameValuePair("searchParameterNameValue", "searchTermValue"));
		when(mockAddressRetrivalUtil.getURL("baseUrl", nvps, "tokenValue")).thenReturn("expectedResult");

		String result = addressRetrievalService.getSearchURL("baseUrl", "tokenValue", "searchParameterNameValue", "searchTermValue");

		assertThat(result, equalTo("expectedResult"));
	}
}
