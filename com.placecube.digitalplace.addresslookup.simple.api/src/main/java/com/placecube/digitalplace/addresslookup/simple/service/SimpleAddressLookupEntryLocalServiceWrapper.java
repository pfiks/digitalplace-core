/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link SimpleAddressLookupEntryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SimpleAddressLookupEntryLocalService
 * @generated
 */
public class SimpleAddressLookupEntryLocalServiceWrapper
	implements ServiceWrapper<SimpleAddressLookupEntryLocalService>,
			   SimpleAddressLookupEntryLocalService {

	public SimpleAddressLookupEntryLocalServiceWrapper() {
		this(null);
	}

	public SimpleAddressLookupEntryLocalServiceWrapper(
		SimpleAddressLookupEntryLocalService
			simpleAddressLookupEntryLocalService) {

		_simpleAddressLookupEntryLocalService =
			simpleAddressLookupEntryLocalService;
	}

	/**
	 * Adds the simple address lookup entry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 * @return the simple address lookup entry that was added
	 */
	@Override
	public com.placecube.digitalplace.addresslookup.simple.model.
		SimpleAddressLookupEntry addSimpleAddressLookupEntry(
			com.placecube.digitalplace.addresslookup.simple.model.
				SimpleAddressLookupEntry simpleAddressLookupEntry) {

		return _simpleAddressLookupEntryLocalService.
			addSimpleAddressLookupEntry(simpleAddressLookupEntry);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _simpleAddressLookupEntryLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Creates a new simple address lookup entry with the primary key. Does not add the simple address lookup entry to the database.
	 *
	 * @param simpleAddressLookupEntryPK the primary key for the new simple address lookup entry
	 * @return the new simple address lookup entry
	 */
	@Override
	public com.placecube.digitalplace.addresslookup.simple.model.
		SimpleAddressLookupEntry createSimpleAddressLookupEntry(
			com.placecube.digitalplace.addresslookup.simple.service.persistence.
				SimpleAddressLookupEntryPK simpleAddressLookupEntryPK) {

		return _simpleAddressLookupEntryLocalService.
			createSimpleAddressLookupEntry(simpleAddressLookupEntryPK);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _simpleAddressLookupEntryLocalService.deletePersistedModel(
			persistedModel);
	}

	/**
	 * Deletes the simple address lookup entry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 */
	@Override
	public com.placecube.digitalplace.addresslookup.simple.model.
		SimpleAddressLookupEntry deleteSimpleAddressLookupEntry(
			com.placecube.digitalplace.addresslookup.simple.model.
				SimpleAddressLookupEntry simpleAddressLookupEntry) {

		return _simpleAddressLookupEntryLocalService.
			deleteSimpleAddressLookupEntry(simpleAddressLookupEntry);
	}

	/**
	 * Deletes the simple address lookup entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 * @throws PortalException if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.addresslookup.simple.model.
		SimpleAddressLookupEntry deleteSimpleAddressLookupEntry(
				com.placecube.digitalplace.addresslookup.simple.service.
					persistence.SimpleAddressLookupEntryPK
						simpleAddressLookupEntryPK)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _simpleAddressLookupEntryLocalService.
			deleteSimpleAddressLookupEntry(simpleAddressLookupEntryPK);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _simpleAddressLookupEntryLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _simpleAddressLookupEntryLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _simpleAddressLookupEntryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _simpleAddressLookupEntryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _simpleAddressLookupEntryLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _simpleAddressLookupEntryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _simpleAddressLookupEntryLocalService.dynamicQueryCount(
			dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _simpleAddressLookupEntryLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.addresslookup.simple.model.
		SimpleAddressLookupEntry fetchSimpleAddressLookupEntry(
			com.placecube.digitalplace.addresslookup.simple.service.persistence.
				SimpleAddressLookupEntryPK simpleAddressLookupEntryPK) {

		return _simpleAddressLookupEntryLocalService.
			fetchSimpleAddressLookupEntry(simpleAddressLookupEntryPK);
	}

	/**
	 * Returns the simple address lookup entry with the matching UUID and company.
	 *
	 * @param uuid the simple address lookup entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	@Override
	public com.placecube.digitalplace.addresslookup.simple.model.
		SimpleAddressLookupEntry
			fetchSimpleAddressLookupEntryByUuidAndCompanyId(
				String uuid, long companyId) {

		return _simpleAddressLookupEntryLocalService.
			fetchSimpleAddressLookupEntryByUuidAndCompanyId(uuid, companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _simpleAddressLookupEntryLocalService.
			getActionableDynamicQuery();
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.addresslookup.simple.model.
			SimpleAddressLookupEntry> getByCompanyIdAndPostcode(
				long companyId, String postcode) {

		return _simpleAddressLookupEntryLocalService.getByCompanyIdAndPostcode(
			companyId, postcode);
	}

	@Override
	public java.util.Optional
		<com.placecube.digitalplace.addresslookup.simple.model.
			SimpleAddressLookupEntry> getByCompanyIdAndUprn(
				long companyId, String uprn) {

		return _simpleAddressLookupEntryLocalService.getByCompanyIdAndUprn(
			companyId, uprn);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _simpleAddressLookupEntryLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _simpleAddressLookupEntryLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _simpleAddressLookupEntryLocalService.getPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Returns a range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of simple address lookup entries
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.addresslookup.simple.model.
			SimpleAddressLookupEntry> getSimpleAddressLookupEntries(
				int start, int end) {

		return _simpleAddressLookupEntryLocalService.
			getSimpleAddressLookupEntries(start, end);
	}

	/**
	 * Returns the number of simple address lookup entries.
	 *
	 * @return the number of simple address lookup entries
	 */
	@Override
	public int getSimpleAddressLookupEntriesCount() {
		return _simpleAddressLookupEntryLocalService.
			getSimpleAddressLookupEntriesCount();
	}

	/**
	 * Returns the simple address lookup entry with the primary key.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry
	 * @throws PortalException if a simple address lookup entry with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.addresslookup.simple.model.
		SimpleAddressLookupEntry getSimpleAddressLookupEntry(
				com.placecube.digitalplace.addresslookup.simple.service.
					persistence.SimpleAddressLookupEntryPK
						simpleAddressLookupEntryPK)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _simpleAddressLookupEntryLocalService.
			getSimpleAddressLookupEntry(simpleAddressLookupEntryPK);
	}

	/**
	 * Returns the simple address lookup entry with the matching UUID and company.
	 *
	 * @param uuid the simple address lookup entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching simple address lookup entry
	 * @throws PortalException if a matching simple address lookup entry could not be found
	 */
	@Override
	public com.placecube.digitalplace.addresslookup.simple.model.
		SimpleAddressLookupEntry getSimpleAddressLookupEntryByUuidAndCompanyId(
				String uuid, long companyId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _simpleAddressLookupEntryLocalService.
			getSimpleAddressLookupEntryByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Updates the simple address lookup entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 * @return the simple address lookup entry that was updated
	 */
	@Override
	public com.placecube.digitalplace.addresslookup.simple.model.
		SimpleAddressLookupEntry updateSimpleAddressLookupEntry(
			com.placecube.digitalplace.addresslookup.simple.model.
				SimpleAddressLookupEntry simpleAddressLookupEntry) {

		return _simpleAddressLookupEntryLocalService.
			updateSimpleAddressLookupEntry(simpleAddressLookupEntry);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _simpleAddressLookupEntryLocalService.getBasePersistence();
	}

	@Override
	public SimpleAddressLookupEntryLocalService getWrappedService() {
		return _simpleAddressLookupEntryLocalService;
	}

	@Override
	public void setWrappedService(
		SimpleAddressLookupEntryLocalService
			simpleAddressLookupEntryLocalService) {

		_simpleAddressLookupEntryLocalService =
			simpleAddressLookupEntryLocalService;
	}

	private SimpleAddressLookupEntryLocalService
		_simpleAddressLookupEntryLocalService;

}