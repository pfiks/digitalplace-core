<%@ include file="../init.jsp" %>

<c:choose>
	<c:when test="${invalidConfiguration}">
		<%@ include file="../invalid-configuration.jspf" %>
	</c:when>

	<c:when test="${displayStyleListGeoPoint}">
		<div>
			${geoPointContent}
		</div>
	</c:when>

	<c:otherwise>

		<liferay-ui:search-container searchContainer="${searchContainer}"
				total="${searchContainer.getTotal()}"
				compactEmptyResultsMessage="true"
				emptyResultsMessage="${searchContainer.getEmptyResultsMessage()}"
				cssClass="${displayStyleList ? 'display-style-list' : 'display-style-grid'}"
				>

			<liferay-ui:search-container-results results="${searchContainer.getResults()}" />

			<liferay-ui:search-container-row className="com.placecube.digitalplace.ddl.web.portlet.ddlsearch.model.DDLDisplayEntry" modelVar="ddlDisplayEntry" cssClass="${displayStyleList ? 'col-md-12' : gridDisplayStyleCss}">

				<liferay-ui:search-container-column-text>
					${ddlDisplayEntry.getContent()}
				</liferay-ui:search-container-column-text>

			</liferay-ui:search-container-row>

			<aui:form useNamespace="<%= false %>">
				<liferay-ui:search-iterator markupView="lexicon" displayStyle="icon" searchContainer="${searchContainer}" paginate="${not hidePagination}" />
			</aui:form>

		</liferay-ui:search-container>

	</c:otherwise>
</c:choose>
