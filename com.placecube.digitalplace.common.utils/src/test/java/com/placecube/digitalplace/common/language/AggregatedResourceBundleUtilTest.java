package com.placecube.digitalplace.common.language;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.placecube.digitalplace.common.internal.utils.DigitalPlaceResourceBundleUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DigitalPlaceResourceBundleUtil.class, LanguageUtil.class })
public class AggregatedResourceBundleUtilTest extends PowerMockito {

	private static final String[] BUNDLE_NAMES = new String[] { "bundle1", "bundle2" };
	private static final String EXPECTED_RESULT = "expectedResultValue";
	private static final Locale LOCALE = Locale.CANADA_FRENCH;
	private static final String MESSAGE_KEY = "messageKeyValue";

	@InjectMocks
	private AggregatedResourceBundleUtil aggregatedResourceBundleUtil;

	@Mock
	private ResourceBundle mockResourceBundle;

	@Before
	public void activateSetup() {
		mockStatic(DigitalPlaceResourceBundleUtil.class, LanguageUtil.class);
	}

	@Test
	public void format_WithMessageKeyAndArgumentAndLocaleAndBundleNamesParameters_WhenNoError_ThenReturnsTheFormattedKeyValue() {
		String argument = "argumentVal";
		when(DigitalPlaceResourceBundleUtil.getAggregatedResourceBundle(LOCALE, BUNDLE_NAMES)).thenReturn(mockResourceBundle);
		when(LanguageUtil.format(mockResourceBundle, MESSAGE_KEY, argument)).thenReturn(EXPECTED_RESULT);

		String result = AggregatedResourceBundleUtil.format(MESSAGE_KEY, argument, LOCALE, BUNDLE_NAMES);

		assertThat(result, equalTo(EXPECTED_RESULT));
	}

	@Test
	public void format_WithMessageKeyAndArgumentsAndLocaleAndBundleNamesParameters_WhenNoError_ThenReturnsTheFormattedKeyValue() {
		String[] arguments = new String[] { "argumentVal1", "argumentVal2" };
		when(DigitalPlaceResourceBundleUtil.getAggregatedResourceBundle(LOCALE, BUNDLE_NAMES)).thenReturn(mockResourceBundle);
		when(LanguageUtil.format(mockResourceBundle, MESSAGE_KEY, arguments)).thenReturn(EXPECTED_RESULT);

		String result = AggregatedResourceBundleUtil.format(MESSAGE_KEY, arguments, LOCALE, BUNDLE_NAMES);

		assertThat(result, equalTo(EXPECTED_RESULT));
	}

	@Test
	public void get_WhenNoError_ThenReturnsTheFormattedKeyValue() {
		when(DigitalPlaceResourceBundleUtil.getAggregatedResourceBundle(LOCALE, BUNDLE_NAMES)).thenReturn(mockResourceBundle);
		when(LanguageUtil.get(mockResourceBundle, MESSAGE_KEY)).thenReturn(EXPECTED_RESULT);

		String result = AggregatedResourceBundleUtil.get(MESSAGE_KEY, LOCALE, BUNDLE_NAMES);

		assertThat(result, equalTo(EXPECTED_RESULT));
	}

	@Test
	public void getAggregatedResourceBundle_WhenNoError_ThenReturnsTheResourceBundle() {
		when(DigitalPlaceResourceBundleUtil.getAggregatedResourceBundle(LOCALE, BUNDLE_NAMES)).thenReturn(mockResourceBundle);

		ResourceBundle result = AggregatedResourceBundleUtil.getAggregatedResourceBundle(LOCALE, BUNDLE_NAMES);

		assertThat(result, sameInstance(mockResourceBundle));
	}
}
