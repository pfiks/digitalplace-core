package com.placecube.digitalplace.group.restadmin.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.group.rest.exception.GroupRestKeyException;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;

@RunWith(PowerMockRunner.class)
public class GroupRestAdminRegenerateAPIKeysMVCActionCommandTest extends PowerMockito {

	@Mock
	private GroupApiKeyLocalService mockGroupApiKeyLocalService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Group mockGroup;

	@InjectMocks
	private GroupRestAdminRegenerateAPIKeysMVCActionCommand groupRestAdminRegenerateAPIKeysMVCActionCommand;

	@Test
	public void doProcessAction_WhenNoError_ThenRegeneratesGroupApiKey() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);

		groupRestAdminRegenerateAPIKeysMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockGroupApiKeyLocalService, times(1)).regenerateGroupApiKey(mockGroup);
	}

	@Test(expected = GroupRestKeyException.class)
	public void doProcessAction_WhenException_ThenThrowsGroupRestKeyException() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		doThrow(new GroupRestKeyException()).when(mockGroupApiKeyLocalService).regenerateGroupApiKey(mockGroup);

		groupRestAdminRegenerateAPIKeysMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

}
