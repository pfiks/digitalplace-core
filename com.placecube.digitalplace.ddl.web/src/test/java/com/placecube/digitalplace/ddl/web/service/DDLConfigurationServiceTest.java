package com.placecube.digitalplace.ddl.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLSearchListingsPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration;

public class DDLConfigurationServiceTest extends PowerMockito {

	@InjectMocks
	private DDLConfigurationService ddlConfigurationService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private DDLDisplayPortletInstanceConfiguration mockDDLDisplayPortletInstanceConfiguration;

	@Mock
	private DDLSearchListingsPortletInstanceConfiguration mockDDLSearchListingsPortletInstanceConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = ConfigurationException.class)
	public void getDDLDisplayPortletInstanceConfiguration_WhenExceptioNRetrievingTheConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DDLDisplayPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		ddlConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay);
	}

	@Test
	public void getDDLDisplayPortletInstanceConfiguration_WhenNoError_ThenReturnsThePortletConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DDLDisplayPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockDDLDisplayPortletInstanceConfiguration);

		DDLDisplayPortletInstanceConfiguration result = ddlConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay);

		assertThat(result, sameInstance(mockDDLDisplayPortletInstanceConfiguration));
	}

	@Test(expected = ConfigurationException.class)
	public void getDDLSearchListingsPortletInstanceConfiguration_WhenExceptioNRetrievingTheConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DDLSearchListingsPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		ddlConfigurationService.getDDLSearchListingsPortletInstanceConfiguration(mockThemeDisplay);
	}

	@Test
	public void getDDLSearchListingsPortletInstanceConfiguration_WhenNoError_ThenReturnsThePortletConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DDLSearchListingsPortletInstanceConfiguration.class, mockThemeDisplay))
				.thenReturn(mockDDLSearchListingsPortletInstanceConfiguration);

		DDLSearchListingsPortletInstanceConfiguration result = ddlConfigurationService.getDDLSearchListingsPortletInstanceConfiguration(mockThemeDisplay);

		assertThat(result, sameInstance(mockDDLSearchListingsPortletInstanceConfiguration));
	}

}
