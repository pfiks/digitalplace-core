package com.placecube.digitalplace.navigation.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.navigation.constants.ConfigurationConstants;

@Component(immediate = true, service = AssetVocabularyService.class)
public class AssetVocabularyService {

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;

	public long getVocabularyId(long groupId, String vocabularyId) {

		if (ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID.equalsIgnoreCase(vocabularyId)) {

			Optional<AssetVocabulary> vocabOptional = Optional
					.ofNullable(assetVocabularyLocalService.fetchGroupVocabulary(groupId, ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_NAME));
			return vocabOptional.isPresent() ? vocabOptional.get().getVocabularyId() : GetterUtil.getLong(ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID);
		} else {
			return GetterUtil.getLong(vocabularyId, Long.valueOf(ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID));
		}
	}

}
