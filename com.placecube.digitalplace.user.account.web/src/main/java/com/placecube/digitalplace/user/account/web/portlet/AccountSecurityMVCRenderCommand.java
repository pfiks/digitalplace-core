package com.placecube.digitalplace.user.account.web.portlet;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.ACCOUNT_SECURITY }, service = MVCRenderCommand.class)
public class AccountSecurityMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AccountContextService accountContextService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(renderRequest);

			renderRequest.setAttribute(RequestKeys.ACCOUNT_CONTEXT, accountContext);
			renderRequest.setAttribute(RequestKeys.TWO_FACTOR_AUTHENTICATION_ENABLED_OPTIONS, getTwoFactorAuthenticationEnabledOptions());
			renderRequest.setAttribute(RequestKeys.CAPTCHA_ENABLED, accountContextService.isShowCaptchaOnAccountSecurityStep(accountContext, renderRequest));
			accountContextService.updateContextInSession(accountContext, renderRequest);
		} catch (UserAccountException e) {
			throw new PortletException(e);
		}
		return "/account-security.jsp";
	}

	private Map<String, String> getTwoFactorAuthenticationEnabledOptions() {

		Map<String, String> twoFactorAuthenticationEnabledOptions = new LinkedHashMap<>();

		twoFactorAuthenticationEnabledOptions.put(Boolean.TRUE.toString(), AccountConstants.I_WANT_TO_SETUP_2FA_LABEL);

		twoFactorAuthenticationEnabledOptions.put(Boolean.FALSE.toString(), AccountConstants.I_DONT_WANT_TO_SETUP_2FA_LABEL);

		return twoFactorAuthenticationEnabledOptions;
	}
}