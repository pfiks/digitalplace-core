package com.placecube.digitalplace.payment.govuk.service.commerce.servlet;

import static com.placecube.digitalplace.payment.govuk.service.internal.GovUKPaymentOrderFactory.PAYMENT_ID;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.commerce.constants.CommerceOrderPaymentConstants;
import com.liferay.commerce.model.CommerceOrder;
import com.liferay.commerce.model.CommerceOrderPayment;
import com.liferay.commerce.service.CommerceOrderLocalService;
import com.liferay.commerce.service.CommerceOrderPaymentLocalService;
import com.liferay.petra.string.StringUtil;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.servlet.PortalSessionThreadLocal;
import com.liferay.portal.kernel.settings.GroupServiceSettingsLocator;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.govuk.configuration.GovUKCompanyConfiguration;
import com.placecube.digitalplace.payment.govuk.service.GovUKAuthKeyService;
import com.placecube.digitalplace.payment.govuk.service.commerce.GovUKCommerceConstants;
import com.placecube.digitalplace.payment.govuk.service.commerce.configuration.GovUKPayGroupServiceConfiguration;
import com.placecube.digitalplace.payment.govuk.service.internal.GovUKPaymentSecurityUtil;

@Component(
	property = {
		"osgi.http.whiteboard.context.path=/" + GovUKCommerceConstants.STATUS_SERVLET_PATH,
		"osgi.http.whiteboard.servlet.name=com.placecube.digitalplace.payment.govuk.service.commerce.servlet.GovUKPayCommercePaymentStatusUpdateServlet",
		"osgi.http.whiteboard.servlet.pattern=/" + GovUKCommerceConstants.STATUS_SERVLET_PATH + "/*"
	},
	service = Servlet.class
)
public class GovUKPayCommercePaymentStatusUpdateServlet extends HttpServlet {

	private static final Log LOG = LogFactoryUtil.getLog(GovUKPayCommercePaymentStatusUpdateServlet.class);

	@Reference
	private CommerceOrderLocalService commerceOrderLocalService;

	@Reference
	private CommerceOrderPaymentLocalService commerceOrderPaymentLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private GovUKAuthKeyService govUKAuthKeyService;

	@Reference
	private GovUKPaymentSecurityUtil govUKPaymentSecurityUtil;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

		try {
			if (PortalSessionThreadLocal.getHttpSession() == null) {
				PortalSessionThreadLocal.setHttpSession(httpServletRequest.getSession());
			}
			ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
			serviceContext.setRequest(httpServletRequest);

			String httpContent = StringUtil.read(httpServletRequest.getInputStream());
			JSONObject content = jsonFactory.createJSONObject(httpContent);
			JSONObject resource = content.getJSONObject("resource");
			String paymentId = resource.getString(PAYMENT_ID);
			String reference = resource.getString("reference");
			JSONObject state = resource.getJSONObject("state");
			String status = state.getString("status");
			boolean finished = state.getBoolean("finished");
			CommerceOrder commerceOrder = commerceOrderLocalService.getCommerceOrderByExternalReferenceCode(reference, PortalUtil.getDefaultCompanyId());

			GovUKCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(GovUKCompanyConfiguration.class, serviceContext.getCompanyId());
			GovUKPayGroupServiceConfiguration channelConfiguration = configurationProvider.getConfiguration(
					GovUKPayGroupServiceConfiguration.class, new GroupServiceSettingsLocator(commerceOrder.getGroupId(), GovUKCommerceConstants.SERVICE_NAME));
			String secretKey = govUKAuthKeyService.getApiMessageVerificationSecretKey(configuration, channelConfiguration.keyName());

			String hmac = govUKPaymentSecurityUtil.hmacWithJava(GovUKPaymentSecurityUtil.ALGORITHM, httpContent, secretKey);
			String paySignatureHeader = httpServletRequest.getHeader("Pay-Signature");

			if (Validator.isNull(hmac) || !hmac.equals(paySignatureHeader)) {
				LOG.error("Incorrect HMAC - disregarding request");
				return;
			}

			CommerceOrderPayment latestCommerceOrderPayment = commerceOrderPaymentLocalService.fetchLatestCommerceOrderPayment(commerceOrder.getCommerceOrderId());
			if (latestCommerceOrderPayment.getStatus() == CommerceOrderPaymentConstants.STATUS_COMPLETED) {
				String completedPaymentContentJSON = latestCommerceOrderPayment.getContent();
				JSONObject completedPaymentContent = jsonFactory.createJSONObject(completedPaymentContentJSON);
				if (paymentId.equals(completedPaymentContent.getString("paymentId"))) {
					LOG.info("SKIPPING - already have completed status");
					httpServletResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
					return; // There already is a commerceOrderPayment with completed status
				}
			}

			if (finished) {
				if ("success".equals(status)) {
					JSONObject paymentContent = jsonFactory.createJSONObject();
					paymentContent.put("paymentId", paymentId);
					paymentContent.put("webhookGenerated", true);
					commerceOrderPaymentLocalService.addCommerceOrderPayment(commerceOrder.getCommerceOrderId(), CommerceOrderPaymentConstants.STATUS_COMPLETED, paymentContent.toJSONString());
				} else if ("failed".equals(status)) {
					commerceOrderPaymentLocalService.addCommerceOrderPayment(commerceOrder.getCommerceOrderId(), CommerceOrderPaymentConstants.STATUS_FAILED, state.getString("message"));
				} else {
					commerceOrderPaymentLocalService.addCommerceOrderPayment(commerceOrder.getCommerceOrderId(), CommerceOrderPaymentConstants.STATUS_FAILED, status);
				}
			}

			httpServletResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
		}
		catch (Exception exception) {
			LOG.error(exception);
		}
	}

}