package com.placecube.digitalplace.announcementsbanner.web.constants;

public final class PortletKeys {

	public static final String ANNOUNCEMENTS_BANNER = "com_placecube_digitalplace_announcements_banner_web_AnnouncementsBannerPortlet";

	private PortletKeys() {
		return;
	}
}