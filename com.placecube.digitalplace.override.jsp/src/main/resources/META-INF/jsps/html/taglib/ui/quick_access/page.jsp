<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-util:buffer var="html">
	<liferay-util:include page="/html/taglib/ui/quick_access/page.portal.jsp" servletContext="<%= application %>" />
</liferay-util:buffer>

<% 
	html = html.replaceAll("<h1 class=\"sr-only\">Navigation</h1>", "");
%>
	
<%= html %>