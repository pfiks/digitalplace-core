package com.placecube.digitalplace.calendar.web.portlet;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceParameters;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarEvent;
import com.placecube.digitalplace.calendar.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.calendar.web.constants.PortletKeys;
import com.placecube.digitalplace.calendar.web.service.DigitalPlaceCalendarEventParserService;
import com.placecube.digitalplace.calendar.web.service.DigitalPlaceCalendarEventService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.CALENDAR, "mvc.command.name=" + MVCCommandKeys.RESOURCE_LOAD_EVENTS }, service = MVCResourceCommand.class)
public class LoadEventsResourceCommand implements MVCResourceCommand {

	private static final Log LOG = LogFactoryUtil.getLog(LogFactoryUtil.class);

	@Reference
	private DigitalPlaceCalendarEventParserService digitalPlaceCalendarEventParserUtil;

	@Reference
	private DigitalPlaceCalendarEventService digitalPlaceCalendarEventService;

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String currentURL = PortletURLUtil.getCurrent(resourceRequest, resourceResponse).toString();

			long userId = themeDisplay.getUserId();

			long[] groupIds = digitalPlaceCalendarEventService.getCalendarScopeGroupIds(themeDisplay);

			ResourceParameters resourceParameters = resourceRequest.getResourceParameters();
			String start = GetterUtil.getString(resourceParameters.getValue("start"));
			String end = GetterUtil.getString(resourceParameters.getValue("end"));

			DateTimeFormatter timeFormat = digitalPlaceCalendarEventService.getCalendarEventsDateFormatter(themeDisplay);

			LocalDateTime localDateTimeStart = digitalPlaceCalendarEventService.parseDate(start, timeFormat);
			LocalDateTime localDateTimeEnd = digitalPlaceCalendarEventService.parseDate(end, timeFormat);

			List<DigitalPlaceCalendarEvent> calendarEvents = digitalPlaceCalendarEventService.getCalendarEvents(userId, groupIds, localDateTimeStart, localDateTimeEnd, themeDisplay);
			JSONArray json = digitalPlaceCalendarEventParserUtil.parseCalendarEvents(calendarEvents, currentURL);

			resourceResponse.getWriter().write(json.toString());
		} catch (Exception e) {
			LOG.error("Error while getting calendar events (" + e.getMessage() + ")");
			LOG.debug(e);
		}

		return false;
	}
}
