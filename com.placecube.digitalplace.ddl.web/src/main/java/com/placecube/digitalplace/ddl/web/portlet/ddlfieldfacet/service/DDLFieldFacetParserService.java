package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.model.FilterField;
import com.placecube.digitalplace.search.shared.model.FacetResult;

@Component(immediate = true, service = DDLFieldFacetParserService.class)
public class DDLFieldFacetParserService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLFieldFacetParserService.class);

	@Reference
	private DDLFieldFacetDDMFormFieldService ddlFieldFacetDDMFormFieldService;

	@Reference
	private DDLFieldFacetRecordService ddlFieldFacetRecordService;

	public String getIndexedFieldName(DDMStructure ddmStructure, String fieldName) {
		return "ddm__keyword__" + ddmStructure.getStructureId() + "__" + fieldName + "_" + ddmStructure.getDefaultLanguageId();
	}

	public List<FacetResult> parseFacetsWithFieldOptions(Map<String, Integer> facetValues, Set<String> selectedValues, DDMFormFieldOptions ddmFormFieldOptions) {
		Set<FacetResult> results = new HashSet<>();

		for (Entry<String, Integer> facet : facetValues.entrySet()) {
			String facetValue = facet.getKey();
			results.add(FacetResult.init(facetValue, facet.getValue(), selectedValues.contains(facetValue), getLabelMapOrDefault(ddmFormFieldOptions, facetValue)));
		}
		return new ArrayList<>(results);
	}

	public List<FacetResult> parseAllFieldOptions(Map<String, Integer> facetValues, Set<String> selectedValues, DDMFormFieldOptions ddmFormFieldOptions) {
		Set<FacetResult> results = new HashSet<>();

		for (String optionValue : ddmFormFieldOptions.getOptionsValues()) {
			Integer facetValue = GetterUtil.getInteger(facetValues.get(optionValue));
			results.add(FacetResult.init(optionValue, facetValue, selectedValues.contains(optionValue), getLabelMapOrDefault(ddmFormFieldOptions, optionValue)));
		}

		return new ArrayList<>(results);
	}

	public List<FacetResult> parseAllFacetValues(Map<String, Integer> facetValues, Set<String> selectedValues, boolean isDDLRecordLink, String ddlRecordLinkFieldName, long ddmLinkStructureId,
			HttpServletRequest httpServletRequest) {
		Set<FacetResult> results = new HashSet<>();

		if (isDDLRecordLink && ddmLinkStructureId > 0) {
			try {
				List<DDLRecord> records = ddlFieldFacetRecordService.searchDDLRecords(ddmLinkStructureId, httpServletRequest);

				Locale locale = httpServletRequest.getLocale();

				for (DDLRecord ddlRecord : records) {

					String ddlRecordId = String.valueOf(ddlRecord.getRecordId());

					Integer facetValue = facetValues.get(ddlRecordId);

					String fieldValue = ddlFieldFacetDDMFormFieldService.getDDMFormFieldStringValue(ddlRecord, ddlRecordLinkFieldName, locale);

					Map<Locale, String> facetLabelsMap = getSingleValuedLocalizedMap(locale, fieldValue);

					results.add(FacetResult.init(ddlRecordId, Objects.nonNull(facetValue) ? facetValue : 0, selectedValues.contains(ddlRecordId), facetLabelsMap));
				}

			} catch (Exception e) {
				LOG.error("Error retrieving all facet values: " + e.toString());
				LOG.debug(e);
			}
		}

		return new ArrayList<>(results);
	}

	public List<FacetResult> parseFacetsWithFacetValues(Map<String, Integer> facetValues, Set<String> selectedValues, boolean isDDLRecordLink, String ddlRecordLinkFieldName, Locale locale) {
		Set<FacetResult> results = new HashSet<>();

		for (Entry<String, Integer> facet : facetValues.entrySet()) {
			String facetValue = facet.getKey();

			Map<Locale, String> facetLabelsMap = null;

			if (isDDLRecordLink) {
				String mappedDDLRecordFieldValue = ddlFieldFacetRecordService.getDDLRecordFieldValue(Long.valueOf(facetValue), ddlRecordLinkFieldName, locale);
				facetLabelsMap = getSingleValuedLocalizedMap(locale, mappedDDLRecordFieldValue);
			} else {
				facetLabelsMap = getSingleValuedLocalizedMap(locale, facetValue);
			}

			results.add(FacetResult.init(facetValue, facet.getValue(), selectedValues.contains(facetValue), facetLabelsMap));
		}
		return new ArrayList<>(results);
	}

	public Set<FilterField> parseToFilterFields(List<DDMFormField> ddmFormFields) {
		Set<FilterField> selectableFields = new HashSet<>();
		for (DDMFormField ddmFormField : ddmFormFields) {
			if (isValidField(ddmFormField)) {
				selectableFields.add(FilterField.init(ddmFormField));
			}
		}
		return selectableFields;
	}

	private Map<Locale, String> getLabelMapOrDefault(DDMFormFieldOptions ddmFormFieldOptions, String facetValue) {
		LocalizedValue optionLabels = ddmFormFieldOptions.getOptionLabels(facetValue);
		if (optionLabels == null) {
			return getSingleValuedLocalizedMap(Locale.UK, facetValue);
		}
		return optionLabels.getValues();
	}

	private boolean isValidField(DDMFormField ddmFormField) {
		return !ddmFormField.isRepeatable();
	}

	private Map<Locale, String> getSingleValuedLocalizedMap(Locale locale, String value) {
		Map<Locale, String> map = new HashMap<>();
		map.put(locale, value);
		return map;
	}

}
