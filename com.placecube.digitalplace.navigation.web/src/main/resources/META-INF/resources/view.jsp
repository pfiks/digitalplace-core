<%@ include file="init.jsp"%>

<c:choose>
	<c:when test="${not empty siteNavigationMenu && not empty widgetTemplate}">
		<liferay-site-navigation:navigation-menu
			ddmTemplateGroupId="${widgetTemplate.getGroupId()}"
			ddmTemplateKey="${widgetTemplate.getTemplateKey()}"
			displayDepth="1"
			expandedLevels="auto"
			preview="false"
			rootItemId="0"
			rootItemLevel="0"
			rootItemType="absolute"
			siteNavigationMenuId="${siteNavigationMenu.getSiteNavigationMenuId()}"
		/>
	</c:when>
	<c:otherwise>
		<div class="alert alert-info text-center">
			<div>
				<liferay-ui:message key="portlet-preferences-are-not-configured-properly" />
			</div>
		
			<div>
				<aui:a href="javascript:;" onClick="${ portletDisplay.getURLConfigurationJS() }">
					<liferay-ui:message key="configure-this-application" />
				</aui:a>
			</div>
		</div>
	</c:otherwise>
</c:choose>