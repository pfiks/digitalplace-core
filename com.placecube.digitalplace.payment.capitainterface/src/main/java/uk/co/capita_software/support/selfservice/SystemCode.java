/**
 * SystemCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uk.co.capita_software.support.selfservice;

public class SystemCode implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SystemCode(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _ACP = "ACP";
    public static final java.lang.String _ACR = "ACR";
    public static final java.lang.String _AIM = "AIM";
    public static final java.lang.String _AIP = "AIP";
    public static final java.lang.String _API = "API";
    public static final java.lang.String _APN = "APN";
    public static final java.lang.String _APP = "APP";
    public static final java.lang.String _AOS = "AOS";
    public static final java.lang.String _ASR = "ASR";
    public static final java.lang.String _ATP = "ATP";
    public static final java.lang.String _ATT = "ATT";
    public static final java.lang.String _DIR = "DIR";
    public static final java.lang.String _EXT = "EXT";
    public static final java.lang.String _HPAY = "HPAY";
    public static final java.lang.String _MAPN = "MAPN";
    public static final java.lang.String _MHPAY = "MHPAY";
    public static final java.lang.String _MOTO = "MOTO";
    public static final java.lang.String _PCR = "PCR";
    public static final java.lang.String _SBS = "SBS";
    public static final java.lang.String _SCM = "SCM";
    public static final java.lang.String _SCP = "SCP";
    public static final java.lang.String _SGW = "SGW";
    public static final java.lang.String _SMS = "SMS";
    public static final java.lang.String _THY = "THY";
    public static final java.lang.String _TVWAP = "TVWAP";
    public static final SystemCode ACP = new SystemCode(_ACP);
    public static final SystemCode ACR = new SystemCode(_ACR);
    public static final SystemCode AIM = new SystemCode(_AIM);
    public static final SystemCode AIP = new SystemCode(_AIP);
    public static final SystemCode API = new SystemCode(_API);
    public static final SystemCode APN = new SystemCode(_APN);
    public static final SystemCode APP = new SystemCode(_APP);
    public static final SystemCode AOS = new SystemCode(_AOS);
    public static final SystemCode ASR = new SystemCode(_ASR);
    public static final SystemCode ATP = new SystemCode(_ATP);
    public static final SystemCode ATT = new SystemCode(_ATT);
    public static final SystemCode DIR = new SystemCode(_DIR);
    public static final SystemCode EXT = new SystemCode(_EXT);
    public static final SystemCode HPAY = new SystemCode(_HPAY);
    public static final SystemCode MAPN = new SystemCode(_MAPN);
    public static final SystemCode MHPAY = new SystemCode(_MHPAY);
    public static final SystemCode MOTO = new SystemCode(_MOTO);
    public static final SystemCode PCR = new SystemCode(_PCR);
    public static final SystemCode SBS = new SystemCode(_SBS);
    public static final SystemCode SCM = new SystemCode(_SCM);
    public static final SystemCode SCP = new SystemCode(_SCP);
    public static final SystemCode SGW = new SystemCode(_SGW);
    public static final SystemCode SMS = new SystemCode(_SMS);
    public static final SystemCode THY = new SystemCode(_THY);
    public static final SystemCode TVWAP = new SystemCode(_TVWAP);
    public java.lang.String getValue() { return _value_;}
    public static SystemCode fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SystemCode enumeration = (SystemCode)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SystemCode fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SystemCode.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "systemCode"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
