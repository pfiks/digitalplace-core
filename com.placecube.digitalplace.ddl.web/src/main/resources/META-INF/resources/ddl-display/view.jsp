<%@ include file="init.jsp"%>

<c:choose>

	<c:when test="${invalidConfiguration}">
		<%@ include file="../invalid-configuration.jspf" %>
	</c:when>

	<c:otherwise>
		<div class="row ddl-add-entry-button add-entry-button">
			<div class="col-md-12">
				<portlet:renderURL var="addDDLEntryURL" windowState="${windowStateMaximized}">
					<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE_ENTRY %>" />
					<portlet:param name="ddmStructureId" value="${ddmStructure.getStructureId()}"/>
					<portlet:param name="ddlRecordSetId" value="${ddlRecordSetId}"/>
				</portlet:renderURL>

				<c:choose>
					<c:when test="${hasAddPermission}">
						<a href="${addDDLEntryURL}" class="btn btn-primary">
							<span>
								<liferay-ui:message key="add-x" arguments="${ddmStructure.getName(locale)}" />
							</span>
						</a>
					</c:when>

					<c:when test="${not themeDisplay.isSignedIn() && showLoginOption}">

						<c:url value="${themeDisplay.getURLSignIn()}" var="signInAndCreateEntryFormURL">
							<c:param name="redirect" value="${addDDLEntryURL}" />
						</c:url>

						<div class="alert alert-warning">
							<liferay-ui:message key="sign-in-to-add-content" arguments="${signInAndCreateEntryFormURL}"/>
						</div>
					</c:when>

				</c:choose>
			</div>
		</div>

	</c:otherwise>

</c:choose>
