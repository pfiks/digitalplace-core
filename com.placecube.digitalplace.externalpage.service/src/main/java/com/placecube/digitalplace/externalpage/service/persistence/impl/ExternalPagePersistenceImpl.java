/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.externalpage.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.externalpage.exception.NoSuchExternalPageException;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.model.ExternalPageTable;
import com.placecube.digitalplace.externalpage.model.impl.ExternalPageImpl;
import com.placecube.digitalplace.externalpage.model.impl.ExternalPageModelImpl;
import com.placecube.digitalplace.externalpage.service.persistence.ExternalPagePersistence;
import com.placecube.digitalplace.externalpage.service.persistence.ExternalPageUtil;
import com.placecube.digitalplace.externalpage.service.persistence.impl.constants.Placecube_ExternalPagePersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the external page service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = ExternalPagePersistence.class)
public class ExternalPagePersistenceImpl
	extends BasePersistenceImpl<ExternalPage>
	implements ExternalPagePersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>ExternalPageUtil</code> to access the external page persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		ExternalPageImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the external pages where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching external pages
	 */
	@Override
	public List<ExternalPage> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the external pages where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<ExternalPage> list = null;

		if (useFinderCache) {
			list = (List<ExternalPage>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ExternalPage externalPage : list) {
					if (!uuid.equals(externalPage.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_EXTERNALPAGE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ExternalPageModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<ExternalPage>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	@Override
	public ExternalPage findByUuid_First(
			String uuid, OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByUuid_First(uuid, orderByComparator);

		if (externalPage != null) {
			return externalPage;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchExternalPageException(sb.toString());
	}

	/**
	 * Returns the first external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByUuid_First(
		String uuid, OrderByComparator<ExternalPage> orderByComparator) {

		List<ExternalPage> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	@Override
	public ExternalPage findByUuid_Last(
			String uuid, OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByUuid_Last(uuid, orderByComparator);

		if (externalPage != null) {
			return externalPage;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchExternalPageException(sb.toString());
	}

	/**
	 * Returns the last external page in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByUuid_Last(
		String uuid, OrderByComparator<ExternalPage> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ExternalPage> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the external pages before and after the current external page in the ordered set where uuid = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	@Override
	public ExternalPage[] findByUuid_PrevAndNext(
			long externalPageId, String uuid,
			OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		uuid = Objects.toString(uuid, "");

		ExternalPage externalPage = findByPrimaryKey(externalPageId);

		Session session = null;

		try {
			session = openSession();

			ExternalPage[] array = new ExternalPageImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, externalPage, uuid, orderByComparator, true);

			array[1] = externalPage;

			array[2] = getByUuid_PrevAndNext(
				session, externalPage, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExternalPage getByUuid_PrevAndNext(
		Session session, ExternalPage externalPage, String uuid,
		OrderByComparator<ExternalPage> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_EXTERNALPAGE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ExternalPageModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(externalPage)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ExternalPage> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the external pages where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ExternalPage externalPage :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(externalPage);
		}
	}

	/**
	 * Returns the number of external pages where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching external pages
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_EXTERNALPAGE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"externalPage.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(externalPage.uuid IS NULL OR externalPage.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the external page where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchExternalPageException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	@Override
	public ExternalPage findByUUID_G(String uuid, long groupId)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByUUID_G(uuid, groupId);

		if (externalPage == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchExternalPageException(sb.toString());
		}

		return externalPage;
	}

	/**
	 * Returns the external page where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the external page where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof ExternalPage) {
			ExternalPage externalPage = (ExternalPage)result;

			if (!Objects.equals(uuid, externalPage.getUuid()) ||
				(groupId != externalPage.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_EXTERNALPAGE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<ExternalPage> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					ExternalPage externalPage = list.get(0);

					result = externalPage;

					cacheResult(externalPage);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ExternalPage)result;
		}
	}

	/**
	 * Removes the external page where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the external page that was removed
	 */
	@Override
	public ExternalPage removeByUUID_G(String uuid, long groupId)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = findByUUID_G(uuid, groupId);

		return remove(externalPage);
	}

	/**
	 * Returns the number of external pages where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching external pages
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EXTERNALPAGE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"externalPage.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(externalPage.uuid IS NULL OR externalPage.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"externalPage.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching external pages
	 */
	@Override
	public List<ExternalPage> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<ExternalPage> list = null;

		if (useFinderCache) {
			list = (List<ExternalPage>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ExternalPage externalPage : list) {
					if (!uuid.equals(externalPage.getUuid()) ||
						(companyId != externalPage.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_EXTERNALPAGE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ExternalPageModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<ExternalPage>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	@Override
	public ExternalPage findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (externalPage != null) {
			return externalPage;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchExternalPageException(sb.toString());
	}

	/**
	 * Returns the first external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ExternalPage> orderByComparator) {

		List<ExternalPage> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	@Override
	public ExternalPage findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (externalPage != null) {
			return externalPage;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchExternalPageException(sb.toString());
	}

	/**
	 * Returns the last external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ExternalPage> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ExternalPage> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the external pages before and after the current external page in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	@Override
	public ExternalPage[] findByUuid_C_PrevAndNext(
			long externalPageId, String uuid, long companyId,
			OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		uuid = Objects.toString(uuid, "");

		ExternalPage externalPage = findByPrimaryKey(externalPageId);

		Session session = null;

		try {
			session = openSession();

			ExternalPage[] array = new ExternalPageImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, externalPage, uuid, companyId, orderByComparator,
				true);

			array[1] = externalPage;

			array[2] = getByUuid_C_PrevAndNext(
				session, externalPage, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExternalPage getByUuid_C_PrevAndNext(
		Session session, ExternalPage externalPage, String uuid, long companyId,
		OrderByComparator<ExternalPage> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_EXTERNALPAGE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ExternalPageModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(externalPage)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ExternalPage> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the external pages where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ExternalPage externalPage :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(externalPage);
		}
	}

	/**
	 * Returns the number of external pages where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching external pages
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EXTERNALPAGE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"externalPage.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(externalPage.uuid IS NULL OR externalPage.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"externalPage.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByGroupIdUrl;
	private FinderPath _finderPathWithoutPaginationFindByGroupIdUrl;
	private FinderPath _finderPathCountByGroupIdUrl;

	/**
	 * Returns all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @return the matching external pages
	 */
	@Override
	public List<ExternalPage> findByGroupIdUrl(long groupId, String url) {
		return findByGroupIdUrl(
			groupId, url, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByGroupIdUrl(
		long groupId, String url, int start, int end) {

		return findByGroupIdUrl(groupId, url, start, end, null);
	}

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByGroupIdUrl(
		long groupId, String url, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return findByGroupIdUrl(
			groupId, url, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63; and url = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByGroupIdUrl(
		long groupId, String url, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		url = Objects.toString(url, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupIdUrl;
				finderArgs = new Object[] {groupId, url};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupIdUrl;
			finderArgs = new Object[] {
				groupId, url, start, end, orderByComparator
			};
		}

		List<ExternalPage> list = null;

		if (useFinderCache) {
			list = (List<ExternalPage>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ExternalPage externalPage : list) {
					if ((groupId != externalPage.getGroupId()) ||
						!url.equals(externalPage.getUrl())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_EXTERNALPAGE_WHERE);

			sb.append(_FINDER_COLUMN_GROUPIDURL_GROUPID_2);

			boolean bindUrl = false;

			if (url.isEmpty()) {
				sb.append(_FINDER_COLUMN_GROUPIDURL_URL_3);
			}
			else {
				bindUrl = true;

				sb.append(_FINDER_COLUMN_GROUPIDURL_URL_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ExternalPageModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				if (bindUrl) {
					queryPos.add(url);
				}

				list = (List<ExternalPage>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	@Override
	public ExternalPage findByGroupIdUrl_First(
			long groupId, String url,
			OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByGroupIdUrl_First(
			groupId, url, orderByComparator);

		if (externalPage != null) {
			return externalPage;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", url=");
		sb.append(url);

		sb.append("}");

		throw new NoSuchExternalPageException(sb.toString());
	}

	/**
	 * Returns the first external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByGroupIdUrl_First(
		long groupId, String url,
		OrderByComparator<ExternalPage> orderByComparator) {

		List<ExternalPage> list = findByGroupIdUrl(
			groupId, url, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	@Override
	public ExternalPage findByGroupIdUrl_Last(
			long groupId, String url,
			OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByGroupIdUrl_Last(
			groupId, url, orderByComparator);

		if (externalPage != null) {
			return externalPage;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", url=");
		sb.append(url);

		sb.append("}");

		throw new NoSuchExternalPageException(sb.toString());
	}

	/**
	 * Returns the last external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByGroupIdUrl_Last(
		long groupId, String url,
		OrderByComparator<ExternalPage> orderByComparator) {

		int count = countByGroupIdUrl(groupId, url);

		if (count == 0) {
			return null;
		}

		List<ExternalPage> list = findByGroupIdUrl(
			groupId, url, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the external pages before and after the current external page in the ordered set where groupId = &#63; and url = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param groupId the group ID
	 * @param url the url
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	@Override
	public ExternalPage[] findByGroupIdUrl_PrevAndNext(
			long externalPageId, long groupId, String url,
			OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		url = Objects.toString(url, "");

		ExternalPage externalPage = findByPrimaryKey(externalPageId);

		Session session = null;

		try {
			session = openSession();

			ExternalPage[] array = new ExternalPageImpl[3];

			array[0] = getByGroupIdUrl_PrevAndNext(
				session, externalPage, groupId, url, orderByComparator, true);

			array[1] = externalPage;

			array[2] = getByGroupIdUrl_PrevAndNext(
				session, externalPage, groupId, url, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExternalPage getByGroupIdUrl_PrevAndNext(
		Session session, ExternalPage externalPage, long groupId, String url,
		OrderByComparator<ExternalPage> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_EXTERNALPAGE_WHERE);

		sb.append(_FINDER_COLUMN_GROUPIDURL_GROUPID_2);

		boolean bindUrl = false;

		if (url.isEmpty()) {
			sb.append(_FINDER_COLUMN_GROUPIDURL_URL_3);
		}
		else {
			bindUrl = true;

			sb.append(_FINDER_COLUMN_GROUPIDURL_URL_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ExternalPageModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (bindUrl) {
			queryPos.add(url);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(externalPage)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ExternalPage> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the external pages where groupId = &#63; and url = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 */
	@Override
	public void removeByGroupIdUrl(long groupId, String url) {
		for (ExternalPage externalPage :
				findByGroupIdUrl(
					groupId, url, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(externalPage);
		}
	}

	/**
	 * Returns the number of external pages where groupId = &#63; and url = &#63;.
	 *
	 * @param groupId the group ID
	 * @param url the url
	 * @return the number of matching external pages
	 */
	@Override
	public int countByGroupIdUrl(long groupId, String url) {
		url = Objects.toString(url, "");

		FinderPath finderPath = _finderPathCountByGroupIdUrl;

		Object[] finderArgs = new Object[] {groupId, url};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EXTERNALPAGE_WHERE);

			sb.append(_FINDER_COLUMN_GROUPIDURL_GROUPID_2);

			boolean bindUrl = false;

			if (url.isEmpty()) {
				sb.append(_FINDER_COLUMN_GROUPIDURL_URL_3);
			}
			else {
				bindUrl = true;

				sb.append(_FINDER_COLUMN_GROUPIDURL_URL_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				if (bindUrl) {
					queryPos.add(url);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPIDURL_GROUPID_2 =
		"externalPage.groupId = ? AND ";

	private static final String _FINDER_COLUMN_GROUPIDURL_URL_2 =
		"externalPage.url = ?";

	private static final String _FINDER_COLUMN_GROUPIDURL_URL_3 =
		"(externalPage.url IS NULL OR externalPage.url = '')";

	private FinderPath _finderPathWithPaginationFindByGroupId;
	private FinderPath _finderPathWithoutPaginationFindByGroupId;
	private FinderPath _finderPathCountByGroupId;

	/**
	 * Returns all the external pages where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching external pages
	 */
	@Override
	public List<ExternalPage> findByGroupId(long groupId) {
		return findByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the external pages where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the external pages where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching external pages
	 */
	@Override
	public List<ExternalPage> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId;
				finderArgs = new Object[] {groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId;
			finderArgs = new Object[] {groupId, start, end, orderByComparator};
		}

		List<ExternalPage> list = null;

		if (useFinderCache) {
			list = (List<ExternalPage>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ExternalPage externalPage : list) {
					if (groupId != externalPage.getGroupId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_EXTERNALPAGE_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ExternalPageModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				list = (List<ExternalPage>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	@Override
	public ExternalPage findByGroupId_First(
			long groupId, OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByGroupId_First(
			groupId, orderByComparator);

		if (externalPage != null) {
			return externalPage;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchExternalPageException(sb.toString());
	}

	/**
	 * Returns the first external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByGroupId_First(
		long groupId, OrderByComparator<ExternalPage> orderByComparator) {

		List<ExternalPage> list = findByGroupId(
			groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page
	 * @throws NoSuchExternalPageException if a matching external page could not be found
	 */
	@Override
	public ExternalPage findByGroupId_Last(
			long groupId, OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByGroupId_Last(
			groupId, orderByComparator);

		if (externalPage != null) {
			return externalPage;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchExternalPageException(sb.toString());
	}

	/**
	 * Returns the last external page in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching external page, or <code>null</code> if a matching external page could not be found
	 */
	@Override
	public ExternalPage fetchByGroupId_Last(
		long groupId, OrderByComparator<ExternalPage> orderByComparator) {

		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<ExternalPage> list = findByGroupId(
			groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the external pages before and after the current external page in the ordered set where groupId = &#63;.
	 *
	 * @param externalPageId the primary key of the current external page
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	@Override
	public ExternalPage[] findByGroupId_PrevAndNext(
			long externalPageId, long groupId,
			OrderByComparator<ExternalPage> orderByComparator)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = findByPrimaryKey(externalPageId);

		Session session = null;

		try {
			session = openSession();

			ExternalPage[] array = new ExternalPageImpl[3];

			array[0] = getByGroupId_PrevAndNext(
				session, externalPage, groupId, orderByComparator, true);

			array[1] = externalPage;

			array[2] = getByGroupId_PrevAndNext(
				session, externalPage, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ExternalPage getByGroupId_PrevAndNext(
		Session session, ExternalPage externalPage, long groupId,
		OrderByComparator<ExternalPage> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_EXTERNALPAGE_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ExternalPageModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(externalPage)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ExternalPage> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the external pages where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (ExternalPage externalPage :
				findByGroupId(
					groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(externalPage);
		}
	}

	/**
	 * Returns the number of external pages where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching external pages
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = _finderPathCountByGroupId;

		Object[] finderArgs = new Object[] {groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_EXTERNALPAGE_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 =
		"externalPage.groupId = ?";

	public ExternalPagePersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(ExternalPage.class);

		setModelImplClass(ExternalPageImpl.class);
		setModelPKClass(long.class);

		setTable(ExternalPageTable.INSTANCE);
	}

	/**
	 * Caches the external page in the entity cache if it is enabled.
	 *
	 * @param externalPage the external page
	 */
	@Override
	public void cacheResult(ExternalPage externalPage) {
		entityCache.putResult(
			ExternalPageImpl.class, externalPage.getPrimaryKey(), externalPage);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {externalPage.getUuid(), externalPage.getGroupId()},
			externalPage);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the external pages in the entity cache if it is enabled.
	 *
	 * @param externalPages the external pages
	 */
	@Override
	public void cacheResult(List<ExternalPage> externalPages) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (externalPages.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (ExternalPage externalPage : externalPages) {
			if (entityCache.getResult(
					ExternalPageImpl.class, externalPage.getPrimaryKey()) ==
						null) {

				cacheResult(externalPage);
			}
		}
	}

	/**
	 * Clears the cache for all external pages.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ExternalPageImpl.class);

		finderCache.clearCache(ExternalPageImpl.class);
	}

	/**
	 * Clears the cache for the external page.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ExternalPage externalPage) {
		entityCache.removeResult(ExternalPageImpl.class, externalPage);
	}

	@Override
	public void clearCache(List<ExternalPage> externalPages) {
		for (ExternalPage externalPage : externalPages) {
			entityCache.removeResult(ExternalPageImpl.class, externalPage);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(ExternalPageImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(ExternalPageImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		ExternalPageModelImpl externalPageModelImpl) {

		Object[] args = new Object[] {
			externalPageModelImpl.getUuid(), externalPageModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, externalPageModelImpl);
	}

	/**
	 * Creates a new external page with the primary key. Does not add the external page to the database.
	 *
	 * @param externalPageId the primary key for the new external page
	 * @return the new external page
	 */
	@Override
	public ExternalPage create(long externalPageId) {
		ExternalPage externalPage = new ExternalPageImpl();

		externalPage.setNew(true);
		externalPage.setPrimaryKey(externalPageId);

		String uuid = PortalUUIDUtil.generate();

		externalPage.setUuid(uuid);

		externalPage.setCompanyId(CompanyThreadLocal.getCompanyId());

		return externalPage;
	}

	/**
	 * Removes the external page with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page that was removed
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	@Override
	public ExternalPage remove(long externalPageId)
		throws NoSuchExternalPageException {

		return remove((Serializable)externalPageId);
	}

	/**
	 * Removes the external page with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the external page
	 * @return the external page that was removed
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	@Override
	public ExternalPage remove(Serializable primaryKey)
		throws NoSuchExternalPageException {

		Session session = null;

		try {
			session = openSession();

			ExternalPage externalPage = (ExternalPage)session.get(
				ExternalPageImpl.class, primaryKey);

			if (externalPage == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchExternalPageException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(externalPage);
		}
		catch (NoSuchExternalPageException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ExternalPage removeImpl(ExternalPage externalPage) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(externalPage)) {
				externalPage = (ExternalPage)session.get(
					ExternalPageImpl.class, externalPage.getPrimaryKeyObj());
			}

			if (externalPage != null) {
				session.delete(externalPage);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (externalPage != null) {
			clearCache(externalPage);
		}

		return externalPage;
	}

	@Override
	public ExternalPage updateImpl(ExternalPage externalPage) {
		boolean isNew = externalPage.isNew();

		if (!(externalPage instanceof ExternalPageModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(externalPage.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					externalPage);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in externalPage proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom ExternalPage implementation " +
					externalPage.getClass());
		}

		ExternalPageModelImpl externalPageModelImpl =
			(ExternalPageModelImpl)externalPage;

		if (Validator.isNull(externalPage.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			externalPage.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (externalPage.getCreateDate() == null)) {
			if (serviceContext == null) {
				externalPage.setCreateDate(date);
			}
			else {
				externalPage.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!externalPageModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				externalPage.setModifiedDate(date);
			}
			else {
				externalPage.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(externalPage);
			}
			else {
				externalPage = (ExternalPage)session.merge(externalPage);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			ExternalPageImpl.class, externalPageModelImpl, false, true);

		cacheUniqueFindersCache(externalPageModelImpl);

		if (isNew) {
			externalPage.setNew(false);
		}

		externalPage.resetOriginalValues();

		return externalPage;
	}

	/**
	 * Returns the external page with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the external page
	 * @return the external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	@Override
	public ExternalPage findByPrimaryKey(Serializable primaryKey)
		throws NoSuchExternalPageException {

		ExternalPage externalPage = fetchByPrimaryKey(primaryKey);

		if (externalPage == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchExternalPageException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return externalPage;
	}

	/**
	 * Returns the external page with the primary key or throws a <code>NoSuchExternalPageException</code> if it could not be found.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page
	 * @throws NoSuchExternalPageException if a external page with the primary key could not be found
	 */
	@Override
	public ExternalPage findByPrimaryKey(long externalPageId)
		throws NoSuchExternalPageException {

		return findByPrimaryKey((Serializable)externalPageId);
	}

	/**
	 * Returns the external page with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page, or <code>null</code> if a external page with the primary key could not be found
	 */
	@Override
	public ExternalPage fetchByPrimaryKey(long externalPageId) {
		return fetchByPrimaryKey((Serializable)externalPageId);
	}

	/**
	 * Returns all the external pages.
	 *
	 * @return the external pages
	 */
	@Override
	public List<ExternalPage> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of external pages
	 */
	@Override
	public List<ExternalPage> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of external pages
	 */
	@Override
	public List<ExternalPage> findAll(
		int start, int end, OrderByComparator<ExternalPage> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of external pages
	 */
	@Override
	public List<ExternalPage> findAll(
		int start, int end, OrderByComparator<ExternalPage> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<ExternalPage> list = null;

		if (useFinderCache) {
			list = (List<ExternalPage>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_EXTERNALPAGE);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_EXTERNALPAGE;

				sql = sql.concat(ExternalPageModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<ExternalPage>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the external pages from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ExternalPage externalPage : findAll()) {
			remove(externalPage);
		}
	}

	/**
	 * Returns the number of external pages.
	 *
	 * @return the number of external pages
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_EXTERNALPAGE);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "externalPageId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_EXTERNALPAGE;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ExternalPageModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the external page persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByGroupIdUrl = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupIdUrl",
			new String[] {
				Long.class.getName(), String.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"groupId", "url"}, true);

		_finderPathWithoutPaginationFindByGroupIdUrl = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupIdUrl",
			new String[] {Long.class.getName(), String.class.getName()},
			new String[] {"groupId", "url"}, true);

		_finderPathCountByGroupIdUrl = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupIdUrl",
			new String[] {Long.class.getName(), String.class.getName()},
			new String[] {"groupId", "url"}, false);

		_finderPathWithPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"groupId"}, true);

		_finderPathWithoutPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			true);

		_finderPathCountByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			false);

		ExternalPageUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		ExternalPageUtil.setPersistence(null);

		entityCache.removeCache(ExternalPageImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_ExternalPagePersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_ExternalPagePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_ExternalPagePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_EXTERNALPAGE =
		"SELECT externalPage FROM ExternalPage externalPage";

	private static final String _SQL_SELECT_EXTERNALPAGE_WHERE =
		"SELECT externalPage FROM ExternalPage externalPage WHERE ";

	private static final String _SQL_COUNT_EXTERNALPAGE =
		"SELECT COUNT(externalPage) FROM ExternalPage externalPage";

	private static final String _SQL_COUNT_EXTERNALPAGE_WHERE =
		"SELECT COUNT(externalPage) FROM ExternalPage externalPage WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "externalPage.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No ExternalPage exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No ExternalPage exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		ExternalPagePersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}