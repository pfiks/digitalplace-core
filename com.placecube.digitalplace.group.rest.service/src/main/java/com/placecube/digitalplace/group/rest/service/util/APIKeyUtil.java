package com.placecube.digitalplace.group.rest.service.util;

import java.nio.charset.Charset;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.group.rest.exception.GroupRestKeyException;

@Component(immediate = true, service = APIKeyUtil.class)
public class APIKeyUtil {

	static final String ALGORITHM = "HmacSHA1";
	static final String CHARSET_NAME = "UTF-8";

	public SecretKey generateSecretKey(KeyGenerator keyGenerator) {
		return keyGenerator.generateKey();
	}

	public byte[] getBytes(String textToEncode) {
		return textToEncode.getBytes(Charset.forName(CHARSET_NAME));
	}

	public String getEncodedSecretKey(SecretKey secretKey) {
		return Base64.getEncoder().encodeToString(secretKey.getEncoded());
	}

	public String getEncodedText(Mac mac, byte[] bytes) {
		return Base64.getEncoder().encodeToString(mac.doFinal(bytes));
	}

	public Key getKey(String key) {
		final byte[] decode = Base64.getDecoder().decode(key);
		return new SecretKeySpec(decode, ALGORITHM);
	}

	public KeyGenerator getKeyGenerator() throws NoSuchAlgorithmException {
		KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
		keyGenerator.init(1024);
		return keyGenerator;
	}

	public Mac getMacForKey(Key key) throws GroupRestKeyException {
		try {
			final Mac mac = Mac.getInstance(ALGORITHM);
			mac.init(key);
			return mac;
		} catch (Exception e) {
			throw new GroupRestKeyException(e);
		}
	}

}
