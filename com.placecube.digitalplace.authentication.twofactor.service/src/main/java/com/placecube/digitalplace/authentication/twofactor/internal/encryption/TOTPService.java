package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = TOTPService.class)
public class TOTPService {

	private static final Log LOG = LogFactoryUtil.getLog(TOTPService.class);

	@Reference
	private TOTPCodeService totpCodeService;

	public Optional<String> getOTPUrl(String qrBarCodeUrl, String issuer, String emailAddress, String secretKey, String algorithm, int authenticatorCodeLength, int authenticatorCodeDurationInSecs) {
		if (Validator.isBlank(qrBarCodeUrl) || Validator.isBlank(issuer) || Validator.isBlank(emailAddress) || Validator.isBlank(secretKey)) {
			throw new IllegalArgumentException("Barcode url, Issuer, emailaddress and secretKey  cannot be null or empty");
		}
		String otpUrl = totpCodeService.buildOTPUrl(issuer, emailAddress, secretKey, algorithm, authenticatorCodeLength, authenticatorCodeDurationInSecs);
		Optional<String> encodedOtpUrl = encode(otpUrl, "UTF-8");
		return encodedOtpUrl.map(s -> format(qrBarCodeUrl, s));
	}

	public boolean verifyUserOneTimeCode(String secretKey, String oneTimeCode) {
		String oneTimeCodeNoSpace = oneTimeCode.replaceAll("\\s", "");
		Optional<String> generateTopCode = totpCodeService.generateTopCode(secretKey);
		return generateTopCode.map(oneTimeCodeNoSpace::equalsIgnoreCase).orElse(Boolean.FALSE);
	}

	private Optional<String> encode(String str, String encoding) {
		try {
			return Optional.of(URLEncoder.encode(str, encoding));
		} catch (UnsupportedEncodingException e) {
			LOG.error(encoding + " encoding is not supported.", e);
		}
		return Optional.empty();
	}

	private String format(String format, String str) {
		return String.format(format, str);
	}

}
