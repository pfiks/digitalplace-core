package com.placecube.digitalplace.group.rest.application.exceptions.handlers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;

public class GroupRestSecurityExceptionHandlerTest extends PowerMockito {

	@InjectMocks
	private GroupRestSecurityExceptionHandler groupRestSecurityExceptionHandler;

	@Mock
	private GroupRestSecurityException mockGroupRestSecurityException;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void toResponse_WhenNoError_ThenReturnsAnewResponseWithValuesFromTheException() {
		String messageKey = "messageKey";
		int status = Response.Status.CONFLICT.getStatusCode();
		Response exceptionResponse = Response.status(status).build();

		when(mockGroupRestSecurityException.getResponse()).thenReturn(exceptionResponse);
		when(mockGroupRestSecurityException.getMessage()).thenReturn(messageKey);

		Response result = groupRestSecurityExceptionHandler.toResponse(mockGroupRestSecurityException);

		assertThat(result.getStatus(), equalTo(status));
		assertThat(result.getEntity().toString(), equalTo(messageKey));
	}

}
