package com.placecube.digitalplace.userimport.admin.web.constants;

public final class PortletKeys {

	public static final String USER_IMPORT = "com_placecube_digitalplace_userimport_UserImportPortlet";

	private PortletKeys() {
	}

}
