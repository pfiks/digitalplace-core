package com.placecube.digitalplace.user.settings.web.displaycontext;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.taglib.servlet.PipingServletResponseFactory;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.model.UserSettingsSection;

public class UserSettingsDisplayContext {

	public static UserSettingsDisplayContext initDisplayContext(Map<DisplaySection, List<UserSettingsSection>> userSettingsSections) {
		return new UserSettingsDisplayContext(userSettingsSections);
	}

	private final List<UserSettingsSection> allSections;
	private final Map<DisplaySection, List<UserSettingsSection>> userSettingsSections;

	private UserSettingsDisplayContext(Map<DisplaySection, List<UserSettingsSection>> userSettingsSections) {
		this.userSettingsSections = userSettingsSections;
		allSections = userSettingsSections.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
	}

	public HttpServletResponse createPipingServletResponse(PageContext pageContext) {
		return PipingServletResponseFactory.createPipingServletResponse(pageContext);
	}

	public UserSettingsSection getSelectedSection(HttpServletRequest request) {
		String selectedSectionId = ParamUtil.getString(request, PortletRequestKeys.CURRENT_SECTION_ID, StringPool.BLANK);
		Optional<UserSettingsSection> selectedEntry = allSections.stream().filter(entry -> selectedSectionId.equals(entry.getId())).findFirst();
		return selectedEntry.isPresent() ? selectedEntry.get() : allSections.get(0);
	}

	public Map<DisplaySection, List<UserSettingsSection>> getUserSettingsSections() {
		return userSettingsSections;
	}

	public PortletURL getViewURL(PortletURL portletURL, LiferayPortletResponse liferayPortletResponse, String sectionId) throws PortletException {
		PortletURL cloneURL = PortletURLUtil.clone(portletURL, liferayPortletResponse);
		cloneURL.getRenderParameters().setValue(PortletRequestKeys.CURRENT_SECTION_ID, sectionId);
		return cloneURL;
	}

}
