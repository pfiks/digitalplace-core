<%@ include file="init.jsp" %>

<div class="postcode-lookup">
	<div class="row postcode-input no-gutters" id="<portlet:namespace/>postcodeInputContainer">
		<div class="col-md-4" id="<portlet:namespace/>postcodeFieldContainer">
			<aui:input name="postcode" label="postcode" required="false" value="${postcode}" cssClass="text-uppercase ${empty postcode ? '' : 'hide'}" />
			<span id="<portlet:namespace/>postcodeLabel" class="text-uppercase postcode-label ${empty postcode ? 'hide' : ''}">${postcode}</span>
		</div>
	</div>
	<div class="row form-group no-gutters">
		<div class="col-md-12">
			<aui:button value="find-address" id="btnAddressLookup" cssClass="btn btn-primary btn-address-lookup ${empty postcode ? '' : 'hide'}" />
			<aui:a href="#" id="addressLookupReset" label="change-postcode" cssClass="${empty postcode ? 'hide' : ''}" />
		</div>
	</div>
	<div class="row ${empty fullAddress ? 'hide' : '' } no-gutters" id="<portlet:namespace/>addressFormContainer">
		<div class="col-md-12">
			<aui:select name="uprn" label="select-your-address" showEmptyOption="false" required="${required}">
				<option selected value="${ uprn }" data-addressLine1="${addressLine1}" data-addressLine2="${addressLine2}" data-addressLine3="${addressLine3}" data-addressLine4="${addressLine4}"
					data-city="${city}">${ fullAddress }</option>
			</aui:select>
		</div>

		<aui:input type="hidden" name="fullAddress" value="${ fullAddress }" />

		<c:if test="${ displaySelected }">
			<div class="col-md-12 ${not empty fullAddress ? '' : 'hide'}"
				id="<portlet:namespace/>addressDisplayContainer">
				<div class="callout callout-success">
					<h4>
						<liferay-ui:message key="your-selected-address" />
					</h4>
					<span id="<portlet:namespace/>fullAddressDetails">
						<c:if test="${not empty fullAddress}">
							${ fullAddress }
						</c:if>
					</span>
				</div>
			</div>
		</c:if>
	</div>
</div>

<aui:script use="com-placecube-digitalplace-address-postcodelookup">

	new A.PostcodeLookup({ns: '<portlet:namespace />', selectedCallback: '${ selectedCallback }', unselectedCallback: '${ unselectedCallback }', fallbackToNationalLookup: '${ fallbackToNationalLookup }'}).render();

</aui:script>