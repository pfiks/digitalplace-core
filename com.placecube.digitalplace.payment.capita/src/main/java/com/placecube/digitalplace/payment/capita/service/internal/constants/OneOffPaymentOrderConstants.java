package com.placecube.digitalplace.payment.capita.service.internal.constants;

public class OneOffPaymentOrderConstants {

	public static final String EVERY_HOUR_CRON_EXPRESSION = "0 0 * * * ? *";

	public static final String TEMP_FILES_FOLDER_NAME = "payment_temp_files";

}
