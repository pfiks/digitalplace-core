package com.placecube.digitalplace.payment.mock.service;

import java.util.Locale;
import java.util.Optional;

import javax.portlet.PortletMode;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.VirtualHost;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.VirtualHostLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.mock.configuration.MockCompanyConfiguration;
import com.placecube.digitalplace.payment.mock.web.constants.PortletKeys;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;
import com.placecube.digitalplace.payment.service.PaymentConnector;

@Component(immediate = true, service = PaymentConnector.class)
public class MockPaymentConnector implements PaymentConnector {

	private static final Log LOG = LogFactoryUtil.getLog(MockPaymentConnector.class);

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private Portal portal;

	@Reference
	private VirtualHostLocalService virtualHostLocalService;

	@Override
	public boolean enabled(long companyId) {
		try {
			MockCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockCompanyConfiguration.class, companyId);
			return configuration.enabled();
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Optional<String> getBackURL(String paymentReference, ServiceContext serviceContext) {
		return Optional.ofNullable(GetterUtil.getString(serviceContext.getAttribute("redirect")));
	}

	@Override
	public PaymentStatus getPaymentStatus(String paymentReference, ServiceContext serviceContext) {

		boolean paymentSuccess = true;
		for (String parameter : serviceContext.getRequest().getQueryString().split(StringPool.AMPERSAND)) {
			String[] paramAndVal = parameter.split(StringPool.EQUAL);
			if ("paymentSuccess".equals(paramAndVal[0])) {
				paymentSuccess = Boolean.parseBoolean(paramAndVal[1]);
			}
		}

		if (paymentSuccess) {
			PaymentStatus result = PaymentStatus.success();
			result.setPaymentUniqueReferenceNumber("UniquePRN-" + paymentReference);
			return result;
		}
		return PaymentStatus.paymentError();

	}

	@Override
	public PaymentResponse preparePayment(PaymentContext paymentContext, ServiceContext serviceContext) {
		if ("failPreparePaymentAccountId".equals(paymentContext.getAccountId())) {
			return new PaymentResponse(paymentContext.getReturnURL(), PaymentStatus.appError());
		}

		try {
			String paymentUrl = getPaymentUrl(paymentContext, serviceContext);
			return new PaymentResponse(paymentUrl, PaymentStatus.success());
		} catch (Exception e) {
			LOG.error("Error creating payment response.", e);
			return new PaymentResponse(paymentContext.getReturnURL(), PaymentStatus.appError());
		}
	}

	@Override
	public String getAgentReferredPaymentPayload(long companyId, AgentReferredPaymentContext agentReferredPaymentContext) throws PortalException {
		JSONObject fullResponse = jsonFactory.createJSONObject();
		fullResponse.put("endpoint", "/o/mock-arp-payment/process-payment");

		JSONObject payload = jsonFactory.createJSONObject();
		payload.put("returnUrl", agentReferredPaymentContext.getReturnUrl());
		payload.put("logUrl", agentReferredPaymentContext.getLogUrl());

		fullResponse.put("requestPayload", payload.toJSONString());

		return fullResponse.toJSONString();
	}

	@Override
	public PaymentResponse processAgentReferredPaymentResponse(String externalReferenceCode, ServiceContext serviceContext) {
		PaymentStatus successStatus = PaymentStatus.success();
		successStatus.setPaymentUniqueReferenceNumber("12345");
		return new PaymentResponse(StringPool.BLANK, successStatus);
	}

	@SuppressWarnings("deprecation")
	private String getPaymentUrl(PaymentContext paymentContext, ServiceContext serviceContext) throws Exception {

		LiferayPortletRequest liferayPortletRequest = serviceContext.getLiferayPortletRequest();
		if (Validator.isNotNull(liferayPortletRequest)) {
			LiferayPortletURL portletURL = PortletURLFactoryUtil.create(portal.getHttpServletRequest(liferayPortletRequest), PortletKeys.MOCK_PAYMENT_PORTLET, PortletRequest.RENDER_PHASE);
			portletURL.setWindowState(LiferayWindowState.EXCLUSIVE);
			portletURL.setPortletMode(PortletMode.VIEW);
			portletURL.setParameter("amount", paymentContext.getAmount().toString());
			portletURL.setParameter("returnUrl", paymentContext.getReturnURL());

			return portletURL.toString();

		}

		return getPaymentUrlForRestCall(paymentContext, serviceContext);
	}

	private String getPaymentUrlForRestCall(PaymentContext paymentContext, ServiceContext serviceContext) throws PortalException {
		Layout layout = layoutLocalService.getLayout(serviceContext.getPlid());
		Company company = companyLocalService.getCompanyById(layout.getCompanyId());
		Group group = layout.getGroup();

		Optional<VirtualHost> virtualHostOptional = virtualHostLocalService.getVirtualHosts(//
				company.getCompanyId(), layout.getLayoutSet().getLayoutSetId()).stream().findFirst();

		String portalURL;
		if (virtualHostOptional.isPresent()) {
			int serverPort = portal.getPortalServerPort(false) > 0 ? portal.getPortalServerPort(false) : portal.getPortalServerPort(true);
			portalURL = portal.getPortalURL(virtualHostOptional.get().getHostname(), serverPort, true);
		} else {
			portalURL = portal.getPortalURL(company.getVirtualHostname(), -1, true);
		}

		String pathFriendlyURL = layout.isPrivateLayout() ? portal.getPathFriendlyURLPrivateGroup() : portal.getPathFriendlyURLPublic();
		Locale locale = LocaleUtil.fromLanguageId(layout.getDefaultLanguageId());
		String baseDashboardUrl = pathFriendlyURL + group.getFriendlyURL() + layout.getFriendlyURL(locale);

		String portletId = PortletKeys.MOCK_PAYMENT_PORTLET;

		StringBuilder sb = new StringBuilder();
		sb.append(portalURL);
		sb.append(baseDashboardUrl);
		sb.append("?p_p_id=" + portletId);
		sb.append("&p_p_lifecycle=0&p_p_state=exclusive&p_p_mode=view");
		sb.append("&_" + portletId + "_amount=" + paymentContext.getAmount().toString());
		sb.append("&_" + portletId + "_returnUrl=" + paymentContext.getReturnURL());

		return sb.toString();
	}

}
