package com.placecube.digitalplace.form.service.impl.internal;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.data.provider.DDMDataProvider;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderRegistry;
import com.liferay.dynamic.data.mapping.io.DDMFormDeserializer;
import com.liferay.dynamic.data.mapping.io.DDMFormDeserializerDeserializeRequest;
import com.liferay.dynamic.data.mapping.io.DDMFormDeserializerDeserializeResponse;
import com.liferay.dynamic.data.mapping.io.DDMFormLayoutDeserializer;
import com.liferay.dynamic.data.mapping.io.DDMFormLayoutDeserializerDeserializeRequest;
import com.liferay.dynamic.data.mapping.io.DDMFormLayoutDeserializerDeserializeResponse;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializer;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializerDeserializeRequest;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializerDeserializeResponse;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesSerializer;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesSerializerSerializeRequest;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesSerializerSerializeResponse;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormLayout;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.util.DDMFormFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.form.model.FormDefinition;

@Component(immediate = true, service = FormDefinitionDeserializerService.class)
public class FormDefinitionDeserializerService {

	private static final Log LOG = LogFactoryUtil.getLog(FormDefinitionDeserializerService.class);

	@Reference
	private DDMDataProviderRegistry ddmDataProviderRegistry;

	@Reference(target = "(ddm.form.deserializer.type=json)")
	private DDMFormDeserializer jsonDDMFormDeserializer;

	@Reference(target = "(ddm.form.layout.deserializer.type=json)")
	private DDMFormLayoutDeserializer jsonDDMFormLayoutDeserializer;

	@Reference(target = "(ddm.form.values.deserializer.type=json)")
	private DDMFormValuesDeserializer jsonDDMFormValuesDeserializer;

	@Reference(target = "(ddm.form.values.serializer.type=json)")
	private DDMFormValuesSerializer jsonDDMFormValuesSerializer;

	public Map<String, DDMFormValues> getDDMDataProviderFormValues(FormDefinition formDefinition, ServiceContext serviceContext) {

		List<URL> dataProviderDDMFormValuesURLs = formDefinition.getUrlsToDataProviderDDMFormValuesDefinitionFiles();

		Map<String, DDMFormValues> ddmDataProviderFormValues = new HashMap<>();

		dataProviderDDMFormValuesURLs.forEach(url -> {
			try {
				String content = StringUtil.read(url.openStream());
				content = content.replace("[$HOST_URL$]", serviceContext.getPortalURL());

				String dataProviderName = getDataProviderNameFromURL(url);
				ddmDataProviderFormValues.put(dataProviderName, getDDMFormValues(content, getDDMFormFromDataProviderSettings()));
			} catch (IOException ioe) {
				LOG.error("Error getting data provider DDMFormValues: " + ioe.getMessage(), ioe);
			}
		});

		return ddmDataProviderFormValues;
	}

	private String getDataProviderNameFromURL(URL url) {
		String dataProviderName = url.getPath().substring(url.getPath().lastIndexOf("/") + 1);
		dataProviderName = dataProviderName.substring(0, dataProviderName.indexOf("."));
		dataProviderName = dataProviderName.replace("-", " ");
		dataProviderName = dataProviderName.substring(0, 1).toUpperCase() + dataProviderName.substring(1);
		return dataProviderName;
	}

	public DDMForm getDDMForm(FormDefinition formDefinition, Locale locale) throws IOException {

		String jsonFormDefinition = StringUtil.read(formDefinition.getUrlToDDMFormDefinitionFile().openStream());

		return getDDMForm(jsonFormDefinition, locale);
	}

	public DDMForm getDDMForm(String jsonFormDefinition, Locale locale) {

		jsonFormDefinition = StringUtil.replace(jsonFormDefinition, "[$LOCALE_DEFAULT$]", locale.toString());

		DDMFormDeserializerDeserializeRequest.Builder builder = DDMFormDeserializerDeserializeRequest.Builder.newBuilder(jsonFormDefinition);

		DDMFormDeserializerDeserializeResponse ddmFormDeserializerDeserializeResponse = jsonDDMFormDeserializer.deserialize(builder.build());

		return ddmFormDeserializerDeserializeResponse.getDDMForm();
	}

	public String getDDMFormDefinition(FormDefinition formDefinition) throws IOException {
		return StringUtil.read(formDefinition.getUrlToDDMFormDefinitionFile().openStream());
	}

	public DDMForm getDDMFormFromDataProviderSettings() {

		DDMDataProvider ddmDataProvider = ddmDataProviderRegistry.getDDMDataProvider("rest");

		Class<?> clazz = ddmDataProvider.getSettings();

		return DDMFormFactory.create(clazz);
	}

	public DDMFormLayout getDDMFormLayout(FormDefinition formDefinition) throws IOException {

		String layoutContent = StringUtil.read(formDefinition.getUrlToDDMFormLayoutDefinitionFile().openStream());

		return getDDMFormLayout(layoutContent);
	}

	public DDMFormLayout getDDMFormLayout(String formLayoutDefinition) {

		DDMFormLayoutDeserializerDeserializeRequest.Builder builder = DDMFormLayoutDeserializerDeserializeRequest.Builder.newBuilder(formLayoutDefinition.trim());

		DDMFormLayoutDeserializerDeserializeResponse ddmFormLayoutDeserializerDeserializeResponse = jsonDDMFormLayoutDeserializer.deserialize(builder.build());

		return ddmFormLayoutDeserializerDeserializeResponse.getDDMFormLayout();
	}

	public DDMFormValues getDDMFormValues(FormDefinition formDefinition, DDMForm ddmForm, ServiceContext serviceContext) throws IOException {

		String content = StringUtil.read(formDefinition.getUrlToSettingsDDMFormValuesDefinitionFile().openStream());
		content = content.replace("[$HOST_URL$]", serviceContext.getPortalURL());

		return getDDMFormValues(content, ddmForm);

	}

	public DDMFormValues getDDMFormValues(String formValuesDefinition, DDMForm ddmForm) {

		DDMFormValuesDeserializerDeserializeRequest.Builder builder = DDMFormValuesDeserializerDeserializeRequest.Builder.newBuilder(formValuesDefinition, ddmForm);

		DDMFormValuesDeserializerDeserializeResponse ddmFormValuesDeserializerDeserializeResponse = jsonDDMFormValuesDeserializer.deserialize(builder.build());

		return ddmFormValuesDeserializerDeserializeResponse.getDDMFormValues();
	}

	public String serialize(DDMFormValues ddmFormValues) {
		DDMFormValuesSerializerSerializeRequest.Builder builder = DDMFormValuesSerializerSerializeRequest.Builder.newBuilder(ddmFormValues);

		DDMFormValuesSerializerSerializeResponse ddmFormValuesSerializerSerializeResponse = jsonDDMFormValuesSerializer.serialize(builder.build());

		return ddmFormValuesSerializerSerializeResponse.getContent();
	}
}
