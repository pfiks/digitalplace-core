package com.placecube.digitalplace.navigation.service.util;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;

@Component(immediate = true, service = BreadcrumbEntryFactory.class)
public class BreadcrumbEntryFactory {

	public BreadcrumbEntry createBreadcrumbEntry(String title, String url) {
		BreadcrumbEntry breadcrumbEntry = new BreadcrumbEntry();

		breadcrumbEntry.setTitle(title);
		breadcrumbEntry.setURL(url);

		return breadcrumbEntry;
	}
}
