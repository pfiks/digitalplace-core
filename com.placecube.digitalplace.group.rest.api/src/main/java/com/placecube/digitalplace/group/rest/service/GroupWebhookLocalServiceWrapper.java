/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link GroupWebhookLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see GroupWebhookLocalService
 * @generated
 */
public class GroupWebhookLocalServiceWrapper
	implements GroupWebhookLocalService,
			   ServiceWrapper<GroupWebhookLocalService> {

	public GroupWebhookLocalServiceWrapper() {
		this(null);
	}

	public GroupWebhookLocalServiceWrapper(
		GroupWebhookLocalService groupWebhookLocalService) {

		_groupWebhookLocalService = groupWebhookLocalService;
	}

	/**
	 * Adds the group webhook to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupWebhookLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupWebhook the group webhook
	 * @return the group webhook that was added
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupWebhook
		addGroupWebhook(
			com.placecube.digitalplace.group.rest.model.GroupWebhook
				groupWebhook) {

		return _groupWebhookLocalService.addGroupWebhook(groupWebhook);
	}

	/**
	 * Creates a new group webhook with the primary key. Does not add the group webhook to the database.
	 *
	 * @param webhookId the primary key for the new group webhook
	 * @return the new group webhook
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupWebhook
		createGroupWebhook(long webhookId) {

		return _groupWebhookLocalService.createGroupWebhook(webhookId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupWebhookLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the group webhook from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupWebhookLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupWebhook the group webhook
	 * @return the group webhook that was removed
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupWebhook
		deleteGroupWebhook(
			com.placecube.digitalplace.group.rest.model.GroupWebhook
				groupWebhook) {

		return _groupWebhookLocalService.deleteGroupWebhook(groupWebhook);
	}

	/**
	 * Deletes the group webhook with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupWebhookLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook that was removed
	 * @throws PortalException if a group webhook with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupWebhook
			deleteGroupWebhook(long webhookId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupWebhookLocalService.deleteGroupWebhook(webhookId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupWebhookLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _groupWebhookLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _groupWebhookLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _groupWebhookLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _groupWebhookLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _groupWebhookLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _groupWebhookLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _groupWebhookLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _groupWebhookLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.group.rest.model.GroupWebhook
		fetchGroupWebhook(long webhookId) {

		return _groupWebhookLocalService.fetchGroupWebhook(webhookId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _groupWebhookLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns a list of all the enabled webhooks configured for the specified
	 * className, classPK and event
	 *
	 * @param groupId the groupId
	 * @param className the className to match
	 * @param classPK the classPK to match
	 * @param event the event
	 * @return list of enabled webhooks
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.group.rest.model.GroupWebhook>
			getConfiguredGroupWebhooks(
				long groupId, String className, long classPK, String event) {

		return _groupWebhookLocalService.getConfiguredGroupWebhooks(
			groupId, className, classPK, event);
	}

	/**
	 * Returns the group webhook with the primary key.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook
	 * @throws PortalException if a group webhook with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupWebhook
			getGroupWebhook(long webhookId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupWebhookLocalService.getGroupWebhook(webhookId);
	}

	/**
	 * Returns a range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of group webhooks
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.group.rest.model.GroupWebhook>
			getGroupWebhooks(int start, int end) {

		return _groupWebhookLocalService.getGroupWebhooks(start, end);
	}

	/**
	 * Returns all the webhooks configured for the group
	 *
	 * @param groupId the groupId
	 * @param start pagination start
	 * @param end pagination end
	 * @return list of webhooks
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.group.rest.model.GroupWebhook>
			getGroupWebhooks(long groupId, int start, int end) {

		return _groupWebhookLocalService.getGroupWebhooks(groupId, start, end);
	}

	/**
	 * Returns the number of group webhooks.
	 *
	 * @return the number of group webhooks
	 */
	@Override
	public int getGroupWebhooksCount() {
		return _groupWebhookLocalService.getGroupWebhooksCount();
	}

	/**
	 * Returns the total number of webhooks for the group
	 *
	 * @param groupId the groupId
	 * @return total count
	 */
	@Override
	public int getGroupWebhooksCount(long groupId) {
		return _groupWebhookLocalService.getGroupWebhooksCount(groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _groupWebhookLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * If a webhook exists for the specified id, then it returns the webhook. If
	 * no webhook is found, then returns a newly created webhoo
	 *
	 * @param webhookId the id to match
	 * @return webhook
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupWebhook
		getOrCreateGroupWebhook(long webhookId) {

		return _groupWebhookLocalService.getOrCreateGroupWebhook(webhookId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _groupWebhookLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupWebhookLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the group webhook in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupWebhookLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupWebhook the group webhook
	 * @return the group webhook that was updated
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupWebhook
		updateGroupWebhook(
			com.placecube.digitalplace.group.rest.model.GroupWebhook
				groupWebhook) {

		return _groupWebhookLocalService.updateGroupWebhook(groupWebhook);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _groupWebhookLocalService.getBasePersistence();
	}

	@Override
	public GroupWebhookLocalService getWrappedService() {
		return _groupWebhookLocalService;
	}

	@Override
	public void setWrappedService(
		GroupWebhookLocalService groupWebhookLocalService) {

		_groupWebhookLocalService = groupWebhookLocalService;
	}

	private GroupWebhookLocalService _groupWebhookLocalService;

}