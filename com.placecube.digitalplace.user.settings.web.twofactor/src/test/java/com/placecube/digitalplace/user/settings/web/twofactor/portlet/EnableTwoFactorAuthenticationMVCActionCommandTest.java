package com.placecube.digitalplace.user.settings.web.twofactor.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionMessages.class })
public class EnableTwoFactorAuthenticationMVCActionCommandTest {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MutableRenderParameters mockRenderParameters;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	@InjectMocks
	private EnableTwoFactorAuthenticationMVCActionCommand enableTwoFactorAuthenticationMVCAction;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionMessages.class);
	}

	@Test
	public void doProcessAction_WhenTwoFactorAuthenticationIsSetToEnable_ThenSetsTheAttribute() {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getBoolean(mockActionRequest, TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM)).thenReturn(true);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		enableTwoFactorAuthenticationMVCAction.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, true);
		verify(mockRenderParameters, times(1)).setValue(PortletRequestKeys.CURRENT_SECTION_ID, TwoFactorAuthenticationKeys.BUNDLE_ID);
	}

	@Test
	public void doProcessAction_WhenTwoFactorAuthenticationIsNotSetToEnable_ThenSetsTheAttributeAndDisables2FAForUser() {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(ParamUtil.getBoolean(mockActionRequest, TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM)).thenReturn(false);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		enableTwoFactorAuthenticationMVCAction.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, false);
		verify(mockUserTwoFactorAuthenticationLocalService, times(1)).disableTwoFactorAuthenticationForUser(mockUser);
		verify(mockRenderParameters, times(1)).setValue(PortletRequestKeys.CURRENT_SECTION_ID, TwoFactorAuthenticationKeys.BUNDLE_ID);
	}

}
