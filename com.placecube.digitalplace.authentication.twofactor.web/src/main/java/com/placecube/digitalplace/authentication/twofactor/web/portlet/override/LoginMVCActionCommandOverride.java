package com.placecube.digitalplace.authentication.twofactor.web.portlet.override;

import java.security.Key;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.ActionURL;
import javax.portlet.PortletException;
import javax.portlet.RenderURL;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.encryptor.Encryptor;
import com.liferay.portal.kernel.encryptor.EncryptorException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.DigesterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.security.auth.session.AuthenticatedSessionManagerUtil;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.model.TwoFactorAuthenticationMethod;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorSessionKeys;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorUrlService;

@Component(property = { "javax.portlet.name=" + PortletKeys.FAST_LOGIN, "javax.portlet.name=" + PortletKeys.LOGIN, "mvc.command.name=/login/login",
		"service.ranking:Integer=2" }, service = MVCActionCommand.class)
public class LoginMVCActionCommandOverride extends BaseMVCActionCommand {

	@Reference
	private Encryptor encryptor;

	@Reference(target = "(component.name=com.liferay.multi.factor.authentication.web.internal.portlet.action.LoginMVCActionCommand)")
	private MVCActionCommand originalLoginMVCActionCommand;

	@Reference
	private Portal portal;

	@Reference
	private TwoFactorRequestUtils twoFactorRequestUtils;

	@Reference
	private TwoFactorService twoFactorService;

	@Reference
	private TwoFactorUrlService twoFactorUrlService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		Optional<AuthenticationTwoFactorCompanyConfiguration> enabledConfig = twoFactorService.getEnabledConfiguration(portal.getCompanyId(actionRequest));

		if (!enabledConfig.isPresent()) {

			originalLoginMVCActionCommand.processAction(actionRequest, actionResponse);

		} else {

			HttpServletRequest httpServletRequest = twoFactorRequestUtils.getOriginalServletRequest(actionRequest);

			actionRequest = twoFactorRequestUtils.getActionRequest(httpServletRequest, actionRequest);

			String login = ParamUtil.getString(actionRequest, "login");
			String password = ParamUtil.getString(actionRequest, "password");

			if (Validator.isNotNull(login) && Validator.isNotNull(password)) {
				processValidLogin(actionRequest, actionResponse, httpServletRequest, login, password, enabledConfig.get());
			}
		}
	}

	private void cleanUp2FASession(HttpSession httpSession) {
		httpSession.removeAttribute(TwoFactorSessionKeys.TWO_FACTOR_USER_ID);
		httpSession.removeAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_DIGEST);
		httpSession.removeAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_KEY);
		httpSession.removeAttribute(TwoFactorSessionKeys.TWO_FACTOR_REDIRECT);

	}

	private String getRedirectValue(ActionRequest actionRequest, ActionResponse actionResponse, long userId, HttpServletRequest httpServletRequest, String encryptedStateMapJSON,
			AuthenticationTwoFactorCompanyConfiguration authenticationTwoFactorCompanyConfiguration) throws WindowStateException {
		twoFactorService.getEnabledConfiguration(portal.getCompanyId(actionRequest));

		LiferayPortletResponse liferayPortletResponse = portal.getLiferayPortletResponse(actionResponse);

		ActionURL actionURL = twoFactorUrlService.getLoginActionURL(liferayPortletResponse, encryptedStateMapJSON);

		RenderURL returnToFullPageRenderURL = twoFactorUrlService.getReturnToFullPageURL(liferayPortletResponse, ParamUtil.getString(actionRequest, "redirect"));

		String twoFactorMVCCommandName = null;
		if (authenticationTwoFactorCompanyConfiguration.authenticationMethod().equals(TwoFactorAuthenticationMethod.EMAIL)) {
			twoFactorMVCCommandName = TwoFactorMVCCommandKeys.VERIFY_BY_EMAIL;
		} else {
			twoFactorMVCCommandName = twoFactorRequestUtils.getTwoFactorMVCCommandName(twoFactorService.getUserTwoFactorAuthentication(userId));
		}
		long plid = twoFactorRequestUtils.getPlid(httpServletRequest);

		return twoFactorUrlService.getRedirectURL(twoFactorMVCCommandName, plid, actionRequest, actionURL, httpServletRequest, returnToFullPageRenderURL);
	}

	private void processValidLogin(ActionRequest actionRequest, ActionResponse actionResponse, HttpServletRequest httpServletRequest, String login, String password,
			AuthenticationTwoFactorCompanyConfiguration authenticationTwoFactorCompanyConfiguration) throws PortalException, PortletException, EncryptorException {

		long userId = AuthenticatedSessionManagerUtil.getAuthenticatedUserId(httpServletRequest, login, password, null);
		User user = userLocalService.fetchUser(userId);

		if (userId > 0) {
			if (twoFactorService.isTwoFactorAlreadyVerified(httpServletRequest, user, userId)) {
				HttpSession httpSession = httpServletRequest.getSession();
				String redirectURL = (String) httpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_REDIRECT);
				actionRequest.setAttribute(WebKeys.REDIRECT, redirectURL);
				cleanUp2FASession(httpSession);
				originalLoginMVCActionCommand.processAction(actionRequest, actionResponse);
			} else {
				if (twoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(user, userId)) {
					redirectToVerifyTwoFactor(actionRequest, actionResponse, userId, httpServletRequest, authenticationTwoFactorCompanyConfiguration);
				} else {
					originalLoginMVCActionCommand.processAction(actionRequest, actionResponse);
				}
			}
		}
	}

	private void redirectToVerifyTwoFactor(ActionRequest actionRequest, ActionResponse actionResponse, long userId, HttpServletRequest httpServletRequest,
			AuthenticationTwoFactorCompanyConfiguration authenticationTwoFactorCompanyConfiguration) throws EncryptorException, WindowStateException {

		Key key = encryptor.generateKey();

		String encryptedStateMapJSON = twoFactorRequestUtils.getEncryptedStateMapJSON(actionRequest, key);

		String redirectURL = getRedirectValue(actionRequest, actionResponse, userId, httpServletRequest, encryptedStateMapJSON, authenticationTwoFactorCompanyConfiguration);

		actionRequest.setAttribute(WebKeys.REDIRECT, redirectURL);

		HttpSession httpSession = httpServletRequest.getSession();
		httpSession.setAttribute(TwoFactorSessionKeys.TWO_FACTOR_USER_ID, userId);
		httpSession.setAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_DIGEST, DigesterUtil.digest(encryptedStateMapJSON));
		httpSession.setAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_KEY, key);
		httpSession.setAttribute(TwoFactorSessionKeys.TWO_FACTOR_REDIRECT, ParamUtil.getString(actionRequest, "redirect"));
	}

}
