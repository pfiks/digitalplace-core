<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%@page import="com.liferay.document.library.kernel.exception.FileExtensionException"%>
<%@page import="com.liferay.document.library.kernel.exception.FileSizeException"%>
<%@page import="com.liferay.portal.kernel.json.JSONException"%>
<%@page import="com.placecube.digitalplace.userimport.admin.web.constants.MVCCommandKeys"%>
