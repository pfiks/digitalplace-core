package com.placecube.digitalplace.authentication.twofactor.web.constants;

public final class TwoFactorMVCCommandKeys {

	public static final String RESET = "two-factor-authentication/reset";
	
	public static final String SEND_EMAIL_WITH_CODE = "/two-factor-authentication/send-email-with-code";

	public static final String SETUP = "two-factor-authentication/setup";

	public static final String USER_RESET = "two-factor-authentication/user-reset";

	public static final String VERIFY = "/two-factor-authentication/verify";

	public static final String VERIFY_BY_EMAIL = "/two-factor-authentication/verify-by-email";

	private TwoFactorMVCCommandKeys() {
	}

}
