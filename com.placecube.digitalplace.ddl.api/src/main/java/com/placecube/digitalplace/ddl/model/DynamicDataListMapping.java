package com.placecube.digitalplace.ddl.model;

import java.util.Optional;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;

public interface DynamicDataListMapping {

	DDMStructure getDDMStructure();

	long getDDMStructureId();

	Optional<DDMTemplate> getFullViewDefaultTemplate();

	long getFullViewDefaultTemplateId();

	String getIconName();

	String getIndexerContentFieldName();

	String getIndexerTitleFieldName();

	String getMappingName();

	Optional<DDMTemplate> getSearchDefaultGeopointViewTemplate();

	long getSearchDefaultGeopointViewTemplateId();

	Optional<DDMTemplate> getSearchDefaultGridViewTemplate();

	long getSearchDefaultGridViewTemplateId();

	Optional<DDMTemplate> getSearchDefaultListViewTemplate();

	long getSearchDefaultListViewTemplateId();
}
