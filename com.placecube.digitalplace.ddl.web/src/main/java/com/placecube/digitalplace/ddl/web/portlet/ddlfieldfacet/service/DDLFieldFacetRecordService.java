package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = DDLFieldFacetRecordService.class)
public class DDLFieldFacetRecordService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLFieldFacetRecordService.class);

	private static final String DDM_STRUCTURE_KEY = "ddmStructureKey";

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDLFieldFacetDDMFormFieldService ddlFieldFacetDDMFormFieldService;

	public List<DDLRecord> searchDDLRecords(long ddmLinkStructureId, HttpServletRequest httpServletRequest) throws PortalException {
		DDMStructure ddmLinkStructure = ddmStructureLocalService.getDDMStructure(ddmLinkStructureId);

		SearchContext searchContext = SearchContextFactory.getInstance(httpServletRequest);

		addBooleanClause(searchContext, DDM_STRUCTURE_KEY, ddmLinkStructure.getStructureKey(), BooleanClauseOccur.MUST.getName());

		Hits hits = ddlRecordLocalService.search(searchContext);

		Document[] documents = hits.getDocs();

		if (Objects.nonNull(documents)) {
			return Arrays.stream(documents).map(d -> ddlRecordLocalService.fetchDDLRecord(getDocumentEntryClassPK(d))).filter(Objects::nonNull).collect(Collectors.toList());
		} else {
			return Collections.emptyList();
		}
	}

	public String getDDLRecordFieldValue(long recordId, String fieldName, Locale locale) {
		try {
			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(recordId);

			return ddlFieldFacetDDMFormFieldService.getDDMFormFieldStringValue(ddlRecord, fieldName, locale);
		} catch (PortalException e) {
			LOG.error("Error retrieving label for DDL record: " + recordId + ", fieldName: " + fieldName);
		}

		return StringPool.BLANK;
	}

	private long getDocumentEntryClassPK(Document document) {
		return GetterUtil.getLong(document.get(Field.ENTRY_CLASS_PK));
	}

	private void addBooleanClause(SearchContext searchContext, String field, String value, String occur) {
		BooleanClause<Query> queryToAdd = BooleanClauseFactoryUtil.create(field, value, occur);

		BooleanClause<Query>[] booleanClauses = searchContext.getBooleanClauses();
		if (Validator.isNull(booleanClauses)) {
			booleanClauses = new BooleanClause[0];
		}

		searchContext.setBooleanClauses(ArrayUtil.append(booleanClauses, queryToAdd));
	}

}
