package com.placecube.digitalplace.form.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.constants.DDMStructureConstants;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormLayout;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureLayout;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLayoutLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.storage.StorageType;
import com.liferay.exportimport.kernel.lar.ExportImportThreadLocal;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.form.exception.FormDefinitionException;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.digitalplace.form.service.impl.internal.DDMDataProviderService;
import com.placecube.digitalplace.form.service.impl.internal.DDMFormInstanceService;
import com.placecube.digitalplace.form.service.impl.internal.FormDefinitionDeserializerService;
import com.placecube.digitalplace.form.service.impl.internal.FormJournalArticleService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ExportImportThreadLocal.class ,LanguageUtil.class })
public class FormDefinitionServiceImplTest {

	private static final String CATEGORY_1 = "category1";

	private static final String CATEGORY_2 = "category2";

	private static final long CLASS_NAME_ID = 3;

	private static final String DESCRIPTION = "description";

	private static final String FORM_JOURNAL_ARTICLE_FOLDER_NAME = "folder";

	private static final long GROUP_ID = 2;

	private static final String KEY = "key";

	private static final String LOCALISED_DESCRIPTION = "localisedDescription";

	private static final String LOCALISED_NAME = "localisedName";

	private static final String NAME = "name";

	private static final long USER_ID = 1;

	private static final long COMPANY_ID = 1234;

	private Map<Locale, String> descriptionMap;

	@InjectMocks
	private FormDefinitionServiceImpl formDefinitionServiceImpl;

	@Mock
	private HttpURLConnection mockConnection;

	@Mock
	private DDMDataProviderInstance mockDDMDataProviderInstance;

	@Mock
	private DDMDataProviderService mockDDMDataProviderService;

	@Mock
	private DDMForm mockDDMForm;

	@Mock
	private DDMFormInstance mockDDMFormInstance;

	@Mock
	private DDMFormInstanceLocalService mockDDMFormInstanceLocalService;

	@Mock
	private DDMFormInstanceService mockDDMFormInstanceService;

	@Mock
	private DDMFormLayout mockDDMFormLayout;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLayout mockDDMStructureLayout;

	@Mock
	private DDMStructureLayoutLocalService mockDDMStructureLayoutLocalService;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private JournalFolder mockFolder;

	@Mock
	private FormDefinition mockFormDefinition;

	@Mock
	private FormDefinition mockFormDefinition1;

	@Mock
	private FormDefinition mockFormDefinition2;

	@Mock
	private FormDefinitionDeserializerService mockFormDefinitionDeserializerService;

	@Mock
	private FormJournalArticleService mockFormJournalArticleService;

	@Mock
	private String mockJsonDDMFormDefinition;

	@Mock
	private String mockJsonDDMFormLayout;

	@Mock
	private String mockJsonSettingsDDMFormValues;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;
	private Map<Locale, String> nameMap;

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorAddingFormDefinition_ThenFormDefinitionExceptionIsThrown() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		when(mockDDMFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext)).thenThrow(new PortalException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorAddingFormInstanceAndStructureIsNotNull_ThenDeleteStructure() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		when(mockDDMFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext)).thenThrow(new PortalException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verify(mockDDMStructureLocalService, times(1)).deleteDDMStructure(mockDDMStructure);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorAddingStructure_ThenFormDefinitionExceptionIsThrown() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenThrow(new PortalException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorAddingStructureAndStructureLayoutIsNotNull_ThenStructureLayoutIsDeleted() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenThrow(new PortalException());
		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(CLASS_NAME_ID);
		when(mockDDMStructureLayoutLocalService.fetchStructureLayout(GROUP_ID, CLASS_NAME_ID, KEY.toUpperCase())).thenReturn(mockDDMStructureLayout);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verify(mockDDMStructureLayoutLocalService, times(1)).deleteDDMStructureLayout(mockDDMStructureLayout);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorCreatingArticles_ThenFormInstanceIsDeleted() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		List<URL> journalArticleURLs = new ArrayList<>();
		URL journalArticle = new URL(null, "file://journal-article.html", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(journalArticleURLs);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockDDMFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext)).thenReturn(mockDDMFormInstance);

		when(mockFormJournalArticleService.addJournalFolder(mockFormDefinition, mockServiceContext)).thenReturn(mockFolder);
		doThrow(PortalException.class).when(mockFormJournalArticleService).addJournalArticles(mockFormDefinition, mockFolder, mockServiceContext);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verify(mockDDMFormInstanceLocalService, times(1)).deleteDDMFormInstance(mockDDMFormInstance);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorCreatingFolder_ThenFormInstanceIsDeleted() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		List<URL> journalArticleURLs = new ArrayList<>();
		URL journalArticle = new URL(null, "file://journal-article.html", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(journalArticleURLs);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockDDMFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext)).thenReturn(mockDDMFormInstance);

		when(mockFormJournalArticleService.addJournalFolder(mockFormDefinition, mockServiceContext)).thenThrow(PortalException.class);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verify(mockDDMFormInstanceLocalService, times(1)).deleteDDMFormInstance(mockDDMFormInstance);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorGettingDDMForm_ThenFormDefinitionExceptionIsThrown() throws Exception {

		mockAddFormDefinition();
		List<DDMDataProviderInstance> ddmDataProviderInstances = new ArrayList<>();
		Map<String, String> placeholderValues = new HashMap<>();

		when(mockDDMDataProviderService.getOrCreateDataProviders(mockFormDefinition, mockServiceContext)).thenReturn(ddmDataProviderInstances);
		when(mockDDMDataProviderService.getDataProviderPlaceholders(ddmDataProviderInstances, Locale.UK)).thenReturn(placeholderValues);

		when(mockFormDefinitionDeserializerService.getDDMFormDefinition(mockFormDefinition)).thenThrow(new IOException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorGettingDDMFormDefinitionAndStructureIsNull_ThenNothingIsDeleted() throws Exception {

		mockAddFormDefinition();
		List<DDMDataProviderInstance> ddmDataProviderInstances = new ArrayList<>();
		Map<String, String> placeholderValues = new HashMap<>();

		when(mockDDMDataProviderService.getOrCreateDataProviders(mockFormDefinition, mockServiceContext)).thenReturn(ddmDataProviderInstances);
		when(mockDDMDataProviderService.getDataProviderPlaceholders(ddmDataProviderInstances, Locale.UK)).thenReturn(placeholderValues);

		when(mockFormDefinitionDeserializerService.getDDMFormDefinition(mockFormDefinition)).thenThrow(new IOException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verify(mockDDMStructureLocalService, never()).deleteDDMStructure(any(DDMStructure.class));

	}

	@Test(expected = Exception.class)
	public void addFormDefinition_WhenErrorGettingDDMFormLayout_ThenFormDefinitionExceptionIsThrown() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockFormDefinitionDeserializerService.getDDMFormLayout(mockFormDefinition)).thenThrow(new IOException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorGettingDDMFormLayoutDefinitionAndStructureIsNull_ThenNothingIsDeleted() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockFormDefinitionDeserializerService.getDDMFormLayout(mockFormDefinition)).thenThrow(new IOException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verify(mockDDMStructureLocalService, never()).deleteDDMStructure(any(DDMStructure.class));

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorGettingDDMFormValues_ThenFormDefinitionExceptionIsThrown() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockFormDefinitionDeserializerService.getDDMFormValues(mockFormDefinition, mockDDMForm, mockServiceContext)).thenThrow(new IOException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinition_WhenErrorGettingDDMFormValuesAndStructureIsNotNull_ThenDeleteStructure() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockFormDefinitionDeserializerService.getDDMFormValues(mockFormDefinition, mockDDMForm, mockServiceContext)).thenThrow(new IOException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verify(mockDDMStructureLocalService, times(1)).deleteDDMStructure(mockDDMStructure);

	}

	@Test
	public void addFormDefinition_WhenJournalFolderNameIsDefinedAndJournalArticleURLListIsEmpty_ThenDoesNotCreateFolderOrImportJournalArticles() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(Collections.emptyList());

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		when(mockFormJournalArticleService.addJournalFolder(mockFormDefinition, mockServiceContext)).thenReturn(mockFolder);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verifyZeroInteractions(mockFormJournalArticleService);
	}

	@Test
	public void addFormDefinition_WhenJournalFolderNameIsDefinedAndJournalArticleURLsAreDefined_ThenDoesCreateFolderAndImportJournalArticles() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		List<URL> journalArticleURLs = new ArrayList<>();
		URL journalArticle = new URL(null, "file://journal-article.html", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(journalArticleURLs);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		when(mockFormJournalArticleService.addJournalFolder(mockFormDefinition, mockServiceContext)).thenReturn(mockFolder);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		InOrder inOrder = inOrder(mockFormJournalArticleService);
		inOrder.verify(mockFormJournalArticleService, times(1)).addJournalFolder(mockFormDefinition, mockServiceContext);
		verifyStatic(ExportImportThreadLocal.class, times(1));
		ExportImportThreadLocal.setLayoutValidationInProcess(true);
		inOrder.verify(mockFormJournalArticleService, times(1)).addJournalArticles(mockFormDefinition, mockFolder, mockServiceContext);
		verifyStatic(ExportImportThreadLocal.class, times(1));
		ExportImportThreadLocal.setLayoutValidationInProcess(false);

	}

	@Test
	public void addFormDefinition_WhenJournalFolderNameIsDefinedAndJournalArticleURLsAreNotDefined_ThenDoesNotCreateFolderOrImportJournalArticles() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(null);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verifyZeroInteractions(mockFormJournalArticleService);
	}

	@Test
	public void addFormDefinition_WhenJournalFolderNameIsNotDefined_ThenDoesNotCreateFolderOrImportJournalArticles() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(null);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verifyZeroInteractions(mockFormJournalArticleService);
	}

	@Test
	public void addFormDefinition_WhenStructureDoesNotExistAndCollectionNoEmpty_ThenFormInstanceIsAdded() throws Exception {

		mockAddFormDefinition();
		List<DDMDataProviderInstance> ddmDataProviderInstances = new ArrayList<>();
		Map<String, String> placeholderValues = new HashMap<>();
		String jsonDDMFormDefinition = "jsonDDMFormDefinition";

		ddmDataProviderInstances.add(mockDDMDataProviderInstance);
		placeholderValues.put("placeholderKey", "placeholderValue");

		when(mockDDMDataProviderService.getOrCreateDataProviders(mockFormDefinition, mockServiceContext)).thenReturn(ddmDataProviderInstances);
		when(mockDDMDataProviderService.getDataProviderPlaceholders(ddmDataProviderInstances, Locale.UK)).thenReturn(placeholderValues);
		when(mockFormDefinitionDeserializerService.getDDMFormDefinition(mockFormDefinition)).thenReturn(jsonDDMFormDefinition);
		when(mockDDMDataProviderService.replacePlaceholders(jsonDDMFormDefinition, placeholderValues)).thenReturn(mockJsonDDMFormDefinition);

		when(mockFormDefinitionDeserializerService.getDDMForm(mockFormDefinition, Locale.UK)).thenReturn(mockDDMForm);
		when(mockFormDefinitionDeserializerService.getDDMForm(mockJsonDDMFormDefinition, Locale.UK)).thenReturn(mockDDMForm);
		when(mockFormDefinitionDeserializerService.getDDMFormLayout(mockFormDefinition)).thenReturn(mockDDMFormLayout);
		when(mockFormDefinitionDeserializerService.getDDMFormValues(mockFormDefinition, mockDDMForm, mockServiceContext)).thenReturn(mockDDMFormValues);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		InOrder inOrder = inOrder(mockDDMDataProviderService, mockDDMFormInstanceService, mockServiceContext);
		inOrder.verify(mockDDMDataProviderService, times(1)).replacePlaceholders(jsonDDMFormDefinition, placeholderValues);
		inOrder.verify(mockServiceContext, times(1)).setAttribute("status", WorkflowConstants.STATUS_APPROVED);
		inOrder.verify(mockDDMFormInstanceService, times(1)).addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext);
	}

	@Test
	public void addFormDefinition_WhenStructureDoesNotExistAndCollectionsEmpty_ThenFormInstanceIsAdded() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		InOrder inOrder = inOrder(mockDDMFormInstanceService, mockServiceContext);
		inOrder.verify(mockServiceContext, times(1)).setAttribute("status", WorkflowConstants.STATUS_APPROVED);
		inOrder.verify(mockDDMFormInstanceService, times(1)).addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext);

	}

	@Test
	public void addFormDefinition_WhenStructureExists_ThenNothingIsAdded() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, CLASS_NAME_ID, KEY)).thenReturn(mockDDMStructure);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockServiceContext);

		verify(mockDDMStructureLocalService, never()).addStructure(anyLong(), anyLong(), anyLong(), anyLong(), anyString(), any(), any(), any(), any(), any(), anyInt(), any());
	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenErrorAddingFormDefinition_ThenFormDefinitionExceptionIsThrown() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		when(mockDDMFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext)).thenThrow(new PortalException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenErrorAddingFormInstanceAndStructureIsNotNull_ThenDeleteStructure() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		when(mockDDMFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext)).thenThrow(new PortalException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		verify(mockDDMStructureLocalService, times(1)).deleteDDMStructure(mockDDMStructure);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenErrorAddingStructure_ThenFormDefinitionExceptionIsThrown() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenThrow(new PortalException());

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenErrorAddingStructureAndStructureLayoutIsNotNull_ThenStructureLayoutIsDeleted() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenThrow(new PortalException());

		when(mockDDMStructureLayoutLocalService.fetchStructureLayout(GROUP_ID, CLASS_NAME_ID, KEY.toUpperCase())).thenReturn(mockDDMStructureLayout);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		verify(mockDDMStructureLayoutLocalService, times(1)).deleteDDMStructureLayout(mockDDMStructureLayout);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenErrorCreatingArticles_ThenFormInstanceIsDeleted() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		List<URL> journalArticleURLs = new ArrayList<>();
		URL journalArticle = new URL(null, "file://journal-article.html", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(journalArticleURLs);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockDDMFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext)).thenReturn(mockDDMFormInstance);

		when(mockFormJournalArticleService.addJournalFolder(mockFormDefinition, mockServiceContext)).thenReturn(mockFolder);
		doThrow(PortalException.class).when(mockFormJournalArticleService).addJournalArticles(mockFormDefinition, mockFolder, mockServiceContext);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		verify(mockDDMFormInstanceLocalService, times(1)).deleteDDMFormInstance(mockDDMFormInstance);

	}

	@Test(expected = FormDefinitionException.class)
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenErrorCreatingFolder_ThenFormInstanceIsDeleted() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		List<URL> journalArticleURLs = new ArrayList<>();
		URL journalArticle = new URL(null, "file://journal-article.html", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(journalArticleURLs);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);
		when(mockDDMFormInstanceService.addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext)).thenReturn(mockDDMFormInstance);

		when(mockFormJournalArticleService.addJournalFolder(mockFormDefinition, mockServiceContext)).thenThrow(PortalException.class);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		verify(mockDDMFormInstanceLocalService, times(1)).deleteDDMFormInstance(mockDDMFormInstance);

	}

	@Test
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenJournalFolderNameIsDefinedAndJournalArticleURLListIsEmpty_ThenDoesNotCreateFolderOrImportJournalArticles() throws Exception {

		mockAddFormDefinition();
		mockDefitionFromFormDefinitionObject();

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(Collections.emptyList());

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		when(mockFormJournalArticleService.addJournalFolder(mockFormDefinition, mockServiceContext)).thenReturn(mockFolder);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		verifyZeroInteractions(mockFormJournalArticleService);
	}

	@Test
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenJournalFolderNameIsDefinedAndJournalArticleURLsAreDefined_ThenDoesCreateFolderAndImportJournalArticles() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		List<URL> journalArticleURLs = new ArrayList<>();
		URL journalArticle = new URL(null, "file://journal-article.html", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(journalArticleURLs);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		when(mockFormJournalArticleService.addJournalFolder(mockFormDefinition, mockServiceContext)).thenReturn(mockFolder);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		InOrder inOrder = inOrder(mockFormJournalArticleService);
		inOrder.verify(mockFormJournalArticleService, times(1)).addJournalFolder(mockFormDefinition, mockServiceContext);
		verifyStatic(ExportImportThreadLocal.class, times(1));
		ExportImportThreadLocal.setLayoutValidationInProcess(true);
		inOrder.verify(mockFormJournalArticleService, times(1)).addJournalArticles(mockFormDefinition, mockFolder, mockServiceContext);
		verifyStatic(ExportImportThreadLocal.class, times(1));
		ExportImportThreadLocal.setLayoutValidationInProcess(false);	}

	@Test
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenJournalFolderNameIsDefinedAndJournalArticleURLsAreNotDefined_ThenDoesNotCreateFolderOrImportJournalArticles()
			throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FORM_JOURNAL_ARTICLE_FOLDER_NAME);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(null);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		verifyZeroInteractions(mockFormJournalArticleService);
	}

	@Test
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenJournalFolderNameIsNotDefined_ThenDoesNotCreateFolderOrImportJournalArticles() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(null);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		verifyZeroInteractions(mockFormJournalArticleService);
	}

	@Test
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenStructureDoesNotExist_ThenFormInstanceIsAdded() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, CLASS_NAME_ID, KEY, nameMap, descriptionMap, mockDDMForm,
				mockDDMFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, mockServiceContext)).thenReturn(mockDDMStructure);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		InOrder inOrder = inOrder(mockDDMFormInstanceService, mockServiceContext);
		inOrder.verify(mockServiceContext, times(1)).setAttribute("status", WorkflowConstants.STATUS_APPROVED);
		inOrder.verify(mockDDMFormInstanceService, times(1)).addFormInstance(USER_ID, GROUP_ID, mockDDMStructure, nameMap, descriptionMap, mockDDMFormValues, mockServiceContext);

	}

	@Test
	public void addFormDefinitionWithFormDefinitionAndJSONParameters_WhenStructureExists_ThenNothingIsAdded() throws Exception {

		mockAddFormDefinition();
		mockDefinitionFromJSON();

		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, CLASS_NAME_ID, KEY)).thenReturn(mockDDMStructure);

		formDefinitionServiceImpl.addFormDefinition(mockFormDefinition, mockJsonDDMFormDefinition, mockJsonDDMFormLayout, mockJsonSettingsDDMFormValues, mockServiceContext);

		verify(mockDDMStructureLocalService, never()).addStructure(anyLong(), anyLong(), anyLong(), anyLong(), anyString(), any(), any(), any(), any(), any(), anyInt(), any());
	}

	@Test
	public void create_WhenNoError_ThenReturnsFormDefinitionCreated() throws IOException {

		String category = "category";
		String name = "name";
		String key = "key";
		String description = "description";
		String journalArticleFolderName = "folder";

		URL ddmForm = new URL(null, "file://ddm-form.json", createURLStreamHandler());
		URL ddmFormLayout = new URL(null, "file://ddm-form-layout.json", createURLStreamHandler());
		URL settingsDDMFormValues = new URL(null, "file://settings-ddm-form-values.json", createURLStreamHandler());

		List<URL> journalArticleURLs = new ArrayList<>();
		URL journalArticle = new URL(null, "file://journal-article.xml", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		List<URL> dataProviderURLs = new ArrayList<>();
		URL dataProviderURL = new URL(null, "file://data-provider.json", createURLStreamHandler());
		dataProviderURLs.add(dataProviderURL);

		FormDefinition result = formDefinitionServiceImpl.create(name, description, category, key, journalArticleFolderName, ddmForm, ddmFormLayout, settingsDDMFormValues, journalArticleURLs,
				dataProviderURLs);

		assertThat(result.getName(), equalTo(name));
		assertThat(result.getCategory(), equalTo(category));
		assertThat(result.getDescription(), equalTo(description));
		assertThat(result.getUrlToDDMFormDefinitionFile(), equalTo(ddmForm));
		assertThat(result.getUrlToDDMFormLayoutDefinitionFile(), equalTo(ddmFormLayout));
		assertThat(result.getUrlToSettingsDDMFormValuesDefinitionFile(), equalTo(settingsDDMFormValues));

	}

	@Test
	public void create_WithNameAndDescriptionAndKeyParameter_WhenNoError_ThenReturnsFormDefinitionCreated() {

		String name = "name";
		String key = "key";
		String description = "description";

		FormDefinition result = formDefinitionServiceImpl.create(name, description, key);

		assertThat(result.getName(), equalTo(name));
		assertThat(result.getDescription(), equalTo(description));

	}

	@Before
	public void setUp() {

		mockStatic(ExportImportThreadLocal.class, LanguageUtil.class);

	}

	@Test
	public void sort_WhenNoError_ThenReturnSortedList() {

		when(mockFormDefinition1.getCategory()).thenReturn(CATEGORY_1);
		when(mockFormDefinition2.getCategory()).thenReturn(CATEGORY_2);

		List<FormDefinition> formDefinitions = new ArrayList<>();
		formDefinitions.add(mockFormDefinition2);
		formDefinitions.add(mockFormDefinition1);

		List<FormDefinition> result = formDefinitionServiceImpl.sort(formDefinitions);

		assertThat(result.get(0), sameInstance(mockFormDefinition1));
		assertThat(result.get(1), sameInstance(mockFormDefinition2));
	}

	private URLStreamHandler createURLStreamHandler() {

		URLStreamHandler stubURLStreamHandler = new URLStreamHandler() {

			@Override
			protected URLConnection openConnection(URL u) {
				return mockConnection;
			}
		};

		return stubURLStreamHandler;
	}

	private void mockAddFormDefinition() throws Exception {
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(CLASS_NAME_ID);
		when(mockPortal.getSiteDefaultLocale(GROUP_ID)).thenReturn(Locale.UK);
		when(mockFormDefinition.getKey()).thenReturn(KEY);
		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(null);
		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(null);

		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, CLASS_NAME_ID, KEY)).thenReturn(null);

		when(mockFormDefinition.getName()).thenReturn(NAME);
		when(mockFormDefinition.getDescription()).thenReturn(DESCRIPTION);
		Set<Locale> locales = new HashSet<>();
		locales.add(Locale.UK);
		when(LanguageUtil.getAvailableLocales(GROUP_ID)).thenReturn(locales);
		when(LanguageUtil.get(Locale.UK, NAME)).thenReturn(LOCALISED_NAME);
		when(LanguageUtil.get(Locale.UK, DESCRIPTION)).thenReturn(LOCALISED_DESCRIPTION);

		nameMap = new HashMap<>();
		nameMap.put(Locale.UK, LOCALISED_NAME);
		descriptionMap = new HashMap<>();
		descriptionMap.put(Locale.UK, LOCALISED_DESCRIPTION);
	}

	private void mockDefinitionFromJSON() {
		when(mockFormDefinitionDeserializerService.getDDMForm(mockJsonDDMFormDefinition, Locale.UK)).thenReturn(mockDDMForm);
		when(mockFormDefinitionDeserializerService.getDDMFormLayout(mockJsonDDMFormLayout)).thenReturn(mockDDMFormLayout);
		when(mockFormDefinitionDeserializerService.getDDMFormValues(mockJsonSettingsDDMFormValues, mockDDMForm)).thenReturn(mockDDMFormValues);
	}

	private void mockDefitionFromFormDefinitionObject() throws Exception {

		List<DDMDataProviderInstance> ddmDataProviderInstances = new ArrayList<>();
		Map<String, String> placeholderValues = new HashMap<>();
		String jsonDDMFormDefinition = "jsonDDMFormDefinition";

		when(mockDDMDataProviderService.getOrCreateDataProviders(mockFormDefinition, mockServiceContext)).thenReturn(ddmDataProviderInstances);
		when(mockDDMDataProviderService.getDataProviderPlaceholders(ddmDataProviderInstances, Locale.UK)).thenReturn(placeholderValues);
		when(mockFormDefinitionDeserializerService.getDDMFormDefinition(mockFormDefinition)).thenReturn(jsonDDMFormDefinition);
		when(mockDDMDataProviderService.replacePlaceholders(jsonDDMFormDefinition, placeholderValues)).thenReturn(mockJsonDDMFormDefinition);

		when(mockFormDefinitionDeserializerService.getDDMForm(mockFormDefinition, Locale.UK)).thenReturn(mockDDMForm);
		when(mockFormDefinitionDeserializerService.getDDMForm(mockJsonDDMFormDefinition, Locale.UK)).thenReturn(mockDDMForm);
		when(mockFormDefinitionDeserializerService.getDDMFormLayout(mockFormDefinition)).thenReturn(mockDDMFormLayout);
		when(mockFormDefinitionDeserializerService.getDDMFormValues(mockFormDefinition, mockDDMForm, mockServiceContext)).thenReturn(mockDDMFormValues);
	}
}
