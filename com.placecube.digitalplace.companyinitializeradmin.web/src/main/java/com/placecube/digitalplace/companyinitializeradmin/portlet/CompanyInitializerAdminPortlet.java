package com.placecube.digitalplace.companyinitializeradmin.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.companyinitializeradmin.constants.PortletKeys;
import com.placecube.digitalplace.companyinitializeradmin.constants.PortletRequestKeys;
import com.placecube.digitalplace.companyinitializeradmin.constants.ViewKeys;
import com.placecube.digitalplace.companyinitializeradmin.initializer.internal.service.CompanyInitializerRegistry;
import com.placecube.digitalplace.initialiser.CompanyInitializer;

@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true", "com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false", "com.liferay.portlet.private-session-attributes=false", "com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true", "javax.portlet.expiration-cache=0", "javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=" + ViewKeys.VIEW,
		"javax.portlet.name=" + PortletKeys.COMPANY_INITIALIZER_ADMIN, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html" }, service = Portlet.class)
public class CompanyInitializerAdminPortlet extends MVCPortlet {

	@Reference
	CompanyInitializerRegistry companyInitializerRegistry;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		List<CompanyInitializer> companyInitializers = companyInitializerRegistry.getCompanyInitializers();

		renderRequest.setAttribute(PortletRequestKeys.COMPANY_INITIALIZERS, companyInitializers);

		super.render(renderRequest, renderResponse);
	}
}