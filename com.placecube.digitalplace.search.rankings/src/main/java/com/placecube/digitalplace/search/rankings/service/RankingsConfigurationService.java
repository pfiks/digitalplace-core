package com.placecube.digitalplace.search.rankings.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.search.rankings.configuration.RankingsCompanyConfiguration;

@Component(immediate = true, service = RankingsConfigurationService.class)
public class RankingsConfigurationService {

	private static final Log LOG = LogFactoryUtil.getLog(RankingsConfigurationService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	public boolean isEnableWildcardRankings(long companyId) {
		try {
			RankingsCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(RankingsCompanyConfiguration.class, companyId);
			return configuration.enableWildcardRankings();

		} catch (ConfigurationException e) {
			LOG.error("Error retrieving enable wildcard ranking setting.", e);

			return false;
		}

	}
}
