/**
 * CustomerInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class CustomerInfo  implements java.io.Serializable {
    private java.lang.String customerString1;

    private java.lang.String customerString2;

    private java.lang.String customerString3;

    private java.lang.String customerString4;

    private java.lang.String customerString5;

    private java.lang.Integer customerNumber1;

    private java.lang.Integer customerNumber2;

    private java.lang.Integer customerNumber3;

    private java.lang.Integer customerNumber4;

    public CustomerInfo() {
    }

    public CustomerInfo(
           java.lang.String customerString1,
           java.lang.String customerString2,
           java.lang.String customerString3,
           java.lang.String customerString4,
           java.lang.String customerString5,
           java.lang.Integer customerNumber1,
           java.lang.Integer customerNumber2,
           java.lang.Integer customerNumber3,
           java.lang.Integer customerNumber4) {
           this.customerString1 = customerString1;
           this.customerString2 = customerString2;
           this.customerString3 = customerString3;
           this.customerString4 = customerString4;
           this.customerString5 = customerString5;
           this.customerNumber1 = customerNumber1;
           this.customerNumber2 = customerNumber2;
           this.customerNumber3 = customerNumber3;
           this.customerNumber4 = customerNumber4;
    }


    /**
     * Gets the customerString1 value for this CustomerInfo.
     *
     * @return customerString1
     */
    public java.lang.String getCustomerString1() {
        return customerString1;
    }


    /**
     * Sets the customerString1 value for this CustomerInfo.
     *
     * @param customerString1
     */
    public void setCustomerString1(java.lang.String customerString1) {
        this.customerString1 = customerString1;
    }


    /**
     * Gets the customerString2 value for this CustomerInfo.
     *
     * @return customerString2
     */
    public java.lang.String getCustomerString2() {
        return customerString2;
    }


    /**
     * Sets the customerString2 value for this CustomerInfo.
     *
     * @param customerString2
     */
    public void setCustomerString2(java.lang.String customerString2) {
        this.customerString2 = customerString2;
    }


    /**
     * Gets the customerString3 value for this CustomerInfo.
     *
     * @return customerString3
     */
    public java.lang.String getCustomerString3() {
        return customerString3;
    }


    /**
     * Sets the customerString3 value for this CustomerInfo.
     *
     * @param customerString3
     */
    public void setCustomerString3(java.lang.String customerString3) {
        this.customerString3 = customerString3;
    }


    /**
     * Gets the customerString4 value for this CustomerInfo.
     *
     * @return customerString4
     */
    public java.lang.String getCustomerString4() {
        return customerString4;
    }


    /**
     * Sets the customerString4 value for this CustomerInfo.
     *
     * @param customerString4
     */
    public void setCustomerString4(java.lang.String customerString4) {
        this.customerString4 = customerString4;
    }


    /**
     * Gets the customerString5 value for this CustomerInfo.
     *
     * @return customerString5
     */
    public java.lang.String getCustomerString5() {
        return customerString5;
    }


    /**
     * Sets the customerString5 value for this CustomerInfo.
     *
     * @param customerString5
     */
    public void setCustomerString5(java.lang.String customerString5) {
        this.customerString5 = customerString5;
    }


    /**
     * Gets the customerNumber1 value for this CustomerInfo.
     *
     * @return customerNumber1
     */
    public java.lang.Integer getCustomerNumber1() {
        return customerNumber1;
    }


    /**
     * Sets the customerNumber1 value for this CustomerInfo.
     *
     * @param customerNumber1
     */
    public void setCustomerNumber1(java.lang.Integer customerNumber1) {
        this.customerNumber1 = customerNumber1;
    }


    /**
     * Gets the customerNumber2 value for this CustomerInfo.
     *
     * @return customerNumber2
     */
    public java.lang.Integer getCustomerNumber2() {
        return customerNumber2;
    }


    /**
     * Sets the customerNumber2 value for this CustomerInfo.
     *
     * @param customerNumber2
     */
    public void setCustomerNumber2(java.lang.Integer customerNumber2) {
        this.customerNumber2 = customerNumber2;
    }


    /**
     * Gets the customerNumber3 value for this CustomerInfo.
     *
     * @return customerNumber3
     */
    public java.lang.Integer getCustomerNumber3() {
        return customerNumber3;
    }


    /**
     * Sets the customerNumber3 value for this CustomerInfo.
     *
     * @param customerNumber3
     */
    public void setCustomerNumber3(java.lang.Integer customerNumber3) {
        this.customerNumber3 = customerNumber3;
    }


    /**
     * Gets the customerNumber4 value for this CustomerInfo.
     *
     * @return customerNumber4
     */
    public java.lang.Integer getCustomerNumber4() {
        return customerNumber4;
    }


    /**
     * Sets the customerNumber4 value for this CustomerInfo.
     *
     * @param customerNumber4
     */
    public void setCustomerNumber4(java.lang.Integer customerNumber4) {
        this.customerNumber4 = customerNumber4;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerInfo)) return false;
        CustomerInfo other = (CustomerInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.customerString1==null && other.getCustomerString1()==null) ||
             (this.customerString1!=null &&
              this.customerString1.equals(other.getCustomerString1()))) &&
            ((this.customerString2==null && other.getCustomerString2()==null) ||
             (this.customerString2!=null &&
              this.customerString2.equals(other.getCustomerString2()))) &&
            ((this.customerString3==null && other.getCustomerString3()==null) ||
             (this.customerString3!=null &&
              this.customerString3.equals(other.getCustomerString3()))) &&
            ((this.customerString4==null && other.getCustomerString4()==null) ||
             (this.customerString4!=null &&
              this.customerString4.equals(other.getCustomerString4()))) &&
            ((this.customerString5==null && other.getCustomerString5()==null) ||
             (this.customerString5!=null &&
              this.customerString5.equals(other.getCustomerString5()))) &&
            ((this.customerNumber1==null && other.getCustomerNumber1()==null) ||
             (this.customerNumber1!=null &&
              this.customerNumber1.equals(other.getCustomerNumber1()))) &&
            ((this.customerNumber2==null && other.getCustomerNumber2()==null) ||
             (this.customerNumber2!=null &&
              this.customerNumber2.equals(other.getCustomerNumber2()))) &&
            ((this.customerNumber3==null && other.getCustomerNumber3()==null) ||
             (this.customerNumber3!=null &&
              this.customerNumber3.equals(other.getCustomerNumber3()))) &&
            ((this.customerNumber4==null && other.getCustomerNumber4()==null) ||
             (this.customerNumber4!=null &&
              this.customerNumber4.equals(other.getCustomerNumber4())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomerString1() != null) {
            _hashCode += getCustomerString1().hashCode();
        }
        if (getCustomerString2() != null) {
            _hashCode += getCustomerString2().hashCode();
        }
        if (getCustomerString3() != null) {
            _hashCode += getCustomerString3().hashCode();
        }
        if (getCustomerString4() != null) {
            _hashCode += getCustomerString4().hashCode();
        }
        if (getCustomerString5() != null) {
            _hashCode += getCustomerString5().hashCode();
        }
        if (getCustomerNumber1() != null) {
            _hashCode += getCustomerNumber1().hashCode();
        }
        if (getCustomerNumber2() != null) {
            _hashCode += getCustomerNumber2().hashCode();
        }
        if (getCustomerNumber3() != null) {
            _hashCode += getCustomerNumber3().hashCode();
        }
        if (getCustomerNumber4() != null) {
            _hashCode += getCustomerNumber4().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerString1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerString1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerString2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerString2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerString3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerString3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerString4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerString4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerString5");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerString5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNumber1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerNumber1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNumber2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerNumber2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNumber3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerNumber3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNumber4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "customerNumber4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
