package com.placecube.digitalplace.template.contributor.internal.upgrade.upgrade_1_0_0;

import java.io.File;
import java.util.List;

import com.liferay.fragment.model.FragmentEntry;
import com.liferay.fragment.service.FragmentEntryLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.template.contributor.internal.util.UpgradeHelper;

public class FragmentsUpgradeProcess extends UpgradeProcess {

	private static final String OUT_PUT_FORMAT = "CompanyId: %s, GroupId: %s, FragmentCollectionId: %s, FragmentEntryId: %s, FragmentEntryKey: %s \n";

	private final CompanyLocalService companyLocalService;

	private final FragmentEntryLocalService fragmentEntryLocalService;

	private File file;

	public FragmentsUpgradeProcess(CompanyLocalService companyLocalService, FragmentEntryLocalService fragmentEntryLocalService) {
		this.companyLocalService = companyLocalService;
		this.fragmentEntryLocalService = fragmentEntryLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {

		file = UpgradeHelper.getOutputFile(this);

		if (CompanyThreadLocal.getCompanyId() == 0) {
			for (Company company : companyLocalService.getCompanies()) {
				upgradeFragments(company.getCompanyId());
			}
		} else {
			upgradeFragments(CompanyThreadLocal.getCompanyId());
		}
	}

	private void upgradeFragments(long companyId) throws Exception {

		DynamicQuery dynamicQuery = fragmentEntryLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));

		List<FragmentEntry> fragmentEntries = fragmentEntryLocalService.dynamicQuery(dynamicQuery);

		for (FragmentEntry fragmentEntry : fragmentEntries) {
			updateFragmentEntry(fragmentEntry);
		}
	}

	private void updateFragmentEntry(FragmentEntry fragmentEntry) throws Exception {
		if (UpgradeHelper.hasComponent(fragmentEntry.getHtml())) {
			fragmentEntry.setHtml(UpgradeHelper.upgradeScript(fragmentEntry.getHtml()));
			fragmentEntryLocalService.updateFragmentEntry(fragmentEntry);
			UpgradeHelper.writeOutput(file, formatOutput(fragmentEntry));
		}
	}

	private String formatOutput(FragmentEntry fragmentEntry) {
		return String.format(OUT_PUT_FORMAT, fragmentEntry.getCompanyId(), fragmentEntry.getGroupId(), fragmentEntry.getFragmentCollectionId(), fragmentEntry.getFragmentEntryId(),
				fragmentEntry.getFragmentEntryKey());
	}

}
