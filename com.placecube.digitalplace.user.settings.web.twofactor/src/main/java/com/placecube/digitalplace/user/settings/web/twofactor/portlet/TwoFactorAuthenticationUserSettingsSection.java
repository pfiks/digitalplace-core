package com.placecube.digitalplace.user.settings.web.twofactor.portlet;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;
import com.placecube.digitalplace.user.settings.web.model.UserSettingsSection;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.util.TwoFactorAuthenticationUtil;

@Component(immediate = true, service = UserSettingsSection.class)
public class TwoFactorAuthenticationUserSettingsSection implements UserSettingsSection {

	private static final Log LOG = LogFactoryUtil.getLog(TwoFactorAuthenticationUserSettingsSection.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JSPRenderer jspRenderer;

	@Reference
	private TwoFactorAuthenticationUtil twoFactorAuthenticationUtil;

	@Reference
	private UserTwoFactorAuthenticationLocalService userTwoFactorAuthenticationLocalService;

	@Reference(target = "(osgi.web.symbolicname=" + TwoFactorAuthenticationKeys.BUNDLE_ID + ")", unbind = "-")
	private ServletContext servletContext;

	@Override
	public String getBundleId() {
		return TwoFactorAuthenticationKeys.BUNDLE_ID;
	}

	@Override
	public int getDisplayOrder() {
		return TwoFactorAuthenticationKeys.DISPLAY_ORDER;
	}

	@Override
	public DisplaySection getDisplaySection() {
		return DisplaySection.ACCOUNT_SETTINGS;
	}

	@Override
	public String getTitleKey() {
		return "two-factor-authentication";
	}

	@Override
	public boolean displayForUser(User user) {
		try {
			AuthenticationTwoFactorCompanyConfiguration companyConfiguration = configurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, user.getCompanyId());
			return companyConfiguration.enabled();
		} catch (ConfigurationException e) {
			LOG.error("Can not get 2FA company configuration - will not be visible to user " + e.getMessage());
			LOG.debug(e);
			return false;
		}
	}

	@Override
	public void render(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
		RenderRequest renderRequest = (RenderRequest) httpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST);

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User user = themeDisplay.getUser();

		Optional<UserTwoFactorAuthentication> userTwoFactorAuthenticationOpt = userTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(user);

		boolean twoFactorEnabled = twoFactorAuthenticationUtil.isTwoFactorEnabled(renderRequest, userTwoFactorAuthenticationOpt);

		if (twoFactorEnabled) {
			boolean twoFactorEnforced = userTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(user);
			renderRequest.setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_ENFORCED, twoFactorEnforced);

			String secretKey = (String) renderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY);

			if (Validator.isNull(secretKey)) {
				secretKey = twoFactorAuthenticationUtil.getSecretKey(renderRequest, userTwoFactorAuthenticationOpt);
				renderRequest.setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY, secretKey);
			}

			try {
				String qrURL = userTwoFactorAuthenticationLocalService.getQRBarCodeURL(user.getCompanyId(), user.getEmailAddress(), secretKey);
				renderRequest.setAttribute(TwoFactorAuthenticationRequestKeys.QR_URL, qrURL);
			} catch (PortalException exception) {
				LOG.error("Error getting qr URL " + exception.getMessage());
				LOG.debug(exception);
			}

		}

		renderRequest.setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, twoFactorEnabled);

		jspRenderer.renderJSP(servletContext, httpServletRequest, httpServletResponse, "/view.jsp");
	}

}
