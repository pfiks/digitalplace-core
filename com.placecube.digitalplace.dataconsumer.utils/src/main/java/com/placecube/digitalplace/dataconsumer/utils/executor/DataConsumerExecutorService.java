package com.placecube.digitalplace.dataconsumer.utils.executor;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.service.DataConsumerLocalService;
import com.placecube.digitalplace.dataconsumer.utils.DataConsumerUtils;
import com.placecube.digitalplace.dataconsumer.utils.DataConsumerWebClient;
import com.placecube.digitalplace.dataconsumer.utils.DataConsumerWebClientUtils;

@Component(immediate = true, service = DataConsumerExecutorService.class)
public class DataConsumerExecutorService {

	private static final Log LOG = LogFactoryUtil.getLog(DataConsumerExecutorService.class);

	@Reference
	private DataConsumerLocalService dataConsumerLocalService;

	@Reference
	private DataConsumerUtils dataConsumerUtils;

	@Reference
	private DataConsumerWebClient dataconsumerWebClient;

	@Reference
	private DataConsumerWebClientUtils dataconsumerWebClientUtils;

	@Reference
	private DDMFormInstanceLocalService ddmFormInstanceLocalService;

	@Reference
	private JSONFactory json;

	@Reference
	private Portal portal;

	public void executeDataConsumersFromActionRequest(DDMFormInstance ddmFormInstance, long formInstanceRecordId, ActionRequest actionRequest) throws PortalException, IOException {
		List<DataConsumer> dataConsumers = dataConsumerLocalService.getDataConsumersByDDMFormInstanceId(ddmFormInstance.getFormInstanceId());
		String jsonResponse = StringPool.BLANK;

		for (DataConsumer dataconsumer : dataConsumers) {
			jsonResponse = callDataConsumer(dataconsumer, formInstanceRecordId, actionRequest, jsonResponse, ddmFormInstance.getStructure());
		}
	}

	public void executeDataConsumersFromFormValues(long formInstanceId, long formInstanceRecordId, Locale locale, DDMFormValues ddmFormValues) throws PortalException, IOException {
		List<DataConsumer> dataConsumers = dataConsumerLocalService.getDataConsumersByDDMFormInstanceId(formInstanceId);
		String jsonResponse = StringPool.BLANK;

		for (DataConsumer dataconsumer : dataConsumers) {
			jsonResponse = callDataConsumer(dataconsumer, formInstanceRecordId, ddmFormValues, locale, jsonResponse);
		}
	}

	public boolean hasDataConsumers(long formInstanceId) {
		return !dataConsumerLocalService.getDataConsumersByDDMFormInstanceId(formInstanceId).isEmpty();
	}

	private String addClassPKAndClassNameIdToBody(String body, long formInstanceRecordId) {
		try {
			JSONObject jsonBody = json.createJSONObject(body);
			jsonBody.put("classPK", formInstanceRecordId);
			jsonBody.put("classNameId", String.valueOf(portal.getClassNameId(DDMFormInstanceRecord.class)));

			return jsonBody.toJSONString();
		} catch (JSONException e) {
			// Fail silently
			LOG.debug("Unable to add classPK and ClassNameId to the jsonBody.");
		}

		return body;
	}

	private String callDataConsumer(DataConsumer dataconsumer, long formInstanceRecordId, ActionRequest actionRequest, String jsonResponse, DDMStructure ddmStructure)
			throws PortalException, IOException {
		DDMFormValues dataProviderFields = dataConsumerUtils.getDDMFormValues(dataconsumer);
		Map<String, String> confirmedMapping = dataConsumerUtils.getInputParametersMapping(dataconsumer, actionRequest.getLocale(), dataProviderFields);

		Map<String, String> dataProviderFieldsAndValues = dataconsumer.getChain() == 0 ? dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, actionRequest, ddmStructure)
				: dataConsumerUtils.mapDataProviderFieldsToResponseValues(confirmedMapping, jsonResponse, actionRequest);

		String body = dataConsumerUtils.replaceBodyPlaceholdersWithDataProviderFieldValues(dataProviderFieldsAndValues, dataconsumer.getBody());

		return executeCall(dataconsumer, dataProviderFields, dataProviderFieldsAndValues, formInstanceRecordId, body, actionRequest.getLocale());
	}

	private String callDataConsumer(DataConsumer dataconsumer, long formInstanceRecordId, DDMFormValues ddmFormValues, Locale locale, String jsonResponse) throws PortalException, IOException {
		DDMFormValues dataProviderFields = dataConsumerUtils.getDDMFormValues(dataconsumer);
		Map<String, String> confirmedMapping = dataConsumerUtils.getInputParametersMapping(dataconsumer, locale, dataProviderFields);

		Map<String, String> dataProviderFieldsAndValues = dataconsumer.getChain() == 0 ? dataConsumerUtils.mapDataProviderFieldsToFormValues(confirmedMapping, locale, ddmFormValues)
				: dataConsumerUtils.mapDataProviderFieldsToResponseValues(confirmedMapping, jsonResponse, locale, ddmFormValues);

		String body = dataConsumerUtils.replaceBodyPlaceholdersWithDataProviderFieldValues(dataProviderFieldsAndValues, dataconsumer.getBody());

		return executeCall(dataconsumer, dataProviderFields, dataProviderFieldsAndValues, formInstanceRecordId, body, locale);
	}

	private String executeCall(DataConsumer dataconsumer, DDMFormValues dataProviderFields, Map<String, String> dataProviderFieldsAndValues, long formInstanceRecordId, String body, Locale locale)
			throws PortalException, IOException {

		String urlString = dataProviderFields.getDDMFormFieldValuesMap().get("url").get(0).getValue().getString(locale);
		Map<String, String> headers = dataConsumerUtils.getHeaders(dataconsumer, locale, dataProviderFields);

		if (dataconsumerWebClientUtils.isHttpMethodRequiresBody(dataconsumer.getMethod())) {
			body = addClassPKAndClassNameIdToBody(body, formInstanceRecordId);
		} else {
			dataProviderFieldsAndValues.put("classPK", String.valueOf(formInstanceRecordId));
			dataProviderFieldsAndValues.put("classNameId", String.valueOf(portal.getClassNameId(DDMFormInstanceRecord.class)));
		}

		return dataconsumerWebClient.execute(urlString, dataconsumer.getMethod(), headers, dataProviderFieldsAndValues, body);

	}

}
