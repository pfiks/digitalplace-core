package com.placecube.digitalplace.authentication.twofactor.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Optional;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.security.SecureRandom;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.internal.encryption.CipherService;
import com.placecube.digitalplace.authentication.twofactor.internal.encryption.ModelFactoryBuilder;
import com.placecube.digitalplace.authentication.twofactor.internal.encryption.TOTPService;
import com.placecube.digitalplace.authentication.twofactor.service.persistence.UserTwoFactorAuthenticationPK;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Base64.class, Cipher.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class TwoFactorUtilsTest extends PowerMockito {

	private static final String BASE64_ENCRYPTED_SECRET_KEY = "base64EncryptedSecretKey";
	private static final String ENCRYPTED_SECRET_KEY = "encryptedSecretKey";
	private static final String SECRET_KEY = "secretKey";
	private static final String UUID = "237e8c17-6f94-56c5-f15a-fa46ed5f53f3";

	@Mock
	private AuthenticationTwoFactorCompanyConfiguration mockAuthenticationTwoFactorCompanyConfiguration;

	@Mock
	private Base32 mockBase32;

	@Mock
	private Cipher mockCipher;

	@Mock
	private CipherService mockCipherService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private ModelFactoryBuilder mockModelFactoryBuilder;

	@Mock
	private SecureRandom mockSecureRandom;

	@Mock
	private TOTPService mockTOTPService;

	@Mock
	private User mockUser;

	@InjectMocks
	private TwoFactorUtils twoFactorUtils;

	@Before
	public void activateSetup() {
		mockStatic(Base64.class, Cipher.class);

		mockCipher = PowerMockito.mock(Cipher.class);
	}

	@Test
	public void decrypt_WhenBadPaddingExceptionOnDoFinal_ThenReturnsEmptyOptional() throws Exception {
		when(mockCipherService.getCipher(Cipher.DECRYPT_MODE, UUID)).thenReturn(Optional.of(mockCipher));
		PowerMockito.doReturn(BASE64_ENCRYPTED_SECRET_KEY.getBytes()).when(Base64.class, "decodeBase64", ENCRYPTED_SECRET_KEY);
		when(mockCipher.doFinal(BASE64_ENCRYPTED_SECRET_KEY.getBytes())).thenThrow(new BadPaddingException());

		Optional<String> result = twoFactorUtils.decrypt(ENCRYPTED_SECRET_KEY, UUID);

		assertFalse(result.isPresent());
	}

	@Test
	public void decrypt_WhenIllegalBlockSizeExceptionOnDoFinal_ThenReturnsEmptyOptional() throws Exception {
		when(mockCipherService.getCipher(Cipher.DECRYPT_MODE, UUID)).thenReturn(Optional.of(mockCipher));
		PowerMockito.doReturn(BASE64_ENCRYPTED_SECRET_KEY.getBytes()).when(Base64.class, "decodeBase64", ENCRYPTED_SECRET_KEY);
		when(mockCipher.doFinal(BASE64_ENCRYPTED_SECRET_KEY.getBytes())).thenThrow(new IllegalBlockSizeException());

		Optional<String> result = twoFactorUtils.decrypt(ENCRYPTED_SECRET_KEY, UUID);

		assertFalse(result.isPresent());
	}

	@Test
	public void decrypt_WhenNoError_ThenReturnsUnencryptedSecretKey() throws Exception {
		when(mockCipherService.getCipher(Cipher.DECRYPT_MODE, UUID)).thenReturn(Optional.of(mockCipher));
		PowerMockito.doReturn(BASE64_ENCRYPTED_SECRET_KEY.getBytes()).when(Base64.class, "decodeBase64", ENCRYPTED_SECRET_KEY);
		when(mockCipher.doFinal(BASE64_ENCRYPTED_SECRET_KEY.getBytes())).thenReturn(SECRET_KEY.getBytes());
		when(mockModelFactoryBuilder.bytesToString(SECRET_KEY.getBytes())).thenReturn(SECRET_KEY);

		Optional<String> result = twoFactorUtils.decrypt(ENCRYPTED_SECRET_KEY, UUID);

		assertThat(result.get(), equalTo(SECRET_KEY));
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters({ "null, uuid", "encrypted, null", "null, null" })
	public void decrypt_WithWrongArguments_ThenThrowsIllegalArgumentException(String encrypted, String uuid) throws Exception {
		twoFactorUtils.decrypt(encrypted, uuid);
	}

	@Test
	public void encrypt_WhenBadPaddingExceptionOnDoFinal_ThenReturnsEmptyOptional() throws Exception {
		when(mockCipherService.getCipher(Cipher.ENCRYPT_MODE, UUID)).thenReturn(Optional.of(mockCipher));
		when(mockCipher.doFinal(SECRET_KEY.getBytes())).thenThrow(new BadPaddingException());

		Optional<String> result = twoFactorUtils.encrypt(SECRET_KEY, UUID);

		assertFalse(result.isPresent());
	}

	@Test
	public void encrypt_WhenIllegalBlockSizeExceptionOnDoFinal_ThenReturnsEmptyOptional() throws Exception {
		when(mockCipherService.getCipher(Cipher.ENCRYPT_MODE, UUID)).thenReturn(Optional.of(mockCipher));
		when(mockCipher.doFinal(SECRET_KEY.getBytes())).thenThrow(new IllegalBlockSizeException());

		Optional<String> result = twoFactorUtils.encrypt(SECRET_KEY, UUID);

		assertFalse(result.isPresent());
	}

	@Test
	public void encrypt_WhenNoError_ThenReturnsEncryptedSecretKey() throws Exception {
		when(mockCipherService.getCipher(Cipher.ENCRYPT_MODE, UUID)).thenReturn(Optional.of(mockCipher));
		when(mockCipher.doFinal(SECRET_KEY.getBytes())).thenReturn(ENCRYPTED_SECRET_KEY.getBytes());
		PowerMockito.doReturn(BASE64_ENCRYPTED_SECRET_KEY).when(Base64.class, "encodeBase64String", ENCRYPTED_SECRET_KEY.getBytes());

		Optional<String> result = twoFactorUtils.encrypt(SECRET_KEY, UUID);

		assertTrue(result.isPresent());
		assertThat(result.get(), equalTo(BASE64_ENCRYPTED_SECRET_KEY));
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters({ "null, uuid", "value, null", "null, null" })
	public void encrypt_WithWrongArgument_ThenThrowsIllegalArgumentException(String value, String uuid) throws Exception {
		twoFactorUtils.encrypt(value, uuid);
	}

	@Test
	public void generateRandomSecretKey_WhenNoError_ThenReturnsRandomSecretKey() {
		String securityKey = "randomSecurityKey";
		byte[] bytes = new byte[20];
		when(mockModelFactoryBuilder.getSecureRandomInstance()).thenReturn(mockSecureRandom);
		when(mockModelFactoryBuilder.instantiateBytes()).thenReturn(bytes);
		when(mockModelFactoryBuilder.getBase32Instance()).thenReturn(mockBase32);
		when(mockBase32.encodeToString(bytes)).thenReturn(securityKey);

		String result = twoFactorUtils.generateRandomSecretKey();

		InOrder inOrder = inOrder(mockSecureRandom, mockBase32);
		inOrder.verify(mockSecureRandom, times(1)).nextBytes(bytes);
		inOrder.verify(mockBase32, times(1)).encodeToString(bytes);
		assertThat(result, equalTo(securityKey));
	}

	@Test(expected = PortalException.class)
	public void getConfiguration_WhenExceptionRetrievingTheConfiguration_ThenThrowsPortalException() throws PortalException {
		long companyId = 123;
		when(mockConfigurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, companyId)).thenThrow(new ConfigurationException());

		twoFactorUtils.getConfiguration(companyId);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsTheConfiguration() throws PortalException {
		long companyId = 123;
		when(mockConfigurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, companyId)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);

		AuthenticationTwoFactorCompanyConfiguration result = twoFactorUtils.getConfiguration(companyId);

		assertThat(result, sameInstance(mockAuthenticationTwoFactorCompanyConfiguration));
	}

	@Test
	public void getOTPUrl_WhenNoError_ThenReturnsTheUrl() {
		String issuer = "issuerValue";
		String qrUrl = "qrUrlValue";
		String algorithm = "algorithmVal";
		Integer duration = 11;
		Integer length = 22;
		String emailAddress = "emailAddressVal";
		String secretKey = "secretKeyVal";
		String expected = "expectedVal";
		when(mockAuthenticationTwoFactorCompanyConfiguration.qrCodeIssuer()).thenReturn(issuer);
		when(mockAuthenticationTwoFactorCompanyConfiguration.qrCodeUrl()).thenReturn(qrUrl);
		when(mockAuthenticationTwoFactorCompanyConfiguration.algorithmHmac()).thenReturn(algorithm);
		when(mockAuthenticationTwoFactorCompanyConfiguration.authenticatorCodeDuration()).thenReturn(duration);
		when(mockAuthenticationTwoFactorCompanyConfiguration.authenticatorCodeLength()).thenReturn(length);
		when(mockTOTPService.getOTPUrl(qrUrl, issuer, emailAddress, secretKey, algorithm, length, duration)).thenReturn(Optional.of(expected));

		String result = twoFactorUtils.getOTPUrl(mockAuthenticationTwoFactorCompanyConfiguration, emailAddress, secretKey);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getOTPUrl_WhenNoOTPUrlFound_ThenReturnsEmptyString() {
		String issuer = "issuerValue";
		String qrUrl = "qrUrlValue";
		String algorithm = "algorithmVal";
		Integer duration = 11;
		Integer length = 22;
		String emailAddress = "emailAddressVal";
		String secretKey = "secretKeyVal";
		when(mockAuthenticationTwoFactorCompanyConfiguration.qrCodeIssuer()).thenReturn(issuer);
		when(mockAuthenticationTwoFactorCompanyConfiguration.qrCodeUrl()).thenReturn(qrUrl);
		when(mockAuthenticationTwoFactorCompanyConfiguration.algorithmHmac()).thenReturn(algorithm);
		when(mockAuthenticationTwoFactorCompanyConfiguration.authenticatorCodeDuration()).thenReturn(duration);
		when(mockAuthenticationTwoFactorCompanyConfiguration.authenticatorCodeLength()).thenReturn(length);
		when(mockTOTPService.getOTPUrl(qrUrl, issuer, emailAddress, secretKey, algorithm, length, duration)).thenReturn(Optional.empty());

		String result = twoFactorUtils.getOTPUrl(mockAuthenticationTwoFactorCompanyConfiguration, emailAddress, secretKey);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getPrimaryKey_WithCompanyIdAndUserIdParameters_WhenNoError_ThenReturnsThePrimaryKey() {
		long companyId = 1;
		long userId = 2;

		UserTwoFactorAuthenticationPK result = twoFactorUtils.getPrimaryKey(companyId, userId);

		assertThat(result.getCompanyId(), equalTo(companyId));
		assertThat(result.getUserId(), equalTo(userId));
	}

	@Test
	public void getPrimaryKey_WithUserarameter_WhenNoError_ThenReturnsThePrimaryKey() {
		long companyId = 1;
		long userId = 2;
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getCompanyId()).thenReturn(companyId);

		UserTwoFactorAuthenticationPK result = twoFactorUtils.getPrimaryKey(mockUser);

		assertThat(result.getCompanyId(), equalTo(companyId));
		assertThat(result.getUserId(), equalTo(userId));
	}

	@Test
	@Parameters({ "true", "false" })
	public void verifyUserOneTimeCode_WhenNoError_ThenReturnsTheResultOfTheTimeCodeVerification(boolean expected) {

		String oneTimeCode = "oneTimeCodeVal";
		String secretKey = "secretKeyVal";

		when(mockTOTPService.verifyUserOneTimeCode(secretKey, oneTimeCode)).thenReturn(expected);

		boolean result = twoFactorUtils.verifyUserOneTimeCode(secretKey, oneTimeCode);

		assertThat(result, equalTo(expected));
	}
}
