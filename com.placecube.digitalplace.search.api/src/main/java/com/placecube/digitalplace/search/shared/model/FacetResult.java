package com.placecube.digitalplace.search.shared.model;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;

import com.liferay.petra.string.StringPool;

public class FacetResult {

	private final int frequency;
	private final Map<Locale, String> labelMap;
	private boolean selected;
	private String value;

	public static FacetResult init(String value, int frequency, boolean selected) {
		return new FacetResult(value, frequency, selected, Collections.singletonMap(Locale.getDefault(), value));
	}

	public static FacetResult init(String value, Integer frequency, boolean selected, Map<Locale, String> labelMap) {
		return new FacetResult(value, frequency, selected, labelMap);
	}

	private FacetResult(String value, int frequency, boolean selected, Map<Locale, String> labelMap) {
		this.value = value;
		this.frequency = frequency;
		this.selected = selected;
		this.labelMap = labelMap;
	}

	public void appendValue(String value) {
		this.value += StringPool.COMMA + value;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getFrequency() {
		return frequency;
	}

	public String getLabel(Locale locale) {
		return labelMap.getOrDefault(locale, labelMap.values().iterator().next());
	}

	public String getValue() {
		return value;
	}

	public boolean isSelected() {
		return selected;
	}

}
