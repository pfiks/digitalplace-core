package com.placecube.digitalplace.search.web.service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.web.portlet.contenttype.service.ContentTypeSearchService;

@Component(immediate = true, service = FacetParsingService.class)
public class FacetParsingService {

	private static final Log LOG = LogFactoryUtil.getLog(FacetParsingService.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private ContentTypeSearchService contentTypeSearchService;

	public List<FacetResult> parseAllCategories(Map<String, Integer> facetValues, Set<Long> categories, Set<String> selectedFacets) {
		List<FacetResult> results = new LinkedList<>();

		for (Long categoryId : categories) {
			try {
				AssetCategory assetCategory = assetCategoryLocalService.getAssetCategory(categoryId);

				String categoryIdString = String.valueOf(categoryId);

				Integer facetValue = facetValues.get(categoryIdString);

				results.add(FacetResult.init(categoryIdString, Objects.nonNull(facetValue) ? facetValue : 0, selectedFacets.contains(categoryIdString), assetCategory.getTitleMap()));
			} catch (PortalException e) {
				LOG.warn("Unable to retrieve category - " + e.getMessage());
			}
		}

		return results;
	}

	public List<FacetResult> parseCategoryFacets(Map<String, Integer> facetValues, Set<String> selectedFacets) {
		List<FacetResult> results = new LinkedList<>();

		for (Entry<String, Integer> facet : facetValues.entrySet()) {
			try {
				String categoryId = facet.getKey();
				AssetCategory assetCategory = assetCategoryLocalService.getAssetCategory(GetterUtil.getLong(categoryId));
				results.add(FacetResult.init(categoryId, facet.getValue(), selectedFacets.contains(categoryId), assetCategory.getTitleMap()));
			} catch (PortalException e) {
				LOG.warn("Unable to retrieve category from facet - " + e.getMessage());
			}
		}

		return results;
	}

	public List<FacetResult> parseContentTypes(Map<String, Integer> facetValues, Set<String> selectedFacets, Locale locale, boolean joinFiltersWithTheSameName) {
		List<FacetResult> results;
		if (joinFiltersWithTheSameName) {
			results = parseContentTypesGrouped(facetValues, selectedFacets, locale);
		} else {
			results = parseContentTypes(facetValues, selectedFacets, locale);
		}
		results.sort(Comparator.comparing(facetResult -> facetResult.getLabel(locale)));
		return results;
	}

	public List<FacetResult> parseFacets(Map<String, Integer> facetValues, Set<String> selectedFacets) {
		List<FacetResult> results = new LinkedList<>();

		for (Entry<String, Integer> facet : facetValues.entrySet()) {
			String facetValue = facet.getKey();
			results.add(FacetResult.init(facetValue, facet.getValue(), selectedFacets.contains(facetValue)));
		}

		return results;
	}

	private List<FacetResult> parseContentTypes(Map<String, Integer> facetValues, Set<String> selectedFacets, Locale locale) {
		List<FacetResult> results = new LinkedList<>();

		for (Entry<String, Integer> facet : facetValues.entrySet()) {
			String contentTypeValue = facet.getKey();

			Map<Locale, String> labelMap = contentTypeSearchService.getLabelMapForContentType(locale, contentTypeValue);

			FacetResult init = FacetResult.init(contentTypeValue, facet.getValue(), selectedFacets.contains(contentTypeValue), labelMap);
			results.add(init);
		}

		return results;
	}

	private List<FacetResult> parseContentTypesGrouped(Map<String, Integer> facetValues, Set<String> selectedFacets, Locale locale) {
		List<FacetResult> results = new LinkedList<>();

		Map<String, FacetResult> nameToFacetResultMap = new HashMap<>();

		for (Entry<String, Integer> facet : facetValues.entrySet()) {
			String contentTypeValue = facet.getKey();

			Map<Locale, String> labelMap = contentTypeSearchService.getLabelMapForContentType(locale, contentTypeValue);
			String currentLabel = labelMap.get(locale);
			if (nameToFacetResultMap.containsKey(currentLabel)) {
				FacetResult facetResult = nameToFacetResultMap.get(currentLabel);
				facetResult.appendValue(contentTypeValue);
			} else {
				FacetResult facetResult = FacetResult.init(contentTypeValue, facet.getValue(), false, labelMap);
				results.add(facetResult);
				nameToFacetResultMap.put(currentLabel, facetResult);
			}
		}

		results.forEach(facetResult -> facetResult.setSelected(selectedFacets.contains(facetResult.getValue())));

		return results;
	}
}
