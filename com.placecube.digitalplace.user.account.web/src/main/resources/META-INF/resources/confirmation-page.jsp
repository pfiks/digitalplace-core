<%@ include file="init.jsp"%>

<div class="row">
	<div class="col-md-12 ">
	
		<c:choose>		
			<c:when test="${themeDisplay.isSignedIn()}">
				<liferay-journal:journal-article 
				articleId="<%= WebContentArticles.USER_ACCOUNT_CONFIRMATION_CREATED_BY_ADMIN.getArticleId() %>" 
				groupId="${themeDisplay.companyGroupId}" 
				showTitle="false" />	
				<c:if test="${not empty userEmailAddressToSendResetPassword}">
					<portlet:actionURL name="<%=MVCCommandKeys.PASSWORD_RESET%>" var="passwordResetURL" >
						<portlet:param name="<%= RequestKeys.USER_EMAIL_ADDRESS_TO_SEND_RESET_PASSWORD %>" value="${userEmailAddressToSendResetPassword}"/>
						<portlet:param name="<%= RequestKeys.MVC_RENDER_COMMAND_NAME %>" value="<%= MVCCommandKeys.CONFIRMATION_PASSWORD_RESET_PAGE %>"/>
					</portlet:actionURL>		
					
					<div class="col-md-12 text-center">
						<a href="${passwordResetURL}">
							<aui:button value="send-password-reset-link" type="submit"/>
						</a>
					</div>
				</c:if>
			</c:when>
			<c:when test="${emailVerificationEnabled}">
				<liferay-journal:journal-article 
				articleId="<%= WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITH_EMAIL_VERIFICATION.getArticleId() %>" 
				groupId="${themeDisplay.companyGroupId}" 
				showTitle="false" />				
			</c:when>		
			<c:otherwise>
				<liferay-journal:journal-article 
				articleId="<%= WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITHOUT_EMAIL_VERIFICATION.getArticleId() %>" 
				groupId="${themeDisplay.companyGroupId}" 
				showTitle="false" />				
			</c:otherwise>
		</c:choose>

		<c:if test="${accountCreatedWithErrors}">
			<div class="alert alert-warning mt-5">
				<liferay-ui:message key="account-created-with-errors" />
			</div>	
		</c:if>

		<c:if test="${ not empty redirectUrl }">
			<portlet:renderURL var="closePopUp">
				<portlet:param name="<%=RequestKeys.MVC_RENDER_COMMAND_NAME%>" value="<%=MVCCommandKeys.CLOSE_POPUP%>"/>
				<portlet:param name="<%=RequestKeys.REDIRECT_URL%>" value="${redirectUrl}"/>
				<portlet:param name="<%=AccountConnectorConstant.MODEL_ID%>" value="${modelId}"/>
			</portlet:renderURL>

			<aui:button-row cssClass="text-center">
				<a href="${closePopUp}">
					<aui:button cssClass="btn-primary" value="close"/>
				</a>
			</aui:button-row>
		</c:if>
	</div>
</div>
