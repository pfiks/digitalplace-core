package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = SiteInitialiserSetupContentService.class)
public class SiteInitialiserSetupContentService {

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private SiteInitialiserSetupFragmentsService siteInitialiserSetupFragmentsService;

	public void configureAssetListEntryUsage(long userId, long groupId, JSONArray jsonArray) throws PortalException {
		if (Validator.isNotNull(jsonArray)) {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				String layoutUrl = jsonObject.getString("layoutUrl");
				String assetListEntryUsageKey = jsonObject.getString("assetListEntryUsageKey");
				String targetCollectionPlaceholderValue = jsonObject.getString("targetCollectionPlaceholderValue");
				boolean privateLayout = jsonObject.getBoolean("privateLayout");

				Layout layout = layoutLocalService.getLayoutByFriendlyURL(groupId, privateLayout, layoutUrl);
				siteInitialiserSetupFragmentsService.updateFragmentEntryLinksInLayout(userId, layout, assetListEntryUsageKey, targetCollectionPlaceholderValue);
			}
		}
	}

	public String getFileContentWithDefaultReplacements(Optional<URL> urlToFile, Group group) throws IOException {
		String fileContent = StringUtil.read(urlToFile.get().openStream());
		return StringUtil.replace(fileContent, new String[] { "[$COMPANY_ID$]" }, new String[] { String.valueOf(group.getCompanyId()) });
	}

}
