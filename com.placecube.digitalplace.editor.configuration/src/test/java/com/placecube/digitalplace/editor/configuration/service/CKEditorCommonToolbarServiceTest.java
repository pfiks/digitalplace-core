package com.placecube.digitalplace.editor.configuration.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.editor.configuration.CKEditorConfigurationService;
import com.placecube.digitalplace.editor.configuration.constants.CKEditorCommonToolbarConstants;

@RunWith(PowerMockRunner.class)
public class CKEditorCommonToolbarServiceTest extends Mockito {

	@Mock
	private JSONArray mockAdditionalPluginsBar;

	@Mock
	private JSONArray mockAudioBar;

	@Mock
	private CKEditorCommonToolbarHelper mockCKEditorCommonToolbarHelper;

	@Mock
	private CKEditorConfigurationService mockCKEditorConfigurationService;

	@Mock
	private JSONArray mockHelpBar;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONArray mockLinkBar;

	@Mock
	private JSONArray mockNumberListBar;

	@Mock
	private JSONArray mockPasteBar;

	@Mock
	private JSONObject mockPluginAndRolesConfig;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private JSONArray mockSourceBar;

	@Mock
	private JSONArray mockTableBar;

	@Mock
	private JSONArray mockTextFormatBar;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private JSONArray mockToolbar;

	@Mock
	private JSONArray mockUndoRedoBar;

	@Mock
	private User mockUser;

	@Mock
	private Role mockUserRole1;

	@Mock
	private Role mockUserRole2;

	@Mock
	private Role mockUserRole3;

	@Mock
	private Role mockUserRole4;

	@InjectMocks
	CKEditorCommonToolbarService ckEditorCommonToolbarService;

	@Test
	public void buildToobar_WhenNoError_ThenBuildToolbar() {
		long companyId = 1L;
		long userId = 10L;
		long scopeGroupId = 20L;
		Optional<JSONObject> optMockPluginAndRolesConfig = mockGetPluginAndRolesJsonConfiguration(companyId);

		Set<String> roleList = mockGetUserRoleName(userId, scopeGroupId);

		mockToolBar(optMockPluginAndRolesConfig, roleList);

		JSONArray result = ckEditorCommonToolbarService.buildToolbar(mockThemeDisplay);

		InOrder inOrder = inOrder(mockThemeDisplay, mockCKEditorConfigurationService, mockUser, mockUserRole1, mockUserRole2, mockRoleLocalService, mockUserRole3, mockUserRole4,
				mockCKEditorCommonToolbarHelper, mockJSONFactory, mockToolbar);

		inOrder.verify(mockThemeDisplay, times(1)).getCompanyId();
		inOrder.verify(mockCKEditorConfigurationService, times(1)).getPluginAndRolesJsonConfiguration(companyId);

		inOrder.verify(mockThemeDisplay, times(1)).getUser();
		inOrder.verify(mockUser, times(1)).getRoles();
		inOrder.verify(mockUserRole1, times(1)).getName();
		inOrder.verify(mockUserRole2, times(1)).getName();
		inOrder.verify(mockThemeDisplay, times(1)).getUserId();
		inOrder.verify(mockThemeDisplay, times(1)).getScopeGroupId();
		inOrder.verify(mockRoleLocalService, times(1)).getUserGroupRoles(userId, scopeGroupId);
		inOrder.verify(mockUserRole3, times(1)).getName();
		inOrder.verify(mockUserRole4, times(1)).getName();

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.PASTE_TEXT_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.PASTE_FROM_WORD_PLUGIN_NAME);

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.BOLD_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.ITALIC_PLUGIN_NAME, CKEditorCommonToolbarConstants.UNDERLINE_PLUGIN_NAME, CKEditorCommonToolbarConstants.STRIKETHROUGH_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.BLOCKQUOTE_PLUGIN_NAME);

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.NUMBERED_LIST_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.BULLETED_LIST_PLUGIN_NAME);

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.TABLE_PLUGIN_NAME);

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.LINK_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.UNLINK_PLUGIN_NAME, CKEditorCommonToolbarConstants.ANCHOR, CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME, CKEditorCommonToolbarConstants.IMAGE_SELECTOR_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.VIDEO_SELECTOR_PLUGIN_NAME);

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.AUDIO_SELECTOR_PLUGIN_NAME);

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.SOURCE_PLUGIN_NAME);

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.GDS_CONFIRMATION_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_DEFAULT_BUTTON_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_DETAILS_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_DISABLED_BUTTON_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_NOTIFICATION_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_SECONDARY_BUTTON_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_START_BUTTON_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_WARNING_BUTTON_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_WARNING_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.MAXIMIZE_PLUGIN_NAME, CKEditorCommonToolbarConstants.SCAYT_PLUGIN_NAME);

		inOrder.verify(mockCKEditorCommonToolbarHelper, times(1)).createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList, CKEditorCommonToolbarConstants.HELP_DIALOG);

		inOrder.verify(mockJSONFactory, times(1)).createJSONArray();
		inOrder.verify(mockToolbar, times(1)).put(mockPasteBar);
		inOrder.verify(mockToolbar, times(1)).put(mockUndoRedoBar);
		inOrder.verify(mockToolbar, times(1)).put(mockTextFormatBar);
		inOrder.verify(mockToolbar, times(1)).put(mockNumberListBar);
		inOrder.verify(mockToolbar, times(1)).put(mockTableBar);
		inOrder.verify(mockToolbar, times(1)).put(mockLinkBar);
		inOrder.verify(mockToolbar, times(1)).put(mockAudioBar);
		inOrder.verify(mockToolbar, times(1)).put(mockSourceBar);
		inOrder.verify(mockToolbar, times(1)).put(mockAdditionalPluginsBar);
		inOrder.verify(mockToolbar, times(1)).put(mockHelpBar);

		assertThat(result, equalTo(mockToolbar));

	}

	@Test
	public void getUserRoleName_WhenThemeDisplayIsPresent_ThenRoleNameIsAdded() {
		long userId = 10L;
		long scopeGroupId = 20L;
		Set<String> roleList = mockGetUserRoleName(userId, scopeGroupId);

		Set<String> result = ckEditorCommonToolbarService.getUserRoleName(mockThemeDisplay);

		InOrder inOrder = inOrder(mockThemeDisplay, mockUser, mockUserRole1, mockUserRole2, mockRoleLocalService, mockUserRole3, mockUserRole4);

		inOrder.verify(mockThemeDisplay, times(1)).getUser();
		inOrder.verify(mockUser, times(1)).getRoles();
		inOrder.verify(mockUserRole1, times(1)).getName();
		inOrder.verify(mockUserRole2, times(1)).getName();
		inOrder.verify(mockThemeDisplay, times(1)).getUserId();
		inOrder.verify(mockThemeDisplay, times(1)).getScopeGroupId();
		inOrder.verify(mockRoleLocalService, times(1)).getUserGroupRoles(userId, scopeGroupId);
		inOrder.verify(mockUserRole3, times(1)).getName();
		inOrder.verify(mockUserRole4, times(1)).getName();

		assertThat(result, equalTo(roleList));
	}

	private Optional<JSONObject>  mockGetPluginAndRolesJsonConfiguration(long companyId) {
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockCKEditorConfigurationService.getPluginAndRolesJsonConfiguration(companyId)).thenReturn(Optional.of(mockPluginAndRolesConfig));

		return Optional.ofNullable(mockPluginAndRolesConfig);
	}

	private Set<String> mockGetUserRoleName(long userId, long scopeGroupId) {
		String roleName1 = "roleName1";
		String roleName2 = "roleName2";
		String roleName3 = "roleName3";
		String roleName4 = "roleName4";

		List<Role> roles = Arrays.asList(mockUserRole1, mockUserRole2);
		List<Role> roles2 = Arrays.asList(mockUserRole3, mockUserRole4);
		Set<String> roleList = new HashSet<>();
		roleList.add(roleName1);
		roleList.add(roleName2);
		roleList.add(roleName3);
		roleList.add(roleName4);

		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockUser.getRoles()).thenReturn(roles);
		when(mockUserRole1.getName()).thenReturn(roleName1);
		when(mockUserRole2.getName()).thenReturn(roleName2);

		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockRoleLocalService.getUserGroupRoles(userId, scopeGroupId)).thenReturn(roles2);
		when(mockUserRole3.getName()).thenReturn(roleName3);
		when(mockUserRole4.getName()).thenReturn(roleName4);

		return roleList;
	}

	private void mockToolBar(Optional<JSONObject> optMockPluginAndRolesConfig, Set<String> roleList) {
		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.PASTE_TEXT_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.PASTE_FROM_WORD_PLUGIN_NAME)
				).thenReturn(mockPasteBar);

		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME)
				).thenReturn(mockUndoRedoBar);

		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.BOLD_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.ITALIC_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.UNDERLINE_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.STRIKETHROUGH_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.BLOCKQUOTE_PLUGIN_NAME)
				).thenReturn(mockTextFormatBar);

		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.NUMBERED_LIST_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.BULLETED_LIST_PLUGIN_NAME)
				).thenReturn(mockNumberListBar);

		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.TABLE_PLUGIN_NAME)
				).thenReturn(mockTableBar);

		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.LINK_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.UNLINK_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.ANCHOR,
				CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.IMAGE_SELECTOR_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.VIDEO_SELECTOR_PLUGIN_NAME)
				).thenReturn(mockLinkBar);

		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.AUDIO_SELECTOR_PLUGIN_NAME)
				).thenReturn(mockAudioBar);

		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.SOURCE_PLUGIN_NAME)
				).thenReturn(mockSourceBar);

		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.GDS_CONFIRMATION_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_DEFAULT_BUTTON_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_DETAILS_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_DISABLED_BUTTON_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_NOTIFICATION_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_SECONDARY_BUTTON_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_START_BUTTON_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_WARNING_BUTTON_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_WARNING_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.MAXIMIZE_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.SCAYT_PLUGIN_NAME)
				).thenReturn(mockAdditionalPluginsBar);

		when(mockCKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optMockPluginAndRolesConfig, roleList,
				CKEditorCommonToolbarConstants.HELP_DIALOG)
				).thenReturn(mockHelpBar);

		when(mockJSONFactory.createJSONArray()).thenReturn(mockToolbar);

		when(mockToolbar.put(mockUndoRedoBar)).thenReturn(mockToolbar);
		when(mockToolbar.put(mockTextFormatBar)).thenReturn(mockToolbar);
		when(mockToolbar.put(mockNumberListBar)).thenReturn(mockToolbar);
		when(mockToolbar.put(mockTableBar)).thenReturn(mockToolbar);
		when(mockToolbar.put(mockLinkBar)).thenReturn(mockToolbar);
		when(mockToolbar.put(mockAudioBar)).thenReturn(mockToolbar);
		when(mockToolbar.put(mockSourceBar)).thenReturn(mockToolbar);
		when(mockToolbar.put(mockAdditionalPluginsBar)).thenReturn(mockToolbar);
		when(mockToolbar.put(mockHelpBar)).thenReturn(mockToolbar);
	}

}
