package com.placecube.digitalplace.audit.listener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.security.audit.event.generators.constants.EventTypes;
import com.placecube.digitalplace.audit.service.AuditService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AuditGroupListenerTest extends PowerMockito {

	@InjectMocks
	private AuditGroupListener auditGroupListener;

	@Mock
	private Map<String, String> mockAdditionalInformationMap;

	@Mock
	private AuditService mockAuditService;

	@Mock
	private Group mockGroup;

	@Mock
	private Group mockPreviousGroup;

	@Test
	public void onAfterCreate_WhenGroupIsNotSiteNorOrganization_ThenDoesNotAddAuditLogEntry() {
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockGroup.isSite()).thenReturn(false);

		auditGroupListener.onAfterCreate(mockGroup);

		verifyNoInteractions(mockAuditService);
	}

	@Test
	@Parameters({ "true,false", "false,true" })
	public void onAfterCreate_WhenGroupIsSiteOrOrganization_ThenAddsAuditLogEntry(boolean isSite, boolean isOrganization) {
		long groupId = 1;
		when(mockGroup.isOrganization()).thenReturn(isOrganization);
		when(mockGroup.isSite()).thenReturn(isSite);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockAuditService.getGroupAuditInfo(mockGroup)).thenReturn(mockAdditionalInformationMap);

		auditGroupListener.onAfterCreate(mockGroup);

		verify(mockAuditService, times(1)).addAuditLogEntry(Group.class.getName(), groupId, EventTypes.ADD, mockAdditionalInformationMap);
	}

	@Test
	public void onAfterRemove_WhenGroupIsNotSiteNorOrganization_ThenDoesNotAddAuditLogEntry() {
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockGroup.isSite()).thenReturn(false);

		auditGroupListener.onAfterRemove(mockGroup);

		verifyNoInteractions(mockAuditService);
	}

	@Test
	@Parameters({ "true,false", "false,true" })
	public void onAfterRemove_WhenGroupIsSiteOrOrganization_ThenAddsAuditLogEntry(boolean isSite, boolean isOrganization) {
		long groupId = 1;
		when(mockGroup.isOrganization()).thenReturn(isOrganization);
		when(mockGroup.isSite()).thenReturn(isSite);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockAuditService.getGroupAuditInfo(mockGroup)).thenReturn(mockAdditionalInformationMap);

		auditGroupListener.onAfterRemove(mockGroup);

		verify(mockAuditService, times(1)).addAuditLogEntry(Group.class.getName(), groupId, EventTypes.DELETE, mockAdditionalInformationMap);
	}

	@Test
	public void onAfterUpdate_WhenGroupIsNotSiteNorOrganization_ThenDoesNotAddAuditLogEntry() {
		when(mockGroup.isOrganization()).thenReturn(false);
		when(mockGroup.isSite()).thenReturn(false);

		auditGroupListener.onAfterUpdate(mockPreviousGroup, mockGroup);

		verifyNoInteractions(mockAuditService);
	}

	@Test
	@Parameters({ "true,false", "false,true" })
	public void onAfterUpdate_WhenGroupIsSiteOrOrganization_ThenAddsAuditLogEntry(boolean isSite, boolean isOrganization) {
		long groupId = 1;
		when(mockGroup.isOrganization()).thenReturn(isOrganization);
		when(mockGroup.isSite()).thenReturn(isSite);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockAuditService.getGroupAuditInfo(mockGroup)).thenReturn(mockAdditionalInformationMap);

		auditGroupListener.onAfterUpdate(mockPreviousGroup, mockGroup);

		verify(mockAuditService, times(1)).addAuditLogEntry(Group.class.getName(), groupId, EventTypes.UPDATE, mockAdditionalInformationMap);
	}

}
