package com.placecube.digitalplace.addresslookup.statmap.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.statmap.configuration.StatMapLocalLookupCompanyConfiguration;
import com.placecube.digitalplace.addresslookup.statmap.constants.StatMapConstants;
import com.placecube.digitalplace.addresslookup.statmap.service.utils.StatMapAddressLookupService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class StatMapLocalAddressLookupTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@Mock
	private AddressContext mockAddressLookup1;

	@Mock
	private AddressContext mockAddressLookup2;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private StatMapAddressLookupService mockStatMapAddressLookupService;

	@Mock
	private StatMapLocalLookupCompanyConfiguration mockStatMapLocalLookupCompanyConfiguration;

	@InjectMocks
	private StatMapLocalAddressLookup statMapAddressLookup;

	@Test
	public void enabled_WhenExceptionRetrievingConfiguration_ThenReturnsFalse() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(StatMapLocalLookupCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = statMapAddressLookup.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(StatMapLocalLookupCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockStatMapLocalLookupCompanyConfiguration);
		when(mockStatMapLocalLookupCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = statMapAddressLookup.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void findByPostcode_WhenException_ThenReturnsEmptySet() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(StatMapLocalLookupCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		Set<AddressContext> results = statMapAddressLookup.findByPostcode(COMPANY_ID, "myPostcode", null);

		assertTrue(results.isEmpty());
	}

	@Test
	public void findByPostcode_WhenNoError_ThenReturnsSetWithAllValidAddressContexts() throws Exception {
		String postcode = "myPostcode";
		when(mockConfigurationProvider.getCompanyConfiguration(StatMapLocalLookupCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockStatMapLocalLookupCompanyConfiguration);
		when(mockStatMapLocalLookupCompanyConfiguration.baseRecordEndpoint()).thenReturn("recordEndpointValue");
		when(mockStatMapLocalLookupCompanyConfiguration.baseSearchEndpoint()).thenReturn("searchEndpointValue");
		when(mockStatMapLocalLookupCompanyConfiguration.token()).thenReturn("tokenValue");
		Set<AddressContext> expectedResults = new LinkedHashSet<>();
		expectedResults.add(mockAddressLookup1);
		expectedResults.add(mockAddressLookup2);
		when(mockStatMapAddressLookupService.getAddressContexts("searchEndpointValue", "recordEndpointValue", "tokenValue", StatMapConstants.PARAM_SEARCH_TERM, postcode)).thenReturn(expectedResults);

		Set<AddressContext> results = statMapAddressLookup.findByPostcode(COMPANY_ID, postcode, null);

		assertThat(results, sameInstance(expectedResults));
	}

	@Test
	public void findByPUprn_WhenException_ThenReturnsEmptyOptional() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(StatMapLocalLookupCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		Optional<AddressContext> result = statMapAddressLookup.findByUprn(COMPANY_ID, "myUprn", null);

		assertFalse(result.isPresent());
	}

	@Test
	public void findByUprn_WhenNoError_ThenReturnsTheFirstAddressContextFound() throws Exception {
		String uprn = "myUprn";
		when(mockConfigurationProvider.getCompanyConfiguration(StatMapLocalLookupCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockStatMapLocalLookupCompanyConfiguration);
		when(mockStatMapLocalLookupCompanyConfiguration.baseRecordEndpoint()).thenReturn("recordEndpointValue");
		when(mockStatMapLocalLookupCompanyConfiguration.baseSearchEndpoint()).thenReturn("searchEndpointValue");
		when(mockStatMapLocalLookupCompanyConfiguration.token()).thenReturn("tokenValue");
		Set<AddressContext> expectedResults = new LinkedHashSet<>();
		expectedResults.add(mockAddressLookup1);
		expectedResults.add(mockAddressLookup2);
		when(mockStatMapAddressLookupService.getAddressContexts("searchEndpointValue", "recordEndpointValue", "tokenValue", StatMapConstants.PARAM_SEARCH_UPRN, uprn)).thenReturn(expectedResults);

		Optional<AddressContext> result = statMapAddressLookup.findByUprn(COMPANY_ID, uprn, null);

		assertThat(result.get(), sameInstance(mockAddressLookup1));
	}

	@Test
	public void getWeight_WhenExceptionRetrievingConfiguration_ThenReturnsZero() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(StatMapLocalLookupCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		int result = statMapAddressLookup.getWeight(COMPANY_ID);

		assertThat(result, equalTo(0));
	}

	@Test
	public void getWeight_WhenNoError_ThenReturnsTheWeight() throws ConfigurationException {
		Integer expected = 11;
		when(mockConfigurationProvider.getCompanyConfiguration(StatMapLocalLookupCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockStatMapLocalLookupCompanyConfiguration);
		when(mockStatMapLocalLookupCompanyConfiguration.weight()).thenReturn(expected);

		Integer result = statMapAddressLookup.getWeight(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void national_WhenNoError_ThenReturnsFalse() throws ConfigurationException {
		boolean result = statMapAddressLookup.national(COMPANY_ID);

		assertThat(result, equalTo(false));
		verifyNoInteractions(mockConfigurationProvider);
	}

}
