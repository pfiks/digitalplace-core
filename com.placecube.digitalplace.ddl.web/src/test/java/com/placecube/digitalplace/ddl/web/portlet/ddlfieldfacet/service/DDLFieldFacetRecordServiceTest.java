package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SearchContextFactory.class, BooleanClauseFactoryUtil.class })
public class DDLFieldFacetRecordServiceTest {

	private static final long DDM_STRUCTURE_ID = 3223L;
	private static final String DDM_STRUCTURE_KEY = "structureKey";
	private static final long DDL_RECORD_ID = 865L;
	private static final Locale LOCALE = Locale.CANADA;

	@InjectMocks
	private DDLFieldFacetRecordService ddlFieldFacetRecordService;

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDLFieldFacetDDMFormFieldService mockDDLFieldFacetDDMFormFieldService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private Hits mockHits;

	@Mock
	private Document mockDocument;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private BooleanClause<Query> mockBooleanClause;

	@Captor
	private ArgumentCaptor<BooleanClause<Query>[]> booleanClausesArrayCaptor;

	@Before
	public void setUp() {
		mockStatic(SearchContextFactory.class, BooleanClauseFactoryUtil.class);
	}

	@Test
	public void searchDDLRecords_WhenNoErrors_ThenReturnsDDLRecordsFoundForStructure() throws Exception {

		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);
		when(mockDDLRecordLocalService.search(mockSearchContext)).thenReturn(mockHits);
		when(mockHits.getDocs()).thenReturn(new Document[] { mockDocument });
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(DDL_RECORD_ID));
		when(mockDDLRecordLocalService.fetchDDLRecord(DDL_RECORD_ID)).thenReturn(mockDDLRecord);

		List<DDLRecord> result = ddlFieldFacetRecordService.searchDDLRecords(DDM_STRUCTURE_ID, mockHttpServletRequest);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(0), sameInstance(mockDDLRecord));
	}

	@Test
	public void searchDDLRecords_WhenNoErrors_ThenAddsStructureKeyMustBooleanClauseAndSearchesInOrder() throws Exception {
		BooleanClause<Query>[] booleanClauses = new BooleanClause[0];

		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureKey()).thenReturn(DDM_STRUCTURE_KEY);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);
		when(mockDDLRecordLocalService.search(mockSearchContext)).thenReturn(mockHits);
		when(mockHits.getDocs()).thenReturn(new Document[] { mockDocument });
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(DDL_RECORD_ID));
		when(BooleanClauseFactoryUtil.create("ddmStructureKey", DDM_STRUCTURE_KEY, BooleanClauseOccur.MUST.getName())).thenReturn(mockBooleanClause);
		when(mockSearchContext.getBooleanClauses()).thenReturn(booleanClauses);

		ddlFieldFacetRecordService.searchDDLRecords(DDM_STRUCTURE_ID, mockHttpServletRequest);

		InOrder inOrder = inOrder(mockSearchContext, mockDDLRecordLocalService);

		inOrder.verify(mockSearchContext, times(1)).setBooleanClauses(booleanClausesArrayCaptor.capture());
		inOrder.verify(mockDDLRecordLocalService, times(1)).search(mockSearchContext);

		BooleanClause<Query>[] addedBooleanClauses = booleanClausesArrayCaptor.getValue();

		assertThat(addedBooleanClauses.length, equalTo(1));
		assertThat(addedBooleanClauses[0], sameInstance(mockBooleanClause));
	}

	@Test
	public void searchDDLRecords_WhenResultDocumentDDLRecordIsNotFound_ThenDoesNotReturnDDLRecordForDocument() throws Exception {

		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);
		when(mockDDLRecordLocalService.search(mockSearchContext)).thenReturn(mockHits);
		when(mockHits.getDocs()).thenReturn(new Document[] { mockDocument });
		when(mockDocument.get(Field.ENTRY_CLASS_PK)).thenReturn(String.valueOf(DDL_RECORD_ID));
		when(mockDDLRecordLocalService.fetchDDLRecord(DDL_RECORD_ID)).thenReturn(null);

		List<DDLRecord> result = ddlFieldFacetRecordService.searchDDLRecords(DDM_STRUCTURE_ID, mockHttpServletRequest);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test(expected = PortalException.class)
	public void searchDDLRecords_WhenDDMStructureCannotBeFound_ThenThrowsPortalException() throws Exception {

		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenThrow(new PortalException());

		ddlFieldFacetRecordService.searchDDLRecords(DDM_STRUCTURE_ID, mockHttpServletRequest);
	}

	@Test
	public void searchDDLRecords_WhenNoResultDocuments_ThenReturnsEmptyDDLRecordsList() throws Exception {

		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(SearchContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockSearchContext);
		when(mockDDLRecordLocalService.search(mockSearchContext)).thenReturn(mockHits);
		when(mockHits.getDocs()).thenReturn(new Document[0]);

		List<DDLRecord> result = ddlFieldFacetRecordService.searchDDLRecords(DDM_STRUCTURE_ID, mockHttpServletRequest);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getDDLRecordFieldValue_WhenNoErrors_ThenReturnsFieldLocalizedStringValue() throws PortalException {
		final String fieldName = "fieldName";
		final String fieldValue = "fieldValue";

		when(mockDDLRecordLocalService.getDDLRecord(DDL_RECORD_ID)).thenReturn(mockDDLRecord);
		when(mockDDLFieldFacetDDMFormFieldService.getDDMFormFieldStringValue(mockDDLRecord, fieldName, LOCALE)).thenReturn(fieldValue);

		String result = ddlFieldFacetRecordService.getDDLRecordFieldValue(DDL_RECORD_ID, fieldName, LOCALE);

		assertThat(result, equalTo(fieldValue));
	}

	@Test
	public void getDDLRecordFieldValue_WhenDDLRecordDoesNotExist_ThenReturnsEmptyString() throws PortalException {
		final String fieldName = "fieldName";

		when(mockDDLRecordLocalService.getDDLRecord(DDL_RECORD_ID)).thenThrow(new PortalException());

		String result = ddlFieldFacetRecordService.getDDLRecordFieldValue(DDL_RECORD_ID, fieldName, LOCALE);

		assertThat(result, equalTo(StringPool.BLANK));
	}

}
