package com.placecube.digitalplace.user.account.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.GROUP)
@Meta.OCD(id = "com.placecube.digitalplace.user.account.configuration.AccountGroupConfiguration", localization = "content/Language", name = "site-create-account")
public interface AccountGroupConfiguration {

	@Meta.AD(required = false, deflt = "true", name = "email-address-required")
	boolean emailAddressRequired();
}