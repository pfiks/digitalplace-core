<%@ include file="/init.jsp" %>

<c:if test="${not empty configuration}">
	<c:if test="${not empty configuration.twitterUsername()}">
		<div class="row">
			<div class="col-md-12">
				<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

				<a class="twitter-timeline" data-dnt="true"
				   data-chrome="nofooter"
				   href="https://twitter.com/${configuration.twitterUsername()}"
				   data-tweet-limit="${configuration.feedsToShow()}">
				</a>
			</div>
		</div>
	</c:if>

	<c:if test="${not empty configuration.twitterEmbedded()}">
		<div class="row">
			<div class="col-md-12">
					${configuration.twitterEmbedded()}
			</div>
		</div>
	</c:if>
</c:if>



