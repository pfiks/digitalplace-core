package com.placecube.digitalplace.frontend.taglib.servlet.taglib.tag;

import javax.servlet.jsp.PageContext;

import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.frontend.taglib.servlet.taglib.util.ServletContextUtil;


public class FieldsetGroupTag extends IncludeTag {

	private static final String END_PAGE = "/META-INF/taglibs/fieldset_group/end.jsp";

	private static final String START_PAGE = "/META-INF/taglibs/fieldset_group/start.jsp";

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getDpFrontendServletContext());
	}

	@Override
	protected String getEndPage() {
		return END_PAGE;
	}

	@Override
	protected String getStartPage() {
		return START_PAGE;
	}

}