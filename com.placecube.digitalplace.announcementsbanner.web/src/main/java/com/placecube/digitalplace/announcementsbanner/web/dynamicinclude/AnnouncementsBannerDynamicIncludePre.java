package com.placecube.digitalplace.announcementsbanner.web.dynamicinclude;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.servlet.taglib.BaseDynamicInclude;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.announcementsbanner.web.constants.PortletKeys;

@Component(immediate = true, service = DynamicInclude.class)
public class AnnouncementsBannerDynamicIncludePre extends BaseDynamicInclude {

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String key) throws IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		if (!themeDisplay.getLayout().isTypeControlPanel()) {
			String[] runtimePortletIdsPre = GetterUtil.getStringValues(httpServletRequest.getAttribute("runtimePortletIdsBodyTopPre"), new String[0]);
			String[] append = ArrayUtil.append(runtimePortletIdsPre, new String[] { PortletKeys.ANNOUNCEMENTS_BANNER });

			httpServletRequest.setAttribute("runtimePortletIdsBodyTopPre", append);
			httpServletRequest.setAttribute("includeBodyTopPre", true);
		}
	}

	@Override
	public void register(DynamicIncludeRegistry dynamicIncludeRegistry) {
		dynamicIncludeRegistry.register("/html/common/themes/body_top.jsp#pre");
	}

}
