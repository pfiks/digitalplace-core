package com.placecube.digitalplace.exportimport.service.impl;

import java.util.Dictionary;

import org.osgi.service.component.annotations.Component;

import com.liferay.exportimport.configuration.ExportImportServiceConfiguration;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.util.HashMapDictionary;
import com.placecube.digitalplace.exportimport.service.ExportImportService;

@Component(immediate = true, service = ExportImportService.class)
public class ExportImportServiceImpl implements ExportImportService {

	@Override
	public void disableValidateLayoutReferences(long companyId) throws PortalException {
		setValidateLayoutReferences(companyId, false);
	}

	@Override
	public void enableValidateLayoutReferences(long companyId) throws PortalException {
		setValidateLayoutReferences(companyId,true);
	}

	private void setValidateLayoutReferences(long companyId, boolean validateLayoutReferences) throws PortalException {
		try {
			Dictionary<String, Object> properties = new HashMapDictionary<>();
			properties.put("validateLayoutReferences", validateLayoutReferences);

			ConfigurationProviderUtil.saveCompanyConfiguration(ExportImportServiceConfiguration.class, companyId, properties);
		} catch (ConfigurationException e) {
			throw new PortalException(e);
		}
	}

}
