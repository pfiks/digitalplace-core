package com.placecube.digitalplace.layout.util;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.model.PortletPreferences;
import com.liferay.portal.kernel.service.PortletLocalService;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.util.PortletKeys;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LayoutWithPortletsRetrievalHelperUtilTest extends PowerMockito {

	private static final long COMPANY_ID = 1;

	@InjectMocks
	private LayoutWithPortletsRetrievalHelperUtil layoutWithPortletsRetrievalHelperUtil;

	@Mock
	private Layout mockLayoutOne;

	@Mock
	private LayoutTypePortlet mockLayoutTypePortlet;

	@Mock
	private Portlet mockPortlet1;

	@Mock
	private Portlet mockPortlet2;

	@Mock
	private Portlet mockPortlet3;

	@Mock
	private Portlet mockPortlet4;

	@Mock
	private PortletLocalService mockPortletLocalService;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletPreferences mockPortletPreferences2;

	@Mock
	private PortletPreferences mockPortletPreferences3;

	@Mock
	private PortletPreferencesLocalService mockPortletPreferencesLocalService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getAllPortletsOnLayout_WhenLayoutHasValidPortlets_ThenReturnsThem() {
		List<Portlet> portlets = Arrays.asList(mockPortlet1, mockPortlet2);
		when(mockLayoutTypePortlet.getAllPortlets()).thenReturn(portlets);
		when(mockLayoutOne.getLayoutType()).thenReturn(mockLayoutTypePortlet);
		when(mockLayoutOne.getType()).thenReturn(LayoutConstants.TYPE_PORTLET);

		List<Portlet> result = layoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne);

		assertThat(result, equalTo(portlets));
	}

	@Test
	public void getAllPortletsOnLayout_WhenLayoutDoesNotHavePortlets_ThenReturnsEmptyList() {
		List<Portlet> portlets = new ArrayList<>();
		when(mockLayoutTypePortlet.getAllPortlets()).thenReturn(portlets);
		when(mockLayoutOne.getLayoutType()).thenReturn(mockLayoutTypePortlet);
		when(mockLayoutOne.getType()).thenReturn(LayoutConstants.TYPE_PORTLET);

		List<Portlet> result = layoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getAllPortletsOnLayout_WhenLayoutIsNotPortletTypeAndHasValidPortlet_ThenReturnsThem() {
		long plid = 123;
		List<Portlet> portlets = Collections.singletonList(mockPortlet1);
		when(mockLayoutTypePortlet.getAllPortlets()).thenReturn(portlets);
		when(mockLayoutOne.getLayoutType()).thenReturn(mockLayoutTypePortlet);
		when(mockLayoutOne.getType()).thenReturn("somethingElse");
		when(mockLayoutOne.getPlid()).thenReturn(plid);
		when(mockLayoutOne.getCompanyId()).thenReturn(COMPANY_ID);

		List<PortletPreferences> portletPrefs = new ArrayList<>();
		portletPrefs.add(mockPortletPreferences);
		portletPrefs.add(mockPortletPreferences2);
		portletPrefs.add(mockPortletPreferences3);
		when(mockPortletPreferencesLocalService.getPortletPreferences(PortletKeys.PREFS_OWNER_ID_DEFAULT, PortletKeys.PREFS_OWNER_TYPE_LAYOUT, plid)).thenReturn(portletPrefs);

		mockPortletRetrieval(mockPortletPreferences, "id1", mockPortlet2, true, "instance1");
		mockPortletRetrieval(mockPortletPreferences2, "id2", mockPortlet3, true, "");
		mockPortletRetrieval(mockPortletPreferences3, "id3", mockPortlet4, false, "");

		List<Portlet> result = layoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne);

		assertThat(result, containsInAnyOrder(mockPortlet1, mockPortlet2, mockPortlet4));
	}

	@Test
	public void getAllPortletsOnLayout_WhenLayoutIsNotPortletTypeAndDoesNotHavePortlets_ThenReturnsEmptyList() {
		long plid = 123;

		List<Portlet> portlets = new ArrayList<>();
		when(mockLayoutTypePortlet.getAllPortlets()).thenReturn(portlets);
		when(mockLayoutOne.getLayoutType()).thenReturn(mockLayoutTypePortlet);
		when(mockLayoutOne.getType()).thenReturn("somethingElse");
		when(mockLayoutOne.getPlid()).thenReturn(plid);
		when(mockLayoutOne.getCompanyId()).thenReturn(COMPANY_ID);

		List<PortletPreferences> portletPrefs = new ArrayList<>();
		when(mockPortletPreferencesLocalService.getPortletPreferences(PortletKeys.PREFS_OWNER_ID_DEFAULT, PortletKeys.PREFS_OWNER_TYPE_LAYOUT, plid)).thenReturn(portletPrefs);

		List<Portlet> result = layoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne);

		assertTrue(result.isEmpty());
	}

	@Test
	public void hasPortletId_WhenAnyOfThePortletsContainPortletId_ThenReturnTrue() {
		String portletId = "portletIdToMatchValue";
		List<Portlet> portlets = Arrays.asList(mockPortlet1, mockPortlet2);

		when(mockPortlet1.getPortletId()).thenReturn("someId");
		when(mockPortlet2.getPortletId()).thenReturn("something" + portletId + "else");

		boolean result = layoutWithPortletsRetrievalHelperUtil.hasPortletId(portlets, portletId);

		assertTrue(result);
	}

	@Test
	public void hasPortletId_WhenNoneOfThePortletsContainPortletId_ThenReturnFalse() {
		String portletId = "portletIdToMatchValue";
		List<Portlet> portlets = Arrays.asList(mockPortlet1, mockPortlet2);

		when(mockPortlet1.getPortletId()).thenReturn("someId");
		when(mockPortlet2.getPortletId()).thenReturn("other");

		boolean result = layoutWithPortletsRetrievalHelperUtil.hasPortletId(portlets, portletId);

		assertFalse(result);
	}

	@Test
	public void getPortletIdListFromLayoutPortletPreferences_WhenNoError_ThenReturnsTheListOfPortletIds() {
		String portletId = "portletIdToMatchValue";

		when(mockPortlet1.getPortletId()).thenReturn("differentProtletId");
		when(mockPortlet2.getPortletId()).thenReturn("someValue2" + portletId + "ending");
		when(mockPortlet3.getPortletId()).thenReturn("someValue3" + portletId + "ending");

		List<Portlet> portlets = new ArrayList<>();
		portlets.add(mockPortlet1);
		portlets.add(mockPortlet2);
		portlets.add(mockPortlet3);

		List<String> result = layoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(portlets, portletId);

		assertThat(result, containsInAnyOrder("someValue2" + portletId + "ending", "someValue3" + portletId + "ending"));
	}

	private void mockPortletRetrieval(PortletPreferences mockPortletPreferences, String portletId, Portlet portlet, boolean instanceable, String instanceId) {
		when(mockPortletPreferences.getPortletId()).thenReturn(portletId);
		when(mockPortletLocalService.getPortletById(COMPANY_ID, portletId)).thenReturn(portlet);
		when(portlet.isInstanceable()).thenReturn(instanceable);
		when(portlet.getInstanceId()).thenReturn(instanceId);
	}

}
