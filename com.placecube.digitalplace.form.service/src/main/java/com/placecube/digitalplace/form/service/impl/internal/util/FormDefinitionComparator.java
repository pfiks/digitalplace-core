package com.placecube.digitalplace.form.service.impl.internal.util;

import java.io.Serializable;
import java.util.Comparator;

import com.placecube.digitalplace.form.model.FormDefinition;

@SuppressWarnings("serial")
public class FormDefinitionComparator implements Comparator<FormDefinition>, Serializable {

	private static FormDefinitionComparator instance;

	@Override
	public int compare(FormDefinition formDefinition1, FormDefinition formDefinition2) {

		int value = formDefinition1.getCategory().compareTo(formDefinition2.getCategory());

		if (value == 0) {
			value = formDefinition1.getName().compareTo(formDefinition2.getName());
		}

		return value;
	}

	public static FormDefinitionComparator getInstance() {
		if (instance == null) {
			instance = new FormDefinitionComparator();
		}

		return instance;
	}
}
