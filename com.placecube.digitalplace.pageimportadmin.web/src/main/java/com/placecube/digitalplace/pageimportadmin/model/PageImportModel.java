package com.placecube.digitalplace.pageimportadmin.model;

import java.util.List;
import java.util.Optional;

import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.model.Layout;

public class PageImportModel extends RowImportModel {

	private String parentPageURL;
	private String pageTemplateName;
	private Optional<Layout> parentLayout;
	private Optional<LayoutPageTemplateEntry> pageTemplate;
	private boolean privateLayout;

	public PageImportModel(long groupId, int index, String title, String url, List<String> categoriesNames) {
		super(groupId, index, title, url, categoriesNames);
	}

	public Optional<LayoutPageTemplateEntry> getPageTemplate() {
		return pageTemplate;
	}

	public String getPageTemplateName() {
		return pageTemplateName;
	}

	public Optional<Layout> getParentLayout() {
		return parentLayout;
	}

	public String getParentPageURL() {
		return parentPageURL;
	}

	public boolean isPrivateLayout() {
		return privateLayout;
	}

	public void setPageTemplate(Optional<LayoutPageTemplateEntry> pageTemplate) {
		this.pageTemplate = pageTemplate;
	}

	public void setPageTemplateName(String pageTemplateName) {
		this.pageTemplateName = pageTemplateName;
	}

	public void setParentLayout(Optional<Layout> parentLayout) {
		this.parentLayout = parentLayout;
	}

	public void setParentPageURL(String parentPageURL) {
		this.parentPageURL = parentPageURL;
	}

	public void setPrivateLayout(boolean privateLayout) {
		this.privateLayout = privateLayout;
	}

}
