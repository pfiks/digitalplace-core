package com.placecube.digitalplace.layout.util;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactory;
import com.liferay.portal.kernel.service.LayoutService;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = LayoutRetrievalUtil.class)
public class LayoutRetrievalUtil {

	private static final Log LOG = LogFactoryUtil.getLog(LayoutRetrievalUtil.class);

	@Reference
	private LayoutService layoutService;

	@Reference
	private LayoutWithPortletsRetrievalHelperUtil layoutWithPortletsRetrievalHelperUtil;

	@Reference
	private PortletPreferencesFactory portletPreferencesFactory;

	public List<Layout> getLayoutsWithPortletId(long groupId, String portletId, boolean privateLayout) {
		List<Layout> layouts = layoutService.getLayouts(groupId, privateLayout);
		return layouts.stream().filter(entry -> layoutWithPortletsRetrievalHelperUtil.hasPortletId(layoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(entry), portletId))
				.collect(Collectors.toList());
	}

	public Optional<Layout> getFirstNonHiddenLayout(List<Layout> layouts) {
		for (Layout layout : layouts) {
			if (!layout.isHidden()) {
				return Optional.of(layout);
			}
		}
		return Optional.empty();
	}

	public Optional<Layout> getFirstNonHiddenLayoutWithCorrectPrefs(long groupId, boolean privateLayout, Map<String, String> prefs, String portletId) {
		List<Layout> layouts = layoutService.getLayouts(groupId, privateLayout);
		for (Layout layout : layouts) {
			if (!layout.isHidden()) {
				List<String> portletIds = layoutWithPortletsRetrievalHelperUtil.getPortletIdListFromListOfPortlets(layoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(layout), portletId);
				if (isLayoutWithCorrectPreferencesConfigured(layout, prefs, portletIds)) {
					return Optional.of(layout);
				}
			}
		}
		return Optional.empty();
	}

	private boolean isLayoutWithCorrectPreferencesConfigured(Layout layout, Map<String, String> prefsToMatch, List<String> portletIds) {
		for (String portletId : portletIds) {
			try {
				PortletPreferences preferences = portletPreferencesFactory.getExistingPortletSetup(layout, portletId);
				if (Validator.isNotNull(preferences)) {
					for (Map.Entry<String, String> pref : prefsToMatch.entrySet()) {
						if (!preferences.getValue(pref.getKey(), StringPool.BLANK).equals(pref.getValue())) {
							return false;
						}
					}
					return true;
				}

			} catch (PortalException exception) {
				LOG.error("Could not get portlet preferences for a given layout with id: " + layout.getLayoutId() + " :" + exception.getMessage());
				LOG.debug(exception);
			}
		}
		return false;
	}

}
