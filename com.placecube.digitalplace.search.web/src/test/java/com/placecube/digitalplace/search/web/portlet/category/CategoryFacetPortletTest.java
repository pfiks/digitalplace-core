package com.placecube.digitalplace.search.web.portlet.category;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.common.portlet.PortletURLService;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.category.configuration.CategoryFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.category.constants.CategoryFacetDisplayConstants;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryConfigurationService;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryFacetTreeService;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryLocalService;
import com.placecube.digitalplace.search.web.service.FacetParsingService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class CategoryFacetPortletTest extends PowerMockito {

	private static final String INTERNAL_PORTLET_ID = "kygnf";

	@InjectMocks
	private CategoryFacetPortlet categoryFacetPortlet;

	@Mock
	private PortletURLService mockPortletURLService;

	@Mock
	private CategoryConfigurationService mockCategoryConfigurationService;

	@Mock
	private CategoryLocalService mockCategoryLocalService;

	@Mock
	private CategoryFacetPortletInstanceConfiguration mockConfiguration;

	@Mock
	private CategoryFacetTreeService mockCategoryFacetTreeService;

	@Mock
	private FacetParsingService mockFacetParsingService;

	@Mock
	private List<FacetResult> mockFacetResults;

	@Mock
	private FacetResult mockFacetResult1;

	@Mock
	private FacetResult mockFacetResult2;

	@Mock
	private Map<String, Integer> mockFacetValues;

	@Mock
	private JSONArray mockJSONTree;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private Set<String> mockSelectedValues;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Set<Long> mockLongSet;

	@Before
	public void activateSetup() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test
	public void render_WhenConfigurationIsInvalid_ThenSetsInvalidConfigurationTrueAsRequestAttributeAndDoesNotExecuteTheSearch() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.empty());

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verifyZeroInteractions(mockSharedSearchRequestService);
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenConfigurationIsValid_ThenSetsConfigurationAndRedirectUrlAsRequestAttributes() throws Exception {
		String redirectURL = "expectedVal";
		when(mockConfiguration.internalPortletId()).thenReturn(INTERNAL_PORTLET_ID);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockConfiguration.displayStyle()).thenReturn(CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_CHECKBOX);
		when(mockSharedSearchResponse.getEscapedSearchResultsIteratorURL()).thenReturn(redirectURL);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("configuration", mockConfiguration);
		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", redirectURL);
	}

	@Test
	public void render_WhenConfigurationIsValid_ThenSetsIsEditLayoutModeInRequest() throws Exception {
		when(mockConfiguration.internalPortletId()).thenReturn(INTERNAL_PORTLET_ID);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockPortletURLService.isEditLayoutMode(mockRenderRequest)).thenReturn(true);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockConfiguration.displayStyle()).thenReturn(CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_CHECKBOX);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("isEditLayoutMode", true);
	}

	@Test
	public void render_WhenShowAllCategoriesIsConfigured_ThenConfiguresAllTheCategoriesSelectedAsRequestAttributes() throws Exception {
		String[] vocabularyIds = { "ID", "ID2" };
		String[] categoryIds = { "ID3", "ID4" };

		when(mockConfiguration.showAllCategoriesAlways()).thenReturn(true);
		when(mockConfiguration.displayCategoryIds()).thenReturn(categoryIds);
		when(mockConfiguration.displayVocabularyIds()).thenReturn(vocabularyIds);
		when(mockConfiguration.internalPortletId()).thenReturn(INTERNAL_PORTLET_ID);
		when(mockConfiguration.displayStyle()).thenReturn(CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.ASSET_CATEGORY_IDS)).thenReturn(mockFacetValues);
		when(mockSharedSearchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(mockSelectedValues);
		when(mockCategoryLocalService.getAggregateCategoryLists(mockThemeDisplay, vocabularyIds, categoryIds)).thenReturn(mockLongSet);
		when(mockFacetParsingService.parseAllCategories(mockFacetValues, mockLongSet, mockSelectedValues)).thenReturn(mockFacetResults);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableCategories", mockFacetResults);
	}

	@Test
	public void render_WhenShowAllCategoriesIsNotConfigured_ThenSetsAvailableCategoriesFromResultsInRequest() throws Exception {

		List<FacetResult> facetList = new ArrayList<>();
		facetList.add(mockFacetResult1);
		facetList.add(mockFacetResult2);

		Set<Long> aggregatedCategoryList = Stream.of(1l).collect(Collectors.toSet());

		List<FacetResult> facetListResult = new ArrayList<>();
		facetListResult.add(mockFacetResult1);

		String[] vocabularyIds = { "ID", "ID2" };
		String[] categoryIds = { "ID3", "ID4" };

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));

		when(mockConfiguration.showAllCategoriesAlways()).thenReturn(false);
		when(mockConfiguration.displayCategoryIds()).thenReturn(categoryIds);
		when(mockConfiguration.displayVocabularyIds()).thenReturn(vocabularyIds);
		when(mockConfiguration.internalPortletId()).thenReturn(INTERNAL_PORTLET_ID);
		when(mockConfiguration.displayStyle()).thenReturn(CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_CHECKBOX);

		when(mockCategoryLocalService.getAggregateCategoryLists(mockThemeDisplay, vocabularyIds, categoryIds)).thenReturn(aggregatedCategoryList);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.ASSET_CATEGORY_IDS)).thenReturn(mockFacetValues);
		when(mockSharedSearchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(mockSelectedValues);

		when(mockFacetParsingService.parseCategoryFacets(mockFacetValues, mockSelectedValues)).thenReturn(facetList);

		when(mockFacetResult1.getValue()).thenReturn("1");
		when(mockFacetResult2.getValue()).thenReturn("2");

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableCategories", facetListResult);

	}

	@Test
	public void render_WhenDisplayStyleIsDropdown_ThenConfiguresFirstSelectedCategoryAsRequestAttributes() throws Exception {
		final String facetValue = "value";
		final String[] vocabularyIds = { "ID", "ID2" };
		final String[] categoryIds = { "ID3", "ID4" };
		final List<FacetResult> facetResults = Arrays.asList(new FacetResult[] { mockFacetResult1, mockFacetResult2 });

		when(mockConfiguration.showAllCategoriesAlways()).thenReturn(true);
		when(mockConfiguration.displayCategoryIds()).thenReturn(categoryIds);
		when(mockConfiguration.displayVocabularyIds()).thenReturn(vocabularyIds);
		when(mockConfiguration.internalPortletId()).thenReturn(INTERNAL_PORTLET_ID);
		when(mockConfiguration.displayStyle()).thenReturn(CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.ASSET_CATEGORY_IDS)).thenReturn(mockFacetValues);
		when(mockSharedSearchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(mockSelectedValues);
		when(mockCategoryLocalService.getAggregateCategoryLists(mockThemeDisplay, vocabularyIds, categoryIds)).thenReturn(mockLongSet);
		when(mockFacetParsingService.parseAllCategories(mockFacetValues, mockLongSet, mockSelectedValues)).thenReturn(facetResults);

		when(mockFacetResult1.isSelected()).thenReturn(false);
		when(mockFacetResult2.isSelected()).thenReturn(true);
		when(mockFacetResult2.getValue()).thenReturn(facetValue);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("selectedFacetValue", facetValue);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresDisplayStyleAsRequestAttribute() throws Exception {
		String displayStyle = "displayStyle";

		when(mockConfiguration.displayStyle()).thenReturn(displayStyle);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("displayStyle", displayStyle);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresFacetFieldNameAsRequestAttribute() throws Exception {
		when(mockConfiguration.internalPortletId()).thenReturn(INTERNAL_PORTLET_ID);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("facetFieldName", SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID);

	}

	@Test
	public void render_WhenNoErrorAndTreeViewSelected_ThenConfiguresCategoriesTreeAsRequestAttributes() throws Exception {
		int maxTreeViewDepth = 2;
		Locale locale = Locale.CANADA;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.ASSET_CATEGORY_IDS)).thenReturn(mockFacetValues);
		when(mockSharedSearchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(mockSelectedValues);
		when(mockFacetParsingService.parseCategoryFacets(mockFacetValues, mockSelectedValues)).thenReturn(mockFacetResults);
		when(mockConfiguration.internalPortletId()).thenReturn(INTERNAL_PORTLET_ID);
		when(mockConfiguration.displayStyle()).thenReturn(CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_TREE);
		when(mockConfiguration.maxTreeViewDepth()).thenReturn(maxTreeViewDepth);
		when(mockCategoryFacetTreeService.toJSONTree(mockFacetResults, mockSelectedValues, maxTreeViewDepth, locale)).thenReturn(mockJSONTree);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("categoriesTree", mockJSONTree);
	}

	@Test
	public void render_WhenInvisibleEnabled_ThenNoAvailableCategoriesReturned() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockConfiguration.invisible()).thenReturn(true);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableCategories", new ArrayList<>());
	}

	@Test
	public void render_WhenAlphaSortCategoriesIsSet_ThenReturnsCategoriesSortedByLabel() throws Exception {
		Locale local = LocaleUtil.UK;
		List<FacetResult> facetList = new ArrayList<>();
		facetList.add(mockFacetResult1);
		facetList.add(mockFacetResult2);

		when(mockFacetResult1.getLabel(LocaleUtil.UK)).thenReturn("B");
		when(mockFacetResult2.getLabel(LocaleUtil.UK)).thenReturn("A");

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(local);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockConfiguration.alphaSort()).thenReturn(true);
		when(mockConfiguration.internalPortletId()).thenReturn(INTERNAL_PORTLET_ID);

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.ASSET_CATEGORY_IDS)).thenReturn(mockFacetValues);
		when(mockSharedSearchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(mockSelectedValues);
		when(mockFacetParsingService.parseCategoryFacets(mockFacetValues, mockSelectedValues)).thenReturn(facetList);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		assertThat(facetList.get(0), equalTo(mockFacetResult2));
		assertThat(facetList.get(1), equalTo(mockFacetResult1));

	}

	@Test
	public void render_WhenAlphaSortCategoriesIsSet_ThenReturnsCategoriesInOriginalOrder() throws Exception {
		Locale local = LocaleUtil.UK;
		List<FacetResult> facetList = new ArrayList<>();
		facetList.add(mockFacetResult1);
		facetList.add(mockFacetResult2);

		when(mockFacetResult1.getLabel(LocaleUtil.UK)).thenReturn("B");
		when(mockFacetResult2.getLabel(LocaleUtil.UK)).thenReturn("A");

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(local);
		when(mockCategoryConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockConfiguration.alphaSort()).thenReturn(false);
		when(mockConfiguration.internalPortletId()).thenReturn(INTERNAL_PORTLET_ID);

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.ASSET_CATEGORY_IDS)).thenReturn(mockFacetValues);
		when(mockSharedSearchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_CATEGORIES + "_" + INTERNAL_PORTLET_ID)).thenReturn(mockSelectedValues);
		when(mockFacetParsingService.parseCategoryFacets(mockFacetValues, mockSelectedValues)).thenReturn(facetList);

		categoryFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		assertThat(facetList.get(0), equalTo(mockFacetResult1));
		assertThat(facetList.get(1), equalTo(mockFacetResult2));

	}
}
