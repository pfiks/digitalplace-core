package com.placecube.digitalplace.form.model;

import java.net.URL;
import java.util.List;

public interface FormDefinition {

	String getName();

	String getDescription();

	String getCategory();

	String getKey();

	String getJournalArticleFolderName();

	URL getUrlToDDMFormDefinitionFile();

	URL getUrlToDDMFormLayoutDefinitionFile();

	List<URL> getUrlsToJournalArticles();

	URL getUrlToSettingsDDMFormValuesDefinitionFile();

	List<URL> getUrlsToDataProviderDDMFormValuesDefinitionFiles();

}
