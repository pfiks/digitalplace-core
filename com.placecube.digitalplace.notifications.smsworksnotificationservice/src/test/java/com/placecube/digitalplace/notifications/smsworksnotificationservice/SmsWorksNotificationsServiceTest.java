package com.placecube.digitalplace.notifications.smsworksnotificationservice;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.notifications.model.SendSmsContext;
import com.placecube.digitalplace.notifications.smsworksnotificationservice.configuration.SmsWorksNotificationServiceConfiguration;
import com.placecube.digitalplace.notifications.smsworksnotificationservice.service.SmsWorksNotificationsService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SmsWorksNotificationsServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 11l;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private SendSmsContext mockSendSmsContext;

	@Mock
	private SmsWorksNotificationServiceConfiguration mockSmsWorksNotificationServiceConfiguration;

	@InjectMocks
	private SmsWorksNotificationsService smsWorksNotificationsService;

	@Test
	public void getBody_WhenSenderIdIsNull_ThenReturnsBodyWithEmptySenderId() {
		String senderId = StringPool.BLANK;
		String phoneNumber = "phoneValue";
		String content = "contentValue";
		String reference = "refValue";
		String jsonString = "jsonString";
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJSONObject);
		when(mockSendSmsContext.getSmsSenderId()).thenReturn(senderId);
		when(mockSendSmsContext.getPhoneNumber()).thenReturn(phoneNumber);
		when(mockSendSmsContext.getReference()).thenReturn(reference);
		when(mockJSONObject.toJSONString()).thenReturn(jsonString);

		String result = smsWorksNotificationsService.getBody(mockSendSmsContext, content);

		assertThat(result, equalTo(jsonString));
		InOrder inOrder = inOrder(mockJSONObject);
		inOrder.verify(mockJSONObject, times(1)).put("sender", senderId);
		inOrder.verify(mockJSONObject, times(1)).put("destination", phoneNumber);
		inOrder.verify(mockJSONObject, times(1)).put("content", content);
		inOrder.verify(mockJSONObject, times(1)).put("schedule", StringPool.BLANK);
		inOrder.verify(mockJSONObject, times(1)).put("tag", reference);
		inOrder.verify(mockJSONObject, times(1)).toJSONString();
	}

	@Test
	@Parameters({ "ThisIsOne..", "ThisIsOne..Extra" })
	public void getBody_WhenSenderIdIsValid_ThenReturnsBodyWithSenderIdWithMax11Characters(String senderId) {
		String phoneNumber = "phoneValue";
		String content = "contentValue";
		String reference = "refValue";
		String jsonString = "jsonString";
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJSONObject);
		when(mockSendSmsContext.getSmsSenderId()).thenReturn(senderId);
		when(mockSendSmsContext.getPhoneNumber()).thenReturn(phoneNumber);
		when(mockSendSmsContext.getReference()).thenReturn(reference);
		when(mockJSONObject.toJSONString()).thenReturn(jsonString);

		String result = smsWorksNotificationsService.getBody(mockSendSmsContext, content);

		assertThat(result, equalTo(jsonString));
		InOrder inOrder = inOrder(mockJSONObject);
		inOrder.verify(mockJSONObject, times(1)).put("sender", "ThisIsOne..");
		inOrder.verify(mockJSONObject, times(1)).put("destination", phoneNumber);
		inOrder.verify(mockJSONObject, times(1)).put("content", content);
		inOrder.verify(mockJSONObject, times(1)).put("schedule", StringPool.BLANK);
		inOrder.verify(mockJSONObject, times(1)).put("tag", reference);
		inOrder.verify(mockJSONObject, times(1)).toJSONString();
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(SmsWorksNotificationServiceConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		smsWorksNotificationsService.getConfiguration(COMPANY_ID);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsTheConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(SmsWorksNotificationServiceConfiguration.class, COMPANY_ID)).thenReturn(mockSmsWorksNotificationServiceConfiguration);

		SmsWorksNotificationServiceConfiguration result = smsWorksNotificationsService.getConfiguration(COMPANY_ID);

		assertThat(result, sameInstance(mockSmsWorksNotificationServiceConfiguration));
	}

	@Test
	public void getHttpPost_WhenNoError_ThenReturnsTheHttpPostWithTheConfiguredEndpoint() {
		String endpoint = "http://placecube.com";
		when(mockSmsWorksNotificationServiceConfiguration.endpoint()).thenReturn(endpoint);

		HttpPost result = smsWorksNotificationsService.getHttpPost(mockSmsWorksNotificationServiceConfiguration);

		assertNotNull(result);
	}

	@Test
	public void getStringEntity_WhenNoError_ThenReturnsTheStringEntityForTheJsonBodyValue() {
		String body = "{jsonBody}";

		StringEntity result = smsWorksNotificationsService.getStringEntity(body);

		assertThat(result.getContentType().getValue(), equalTo("application/json"));
	}

	@Test
	public void getSubjectTextField_WhenNoError_ThenReturnsTheTextWithTheReplacedPlaceholders() {
		Map<String, Object> variables = new HashMap<>();
		variables.put("ONE", "1a");
		variables.put("two", "2a");
		String text = "This is my $ONE$ text ONE with some $two$ placeholder two values";
		String expected = "This is my 1a text ONE with some 2a placeholder two values";
		when(mockSendSmsContext.getTemplateId()).thenReturn(text);
		when(mockSendSmsContext.getPersonalisation()).thenReturn(variables);

		String result = smsWorksNotificationsService.getContentWithPlaceholderValues(mockSendSmsContext);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = ConfigurationException.class)
	public void isSmsEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(SmsWorksNotificationServiceConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		smsWorksNotificationsService.isSmsEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isSmsEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsSmsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(SmsWorksNotificationServiceConfiguration.class, COMPANY_ID)).thenReturn(mockSmsWorksNotificationServiceConfiguration);
		when(mockSmsWorksNotificationServiceConfiguration.smsEnabled()).thenReturn(expected);

		boolean result = smsWorksNotificationsService.isSmsEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

}
