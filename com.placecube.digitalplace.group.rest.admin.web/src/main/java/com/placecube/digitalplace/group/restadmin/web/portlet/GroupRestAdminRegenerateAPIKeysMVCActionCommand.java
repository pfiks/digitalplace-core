package com.placecube.digitalplace.group.restadmin.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;
import com.placecube.digitalplace.group.restadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.GROUP_REST_ADMIN, "mvc.command.name=" + MVCCommandKeys.UPDATE_API_KEYS }, service = MVCActionCommand.class)
public class GroupRestAdminRegenerateAPIKeysMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private GroupApiKeyLocalService groupApiKeyLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		groupApiKeyLocalService.regenerateGroupApiKey(themeDisplay.getScopeGroup());
	}

}