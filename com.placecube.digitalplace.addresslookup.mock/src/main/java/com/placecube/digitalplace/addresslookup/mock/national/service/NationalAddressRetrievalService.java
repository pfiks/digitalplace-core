package com.placecube.digitalplace.addresslookup.mock.national.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.mock.national.configuration.MockAddressNationalLookupCompanyConfiguration;

@Component(immediate = true, service = NationalAddressRetrievalService.class)
public class NationalAddressRetrievalService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private NationalResponseParserService nationalResponseParserService;

	public AddressContext getAddressContext(String uprn) {

		if (uprn.length() > 3) {
			String digit = uprn.substring(3, 4);
			String postcode = uprn.replace(digit, StringPool.SPACE);
			return getAddressLookup(nationalResponseParserService.getDPA(Integer.valueOf(digit), postcode));
		} else {

			try {
				return getAddressLookup(nationalResponseParserService.getDPA(Integer.valueOf(uprn), uprn));
			} catch (NumberFormatException e) {
				return getAddressLookup(nationalResponseParserService.getDPA(1, "1"));
			}

		}

	}

	public AddressContext getAddressLookup(JSONObject jsonObject) {
		JSONObject entryJsonObject = jsonObject.getJSONObject("DPA");

		String buildingDetails = nationalResponseParserService.getBuildingDetails(entryJsonObject);
		String buildingNumber = entryJsonObject.getString("BUILDING_NUMBER", StringPool.BLANK);
		String streetName = nationalResponseParserService.getStreeetName(entryJsonObject);

		String addressLine1 = nationalResponseParserService.getAddressLine1(buildingDetails, buildingNumber);
		String addressLine2 = nationalResponseParserService.getAddressLine2(buildingDetails, buildingNumber, streetName);
		String addressLine3 = nationalResponseParserService.getAddressLine3(entryJsonObject);

		return nationalResponseParserService.getAddressContext(entryJsonObject, addressLine1, addressLine2, addressLine3);
	}

	public JSONArray getResults(String postcode) throws PortalException {
		try {
			return nationalResponseParserService.getResults(postcode);
		} catch (JSONException e) {
			throw new PortalException(e);
		}
	}

	public Integer getWeight(long companyId) throws ConfigurationException {
		MockAddressNationalLookupCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockAddressNationalLookupCompanyConfiguration.class, companyId);
		return configuration.weight();
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		MockAddressNationalLookupCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockAddressNationalLookupCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}

	public boolean isNational(long companyId) throws ConfigurationException {
		MockAddressNationalLookupCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(MockAddressNationalLookupCompanyConfiguration.class, companyId);
		return configuration.national();
	}

}
