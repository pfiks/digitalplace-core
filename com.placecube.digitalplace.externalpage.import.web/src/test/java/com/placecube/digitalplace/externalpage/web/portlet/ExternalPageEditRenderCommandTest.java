package com.placecube.digitalplace.externalpage.web.portlet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.ViewKeys;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, ExternalPageEditRenderCommandTest.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.util.ParamUtil")
public class ExternalPageEditRenderCommandTest extends PowerMockito {

	@InjectMocks
	private ExternalPageEditRenderCommand externalPageEditRenderCommand;

	@Mock
	private ExternalPage mockExternalPage;

	@Mock
	private ExternalPageLocalService mockExternalService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Before
	public void activeSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenExternalPageIdIsGreaterThanZero_ThenFetchExternalPageAndReturnEditExternalPageJSP() throws Exception {
		long externalPageId = 9574;
		when(ParamUtil.getLong(mockRenderRequest, "externalPageId")).thenReturn(externalPageId);
		when(mockExternalService.fetchExternalPage(externalPageId)).thenReturn(mockExternalPage);
		
		String jsp = externalPageEditRenderCommand.render(mockRenderRequest, mockRenderResponse);
		
		verify(mockRenderRequest, times(1)).setAttribute("externalPage", mockExternalPage);
		assertEquals(ViewKeys.EDIT, jsp);
	}

	@Test
	@Parameters({"-1", "0"})
	public void render_WhenExternalPageIdIsZeroOrLess_ThenDoNotFetchExternalPage(long externalPageId) throws Exception {
		when(ParamUtil.getLong(mockRenderRequest, "externalPageId")).thenReturn(externalPageId);
		
		String jsp = externalPageEditRenderCommand.render(mockRenderRequest, mockRenderResponse);
		
		verifyZeroInteractions(mockExternalService);
		verify(mockRenderRequest, times(0)).setAttribute("externalPage", mockExternalPage);
		assertEquals(ViewKeys.EDIT, jsp);
	}
}
