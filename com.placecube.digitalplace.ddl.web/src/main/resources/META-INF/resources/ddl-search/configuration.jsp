<%@page import="com.placecube.digitalplace.search.shared.constants.SharedSearchDisplayCostants"%>
<%@page import="com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings"%>
<%@page import="com.placecube.digitalplace.search.shared.service.SharedSearchDisplayService"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="/init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post" name="ddlSearchListingForm" >

	<c:set var="displayStyleGrid" value="<%= SharedSearchDisplayCostants.DISPLAY_STYLE_GRID%>" />
	<c:set var="displayStyleList" value="<%= SharedSearchDisplayCostants.DISPLAY_STYLE_LIST%>" />
	<c:set var="displayStyleGeoPoint" value="geo-point" />

	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />

	<liferay-frontend:edit-form-body>
		<dp-frontend:fieldset-group>

			<liferay-frontend:fieldset>
				<%@ include file ="../configuration-structure-select.jspf" %>
            </liferay-frontend:fieldset>

			<liferay-frontend:fieldset>

				<aui:select label="default-display-style" name="defaultDisplayStyle" showEmptyOption="true" required="false">
					<aui:option label="${displayStyleGrid}" value="${displayStyleGrid}" selected="${displayStyleGrid == configuration.defaultDisplayStyle()}"/>
					<aui:option label="${displayStyleList}" value="${displayStyleList}" selected="${displayStyleList == configuration.defaultDisplayStyle()}"/>
					<aui:option label="${displayStyleGeoPoint}" value="${displayStyleGeoPoint}" selected="${displayStyleGeoPoint == configuration.defaultDisplayStyle()}"/>
				</aui:select>

			</liferay-frontend:fieldset>
			
			<liferay-frontend:fieldset>
					<aui:input label="grid-display-style-css" name="gridDisplayStyleCss" type="text" required="false" value="${configuration.gridDisplayStyleCss()}" />
			</liferay-frontend:fieldset>

			<liferay-frontend:fieldset>
				<aui:select label="default-number-of-results-per-page" name="numberOfResultsPerPage">

					<c:forEach items="${availablePageDeltas}" var="pageDelta">
						<aui:option value="${pageDelta}" selected="${pageDelta == configuration.numberOfResultsPerPage()}">
							${pageDelta}
						</aui:option>
					</c:forEach>
				</aui:select>
				
				<aui:input type="toggle-switch" label="hide-pagination" name="hidePagination" value="${configuration.hidePagination()}" />
			</liferay-frontend:fieldset>

		</dp-frontend:fieldset-group>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>

