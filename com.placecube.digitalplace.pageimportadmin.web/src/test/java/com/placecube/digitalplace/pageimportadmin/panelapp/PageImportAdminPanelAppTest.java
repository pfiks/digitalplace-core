package com.placecube.digitalplace.pageimportadmin.panelapp;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.pageimportadmin.constants.PortletKeys;

public class PageImportAdminPanelAppTest {

	@InjectMocks
	private PageImportAdminPanelApp pageImportAdminPanelApp;

	@Mock
	private Group mockGroup;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void getPortletId_ThenReturnsThePortletId() {
		String result = pageImportAdminPanelApp.getPortletId();

		assertThat(result, equalTo(PortletKeys.PAGE_IMPORT_ADMIN));
	}

	@Test
	public void isShow_WhenGroupIsGlobal_ThenReturnsFalse() throws PortalException {
		when(mockGroup.isCompany()).thenReturn(true);

		boolean result = pageImportAdminPanelApp.isShow(mockPermissionChecker, mockGroup);

		assertFalse(result);
	}

}
