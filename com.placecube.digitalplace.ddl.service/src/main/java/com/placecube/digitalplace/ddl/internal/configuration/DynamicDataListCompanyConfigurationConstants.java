package com.placecube.digitalplace.ddl.internal.configuration;

public final class DynamicDataListCompanyConfigurationConstants {

	public static final String CONFIG_FIELD_FULL_VIEW_TEMPLATE_ID = "fullViewDefaultTemplateId";

	public static final String CONFIG_FIELD_GEOPOINT_TEMPLATE_ID = "searchDefaultGeoPointTemplateId";

	public static final String CONFIG_FIELD_GRID_TEMPLATE_ID = "searchDefaultGridViewTemplateId";

	public static final String CONFIG_FIELD_ICON_NAME = "iconName";

	public static final String CONFIG_FIELD_INDEX_CONTENT = "indexerContentFieldName";

	public static final String CONFIG_FIELD_INDEX_TITLE = "indexerTitleFieldName";

	public static final String CONFIG_FIELD_LIST_TEMPLATE_ID = "searchDefaultListViewTemplateId";

	public static final String CONFIG_FIELD_MAPPING_NAME = "mappingName";

	public static final String CONFIG_FIELD_STRUCTURE_ID = "ddlStructureId";

	public static final String PID = "com.placecube.digitalplace.ddl.internal.configuration.DynamicDataListCompanyConfiguration";

	private DynamicDataListCompanyConfigurationConstants() {
	}
}
