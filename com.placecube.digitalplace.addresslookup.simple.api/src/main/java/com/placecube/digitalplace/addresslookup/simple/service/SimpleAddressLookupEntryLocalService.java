/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;
import com.placecube.digitalplace.addresslookup.simple.service.persistence.SimpleAddressLookupEntryPK;

import java.io.Serializable;

import java.util.List;
import java.util.Optional;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for SimpleAddressLookupEntry. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see SimpleAddressLookupEntryLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface SimpleAddressLookupEntryLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>com.placecube.digitalplace.addresslookup.simple.service.impl.SimpleAddressLookupEntryLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the simple address lookup entry local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link SimpleAddressLookupEntryLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the simple address lookup entry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 * @return the simple address lookup entry that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public SimpleAddressLookupEntry addSimpleAddressLookupEntry(
		SimpleAddressLookupEntry simpleAddressLookupEntry);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Creates a new simple address lookup entry with the primary key. Does not add the simple address lookup entry to the database.
	 *
	 * @param simpleAddressLookupEntryPK the primary key for the new simple address lookup entry
	 * @return the new simple address lookup entry
	 */
	@Transactional(enabled = false)
	public SimpleAddressLookupEntry createSimpleAddressLookupEntry(
		SimpleAddressLookupEntryPK simpleAddressLookupEntryPK);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	/**
	 * Deletes the simple address lookup entry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public SimpleAddressLookupEntry deleteSimpleAddressLookupEntry(
		SimpleAddressLookupEntry simpleAddressLookupEntry);

	/**
	 * Deletes the simple address lookup entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 * @throws PortalException if a simple address lookup entry with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public SimpleAddressLookupEntry deleteSimpleAddressLookupEntry(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> T dslQuery(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int dslQueryCount(DSLQuery dslQuery);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public SimpleAddressLookupEntry fetchSimpleAddressLookupEntry(
		SimpleAddressLookupEntryPK simpleAddressLookupEntryPK);

	/**
	 * Returns the simple address lookup entry with the matching UUID and company.
	 *
	 * @param uuid the simple address lookup entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public SimpleAddressLookupEntry
		fetchSimpleAddressLookupEntryByUuidAndCompanyId(
			String uuid, long companyId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<SimpleAddressLookupEntry> getByCompanyIdAndPostcode(
		long companyId, String postcode);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Optional<SimpleAddressLookupEntry> getByCompanyIdAndUprn(
		long companyId, String uprn);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Returns a range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of simple address lookup entries
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<SimpleAddressLookupEntry> getSimpleAddressLookupEntries(
		int start, int end);

	/**
	 * Returns the number of simple address lookup entries.
	 *
	 * @return the number of simple address lookup entries
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getSimpleAddressLookupEntriesCount();

	/**
	 * Returns the simple address lookup entry with the primary key.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry
	 * @throws PortalException if a simple address lookup entry with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public SimpleAddressLookupEntry getSimpleAddressLookupEntry(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws PortalException;

	/**
	 * Returns the simple address lookup entry with the matching UUID and company.
	 *
	 * @param uuid the simple address lookup entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching simple address lookup entry
	 * @throws PortalException if a matching simple address lookup entry could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public SimpleAddressLookupEntry
			getSimpleAddressLookupEntryByUuidAndCompanyId(
				String uuid, long companyId)
		throws PortalException;

	/**
	 * Updates the simple address lookup entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 * @return the simple address lookup entry that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public SimpleAddressLookupEntry updateSimpleAddressLookupEntry(
		SimpleAddressLookupEntry simpleAddressLookupEntry);

}