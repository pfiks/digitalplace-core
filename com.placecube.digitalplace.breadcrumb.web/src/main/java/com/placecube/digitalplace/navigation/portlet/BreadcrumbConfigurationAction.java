package com.placecube.digitalplace.navigation.portlet;

import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.navigation.configuration.BreadcrumbPortletInstanceConfiguration;
import com.placecube.digitalplace.navigation.constants.ConfigurationConstants;
import com.placecube.digitalplace.navigation.constants.ParamKeys;
import com.placecube.digitalplace.navigation.constants.PortletKeys;
import com.placecube.digitalplace.navigation.constants.PortletPreferenceKeys;
import com.placecube.digitalplace.navigation.service.AssetCategoryService;
import com.placecube.digitalplace.navigation.service.AssetVocabularyService;

@Component(immediate = true, configurationPid = ConfigurationConstants.BREADCRUMB_PID, configurationPolicy = ConfigurationPolicy.OPTIONAL, property = "javax.portlet.name="
		+ PortletKeys.BREADCRUMB, service = ConfigurationAction.class)
public class BreadcrumbConfigurationAction extends DefaultConfigurationAction {

	@Reference
	AssetCategoryService assetCategoryService;

	@Reference
	AssetVocabularyLocalService assetVocabularyLocalService;

	@Reference
	AssetVocabularyService assetVocabularyService;

	private volatile BreadcrumbPortletInstanceConfiguration breadcrumbConfiguration;

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {

		breadcrumbConfiguration = ConfigurableUtil.createConfigurable(BreadcrumbPortletInstanceConfiguration.class, properties);

		if (breadcrumbConfiguration == null) {
			throw new IllegalStateException("Breadcrumb configuration missing - PID: " + ConfigurationConstants.BREADCRUMB_PID);
		}

	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {

		PortletRequest portletRequest = (PortletRequest) httpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST);
		PortletPreferences preferences = portletRequest.getPreferences();

		long scopeGroupId = PortalUtil.getScopeGroupId(httpServletRequest);
		long globalGroupId = PortalUtil.getCompany(httpServletRequest).getGroupId();
		long[] groupIds = { scopeGroupId, globalGroupId };

		List<AssetVocabulary> groupVocabularies = assetVocabularyLocalService.getGroupVocabularies(groupIds);
		String categoryNavUrlCategoryPropertyName = preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME, breadcrumbConfiguration.categoryNavUrlCategoryPropertyName());
		String categoryNavBuildCategoriesHierarchyUrlPropertyName = preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME,
				breadcrumbConfiguration.categoryNavBuildCategoriesHierarchyUrlPropertyName());
		String categoryNavFilterVocabId = preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, String.valueOf(breadcrumbConfiguration.categoryNavFilterVocabularyId()));
		categoryNavFilterVocabId = String.valueOf(assetVocabularyService.getVocabularyId(scopeGroupId, categoryNavFilterVocabId));

		boolean forceRootForLayoutBreadcrumbs = Boolean.parseBoolean(
				preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS, String.valueOf(breadcrumbConfiguration.forceRootForLayoutBreadcrumbs())));

		String categoryNavRootEntryName = preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME, breadcrumbConfiguration.categoryNavRootEntryName());
		boolean categoryNavShowCurrentLayout = Boolean.parseBoolean(
				preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, String.valueOf(breadcrumbConfiguration.categoryNavShowCurrentLayout())));

		boolean useLayoutUrl = Boolean.parseBoolean(preferences.getValue(PortletPreferenceKeys.USE_LAYOUT_URL, String.valueOf(breadcrumbConfiguration.useLayoutUrl())));
		boolean displayOnlyOneAssetCategoryBreadcrumb = Boolean.parseBoolean(
				preferences.getValue(PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB, String.valueOf(breadcrumbConfiguration.displayOnlyOneAssetCategoryBreadcrumb())));

		httpServletRequest.setAttribute(PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, categoryNavFilterVocabId);
		httpServletRequest.setAttribute(ParamKeys.CATEGORY_NAV_FILTER_VOCABS, groupVocabularies);
		httpServletRequest.setAttribute(ParamKeys.GLOBAL_GROUP_ID, globalGroupId);
		httpServletRequest.setAttribute(PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME, categoryNavRootEntryName);
		httpServletRequest.setAttribute(PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME, categoryNavUrlCategoryPropertyName);
		httpServletRequest.setAttribute(PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, categoryNavBuildCategoriesHierarchyUrlPropertyName);
		httpServletRequest.setAttribute(PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS, forceRootForLayoutBreadcrumbs);
		httpServletRequest.setAttribute(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, categoryNavShowCurrentLayout);
		httpServletRequest.setAttribute(PortletPreferenceKeys.USE_LAYOUT_URL, useLayoutUrl);
		httpServletRequest.setAttribute(PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB, displayOnlyOneAssetCategoryBreadcrumb);
		copyInheritedPreferencesIntoRequest(preferences, httpServletRequest);

		super.include(portletConfig, httpServletRequest, httpServletResponse);

	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		String categoryNavFilterVocabId = ParamUtil.getString(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID);
		String categoryNavRootEntryName = ParamUtil.getString(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME);
		String categoryNavUrlCategoryPropertyName = ParamUtil.getString(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME);
		String categoryNavBuildCategoriesHierarchyUrlPropertyName = ParamUtil.getString(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME);
		boolean categoryNavShowCurrentLayout = ParamUtil.getBoolean(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT);
		boolean useLayoutUrl = ParamUtil.getBoolean(actionRequest, PortletPreferenceKeys.USE_LAYOUT_URL);
		boolean forceRootForLayoutBreadcrumbs = ParamUtil.getBoolean(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS);
		boolean displayOnlyOneAssetCategoryBreadcrumb = ParamUtil.getBoolean(actionRequest, PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB);

		setPreference(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, categoryNavFilterVocabId);
		setPreference(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME, categoryNavRootEntryName);
		setPreference(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME, categoryNavUrlCategoryPropertyName);
		setPreference(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, categoryNavBuildCategoriesHierarchyUrlPropertyName);
		setPreference(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS, String.valueOf(forceRootForLayoutBreadcrumbs));
		setPreference(actionRequest, PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, String.valueOf(categoryNavShowCurrentLayout));
		setPreference(actionRequest, PortletPreferenceKeys.USE_LAYOUT_URL, String.valueOf(useLayoutUrl));
		setPreference(actionRequest, PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB, String.valueOf(displayOnlyOneAssetCategoryBreadcrumb));

		super.processAction(portletConfig, actionRequest, actionResponse);

	}

	private void copyInheritedPreferencesIntoRequest(PortletPreferences preferences, HttpServletRequest httpServletRequest) {
		String displayStyle = preferences.getValue(PortletPreferenceKeys.DISPLAY_STYLE, StringPool.BLANK);
		String displayStyleGroupId = preferences.getValue(PortletPreferenceKeys.DISPLAY_STYLE_GROUP_ID, StringPool.BLANK);

		httpServletRequest.setAttribute(PortletPreferenceKeys.DISPLAY_STYLE, displayStyle);
		httpServletRequest.setAttribute(PortletPreferenceKeys.DISPLAY_STYLE_GROUP_ID, displayStyleGroupId);
	}
}
