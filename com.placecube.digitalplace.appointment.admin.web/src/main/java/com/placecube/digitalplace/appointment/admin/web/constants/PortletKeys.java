package com.placecube.digitalplace.appointment.admin.web.constants;

public final class PortletKeys {

	public static final String APPOINTMENTS_MANAGER = "com_placecube_digitalplace_appointment_admin_web_AppointmentManagerPortlet";

	private PortletKeys() {
	}
}