<%@ include file="init.jsp" %>

<liferay-portlet:resourceURL copyCurrentRenderParameters="false" id="<%= MVCCommandKeys.RESOURCE_LOAD_EVENTS %>" var="loadEventsURL" />

<c:set var="portletNamespace"><portlet:namespace/></c:set>

<div id="${portletNamespace}calendar">
</div>

<aui:script>
	$(function() {
		var calendarEl = document.getElementById('${portletNamespace}calendar');
		var calendar = new FullCalendar.Calendar(calendarEl, {
			timeZone: '${timeZone.getID()}',
			locale: '${locale.toLanguageTag()}',
			initialView: 'dayGridMonth',
			eventDisplay: 'block',
			nowIndicator: true,
			headerToolbar: {
				left: 'prev,next today',
				center: 'title',
				right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
			},
			eventTimeFormat: {
				hour: '2-digit',
				minute: '2-digit',
				meridiem: false,
				hour12: false
			},
			eventDidMount: function(info) {
				var tooltip = new Tooltip(info.el, {
					title: info.event.title,
					placement: 'top',
					trigger: 'hover focus',
					container: '#${portletNamespace}calendar',
					template: '<div class="fc-tooltip" role="tooltip"><div class="fc-tooltip-arrow"></div><div class="fc-tooltip-inner"></div></div>',
					arrowSelector: ".fc-tooltip-arrow, .tooltip__arrow",
					innerSelector: ".fc-tooltip-inner, .tooltip__inner"
				});
			},
			navLinks: true, // can click day/week names to navigate views
			dayMaxEvents: true, // allow "more" link when too many events,
			startParam: "${portletNamespace}start",
			endParam: "${portletNamespace}end",
			timeZoneParam: "${portletNamespace}timeZone",
			handleWindowResize: true,
			expandRows: true,
			events: {
				url: '${loadEventsURL}',
				method: 'POST',
				failure: function() {
					alert('there was an error while fetching events!');
				}
			}
		});
		calendar.render();
	});

</aui:script>