package com.placecube.digitalplace.navigation.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.entry.rel.model.AssetEntryAssetCategoryRel;
import com.liferay.asset.entry.rel.service.AssetEntryAssetCategoryRelLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestrictionsFactoryUtil.class })
public class AssetCategoryLayoutServiceTest extends PowerMockito {

	@InjectMocks
	private AssetCategoryLayoutService assetCategoryLayoutService;

	private long ASSET_CATEGORY_ID = 3l;

	private long CLASS_NAME_ID = 1l;

	private long ENTRY_CLASS_PK_1 = 201l;

	private long ENTRY_CLASS_PK_2 = 202l;

	private long ENTRY_ID_1 = 101l;

	private long ENTRY_ID_2 = 102l;

	private long GROUP_ID = 4l;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private Criterion mockAssetCategoryIdCriterion;

	@Mock
	private AssetEntry mockAssetEntry1;

	@Mock
	private AssetEntry mockAssetEntry2;

	@Mock
	private AssetEntryAssetCategoryRel mockAssetEntryAssetCategoryRel1;

	@Mock
	private AssetEntryAssetCategoryRel mockAssetEntryAssetCategoryRel2;

	@Mock
	private AssetEntryAssetCategoryRelLocalService mockAssetEntryAssetCategoryRelLocalService;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private DynamicQuery mockCategoryRelQuery;

	@Mock
	private Criterion mockClassNameIdCriterion;

	@Mock
	private ClassNameLocalService mockClassNameLocalService;

	@Mock
	private Criterion mockEntryIdCriterion;

	@Mock
	private Criterion mockGroupIdCriterion;

	@Mock
	private Layout mockLayout;

	@Mock
	private Layout mockSystemLayout;

	@Mock
	private DynamicQuery mockLayoutAssetEntryQuery;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Before
	public void setUp() {
		mockStatic(RestrictionsFactoryUtil.class);
	}

	@Test
	public void getLayoutForCategory_WhenCategoryRelItemsNotExist_ThenReturnsEmptyOptional() throws PortalException {

		List<Object> assetEntryAssetCategoryRelItems = new ArrayList<>();

		when(mockAssetCategory.getCategoryId()).thenReturn(ASSET_CATEGORY_ID);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery()).thenReturn(mockCategoryRelQuery);
		when(RestrictionsFactoryUtil.eq("assetCategoryId", ASSET_CATEGORY_ID)).thenReturn(mockAssetCategoryIdCriterion);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery(mockCategoryRelQuery)).thenReturn(assetEntryAssetCategoryRelItems);

		Optional<Layout> returnedLayout = assetCategoryLayoutService.getLayoutForCategory(mockAssetCategory, GROUP_ID);

		verify(mockCategoryRelQuery, times(1)).add(mockAssetCategoryIdCriterion);
		verifyZeroInteractions(mockLayoutLocalService);
		assertFalse(returnedLayout.isPresent());
	}

	@Test
	public void getLayoutForCategory_WhenCategoryRelItemsExistAndLayoutAssetEntriesNotExist_ThenReturnsEmptyOptional() throws PortalException {

		List<Object> assetEntryAssetCategoryRelItems = new ArrayList<>();
		assetEntryAssetCategoryRelItems.add(mockAssetEntryAssetCategoryRel1);

		List<Object> assetEntries = new ArrayList<>();

		when(mockAssetCategory.getCategoryId()).thenReturn(ASSET_CATEGORY_ID);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery()).thenReturn(mockCategoryRelQuery);
		when(RestrictionsFactoryUtil.eq("assetCategoryId", ASSET_CATEGORY_ID)).thenReturn(mockAssetCategoryIdCriterion);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery(mockCategoryRelQuery)).thenReturn(assetEntryAssetCategoryRelItems);
		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockLayoutAssetEntryQuery);
		when(mockAssetEntryLocalService.dynamicQuery(mockLayoutAssetEntryQuery)).thenReturn(assetEntries);

		Optional<Layout> returnedLayout = assetCategoryLayoutService.getLayoutForCategory(mockAssetCategory, GROUP_ID);

		verify(mockCategoryRelQuery, times(1)).add(mockAssetCategoryIdCriterion);
		verifyZeroInteractions(mockLayoutLocalService);
		assertFalse(returnedLayout.isPresent());

	}

	@Test
	public void getLayoutForCategory_WhenCategoryRelItemsExistAndLayoutAssetEntriesExistAndFirstLayoutRetrievedIsNotNull_ThenReturnsFirstLayout() throws PortalException {

		List<Object> assetEntryAssetCategoryRelItems = new ArrayList<>();
		assetEntryAssetCategoryRelItems.add(mockAssetEntryAssetCategoryRel1);
		assetEntryAssetCategoryRelItems.add(mockAssetEntryAssetCategoryRel2);

		List<Object> assetEntries = new ArrayList<>();
		assetEntries.add(mockAssetEntry1);
		assetEntries.add(mockAssetEntry2);

		List<Object> assetEntryIds = new ArrayList<>();
		assetEntryIds.add(ENTRY_ID_1);
		assetEntryIds.add(ENTRY_ID_2);

		when(mockClassNameLocalService.getClassNameId(Layout.class)).thenReturn(CLASS_NAME_ID);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery()).thenReturn(mockCategoryRelQuery);
		when(mockAssetCategory.getCategoryId()).thenReturn(ASSET_CATEGORY_ID);
		when(RestrictionsFactoryUtil.eq("assetCategoryId", ASSET_CATEGORY_ID)).thenReturn(mockAssetCategoryIdCriterion);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery(mockCategoryRelQuery)).thenReturn(assetEntryAssetCategoryRelItems);

		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockLayoutAssetEntryQuery);
		when(RestrictionsFactoryUtil.eq("classNameId", CLASS_NAME_ID)).thenReturn(mockClassNameIdCriterion);
		when(RestrictionsFactoryUtil.eq("groupId", GROUP_ID)).thenReturn(mockGroupIdCriterion);
		when(RestrictionsFactoryUtil.in(eq("entryId"), ArgumentMatchers.anyList())).thenReturn(mockEntryIdCriterion);
		when(mockAssetEntryLocalService.dynamicQuery(mockLayoutAssetEntryQuery)).thenReturn(assetEntries);

		when(mockAssetEntry1.getClassPK()).thenReturn(ENTRY_CLASS_PK_1);
		when(mockAssetEntry2.getClassPK()).thenReturn(ENTRY_CLASS_PK_2);
		when(mockLayoutLocalService.fetchLayout(ENTRY_CLASS_PK_1)).thenReturn(mockLayout);

		Optional<Layout> returnedLayout = assetCategoryLayoutService.getLayoutForCategory(mockAssetCategory, GROUP_ID);

		InOrder inOrder = inOrder(mockCategoryRelQuery, mockLayoutAssetEntryQuery);
		inOrder.verify(mockCategoryRelQuery, times(1)).add(mockAssetCategoryIdCriterion);
		inOrder.verify(mockLayoutAssetEntryQuery, times(1)).add(mockClassNameIdCriterion);
		inOrder.verify(mockLayoutAssetEntryQuery, times(1)).add(mockGroupIdCriterion);
		inOrder.verify(mockLayoutAssetEntryQuery, times(1)).add(mockEntryIdCriterion);

		verify(mockLayoutLocalService, times(0)).fetchLayout(ENTRY_CLASS_PK_2);

		assertThat(returnedLayout.get(), sameInstance(mockLayout));

	}

	@Test
	public void getLayoutForCategory_WhenCategoryRelItemsExistAndLayoutAssetEntriesExistAndFirstLayoutRetrievedIsNull_ThenReturnsFirstNonNullLayout() throws PortalException {

		List<Object> assetEntryAssetCategoryRelItems = new ArrayList<>();
		assetEntryAssetCategoryRelItems.add(mockAssetEntryAssetCategoryRel1);
		assetEntryAssetCategoryRelItems.add(mockAssetEntryAssetCategoryRel2);

		List<Object> assetEntries = new ArrayList<>();
		assetEntries.add(mockAssetEntry1);
		assetEntries.add(mockAssetEntry2);

		List<Object> assetEntryIds = new ArrayList<>();
		assetEntryIds.add(ENTRY_ID_1);
		assetEntryIds.add(ENTRY_ID_2);

		Layout firstLayout = null;

		when(mockClassNameLocalService.getClassNameId(Layout.class)).thenReturn(CLASS_NAME_ID);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery()).thenReturn(mockCategoryRelQuery);
		when(mockAssetCategory.getCategoryId()).thenReturn(ASSET_CATEGORY_ID);
		when(RestrictionsFactoryUtil.eq("assetCategoryId", ASSET_CATEGORY_ID)).thenReturn(mockAssetCategoryIdCriterion);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery(mockCategoryRelQuery)).thenReturn(assetEntryAssetCategoryRelItems);

		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockLayoutAssetEntryQuery);
		when(RestrictionsFactoryUtil.eq("classNameId", CLASS_NAME_ID)).thenReturn(mockClassNameIdCriterion);
		when(RestrictionsFactoryUtil.eq("groupId", GROUP_ID)).thenReturn(mockGroupIdCriterion);
		when(RestrictionsFactoryUtil.in(eq("entryId"), ArgumentMatchers.anyList())).thenReturn(mockEntryIdCriterion);
		when(mockAssetEntryLocalService.dynamicQuery(mockLayoutAssetEntryQuery)).thenReturn(assetEntries);

		when(mockAssetEntry1.getClassPK()).thenReturn(ENTRY_CLASS_PK_1);
		when(mockAssetEntry2.getClassPK()).thenReturn(ENTRY_CLASS_PK_2);
		when(mockLayoutLocalService.fetchLayout(ENTRY_CLASS_PK_1)).thenReturn(firstLayout);
		when(mockLayoutLocalService.fetchLayout(ENTRY_CLASS_PK_2)).thenReturn(mockLayout);

		Optional<Layout> returnedLayout = assetCategoryLayoutService.getLayoutForCategory(mockAssetCategory, GROUP_ID);

		InOrder inOrder = inOrder(mockCategoryRelQuery, mockLayoutAssetEntryQuery);
		inOrder.verify(mockCategoryRelQuery, times(1)).add(mockAssetCategoryIdCriterion);
		inOrder.verify(mockLayoutAssetEntryQuery, times(1)).add(mockClassNameIdCriterion);
		inOrder.verify(mockLayoutAssetEntryQuery, times(1)).add(mockGroupIdCriterion);
		inOrder.verify(mockLayoutAssetEntryQuery, times(1)).add(mockEntryIdCriterion);

		assertThat(returnedLayout.get(), sameInstance(mockLayout));

	}

	@Test
	public void getLayoutForCategory_WhenCategoryRelItemsExistAndLayoutAssetEntriesExistAndFirstLayoutRetrievedIsSystem_ThenReturnsFirstNonSystemLayout() throws PortalException {

		List<Object> assetEntryAssetCategoryRelItems = new ArrayList<>();
		assetEntryAssetCategoryRelItems.add(mockAssetEntryAssetCategoryRel1);
		assetEntryAssetCategoryRelItems.add(mockAssetEntryAssetCategoryRel2);

		List<Object> assetEntries = new ArrayList<>();
		assetEntries.add(mockAssetEntry1);
		assetEntries.add(mockAssetEntry2);

		List<Object> assetEntryIds = new ArrayList<>();
		assetEntryIds.add(ENTRY_ID_1);
		assetEntryIds.add(ENTRY_ID_2);

		when(mockSystemLayout.isSystem()).thenReturn(true);

		when(mockClassNameLocalService.getClassNameId(Layout.class)).thenReturn(CLASS_NAME_ID);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery()).thenReturn(mockCategoryRelQuery);
		when(mockAssetCategory.getCategoryId()).thenReturn(ASSET_CATEGORY_ID);
		when(RestrictionsFactoryUtil.eq("assetCategoryId", ASSET_CATEGORY_ID)).thenReturn(mockAssetCategoryIdCriterion);
		when(mockAssetEntryAssetCategoryRelLocalService.dynamicQuery(mockCategoryRelQuery)).thenReturn(assetEntryAssetCategoryRelItems);

		when(mockAssetEntryLocalService.dynamicQuery()).thenReturn(mockLayoutAssetEntryQuery);
		when(RestrictionsFactoryUtil.eq("classNameId", CLASS_NAME_ID)).thenReturn(mockClassNameIdCriterion);
		when(RestrictionsFactoryUtil.eq("groupId", GROUP_ID)).thenReturn(mockGroupIdCriterion);
		when(RestrictionsFactoryUtil.in(eq("entryId"), ArgumentMatchers.anyList())).thenReturn(mockEntryIdCriterion);
		when(mockAssetEntryLocalService.dynamicQuery(mockLayoutAssetEntryQuery)).thenReturn(assetEntries);

		when(mockAssetEntry1.getClassPK()).thenReturn(ENTRY_CLASS_PK_1);
		when(mockAssetEntry2.getClassPK()).thenReturn(ENTRY_CLASS_PK_2);
		when(mockLayoutLocalService.fetchLayout(ENTRY_CLASS_PK_1)).thenReturn(mockSystemLayout);
		when(mockLayoutLocalService.fetchLayout(ENTRY_CLASS_PK_2)).thenReturn(mockLayout);

		Optional<Layout> returnedLayout = assetCategoryLayoutService.getLayoutForCategory(mockAssetCategory, GROUP_ID);

		InOrder inOrder = inOrder(mockCategoryRelQuery, mockLayoutAssetEntryQuery);
		inOrder.verify(mockCategoryRelQuery, times(1)).add(mockAssetCategoryIdCriterion);
		inOrder.verify(mockLayoutAssetEntryQuery, times(1)).add(mockClassNameIdCriterion);
		inOrder.verify(mockLayoutAssetEntryQuery, times(1)).add(mockGroupIdCriterion);
		inOrder.verify(mockLayoutAssetEntryQuery, times(1)).add(mockEntryIdCriterion);

		assertThat(returnedLayout.get(), sameInstance(mockLayout));

	}

}
