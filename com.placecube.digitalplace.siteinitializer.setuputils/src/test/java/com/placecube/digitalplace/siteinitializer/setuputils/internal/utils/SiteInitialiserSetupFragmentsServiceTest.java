package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.list.model.AssetListEntryUsage;
import com.liferay.fragment.model.FragmentEntryLink;
import com.liferay.fragment.service.FragmentEntryLinkLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;

public class SiteInitialiserSetupFragmentsServiceTest extends PowerMockito {

	@Mock
	private AssetListEntryUsage mockAssetListEntryUsage;

	@Mock
	private Layout mockDraftLayout;

	@Mock
	private FragmentEntryLink mockFragmentEntryLink1;

	@Mock
	private FragmentEntryLink mockFragmentEntryLink2;

	@Mock
	private FragmentEntryLink mockFragmentEntryLink3;

	@Mock
	private FragmentEntryLink mockFragmentEntryLink4;

	@Mock
	private FragmentEntryLinkLocalService mockFragmentEntryLinkLocalService;

	@Mock
	private Layout mockLayout;

	@Mock
	private SiteInitialiserDataRetrieval mockSiteInitialiserDataRetrieval;

	@InjectMocks
	private SiteInitialiserSetupFragmentsService siteInitialiserSetupFragmentsService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void updateFragmentEntryLinksInLayout_WhenLayoutIsNotNullAndAssetListEntryUsageFoundAndLayoutHasDraft_ThenUpdatesAllRelevantFragmentsWithThePlaceholderValueForTheLayoutAndItsDraftLayout()
			throws PortalException {
		String containerKey = "myContainerKey";
		long userId = 2;
		long layoutGroupId1 = 12;
		long layoutPlid1 = 13;
		long layoutGroupId2 = 22;
		long layoutPlid2 = 23;
		long fragmentId1 = 55;
		long fragmentId2 = 66;
		when(mockSiteInitialiserDataRetrieval.findAssetListEntryUsage(mockLayout, "key")).thenReturn(Optional.of(mockAssetListEntryUsage));
		when(mockLayout.getGroupId()).thenReturn(layoutGroupId1);
		when(mockLayout.getPlid()).thenReturn(layoutPlid1);
		when(mockDraftLayout.getGroupId()).thenReturn(layoutGroupId2);
		when(mockDraftLayout.getPlid()).thenReturn(layoutPlid2);
		when(mockAssetListEntryUsage.getContainerKey()).thenReturn(containerKey);
		when(mockFragmentEntryLinkLocalService.getFragmentEntryLinksByPlid(layoutGroupId1, layoutPlid1))
				.thenReturn(Arrays.asList(mockFragmentEntryLink1, mockFragmentEntryLink2, mockFragmentEntryLink3));
		when(mockFragmentEntryLink1.getEditableValues()).thenReturn("");
		when(mockFragmentEntryLink2.getEditableValues()).thenReturn("myEditableValuesWithoutPlaceholder");
		when(mockFragmentEntryLink3.getEditableValues()).thenReturn("myEditableValuesWithplaceholderValueAndSomeMore");
		when(mockFragmentEntryLink3.getFragmentEntryLinkId()).thenReturn(fragmentId1);
		when(mockFragmentEntryLink4.getEditableValues()).thenReturn("myEditableValuesWith_placeholderValue_AndSomeMore");
		when(mockFragmentEntryLink4.getFragmentEntryLinkId()).thenReturn(fragmentId2);
		when(mockLayout.fetchDraftLayout()).thenReturn(mockDraftLayout);
		when(mockFragmentEntryLinkLocalService.getFragmentEntryLinksByPlid(layoutGroupId2, layoutPlid2)).thenReturn(Arrays.asList(mockFragmentEntryLink1, mockFragmentEntryLink4));

		siteInitialiserSetupFragmentsService.updateFragmentEntryLinksInLayout(userId, mockLayout, "key", "placeholderValue");

		verify(mockFragmentEntryLinkLocalService, times(1)).updateFragmentEntryLink(userId, fragmentId1, "myEditableValuesWithmyContainerKeyAndSomeMore");
		verify(mockFragmentEntryLinkLocalService, times(1)).updateFragmentEntryLink(userId, fragmentId2, "myEditableValuesWith_myContainerKey_AndSomeMore");
	}

	@Test
	public void updateFragmentEntryLinksInLayout_WhenLayoutIsNotNullAndAssetListEntryUsageFoundAndLayoutHasNoDraft_ThenUpdatesAllRelevantFragmentsWithThePlaceholderValueForTheLayoutOnly()
			throws PortalException {
		String containerKey = "myContainerKey";
		long layoutGroupId1 = 12;
		long layoutPlid1 = 13;
		long fragmentId1 = 55;
		long userId = 2;
		when(mockSiteInitialiserDataRetrieval.findAssetListEntryUsage(mockLayout, "key")).thenReturn(Optional.of(mockAssetListEntryUsage));
		when(mockLayout.getGroupId()).thenReturn(layoutGroupId1);
		when(mockLayout.getPlid()).thenReturn(layoutPlid1);
		when(mockAssetListEntryUsage.getContainerKey()).thenReturn(containerKey);
		when(mockFragmentEntryLinkLocalService.getFragmentEntryLinksByPlid(layoutGroupId1, layoutPlid1))
				.thenReturn(Arrays.asList(mockFragmentEntryLink1, mockFragmentEntryLink2, mockFragmentEntryLink3));
		when(mockFragmentEntryLink1.getEditableValues()).thenReturn("");
		when(mockFragmentEntryLink2.getEditableValues()).thenReturn("myEditableValuesWithoutPlaceholder");
		when(mockFragmentEntryLink3.getEditableValues()).thenReturn("myEditableValuesWithplaceholderValueAndSomeMore");
		when(mockFragmentEntryLink3.getFragmentEntryLinkId()).thenReturn(fragmentId1);
		when(mockLayout.fetchDraftLayout()).thenReturn(null);

		siteInitialiserSetupFragmentsService.updateFragmentEntryLinksInLayout(userId, mockLayout, "key", "placeholderValue");

		verify(mockFragmentEntryLinkLocalService, times(1)).updateFragmentEntryLink(userId, fragmentId1, "myEditableValuesWithmyContainerKeyAndSomeMore");
	}

	@Test
	public void updateFragmentEntryLinksInLayout_WhenLayoutIsNotNullAndAssetListEntryUsageNotFound_ThenNoActionIsPerformed() throws PortalException {
		long userId = 2;
		when(mockSiteInitialiserDataRetrieval.findAssetListEntryUsage(mockDraftLayout, "key")).thenReturn(Optional.empty());

		siteInitialiserSetupFragmentsService.updateFragmentEntryLinksInLayout(userId, mockLayout, "key", "placeholderValue");

		verifyZeroInteractions(mockFragmentEntryLinkLocalService);
	}

	@Test
	public void updateFragmentEntryLinksInLayout_WhenLayoutIsNull_ThenNoActionIsPerformed() throws PortalException {
		long userId = 2;

		siteInitialiserSetupFragmentsService.updateFragmentEntryLinksInLayout(userId, null, "key", "placeholderValue");

		verifyZeroInteractions(mockFragmentEntryLinkLocalService, mockSiteInitialiserDataRetrieval);
	}

}
