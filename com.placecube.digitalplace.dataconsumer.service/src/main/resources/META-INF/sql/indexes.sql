create index IX_E12E0244 on Placecube_DataConsumer_DataConsumer (companyId, groupId);
create index IX_7FFE85A6 on Placecube_DataConsumer_DataConsumer (ddmDataProviderInstanceId);
create index IX_49B3D62F on Placecube_DataConsumer_DataConsumer (ddmFormInstanceId);
create index IX_A8F131B2 on Placecube_DataConsumer_DataConsumer (uuid_[$COLUMN_LENGTH:75$]);