package com.placecube.digitalplace.userimport.admin.web.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailService;
import com.liferay.petra.string.StringUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.resource.bundle.AggregateResourceBundleLoader;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoader;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoaderUtil;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.userimport.admin.web.constants.NotificationArticle;
import com.placecube.digitalplace.userimport.admin.web.model.ImportResultWrapper;
import com.placecube.digitalplace.userimport.admin.web.model.ModelFactoryBuilder;
import com.placecube.digitalplace.userimport.admin.web.model.UserImportResult;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = UserImportNotificationService.class)
public class UserImportNotificationService {

	private static final Log LOG = LogFactoryUtil.getLog(UserImportNotificationService.class);

	private static final String PLACEHOLDER_IMPORT_RESULT = "[$IMPORT_RESULT$]";

	@Reference
	private EmailWebContentService emailWebContentService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private MailService mailService;

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	@Reference
	private UserLocalService userLocalService;

	public void createMissingArticle(NotificationArticle notificationArticle, JournalFolder journalFolder, DDMStructure ddmStructure, boolean indexable, ServiceContext serviceContext)
			throws PortalException, IOException {

		String content = StringUtil.read(getClass().getResourceAsStream(notificationArticle.getPath()));
		JournalArticleContext journalArticleContext = JournalArticleContext.init(notificationArticle.getArticleId(), notificationArticle.getArticleTitle(), content);
		journalArticleContext.setDDMStructure(ddmStructure);
		journalArticleContext.setJournalFolder(journalFolder);
		journalArticleContext.setIndexable(indexable);
		journalArticleCreationService.getOrCreateArticle(journalArticleContext, serviceContext);
	}

	public DDMStructure getEmailStructure(ServiceContext serviceContext) throws PortalException {
		return emailWebContentService.getOrCreateDDMStructure(serviceContext);
	}

	public JournalFolder getOrCreateFolder(ServiceContext serviceContext) throws PortalException {
		return journalArticleCreationService.getOrCreateJournalFolder("User import", serviceContext);
	}

	public ResourceBundle getResourceBundle(Locale locale, String... bundleSymbolicNames) {

		List<ResourceBundleLoader> resourceBundleLoaders = new ArrayList<>();
		ResourceBundleLoader portalResourceBundleLoader = ResourceBundleLoaderUtil.getPortalResourceBundleLoader();
		resourceBundleLoaders.add(portalResourceBundleLoader);

		for (String bundleSymbolicName : bundleSymbolicNames) {

			ResourceBundleLoader bundleResourceBundleLoader = ResourceBundleLoaderUtil.getResourceBundleLoaderByBundleSymbolicName(bundleSymbolicName);
			resourceBundleLoaders.add(bundleResourceBundleLoader);

		}

		ResourceBundleLoader[] resourceBundleLoadersArray = resourceBundleLoaders.toArray(new ResourceBundleLoader[resourceBundleLoaders.size()]);
		AggregateResourceBundleLoader aggregateResourceBundleLoader = new AggregateResourceBundleLoader(resourceBundleLoadersArray);

		return aggregateResourceBundleLoader.loadResourceBundle(locale);

	}

	public void notifyUserOfImportCompleted(ServiceContext serviceContext, ImportResultWrapper importResults) {

		try {
			String importResultText = getImportResultText(importResults, serviceContext.getLocale());

			LOG.info("Notifying user of user import. Results:");
			LOG.info(importResultText);

			notifyUser(serviceContext, NotificationArticle.IMPORT_COMPLETED, importResultText);

		} catch (Exception e) {
			LOG.error("Unable to notify user of completed user data import", e);
		}
	}

	public void notifyUserOfImportFailed(ServiceContext serviceContext, String exceptionMessage) {
		try {
			LOG.info("Notifying user of import failure");

			notifyUser(serviceContext, NotificationArticle.IMPORT_FAILED, exceptionMessage);
		} catch (Exception e) {
			LOG.error("Unable to notify user of failed user data import", e);
		}
	}

	private String getImportResultText(ImportResultWrapper importResults, Locale locale) {

		List<UserImportResult> userImportResults = importResults.getUserImportResults();

		StringBuilder importResultText = new StringBuilder();

		ResourceBundle resourceBundle = getResourceBundle(locale, "com.placecube.digitalplace.userimport.admin.web");

		importResultText.append(importResults.getFriendlyGroupAssignmentMessage(resourceBundle));

		importResultText.append("<ul>");

		for (UserImportResult userImportResult : userImportResults) {
			String importResultMessage = userImportResult.getFriendlyImportResultMessage(resourceBundle);
			importResultText.append("<li>").append(importResultMessage).append("</li>");
		}

		importResultText.append("</ul>");

		return importResultText.toString();
	}

	private void notifyUser(ServiceContext serviceContext, NotificationArticle article, String importResultText) throws PortalException {
		User userToNotify = userLocalService.getUser(serviceContext.getUserId());
		long globalGroupId = groupLocalService.getCompanyGroup(userToNotify.getCompanyId()).getGroupId();
		JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticle(globalGroupId, article.getArticleId());
		Optional<String> subjectField = journalArticleRetrievalService.getFieldValue(journalArticle, "Subject", serviceContext.getLocale());
		Optional<String> bodyField = journalArticleRetrievalService.getFieldValue(journalArticle, "Body", serviceContext.getLocale());

		if (bodyField.isPresent()) {
			String bodyText = bodyField.get();
			Map<String, Object> personalisation = new HashMap<>();
			personalisation.put(PLACEHOLDER_IMPORT_RESULT, importResultText);
			bodyText = substituteVariables(bodyText, personalisation);

			sendMail(userToNotify.getEmailAddress(), "noreply@placecube.com", subjectField.orElse("User import report"), bodyText);
		} else {
			LOG.error("NotificationArticle body is blank so not sending notification");
		}
	}

	private void sendMail(String to, String from, String subject, String body) throws PortalException {
		try {
			InternetAddress toAddress = modelFactoryBuilder.createInternetAddress(to);
			InternetAddress fromAddress = modelFactoryBuilder.createInternetAddress(from);

			MailMessage mailMessage = modelFactoryBuilder.createMailMessage();
			mailMessage.setTo(toAddress);
			mailMessage.setFrom(fromAddress);
			mailMessage.setSubject(subject);
			mailMessage.setBody(body);
			mailMessage.setHTMLFormat(true);

			mailService.sendEmail(mailMessage);
		} catch (AddressException e) {
			throw new PortalException("Error sending mail: " + e.getMessage(), e);
		}
	}

	private String substituteVariables(String text, Map<String, Object> variables) {
		for (Map.Entry<String, Object> variable : variables.entrySet()) {
			text = text.replace(variable.getKey(), String.valueOf(variable.getValue()));
		}

		return text;
	}

}
