package com.placecube.digitalplace.search.rankings.contributor;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.engine.adapter.search.SearchSearchResponse;
import com.liferay.portal.search.hits.SearchHits;
import com.liferay.portal.search.searcher.SearchRequest;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.SearchRequestBuilderFactory;
import com.liferay.portal.search.spi.searcher.SearchRequestContributor;
import com.placecube.digitalplace.search.rankings.model.SearchRanking;
import com.placecube.digitalplace.search.rankings.service.RankingsConfigurationService;
import com.placecube.digitalplace.search.rankings.service.RankingsRegexService;
import com.placecube.digitalplace.search.rankings.service.RankingsService;

@Component(immediate = true, property = "search.request.contributor.id=com.placecube.digitalplace.search.rankings.wildcard.contributor", service = SearchRequestContributor.class)
public class WildcardSearchRequestContributor implements SearchRequestContributor {

	@Reference
	private RankingsService rankingsService;

	@Reference
	private RankingsRegexService searchRegexService;

	@Reference
	private SearchRequestBuilderFactory searchRequestBuilderFactory;

	@Reference
	private RankingsConfigurationService rankingsConfigurationService;

	@Override
	public SearchRequest contribute(SearchRequest originalSearchRequest) {
		SearchRequestBuilder searchRequestBuilder = searchRequestBuilderFactory.builder(originalSearchRequest);
		long[] companyIds = new long[1];
		Locale[] locale = new Locale[1];

		searchRequestBuilder.withSearchContext(searchContext -> {
			companyIds[0] = searchContext.getCompanyId();
			locale[0] = searchContext.getLocale();
		});

		String keywords = originalSearchRequest.getQueryString();
		if (rankingsConfigurationService.isEnableWildcardRankings(companyIds[0]) && Validator.isNotNull(keywords)) {
			processSearchRankings(searchRequestBuilder, keywords, locale[0], companyIds[0]);
		}

		return searchRequestBuilder.build();

	}

	private void processSearchRankings(SearchRequestBuilder searchRequestBuilder, String keywords, Locale locale, long companyId) {

		keywords = keywords.toLowerCase(locale);

		SearchSearchResponse searchRankingsSearchResponse = rankingsService.getSearchRankingsForKeywords(keywords, companyId);
		SearchHits searchRankingsHits = searchRankingsSearchResponse.getSearchHits();

		List<SearchRanking> enabledSearchRankings = searchRankingsHits.getSearchHits().stream().map(hit -> SearchRanking.fromSearchDocument(hit.getDocument()))
				.filter(searchRanking -> !searchRanking.isDisabled()).collect(Collectors.toList());

		for (SearchRanking searchRanking : enabledSearchRankings) {

			boolean matched = false;
			for (String alias : searchRanking.getAliases()) {
				alias = alias.toLowerCase(locale);

				if (rankingsService.isAliasWildcard(alias)) {
					matched = matchWildcardAlias(searchRequestBuilder, alias, searchRanking.getPinnedIds(), keywords);
				} else {
					matched = matchExactAlias(searchRequestBuilder, alias, searchRanking.getPinnedIds(), keywords);
				}

				if (matched) {
					break;
				}
			}

			if (matched) {
				rankingsService.addBlockedIdsQuery(searchRequestBuilder, searchRanking);
				break;
			}
		}

	}

	private boolean matchExactAlias(SearchRequestBuilder searchRequestBuilder, String alias, List<Map<String, String>> pinValues, String keywords) {
		if (keywords.equals(alias)) {
			rankingsService.addPinnedIdsQueries(searchRequestBuilder, pinValues);
			return true;
		}

		return false;
	}

	private boolean matchWildcardAlias(SearchRequestBuilder searchRequestBuilder, String alias, List<Map<String, String>> pinValues, String keywords) {

		String queryStringRegex = searchRegexService.getAliasWildcardRegex(alias);

		if (Pattern.matches(queryStringRegex, keywords)) {
			rankingsService.addPinnedIdsQueries(searchRequestBuilder, pinValues);
			return true;
		}

		return false;
	}

}
