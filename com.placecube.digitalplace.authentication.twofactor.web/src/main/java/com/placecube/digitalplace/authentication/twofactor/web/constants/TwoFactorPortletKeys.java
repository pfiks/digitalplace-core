package com.placecube.digitalplace.authentication.twofactor.web.constants;

public final class TwoFactorPortletKeys {

	public static final String TWO_FACTOR_AUTHENTICATION = "com_placecube_digitalplace_authentication_twofactor_web_TwoFactorAuthenticationPortlet";

	private TwoFactorPortletKeys() {
	}

}
