package com.placecube.digitalplace.payment.capitapaymentinterface.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleItemConfiguration;

public class SimpleSaleItemConfigurationTest {

	@Test
	public void putSaleItemDetail() {

		SimpleSaleItemConfiguration simpleSaleItemConfiguration = SimpleSaleItemConfiguration.newSimpleSaleItemConfiguration();
		String itemDetailKey = "key";
		String itemDetailValue = "value";

		simpleSaleItemConfiguration.putSaleItemDetail(itemDetailKey, itemDetailValue);

		String result = simpleSaleItemConfiguration.getSaleItemDetail(itemDetailKey);

		assertEquals(itemDetailValue, result);

	}

	@Test
	public void getSaleItemDetail() {

		SimpleSaleItemConfiguration simpleSaleItemConfiguration = SimpleSaleItemConfiguration.newSimpleSaleItemConfiguration();
		String itemDetailKey = "key";
		String itemDetailValue = "value";

		simpleSaleItemConfiguration.putSaleItemDetail(itemDetailKey, itemDetailValue);

		String result = simpleSaleItemConfiguration.getSaleItemDetail(itemDetailKey);

		assertEquals(itemDetailValue, result);

	}

}
