package com.placecube.digitalplace.search.web.portlet.sortby.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.web.portlet.sortby.service.SortByService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SortByPortletConfigurationActionTest extends PowerMockito {

	private static final long COMPANY_ID = 9;

	private static final Locale LOCALE_ENGLISH = Locale.ENGLISH;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private SortByPortletInstanceConfiguration mockConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private List<KeyValuePair> mocKeyValuePair;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private SortByService mockSortByService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SortByPortletConfigurationAction sortByPortletConfigurationAction;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void include_WhenNoError_ThenSetsConfigurationAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		mockConfiguration();

		sortByPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("configuration", mockConfiguration);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheAvailableSortByOptionsKeyValuePairAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String[] enabledSortByOptions = { "opt1", "opt2" };
		mockConfiguration();
		when(mockConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);

		when(mockSortByService.getAvailableSortByOptionsKeyValuePair(Arrays.asList(enabledSortByOptions), LOCALE_ENGLISH)).thenReturn(mocKeyValuePair);

		sortByPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("availableSortByOptionsKeyValuePair", mocKeyValuePair);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheEnabledSortByOptionsKeyValuePairAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String[] enabledSortByOptions = { "opt1", "opt2" };
		mockConfiguration();
		when(mockConfiguration.enabledSortByOptions()).thenReturn(enabledSortByOptions);

		when(mockSortByService.getEnabledSortByOptionsKeyValuePair(enabledSortByOptions, LOCALE_ENGLISH)).thenReturn(mocKeyValuePair);

		sortByPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("enabledSortByOptionsKeyValuePair", mocKeyValuePair);
	}

	@Test
	@Parameters({ "true", "false" })
	public void processAction_WhenNoError_ThenSavesChainSortByOptionsInPreference(String expected) throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		when(ParamUtil.getString(mockActionRequest, "chainSortByOptions", Boolean.FALSE.toString())).thenReturn(expected);
		sortByPortletConfigurationAction = spy(new SortByPortletConfigurationAction());

		sortByPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(sortByPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "chainSortByOptions", expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesDestinationPageInPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String[] expected = { "opt1", "opt2" };
		when(ParamUtil.getStringValues(mockActionRequest, "enabledSortByOptions", new String[0])).thenReturn(expected);
		sortByPortletConfigurationAction = spy(new SortByPortletConfigurationAction());

		sortByPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(sortByPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "enabledSortByOptions", expected);
	}

	@Test
	@Parameters({ "true", "false" })
	public void processAction_WhenNoError_ThenSavesDisplayPortletInPreference(String expected) throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		when(ParamUtil.getString(mockActionRequest, "displayPortlet", Boolean.TRUE.toString())).thenReturn(expected);
		sortByPortletConfigurationAction = spy(new SortByPortletConfigurationAction());

		sortByPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(sortByPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "displayPortlet", expected);
	}

	@Test
	@Parameters({ "true", "false" })
	public void processAction_WhenNoError_ThenSavesSortDirectionChangeableInPreference(String expected) throws Exception {

		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		when(ParamUtil.getString(mockActionRequest, "sortDirectionChangeable", Boolean.FALSE.toString())).thenReturn(expected);
		sortByPortletConfigurationAction = spy(new SortByPortletConfigurationAction());

		sortByPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(sortByPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "sortDirectionChangeable", expected);
	}

	private void mockConfiguration() throws ConfigurationException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(LOCALE_ENGLISH);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.enabledSortByOptions()).thenReturn(new String[0]);
	}

}
