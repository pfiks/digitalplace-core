package com.placecube.digitalplace.form.model.impl;

import java.net.URL;
import java.util.List;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.form.model.FormDefinition;

public class FormDefinitionImpl implements FormDefinition {

	private String name;
	private String description;
	private String category;
	private String key;
	private String journalArticleFolderName;
	private URL ddmForm;
	private URL ddmFormLayout;
	private List<URL> journalArticles;
	private URL settingsDDMFormValues;
	private List<URL> dataProviderDDMFormValues;

	public FormDefinitionImpl(String name, String description, String key) {
		this.description = description;
		this.name = name;
		this.key = key;
	}

	public FormDefinitionImpl(String name, String description, String category, String key, String journalArticleFolderName, URL ddmForm, URL ddmFormLayout, URL settingsDDMFormValues,
			List<URL> journalArticles, List<URL> dataProviderDDMFormValues) {

		this.category = category;
		this.description = description;
		this.key = key;
		this.ddmForm = ddmForm;
		this.ddmFormLayout = ddmFormLayout;
		this.settingsDDMFormValues = settingsDDMFormValues;
		this.name = name;
		this.journalArticles = journalArticles;
		this.journalArticleFolderName = journalArticleFolderName;
		this.dataProviderDDMFormValues = dataProviderDDMFormValues;
	}

	@Override
	public String getCategory() {
		return category;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getKey() {
		return key.replace(StringPool.SPACE, StringPool.DASH).toLowerCase();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public URL getUrlToDDMFormDefinitionFile() {
		return ddmForm;
	}

	@Override
	public URL getUrlToDDMFormLayoutDefinitionFile() {
		return ddmFormLayout;
	}

	@Override
	public URL getUrlToSettingsDDMFormValuesDefinitionFile() {
		return settingsDDMFormValues;
	}

	@Override
	public String toString() {
		return "FormDefinition [name=" + name + ",key=" + key + ", category=" + category + "]";
	}

	@Override
	public List<URL> getUrlsToJournalArticles() {
		return journalArticles;
	}

	@Override
	public String getJournalArticleFolderName() {
		return journalArticleFolderName;
	}

	@Override
	public List<URL> getUrlsToDataProviderDDMFormValuesDefinitionFiles() {
		return dataProviderDDMFormValues;
	}

}
