package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.util.DDMIndexer;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.constants.JsonFilterConfigKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model.ComparisonOperator;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model.RangeSearchFilter;

@Component(immediate = true, service = DDLFieldFilterFactoryService.class)
public class DDLFieldFilterFactoryService {

	private static final String DATE_TYPE = "date";

	@Reference
	private DDLFieldFilterUtcDateService ddlFilterUtcDateService;

	@Reference
	private DDMIndexer ddmIndexer;

	private static final Log LOG = LogFactoryUtil.getLog(DDLFieldFilterFactoryService.class);

	public RangeSearchFilter getRangeFilterForOption(JSONObject optionJson, long ddmStructureId, Locale locale) {

		JSONArray filters = optionJson.getJSONArray(JsonFilterConfigKeys.FILTER_FILTERS);
		RangeSearchFilter rangeSearchFilter = RangeSearchFilter.newInstance();

		for (int i = 0; i < filters.length(); i++) {
			JSONObject filter = filters.getJSONObject(i);
			String fieldType = filter.getString("fieldType");

			if (DATE_TYPE.equals(fieldType)) {
				addDateRangeFilter(rangeSearchFilter, filter);
			} else {
				addNumberRangeFilter(rangeSearchFilter, filter, ddmStructureId, locale);
			}
		}

		return rangeSearchFilter;
	}

	private void addDateRangeFilter(RangeSearchFilter rangeSearchFilter, JSONObject filter) {

		String fieldName = filter.getString("fieldName");

		LocalDate date = getReferenceLocalDate(filter);

		int numDays = GetterUtil.getInteger(filter.getString("numDays"));
		ComparisonOperator comparisonOperator = ComparisonOperator.fromKey(filter.getString("filterOperator"));

		long timeInMillisvalue = ddlFilterUtcDateService.getDateWithDaysOffsetInMillis(date, numDays);
		String filterValue = String.valueOf(timeInMillisvalue);

		rangeSearchFilter.addUnboundedRange(fieldName + "_sortable", filterValue, comparisonOperator, BooleanClauseOccur.MUST);
	}

	private void addNumberRangeFilter(RangeSearchFilter rangeSearchFilter, JSONObject filter, long ddmStructureId, Locale locale) {

		String fieldName = filter.getString("fieldName");
		String filterValue = filter.getString("fieldValue");

		int numberFilterValue = GetterUtil.getInteger(filterValue);

		String indexerFieldName = ddmIndexer.encodeName(ddmStructureId, fieldName, locale) + "_Number_sortable";

		ComparisonOperator comparisonOperator = ComparisonOperator.fromKey(filter.getString("filterOperator"));

		rangeSearchFilter.addUnboundedRange(indexerFieldName, String.valueOf(numberFilterValue), comparisonOperator, BooleanClauseOccur.MUST);
	}

	private LocalDate getReferenceLocalDate(JSONObject filter) {
		String dateValue = filter.getString("fieldValue");

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

		if (Validator.isNotNull(dateValue)) {

			try {
				return LocalDate.parse(dateValue, formatter);
			} catch (DateTimeParseException e) {
				LOG.error(e);
			}
		}

		return LocalDate.now();
	}
}
