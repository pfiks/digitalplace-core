package com.placecube.digitalplace.address.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Response.class, JSONFactoryUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AddressRESTApplicationTest extends PowerMockito {

	@InjectMocks
	private AddressRESTApplication addressRESTApplication;

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private AddressContext mockAddressContext2;

	@Mock
	private AddressLookupService mockAddressLookupService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Response mockResponse;

	@Mock
	private ResponseBuilder mockResponseBuilder;

	@Before
	public void activateSetup() {
		mockStatic(Response.class, JSONFactoryUtil.class);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getAddressByUPRN_WhenAddressFound_ThenReturnsTheResponseWithTheAddress(boolean fallbackToNationalLookup) throws PortalException {
		String uprn = "myValue";
		String[] paramsAndValues = new String[] { "test1", "test2" };
		long companyId = 12;
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(companyId);
		when(mockAddressLookupService.getByUprn(companyId, uprn, paramsAndValues, fallbackToNationalLookup)).thenReturn(Optional.of(mockAddressContext));
		when(Response.status(Status.OK)).thenReturn(mockResponseBuilder);
		when(JSONFactoryUtil.looseSerialize(mockAddressContext)).thenReturn("responseValue");
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response result = addressRESTApplication.getAddressByUPRN(fallbackToNationalLookup, uprn, paramsAndValues, mockHttpServletRequest);

		assertThat(result, sameInstance(mockResponse));

		InOrder inOrder = inOrder(mockResponseBuilder);
		inOrder.verify(mockResponseBuilder, times(1)).entity("responseValue");
		inOrder.verify(mockResponseBuilder, times(1)).build();
	}

	@Test
	@Parameters({ "true", "false" })
	public void getAddressByUPRN_WhenAddressNotFound_ThenReturnsEmptyTheResponse(boolean fallbackToNationalLookup) throws PortalException {
		String uprn = "myValue";
		String[] paramsAndValues = new String[] { "test1", "test2" };
		long companyId = 12;
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(companyId);
		when(mockAddressLookupService.getByUprn(companyId, uprn, paramsAndValues, fallbackToNationalLookup)).thenReturn(Optional.empty());
		when(Response.status(Status.OK)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response result = addressRESTApplication.getAddressByUPRN(fallbackToNationalLookup, uprn, paramsAndValues, mockHttpServletRequest);

		assertThat(result, sameInstance(mockResponse));

		verify(mockResponseBuilder, never()).entity(anyString());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getAddressByUPRN_WhenExceptionRetrievingAddress_ThenReturnsErrorResponse(boolean fallbackToNationalLookup) throws PortalException {
		String uprn = "myValue";
		String[] paramsAndValues = new String[] { "test1", "test2" };
		long companyId = 12;
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(companyId);
		when(mockAddressLookupService.getByUprn(companyId, uprn, paramsAndValues, fallbackToNationalLookup)).thenThrow(new PortalException("myErrorMessage"));
		when(Response.status(Status.INTERNAL_SERVER_ERROR)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response result = addressRESTApplication.getAddressByUPRN(fallbackToNationalLookup, uprn, paramsAndValues, mockHttpServletRequest);

		assertThat(result, sameInstance(mockResponse));

		InOrder inOrder = inOrder(mockResponseBuilder);
		inOrder.verify(mockResponseBuilder, times(1)).entity("myErrorMessage");
		inOrder.verify(mockResponseBuilder, times(1)).build();
	}

	@Test
	@Parameters({ "true", "false" })
	public void searchAddressByKeyword_WhenAddressesFound_ThenSetsRequestContextAndReturnsTheResponseWithTheAddresses(boolean fallbackToNationalLookup) throws PortalException {
		String keyword = "someKeyword";
		String[] paramsAndValues = new String[] { "test1", "test2" };
		long companyId = 12;
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(companyId);
		Set<AddressContext> addressContexts = new HashSet<>();
		addressContexts.add(mockAddressContext);
		when(mockAddressLookupService.getByKeyword(companyId, keyword, paramsAndValues, fallbackToNationalLookup)).thenReturn(addressContexts);
		when(Response.status(Status.OK)).thenReturn(mockResponseBuilder);
		when(JSONFactoryUtil.looseSerialize(addressContexts)).thenReturn("responseValue");
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response result = addressRESTApplication.searchAddressByKeyword(fallbackToNationalLookup, keyword, paramsAndValues, mockHttpServletRequest);

		assertThat(result, sameInstance(mockResponse));

		InOrder inOrder = inOrder(mockResponseBuilder);
		inOrder.verify(mockResponseBuilder, times(1)).entity("responseValue");
		inOrder.verify(mockResponseBuilder, times(1)).build();
	}

	@Test
	@Parameters({ "true", "false" })
	public void searchAddressByKeyword_WhenExceptionRetrievingAddresses_ThenReturnsErrorResponse(boolean fallbackToNationalLookup) throws PortalException {
		String keyword = "someKeyword";
		String[] paramsAndValues = new String[] { "test1", "test2" };
		long companyId = 12;
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(companyId);
		when(mockAddressLookupService.getByKeyword(companyId, keyword, paramsAndValues, fallbackToNationalLookup)).thenThrow(new PortalException("myErrorMessage"));
		when(Response.status(Status.INTERNAL_SERVER_ERROR)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response result = addressRESTApplication.searchAddressByKeyword(fallbackToNationalLookup, keyword, paramsAndValues, mockHttpServletRequest);

		assertThat(result, sameInstance(mockResponse));

		InOrder inOrder = inOrder(mockResponseBuilder);
		inOrder.verify(mockResponseBuilder, times(1)).entity("myErrorMessage");
		inOrder.verify(mockResponseBuilder, times(1)).build();
	}

	@Test
	@Parameters({ "true", "false" })
	public void searchAddressByKeyword_WhenNoAddressesFound_ThenSetsRequestContextAndReturnsEmptyResponse(boolean fallbackToNationalLookup) throws PortalException {
		String keyword = "someKeyword";
		long companyId = 12;
		String[] paramsAndValues = new String[] { "test1", "test2" };
		Set<AddressContext> addressContexts = new HashSet<>();
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(companyId);
		when(mockAddressLookupService.getByKeyword(companyId, keyword, paramsAndValues, fallbackToNationalLookup)).thenReturn(addressContexts);
		when(Response.status(Status.OK)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response result = addressRESTApplication.searchAddressByKeyword(fallbackToNationalLookup, keyword, paramsAndValues, mockHttpServletRequest);

		assertThat(result, sameInstance(mockResponse));

		verify(mockResponseBuilder, never()).entity(anyString());
	}

	@Test
	@Parameters({ "true", "false" })
	public void searchAddressByPostcode_WhenAddressesFound_ThenReturnsTheResponseWithTheAddresses(boolean fallbackToNationalLookup) throws PortalException {
		String postcode = "myValue";
		String[] paramsAndValues = new String[] { "test1", "test2" };
		long companyId = 12;
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(companyId);
		Set<AddressContext> addressesFound = new HashSet<>();
		addressesFound.add(mockAddressContext);
		addressesFound.add(mockAddressContext2);
		when(mockAddressLookupService.getByPostcode(companyId, postcode, paramsAndValues, fallbackToNationalLookup)).thenReturn(addressesFound);
		when(Response.status(Status.OK)).thenReturn(mockResponseBuilder);
		when(JSONFactoryUtil.looseSerialize(addressesFound)).thenReturn("responseValue");
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response result = addressRESTApplication.searchAddressByPostcode(fallbackToNationalLookup, postcode, paramsAndValues, mockHttpServletRequest);

		assertThat(result, sameInstance(mockResponse));

		InOrder inOrder = inOrder(mockResponseBuilder);
		inOrder.verify(mockResponseBuilder, times(1)).entity("responseValue");
		inOrder.verify(mockResponseBuilder, times(1)).build();
	}

	@Test
	@Parameters({ "true", "false" })
	public void searchAddressByPostcode_WhenExceptionRetrievingAddresses_ThenReturnsErrorResponse(boolean fallbackToNationalLookup) throws PortalException {
		String postcode = "myValue";
		String[] paramsAndValues = new String[] { "test1", "test2" };
		long companyId = 12;
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(companyId);
		when(mockAddressLookupService.getByPostcode(companyId, postcode, paramsAndValues, fallbackToNationalLookup)).thenThrow(new PortalException("myErrorMessage"));
		when(Response.status(Status.INTERNAL_SERVER_ERROR)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response result = addressRESTApplication.searchAddressByPostcode(fallbackToNationalLookup, postcode, paramsAndValues, mockHttpServletRequest);

		assertThat(result, sameInstance(mockResponse));

		InOrder inOrder = inOrder(mockResponseBuilder);
		inOrder.verify(mockResponseBuilder, times(1)).entity("myErrorMessage");
		inOrder.verify(mockResponseBuilder, times(1)).build();
	}

	@Test
	@Parameters({ "true", "false" })
	public void searchAddressByPostcode_WhenNoAddressesFound_ThenSetsRequestContextAndReturnsEmptyResponse(boolean fallbackToNationalLookup) throws PortalException {
		String postcode = "myValue";
		long companyId = 12;
		String[] paramsAndValues = new String[] { "test1", "test2" };
		Set<AddressContext> addressContexts = new HashSet<>();
		when(mockHttpServletRequest.getAttribute("COMPANY_ID")).thenReturn(companyId);
		when(mockAddressLookupService.getByPostcode(companyId, postcode, paramsAndValues, fallbackToNationalLookup)).thenReturn(addressContexts);
		when(Response.status(Status.OK)).thenReturn(mockResponseBuilder);
		when(mockResponseBuilder.build()).thenReturn(mockResponse);

		Response result = addressRESTApplication.searchAddressByPostcode(fallbackToNationalLookup, postcode, paramsAndValues, mockHttpServletRequest);

		assertThat(result, sameInstance(mockResponse));

		verify(mockResponseBuilder, never()).entity(anyString());
	}
}
