package com.placecube.digitalplace.search.web.portlet.sortby.configuration;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.sortby.service.SortByService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.SORT_BY, service = ConfigurationAction.class)
public class SortByPortletConfigurationAction extends DefaultConfigurationAction {

	private static final String CHAIN_SORT_BY_OPTIONS = "chainSortByOptions";

	private static final String DISPLAY_PORTLET = "displayPortlet";

	private static final String ENABLED_SORT_BY_OPTIONS = "enabledSortByOptions";

	private static final String SORT_DIRECTION_CHANGEABLE = "sortDirectionChangeable";

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private SortByService sortByService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Locale locale = themeDisplay.getLocale();

		SortByPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, themeDisplay);

		List<String> enabledSortByOptions = Arrays.asList(configuration.enabledSortByOptions());

		httpServletRequest.setAttribute(ENABLED_SORT_BY_OPTIONS, enabledSortByOptions);
		httpServletRequest.setAttribute("configuration", configuration);
		httpServletRequest.setAttribute("enabledSortByOptionsKeyValuePair", sortByService.getEnabledSortByOptionsKeyValuePair(configuration.enabledSortByOptions(), locale));
		httpServletRequest.setAttribute("availableSortByOptionsKeyValuePair", sortByService.getAvailableSortByOptionsKeyValuePair(enabledSortByOptions, locale));

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		String[] enabledSortByOptions = ParamUtil.getStringValues(actionRequest, ENABLED_SORT_BY_OPTIONS, new String[0]);
		String chainSortByOptions = ParamUtil.getString(actionRequest, CHAIN_SORT_BY_OPTIONS, Boolean.FALSE.toString());
		String displayPortlet = ParamUtil.getString(actionRequest, DISPLAY_PORTLET, Boolean.TRUE.toString());
		String sortDirectionChangeable = ParamUtil.getString(actionRequest, SORT_DIRECTION_CHANGEABLE, Boolean.FALSE.toString());

		setPreference(actionRequest, ENABLED_SORT_BY_OPTIONS, enabledSortByOptions);
		setPreference(actionRequest, CHAIN_SORT_BY_OPTIONS, chainSortByOptions);
		setPreference(actionRequest, DISPLAY_PORTLET, displayPortlet);
		setPreference(actionRequest, SORT_DIRECTION_CHANGEABLE, sortDirectionChangeable);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
