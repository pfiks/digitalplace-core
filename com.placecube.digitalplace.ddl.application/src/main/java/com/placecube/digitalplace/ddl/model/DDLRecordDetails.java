package com.placecube.digitalplace.ddl.model;

import java.util.HashMap;
import java.util.Map;

import com.liferay.dynamic.data.lists.model.DDLRecord;

public class DDLRecordDetails {

	public DDLRecord ddlRecord;

	private Map<String, String> ddmFormFieldValues = new HashMap<>();

	public DDLRecordDetails() {
	}

	public DDLRecord getDdlRecord() {
		return ddlRecord;
	}

	public Map<String, String> getDdmFormFieldValues() {
		return ddmFormFieldValues;
	}

	public void setDdlRecord(DDLRecord ddlRecord) {
		this.ddlRecord = ddlRecord;
	}

	public void setDdmFormFieldValues(Map<String, String> ddmFormFieldValues) {
		this.ddmFormFieldValues = ddmFormFieldValues;
	}
}
