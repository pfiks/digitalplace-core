package com.placecube.digitalplace.appointment.admin.web.panelapp;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.application.list.BasePanelCategory;
import com.liferay.application.list.PanelCategory;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.placecube.digitalplace.appointment.admin.web.constants.DigitalServicesPanelCategoryConstants;

@Component(immediate = true, property = { "panel.category.key=" + PanelCategoryKeys.APPLICATIONS_MENU_APPLICATIONS, "panel.category.order:Integer=500" }, service = PanelCategory.class)
public class DigitalServicesPanelCategory extends BasePanelCategory {

	@Override
	public String getKey() {
		return DigitalServicesPanelCategoryConstants.CONTROL_PANEL_CATEGORY_DIGITAL_SERVICES;
	}

	@Override
	public String getLabel(Locale locale) {
		return LanguageUtil.get(locale, "control_panel.digital_services");
	}

}