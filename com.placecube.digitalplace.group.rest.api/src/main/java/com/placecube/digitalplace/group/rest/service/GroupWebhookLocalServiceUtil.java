/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.group.rest.model.GroupWebhook;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for GroupWebhook. This utility wraps
 * <code>com.placecube.digitalplace.group.rest.service.impl.GroupWebhookLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see GroupWebhookLocalService
 * @generated
 */
public class GroupWebhookLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.group.rest.service.impl.GroupWebhookLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the group webhook to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupWebhookLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupWebhook the group webhook
	 * @return the group webhook that was added
	 */
	public static GroupWebhook addGroupWebhook(GroupWebhook groupWebhook) {
		return getService().addGroupWebhook(groupWebhook);
	}

	/**
	 * Creates a new group webhook with the primary key. Does not add the group webhook to the database.
	 *
	 * @param webhookId the primary key for the new group webhook
	 * @return the new group webhook
	 */
	public static GroupWebhook createGroupWebhook(long webhookId) {
		return getService().createGroupWebhook(webhookId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the group webhook from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupWebhookLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupWebhook the group webhook
	 * @return the group webhook that was removed
	 */
	public static GroupWebhook deleteGroupWebhook(GroupWebhook groupWebhook) {
		return getService().deleteGroupWebhook(groupWebhook);
	}

	/**
	 * Deletes the group webhook with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupWebhookLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook that was removed
	 * @throws PortalException if a group webhook with the primary key could not be found
	 */
	public static GroupWebhook deleteGroupWebhook(long webhookId)
		throws PortalException {

		return getService().deleteGroupWebhook(webhookId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static GroupWebhook fetchGroupWebhook(long webhookId) {
		return getService().fetchGroupWebhook(webhookId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns a list of all the enabled webhooks configured for the specified
	 * className, classPK and event
	 *
	 * @param groupId the groupId
	 * @param className the className to match
	 * @param classPK the classPK to match
	 * @param event the event
	 * @return list of enabled webhooks
	 */
	public static List<GroupWebhook> getConfiguredGroupWebhooks(
		long groupId, String className, long classPK, String event) {

		return getService().getConfiguredGroupWebhooks(
			groupId, className, classPK, event);
	}

	/**
	 * Returns the group webhook with the primary key.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook
	 * @throws PortalException if a group webhook with the primary key could not be found
	 */
	public static GroupWebhook getGroupWebhook(long webhookId)
		throws PortalException {

		return getService().getGroupWebhook(webhookId);
	}

	/**
	 * Returns a range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of group webhooks
	 */
	public static List<GroupWebhook> getGroupWebhooks(int start, int end) {
		return getService().getGroupWebhooks(start, end);
	}

	/**
	 * Returns all the webhooks configured for the group
	 *
	 * @param groupId the groupId
	 * @param start pagination start
	 * @param end pagination end
	 * @return list of webhooks
	 */
	public static List<GroupWebhook> getGroupWebhooks(
		long groupId, int start, int end) {

		return getService().getGroupWebhooks(groupId, start, end);
	}

	/**
	 * Returns the number of group webhooks.
	 *
	 * @return the number of group webhooks
	 */
	public static int getGroupWebhooksCount() {
		return getService().getGroupWebhooksCount();
	}

	/**
	 * Returns the total number of webhooks for the group
	 *
	 * @param groupId the groupId
	 * @return total count
	 */
	public static int getGroupWebhooksCount(long groupId) {
		return getService().getGroupWebhooksCount(groupId);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * If a webhook exists for the specified id, then it returns the webhook. If
	 * no webhook is found, then returns a newly created webhoo
	 *
	 * @param webhookId the id to match
	 * @return webhook
	 */
	public static GroupWebhook getOrCreateGroupWebhook(long webhookId) {
		return getService().getOrCreateGroupWebhook(webhookId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the group webhook in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupWebhookLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupWebhook the group webhook
	 * @return the group webhook that was updated
	 */
	public static GroupWebhook updateGroupWebhook(GroupWebhook groupWebhook) {
		return getService().updateGroupWebhook(groupWebhook);
	}

	public static GroupWebhookLocalService getService() {
		return _service;
	}

	public static void setService(GroupWebhookLocalService service) {
		_service = service;
	}

	private static volatile GroupWebhookLocalService _service;

}