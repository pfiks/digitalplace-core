package com.placecube.digitalplace.group.rest.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.placecube.digitalplace.group.rest.constants.GroupRestKeyType;
import com.placecube.digitalplace.group.rest.exception.GroupRestKeyException;
import com.placecube.digitalplace.group.rest.model.GroupApiKey;
import com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK;
import com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPersistence;
import com.placecube.digitalplace.group.rest.service.util.APIKeyUtil;
import com.placecube.digitalplace.group.rest.service.util.GroupApiKeyPKUtil;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

public class GroupApiKeyLocalServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 4322;
	private static final long GROUP_ID = 123;

	@InjectMocks
	private GroupApiKeyLocalServiceImpl groupApiKeyLocalService;

	@Mock
	private GroupApiKeyPersistence mockGroupApiKeyPersistence;

	@Mock
	private APIKeyUtil mockApiKeyUtil;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private Key mockKey;

	@Mock
	private Mac mockMac;

	@Mock
	private KeyGenerator mockKeyGenerator;

	@Mock
	private SecretKey mockSecretKey;

	@Mock
	private KeyGenerator mockKeyGeneratorTwo;

	@Mock
	private SecretKey mockSecretKeyTwo;

	@Mock
	private GroupApiKey mockGroupApiKey;

	@Mock
	private GroupApiKeyPKUtil mockGroupApiKeyPKUtil;

	@Mock
	private GroupApiKeyPK mockGroupApiKeyPK;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test(expected = GroupRestKeyException.class)
	public void generateKey_WhenNoError_ThenThrowsGroupRestKeyException() throws Exception {
		GroupRestKeyType keyType = GroupRestKeyType.APPLICATION_KEY;
		when(mockApiKeyUtil.getKeyGenerator()).thenThrow(new NoSuchAlgorithmException());

		groupApiKeyLocalService.generateKey(keyType);
	}

	@Test
	public void generateKey_WhenNoError_ThenReturnsGeneratedKey() throws Exception {
		GroupRestKeyType keyType = GroupRestKeyType.APPLICATION_KEY;
		String generatedKey = "generatedKey";
		int count = 0;
		when(mockApiKeyUtil.getKeyGenerator()).thenReturn(mockKeyGenerator);
		when(mockApiKeyUtil.generateSecretKey(mockKeyGenerator)).thenReturn(mockSecretKey);
		when(mockApiKeyUtil.getEncodedSecretKey(mockSecretKey)).thenReturn(generatedKey);
		when(mockGroupApiKeyPersistence.countByApplicationKey(generatedKey)).thenReturn(count);

		String result = groupApiKeyLocalService.generateKey(keyType);

		assertThat(result, equalTo(generatedKey));
	}

	@Test(expected = GroupRestKeyException.class)
	public void getGroupByKey_WhenKeyValueIsNull_ThenThrowsGroupRestKeyException() throws GroupRestKeyException {
		groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, StringPool.BLANK);
	}

	@Test(expected = GroupRestKeyException.class)
	public void getGroupByKey_WhenKeyValueIsValidAndNoGroupsAreFound_ThenThrowsGroupRestKeyException() throws GroupRestKeyException {
		String keyValue = "keyValue";
		GroupRestKeyType keyType = GroupRestKeyType.APPLICATION_KEY;
		when(mockGroupApiKeyPersistence.findByApplicationKey(keyValue)).thenReturn(Collections.emptyList());

		groupApiKeyLocalService.getGroupByKey(keyType, keyValue);
	}

	@Test(expected = GroupRestKeyException.class)
	public void getGroupByKey_WhenKeyValueIsValidAndMultipleGroupsAreFound_ThenThrowsGroupRestKeyException() throws GroupRestKeyException {
		String keyValue = "keyValue";
		GroupRestKeyType keyType = GroupRestKeyType.APPLICATION_KEY;
		List<GroupApiKey> groups = new ArrayList<>();
		groups.add(mockGroupApiKey);
		groups.add(mockGroupApiKey);
		when(mockGroupApiKeyPersistence.findByApplicationKey(keyValue)).thenReturn(groups);
		when(mockGroupApiKey.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.fetchGroup(GROUP_ID)).thenReturn(mockGroup);

		groupApiKeyLocalService.getGroupByKey(keyType, keyValue);
	}

	@Test
	public void getGroupByKey_WhenKeyValueIsValidAndAsingleGroupIsFound_ThenReturnsTheGroup() throws GroupRestKeyException {
		String keyValue = "keyValue";
		GroupRestKeyType keyType = GroupRestKeyType.APPLICATION_KEY;
		List<GroupApiKey> groups = new ArrayList<>();
		groups.add(mockGroupApiKey);
		when(mockGroupApiKeyPersistence.findByApplicationKey(keyValue)).thenReturn(groups);
		when(mockGroupApiKey.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.fetchGroup(GROUP_ID)).thenReturn(mockGroup);

		Group result = groupApiKeyLocalService.getGroupByKey(keyType, keyValue);

		assertThat(result, sameInstance(mockGroup));
	}

	@Test
	public void getGroupRestKeyValue_WhenNoError_ThenReturnsTheGroupRestKeyValue() {
		GroupRestKeyType keyType = GroupRestKeyType.APPLICATION_KEY;
		String expected = "expected";

		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(GROUP_ID)).thenReturn(mockGroupApiKey);
		when(mockGroupApiKey.getApplicationKey()).thenReturn(expected);

		String result = groupApiKeyLocalService.getGroupRestKeyValue(mockGroup, keyType);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = GroupRestKeyException.class)
	public void getEncodedTextWithKey_WhenException_ThenThrowsGroupRestKeyException() throws Exception {
		String keyValue = "keyValue";
		when(mockGroupApiKeyPersistence.findByApplicationKey(keyValue)).thenReturn(Collections.emptyList());

		groupApiKeyLocalService.getEncodedTextWithKey(keyValue, "textToEncode");
	}

	@Test
	public void getEncodedTextWithKey_WhenNoError_ThenReturnsEncodedText() throws Exception {
		String applicationKeyValue = "keyValue";
		String applicationSecretValue = "applicationSecretValue";
		String textToEncode = "textToEncodeValue";
		String expected = "expected";
		byte[] bytes = new byte[3];
		List<GroupApiKey> groups = new ArrayList<>();
		groups.add(mockGroupApiKey);
		when(mockGroupApiKey.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.fetchGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroupApiKeyPersistence.findByApplicationKey(applicationKeyValue)).thenReturn(groups);
		when(mockGroupApiKey.getApplicationSecret()).thenReturn(applicationSecretValue);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(GROUP_ID)).thenReturn(mockGroupApiKey);
		when(mockGroupApiKey.getApplicationSecret()).thenReturn(applicationSecretValue);
		when(mockApiKeyUtil.getKey(applicationSecretValue)).thenReturn(mockKey);
		when(mockApiKeyUtil.getMacForKey(mockKey)).thenReturn(mockMac);
		when(mockApiKeyUtil.getBytes(textToEncode)).thenReturn(bytes);
		when(mockApiKeyUtil.getEncodedText(mockMac, bytes)).thenReturn(expected);

		String result = groupApiKeyLocalService.getEncodedTextWithKey(applicationKeyValue, textToEncode);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void regenerateGroupApiKey_WhenGroupApiKeyExistsAndKeysAreValidAndGroupIsValid_ThenSetsGeneratedKeyAndSecretAndUpdatesGroupApiKey() throws Exception {
		String apiKey = "generatedKey";
		String apiSecret = "generatedSecret";

		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(mockGroupApiKey);
		when(mockApiKeyUtil.getKeyGenerator()).thenReturn(mockKeyGenerator, mockKeyGeneratorTwo);
		when(mockGroup.isSite()).thenReturn(true);

		mockKeyGeneration(GroupRestKeyType.APPLICATION_KEY, apiKey);
		mockKeyGeneration(GroupRestKeyType.APPLICATION_SECRET, apiSecret);

		groupApiKeyLocalService.regenerateGroupApiKey(mockGroup);

		InOrder inOrder = inOrder(mockGroupApiKey, mockGroupApiKeyPersistence);
		inOrder.verify(mockGroupApiKey, times(1)).setApplicationKey(apiKey);
		inOrder.verify(mockGroupApiKey, times(1)).setApplicationSecret(apiSecret);
		inOrder.verify(mockGroupApiKeyPersistence, times(1)).update(mockGroupApiKey);
	}

	@Test
	public void regenerateGroupApiKey_WhenGroupApiKeyDoesNotExistAndKeysAreValidAndGroupIsValid_ThenCreatesApiKeyAndSetsGeneratedKeyAndSecretAndUpdatesGroupApiKey() throws Exception {
		String apiKey = "generatedKey";
		String apiSecret = "generatedSecret";

		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(null);
		when(mockGroupApiKeyPersistence.create(mockGroupApiKeyPK)).thenReturn(mockGroupApiKey);
		when(mockApiKeyUtil.getKeyGenerator()).thenReturn(mockKeyGenerator, mockKeyGeneratorTwo);
		when(mockGroup.isSite()).thenReturn(true);

		mockKeyGeneration(GroupRestKeyType.APPLICATION_KEY, apiKey);
		mockKeyGeneration(GroupRestKeyType.APPLICATION_SECRET, apiSecret);

		groupApiKeyLocalService.regenerateGroupApiKey(mockGroup);

		InOrder inOrder = inOrder(mockGroupApiKey, mockGroupApiKeyPersistence);
		inOrder.verify(mockGroupApiKey, times(1)).setApplicationKey(apiKey);
		inOrder.verify(mockGroupApiKey, times(1)).setApplicationSecret(apiSecret);
		inOrder.verify(mockGroupApiKeyPersistence, times(1)).update(mockGroupApiKey);
	}

	@Test
	public void regenerateGroupApiKey_WhenGroupApiKeyExistsAndKeysAreValidAndGroupIsNotValid_ThenDoesNotUpdateGroupApiKey() throws Exception {
		String apiKey = "generatedKey";
		String apiSecret = "generatedSecret";

		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(mockGroupApiKey);
		when(mockApiKeyUtil.getKeyGenerator()).thenReturn(mockKeyGenerator, mockKeyGeneratorTwo);
		when(mockGroup.isSite()).thenReturn(false);

		mockKeyGeneration(GroupRestKeyType.APPLICATION_KEY, apiKey);
		mockKeyGeneration(GroupRestKeyType.APPLICATION_SECRET, apiSecret);

		groupApiKeyLocalService.regenerateGroupApiKey(mockGroup);

		verify(mockGroupApiKeyPersistence, never()).update(any(GroupApiKey.class));
	}

	@Test
	public void regenerateGroupApiKey_WhenGroupApiKeyExistsAndApiKeyIsNotValidAndGroupIsValid_ThenDoesNotUpdateGroupApiKey() throws Exception {
		String apiKey = "generatedKey";

		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(mockGroupApiKey);
		when(mockApiKeyUtil.getKeyGenerator()).thenReturn(mockKeyGenerator, mockKeyGeneratorTwo);
		when(mockGroup.isSite()).thenReturn(true);

		mockKeyGeneration(GroupRestKeyType.APPLICATION_KEY, apiKey);
		mockKeyGeneration(GroupRestKeyType.APPLICATION_SECRET, StringPool.BLANK);

		groupApiKeyLocalService.regenerateGroupApiKey(mockGroup);

		verify(mockGroupApiKeyPersistence, never()).update(any(GroupApiKey.class));
	}

	@Test
	public void regenerateGroupApiKey_WhenGroupApiKeyExistsAndApiSecretIsNotValidAndGroupIsValid_ThenDoesNotUpdateGroupApiKey() throws Exception {
		String apiSecret = "generatedSecret";

		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(mockGroupApiKey);
		when(mockApiKeyUtil.getKeyGenerator()).thenReturn(mockKeyGenerator, mockKeyGeneratorTwo);
		when(mockGroup.isSite()).thenReturn(true);

		mockKeyGeneration(GroupRestKeyType.APPLICATION_KEY, StringPool.BLANK);
		mockKeyGeneration(GroupRestKeyType.APPLICATION_SECRET, apiSecret);

		groupApiKeyLocalService.regenerateGroupApiKey(mockGroup);

		verify(mockGroupApiKeyPersistence, never()).update(any(GroupApiKey.class));
	}

	@Test(expected = GroupRestKeyException.class)
	public void regenerateGroupApiKey_WhenKeyGeneratorCannotBeRetrieved_ThenThrowsGroupRestKeyException() throws Exception {
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(mockGroupApiKey);
		when(mockApiKeyUtil.getKeyGenerator()).thenThrow(new NoSuchAlgorithmException());

		groupApiKeyLocalService.regenerateGroupApiKey(mockGroup);
	}

	@Test
	public void getGroupApplicationKey_WhenGroupApiKeyExists_ThenReturnsGroupApiApplicationKey() {
		final String apiKey = "apiKey";
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(mockGroupApiKey);
		when(mockGroupApiKey.getApplicationKey()).thenReturn(apiKey);

		String result = groupApiKeyLocalService.getGroupApplicationKey(COMPANY_ID, GROUP_ID);

		assertThat(result, equalTo(apiKey));
	}

	@Test
	public void getGroupApplicationKey_WhenGroupApiKeyDoesNotExists_ThenReturnsEmptyString() {
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(null);

		String result = groupApiKeyLocalService.getGroupApplicationKey(COMPANY_ID, GROUP_ID);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getGroupApplicationSecret_WhenGroupApiKeyExists_ThenReturnsGroupApiApplicationKey() {
		final String apiSecret = "apiSecret";
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(mockGroupApiKey);
		when(mockGroupApiKey.getApplicationSecret()).thenReturn(apiSecret);

		String result = groupApiKeyLocalService.getGroupApplicationSecret(COMPANY_ID, GROUP_ID);

		assertThat(result, equalTo(apiSecret));
	}

	@Test
	public void getGroupApplicationSecret_WhenGroupApiKeyDoesNotExists_ThenReturnsEmptyString() {
		when(mockGroupApiKeyPKUtil.createGroupApiKeyPK(COMPANY_ID, GROUP_ID)).thenReturn(mockGroupApiKeyPK);
		when(mockGroupApiKeyPersistence.fetchByPrimaryKey(mockGroupApiKeyPK)).thenReturn(null);

		String result = groupApiKeyLocalService.getGroupApplicationSecret(COMPANY_ID, GROUP_ID);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	private void mockKeyGeneration(GroupRestKeyType keyType, String generatedKey) throws Exception {
		if (keyType == GroupRestKeyType.APPLICATION_KEY) {
			when(mockApiKeyUtil.generateSecretKey(mockKeyGenerator)).thenReturn(mockSecretKey);
			when(mockApiKeyUtil.getEncodedSecretKey(mockSecretKey)).thenReturn(generatedKey);
			when(mockGroupApiKeyPersistence.countByApplicationKey(generatedKey)).thenReturn(0);
		} else {
			when(mockApiKeyUtil.generateSecretKey(mockKeyGeneratorTwo)).thenReturn(mockSecretKeyTwo);
			when(mockApiKeyUtil.getEncodedSecretKey(mockSecretKeyTwo)).thenReturn(generatedKey);
			when(mockGroupApiKeyPersistence.countByApplicationSecret(generatedKey)).thenReturn(0);
		}

	}
}
