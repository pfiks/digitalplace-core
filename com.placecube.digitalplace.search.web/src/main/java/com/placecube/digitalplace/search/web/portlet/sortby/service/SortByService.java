package com.placecube.digitalplace.search.web.portlet.sortby.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.calendar.model.CalendarBooking;
import com.liferay.calendar.service.CalendarBookingLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.message.boards.service.MBMessageLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.model.SortByOption;
import com.placecube.digitalplace.search.shared.service.SortByOptionsRegistry;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.constants.SortByDirectionConstants;
import com.placecube.digitalplace.search.web.portlet.sortby.model.impl.SortByScore;

@Component(immediate = true, service = SortByService.class)
public class SortByService {

	private static final Log LOG = LogFactoryUtil.getLog(SortByService.class);

	@Reference
	private CalendarBookingLocalService calendarBookingLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DynamicDataListConfigurationService dynamicDataListConfigurationService;

	@Reference
	private MBMessageLocalService mbMessageLocalService;

	@Reference
	private SortByOptionsRegistry sortByOptionsRegistry;

	@Reference(service = SortByScore.class)
	private SortByScore sortByScore;

	public void addChainedSortByOptions(List<SortByOption> availableSortByOptions, Map<String, SortByOption> sortsToApply) {
		for (SortByOption sortByOption : availableSortByOptions) {
			if (!(sortByOption instanceof SortByScore)) {
				sortsToApply.putIfAbsent(sortByOption.getId(), sortByOption);
			}
		}
	}

	public void addDefaultSortByOption(List<SortByOption> availableSortByOptions, Map<String, SortByOption> sortsToApply) {

		if (sortsToApply.isEmpty() && !availableSortByOptions.isEmpty()) {
			Optional<SortByOption> sortByOptionOpt = availableSortByOptions.stream().filter(option -> !(option instanceof SortByScore)).findFirst();
			if (sortByOptionOpt.isPresent()) {
				addSortByOption(sortsToApply, sortByOptionOpt.get());
			}
		}
	}

	public void addScoreSortByOption(Map<String, SortByOption> sortsToApply) {
		addSortByOption(sortsToApply, sortByScore);
	}

	public void addSelectedSortByOption(List<SortByOption> availableSortByOptions, Map<String, SortByOption> sortsToApply, Optional<String> selectedSortByFieldName) {

		if (selectedSortByFieldName.isPresent()) {
			Optional<SortByOption> sortByOptionOpt = getByFieldName(availableSortByOptions, selectedSortByFieldName.get());

			if (sortByOptionOpt.isPresent()) {
				SortByOption sortByOption = sortByOptionOpt.get();
				sortsToApply.putIfAbsent(sortByOption.getId(), sortByOption);
			}
		}
	}

	public List<DDMFormField> getAvailableDDMFormFieldsIndexabled(long ddmStructureId) {
		DDMStructure ddmStructure = ddmStructureLocalService.fetchDDMStructure(ddmStructureId);
		if (Validator.isNotNull(ddmStructure)) {
			return ddmStructure.getDDMFormFields(false).stream().filter(x -> x.getIndexType().equals("keyword")).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	public List<DDMStructure> getAvailableDDMStructures(long companyId) {
		List<DynamicDataListMapping> dynamicDataListMappings = dynamicDataListConfigurationService.getDynamicDataListMappings(companyId);
		return dynamicDataListMappings.stream().map(DynamicDataListMapping::getDDMStructure).collect(Collectors.toList());
	}

	public List<SortByOption> getAvailableSortByOptions(String[] enabledSortByOptions, String searchKeyword) {

		List<SortByOption> results = new LinkedList<>();

		if (Validator.isNotNull(searchKeyword)) {
			results.add(sortByScore);
		}

		results.addAll(sortByOptionsRegistry.getAvailableSortByOptionsForIds(enabledSortByOptions));

		return results;
	}

	public List<KeyValuePair> getAvailableSortByOptionsKeyValuePair(List<String> enabledSortByOptions, Locale locale) {
		List<KeyValuePair> availableSortingByOptions = new ArrayList<>();

		for (SortByOption availableSortByOption : sortByOptionsRegistry.getAllAvailableSortByOptions()) {
			if (!enabledSortByOptions.contains(availableSortByOption.getId())) {
				availableSortingByOptions.add(new KeyValuePair(availableSortByOption.getId(), availableSortByOption.getLabel(locale)));
			}
		}
		return availableSortingByOptions;
	}

	public List<KeyValuePair> getEnabledSortByOptionsKeyValuePair(String[] enabledSortByOptions, Locale locale) {
		List<KeyValuePair> enabledSortByOptionsKeyValuePair = new ArrayList<>();
		for (SortByOption enabledSortByOption : sortByOptionsRegistry.getAvailableSortByOptionsForIds(enabledSortByOptions)) {
			enabledSortByOptionsKeyValuePair.add(new KeyValuePair(enabledSortByOption.getId(), enabledSortByOption.getLabel(locale)));
		}
		return enabledSortByOptionsKeyValuePair;
	}

	public Sort[] getSortsForSortByOptions(SharedSearchContributorSettings sharedSearchContributorSettings, Collection<SortByOption> sortByOptions, boolean useOppositeSortDirection) {
		Sort[] sorts = new Sort[0];
		for (SortByOption sortByOption : sortByOptions) {
			sorts = ArrayUtil.append(sorts, sortByOption.getSorts(sharedSearchContributorSettings, useOppositeSortDirection));
		}

		return sorts;
	}

	public String getTitleForCalendarBooking(Document document) {
		try {
			long entryClassPK = GetterUtil.getLong(document.get("entryClassPK"));
			CalendarBooking calendarBooking = calendarBookingLocalService.getCalendarBooking(entryClassPK);
			return calendarBooking.getTitle(calendarBooking.getDefaultLanguageId());
		} catch (Exception e) {
			LOG.warn("Unable to retrieve calendar booking " + e.getMessage());
		}
		return StringPool.BLANK;
	}

	public String getTitleForMBMessage(Document document) {
		try {
			long entryClassPK = GetterUtil.getLong(document.get("entryClassPK"));
			return mbMessageLocalService.getMBMessage(entryClassPK).getSubject();
		} catch (Exception e) {
			LOG.warn("Unable to retrieve mbmessage " + e.getMessage());
		}
		return StringPool.BLANK;
	}

	public boolean useOppositeSortByDirection(Optional<String> selectedSortBy, SharedSearchContributorSettings sharedSearchContributorSettings) {

		if (selectedSortBy.isPresent()) {

			Optional<String> sharedSearchSessionSingleValuedAttribute = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY_DIRECTION);
			if (sharedSearchSessionSingleValuedAttribute.isPresent()) {
				String sortByDirection = sharedSearchSessionSingleValuedAttribute.get();
				if (SortByDirectionConstants.DIRECTION_INVERTED.equalsIgnoreCase(sortByDirection)) {
					return true;
				}
			}

		}

		return false;

	}

	private void addSortByOption(Map<String, SortByOption> sortsToApply, SortByOption orderByOption) {
		sortsToApply.putIfAbsent(orderByOption.getId(), orderByOption);
	}

	private Optional<SortByOption> getByFieldName(List<SortByOption> availableSortByOptions, String sortByFieldName) {
		return availableSortByOptions.stream().filter(option -> Objects.equals(option.getId(), sortByFieldName)).findFirst();
	}

}
