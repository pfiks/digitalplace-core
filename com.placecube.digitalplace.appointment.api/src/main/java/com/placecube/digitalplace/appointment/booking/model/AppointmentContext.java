package com.placecube.digitalplace.appointment.booking.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

public class AppointmentContext implements Serializable {

	private static final long serialVersionUID = 1L;

	private long classNameId = 0;
	private long classPK = 0;
	private Optional<Date> endDate = Optional.empty();
	private boolean exactDateMatch = false;
	private Optional<Date> startDate = Optional.empty();

	public long getClassNameId() {
		return classNameId;
	}

	public long getClassPK() {
		return classPK;
	}

	public Optional<Date> getEndDate() {
		return endDate;
	}

	public Optional<Date> getStartDate() {
		return startDate;
	}

	public boolean isExactDateMatch() {
		return exactDateMatch;
	}

	public void setClassNameId(long classNameId) {
		this.classNameId = classNameId;
	}

	public void setClassPK(long classPK) {
		this.classPK = classPK;
	}

	public void setEndDate(Optional<Date> endDate) {
		this.endDate = endDate;
	}

	public void setExactDateMatch(boolean exactDateMatch) {
		this.exactDateMatch = exactDateMatch;
	}

	public void setStartDate(Optional<Date> startDate) {
		this.startDate = startDate;
	}

}
