package com.placecube.digitalplace.userprofile.configuration;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;

public interface UserProfileFieldsConfigurationFactory {

	UserProfileFieldsConfiguration getCompanyUserProfileFieldsConfiguration(long companyId) throws ConfigurationException;

}
