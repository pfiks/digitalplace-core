package com.placecube.digitalplace.payment.mock.web.constants;

public final class MVCCommandKeys {

	public static final String REDIRECT_ACTION_COMMAND = "/redirect-action";

	private MVCCommandKeys() {

	}

}
