package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.settings.LocalizedValuesMap;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.AdvancePeopleSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.displaycontext.AdvancePeopleSearchDisplayContext;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.model.UserSearchField;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service.AdvancePeopleSearchConfigurationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class, AdvancePeopleSearchDisplayContext.class })
public class AdvancePeopleSearchPortletTest extends PowerMockito {

	private static final Locale LOCALE = Locale.UK;

	@InjectMocks
	private AdvancePeopleSearchPortlet advancePeopleSearchPortlet;

	@Mock
	private List<SharedActiveFilter> mockActiveFilters;

	@Mock
	private AdvancePeopleSearchConfigurationService mockAdvancePeopleSearchConfigurationService;

	@Mock
	private AdvancePeopleSearchDisplayContext mockAdvancePeopleSearchDisplayContext;

	@Mock
	private AdvancePeopleSearchPortletInstanceConfiguration mockConfiguration;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private LocalizedValuesMap mockTitleMap;

	@Before
	public void activateSetup() throws Exception {
		mockStatic(AdvancePeopleSearchDisplayContext.class);
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test(expected = PortletException.class)
	public void render_WhenConfigurationExceptionIsThrown_ThenThrowPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAdvancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay)).thenThrow(new ConfigurationException());

		advancePeopleSearchPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoErrorAndInvalidConfiguration_ThenSetInvalidConfigurationRequestAttribute() throws Exception {
		when(mockAdvancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.empty());

		advancePeopleSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
	}

	@Test
	public void render_WhenNoErrorAndValidConfiguration_ThenSetRequestAttributes() throws Exception {
		long companyId = 123;
		String filterColour = "filterColourValue";
		String redirectURL = "redirectURLValue";
		String title = "titleValue";
		String configuredSearchFields = "[]";
		List<UserSearchField> searchFields = new ArrayList<>();

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAdvancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockThemeDisplay.getLocale()).thenReturn(LOCALE);
		when(mockConfiguration.title()).thenReturn(mockTitleMap);
		when(mockConfiguration.filterColour()).thenReturn(filterColour);
		when(mockSharedSearchResponse.getEscapedSearchResultsIteratorURL()).thenReturn(redirectURL);
		when(mockAdvancePeopleSearchConfigurationService.getSearchFields(companyId, LOCALE, configuredSearchFields)).thenReturn(searchFields);
		when(mockAdvancePeopleSearchConfigurationService.getTitle(mockThemeDisplay, mockTitleMap)).thenReturn(title);
		when(mockSharedSearchResponse.getActiveFilters()).thenReturn(mockActiveFilters);
		when(AdvancePeopleSearchDisplayContext.initDisplayContext(mockActiveFilters)).thenReturn(mockAdvancePeopleSearchDisplayContext);

		advancePeopleSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("advancePeopleSearchDisplayContext", mockAdvancePeopleSearchDisplayContext);
		verify(mockRenderRequest, times(1)).setAttribute("filterColour", filterColour);
		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", redirectURL);
		verify(mockRenderRequest, times(1)).setAttribute("searchFields", searchFields);
		verify(mockRenderRequest, times(1)).setAttribute("title", title);
	}

	@Test(expected = PortletException.class)
	public void render_WhenSearchExceptionIsThrown_ThenThrowPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAdvancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		advancePeopleSearchPortlet.render(mockRenderRequest, mockRenderResponse);
	}

}
