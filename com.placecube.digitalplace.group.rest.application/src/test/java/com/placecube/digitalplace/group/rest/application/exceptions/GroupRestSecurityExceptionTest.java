package com.placecube.digitalplace.group.rest.application.exceptions;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.ws.rs.core.Response.Status;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;

public class GroupRestSecurityExceptionTest extends PowerMockito {

	@Test
	public void newGroupRestSecurityException_WhenNoError_ThenIsCreatedWithTheGivenMessageAndStatusCode() {
		Status status = Status.ACCEPTED;
		ErrorCodes message = ErrorCodes.INTERNAL_ERROR;
		GroupRestSecurityException result = new GroupRestSecurityException(message, status);

		assertThat(result.getMessage(), equalTo(message.getCode()));
		assertThat(result.getResponse().getStatus(), equalTo(status.getStatusCode()));
	}

}
