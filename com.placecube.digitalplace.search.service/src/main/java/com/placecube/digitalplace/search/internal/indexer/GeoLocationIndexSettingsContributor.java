package com.placecube.digitalplace.search.internal.indexer;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.search.spi.settings.IndexSettingsContributor;
import com.liferay.portal.search.spi.settings.IndexSettingsHelper;
import com.liferay.portal.search.spi.settings.TypeMappingsHelper;

@Component(immediate = true, service = IndexSettingsContributor.class)
public class GeoLocationIndexSettingsContributor implements IndexSettingsContributor {

	private static final Log LOG = LogFactoryUtil.getLog(GeoLocationIndexSettingsContributor.class);

	@Override
	public void contribute(String indexName, TypeMappingsHelper typeMappingsHelper) {

		try {
			String mappings = StringUtil.read(getClass().getClassLoader(), "META-INF/resources/mappings/index-mappings.json");
			typeMappingsHelper.addTypeMappings(indexName, mappings);
		} catch (Exception e) {
			LOG.error("Error reading mappings file.", e);
		}

	}

	@Override
	public void populate(IndexSettingsHelper indexSettingsHelper) {
		// Auto-generated method stub - not required

	}

}
