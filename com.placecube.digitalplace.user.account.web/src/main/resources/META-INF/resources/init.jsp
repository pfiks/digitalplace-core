<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/captcha" prefix="liferay-captcha" %>
<%@ taglib uri="http://liferay.com/tld/frontend" prefix="liferay-frontend"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/journal" prefix="liferay-journal" %>

<%@ taglib uri="http://placecube.com/digitalplace/tld/address" prefix="dp-address" %>
<%@ taglib uri="http://placecube.com/digitalplace/tld/gds-forms-ui" prefix="gds-forms-ui"%>

<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.exception.WebsiteURLException"%>
<%@page import="com.liferay.portal.kernel.exception.UserScreenNameException"%>
<%@page import="com.liferay.portal.kernel.exception.UserPasswordException"%>
<%@page import="com.liferay.portal.kernel.exception.UserIdException"%>
<%@page import="com.liferay.portal.kernel.exception.UserEmailAddressException"%>
<%@page import="com.liferay.portal.kernel.exception.TermsOfUseException"%>
<%@page import="com.liferay.portal.kernel.exception.RequiredFieldException"%>
<%@page import="com.liferay.portal.kernel.exception.PhoneNumberExtensionException"%>
<%@page import="com.liferay.portal.kernel.exception.PhoneNumberException"%>
<%@page import="com.liferay.portal.kernel.exception.NoSuchRegionException"%>
<%@page import="com.liferay.portal.kernel.exception.NoSuchListTypeException"%>
<%@page import="com.liferay.portal.kernel.exception.NoSuchCountryException"%>
<%@page import="com.liferay.portal.kernel.exception.GroupFriendlyURLException"%>
<%@page import="com.liferay.portal.kernel.exception.EmailAddressException"%>
<%@page import="com.liferay.portal.kernel.exception.DuplicateOpenIdException"%>
<%@page import="com.liferay.portal.kernel.exception.ContactNameException"%>
<%@page import="com.liferay.portal.kernel.exception.ContactBirthdayException"%>
<%@page import="com.liferay.portal.kernel.exception.CompanyMaxUsersException"%>
<%@page import="com.liferay.portal.kernel.captcha.CaptchaTextException"%>
<%@page import="com.liferay.portal.kernel.captcha.CaptchaException"%>
<%@page import="com.liferay.portal.kernel.captcha.CaptchaConfigurationException"%>
<%@page import="com.liferay.portal.kernel.exception.AddressZipException"%>
<%@page import="com.liferay.portal.kernel.exception.AddressStreetException"%>
<%@page import="com.liferay.portal.kernel.exception.AddressCityException"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.util.Constants" %>

<%@page import="com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants" %>
<%@page import="com.placecube.digitalplace.user.account.constants.AccountValidatedField"%>
<%@page import="com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant"%>
<%@page import="com.placecube.digitalplace.user.account.constants.WebContentArticles"%>
<%@page import="com.placecube.digitalplace.user.account.web.constants.AccountConstants" %>
<%@page import="com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys" %>
<%@page import="com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys" %>
<%@page import="com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys"%>
<%@page import="com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys"%>

<%@page import="javax.portlet.PortletException"%>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<c:set var="portletNamespace">
	<portlet:namespace/>
</c:set>
