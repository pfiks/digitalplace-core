package com.placecube.digitalplace.category.service;

import java.util.List;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.kernel.exception.PortalException;

public interface CategoryRetrievalService {

	/**
	 * Given the assetEntry, it retrieves the specified propertyKey from all the
	 * categories belonging to the specified vocabulary and assigned to the
	 * asset entry. The values are returned as a comma-separated string.
	 *
	 * @param assetEntry the asset entry
	 * @param propertyKey the category property to retrieve
	 * @param vocabularyId the vocabulary Id
	 * @return comma-separated values of the properties found, empty string if
	 *         none found
	 */
	String getCategoriesPropertyValues(AssetEntry assetEntry, String propertyKey, long vocabularyId);

	/**
	 * Given the class name and class PK, it retrieves the specified propertyKey
	 * from all the categories belonging to the specified vocabulary and
	 * assigned to the asset entry for the object defined by the class name and
	 * class PK. The values are returned as a comma-separated string.
	 *
	 * @param className the class name
	 * @param classPK the object class PK
	 * @param propertyKey the category property to retrieve
	 * @param vocabularyId the vocabulary id
	 * @return comma-separated values of the properties found, empty string if
	 *         none found
	 * @throws PortalException the portal exception
	 */
	String getCategoriesPropertyValues(String className, long classPK, String propertyKey, long vocabularyId) throws PortalException;

	/**
	 * Given the class name and class PK, it retrieves a list of asset
	 * categories with the parent category specified and assigned to the asset
	 * entry for the object defined by the class name and class PK.
	 *
	 * @param className the class name
	 * @param classPK the object class PK
	 * @param parentCategoryId the parent category id
	 * @return a list of asset categories
	 */
	List<AssetCategory> getCategoriesByParentCategory(String className, long classPK, long parentCategoryId);

	/**
	 * Retrieves the value of the category property specified
	 *
	 * @param assetCategory the asset category
	 * @param propertyKey the property to retrieve return
	 * @return value of the property found, empty string if none found
	 */
	String getPropertyValue(AssetCategory assetCategory, String propertyKey);

}
