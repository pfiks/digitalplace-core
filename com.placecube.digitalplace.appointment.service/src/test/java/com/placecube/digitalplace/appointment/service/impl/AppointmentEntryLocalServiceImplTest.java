package com.placecube.digitalplace.appointment.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.util.DateUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.internal.utils.AppointmentUtils;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService;
import com.placecube.digitalplace.appointment.service.persistence.AppointmentEntryPersistence;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentEntryLocalServiceImplTest {

	@InjectMocks
	private AppointmentEntryLocalServiceImpl appointmentEntryLocalServiceImpl;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry;

	@Mock
	private AppointmentEntry mockAppointmentEntry1;

	@Mock
	private AppointmentEntry mockAppointmentEntry2;

	@Mock
	private AppointmentEntry mockAppointmentEntry3;

	@Mock
	private AppointmentEntryLocalService mockAppointmentEntryLocalService;

	@Mock
	private AppointmentEntryPersistence mockAppointmentEntryPersistence;

	@Mock
	private AppointmentUtils mockAppointmentUtils;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private Date mockDate1;

	@Mock
	private Date mockDate2;

	@Mock
	private Date mockDate3;

	private MockedStatic<DateUtil> mockDateUtil;

	@Before
	public void activateSetup() {
		mockDateUtil = mockStatic(DateUtil.class);
	}

	@Test
	public void addAppointmentEntry_WhenNoError_ThenReturnsTheCreatedEntry() {
		long companyId = 34;
		String serviceId = "myServiceId";
		String connectorClassName = "myConnector";
		String externalAppointmentId = "myExtAppId";
		long id = 12;
		long classPK = 4;
		long classNameId = 5;
		when(mockCounterLocalService.increment(AppointmentEntry.class.getName(), 1)).thenReturn(id);
		when(mockAppointmentEntryPersistence.create(id)).thenReturn(mockAppointmentEntry1);
		mockDateUtil.when(() -> DateUtil.newDate()).thenReturn(mockDate1);
		when(mockAppointmentBookingEntry.getEndDate()).thenReturn(mockDate2);
		when(mockAppointmentBookingEntry.getStartDate()).thenReturn(mockDate3);
		when(mockAppointmentBookingEntry.getClassPK()).thenReturn(classPK);
		when(mockAppointmentBookingEntry.getClassNameId()).thenReturn(classNameId);
		when(mockAppointmentEntryLocalService.addAppointmentEntry(mockAppointmentEntry1)).thenReturn(mockAppointmentEntry2);

		AppointmentEntry result = appointmentEntryLocalServiceImpl.addAppointmentEntry(companyId, serviceId, connectorClassName, mockAppointmentBookingEntry, externalAppointmentId);

		assertThat(result, sameInstance(mockAppointmentEntry2));

		InOrder inOrder = inOrder(mockAppointmentEntry1, mockAppointmentEntryLocalService);
		inOrder.verify(mockAppointmentEntry1, times(1)).setCompanyId(companyId);
		inOrder.verify(mockAppointmentEntry1, times(1)).setServiceId(serviceId);
		inOrder.verify(mockAppointmentEntry1, times(1)).setConnectorClassName(connectorClassName);
		inOrder.verify(mockAppointmentEntry1, times(1)).setAppointmentId(externalAppointmentId);
		inOrder.verify(mockAppointmentEntry1, times(1)).setCreateDate(mockDate1);
		inOrder.verify(mockAppointmentEntry1, times(1)).setAppointmentEndDate(mockDate2);
		inOrder.verify(mockAppointmentEntry1, times(1)).setAppointmentStartDate(mockDate3);
		inOrder.verify(mockAppointmentEntry1, times(1)).setEntryClassPK(classPK);
		inOrder.verify(mockAppointmentEntry1, times(1)).setEntryClassNameId(classNameId);
		inOrder.verify(mockAppointmentEntryLocalService, times(1)).addAppointmentEntry(mockAppointmentEntry1);
	}

	@Test
	public void getAppointmentEntries_WhenNoError_ThenReturnsTheEntries() {
		long companyId = 34;
		long classPK = 4;
		long classNameId = 5;
		List<AppointmentEntry> expected = List.of(mockAppointmentEntry1, mockAppointmentEntry2, mockAppointmentEntry3);
		when(mockAppointmentEntryPersistence.findByCompanyId_EntryClassPK_EntryClassNameId(companyId, classPK, classNameId)).thenReturn(expected);

		List<AppointmentEntry> results = appointmentEntryLocalServiceImpl.getAppointmentEntries(companyId, classPK, classNameId);

		assertThat(results, sameInstance(expected));
	}

	@Test
	public void removeAppointmentEntry_WhenNoError_ThenDeletesTheMatchedEntries() {
		long companyId = 34;
		String serviceId = "myServiceId";
		String connectorClassName = "myConnectorClassName";
		String externalAppointmentId = "myExtAppId";
		long classPK = 4;
		long classNameId = 5;
		when(mockAppointmentEntryPersistence.findByCompanyId_EntryClassPK_EntryClassNameId(companyId, classPK, classNameId))
				.thenReturn(List.of(mockAppointmentEntry1, mockAppointmentEntry2, mockAppointmentEntry3));
		when(mockAppointmentUtils.doesAppointmentMatch(serviceId, connectorClassName, externalAppointmentId, mockAppointmentEntry1)).thenReturn(true);
		when(mockAppointmentUtils.doesAppointmentMatch(serviceId, connectorClassName, externalAppointmentId, mockAppointmentEntry2)).thenReturn(false);
		when(mockAppointmentUtils.doesAppointmentMatch(serviceId, connectorClassName, externalAppointmentId, mockAppointmentEntry3)).thenReturn(true);

		appointmentEntryLocalServiceImpl.removeAppointmentEntry(companyId, serviceId, connectorClassName, classPK, classNameId, externalAppointmentId);

		verify(mockAppointmentEntryLocalService, times(1)).deleteAppointmentEntry(mockAppointmentEntry1);
		verify(mockAppointmentEntryLocalService, never()).deleteAppointmentEntry(mockAppointmentEntry2);
		verify(mockAppointmentEntryLocalService, times(1)).deleteAppointmentEntry(mockAppointmentEntry3);
	}

	@After
	public void tearDown() {
		mockDateUtil.close();
	}

}
