package com.placecube.digitalplace.search.internal.util;

import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.dao.search.SearchContainer;

public class SearchContainerFactory {

    private SearchContainerFactory () {}
    
    public static SearchContainer<Object> createSearchContainer(PortletRequest portletRequest, DisplayTerms displayTerms, DisplayTerms searchTerms, String curParam, int cur, int delta, PortletURL iteratorURL, List<String> headerNames, String emptyResultsMessage) {
        return new SearchContainer<>(portletRequest, displayTerms, searchTerms, curParam, cur, delta, iteratorURL, headerNames, emptyResultsMessage);
    }
}
