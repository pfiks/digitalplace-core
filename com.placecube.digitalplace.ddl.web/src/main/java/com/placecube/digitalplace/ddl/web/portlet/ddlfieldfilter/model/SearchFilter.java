package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.filter.Filter;

public interface SearchFilter {

	BooleanClause<Filter> getFilterBooleanClause();

}
