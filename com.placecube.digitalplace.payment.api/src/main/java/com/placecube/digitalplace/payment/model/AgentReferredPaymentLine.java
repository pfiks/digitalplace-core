package com.placecube.digitalplace.payment.model;

public class AgentReferredPaymentLine {

	private String revenueCode;
	private String referenceNumber;
	private String extraPlaceholder;
	private String amount;

	public String getRevenueCode() {
		return revenueCode;
	}

	public void setRevenueCode(String revenueCode) {
		this.revenueCode = revenueCode;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getExtraPlaceholder() {
		return extraPlaceholder;
	}

	public void setExtraPlaceholder(String extraPlaceholder) {
		this.extraPlaceholder = extraPlaceholder;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
