<%@page import="com.liferay.site.navigation.constants.SiteNavigationConstants"%>
<%@ page import="com.liferay.portal.kernel.util.Constants" %>

<%@ include file="/init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="<%= true %>" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="<%= true %>" var="configurationRenderURL" />

<div class="container-fluid container-fluid-max-xl">

	<aui:form action="<%= configurationActionURL %>" method="post" name="fm">

		<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
		<aui:input name="redirect" type="hidden" value="<%= configurationRenderURL %>" />

		<aui:fieldset>
			<aui:select label="navigation-menu-group-id" name="navigationMenuGroupId">
				<c:forEach items="${availableNavigationMenuGroups}" var="availableNavigationMenuGroup">
					<aui:option value="${availableNavigationMenuGroup.getGroupId()}" selected="${navigationMenuGroupId == availableNavigationMenuGroup.getGroupId()}">
						${availableNavigationMenuGroup.getName(locale)}
					</aui:option>
				</c:forEach>
			</aui:select>
			
			<c:set var="primaryNavType" value="<%=SiteNavigationConstants.TYPE_PRIMARY %>"/>
			<c:set var="secondaryNavType" value="<%=SiteNavigationConstants.TYPE_SECONDARY %>"/>
			<c:set var="socialNavType" value="<%=SiteNavigationConstants.TYPE_SOCIAL %>"/>
			
			<aui:select label="navigation-type" name="navigationType" showEmptyOption="false">
				
				<aui:option value="${primaryNavType}" selected="${navigationType == primaryNavType}">
					<liferay-ui:message key="primary"/>
				</aui:option>
				
				<aui:option value="${secondaryNavType}" selected="${navigationType == secondaryNavType}">
					<liferay-ui:message key="secondary"/>
				</aui:option>
				
				<aui:option value="${socialNavType}" selected="${navigationType == socialNavType}">
					<liferay-ui:message key="social"/>
				</aui:option>
				
			</aui:select>
			
			<aui:input label="navigation-menu-id" name="navigationMenuId"
				helpMessage="navigation-menu-id-help" 
				type="text" value='${navigationMenuId}' />
			
		</aui:fieldset>
		
		<aui:fieldset label="display-settings">
			
			<aui:input label="widget-template-key" name="widgetTemplateKey"
				helpMessage="widget-template-key-help" 
				type="text" value='${ widgetTemplateKey }' required="true"/>
			
			<aui:select label="widget-template-group-id" name="widgetTemplateGroupId">
				<c:forEach items="${availableWidgetTemplateGroups}" var="availableWidgetTemplateGroup">
					<aui:option value="${availableWidgetTemplateGroup.getGroupId()}" selected="${widgetTemplateGroupId == availableWidgetTemplateGroup.getGroupId()}">
						${availableWidgetTemplateGroup.getName(locale)}
					</aui:option>
				</c:forEach>
			</aui:select>
		
		</aui:fieldset>
		
		<aui:button-row>
			<aui:button type="submit" />
		</aui:button-row>
	</aui:form>

</div>
