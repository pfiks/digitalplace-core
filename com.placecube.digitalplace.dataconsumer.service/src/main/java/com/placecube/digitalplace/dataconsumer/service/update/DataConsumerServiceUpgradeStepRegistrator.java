package com.placecube.digitalplace.dataconsumer.service.update;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.dataconsumer.service.update.steps.Update_2_1_0_AddDataConsumerBodyColumn;
import com.placecube.digitalplace.dataconsumer.service.update.steps.Update_2_2_0_DataConsumerTableUpdate;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class DataConsumerServiceUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("1.1.0", "2.1.0", new Update_2_1_0_AddDataConsumerBodyColumn());
		registry.register("2.1.0", "2.2.0", new Update_2_2_0_DataConsumerTableUpdate());
	}

}