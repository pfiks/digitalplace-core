package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetTag;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.search.document.Document;
import com.liferay.portal.search.hits.SearchHit;
import com.liferay.portal.search.hits.SearchHits;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.query.TermQuery;
import com.liferay.portal.search.searcher.SearchRequest;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.SearchResponse;
import com.liferay.portal.search.searcher.Searcher;

public class DDLRelatedSearchQueryUtilTest {

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetTag mockAssetTag;

	@Mock
	private BooleanQuery mockBooleanQuery;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private Document mockDocument;

	@Mock
	private Queries mockQueries;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private Searcher mockSearcher;

	@Mock
	private SearchHit mockSearchHit;

	@Mock
	private SearchHits mockSearchHits;

	@Mock
	private SearchRequest mockSearchRequest;

	@Mock
	private SearchRequestBuilder mockSearchRequestBuilder;

	@Mock
	private SearchResponse mockSearchResponse;

	@Mock
	private TermQuery mockTermQuery2;

	@Mock
	private TermQuery mockTermQuery;

	@InjectMocks
	DDLRelatedSearchQueryUtil ddlRelatedSearchQueryUtil;

	@Before
	public void initSetup() {
		initMocks(this);
	}

	@Test
	public void configureAssetTagIds_WhenNoException_ThenAddsClauseToQuery() {
		List<AssetTag> assetTags = Arrays.asList(mockAssetTag);
		long tagId = 45;

		when(mockAssetEntry.getTags()).thenReturn(assetTags);
		when(mockAssetTag.getTagId()).thenReturn(tagId);
		when(mockQueries.term("assetTagIds", String.valueOf(tagId))).thenReturn(mockTermQuery);

		ddlRelatedSearchQueryUtil.configureAssetTagIds(mockBooleanQuery, mockAssetEntry);

		verify(mockBooleanQuery, times(1)).addShouldQueryClauses(mockTermQuery);
	}

	@Test
	public void configureCategoryIds_WhenNoException_ThenAddsClauseToQuery() {
		long categoryId = 45;
		long[] categoryIds = new long[] { categoryId };

		when(mockAssetEntry.getCategoryIds()).thenReturn(categoryIds);
		when(mockQueries.term("assetCategoryIds", String.valueOf(categoryId))).thenReturn(mockTermQuery);

		ddlRelatedSearchQueryUtil.configureCategoryIds(mockBooleanQuery, mockAssetEntry);

		verify(mockBooleanQuery, times(1)).addShouldQueryClauses(mockTermQuery);
	}

	@Test
	public void configureRecordAndStructureId_WhenNoException_ThenAddsClauseToQuery() throws PortalException {
		long ddlRecordId = 45;
		long ddmStructureId = 34;

		when(mockDDLRecord.getRecordId()).thenReturn(ddlRecordId);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(ddmStructureId);
		when(mockQueries.term("entryClassPK", String.valueOf(ddlRecordId))).thenReturn(mockTermQuery);
		when(mockQueries.term("ddmStructureId", String.valueOf(ddmStructureId))).thenReturn(mockTermQuery2);

		ddlRelatedSearchQueryUtil.configureRecordAndStructureId(mockBooleanQuery, mockDDLRecord);

		verify(mockBooleanQuery, times(1)).addMustNotQueryClauses(mockTermQuery);
		verify(mockBooleanQuery, times(1)).addShouldQueryClauses(mockTermQuery2);

	}

	@Test(expected = PortalException.class)
	public void configureRecordAndStructureId_WhenExceptionRetrievingRecordSet_ThenThrowsPortalException() throws PortalException {
		long ddlRecordId = 45;

		when(mockDDLRecord.getRecordId()).thenReturn(ddlRecordId);
		when(mockDDLRecord.getRecordSet()).thenThrow(PortalException.class);

		ddlRelatedSearchQueryUtil.configureRecordAndStructureId(mockBooleanQuery, mockDDLRecord);

	}

	@Test
	public void configureKeywordsFromTitle() {
		String recordTitle = "Title, and, for Test! question?";
		String[] expectedKeywords = new String[] { "title", "test", "question" };

		List<TermQuery> mocks = new ArrayList<>();

		for (String keyword : expectedKeywords) {
			TermQuery mock = mock(TermQuery.class, "mockTermQuery" + keyword);
			mocks.add(mock);
			when(mockQueries.term("title", keyword)).thenReturn(mock);
		}

		ddlRelatedSearchQueryUtil.configureKeywordsFromTitle(mockBooleanQuery, recordTitle);

		for (TermQuery mock : mocks) {
			verify(mockBooleanQuery, times(1)).addShouldQueryClauses(mock);
		}
		verifyNoMoreInteractions(mockBooleanQuery);

	}

	@Test
	public void configureSearchContext() {
		long companyId = 43;

		doAnswer(invocation -> {
			Consumer<SearchContext> consumer = invocation.getArgument(0);
			consumer.accept(mockSearchContext);
			return null;
		}).when(mockSearchRequestBuilder).withSearchContext(any(Consumer.class));

		ddlRelatedSearchQueryUtil.configureSearchContext(mockSearchRequestBuilder, companyId);

		verify(mockSearchContext, times(1)).setCompanyId(companyId);
		verify(mockSearchContext, times(1)).setEntryClassNames(new String[] { DDLRecord.class.getName() });
	}

	@Test
	public void getResultsFromSearchBuilder() {
		List<SearchHit> searchHitList = Collections.singletonList(mockSearchHit);
		int maxItems = 3;

		when(mockSearchRequestBuilder.query(mockBooleanQuery)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchRequestBuilder.size(maxItems)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchRequestBuilder.build()).thenReturn(mockSearchRequest);
		when(mockSearcher.search(mockSearchRequest)).thenReturn(mockSearchResponse);
		when(mockSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(searchHitList);
		when(mockSearchHit.getDocument()).thenReturn(mockDocument);

		List<Document> result = ddlRelatedSearchQueryUtil.getResultsFromSearchBuilder(mockSearchRequestBuilder, mockBooleanQuery, maxItems);

		assertTrue(result.contains(mockDocument));
	}
}