<%@ include file="../init.jsp"%>

<c:set var="hasError" value="${not empty errorMessage}" />

<div class="govuk-character-count" data-module="govuk-character-count" data-maxlength="${maxCharacters}">

	<div class="govuk-form-group ${hasError ? 'govuk-form-group--error' : ''}" id="${portletNamespace}_field_${fieldName}">
		<label class="govuk-label" for="${portletNamespace}${fieldName}">
			<liferay-ui:message key="${fieldLabel}" />
		</label>
		
		<%@ include file="../fieldErrorMessage.jspf"%>
		
		<textarea class="govuk-textarea govuk-js-character-count ${hasError ? 'govuk-textarea--error' : ''}" id="${portletNamespace}${fieldName}id" 
			name="${portletNamespace}${fieldName}" 
			rows="${fieldRows}" aria-describedby="${portletNamespace}${fieldName}id-info">${fieldValue}</textarea>
		
	</div>
	
	<div id="${portletNamespace}${fieldName}id-info" class="govuk-hint govuk-character-count__message">
		${infoMessage}
	</div>

</div>


