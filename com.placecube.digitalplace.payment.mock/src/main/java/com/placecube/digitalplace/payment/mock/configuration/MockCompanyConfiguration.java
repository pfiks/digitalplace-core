package com.placecube.digitalplace.payment.mock.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.payment.mock.configuration.MockCompanyConfiguration", localization = "content/Language", name = "payment-mock", description = "payment-mock-description")
public interface MockCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();
}
