package com.placecube.digitalplace.editor.configuration.service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.editor.configuration.CKEditorConfigurationService;
import com.placecube.digitalplace.editor.configuration.constants.CKEditorCommonToolbarConstants;

@Component(immediate = true, service = CKEditorCommonToolbarService.class)
public class CKEditorCommonToolbarService {

	@Reference
	private CKEditorCommonToolbarHelper ckEditorCommonToolbarHelper;

	@Reference
	private CKEditorConfigurationService ckEditorConfigurationService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private RoleLocalService roleLocalService;

	public JSONArray buildToolbar(ThemeDisplay themeDisplay) {

		Optional<JSONObject> pluginAndRolesConfig = ckEditorConfigurationService.getPluginAndRolesJsonConfiguration(themeDisplay.getCompanyId());

		Set<String> userRoleNames = getUserRoleName(themeDisplay);

		JSONArray pasteBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.PASTE_TEXT_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.PASTE_FROM_WORD_PLUGIN_NAME);

		JSONArray undoRedoBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);

		JSONArray textFormatBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.BOLD_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.ITALIC_PLUGIN_NAME, CKEditorCommonToolbarConstants.UNDERLINE_PLUGIN_NAME, CKEditorCommonToolbarConstants.STRIKETHROUGH_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.BLOCKQUOTE_PLUGIN_NAME);

		JSONArray numberListBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.NUMBERED_LIST_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.BULLETED_LIST_PLUGIN_NAME);

		JSONArray tableBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.TABLE_PLUGIN_NAME);

		JSONArray linkBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.LINK_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.UNLINK_PLUGIN_NAME, CKEditorCommonToolbarConstants.ANCHOR, CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME, CKEditorCommonToolbarConstants.IMAGE_SELECTOR_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.VIDEO_SELECTOR_PLUGIN_NAME);

		JSONArray audioBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.AUDIO_SELECTOR_PLUGIN_NAME);

		JSONArray sourceBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.SOURCE_PLUGIN_NAME);

		JSONArray additionalPluginsBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.GDS_CONFIRMATION_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_DEFAULT_BUTTON_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_DETAILS_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_DISABLED_BUTTON_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_NOTIFICATION_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_SECONDARY_BUTTON_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.GDS_START_BUTTON_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_WARNING_BUTTON_PLUGIN_NAME, CKEditorCommonToolbarConstants.GDS_WARNING_PLUGIN_NAME,
				CKEditorCommonToolbarConstants.MAXIMIZE_PLUGIN_NAME, CKEditorCommonToolbarConstants.SCAYT_PLUGIN_NAME);

		JSONArray helpBar = ckEditorCommonToolbarHelper.createBarForPluginsAndRoles(pluginAndRolesConfig, userRoleNames, CKEditorCommonToolbarConstants.HELP_DIALOG);

		JSONArray toolbar = jsonFactory.createJSONArray();
		toolbar.put(pasteBar);
		toolbar.put(undoRedoBar);
		toolbar.put(textFormatBar);
		toolbar.put(numberListBar);
		toolbar.put(tableBar);
		toolbar.put(linkBar);
		toolbar.put(audioBar);
		toolbar.put(sourceBar);
		toolbar.put(additionalPluginsBar);
		toolbar.put(helpBar);

		return toolbar;
	}

	public Set<String> getUserRoleName(ThemeDisplay themeDisplay) {

		Set<String> userRoleNames = themeDisplay.getUser().getRoles().stream().map(Role::getName).collect(Collectors.toSet());
		userRoleNames.addAll(roleLocalService.getUserGroupRoles(themeDisplay.getUserId(), themeDisplay.getScopeGroupId()).stream().map(Role::getName).collect(Collectors.toSet()));

		return userRoleNames;
	}
}
