package com.placecube.digitalplace.payment.capitainterface.common.utilities.beans;

/**
 * Enum Class which defines all the Liferay error codes, as well as the ESB error codes which Liferay/common jar libraries could log.
 * As Java enums cannot start with a number, the enum name should be prefixed with an E. The 'code' value should match the numeric part of the
 * enum name. You should also include the description of the error message. These should match the Confluence error code documenation.
 *
 * Again, due to enum naming restrictions, the ESB error codes should be prefixed with an E, and the '-' trnaslated to an '_' in the
 * enum name, but the code field should match the ESB code itself.
 *
 */
public enum ErrorCode {


    E0001 ("0001", "Error reading Parking ESB properties file"),
    E0002 ("0002", "System failure at the ESB Parking apply level - no error code returned from ESB"),
    E0003 ("0003", "The permit type returned from the ESB should be a number"),
    E0004 ("0004", "Length of the permit scheme name field returned from the ESB is invalid"),
    E0005 ("0005", "Mandatory field missing for the createPermitApplication call"),
    E0006 ("0006", "Mandatory field missing in the Applicant object within getPermitDetails response"),
    E0007 ("0007", "Mandatory field missing in one of the Vehicle objects within getPermitDetails response"),
    E0008 ("0008", "Mandatory field missing in one of the Permit objects within getPermitDetails response"),
    E0009 ("0009", "Unrecognised ESB error code"),
    E0010 ("0010", "Error converting date to XMLGregorianCalendar - DatatypeConfigurationException"),
    E0011 ("0011", "System failure at the ESB Parking apply authoriseApplication level - no error code returned from ESB"),
    E0012 ("0012", "System failure at the ESB Vehicle service - no error code returned from ESB"),
    E0013 ("0013", "System failure at the ESB parking renewal level - no error code returned from ESB"),
    E0014 ("0014", "Drupal Content is missing or unpublished"),
	E0015 ("0015", "Drupal is unavailable"),
	E0016 ("0016", "Problem interpreting response from Drupal"),
    E0017 ("0017", "Parking ESB returned error code 202-001 due to permit numbers mapping to multiple separate applications AND one permit number not matching the address"),
    E0018 ("0018", "Parking ESB returned error code 202-001 due to permit numbers mapping to multiple separate applications"),
    E0019 ("0019", "Parking ESB returned error code 202-001 due to at least one permit number not matching the address"),
    E0020 ("0020", "Error reading Concessionary Travel ESB properties file"),
    E0021 ("0021", "System failure at the ESB Concessionary Travel level - no error code returned from ESB"),
	E0022 ("0022", "Error reading Equalities ESB properties file"),
	E0023 ("0023", "System failure at the ESB equalities level - no error code returned from ESB"),
	E0024 ("0024", "Mandatory field missing for the equalities call"),
	E0025 ("0025", "Error reading Address Verification ESB properties file"),
    E0026 ("0026", "Error reading CaseManagement ESB properties file"),
    E0027 ("0027", "System failure at the ESB address verification level - no error code returned from ESB"),
    E0028 ("0028", "Failure in Marshalling Liferay object for XML"),
    E0029 ("0029", "Mandatory field missing for the address verification call"),
    E0030 ("0030", "Deserialization Error"),
    E0031 ("0031", "Serialization Error"),
    E0032 ("0032", "Failure in unmarshalling XML response from the ESB"),
    E0033 ("0033", "System failure with the ESB REST services - no error code returned from ESB"),
    E0034 ("0034", "NO_DATA_FOUND error from the ESB REST services"),
    E0035 ("0035", "Problem encoding postcode data for ESB call"),
    E0036 ("0036", "ClassCastExceltion thrown when trying to unmarshall ESB response"),
    E0037 ("0037", "Mandatory field missing to make the ESB REST call"),
    E0038 ("0038", "System failure with CaseManagement ESB services"),
    E0039 ("0039", "OpenIDM response error"),
    E0040 ("0040", "OpenIDM -Not available"),
    E0041 ("0041", "Liferay Content Exception - content cannot be found"),
    E0042 ("0042", "Drupal Content Metadata problem"),
    E0043 ("0043", "Liferay Portal Exceptions"),
    E0044 ("0044", "Liferay System Exceptions"),
    E0045 ("0045", "Portlet Exception"),
    E0046 ("0046", "OpenIDM - Duplicate User EmailAddress Exception"),
    E0047 ("0047", "OpenIDM - Reserved User EmailAddress Exception"),
    E0048 ("0048", "OpenIDM - User EmailAddress Exception"),
    E0049 ("0049", "OpenIDM - No Such User Exception"),
    E0050 ("0050", "OpenIDM - Principal Exception"),
    E0051 ("0051", "amountInMinorUnits is not a number"),
    E0052 ("0052", "Capita invoke call returns error response"),
    E0053 ("0053", "Capita query call returns error response"),
    E0054 ("0054", "Capita query call succeeds but reports an unsuccessful transaction"),
    E0055 ("0055", "Error reading Capita properties file"),
    E0056 ("0056", "Mandatory field missing for the Capita scpInvoke call"),
    E0057 ("0057", "Capita invoke call fails due to invalid XML or system error at Capita"),
    E0058 ("0058", "Capita query call fails due to invalid XML or system error at Capita"),
    E0059 ("0059", "Error with the createDigest method - likely invalid Capita HMAC key"),
    E0060 ("0060", "Liferay Content Exception - error reading the contents of the content item"),
    E0061 ("0061", "Error loading properties content"),
    E0062 ("0062", "Error in send email - problem reading template file from disk"),
    E0063 ("0063", "Error in send email - problem within the template file"),
    E0064 ("0064", "Error in send email - general system error"),
    E0065 ("0065", "Error in send email - can't read email properties file"),
    E0066 ("0066", "Problem redirecting the user at the end of the current flow"),
    E0067 ("0067", "Unable to submit Equalities information, unknown parent flow"),
    E0068 ("0068", "Directory does not exist and could not be created"),
    E0069 ("0069", "File upload - Could not write uploaded file to disk"),
    E0070 ("0070", "File upload - there was a problem reading the previously uploaded file from disk"),
    E0071 ("0071", "System error during email verification"),
    E0072 ("0072", "Error in run method of LoginAction"),
    E0073 ("0073", "Error accessing the session in LoginAction"),
    E0074 ("0074", "System error accessing the Liferay User in LoginAction"),
    E0075 ("0075", "Error reading the OpenIDM user details"),
    E0076 ("0076", "Problem sending the newUpdateInformOldEmail email"),
    E0077 ("0077", "Problem updating email address for user on OpenIDM"),
    E0078 ("0078", "Problem changing password - user is locked out"),
    E0079 ("0079", "Problem changing password - problem with the password itself"),
    E0080 ("0080", "Problem changing password - no such user exception"),
    E0081 ("0081", "Problem changing password - system error"),
    E0082 ("0082", "System error trying to obtain a password change Ticket"),
    E0083 ("0083", "Fund code missing for zone, so cannot proceed to payment"),
    E0084 ("0084", "UPRN out of zone in a Parking renewal application"),
    E0085 ("0085", "Error creating a valid Date - likely user input error"),
    E0086 ("0086", "Found problem in the photo upload data just prior to ESB CT application submit"),
    E0087 ("0087", "Error reading saved session attribute"),
    E0088 ("0088", "Error removing saved session attribute"),
    E0089 ("0089", "Mandatory field missing within getPermitDetails response"),
    E0090 ("0090", "Communication issue with booking bug"),
    E0091 ("0091", "Invalid response from booking bug."),
    E0092 ("0092", "Liferay portal error."),
    E0093 ("0093", "Ohms create job error."),
    E0094 ("0094", "lagan create case error."),
    E0095 ("0095", "Street-repairs Area look up error."),
    E0096 ("0096", "Schedule Repair Job Error"),
	E0097 ("0097", "System failure at the ESB file management level - no error code returned from ESB"),
    E0098 ("0098", "Error reading File Management ESB properties file"),

    //ESB error codes
    E101_000 ("101-000", "No Data Found"),
    E101_001 ("101-001", "Vehicle Registration [VRN] Not Found"),
    E102_001 ("102-001", "No UPRN found in Salesforce Address table"),
    E102_002 ("102-002", "UPRN has no associated Parking Zone"),
    E102_003 ("102-003", "Search Identifier required"),
    E102_004 ("102-004", "No data found in Salesforce Case table"),
    E102_005 ("102-005", "Salesforce Error"),
    E102_006 ("102-006", "Input Data Invalid"),
    E104_000 ("104-000", "No Data Found"),
    E104_001 ("104-001", "Property Data Not found in CACHE "),
    E104_002 ("104-002", "Vehicle [VRN] Not found in CACHE"),
    E106_002 ("106-002", "Request Category not valid [ CaseCategory ]"),
    E202_002 ("202-002", "Create case webservice unknown error from Salesforce"),
    E202_003 ("202-003", "Create case webservice unknown warning from Salesforce"),
    E202_004 ("202-004", "Equalities webservice SalesForce Generic Error"),
    E202_005 ("202-005", "Equalities webservice SalesForce Generic Warning"),
    E205_001 ("205-001", "Email Address Already in Use"),
    E205_002 ("205-002", "Duplicate Customer Record"),
    E206_001 ("206-001", "Create case webservice unknown error from Salesforce"),
    E206_002 ("206-002", "Create case webservice unknown warning from Salesforce"),
    E206_003 ("206-003", "Equalities webservice SalesForce Generic Error"),
    E206_004 ("206-004", "Equalities webservice SalesForce Generic Warning"),
    E301_001 ("301-001", "Vehicle Registration [VRN] too long"),
    E302_001 ("302-001", "Permit limit exceeded"),
    E302_002 ("302-002", "The permit has been renewed already."),
    E302_003 ("302-003", "Please specify customer, or for non FOI requests check the anonymous customer box if applicable"),
    E302_004 ("302-004", "Please either deselect anonymous user or clear Contact Name. You cannot be both."),
	E308_001 ("308-001", "Transaction Type not supported."),
    E308_002 ("308-002", "ESB EMS Server Timed Out.");

    private String code;
    private String description;



    private ErrorCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }


    public String getDescription() {
        return description;
    }
}
