package com.placecube.digitalplace.search.web.portlet.searchbar.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.search.web.portlet.searchbar.configuration.SearchBarPortletInstanceConfiguration")
public interface SearchBarPortletInstanceConfiguration {
	
	@Meta.AD(required = false)
	String destinationPage();

	@Meta.AD(required = false, deflt = "false")
	boolean searchGuestGroup();
}