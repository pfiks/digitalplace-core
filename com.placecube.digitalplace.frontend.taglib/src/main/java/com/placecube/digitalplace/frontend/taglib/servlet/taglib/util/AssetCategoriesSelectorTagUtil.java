package com.placecube.digitalplace.frontend.taglib.servlet.taglib.util;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.criteria.InfoItemItemSelectorReturnType;
import com.liferay.item.selector.criteria.info.item.criterion.InfoItemItemSelectorCriterion;
import com.liferay.petra.string.StringPool;
import com.liferay.petra.string.StringUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletProvider;
import com.liferay.portal.kernel.portlet.PortletProviderUtil;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.url.builder.PortletURLBuilder;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HashMapBuilder;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.vulcan.util.TransformUtil;
import com.liferay.taglib.aui.AUIUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;

public class AssetCategoriesSelectorTagUtil {

	protected static final String CATEGORY_IDS_SEPARATOR = StringPool.COMMA;
	protected static final String CATEGORY_TITLES_SEPARATOR = "_CATEGORY_";
	private static final Log LOG = LogFactoryUtil.getLog(AssetCategoriesSelectorTagUtil.class);

	private static List<AssetVocabulary> filterVocabularies(List<AssetVocabulary> vocabularies, String className, long classTypePK) {

		long classNameId = PortalUtil.getClassNameId(className);

		return ListUtil.filter(vocabularies, assetVocabulary -> assetVocabulary.isAssociatedToClassNameIdAndClassTypePK(classNameId, classTypePK));
	}

	/**
	 * Gets namespaced asset categories selector id.
	 *
	 * @param namespace namespace.
	 * @return asset categories selector id.
	 */
	public static String getAssetCategoriesSelectorId(String namespace) {
		return namespace + "assetCategoriesSelector";
	}

	/**
	 * Gets an array of ids of selected categories.
	 *
	 * @param vocabularyId id of a vocabulary for which selected categories are
	 *            to be found.
	 * @param ignoreRequestValue specifies if request value of category ids is
	 *            to be ignored.
	 * @param hiddenInput specifies a name of a hidden input with selected
	 *            categories data.
	 * @param request instance of {@link HttpServletRequest}.
	 * @return selected category ids.
	 */
	public static long[] getCategoryIds(long vocabularyId, boolean ignoreRequestValue, String hiddenInput, HttpServletRequest request) {
		if (!ignoreRequestValue) {
			String categoryIdsParam = request.getParameter(hiddenInput + StringPool.UNDERLINE + vocabularyId);

			if (Validator.isNotNull(categoryIdsParam)) {
				return Arrays.stream(categoryIdsParam.split(StringPool.COMMA)).mapToLong(Long::parseLong).toArray();
			}
		}
		return new long[0];
	}

	private static long[] getCategoryIds(long vocabularyId, String className, boolean ignoreRequestValue, String hiddenInput, HttpServletRequest httpServletRequest) {
		String assetCategoryIds = StringPool.BLANK;

		if (!ignoreRequestValue) {
			if (Validator.isNotNull(className)) {
				String[] categoryIdsParam = httpServletRequest.getParameterValues(hiddenInput + StringPool.UNDERLINE + vocabularyId);

				if (categoryIdsParam != null) {
					assetCategoryIds = StringUtil.merge(categoryIdsParam, StringPool.COMMA);
				}
			} else {
				String categoryIdsParam = httpServletRequest.getParameter(hiddenInput);

				if (categoryIdsParam != null) {
					assetCategoryIds = categoryIdsParam;
				}
			}
		}

		return GetterUtil.getLongValues(StringUtil.split(assetCategoryIds));
	}

	private static List<String[]> getCategoryIdsTitles(HttpServletRequest httpServletRequest, String className, boolean ignoreRequestValue, String hiddenInput, List<AssetVocabulary> vocabularies,
			long[] categoryIds) {

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		List<String[]> categoryIdsTitles = new ArrayList<>();

		for (AssetVocabulary vocabulary : vocabularies) {
			long[] categoryIdsArray = ArrayUtil.isNotEmpty(categoryIds) ? categoryIds : getCategoryIds(vocabulary.getVocabularyId(), className, ignoreRequestValue, hiddenInput, httpServletRequest);

			String[] categoryIdsTitle = AssetCategoryUtil.getCategoryIdsTitles(categoryIdsArray, vocabulary.getVocabularyId(), themeDisplay);

			categoryIdsTitles.add(categoryIdsTitle);
		}

		return categoryIdsTitles;
	}

	/**
	 * Gets browse categories / select category event name.
	 *
	 * @return event name for selecting categories.
	 */
	public static String getEventName() {
		String portletId = PortletProviderUtil.getPortletId(AssetCategory.class.getName(), PortletProvider.Action.BROWSE);

		return PortalUtil.getPortletNamespace(portletId) + "selectCategory";
	}

	/**
	 * Gets group current and ancestors group ids.
	 *
	 * @param requestGroupIds group ids from request.
	 * @param scopeGroupId scope group id used if requestGroupIds are not
	 *            provided.
	 * @return current and ancestor group ids of request group if provided or
	 *         scope group otherwise.
	 */
	public static List<Long> getGroupIds(long[] requestGroupIds, long scopeGroupId) {
		long[] groupIds;
		try {
			if (ArrayUtil.isEmpty(requestGroupIds)) {
				groupIds = PortalUtil.getCurrentAndAncestorSiteGroupIds(scopeGroupId);
			} else {
				groupIds = PortalUtil.getCurrentAndAncestorSiteGroupIds(requestGroupIds);
			}

		} catch (Exception e) {
			LOG.warn(e.getMessage());
			groupIds = new long[0];
		}

		return ListUtil.fromArray(groupIds);
	}

	/**
	 * Gets namespaced hidden input name.
	 *
	 * @param namespace namespace.
	 * @param hiddenInput hidden input name.
	 * @return hidden input name.
	 */
	public static String getHiddenInputName(String namespace, String hiddenInput) {
		return namespace + hiddenInput + StringPool.UNDERLINE;
	}

	/**
	 * Gets namespace from request.
	 *
	 * @param request httpServletRequest.
	 * @return aui:form:portletNamespace parameter value.
	 */
	public static String getNamespace(HttpServletRequest request) {
		PortletRequest portletRequest = (PortletRequest) request.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST);
		PortletResponse portletResponse = (PortletResponse) request.getAttribute(JavaConstants.JAVAX_PORTLET_RESPONSE);

		if (portletRequest == null || portletResponse == null) {
			return AUIUtil.getNamespace(request);
		} else {
			return AUIUtil.getNamespace(portletRequest, portletResponse);
		}
	}

	/**
	 * Gets portlet url for asset categories selection.
	 *
	 * @param eventName name of the event to browse categories.
	 * @param httpServletRequest httpServletRequest.
	 * @return portlet url.
	 */
	public static PortletURL getPortletURL(String eventName, HttpServletRequest httpServletRequest) {
		ItemSelector itemSelector = ItemSelectorUtil.getItemSelector();

		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil.create(httpServletRequest);

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		InfoItemItemSelectorCriterion itemSelectorCriterion = new InfoItemItemSelectorCriterion();

		itemSelectorCriterion.setDesiredItemSelectorReturnTypes(new InfoItemItemSelectorReturnType());
		itemSelectorCriterion.setItemType(AssetCategory.class.getName());
		itemSelectorCriterion.setMultiSelection(true);

		PortletURL itemSelectorURL = itemSelector.getItemSelectorURL(requestBackedPortletURLFactory, themeDisplay.getScopeGroup(), themeDisplay.getScopeGroupId(), eventName, itemSelectorCriterion);

		return PortletURLBuilder.create(itemSelectorURL).buildPortletURL();
	}

	/**
	 * Gets themeDisplay from request.
	 *
	 * @param request httpServletRequest.
	 * @return themeDisplay
	 */
	public static ThemeDisplay getThemeDisplay(HttpServletRequest request) {
		return (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	}

	private static List<AssetVocabulary> getVocabularies(String className, long classTypePK, long[] vocabularyIds) {

		List<AssetVocabulary> vocabularies = new ArrayList<>(AssetsUtil.getNonEmptyVocabulariesByIds(vocabularyIds));

		if (Validator.isNotNull(className)) {
			vocabularies = filterVocabularies(vocabularies, className, classTypePK);
		}
		return vocabularies;
	}

	/**
	 * Gets a List of vocabularies descriptor based on vocabularyIds in a form
	 * of key - value map with vocabularies parameters.
	 *
	 * @param vocabularyIds array of vocabulary ids.
	 * @param className class name of a base entry.
	 * @param classTypePK class type primary key of a base entry.
	 * @param showRequiredLabel specifies if required label is to be shown.
	 * @param isSingleSelect specifies if a given vocabulary should allow only
	 *            single category selection.
	 * @param ignoreRequestValue specifies if request value of category ids is
	 *            to be ignored.
	 * @param hiddenInput specifies a name of a hidden input with selected
	 *            categories data.
	 * @param themeDisplay themeDisplay of the current context
	 * @param request instance of {@link HttpServletRequest}
	 * @return a list of vocabularies descriptors.
	 */
	public static List<Map<String, Object>> getVocabulariesDescriptors(long[] vocabularyIds, long[] assetCategoryIds, String className, long classTypePK, boolean showRequiredLabel,
			boolean isSingleSelect, boolean ignoreRequestValue, String hiddenInput, ThemeDisplay themeDisplay, HttpServletRequest request) {

		List<AssetVocabulary> vocabularies = getVocabularies(className, classTypePK, vocabularyIds);
		List<String[]> categoryIdsTitles = getCategoryIdsTitles(request, className, ignoreRequestValue, hiddenInput, vocabularies, assetCategoryIds);

		IntegerWrapper index = new IntegerWrapper(-1);

		return TransformUtil.transform(vocabularies, vocabulary -> {
			index.increment();

			String selectedCategoryIds = categoryIdsTitles.get(index.getValue())[0];

			return HashMapBuilder.<String, Object>put("id", vocabulary.getVocabularyId()).put("required", vocabulary.isRequired(PortalUtil.getClassNameId(className), classTypePK) && showRequiredLabel)
					.put("selectedCategories", selectedCategoryIds).put("selectedItems", () -> {

						List<Map<String, Object>> selectedItems = new ArrayList<>();

						if (Validator.isNull(selectedCategoryIds)) {
							return selectedItems;
						}

						String[] categoryIds = selectedCategoryIds.split(",");

						String selectedCategoryIdTitles = categoryIdsTitles.get(index.getValue())[1];

						String[] categoryTitles = selectedCategoryIdTitles.split(AssetCategoryUtil.CATEGORY_SEPARATOR);

						for (int j = 0; j < categoryIds.length; j++) {
							selectedItems.add(HashMapBuilder.<String, Object>put("label", categoryTitles[j]).put("value", categoryIds[j]).build());
						}

						return selectedItems;
					}).put("singleSelect", isSingleSelect).put("title", vocabulary.getUnambiguousTitle(vocabularies, themeDisplay.getScopeGroupId(), themeDisplay.getLocale()))
					.put("visibilityType", vocabulary.getVisibilityType()).build();
		});
	}

}
