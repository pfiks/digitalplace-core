package com.placecube.digitalplace.user.account.service;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.user.account.constants.WebContentArticles;

public interface AccountSetupService {

	void addArticle(WebContentArticles webContent, JournalFolder journalFolder, ServiceContext serviceContext) throws PortalException;

	JournalFolder addFolder(ServiceContext serviceContext) throws PortalException;

	ServiceContext getServiceContext(Group group);
}
