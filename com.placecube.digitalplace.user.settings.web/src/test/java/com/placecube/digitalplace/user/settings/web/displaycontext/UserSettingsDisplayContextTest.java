package com.placecube.digitalplace.user.settings.web.displaycontext;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.taglib.servlet.PipingServletResponseFactory;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.model.UserSettingsSection;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PipingServletResponseFactory.class, PropsUtil.class, ParamUtil.class, PortletURLUtil.class })
public class UserSettingsDisplayContextTest extends PowerMockito {

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private LiferayPortletResponse mockLiferayPortletResponse;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PageContext mockPageContext;

	@Mock
	private PortletURL mockPortletURL1;

	@Mock
	private PortletURL mockPortletURL2;

	@Mock
	private UserSettingsSection mockUserSettingsSection1;

	@Mock
	private UserSettingsSection mockUserSettingsSection2;

	@Mock
	private UserSettingsSection mockUserSettingsSection3;

	private UserSettingsDisplayContext userSettingsDisplayContext;

	private Map<DisplaySection, List<UserSettingsSection>> userSettingsSections;

	@Before
	public void activateSetup() {
		mockStatic(PipingServletResponseFactory.class, PropsUtil.class, ParamUtil.class, PortletURLUtil.class);

		mockUserSettingsSectionsConfig();

		userSettingsDisplayContext = UserSettingsDisplayContext.initDisplayContext(userSettingsSections);
	}

	@Test
	public void createPipingServletResponse_WhenNoError_ThenReturnsTheHttpServletResponse() {
		when(PipingServletResponseFactory.createPipingServletResponse(mockPageContext)).thenReturn(mockHttpServletResponse);

		HttpServletResponse result = userSettingsDisplayContext.createPipingServletResponse(mockPageContext);

		assertThat(result, sameInstance(mockHttpServletResponse));
	}

	@Test
	public void getSelectedSection_WhenNoSectionFoundForTheSpecifiedId_ThenReturnsTheFirstSection() {
		mockUserSettingsSectionsConfig();
		when(ParamUtil.getString(mockHttpServletRequest, PortletRequestKeys.CURRENT_SECTION_ID, StringPool.BLANK)).thenReturn("differentVal");

		UserSettingsSection result = userSettingsDisplayContext.getSelectedSection(mockHttpServletRequest);

		assertThat(result, sameInstance(userSettingsSections.entrySet().iterator().next().getValue().get(0)));
	}

	@Test
	public void getSelectedSection_WhenSectionFoundForTheSpecifiedId_ThenReturnsThatSection() {
		mockUserSettingsSectionsConfig();
		when(mockUserSettingsSection2.getId()).thenReturn("matchingValue");
		when(ParamUtil.getString(mockHttpServletRequest, PortletRequestKeys.CURRENT_SECTION_ID, StringPool.BLANK)).thenReturn("matchingValue");

		UserSettingsDisplayContext userSettingsDisplayContext = UserSettingsDisplayContext.initDisplayContext(userSettingsSections);

		UserSettingsSection result = userSettingsDisplayContext.getSelectedSection(mockHttpServletRequest);

		assertThat(result, sameInstance(mockUserSettingsSection2));
	}

	@Test
	public void getUserSettingsSections_WhenNoError_ThenReturnsTheSectionsMap() {
		Map<DisplaySection, List<UserSettingsSection>> results = userSettingsDisplayContext.getUserSettingsSections();

		assertThat(results, sameInstance(userSettingsSections));
	}

	@Test(expected = PortletException.class)
	public void getViewURL_WhenExceptionCloningThePortletURL_ThenThrowsPortletException() throws PortletException {
		when(PortletURLUtil.clone(mockPortletURL1, mockLiferayPortletResponse)).thenThrow(new PortletException());

		userSettingsDisplayContext.getViewURL(mockPortletURL1, mockLiferayPortletResponse, "sectionIdVal");
	}

	@Test
	public void getViewURL_WhenNoError_ThenConfiguresTheCurrentSectionIdInTheClonedPortletURL() throws PortletException {
		when(PortletURLUtil.clone(mockPortletURL1, mockLiferayPortletResponse)).thenReturn(mockPortletURL2);
		when(mockPortletURL2.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		userSettingsDisplayContext.getViewURL(mockPortletURL1, mockLiferayPortletResponse, "sectionIdVal");

		verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.CURRENT_SECTION_ID, "sectionIdVal");
	}

	@Test
	public void getViewURL_WhenNoError_ThenReturnsThePortletURLCloned() throws PortletException {
		when(PortletURLUtil.clone(mockPortletURL1, mockLiferayPortletResponse)).thenReturn(mockPortletURL2);
		when(mockPortletURL2.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		PortletURL result = userSettingsDisplayContext.getViewURL(mockPortletURL1, mockLiferayPortletResponse, "sectionId");

		assertThat(result, sameInstance(mockPortletURL2));
	}

	private void mockUserSettingsSectionsConfig() {
		userSettingsSections = new LinkedHashMap<>();

		List<UserSettingsSection> notifications = new ArrayList<>();
		notifications.add(mockUserSettingsSection1);
		userSettingsSections.put(DisplaySection.NOTIFICATIONS, notifications);

		List<UserSettingsSection> accountSettings = new ArrayList<>();
		accountSettings.add(mockUserSettingsSection2);
		accountSettings.add(mockUserSettingsSection3);
		userSettingsSections.put(DisplaySection.ACCOUNT_SETTINGS, accountSettings);
	}
}
