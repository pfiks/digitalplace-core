package com.placecube.digitalplace.user.settings.web.twofactor.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.Optional;

import javax.portlet.RenderRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ GetterUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class TwoFactorAuthenticationUtilTest {

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private UserTwoFactorAuthentication mockUserTwoFactorAuthentication;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	@InjectMocks
	private TwoFactorAuthenticationUtil twoFactorAuthenticationUtil;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(GetterUtil.class);
	}

	@Test
	public void isTwoFactorEnabled_WhenTwoFactorIsNotPresentAndDisplayFormIsFalse_ThenReturnsFalse() {
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM)).thenReturn(false);
		when(GetterUtil.getBoolean(false)).thenReturn(false);

		boolean result = twoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.empty());

		assertFalse(result);
	}

	@Test
	public void isTwoFactorEnabled_WhenTwoFactorIsNotPresentAndDisplayFormIsTrue_ThenReturnsTrue() {
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM)).thenReturn(true);
		when(GetterUtil.getBoolean(true)).thenReturn(true);

		boolean result = twoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.empty());

		assertTrue(result);
	}

	@Test
	public void isTwoFactorEnabled_WhenTwoFactorIsEnabledAndDisplayFormIsTrue_ThenReturnsTrue() {
		when(mockUserTwoFactorAuthentication.getEnabled()).thenReturn(true);
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM)).thenReturn(true);
		when(GetterUtil.getBoolean(true)).thenReturn(true);

		boolean result = twoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication));

		assertTrue(result);
	}

	@Test
	public void isTwoFactorEnabled_WhenTwoFactorIsEnabledAndDisplayFormIsFalse_ThenReturnsTrue() {
		when(mockUserTwoFactorAuthentication.getEnabled()).thenReturn(true);
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM)).thenReturn(false);
		when(GetterUtil.getBoolean(false)).thenReturn(false);

		boolean result = twoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication));

		assertTrue(result);
	}

	@Test
	public void isTwoFactorEnabled_WhenTwoFactorIsNotEnabledAndDisplayFormIsTrue_ThenReturnsTrue() {
		when(mockUserTwoFactorAuthentication.getEnabled()).thenReturn(false);
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM)).thenReturn(true);
		when(GetterUtil.getBoolean(true)).thenReturn(true);

		boolean result = twoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication));

		assertTrue(result);
	}

	@Test
	public void isTwoFactorEnabled_WhenTwoFactorIsNotEnabledAndDisplayFormIsFalse_ThenReturnsFalse() {
		when(mockUserTwoFactorAuthentication.getEnabled()).thenReturn(false);
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM)).thenReturn(false);
		when(GetterUtil.getBoolean(false)).thenReturn(false);

		boolean result = twoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication));

		assertFalse(result);
	}

	@Test
	public void getSecretKey_WhenUserTwoFactorAuthenticationIsNotPresent_ThenReturnsGeneratedSecretKey() {
		String secretKey = "key";

		when(mockUserTwoFactorAuthenticationLocalService.generateSecretKey()).thenReturn(secretKey);

		String result = twoFactorAuthenticationUtil.getSecretKey(mockRenderRequest, Optional.empty());

		verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, Boolean.TRUE);
		assertThat(result, equalTo(secretKey));
	}

	@Test
	public void getSecretKey_WhenUserHasSecretKeyAndResetIsFalse_ThenReturnsSecretKey() {
		String secretKey = "key";

		when(mockUserTwoFactorAuthentication.getSecretKey()).thenReturn(secretKey);

		String result = twoFactorAuthenticationUtil.getSecretKey(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication));

		assertThat(result, equalTo(secretKey));
	}

	@Test
	public void getSecretKey_WhenUserHasSecretKeyAndResetIsTrue_ThenReturnsGeneratedSecretKey() {
		String secretKey = "key";

		when(mockUserTwoFactorAuthentication.getSecretKey()).thenReturn("other");
		when(mockUserTwoFactorAuthentication.getResetKey()).thenReturn(true);
		when(mockUserTwoFactorAuthenticationLocalService.generateSecretKey()).thenReturn(secretKey);

		String result = twoFactorAuthenticationUtil.getSecretKey(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication));

		verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, Boolean.TRUE);
		assertThat(result, equalTo(secretKey));
	}

	@Test
	public void getSecretKey_WhenUserSecretKeyIsNullAndResetIsFalse_ThenReturnsGeneratedSecretKey() {
		String secretKey = "key";

		when(mockUserTwoFactorAuthentication.getSecretKey()).thenReturn(null);
		when(mockUserTwoFactorAuthenticationLocalService.generateSecretKey()).thenReturn(secretKey);

		String result = twoFactorAuthenticationUtil.getSecretKey(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication));

		verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, Boolean.TRUE);
		assertThat(result, equalTo(secretKey));
	}

	@Test
	public void getSecretKey_WhenUserSecretKeyIsEmptyAndResetIsFalse_ThenReturnsGeneratedSecretKey() {
		String secretKey = "key";

		when(mockUserTwoFactorAuthentication.getSecretKey()).thenReturn(StringPool.BLANK);
		when(mockUserTwoFactorAuthenticationLocalService.generateSecretKey()).thenReturn(secretKey);

		String result = twoFactorAuthenticationUtil.getSecretKey(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication));

		verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, Boolean.TRUE);
		assertThat(result, equalTo(secretKey));
	}

}
