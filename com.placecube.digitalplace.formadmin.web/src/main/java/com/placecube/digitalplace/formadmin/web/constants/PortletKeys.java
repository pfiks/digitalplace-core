package com.placecube.digitalplace.formadmin.web.constants;

public final class PortletKeys {

	public static final String FORMS_MANAGER = "com_placecube_digitalplace_formadmin_FormsManagerPortlet";

	private PortletKeys() {

	}
}