package com.placecube.digitalplace.search.web.portlet.category;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.common.portlet.PortletURLService;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.category.configuration.CategoryFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.category.constants.CategoryFacetDisplayConstants;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryConfigurationService;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryFacetTreeService;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryLocalService;
import com.placecube.digitalplace.search.web.service.FacetParsingService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-search-categoryfacet", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=dp-search-facet-category dp-search-facet dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=true", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.use-default-template=true", //
		"com.liferay.portlet.header-portlet-javascript=/category-filter/js/category-filter.js", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.always-display-default-configuration-icons=true", //
		"javax.portlet.init-param.config-template=/category-filter/configuration.jsp", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/category-filter/view.jsp", //
		"javax.portlet.name=" + PortletKeys.CATEGORY_FILTER, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class CategoryFacetPortlet extends MVCPortlet {

	@Reference
	private CategoryConfigurationService categoryConfigurationService;

	@Reference
	private CategoryFacetTreeService categoryFacetTreeService;

	@Reference
	private CategoryLocalService categoryLocalService;

	@Reference
	private FacetParsingService facetParsingService;

	@Reference
	private PortletURLService portletURLService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			Optional<CategoryFacetPortletInstanceConfiguration> configurationOptional = categoryConfigurationService.getValidConfiguration(themeDisplay);

			if (configurationOptional.isPresent()) {

				CategoryFacetPortletInstanceConfiguration configuration = configurationOptional.get();
				Set<Long> aggregatedDisplayCategoryList = categoryLocalService.getAggregateCategoryLists(themeDisplay, configuration.displayVocabularyIds(), configuration.displayCategoryIds());
				SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

				String displayStyle = configuration.displayStyle();
				boolean isDropDownDisplayStyle = CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN.equals(displayStyle);

				String facetFieldName = SearchParameters.SELECTED_CATEGORIES + "_" + configuration.internalPortletId();

				List<FacetResult> availableCategories = getCategories(aggregatedDisplayCategoryList, searchResponse, configuration.invisible(), facetFieldName,
						configuration.showAllCategoriesAlways());

				if (isDropDownDisplayStyle) {

					Optional<FacetResult> firstSelected = availableCategories.stream().filter(FacetResult::isSelected).findFirst();

					if (firstSelected.isPresent()) {
						renderRequest.setAttribute("selectedFacetValue", firstSelected.get().getValue());
					}
				}

				sortCategories(availableCategories, themeDisplay.getLocale(), configuration.alphaSort());

				renderRequest.setAttribute("availableCategories", availableCategories);
				renderRequest.setAttribute("configuration", configuration);
				renderRequest.setAttribute("displayStyle", displayStyle);
				renderRequest.setAttribute("facetFieldName", facetFieldName);
				renderRequest.setAttribute("isEditLayoutMode", portletURLService.isEditLayoutMode(renderRequest));
				renderRequest.setAttribute("redirectURL", searchResponse.getEscapedSearchResultsIteratorURL());

				if (CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_TREE.equals(displayStyle)) {
					renderRequest.setAttribute("categoriesTree", categoryFacetTreeService.toJSONTree(availableCategories, searchResponse.getActiveValuesForFilter(facetFieldName),
							configuration.maxTreeViewDepth(), themeDisplay.getLocale()));
				}
			} else {
				renderRequest.setAttribute("invalidConfiguration", true);
			}
			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

	private List<FacetResult> getAllCategories(Set<Long> aggregatedDisplayCategoryList, SharedSearchResponse searchResponse, boolean invisible, String facetFieldName) {
		if (invisible) {
			return new ArrayList<>();
		}

		Map<String, Integer> facetValues = searchResponse.getFacetValues(SearchFacetConstants.ASSET_CATEGORY_IDS);
		Set<String> selectedFacets = searchResponse.getActiveValuesForFilter(facetFieldName);
		return facetParsingService.parseAllCategories(facetValues, aggregatedDisplayCategoryList, selectedFacets);
	}

	private List<FacetResult> getAvailableCategories(Set<Long> aggregatedDisplayCategoryList, SharedSearchResponse searchResponse, boolean invisible, String facetFieldName) {
		if (invisible) {
			return new ArrayList<>();
		}

		Map<String, Integer> facetValues = searchResponse.getFacetValues(SearchFacetConstants.ASSET_CATEGORY_IDS);
		Set<String> selectedFacets = searchResponse.getActiveValuesForFilter(facetFieldName);
		List<FacetResult> availableCategories = facetParsingService.parseCategoryFacets(facetValues, selectedFacets);

		if (!aggregatedDisplayCategoryList.isEmpty()) {
			return availableCategories.stream().filter(f -> aggregatedDisplayCategoryList.contains(GetterUtil.getLong(f.getValue()))).collect(Collectors.toList());
		}

		return availableCategories;
	}

	private List<FacetResult> getCategories(Set<Long> aggregatedDisplayCategoryList, SharedSearchResponse searchResponse, boolean invisible, String facetFieldName, boolean showAllCategories) {
		if (showAllCategories) {
			return getAllCategories(aggregatedDisplayCategoryList, searchResponse, invisible, facetFieldName);
		}
		return getAvailableCategories(aggregatedDisplayCategoryList, searchResponse, invisible, facetFieldName);

	}

	private void sortCategories(List<FacetResult> categories, Locale locale, boolean alphaSort) {

		if (alphaSort) {
			categories.sort(Comparator.comparing(fr -> fr.getLabel(locale)));
		}

	}
}