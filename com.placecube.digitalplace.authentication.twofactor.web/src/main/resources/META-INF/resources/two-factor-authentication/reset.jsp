<%@ include file="init.jsp" %>

<portlet:actionURL name="<%=TwoFactorMVCCommandKeys.USER_RESET %>" var="userResetTwoFactorAuthentication">
	<portlet:param name="redirect" value="${redirect}" />
	<portlet:param name="twoFactorSecretKey" value="${twoFactorSecretKey}"/>
</portlet:actionURL>

<portlet:renderURL var="backURL">
	<portlet:param name="mvcRenderCommandName" value="<%= TwoFactorMVCCommandKeys.VERIFY %>" />
	<portlet:param name="redirect" value="${redirect}" />
</portlet:renderURL>

<div class="two-factor-authenticator two-factor-authenticator-setup">

	<aui:form action="${ userResetTwoFactorAuthentication }" cssClass="container-fluid container-fluid-max-xl sign-in-form" data-senna-off="true" method="post" name="userResetTwoFactorAuthenticationForm" >
	
		<liferay-journal:journal-article 
			articleId="<%=WebContentArticles.TWO_FACTOR_USER_RESET.getArticleId() %>"
			groupId="${themeDisplay.getCompanyGroupId()}" 
			showTitle="false" />		
		
		<liferay-ui:error key="twoFactorVerificationFailed" message="two-factor-authentication-has-failed" />
		
		<div class="form-group">		
			<p>
				<liferay-ui:message key="secret-key"/>
			</p>
			<p>${twoFactorSecretKey}</p>
			
			<img alt="qrcode" src="${twoFactorUrlQR}" class="" width="166" height="166">							
		</div>
		
		<aui:input autocomplete="off" label="authenticator-app-code" name="twoFactorAuthCode" required="true" />
		
		<aui:button-row>
			<aui:button type="submit" value="reset" />
			<aui:button value="cancel" href="${backURL}" />
		</aui:button-row>
	</aui:form>
</div>