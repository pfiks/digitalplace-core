package com.placecube.digitalplace.search.web.portlet.searchbar;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.model.impl.SortByScore;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SEARCH_BAR, "mvc.command.name=" + MVCCommandKeys.SEARCH }, service = MVCActionCommand.class)
public class KeywordSearchMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Reference(service = SortByScore.class)
	private SortByScore sortByScore;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String keywords = ParamUtil.getString(actionRequest, SearchParameters.SEARCH_KEYWORD);

		if (Validator.isNotNull(keywords)) {
			sharedSearchRequestService.clearSharedSearchSession(actionRequest);
			sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, SearchParameters.SEARCH_KEYWORD, keywords);
			sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, SearchParameters.SELECTED_SORT_BY, sortByScore.getId());
		} else {
			sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, SearchParameters.SEARCH_KEYWORD);
		}

		sharedSearchRequestService.sendRedirectFromParameter(actionRequest, actionResponse, "redirectURL");
	}

}
