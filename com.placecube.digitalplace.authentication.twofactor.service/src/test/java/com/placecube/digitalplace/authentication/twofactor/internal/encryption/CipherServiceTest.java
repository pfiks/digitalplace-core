package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Base64.class, Cipher.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class CipherServiceTest {

	private static final String UUID = "237e8c17-6f94-56c5-f15a-fa46ed5f53f3";

	@InjectMocks
	private CipherService cipherService;

	@Mock
	private Cipher mockCipher;

	@Mock
	private GCMParameterSpec mockGCMParameterSpec;

	@Mock
	private GCMParameterSpecService mockGCMParameterSpecService;

	@Mock
	private SecretKeySpec mockSecretKeySpec;

	@Mock
	private SecretKeySpecService mockSecretKeySpecService;

	public static Object[][] provideValidCipherModes() {
		return new Object[][] { new Object[] { Cipher.DECRYPT_MODE }, new Object[] { Cipher.ENCRYPT_MODE }, new Object[] { Cipher.UNWRAP_MODE }, new Object[] { Cipher.WRAP_MODE } };
	}

	@Before
	public void activateSetup() {
		mockStatic(Base64.class, Cipher.class);

		mockCipher = PowerMockito.mock(Cipher.class);
	}

	@Test
	@Parameters(method = "provideValidCipherModes")
	public void getCipher_WhenErrorOnGettingGCMParameterSpec_ThenReturnsEmptyOptional(String mode) throws Exception {
		when(mockGCMParameterSpecService.getGCMParameterSpec(UUID)).thenThrow(new IOException());

		Optional<Cipher> result = cipherService.getCipher(Integer.parseInt(mode), UUID);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters(method = "provideValidCipherModes")
	public void getCipher_WhenErrorOnGettingSecretKeySpec_ThenReturnsEmptyOptional(String mode) throws Exception {
		when(mockGCMParameterSpecService.getGCMParameterSpec(UUID)).thenReturn(mockGCMParameterSpec);
		when(mockSecretKeySpecService.getSecretKeySpec(UUID)).thenThrow(new IOException());

		Optional<Cipher> result = cipherService.getCipher(Integer.parseInt(mode), UUID);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters(method = "provideValidCipherModes")
	public void getCipher_WhenInvalidAlgorithmParameterException_ThenReturnsEmptyOptional(String mode) throws Exception {
		when(mockGCMParameterSpecService.getGCMParameterSpec(UUID)).thenReturn(mockGCMParameterSpec);
		when(mockSecretKeySpecService.getSecretKeySpec(UUID)).thenReturn(mockSecretKeySpec);
		PowerMockito.doReturn(mockCipher).when(Cipher.class, "getInstance", CipherService.CIPHER_TRANSFORM_VALUE);
		doThrow(new InvalidAlgorithmParameterException()).when(mockCipher).init(anyInt(), any(SecretKeySpec.class), any(GCMParameterSpec.class));

		Optional<Cipher> result = cipherService.getCipher(Integer.parseInt(mode), UUID);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters(method = "provideValidCipherModes")
	public void getCipher_WhenInvalidKeyException_ThenReturnsEmptyOptional(String mode) throws Exception {
		when(mockGCMParameterSpecService.getGCMParameterSpec(UUID)).thenReturn(mockGCMParameterSpec);
		when(mockSecretKeySpecService.getSecretKeySpec(UUID)).thenReturn(mockSecretKeySpec);
		PowerMockito.doReturn(mockCipher).when(Cipher.class, "getInstance", CipherService.CIPHER_TRANSFORM_VALUE);
		doThrow(new InvalidKeyException()).when(mockCipher).init(anyInt(), any(SecretKeySpec.class), any(GCMParameterSpec.class));

		Optional<Cipher> result = cipherService.getCipher(Integer.parseInt(mode), UUID);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters(method = "provideValidCipherModes")
	public void getCipher_WhenNoError_ThenReturnsCipher(String mode) throws Exception {
		when(mockGCMParameterSpecService.getGCMParameterSpec(UUID)).thenReturn(mockGCMParameterSpec);
		when(mockSecretKeySpecService.getSecretKeySpec(UUID)).thenReturn(mockSecretKeySpec);

		PowerMockito.doReturn(mockCipher).when(Cipher.class, "getInstance", CipherService.CIPHER_TRANSFORM_VALUE);

		Optional<Cipher> result = cipherService.getCipher(Integer.parseInt(mode), UUID);

		assertThat(result.get(), sameInstance(mockCipher));
	}

	@Test
	@Parameters(method = "provideValidCipherModes")
	public void getCipher_WhenNoSuchAlgorithmException_ThenReturnsEmptyOptional(String mode) throws Exception {
		when(mockGCMParameterSpecService.getGCMParameterSpec(UUID)).thenReturn(mockGCMParameterSpec);
		when(mockSecretKeySpecService.getSecretKeySpec(UUID)).thenReturn(mockSecretKeySpec);
		PowerMockito.doThrow(new NoSuchAlgorithmException()).when(Cipher.class, "getInstance", CipherService.CIPHER_TRANSFORM_VALUE);

		Optional<Cipher> result = cipherService.getCipher(Integer.parseInt(mode), UUID);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters(method = "provideValidCipherModes")
	public void getCipher_WhenNoSuchPaddingException_ThenReturnsEmptyOptional(String mode) throws Exception {
		when(mockGCMParameterSpecService.getGCMParameterSpec(UUID)).thenReturn(mockGCMParameterSpec);
		when(mockSecretKeySpecService.getSecretKeySpec(UUID)).thenReturn(mockSecretKeySpec);
		PowerMockito.doThrow(new NoSuchPaddingException()).when(Cipher.class, "getInstance", CipherService.CIPHER_TRANSFORM_VALUE);

		Optional<Cipher> result = cipherService.getCipher(Integer.parseInt(mode), UUID);

		assertFalse(result.isPresent());
	}
}
