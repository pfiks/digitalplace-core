package com.placecube.digitalplace.ddl.web.portlet.display;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLPermissionService;

@Component(immediate = true, property = { //
		"javax.portlet.name=" + PortletKeys.DDL_DISPLAY_PORTLET, //
		"mvc.command.name=" + MVCCommandKeys.DELETE_ENTRY //
}, service = MVCActionCommand.class)
public class DeleteDDLEntryMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(DeleteDDLEntryMVCActionCommand.class);

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private DDLPermissionService ddlPermissionService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long ddlRecordId = ParamUtil.getLong(actionRequest, "ddlRecordId");
		LOG.debug("Removing ddlRecord: " + ddlRecordId);

		ddlPermissionService.validateDeletePermission(themeDisplay.getPermissionChecker(), ddlRecordId);
		ddlRecordLocalService.deleteRecord(ddlRecordId);
	}

}
