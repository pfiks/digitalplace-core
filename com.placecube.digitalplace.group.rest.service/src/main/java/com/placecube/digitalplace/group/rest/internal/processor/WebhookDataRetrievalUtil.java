package com.placecube.digitalplace.group.rest.internal.processor;

import java.util.Locale;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;

@Component(immediate = true, service = WebhookDataRetrievalUtil.class)
public class WebhookDataRetrievalUtil {

	private static final Log LOG = LogFactoryUtil.getLog(WebhookDataRetrievalUtil.class);

	@Reference
	private DynamicDataListConfigurationService dynamicDataListConfigurationService;

	public String getDDLRecordTitle(DDLRecord ddlRecord, Group group) {
		try {
			DDMStructure structure = ddlRecord.getRecordSet().getDDMStructure();
			Optional<DynamicDataListMapping> mappingOptional = dynamicDataListConfigurationService.getDynamicDataListMapping(structure);

			if (mappingOptional.isPresent()) {
				Optional<DDMFormFieldValue> mappedTitleOptionalFormFieldValue = ddlRecord.getDDMFormFieldValues(mappingOptional.get().getIndexerTitleFieldName()).stream().findFirst();
				Locale locale = LocaleUtil.fromLanguageId(group.getDefaultLanguageId());

				if (mappedTitleOptionalFormFieldValue.isPresent()) {
					return mappedTitleOptionalFormFieldValue.get().getValue().getString(locale);
				}
			}
		} catch (Exception e) {
			LOG.debug(e);
			LOG.warn("Unable to get DDLRecord title " + e.getMessage());
		}

		return StringPool.BLANK;
	}

}
