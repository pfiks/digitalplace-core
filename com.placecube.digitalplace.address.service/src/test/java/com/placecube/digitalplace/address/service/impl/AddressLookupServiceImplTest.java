package com.placecube.digitalplace.address.service.impl;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.internal.WhiteboxImpl;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.ListTypeConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.AddressLocalService;
import com.liferay.portal.kernel.service.ListTypeLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.address.constants.AddressType;
import com.placecube.digitalplace.address.constants.expando.UserExpandoUPRN;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookup;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ListUtil.class, PropsUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AddressLookupServiceImplTest extends PowerMockito {

	private static final String ADDRESS_LINE_1 = "addressLine1Value";
	private static final String ADDRESS_LINE_2 = "addressLine2Value";
	private static final String ADDRESS_LINE_3 = "addressLine3Value";
	private static final String CITY = "cityValue";
	private static final Long COMPANY_ID = 11l;
	private static final Long CONTACT_ID = 33l;
	private static final long LIST_TYPE_ID = 44;
	private static final String[] PARAMS_AND_VALUES = new String[] { "one", "two" };
	private static final String POSTCODE = "postcodeValue";
	private static final String UPRN = "uprnValue";
	private static final Long USER_ID = 22l;

	@InjectMocks
	private AddressLookupServiceImpl addressLookupServiceImpl;

	@Mock
	private Address mockAddress;

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private Set<AddressContext> mockAddressContextSet;

	@Mock
	private AddressLocalService mockAddressLocalService;

	@Mock
	private AddressLookupHelper mockAddressLookupHelper;

	@Mock
	private List<AddressLookup> mockAddressLookupListLocal;

	@Mock
	private List<AddressLookup> mockAddressLookupListNational;

	@Mock
	private Set<AddressLookup> mockAddressLookups;

	@Mock
	private ExpandoBridge mockExpandoBridge;

	@Mock
	private ListType mockListType;

	@Mock
	private ListTypeLocalService mockListTypeLocalService;

	@Mock
	private AddressLookup mockNationalAddressLookup;

	@Mock
	private List<AddressLookup> mockNationalAddressLookupList;

	@Mock
	private List<AddressLookup> mockNationalLookupList;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Before
	public void activeSetUp() {
		mockStatic(ListUtil.class, PropsUtil.class);
	}

	@Test
	public void createAddressContext_WhenNoError_ThenReturnAddressContext() {
		String uprn = "uprn";
		String addressLine1 = "addressLine1";
		String addressLine2 = "addressLine2";
		String addressLine3 = "addressLine3";
		String addressLine4 = "addressLine4";
		String city = "city";
		String postcode = "postcode";

		AddressContext result = addressLookupServiceImpl.createAddressContext(uprn, addressLine1, addressLine2, addressLine3, addressLine4, city, postcode);
		assertEquals(uprn, result.getUPRN());
		assertEquals(addressLine1, result.getAddressLine1());
		assertEquals(addressLine2, result.getAddressLine2());
		assertEquals(addressLine3, result.getAddressLine3());
		assertEquals(addressLine4, result.getAddressLine4());
		assertEquals(city, result.getCity());
		assertEquals(postcode, result.getPostcode());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByKeyword_WhenKeywordIsEmpty_ThenReturnsEmptySet(boolean fallBackToNational) throws PortalException {
		Set<AddressContext> results = addressLookupServiceImpl.getByKeyword(COMPANY_ID, "  ", PARAMS_AND_VALUES, fallBackToNational);

		assertThat(results, empty());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByKeyword_WhenKeywordIsNull_ThenReturnsEmptySet(boolean fallBackToNational) throws PortalException {
		Set<AddressContext> results = addressLookupServiceImpl.getByKeyword(COMPANY_ID, null, PARAMS_AND_VALUES, fallBackToNational);

		assertThat(results, empty());
	}

	@Test
	public void getByKeyword_WhenKeywordIsValidValueAndFallbackToNationalIsTrueAndNotFoundInLocalListButFoundInNational_ThenReturnsTheAddressContexts() throws PortalException {

		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		when(mockAddressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Collections.emptySet());
		when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
		when(mockAddressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListNational)).thenReturn(mockAddressContextSet);

		Set<AddressContext> results = addressLookupServiceImpl.getByKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, true);

		assertThat(results, sameInstance(mockAddressContextSet));
	}

	@Test
	public void getByKeyword_WhenKeywordIsValidValueAndFallbackToNationalIsTrueAndNotFoundInLocalListNorInNational_ThenReturnsEmptySet() throws PortalException {

		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		when(mockAddressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Collections.emptySet());
		when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
		when(mockAddressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListNational)).thenReturn(Collections.emptySet());

		Set<AddressContext> results = addressLookupServiceImpl.getByKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, true);

		assertThat(results, empty());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByKeyword_WhenKeywordIsValidValueAndIsFoundInLocalList_ThenReturnsTheAddressContexts(boolean fallBackToNational) throws PortalException {

		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		if (fallBackToNational) {
			when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
		}
		when(mockAddressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(mockAddressContextSet);

		Set<AddressContext> results = addressLookupServiceImpl.getByKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, fallBackToNational);

		assertThat(results, sameInstance(mockAddressContextSet));
		verify(mockAddressLookupHelper, never()).fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockNationalAddressLookupList);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByKeyword_WhenKeywordIsValidValueButNotFound_ThenReturnsEmptySet(boolean fallBackToNational) throws PortalException {
		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		when(mockAddressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Collections.emptySet());
		if (fallBackToNational) {
			when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
			when(mockAddressLookupHelper.fetchKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListNational)).thenReturn(Collections.emptySet());
		}

		Set<AddressContext> results = addressLookupServiceImpl.getByKeyword(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, fallBackToNational);

		assertThat(results, empty());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByPostcode_WhenPostcodeIsEmpty_ThenReturnsEmptySet(boolean fallBackToNational) throws PortalException {
		Set<AddressContext> results = addressLookupServiceImpl.getByPostcode(COMPANY_ID, "  ", PARAMS_AND_VALUES, fallBackToNational);

		assertThat(results, empty());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByPostcode_WhenPostcodeIsNull_ThenReturnsEmptySet(boolean fallBackToNational) throws PortalException {
		Set<AddressContext> results = addressLookupServiceImpl.getByPostcode(COMPANY_ID, null, PARAMS_AND_VALUES, fallBackToNational);

		assertThat(results, empty());
	}

	@Test
	public void getByPostcode_WhenPostcodeIsValidValueAndFallbackToNationalIsTrueAndNotFoundInLocalListButFoundInNational_ThenReturnsTheAddressContexts() throws PortalException {

		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		when(mockAddressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Collections.emptySet());
		when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
		when(mockAddressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListNational)).thenReturn(mockAddressContextSet);

		Set<AddressContext> results = addressLookupServiceImpl.getByPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, true);

		assertThat(results, sameInstance(mockAddressContextSet));
	}

	@Test
	public void getByPostcode_WhenPostcodeIsValidValueAndFallbackToNationalIsTrueAndNotFoundInLocalListNorInNational_ThenReturnsEmptySet() throws PortalException {

		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		when(mockAddressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Collections.emptySet());
		when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
		when(mockAddressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListNational)).thenReturn(Collections.emptySet());

		Set<AddressContext> results = addressLookupServiceImpl.getByPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, true);

		assertThat(results, empty());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByPostcode_WhenPostcodeIsValidValueAndIsFoundInLocalList_ThenReturnsTheAddressContexts(boolean fallBackToNational) throws PortalException {

		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		if (fallBackToNational) {
			when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
		}
		when(mockAddressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(mockAddressContextSet);

		Set<AddressContext> results = addressLookupServiceImpl.getByPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, fallBackToNational);

		assertThat(results, sameInstance(mockAddressContextSet));
		verify(mockAddressLookupHelper, never()).fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockNationalAddressLookupList);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByPostcode_WhenPostcodeIsValidValueButNotFound_ThenReturnsEmptySet(boolean fallBackToNational) throws PortalException {
		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		when(mockAddressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Collections.emptySet());
		if (fallBackToNational) {
			when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
			when(mockAddressLookupHelper.fetchPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListNational)).thenReturn(Collections.emptySet());
		}

		Set<AddressContext> results = addressLookupServiceImpl.getByPostcode(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, fallBackToNational);

		assertThat(results, empty());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByUprn_WhenUprnIsEmpty_ThenReturnsEmptyOptional(boolean fallBackToNational) throws PortalException {
		Optional<AddressContext> result = addressLookupServiceImpl.getByUprn(COMPANY_ID, "  ", PARAMS_AND_VALUES, fallBackToNational);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByUprn_WhenUprnIsNull_ThenReturnsEmptyOptional(boolean fallBackToNational) throws PortalException {
		Optional<AddressContext> result = addressLookupServiceImpl.getByUprn(COMPANY_ID, null, PARAMS_AND_VALUES, fallBackToNational);

		assertFalse(result.isPresent());
	}

	@Test
	public void getByUprn_WhenUprnIsValidValueAndFallbackToNationalIsTrueAndNotFoundInLocalListButFoundInNational_ThenReturnsTheAddressContext() throws PortalException {

		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		when(mockAddressLookupHelper.fetchUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Optional.empty());
		when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
		when(mockAddressLookupHelper.fetchUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListNational)).thenReturn(Optional.of(mockAddressContext));

		Optional<AddressContext> result = addressLookupServiceImpl.getByUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, true);

		assertThat(result.get(), sameInstance(mockAddressContext));
	}

	@Test
	public void getByUprn_WhenUprnIsValidValueAndFallbackToNationalIsTrueAndNotFoundInLocalListNorInNational_ThenReturnsEmptyOptional() throws PortalException {

		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		when(mockAddressLookupHelper.fetchUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Optional.empty());
		when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
		when(mockAddressLookupHelper.fetchUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListNational)).thenReturn(Optional.empty());

		Optional<AddressContext> result = addressLookupServiceImpl.getByUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, true);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByUprn_WhenUprnIsValidValueAndIsFoundInLocalList_ThenReturnsTheAddressContext(boolean fallBackToNational) throws PortalException {

		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		if (fallBackToNational) {
			when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
		}
		when(mockAddressLookupHelper.fetchUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Optional.of(mockAddressContext));

		Optional<AddressContext> result = addressLookupServiceImpl.getByUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, fallBackToNational);

		assertThat(result.get(), sameInstance(mockAddressContext));
		verify(mockAddressLookupHelper, never()).fetchUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockNationalAddressLookupList);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getByUprn_WhenUprnIsValidValueButNoEntryFound_ThenReturnsEmptyOptional(boolean fallBackToNational) throws PortalException {
		WhiteboxImpl.setInternalState(addressLookupServiceImpl, "addressLookups", mockAddressLookups);

		when(mockAddressLookupHelper.getLocalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListLocal);
		when(mockAddressLookupHelper.fetchUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListLocal)).thenReturn(Optional.empty());
		if (fallBackToNational) {
			when(mockAddressLookupHelper.getNationalAddressLookup(COMPANY_ID, mockAddressLookups)).thenReturn(mockAddressLookupListNational);
			when(mockAddressLookupHelper.fetchUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, mockAddressLookupListNational)).thenReturn(Optional.empty());
		}

		Optional<AddressContext> result = addressLookupServiceImpl.getByUprn(COMPANY_ID, POSTCODE, PARAMS_AND_VALUES, fallBackToNational);

		assertFalse(result.isPresent());
	}

	@Test(expected = PortalException.class)
	public void saveAddress_WhenExceptionCreatingTheAddress_ThenThrowsPortalException() throws PortalException {
		mockAddressCreationDetails();
		when(mockAddressLocalService.addAddress(null, USER_ID, Contact.class.getName(), CONTACT_ID, null, null, ADDRESS_LINE_1, ADDRESS_LINE_2, ADDRESS_LINE_3, CITY, POSTCODE, 0, 0, LIST_TYPE_ID,
				false, true, null, mockServiceContext)).thenThrow(new PortalException());
		addressLookupServiceImpl.saveAddress(mockUser, mockAddressContext, mockServiceContext);
	}

	@Test
	public void saveAddress_WhenNoError_ThenReturnsTheCreatedAddress() throws PortalException {
		mockAddressCreationDetails();
		when(mockAddressLocalService.addAddress(null, USER_ID, Contact.class.getName(), CONTACT_ID, null, null, ADDRESS_LINE_1, ADDRESS_LINE_2, ADDRESS_LINE_3, CITY, POSTCODE, 0, 0, LIST_TYPE_ID,
				false, true, null, mockServiceContext)).thenReturn(mockAddress);

		Address result = addressLookupServiceImpl.saveAddress(mockUser, mockAddressContext, mockServiceContext);

		assertThat(result, sameInstance(mockAddress));
	}

	@Test
	public void saveAddress_WhenNoError_ThenSavesTheUPRNAsExpandoValue() throws PortalException {
		mockAddressCreationDetails();
		when(mockAddressLocalService.addAddress(null, USER_ID, Contact.class.getName(), CONTACT_ID, null, null, ADDRESS_LINE_1, ADDRESS_LINE_2, ADDRESS_LINE_3, CITY, POSTCODE, 0, 0, LIST_TYPE_ID,
				false, true, null, mockServiceContext)).thenReturn(mockAddress);

		addressLookupServiceImpl.saveAddress(mockUser, mockAddressContext, mockServiceContext);

		verify(mockExpandoBridge, times(1)).setAttribute(UserExpandoUPRN.FIELD_NAME, UPRN, false);
	}

	private void mockAddressCreationDetails() {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockListTypeLocalService.getListType(COMPANY_ID, AddressType.PRIMARY.getName(), ListTypeConstants.CONTACT_ADDRESS)).thenReturn(mockListType);
		when(mockListType.getListTypeId()).thenReturn(LIST_TYPE_ID);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getContactId()).thenReturn(CONTACT_ID);
		when(mockAddressContext.getAddressLine1()).thenReturn(ADDRESS_LINE_1);
		when(mockAddressContext.getAddressLine2()).thenReturn(ADDRESS_LINE_2);
		when(mockAddressContext.getAddressLine3()).thenReturn(ADDRESS_LINE_3);
		when(mockAddressContext.getCity()).thenReturn(CITY);
		when(mockAddressContext.getPostcode()).thenReturn(POSTCODE);
		when(mockAddressContext.getUPRN()).thenReturn(UPRN);
		when(mockAddressContext.getAddressType()).thenReturn(AddressType.PRIMARY);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
	}
}
