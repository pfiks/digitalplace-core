package com.placecube.digitalplace.editor.configuration.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.editor.configuration.constants.CKEditorCommonToolbarConstants;

@Component(immediate = true, service = CKEditorCommonToolbarHelper.class)
public class CKEditorCommonToolbarHelper {

	@Reference
	private JSONFactory jsonFactory;

	public JSONArray createBarForPluginsAndRoles(Optional<JSONObject> pluginAndRolesConfig, Set<String> userRoleNames, String... pluginNames) {
		JSONArray bar = jsonFactory.createJSONArray();
		for (String pluginName : pluginNames) {
			putPlugin(bar, pluginName, pluginAndRolesConfig, userRoleNames);
		}

		return bar;
	}

	public Optional<JSONArray> getStylePluginJsonArray(Optional<JSONObject> pluginAndRolesConfig) {
		if (pluginAndRolesConfig.isPresent() && Validator.isNotNull(pluginAndRolesConfig.get().getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME))) {
			return Optional.of(pluginAndRolesConfig.get().getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME));
		}
		return Optional.empty();
	}

	public boolean hasUserRole(Set<String> userRoleNames, String roleNames) {

		boolean checkPermission = false;

		List<String> pluginroleNames = Arrays.asList(roleNames.split(StringPool.COMMA)).stream().map(String::trim).collect(Collectors.toList());
		checkPermission = userRoleNames.stream().anyMatch(pluginroleNames::contains);

		return checkPermission;
	}

	private void putPlugin(JSONArray bar, String pluginName, Optional<JSONObject> pluginAndRolesConfig, Set<String> userRoleNames) {

		if (pluginName.equals(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)) {
			putStylePlugin(bar, pluginAndRolesConfig, userRoleNames);

		} else {

			boolean hasPermissionOnPlugin = true;

			if (pluginAndRolesConfig.isPresent() && pluginAndRolesConfig.get().length() > 0) {
				String pluginRoles = pluginAndRolesConfig.get().getString(pluginName);

				if (Validator.isNotNull(pluginRoles)) {
					hasPermissionOnPlugin = hasUserRole(userRoleNames, pluginRoles);
				}
			}

			if (hasPermissionOnPlugin) {
				bar.put(pluginName);
			}
		}

	}

	private void putStylePlugin(JSONArray linkBar, Optional<JSONObject> pluginAndRolesConfig, Set<String> userRoleNames) {

		Optional<JSONArray> styleJSONArray = getStylePluginJsonArray(pluginAndRolesConfig);

		if (styleJSONArray.isPresent() && styleJSONArray.get().length() > 0) {
			for (int i = 0; i < styleJSONArray.get().length(); i++) {

				JSONObject styleJSONObject = styleJSONArray.get().getJSONObject(i);
				String pluginRoles = styleJSONObject.getString(CKEditorCommonToolbarConstants.ROLES);

				if (Validator.isNotNull(pluginRoles) && hasUserRole(userRoleNames, pluginRoles)) {
					linkBar.put(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME);
					break;
				}
			}
		} else {
			linkBar.put(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME);
		}
	}

}
