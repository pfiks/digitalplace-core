package com.placecube.digitalplace.userprofile.service;

import java.util.Set;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

public interface UserProfileFieldsService {

	UserProfileFieldsConfiguration getCompanyUserProfileFieldsConfiguration(long companyId) throws ConfigurationException;

	Set<ExpandoField> getConfiguredExpandoFields(long companyId, String[] configuredFields);

	Set<ExpandoField> getConfiguredExpandoFields(long companyId, UserProfileFieldsConfiguration configuration);

}
