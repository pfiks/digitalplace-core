<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %>

<div id="main-content">
	<liferay-util:dynamic-include key="/html/portal/verify_email_address.jsp#pre" />
	
	<liferay-util:buffer var="html">
		<liferay-util:include page="/html/portal/verify_email_address.portal.jsp" servletContext="<%= application %>" />
	</liferay-util:buffer>
	
	<%= html %>
</div>

<aui:script>
	if (window.top.location.href != window.location.href) {
		Liferay.Util.getWindow().hide();
		window.top.location.reload();
	}
</aui:script>
