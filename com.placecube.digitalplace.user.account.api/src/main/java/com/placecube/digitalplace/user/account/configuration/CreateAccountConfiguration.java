package com.placecube.digitalplace.user.account.configuration;

import aQute.bnd.annotation.ProviderType;

@ProviderType
public interface CreateAccountConfiguration {

	boolean addressOutOfLocalArea();

	String createAccountURL();

	String emailAddressPageLocation();

	boolean emailAddressRequired();

	String emailAddressVerificationURL();

	boolean generatePasswordWhenLoggedInUserCreateAccount();

	String registrationRefererGroupIdUserExpandoFieldName();

	boolean requireCaptchaBeforeEmailAddressEntry();

	String resetPasswordArticleId();

	boolean resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount();
}