package com.placecube.digitalplace.search.internal.task;

import com.liferay.portal.kernel.search.SearchException;

public interface SharedSearchTask<T> {

	T execute() throws SearchException;

}