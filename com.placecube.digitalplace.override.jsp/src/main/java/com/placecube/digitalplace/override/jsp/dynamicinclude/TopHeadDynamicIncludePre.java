package com.placecube.digitalplace.override.jsp.dynamicinclude;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.servlet.taglib.BaseDynamicInclude;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.override.jsp.configuration.CompanyStyleConfiguration;

@Component(immediate = true, service = DynamicInclude.class)
public class TopHeadDynamicIncludePre extends BaseDynamicInclude {

	private static final String CUSTOM_CSS = "customCompanyCSS";
	private static final String CUSTOM_FAVICON_URL = "customCompanyFaviconURL";

	private static final Log LOG = LogFactoryUtil.getLog(TopHeadDynamicIncludePre.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String key) throws IOException {

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		if (Validator.isNotNull(themeDisplay)) {

			String themeFavicon = themeDisplay.getThemeSetting("favicon-url");

			if (Validator.isNotNull(themeFavicon)) {
				httpServletRequest.setAttribute(CUSTOM_FAVICON_URL, themeFavicon);

			} else {
				setStylingFromConfiguration(httpServletRequest, themeDisplay);
			}
		}
	}

	@Override
	public void register(DynamicIncludeRegistry dynamicIncludeRegistry) {
		dynamicIncludeRegistry.register("/html/common/themes/top_head.jsp#pre");
	}

	private void setStylingFromConfiguration(HttpServletRequest httpServletRequest, ThemeDisplay themeDisplay) {
		try {
			CompanyStyleConfiguration companyConfiguration = configurationProvider.getCompanyConfiguration(CompanyStyleConfiguration.class, themeDisplay.getCompanyId());
			httpServletRequest.setAttribute(CUSTOM_FAVICON_URL, companyConfiguration.companyFaviconURL());
			httpServletRequest.setAttribute(CUSTOM_CSS, companyConfiguration.companyCSS());
		} catch (Exception e) {
			LOG.warn(e);
		}
	}

}
