package com.placecube.digitalplace.pageimportadmin.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.pageimportadmin.constants.PortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true", "com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false", "com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=200", "com.liferay.portlet.use-default-template=true", "javax.portlet.expiration-cache=0", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.name=" + PortletKeys.PAGE_IMPORT_ADMIN, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user", "javax.portlet.supports.mime-type=text/html" }, service = Portlet.class)
public class PageImportPortlet extends MVCPortlet {

	private static final String GLOBAL_VOCABULARY = " (Global)";

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Group group = themeDisplay.getScopeGroup();

			if (group.isStaged() && !group.isStagingGroup()) {
				renderRequest.setAttribute("disableImportStaging", Boolean.TRUE);
			} else {
				List<AssetVocabulary> allVocabularies = new ArrayList<>();
				List<AssetVocabulary> siteVocabularies = assetVocabularyLocalService.getGroupVocabularies(group.getGroupId());

				siteVocabularies.stream().forEach(allVocabularies::add);

				long globalGroupId = themeDisplay.getCompany().getGroupId();
				List<AssetVocabulary> globalVocabularies = assetVocabularyLocalService.getGroupVocabularies(globalGroupId);

				globalVocabularies.stream().forEach(vocabulary -> {
					vocabulary.setName(vocabulary.getName() + GLOBAL_VOCABULARY);
					allVocabularies.add(vocabulary);
				});

				sortVocabularies(allVocabularies);

				renderRequest.setAttribute("vocabularies", allVocabularies);
				renderRequest.setAttribute("rowImportModels", renderRequest.getAttribute("rowImportModels"));
			}

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}

	}

	private List<AssetVocabulary> sortVocabularies(List<AssetVocabulary> vocabularies) {
		Collections.sort(vocabularies, (o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
		return vocabularies;
	}

}
