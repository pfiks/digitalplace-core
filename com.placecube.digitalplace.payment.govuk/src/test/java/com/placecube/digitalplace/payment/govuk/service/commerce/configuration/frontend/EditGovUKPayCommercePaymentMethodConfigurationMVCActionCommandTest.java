package com.placecube.digitalplace.payment.govuk.service.commerce.configuration.frontend;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.commerce.product.model.CommerceChannel;
import com.liferay.commerce.product.service.CommerceChannelService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.settings.FallbackKeysSettingsUtil;
import com.liferay.portal.kernel.settings.GroupServiceSettingsLocator;
import com.liferay.portal.kernel.settings.ModifiableSettings;
import com.liferay.portal.kernel.settings.Settings;
import com.liferay.portal.kernel.settings.SettingsException;
import com.liferay.portal.kernel.settings.SettingsLocator;
import com.liferay.portal.kernel.settings.SettingsLocatorHelperUtil;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, SettingsLocatorHelperUtil.class, FallbackKeysSettingsUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil", "com.liferay.portal.kernel.settings.SettingsLocatorHelperUtil",
		"com.liferay.portal.kernel.settings.FallbackKeysSettingsUtil" })
public class EditGovUKPayCommercePaymentMethodConfigurationMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private EditGovUKPayCommercePaymentMethodConfigurationMVCActionCommand editGovUKPayCommercePaymentMethodConfigurationMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CommerceChannel mockCommerceChannel;

	@Mock
	private CommerceChannelService mockCommerceChannelService;

	@Mock
	private ModifiableSettings mockModifiableSettings;

	@Mock
	private Settings mockSettings;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class, SettingsLocatorHelperUtil.class, FallbackKeysSettingsUtil.class);
	}

	@Test(expected = PortalException.class)
	public void doProcessAction_WhenExceptionGettingCommerceChannel_ThenThrowException() throws Exception {
		String cmd = Constants.UPDATE;
		long commerceChannelId = 123;

		when(ParamUtil.getString(mockActionRequest, Constants.CMD)).thenReturn(cmd);
		when(ParamUtil.getLong(mockActionRequest, "commerceChannelId")).thenReturn(commerceChannelId);
		when(mockCommerceChannelService.getCommerceChannel(commerceChannelId)).thenThrow(PortalException.class);

		editGovUKPayCommercePaymentMethodConfigurationMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(FallbackKeysSettingsUtil.class, never());
		FallbackKeysSettingsUtil.getSettings(any(SettingsLocator.class));
	}

	@Test(expected = SettingsException.class)
	public void doProcessAction_WhenExceptionGettingSettings_ThenThrowException() throws Exception {
		String cmd = Constants.UPDATE;
		long commerceChannelId = 123;

		when(ParamUtil.getString(mockActionRequest, Constants.CMD)).thenReturn(cmd);
		when(ParamUtil.getLong(mockActionRequest, "commerceChannelId")).thenReturn(commerceChannelId);
		when(mockCommerceChannelService.getCommerceChannel(commerceChannelId)).thenReturn(mockCommerceChannel);
		when(FallbackKeysSettingsUtil.getSettings(any(GroupServiceSettingsLocator.class))).thenThrow(new SettingsException());

		editGovUKPayCommercePaymentMethodConfigurationMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenSaveSettings() throws Exception {
		String cmd = Constants.UPDATE;
		long commerceChannelId = 123;
		String apiKeyName = "keyName";

		when(ParamUtil.getString(mockActionRequest, Constants.CMD)).thenReturn(cmd);
		when(ParamUtil.getLong(mockActionRequest, "commerceChannelId")).thenReturn(commerceChannelId);
		when(mockCommerceChannelService.getCommerceChannel(commerceChannelId)).thenReturn(mockCommerceChannel);
		when(FallbackKeysSettingsUtil.getSettings(any(GroupServiceSettingsLocator.class))).thenReturn(mockSettings);
		when(mockSettings.getModifiableSettings()).thenReturn(mockModifiableSettings);
		when(ParamUtil.getString(mockActionRequest, "settings--apiKeyName--")).thenReturn(apiKeyName);

		editGovUKPayCommercePaymentMethodConfigurationMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockModifiableSettings);
		inOrder.verify(mockModifiableSettings, times(1)).setValue("keyName", apiKeyName);
		inOrder.verify(mockModifiableSettings, times(1)).store();
	}

	@Test
	public void doProcessAction_WhenNoErrorAndCommandIsNotUpdate_ThenDoNothing() throws Exception {
		String cmd = Constants.ACTION;

		when(ParamUtil.getString(mockActionRequest, Constants.CMD)).thenReturn(cmd);

		editGovUKPayCommercePaymentMethodConfigurationMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(FallbackKeysSettingsUtil.class, never());
		FallbackKeysSettingsUtil.getSettings(any(SettingsLocator.class));
	}

}
