package com.placecube.digitalplace.appointment.booking.digitalplaceconnector;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.sql.dsl.DSLFunctionFactoryUtil;
import com.liferay.petra.sql.dsl.DSLQueryFactoryUtil;
import com.liferay.petra.sql.dsl.expression.Predicate;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.placecube.digitalplace.appointment.booking.helper.AppointmentModelBuilder;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.model.AppointmentEntryTable;
import com.placecube.digitalplace.appointment.model.AppointmentSlot;
import com.placecube.digitalplace.appointment.model.AppointmentSlotTable;
import com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService;
import com.placecube.digitalplace.appointment.service.AppointmentSlotLocalService;

@Component(immediate = true, service = DigitalPlaceAppointmentBookingConnectorService.class)
public class DigitalPlaceAppointmentBookingConnectorService {

	@Reference
	private AppointmentEntryLocalService appointmentEntryLocalService;

	@Reference
	private AppointmentModelBuilder appointmentModelBuilder;

	@Reference
	private AppointmentSlotLocalService appointmentSlotLocalService;

	@Reference
	private DigitalPlaceAppointmentConnectorQueryUtil digitalPlaceAppointmentConnectorQueryUtil;

	public List<AppointmentBookingEntry> convertEntries(List<AppointmentEntry> appointmentEntries) {
		List<AppointmentBookingEntry> results = new LinkedList<>();
		for (AppointmentEntry appointmentEntry : appointmentEntries) {
			results.add(appointmentModelBuilder.getAppointmentBookingEntry(appointmentEntry));
		}
		return results;
	}

	public List<AppointmentBookingSlot> convertSlots(List<AppointmentSlot> appointmentSlots, AppointmentContext appointmentContext) {
		List<AppointmentBookingSlot> results = new ArrayList<>();
		Date filterFromDate = appointmentContext.getStartDate().isPresent() ? appointmentContext.getStartDate().get() : null;
		Date filterToDate = appointmentContext.getEndDate().isPresent() ? appointmentContext.getEndDate().get() : null;

		for (AppointmentSlot appointmentSlot : appointmentSlots) {
			results.addAll(appointmentSlotLocalService.getAppointmentBookingSlots(appointmentSlot, filterFromDate, filterToDate));
		}

		return results;
	}

	public List<AppointmentEntry> getAppointmentEntriesById(long companyId, String serviceId, String appointmentId) {
		DSLQuery dlsQuery = DSLQueryFactoryUtil.select().from(AppointmentEntryTable.INSTANCE)//
				.where(digitalPlaceAppointmentConnectorQueryUtil.getBasePredicateForAppointmentEntry(companyId, serviceId)//
						.and(DSLFunctionFactoryUtil.castText(AppointmentEntryTable.INSTANCE.appointmentId).eq(appointmentId)))//
				.orderBy(AppointmentEntryTable.INSTANCE.appointmentStartDate.ascending());

		return appointmentEntryLocalService.dslQuery(dlsQuery);
	}

	public List<AppointmentEntry> getAppointmentEntriesWithMatchingDates(long companyId, String serviceId, Date startDate, Date endDate) {
		DSLQuery dlsQuery = DSLQueryFactoryUtil.select().from(AppointmentEntryTable.INSTANCE)//
				.where(digitalPlaceAppointmentConnectorQueryUtil.getBasePredicateForAppointmentEntry(companyId, serviceId)//
						.and(AppointmentEntryTable.INSTANCE.appointmentStartDate.eq(startDate))//
						.and(AppointmentEntryTable.INSTANCE.appointmentEndDate.eq(endDate)))//
				.orderBy(AppointmentEntryTable.INSTANCE.appointmentStartDate.ascending());
		return appointmentEntryLocalService.dslQuery(dlsQuery);
	}

	public List<AppointmentSlot> getAppointmentSlots(long companyId, String slotType, String serviceId, AppointmentContext appointmentContext) {
		Predicate predicate = AppointmentSlotTable.INSTANCE.companyId.eq(companyId)//
				.and(AppointmentSlotTable.INSTANCE.slotType.eq(slotType))//
				.and(AppointmentSlotTable.INSTANCE.serviceId.eq(serviceId));

		if (appointmentContext.getStartDate().isPresent()) {
			predicate = predicate.and(AppointmentSlotTable.INSTANCE.endDate.gte(appointmentContext.getStartDate().get()));
		}
		if (appointmentContext.getEndDate().isPresent()) {
			predicate = predicate.and(AppointmentSlotTable.INSTANCE.startDate.lte(appointmentContext.getEndDate().get()));
		}

		DSLQuery dslQuery = DSLQueryFactoryUtil.select(AppointmentSlotTable.INSTANCE).from(AppointmentSlotTable.INSTANCE).where(predicate);
		return appointmentSlotLocalService.dslQuery(dslQuery);
	}

	public List<AppointmentBookingSlot> getSlotsAvailable(List<AppointmentBookingSlot> exclusions, List<AppointmentBookingSlot> configurations) {
		List<AppointmentBookingSlot> result = new LinkedList<>();

		for (AppointmentBookingSlot config : configurations) {
			boolean isExcluded = exclusions.stream().anyMatch(exclusion -> digitalPlaceAppointmentConnectorQueryUtil.doesSlotOverlap(exclusion, config));
			if (!isExcluded) {
				result.add(config);
			}
		}

		return result;
	}

	public List<AppointmentEntry> searchAppointmentEntries(long companyId, String serviceId, AppointmentContext appointmentContext) {
		Predicate predicate = digitalPlaceAppointmentConnectorQueryUtil.getBasePredicateForAppointmentEntry(companyId, serviceId);

		Optional<Date> startDate = appointmentContext.getStartDate();
		if (startDate.isPresent()) {
			if (appointmentContext.isExactDateMatch()) {
				predicate = predicate.and(AppointmentEntryTable.INSTANCE.appointmentStartDate.eq(startDate.get()));
			} else {
				predicate = predicate.and(AppointmentEntryTable.INSTANCE.appointmentStartDate.gte(startDate.get()));
			}
		}

		Optional<Date> endDate = appointmentContext.getEndDate();
		if (endDate.isPresent()) {
			if (appointmentContext.isExactDateMatch()) {
				predicate = predicate.and(AppointmentEntryTable.INSTANCE.appointmentEndDate.eq(endDate.get()));
			} else {
				predicate = predicate.and(AppointmentEntryTable.INSTANCE.appointmentEndDate.lte(endDate.get()));
			}
		}

		if (appointmentContext.getClassNameId() > 0) {
			predicate = predicate.and(AppointmentEntryTable.INSTANCE.entryClassNameId.eq(appointmentContext.getClassNameId()));
		}

		if (appointmentContext.getClassPK() > 0) {
			predicate = predicate.and(AppointmentEntryTable.INSTANCE.entryClassPK.eq(appointmentContext.getClassPK()));
		}

		DSLQuery dlsQuery = DSLQueryFactoryUtil.select().from(AppointmentEntryTable.INSTANCE).where(predicate).orderBy(AppointmentEntryTable.INSTANCE.appointmentStartDate.ascending());
		return appointmentEntryLocalService.dslQuery(dlsQuery);
	}

}
