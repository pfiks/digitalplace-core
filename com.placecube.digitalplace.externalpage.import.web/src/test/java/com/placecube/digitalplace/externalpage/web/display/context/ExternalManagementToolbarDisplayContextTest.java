package com.placecube.digitalplace.externalpage.web.display.context;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import com.liferay.frontend.taglib.clay.servlet.taglib.util.CreationMenu;
import com.liferay.frontend.taglib.clay.servlet.taglib.util.DropdownItem;
import com.liferay.frontend.taglib.clay.servlet.taglib.util.DropdownItemList;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.url.builder.PortletURLBuilder;
import com.liferay.portal.kernel.portlet.url.builder.PortletURLBuilder.PortletURLStep;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.externalpage.web.constants.ViewConstants;

import java.util.List;
import java.util.Map;

import javax.portlet.PortletURL;
import javax.portlet.RenderURL;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LanguageUtil.class, PropsUtil.class, PortletURLUtil.class, RequestBackedPortletURLFactoryUtil.class, PortletURLBuilder.class })
public class ExternalManagementToolbarDisplayContextTest {

	private static final String CURRENT_URL = "/current-url";

	private ExternalManagementToolbarDisplayContext externalManagementToolbarDisplayContext;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LiferayPortletRequest mockLiferayPortletRequest;

	@Mock
	private LiferayPortletResponse mockLiferayPortletResponse;

	@Mock
	private PortletURL mockCurrentURL;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private PortletURLStep mockPortletURLStep;

	@Mock
	private RenderURL mockRenderURL;

	@Mock
	private RequestBackedPortletURLFactory mockRequestBackedPortletURLFactory;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activeSetup() {
		mockStatic(LanguageUtil.class, PropsUtil.class, PortletURLUtil.class, RequestBackedPortletURLFactoryUtil.class, PortletURLBuilder.class);

		when(PortletURLUtil.getCurrent(mockLiferayPortletRequest, mockLiferayPortletResponse)).thenReturn(mockCurrentURL);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		externalManagementToolbarDisplayContext = new ExternalManagementToolbarDisplayContext(mockLiferayPortletRequest, mockLiferayPortletResponse, mockHttpServletRequest);
	}

	@Test
	public void getSearchContainerId_WhenNoErrors_ThenReturnsSearchContainerId() {
		String result = externalManagementToolbarDisplayContext.getSearchContainerId();

		assertThat(result, equalTo(ViewConstants.SEARCH_CONTAINER_ID));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getActionItemsDropdownItems_WhenNoErrors_ThenReturnsListWithDeleteEntriesDropdownItem() {
		final String deleteLabel = "delete";

		when(LanguageUtil.get(mockHttpServletRequest, "delete")).thenReturn(deleteLabel);

		DropdownItemList result = (DropdownItemList) externalManagementToolbarDisplayContext.getActionItemsDropdownItems();

		assertThat(result.size(), equalTo(1));

		DropdownItem dropdownItem = result.get(0);

		Map<String, Object> data = (Map<String, Object>) dropdownItem.get("data");

		assertThat(data.get("action"), equalTo(ViewConstants.DELETE_ENTRIES_ACTION));
		assertThat(dropdownItem.get("icon"), equalTo("trash"));
		assertThat(dropdownItem.get("label"), equalTo(deleteLabel));
		assertThat(dropdownItem.get("quickAction"), equalTo(true));
	}

	@Test
	public void getCreationMenu_WhenNoErrors_thenReturnsCreationMenuWithImportPagesDropdownItem() {
		final String renderUrl = "/render";
		final String importPagesLabel = "import pages";

		when(LanguageUtil.get(mockHttpServletRequest, "import-pages")).thenReturn(importPagesLabel);
		when(mockLiferayPortletResponse.createRenderURL()).thenReturn(mockRenderURL);
		when(mockRenderURL.toString()).thenReturn(renderUrl);
		when(mockCurrentURL.toString()).thenReturn(CURRENT_URL);

		CreationMenu result = externalManagementToolbarDisplayContext.getCreationMenu();

		List<DropdownItem> primaryDropdownItems = (List<DropdownItem>) result.get("primaryItems");

		assertThat(primaryDropdownItems.size(), equalTo(1));

		DropdownItem dropdownItem = primaryDropdownItems.get(0);

		assertThat(dropdownItem.get("href"), equalTo(renderUrl));
		assertThat(dropdownItem.get("label"), equalTo(importPagesLabel));

		verify(mockRenderURL, times(1)).setParameter("redirect", CURRENT_URL);
		verify(mockRenderURL, times(1)).setParameter("mvcRenderCommandName", MVCCommandKeys.EXTERNAL_PAGE_ADD);
	}

	@Test
	public void getAdditionalProps_WhenNoErrors_ThenReturnsManagementToolbarAdditionalProperties() {
		final String deleteEntriesUrlString = "/delete-entries";

		PortletURLStep mockPortletURLStepWithAction = mock(PortletURLStep.class);
		PortletURLStep mockPortletURLStepWithRedirect = mock(PortletURLStep.class);

		when(mockThemeDisplay.getURLCurrent()).thenReturn(CURRENT_URL);

		when(RequestBackedPortletURLFactoryUtil.create(mockHttpServletRequest)).thenReturn(mockRequestBackedPortletURLFactory);
		when(mockRequestBackedPortletURLFactory.createActionURL(PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET)).thenReturn(mockPortletURL);
		when(PortletURLBuilder.create(mockPortletURL)).thenReturn(mockPortletURLStep);
		when(mockPortletURLStep.setActionName(MVCCommandKeys.EXTERNAL_PAGE_DELETE)).thenReturn(mockPortletURLStepWithAction);
		when(mockPortletURLStepWithAction.setRedirect(CURRENT_URL)).thenReturn(mockPortletURLStepWithRedirect);
		when(mockPortletURLStepWithRedirect.buildString()).thenReturn(deleteEntriesUrlString);

		Map<String, Object> result = externalManagementToolbarDisplayContext.getAdditionalProps();

		assertThat(result.entrySet().size(), equalTo(4));

		assertThat(result.get("deleteEntriesURL"), equalTo(deleteEntriesUrlString));
		assertThat(result.get("searchContainerName"), equalTo(ViewConstants.SEARCH_CONTAINER_ID));
		assertThat(result.get("entriesIdsParameterName"), equalTo(PortletRequestKeys.ROW_IDS));
		assertThat(result.get("deleteEntriesActionName"), equalTo(ViewConstants.DELETE_ENTRIES_ACTION));
	}
}
