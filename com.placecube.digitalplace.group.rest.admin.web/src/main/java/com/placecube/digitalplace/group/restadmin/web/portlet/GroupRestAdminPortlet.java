package com.placecube.digitalplace.group.restadmin.web.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.ViewKeys;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, property = { "com.liferay.portlet.css-class-wrapper=portlet-group-rest-admin", "com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.render-weight=80", "com.liferay.portlet.instanceable=false", "javax.portlet.name=" + PortletKeys.GROUP_REST_ADMIN, "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=" + ViewKeys.VIEW_GROUP_REST_ADMIN_JSP, "javax.portlet.resource-bundle=content.Language", "javax.portlet.supports.mime-type=text/html",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class GroupRestAdminPortlet extends MVCPortlet {

	@Reference
	private GroupApiKeyLocalService groupApiKeyLocalService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long scopeGroupId = themeDisplay.getScopeGroupId();
		long companyId = themeDisplay.getCompanyId();

		renderRequest.setAttribute(PortletRequestKeys.APPLICATION_KEY, groupApiKeyLocalService.getGroupApplicationKey(companyId, scopeGroupId));
		renderRequest.setAttribute(PortletRequestKeys.APPLICATION_SECRET, groupApiKeyLocalService.getGroupApplicationSecret(companyId, scopeGroupId));

		super.render(renderRequest, renderResponse);
	}

}