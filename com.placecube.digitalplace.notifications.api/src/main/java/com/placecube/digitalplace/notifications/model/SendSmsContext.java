package com.placecube.digitalplace.notifications.model;

import java.util.Map;

public class SendSmsContext {

	private final String templateId;
	private final String phoneNumber;
	private final Map<String, Object> personalisation;
	private final String reference;
	private final String smsSenderId;
	private final String apiKeyName;

	public SendSmsContext(String templateId, String phoneNumber, Map<String, Object> personalisation, String reference,
			String smsSenderId) {
		this(templateId, phoneNumber, personalisation, reference, smsSenderId, null);
	}

	public SendSmsContext(String templateId, String phoneNumber, Map<String, Object> personalisation, String reference,
			String smsSenderId, String apiKeyName) {
		this.templateId = templateId;
		this.phoneNumber = phoneNumber;
		this.personalisation = personalisation;
		this.reference = reference;
		this.smsSenderId = smsSenderId;
		this.apiKeyName = apiKeyName;
	}

	public String getTemplateId() {
		return templateId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public Map<String, Object> getPersonalisation() {
		return personalisation;
	}

	public String getReference() {
		return reference;
	}

	public String getSmsSenderId() {
		return smsSenderId;
	}

	public String getApiKeyName() {
		return apiKeyName;
	}
}
