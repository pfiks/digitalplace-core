package com.placecube.digitalplace.address.taglib.servlet.taglib.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.address.taglib.servlet.taglib.context.ServletContextUtil;

public class PostcodeLookupTag extends IncludeTag {

	private static final String PAGE = "/postcode_lookup/page.jsp";

	private String addressLine1;

	private String addressLine2;

	private String addressLine3;

	private String addressLine4;

	private String city;

	private boolean displaySelected = false;

	private boolean fallbackToNationalLookup = false;

	private String fullAddress;

	private String postcode;

	private boolean required = true;

	private String selectedCallback;

	private String unselectedCallback;

	private String uprn;

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setDisplaySelected(boolean displaySelected) {
		this.displaySelected = displaySelected;
	}

	public void setFallbackToNationalLookup(boolean fallbackToNationalLookup) {
		this.fallbackToNationalLookup = fallbackToNationalLookup;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public void setSelectedCallback(String selectedCallback) {
		this.selectedCallback = selectedCallback;
	}

	public void setUnselectedCallback(String unselectedCallback) {
		this.unselectedCallback = unselectedCallback;
	}

	public void setUprn(String uprn) {
		this.uprn = uprn;
	}

	@Override
	protected String getPage() {
		return PAGE;
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {

		request.setAttribute("fallbackToNationalLookup", fallbackToNationalLookup);
		request.setAttribute("fullAddress", fullAddress);
		request.setAttribute("postcode", HtmlUtil.escape(postcode));
		request.setAttribute("required", required);
		request.setAttribute("uprn", uprn);
		request.setAttribute("addressLine1", addressLine1);
		request.setAttribute("addressLine2", addressLine2);
		request.setAttribute("addressLine3", addressLine3);
		request.setAttribute("addressLine4", addressLine4);
		request.setAttribute("city", city);
		request.setAttribute("displaySelected", displaySelected);
		request.setAttribute("selectedCallback", selectedCallback);
		request.setAttribute("unselectedCallback", unselectedCallback);
	}

}
