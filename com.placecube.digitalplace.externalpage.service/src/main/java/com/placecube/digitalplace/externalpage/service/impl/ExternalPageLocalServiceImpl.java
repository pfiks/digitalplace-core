/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.externalpage.service.impl;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.base.ExternalPageLocalServiceBaseImpl;

/**
 * The implementation of the external page local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.externalpage.service.ExternalPageLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ExternalPageLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage", service = AopService.class)
public class ExternalPageLocalServiceImpl extends ExternalPageLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.externalpage.service.
	 * ExternalPageLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.externalpage.service.
	 * ExternalPageLocalServiceUtil</code>.
	 */

	@Override
	public ExternalPage addOrUpdateExternalPage(long companyId, long groupId, long userId, String url, String title, String content, boolean indexContent) throws PortalException {
		if (indexContent) {
			return addOrUpdateAndReindexExternalPage(companyId, groupId, userId, url, title, content);
		}
		return addOrUpdateExternalPageWithoutReindexing(companyId, groupId, userId, url, title, content);
	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public void deleteExternalPageByPrimaryKey(long externalPageId) throws PortalException {
		externalPageLocalService.deleteExternalPage(externalPageId);

		assetEntryLocalService.deleteEntry(ExternalPage.class.getName(), externalPageId);
	}

	@Override
	public Optional<ExternalPage> getExternalPageImport(long externalPageImportId) {
		return Optional.ofNullable(externalPageLocalService.fetchExternalPage(externalPageImportId));
	}

	@Override
	public List<ExternalPage> getExternalPages(long groupId, int start, int end) {
		return externalPagePersistence.findByGroupId(groupId, start, end);
	}

	@Override
	public int getExternalPagesCount(long groupId) {
		return externalPagePersistence.countByGroupId(groupId);
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public ExternalPage publishExternalPage(ExternalPage externalPage, boolean status) {
		externalPage.setStatus(status);
		return externalPageLocalService.updateExternalPage(externalPage);
	}

	@Override
	public void removeExternalPagesByGroupId(long groupId) {
		externalPagePersistence.removeByGroupId(groupId);
	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public ExternalPage unPublishExternalPage(ExternalPage externalPage, boolean status) {
		externalPage.setStatus(status);
		return externalPageLocalService.updateExternalPage(externalPage);
	}

	@Indexable(type = IndexableType.REINDEX)
	private ExternalPage addOrUpdateAndReindexExternalPage(long companyId, long groupId, long userId, String url, String title, String content) throws PortalException {
		return saveOrUpdateExternalPage(companyId, groupId, userId, url, title, content, true);
	}

	private ExternalPage addOrUpdateExternalPageWithoutReindexing(long companyId, long groupId, long userId, String url, String title, String content) throws PortalException {
		return saveOrUpdateExternalPage(companyId, groupId, userId, url, title, content, false);
	}

	private ExternalPage getOrCreatePage(long companyId, long groupId, String url, String className) {
		ExternalPage externalPage = externalPagePersistence.fetchByGroupIdUrl_First(groupId, url, null);

		if (Validator.isNull(externalPage)) {
			long externalPageId = counterLocalService.increment(className, 1);
			externalPage = externalPageLocalService.createExternalPage(externalPageId);
			externalPage.setCompanyId(companyId);
			externalPage.setGroupId(groupId);
		}

		return externalPage;
	}

	private ExternalPage saveOrUpdateExternalPage(long companyId, long groupId, long userId, String url, String title, String content, boolean indexContent) throws PortalException {
		if (Validator.isNull(title) || Validator.isNull(url)) {
			throw new PortalException("Missing required fields");
		}
		if (url.length() > 200) {
			throw new PortalException("URL too long");
		}

		String className = ExternalPage.class.getName();

		ExternalPage externalPage = getOrCreatePage(companyId, groupId, url, className);

		String titleToUse = StringUtil.shorten(title, 100);
		externalPage.setUserId(userId);
		externalPage.setTitle(titleToUse);
		externalPage.setUrl(url);
		externalPage.setContent(content);
		externalPage.setStatus(indexContent);

		ExternalPage externalPageUpdated = externalPageLocalService.updateExternalPage(externalPage);

		assetEntryLocalService.updateEntry(userId, groupId, externalPageUpdated.getCreateDate(), externalPageUpdated.getModifiedDate(), className, externalPageUpdated.getExternalPageId(),
				externalPageUpdated.getUuid(), 0l, new long[0], new String[0], true, true, null, null, null, null, ContentTypes.TEXT_HTML, titleToUse, content, titleToUse, url, null, 0, 0, 0.0);

		return externalPageUpdated;
	}
}