package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SessionErrors.class })
public class TwoFactorAuthenticationUserResetMVCRenderCommandTest extends PowerMockito {

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private TwoFactorRequestUtils mockTwoFactorRequestUtils;

	@Mock
	private TwoFactorService mockTwoFactorService;

	@Mock
	private User mockUser;

	@InjectMocks
	private TwoFactorAuthenticationUserResetMVCRenderCommand twoFactorAuthenticationUserResetMVCRenderCommand;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, SessionErrors.class, ParamUtil.class);
	}

	@Test
	public void render_WhenUserFound_ThenReturnsTheUserResetPage() throws PortletException {
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.of(mockUser));

		String result = twoFactorAuthenticationUserResetMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/two-factor-authentication/reset.jsp"));
	}

	@Test
	public void render_WhenUserFound_ThenSetsTheRequestAttributes() throws PortletException {
		long userId = 123;
		String secretKey = "secretKeyValue";
		String qrURL = "qrURLValue";
		String redirect = "redirectValue";
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.of(mockUser));
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockTwoFactorService.getSecretKey(mockRenderRequest)).thenReturn(secretKey);
		when(mockTwoFactorService.getQRBarCodeURL(mockUser, secretKey)).thenReturn(qrURL);
		when(ParamUtil.getString(mockRenderRequest, "redirect")).thenReturn(redirect);

		twoFactorAuthenticationUserResetMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("twoFactorUrlQR", qrURL);
		verify(mockRenderRequest, times(1)).setAttribute("twoFactorSecretKey", secretKey);
		verify(mockRenderRequest, times(1)).setAttribute("redirect", redirect);
	}

	@Test
	public void render_WhenUserNotFound_ThenReturnsTheErrorPage() throws PortletException {
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.empty());

		String result = twoFactorAuthenticationUserResetMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/two-factor-authentication/error.jsp"));
	}

	@Test
	public void render_WhenUserNotFound_ThenSetsTheSessionError() throws PortletException {
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.empty());

		twoFactorAuthenticationUserResetMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "sessionExpired");
	}

}
