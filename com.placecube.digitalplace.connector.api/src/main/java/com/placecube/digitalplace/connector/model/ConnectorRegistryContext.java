package com.placecube.digitalplace.connector.model;

public class ConnectorRegistryContext {

	private String connectorClassName;

	private String connectorType;

	public String getConnectorClassName() {
		return connectorClassName;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorClassName(String connectorClassName) {
		this.connectorClassName = connectorClassName;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}

}
