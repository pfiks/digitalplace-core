package com.placecube.digitalplace.group.rest.application.filters;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

import java.time.Instant;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.pfiks.common.time.DateFormatter;
import com.placecube.digitalplace.group.rest.application.GroupRestApplication;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;
import com.placecube.digitalplace.group.rest.application.service.RequestFiltersService;

/**
 * Filter to validate the timestamp of the request If the timestamp is more than
 * 15 minutes old, the response status is set to SC_BAD_REQUEST (400)
 */
@Provider
@PreMatching
public class DateCheckerFilter implements ContainerRequestFilter {

	private static final long MAX_INTERVAL = MILLISECONDS.convert(15, MINUTES);

	private DateFormatter dateFormatter;
	private RequestFiltersService responseFiltersService;

	public DateCheckerFilter(GroupRestApplication restApplication) {
		dateFormatter = restApplication.getDateFormatter();
		responseFiltersService = restApplication.getRequestFiltersService();
	}

	@Override
	public void filter(ContainerRequestContext requestContext) {
		String dateHeader = responseFiltersService.getDateHeader(requestContext);
		boolean invalidTimestamp = isInvalidTimestamp(dateHeader);
		if (invalidTimestamp) {
			throw new GroupRestSecurityException(ErrorCodes.INVALID_TIMESTAMP, Response.Status.BAD_REQUEST);
		}
	}

	private boolean isInvalidTimestamp(String dateHeader) {
		Instant requestTime = dateFormatter.getInstantFromLongFormatString(dateHeader);
		Instant now = dateFormatter.getNow();
		long timeDifference = now.toEpochMilli() - requestTime.toEpochMilli();
		return timeDifference >= MAX_INTERVAL;
	}

}
