package com.placecube.digitalplace.appointment.mock.connector.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.appointment.mock.connector.configuration.MockConnectorCompanyConfiguration", localization = "content/Language", name = "appointment-mock-connector")
public interface MockConnectorCompanyConfiguration {

	@Meta.AD(required = false, name = "enabled")
	boolean enabled();

}
