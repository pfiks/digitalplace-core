package com.placecube.digitalplace.payment.govuk.service.internal;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.payment.govuk.service.commerce.GovUKPayConstants;
import com.placecube.digitalplace.payment.model.AgreementStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;

@Component(immediate = true, service = GovUKPaymentOrderFactory.class)
public class GovUKPaymentOrderFactory {

	public static final String ACCEPT = "*/*";
	public static final String ACCEPT_ENCODING = "gzip, deflate, br";
	public static final String ACCEPT_HEADER = "application/json";
	public static final String PAYMENT_ID = "payment_id";
	private static final Log LOG = LogFactoryUtil.getLog(GovUKPaymentOrderFactory.class);

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	public JSONObject createGovUKAgreementRequest(PaymentContext paymentContext) {
		JSONObject requestBody = jsonFactory.createJSONObject();

		if (Validator.isNotNull(paymentContext.getAccountId())) {
			requestBody.put("user_identifier", paymentContext.getAccountId());
		}
		requestBody.put("description", paymentContext.getSalesDescription());
		requestBody.put("reference", paymentContext.getItemReference());

		return requestBody;
	}

	public JSONObject createGovUKFirstRecurringPaymentRequest(PaymentContext paymentContext, String agreementId) {
		JSONObject requestBody = jsonFactory.createJSONObject();
		requestBody.put("return_url", paymentContext.getReturnURL());
		requestBody.put("amount", convertToPence(paymentContext.getAmount()));
		requestBody.put("reference", GetterUtil.getString(paymentContext.getItemReference()));
		requestBody.put("description", GetterUtil.getString(paymentContext.getSalesDescription()));
		requestBody.put("email", paymentContext.getEmailAddress());
		requestBody.put("set_up_agreement", agreementId);
		requestBody.put("prefilled_cardholder_details", getCardHolderDetailsJson(paymentContext, getBillingAddressJson(paymentContext)));
		return requestBody;
	}

	public JSONObject createGovUKFollowingRecurringPaymentRequest(PaymentContext paymentContext, String agreementId) {
		JSONObject requestBody = jsonFactory.createJSONObject();
		requestBody.put("amount", convertToPence(paymentContext.getAmount()));
		requestBody.put("reference", GetterUtil.getString(paymentContext.getItemReference()));
		requestBody.put("description", GetterUtil.getString(paymentContext.getSalesDescription()));
		requestBody.put("authorisation_mode", "agreement");
		requestBody.put("agreement_id", agreementId);
		return requestBody;
	}

	public JSONObject createGovUKPaymentRequest(PaymentContext paymentContext) {
		JSONObject requestBody = jsonFactory.createJSONObject();
		requestBody.put("return_url", paymentContext.getReturnURL());
		requestBody.put("amount", convertToPence(paymentContext.getAmount()));
		requestBody.put("reference", GetterUtil.getString(paymentContext.getItemReference()));
		requestBody.put("description", GetterUtil.getString(paymentContext.getSalesDescription()));
		requestBody.put("email", paymentContext.getEmailAddress());
		requestBody.put("prefilled_cardholder_details", getCardHolderDetailsJson(paymentContext, getBillingAddressJson(paymentContext)));
		return requestBody;
	}

	public CloseableHttpResponse executeRequest(HttpUriRequest httpUriRequest, CloseableHttpClient closeableHttpClient) throws IOException {
		return closeableHttpClient.execute(httpUriRequest);
	}

	public CloseableHttpClient getHttpClient() {
		return HttpClients.createDefault();
	}

	public HttpGet getHttpGetMethod(String url, String authKey) {
		HttpGet httpGet = new HttpGet(url);
		addHeader(httpGet, authKey);
		return httpGet;
	}

	public HttpPost getHttpPostMethod(String url, String authKey, JSONObject requestObject) {
		HttpPost httpPost = new HttpPost(url);
		addHeader(httpPost, authKey);
		httpPost.setEntity(new StringEntity(requestObject.toJSONString(), StandardCharsets.UTF_8));
		return httpPost;
	}

	public String getPaymentIdFromSession(HttpServletRequest httpServletRequest) {
		try {
			httpServletRequest = portal.getOriginalServletRequest(httpServletRequest);
			HttpSession httpSession = httpServletRequest.getSession();
			return String.valueOf(httpSession.getAttribute(PAYMENT_ID));
		} catch (Exception e) {
			LOG.error("Unable to get payment id from session", e);
			return StringPool.BLANK;
		}
	}

	public JSONObject getPaymentURLJSON(CloseableHttpResponse closeableHttpResponse) {
		try {
			String result = EntityUtils.toString(closeableHttpResponse.getEntity());
			JSONObject resultJson = jsonFactory.createJSONObject(result);
			LOG.debug("Payment service response:" + resultJson.toJSONString());

			JSONObject linkJsonObject = resultJson.getJSONObject("_links");

			JSONObject nextURLJsonObject = linkJsonObject.getJSONObject("next_url");

			nextURLJsonObject.put(PAYMENT_ID, resultJson.getString(PAYMENT_ID));
			return nextURLJsonObject;
		} catch (Exception e) {
			LOG.error("Unable to get payment url json", e);
			return null;
		}
	}

	public AgreementStatus getResponseAgreementStatus(CloseableHttpResponse closeableHttpResponse) {
		AgreementStatus agreementStatus = new AgreementStatus();

		try {
			String result = EntityUtils.toString(closeableHttpResponse.getEntity());
			JSONObject resultJson = jsonFactory.createJSONObject(result);
			LOG.debug("Payment service response:" + resultJson.toJSONString());

			String agreementId = resultJson.getString(GovUKPayConstants.AGREEMENT_ID_JSON_KEY);
			String status = resultJson.getString(GovUKPayConstants.STATUS_JSON_KEY);

			agreementStatus.setAgreementId(agreementId);
			agreementStatus.setStatus(status);
		} catch (Exception e) {
			LOG.error("Unable to get response agreement status", e);
		}

		return agreementStatus;
	}

	public JSONObject getResponseRecurringPaymentStatusJSON(CloseableHttpResponse closeableHttpResponse) {
		try {
			String result = EntityUtils.toString(closeableHttpResponse.getEntity());

			JSONObject resultJson = jsonFactory.createJSONObject(result);

			JSONObject status = resultJson.getJSONObject("state");
			status.put(PAYMENT_ID, resultJson.getString(PAYMENT_ID));

			return status;
		} catch (Exception e) {
			LOG.error("Unable to get response recurring payment status json", e);
			return jsonFactory.createJSONObject();
		}
	}

	public String getResponseStatus(CloseableHttpResponse closeableHttpResponse) {
		try {
			String result = EntityUtils.toString(closeableHttpResponse.getEntity());

			JSONObject resultJson = jsonFactory.createJSONObject(result);

			JSONObject paymentStateJson = resultJson.getJSONObject("state");

			return paymentStateJson.getString("status");
		} catch (Exception e) {
			LOG.error(e);
			return StringPool.BLANK;
		}
	}

	public int getResponseStatusCode(CloseableHttpResponse closeableHttpResponse) {
		try {
			StatusLine statusLine = closeableHttpResponse.getStatusLine();
			return statusLine.getStatusCode();
		} catch (Exception e) {
			LOG.error("Unable to get response status code", e);
			return 500;
		}
	}

	public JSONObject getResponseStatusJSON(CloseableHttpResponse closeableHttpResponse) {
		try {
			String result = EntityUtils.toString(closeableHttpResponse.getEntity());

			JSONObject resultJson = jsonFactory.createJSONObject(result);

			return resultJson.getJSONObject("state");
		} catch (Exception e) {
			LOG.error("Unable to get response status json", e);
			return jsonFactory.createJSONObject();
		}
	}

	public void quietlyCloseResources(CloseableHttpClient client, CloseableHttpResponse response) {
		try {
			response.close();
		} catch (Exception e) {
			LOG.debug(e);
		}
		try {
			client.close();
		} catch (Exception e) {
			LOG.debug(e);
		}
	}

	private void addHeader(HttpUriRequest httpUriRequest, String authKey) {
		httpUriRequest.addHeader("Content-Type", ACCEPT_HEADER);
		httpUriRequest.addHeader("Accept", ACCEPT);
		httpUriRequest.addHeader("Accept-Encoding", ACCEPT_ENCODING);
		httpUriRequest.addHeader("Authorization", "Bearer " + authKey);

		LOG.debug("Setting Authorization header with key:" + authKey);
	}

	private int convertToPence(BigDecimal amount) {
		return amount.multiply(new BigDecimal(100)).intValue();
	}

	private JSONObject getBillingAddressJson(PaymentContext paymentContext) {
		JSONObject billingAddress = jsonFactory.createJSONObject();
		AddressContext addressContext = paymentContext.getAddress();
		billingAddress.put("line1", addressContext.getAddressLine1());
		billingAddress.put("line2", addressContext.getAddressLine2() + StringPool.SPACE + addressContext.getAddressLine3());
		billingAddress.put("postcode", addressContext.getPostcode());
		billingAddress.put("city", addressContext.getCity());
		billingAddress.put("country", addressContext.getUPRN());
		return billingAddress;
	}

	private JSONObject getCardHolderDetailsJson(PaymentContext paymentContext, JSONObject billingAddress) {
		JSONObject prefilledCardholderDetails = jsonFactory.createJSONObject();

		prefilledCardholderDetails.put("cardholder_name", paymentContext.getFirstName() + StringPool.SPACE + paymentContext.getLastName());

		prefilledCardholderDetails.put("billing_address", billingAddress);
		return prefilledCardholderDetails;
	}

}
