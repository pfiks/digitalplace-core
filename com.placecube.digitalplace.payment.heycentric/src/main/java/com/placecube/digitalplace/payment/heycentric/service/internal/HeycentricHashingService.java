package com.placecube.digitalplace.payment.heycentric.service.internal;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.heycentric.configuration.HeycentricPaymentCompanyConfiguration;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;

@Component(immediate = true, service = HeycentricHashingService.class)
public class HeycentricHashingService {

	public String base64EncodeString(String stringToEncode) {
		return Base64.getEncoder().encodeToString(stringToEncode.getBytes(StandardCharsets.UTF_8));
	}

	public String getHeycentricRawHashString(HeycentricPaymentCompanyConfiguration heycentricPaymentCompanyConfiguration, String amount) {
		String hashFormat = "%s=%s&%s=%s&%s=%s&%s=%s%s";
		return String.format(hashFormat, HeycentricPaymentConstants.HEADER_PARAMETER_CLIENT, heycentricPaymentCompanyConfiguration.client(), HeycentricPaymentConstants.HEADER_PARAMETER_ENTITY,
				heycentricPaymentCompanyConfiguration.entity(), HeycentricPaymentConstants.LINE_PARAMETER_PMTTYP, heycentricPaymentCompanyConfiguration.pmtTyp(),
				HeycentricPaymentConstants.LINE_PARAMETER_AM, amount, heycentricPaymentCompanyConfiguration.privateKey());
	}

	public String rawHashToMd5Hex(String rawHashString) throws NoSuchAlgorithmException {
		if (Validator.isNotNull(rawHashString)) {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			byte[] md5Bytes = messageDigest.digest(rawHashString.getBytes(StandardCharsets.UTF_8));

			return bytesToHexString(md5Bytes);
		}

		return StringPool.BLANK;
	}

	public String rawHashToSHA512(String rawHashString) throws NoSuchAlgorithmException {
		if (Validator.isNotNull(rawHashString)) {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			byte[] sha512Bytes = digest.digest(rawHashString.getBytes());
			return bytesToHexString(sha512Bytes);
		}

		return StringPool.BLANK;
	}

	private String bytesToHexString(byte[] bytes) {
		StringBuilder hexStringBuilder = new StringBuilder();
		for (byte b : bytes) {
			String hex = Integer.toHexString(0xff & b);
			if (hex.length() == 1) {
				hexStringBuilder.append('0');
			}
			hexStringBuilder.append(hex);
		}

		return hexStringBuilder.toString();
	}
}
