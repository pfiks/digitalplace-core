package com.placecube.digitalplace.payment.heycentric.service.arp;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.heycentric.configuration.HeycentricPaymentCompanyConfiguration;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.heycentric.service.internal.arp.HeycentricARPLineService;
import com.placecube.digitalplace.payment.heycentric.service.internal.arp.HeycentricARPResponseParser;
import com.placecube.digitalplace.payment.heycentric.service.internal.arp.HeycentricARPSecurityUtil;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;

@RunWith(MockitoJUnitRunner.class)
public class HeycentricARPServiceTest {

	@InjectMocks
	private HeycentricARPService heycentricARPService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private HeycentricARPLineService mockHeycentricARPLineService;

	@Mock
	private HeycentricARPSecurityUtil mockHeycentricARPSecurityUtil;

	@Mock
	private HeycentricARPResponseParser mockHeycentricARPResponseParser;

	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	private HeycentricPaymentCompanyConfiguration mockHeycentricPaymentCompanyConfiguration;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private AgentReferredPaymentContext mockAgentReferredPaymentContext;

	@Mock
	private User mockUser;

	@Mock
	private PaymentResponse mockPaymentResponse;

	private MockedStatic<ParamUtil> mockParamUtil;

	private MockedStatic<PropsUtil> mockPropsUtil;

	private static final boolean IMMEDIATE_CALLBACK = false;
	private static final long COMPANY_ID = 5445L;
	private static final long USER_ID = 12345L;
	private static final String AGENT_EMAIL = "agent@email.com";
	private static final String AGENT_ID = "agentId";
	private static final String AGENT_PHONE_NUMBER = "0723453222";
	private static final String CALLER_EMAIL = "caller@email.com";
	private static final String CALLER_PHONE_NUMBER = "0843534323";
	private static final String EXTERNAL_REFERENCE_CODE = "externalReferenceCode";
	private static final String EXTERNAL_SYSTEM_NAME = "externalSystemName";
	private static final String LOG_URL = "logUrl";
	private static final String PAYLOAD_DIGEST = "payloadDigest";
	private static final String PAY_LOG = "payLog";
	private static final String RECEIPT_EMAIL = "receiptEmail";
	private static final String RETURN_URL = "returnUrl";
	private static final String SECRET = "secret";
	private static final String SID = "sid";

	@Before
	public void activateSetUp() {
		mockPropsUtil = mockStatic(PropsUtil.class);
		mockParamUtil = mockStatic(ParamUtil.class);
	}

	@Test
	public void processAgentReferredPaymentResponse_WhenPayloadIsValid_ThenReturnsPaymentResponse() throws PortalException, GeneralSecurityException {
		mockParamUtil.when(() -> ParamUtil.getString(mockRequest, HeycentricPaymentConstants.JSON_PAYLOAD_PAYLOG)).thenReturn(PAY_LOG);
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockHeycentricPaymentCompanyConfiguration);
		when(mockHeycentricPaymentCompanyConfiguration.arpSecret()).thenReturn(SECRET);

		when(mockJsonFactory.createJSONObject(PAY_LOG)).thenReturn(mockJsonObject);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_DIGEST)).thenReturn(PAYLOAD_DIGEST);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_SID)).thenReturn(SID);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_AGENT_ID)).thenReturn(AGENT_ID);

		when(mockHeycentricARPSecurityUtil.isValidData(EXTERNAL_REFERENCE_CODE, SID, AGENT_ID, SECRET, PAYLOAD_DIGEST)).thenReturn(true);
		when(mockHeycentricARPResponseParser.parsePaymentResponse(mockJsonObject)).thenReturn(mockPaymentResponse);

		PaymentResponse result = heycentricARPService.processAgentReferredPaymentResponse(mockRequest, EXTERNAL_REFERENCE_CODE);

		assertThat(result, sameInstance(mockPaymentResponse));
	}

	@Test
	public void processAgentReferredPaymentResponse_WhenPaylogContainsInvalidData_ThenReturnsErrorPaymentResponse() throws PortalException, GeneralSecurityException {
		mockParamUtil.when(() -> ParamUtil.getString(mockRequest, HeycentricPaymentConstants.JSON_PAYLOAD_PAYLOG)).thenReturn(PAY_LOG);
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockHeycentricPaymentCompanyConfiguration);
		when(mockHeycentricPaymentCompanyConfiguration.arpSecret()).thenReturn(SECRET);

		when(mockJsonFactory.createJSONObject(PAY_LOG)).thenReturn(mockJsonObject);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_DIGEST)).thenReturn(PAYLOAD_DIGEST);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_SID)).thenReturn(SID);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_AGENT_ID)).thenReturn(AGENT_ID);

		when(mockHeycentricARPSecurityUtil.isValidData(EXTERNAL_REFERENCE_CODE, SID, AGENT_ID, SECRET, PAYLOAD_DIGEST)).thenReturn(false);

		PaymentResponse result = heycentricARPService.processAgentReferredPaymentResponse(mockRequest, EXTERNAL_REFERENCE_CODE);

		assertThat(result.getStatus().getValue(), equalTo(PaymentStatus.paymentError().getValue()));
	}

	@Test
	public void processAgentReferredPaymentResponse_WhenThereIsNoPaylogInPayload_ThenReturnsErrorPaymentResponse() throws PortalException, GeneralSecurityException {
		mockParamUtil.when(() -> ParamUtil.getString(mockRequest, HeycentricPaymentConstants.JSON_PAYLOAD_PAYLOG)).thenReturn(StringPool.BLANK);

		PaymentResponse result = heycentricARPService.processAgentReferredPaymentResponse(mockRequest, EXTERNAL_REFERENCE_CODE);

		assertThat(result.getStatus().getValue(), equalTo(PaymentStatus.paymentError().getValue()));
	}

	@Test(expected = ConfigurationException.class)
	public void processAgentReferredPaymentResponse_WhenConfigurationRetrievalFails_ThenThrowsConfigurationException() throws PortalException, GeneralSecurityException {
		mockParamUtil.when(() -> ParamUtil.getString(mockRequest, HeycentricPaymentConstants.JSON_PAYLOAD_PAYLOG)).thenReturn(PAY_LOG);
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		heycentricARPService.processAgentReferredPaymentResponse(mockRequest, EXTERNAL_REFERENCE_CODE);
	}

	@Test(expected = NoSuchAlgorithmException.class)
	public void processAgentReferredPaymentResponse_WhenPaylogDataValidityCheckFails_ThenThrowsNoSuchAlgorithmException() throws PortalException, GeneralSecurityException {
		mockParamUtil.when(() -> ParamUtil.getString(mockRequest, HeycentricPaymentConstants.JSON_PAYLOAD_PAYLOG)).thenReturn(PAY_LOG);
		when(mockPortal.getCompanyId(mockRequest)).thenReturn(COMPANY_ID);
		when(mockConfigurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockHeycentricPaymentCompanyConfiguration);
		when(mockHeycentricPaymentCompanyConfiguration.arpSecret()).thenReturn(SECRET);

		when(mockJsonFactory.createJSONObject(PAY_LOG)).thenReturn(mockJsonObject);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_DIGEST)).thenReturn(PAYLOAD_DIGEST);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_SID)).thenReturn(SID);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_AGENT_ID)).thenReturn(AGENT_ID);

		when(mockHeycentricARPSecurityUtil.isValidData(EXTERNAL_REFERENCE_CODE, SID, AGENT_ID, SECRET, PAYLOAD_DIGEST)).thenThrow(new NoSuchAlgorithmException());

		heycentricARPService.processAgentReferredPaymentResponse(mockRequest, EXTERNAL_REFERENCE_CODE);
	}

	@Test
	public void getAgentReferredPaymentRequestPayload_WhenNoErrors_ThenReturnsJSONPayloadWithPaymentContextValues() throws GeneralSecurityException {
		when(mockHeycentricPaymentCompanyConfiguration.arpExternalSystemName()).thenReturn(EXTERNAL_SYSTEM_NAME);
		when(mockHeycentricPaymentCompanyConfiguration.arpGroupReceiptEmail()).thenReturn(RECEIPT_EMAIL);
		when(mockHeycentricPaymentCompanyConfiguration.arpSecret()).thenReturn(SECRET);
		when(mockHeycentricARPSecurityUtil.generateDigest(RETURN_URL, USER_ID, SECRET)).thenReturn(PAYLOAD_DIGEST);
		when(mockHeycentricARPLineService.agentReferredPaymentLinesToJSON(mockAgentReferredPaymentContext)).thenReturn(mockJSONArray);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockAgentReferredPaymentContext.isImmediateCallBack()).thenReturn(IMMEDIATE_CALLBACK);
		when(mockAgentReferredPaymentContext.getSid()).thenReturn(SID);
		when(mockAgentReferredPaymentContext.getCallerPhoneNumber()).thenReturn(CALLER_PHONE_NUMBER);
		when(mockAgentReferredPaymentContext.getCallerEmail()).thenReturn(CALLER_EMAIL);
		when(mockAgentReferredPaymentContext.getReturnUrl()).thenReturn(RETURN_URL);
		when(mockAgentReferredPaymentContext.getLogUrl()).thenReturn(LOG_URL);
		when(mockAgentReferredPaymentContext.getUser()).thenReturn(mockUser);
		when(mockAgentReferredPaymentContext.getAgentPhoneNumber()).thenReturn(AGENT_PHONE_NUMBER);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getEmailAddress()).thenReturn(AGENT_EMAIL);

		JSONObject result = heycentricARPService.getAgentReferredPaymentRequestPayload(mockAgentReferredPaymentContext, mockHeycentricPaymentCompanyConfiguration);

		assertThat(result, sameInstance(mockJsonObject));

		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_BU, EXTERNAL_SYSTEM_NAME);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_SID, SID);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_PCN, CALLER_PHONE_NUMBER);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_EMAIL, CALLER_EMAIL);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_CCEMAIL, RECEIPT_EMAIL);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_ICB, Boolean.toString(IMMEDIATE_CALLBACK));
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_RURL, RETURN_URL);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_LOGURL, LOG_URL);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_DIGEST, PAYLOAD_DIGEST);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_AGENT_ID, USER_ID);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_AGENT_EMAIL, AGENT_EMAIL);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_ACN, AGENT_PHONE_NUMBER);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEMS, mockJSONArray);
	}

	@After
	public void teardown() {
		mockPropsUtil.close();
		mockParamUtil.close();
	}
}