package com.placecube.digitalplace.session.timeout.web.service;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.util.PropsValues;

@Component(immediate = true, service = SessionTimeoutPropertyService.class)
public class SessionTimeoutPropertyService {

	public int getSystemConfiguredTimeout() {
		return PropsValues.SESSION_TIMEOUT;
	}

	public int getValidTimeout(int configuredTimeout) {
		return (configuredTimeout >= 2 && configuredTimeout <= 720) ? configuredTimeout : 0;
	}

}
