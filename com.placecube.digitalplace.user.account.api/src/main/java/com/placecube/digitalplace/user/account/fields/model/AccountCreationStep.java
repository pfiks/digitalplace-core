package com.placecube.digitalplace.user.account.fields.model;

public enum AccountCreationStep {

	ADDITIONAL_DETAILS,
	ADDRESS_DETAILS,
	PERSONAL_DETAILS;

}
