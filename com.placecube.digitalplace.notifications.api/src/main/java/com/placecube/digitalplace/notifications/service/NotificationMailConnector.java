package com.placecube.digitalplace.notifications.service;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendMailContext;

public interface NotificationMailConnector {

	boolean enabled(long companyId);

	void sendMail(SendMailContext sendMailContext, ServiceContext serviceContext) throws NotificationsException, ConfigurationException;
}
