package com.placecube.digitalplace.user.settings.web.twofactor.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.placecube.digitalplace.user.settings.web.service.UserSettingsService;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class TwoFactorAuthenticationLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(TwoFactorAuthenticationLifecycleListener.class);

	@Reference
	private UserSettingsService userSettingsService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		LOG.debug("Initialising User settings 2FA for companyId: " + company.getCompanyId());

		userSettingsService.configureUserSettingsWebContentArticle(company, getClass(), "dependencies/webcontent.xml", TwoFactorAuthenticationKeys.WEBCONTENT_ARTICLE_ID, "2FA");

	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Implementation not required
	}

}
