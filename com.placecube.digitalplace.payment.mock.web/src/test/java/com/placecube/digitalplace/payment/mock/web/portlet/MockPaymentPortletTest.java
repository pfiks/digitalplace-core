package com.placecube.digitalplace.payment.mock.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;
import java.util.Calendar;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.payment.mock.web.constants.ParamKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, MVCPortlet.class })
public class MockPaymentPortletTest extends PowerMockito {

	@InjectMocks
	private MockPaymentPortlet mockPaymentPortlet;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void render_WhenNoErrors_ThenAmountAttributeIsSetOnRenderRequest() throws Exception {
		String amount = "25.00";
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);
		when(ParamUtil.getString(mockRenderRequest, ParamKeys.AMOUNT)).thenReturn(amount);

		mockPaymentPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(ParamKeys.AMOUNT, amount);
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void render_WhenNoErrors_ThenReturnUrlIsSetOnRenderRequest() throws Exception {
		String returnUrl = "http://return-url";
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);
		when(ParamUtil.getString(mockRenderRequest, ParamKeys.RETURN_URL)).thenReturn(returnUrl);

		mockPaymentPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(ParamKeys.RETURN_URL, returnUrl);
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void render_WhenNoErrors_ThenCurrentYearIsSetAsStartYearOnRenderRequest() throws Exception {
		int startYear = Calendar.getInstance().get(Calendar.YEAR);
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);

		mockPaymentPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("startYear", startYear);
	}

}
