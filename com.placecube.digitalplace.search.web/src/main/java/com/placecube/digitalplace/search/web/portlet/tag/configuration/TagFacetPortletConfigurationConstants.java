package com.placecube.digitalplace.search.web.portlet.tag.configuration;

public final class TagFacetPortletConfigurationConstants {

	public static final String FILTER_COLOUR = "filterColour";

	public static final String MAX_MORE_TAGS_TO_SHOW = "maxShowMoreTagsToShow";

	public static final String MAX_TAGS_TO_SHOW = "maxTagsToShow";

	public static final String TAGS_QUERY_ANDED = "tagsQueryAnded";

	private TagFacetPortletConfigurationConstants() {
	}

}
