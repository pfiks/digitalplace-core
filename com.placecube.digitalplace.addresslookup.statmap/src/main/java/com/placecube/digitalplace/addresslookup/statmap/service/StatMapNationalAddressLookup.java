package com.placecube.digitalplace.addresslookup.statmap.service;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookup;
import com.placecube.digitalplace.addresslookup.statmap.configuration.StatMapNationalLookupCompanyConfiguration;
import com.placecube.digitalplace.addresslookup.statmap.constants.StatMapConstants;
import com.placecube.digitalplace.addresslookup.statmap.service.utils.StatMapAddressLookupService;

@Component(immediate = true, service = AddressLookup.class)
public class StatMapNationalAddressLookup implements AddressLookup {

	private static final Log LOG = LogFactoryUtil.getLog(StatMapNationalAddressLookup.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private StatMapAddressLookupService statMapAddressLookupService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return getConfiguration(companyId).enabled();
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Set<AddressContext> findByKeyword(long companyId, String keyword, String[] paramsAndValues) {
		throw new UnsupportedOperationException("Unsupported method");
	}

	@Override
	public Set<AddressContext> findByPostcode(long companyId, String postcode, String[] paramsAndValues) {
		return getAddressContexts(companyId, StatMapConstants.PARAM_SEARCH_TERM, postcode);
	}

	@Override
	public Optional<AddressContext> findByUprn(long companyId, String uprn, String[] paramsAndValues) {
		Set<AddressContext> addressContexts = getAddressContexts(companyId, StatMapConstants.PARAM_SEARCH_UPRN, uprn);
		if (addressContexts.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(addressContexts.iterator().next());
	}

	@Override
	public int getWeight(long companyId) {
		try {
			return getConfiguration(companyId).weight();
		} catch (ConfigurationException e) {
			LOG.error(e);
			return 0;
		}
	}

	@Override
	public boolean national(long companyId) {
		return true;
	}

	private Set<AddressContext> getAddressContexts(long companyId, String searchParameterName, String searchValue) {
		try {
			StatMapNationalLookupCompanyConfiguration configuration = getConfiguration(companyId);
			return statMapAddressLookupService.getAddressContexts(configuration.baseSearchEndpoint(), configuration.baseRecordEndpoint(), configuration.token(), searchParameterName, searchValue);
		} catch (Exception e) {
			LOG.error(e);
			return Collections.emptySet();
		}

	}

	private StatMapNationalLookupCompanyConfiguration getConfiguration(long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(StatMapNationalLookupCompanyConfiguration.class, companyId);
	}
}
