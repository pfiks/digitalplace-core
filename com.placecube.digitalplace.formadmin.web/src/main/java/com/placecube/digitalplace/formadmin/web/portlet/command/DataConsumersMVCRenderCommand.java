package com.placecube.digitalplace.formadmin.web.portlet.command;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceService;
import com.liferay.dynamic.data.mapping.util.comparator.DDMFormInstanceNameComparator;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.service.DataConsumerLocalService;
import com.placecube.digitalplace.formadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.service.SearchContainerService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.FORMS_MANAGER, "mvc.command.name=" + MVCCommandKeys.DATA_CONSUMERS }, service = MVCRenderCommand.class)
public class DataConsumersMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private DataConsumerLocalService dataConsumerLocalService;

	@Reference
	private DDMFormInstanceService ddmFormInstanceService;

	@Reference
	private SearchContainerService searchContainerService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId = themeDisplay.getCompanyId();
		long scopeGroupId = themeDisplay.getScopeGroupId();

		SearchContainer<DDMFormInstance> formSearchContainer = getFormInstanceSearchContainer(renderRequest, renderResponse, companyId, scopeGroupId);

		List<DataConsumer> dataConsumers = dataConsumerLocalService.getDataConsumersByCompanyIdAndGroupId(companyId, scopeGroupId);

		renderRequest.setAttribute(PortletRequestKeys.FORM_SEARCH_CONTAINER, formSearchContainer);
		renderRequest.setAttribute(PortletRequestKeys.DATA_CONSUMERS, dataConsumers);

		return "/data-consumer.jsp";
	}

	private SearchContainer<DDMFormInstance> getFormInstanceSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse, long companyId, long scopeGroupId) {

		SearchContainer<DDMFormInstance> searchContainer = searchContainerService.getSearchContainer(renderRequest, renderResponse, "no-entries-were-found");
		searchContainer.setId("dataConsumerSearchContainer");

		List<DDMFormInstance> results = ddmFormInstanceService.search(companyId, scopeGroupId, "", -1, -1, new DDMFormInstanceNameComparator(true));

		searchContainer.setResultsAndTotal(results);

		return searchContainer;
	}

}
