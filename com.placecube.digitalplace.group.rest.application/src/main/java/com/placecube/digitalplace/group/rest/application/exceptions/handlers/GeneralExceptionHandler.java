package com.placecube.digitalplace.group.rest.application.exceptions.handlers;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.placecube.digitalplace.group.rest.application.BasicAuthGroupRestApplication;
import com.placecube.digitalplace.group.rest.application.GroupRestApplication;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.model.ModelBuilder;
import com.placecube.digitalplace.group.rest.application.model.RestErrorMessage;

@Provider
public class GeneralExceptionHandler implements ExceptionMapper<Exception> {

	private ModelBuilder modelBuilder;

	public GeneralExceptionHandler(BasicAuthGroupRestApplication restApplication) {
		modelBuilder = restApplication.getModelBuilder();
	}

	public GeneralExceptionHandler(GroupRestApplication restApplication) {
		modelBuilder = restApplication.getModelBuilder();
	}

	public GeneralExceptionHandler() {
	}

	@Override
	public Response toResponse(Exception exception) {
		RestErrorMessage restErrorMessage = modelBuilder.initRestErrorMessage(Status.INTERNAL_SERVER_ERROR.getStatusCode(), ErrorCodes.INTERNAL_ERROR.getCode(), getStacktraceAsString(exception));
		return Response.status(restErrorMessage.getErrorStatus()).entity(restErrorMessage).build();
	}

	private String getStacktraceAsString(Throwable exception) {
		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

}
