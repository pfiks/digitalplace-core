package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.crypto.spec.SecretKeySpec;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = SecretKeySpecService.class)
public class SecretKeySpecService {

	static final String SECRET_KEY_ALGORITHM = "AES";

	static final String SECRET_KEY_PATH = "dependencies/keys/twofactor_skey";

	static final int SECURITY_KEY_UUID_PADDING = 16;

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	public SecretKeySpec getSecretKeySpec(String uuid) throws IOException {
		byte[] binaryKey = modelFactoryBuilder.getBytesFromFile(SECRET_KEY_PATH);
		byte[] uuidPadding = uuid.substring(0, SECURITY_KEY_UUID_PADDING).getBytes();
		// Append the first SECURITY_KEY_UUID_PADDING bytes of the user UUID to
		// the generated binary
		// key to produce a secret key of 32 bytes
		ByteArrayOutputStream outputStream = modelFactoryBuilder.getByteArrayOutputStreamInstance();
		outputStream.write(binaryKey);
		outputStream.write(uuidPadding);
		byte[] secretKey = outputStream.toByteArray();

		if (secretKey.length != 32) {
			throw new IllegalStateException("The overall secret key length must be 32 bytes");
		}

		return modelFactoryBuilder.getSecretKeySpecInstance(secretKey, SECRET_KEY_ALGORITHM);
	}

}
