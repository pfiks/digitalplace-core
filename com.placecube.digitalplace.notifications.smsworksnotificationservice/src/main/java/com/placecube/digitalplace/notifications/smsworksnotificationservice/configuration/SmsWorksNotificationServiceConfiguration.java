package com.placecube.digitalplace.notifications.smsworksnotificationservice.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.notifications.smsworksnotificationservice.configuration.SmsWorksNotificationServiceConfiguration", localization = "content/Language", name = "notifications-smsworks-notifications-service")
public interface SmsWorksNotificationServiceConfiguration {

	@Meta.AD(required = false, deflt = "", name = "auth-token")
	String authToken();

	@Meta.AD(required = false, deflt = "https://api.thesmsworks.co.uk/v1/message/send", name = "endpoint")
	String endpoint();

	@Meta.AD(required = false, deflt = "false", name = "sms-enabled")
	boolean smsEnabled();
}
