package com.placecube.digitalplace.group.rest.application.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;
import java.nio.charset.Charset;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.group.rest.application.constants.RequestHeaders;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ IOUtils.class })
public class RequestFiltersServiceTest extends PowerMockito {

	private RequestFiltersService filterCheckersService;

	@Mock
	private ContainerRequestContext mockContainerRequestContext;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private InputStream mockInputStreamCopy;

	@Mock
	private InputStream mockInputStreamFromContext;

	@Mock
	private UriInfo mockUriInfo;

	@Test
	public void getAcceptHeader_WhenHeaderValueIsNull_ThenReturnsEmptyString() {
		when(mockContainerRequestContext.getHeaderString(RequestHeaders.ACCEPT)).thenReturn(null);

		String result = filterCheckersService.getAcceptHeader(mockContainerRequestContext);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getAcceptHeader_WhenHeaderValueIsPresent_ThenReturnsItsValue() {
		String headerValue = "headerValue";
		when(mockContainerRequestContext.getHeaderString(RequestHeaders.ACCEPT)).thenReturn(headerValue);

		String result = filterCheckersService.getAcceptHeader(mockContainerRequestContext);

		assertThat(result, equalTo(headerValue));
	}

	@Test
	public void getApiKeyHeader_WhenHeaderValueIsNull_ThenReturnsEmptyString() {
		when(mockContainerRequestContext.getHeaderString(RequestHeaders.X_API_KEY)).thenReturn(null);

		String result = filterCheckersService.getApiKeyHeader(mockContainerRequestContext);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getApiKeyHeader_WhenHeaderValueIsPresent_ThenReturnsItsValue() {
		String headerValue = "headerValue";
		when(mockContainerRequestContext.getHeaderString(RequestHeaders.X_API_KEY)).thenReturn(headerValue);

		String result = filterCheckersService.getApiKeyHeader(mockContainerRequestContext);

		assertThat(result, equalTo(headerValue));
	}

	@Test
	public void getBody_WhenBodyHasValue_ThenReturnsTheValueOfTheBody() throws Exception {
		String body = "bodyValue";
		when(mockContainerRequestContext.getEntityStream()).thenReturn(mockInputStreamFromContext);
		when(IOUtils.toString(mockInputStreamFromContext, Charset.forName("UTF-8"))).thenReturn(body);

		String result = filterCheckersService.getBody(mockContainerRequestContext);

		assertThat(result, equalTo(body));
	}

	@Test
	public void getBody_WhenBodyIsNull_ThenReturnsEmptyString() throws Exception {
		String body = null;
		when(mockContainerRequestContext.getEntityStream()).thenReturn(mockInputStreamFromContext);
		when(IOUtils.toString(mockInputStreamFromContext, Charset.forName("UTF-8"))).thenReturn(body);

		String result = filterCheckersService.getBody(mockContainerRequestContext);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getBody_WhenNoError_ThenSetsTheInputStreamAsEntityStream() throws Exception {
		String body = "bodyValue";
		when(mockContainerRequestContext.getEntityStream()).thenReturn(mockInputStreamFromContext);
		when(IOUtils.toString(mockInputStreamFromContext, Charset.forName("UTF-8"))).thenReturn(body);
		when(IOUtils.toInputStream(body, Charset.forName("UTF-8"))).thenReturn(mockInputStreamCopy);

		filterCheckersService.getBody(mockContainerRequestContext);

		verify(mockContainerRequestContext, times(1)).setEntityStream(mockInputStreamCopy);
	}

	@Test
	public void getContextPath_WhenContextPathIsNull_ThenReturnsEmptyString() {
		when(mockHttpServletRequest.getContextPath()).thenReturn(null);

		String result = filterCheckersService.getContextPath(mockHttpServletRequest);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getContextPath_WhenContextPathValueIsPresent_ThenReturnsItsValue() {
		String contextPath = "contextPathValue";
		when(mockHttpServletRequest.getContextPath()).thenReturn(contextPath);

		String result = filterCheckersService.getContextPath(mockHttpServletRequest);

		assertThat(result, equalTo(contextPath));
	}

	@Test
	public void getDateHeader_WhenHeaderIsNull_ThenReturnsEmptyString() {
		when(mockContainerRequestContext.getHeaderString(RequestHeaders.X_DATE)).thenReturn(null);

		String result = filterCheckersService.getDateHeader(mockContainerRequestContext);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getDateHeader_WhenHeaderValueIsPresent_ThenReturnsItsValue() {
		String headerValue = "headerValue";
		when(mockContainerRequestContext.getHeaderString(RequestHeaders.X_DATE)).thenReturn(headerValue);

		String result = filterCheckersService.getDateHeader(mockContainerRequestContext);

		assertThat(result, equalTo(headerValue));
	}

	@Test
	public void getMethod_WhenMethodIsNull_ThenReturnsEmptyString() {
		when(mockContainerRequestContext.getMethod()).thenReturn(null);

		String result = filterCheckersService.getMethod(mockContainerRequestContext);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getMethod_WhenMethodValueIsPresent_ThenReturnsItsValue() {
		String method = "methodValue";
		when(mockContainerRequestContext.getMethod()).thenReturn(method);

		String result = filterCheckersService.getMethod(mockContainerRequestContext);

		assertThat(result, equalTo(method));
	}

	@Test
	public void getPath_WhenPathInfoIsNull_ThenReturnsEmptyString() {
		when(mockContainerRequestContext.getUriInfo()).thenReturn(mockUriInfo);
		when(mockUriInfo.getPath()).thenReturn(null);

		String result = filterCheckersService.getPath(mockContainerRequestContext);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getPath_WhenPathInfoValueIsPresent_ThenReturnsItsValue() {
		String pathInfo = "/pathInfoValue";
		when(mockContainerRequestContext.getUriInfo()).thenReturn(mockUriInfo);
		when(mockUriInfo.getPath()).thenReturn(pathInfo);

		String result = filterCheckersService.getPath(mockContainerRequestContext);

		assertThat(result, equalTo(pathInfo));
	}

	@Test
	public void getPath_WhenPathInfoValueIsPresentAndDoesNotStartWithForwardSlash_ThenReturnsItsValuePrefixedWithForwardSlash() {
		String pathInfo = "pathInfoValue";
		when(mockContainerRequestContext.getUriInfo()).thenReturn(mockUriInfo);
		when(mockUriInfo.getPath()).thenReturn(pathInfo);

		String result = filterCheckersService.getPath(mockContainerRequestContext);

		assertThat(result, equalTo("/" + pathInfo));
	}

	@Test
	public void getSignatureHeader_WhenHeaderIsNull_ThenReturnsEmptyString() {
		when(mockContainerRequestContext.getHeaderString(RequestHeaders.X_SIGNATURE)).thenReturn(null);

		String result = filterCheckersService.getSignatureHeader(mockContainerRequestContext);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getSignatureHeader_WhenHeaderValueIsPresent_ThenReturnsItsValue() {
		String headerValue = "headerValue";
		when(mockContainerRequestContext.getHeaderString(RequestHeaders.X_SIGNATURE)).thenReturn(headerValue);

		String result = filterCheckersService.getSignatureHeader(mockContainerRequestContext);

		assertThat(result, equalTo(headerValue));
	}

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(IOUtils.class);
		filterCheckersService = new RequestFiltersService();
	}

}
