package com.placecube.digitalplace.group.rest.application.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

public class ModelBuilderTest extends PowerMockito {

	@Mock
	private RestListResult mockRestListResult;

	@InjectMocks
	private ModelBuilder modelBuilder;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void getSuccessResponse_WhenNoError_ThenReturnsResponseWithEntity() {
		Response result = modelBuilder.getSuccessResponse(mockRestListResult);

		assertThat(result.getEntity(), sameInstance(mockRestListResult));
	}

	@Test
	public void getSuccessResponse_WhenNoError_ThenReturnsResponseWithStatus200() {
		Response result = modelBuilder.getSuccessResponse(mockRestListResult);

		assertThat(result.getStatus(), equalTo(200));
	}

	@Test
	public void init_WhenNoError_ThenReturnsEntryWithEmptyData() {
		RestListResult result = modelBuilder.initRestListResult();

		assertNotNull(result);
		assertThat(result.getData(), empty());
	}

	@Test
	public void initRestErrorMessage_WhenNoError_ThenReturnsTheConfiguredErrorMessage() {
		String code = "errorCodeValue";
		int status = 5460;
		String stacktrace = "stacktraceValue";

		RestErrorMessage result = modelBuilder.initRestErrorMessage(status, code, stacktrace);

		assertThat(result.getErrorCode(), equalTo(code));
		assertThat(result.getErrorStatus(), equalTo(status));
		assertThat(result.getStackTrace(), equalTo(stacktrace));
	}

}
