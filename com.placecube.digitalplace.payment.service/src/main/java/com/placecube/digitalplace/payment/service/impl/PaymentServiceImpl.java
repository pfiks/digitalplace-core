package com.placecube.digitalplace.payment.service.impl;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.AgreementStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;
import com.placecube.digitalplace.payment.service.PaymentConnector;
import com.placecube.digitalplace.payment.service.PaymentService;

@Component(immediate = true, service = PaymentService.class)
public class PaymentServiceImpl implements PaymentService {

	private Set<PaymentConnector> paymentConnectors = new LinkedHashSet<>();

	@Override
	public boolean cancelAgreement(String agreementId, ServiceContext serviceContext) {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(serviceContext.getCompanyId());

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().cancelAgreement(agreementId, serviceContext);
		}
		return false;
	}

	@Override
	public AgreementStatus createAgreement(PaymentContext paymentContext, ServiceContext serviceContext) {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(serviceContext.getCompanyId());

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().createAgreement(paymentContext, serviceContext);
		}
		return getNoPaymentConnectorAvailableAgreementStatus();
	}

	@Override
	public AgreementStatus getAgreementStatus(String agreementId, ServiceContext serviceContext) {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(serviceContext.getCompanyId());

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().getAgreementStatus(agreementId, serviceContext);
		}
		return getNoPaymentConnectorAvailableAgreementStatus();
	}

	@Override
	public Optional<String> getBackURL(String paymentReference, ServiceContext serviceContext) {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(serviceContext.getCompanyId());

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().getBackURL(paymentReference, serviceContext);
		}
		return Optional.empty();
	}

	@Override
	public Optional<String> getDefaultAdvancedConfiguration(long companyId) {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(companyId);

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().getDefaultAdvancedConfiguration(companyId);
		}
		return Optional.empty();
	}

	@Override
	public PaymentStatus getPaymentStatus(String paymentReference, ServiceContext serviceContext) {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(serviceContext.getCompanyId());

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().getPaymentStatus(paymentReference, serviceContext);
		}
		return getNoPaymentConnectorAvailableStatus();
	}

	@Override
	public PaymentStatus getRecurringPaymentStatus(String paymentId, ServiceContext serviceContext) {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(serviceContext.getCompanyId());

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().getRecurringPaymentStatus(paymentId, serviceContext);
		}
		return getNoPaymentConnectorAvailableStatus();
	}

	@Override
	public PaymentResponse preparePayment(PaymentContext paymentContext, ServiceContext serviceContext) {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(serviceContext.getCompanyId());

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().preparePayment(paymentContext, serviceContext);
		}
		return new PaymentResponse(StringPool.BLANK, getNoPaymentConnectorAvailableStatus());
	}

	@Override
	public PaymentResponse prepareRecurringPayment(PaymentContext paymentContext, ServiceContext serviceContext) {

		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(serviceContext.getCompanyId());

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().prepareRecurringPayment(paymentContext, serviceContext);
		}
		return new PaymentResponse(StringPool.BLANK, getNoPaymentConnectorAvailableStatus());
	}

	@Override
	public PaymentResponse takeRecurringPayment(PaymentContext paymentContext, String agreementId, String idempotencyKey, ServiceContext serviceContext) {
		return null;
	}

	@Override
	public String getAgentReferredPaymentPayload(long companyId, AgentReferredPaymentContext agentReferredPaymentContext) throws PortalException {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(companyId);

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().getAgentReferredPaymentPayload(companyId, agentReferredPaymentContext);
		}
		return StringPool.BLANK;
	}

	@Override
	public PaymentResponse processAgentReferredPaymentResponse(String externalReferenceCode, ServiceContext serviceContext) {
		Optional<PaymentConnector> paymentConnectorOpt = getPaymentConnector(serviceContext.getCompanyId());

		if (paymentConnectorOpt.isPresent()) {
			return paymentConnectorOpt.get().processAgentReferredPaymentResponse(externalReferenceCode, serviceContext);
		}

		return new PaymentResponse(StringPool.BLANK, getNoPaymentConnectorAvailableStatus());
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setPaymentConnector(PaymentConnector paymentConnector) {
		paymentConnectors.add(paymentConnector);
	}

	protected void unsetPaymentConnector(PaymentConnector paymentConnector) {
		paymentConnectors.remove(paymentConnector);
	}

	private AgreementStatus getNoPaymentConnectorAvailableAgreementStatus() {
		return new AgreementStatus("", "NO_PAYMENT_CONNECTOR_AVAILABLE");
	}

	private PaymentStatus getNoPaymentConnectorAvailableStatus() {
		return PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE");
	}

	private Optional<PaymentConnector> getPaymentConnector(long companyId) {
		return paymentConnectors.stream().filter(connector -> connector.enabled(companyId)).findFirst();
	}

}
