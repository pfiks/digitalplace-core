<%@ include file="init.jsp"%>

<c:set var="availableSections" value="${userSettingsDisplayContext.getUserSettingsSections()}" />

<c:if test="${not empty availableSections}">
	<c:set var="selectedSection" value="${userSettingsDisplayContext.getSelectedSection(pageContext.getRequest()) }" />

	<div class="row">
		<div class="col-md-4">

			<div class="user-settings-nav">
				<h3 class="portlet-section-title">
					<liferay-ui:message key="settings"/>
				</h3>
				
				<nav class="navbar navbar-expand-md navbar-expand-lg">
					<button class="navbar-toggler mt-3 mb-3" type="button" data-toggle="collapse" data-target="#<portlet:namespace />navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false">
						<span class="glyphicon glyphicon-menu-hamburger"></span>
					</button>

					<div class="collapse navbar-collapse" id="<portlet:namespace />navbarNavDropdown">
						<ul class="navbar-nav nav-stacked">
							<c:forEach items="${availableSections}" var="availableSection">
								
								<li class="nav-item nav-item-disabled group-section-title">
									<h4>
										<liferay-ui:message key="${availableSection.key.getKey()}"/>
									</h4>
								</li>
								
								<c:forEach items="${availableSection.value}" var="sectionDisplayEntry">
								
									<li class="nav-item ${selectedSection.getId() eq sectionDisplayEntry.getId() ? 'active' : ''}">
										<a class="nav-link" href="${userSettingsDisplayContext.getViewURL(portletURL, liferayPortletResponse, sectionDisplayEntry.getId())}">
											${sectionDisplayEntry.getTitle(locale)} <span class="glyphicon glyphicon-menu-right"/>
										</a>
									</li>
									
								</c:forEach>

							</c:forEach>
						</ul>
					</div>
				</nav>
			</div>
		</div>

		<div class="col-md-8">
			<div class="user-settings-content">
				${selectedSection.render(pageContext.getRequest(), userSettingsDisplayContext.createPipingServletResponse(pageContext))}
			</div>
		</div>
	</div>
</c:if>