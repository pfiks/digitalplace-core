<%@ include file="../init.jsp"%>

<c:set var="hasError" value="${not empty errorMessage}"/>

<div class="govuk-form-group ${hasError ? 'govuk-form-group--error' : ''}" id="${portletNamespace}_field_${fieldName}">
	<label class="govuk-label" for="${portletNamespace}${fieldName}">
		<liferay-ui:message key="${fieldLabel}"/>
		<%@ include file="../fieldOptionalLabel.jspf" %>
	</label>

	<%@ include file="../fieldHintMessage.jspf" %>		
	<%@ include file="../fieldErrorMessage.jspf" %>

	<div class="${inputWrapperCssClass}">
		<input class="govuk-input ${hasError ? 'govuk-input--error' : ''} ${cssClass}" 
			id="${portletNamespace}${fieldName}"
			name="${portletNamespace}${fieldName}"
			type="${type}"
			value="${fieldValue}"
		/>	
	</div>
</div>