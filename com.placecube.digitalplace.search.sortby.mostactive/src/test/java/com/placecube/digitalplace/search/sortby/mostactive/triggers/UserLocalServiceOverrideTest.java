package com.placecube.digitalplace.search.sortby.mostactive.triggers;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.service.UserLocalService;

public class UserLocalServiceOverrideTest extends PowerMockito {

	@Mock
	private Indexer<User> mockIndexer;

	@Mock
	private IndexerRegistry mockIndexerRegistry;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private UserLocalServiceOverride userLocalServiceOverride;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void updateLastLogin_WhenNoError_ThenReindexAndReturnsTheUpdatedUser() throws PortalException {
		String loginIP = "loginIPVal";
		long userId = 123;
		when(mockIndexerRegistry.nullSafeGetIndexer(User.class)).thenReturn(mockIndexer);
		when(mockUserLocalService.updateLastLogin(userId, loginIP)).thenReturn(mockUser);

		User result = userLocalServiceOverride.updateLastLogin(userId, loginIP);

		assertThat(result, sameInstance(mockUser));
		verify(mockIndexer, times(1)).reindex(mockUser);
	}

}
