package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.user.account.web.util.AccountAddressUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, JSONFactoryUtil.class, AccountAddressUtils.class })
public class AddressLookupMVCResourceCommandTest extends PowerMockito {

	@InjectMocks
	private AddressLookupMVCResourceCommand addressLookupMVCResourceCommand = new AddressLookupMVCResourceCommand();

	private final long COMPANY_ID = 1L;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private AddressLookupService mockAddressLookupService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private PrintWriter mockPrintWriter;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	private final String UPRN = "1234";

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, JSONFactoryUtil.class, AccountAddressUtils.class);
	}

	@Test
	public void render_WhenAddressContextIsEmpty_ThenFullAddressIsPutInJSONObject() throws Exception {
		mockBasic(false);

		boolean result = addressLookupMVCResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verify(mockJSONObject, times(1)).put("fullAddress", StringPool.BLANK);
		assertThat(result, equalTo(true));
	}

	@Test
	public void render_WhenAddressContextIsNotEmpty_ThenFullAddressIsPutInJSONObject() throws Exception {
		mockBasic(true);

		boolean result = addressLookupMVCResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verify(mockJSONObject, times(1)).put("fullAddress", "Full Address");
		assertThat(result, equalTo(true));
	}

	@Test
	public void render_WhenErrorGettingWriter_ThenReturnsFalse() throws Exception {
		mockBasic(true);
		String jsonOutput = "{fullAddress:'full address'}";
		when(mockJSONObject.toJSONString()).thenReturn(jsonOutput);
		doThrow(new IOException()).when(mockResourceResponse).getWriter();

		boolean result = addressLookupMVCResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		assertThat(result, equalTo(false));
	}

	@Test
	public void render_WhenNoError_ThenRetunsTrue() throws Exception {
		mockBasic(true);
		String jsonOutput = "{fullAddress:'full address'}";
		when(mockJSONObject.toJSONString()).thenReturn(jsonOutput);

		boolean result = addressLookupMVCResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		assertThat(result, equalTo(true));
	}

	@Test
	public void render_WhenNoErrors_ThenUpdateContextInSession() throws Exception {
		mockBasic(true);
		String jsonOutput = "{fullAddress:'full address'}";
		when(mockJSONObject.toJSONString()).thenReturn(jsonOutput);

		addressLookupMVCResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockResourceRequest);
	}

	@Test
	public void render_WhenNoErrors_ThenWriteJsonObjectToOutputStream() throws Exception {
		mockBasic(true);
		String jsonOutput = "{fullAddress:'full address'}";
		when(mockJSONObject.toJSONString()).thenReturn(jsonOutput);

		addressLookupMVCResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verify(mockPrintWriter, times(1)).print(jsonOutput);
	}

	private void mockBasic(boolean populateAddress) throws Exception {
		when(ParamUtil.getString(mockResourceRequest, RequestKeys.UPRN)).thenReturn(UPRN);
		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject);
		when(mockAccountContextService.getOrCreateContext(mockResourceRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		Optional<AddressContext> addressContextOpt = Optional.of(mockAddressContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockResourceRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(true);
		when(mockAddressLookupService.getByUprn(COMPANY_ID, UPRN, new String[0], true)).thenReturn(addressContextOpt);
		if (populateAddress) {
			when(AccountAddressUtils.getFullAddress(mockAddressContext, "<br>")).thenReturn("Full Address");
		} else {
			when(AccountAddressUtils.getFullAddress(mockAddressContext, "<br>")).thenReturn(StringPool.BLANK);
		}

		when(mockResourceResponse.getWriter()).thenReturn(mockPrintWriter);
	}

}
