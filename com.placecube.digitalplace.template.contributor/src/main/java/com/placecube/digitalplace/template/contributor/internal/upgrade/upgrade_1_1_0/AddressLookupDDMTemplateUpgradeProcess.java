package com.placecube.digitalplace.template.contributor.internal.upgrade.upgrade_1_1_0;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.template.contributor.internal.util.UpgradeHelper;

public class AddressLookupDDMTemplateUpgradeProcess extends UpgradeProcess {

	private static final String OUT_PUT_FORMAT = "CompanyId: %s, GroupId: %s, TemplateId: %s, TemplateKey: %s, TemplateName: %s, ClassName: %s \n";

	private final CompanyLocalService companyLocalService;

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private File file;

	public AddressLookupDDMTemplateUpgradeProcess(CompanyLocalService companyLocalService, DDMTemplateLocalService ddmTemplateLocalService) {
		this.companyLocalService = companyLocalService;
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {

		file = UpgradeHelper.getOutputFile(this);

		if (CompanyThreadLocal.getCompanyId() == 0) {

			for (Company company : companyLocalService.getCompanies()) {
				updateTemplates(company.getCompanyId());
			}

		} else {
			updateTemplates(CompanyThreadLocal.getCompanyId());

		}
	}

	private String formatOutput(DDMTemplate ddmTemplate) {
		return String.format(OUT_PUT_FORMAT, ddmTemplate.getCompanyId(), ddmTemplate.getGroupId(), ddmTemplate.getTemplateId(), ddmTemplate.getTemplateKey(),
				ddmTemplate.getName(ddmTemplate.getDefaultLanguageId()), ddmTemplate.getClassName());
	}

	private void upgradeTemplate(DDMTemplate ddmTemplate) throws IOException {

		String script = ddmTemplate.getScript();

		if (UpgradeHelper.hasAddressLookupServiceComponent(script)) {
			ddmTemplate.setScript(UpgradeHelper.replaceAddressLookupServiceInScript(script));
			ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
			UpgradeHelper.writeOutput(file, formatOutput(ddmTemplate));
		}
	}

	private void updateTemplates(long companyId) throws IOException {

		DynamicQuery dynamicQuery = ddmTemplateLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		List<DDMTemplate> ddmTemplates = ddmTemplateLocalService.dynamicQuery(dynamicQuery);

		for (DDMTemplate ddmTemplate : ddmTemplates) {
			upgradeTemplate(ddmTemplate);
		}
	}

}
