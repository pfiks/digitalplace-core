package com.placecube.digitalplace.ddl.internal.util;

import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateException;
import com.liferay.portal.kernel.template.TemplateManagerUtil;
import com.liferay.portal.kernel.template.TemplateResource;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.ddl.internal.constants.TemplateKeys;

@Component(immediate = true, service = TemplateService.class)
public class TemplateService {

	@Reference
	private TemplateUtil templateUtil;

	public Template getTemplate(long ddmTemplateId, long companyId, long companyGroupId, long scopeGroupId, String script, String langType) throws TemplateException {
		String templateId = templateUtil.getTemplateId(ddmTemplateId, companyId, companyGroupId, scopeGroupId);
		TemplateResource templateResource = templateUtil.getTemplateResourceInstance(templateId, script);
		return TemplateManagerUtil.getTemplate(langType, templateResource, false);
	}

	public void initTemplate(Template template, Map<String, Object> contextObjects, ThemeDisplay themeDisplay, UnsyncStringWriter unsyncStringWriter) throws TemplateException {
		long companyId = themeDisplay.getCompanyId();
		long scopeGroupId = themeDisplay.getScopeGroupId();
		long siteGroupId = themeDisplay.getSiteGroupId();

		template.putAll(contextObjects);

		long classNameId = GetterUtil.getLong(contextObjects.get(TemplateConstants.CLASS_NAME_ID));

		template.put(TemplateKeys.COMPANY, themeDisplay.getCompany());
		template.put(TemplateKeys.COMPANY_ID, companyId);
		template.put(TemplateKeys.DEVICE, themeDisplay.getDevice());

		String templatesPath = templateUtil.getTemplatesPath(companyId, scopeGroupId, classNameId);

		template.put(TemplateKeys.PERMISSION_CHECKER, PermissionThreadLocal.getPermissionChecker());
		template.put(TemplateKeys.RANDOM_NAMESPACE, StringUtil.randomId() + StringPool.UNDERLINE);
		template.put(TemplateKeys.SCOPE_GROUP_ID, scopeGroupId);
		template.put(TemplateKeys.SITE_GROUP_ID, siteGroupId);
		template.put(TemplateKeys.TEMPLATES_PATH, templatesPath);
		template.put(TemplateKeys.GROUP_ID, scopeGroupId);
		template.put(TemplateKeys.JOURNAL_TEMPLATES_PATH, templatesPath);

		template.processTemplate(unsyncStringWriter);
	}

	public void prepareTemplate(ThemeDisplay themeDisplay, Template template) {
		if (themeDisplay == null) {
			return;
		}
		template.prepare(themeDisplay.getRequest());
		template.prepareTaglib(themeDisplay.getRequest(), themeDisplay.getResponse());
	}
}
