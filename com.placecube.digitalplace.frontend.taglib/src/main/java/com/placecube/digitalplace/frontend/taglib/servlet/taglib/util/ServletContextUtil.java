package com.placecube.digitalplace.frontend.taglib.servlet.taglib.util;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = {})
public class ServletContextUtil {

	private static ServletContext dpFrontendServletContext;
	private static ServletContext servletContext;

	public static ServletContext getDpFrontendServletContext() {
		return dpFrontendServletContext;
	}

	public static ServletContext getServletContext() {
		return servletContext;
	}

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.frontend.taglib)", unbind = "-")
	protected void setDpFrontendServletContext(ServletContext servletContext) {
		dpFrontendServletContext = servletContext;
	}

	@Reference(target = "(osgi.web.symbolicname=com.liferay.asset.taglib)", unbind = "-")
	protected void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}