package com.placecube.digitalplace.admin.emailverify.service;

import javax.portlet.ActionRequest;
import javax.portlet.ActionURL;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.placecube.digitalplace.admin.emailverify.constants.EmailVerifyMVCCommandKeys;
import com.placecube.digitalplace.admin.emailverify.constants.EmailVerifyPortletKeys;

@Component(immediate = true, service = EmailVerifyUrlService.class)
public class EmailVerifyUrlService {

	@Reference
	private Portal portal;

	public String getVerifyEmailURL(PortletResponse portletResponse, User user) {
		LiferayPortletResponse liferayPortletResponse = portal.getLiferayPortletResponse(portletResponse);

		ActionURL actionURL = liferayPortletResponse.createActionURL();
		actionURL.setParameter("p_p_id", EmailVerifyPortletKeys.ADMIN_EMAIL_VERIFY);
		actionURL.setParameter(ActionRequest.ACTION_NAME, EmailVerifyMVCCommandKeys.VERIFY);
		actionURL.setParameter("selectedUserId", String.valueOf(user.getUserId()));
		String urlValue = actionURL.toString();
		return urlValue.replace(PortletKeys.USERS_ADMIN, EmailVerifyPortletKeys.ADMIN_EMAIL_VERIFY);
	}

}
