package com.placecube.digitalplace.announcementsbanner.web.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "announcements", scope = ExtendedObjectClassDefinition.Scope.GROUP)
@Meta.OCD(id = "com.placecube.digitalplace.announcementsbanner.web.configuration.AnnouncementsBannerGroupConfiguration", localization = "content/Language", name = "announcementsbanner")

public interface AnnouncementsBannerGroupConfiguration {

	@Meta.AD(required = false, type = Meta.Type.String, deflt = "#content", name = "announcementbanner-content-selector-name", description = "announcementbanner-content-selector-description")
	String getAnnouncementsBannerPortletHTMLSelector();

}
