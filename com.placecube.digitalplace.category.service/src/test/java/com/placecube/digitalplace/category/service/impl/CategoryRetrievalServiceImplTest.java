package com.placecube.digitalplace.category.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.category.property.model.AssetCategoryProperty;
import com.liferay.asset.category.property.service.AssetCategoryPropertyLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;

@RunWith(MockitoJUnitRunner.class)
public class CategoryRetrievalServiceImplTest extends PowerMockito {

	private static final long CATEGORY_ID_1 = 11;

	private static final long CATEGORY_ID_2 = 22;

	private static final long CATEGORY_ID_3 = 33;

	private static final long CATEGORY_ID_4 = 44;

	private static final String CLASS_NAME = Layout.class.getName();

	private static final long CLASS_PK = 4333L;

	private static final String PROPERTY_KEY = "property";

	private static final String PROPERTY_VALUE_1 = "propertyKeyValue1";

	private static final String PROPERTY_VALUE_2 = "propertyKeyValue2";

	private static final long VOCABULARY_ID = 12384;

	@InjectMocks
	private CategoryRetrievalServiceImpl categoryRetrievalServiceImpl;

	@Mock
	private AssetCategory mockAssetCategory1;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private AssetCategory mockAssetCategory3;

	@Mock
	private AssetCategory mockAssetCategory4;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private AssetCategoryProperty mockAssetCategoryProperty1;

	@Mock
	private AssetCategoryProperty mockAssetCategoryProperty2;

	@Mock
	private AssetCategoryPropertyLocalService mockAssetCategoryPropertyLocalService;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Test
	public void getCategoriesByParentCategory_WhenEntryCategoryDoesNotHaveParentCategoryId_ThenReturnsEmptyList() {
		List<AssetCategory> entryCategories = Arrays.asList(mockAssetCategory2);

		when(mockAssetCategoryLocalService.getCategories(CLASS_NAME, CLASS_PK)).thenReturn(entryCategories);

		List<AssetCategory> result = categoryRetrievalServiceImpl.getCategoriesByParentCategory(CLASS_NAME, CLASS_PK, CATEGORY_ID_1);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getCategoriesByParentCategory_WhenEntryCategoryHasParentCategoryId_ThenReturnsEntryCategoriesWithParentCategory() {
		List<AssetCategory> entryCategories = Arrays.asList(mockAssetCategory2);

		when(mockAssetCategory2.getParentCategoryId()).thenReturn(CATEGORY_ID_1);

		when(mockAssetCategoryLocalService.getCategories(CLASS_NAME, CLASS_PK)).thenReturn(entryCategories);

		List<AssetCategory> result = categoryRetrievalServiceImpl.getCategoriesByParentCategory(CLASS_NAME, CLASS_PK, CATEGORY_ID_1);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(0), sameInstance(mockAssetCategory2));
	}

	@Test
	public void getCategoriesPropertyValues_WhenCalledWithAssetEntryAndPropertyRetrievalFails_ThenDoesNotAppendPropertyValue() throws PortalException {
		mockAssetEntryCategories();

		when(mockAssetCategoryPropertyLocalService.getCategoryProperty(CATEGORY_ID_1, PROPERTY_KEY)).thenThrow(new PortalException());

		String result = categoryRetrievalServiceImpl.getCategoriesPropertyValues(mockAssetEntry, PROPERTY_KEY, VOCABULARY_ID);

		assertThat(result, equalTo(PROPERTY_VALUE_2));
	}

	@Test
	public void getCategoriesPropertyValues_WhenCalledWithAssetEntryAndThereAreNoVocabularyCategoriesWithProperty_ThenReturnsEmptyString() throws PortalException {
		List<AssetCategory> categories = new ArrayList<>();
		categories.add(mockCategoryValue(mockAssetCategory1, CATEGORY_ID_1, mockAssetCategoryProperty1, PROPERTY_VALUE_1, 0L));
		categories.add(mockCategoryValue(mockAssetCategory2, CATEGORY_ID_2, mockAssetCategoryProperty2, PROPERTY_VALUE_2, 0L));
		categories.add(mockCategoryValue(mockAssetCategory3, CATEGORY_ID_3, null, StringPool.BLANK, VOCABULARY_ID));
		categories.add(mockCategoryValue(mockAssetCategory4, CATEGORY_ID_4, mockAssetCategoryProperty1, PROPERTY_VALUE_1, 0L));
		when(mockAssetEntry.getCategories()).thenReturn(categories);

		String result = categoryRetrievalServiceImpl.getCategoriesPropertyValues(mockAssetEntry, PROPERTY_KEY, VOCABULARY_ID);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getCategoriesPropertyValues_WhenCalledWithAssetEntryAndThereAreVocabularyCategoriesWithProperty_ThenReturnsJointCategoriesPropertyValues() throws PortalException {
		mockAssetEntryCategories();

		String result = categoryRetrievalServiceImpl.getCategoriesPropertyValues(mockAssetEntry, PROPERTY_KEY, VOCABULARY_ID);

		assertThat(result, equalTo(PROPERTY_VALUE_1 + StringPool.COMMA + PROPERTY_VALUE_2));
	}

	@Test(expected = PortalException.class)
	public void getCategoriesPropertyValues_WhenCalledWithClassNameAndClassPKAndAssetEntryRetrievalFails_ThenThrowsPortalException() throws PortalException {
		mockAssetEntryCategories();
		when(mockAssetEntryLocalService.getEntry(CLASS_NAME, CLASS_PK)).thenThrow(new PortalException());

		categoryRetrievalServiceImpl.getCategoriesPropertyValues(CLASS_NAME, CLASS_PK, PROPERTY_KEY, VOCABULARY_ID);
	}

	@Test
	public void getCategoriesPropertyValues_WhenCalledWithClassNameAndClassPKAndPropertyRetrievalFails_ThenDoesNotAppendPropertyValue() throws PortalException {
		mockAssetEntryCategories();
		when(mockAssetEntryLocalService.getEntry(CLASS_NAME, CLASS_PK)).thenReturn(mockAssetEntry);

		when(mockAssetCategoryPropertyLocalService.getCategoryProperty(CATEGORY_ID_1, PROPERTY_KEY)).thenThrow(new PortalException());

		String result = categoryRetrievalServiceImpl.getCategoriesPropertyValues(CLASS_NAME, CLASS_PK, PROPERTY_KEY, VOCABULARY_ID);

		assertThat(result, equalTo(PROPERTY_VALUE_2));
	}

	@Test
	public void getCategoriesPropertyValues_WhenCalledWithClassNameAndClassPKAndThereAreNoVocabularyCategoriesWithProperty_ThenReturnsEmptyString() throws PortalException {
		List<AssetCategory> categories = new ArrayList<>();
		categories.add(mockCategoryValue(mockAssetCategory1, CATEGORY_ID_1, mockAssetCategoryProperty1, PROPERTY_VALUE_1, 0L));
		categories.add(mockCategoryValue(mockAssetCategory2, CATEGORY_ID_2, mockAssetCategoryProperty2, PROPERTY_VALUE_2, 0L));
		categories.add(mockCategoryValue(mockAssetCategory3, CATEGORY_ID_3, null, StringPool.BLANK, VOCABULARY_ID));
		categories.add(mockCategoryValue(mockAssetCategory4, CATEGORY_ID_4, mockAssetCategoryProperty1, PROPERTY_VALUE_1, 0L));
		when(mockAssetEntry.getCategories()).thenReturn(categories);
		when(mockAssetEntryLocalService.getEntry(CLASS_NAME, CLASS_PK)).thenReturn(mockAssetEntry);

		String result = categoryRetrievalServiceImpl.getCategoriesPropertyValues(CLASS_NAME, CLASS_PK, PROPERTY_KEY, VOCABULARY_ID);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void getCategoriesPropertyValues_WhenCalledWithClassNameAndClassPKAndThereAreVocabularyCategoriesWithProperty_ThenReturnsJointCategoriesPropertyValues() throws PortalException {
		mockAssetEntryCategories();
		when(mockAssetEntryLocalService.getEntry(CLASS_NAME, CLASS_PK)).thenReturn(mockAssetEntry);

		String result = categoryRetrievalServiceImpl.getCategoriesPropertyValues(CLASS_NAME, CLASS_PK, PROPERTY_KEY, VOCABULARY_ID);

		assertThat(result, equalTo(PROPERTY_VALUE_1 + StringPool.COMMA + PROPERTY_VALUE_2));
	}

	@Test
	public void getPropertyValue_WhenNoError_ThenReturnsThePropertyValue() throws PortalException {
		mockCategoryValue(mockAssetCategory1, CATEGORY_ID_1, mockAssetCategoryProperty1, PROPERTY_VALUE_1, 0L);

		String propertyValue = categoryRetrievalServiceImpl.getPropertyValue(mockAssetCategory1, PROPERTY_KEY);

		assertThat(propertyValue, equalTo(PROPERTY_VALUE_1));
	}

	@Test
	public void getPropertyValue_WhenPropertyCannotBeRetrieved_ThenReturnsEmptyString() throws PortalException {
		mockCategoryValue(mockAssetCategory1, CATEGORY_ID_1, null, StringPool.BLANK, 0L);
		when(mockAssetCategoryPropertyLocalService.getCategoryProperty(CATEGORY_ID_1, PROPERTY_KEY)).thenThrow(new PortalException());

		String propertyValue = categoryRetrievalServiceImpl.getPropertyValue(mockAssetCategory1, PROPERTY_KEY);

		assertThat(propertyValue, equalTo(StringPool.BLANK));
	}

	private void mockAssetEntryCategories() throws PortalException {
		List<AssetCategory> categories = new ArrayList<>();
		categories.add(mockCategoryValue(mockAssetCategory1, CATEGORY_ID_1, mockAssetCategoryProperty1, PROPERTY_VALUE_1, VOCABULARY_ID));
		categories.add(mockCategoryValue(mockAssetCategory2, CATEGORY_ID_2, mockAssetCategoryProperty2, PROPERTY_VALUE_2, VOCABULARY_ID));
		categories.add(mockCategoryValue(mockAssetCategory3, CATEGORY_ID_3, null, StringPool.BLANK, VOCABULARY_ID));
		categories.add(mockCategoryValue(mockAssetCategory4, CATEGORY_ID_4, mockAssetCategoryProperty1, PROPERTY_VALUE_1, 0L));
		when(mockAssetEntry.getCategories()).thenReturn(categories);
	}

	private AssetCategory mockCategoryValue(AssetCategory assetCategory, long categoryId, AssetCategoryProperty assetCategoryProperty, String value, long vocabulary) throws PortalException {
		when(assetCategory.getCategoryId()).thenReturn(categoryId);
		when(assetCategory.getVocabularyId()).thenReturn(vocabulary);

		if (assetCategoryProperty != null) {
			when(mockAssetCategoryPropertyLocalService.getCategoryProperty(categoryId, PROPERTY_KEY)).thenReturn(assetCategoryProperty);
			when(assetCategoryProperty.getValue()).thenReturn(value);
		}

		return assetCategory;
	}
}
