package com.placecube.digitalplace.group.restadmin.web.portlet;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.group.rest.constants.WebhookEvent;
import com.placecube.digitalplace.group.rest.model.GroupWebhook;
import com.placecube.digitalplace.group.rest.service.GroupWebhookLocalService;
import com.placecube.digitalplace.group.restadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.ViewKeys;
import com.placecube.digitalplace.group.restadmin.web.service.GroupWebhookManagementService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.GROUP_WEBHOOKS, "mvc.command.name=" + MVCCommandKeys.VIEW, "mvc.command.name=/" }, service = MVCRenderCommand.class)
public class ViewWebhooksMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private GroupWebhookLocalService groupWebhookLocalService;

	@Reference
	private GroupWebhookManagementService groupWebhookManagementService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long scopeGroupId = themeDisplay.getScopeGroupId();

		SearchContainer<GroupWebhook> searchContainer = groupWebhookManagementService.getSearchContainer(renderRequest, renderResponse);
		searchContainer.setResultsAndTotal(groupWebhookLocalService.getGroupWebhooks(scopeGroupId, -1, -1));
		renderRequest.setAttribute("searchContainer", searchContainer);

		renderRequest.setAttribute("webhookEvents", WebhookEvent.values());

		return ViewKeys.VIEW_GROUP_WEBHOOK_JSP;
	}

}
