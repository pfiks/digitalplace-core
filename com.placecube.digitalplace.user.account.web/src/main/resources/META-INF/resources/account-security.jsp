<%@ include file="init.jsp"%>

<c:set var="progressBarValue" value="90"/>
<%@ include file="/fragments/progress-bar.jspf" %>

<div class="row">
	<%@ include file="/fragments/side-bar.jspf" %>

	<div class="col-md-8">

		<portlet:actionURL name="<%=MVCCommandKeys.ACCOUNT_SECURITY%>" var="accountSecurityURL" />

		<gds-forms-ui:errorSummary portletNamespace="${portletNamespace}" errors="${accountContext.getErrors()}"/>

		<aui:form action="${accountSecurityURL}" method="post" name="accountSecurityForm">
		
			<%@ include file="/fragments/creation-errors.jspf" %>
	
			<liferay-journal:journal-article 
				articleId="<%= WebContentArticles.USER_ACCOUNT_TWO_FACTOR_AUTHENTICATION.getArticleId() %>" 
				groupId="${themeDisplay.companyGroupId}" 
				showTitle="false" />
			
			<div class="mt-5">
				<gds-forms-ui:input-radio 
					portletNamespace="${ portletNamespace }" 
					fieldName="<%= AccountValidatedField.TWO_FACTOR_AUTHENTICATION_ENABLED.getFieldName()%>"
					fieldLabel="two-factor-authentication"
					fieldValue="${accountContext.twoFactorAuthenticationEnabled}"
					fieldOptions="${ twoFactorAuthenticationEnabledOptions }"
					required="true"
				/>
			</div>
						
			<c:if test="${captchaEnabled}">
				<%@ include file="/fragments/captcha.jspf" %>
			</c:if>
			
			<aui:button-row>
				<div class="pull-right">
					<aui:button type="submit" value="register" cssClass="btn btn-primary" icon="glyphicon glyphicon-chevron-right" iconAlign="right"/>
				</div>
				<div class="pull-left">
					<portlet:renderURL var="backURL">
						<portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.PASSWORD_DETAILS%>" />
					</portlet:renderURL>
					<aui:button type="button" value="previous" cssClass="btn btn-default" href="${backURL}" icon="glyphicon glyphicon-chevron-left" iconAlign="left"/>
				</div>
			</aui:button-row>
		</aui:form>					
	</div>
</div>