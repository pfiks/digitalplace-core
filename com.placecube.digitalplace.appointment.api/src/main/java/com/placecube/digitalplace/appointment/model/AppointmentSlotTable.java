/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_Appointment_AppointmentSlot&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentSlot
 * @generated
 */
public class AppointmentSlotTable extends BaseTable<AppointmentSlotTable> {

	public static final AppointmentSlotTable INSTANCE =
		new AppointmentSlotTable();

	public final Column<AppointmentSlotTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Long> appointmentSlotId =
		createColumn(
			"appointmentSlotId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<AppointmentSlotTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, String> userName = createColumn(
		"userName", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, String> slotType = createColumn(
		"slotType", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, String> name = createColumn(
		"name", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, String> serviceId = createColumn(
		"serviceId", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Date> startDate = createColumn(
		"startDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Date> endDate = createColumn(
		"endDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Integer> slotStartTimeHour =
		createColumn(
			"slotStartTimeHour", Integer.class, Types.INTEGER,
			Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Integer> slotStartTimeMinute =
		createColumn(
			"slotStartTimeMinute", Integer.class, Types.INTEGER,
			Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Integer> slotEndTimeHour =
		createColumn(
			"slotEndTimeHour", Integer.class, Types.INTEGER,
			Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Integer> slotEndTimeMinute =
		createColumn(
			"slotEndTimeMinute", Integer.class, Types.INTEGER,
			Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Integer> appointmentDuration =
		createColumn(
			"appointmentDuration", Integer.class, Types.INTEGER,
			Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Long> daysOfWeekBitwise =
		createColumn(
			"daysOfWeekBitwise", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<AppointmentSlotTable, Integer> maxAppointments =
		createColumn(
			"maxAppointments", Integer.class, Types.INTEGER,
			Column.FLAG_DEFAULT);

	private AppointmentSlotTable() {
		super(
			"Placecube_Appointment_AppointmentSlot", AppointmentSlotTable::new);
	}

}