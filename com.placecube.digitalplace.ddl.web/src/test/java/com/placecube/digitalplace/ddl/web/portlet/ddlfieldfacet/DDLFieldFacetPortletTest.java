package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.common.portlet.PortletURLService;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration.DDLFieldFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.constants.DDLFieldFacetDisplayConstants;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetDDMFormFieldService;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetParserService;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetRequestService;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class DDLFieldFacetPortletTest extends PowerMockito {

	private static final String COLOUR = "orange";
	private static final String CUSTOM_HEADING = "customHeading";
	private static final String DDL_RECORD_LINK_FIELD_NAME = "recordLinkFieldName";
	private static final String DDM_FIELD_NAME = "thisField";
	private static final long DDM_STRUCTURE_ID = 1L;
	private static final String INDEXED_FIELD_NAME = "thisIndexedField";
	private static final boolean IS_DDL_RECORD_LINK = true;
	private static final long LINK_DDM_STRUCTURE_ID = 3242;
	private static final String SEARCH_RESULT_URL = "URL";
	private static final boolean SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION = true;
	private static final String TITLE_LABEL = "TITLE";

	@InjectMocks
	private DDLFieldFacetPortlet ddlFieldFacetPortlet;

	@Captor
	private ArgumentCaptor<List<FacetResult>> facetResultsArgumentCaptor;

	@Mock
	private List<FacetResult> mockAvailableFieldValues;

	@Mock
	private DDLFieldFacetPortletInstanceConfiguration mockConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private DDLFieldFacetDDMFormFieldService mockDDLFieldFacetDDMFormFieldService;

	@Mock
	private DDLFieldFacetParserService mockDDLFieldFacetParserService;

	@Mock
	private DDLFieldFacetRequestService mockDDLFieldFacetRequestService;

	@Mock
	private DDMFormField mockDDMFormField;

	@Mock
	private DDMFormFieldOptions mockDDMFormFieldOptions;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private FacetResult mockFacetResultOne;

	@Mock
	private FacetResult mockFacetResultTwo;

	@Mock
	private Map<String, Integer> mockFacetValues;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Locale mockLocale;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletURLService mockPortletURLService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private Set<String> mockSelectedValues;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DDLFieldFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockRenderRequest.getLocale()).thenReturn(mockLocale);

	}

	@Test
	public void render_WhenAlphaSortIsDisabled_ThenDoesNotSortTheAvailableFieldValues() throws Exception {
		mockConfiguration();
		mockDDMObjects();

		List<FacetResult> facetResults = Arrays.asList(mockFacetResultOne, mockFacetResultTwo);

		when(mockFacetResultOne.getLabel(mockLocale)).thenReturn("b");
		when(mockFacetResultTwo.getLabel(mockLocale)).thenReturn("a");

		when(mockConfiguration.alphaSort()).thenReturn(false);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		when(mockDDLFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField)).thenReturn(true);
		when(mockDDLFieldFacetParserService.parseFacetsWithFieldOptions(mockFacetValues, mockSelectedValues, mockDDMFormFieldOptions)).thenReturn(facetResults);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(eq("availableFieldValues"), facetResultsArgumentCaptor.capture());

		List<FacetResult> capturedFacetResults = facetResultsArgumentCaptor.getValue();

		assertThat(capturedFacetResults.size(), equalTo(2));
		assertThat(capturedFacetResults.get(0), sameInstance(mockFacetResultOne));
		assertThat(capturedFacetResults.get(1), sameInstance(mockFacetResultTwo));

	}

	@Test
	public void render_WhenAlphaSortIsEnabled_ThenSortsTheAvailableFieldValues() throws Exception {
		mockConfiguration();
		mockDDMObjects();

		List<FacetResult> facetResults = Arrays.asList(mockFacetResultOne, mockFacetResultTwo);

		when(mockFacetResultOne.getLabel(mockLocale)).thenReturn("b");
		when(mockFacetResultTwo.getLabel(mockLocale)).thenReturn("a");

		when(mockConfiguration.alphaSort()).thenReturn(true);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		when(mockDDLFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField)).thenReturn(true);
		when(mockDDLFieldFacetParserService.parseFacetsWithFieldOptions(mockFacetValues, mockSelectedValues, mockDDMFormFieldOptions)).thenReturn(facetResults);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(eq("availableFieldValues"), facetResultsArgumentCaptor.capture());

		List<FacetResult> capturedFacetResults = facetResultsArgumentCaptor.getValue();

		assertThat(capturedFacetResults.size(), equalTo(2));
		assertThat(capturedFacetResults.get(0), sameInstance(mockFacetResultTwo));
		assertThat(capturedFacetResults.get(1), sameInstance(mockFacetResultOne));
	}

	@Test
	public void render_WhenDDMFieldConfigurationInvalid_ThenShowInvalidConfigurationMessage() throws Exception {
		when(mockConfiguration.ddmStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockConfiguration.ddmFieldName()).thenReturn(null);
		when(mockDDMStructureLocalService.fetchDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
		verify(mockSharedSearchRequestService, times(0)).search(mockRenderRequest);
	}

	@Test
	public void render_WhenDDMStructureAndFieldNameAreNotNull_ThenSetsFieldFacetRequestAttributes() throws Exception {
		mockConfiguration();
		mockDDMObjects();

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);

		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("displayStyle", DDLFieldFacetDisplayConstants.FACET_DISPLAY_STYLE_CHECKBOX);
		verify(mockRenderRequest, times(1)).setAttribute("customHeading", CUSTOM_HEADING);
		verify(mockRenderRequest, times(1)).setAttribute("facetFieldName", INDEXED_FIELD_NAME);
		verify(mockRenderRequest, times(1)).setAttribute("filterTitle", TITLE_LABEL);
		verify(mockRenderRequest, times(1)).setAttribute("filterColour", COLOUR);
	}

	@Test
	public void render_WhenDDMStructureAndFieldNameAreNotNullAndConfiguredFieldIsInUrl_ThenSetsEscapedRedirectUrlWithoutFieldUrlParameterInRequest() throws Exception {
		mockConfiguration();
		mockDDMObjects();

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);

		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		when(mockRenderRequest.getPreferences()).thenReturn(mockPortletPreferences);
		when(mockDDLFieldFacetRequestService.isConfiguredFieldNameValueInUrl(mockThemeDisplay, mockPortletPreferences)).thenReturn(true);

		when(mockDDLFieldFacetRequestService.getEscapedRedirectUrlWithoutFieldUrlParameter(mockSharedSearchResponse, mockPortletPreferences)).thenReturn(SEARCH_RESULT_URL);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", SEARCH_RESULT_URL);
	}

	@Test
	public void render_WhenDDMStructureAndFieldNameAreNotNullAndConfiguredFieldIsNotInUrl_ThenSetsEscapedIteratorUrlAsRedirectInRequest() throws Exception {
		mockConfiguration();
		mockDDMObjects();

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);

		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		when(mockRenderRequest.getPreferences()).thenReturn(mockPortletPreferences);
		when(mockDDLFieldFacetRequestService.isConfiguredFieldNameValueInUrl(mockThemeDisplay, mockPortletPreferences)).thenReturn(false);

		when(mockSharedSearchResponse.getEscapedSearchResultsIteratorURL()).thenReturn(SEARCH_RESULT_URL);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", SEARCH_RESULT_URL);
	}

	@Test
	public void render_WhenDDMStructureAndFieldNameAreNotNullAndFieldTypeAllowsOptionsAndDisplayStyleIsCheckbox_ThenCreatesAvailableFacetResultsFromOptionsAndSetsThemInRequest() throws Exception {
		mockConfiguration();
		mockDDMObjects();

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		when(mockDDLFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField)).thenReturn(true);
		when(mockDDLFieldFacetParserService.parseFacetsWithFieldOptions(mockFacetValues, mockSelectedValues, mockDDMFormFieldOptions)).thenReturn(mockAvailableFieldValues);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableFieldValues", mockAvailableFieldValues);
	}

	@Test
	public void render_WhenDDMStructureAndFieldNameAreNotNullAndFieldTypeAllowsOptionsAndDisplayStyleIsDropdown_ThenCreatesAvailableFacetResultsFromAllOptionsAndSetsThemInRequest() throws Exception {
		mockConfiguration();
		mockDDMObjects();

		when(mockConfiguration.displayStyle()).thenReturn(DDLFieldFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		when(mockDDLFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField)).thenReturn(true);
		when(mockDDLFieldFacetParserService.parseAllFieldOptions(mockFacetValues, mockSelectedValues, mockDDMFormFieldOptions)).thenReturn(mockAvailableFieldValues);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableFieldValues", mockAvailableFieldValues);
	}

	@Test
	public void render_WhenDDMStructureAndFieldNameAreNotNullAndFieldTypeAllowsOptionsAndDisplayStyleIsDropdown_ThenSetsShowHaeadingAsFirstOptionAttributeInRequest() throws Exception {
		mockConfiguration();
		mockDDMObjects();

		when(mockConfiguration.displayStyle()).thenReturn(DDLFieldFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		when(mockDDLFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField)).thenReturn(true);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("showHeadingAsFirstDropdownOption", SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION);
	}

	@Test
	public void render_WhenDDMStructureAndFieldNameAreNotNullAndFieldTypeDoesNotAllowOptionsAndDisplayStyleIsCheckbox_ThenCreatesAvailableFieldValuesFromFacetValuesAndSetsThemInRequest()
			throws Exception {
		mockConfiguration();
		mockDDMObjects();

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		when(mockDDLFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField)).thenReturn(false);
		when(mockDDLFieldFacetParserService.parseFacetsWithFacetValues(mockFacetValues, mockSelectedValues, IS_DDL_RECORD_LINK, DDL_RECORD_LINK_FIELD_NAME, mockLocale))
				.thenReturn(mockAvailableFieldValues);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableFieldValues", mockAvailableFieldValues);
	}

	@Test
	public void render_WhenDDMStructureAndFieldNameAreNotNullAndFieldTypeDoesNotAllowOptionsAndDisplayStyleIsDropdown_ThenCreatesAvailableFieldValuesFromAllFieldValuesAndSetsThemInRequest()
			throws Exception {
		mockConfiguration();
		mockDDMObjects();

		when(mockConfiguration.displayStyle()).thenReturn(DDLFieldFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockDDLFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME)).thenReturn(INDEXED_FIELD_NAME);

		when(mockSharedSearchResponse.getActiveValuesForFilter(INDEXED_FIELD_NAME)).thenReturn(mockSelectedValues);
		when(mockSharedSearchResponse.getFacetValues(INDEXED_FIELD_NAME)).thenReturn(mockFacetValues);

		when(mockDDLFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField)).thenReturn(false);
		when(mockDDLFieldFacetParserService.parseAllFacetValues(mockFacetValues, mockSelectedValues, IS_DDL_RECORD_LINK, DDL_RECORD_LINK_FIELD_NAME, LINK_DDM_STRUCTURE_ID, mockHttpServletRequest))
				.thenReturn(mockAvailableFieldValues);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableFieldValues", mockAvailableFieldValues);
	}

	@Test
	public void render_WhenDDMStructureConfigurationInvalid_ThenShowInvalidConfigurationMessage() throws Exception {
		when(mockConfiguration.ddmStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockConfiguration.ddmFieldName()).thenReturn(DDM_FIELD_NAME);
		when(mockDDMStructureLocalService.fetchDDMStructure(DDM_STRUCTURE_ID)).thenReturn(null);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
		verify(mockSharedSearchRequestService, times(0)).search(mockRenderRequest);
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		mockConfiguration();
		mockDDMObjects();

		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenPortletIsSetToInvisible_ThenSetsIsEditLayoutModeAttributeInRequest() throws Exception {
		when(mockConfiguration.invisible()).thenReturn(true);
		when(mockPortletURLService.isEditLayoutMode(mockRenderRequest)).thenReturn(true);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("isEditLayoutMode", true);
	}

	@Test
	public void render_WhenPortletsetToInvisible_ThenSetInvisibleParam() throws Exception {
		when(mockConfiguration.invisible()).thenReturn(true);

		ddlFieldFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invisible", true);
		verify(mockSharedSearchRequestService, times(0)).search(mockRenderRequest);
	}

	private void mockConfiguration() {
		when(mockConfiguration.ddmStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockConfiguration.ddmFieldName()).thenReturn(DDM_FIELD_NAME);
		when(mockConfiguration.filterColour()).thenReturn(COLOUR);
		when(mockConfiguration.displayStyle()).thenReturn(DDLFieldFacetDisplayConstants.FACET_DISPLAY_STYLE_CHECKBOX);
		when(mockConfiguration.isFieldNameDDLRecordLink()).thenReturn(IS_DDL_RECORD_LINK);
		when(mockConfiguration.ddlRecordLinkFieldName()).thenReturn(DDL_RECORD_LINK_FIELD_NAME);
		when(mockConfiguration.ddmLinkStructureId()).thenReturn(LINK_DDM_STRUCTURE_ID);
		when(mockConfiguration.customHeading()).thenReturn(CUSTOM_HEADING);
		when(mockConfiguration.showHeadingAsFirstDropdownOption()).thenReturn(SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION);
	}

	private void mockDDMObjects() throws Exception {
		when(mockDDMStructureLocalService.fetchDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getFieldLabel(DDM_FIELD_NAME, mockLocale)).thenReturn(TITLE_LABEL);

		when(mockDDMStructure.getDDMFormField(DDM_FIELD_NAME)).thenReturn(mockDDMFormField);
		when(mockDDMFormField.getDDMFormFieldOptions()).thenReturn(mockDDMFormFieldOptions);

	}
}
