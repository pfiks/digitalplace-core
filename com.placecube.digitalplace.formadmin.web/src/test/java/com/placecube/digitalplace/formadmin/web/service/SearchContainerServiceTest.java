package com.placecube.digitalplace.formadmin.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class SearchContainerServiceTest {

	@Mock
	private PortletURL mockPortletRenderURL;

	@Mock
	private MutableRenderParameters mockRenderParameters;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@InjectMocks
	private SearchContainerService searchContainerService;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void getSearchContainer_WhenNoError_ThenReturnsSearchContainerCreated() {
		String noResultsMessage = "noResultsMessageVal";
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletRenderURL);
		when(ParamUtil.getString(mockRenderRequest, "currentTab")).thenReturn("data-consumer");
		when(ParamUtil.getString(mockRenderRequest, "mvcRenderCommandName")).thenReturn("data-consumer");
		when(mockPortletRenderURL.getRenderParameters()).thenReturn(mockRenderParameters);

		SearchContainer result = searchContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse, noResultsMessage);

		assertThat(result, notNullValue());
		assertThat(result.getHeaderNames(), nullValue());
		assertThat(result.getEmptyResultsMessage(), equalTo(noResultsMessage));
		assertThat(result.getIteratorURL(), sameInstance(mockPortletRenderURL));
		assertThat(result.isDeltaConfigurable(), equalTo(true));
	}
}
