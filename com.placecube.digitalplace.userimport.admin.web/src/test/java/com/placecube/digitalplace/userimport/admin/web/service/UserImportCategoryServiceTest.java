package com.placecube.digitalplace.userimport.admin.web.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.entry.rel.service.AssetEntryAssetCategoryRelLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.userimport.admin.web.model.UserImportResult;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class UserImportCategoryServiceTest {

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private AssetEntryAssetCategoryRelLocalService mockAssetEntryAssetCategoryRelLocalService;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private ClassNameLocalService mockClassNameLocalService;

	@Mock
	private CompanyLocalService mockCompanyLocalService;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private UserImportCategoryService userImportCategoryService;

	@Before
	public void activeSetUp() {
		PowerMockito.mockStatic(ParamUtil.class);
	}

	@Test
	public void addCategoriesToUser_WhenCategoriesDoNotExist_ThenAddExceptionToResult() throws PortalException {
		JSONObject mockUserToImport = mock(JSONObject.class);
		User mockUser = mock(User.class);
		UserImportResult mockUserImportResult = mock(UserImportResult.class);
		Map<String, String> mockVocabularyMappings = Collections.singletonMap("OriginalVocabulary", "MappedVocabulary");

		long mockGlobalGroupId = 123L;
		AssetVocabulary mockVocabulary = mock(AssetVocabulary.class);
		AssetEntry mockAssetEntry = mock(AssetEntry.class);

		when(mockCompanyLocalService.getCompany(anyLong())).thenReturn(mock(Company.class));
		when(mockCompanyLocalService.getCompany(anyLong()).getGroupId()).thenReturn(mockGlobalGroupId);
		when(mockAssetEntryLocalService.getEntry(User.class.getName(), mockUser.getUserId())).thenReturn(mockAssetEntry);
		when(mockAssetVocabularyLocalService.getGroupVocabulary(mockGlobalGroupId, "OriginalVocabulary")).thenReturn(mockVocabulary);
		when(mockAssetCategoryLocalService.getVocabularyCategories(anyLong(), anyInt(), anyInt(), isNull())).thenReturn(Collections.emptyList());
		when(mockUserToImport.get("MappedVocabulary")).thenReturn("NonexistentCategory");

		userImportCategoryService.addCategoriesToUser(mockUserToImport, mockVocabularyMappings, mockUser, mockUserImportResult);

		verify(mockUserImportResult, times(1))
				.addExceptionMessage(argThat(exception -> exception.getMessage().contains("Following categories were not found in vocabulary 'MappedVocabulary': NonexistentCategory")));
		verify(mockAssetEntryAssetCategoryRelLocalService, never()).addAssetEntryAssetCategoryRel(anyLong(), anyLong());
	}

	@Test
	public void addCategoriesToUser_WhenCategoriesExist_ThenAddsCorrectCategories() throws PortalException {
		JSONObject mockUserToImport = mock(JSONObject.class);
		User mockUser = mock(User.class);
		UserImportResult mockUserImportResult = mock(UserImportResult.class);
		Map<String, String> mockVocabularyMappings = Collections.singletonMap("OriginalVocabulary", "MappedVocabulary");

		long mockGlobalGroupId = 123L;
		AssetVocabulary mockVocabulary = mock(AssetVocabulary.class);
		AssetCategory mockCategory = mock(AssetCategory.class);
		AssetEntry mockAssetEntry = mock(AssetEntry.class);

		when(mockCompanyLocalService.getCompany(anyLong())).thenReturn(mock(Company.class));
		when(mockCompanyLocalService.getCompany(anyLong()).getGroupId()).thenReturn(mockGlobalGroupId);
		when(mockAssetEntryLocalService.getEntry(User.class.getName(), mockUser.getUserId())).thenReturn(mockAssetEntry);
		when(mockAssetVocabularyLocalService.getGroupVocabulary(mockGlobalGroupId, "OriginalVocabulary")).thenReturn(mockVocabulary);
		when(mockAssetCategoryLocalService.getVocabularyCategories(anyLong(), anyInt(), anyInt(), isNull())).thenReturn(Collections.singletonList(mockCategory));
		when(mockCategory.getName()).thenReturn("CategoryName");
		when(mockCategory.getCategoryId()).thenReturn(789L);
		when(mockUserToImport.get("MappedVocabulary")).thenReturn("CategoryName");

		userImportCategoryService.addCategoriesToUser(mockUserToImport, mockVocabularyMappings, mockUser, mockUserImportResult);

		verify(mockAssetEntryAssetCategoryRelLocalService, times(1)).addAssetEntryAssetCategoryRel(mockAssetEntry.getEntryId(), 789L);
	}

	@Test
	public void addCategoriesToUser_WhenGettingGroupVocabularyThrowsError_ThenNoCategoriesAdded() throws PortalException {
		JSONObject mockUserToImport = mock(JSONObject.class);
		User mockUser = mock(User.class);
		UserImportResult mockUserImportResult = mock(UserImportResult.class);
		Map<String, String> mockVocabularyMappings = Collections.singletonMap("OriginalVocabulary", "MappedVocabulary");

		long mockGlobalGroupId = 123L;
		AssetEntry mockAssetEntry = mock(AssetEntry.class);

		when(mockCompanyLocalService.getCompany(anyLong())).thenReturn(mock(Company.class));
		when(mockCompanyLocalService.getCompany(anyLong()).getGroupId()).thenReturn(mockGlobalGroupId);
		when(mockAssetEntryLocalService.getEntry(User.class.getName(), mockUser.getUserId())).thenReturn(mockAssetEntry);
		when(mockAssetVocabularyLocalService.getGroupVocabulary(mockGlobalGroupId, "OriginalVocabulary")).thenThrow(new PortalException("Error fetching vocabulary"));
		when(mockUserToImport.get("MappedVocabulary")).thenReturn("CategoryName");

		userImportCategoryService.addCategoriesToUser(mockUserToImport, mockVocabularyMappings, mockUser, mockUserImportResult);

		verify(mockAssetEntryAssetCategoryRelLocalService, never()).addAssetEntryAssetCategoryRel(anyLong(), anyLong());
		verify(mockUserImportResult, never()).addExceptionMessage(any());
	}

	@Test
	public void getGlobalVocabulariesOfUserType_WhenCalled_ThenReturnsFilteredVocabularies() throws PortalException {
		long mockGlobalGroupId = 123L;
		long mockUserClassNameId = 456L;
		long mockCompanyId = 123L;

		Company mockCompany = mock(Company.class);
		when(mockCompanyLocalService.getCompany(mockCompanyId)).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(mockGlobalGroupId);

		when(mockClassNameLocalService.getClassNameId(User.class)).thenReturn(mockUserClassNameId);

		AssetVocabulary mockVocabulary = mock(AssetVocabulary.class);
		when(mockVocabulary.isAssociatedToClassNameId(mockUserClassNameId)).thenReturn(true);

		when(mockAssetVocabularyLocalService.getGroupVocabularies(mockGlobalGroupId)).thenReturn(Collections.singletonList(mockVocabulary));

		List<AssetVocabulary> result = userImportCategoryService.getGlobalVocabulariesOfUserType(mockCompanyId);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(0), equalTo(mockVocabulary));
	}

	@Test
	public void getGlobalVocabulariesOfUserType_WhenCategoriesAssociatedWithUserClass_ThenReturnEmptyList() throws PortalException {
		long mockGlobalGroupId = 123L;
		long mockUserClassNameId = 456L;
		long mockCompanyId = 123L;

		Company mockCompany = mock(Company.class);
		when(mockCompanyLocalService.getCompany(mockCompanyId)).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(mockGlobalGroupId);

		when(mockClassNameLocalService.getClassNameId(User.class)).thenReturn(mockUserClassNameId);

		AssetVocabulary mockVocabulary = mock(AssetVocabulary.class);

		when(mockVocabulary.isAssociatedToClassNameId(mockUserClassNameId)).thenReturn(false);

		when(mockAssetVocabularyLocalService.getGroupVocabularies(mockGlobalGroupId)).thenReturn(Collections.singletonList(mockVocabulary));

		List<AssetVocabulary> result = userImportCategoryService.getGlobalVocabulariesOfUserType(mockCompanyId);

		assertThat(result.size(), equalTo(0));
	}

	@Test
	public void getGlobalVocabulariesOfUserType_WhenNoCategoriesInVocabulary_ThenReturnEmptyList() throws PortalException {
		long mockGlobalGroupId = 123L;
		long mockUserClassNameId = 456L;
		long mockCompanyId = 123L;

		Company mockCompany = mock(Company.class);
		when(mockCompanyLocalService.getCompany(mockCompanyId)).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(mockGlobalGroupId);

		when(mockClassNameLocalService.getClassNameId(User.class)).thenReturn(mockUserClassNameId);

		when(mockAssetVocabularyLocalService.getGroupVocabularies(mockGlobalGroupId)).thenReturn(Collections.emptyList());

		List<AssetVocabulary> result = userImportCategoryService.getGlobalVocabulariesOfUserType(mockCompanyId);

		assertThat(result.size(), equalTo(0));
	}

	@Test
	public void getUserVocabularyMappings_WhenMappingsExist_ThenReturnsCorrectMappings() throws PortalException {
		long mockGlobalGroupId = 123L;
		long mockUserClassNameId = 456L;
		long mockCompanyId = 124l;

		when(mockServiceContext.getCompanyId()).thenReturn(mockCompanyId);
		Company mockCompany = mock(Company.class);
		when(mockCompanyLocalService.getCompany(mockCompanyId)).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(mockGlobalGroupId);

		when(mockPortal.getDefaultCompanyId()).thenReturn(1L);
		when(mockClassNameLocalService.getClassNameId(User.class)).thenReturn(mockUserClassNameId);

		ActionRequest mockActionRequest = mock(ActionRequest.class);
		AssetVocabulary mockVocabulary = mock(AssetVocabulary.class);
		when(mockVocabulary.getName()).thenReturn("TestVocabulary");
		when(mockVocabulary.isAssociatedToClassNameId(mockUserClassNameId)).thenReturn(true);

		when(mockAssetVocabularyLocalService.getGroupVocabularies(mockGlobalGroupId)).thenReturn(Collections.singletonList(mockVocabulary));
		when(ParamUtil.getString(mockActionRequest, "TestVocabulary")).thenReturn("MappedValue");

		Map<String, String> result = userImportCategoryService.getUserVocabularyMappings(mockActionRequest, mockServiceContext);

		assertThat(result.size(), equalTo(1));
		assertThat(result, hasEntry("TestVocabulary", "MappedValue"));
	}

	@Test
	public void getUserVocabularyMappings_WhenNoVocabularyNameInActionRequest_ThenReturnEmptyMapping() throws PortalException {
		long mockGlobalGroupId = 123L;
		long mockUserClassNameId = 456L;
		long mockCompanyId = 124l;

		when(mockServiceContext.getCompanyId()).thenReturn(mockCompanyId);
		Company mockCompany = mock(Company.class);
		when(mockCompanyLocalService.getCompany(mockCompanyId)).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(mockGlobalGroupId);

		when(mockPortal.getDefaultCompanyId()).thenReturn(1L);
		when(mockClassNameLocalService.getClassNameId(User.class)).thenReturn(mockUserClassNameId);

		ActionRequest mockActionRequest = mock(ActionRequest.class);
		AssetVocabulary mockVocabulary = mock(AssetVocabulary.class);
		when(mockVocabulary.getName()).thenReturn("TestVocabulary");
		when(mockVocabulary.isAssociatedToClassNameId(mockUserClassNameId)).thenReturn(true);

		when(mockAssetVocabularyLocalService.getGroupVocabularies(mockGlobalGroupId)).thenReturn(Collections.singletonList(mockVocabulary));
		when(ParamUtil.getString(mockActionRequest, "TestVocabulary")).thenReturn(null);

		Map<String, String> result = userImportCategoryService.getUserVocabularyMappings(mockActionRequest, mockServiceContext);

		assertThat(result.size(), equalTo(0));
	}

}
