package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.displaycontext;

import java.util.List;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;

public class AdvancePeopleSearchDisplayContext {

	private final List<SharedActiveFilter> activeFilters;

	private AdvancePeopleSearchDisplayContext(List<SharedActiveFilter> activeFilters) {
		this.activeFilters = activeFilters;
	}

	public static AdvancePeopleSearchDisplayContext initDisplayContext(List<SharedActiveFilter> activeFilters) {
		return new AdvancePeopleSearchDisplayContext(activeFilters);
	}

	public String getFilterValue(String field) {
		String filterValue = StringPool.BLANK;
		SharedActiveFilter activeFilter = activeFilters.stream().filter(filter -> filter.getName().equals(field)).findFirst().orElse(null);
		if (activeFilter != null) {
			filterValue = activeFilter.getValue();
		}
		return filterValue;
	}

}
