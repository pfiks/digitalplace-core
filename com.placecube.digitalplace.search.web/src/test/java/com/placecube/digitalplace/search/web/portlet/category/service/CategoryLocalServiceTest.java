package com.placecube.digitalplace.search.web.portlet.category.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

public class CategoryLocalServiceTest extends PowerMockito {

	private static final long SCOPE_GROUP_ID = 1l;

	private static final long COMPANY_GROUP_ID = 2l;
	
	@InjectMocks
	private CategoryLocalService categoryLocalService;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private AssetCategory mockAssetCategory1;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private AssetCategory mockAssetCategory3;

	@Mock
	private AssetVocabulary mockAssetVocabulary1;

	@Mock
	private AssetVocabulary mockAssetVocabulary2;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	private List<AssetVocabulary> assetVocabularyList;

	@Before
	public void before() {
		initMocks(this);
		assetVocabularyList = new ArrayList<>();
	}

	@Test
	public void getAvailableVocabularies_WhenAllVocabulariesHaveCategories_ThenReturnAllVocabularies() {
		givenAssetVocabulariesExist();

		List<AssetVocabulary> result = categoryLocalService.getAvailableVocabularies(mockThemeDisplay);

		assertThat(result.size(), equalTo(2));
		assertTrue(result.contains(mockAssetVocabulary1));
		assertTrue(result.contains(mockAssetVocabulary2));
	}

	@Test
	public void getAvailableVocabularies_WhenOneVocabularyDoesNotHaveCategories_ThenReturnOnlyOnesWithCategories() {
		givenAssetVocabulariesExist();
		when(mockAssetVocabulary2.getCategoriesCount()).thenReturn(0);

		List<AssetVocabulary> result = categoryLocalService.getAvailableVocabularies(mockThemeDisplay);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(0), equalTo(mockAssetVocabulary1));
	}

	@Test
	public void getLongValueList_WhenNullPassed_ThenReturnEmptyList() {
		List<Long> result = categoryLocalService.getLongValueList(null);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getLongValueList_WhenNoError_ThenReturnLongValueList() {
		String[] valuesAsString = {"1", "2"};

		List<Long> result = categoryLocalService.getLongValueList(valuesAsString);

		assertThat(result.size(), equalTo(2));
		assertTrue(result.contains(1l));
		assertTrue(result.contains(2l));
	}

	@Test
	public void getAggregateCategoryLists_WhenNoError_ThenCombineCategoriesFromSelectedVocabulariesWithSelectedCategories (){
		givenAssetVocabulariesExist();

		long expectedCategoryId1 = 10l;
		long expectedCategoryId2 = 2l;

		List<AssetCategory> listOfCategories1 = new ArrayList<>();
		listOfCategories1.add(mockAssetCategory1);

		List<AssetCategory> listOfCategories2 = new ArrayList<>();
		listOfCategories2.add(mockAssetCategory2);
		listOfCategories2.add(mockAssetCategory3);

		String[] selectedVocabularies = {"1"};
		String[] selectedCategories = {"2"};

		when(mockAssetVocabulary1.getVocabularyId()).thenReturn(1l);
		when(mockAssetVocabulary1.getCategories()).thenReturn(listOfCategories1);
		when(mockAssetCategory1.getCategoryId()).thenReturn(expectedCategoryId1);

		when(mockAssetVocabulary2.getVocabularyId()).thenReturn(777l);
		when(mockAssetVocabulary2.getCategories()).thenReturn(listOfCategories2);
		when(mockAssetCategory2.getCategoryId()).thenReturn(expectedCategoryId2);
		when(mockAssetCategory3.getCategoryId()).thenReturn(5l);

		Set<Long> result = categoryLocalService.getAggregateCategoryLists(mockThemeDisplay, selectedVocabularies, selectedCategories);

		assertThat(result.size(), equalTo(2));
		assertTrue(result.contains(expectedCategoryId1));
		assertTrue(result.contains(expectedCategoryId2));

	}

	private void givenAssetVocabulariesExist() {
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockThemeDisplay.getCompanyGroupId()).thenReturn(COMPANY_GROUP_ID);

		assetVocabularyList.add(mockAssetVocabulary1);
		when(mockAssetVocabulary1.getCategoriesCount()).thenReturn(2);
		
		assetVocabularyList.add(mockAssetVocabulary2);
		when(mockAssetVocabulary2.getCategoriesCount()).thenReturn(1);

		when(mockAssetVocabularyLocalService.getGroupsVocabularies(new long[] { SCOPE_GROUP_ID, COMPANY_GROUP_ID })).thenReturn(assetVocabularyList);
	}
}
