package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldType;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.model.FilterField;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.constants.JsonFilterConfigKeys;

@Component(immediate = true, service = DDLFieldFilterService.class)
public class DDLFieldFilterService {

	public List<String> getAppliedFilterNames(String appliedFiltersValue) {
		String[] appliedFilters = appliedFiltersValue.split(StringPool.COMMA);

		return Arrays.asList(appliedFilters);
	}

	public Optional<JSONObject> getOptionJsonFromGroupAndOptionName(JSONArray filterGroupsConfig, String filterGroupName, String optionName) {
		for (int i = 0; i < filterGroupsConfig.length(); i++) {
			JSONObject filterGroup = filterGroupsConfig.getJSONObject(i);

			String currentFilterGroupName = filterGroup.getString(JsonFilterConfigKeys.FILTER_GROUP_NAME);
			if (currentFilterGroupName.equals(filterGroupName)) {

				JSONArray options = filterGroup.getJSONArray(JsonFilterConfigKeys.FILTER_OPTIONS);
				for (int y = 0; y < options.length(); y++) {
					JSONObject option = options.getJSONObject(y);

					String currentOptionName = option.getString(JsonFilterConfigKeys.FILTER_OPTION_NAME);
					if (currentOptionName.equals(optionName)) {
						return Optional.of(option);
					}
				}
			}
		}

		return Optional.empty();
	}

	public Optional<String> getSelectedFilterByGroupName(String groupName, Optional<String> appliedFilterOpt) {
		if (Validator.isNotNull(groupName) && appliedFilterOpt.isPresent()) {
			String[] selectedFilters = appliedFilterOpt.get().split(StringPool.COMMA);

			return Arrays.stream(selectedFilters).filter(f -> filterBelongsToGroupName(f, groupName)).findFirst();
		}
		return Optional.empty();
	}

	public Set<FilterField> getSupportedStructureFilterFields(List<DDMFormField> ddmFormFields) {
		return ddmFormFields.stream().filter(this::isSupportedFieldType).map(FilterField::init).collect(Collectors.toSet());
	}

	private boolean filterBelongsToGroupName(String filter, String groupName) {
		if (Validator.isNotNull(filter)) {
			String[] groupAndOptionName = filter.split(StringPool.UNDERLINE);
			return groupAndOptionName[0].equals(groupName);
		}
		return false;

	}

	private boolean isSupportedFieldType(DDMFormField ddmFormField) {
		String type = ddmFormField.getType();
		return DDMFormFieldType.DATE.equals(type)//
				|| DDMFormFieldTypeConstants.DATE.equals(type)//
				|| DDMFormFieldType.INTEGER.equals(type)//
				|| DDMFormFieldTypeConstants.NUMERIC.equals(type);
	}
}
