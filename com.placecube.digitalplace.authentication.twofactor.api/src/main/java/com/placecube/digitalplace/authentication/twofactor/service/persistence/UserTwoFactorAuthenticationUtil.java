/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.authentication.twofactor.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the user two factor authentication service. This utility wraps <code>com.placecube.digitalplace.authentication.twofactor.service.persistence.impl.UserTwoFactorAuthenticationPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTwoFactorAuthenticationPersistence
 * @generated
 */
public class UserTwoFactorAuthenticationUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(
		UserTwoFactorAuthentication userTwoFactorAuthentication) {

		getPersistence().clearCache(userTwoFactorAuthentication);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, UserTwoFactorAuthentication>
		fetchByPrimaryKeys(Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UserTwoFactorAuthentication> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UserTwoFactorAuthentication> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UserTwoFactorAuthentication> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static UserTwoFactorAuthentication update(
		UserTwoFactorAuthentication userTwoFactorAuthentication) {

		return getPersistence().update(userTwoFactorAuthentication);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static UserTwoFactorAuthentication update(
		UserTwoFactorAuthentication userTwoFactorAuthentication,
		ServiceContext serviceContext) {

		return getPersistence().update(
			userTwoFactorAuthentication, serviceContext);
	}

	/**
	 * Returns all the user two factor authentications where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the user two factor authentications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @return the range of matching user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the user two factor authentications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the user two factor authentications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication findByUuid_First(
			String uuid,
			OrderByComparator<UserTwoFactorAuthentication> orderByComparator)
		throws com.placecube.digitalplace.authentication.twofactor.exception.
			NoSuchUserTwoFactorAuthenticationException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication fetchByUuid_First(
		String uuid,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication findByUuid_Last(
			String uuid,
			OrderByComparator<UserTwoFactorAuthentication> orderByComparator)
		throws com.placecube.digitalplace.authentication.twofactor.exception.
			NoSuchUserTwoFactorAuthenticationException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication fetchByUuid_Last(
		String uuid,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the user two factor authentications before and after the current user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the current user two factor authentication
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a user two factor authentication with the primary key could not be found
	 */
	public static UserTwoFactorAuthentication[] findByUuid_PrevAndNext(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK,
			String uuid,
			OrderByComparator<UserTwoFactorAuthentication> orderByComparator)
		throws com.placecube.digitalplace.authentication.twofactor.exception.
			NoSuchUserTwoFactorAuthenticationException {

		return getPersistence().findByUuid_PrevAndNext(
			userTwoFactorAuthenticationPK, uuid, orderByComparator);
	}

	/**
	 * Removes all the user two factor authentications where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of user two factor authentications where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching user two factor authentications
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns all the user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @return the range of matching user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<UserTwoFactorAuthentication> orderByComparator)
		throws com.placecube.digitalplace.authentication.twofactor.exception.
			NoSuchUserTwoFactorAuthenticationException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<UserTwoFactorAuthentication> orderByComparator)
		throws com.placecube.digitalplace.authentication.twofactor.exception.
			NoSuchUserTwoFactorAuthenticationException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the user two factor authentications before and after the current user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the current user two factor authentication
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a user two factor authentication with the primary key could not be found
	 */
	public static UserTwoFactorAuthentication[] findByUuid_C_PrevAndNext(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK,
			String uuid, long companyId,
			OrderByComparator<UserTwoFactorAuthentication> orderByComparator)
		throws com.placecube.digitalplace.authentication.twofactor.exception.
			NoSuchUserTwoFactorAuthenticationException {

		return getPersistence().findByUuid_C_PrevAndNext(
			userTwoFactorAuthenticationPK, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the user two factor authentications where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching user two factor authentications
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Caches the user two factor authentication in the entity cache if it is enabled.
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 */
	public static void cacheResult(
		UserTwoFactorAuthentication userTwoFactorAuthentication) {

		getPersistence().cacheResult(userTwoFactorAuthentication);
	}

	/**
	 * Caches the user two factor authentications in the entity cache if it is enabled.
	 *
	 * @param userTwoFactorAuthentications the user two factor authentications
	 */
	public static void cacheResult(
		List<UserTwoFactorAuthentication> userTwoFactorAuthentications) {

		getPersistence().cacheResult(userTwoFactorAuthentications);
	}

	/**
	 * Creates a new user two factor authentication with the primary key. Does not add the user two factor authentication to the database.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key for the new user two factor authentication
	 * @return the new user two factor authentication
	 */
	public static UserTwoFactorAuthentication create(
		UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK) {

		return getPersistence().create(userTwoFactorAuthenticationPK);
	}

	/**
	 * Removes the user two factor authentication with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication that was removed
	 * @throws NoSuchUserTwoFactorAuthenticationException if a user two factor authentication with the primary key could not be found
	 */
	public static UserTwoFactorAuthentication remove(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK)
		throws com.placecube.digitalplace.authentication.twofactor.exception.
			NoSuchUserTwoFactorAuthenticationException {

		return getPersistence().remove(userTwoFactorAuthenticationPK);
	}

	public static UserTwoFactorAuthentication updateImpl(
		UserTwoFactorAuthentication userTwoFactorAuthentication) {

		return getPersistence().updateImpl(userTwoFactorAuthentication);
	}

	/**
	 * Returns the user two factor authentication with the primary key or throws a <code>NoSuchUserTwoFactorAuthenticationException</code> if it could not be found.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a user two factor authentication with the primary key could not be found
	 */
	public static UserTwoFactorAuthentication findByPrimaryKey(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK)
		throws com.placecube.digitalplace.authentication.twofactor.exception.
			NoSuchUserTwoFactorAuthenticationException {

		return getPersistence().findByPrimaryKey(userTwoFactorAuthenticationPK);
	}

	/**
	 * Returns the user two factor authentication with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication, or <code>null</code> if a user two factor authentication with the primary key could not be found
	 */
	public static UserTwoFactorAuthentication fetchByPrimaryKey(
		UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK) {

		return getPersistence().fetchByPrimaryKey(
			userTwoFactorAuthenticationPK);
	}

	/**
	 * Returns all the user two factor authentications.
	 *
	 * @return the user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the user two factor authentications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @return the range of user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findAll(
		int start, int end) {

		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the user two factor authentications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findAll(
		int start, int end,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the user two factor authentications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication> findAll(
		int start, int end,
		OrderByComparator<UserTwoFactorAuthentication> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the user two factor authentications from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of user two factor authentications.
	 *
	 * @return the number of user two factor authentications
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getCompoundPKColumnNames() {
		return getPersistence().getCompoundPKColumnNames();
	}

	public static UserTwoFactorAuthenticationPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(
		UserTwoFactorAuthenticationPersistence persistence) {

		_persistence = persistence;
	}

	private static volatile UserTwoFactorAuthenticationPersistence _persistence;

}