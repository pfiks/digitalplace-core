package com.placecube.digitalplace.user.account.service;

import com.placecube.digitalplace.user.account.model.AccountContext;

public interface AccountContextFactory {

	AccountContext createAccountContext();

}
