package com.placecube.digitalplace.user.serviceoverride.model;

import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.user.serviceoverride.constants.AuthenticationMode;

public class AuthenticationActionContext {

	private final AuthenticationMode authenticationMode;

	private final int authenticationResult;

	private final long companyId;

	private String emailAddress;

	private String screenName;

	private long userId;

	public AuthenticationActionContext(long companyId, AuthenticationMode authenticationMode, int authenticationResult) {
		this.companyId = companyId;
		this.authenticationMode = authenticationMode;
		this.authenticationResult = authenticationResult;
	}

	public AuthenticationMode getAuthenticationMode() {
		return authenticationMode;
	}

	public int getAuthenticationResult() {
		return authenticationResult;
	}

	public long getCompanyId() {
		return companyId;
	}

	public String getEmailAddress() {
		return GetterUtil.getString(emailAddress);
	}

	public String getScreenName() {
		return GetterUtil.getString(screenName);
	}

	public long getUserId() {
		return userId;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

}
