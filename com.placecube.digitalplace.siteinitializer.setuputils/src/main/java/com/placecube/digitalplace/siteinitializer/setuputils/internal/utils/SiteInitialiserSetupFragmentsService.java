package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.list.model.AssetListEntryUsage;
import com.liferay.fragment.model.FragmentEntryLink;
import com.liferay.fragment.service.FragmentEntryLinkLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = SiteInitialiserSetupFragmentsService.class)
public class SiteInitialiserSetupFragmentsService {

	@Reference
	private FragmentEntryLinkLocalService fragmentEntryLinkLocalService;

	@Reference
	private SiteInitialiserDataRetrieval siteInitialiserDataRetrieval;

	public void updateFragmentEntryLinksInLayout(long userId, Layout layout, String assetListEntryUsageKey, String placeholderValue) throws PortalException {
		if (Validator.isNotNull(layout)) {

			Optional<AssetListEntryUsage> assetListEntryUsage = siteInitialiserDataRetrieval.findAssetListEntryUsage(layout, assetListEntryUsageKey);
			if (assetListEntryUsage.isPresent()) {
				String targetCollectionUUid = assetListEntryUsage.get().getContainerKey();

				updateFragmentEntryLinksPlaceholderValues(userId, layout, placeholderValue, targetCollectionUUid);

				Layout fetchDraftLayout = layout.fetchDraftLayout();
				if (Validator.isNotNull(fetchDraftLayout)) {
					updateFragmentEntryLinksPlaceholderValues(userId, fetchDraftLayout, placeholderValue, targetCollectionUUid);
				}
			}
		}
	}

	private void updateFragmentEntryLinksPlaceholderValues(long userId, Layout layout, String placeholderValue, String targetCollectionUUid) throws PortalException {
		List<FragmentEntryLink> fragmentEntryLinksByPlid = fragmentEntryLinkLocalService.getFragmentEntryLinksByPlid(layout.getGroupId(), layout.getPlid());
		for (FragmentEntryLink fragmentEntryLink : fragmentEntryLinksByPlid) {
			String editableValues = fragmentEntryLink.getEditableValues();
			if (Validator.isNotNull(editableValues) && editableValues.contains(placeholderValue)) {
				String updatedValues = editableValues.replace(placeholderValue, targetCollectionUUid);
				fragmentEntryLinkLocalService.updateFragmentEntryLink(userId, fragmentEntryLink.getFragmentEntryLinkId(), updatedValues);
			}
		}
	}

}
