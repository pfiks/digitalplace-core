package com.placecube.digitalplace.form.service.impl.internal;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;

@Component(immediate = true, service = FormJournalArticleService.class)
public class FormJournalArticleService {

	@Reference
	private ClassNameLocalService classNameLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	public JournalFolder addJournalFolder(FormDefinition formDefinition, ServiceContext serviceContext) throws PortalException {
		return journalArticleCreationService.getOrCreateJournalFolder(formDefinition.getJournalArticleFolderName(), serviceContext);
	}

	public void addJournalArticles(FormDefinition formDefinition, JournalFolder journalFolder, ServiceContext serviceContext) throws IOException, PortalException {
		List<URL> journalArticleURLs = formDefinition.getUrlsToJournalArticles();

		for (URL journalArticleURL : journalArticleURLs) {
			String articleContent = StringUtil.read(journalArticleURL.openStream());

			String path = journalArticleURL.getPath();
			String[] tokens = path.split("/");

			String title = tokens[tokens.length - 1].substring(0, tokens[tokens.length - 1].lastIndexOf("."));
			String id = title.toUpperCase().replaceAll("\\s", "-");

			JournalArticleContext journalArticleContext = JournalArticleContext.init(id, title, articleContent);
			journalArticleContext.setJournalFolder(journalFolder);

			if (tokens.length == 4) {
				String structureKey = tokens[2];
				long classNameId = classNameLocalService.getClassNameId(JournalArticle.class);
				long companyGroupId = groupLocalService.getCompanyGroup(serviceContext.getCompanyId()).getGroupId();
				DDMStructure ddmStructure = ddmStructureLocalService.fetchStructure(companyGroupId, classNameId, structureKey);

				if (Validator.isNotNull(ddmStructure)) {
					journalArticleContext.setDDMStructure(ddmStructure);
					journalArticleCreationService.getOrCreateArticle(journalArticleContext, serviceContext);
				}
			} else {
				journalArticleCreationService.getOrCreateArticle(journalArticleContext, serviceContext);
			}
		}
	}
}