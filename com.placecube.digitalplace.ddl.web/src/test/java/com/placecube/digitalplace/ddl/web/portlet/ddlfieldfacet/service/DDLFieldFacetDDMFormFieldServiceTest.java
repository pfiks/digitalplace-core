package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldType;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;

@RunWith(MockitoJUnitRunner.class)
public class DDLFieldFacetDDMFormFieldServiceTest {

	private static final Locale LOCALE = Locale.UK;
	private static final String FIELD_NAME = "fieldName";

	@InjectMocks
	private DDLFieldFacetDDMFormFieldService ddlFieldFacetDDMFormFieldService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDMFormField mockDDMFormField;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue;

	@Mock
	private Value mockValue;

	@Test
	public void getDDMFormFieldStringValue_WhenNoErrors_ThenReturnsFirstLocalizedFormFieldValue() throws Exception {
		final String localizedValue = "value";

		when(mockDDMFormFieldValue.getValue()).thenReturn(mockValue);
		when(mockValue.getString(LOCALE)).thenReturn(localizedValue);
		when(mockDDLRecord.getDDMFormFieldValues(FIELD_NAME)).thenReturn(Arrays.asList(new DDMFormFieldValue[] { mockDDMFormFieldValue }));

		String result = ddlFieldFacetDDMFormFieldService.getDDMFormFieldStringValue(mockDDLRecord, FIELD_NAME, LOCALE);

		assertThat(result, equalTo(localizedValue));
	}

	@Test
	public void getDDMFormFieldStringValue_WhenFormValuesRetrievalFails_ThenReturnsEmtpyString() throws Exception {
		when(mockDDLRecord.getDDMFormFieldValues(FIELD_NAME)).thenThrow(new PortalException());

		String result = ddlFieldFacetDDMFormFieldService.getDDMFormFieldStringValue(mockDDLRecord, FIELD_NAME, LOCALE);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getDDMFormFieldStringValue_WhenDDMFormValuesListIsNull_ThenReturnsEmtpyString() throws Exception {
		when(mockDDLRecord.getDDMFormFieldValues(FIELD_NAME)).thenReturn(null);

		String result = ddlFieldFacetDDMFormFieldService.getDDMFormFieldStringValue(mockDDLRecord, FIELD_NAME, LOCALE);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getDDMFormFieldStringValue_WhenDDMFormValuesListIsEmpty_ThenReturnsEmtpyString() throws Exception {
		when(mockDDLRecord.getDDMFormFieldValues(FIELD_NAME)).thenReturn(new ArrayList<>());

		String result = ddlFieldFacetDDMFormFieldService.getDDMFormFieldStringValue(mockDDLRecord, FIELD_NAME, LOCALE);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void fieldTypeAllowsOptions_WhenDDMFormFieldIsRepeatable_ThenReturnsFalse() {
		when(mockDDMFormField.isRepeatable()).thenReturn(true);

		assertThat(ddlFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField), equalTo(false));
	}

	@Test
	public void fieldTypeAllowsOptions_WhenDDMFormFieldIsNotRepeatableAndFormFieldTypeIsCheckbox_ThenReturnsTrue() {
		when(mockDDMFormField.isRepeatable()).thenReturn(false);
		when(mockDDMFormField.getType()).thenReturn(DDMFormFieldType.CHECKBOX);

		assertThat(ddlFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField), equalTo(true));
	}

	@Test
	public void fieldTypeAllowsOptions_WhenDDMFormFieldIsNotRepeatableAndFormFieldTypeIsRadio_ThenReturnsTrue() {
		when(mockDDMFormField.isRepeatable()).thenReturn(false);
		when(mockDDMFormField.getType()).thenReturn(DDMFormFieldType.RADIO);

		assertThat(ddlFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField), equalTo(true));
	}

	@Test
	public void fieldTypeAllowsOptions_WhenDDMFormFieldIsNotRepeatableAndFormFieldTypeIsSelect_ThenReturnsTrue() {
		when(mockDDMFormField.isRepeatable()).thenReturn(false);
		when(mockDDMFormField.getType()).thenReturn(DDMFormFieldType.SELECT);

		assertThat(ddlFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField), equalTo(true));
	}

	@Test
	public void fieldTypeAllowsOptions_WhenDDMFormFieldIsNotRepeatableAndFormFieldTypeIsNotCheckboxRadioOrSelect_ThenReturnsFalse() {
		when(mockDDMFormField.isRepeatable()).thenReturn(false);
		when(mockDDMFormField.getType()).thenReturn(DDMFormFieldType.TEXT);

		assertThat(ddlFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(mockDDMFormField), equalTo(false));
	}
}
