package com.placecube.digitalplace.user.account.web.util;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

public final class AccountPermissionUtils {

	private AccountPermissionUtils() {

	}

	public static boolean hasEditPermission(PortletRequest portletRequest) {
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		return themeDisplay.getPermissionChecker().hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW);
	}

}
