package com.placecube.digitalplace.search.shared.service;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;

import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;

public interface SharedSearchRequestService {

	void addSharedActiveFilter(PortletRequest portletRequest, String filterName, String filterValue, String filterLabel, String filterColour);

	void addSharedActiveFilter(ThemeDisplay themeDisplay, String attributeName, String attributeValue, String filterLabel, String filterColour);

	void clearSharedSearchSession(PortletRequest portletRequest);

	void removeSharedActiveFilterValue(PortletRequest portletRequest, String filterName, String filterValue);

	void removeSharedSearchSessionAttribute(PortletRequest portletRequest, String attributeName);

	/**
	 * Executes the shared search once. SharedSearchContributor are applied
	 * before the search is executed
	 *
	 * @param renderRequest the request
	 * @return the search shared response
	 * @throws SearchException any error in executing the search
	 */
	SharedSearchResponse search(RenderRequest renderRequest) throws SearchException;

	/**
	 * If a value for the specified redirectParameterName is found as request
	 * parameter, then a redirect to that value is executed. If no value is
	 * found then no redirect is performed
	 *
	 * @param actionRequest the action request
	 * @param actionResponse the action response
	 * @param redirectParameterName the parameter name to use as redirect
	 * @throws IOException if any error while performing the redirect
	 */
	void sendRedirectFromParameter(ActionRequest actionRequest, ActionResponse actionResponse, String redirectParameterName) throws IOException;

	void setSharedSearchSessionSingleValuedAttribute(PortletRequest portletRequest, String attributeName, String attributeValue);

}
