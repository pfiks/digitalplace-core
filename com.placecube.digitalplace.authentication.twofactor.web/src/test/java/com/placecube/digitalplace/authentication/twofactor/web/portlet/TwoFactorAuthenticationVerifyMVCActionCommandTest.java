package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SessionErrors.class, PortalUtil.class, SessionMessages.class })

public class TwoFactorAuthenticationVerifyMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private TwoFactorRequestUtils mockTwoFactorRequestUtils;

	@Mock
	private TwoFactorService mockTwoFactorService;

	@Mock
	private User mockUser;

	@InjectMocks
	private TwoFactorAuthenticationVerifyMVCActionCommand twoFactorAuthenticationVerifyMVCActionCommand;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, SessionErrors.class, PortalUtil.class, SessionMessages.class, ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenUserFound_ThenChecksTheCodeValidity() throws Exception {
		String twoFactorAuthCode = "twoFactorAuthCodeValue";
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockActionRequest, mockHttpServletRequest)).thenReturn(Optional.of(mockUser));
		when(ParamUtil.getString(mockActionRequest, "twoFactorAuthCode")).thenReturn(twoFactorAuthCode);

		twoFactorAuthenticationVerifyMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockTwoFactorService, times(1)).isValidTwoFactorCode(mockUser, twoFactorAuthCode, mockHttpServletRequest);
	}

	@Test
	public void doProcessAction_WhenUserFoundAndCodeIsInvalid_ThenAddsSessionErrorAndHidesDefaultErrorMessage() throws Exception {
		String portletId = "myPortletId";
		String twoFactorAuthCode = "twoFactorAuthCodeValue";
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockActionRequest, mockHttpServletRequest)).thenReturn(Optional.of(mockUser));
		when(PortalUtil.getPortletId(mockActionRequest)).thenReturn(portletId);
		when(ParamUtil.getString(mockActionRequest, "twoFactorAuthCode")).thenReturn(twoFactorAuthCode);
		when(mockTwoFactorService.isValidTwoFactorCode(mockUser, twoFactorAuthCode, mockHttpServletRequest)).thenReturn(false);

		twoFactorAuthenticationVerifyMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "twoFactorVerificationFailed");

		verifyStatic(SessionMessages.class, times(1));
		SessionMessages.add(mockActionRequest, portletId + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

		verify(mockActionResponse, never()).getRenderParameters();
	}

	@Test
	public void doProcessAction_WhenUserFoundAndCodeIsValid_ThenDoesNotAddAnyRenderParameter() throws Exception {
		String twoFactorAuthCode = "twoFactorAuthCodeValue";
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockActionRequest, mockHttpServletRequest)).thenReturn(Optional.of(mockUser));
		when(ParamUtil.getString(mockActionRequest, "twoFactorAuthCode")).thenReturn(twoFactorAuthCode);
		when(mockTwoFactorService.isValidTwoFactorCode(mockUser, twoFactorAuthCode, mockHttpServletRequest)).thenReturn(true);

		twoFactorAuthenticationVerifyMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionResponse, never()).getRenderParameters();
	}

	@Test
	public void doProcessAction_WhenUserNotFound_ThenAddsSessionErrorAndHideDefaultErrorMessage() throws Exception {
		String portletId = "myPortletId";
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockActionRequest, mockHttpServletRequest)).thenReturn(Optional.empty());
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(PortalUtil.getPortletId(mockActionRequest)).thenReturn(portletId);

		twoFactorAuthenticationVerifyMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "sessionExpired");

		verifyStatic(SessionMessages.class, times(1));
		SessionMessages.add(mockActionRequest, portletId + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
	}

	@Test
	public void doProcessAction_WhenUserNotFound_ThenConfiguresTheErrorRedirectParameters() throws Exception {
		String portletId = "myPortletId";
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockActionRequest, mockHttpServletRequest)).thenReturn(Optional.empty());
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(PortalUtil.getPortletId(mockActionRequest)).thenReturn(portletId);

		twoFactorAuthenticationVerifyMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue("mvcPath", "/two-factor-authentication/error.jsp");
		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", "/");
	}

}
