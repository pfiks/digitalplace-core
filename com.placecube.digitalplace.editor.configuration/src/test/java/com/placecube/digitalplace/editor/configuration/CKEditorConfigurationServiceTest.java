package com.placecube.digitalplace.editor.configuration;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;

@RunWith(PowerMockRunner.class)
public class CKEditorConfigurationServiceTest extends Mockito {

	@Mock
	private CKEditorGuidelineHelpGroupConfiguration mockCKEditorGuidelineHelpGroupConfiguration;

	@Mock
	private JSONObject mockConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private JSONFactory mockJsonFactory;

	@InjectMocks
	CKEditorConfigurationService cKEditorConfigurationService;

	@Mock
	CKEditorCompanyConfiguration mockCKEditorCompanyConfiguration;

	@Test
	public void getPluginAndRolesJsonConfiguration_WhenErrorGettingConfigurations_ThenConfigurationsAreReturned() throws ConfigurationException, JSONException {
		long companyId = 123L;
		String configuration = null;

		when(mockConfigurationProvider.getCompanyConfiguration(CKEditorCompanyConfiguration.class, companyId)).thenReturn(mockCKEditorCompanyConfiguration);
		when(mockCKEditorCompanyConfiguration.pluginAndRolesJsonConfiguration()).thenReturn(configuration);

		when(mockJsonFactory.createJSONObject(configuration)).thenThrow(new RuntimeException());

		Optional<JSONObject> pluginAndRolesJsonConfiguration = cKEditorConfigurationService.getPluginAndRolesJsonConfiguration(companyId);

		assertThat(pluginAndRolesJsonConfiguration, equalTo(Optional.empty()));
	}

	@Test
	public void getPluginAndRolesJsonConfiguration_WhenNoErrors_ThenConfigurationsAreReturned() throws ConfigurationException, JSONException {
		long companyId = 123L;
		String configuration = "test";

		when(mockConfigurationProvider.getCompanyConfiguration(CKEditorCompanyConfiguration.class, companyId)).thenReturn(mockCKEditorCompanyConfiguration);
		when(mockCKEditorCompanyConfiguration.pluginAndRolesJsonConfiguration()).thenReturn(configuration);

		when(mockJsonFactory.createJSONObject(configuration)).thenReturn(mockConfiguration);

		Optional<JSONObject> pluginAndRolesJsonConfiguration = cKEditorConfigurationService.getPluginAndRolesJsonConfiguration(companyId);

		assertThat(pluginAndRolesJsonConfiguration.get(), equalTo(mockConfiguration));
	}
}
