<#assign ddmFormFieldTypeUtil = digitalplace_ddmFormFieldTypeUtil />
<#assign httpServletRequest = digitalplace_serviceContext.getRequest()/>
<#assign userLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.UserLocalService")>

<#if httpServletRequest.getSession().getAttribute("equinox.http.dynamic-data-mapping-form-webddmFormInstanceRecord") ?has_content>
	<#assign ddmFormInstanceRecord = httpServletRequest.getSession().getAttribute("equinox.http.dynamic-data-mapping-form-webddmFormInstanceRecord") />
	<#assign user = userLocalService.getUserById(ddmFormInstanceRecord.getUserId())/>
	<#assign ddmFormValues = httpServletRequest.getSession().getAttribute("equinox.http.dynamic-data-mapping-form-webddmFormInstanceRecord").getDDMFormValues() />
	<#assign ddmFormFieldValues = ddmFormValues.getDDMFormFieldValues() />
<#else>
	<#assign formInstanceRecordId = getterUtil.getLong(httpServletRequest.getSession().getAttribute("equinox.http.dynamic-data-mapping-form-webformInstanceRecordId"), 0) />
	<#if (formInstanceRecordId > 0) >
		<#assign ddmFormInstanceRecordLocalService = serviceLocator.findService("com.liferay.dynamic.data.mapping.service.DDMFormInstanceRecordLocalService") />
		<#assign ddmFormInstanceRecord = ddmFormInstanceRecordLocalService.fetchFormInstanceRecord(formInstanceRecordId) />
		<#assign user = userLocalService.getUserById(ddmFormInstanceRecord.getUserId())/>
		<#assign ddmFormValues = ddmFormInstanceRecord.getDDMFormValues() />
		<#assign ddmFormFieldValues = ddmFormValues.getDDMFormFieldValues() />
	</#if>
</#if>

<#assign bodyText = Body.getData()/>

<#if user ??>
	<#-- do your thing with the user object. Example -->
	<#-- <#assign bodyText = bodyText ?replace("$USER_FULL_NAME$", user.getFullName()) /> -->
</#if>

<#if ddmFormFieldValues ?has_content>
	<#assign formFieldReferencesValueMap = ddmFormFieldTypeUtil.getFormFieldReferenceValueMap(ddmFormValues) />
	<#list formFieldReferencesValueMap as fieldReference, fieldValue>
		<#if fieldValue ?? >
			<#assign bodyText = bodyText ?replace("$FORM_"+fieldReference+"$", fieldValue) />
		</#if>
	</#list>
</#if>
${bodyText}