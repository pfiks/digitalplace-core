package com.placecube.digitalplace.pageimportadmin.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.pageimportadmin.model.PageImportModel;
import com.placecube.digitalplace.pageimportadmin.model.RowImportModel;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class FileParserServiceTest extends PowerMockito {

	@InjectMocks
	private FileParserService fileParserService;

	private static final String TITLE = "titleValue";
	private static final String URL = "urlValue";
	private static final String PARENT_LAYOUT_URL = "parentLayoutURL";
	private static final String PAGE_TEMPLATE_NAME = "pageTemplateName";
	private static final String STRUCTURE_NAME = "structureName";
	private static final String CATEGORIES = "category1A,category1B;category2;category3";
	private static final long GROUP_ID = 20;
	private static final int ROW_INDEX = 123456;

	@Mock
	private ModelFactoryBuilder mockModelFactoryBuilder;

	@Mock
	private Sheet mockSheet;

	@Mock
	private Row mockRow;

	@Mock
	private Cell mockCell;

	@Mock
	private Cell mockCellTitle;

	@Mock
	private Cell mockCellUrl;

	@Mock
	private Cell mockCellCategories;

	@Mock
	private Cell mockCellParentUrl;

	@Mock
	private Cell mockCellPageTemplate;

	@Mock
	private Cell mockCellWebContentStructure;

	@Mock
	private WebContentImportModel mockWebContentImportModel;

	@Mock
	private PageImportModel mockPageImportModel;

	@Mock
	private RowImportModel mockRowImportModel;


	@Test
	@Parameters({ "true", "false" })
	public void getRowContent_WhenRowIsNull_ThenReturnsEmptyRowImportModelWithError(boolean privateLayout) {
		when(mockSheet.getRow(ROW_INDEX)).thenReturn(null);
		when(mockModelFactoryBuilder.getEmptyRowImportModel(ROW_INDEX, GROUP_ID)).thenReturn(mockRowImportModel);

		RowImportModel result = fileParserService.getRowContent(mockSheet, ROW_INDEX, GROUP_ID, privateLayout);

		verify(mockModelFactoryBuilder, times(1)).getEmptyRowImportModel(ROW_INDEX, GROUP_ID);
		verify(mockRowImportModel, times(1)).addErrorMessage("Unable to parse empty row.");
	}

	@Test
	@Parameters({ "true", "false" })
	public void getRowContent_WhenImportAsWebContentIsTrue_ThenReturnsTheWebContentImportModel(boolean privateLayout) {
		when(mockSheet.getRow(ROW_INDEX)).thenReturn(mockRow);
		mockTitle(TITLE);
		mockUrl(URL);
		mockParentUrl(PARENT_LAYOUT_URL);
		mockPageTemplateName(PAGE_TEMPLATE_NAME);
		mockStructureName(STRUCTURE_NAME);
		List<String> categories = mockCategories();
		when(mockModelFactoryBuilder.importAsWebContent(PAGE_TEMPLATE_NAME, STRUCTURE_NAME)).thenReturn(true);
		when(mockModelFactoryBuilder.getWebContentImportModel(ROW_INDEX, GROUP_ID, TITLE, URL, categories, STRUCTURE_NAME)).thenReturn(mockWebContentImportModel);

		RowImportModel result = fileParserService.getRowContent(mockSheet, ROW_INDEX, GROUP_ID, privateLayout);

		assertThat(result, sameInstance(mockWebContentImportModel));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getRowContent_WhenImportAsWebContentIsFalse_ThenReturnsThePageImportModel(boolean privateLayout) {
		when(mockSheet.getRow(ROW_INDEX)).thenReturn(mockRow);
		mockTitle(TITLE);
		mockUrl(URL);
		mockParentUrl(PARENT_LAYOUT_URL);
		mockPageTemplateName(PAGE_TEMPLATE_NAME);
		mockStructureName(STRUCTURE_NAME);
		List<String> categories = mockCategories();
		when(mockModelFactoryBuilder.importAsWebContent(PAGE_TEMPLATE_NAME, STRUCTURE_NAME)).thenReturn(false);
		when(mockModelFactoryBuilder.getPageImportModel(ROW_INDEX, GROUP_ID, TITLE, URL, categories, privateLayout, PARENT_LAYOUT_URL, PAGE_TEMPLATE_NAME)).thenReturn(mockPageImportModel);

		RowImportModel result = fileParserService.getRowContent(mockSheet, ROW_INDEX, GROUP_ID, privateLayout);

		assertThat(result, sameInstance(mockPageImportModel));
	}

	@Test
	@Parameters({ "true|true", "true|false", "false|false", "false|true" })
	public void getRowContent_WhenCellsAreNull_ThenReturnsTheImportModelWithEmptyValues(boolean privateLayout, boolean importAsWebContent) {
		RowImportModel expected = getExpectedModel(privateLayout, importAsWebContent);

		RowImportModel result = fileParserService.getRowContent(mockSheet, ROW_INDEX, GROUP_ID, privateLayout);

		assertThat(result, sameInstance(expected));
	}

	private RowImportModel getExpectedModel(boolean privateLayout, boolean importAsWebContent) {
		when(mockSheet.getRow(ROW_INDEX)).thenReturn(mockRow);
		when(mockModelFactoryBuilder.importAsWebContent(StringPool.BLANK, StringPool.BLANK)).thenReturn(importAsWebContent);
		mockTitle(StringPool.BLANK);
		mockUrl(StringPool.BLANK);
		mockParentUrl(StringPool.BLANK);
		mockPageTemplateName(StringPool.BLANK);
		mockStructureName(StringPool.BLANK);
		if (importAsWebContent) {
			when(mockModelFactoryBuilder.getWebContentImportModel(ROW_INDEX, GROUP_ID, StringPool.BLANK, StringPool.BLANK, Collections.emptyList(), StringPool.BLANK))
					.thenReturn(mockWebContentImportModel);
			return mockWebContentImportModel;
		} else {
			when(mockModelFactoryBuilder.getPageImportModel(ROW_INDEX, GROUP_ID, StringPool.BLANK, StringPool.BLANK, Collections.emptyList(), privateLayout, StringPool.BLANK, StringPool.BLANK))
					.thenReturn(mockPageImportModel);
			return mockPageImportModel;
		}
	}

	private List<String> mockCategories() {
		List<String> categories = new ArrayList<>();
		categories.add("category1A,category1B");
		categories.add("category2");
		categories.add("category3");
		when(mockRow.getCell(FileParserService.ROW_INDEX_CATEGORIES)).thenReturn(mockCellCategories);
		when(mockModelFactoryBuilder.getStringField(mockRow, FileParserService.ROW_INDEX_CATEGORIES))
		.thenReturn(CATEGORIES);
		return categories;
	}


	private void mockStructureName(String expected) {
		when(mockRow.getCell(FileParserService.ROW_INDEX_WEBCONTENT_STRUCTURE_NAME)).thenReturn(mockCellTitle);
		when(mockModelFactoryBuilder.getStringField(mockRow, FileParserService.ROW_INDEX_WEBCONTENT_STRUCTURE_NAME))
		.thenReturn(expected);
	}

	private void mockPageTemplateName(String expected) {
		when(mockRow.getCell(FileParserService.ROW_INDEX_PAGE_TEMPLATE_NAME)).thenReturn(mockCellTitle);
		when(mockModelFactoryBuilder.getStringField(mockRow, FileParserService.ROW_INDEX_PAGE_TEMPLATE_NAME))
		.thenReturn(expected);
	}

	private void mockParentUrl(String expected) {
		when(mockRow.getCell(FileParserService.ROW_INDEX_PARENT_PAGE_URL)).thenReturn(mockCellTitle);
		when(mockModelFactoryBuilder.getStringField(mockRow, FileParserService.ROW_INDEX_PARENT_PAGE_URL))
		.thenReturn(expected);
	}

	private void mockUrl(String expected) {
		when(mockRow.getCell(FileParserService.ROW_INDEX_URL)).thenReturn(mockCellTitle);
		when(mockModelFactoryBuilder.getStringField(mockRow, FileParserService.ROW_INDEX_URL))
		.thenReturn(expected);
	}

	private void mockTitle(String expected) {
		when(mockRow.getCell(FileParserService.ROW_INDEX_TITLE)).thenReturn(mockCellTitle);
		when(mockModelFactoryBuilder.getStringField(mockRow, FileParserService.ROW_INDEX_TITLE))
		.thenReturn(expected);
	}

}
