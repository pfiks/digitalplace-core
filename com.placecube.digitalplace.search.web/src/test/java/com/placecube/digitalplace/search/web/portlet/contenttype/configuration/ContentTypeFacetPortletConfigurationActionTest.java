package com.placecube.digitalplace.search.web.portlet.contenttype.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.web.portlet.contenttype.service.ContentTypeSearchService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class ContentTypeFacetPortletConfigurationActionTest extends PowerMockito {

	@InjectMocks
	private ContentTypeFacetPortletConfigurationAction contentTypeFacetPortletConfigurationAction;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ContentTypeFacetPortletInstanceConfiguration mockContentTypeFacetPortletInstanceConfiguration;

	@Mock
	private ContentTypeSearchService mockContentTypeSearchService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	@Parameters({ "true", "false" })
	public void include_WhenNoError_ThenSetsTheFilterColourAndJoinFiltersWithTheSameNameAsRequestAttributes(boolean joinFiltersWithTheSameName) throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String filterColourValue = "filterColourValue";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockContentTypeSearchService.getConfiguration(mockThemeDisplay)).thenReturn(mockContentTypeFacetPortletInstanceConfiguration);
		when(mockContentTypeFacetPortletInstanceConfiguration.filterColour()).thenReturn(filterColourValue);
		when(mockContentTypeFacetPortletInstanceConfiguration.joinFiltersWithTheSameName()).thenReturn(joinFiltersWithTheSameName);

		contentTypeFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("filterColour", filterColourValue);
		verify(mockHttpServletRequest, times(1)).setAttribute("joinFiltersWithTheSameName", joinFiltersWithTheSameName);
	}

	@Test
	@Parameters({ "true", "false" })
	public void processAction_WhenNoError_ThenSavesFilterColourAndJoinFiltersWithTheSameNameAsPreferences(boolean joinFiltersWithTheSameName) throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String filterColourValue = "colorVal";
		when(ParamUtil.getString(mockActionRequest, "filterColour", StringPool.BLANK)).thenReturn(filterColourValue);
		when(ParamUtil.getBoolean(mockActionRequest, "joinFiltersWithTheSameName")).thenReturn(joinFiltersWithTheSameName);
		contentTypeFacetPortletConfigurationAction = spy(new ContentTypeFacetPortletConfigurationAction());

		contentTypeFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(contentTypeFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "filterColour", filterColourValue);
		verify(contentTypeFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "joinFiltersWithTheSameName", String.valueOf(joinFiltersWithTheSameName));
	}

}
