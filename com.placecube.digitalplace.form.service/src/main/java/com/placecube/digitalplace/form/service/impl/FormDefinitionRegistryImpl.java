package com.placecube.digitalplace.form.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;

import com.placecube.digitalplace.form.exception.FormDefinitionException;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.digitalplace.form.service.FormDefinitionRegistry;

@Component(immediate = true, service = FormDefinitionRegistry.class)
public class FormDefinitionRegistryImpl implements FormDefinitionRegistry {

	private Map<String, FormDefinition> formDefinitionMap = new HashMap<>();

	@Override
	public FormDefinition getFormDefinitionByKey(String key) throws FormDefinitionException {
		FormDefinition formDefinition = formDefinitionMap.get(key);
		if (formDefinition != null) {
			return formDefinition;
		} else {
			throw new FormDefinitionException("No definition form found for key:" + key);
		}

	}

	@Override
	public Map<String, FormDefinition> getFormNamesAndFormDefinitions() {
		return formDefinitionMap;
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC, policyOption = ReferencePolicyOption.GREEDY)
	protected void setFormDefinition(FormDefinition formDefinition) {

		formDefinitionMap.put(formDefinition.getKey(), formDefinition);

	}

	protected void unsetFormDefinition(FormDefinition formDefinition) {

		formDefinitionMap.remove(formDefinition.getKey());

	}

}
