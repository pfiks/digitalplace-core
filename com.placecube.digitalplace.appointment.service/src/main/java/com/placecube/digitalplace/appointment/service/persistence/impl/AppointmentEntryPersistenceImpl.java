/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.appointment.exception.NoSuchAppointmentEntryException;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.model.AppointmentEntryTable;
import com.placecube.digitalplace.appointment.model.impl.AppointmentEntryImpl;
import com.placecube.digitalplace.appointment.model.impl.AppointmentEntryModelImpl;
import com.placecube.digitalplace.appointment.service.persistence.AppointmentEntryPersistence;
import com.placecube.digitalplace.appointment.service.persistence.AppointmentEntryUtil;
import com.placecube.digitalplace.appointment.service.persistence.impl.constants.Placecube_AppointmentPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the appointment entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = AppointmentEntryPersistence.class)
public class AppointmentEntryPersistenceImpl
	extends BasePersistenceImpl<AppointmentEntry>
	implements AppointmentEntryPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>AppointmentEntryUtil</code> to access the appointment entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		AppointmentEntryImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the appointment entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the appointment entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<AppointmentEntry> list = null;

		if (useFinderCache) {
			list = (List<AppointmentEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (AppointmentEntry appointmentEntry : list) {
					if (!uuid.equals(appointmentEntry.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_APPOINTMENTENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AppointmentEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<AppointmentEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry findByUuid_First(
			String uuid, OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry = fetchByUuid_First(
			uuid, orderByComparator);

		if (appointmentEntry != null) {
			return appointmentEntry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchAppointmentEntryException(sb.toString());
	}

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry fetchByUuid_First(
		String uuid, OrderByComparator<AppointmentEntry> orderByComparator) {

		List<AppointmentEntry> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry findByUuid_Last(
			String uuid, OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry = fetchByUuid_Last(
			uuid, orderByComparator);

		if (appointmentEntry != null) {
			return appointmentEntry;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchAppointmentEntryException(sb.toString());
	}

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry fetchByUuid_Last(
		String uuid, OrderByComparator<AppointmentEntry> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<AppointmentEntry> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	@Override
	public AppointmentEntry[] findByUuid_PrevAndNext(
			long appointmentEntryId, String uuid,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		uuid = Objects.toString(uuid, "");

		AppointmentEntry appointmentEntry = findByPrimaryKey(
			appointmentEntryId);

		Session session = null;

		try {
			session = openSession();

			AppointmentEntry[] array = new AppointmentEntryImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, appointmentEntry, uuid, orderByComparator, true);

			array[1] = appointmentEntry;

			array[2] = getByUuid_PrevAndNext(
				session, appointmentEntry, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected AppointmentEntry getByUuid_PrevAndNext(
		Session session, AppointmentEntry appointmentEntry, String uuid,
		OrderByComparator<AppointmentEntry> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_APPOINTMENTENTRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AppointmentEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						appointmentEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<AppointmentEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the appointment entries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (AppointmentEntry appointmentEntry :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(appointmentEntry);
		}
	}

	/**
	 * Returns the number of appointment entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching appointment entries
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_APPOINTMENTENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"appointmentEntry.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(appointmentEntry.uuid IS NULL OR appointmentEntry.uuid = '')";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<AppointmentEntry> list = null;

		if (useFinderCache) {
			list = (List<AppointmentEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (AppointmentEntry appointmentEntry : list) {
					if (!uuid.equals(appointmentEntry.getUuid()) ||
						(companyId != appointmentEntry.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_APPOINTMENTENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AppointmentEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<AppointmentEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (appointmentEntry != null) {
			return appointmentEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchAppointmentEntryException(sb.toString());
	}

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		List<AppointmentEntry> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (appointmentEntry != null) {
			return appointmentEntry;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchAppointmentEntryException(sb.toString());
	}

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<AppointmentEntry> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	@Override
	public AppointmentEntry[] findByUuid_C_PrevAndNext(
			long appointmentEntryId, String uuid, long companyId,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		uuid = Objects.toString(uuid, "");

		AppointmentEntry appointmentEntry = findByPrimaryKey(
			appointmentEntryId);

		Session session = null;

		try {
			session = openSession();

			AppointmentEntry[] array = new AppointmentEntryImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, appointmentEntry, uuid, companyId, orderByComparator,
				true);

			array[1] = appointmentEntry;

			array[2] = getByUuid_C_PrevAndNext(
				session, appointmentEntry, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected AppointmentEntry getByUuid_C_PrevAndNext(
		Session session, AppointmentEntry appointmentEntry, String uuid,
		long companyId, OrderByComparator<AppointmentEntry> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_APPOINTMENTENTRY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AppointmentEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						appointmentEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<AppointmentEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the appointment entries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (AppointmentEntry appointmentEntry :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(appointmentEntry);
		}
	}

	/**
	 * Returns the number of appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching appointment entries
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_APPOINTMENTENTRY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"appointmentEntry.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(appointmentEntry.uuid IS NULL OR appointmentEntry.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"appointmentEntry.companyId = ?";

	private FinderPath
		_finderPathWithPaginationFindByCompanyId_EntryClassPK_EntryClassNameId;
	private FinderPath
		_finderPathWithoutPaginationFindByCompanyId_EntryClassPK_EntryClassNameId;
	private FinderPath
		_finderPathCountByCompanyId_EntryClassPK_EntryClassNameId;

	/**
	 * Returns all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @return the matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId) {

		return findByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId, int start,
		int end) {

		return findByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId, int start,
		int end, OrderByComparator<AppointmentEntry> orderByComparator) {

		return findByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry> findByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId, int start,
		int end, OrderByComparator<AppointmentEntry> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByCompanyId_EntryClassPK_EntryClassNameId;
				finderArgs = new Object[] {
					companyId, entryClassPK, entryClassNameId
				};
			}
		}
		else if (useFinderCache) {
			finderPath =
				_finderPathWithPaginationFindByCompanyId_EntryClassPK_EntryClassNameId;
			finderArgs = new Object[] {
				companyId, entryClassPK, entryClassNameId, start, end,
				orderByComparator
			};
		}

		List<AppointmentEntry> list = null;

		if (useFinderCache) {
			list = (List<AppointmentEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (AppointmentEntry appointmentEntry : list) {
					if ((companyId != appointmentEntry.getCompanyId()) ||
						(entryClassPK != appointmentEntry.getEntryClassPK()) ||
						(entryClassNameId !=
							appointmentEntry.getEntryClassNameId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					5 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(5);
			}

			sb.append(_SQL_SELECT_APPOINTMENTENTRY_WHERE);

			sb.append(
				_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_COMPANYID_2);

			sb.append(
				_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_ENTRYCLASSPK_2);

			sb.append(
				_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_ENTRYCLASSNAMEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AppointmentEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(entryClassPK);

				queryPos.add(entryClassNameId);

				list = (List<AppointmentEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry findByCompanyId_EntryClassPK_EntryClassNameId_First(
			long companyId, long entryClassPK, long entryClassNameId,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry =
			fetchByCompanyId_EntryClassPK_EntryClassNameId_First(
				companyId, entryClassPK, entryClassNameId, orderByComparator);

		if (appointmentEntry != null) {
			return appointmentEntry;
		}

		StringBundler sb = new StringBundler(8);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", entryClassPK=");
		sb.append(entryClassPK);

		sb.append(", entryClassNameId=");
		sb.append(entryClassNameId);

		sb.append("}");

		throw new NoSuchAppointmentEntryException(sb.toString());
	}

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry
		fetchByCompanyId_EntryClassPK_EntryClassNameId_First(
			long companyId, long entryClassPK, long entryClassNameId,
			OrderByComparator<AppointmentEntry> orderByComparator) {

		List<AppointmentEntry> list =
			findByCompanyId_EntryClassPK_EntryClassNameId(
				companyId, entryClassPK, entryClassNameId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry findByCompanyId_EntryClassPK_EntryClassNameId_Last(
			long companyId, long entryClassPK, long entryClassNameId,
			OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry =
			fetchByCompanyId_EntryClassPK_EntryClassNameId_Last(
				companyId, entryClassPK, entryClassNameId, orderByComparator);

		if (appointmentEntry != null) {
			return appointmentEntry;
		}

		StringBundler sb = new StringBundler(8);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", entryClassPK=");
		sb.append(entryClassPK);

		sb.append(", entryClassNameId=");
		sb.append(entryClassNameId);

		sb.append("}");

		throw new NoSuchAppointmentEntryException(sb.toString());
	}

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry fetchByCompanyId_EntryClassPK_EntryClassNameId_Last(
		long companyId, long entryClassPK, long entryClassNameId,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		int count = countByCompanyId_EntryClassPK_EntryClassNameId(
			companyId, entryClassPK, entryClassNameId);

		if (count == 0) {
			return null;
		}

		List<AppointmentEntry> list =
			findByCompanyId_EntryClassPK_EntryClassNameId(
				companyId, entryClassPK, entryClassNameId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	@Override
	public AppointmentEntry[]
			findByCompanyId_EntryClassPK_EntryClassNameId_PrevAndNext(
				long appointmentEntryId, long companyId, long entryClassPK,
				long entryClassNameId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry = findByPrimaryKey(
			appointmentEntryId);

		Session session = null;

		try {
			session = openSession();

			AppointmentEntry[] array = new AppointmentEntryImpl[3];

			array[0] = getByCompanyId_EntryClassPK_EntryClassNameId_PrevAndNext(
				session, appointmentEntry, companyId, entryClassPK,
				entryClassNameId, orderByComparator, true);

			array[1] = appointmentEntry;

			array[2] = getByCompanyId_EntryClassPK_EntryClassNameId_PrevAndNext(
				session, appointmentEntry, companyId, entryClassPK,
				entryClassNameId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected AppointmentEntry
		getByCompanyId_EntryClassPK_EntryClassNameId_PrevAndNext(
			Session session, AppointmentEntry appointmentEntry, long companyId,
			long entryClassPK, long entryClassNameId,
			OrderByComparator<AppointmentEntry> orderByComparator,
			boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				6 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(5);
		}

		sb.append(_SQL_SELECT_APPOINTMENTENTRY_WHERE);

		sb.append(
			_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_COMPANYID_2);

		sb.append(
			_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_ENTRYCLASSPK_2);

		sb.append(
			_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_ENTRYCLASSNAMEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AppointmentEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(companyId);

		queryPos.add(entryClassPK);

		queryPos.add(entryClassNameId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						appointmentEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<AppointmentEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 */
	@Override
	public void removeByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId) {

		for (AppointmentEntry appointmentEntry :
				findByCompanyId_EntryClassPK_EntryClassNameId(
					companyId, entryClassPK, entryClassNameId,
					QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(appointmentEntry);
		}
	}

	/**
	 * Returns the number of appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @return the number of matching appointment entries
	 */
	@Override
	public int countByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId) {

		FinderPath finderPath =
			_finderPathCountByCompanyId_EntryClassPK_EntryClassNameId;

		Object[] finderArgs = new Object[] {
			companyId, entryClassPK, entryClassNameId
		};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_COUNT_APPOINTMENTENTRY_WHERE);

			sb.append(
				_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_COMPANYID_2);

			sb.append(
				_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_ENTRYCLASSPK_2);

			sb.append(
				_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_ENTRYCLASSNAMEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(entryClassPK);

				queryPos.add(entryClassNameId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_COMPANYID_2 =
			"appointmentEntry.companyId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_ENTRYCLASSPK_2 =
			"appointmentEntry.entryClassPK = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYID_ENTRYCLASSPK_ENTRYCLASSNAMEID_ENTRYCLASSNAMEID_2 =
			"appointmentEntry.entryClassNameId = ?";

	private FinderPath
		_finderPathWithPaginationFindByCompanyId_ServiceId_ConnectorClassName_AppointmentId;
	private FinderPath
		_finderPathWithoutPaginationFindByCompanyId_ServiceId_ConnectorClassName_AppointmentId;
	private FinderPath
		_finderPathCountByCompanyId_ServiceId_ConnectorClassName_AppointmentId;

	/**
	 * Returns all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @return the matching appointment entries
	 */
	@Override
	public List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId) {

		return findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			companyId, serviceId, connectorClassName, appointmentId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId, int start, int end) {

		return findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			companyId, serviceId, connectorClassName, appointmentId, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId, int start, int end,
			OrderByComparator<AppointmentEntry> orderByComparator) {

		return findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			companyId, serviceId, connectorClassName, appointmentId, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	@Override
	public List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId, int start, int end,
			OrderByComparator<AppointmentEntry> orderByComparator,
			boolean useFinderCache) {

		serviceId = Objects.toString(serviceId, "");
		connectorClassName = Objects.toString(connectorClassName, "");
		appointmentId = Objects.toString(appointmentId, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByCompanyId_ServiceId_ConnectorClassName_AppointmentId;
				finderArgs = new Object[] {
					companyId, serviceId, connectorClassName, appointmentId
				};
			}
		}
		else if (useFinderCache) {
			finderPath =
				_finderPathWithPaginationFindByCompanyId_ServiceId_ConnectorClassName_AppointmentId;
			finderArgs = new Object[] {
				companyId, serviceId, connectorClassName, appointmentId, start,
				end, orderByComparator
			};
		}

		List<AppointmentEntry> list = null;

		if (useFinderCache) {
			list = (List<AppointmentEntry>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (AppointmentEntry appointmentEntry : list) {
					if ((companyId != appointmentEntry.getCompanyId()) ||
						!serviceId.equals(appointmentEntry.getServiceId()) ||
						!connectorClassName.equals(
							appointmentEntry.getConnectorClassName()) ||
						!appointmentId.equals(
							appointmentEntry.getAppointmentId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					6 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(6);
			}

			sb.append(_SQL_SELECT_APPOINTMENTENTRY_WHERE);

			sb.append(
				_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_COMPANYID_2);

			boolean bindServiceId = false;

			if (serviceId.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_SERVICEID_3);
			}
			else {
				bindServiceId = true;

				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_SERVICEID_2);
			}

			boolean bindConnectorClassName = false;

			if (connectorClassName.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_CONNECTORCLASSNAME_3);
			}
			else {
				bindConnectorClassName = true;

				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_CONNECTORCLASSNAME_2);
			}

			boolean bindAppointmentId = false;

			if (appointmentId.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_APPOINTMENTID_3);
			}
			else {
				bindAppointmentId = true;

				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_APPOINTMENTID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AppointmentEntryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				if (bindServiceId) {
					queryPos.add(serviceId);
				}

				if (bindConnectorClassName) {
					queryPos.add(connectorClassName);
				}

				if (bindAppointmentId) {
					queryPos.add(appointmentId);
				}

				list = (List<AppointmentEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_First(
				long companyId, String serviceId, String connectorClassName,
				String appointmentId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry =
			fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_First(
				companyId, serviceId, connectorClassName, appointmentId,
				orderByComparator);

		if (appointmentEntry != null) {
			return appointmentEntry;
		}

		StringBundler sb = new StringBundler(10);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", serviceId=");
		sb.append(serviceId);

		sb.append(", connectorClassName=");
		sb.append(connectorClassName);

		sb.append(", appointmentId=");
		sb.append(appointmentId);

		sb.append("}");

		throw new NoSuchAppointmentEntryException(sb.toString());
	}

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry
		fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_First(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId,
			OrderByComparator<AppointmentEntry> orderByComparator) {

		List<AppointmentEntry> list =
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
				companyId, serviceId, connectorClassName, appointmentId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_Last(
				long companyId, String serviceId, String connectorClassName,
				String appointmentId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry =
			fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_Last(
				companyId, serviceId, connectorClassName, appointmentId,
				orderByComparator);

		if (appointmentEntry != null) {
			return appointmentEntry;
		}

		StringBundler sb = new StringBundler(10);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", serviceId=");
		sb.append(serviceId);

		sb.append(", connectorClassName=");
		sb.append(connectorClassName);

		sb.append(", appointmentId=");
		sb.append(appointmentId);

		sb.append("}");

		throw new NoSuchAppointmentEntryException(sb.toString());
	}

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	@Override
	public AppointmentEntry
		fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_Last(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId,
			OrderByComparator<AppointmentEntry> orderByComparator) {

		int count = countByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			companyId, serviceId, connectorClassName, appointmentId);

		if (count == 0) {
			return null;
		}

		List<AppointmentEntry> list =
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
				companyId, serviceId, connectorClassName, appointmentId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	@Override
	public AppointmentEntry[]
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_PrevAndNext(
				long appointmentEntryId, long companyId, String serviceId,
				String connectorClassName, String appointmentId,
				OrderByComparator<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException {

		serviceId = Objects.toString(serviceId, "");
		connectorClassName = Objects.toString(connectorClassName, "");
		appointmentId = Objects.toString(appointmentId, "");

		AppointmentEntry appointmentEntry = findByPrimaryKey(
			appointmentEntryId);

		Session session = null;

		try {
			session = openSession();

			AppointmentEntry[] array = new AppointmentEntryImpl[3];

			array[0] =
				getByCompanyId_ServiceId_ConnectorClassName_AppointmentId_PrevAndNext(
					session, appointmentEntry, companyId, serviceId,
					connectorClassName, appointmentId, orderByComparator, true);

			array[1] = appointmentEntry;

			array[2] =
				getByCompanyId_ServiceId_ConnectorClassName_AppointmentId_PrevAndNext(
					session, appointmentEntry, companyId, serviceId,
					connectorClassName, appointmentId, orderByComparator,
					false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected AppointmentEntry
		getByCompanyId_ServiceId_ConnectorClassName_AppointmentId_PrevAndNext(
			Session session, AppointmentEntry appointmentEntry, long companyId,
			String serviceId, String connectorClassName, String appointmentId,
			OrderByComparator<AppointmentEntry> orderByComparator,
			boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				7 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(6);
		}

		sb.append(_SQL_SELECT_APPOINTMENTENTRY_WHERE);

		sb.append(
			_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_COMPANYID_2);

		boolean bindServiceId = false;

		if (serviceId.isEmpty()) {
			sb.append(
				_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_SERVICEID_3);
		}
		else {
			bindServiceId = true;

			sb.append(
				_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_SERVICEID_2);
		}

		boolean bindConnectorClassName = false;

		if (connectorClassName.isEmpty()) {
			sb.append(
				_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_CONNECTORCLASSNAME_3);
		}
		else {
			bindConnectorClassName = true;

			sb.append(
				_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_CONNECTORCLASSNAME_2);
		}

		boolean bindAppointmentId = false;

		if (appointmentId.isEmpty()) {
			sb.append(
				_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_APPOINTMENTID_3);
		}
		else {
			bindAppointmentId = true;

			sb.append(
				_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_APPOINTMENTID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AppointmentEntryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(companyId);

		if (bindServiceId) {
			queryPos.add(serviceId);
		}

		if (bindConnectorClassName) {
			queryPos.add(connectorClassName);
		}

		if (bindAppointmentId) {
			queryPos.add(appointmentId);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						appointmentEntry)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<AppointmentEntry> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 */
	@Override
	public void removeByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
		long companyId, String serviceId, String connectorClassName,
		String appointmentId) {

		for (AppointmentEntry appointmentEntry :
				findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
					companyId, serviceId, connectorClassName, appointmentId,
					QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(appointmentEntry);
		}
	}

	/**
	 * Returns the number of appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @return the number of matching appointment entries
	 */
	@Override
	public int countByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
		long companyId, String serviceId, String connectorClassName,
		String appointmentId) {

		serviceId = Objects.toString(serviceId, "");
		connectorClassName = Objects.toString(connectorClassName, "");
		appointmentId = Objects.toString(appointmentId, "");

		FinderPath finderPath =
			_finderPathCountByCompanyId_ServiceId_ConnectorClassName_AppointmentId;

		Object[] finderArgs = new Object[] {
			companyId, serviceId, connectorClassName, appointmentId
		};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(5);

			sb.append(_SQL_COUNT_APPOINTMENTENTRY_WHERE);

			sb.append(
				_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_COMPANYID_2);

			boolean bindServiceId = false;

			if (serviceId.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_SERVICEID_3);
			}
			else {
				bindServiceId = true;

				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_SERVICEID_2);
			}

			boolean bindConnectorClassName = false;

			if (connectorClassName.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_CONNECTORCLASSNAME_3);
			}
			else {
				bindConnectorClassName = true;

				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_CONNECTORCLASSNAME_2);
			}

			boolean bindAppointmentId = false;

			if (appointmentId.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_APPOINTMENTID_3);
			}
			else {
				bindAppointmentId = true;

				sb.append(
					_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_APPOINTMENTID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				if (bindServiceId) {
					queryPos.add(serviceId);
				}

				if (bindConnectorClassName) {
					queryPos.add(connectorClassName);
				}

				if (bindAppointmentId) {
					queryPos.add(appointmentId);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_COMPANYID_2 =
			"appointmentEntry.companyId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_SERVICEID_2 =
			"appointmentEntry.serviceId = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_SERVICEID_3 =
			"(appointmentEntry.serviceId IS NULL OR appointmentEntry.serviceId = '') AND ";

	private static final String
		_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_CONNECTORCLASSNAME_2 =
			"appointmentEntry.connectorClassName = ? AND ";

	private static final String
		_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_CONNECTORCLASSNAME_3 =
			"(appointmentEntry.connectorClassName IS NULL OR appointmentEntry.connectorClassName = '') AND ";

	private static final String
		_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_APPOINTMENTID_2 =
			"CAST_CLOB_TEXT(appointmentEntry.appointmentId) = ?";

	private static final String
		_FINDER_COLUMN_COMPANYID_SERVICEID_CONNECTORCLASSNAME_APPOINTMENTID_APPOINTMENTID_3 =
			"(appointmentEntry.appointmentId IS NULL OR CAST_CLOB_TEXT(appointmentEntry.appointmentId) = '')";

	public AppointmentEntryPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(AppointmentEntry.class);

		setModelImplClass(AppointmentEntryImpl.class);
		setModelPKClass(long.class);

		setTable(AppointmentEntryTable.INSTANCE);
	}

	/**
	 * Caches the appointment entry in the entity cache if it is enabled.
	 *
	 * @param appointmentEntry the appointment entry
	 */
	@Override
	public void cacheResult(AppointmentEntry appointmentEntry) {
		entityCache.putResult(
			AppointmentEntryImpl.class, appointmentEntry.getPrimaryKey(),
			appointmentEntry);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the appointment entries in the entity cache if it is enabled.
	 *
	 * @param appointmentEntries the appointment entries
	 */
	@Override
	public void cacheResult(List<AppointmentEntry> appointmentEntries) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (appointmentEntries.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (AppointmentEntry appointmentEntry : appointmentEntries) {
			if (entityCache.getResult(
					AppointmentEntryImpl.class,
					appointmentEntry.getPrimaryKey()) == null) {

				cacheResult(appointmentEntry);
			}
		}
	}

	/**
	 * Clears the cache for all appointment entries.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(AppointmentEntryImpl.class);

		finderCache.clearCache(AppointmentEntryImpl.class);
	}

	/**
	 * Clears the cache for the appointment entry.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AppointmentEntry appointmentEntry) {
		entityCache.removeResult(AppointmentEntryImpl.class, appointmentEntry);
	}

	@Override
	public void clearCache(List<AppointmentEntry> appointmentEntries) {
		for (AppointmentEntry appointmentEntry : appointmentEntries) {
			entityCache.removeResult(
				AppointmentEntryImpl.class, appointmentEntry);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(AppointmentEntryImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(AppointmentEntryImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new appointment entry with the primary key. Does not add the appointment entry to the database.
	 *
	 * @param appointmentEntryId the primary key for the new appointment entry
	 * @return the new appointment entry
	 */
	@Override
	public AppointmentEntry create(long appointmentEntryId) {
		AppointmentEntry appointmentEntry = new AppointmentEntryImpl();

		appointmentEntry.setNew(true);
		appointmentEntry.setPrimaryKey(appointmentEntryId);

		String uuid = PortalUUIDUtil.generate();

		appointmentEntry.setUuid(uuid);

		appointmentEntry.setCompanyId(CompanyThreadLocal.getCompanyId());

		return appointmentEntry;
	}

	/**
	 * Removes the appointment entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry that was removed
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	@Override
	public AppointmentEntry remove(long appointmentEntryId)
		throws NoSuchAppointmentEntryException {

		return remove((Serializable)appointmentEntryId);
	}

	/**
	 * Removes the appointment entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the appointment entry
	 * @return the appointment entry that was removed
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	@Override
	public AppointmentEntry remove(Serializable primaryKey)
		throws NoSuchAppointmentEntryException {

		Session session = null;

		try {
			session = openSession();

			AppointmentEntry appointmentEntry = (AppointmentEntry)session.get(
				AppointmentEntryImpl.class, primaryKey);

			if (appointmentEntry == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAppointmentEntryException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(appointmentEntry);
		}
		catch (NoSuchAppointmentEntryException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AppointmentEntry removeImpl(AppointmentEntry appointmentEntry) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(appointmentEntry)) {
				appointmentEntry = (AppointmentEntry)session.get(
					AppointmentEntryImpl.class,
					appointmentEntry.getPrimaryKeyObj());
			}

			if (appointmentEntry != null) {
				session.delete(appointmentEntry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (appointmentEntry != null) {
			clearCache(appointmentEntry);
		}

		return appointmentEntry;
	}

	@Override
	public AppointmentEntry updateImpl(AppointmentEntry appointmentEntry) {
		boolean isNew = appointmentEntry.isNew();

		if (!(appointmentEntry instanceof AppointmentEntryModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(appointmentEntry.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					appointmentEntry);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in appointmentEntry proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom AppointmentEntry implementation " +
					appointmentEntry.getClass());
		}

		AppointmentEntryModelImpl appointmentEntryModelImpl =
			(AppointmentEntryModelImpl)appointmentEntry;

		if (Validator.isNull(appointmentEntry.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			appointmentEntry.setUuid(uuid);
		}

		if (isNew && (appointmentEntry.getCreateDate() == null)) {
			ServiceContext serviceContext =
				ServiceContextThreadLocal.getServiceContext();

			Date date = new Date();

			if (serviceContext == null) {
				appointmentEntry.setCreateDate(date);
			}
			else {
				appointmentEntry.setCreateDate(
					serviceContext.getCreateDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(appointmentEntry);
			}
			else {
				appointmentEntry = (AppointmentEntry)session.merge(
					appointmentEntry);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			AppointmentEntryImpl.class, appointmentEntryModelImpl, false, true);

		if (isNew) {
			appointmentEntry.setNew(false);
		}

		appointmentEntry.resetOriginalValues();

		return appointmentEntry;
	}

	/**
	 * Returns the appointment entry with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the appointment entry
	 * @return the appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	@Override
	public AppointmentEntry findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAppointmentEntryException {

		AppointmentEntry appointmentEntry = fetchByPrimaryKey(primaryKey);

		if (appointmentEntry == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAppointmentEntryException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return appointmentEntry;
	}

	/**
	 * Returns the appointment entry with the primary key or throws a <code>NoSuchAppointmentEntryException</code> if it could not be found.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	@Override
	public AppointmentEntry findByPrimaryKey(long appointmentEntryId)
		throws NoSuchAppointmentEntryException {

		return findByPrimaryKey((Serializable)appointmentEntryId);
	}

	/**
	 * Returns the appointment entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry, or <code>null</code> if a appointment entry with the primary key could not be found
	 */
	@Override
	public AppointmentEntry fetchByPrimaryKey(long appointmentEntryId) {
		return fetchByPrimaryKey((Serializable)appointmentEntryId);
	}

	/**
	 * Returns all the appointment entries.
	 *
	 * @return the appointment entries
	 */
	@Override
	public List<AppointmentEntry> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of appointment entries
	 */
	@Override
	public List<AppointmentEntry> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of appointment entries
	 */
	@Override
	public List<AppointmentEntry> findAll(
		int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of appointment entries
	 */
	@Override
	public List<AppointmentEntry> findAll(
		int start, int end,
		OrderByComparator<AppointmentEntry> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<AppointmentEntry> list = null;

		if (useFinderCache) {
			list = (List<AppointmentEntry>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_APPOINTMENTENTRY);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_APPOINTMENTENTRY;

				sql = sql.concat(AppointmentEntryModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<AppointmentEntry>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the appointment entries from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (AppointmentEntry appointmentEntry : findAll()) {
			remove(appointmentEntry);
		}
	}

	/**
	 * Returns the number of appointment entries.
	 *
	 * @return the number of appointment entries
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_APPOINTMENTENTRY);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "appointmentEntryId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_APPOINTMENTENTRY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return AppointmentEntryModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the appointment entry persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByCompanyId_EntryClassPK_EntryClassNameId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByCompanyId_EntryClassPK_EntryClassNameId",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Long.class.getName(), Integer.class.getName(),
					Integer.class.getName(), OrderByComparator.class.getName()
				},
				new String[] {"companyId", "entryClassPK", "entryClassNameId"},
				true);

		_finderPathWithoutPaginationFindByCompanyId_EntryClassPK_EntryClassNameId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByCompanyId_EntryClassPK_EntryClassNameId",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Long.class.getName()
				},
				new String[] {"companyId", "entryClassPK", "entryClassNameId"},
				true);

		_finderPathCountByCompanyId_EntryClassPK_EntryClassNameId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"countByCompanyId_EntryClassPK_EntryClassNameId",
				new String[] {
					Long.class.getName(), Long.class.getName(),
					Long.class.getName()
				},
				new String[] {"companyId", "entryClassPK", "entryClassNameId"},
				false);

		_finderPathWithPaginationFindByCompanyId_ServiceId_ConnectorClassName_AppointmentId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByCompanyId_ServiceId_ConnectorClassName_AppointmentId",
				new String[] {
					Long.class.getName(), String.class.getName(),
					String.class.getName(), String.class.getName(),
					Integer.class.getName(), Integer.class.getName(),
					OrderByComparator.class.getName()
				},
				new String[] {
					"companyId", "serviceId", "connectorClassName",
					"appointmentId"
				},
				true);

		_finderPathWithoutPaginationFindByCompanyId_ServiceId_ConnectorClassName_AppointmentId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByCompanyId_ServiceId_ConnectorClassName_AppointmentId",
				new String[] {
					Long.class.getName(), String.class.getName(),
					String.class.getName(), String.class.getName()
				},
				new String[] {
					"companyId", "serviceId", "connectorClassName",
					"appointmentId"
				},
				true);

		_finderPathCountByCompanyId_ServiceId_ConnectorClassName_AppointmentId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"countByCompanyId_ServiceId_ConnectorClassName_AppointmentId",
				new String[] {
					Long.class.getName(), String.class.getName(),
					String.class.getName(), String.class.getName()
				},
				new String[] {
					"companyId", "serviceId", "connectorClassName",
					"appointmentId"
				},
				false);

		AppointmentEntryUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		AppointmentEntryUtil.setPersistence(null);

		entityCache.removeCache(AppointmentEntryImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_AppointmentPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_AppointmentPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_AppointmentPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_APPOINTMENTENTRY =
		"SELECT appointmentEntry FROM AppointmentEntry appointmentEntry";

	private static final String _SQL_SELECT_APPOINTMENTENTRY_WHERE =
		"SELECT appointmentEntry FROM AppointmentEntry appointmentEntry WHERE ";

	private static final String _SQL_COUNT_APPOINTMENTENTRY =
		"SELECT COUNT(appointmentEntry) FROM AppointmentEntry appointmentEntry";

	private static final String _SQL_COUNT_APPOINTMENTENTRY_WHERE =
		"SELECT COUNT(appointmentEntry) FROM AppointmentEntry appointmentEntry WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "appointmentEntry.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No AppointmentEntry exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No AppointmentEntry exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		AppointmentEntryPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}