package com.placecube.digitalplace.search.rankings.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.search.rankings.configuration.RankingsCompanyConfiguration;

@RunWith(MockitoJUnitRunner.class)
public class RankingsConfigurationServiceTest {

	@InjectMocks
	private RankingsConfigurationService rankingsConfigurationService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private RankingsCompanyConfiguration mockRankingsCompanyConfiguration;

	@Test
	public void isEnableWildcardRanking_WhenNoErrors_ThenReturnsConfiguredWildcardEnabledSetting() throws ConfigurationException {
		final long companyId = 342L;

		when(mockConfigurationProvider.getCompanyConfiguration(RankingsCompanyConfiguration.class, companyId)).thenReturn(mockRankingsCompanyConfiguration);
		when(mockRankingsCompanyConfiguration.enableWildcardRankings()).thenReturn(true);

		boolean result = rankingsConfigurationService.isEnableWildcardRankings(companyId);

		assertTrue(result);
	}

	@Test
	public void isEnableWildcardRanking_WhenConfigurationRetrievalFails_ThenReturnsFalse() throws ConfigurationException {
		final long companyId = 342L;

		when(mockConfigurationProvider.getCompanyConfiguration(RankingsCompanyConfiguration.class, companyId)).thenThrow(new ConfigurationException());

		boolean result = rankingsConfigurationService.isEnableWildcardRankings(companyId);

		assertFalse(result);
	}
}
