package com.placecube.digitalplace.search.internal.util;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.model.PortletPreferences;
import com.liferay.portal.kernel.service.PortletLocalService;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortletKeys;

public class RetrievalUtilTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	private static final long GROUP_ID = 20;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutTypePortlet mockLayoutTypePortlet;

	@Mock
	private Portlet mockPortlet1;

	@Mock
	private Portlet mockPortlet2;

	@Mock
	private Portlet mockPortlet3;

	@Mock
	private Portlet mockPortlet4;

	@Mock
	private Portlet mockPortlet5;

	@Mock
	private Portlet mockPortlet6;

	@Mock
	private PortletLocalService mockPortletLocalService;

	@Mock
	private javax.portlet.PortletPreferences mockPortletPreferences;

	@Mock
	private PortletPreferences mockPortletPreferences1;

	@Mock
	private PortletPreferences mockPortletPreferences2;

	@Mock
	private PortletPreferences mockPortletPreferences3;

	@Mock
	private PortletPreferences mockPortletPreferences4;

	@Mock
	private PortletPreferences mockPortletPreferences5;

	@Mock
	private PortletPreferences mockPortletPreferences6;

	@Mock
	private PortletPreferencesLocalService mockPortletPreferencesLocalService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private RetrievalUtil retrievalUtil;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getAllPortletsOnLayout_WhenNoError_ThenReturnsAllPortletsOnTheLayout() {
		List<Portlet> portlets = new ArrayList<>();
		portlets.add(mockPortlet1);
		portlets.add(mockPortlet2);
		when(mockLayout.getLayoutType()).thenReturn(mockLayoutTypePortlet);
		when(mockLayoutTypePortlet.getAllPortlets(false)).thenReturn(portlets);

		Stream<Portlet> results = retrievalUtil.getAllPortletsOnLayout(mockLayout);

		assertThat(results.collect(Collectors.toList()), contains(mockPortlet1, mockPortlet2));
	}

	@Test
	public void getPortletPreferences_WhenPortletIsNotStatic_WhenReturnsOptionalWithThePortletPreferencesForThePlid() {
		String portletId = "portletIdVal";
		long plid = 12345;
		when(mockPortlet1.isStatic()).thenReturn(false);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getPlid()).thenReturn(plid);
		when(mockPortlet1.getPortletId()).thenReturn(portletId);
		when(mockPortletPreferencesLocalService.fetchPreferences(COMPANY_ID, PortletKeys.PREFS_OWNER_ID_DEFAULT, PortletKeys.PREFS_OWNER_TYPE_LAYOUT, plid, portletId))
				.thenReturn(mockPortletPreferences);

		Optional<javax.portlet.PortletPreferences> result = retrievalUtil.getPortletPreferences(mockPortlet1, mockThemeDisplay);

		assertThat(result.get(), sameInstance(mockPortletPreferences));
	}

	@Test
	public void getPortletPreferences_WhenPortletIsStatic_WhenReturnsOptionalWithThePortletPreferencesForTheSharedPlid() {
		String portletId = "portletIdVal";
		when(mockPortlet1.isStatic()).thenReturn(true);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID);
		when(mockPortlet1.getPortletId()).thenReturn(portletId);
		when(mockPortletPreferencesLocalService.fetchPreferences(COMPANY_ID, GROUP_ID, PortletKeys.PREFS_OWNER_TYPE_LAYOUT, PortletKeys.PREFS_PLID_SHARED, portletId))
				.thenReturn(mockPortletPreferences);

		Optional<javax.portlet.PortletPreferences> result = retrievalUtil.getPortletPreferences(mockPortlet1, mockThemeDisplay);

		assertThat(result.get(), sameInstance(mockPortletPreferences));
	}

	@Test
	public void getValidPortletsOnLayout_WhenNoError_ThenReturnsAllValidPortletsOnTheLayout() {
		long plid = 123;
		when(mockLayout.getPlid()).thenReturn(plid);

		List<PortletPreferences> portletPrefs = new ArrayList<>();
		portletPrefs.add(mockPortletPreferences1);
		portletPrefs.add(mockPortletPreferences2);
		portletPrefs.add(mockPortletPreferences3);
		portletPrefs.add(mockPortletPreferences4);
		portletPrefs.add(mockPortletPreferences5);
		portletPrefs.add(mockPortletPreferences6);
		when(mockPortletPreferencesLocalService.getPortletPreferences(PortletKeys.PREFS_OWNER_ID_DEFAULT, PortletKeys.PREFS_OWNER_TYPE_LAYOUT, plid)).thenReturn(portletPrefs);

		mockPortletRetrieval(mockPortletPreferences1, "portletId1", mockPortlet1, true, "instance1");
		mockPortletRetrieval(mockPortletPreferences2, "portletId2", mockPortlet2, false, "");
		mockPortletRetrieval(mockPortletPreferences3, "portletId3", mockPortlet3, true, "");
		mockPortletRetrieval(mockPortletPreferences4, "portletId4", mockPortlet4, true, "instance4");
		mockPortletRetrieval(mockPortletPreferences5, "portletId5", mockPortlet5, true, null);
		mockPortletRetrieval(mockPortletPreferences6, "portletId6", mockPortlet6, false, null);

		Stream<Portlet> results = retrievalUtil.getValidPortletsOnLayout(mockLayout, COMPANY_ID);

		assertThat(results.collect(Collectors.toList()), contains(mockPortlet1, mockPortlet2, mockPortlet4, mockPortlet6));
	}

	private void mockPortletRetrieval(PortletPreferences mockPortletPreferences, String portletId, Portlet portlet, boolean instanceable, String instanceId) {
		when(mockPortletPreferences.getPortletId()).thenReturn(portletId);
		when(mockPortletLocalService.getPortletById(COMPANY_ID, portletId)).thenReturn(portlet);
		when(portlet.isInstanceable()).thenReturn(instanceable);
		when(portlet.getInstanceId()).thenReturn(instanceId);
	}
}
