package com.placecube.digitalplace.pageimportadmin.service;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.antivirus.AntivirusScannerException;
import com.liferay.document.library.kernel.antivirus.AntivirusScannerUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.LayoutFriendlyURLsException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.pageimportadmin.model.PageImportModel;
import com.placecube.digitalplace.pageimportadmin.model.RowImportModel;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.service.LayoutInitializer;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;

@Component(immediate = true, service = ContentImportService.class)
public class ContentImportService {

	private static final Log LOG = LogFactoryUtil.getLog(ContentImportService.class);

	@Reference
	private ContentValidationService contentValidationService;

	@Reference
	private FileParserService fileParserService;

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	@Reference
	private LayoutInitializer layoutInitializer;

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	@Reference
	private Portal portal;

	public void createPage(PageImportModel pageImportModel, Locale locale, ActionRequest actionRequest) {
		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(Layout.class.getName(), actionRequest);
			contentValidationService.validatePage(pageImportModel, serviceContext);
			if (pageImportModel.isValid()) {
				LOG.debug("Importing page with name: " + pageImportModel.getTitle() + ", url: " + pageImportModel.getUrl());

				LayoutContext layoutContext = modelFactoryBuilder.getLayoutContext(pageImportModel);

				boolean createAsContentPage = true;

				if (pageImportModel.getPageTemplate().isPresent()) {
					LayoutPageTemplateEntry layoutPageTemplateEntry = pageImportModel.getPageTemplate().get();
					createAsContentPage = layoutPageTemplateEntry.getType() == 0;
					modelFactoryBuilder.configurePageTemplate(layoutContext, layoutPageTemplateEntry);
				}

				addCategoriesInServiceContext(pageImportModel, serviceContext);

				if (createAsContentPage) {
					layoutInitializer.getOrCreateContentLayout(layoutContext, serviceContext);
				} else {
					layoutInitializer.getOrCreatePortletLayout(layoutContext, serviceContext);
				}
			}
		} catch (LayoutFriendlyURLsException e) {
			LOG.debug(e);
			pageImportModel.addErrorMessage(LanguageUtil.get(locale, "invalid-url"));
		} catch (Exception e) {
			LOG.debug(e);
			LOG.error("Unable to import page " + e.getMessage());
			pageImportModel.addErrorMessage(LanguageUtil.format(locale, "unable-to-import-page", e.getMessage()));
		}
	}

	public void createWebContent(WebContentImportModel webContentImportModel, Locale locale, ActionRequest actionRequest) {
		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(JournalArticle.class.getName(), actionRequest);
			contentValidationService.validateWebContent(webContentImportModel, serviceContext);
			if (webContentImportModel.isValid()) {
				LOG.debug("Importing web content with title: " + webContentImportModel.getTitle() + ", url: " + webContentImportModel.getUrl());

				addCategoriesInServiceContext(webContentImportModel, serviceContext);

				JournalArticleContext journalArticleContext = modelFactoryBuilder.getJournalArticleContext(webContentImportModel);

				journalArticleCreationService.getOrCreateArticle(journalArticleContext, serviceContext);
			}
		} catch (Exception e) {
			LOG.debug(e);
			LOG.error("Unable to import web content " + e.getMessage());
			webContentImportModel.addErrorMessage(LanguageUtil.format(locale, "unable-to-import-web-content", e.getMessage()));
		}
	}

	public File getFile(ActionRequest actionRequest) throws AntivirusScannerException {
		UploadPortletRequest uploadPortletRequest = portal.getUploadPortletRequest(actionRequest);
		File file = uploadPortletRequest.getFile("file");
		AntivirusScannerUtil.scan(file);
		return file;
	}

	public Optional<Sheet> getFirstSheet(File file) throws PortletException {
		Workbook workbook = null;
		try {
			workbook = WorkbookFactory.create(file);
			Sheet sheet = workbook.getSheetAt(0);

			if (isEmptySheet(sheet)) {
				return Optional.empty();
			}

			return Optional.of(sheet);
		} catch (Exception e) {
			throw new PortletException(e);
		} finally {
			if (workbook != null) {
				quietlyCloseWorkbook(workbook);
			}
		}
	}

	public RowImportModel getRowContent(Sheet sheet, int index, long groupId, Locale locale, boolean privateLayout, long assetVocabularyId) {
		RowImportModel rowImportModel = fileParserService.getRowContent(sheet, index, groupId, privateLayout);

		if (rowImportModel.isPageImport() || rowImportModel.isWebContent()) {
			modelFactoryBuilder.configureAndValidateCategories(rowImportModel, assetVocabularyId, locale);
		}

		return rowImportModel;
	}

	private void addCategoriesInServiceContext(RowImportModel pageImportModel, ServiceContext serviceContext) {
		List<Long> categoryIds = pageImportModel.getCategoryIds();
		serviceContext.setAssetCategoryIds(ArrayUtil.toArray(categoryIds.toArray(new Long[categoryIds.size()])));
	}

	private boolean isEmptySheet(Sheet sheet) {
		return sheet.getLastRowNum() <= 0 && sheet.getRow(sheet.getLastRowNum()) == null;
	}

	private void quietlyCloseWorkbook(Workbook workbook) {
		try {
			workbook.close();
		} catch (Exception e) {
			LOG.debug(e);
			LOG.error("Unable to close workbook " + e.getMessage());
		}
	}

}
