package com.placecube.digitalplace.user.account.web.constants;

public final class AccountPortletKeys {

	public static final String CREATE_ACCOUNT = "com_placecube_digitalplace_user_account_CreateAccountPortlet";

	public static final String EDIT_ACCOUNT = "com_placecube_digitalplace_user_account_EditAccountPortlet";

	private AccountPortletKeys() {
	}
}
