package com.placecube.digitalplace.payment.heycentric.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.payment.heycentric.configuration.HeycentricPaymentCompanyConfiguration", localization = "content/Language", name = "payment-heycentric")
public interface HeycentricPaymentCompanyConfiguration {

	@Meta.AD(required = false, deflt = "", name = "area-name", description = "area-description")
	String area();

	@Meta.AD(required = false, deflt = "", name = "arp-endpoint", description = "arp-endpoint-description")
	String arpEndpoint();

	@Meta.AD(required = false, deflt = "Placecube", name = "arp-external-system-name", description = "arp-external-system-name-description")
	String arpExternalSystemName();

	@Meta.AD(required = false, deflt = "", name = "arp-group-receipt-email", description = "arp-group-receipt-email-description")
	String arpGroupReceiptEmail();

	@Meta.AD(required = false, deflt = "", name = "arp-secret", description = "arp-secret-description")
	String arpSecret();

	@Meta.AD(required = false, deflt = "https://stg.payments.heycentric.io/?", name = "base-url-name", description = "base-url-description")
	String baseURL();

	@Meta.AD(required = false, deflt = "", name = "client-name", description = "client-description")
	String client();

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "", name = "entity-name", description = "entity-description")
	String entity();

	@Meta.AD(required = false, deflt = "true", name = "use-hash", description = "use-hash-description")
	boolean isHash();

	@Meta.AD(required = false, deflt = "", name = "pmtTyp-name", description = "pmtTyp-description")
	String pmtTyp();

	@Meta.AD(required = false, deflt = "", name = "private-key")
	String privateKey();

	@Meta.AD(required = false, deflt = "", name = "till-name", description = "till-description")
	String till();
}
