package com.placecube.digitalplace.addresslookup.dtf.constants;

public class DTFConstants {

	public static final String FIELD_NAME_PAODESC = "PaoDesc";

	public static final String FIELD_NAME_PAONUMBER = "PaoNumber";

	public static final String FIELD_NAME_POSTCODE = "POSTCODE";

	public static final String FIELD_NAME_POSTTOWN = "PostTown";

	public static final String FIELD_NAME_SAODESC = "SaoDesc";

	public static final String FIELD_NAME_SAONUMBER = "SaoNumber";

	public static final String FIELD_NAME_STREET = "Street";

	public static final String FIELD_NAME_UPRN = "UPRN";

	public static final String FIND_BY_POSTCODE_SQL = "select UPRN,SaoDesc,SaoNumber,PaoDesc,PaoNumber,Street,PostTown,POSTCODE from vwAddresses where REPLACE(POSTCODE, ' ', '')='";

	public static final String FIND_BY_UPRN_SQL = "select UPRN,SaoDesc,SaoNumber,PaoDesc,PaoNumber,Street,PostTown,POSTCODE from vwAddresses where UPRN='";

	public static final String SUFFIX_SQL = "' order by PaoNumber, SaoNumber, SaoDesc";

	private DTFConstants() {
	}
}