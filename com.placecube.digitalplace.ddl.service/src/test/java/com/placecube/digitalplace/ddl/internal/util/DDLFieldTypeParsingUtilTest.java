package com.placecube.digitalplace.ddl.internal.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;

public class DDLFieldTypeParsingUtilTest extends PowerMockito {

	@InjectMocks
	private DDLFieldTypeParsingUtil ddlFieldTypeParsingUtil;

	@Mock
	private DDMFormField mockDDMFormField;

	@Mock
	private DDMFormFieldOptions mockDDMFormFieldOptions;

	@Mock
	private LocalizedValue mockLocalizedValue;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getOptionLabel_WhenNoValueForOptionFound_ThenReturnsTheOriginalValue() {
		String myValue = "myValue";
		Locale locale = Locale.ENGLISH;
		when(mockDDMFormField.getDDMFormFieldOptions()).thenReturn(mockDDMFormFieldOptions);
		when(mockDDMFormFieldOptions.getOptionLabels(myValue)).thenReturn(null);

		String result = ddlFieldTypeParsingUtil.getOptionLabel(mockDDMFormField, locale, myValue);

		assertThat(result, equalTo(myValue));
	}

	@Test
	public void getOptionLabel_WhenValueForOptionFound_ThenReturnsTheLocalisedValue() {
		String myValue = "myValue";
		String expected = "expectedValue";
		Locale locale = Locale.ENGLISH;
		when(mockDDMFormField.getDDMFormFieldOptions()).thenReturn(mockDDMFormFieldOptions);
		when(mockDDMFormFieldOptions.getOptionLabels(myValue)).thenReturn(mockLocalizedValue);
		when(mockLocalizedValue.getString(locale)).thenReturn(expected);

		String result = ddlFieldTypeParsingUtil.getOptionLabel(mockDDMFormField, locale, myValue);

		assertThat(result, equalTo(expected));
	}

}
