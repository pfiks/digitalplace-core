package com.placecube.digitalplace.appointmentweb.service;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.object.constants.ObjectDefinitionConstants;
import com.liferay.object.model.ObjectDefinition;
import com.liferay.object.rest.filter.factory.FilterFactory;
import com.liferay.object.service.ObjectDefinitionLocalService;
import com.liferay.object.service.ObjectEntryLocalService;
import com.liferay.petra.sql.dsl.expression.Predicate;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = ObjectEntryRetrievalService.class)
public class ObjectEntryRetrievalService {

	@Reference(target = "(filter.factory.key=" + ObjectDefinitionConstants.STORAGE_TYPE_DEFAULT + ")")
	private FilterFactory<Predicate> filterFactory;

	@Reference
	private ObjectDefinitionLocalService objectDefinitionLocalService;

	@Reference
	private ObjectEntryLocalService objectEntryLocalService;

	public Set<Long> getObjectEntryIdsFromFilterExpressions(long objectDefinitionId, String filterExpression) throws PortalException {
		Set<Long> ids = new HashSet<>();

		if (Validator.isNotNull(filterExpression)) {
			ObjectDefinition objectDefinition = objectDefinitionLocalService.getObjectDefinition(objectDefinitionId);

			Predicate predicate = filterFactory.create(filterExpression, objectDefinition);

			List<Map<String, Serializable>> valuesList = objectEntryLocalService.getValuesList(0l, objectDefinition.getCompanyId(), 0l, objectDefinitionId, new String[] { "id" }, predicate,
					StringPool.BLANK, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);

			for (Map<String, Serializable> val : valuesList) {
				ids.add((Long) val.get("objectEntryId"));
			}
		}

		return ids;
	}

}
