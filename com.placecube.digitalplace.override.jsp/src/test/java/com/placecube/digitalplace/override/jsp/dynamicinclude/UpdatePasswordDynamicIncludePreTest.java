package com.placecube.digitalplace.override.jsp.dynamicinclude;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.portlet.PortletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.servlet.taglib.DynamicInclude.DynamicIncludeRegistry;
import com.placecube.digitalplace.user.account.service.UserAuthenticationService;

@RunWith(MockitoJUnitRunner.class)
public class UpdatePasswordDynamicIncludePreTest {

	@Mock
	private DynamicIncludeRegistry mockDynamicIncludeRegistry;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private UserAuthenticationService mockUserAuthenticationService;

	@InjectMocks
	private UpdatePasswordDynamicIncludePre updatePasswordDynamicIncludePre;

	@Test
	public void include_WhenNoErrors_ThenThePasswordPolicyAttributeIsAddedToTheRequest() throws Exception {
		final String passwordPolicy = "passwordPolicy";

		when(mockUserAuthenticationService.getUserAccountPasswordPolicyHtml(mockHttpServletRequest)).thenReturn(passwordPolicy);

		updatePasswordDynamicIncludePre.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, times(1)).setAttribute("passwordPolicy", passwordPolicy);
	}

	@Test
	public void include_WhenPasswordPolicyRetrievalFails_ThenThePasswordPolicyAttributeIsNotAddedToTheRequest() throws Exception {
		when(mockUserAuthenticationService.getUserAccountPasswordPolicyHtml(mockHttpServletRequest)).thenThrow(new PortletException());

		updatePasswordDynamicIncludePre.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, never()).setAttribute(anyString(), any());
	}

	@Test
	public void register_WhenNoErrors_ThenRegistersUpdatePasswordPre() {
		updatePasswordDynamicIncludePre.register(mockDynamicIncludeRegistry);

		verify(mockDynamicIncludeRegistry, times(1)).register("/html/portal/update_password.jsp#pre");
	}
}
