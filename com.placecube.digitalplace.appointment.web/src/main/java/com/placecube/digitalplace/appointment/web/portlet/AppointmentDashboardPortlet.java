package com.placecube.digitalplace.appointment.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.appointment.web.constants.PortletKeys;

@Component(immediate = true, property = { //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=portlet-appointmentsdashboard", //
		"com.liferay.portlet.display-category=category.digitalservices", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.render-weight=10", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.template-path=/", //
		"javax.portlet.init-param.view-template=/view-appointments.jsp", //
		"javax.portlet.name=" + PortletKeys.APPOINTMENTS_DASHBOARD, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=power-user,user", //
		"javax.portlet.supports.mime-type=text/html", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class AppointmentDashboardPortlet extends MVCPortlet {

}