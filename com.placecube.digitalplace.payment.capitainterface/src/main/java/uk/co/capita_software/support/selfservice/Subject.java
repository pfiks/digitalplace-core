/**
 * Subject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uk.co.capita_software.support.selfservice;

public class Subject  implements java.io.Serializable {
    private uk.co.capita_software.support.selfservice.SubjectType subjectType;

    private int identifier;

    private uk.co.capita_software.support.selfservice.SystemCode systemCode;

    public Subject() {
    }

    public Subject(
           uk.co.capita_software.support.selfservice.SubjectType subjectType,
           int identifier,
           uk.co.capita_software.support.selfservice.SystemCode systemCode) {
           this.subjectType = subjectType;
           this.identifier = identifier;
           this.systemCode = systemCode;
    }


    /**
     * Gets the subjectType value for this Subject.
     *
     * @return subjectType
     */
    public uk.co.capita_software.support.selfservice.SubjectType getSubjectType() {
        return subjectType;
    }


    /**
     * Sets the subjectType value for this Subject.
     *
     * @param subjectType
     */
    public void setSubjectType(uk.co.capita_software.support.selfservice.SubjectType subjectType) {
        this.subjectType = subjectType;
    }


    /**
     * Gets the identifier value for this Subject.
     *
     * @return identifier
     */
    public int getIdentifier() {
        return identifier;
    }


    /**
     * Sets the identifier value for this Subject.
     *
     * @param identifier
     */
    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }


    /**
     * Gets the systemCode value for this Subject.
     *
     * @return systemCode
     */
    public uk.co.capita_software.support.selfservice.SystemCode getSystemCode() {
        return systemCode;
    }


    /**
     * Sets the systemCode value for this Subject.
     *
     * @param systemCode
     */
    public void setSystemCode(uk.co.capita_software.support.selfservice.SystemCode systemCode) {
        this.systemCode = systemCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Subject)) return false;
        Subject other = (Subject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.subjectType==null && other.getSubjectType()==null) ||
             (this.subjectType!=null &&
              this.subjectType.equals(other.getSubjectType()))) &&
            this.identifier == other.getIdentifier() &&
            ((this.systemCode==null && other.getSystemCode()==null) ||
             (this.systemCode!=null &&
              this.systemCode.equals(other.getSystemCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSubjectType() != null) {
            _hashCode += getSubjectType().hashCode();
        }
        _hashCode += getIdentifier();
        if (getSystemCode() != null) {
            _hashCode += getSystemCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Subject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "subject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subjectType");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "subjectType"));
        elemField.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "subjectType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCode");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "systemCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "systemCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

    @Override
    public String toString() {
        return "Subject [subjectType=" + subjectType + ", identifier="
                + identifier + ", systemCode=" + systemCode + "]";
    }



}
