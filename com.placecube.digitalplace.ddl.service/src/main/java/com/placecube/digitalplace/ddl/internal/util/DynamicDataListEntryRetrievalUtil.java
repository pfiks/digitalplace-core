package com.placecube.digitalplace.ddl.internal.util;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.model.DynamicDataListEntry;
import com.placecube.digitalplace.ddl.model.DynamicDataListValue;
import com.placecube.digitalplace.ddl.model.impl.DynamicDataListEntryImpl;
import com.placecube.digitalplace.ddl.model.impl.ModelBuilder;

@Component(immediate = true, service = DynamicDataListEntryRetrievalUtil.class)
public class DynamicDataListEntryRetrievalUtil {

	private static final Log LOG = LogFactoryUtil.getLog(DynamicDataListEntryRetrievalUtil.class);

	@Reference
	private DDLRecordParsingUtil ddlRecordParsingUtil;

	@Reference
	private ModelBuilder modelBuilder;

	public Optional<DynamicDataListEntry> getDynamicDataListEntry(DDLRecord ddlRecord) {
		try {
			List<DDMFormFieldValue> ddmFormFieldValues = ddlRecord.getDDMFormValues().getDDMFormFieldValues();

			DynamicDataListEntryImpl dynamicDataListEntry = modelBuilder.initialiseDynamicDataListEntry(ddlRecord);

			for (DDMFormFieldValue ddmFormFieldValue : ddmFormFieldValues) {
				DDMFormField ddmFormField = ddmFormFieldValue.getDDMFormField();
				if (Validator.isNotNull(ddmFormField)) {

					String fieldName = ddlRecordParsingUtil.getFieldLabel(ddmFormField);

					String[] fieldValueAndDetails = ddlRecordParsingUtil.getFieldValueAndAdditionalDetails(ddmFormField, ddmFormFieldValue);

					DynamicDataListValue value = modelBuilder.initialiseDynamicDataListValue(fieldName, fieldValueAndDetails[0], fieldValueAndDetails[1]);
					dynamicDataListEntry.addDynamicDataListValue(value);
				}
			}

			return Optional.of(dynamicDataListEntry);
		} catch (Exception e) {
			LOG.warn("Unable to parse ddl record fields " + e.getMessage());
			LOG.debug(e);
			return Optional.empty();
		}
	}

}
