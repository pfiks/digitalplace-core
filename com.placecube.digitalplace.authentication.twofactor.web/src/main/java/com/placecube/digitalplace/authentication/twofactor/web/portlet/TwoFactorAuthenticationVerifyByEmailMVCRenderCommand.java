package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorPortletKeys;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorEmailService;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;

@Component(property = { "javax.portlet.name=" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION, "mvc.command.name=" + TwoFactorMVCCommandKeys.VERIFY_BY_EMAIL }, service = MVCRenderCommand.class)
public class TwoFactorAuthenticationVerifyByEmailMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private TwoFactorEmailService twoFactorEmailService;

	@Reference
	private TwoFactorRequestUtils twoFactorRequestUtils;

	@Reference
	private TwoFactorService twoFactorService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {
		Optional<User> twoFactorUser = twoFactorService.getTwoFactorUser(renderRequest, twoFactorRequestUtils.getOriginalServletRequest(renderRequest));

		if (!twoFactorUser.isPresent()) {
			SessionErrors.add(renderRequest, "sessionExpired");
			return "/two-factor-authentication/error.jsp";
		}
		renderRequest.setAttribute("redirect", ParamUtil.getString(renderRequest, "redirect"));
		User user = twoFactorUser.get();
		long expirySeconds = twoFactorEmailService.getExpirySeconds(user.getCompanyId());

		renderRequest.setAttribute("twoFactorAuthenticationEmailUserHidedEmailAddress", twoFactorEmailService.getMaskedEmailAddress(user.getEmailAddress()));
		renderRequest.setAttribute("expiryInSeconds", expirySeconds);

		return "/two-factor-authentication/verify-by-email.jsp";
	}

}
