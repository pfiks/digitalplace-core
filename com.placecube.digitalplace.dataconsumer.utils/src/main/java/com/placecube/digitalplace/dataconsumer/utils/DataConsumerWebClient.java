package com.placecube.digitalplace.dataconsumer.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.Map.Entry;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.dataconsumer.service.DataTransformationService;

@Component(immediate = true, service = DataConsumerWebClient.class)
public class DataConsumerWebClient {

	private static final Log LOG = LogFactoryUtil.getLog(DataConsumerWebClient.class);

	@Reference
	private DataConsumerWebClientUtils dataConsumerWebClientUtils;

	@Reference
	private DataTransformationService dataTransformationService;

	public String execute(String url, String method, Map<String, String> headers, Map<String, String> parameterMap, String body) throws IOException {

		boolean methodRequiresBody = dataConsumerWebClientUtils.isHttpMethodRequiresBody(method);

		for (Entry<String, String> parameter : parameterMap.entrySet()) {
			url = url.replace(StringPool.OPEN_CURLY_BRACE + parameter.getKey() + StringPool.CLOSE_CURLY_BRACE, dataConsumerWebClientUtils.getEncodedParameter(parameter.getValue()));
		}

		if (!methodRequiresBody && !url.contains(StringPool.QUESTION)) {
			String paramString = dataConsumerWebClientUtils.getParamString(parameterMap);
			url = url + paramString;
		}

		LOG.debug("Opening connection from url: " + url);

		HttpURLConnection conn = dataConsumerWebClientUtils.openConnection(url);

		headers.entrySet().forEach(h -> {
			conn.setRequestProperty(h.getKey(), h.getValue());
			LOG.debug("Header: " + h.getKey() + ", " + h.getValue());
		});
		conn.setRequestMethod(method);

		LOG.debug("Body: " + GetterUtil.getString(body, StringPool.BLANK));

		dataConsumerWebClientUtils.appendBody(conn, methodRequiresBody ? body : null);

		String result = dataConsumerWebClientUtils.readConnectionOutput(conn);
		
		LOG.debug("Response: " + result);

		conn.disconnect();

		return result;
	}

}
