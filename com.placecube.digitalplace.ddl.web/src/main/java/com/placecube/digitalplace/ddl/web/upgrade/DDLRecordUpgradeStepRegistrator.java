package com.placecube.digitalplace.ddl.web.upgrade;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;

@Component(immediate = true, service = com.liferay.portal.upgrade.registry.UpgradeStepRegistrator.class)
public class DDLRecordUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new Upgrade_1_0_0_AddResourceActions());
	}
}