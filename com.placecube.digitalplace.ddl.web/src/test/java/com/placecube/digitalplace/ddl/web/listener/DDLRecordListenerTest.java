package com.placecube.digitalplace.ddl.web.listener;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.message.boards.service.MBMessageLocalService;
import com.liferay.portal.kernel.exception.PortalException;

public class DDLRecordListenerTest {

	@InjectMocks
	private DDLRecordListener ddlRecordListener;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private MBMessageLocalService mockMBMessageLocalService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void onAfterRemove_WhenExceptionRemovingTheDiscussionMessagesForTheRecord_ThenNoErrorIsThrown() throws PortalException {
		long id = 123;
		when(mockDDLRecord.getRecordId()).thenReturn(id);
		doThrow(new PortalException()).when(mockMBMessageLocalService).deleteDiscussionMessages(DDLRecord.class.getName(), id);

		try {
			ddlRecordListener.onAfterRemove(mockDDLRecord);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void onAfterRemove_WhenNoError_ThenRemovesTheDiscussionMessagesForTheRecord() throws PortalException {
		long id = 123;
		when(mockDDLRecord.getRecordId()).thenReturn(id);

		ddlRecordListener.onAfterRemove(mockDDLRecord);

		verify(mockMBMessageLocalService, times(1)).deleteDiscussionMessages(DDLRecord.class.getName(), id);
	}

}
