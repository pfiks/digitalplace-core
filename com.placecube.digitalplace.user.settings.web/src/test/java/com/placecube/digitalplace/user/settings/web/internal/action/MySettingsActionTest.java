package com.placecube.digitalplace.user.settings.web.internal.action;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.layout.service.LayoutRetrievalService;
import com.placecube.digitalplace.user.settings.web.constants.PortletKeys;

public class MySettingsActionTest extends PowerMockito {

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutRetrievalService mockLayoutRetrievalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletURLFactory mockPortletURLFactory;

	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	private HttpServletResponse mockResponse;

	@Mock
	private User mockUser;

	@Mock
	private LiferayPortletURL mockViewURL;

	@InjectMocks
	private MySettingsAction myNotificationsAction;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void execute_WhenErrorWhileSendingRedirect_ThenDoesNotThrowError() throws Exception {
		long groupId = 123;
		String viewURL = "viewURL";

		when(mockPortal.getUser(mockRequest)).thenReturn(mockUser);
		when(mockUser.getGroupId()).thenReturn(groupId);
		when(mockLayoutRetrievalService.getLayoutWithPortletId(groupId, PortletKeys.USER_SETTINGS, true)).thenReturn(Optional.of(mockLayout));
		when(mockPortletURLFactory.create(mockRequest, PortletKeys.USER_SETTINGS, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockViewURL);
		when(mockViewURL.toString()).thenReturn(viewURL);
		doThrow(new IOException()).when(mockResponse).sendRedirect(viewURL);

		try {
			myNotificationsAction.execute(mockRequest, mockResponse);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void execute_WhenUserDoesNotExists_ThenSendsRedirectToLoginPageWithCurrentURLEncodedInTheRedirectParam() throws Exception {
		String mainPath = "c";
		String currentURL = "current URL 2021 ^^ &&";

		when(mockPortal.getUser(mockRequest)).thenReturn(null);
		when(mockPortal.getPathMain()).thenReturn(mainPath);
		when(mockPortal.getCurrentURL(mockRequest)).thenReturn(currentURL);

		myNotificationsAction.execute(mockRequest, mockResponse);

		verify(mockResponse, times(1)).sendRedirect(mainPath + "/portal/login?redirect=" + URLEncoder.encode(currentURL, StandardCharsets.UTF_8.name()));
	}

	@Test
	public void execute_WhenUserExistsAndIsNotDefaultUserAndSettingsLayoutIsPresent_ThenSendsRedirectToLayoutWithMySettingsModule() throws Exception {
		long groupId = 123;
		String viewURL = "viewURL";

		when(mockPortal.getUser(mockRequest)).thenReturn(mockUser);
		when(mockUser.getGroupId()).thenReturn(groupId);
		when(mockLayoutRetrievalService.getLayoutWithPortletId(groupId, PortletKeys.USER_SETTINGS, true)).thenReturn(Optional.of(mockLayout));
		when(mockPortletURLFactory.create(mockRequest, PortletKeys.USER_SETTINGS, mockLayout, PortletRequest.RENDER_PHASE)).thenReturn(mockViewURL);
		when(mockViewURL.toString()).thenReturn(viewURL);

		myNotificationsAction.execute(mockRequest, mockResponse);

		verify(mockResponse, times(1)).sendRedirect(viewURL);
	}

	@Test
	public void execute_WhenUserExistsAndIsNotDefaultUserButSettingsLayoutIsNotPresent_ThenDoesNotSendAnyRedirects() throws Exception {
		long groupId = 123;

		when(mockPortal.getUser(mockRequest)).thenReturn(mockUser);
		when(mockUser.getGroupId()).thenReturn(groupId);
		when(mockLayoutRetrievalService.getLayoutWithPortletId(groupId, PortletKeys.USER_SETTINGS, true)).thenReturn(Optional.empty());

		myNotificationsAction.execute(mockRequest, mockResponse);

		verify(mockResponse, never()).sendRedirect(anyString());
	}

	@Test
	public void execute_WhenUserExistsButIsADefaultUser_ThenDoesNotSendAnyRedirects() throws Exception {
		when(mockPortal.getUser(mockRequest)).thenReturn(mockUser);
		when(mockUser.isGuestUser()).thenReturn(true);

		myNotificationsAction.execute(mockRequest, mockResponse);

		verify(mockResponse, never()).sendRedirect(anyString());
	}

}
