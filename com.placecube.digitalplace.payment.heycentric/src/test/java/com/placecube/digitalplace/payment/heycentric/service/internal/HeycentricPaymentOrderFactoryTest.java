package com.placecube.digitalplace.payment.heycentric.service.internal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.payment.heycentric.configuration.HeycentricPaymentCompanyConfiguration;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.model.PaymentContext;

@RunWith(MockitoJUnitRunner.class)
public class HeycentricPaymentOrderFactoryTest extends Mockito {

	private static final BigDecimal AMOUNT = BigDecimal.valueOf(23.4);

	private static final String AREA = "AREA";

	private static final String BACK_URL = "BACK URL";

	private static final String BACK_URL_ENCODED = "BACK+URL";

	private static final String BASE_URL = "BASE_URL";

	private static final String CLIENT = "CLIENT";

	private static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";

	private static final String ENTITY = "XPAYINT";

	private static final String PM_TTYP = "PM_TTYP";

	private static final String RETURN_URL = "RETURN URL";

	private static final String RETURN_URL_ENCODED = "RETURN+URL";

	private static final String SALES_DESCRIPTION = "SALES_DESCRIPTION";

	private static final String SALES_REFERENCE = "SALES_REFERENCE";

	private static final String TILL = "TILL";

	@InjectMocks
	private HeycentricPaymentOrderFactory heycentricPaymentOrderFactory;

	@Mock
	private HeycentricHashingService mockHeycentricHashingService;

	@Mock
	private HeycentricPaymentCompanyConfiguration mockHeycentricPaymentCompanyConfiguration;

	@Mock
	private PaymentContext mockPaymentContext;

	@Test
	public void getPaymentURL_WhenNoErrorAndIsHashSetFalse_ThenReturnPaymentURLWithNoHash() throws ConfigurationException {
		when(mockPaymentContext.getReturnURL()).thenReturn(RETURN_URL);
		when(mockPaymentContext.getBackURL()).thenReturn(BACK_URL);
		when(mockPaymentContext.getSalesReference()).thenReturn(SALES_REFERENCE);
		when(mockPaymentContext.getSalesDescription()).thenReturn(SALES_DESCRIPTION);
		when(mockPaymentContext.getAmount()).thenReturn(AMOUNT);
		when(mockPaymentContext.getEmailAddress()).thenReturn(EMAIL_ADDRESS);

		when(mockHeycentricPaymentCompanyConfiguration.baseURL()).thenReturn(BASE_URL);
		when(mockHeycentricPaymentCompanyConfiguration.client()).thenReturn(CLIENT);
		when(mockHeycentricPaymentCompanyConfiguration.entity()).thenReturn(ENTITY);
		when(mockHeycentricPaymentCompanyConfiguration.area()).thenReturn(AREA);
		when(mockHeycentricPaymentCompanyConfiguration.till()).thenReturn(TILL);
		when(mockHeycentricPaymentCompanyConfiguration.pmtTyp()).thenReturn(PM_TTYP);
		when(mockHeycentricPaymentCompanyConfiguration.isHash()).thenReturn(false);
		String result = heycentricPaymentOrderFactory.getPaymentURL(mockHeycentricPaymentCompanyConfiguration, mockPaymentContext);

		String expected = BASE_URL + "?" + HeycentricPaymentConstants.HEADER_PARAMETER_CLIENT + "=" + CLIENT + "&" + HeycentricPaymentConstants.HEADER_PARAMETER_ENTITY + "=" + ENTITY + "&"
				+ HeycentricPaymentConstants.HEADER_PARAMETER_AREA + "=" + AREA + "&" + HeycentricPaymentConstants.HEADER_PARAMETER_TILL + "=" + TILL + "&"
				+ HeycentricPaymentConstants.HEADER_PARAMETER_RURL + "=" + RETURN_URL_ENCODED + "&" + HeycentricPaymentConstants.HEADER_PARAMETER_BURL + "=" + BACK_URL_ENCODED + "&"
				+ HeycentricPaymentConstants.LINE_PARAMETER_PMTTYP + "=" + PM_TTYP + "&" + HeycentricPaymentConstants.LINE_PARAMETER_VAL1 + "=" + SALES_REFERENCE + "&"
				+ HeycentricPaymentConstants.LINE_PARAMETER_VAL1DESC + "=" + SALES_DESCRIPTION + "&" + HeycentricPaymentConstants.LINE_PARAMETER_AM + "=" + AMOUNT + "&"
				+ HeycentricPaymentConstants.HEADER_PARAMETER_EMAIL + "=" + EMAIL_ADDRESS + "&" + HeycentricPaymentConstants.HASH_PARAMETER + "=" + HeycentricPaymentConstants.HASH_PARAMETER_VALUE;

		assertEquals(expected, result);
	}

	@Test
	public void getPaymentURL_WhenNoErrorAndIsHashSetTrueAndPrivateKeNotIsSet_ThenThrowConfiguratioException() throws ConfigurationException {
		when(mockPaymentContext.getReturnURL()).thenReturn(RETURN_URL);
		when(mockPaymentContext.getBackURL()).thenReturn(BACK_URL);
		when(mockPaymentContext.getSalesReference()).thenReturn(SALES_REFERENCE);
		when(mockPaymentContext.getSalesDescription()).thenReturn(SALES_DESCRIPTION);
		when(mockPaymentContext.getAmount()).thenReturn(AMOUNT);
		when(mockPaymentContext.getEmailAddress()).thenReturn(EMAIL_ADDRESS);

		when(mockHeycentricPaymentCompanyConfiguration.baseURL()).thenReturn(BASE_URL);
		when(mockHeycentricPaymentCompanyConfiguration.client()).thenReturn(CLIENT);
		when(mockHeycentricPaymentCompanyConfiguration.entity()).thenReturn(ENTITY);
		when(mockHeycentricPaymentCompanyConfiguration.area()).thenReturn(AREA);
		when(mockHeycentricPaymentCompanyConfiguration.till()).thenReturn(TILL);
		when(mockHeycentricPaymentCompanyConfiguration.pmtTyp()).thenReturn(PM_TTYP);
		when(mockHeycentricPaymentCompanyConfiguration.isHash()).thenReturn(true);

		try {
			heycentricPaymentOrderFactory.getPaymentURL(mockHeycentricPaymentCompanyConfiguration, mockPaymentContext);
			fail("Expected ConfigurationException.");
		} catch (ConfigurationException e) {
			assertEquals("Heycentric is configured to use a hash but no private key is configured.", e.getMessage());
		}

	}

	@Test
	public void getPaymentURL_WhenNoErrorAndIsHashSetTrueAndPrivateKeyIsSet_ThenReturnPaymentURLWithCaculatedHash() throws ConfigurationException, NoSuchAlgorithmException {
		String rawHash = "RAWHASH123";
		String hexMd5Hash = "HEX1f2f";
		String base64Hash = "base64Hmkdh";
		String privateKey = "privateKey123";

		when(mockPaymentContext.getReturnURL()).thenReturn(RETURN_URL);
		when(mockPaymentContext.getBackURL()).thenReturn(BACK_URL);
		when(mockPaymentContext.getSalesReference()).thenReturn(SALES_REFERENCE);
		when(mockPaymentContext.getSalesDescription()).thenReturn(SALES_DESCRIPTION);
		when(mockPaymentContext.getAmount()).thenReturn(AMOUNT);
		when(mockPaymentContext.getEmailAddress()).thenReturn(EMAIL_ADDRESS);

		when(mockHeycentricPaymentCompanyConfiguration.baseURL()).thenReturn(BASE_URL);
		when(mockHeycentricPaymentCompanyConfiguration.client()).thenReturn(CLIENT);
		when(mockHeycentricPaymentCompanyConfiguration.entity()).thenReturn(ENTITY);
		when(mockHeycentricPaymentCompanyConfiguration.area()).thenReturn(AREA);
		when(mockHeycentricPaymentCompanyConfiguration.till()).thenReturn(TILL);
		when(mockHeycentricPaymentCompanyConfiguration.pmtTyp()).thenReturn(PM_TTYP);
		when(mockHeycentricPaymentCompanyConfiguration.isHash()).thenReturn(true);
		when(mockHeycentricPaymentCompanyConfiguration.privateKey()).thenReturn(privateKey);

		when(mockHeycentricHashingService.getHeycentricRawHashString(mockHeycentricPaymentCompanyConfiguration, AMOUNT.toString())).thenReturn(rawHash);
		when(mockHeycentricHashingService.rawHashToMd5Hex(rawHash)).thenReturn(hexMd5Hash);
		when(mockHeycentricHashingService.base64EncodeString(hexMd5Hash)).thenReturn(base64Hash);

		String result = heycentricPaymentOrderFactory.getPaymentURL(mockHeycentricPaymentCompanyConfiguration, mockPaymentContext);

		String expected = BASE_URL + "?" + HeycentricPaymentConstants.HEADER_PARAMETER_CLIENT + "=" + CLIENT + "&" + HeycentricPaymentConstants.HEADER_PARAMETER_ENTITY + "=" + ENTITY + "&"
				+ HeycentricPaymentConstants.HEADER_PARAMETER_AREA + "=" + AREA + "&" + HeycentricPaymentConstants.HEADER_PARAMETER_TILL + "=" + TILL + "&"
				+ HeycentricPaymentConstants.HEADER_PARAMETER_RURL + "=" + RETURN_URL_ENCODED + "&" + HeycentricPaymentConstants.HEADER_PARAMETER_BURL + "=" + BACK_URL_ENCODED + "&"
				+ HeycentricPaymentConstants.LINE_PARAMETER_PMTTYP + "=" + PM_TTYP + "&" + HeycentricPaymentConstants.LINE_PARAMETER_VAL1 + "=" + SALES_REFERENCE + "&"
				+ HeycentricPaymentConstants.LINE_PARAMETER_VAL1DESC + "=" + SALES_DESCRIPTION + "&" + HeycentricPaymentConstants.LINE_PARAMETER_AM + "=" + AMOUNT + "&"
				+ HeycentricPaymentConstants.HEADER_PARAMETER_EMAIL + "=" + EMAIL_ADDRESS + "&" + HeycentricPaymentConstants.HASH_PARAMETER + "=" + base64Hash;

		assertEquals(expected, result);
	}

}
