package com.placecube.digitalplace.authentication.twofactor.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.authentication.twofactor.model.TwoFactorAuthenticationMethod;

import aQute.bnd.annotation.metatype.Meta;
import aQute.bnd.annotation.metatype.Meta.Type;

@ExtendedObjectClassDefinition(category = "multi-factor-authentication", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration", localization = "content/Language", name = "authentication-two-factor")
public interface AuthenticationTwoFactorCompanyConfiguration {

	String TOTP_ALGORITHM_HMAC_SHA1 = "HmacSHA1";
	String TOTP_ALGORITHM_HMAC_SHA256 = "HmacSHA256";
	String TOTP_ALGORITHM_HMAC_SHA512 = "HmacSHA512";

	@Meta.AD(deflt = "90", description = "authentication-two-factor-resend-email-timeout-description", name = "authentication-two-factor-resend-email-timeout-name", required = false)
	public long resendEmailTimeout();

	@Meta.AD(deflt = TOTP_ALGORITHM_HMAC_SHA1, required = false, optionLabels = { TOTP_ALGORITHM_HMAC_SHA1, TOTP_ALGORITHM_HMAC_SHA256, TOTP_ALGORITHM_HMAC_SHA512 }, optionValues = {
			TOTP_ALGORITHM_HMAC_SHA1, TOTP_ALGORITHM_HMAC_SHA256,
			TOTP_ALGORITHM_HMAC_SHA512 }, type = Type.String, name = "authentication-two-factor-algorith-hmac-name", description = "authentication-two-factor-algorith-hmac-desc")
	String algorithmHmac();

	@Meta.AD(deflt = "[$TO_NAME$],<br /><br />\n" + "\n" + "Somebody requested a one-time password to access [$PORTAL_URL$]. <b>Ignore this email if you did not make the request.</b><br /><br />\n"
			+ "\n" + "Your one-time password is: <pre>[$ONE_TIME_PASSWORD$]</pre><br /><br />\n" + "\n" + "Sincerely,<br />\n"
			+ "[$PORTAL_URL$]", required = false, type = Type.String, name = "authentication-two-factor-email-body-name")
	String authenticationEmailBody();

	@Meta.AD(deflt = "[$PORTAL_URL$]: Your One-Time Password", required = false, type = Type.String, name = "authentication-two-factor-email-subject-name")
	String authenticationEmailSubject();

	@Meta.AD(deflt = TwoFactorAuthenticationMethod.TOTP_APP, required = false, optionLabels = { TwoFactorAuthenticationMethod.TOTP_APP, TwoFactorAuthenticationMethod.EMAIL }, optionValues = {
			TwoFactorAuthenticationMethod.TOTP_APP, TwoFactorAuthenticationMethod.EMAIL }, type = Type.String, name = "authentication-two-factor-authentication-method-name")
	String authenticationMethod();

	@Meta.AD(deflt = "30", required = false, type = Type.Integer, name = "authentication-two-factor-authenticator-code-duration-name", description = "authentication-two-factor-authenticator-code-duration-desc")
	int authenticatorCodeDuration();

	@Meta.AD(deflt = "6", required = false, optionLabels = { "6", "7", "8" }, optionValues = { "6", "7",
			"8" }, type = Type.Integer, name = "authentication-two-factor-authenticator-code-length-name", description = "authentication-two-factor-authenticator-code-length-desc")
	int authenticatorCodeLength();

	@Meta.AD(deflt = "false", required = false, type = Type.Boolean, name = "authentication-two-factor-enabled-name")
	boolean enabled();

	@Meta.AD(deflt = "Placecube", required = false, type = Type.String, name = "authentication-two-factor-qr-code-issuer-name", description = "authentication-two-factor-qr-code-issuer-desc")
	String qrCodeIssuer();

	@Meta.AD(deflt = "https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=%s", required = false, type = Type.String, name = "authentication-two-factor-qr-code-url-name", description = "authentication-two-factor-qr-code-url-desc")
	String qrCodeUrl();

	@Meta.AD(deflt = "-1", required = false, type = Type.String, name = "authentication-two-factor-enforced-role-ids-name", description = "authentication-two-factor-enforced-role-ids-desc")
	String[] twoFactorAuthenticationEnforcedRoleIds();

	@Meta.AD(deflt = "false", required = false, type = Type.Boolean, name = "authentication-two-factor-verify-reset-button-enabled-name")
	boolean verificationPageResetButtonEnabled();

}
