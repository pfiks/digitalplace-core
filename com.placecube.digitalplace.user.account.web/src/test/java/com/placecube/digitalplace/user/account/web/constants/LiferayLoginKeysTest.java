package com.placecube.digitalplace.user.account.web.constants;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.placecube.digitalplace.user.account.constants.LiferayLoginKeys;

public class LiferayLoginKeysTest {

	@Test
	public void verifyLiferayLoginKeys() {

		assertThat(LiferayLoginKeys.CREATE_ACCOUNT_CLASS_NAME, equalTo("com.liferay.login.web.internal.portlet.action.CreateAccountMVCRenderCommand"));
		assertThat(LiferayLoginKeys.FAST_LOGIN_PORTLET_KEY, equalTo("com_liferay_login_web_portlet_FastLoginPortlet"));
		assertThat(LiferayLoginKeys.LOGIN_PORTLET_KEY, equalTo("com_liferay_login_web_portlet_LoginPortlet"));
		assertThat(LiferayLoginKeys.MVC_COMMAND_NAME, equalTo("/login/create_account"));
	}

}
