package com.placecube.digitalplace.user.account.web.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Collections;
import java.util.Set;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfigurationFactory;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;
import com.placecube.digitalplace.userprofile.service.UserProfileFieldsService;

@PrepareForTest({ PrefsPropsUtil.class })
@RunWith(PowerMockRunner.class)
public class AccountConfigurationServiceImplTest {

	private static final long COMPANY_ID = 123L;

	private static final long GROUP_ID = 456L;

	@InjectMocks
	private AccountConfigurationServiceImpl accountConfigurationServiceImpl;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private Company mockCompany;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private CreateAccountConfigurationFactory mockCreateAccountConfigurationFactory;

	@Mock
	private Set<ExpandoField> mockExpandoFields;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletPreferences mockPreferences;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Mock
	private UserProfileFieldsService mockUserProfileFieldsService;

	@Before
	public void activateSetup() {
		initMocks(this);
		mockStatic(PrefsPropsUtil.class);
	}

	@Test(expected = UserAccountException.class)
	public void getAccountConfiguration_WhenExceptionRetrievingTheConfiguration_ThenThrowsPortletException() throws Exception {
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockCreateAccountConfigurationFactory.getCreateAccountConfiguration(mockGroup)).thenThrow(new ConfigurationException());

		accountConfigurationServiceImpl.getAccountConfiguration(mockAccountContext);
	}

	@Test
	public void getAccountConfiguration_WhenNoError_ThenReturnsTheConfiguration() throws Exception {
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockCreateAccountConfigurationFactory.getCreateAccountConfiguration(mockGroup)).thenReturn(mockCreateAccountConfiguration);

		CreateAccountConfiguration result = accountConfigurationServiceImpl.getAccountConfiguration(mockAccountContext);

		assertThat(result, sameInstance(mockCreateAccountConfiguration));
	}

	@Test(expected = UserAccountException.class)
	public void getAccountConfiguration_WithPortletRequestParameter_WhenExceptionRetrievingTheConfiguration_ThenThrowsPortletException() throws Exception {
		when(mockCreateAccountConfigurationFactory.getCreateAccountConfiguration(mockPortletRequest)).thenThrow(new ConfigurationException());

		accountConfigurationServiceImpl.getAccountConfiguration(mockAccountContext, mockPortletRequest);
	}

	@Test
	public void getAccountConfiguration_WithPortletRequestParameter_WhenNoError_ThenReturnsTheConfiguration() throws Exception {
		when(mockCreateAccountConfigurationFactory.getCreateAccountConfiguration(mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);

		CreateAccountConfiguration result = accountConfigurationServiceImpl.getAccountConfiguration(mockAccountContext, mockPortletRequest);

		assertThat(result, sameInstance(mockCreateAccountConfiguration));
	}

	@Test
	public void getAccountGroupConfiguration_WhenNoError_ThenReturnsTheConfiguration() throws Exception {
		when(mockAccountContext.getAccountConnectorModelId()).thenReturn("modelId");
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockCreateAccountConfigurationFactory.getCreateAccountConfiguration(mockGroup)).thenReturn(mockCreateAccountConfiguration);

		CreateAccountConfiguration result = accountConfigurationServiceImpl.getAccountConfiguration(mockAccountContext, null);

		assertThat(result, sameInstance(mockCreateAccountConfiguration));
	}

	@Test(expected = UserAccountException.class)
	public void getAccountPortletInstanceConfiguration_WhenExceptionRetrievingTheConfiguration_ThenThrowsPortletException() throws Exception {
		when(mockCreateAccountConfigurationFactory.getCreateAccountConfiguration(mockPortletRequest)).thenThrow(new ConfigurationException());

		accountConfigurationServiceImpl.getAccountConfiguration(mockAccountContext, mockPortletRequest);
	}

	@Test
	public void getAdditionalDetailsPreviousStep_WhenAddressEnabled_ThenReturnsAddressDetails() {
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getAdditionalDetailsPreviousStep(mockUserProfileFieldsConfiguration);

		assertThat(result, equalTo(MVCCommandKeys.ADDRESS_DETAILS));
	}

	@Test
	public void getAdditionalDetailsPreviousStep_WhenAddressIsNotEnabled_mockUserProfileFieldsConfigurationThenReturnsPersonalDetails() {
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(false);

		String result = accountConfigurationServiceImpl.getAdditionalDetailsPreviousStep(mockUserProfileFieldsConfiguration);

		assertThat(result, equalTo(MVCCommandKeys.PERSONAL_DETAILS));
	}

	@Test(expected = UserAccountException.class)
	public void getAddressNextStep_WhenErrorGettingConfiguration_ThenThrowException() throws ConfigurationException, UserAccountException {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		accountConfigurationServiceImpl.getAddressNextStep(mockAccountContext);
	}

	@Test
	public void getAddressNextStep_WhenExpandoFieldsIsNotEmpty_ThenReturnAdditionalDetailsPage() throws ConfigurationException, UserAccountException {

		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(mockExpandoFields);

		String result = accountConfigurationServiceImpl.getAddressNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getAddressNextStep_WhenHomePhoneNumberIsEnabled_ThenReturnAdditionalDetailsPage() throws ConfigurationException, UserAccountException {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getAddressNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getAddressNextStep_WhenJobTitleEnabled_ThenReturnAdditionalDetailsPage() throws ConfigurationException, UserAccountException {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getAddressNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getAddressNextStep_WhenMobilePhoneNumberEnabled_ThenReturnAdditionalDetailsPage() throws ConfigurationException, UserAccountException {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getAddressNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getAddressNextStep_WhenNoAdditionalDetailsAndNoSkipPassword_ThenReturnPasswordPage() throws ConfigurationException, UserAccountException {

		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(Collections.emptySet());

		when(mockAccountContext.isSkipPasswordStep()).thenReturn(false);

		String result = accountConfigurationServiceImpl.getAddressNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.PASSWORD_DETAILS, result);
	}

	@Test
	public void getAddressNextStep_WhenNoAdditionalDetailsAndSkipPassword_ThenReturnAccountSecurityPage() throws ConfigurationException, UserAccountException {

		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(Collections.emptySet());

		when(mockAccountContext.isSkipPasswordStep()).thenReturn(true);

		when(mockUserProfileFieldsConfiguration.twoFactorAuthenticationPromptEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getAddressNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ACCOUNT_SECURITY, result);
	}

	@Test(expected = UserAccountException.class)
	public void getAddressNextStep_WhenNoAdditionalDetailsAndSkipPasswordAndErrorGettingConfiguration_ThenThrowExceptionn() throws ConfigurationException, UserAccountException {

		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(Collections.emptySet());

		when(mockAccountContext.isSkipPasswordStep()).thenReturn(true);

		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		accountConfigurationServiceImpl.getAddressNextStep(mockAccountContext);
	}

	@Test
	public void getAddressNextStep_WhenNoAdditionalDetailsAndSkipPasswordAndNoTwoFactorEnabled_ThenReturnBlank() throws ConfigurationException, UserAccountException {

		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(Collections.emptySet());

		when(mockAccountContext.isSkipPasswordStep()).thenReturn(true);

		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.twoFactorAuthenticationPromptEnabled()).thenReturn(false);

		String result = accountConfigurationServiceImpl.getAddressNextStep(mockAccountContext);

		assertEquals(StringPool.BLANK, result);
	}

	@Test(expected = UserAccountException.class)
	public void getCompanyUserProfileFieldsConfiguration_WhenExceptionRetrievingTheConfiguration_ThenThrowsPortletException() throws Exception {
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);

		accountConfigurationServiceImpl.getCompanyUserProfileFieldsConfiguration(COMPANY_ID);
	}

	@Test
	public void getConfiguredExpandoFields_WhenNoError_ThenReturnsTheConfiguredExpandoFields() {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(mockExpandoFields);

		Set<ExpandoField> result = accountConfigurationServiceImpl.getConfiguredExpandoFields(mockAccountContext, mockUserProfileFieldsConfiguration);

		assertThat(result, sameInstance(mockExpandoFields));
	}

	@Test
	public void getEmailVerificationEnabled_WhenCompanySecurityStrangersVerifyPreferenceIsFalse_ThenReturnsFalse() {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getPreferences(COMPANY_ID)).thenReturn(mockPreferences);
		when(mockPreferences.getValue(PropsKeys.COMPANY_SECURITY_STRANGERS_VERIFY, null)).thenReturn(Boolean.FALSE.toString());

		assertThat(accountConfigurationServiceImpl.getEmailVerificationEnabled(mockAccountContext), equalTo(false));
	}

	@Test
	public void getEmailVerificationEnabled_WhenCompanySecurityStrangersVerifyPreferenceIsNull_ThenReturnsFalse() {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getPreferences(COMPANY_ID)).thenReturn(mockPreferences);
		when(mockPreferences.getValue(PropsKeys.COMPANY_SECURITY_STRANGERS_VERIFY, null)).thenReturn(null);

		assertThat(accountConfigurationServiceImpl.getEmailVerificationEnabled(mockAccountContext), equalTo(false));
	}

	@Test
	public void getEmailVerificationEnabled_WhenCompanySecurityStrangersVerifyPreferenceIsTrue_ThenReturnsTrue() {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getPreferences(COMPANY_ID)).thenReturn(mockPreferences);
		when(mockPreferences.getValue(PropsKeys.COMPANY_SECURITY_STRANGERS_VERIFY, null)).thenReturn(Boolean.TRUE.toString());

		assertThat(accountConfigurationServiceImpl.getEmailVerificationEnabled(mockAccountContext), equalTo(true));
	}

	@Test
	public void getPasswordPreviousStep_WhenExpandoFieldsConfigured_ThenReturnsAdditionalDetails() {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(mockExpandoFields);

		String result = accountConfigurationServiceImpl.getPasswordPreviousStep(mockAccountContext, mockUserProfileFieldsConfiguration);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getPasswordPreviousStep_WhenHomeNumberDisabledAndMobileNumberDisabledAndJobTitleDisabledAndNoExpandoFieldsConfiguredAndAddressIsEnabled_ThenReturnsAddressDetails() throws Exception {
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(false);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(Collections.emptySet());
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getPasswordPreviousStep(mockAccountContext, mockUserProfileFieldsConfiguration);

		assertEquals(MVCCommandKeys.ADDRESS_DETAILS, result);
	}

	@Test
	public void getPasswordPreviousStep_WhenHomeNumberDisabledAndMobileNumberDisabledAndJobTitleDisabledAndNoExpandoFieldsConfiguredAndAddressIsNotEnabled_ThenReturnsPersonalDetails()
			throws Exception {
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(false);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(Collections.emptySet());
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(false);

		String result = accountConfigurationServiceImpl.getPasswordPreviousStep(mockAccountContext, mockUserProfileFieldsConfiguration);

		assertEquals(MVCCommandKeys.PERSONAL_DETAILS, result);
	}

	@Test
	public void getPasswordPreviousStep_WhenHomeNumberEnabled_ThenReturnsAdditionalDetails() throws Exception {
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getPasswordPreviousStep(mockAccountContext, mockUserProfileFieldsConfiguration);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getPasswordPreviousStep_WhenJobTitleEnabled_ThenReturnsAdditionalDetails() throws Exception {
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getPasswordPreviousStep(mockAccountContext, mockUserProfileFieldsConfiguration);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getPasswordPreviousStep_WhenMobileNumberEnabled_ThenReturnsAdditionalDetails() throws Exception {
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getPasswordPreviousStep(mockAccountContext, mockUserProfileFieldsConfiguration);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getPersonalDetailsNextStep_WhenAddressIsEnabled_ThenReturnsAddressDetails() throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getPersonalDetailsNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ADDRESS_DETAILS, result);
	}

	@Test
	public void getPersonalDetailsNextStep_WhenAddressIsNotEnabledAndExpandoFieldsConfigured_ThenReturnsAdditionalDetails() throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(false);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(mockExpandoFields);

		String result = accountConfigurationServiceImpl.getPersonalDetailsNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getPersonalDetailsNextStep_WhenAddressIsNotEnabledANdHomeNumberEnabled_ThenReturnsAdditionalDetails() throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getPersonalDetailsNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getPersonalDetailsNextStep_WhenAddressIsNotEnabledAndJobTitleEnabled_ThenReturnsAdditionalDetails() throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getPersonalDetailsNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test
	public void getPersonalDetailsNextStep_WhenAddressIsNotEnabledAndMobileNumberEnabled_ThenReturnsAdditionalDetails() throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getPersonalDetailsNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ADDITIONAL_DETAILS, result);
	}

	@Test(expected = UserAccountException.class)
	public void getPersonalDetailsNextStep_WhenExceptionRetrievingTheConfiguration_ThenThrowsPortletException() throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		accountConfigurationServiceImpl.getPersonalDetailsNextStep(mockAccountContext);
	}

	@Test
	public void getPersonalDetailsNextStep_WhenHomeNumberDisabledAndMobileNumberDisabledAndJobTitleDisabledAndNoExpandoFieldsConfiguredAndAddressIsNotEnabled_ThenReturnsPasswordDetails()
			throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(false);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(Collections.emptySet());
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(false);
		when(mockAccountContext.isSkipPasswordStep()).thenReturn(false);

		String result = accountConfigurationServiceImpl.getPersonalDetailsNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.PASSWORD_DETAILS, result);
	}

	@Test
	public void getPersonalDetailsNextStep_WhenHomeNumberDisabledAndMobileNumberDisabledAndJobTitleDisabledAndNoExpandoFieldsConfiguredAndAddressIsNotEnabledAndSkipPasswordStep_ThenReturnAccountSecurityPage()
			throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(false);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(Collections.emptySet());
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(false);
		when(mockAccountContext.isSkipPasswordStep()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.twoFactorAuthenticationPromptEnabled()).thenReturn(true);

		String result = accountConfigurationServiceImpl.getPersonalDetailsNextStep(mockAccountContext);

		assertEquals(MVCCommandKeys.ACCOUNT_SECURITY, result);
	}

	@Test
	public void getPersonalDetailsNextStep_WhenHomeNumberDisabledAndMobileNumberDisabledAndJobTitleDisabledAndNoExpandoFieldsConfiguredAndAddressIsNotEnabledAndSkipPasswordStepAndNoTwoFactorAuthentication_ThenReturnConfirmationPage()
			throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(false);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(Collections.emptySet());
		when(mockUserProfileFieldsConfiguration.addressEnabled()).thenReturn(false);
		when(mockAccountContext.isSkipPasswordStep()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.twoFactorAuthenticationPromptEnabled()).thenReturn(false);

		String result = accountConfigurationServiceImpl.getPersonalDetailsNextStep(mockAccountContext);

		assertEquals("", result);
	}

	@Test(expected = UserAccountException.class)
	public void getUserProfileFieldsConfiguration_WhenExceptionRetrievingTheConfiguration_ThenThrowsPortletException() throws Exception {
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		accountConfigurationServiceImpl.getCompanyUserProfileFieldsConfiguration(COMPANY_ID);
	}

	@Test
	public void getUserProfileFieldsConfiguration_WhenNoError_ThenReturnsTheConfiguration() throws Exception {
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		UserProfileFieldsConfiguration result = accountConfigurationServiceImpl.getCompanyUserProfileFieldsConfiguration(COMPANY_ID);

		assertThat(result, sameInstance(mockUserProfileFieldsConfiguration));
	}
}
