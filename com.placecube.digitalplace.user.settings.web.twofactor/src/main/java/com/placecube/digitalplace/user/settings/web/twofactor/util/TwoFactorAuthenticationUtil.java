package com.placecube.digitalplace.user.settings.web.twofactor.util;

import java.util.Optional;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthenticationModel;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;

@Component(immediate = true, service = TwoFactorAuthenticationUtil.class)
public class TwoFactorAuthenticationUtil {

	@Reference
	private UserTwoFactorAuthenticationLocalService userTwoFactorAuthenticationLocalService;

	public String getSecretKey(RenderRequest renderRequest, Optional<UserTwoFactorAuthentication> userTwoFactorAuthenticationOpt) {
		String userSecretKey = "";
		boolean isReset = false;
		if (userTwoFactorAuthenticationOpt.isPresent()) {
			UserTwoFactorAuthentication userTwoFactorAuthentication = userTwoFactorAuthenticationOpt.get();
			userSecretKey = userTwoFactorAuthentication.getSecretKey();
			isReset = userTwoFactorAuthentication.getResetKey();
		}

		if (isReset || Validator.isNull(userSecretKey)) {
			renderRequest.setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, Boolean.TRUE);
			return userTwoFactorAuthenticationLocalService.generateSecretKey();
		} else {
			return userSecretKey;
		}
	}

	public boolean isTwoFactorEnabled(RenderRequest renderRequest, Optional<UserTwoFactorAuthentication> userTwoFactorAuthenticationOpt) {
		return GetterUtil.getBoolean(renderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM)) || userTwoFactorAuthenticationOpt
				.map(UserTwoFactorAuthenticationModel::getEnabled).orElse(false);
	}

}
