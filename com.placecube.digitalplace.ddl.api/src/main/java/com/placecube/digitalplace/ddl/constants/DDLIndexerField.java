package com.placecube.digitalplace.ddl.constants;

public final class DDLIndexerField {

	public static final String DDM_STRUCTURE_ID = "ddmStructureId";

	public static final String DDM_STRUCTURE_KEY = "ddmStructureKey";

	private DDLIndexerField() {
	}
}
