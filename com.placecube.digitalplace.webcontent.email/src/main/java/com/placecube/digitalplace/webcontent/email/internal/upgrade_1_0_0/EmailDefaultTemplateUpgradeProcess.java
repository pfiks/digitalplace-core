package com.placecube.digitalplace.webcontent.email.internal.upgrade_1_0_0;

import java.util.List;

import com.liferay.dynamic.data.mapping.kernel.DDMTemplate;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.service.permission.ModelPermissions;
import com.liferay.portal.kernel.service.permission.ModelPermissionsFactory;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.webcontent.email.constants.EmailConstants;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;

public class EmailDefaultTemplateUpgradeProcess extends UpgradeProcess {

	private DDMStructureLocalService ddmStructureLocalService;

	private EmailWebContentService emailWebContentService;

	private Portal portal;

	private UserLocalService userLocalService;

	public EmailDefaultTemplateUpgradeProcess(EmailWebContentService emailWebContentService, DDMStructureLocalService ddmStructureLocalService, Portal portal, UserLocalService userLocalService) {
		this.emailWebContentService = emailWebContentService;
		this.ddmStructureLocalService = ddmStructureLocalService;
		this.portal = portal;
		this.userLocalService = userLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		DynamicQuery dynamicQuery = ddmStructureLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("structureKey", EmailConstants.STRUCTURE_KEY));

		List<DDMStructure> ddmStructures = ddmStructureLocalService.dynamicQuery(dynamicQuery);

		ServiceContext serviceContext = createServiceContext();
		for (DDMStructure ddmStructure : ddmStructures) {

			serviceContext.setCompanyId(ddmStructure.getCompanyId());
			serviceContext.setScopeGroupId(ddmStructure.getGroupId());

			emailWebContentService.getOrCreateDDMTemplate(serviceContext);
		}
	}

	private ServiceContext createServiceContext() throws PortalException {
		ServiceContext serviceContext = new ServiceContext();

		long companyId = portal.getDefaultCompanyId();
		long defaultUserId = userLocalService.getDefaultUser(companyId).getUserId();

		serviceContext.setUserId(defaultUserId);
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);

		ModelPermissions modelPermissions = ModelPermissionsFactory.createWithDefaultPermissions(DDMTemplate.class.getName());
		serviceContext.setModelPermissions(modelPermissions);

		return serviceContext;
	}
}
