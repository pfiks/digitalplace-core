package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.contributor;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service.AdvancePeopleSearchConfigurationService;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service.AdvancePeopleSearchQueryHelper;
import com.placecube.digitalplace.search.web.service.ObjectFactoryUtil;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.ADVANCE_PEOPLE_SEARCH, service = SharedSearchContributor.class)
public class AdvancePeopleSearchPortletSharedSearchContributor implements SharedSearchContributor {

	private static final Log LOG = LogFactoryUtil.getLog(AdvancePeopleSearchPortletSharedSearchContributor.class);

	@Reference
	private AdvancePeopleSearchConfigurationService advancePeopleSearchConfigurationService;

	@Reference
	private AdvancePeopleSearchQueryHelper advancePeopleSearchQueryHelper;

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {
		addFilters(sharedSearchContributorSettings);
	}

	private void addFilter(SharedSearchContributorSettings sharedSearchContributorSettings, boolean applyPrivacy, String field) {
		sharedSearchContributorSettings.getActiveFiltersForField(field).stream().map(SharedActiveFilter::getValue).map(String::toLowerCase).filter(Validator::isNotNull).forEach(value -> {
			if ("biography".equals(field)) {
				addFieldQuery(sharedSearchContributorSettings, applyPrivacy, "description", value);
				addFieldQuery(sharedSearchContributorSettings, applyPrivacy, "content", value);
			} else {
				addFieldQuery(sharedSearchContributorSettings, applyPrivacy, field, value);
			}
		});
	}

	private void addFieldQuery(SharedSearchContributorSettings sharedSearchContributorSettings, boolean applyPrivacy, String field, String value) {
		if (Validator.isNull(field) || Validator.isNull(value)) {
			return;
		}

		try {

			BooleanQuery fieldQuery = ObjectFactoryUtil.createBooleanQuery();
			BooleanQuery valueSplitBySpaceQuery = advancePeopleSearchQueryHelper.getValueAsTermsQuery(field, value);
			fieldQuery.add(valueSplitBySpaceQuery, BooleanClauseOccur.SHOULD);

			BooleanQuery valueAsSingleTermQuery = advancePeopleSearchQueryHelper.getValueAsExactMatchQuery(field, value);
			fieldQuery.add(valueAsSingleTermQuery, BooleanClauseOccur.SHOULD);

			sharedSearchContributorSettings.addBooleanQuery(advancePeopleSearchQueryHelper.createBooleanClause(fieldQuery, BooleanClauseOccur.MUST));
		} catch (ParseException e) {
			LOG.error(String.format("Unable to create field query fieldname:%s value:%s", field, value), e);
		}

		if (applyPrivacy) {
			sharedSearchContributorSettings.addBooleanQueryForMultipleValues("privacy_" + field, getSearchUserIds(sharedSearchContributorSettings), BooleanClauseOccur.MUST);
		}
	}

	private void addFilters(SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<PortletPreferences> prefs = sharedSearchContributorSettings.getPortletPreferences();
		if (prefs.isPresent()) {
			PortletPreferences portletPreferences = prefs.get();
			ThemeDisplay themeDisplay = sharedSearchContributorSettings.getThemeDisplay();
			String configuredSearchFieldsJSON = portletPreferences.getValue("searchFields", StringPool.BLANK);
			boolean applyPrivacy = Boolean.parseBoolean(portletPreferences.getValue("applyPrivacy", StringPool.FALSE));
			advancePeopleSearchConfigurationService.getSearchFields(themeDisplay.getCompanyId(), themeDisplay.getLocale(), configuredSearchFieldsJSON)
					.forEach(userSearchField -> addFilter(sharedSearchContributorSettings, applyPrivacy, userSearchField.getField()));
		} else {
			throw new SystemException("Preferences not found");
		}
	}

	private List<String> getSearchUserIds(SharedSearchContributorSettings sharedSearchContributorSettings) {
		return Arrays.asList("0", String.valueOf(sharedSearchContributorSettings.getThemeDisplay().getUserId()));
	}
}
