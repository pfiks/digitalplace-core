package com.placecube.digitalplace.externalpage.web.display.context;

import com.liferay.frontend.taglib.clay.servlet.taglib.display.context.BaseManagementToolbarDisplayContext;
import com.liferay.frontend.taglib.clay.servlet.taglib.util.CreationMenu;
import com.liferay.frontend.taglib.clay.servlet.taglib.util.DropdownItem;
import com.liferay.frontend.taglib.clay.servlet.taglib.util.DropdownItemList;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.url.builder.PortletURLBuilder;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HashMapBuilder;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.externalpage.web.constants.ViewConstants;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ExternalManagementToolbarDisplayContext extends BaseManagementToolbarDisplayContext {

	private final ThemeDisplay themeDisplay;

	public ExternalManagementToolbarDisplayContext(LiferayPortletRequest liferayPortletRequest, LiferayPortletResponse liferayPortletResponse, HttpServletRequest httpServletRequest) {
		super(liferayPortletRequest, liferayPortletResponse, httpServletRequest);

		themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@SuppressWarnings("serial")
	public List<DropdownItem> getActionItemsDropdownItems() {
		return new DropdownItemList() {

			{
				add(dropdownItem -> {
					dropdownItem.putData("action", ViewConstants.DELETE_ENTRIES_ACTION);
					dropdownItem.setIcon("trash");
					dropdownItem.setLabel(LanguageUtil.get(httpServletRequest, "delete"));
					dropdownItem.setQuickAction(true);
				});
			}
		};
	}

	@Override
	public Map<String, Object> getAdditionalProps() {

		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil.create(httpServletRequest);

		return HashMapBuilder
				.<String, Object>put("deleteEntriesURL",
						() -> PortletURLBuilder.create(requestBackedPortletURLFactory.createActionURL(PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET)).setActionName(MVCCommandKeys.EXTERNAL_PAGE_DELETE)
								.setRedirect(themeDisplay.getURLCurrent()).buildString())
				.put("searchContainerName", ViewConstants.SEARCH_CONTAINER_ID).put("entriesIdsParameterName", PortletRequestKeys.ROW_IDS)
				.put("deleteEntriesActionName", ViewConstants.DELETE_ENTRIES_ACTION).build();
	}

	@Override
	public CreationMenu getCreationMenu() {

		return new CreationMenu() {

			private static final long serialVersionUID = 1L;

			{
				addDropdownItem(dropdownItem -> {
					dropdownItem.setHref(liferayPortletResponse.createRenderURL(), "mvcRenderCommandName", MVCCommandKeys.EXTERNAL_PAGE_ADD, "redirect", currentURLObj.toString());
					dropdownItem.setLabel(LanguageUtil.get(httpServletRequest, "import-pages"));
				});

			}
		};
	}

	@Override
	public String getSearchContainerId() {
		return ViewConstants.SEARCH_CONTAINER_ID;
	}
}
