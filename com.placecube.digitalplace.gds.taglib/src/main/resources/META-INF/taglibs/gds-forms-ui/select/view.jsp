<%@ include file="../init.jsp"%>

<c:set var="hasError" value="${not empty errorMessage}"/>

<div class="govuk-form-group ${hasError ? 'govuk-form-group--error' : ''}" id="${portletNamespace}_field_${fieldName}">
		
	<label class="govuk-label" for="${portletNamespace}${fieldName}">
		<liferay-ui:message key="${fieldLabel}"/>
		<%@ include file="../fieldOptionalLabel.jspf" %>
	</label>
	
	<%@ include file="../fieldErrorMessage.jspf" %>
	
	<select class="govuk-select ${hasError ? 'govuk-select--error' : ''}" id="${portletNamespace}${fieldName}" name="${portletNamespace}${fieldName}">
		<c:choose>
			<c:when test="${showEmptyOption && empty placeholder}">
				<option value="" />
			</c:when>
			<c:otherwise>			
				<c:if test="${not empty placeholder}">
					<option value="${not empty placeholderValue ? placeholderValue : ''}"
						${placeholderValue eq fieldValue ? 'selected' : ''}
						${empty placeholderValue ? 'disabled hidden' : ''}>${placeholder}</option>
				</c:if>				
			</c:otherwise>		
		</c:choose>
		<c:forEach items="${fieldOptions}" var="availableOption">
			<option value="${availableOption.key}" ${availableOption.key.equals(fieldValue) ? 'selected' : ''}>
				<liferay-ui:message key="${availableOption.value}"/>
			</option>
		</c:forEach>
	</select>
</div>
