package com.placecube.digitalplace.userimport.admin.web.constants;

public enum NotificationArticle {

	IMPORT_COMPLETED("/dependencies/email-import-completed.xml", "USER-IMPORT-COMPLETED", "User data import completed"),

	IMPORT_FAILED("/dependencies/email-import-failed.xml", "USER-IMPORT-FAILED", "User data import failed");

	private final String articleId;
	private final String articleTitle;
	private final String path;

	private NotificationArticle(String path, String articleId, String articleTitle) {
		this.path = path;
		this.articleId = articleId;
		this.articleTitle = articleTitle;
	}

	public final String getArticleId() {
		return articleId;
	}

	public final String getArticleTitle() {
		return articleTitle;
	}

	public final String getPath() {
		return path;
	}

}
