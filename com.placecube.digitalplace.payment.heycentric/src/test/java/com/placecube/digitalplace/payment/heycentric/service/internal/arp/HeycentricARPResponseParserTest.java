package com.placecube.digitalplace.payment.heycentric.service.internal.arp;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricARPConstants;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.model.PaymentResponse;

@RunWith(MockitoJUnitRunner.class)
public class HeycentricARPResponseParserTest {

	private static final String PAYMENT_REFERENCE = "paymentReference";

	@InjectMocks
	private HeycentricARPResponseParser heycentricARPResponseParser;

	@Mock
	private JSONObject mockJsonObject;

	@Test
	public void parsePaymentResponse_WhenPayloadResultIsAuthorised_ThenReturnsSuccessPaymentResponse() {
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_RESULT)).thenReturn(HeycentricARPConstants.AUTHORISED);
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_RECEIPT)).thenReturn(PAYMENT_REFERENCE);

		PaymentResponse result = heycentricARPResponseParser.parsePaymentResponse(mockJsonObject);

		assertThat(result.getStatus().getValue(), equalTo(PaymentStatus.success().getValue()));
		assertThat(result.getStatus().getPaymentUniqueReferenceNumber(), equalTo(PAYMENT_REFERENCE));
	}

	@Test
	public void parsePaymentResponse_WhenPayloadResultIsIncomplete_ThenReturnsAbortedPaymentResponse() {
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_RESULT)).thenReturn(HeycentricARPConstants.INCOMPLETE);

		PaymentResponse result = heycentricARPResponseParser.parsePaymentResponse(mockJsonObject);

		assertThat(result.getStatus().getValue(), equalTo(PaymentStatus.aborted().getValue()));
	}

	@Test
	public void parsePaymentResponse_WhenPayloadResultIsNotAuthorisedOrIncomplete_ThenReturnsPaymentErrorResponse() {
		when(mockJsonObject.getString(HeycentricPaymentConstants.JSON_PAYLOAD_RESULT)).thenReturn("error");

		PaymentResponse result = heycentricARPResponseParser.parsePaymentResponse(mockJsonObject);

		assertThat(result.getStatus().getValue(), equalTo(PaymentStatus.paymentError().getValue()));
	}
}