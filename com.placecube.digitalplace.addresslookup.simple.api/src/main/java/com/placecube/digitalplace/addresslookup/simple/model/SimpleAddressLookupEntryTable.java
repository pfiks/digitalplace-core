/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

/**
 * The table class for the &quot;Placecube_AddressLookupSimple_SimpleAddressLookupEntry&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see SimpleAddressLookupEntry
 * @generated
 */
public class SimpleAddressLookupEntryTable
	extends BaseTable<SimpleAddressLookupEntryTable> {

	public static final SimpleAddressLookupEntryTable INSTANCE =
		new SimpleAddressLookupEntryTable();

	public final Column<SimpleAddressLookupEntryTable, String> uuid =
		createColumn("uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<SimpleAddressLookupEntryTable, Long> companyId =
		createColumn(
			"companyId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<SimpleAddressLookupEntryTable, String> uprn =
		createColumn("uprn", String.class, Types.VARCHAR, Column.FLAG_PRIMARY);
	public final Column<SimpleAddressLookupEntryTable, String> addressLine1 =
		createColumn(
			"addressLine1", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<SimpleAddressLookupEntryTable, String> addressLine2 =
		createColumn(
			"addressLine2", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<SimpleAddressLookupEntryTable, String> addressLine3 =
		createColumn(
			"addressLine3", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<SimpleAddressLookupEntryTable, String> city =
		createColumn("city", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<SimpleAddressLookupEntryTable, String> postcode =
		createColumn(
			"postcode", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);

	private SimpleAddressLookupEntryTable() {
		super(
			"Placecube_AddressLookupSimple_SimpleAddressLookupEntry",
			SimpleAddressLookupEntryTable::new);
	}

}