package com.placecube.digitalplace.user.serviceoverride.internal.override;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import com.placecube.digitalplace.user.serviceoverride.model.AuthenticationActionContext;
import com.placecube.digitalplace.user.serviceoverride.constants.AuthenticationMode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.TicketLocalService;
import com.liferay.portal.kernel.service.UserLocalServiceWrapper;
import com.placecube.digitalplace.user.serviceoverride.internal.util.ReindexUserUtil;
import com.placecube.digitalplace.user.serviceoverride.internal.util.ServiceContextDetailsUtil;
import com.placecube.digitalplace.user.serviceoverride.internal.util.UserServiceOverrideActionUtil;

@Component(immediate = true, service = ServiceWrapper.class)
public class UserLocalServiceOverride extends UserLocalServiceWrapper {

	private static final Log LOG = LogFactoryUtil.getLog(UserLocalServiceOverride.class);

	@Reference(unbind = "-")
	private ReindexUserUtil reindexUserUtil;

	@Reference(unbind = "-")
	private ServiceContextDetailsUtil serviceContextDetailsUtil;

	@Reference(unbind = "-")
	private TicketLocalService ticketLocalService;

	@Reference(unbind = "-")
	private UserServiceOverrideActionUtil userServiceOverrideActionUtil;

	public UserLocalServiceOverride() {
		super(null);
	}

	@Override
	public int authenticateByEmailAddress(long companyId, String emailAddress, String password, Map<String, String[]> headerMap, Map<String, String[]> parameterMap, Map<String, Object> resultsMap) throws PortalException {

		int authenticationResult = super.authenticateByEmailAddress(companyId, emailAddress, password, headerMap, parameterMap, resultsMap);
		AuthenticationActionContext authenticationActionContext = new AuthenticationActionContext(companyId, AuthenticationMode.EMAIL_ADDRESS, authenticationResult);
		authenticationActionContext.setEmailAddress(emailAddress);
		userServiceOverrideActionUtil.executeActionsAfterUserAuthentication(authenticationActionContext);

		return authenticationResult;

	}

	@Override
	public int authenticateByScreenName(long companyId, String screenName, String password, Map<String, String[]> headerMap, Map<String, String[]> parameterMap, Map<String, Object> resultsMap) throws PortalException {

		int authenticationResult = super.authenticateByScreenName(companyId, screenName, password, headerMap, parameterMap, resultsMap);
		AuthenticationActionContext authenticationActionContext = new AuthenticationActionContext(companyId, AuthenticationMode.SCREEN_NAME, authenticationResult);
		authenticationActionContext.setScreenName(screenName);
		userServiceOverrideActionUtil.executeActionsAfterUserAuthentication(authenticationActionContext);

		return authenticationResult;

	}

	@Override
	public int authenticateByUserId(long companyId, long userId, String password, Map<String, String[]> headerMap, Map<String, String[]> parameterMap, Map<String, Object> resultsMap) throws PortalException {

		int authenticationResult = super.authenticateByUserId(companyId, userId, password, headerMap, parameterMap, resultsMap);
		AuthenticationActionContext authenticationActionContext = new AuthenticationActionContext(companyId, AuthenticationMode.USER_ID, authenticationResult);
		authenticationActionContext.setUserId(userId);
		userServiceOverrideActionUtil.executeActionsAfterUserAuthentication(authenticationActionContext);

		return authenticationResult;

	}

	@Override
	public boolean addGroupUser(long groupId, long userId) {
		boolean result = super.addGroupUser(groupId, userId);

		userServiceOverrideActionUtil.executeActionsForUserAddedToGroup(userId, groupId);

		return result;
	}

	@Override
	public boolean addGroupUser(long groupId, User user) {
		boolean result = super.addGroupUser(groupId, user);

		userServiceOverrideActionUtil.executeActionsForUserAddedToGroup(user.getUserId(), groupId);

		return result;
	}

	@Override
	public boolean addGroupUsers(long groupId, List<User> users) throws PortalException {
		boolean result = super.addGroupUsers(groupId, users);

		users.forEach(user -> userServiceOverrideActionUtil.executeActionsForUserAddedToGroup(user.getUserId(), groupId));

		return result;
	}

	@Override
	public boolean addGroupUsers(long groupId, long[] userIds) throws PortalException {
		boolean result = super.addGroupUsers(groupId, userIds);

		for (long userId : userIds) {
			userServiceOverrideActionUtil.executeActionsForUserAddedToGroup(userId, groupId);
		}

		return result;
	}

	@Override
	public User addUser(long creatorUserId, long companyId, boolean autoPassword, String password1, String password2, boolean autoScreenName, String screenName, String emailAddress, Locale locale,
			String firstName, String middleName, String lastName, long prefixId, long suffixId, boolean male, int birthdayMonth, int birthdayDay, int birthdayYear, String jobTitle,  int type, long[] groupIds,
			long[] organizationIds, long[] roleIds, long[] userGroupIds, boolean sendEmail, ServiceContext serviceContext) throws PortalException {

		serviceContextDetailsUtil.populateMissingServiceContextDetails(companyId, serviceContext);

		User userCreated = super.addUser(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, locale, firstName, middleName, lastName, prefixId,
				suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, type, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, serviceContext);

		return userServiceOverrideActionUtil.executeActionsForUserCreation(userCreated, Optional.ofNullable(serviceContext));
	}

	@Override
	public User addUser(User user) {
		User userCreated = super.addUser(user);
		return userServiceOverrideActionUtil.executeActionsForUserCreation(userCreated, Optional.empty());
	}

	@Override
	public User addUserWithWorkflow(long creatorUserId, long companyId, boolean autoPassword, String password1, String password2, boolean autoScreenName, String screenName, String emailAddress,
			Locale locale, String firstName, String middleName, String lastName, long prefixId, long suffixId, boolean male, int birthdayMonth, int birthdayDay, int birthdayYear, String jobTitle, int type,
			long[] groupIds, long[] organizationIds, long[] roleIds, long[] userGroupIds, boolean sendEmail, ServiceContext serviceContext) throws PortalException {

		serviceContextDetailsUtil.populateMissingServiceContextDetails(companyId, serviceContext);

		User userCreated = super.addUserWithWorkflow(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, locale, firstName, middleName, lastName,
				prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, type, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, serviceContext);
		return userServiceOverrideActionUtil.executeActionsForUserCreation(userCreated, Optional.ofNullable(serviceContext));
	}

	@Override
	public void deleteGroupUser(long groupId, long userId) {
		super.deleteGroupUser(groupId, userId);

		userServiceOverrideActionUtil.executeActionsForUserRemovedFromGroup(userId, groupId, Optional.empty());
	}

	@Override
	public void deleteGroupUser(long groupId, User user) {
		super.deleteGroupUser(groupId, user);

		userServiceOverrideActionUtil.executeActionsForUserRemovedFromGroup(user.getUserId(), groupId, Optional.empty());
	}

	@Override
	public void deleteGroupUsers(long groupId, List<User> users) {
		super.deleteGroupUsers(groupId, users);

		users.forEach(user -> userServiceOverrideActionUtil.executeActionsForUserRemovedFromGroup(user.getUserId(), groupId, Optional.empty()));
	}

	@Override
	public void deleteGroupUsers(long groupId, long[] userIds) {
		super.deleteGroupUsers(groupId, userIds);

		for (long userId : userIds) {
			userServiceOverrideActionUtil.executeActionsForUserRemovedFromGroup(userId, groupId, Optional.empty());
		}
	}

	@Override
	public User deleteUser(long userId) throws PortalException {
		User userDeleted = super.deleteUser(userId);
		userServiceOverrideActionUtil.executeActionsForUserDeletion(userDeleted);
		return userDeleted;
	}

	@Override
	public User deleteUser(User user) throws PortalException {
		User userDeleted = super.deleteUser(user);
		userServiceOverrideActionUtil.executeActionsForUserDeletion(userDeleted);
		return userDeleted;
	}

	@Override
	public void setGroupUsers(long groupId, long[] userIds) {
		super.setGroupUsers(groupId, userIds);

		for (long userId : userIds) {
			userServiceOverrideActionUtil.executeActionsForUserAddedToGroup(userId, groupId);
		}
	}

	@Override
	public void unsetGroupUsers(long groupId, long[] userIds, ServiceContext serviceContext) throws PortalException {
		super.unsetGroupUsers(groupId, userIds, serviceContext);

		for (long userId : userIds) {
			userServiceOverrideActionUtil.executeActionsForUserRemovedFromGroup(userId, groupId, Optional.ofNullable(serviceContext));
		}
	}

	@Override
	public User updateAgreedToTermsOfUse(long userId, boolean agreedToTermsOfUse) throws PortalException {
		User updatedUser = super.updateAgreedToTermsOfUse(userId, agreedToTermsOfUse);
		reindexUserUtil.reindexUser(updatedUser);
		return updatedUser;
	}

	@Override
	public User updateEmailAddressVerified(long userId, boolean emailAddressVerified) throws PortalException {
		User updatedUser = super.updateEmailAddressVerified(userId, emailAddressVerified);
		reindexUserUtil.reindexUser(updatedUser);
		return updatedUser;
	}

	@Override
	public User updateUser(long userId, String oldPassword, String newPassword1, String newPassword2, boolean passwordReset, String reminderQueryQuestion, String reminderQueryAnswer,
			String screenName, String emailAddress, boolean portrait, byte[] portraitBytes, String languageId, String timeZoneId, String greeting, String comments, String firstName, String middleName,
			String lastName, long prefixId, long suffixId, boolean male, int birthdayMonth, int birthdayDay, int birthdayYear, String smsSn, String facebookSn, String jabberSn, String skypeSn,
			String twitterSn, String jobTitle, long[] groupIds, long[] organizationIds, long[] roleIds, List<UserGroupRole> userGroupRoles, long[] userGroupIds, ServiceContext serviceContext)
			throws PortalException {

		List<Long> originalGroupIds = userServiceOverrideActionUtil.getGroupIds(super.fetchUser(userId));

		User updatedUser = super.updateUser(userId, oldPassword, newPassword1, newPassword2, passwordReset, reminderQueryQuestion, reminderQueryAnswer, screenName, emailAddress, portrait,
				portraitBytes, languageId, timeZoneId, greeting, comments, firstName, middleName, lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, smsSn, facebookSn,
				jabberSn, skypeSn, twitterSn, jobTitle, groupIds, organizationIds, roleIds, userGroupRoles, userGroupIds, serviceContext);

		userServiceOverrideActionUtil.executeActionsForUpdateUser(updatedUser, Optional.of(serviceContext));

		List<Long> updatedGroupIds = userServiceOverrideActionUtil.getGroupIds(updatedUser);

		List<Long> groupIdsAdded = userServiceOverrideActionUtil.getGroupIdsAdded(originalGroupIds, updatedGroupIds);
		groupIdsAdded.forEach(groupId -> userServiceOverrideActionUtil.executeActionsForUserAddedToGroup(userId, groupId));

		List<Long> groupIdsRemoved = userServiceOverrideActionUtil.getGroupIdsRemoved(originalGroupIds, updatedGroupIds);
		groupIdsRemoved.forEach(groupId -> userServiceOverrideActionUtil.executeActionsForUserRemovedFromGroup(userId, groupId, Optional.of(serviceContext)));

		return updatedUser;
	}

	@Override
	public void verifyEmailAddress(String ticketKey) throws PortalException {

		long userId = getUserIdFromTicket(ticketKey);

		super.verifyEmailAddress(ticketKey);

		userServiceOverrideActionUtil.executeActionsAfterVerifyEmailAddress(userId);

		reindexUserUtil.reindexUser(userId);
	}

	private long getUserIdFromTicket(String ticketKey) {
		try {
			Ticket ticket = ticketLocalService.getTicket(ticketKey);
			return ticket.getClassPK();
		} catch (Exception e) {
			LOG.warn("Unable to retrieve user from ticketKey: " + ticketKey + " - " + e.getMessage());
			return 0L;
		}
	}

}
