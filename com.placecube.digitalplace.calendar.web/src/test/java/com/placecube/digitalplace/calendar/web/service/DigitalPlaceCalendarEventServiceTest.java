package com.placecube.digitalplace.calendar.web.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.MockitoAnnotations.initMocks;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarConnector;
import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarEvent;
import com.placecube.digitalplace.calendar.web.constants.DigitalPlaceCalendarConfigurationConstants;
import com.placecube.digitalplace.calendar.web.portlet.configuration.DigitalPlaceCalendarInstanceConfiguration;
import com.placecube.digitalplace.calendar.web.registry.DigitalPlaceCalendarConnectorRegistry;

public class DigitalPlaceCalendarEventServiceTest extends PowerMockito {

	@InjectMocks
	private DigitalPlaceCalendarEventService digitalPlaceCalendarEventService;

	@Mock
	private DigitalPlaceCalendarConnectorRegistry mockDigitalPlaceCalendarConnectorRegistry;

	@Mock
	private DigitalPlaceCalendarInstanceConfiguration mockConfiguration;

	@Mock
	private DigitalPlaceCalendarConfigurationService mockDigitalPlaceCalendarConfigurationService;

	@Mock
	private DigitalPlaceCalendarConnector mockConnector1;

	@Mock
	private DigitalPlaceCalendarConnector mockConnector2;

	@Mock
	private DigitalPlaceCalendarEvent mockCalendarEvent1;

	@Mock
	private DigitalPlaceCalendarEvent mockCalendarEvent2;

	@Mock
	private DigitalPlaceCalendarEvent mockCalendarEvent3;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getCalendarEvents_WhenNoError_ThenReturnsCalendarEventsFromAllConnectors() throws PortalException {
		long userId = 123;
		long[] groupIds = new long[] { 456 };
		LocalDateTime startDate = LocalDateTime.now();
		LocalDateTime endDate = startDate.plus(2, ChronoUnit.DAYS);

		List<DigitalPlaceCalendarConnector> calendarConnectors = Arrays.asList(mockConnector1, mockConnector2);

		when(mockDigitalPlaceCalendarConnectorRegistry.getCalendarConnectors()).thenReturn(calendarConnectors);
		when(mockConnector1.getCalendarEvents(userId, groupIds, startDate, endDate, mockThemeDisplay)).thenReturn(Arrays.asList(mockCalendarEvent1, mockCalendarEvent2));
		when(mockConnector2.getCalendarEvents(userId, groupIds, startDate, endDate, mockThemeDisplay)).thenReturn(Collections.singletonList(mockCalendarEvent3));

		List<DigitalPlaceCalendarEvent> result = digitalPlaceCalendarEventService.getCalendarEvents(userId, groupIds, startDate, endDate, mockThemeDisplay);

		assertThat(result, containsInAnyOrder(mockCalendarEvent1, mockCalendarEvent2, mockCalendarEvent3));
	}

	@Test
	public void getCalendarScopeGroupIds_WhenNoErrorAndScopeIsGroup_ThenReturnsScopeGroupId() throws ConfigurationException {
		long scopeGroupId = 123;

		when(mockDigitalPlaceCalendarConfigurationService.getDigitalPlaceCalendarConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.scope()).thenReturn(DigitalPlaceCalendarConfigurationConstants.SCOPE_GROUP);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);

		long[] result = digitalPlaceCalendarEventService.getCalendarScopeGroupIds(mockThemeDisplay);

		assertThat(result.length, equalTo(1));
		assertThat(result[0], equalTo(scopeGroupId));

	}

	@Test
	public void getCalendarScopeGroupIds_WhenNoErrorAndScopeIsNotGroup_ThenReturnsEmptyArray() throws ConfigurationException {
		when(mockDigitalPlaceCalendarConfigurationService.getDigitalPlaceCalendarConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.scope()).thenReturn(DigitalPlaceCalendarConfigurationConstants.SCOPE_ALL);

		long[] result = digitalPlaceCalendarEventService.getCalendarScopeGroupIds(mockThemeDisplay);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void getCalendarScopeGroupIds_WhenErrorGettingConfiguration_ThenReturnsEmptyArray() throws ConfigurationException {
		when(mockDigitalPlaceCalendarConfigurationService.getDigitalPlaceCalendarConfiguration(mockThemeDisplay)).thenThrow(new ConfigurationException(""));

		long[] result = digitalPlaceCalendarEventService.getCalendarScopeGroupIds(mockThemeDisplay);

		assertThat(result.length, equalTo(0));
	}

	@Test
	public void getCalendarEventsDateFormatter_WhenTimeZoneIsUTC_ThenReturnsISODateTime() {
		when(mockThemeDisplay.getTimeZone()).thenReturn(TimeZone.getTimeZone("UTC"));

		DateTimeFormatter result = digitalPlaceCalendarEventService.getCalendarEventsDateFormatter(mockThemeDisplay);

		assertThat(result, equalTo(DateTimeFormatter.ISO_DATE_TIME));
	}

	@Test
	public void getCalendarEventsDateFormatter_WhenTimeZoneIsNotUTC_ThenReturnsISOLocalDateTime() {
		when(mockThemeDisplay.getTimeZone()).thenReturn(TimeZone.getTimeZone("Europe/Warsaw"));

		DateTimeFormatter result = digitalPlaceCalendarEventService.getCalendarEventsDateFormatter(mockThemeDisplay);

		assertThat(result, equalTo(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
	}

	@Test
	public void parseDate_WhenNoError_ThenReturnsParsedDate() {
		String date = "2011-12-03T10:15:30";
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

		LocalDateTime result = digitalPlaceCalendarEventService.parseDate(date, formatter);

		assertThat(result, equalTo(LocalDateTime.parse(date, formatter)));
	}

}
