package com.placecube.digitalplace.user.account.service;

import java.util.List;
import java.util.Optional;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.constants.PhoneType;

public interface AccountPhoneService {

	void addPhone(User user, String number, PhoneType type, ServiceContext serviceContext) throws PortalException;

	Optional<Phone> getFirstPhoneOfType(List<Phone> phones, PhoneType type) throws PortalException;

	void populateContextWithExistingPhones(AccountContext accountContext, User user) throws PortalException;
}
