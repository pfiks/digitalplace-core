package com.placecube.digitalplace.addresslookup.statmap.service.utils;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.addresslookup.statmap.constants.StatMapConstants;

@Component(immediate = true, service = AddressRetrivalUtil.class)
public class AddressRetrivalUtil {

	private static final Log LOG = LogFactoryUtil.getLog(AddressRetrivalUtil.class);

	public HttpGet getHttpGetMethod(String url) {
		HttpGet method = new HttpGet(url);
		method.addHeader("Accept", "application/json");
		return method;
	}

	public String getJoinedValuesFromFieldNamesWithNoSpace(JSONObject recordJSONObject, String... fieldNames) {
		List<String> values = new ArrayList<>();
		for (String fieldName : fieldNames) {
			values.add(recordJSONObject.getString(fieldName));
		}
		return values.stream().filter(Validator::isNotNull).collect(Collectors.joining(StringPool.BLANK));
	}

	public String getJoinedValuesWithSpace(String... values) {
		return Arrays.asList(values).stream().filter(Validator::isNotNull).collect(Collectors.joining(StringPool.SPACE));
	}

	public String getURL(String baseEndpoint, List<NameValuePair> nvps, String token) {
		nvps.add(new BasicNameValuePair(StatMapConstants.PARAM_TOKEN, token));

		StringBuilder requestUrl = new StringBuilder(baseEndpoint);
		requestUrl.append(StringPool.QUESTION);
		requestUrl.append(URLEncodedUtils.format(nvps, StandardCharsets.UTF_8.toString()));

		return requestUrl.toString();
	}

	public void quietlyCloseResources(CloseableHttpClient client, CloseableHttpResponse response) {
		try {
			response.close();
		} catch (Exception e) {
			LOG.debug(e);
		}
		try {
			client.close();
		} catch (Exception e) {
			LOG.debug(e);
		}
	}
}
