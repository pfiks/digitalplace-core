package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.TermQuery;
import com.placecube.digitalplace.search.web.service.ObjectFactoryUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ObjectFactoryUtil.class })
public class AdvancePeopleSearchQueryHelperTest extends PowerMockito {

	@InjectMocks
	private AdvancePeopleSearchQueryHelper advancePeopleSearchQueryHelper;

	@Mock
	private BooleanQuery mockBooleanQuery;

	@Mock
	private TermQuery mockTermQuery1;

	@Mock
	private TermQuery mockTermQuery2;

	@Before
	public void activate() {
		mockStatic(ObjectFactoryUtil.class);
		initMocks(this);
	}

	@Test(expected = NullPointerException.class)
	public void getValueAsTermsQuery_WhenFieldValue_IsEmptyThenThrowsNullPointerException() throws ParseException {
		String fieldName = "fieldname";
		String fieldValue = "";

		advancePeopleSearchQueryHelper.getValueAsTermsQuery(fieldName, fieldValue);
	}

	@Test(expected = NullPointerException.class)
	public void getValueAsTermsQuery_WhenFieldName_IsEmptyThenThrowsNullPointerException() throws ParseException {
		String fieldName = "";
		String fieldValue = "fieldvalue";

		advancePeopleSearchQueryHelper.getValueAsTermsQuery(fieldName, fieldValue);
	}

	@Test
	public void getValueAsTermsQuery_WhenFieldNameIsPresentAndValueHasSingleTerm_ThenOneClauseIsAddedToQueryReturned() throws ParseException {
		String fieldName = "fieldname";
		String fieldValue = "term";
		when(ObjectFactoryUtil.createBooleanQuery()).thenReturn(mockBooleanQuery);
		when(ObjectFactoryUtil.createTermQuery(fieldName, fieldValue)).thenReturn(mockTermQuery1);

		BooleanQuery result = advancePeopleSearchQueryHelper.getValueAsTermsQuery(fieldName, fieldValue);
		
		verify(mockBooleanQuery, times(1)).add(mockTermQuery1, BooleanClauseOccur.MUST);
		assertThat(result, sameInstance(mockBooleanQuery));
	}

	@Test
	public void getValueAsTermsQuery_WhenFieldNameIsPresentAndValueHasMultipleTerms_ThenMultipleClausesesAddedToQueryReturned() throws ParseException {
		String fieldName = "fieldname";
		String fieldValue = "term1 term2";
		when(ObjectFactoryUtil.createBooleanQuery()).thenReturn(mockBooleanQuery);
		when(ObjectFactoryUtil.createTermQuery(fieldName, "term1")).thenReturn(mockTermQuery1);
		when(ObjectFactoryUtil.createTermQuery(fieldName, "term2")).thenReturn(mockTermQuery2);

		BooleanQuery result = advancePeopleSearchQueryHelper.getValueAsTermsQuery(fieldName, fieldValue);

		verify(mockBooleanQuery, times(1)).add(mockTermQuery1, BooleanClauseOccur.MUST);
		verify(mockBooleanQuery, times(1)).add(mockTermQuery2, BooleanClauseOccur.MUST);
		assertThat(result, sameInstance(mockBooleanQuery));
	}

	@Test(expected = NullPointerException.class)
	public void getValueAsExactMatchQuery_WhenFieldValue_IsEmptyThenThrowsNullPointerException() {
		String fieldName = "fieldname";
		String fieldValue = "";

		advancePeopleSearchQueryHelper.getValueAsExactMatchQuery(fieldName, fieldValue);
	}

	@Test(expected = NullPointerException.class)
	public void getValueAsExactMatchQuery_WhenFieldName_IsEmptyThenThrowsNullPointerException() {
		String fieldName = "";
		String fieldValue = "fieldvalue";

		advancePeopleSearchQueryHelper.getValueAsExactMatchQuery(fieldName, fieldValue);
	}

	@Test
	public void getValueAsExactMatchQuery_WhenFieldNameIsPresentAndValueHasSingleTerm_ThenSingleClauseAddedToQueryReturned() throws ParseException {
		String fieldName = "fieldname";
		String fieldValue = "term1 term2";
		when(ObjectFactoryUtil.createBooleanQuery()).thenReturn(mockBooleanQuery);

		advancePeopleSearchQueryHelper.getValueAsExactMatchQuery(fieldName, fieldValue);

		BooleanQuery result = advancePeopleSearchQueryHelper.getValueAsTermsQuery(fieldName, fieldValue);

		verify(mockBooleanQuery, times(1)).addExactTerm(fieldName, fieldValue);
		assertThat(result, sameInstance(mockBooleanQuery));
	}

	@Test
	public void getValueAsExactMatchQuery_WhenFieldNameIsPresentAndValueHasMultipleTerms_ThenSingleClauseAddedToQueryReturned() throws ParseException {
		String fieldName = "fieldname";
		String fieldValue = "term1 term2";
		when(ObjectFactoryUtil.createBooleanQuery()).thenReturn(mockBooleanQuery);

		advancePeopleSearchQueryHelper.getValueAsExactMatchQuery(fieldName, fieldValue);

		BooleanQuery result = advancePeopleSearchQueryHelper.getValueAsTermsQuery(fieldName, fieldValue);

		verify(mockBooleanQuery, times(1)).addExactTerm(fieldName, fieldValue);
		assertThat(result, sameInstance(mockBooleanQuery));
	}

}
