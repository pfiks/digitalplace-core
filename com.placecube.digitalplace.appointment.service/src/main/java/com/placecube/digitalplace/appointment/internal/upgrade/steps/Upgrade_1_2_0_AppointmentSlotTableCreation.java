package com.placecube.digitalplace.appointment.internal.upgrade.steps;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class Upgrade_1_2_0_AppointmentSlotTableCreation extends UpgradeProcess {

	public Upgrade_1_2_0_AppointmentSlotTableCreation() {
	}

	@Override
	protected void doUpgrade() throws Exception {

		if (!hasTable("Placecube_Appointment_AppointmentSlot")) {

			runSQL("create table Placecube_Appointment_AppointmentSlot (uuid_ VARCHAR(75) null, appointmentSlotId LONG not null primary key, companyId LONG, userId LONG, userName VARCHAR(75) null, createDate DATE null, modifiedDate DATE null, slotType VARCHAR(75) null, name STRING null, serviceId STRING null, startDate DATE null, endDate DATE null, slotStartTimeHour INTEGER, slotStartTimeMinute INTEGER, slotEndTimeHour INTEGER, slotEndTimeMinute INTEGER, appointmentDuration INTEGER, daysOfWeekBitwise LONG, maxAppointments INTEGER);");

			runSQL("create index IX_17D6EAEC on Placecube_Appointment_AppointmentSlot (companyId, slotType[$COLUMN_LENGTH:75$], serviceId[$COLUMN_LENGTH:4000$]);");
			runSQL("create index IX_9FA8DB34 on Placecube_Appointment_AppointmentSlot (uuid_[$COLUMN_LENGTH:75$]);");
		}

	}

}
