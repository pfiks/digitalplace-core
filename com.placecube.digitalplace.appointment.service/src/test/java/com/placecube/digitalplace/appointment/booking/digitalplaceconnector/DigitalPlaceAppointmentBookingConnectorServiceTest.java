package com.placecube.digitalplace.appointment.booking.digitalplaceconnector;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.appointment.booking.helper.AppointmentModelBuilder;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.model.AppointmentSlot;
import com.placecube.digitalplace.appointment.service.AppointmentSlotLocalService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DigitalPlaceAppointmentBookingConnectorServiceTest {

	@InjectMocks
	private DigitalPlaceAppointmentBookingConnectorService digitalPlaceAppointmentBookingConnectorService;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry1;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry2;

	@Mock
	private AppointmentBookingSlot mockAppointmentBookingSlot1;

	@Mock
	private AppointmentBookingSlot mockAppointmentBookingSlot2;

	@Mock
	private AppointmentBookingSlot mockAppointmentBookingSlot3;

	@Mock
	private AppointmentBookingSlot mockAppointmentBookingSlot4;

	@Mock
	private AppointmentContext mockAppointmentContext;

	@Mock
	private AppointmentEntry mockAppointmentEntry1;

	@Mock
	private AppointmentEntry mockAppointmentEntry2;

	@Mock
	private AppointmentModelBuilder mockAppointmentModelBuilder;

	@Mock
	private AppointmentSlot mockAppointmentSlot1;

	@Mock
	private AppointmentSlot mockAppointmentSlot2;

	@Mock
	private AppointmentSlotLocalService mockAppointmentSlotLocalService;

	@Mock
	private Date mockDate1;

	@Mock
	private Date mockDate2;

	@Mock
	private DigitalPlaceAppointmentConnectorQueryUtil mockDigitalPlaceAppointmentConnectorQueryUtil;

	@Test
	public void convertEntries_WhenNoError_ThenReturnsListOfAppointmentBookingEntries() {
		when(mockAppointmentModelBuilder.getAppointmentBookingEntry(mockAppointmentEntry1)).thenReturn(mockAppointmentBookingEntry1);
		when(mockAppointmentModelBuilder.getAppointmentBookingEntry(mockAppointmentEntry2)).thenReturn(mockAppointmentBookingEntry2);

		List<AppointmentBookingEntry> results = digitalPlaceAppointmentBookingConnectorService.convertEntries(List.of(mockAppointmentEntry1, mockAppointmentEntry2));

		assertThat(results, contains(mockAppointmentBookingEntry1, mockAppointmentBookingEntry2));
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void convertSlots_WhenNoError_ThenReturnsListOfAppointmentBookingEntries(boolean hasStartDate, boolean hasEndDate) {
		if (hasStartDate) {
			when(mockAppointmentContext.getStartDate()).thenReturn(Optional.of(mockDate1));
		} else {
			when(mockAppointmentContext.getStartDate()).thenReturn(Optional.empty());
		}

		if (hasEndDate) {
			when(mockAppointmentContext.getEndDate()).thenReturn(Optional.of(mockDate2));
		} else {
			when(mockAppointmentContext.getEndDate()).thenReturn(Optional.empty());
		}

		when(mockAppointmentSlotLocalService.getAppointmentBookingSlots(mockAppointmentSlot1, hasStartDate ? mockDate1 : null, hasEndDate ? mockDate2 : null))
				.thenReturn(List.of(mockAppointmentBookingSlot1, mockAppointmentBookingSlot2));
		when(mockAppointmentSlotLocalService.getAppointmentBookingSlots(mockAppointmentSlot2, hasStartDate ? mockDate1 : null, hasEndDate ? mockDate2 : null))
				.thenReturn(List.of(mockAppointmentBookingSlot3));

		List<AppointmentBookingSlot> results = digitalPlaceAppointmentBookingConnectorService.convertSlots(List.of(mockAppointmentSlot1, mockAppointmentSlot2), mockAppointmentContext);

		assertThat(results, contains(mockAppointmentBookingSlot1, mockAppointmentBookingSlot2, mockAppointmentBookingSlot3));
	}

	@Test
	public void getSlotsAvailable_WhenNoError_ThenReturnsTheConfigurationSlotsThatDoNotFallIntoExclusionSlots() {
		List<AppointmentBookingSlot> exclusions = List.of(mockAppointmentBookingSlot1);
		List<AppointmentBookingSlot> configurations = List.of(mockAppointmentBookingSlot2, mockAppointmentBookingSlot3, mockAppointmentBookingSlot4);
		when(mockDigitalPlaceAppointmentConnectorQueryUtil.doesSlotOverlap(mockAppointmentBookingSlot1, mockAppointmentBookingSlot2)).thenReturn(false);
		when(mockDigitalPlaceAppointmentConnectorQueryUtil.doesSlotOverlap(mockAppointmentBookingSlot1, mockAppointmentBookingSlot3)).thenReturn(true);
		when(mockDigitalPlaceAppointmentConnectorQueryUtil.doesSlotOverlap(mockAppointmentBookingSlot1, mockAppointmentBookingSlot4)).thenReturn(false);

		List<AppointmentBookingSlot> results = digitalPlaceAppointmentBookingConnectorService.getSlotsAvailable(exclusions, configurations);

		assertThat(results, contains(mockAppointmentBookingSlot2, mockAppointmentBookingSlot4));
	}

}
