package com.placecube.digitalplace.user.account.service.util;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleDisplay;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.PasswordPolicy;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.PortletRequestModel;
import com.liferay.portal.kernel.service.PasswordPolicyLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.user.account.configuration.UserAuthenticationCompanyConfiguration;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UserAuthenticationServiceUtil.class)
public class UserAuthenticationServiceUtil {

	private static final Log LOG = LogFactoryUtil.getLog(UserAuthenticationServiceUtil.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private PasswordPolicyLocalService passwordPolicyLocalService;

	public String getDefaultPasswordPolicyDescription(long companyId) {
		try {
			PasswordPolicy passwordPolicy = passwordPolicyLocalService.getDefaultPasswordPolicy(companyId);

			return passwordPolicy.getDescription();

		} catch (PortalException e) {
			LOG.warn("Exception retrieving password policy - " + e.getMessage());
			LOG.debug(e);
			return StringPool.BLANK;
		}
	}

	public String getConfiguredPasswordPolicyArticleContent(PortletRequestModel portletRequestModel, ThemeDisplay themeDisplay, String languageId) throws PortletException, PortalException {
		try {
			UserAuthenticationCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(UserAuthenticationCompanyConfiguration.class, themeDisplay.getCompanyId());

			String articleId = configuration.passwordPolicyArticleId();

			if (Validator.isNotNull(articleId)) {
				JournalArticle article = journalArticleLocalService.getLatestArticle(configuration.passwordPolicyGroupId(), articleId, WorkflowConstants.STATUS_APPROVED);

				JournalArticleDisplay articleDisplay = journalArticleLocalService.getArticleDisplay(article.getGroupId(), article.getArticleId(), article.getVersion(), article.getDDMTemplateKey(),
						Constants.VIEW, languageId, 1, portletRequestModel, themeDisplay);

				return articleDisplay.getContent();
			} else {
				return StringPool.BLANK;
			}

		} catch (ConfigurationException e) {
			throw new PortletException(e);

		}
	}

	public PortletRequestModel getPortletRequestModel(PortletRequest portletRequest) {

		PortletResponse portletResponse = (PortletResponse) portletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_RESPONSE);

		return new PortletRequestModel(portletRequest, portletResponse);
	}
}
