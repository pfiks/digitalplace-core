package com.placecube.digitalplace.pageimportadmin.service;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;

@Component(immediate = true, service = ContentRetrievalService.class)
public class ContentRetrievalService {

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private LayoutPageTemplateEntryLocalService layoutPageTemplateEntryLocalService;

	public boolean doesArticleWithURLTitleExists(WebContentImportModel webContentImportModel) {
		String url = webContentImportModel.getUrl();
		if (Validator.isNotNull(url)) {
			return Validator.isNotNull(journalArticleLocalService.fetchArticleByUrlTitle(webContentImportModel.getGroupId(), url));
		}
		return false;
	}

	public List<Long> getCategoriesByName(long assetVocabularyId, String categoryName) {
		DynamicQuery dynamicQuery = assetCategoryLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("vocabularyId", assetVocabularyId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("name", StringUtil.trim(categoryName)));
		dynamicQuery.setProjection(ProjectionFactoryUtil.property("categoryId"));

		return assetCategoryLocalService.dynamicQuery(dynamicQuery);
	}

	public Optional<Layout> getLayout(long groupId, String layoutURL, boolean privateLayout) {
		if (Validator.isNotNull(layoutURL)) {
			return Optional.ofNullable(layoutLocalService.fetchLayoutByFriendlyURL(groupId, privateLayout, layoutURL));
		}
		return Optional.empty();
	}

	public Optional<LayoutPageTemplateEntry> getPageTemplate(long groupId, String pageTemplateName) {
		if (Validator.isNotNull(pageTemplateName)) {
			return Optional.ofNullable(layoutPageTemplateEntryLocalService.fetchLayoutPageTemplateEntry(groupId, pageTemplateName));
		}
		return Optional.empty();
	}

	public Optional<DDMStructure> getWebContentStructure(long groupId, String webContentStructureName) {
		DynamicQuery dynamicQuery = ddmStructureLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", groupId));
		dynamicQuery.add(RestrictionsFactoryUtil.like("name", "%" + webContentStructureName + "%"));
		List<DDMStructure> structures = ddmStructureLocalService.dynamicQuery(dynamicQuery);
		return structures.stream().filter(ddmStructure -> webContentStructureName.equals(ddmStructure.getName(ddmStructure.getDefaultLanguageId()))).findFirst();
	}

}
