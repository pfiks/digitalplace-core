package com.placecube.digitalplace.address.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.address.configuration.AddressTypeCompanyConfiguration;
import com.placecube.digitalplace.address.constants.AddressType;

@RunWith(MockitoJUnitRunner.class)
public class AddressTypeConfigurationServiceImplTest {

	private static final long COMPANY_ID = 435l;

	@InjectMocks
	private AddressTypeConfigurationServiceImpl addressTypeConfigurationServiceImpl;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private AddressTypeCompanyConfiguration mockAddressTypeCompanyConfiguration;

	@Test
	public void getDefaultAddressType_WhenNoErrors_ThenReturnsConfiguredDefaultAddressType() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(AddressTypeCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockAddressTypeCompanyConfiguration);
		when(mockAddressTypeCompanyConfiguration.defaultAddressType()).thenReturn(AddressType.BUSINESS.getName());

		AddressType result = addressTypeConfigurationServiceImpl.getDefaultAddressType(COMPANY_ID);

		assertThat(result, equalTo(AddressType.BUSINESS));
	}

	@Test
	public void getDefaultAddressType_WhenConfiguredDefaultAddressTypeNameCannotBeFound_ThenReturnsPersonalAddressType() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(AddressTypeCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockAddressTypeCompanyConfiguration);
		when(mockAddressTypeCompanyConfiguration.defaultAddressType()).thenReturn("unknown");

		AddressType result = addressTypeConfigurationServiceImpl.getDefaultAddressType(COMPANY_ID);

		assertThat(result, equalTo(AddressType.PERSONAL));
	}

	@Test
	public void getDefaultAddressType_WhenConfiguredDefaultAddressTypeIsEmpty_ThenReturnsPersonalAddressType() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(AddressTypeCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockAddressTypeCompanyConfiguration);
		when(mockAddressTypeCompanyConfiguration.defaultAddressType()).thenReturn("");

		AddressType result = addressTypeConfigurationServiceImpl.getDefaultAddressType(COMPANY_ID);

		assertThat(result, equalTo(AddressType.PERSONAL));
	}

	@Test
	public void getDefaultAddressType_WhenConfigurationCannotBeRetrieved_ThenReturnsPersonalAddressType() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(AddressTypeCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		AddressType result = addressTypeConfigurationServiceImpl.getDefaultAddressType(COMPANY_ID);

		assertThat(result, equalTo(AddressType.PERSONAL));
	}

}
