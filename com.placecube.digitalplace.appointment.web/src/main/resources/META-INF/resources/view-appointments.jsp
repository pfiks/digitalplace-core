<%@ include file="init.jsp" %>

<portlet:renderURL var="refreshPageURL">
	<portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.VIEW_APPOINTMENTS%>" />
</portlet:renderURL>

<aui:form action="${refreshPageURL}" method="GET" name="appointmentsListForm" cssClass="mt-2">
	<div class="container-fluid container-fluid-max-xl">
		<c:choose>
			<c:when test="${ invalidConfigurationError }">
				<%@ include file="/partials/invalid-configuration.jspf" %>
			</c:when>
			<c:otherwise>
				<aui:row>
					<aui:col span="3">
						<label class="govuk-label" for="<portlet:namespace/>dateFilter">
							<liferay-ui:message key="${ configuredFilterColumnLabel eq '' ? 'dpappointment-filter-options-label' : configuredFilterColumnLabel }" />
						</label>
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col span="3">
						<aui:select cssClass="govuk-input" name="dateFilter" label="" showEmptyOption="false" ignoreRequestValue="true">
							<aui:option value="TODAY" selected="${dateFilterType eq 'TODAY'}" label="dpappointment-todays-appointments" />
							<aui:option value="FUTURE" selected="${dateFilterType eq 'FUTURE'}" label="dpappointment-future-appointments" />
							<aui:option value="ALL" selected="${dateFilterType eq 'ALL'}" label="dpappointment-all-appointments" />
							<aui:option value="RANGE" selected="${dateFilterType eq 'RANGE'}" label="dpappointment-appointment-date-range" />
						</aui:select>
					</aui:col>
				</aui:row>

				<div id="<portlet:namespace/>dateRangeFilters" class="${dateFilterType eq 'RANGE' ? '' : 'hide'}">
					<aui:row>
						<aui:col span="3">
							<label class="govuk-label" for="<portlet:namespace/>fromDate">
								Appointment date start
							</label>
						</aui:col>
						<aui:col span="3">
							<label class="govuk-label" for="<portlet:namespace/>toDate"}">
								Appointment date end
							</label>
						</aui:col>
					</aui:row>
					<aui:row>
						<aui:col span="3">
							<liferay-ui:input-date name="fromDate"
									dayValue="${selectedDateFrom.day}" dayParam="fromDay"
									monthValue="${selectedDateFrom.month}" monthParam="fromMonth"
									yearValue="${selectedDateFrom.year}" yearParam="fromYear"
									nullable="true" cssClass="govuk-input"
									showDisableCheckbox="false"/>
						</aui:col>
						<aui:col span="3">
							<liferay-ui:input-date name="toDate"
									dayValue="${selectedDateTo.day}" dayParam="toDay"
									monthValue="${selectedDateTo.month}" monthParam="toMonth"
									yearValue="${selectedDateTo.year}" yearParam="toYear"
									nullable="true" cssClass="govuk-input"
									showDisableCheckbox="false"/>
						</aui:col>
						<aui:col span="2">
							<button
								type="submit"
								data-module="govuk-button"
								ariaLabel='<liferay-ui:message key="apply" />'
								class="govuk-button"
								style="margin-bottom: 0px"
								><liferay-ui:message key="apply" />
							</button>
						</aui:col>
					</aui:row>
				</div>

				<liferay-ui:search-container
					deltaConfigurable="${searchContainerAppointments.isDeltaConfigurable()}"
					id="searchContainerAppointmentsId"
					curParam="cur"
					searchContainer="${searchContainerAppointments}"
					total="${searchContainerAppointments.getTotal()}"
					emptyResultsMessage="${searchContainerAppointments.getEmptyResultsMessage()}"
					emptyResultsMessageCssClass="${searchContainerAppointments.getEmptyResultsMessageCssClass()}">
					
					<liferay-ui:search-container-results results="${searchContainerAppointments.getResults()}" />
					
					<liferay-ui:search-container-row className="com.placecube.digitalplace.appointment.web.model.AppointmentDashboardModel" modelVar="appointmentBooking">
						
						<liferay-ui:search-container-column-text name="${ configuredIdColumnLabel eq '' ? 'dpappointment-entry-id' : configuredIdColumnLabel }">
							<a href="${appointmentBooking.viewUrl}">
								${appointmentBooking.id}
							</a>
						</liferay-ui:search-container-column-text>
						
						<liferay-ui:search-container-column-text name="${ configuredSummaryColumnLabel eq '' ? 'dpappointment-summary' : configuredSummaryColumnLabel }" value="${appointmentBooking.summary}" />
						
						<liferay-ui:search-container-column-text name="${ configuredStartDateColumnLabel eq '' ? 'dpappointment-start-date' : configuredStartDateColumnLabel }">
							<fmt:formatDate value="${appointmentBooking.getStartDate()}" pattern="dd MMM yyyy HH:mm" timeZone="${timeZone}" />
						</liferay-ui:search-container-column-text>
			
					</liferay-ui:search-container-row>
					
					<liferay-ui:search-iterator paginate="true" displayStyle="list" markupView="lexicon" searchContainer="${searchContainerAppointments}"  />
					
				</liferay-ui:search-container>
				
				<aui:script>
					var namespace = '<portlet:namespace/>';
					$('#' + namespace + 'dateFilter').on('change', function () {
						if ($('#' + namespace + 'dateFilter').val() === 'RANGE') {
							$('#' + namespace + 'dateRangeFilters').removeClass('hide')
						} else {
							$('#' + namespace + 'dateRangeFilters').addClass('hide')
							$('#' + namespace + 'appointmentsListForm').submit();
						}
					});
				</aui:script>
			</c:otherwise>
		</c:choose>
	</div>
</aui:form>