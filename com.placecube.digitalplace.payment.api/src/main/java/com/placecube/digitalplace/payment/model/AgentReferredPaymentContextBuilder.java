package com.placecube.digitalplace.payment.model;

import java.util.List;

import com.liferay.portal.kernel.model.User;

public class AgentReferredPaymentContextBuilder {

	private String callerPhoneNumber;
	private String callerEmail;
	private String receiptEmail;
	private boolean immediateCallBack;
	private String agentPhoneNumber;
	private String returnUrl;
	private String sid;
	private String logUrl;
	private User user;
	private List<AgentReferredPaymentLine> paymentLines;

	public AgentReferredPaymentContextBuilder withCallerPhoneNumber(String callerPhoneNumber) {
		this.callerPhoneNumber = callerPhoneNumber;
		return this;
	}

	public AgentReferredPaymentContextBuilder withCallerEmail(String callerEmail) {
		this.callerEmail = callerEmail;
		return this;
	}

	public AgentReferredPaymentContextBuilder withReceiptEmail(String receiptEmail) {
		this.receiptEmail = receiptEmail;
		return this;
	}

	public AgentReferredPaymentContextBuilder withImmediateCallBack(boolean immediateCallBack) {
		this.immediateCallBack = immediateCallBack;
		return this;
	}

	public AgentReferredPaymentContextBuilder withAgentPhoneNumber(String phoneNumber) {
		agentPhoneNumber = phoneNumber;
		return this;
	}

	public AgentReferredPaymentContextBuilder withReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
		return this;
	}

	public AgentReferredPaymentContextBuilder withSid(String sid) {
		this.sid = sid;
		return this;
	}

	public AgentReferredPaymentContextBuilder withLogUrl(String logUrl) {
		this.logUrl = logUrl;
		return this;
	}

	public AgentReferredPaymentContextBuilder withUser(User user) {
		this.user = user;
		return this;
	}

	public AgentReferredPaymentContextBuilder withPaymentLines(List<AgentReferredPaymentLine> paymentLines) {
		this.paymentLines = paymentLines;
		return this;
	}

	public AgentReferredPaymentContext build() {
		AgentReferredPaymentContext context = new AgentReferredPaymentContext();
		context.setCallerPhoneNumber(callerPhoneNumber);
		context.setCallerEmail(callerEmail);
		context.setReceiptEmail(receiptEmail);
		context.setImmediateCallBack(immediateCallBack);
		context.setAgentPhoneNumber(agentPhoneNumber);
		context.setReturnUrl(returnUrl);
		context.setSid(sid);
		context.setLogUrl(logUrl);
		context.setUser(user);
		context.setPaymentLines(paymentLines);
		return context;
	}
}