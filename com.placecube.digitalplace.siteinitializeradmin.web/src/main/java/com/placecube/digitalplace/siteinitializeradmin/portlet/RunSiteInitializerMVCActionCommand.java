package com.placecube.digitalplace.siteinitializeradmin.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.siteinitializer.setuputils.service.SiteInitializerSetupService;
import com.placecube.digitalplace.siteinitializeradmin.constants.MVCCommandKeys;
import com.placecube.digitalplace.siteinitializeradmin.constants.PortletKeys;
import com.placecube.digitalplace.siteinitializeradmin.constants.PortletRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SITE_INITIALIZER_ADMIN, "mvc.command.name=" + MVCCommandKeys.RUN_SITE_INITIALIZER }, service = MVCActionCommand.class)
public class RunSiteInitializerMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(RunSiteInitializerMVCActionCommand.class);

	@Reference
	private Portal portal;

	@Reference
	private SiteInitializerSetupService siteInitializerSetupService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {
		try {
			String siteInitializerkey = ParamUtil.getString(actionRequest, PortletRequestKeys.SELECTED_SITE_INITIALIZER);
			long currentGroupId = portal.getScopeGroupId(actionRequest);
			long userId = portal.getUserId(actionRequest);

			siteInitializerSetupService.runSiteInitialiser(userId, currentGroupId, siteInitializerkey);

		} catch (Exception e) {
			LOG.error(e);
			throw new PortletException(e);
		}
	}

}
