package com.placecube.digitalplace.appointment.admin.web.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.TimeZone;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.admin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.admin.web.model.AppointmentSlotForm;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;

@RunWith(MockitoJUnitRunner.class)
public class UpdateAppointmentSlotMVCActionCommandTest {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private AppointmentSlotForm mockAppointmentSlotForm;

	@Mock
	private AppointmentsManagerService mockAppointmentsManagerService;

	private MockedStatic<ExceptionUtils> mockExceptionUtils;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	private MockedStatic<SessionErrors> mockSessionErrors;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TimeZone mockTimeZone;

	@Mock
	private User mockUser;

	@InjectMocks
	private UpdateAppointmentSlotMVCActionCommand updateAppointmentSlotMVCActionCommand;

	@Before
	public void activateSetUp() {
		mockSessionErrors = mockStatic(SessionErrors.class);
		mockExceptionUtils = mockStatic(ExceptionUtils.class);
	}

	@Test
	public void doProcessAction_WhenAppointmentSlotIsNotNewAndExceptionUpdatingTheAppointmentSlot_ThenSetsSessionMessage() throws Exception {
		when(mockAppointmentsManagerService.getAppointmentSlotFormFromRequest(mockActionRequest)).thenReturn(mockAppointmentSlotForm);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getTimeZone()).thenReturn(mockTimeZone);
		when(mockAppointmentSlotForm.isNew()).thenReturn(false);
		PortalException exception = new PortalException("this is my message", new NullPointerException());
		doThrow(exception).when(mockAppointmentsManagerService).updateAppointmentSlot(mockAppointmentSlotForm, mockTimeZone);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		mockExceptionUtils.when(() -> ExceptionUtils.getStackTrace(exception)).thenReturn("myStacktraceMessage");

		updateAppointmentSlotMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_ALL_SLOTS);
		mockSessionErrors.verify(() -> SessionErrors.add(mockActionRequest, "appointment-slot-update-error", "myStacktraceMessage"));

		verify(mockAppointmentsManagerService, never()).createNewAppointmentSlot(any(AppointmentSlotForm.class), any(User.class));
	}

	@Test
	public void doProcessAction_WhenNoErrorAndAppointmentSlotIsNew_ThenCreatesTheAppointmentSlot() throws Exception {
		String serviceId = "myServiceId";
		when(mockAppointmentsManagerService.getAppointmentSlotFormFromRequest(mockActionRequest)).thenReturn(mockAppointmentSlotForm);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockAppointmentSlotForm.isNew()).thenReturn(true);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockAppointmentSlotForm.getServiceId()).thenReturn(serviceId);

		updateAppointmentSlotMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue("serviceId", serviceId);
		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_ALL_SLOTS);
		verify(mockAppointmentsManagerService, times(1)).createNewAppointmentSlot(mockAppointmentSlotForm, mockUser);
		verify(mockAppointmentsManagerService, never()).updateAppointmentSlot(any(AppointmentSlotForm.class), any(TimeZone.class));
	}

	@Test
	public void doProcessAction_WhenNoErrorAndAppointmentSlotIsNotNew_ThenUpdatesTheAppointmentSlot() throws Exception {
		String serviceId = "myServiceId";
		when(mockAppointmentsManagerService.getAppointmentSlotFormFromRequest(mockActionRequest)).thenReturn(mockAppointmentSlotForm);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getTimeZone()).thenReturn(mockTimeZone);
		when(mockAppointmentSlotForm.isNew()).thenReturn(false);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockAppointmentSlotForm.getServiceId()).thenReturn(serviceId);

		updateAppointmentSlotMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAppointmentsManagerService, times(1)).updateAppointmentSlot(mockAppointmentSlotForm, mockTimeZone);
		verify(mockAppointmentsManagerService, never()).createNewAppointmentSlot(any(AppointmentSlotForm.class), any(User.class));
		verify(mockMutableRenderParameters, times(1)).setValue("serviceId", serviceId);
		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_ALL_SLOTS);
	}

	@After
	public void teardown() {
		mockSessionErrors.close();
		mockExceptionUtils.close();
	}
}
