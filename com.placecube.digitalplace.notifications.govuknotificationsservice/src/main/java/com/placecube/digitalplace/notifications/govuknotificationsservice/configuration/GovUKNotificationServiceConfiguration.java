package com.placecube.digitalplace.notifications.govuknotificationsservice.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.notifications.govuknotificationsservice.configuration.GovUKNotificationServiceConfiguration", localization = "content/Language", name = "notifications-govuk-notifications-service")
public interface GovUKNotificationServiceConfiguration {

	@Meta.AD(required = false, deflt = "GovUKNotifyTestAPIKey", name = "key")
	String applicationKey();

	@Meta.AD(required = false, deflt = "", name = "additional-key", description = "additional-key-description")
	String[] additionalApplicationKeys();

	@Meta.AD(required = false, deflt = "false", name = "email-enabled")
	boolean emailEnabled();

	@Meta.AD(required = false, deflt = "false", name = "sms-enabled")
	boolean smsEnabled();
}
