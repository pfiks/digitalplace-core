package com.placecube.digitalplace.appointment.admin.web.portlet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.TimeZone;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.common.time.model.DateTimeSelection;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;

@RunWith(MockitoJUnitRunner.class)
public class BookedAppointmentsListMVCRenderCommandTest {

	@InjectMocks
	private BookedAppointmentsListMVCRenderCommand bookedAppointmentsListMVCRenderCommand;

	@Mock
	private List<AppointmentBookingEntry> mockAppointmentBookingEntryList;

	@Mock
	private AppointmentsManagerService mockAppointmentsManagerService;

	@Mock
	private DateTimeSelection mockDateTimeSelection1;

	@Mock
	private DateTimeSelection mockDateTimeSelection2;

	private MockedStatic<ParamUtil> mockParamUtil;

	private MockedStatic<PropsUtil> mockPropsUtil;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TimeZone mockTimeZone;

	@Before
	public void activateSetUp() {
		mockPropsUtil = mockStatic(PropsUtil.class);
		mockParamUtil = mockStatic(ParamUtil.class);
	}

	@Test
	public void render_WhenNoErrorAndSearchExecuted_ThenSetsRequestAttributesIncludingBookedAppointments() throws Exception {
		long companyId = 11;
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		mockParamUtil.when(() -> ParamUtil.getString(mockRenderRequest, "serviceId", "")).thenReturn("serviceIdValue");
		mockParamUtil.when(() -> ParamUtil.getBoolean(mockRenderRequest, "searchExecuted", false)).thenReturn(true);
		when(mockThemeDisplay.getTimeZone()).thenReturn(mockTimeZone);
		when(mockAppointmentsManagerService.getFilterStartDate(true, mockRenderRequest, mockTimeZone)).thenReturn(mockDateTimeSelection1);
		when(mockAppointmentsManagerService.getFilterEndDate(true, mockRenderRequest, mockTimeZone)).thenReturn(mockDateTimeSelection2);
		when(mockAppointmentsManagerService.getAvailableServiceIds(companyId)).thenReturn(List.of("s1", "s2"));
		when(mockAppointmentsManagerService.getBookedAppointments(companyId, "serviceIdValue", mockDateTimeSelection1, mockDateTimeSelection2)).thenReturn(mockAppointmentBookingEntryList);

		String result = bookedAppointmentsListMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/view-appointments.jsp"));

		verify(mockRenderRequest, times(1)).setAttribute("filterStartDateTime", mockDateTimeSelection1);
		verify(mockRenderRequest, times(1)).setAttribute("filterEndDateTime", mockDateTimeSelection2);
		verify(mockRenderRequest, times(1)).setAttribute("availableServiceIds", List.of("s1", "s2"));
		verify(mockRenderRequest, times(1)).setAttribute("serviceId", "serviceIdValue");
		verify(mockRenderRequest, times(1)).setAttribute("bookedAppointments", mockAppointmentBookingEntryList);
	}

	@Test
	public void render_WhenNoErrorAndSearchNotExecuted_ThenSetsRequestAttributesApartFromBookedAppointments() throws Exception {
		long companyId = 11;
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		mockParamUtil.when(() -> ParamUtil.getBoolean(mockRenderRequest, "searchExecuted", false)).thenReturn(false);
		when(mockThemeDisplay.getTimeZone()).thenReturn(mockTimeZone);
		when(mockAppointmentsManagerService.getFilterStartDate(false, mockRenderRequest, mockTimeZone)).thenReturn(mockDateTimeSelection1);
		when(mockAppointmentsManagerService.getFilterEndDate(false, mockRenderRequest, mockTimeZone)).thenReturn(mockDateTimeSelection2);
		when(mockAppointmentsManagerService.getAvailableServiceIds(companyId)).thenReturn(List.of("s1", "s2"));

		String result = bookedAppointmentsListMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/view-appointments.jsp"));

		verify(mockRenderRequest, times(1)).setAttribute("filterStartDateTime", mockDateTimeSelection1);
		verify(mockRenderRequest, times(1)).setAttribute("filterEndDateTime", mockDateTimeSelection2);
		verify(mockRenderRequest, times(1)).setAttribute("availableServiceIds", List.of("s1", "s2"));
		verify(mockAppointmentsManagerService, never()).getBookedAppointments(anyLong(), anyString(), any(), any());
		verify(mockRenderRequest, never()).setAttribute(eq("bookedAppointments"), any());
	}

	@After
	public void teardown() {
		mockPropsUtil.close();
		mockParamUtil.close();
	}
}
