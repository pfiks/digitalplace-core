package com.placecube.digitalplace.search.web.portlet.contenttype;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.contenttype.configuration.ContentTypeFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.contenttype.service.ContentTypeSearchService;
import com.placecube.digitalplace.search.web.service.FacetParsingService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-search-contenttypefacet", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=dp-search-facet-content-type dp-search-facet dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.always-display-default-configuration-icons=true", //
		"javax.portlet.init-param.config-template=/content-type-filter/configuration.jsp", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/content-type-filter/view.jsp", //
		"javax.portlet.name=" + PortletKeys.CONTENT_TYPE_FACET, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class ContentTypeFacetPortlet extends MVCPortlet {

	private static final String ALL_CONTENT_TYPES_FACETS_SESSION_ATTRIBUTE = "contentTypeFacetPortlet_allAvailableContentTypesInitialFacets";

	@Reference
	private ContentTypeSearchService contentTypeSearchService;

	@Reference
	private FacetParsingService facetParsingService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			ContentTypeFacetPortletInstanceConfiguration configuration = contentTypeSearchService.getConfiguration(themeDisplay);
			renderRequest.setAttribute("filterColour", configuration.filterColour());

			SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

			List<FacetResult> availableContentTypes = getAvailableContentTypes(searchResponse, renderRequest, themeDisplay, configuration.joinFiltersWithTheSameName());

			renderRequest.setAttribute("availableContentTypes", availableContentTypes);
			renderRequest.setAttribute("redirectURL", searchResponse.getEscapedSearchResultsIteratorURL());

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private List<FacetResult> getAvailableContentTypes(SharedSearchResponse searchResponse, RenderRequest renderRequest, ThemeDisplay themeDisplay, boolean joinFiltersWithTheSameName) {
		PortletSession portletSession = renderRequest.getPortletSession();

		Map<String, Integer> facetValues = (Map<String, Integer>) portletSession.getAttribute(ALL_CONTENT_TYPES_FACETS_SESSION_ATTRIBUTE);
		Set<String> selectedValues = searchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_CONTENT_TYPES);

		if (Validator.isNull(facetValues) || facetValues.isEmpty() || selectedValues.isEmpty()) {
			facetValues = searchResponse.getFacetValues(SearchFacetConstants.CONTENT_TYPE);
			portletSession.setAttribute(ALL_CONTENT_TYPES_FACETS_SESSION_ATTRIBUTE, facetValues);
		}

		return facetParsingService.parseContentTypes(facetValues, selectedValues, themeDisplay.getLocale(), joinFiltersWithTheSameName);
	}

}