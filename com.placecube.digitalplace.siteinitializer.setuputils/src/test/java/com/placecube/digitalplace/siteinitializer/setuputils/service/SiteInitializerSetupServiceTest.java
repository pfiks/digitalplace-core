package com.placecube.digitalplace.siteinitializer.setuputils.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.net.URL;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.site.exception.InitializationException;
import com.liferay.site.initializer.SiteInitializer;
import com.liferay.site.initializer.SiteInitializerRegistry;
import com.placecube.digitalplace.siteinitializer.setuputils.internal.utils.SiteInitializerBackupService;
import com.placecube.digitalplace.siteinitializer.setuputils.internal.utils.SiteInitializerUtils;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PrincipalThreadLocal.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SiteInitializerSetupServiceTest extends PowerMockito {

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONArray mockJSONArrayFile;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private JSONObject mockJSONObjectSettings;

	@Mock
	private Layout mockLayout;

	@Mock
	private Layout mockLayoutDraft;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private Map<String, String> mockPlaceholdersValues;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private SiteInitializer mockSiteInitializer;

	@Mock
	private SiteInitializerBackupService mockSiteInitializerBackupService;

	@Mock
	private SiteInitializerRegistry mockSiteInitializerRegistry;

	@Mock
	private SiteInitializerUtils mockSiteInitializerUtils;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private SiteInitializerSetupService siteInitializerSetupService;

	@Before
	public void activateSetup() {
		mockStatic(PrincipalThreadLocal.class);
	}

	@Test
	public void configureAdditionalSiteData_WhenJSONObjectNotNull_ThenConfiguresTheAdditionalData() throws PortalException {
		long groupId = 1;
		long userId = 2;

		siteInitializerSetupService.configureAdditionalSiteData(userId, groupId, mockJSONObject);

		verify(mockSiteInitializerUtils, times(1)).configureAdditionalData(userId, groupId, mockJSONObject);
	}

	@Test
	public void configureAdditionalSiteData_WhenJSONObjectNull_ThenNoActionIsPerformed() throws PortalException {
		long groupId = 1;
		long userId = 2;

		siteInitializerSetupService.configureAdditionalSiteData(userId, groupId, null);

		verifyNoInteractions(mockSiteInitializerUtils);
	}

	@Test
	public void createJobSchedulerTriggers_WhenNoError_ThenCreatesTheJobTriggers() throws PortalException {
		when(mockJSONArrayFile.length()).thenReturn(2);
		when(mockJSONArrayFile.getJSONObject(0)).thenReturn(mockJSONObject);
		when(mockJSONObject.getString("externalReferenceCode")).thenReturn("externalReferenceCode1");
		when(mockJSONObject.getString("taskExecutorType")).thenReturn("taskExecutorType1");
		when(mockJSONObject.getString("name")).thenReturn("name1");

		when(mockJSONArrayFile.getJSONObject(1)).thenReturn(mockJSONObject2);
		when(mockJSONObject2.getString("externalReferenceCode")).thenReturn("externalReferenceCode2");
		when(mockJSONObject2.getString("taskExecutorType")).thenReturn("taskExecutorType2");
		when(mockJSONObject2.getString("name")).thenReturn("name2");
		when(mockJSONObject2.getJSONObject("settings")).thenReturn(mockJSONObjectSettings);

		siteInitializerSetupService.createJobSchedulerTriggers(mockJSONArrayFile, mockServiceContext);

		verify(mockSiteInitializerUtils, times(1)).createMissingDispatchTrigger("externalReferenceCode1", "taskExecutorType1", "name1", null, mockServiceContext);
		verify(mockSiteInitializerUtils, times(1)).createMissingDispatchTrigger("externalReferenceCode2", "taskExecutorType2", "name2", mockJSONObjectSettings, mockServiceContext);
	}

	@Test
	public void getServiceContext_WithGroupIdParameter_WhenNoError_ThenReturnsTheConfiguredServiceContext() throws PortalException {
		long groupId = 123l;
		long userId = 56;
		when(PrincipalThreadLocal.getUserId()).thenReturn(userId);
		when(mockUserLocalService.getUser(userId)).thenReturn(mockUser);
		when(mockSiteInitializerUtils.getServiceContext(mockUser, groupId)).thenReturn(mockServiceContext);

		ServiceContext result = siteInitializerSetupService.getServiceContext(mockUser, groupId);

		assertThat(result, sameInstance(mockServiceContext));
	}

	@Test
	public void getServiceContext_WithUserAndGroupIdParameters_WhenNoError_ThenReturnsTheConfiguredServiceContext() throws PortalException {
		long groupId = 123l;
		when(mockSiteInitializerUtils.getServiceContext(mockUser, groupId)).thenReturn(mockServiceContext);

		ServiceContext result = siteInitializerSetupService.getServiceContext(mockUser, groupId);

		assertThat(result, sameInstance(mockServiceContext));
	}

	@Test(expected = PortalException.class)
	public void runSiteInitialiser_WithGroupIdAndSiteInitialiseKeyAndDataUrlParameters_WhenSiteInitializerNotFound_ThenThrowsPortalException() throws Exception {
		String siteInitializerKey = "siteInitializerKeyValue";
		long groupId = 123;
		long userId = 2;
		Optional<URL> urlData = Optional.of(Thread.currentThread().getContextClassLoader().getResource("test-additional-post-setup-data.json"));
		when(mockSiteInitializerRegistry.getSiteInitializer(siteInitializerKey)).thenReturn(null);

		siteInitializerSetupService.runSiteInitialiser(userId, groupId, siteInitializerKey, urlData);

		verify(mockSiteInitializerBackupService, times(1)).importBackedUpContentAndRemoveTemporaryGroup(0l, 0l);
	}

	@Test(expected = PortalException.class)
	public void runSiteInitialiser_WithGroupIdAndSiteInitialiseKeyDataUrlParameters_WhenSiteInitializerFoundAndExceptionExecutingInitializeCode_ThenThrowsPortalExceptionAfterImportingBackedUpContentAgain()
			throws Exception {
		String siteInitializerKey = "siteInitializerKeyValue";
		long groupId = 11l;
		long userId = 2;
		long backupGroupId = 33;
		Optional<URL> urlData = Optional.of(Thread.currentThread().getContextClassLoader().getResource("test-additional-post-setup-data.json"));
		when(mockSiteInitializerRegistry.getSiteInitializer(siteInitializerKey)).thenReturn(mockSiteInitializer);
		when(mockSiteInitializerBackupService.backupContentInTemporaryGroup(groupId)).thenReturn(backupGroupId);
		doThrow(new InitializationException()).when(mockSiteInitializer).initialize(groupId);

		siteInitializerSetupService.runSiteInitialiser(userId, groupId, siteInitializerKey, urlData);

		verify(mockSiteInitializerBackupService, times(1)).importBackedUpContentAndRemoveTemporaryGroup(groupId, backupGroupId);
	}

	@Test(expected = PortalException.class)
	public void runSiteInitialiser_WithGroupIdAndSiteInitialiseKeyDataUrlParameters_WhenSiteInitializerFoundAndExceptionExecutingTheBackup_ThenThrowsPortalExceptionAfterImportingBackedUpContentAgain()
			throws Exception {
		String siteInitializerKey = "siteInitializerKeyValue";
		long groupId = 11l;
		long userId = 2;
		Optional<URL> urlData = Optional.of(Thread.currentThread().getContextClassLoader().getResource("test-additional-post-setup-data.json"));
		when(mockSiteInitializerRegistry.getSiteInitializer(siteInitializerKey)).thenReturn(mockSiteInitializer);
		when(mockSiteInitializerBackupService.backupContentInTemporaryGroup(groupId)).thenThrow(new PortalException());

		siteInitializerSetupService.runSiteInitialiser(userId, groupId, siteInitializerKey, urlData);

		verify(mockSiteInitializerBackupService, times(1)).importBackedUpContentAndRemoveTemporaryGroup(groupId, 0l);
	}

	@Test
	public void runSiteInitialiser_WithGroupIdAndSiteInitialiseKeyDataUrlParameters_WhenSiteInitializerFoundAndNoError_ThenRunsTheInitializerOnTheScopeGroup() throws Exception {
		String siteInitializerKey = "siteInitializerKeyValue";
		Long groupId = 11l;
		long backupGroupId = 33;
		long userId = 2;
		Optional<URL> urlData = Optional.of(Thread.currentThread().getContextClassLoader().getResource("test-additional-post-setup-data.json"));
		when(mockSiteInitializerRegistry.getSiteInitializer(siteInitializerKey)).thenReturn(mockSiteInitializer);
		when(mockSiteInitializerBackupService.backupContentInTemporaryGroup(groupId)).thenReturn(backupGroupId);

		siteInitializerSetupService.runSiteInitialiser(userId, groupId, siteInitializerKey, urlData);

		InOrder inOrder = inOrder(mockSiteInitializer, mockSiteInitializerBackupService);
		inOrder.verify(mockSiteInitializerBackupService, times(1)).backupContentInTemporaryGroup(groupId);
		inOrder.verify(mockSiteInitializer, times(1)).initialize(groupId);
		inOrder.verify(mockSiteInitializerBackupService, times(1)).importBackedUpContentAndRemoveTemporaryGroup(groupId, backupGroupId);
	}

	@Test(expected = PortalException.class)
	public void runSiteInitialiser_WithGroupIdAndSiteInitialiseKeyParameters_WhenSiteInitializerFoundAndExceptionExecutingInitializeCode_ThenThrowsPortalExceptionAfterImportingBackedUpContentAgain()
			throws Exception {
		String siteInitializerKey = "siteInitializerKeyValue";
		long groupId = 11l;
		long userId = 2;
		long backupGroupId = 33;
		when(mockSiteInitializerRegistry.getSiteInitializer(siteInitializerKey)).thenReturn(mockSiteInitializer);
		when(mockSiteInitializerBackupService.backupContentInTemporaryGroup(groupId)).thenReturn(backupGroupId);
		doThrow(new InitializationException()).when(mockSiteInitializer).initialize(groupId);

		siteInitializerSetupService.runSiteInitialiser(userId, groupId, siteInitializerKey);

		verify(mockSiteInitializerBackupService, times(1)).importBackedUpContentAndRemoveTemporaryGroup(groupId, backupGroupId);
	}

	@Test(expected = PortalException.class)
	public void runSiteInitialiser_WithGroupIdAndSiteInitialiseKeyParameters_WhenSiteInitializerFoundAndExceptionExecutingTheBackup_ThenThrowsPortalExceptionAfterImportingBackedUpContentAgain()
			throws Exception {
		String siteInitializerKey = "siteInitializerKeyValue";
		long groupId = 11l;
		long userId = 2;
		when(mockSiteInitializerRegistry.getSiteInitializer(siteInitializerKey)).thenReturn(mockSiteInitializer);
		when(mockSiteInitializerBackupService.backupContentInTemporaryGroup(groupId)).thenThrow(new PortalException());

		siteInitializerSetupService.runSiteInitialiser(userId, groupId, siteInitializerKey);

		verify(mockSiteInitializerBackupService, times(1)).importBackedUpContentAndRemoveTemporaryGroup(groupId, 0l);
	}

	@Test
	public void runSiteInitialiser_WithGroupIdAndSiteInitialiseKeyParameters_WhenSiteInitializerFoundAndNoError_ThenRunsTheInitializerOnTheScopeGroup() throws Exception {
		String siteInitializerKey = "siteInitializerKeyValue";
		Long groupId = 11l;
		long userId = 2;
		long backupGroupId = 33;
		when(mockSiteInitializerRegistry.getSiteInitializer(siteInitializerKey)).thenReturn(mockSiteInitializer);
		when(mockSiteInitializerBackupService.backupContentInTemporaryGroup(groupId)).thenReturn(backupGroupId);

		siteInitializerSetupService.runSiteInitialiser(userId, groupId, siteInitializerKey);

		InOrder inOrder = inOrder(mockSiteInitializer, mockSiteInitializerBackupService);
		inOrder.verify(mockSiteInitializerBackupService, times(1)).backupContentInTemporaryGroup(groupId);
		inOrder.verify(mockSiteInitializer, times(1)).initialize(groupId);
		inOrder.verify(mockSiteInitializerBackupService, times(1)).importBackedUpContentAndRemoveTemporaryGroup(groupId, backupGroupId);
	}

	@Test(expected = PortalException.class)
	public void runSiteInitialiser_WithGroupIdAndSiteInitialiseKeyParameters_WhenSiteInitializerNotFound_ThenThrowsPortalException() throws PortalException {
		String siteInitializerKey = "siteInitializerKeyValue";
		long groupId = 123;
		long userId = 2;
		when(mockSiteInitializerRegistry.getSiteInitializer(siteInitializerKey)).thenReturn(null);

		siteInitializerSetupService.runSiteInitialiser(userId, groupId, siteInitializerKey);

		verify(mockSiteInitializerBackupService, times(1)).importBackedUpContentAndRemoveTemporaryGroup(0l, 0l);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateExpandoFieldsForPages_WhenNoError_ThenUpdatesThePagesExpandoFields(boolean privatePage) throws Exception {
		long groupId = 123l;
		long plid = 56;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockJSONArrayFile.length()).thenReturn(1);
		when(mockJSONArrayFile.getJSONObject(0)).thenReturn(mockJSONObject);
		when(mockJSONObject.getJSONArray("expandoFields")).thenReturn(mockJSONArray);
		when(mockJSONObject.getBoolean("private")).thenReturn(privatePage);
		when(mockJSONObject.getString("friendlyURL")).thenReturn("urlValue");
		when(mockSiteInitializerUtils.getLayout(mockServiceContext, mockJSONObject)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(plid);
		when(mockLayoutLocalService.fetchDraftLayout(plid)).thenReturn(mockLayoutDraft);

		siteInitializerSetupService.updateExpandoFieldsForPages(mockJSONArrayFile, mockPlaceholdersValues, mockServiceContext);

		InOrder inOrder = inOrder(mockSiteInitializerUtils);
		inOrder.verify(mockSiteInitializerUtils, times(1)).addExpandoValues(mockLayout, mockJSONArray, mockPlaceholdersValues);
		inOrder.verify(mockSiteInitializerUtils, times(1)).addExpandoValues(mockLayoutDraft, mockJSONArray, mockPlaceholdersValues);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updatePortletPreferencesForPages_WhenNoError_ThenUpdatesThePagePortletPreferences(boolean privatePage) throws Exception {
		long groupId = 123l;
		long plid = 56;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockJSONArrayFile.length()).thenReturn(1);
		when(mockJSONArrayFile.getJSONObject(0)).thenReturn(mockJSONObject);
		when(mockJSONObject.getJSONArray("placeholdersToReplace")).thenReturn(mockJSONArray);
		when(mockJSONObject.getBoolean("private")).thenReturn(privatePage);
		when(mockJSONObject.getString("friendlyURL")).thenReturn("urlValue");
		when(mockSiteInitializerUtils.getLayout(mockServiceContext, mockJSONObject)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(plid);
		when(mockLayoutLocalService.fetchDraftLayout(plid)).thenReturn(mockLayoutDraft);

		siteInitializerSetupService.updatePortletPreferencesForPages(mockJSONArrayFile, mockPlaceholdersValues, mockServiceContext);

		InOrder inOrder = inOrder(mockSiteInitializerUtils);
		inOrder.verify(mockSiteInitializerUtils, times(1)).updatePortletPreferencesWithPlaceholders(mockLayout, mockJSONArray, mockPlaceholdersValues);
		inOrder.verify(mockSiteInitializerUtils, times(1)).publishLayout(mockLayout);
		inOrder.verify(mockSiteInitializerUtils, times(1)).updatePortletPreferencesWithPlaceholders(mockLayoutDraft, mockJSONArray, mockPlaceholdersValues);
		inOrder.verify(mockSiteInitializerUtils, times(1)).publishLayout(mockLayoutDraft);
	}

}
