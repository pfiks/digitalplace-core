package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLSearchListingsConfigurationConstants;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.DDL_LOCATION_SEARCH_PORTLET, service = ConfigurationAction.class)
public class DDLLocationSearchPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		DDLLocationSearchPortletInstanceConfiguration configuration = ddlConfigurationService.getDDLLocationSearchPortletInstanceConfiguration(themeDisplay);

		httpServletRequest.setAttribute("configuration", configuration);

		httpServletRequest.setAttribute("availableStructures", ddlConfigurationService.getAvailableDDMStructures(themeDisplay.getScopeGroup()));

		httpServletRequest.setAttribute(DDLSearchListingsConfigurationConstants.DESTINATION_PAGE, configuration.destinationPage());
		httpServletRequest.setAttribute(DDLSearchListingsConfigurationConstants.GEOLOCATION_FIELD, configuration.geoLocationField());

		super.include(portletConfig, httpServletRequest, httpServletResponse);

	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long ddmStructureId = ParamUtil.getLong(actionRequest, DDLSearchListingsConfigurationConstants.STRUCTURE_ID, 0);
		String destinationPage = ParamUtil.getString(actionRequest, DDLSearchListingsConfigurationConstants.DESTINATION_PAGE, StringPool.BLANK);
		String geoLocationField = ParamUtil.getString(actionRequest, DDLSearchListingsConfigurationConstants.GEOLOCATION_FIELD, StringPool.BLANK);
		setPreference(actionRequest, DDLSearchListingsConfigurationConstants.STRUCTURE_ID, String.valueOf(ddmStructureId));
		setPreference(actionRequest, DDLSearchListingsConfigurationConstants.DESTINATION_PAGE, destinationPage);
		setPreference(actionRequest, DDLSearchListingsConfigurationConstants.GEOLOCATION_FIELD, geoLocationField);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}