package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import java.util.Optional;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@Component(immediate = true, service = CipherService.class)
public class CipherService {

	static final String CIPHER_TRANSFORM_VALUE = "AES/GCM/NoPadding";

	private static final Log LOG = LogFactoryUtil.getLog(CipherService.class);

	@Reference
	private GCMParameterSpecService gcmParameterSpecService;

	@Reference
	private SecretKeySpecService secretKeySpecService;

	public Optional<Cipher> getCipher(int mode, String uuid) {
		try {
			GCMParameterSpec gcmSpec = gcmParameterSpecService.getGCMParameterSpec(uuid);
			SecretKeySpec skeySpec = secretKeySpecService.getSecretKeySpec(uuid);
			Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORM_VALUE);
			cipher.init(mode, skeySpec, gcmSpec);
			return Optional.of(cipher);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return Optional.empty();
	}

}
