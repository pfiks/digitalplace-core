package com.placecube.digitalplace.ddl.web.portlet.display.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;

@Component(immediate = true, service = DDLCategorisationService.class)
public class DDLCategorisationService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLCategorisationService.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;

	@Reference
	private JSONFactory jsonFactory;

	public boolean areChildCategoriesSelected(List<Long> requiredVocabularyIds, List<Long> requiredRootCategoryIds, List<Long> selectedAssetCategoryIds) {
		for (Long selectedAssetCategoryId : selectedAssetCategoryIds) {
			try {
				AssetCategory assetCategory = assetCategoryLocalService.getAssetCategory(selectedAssetCategoryId);
				if (isVocabularySelected(requiredVocabularyIds, assetCategory) || isChildCategorySelected(requiredRootCategoryIds, assetCategory)) {
					return true;
				}
			} catch (Exception e) {
				LOG.debug(e);
				LOG.warn(e.getMessage());
			}
		}
		return false;
	}

	public List<AssetCategory> getAvailableCategories(List<AssetVocabulary> assetVocabularies) {
		List<AssetCategory> results = new ArrayList<>();

		assetVocabularies.stream().forEach(assetVocabulary -> results.addAll(assetVocabulary.getCategories()));

		return results.stream().filter(AssetCategory::isRootCategory).collect(Collectors.toList());
	}

	public List<AssetVocabulary> getAvailableVocabularies(ThemeDisplay themeDisplay) {
		List<AssetVocabulary> vocabularies = assetVocabularyLocalService.getGroupsVocabularies(new long[] { themeDisplay.getScopeGroupId(), themeDisplay.getCompanyGroupId() });
		return vocabularies.stream().filter(assetVocabulary -> assetVocabulary.getCategoriesCount() > 0).collect(Collectors.toList());
	}

	public Set<AssetCategory> getMandatoryCategories(List<Long> requiredCategoryIds) {
		Set<AssetCategory> results = new HashSet<>();
		for (Long mandatoryCategoryId : requiredCategoryIds) {
			try {
				results.add(assetCategoryLocalService.getCategory(mandatoryCategoryId));
			} catch (PortalException e) {
				LOG.warn("Unable to retrieve mandatory configured category " + e.getMessage());
			}
		}
		return results;
	}

	public Set<AssetVocabulary> getMandatoryVocabularies(List<Long> requiredVocabularyIds) {
		Set<AssetVocabulary> results = new HashSet<>();
		for (Long vocabularyId : requiredVocabularyIds) {
			try {
				results.add(assetVocabularyLocalService.getVocabulary(vocabularyId));
			} catch (PortalException e) {
				LOG.warn("Unable to retrieve mandatory configured vocabulary" + e.getMessage());
			}
		}
		return results;
	}

	public String getRequiredCategorisationMessage(Locale locale, Set<AssetCategory> mandatoryCategories, Set<AssetVocabulary> mandatoryVocabularies) {
		String categoryString = mandatoryCategories.stream().map(entry -> entry.getTitle(locale)).collect(Collectors.joining(", "));
		String vocabularyString = mandatoryVocabularies.stream().map(entry -> entry.getTitle(locale)).collect(Collectors.joining(", "));
		return String.join(", ", categoryString, vocabularyString);
	}

	public JSONArray getVocabularyIdsJSON(Set<AssetCategory> assetCategories, Set<AssetVocabulary> mandatoryVocabularies) {
		JSONArray results = jsonFactory.createJSONArray();
		Set<Long> vocabularyIds = assetCategories.stream().map(AssetCategory::getVocabularyId).collect(Collectors.toSet());
		vocabularyIds.addAll(mandatoryVocabularies.stream().map(AssetVocabulary::getVocabularyId).collect(Collectors.toSet()));

		for (Long vocabularyId : vocabularyIds) {
			results.put(vocabularyId);
		}
		return results;
	}

	private boolean isCategoryIdSelected(List<Long> requiredRootCategoryIds, AssetCategory assetCategory) {
		return requiredRootCategoryIds.contains(assetCategory.getCategoryId());
	}

	private boolean isChildCategorySelected(List<Long> requiredRootCategoryIds, AssetCategory assetCategory) throws PortalException {
		List<AssetCategory> parentCategories = assetCategory.getAncestors();
		for (AssetCategory parentCategory : parentCategories) {
			if (isVocabularySelected(requiredRootCategoryIds, parentCategory) || isCategoryIdSelected(requiredRootCategoryIds, parentCategory)) {
				return true;
			}
		}
		return false;
	}

	private boolean isVocabularySelected(List<Long> requiredVocabularyIds, AssetCategory assetCategory) {
		return requiredVocabularyIds.contains(assetCategory.getVocabularyId());
	}

}