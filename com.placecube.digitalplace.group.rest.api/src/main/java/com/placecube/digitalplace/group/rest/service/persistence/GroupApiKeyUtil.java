/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.group.rest.model.GroupApiKey;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the group api key service. This utility wraps <code>com.placecube.digitalplace.group.rest.service.persistence.impl.GroupApiKeyPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GroupApiKeyPersistence
 * @generated
 */
public class GroupApiKeyUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(GroupApiKey groupApiKey) {
		getPersistence().clearCache(groupApiKey);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, GroupApiKey> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<GroupApiKey> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<GroupApiKey> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<GroupApiKey> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<GroupApiKey> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static GroupApiKey update(GroupApiKey groupApiKey) {
		return getPersistence().update(groupApiKey);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static GroupApiKey update(
		GroupApiKey groupApiKey, ServiceContext serviceContext) {

		return getPersistence().update(groupApiKey, serviceContext);
	}

	/**
	 * Returns all the group api keys where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @return the matching group api keys
	 */
	public static List<GroupApiKey> findByApplicationKey(
		String applicationKey) {

		return getPersistence().findByApplicationKey(applicationKey);
	}

	/**
	 * Returns a range of all the group api keys where applicationKey = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationKey the application key
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of matching group api keys
	 */
	public static List<GroupApiKey> findByApplicationKey(
		String applicationKey, int start, int end) {

		return getPersistence().findByApplicationKey(
			applicationKey, start, end);
	}

	/**
	 * Returns an ordered range of all the group api keys where applicationKey = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationKey the application key
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group api keys
	 */
	public static List<GroupApiKey> findByApplicationKey(
		String applicationKey, int start, int end,
		OrderByComparator<GroupApiKey> orderByComparator) {

		return getPersistence().findByApplicationKey(
			applicationKey, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the group api keys where applicationKey = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationKey the application key
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group api keys
	 */
	public static List<GroupApiKey> findByApplicationKey(
		String applicationKey, int start, int end,
		OrderByComparator<GroupApiKey> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByApplicationKey(
			applicationKey, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	public static GroupApiKey findByApplicationKey_First(
			String applicationKey,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupApiKeyException {

		return getPersistence().findByApplicationKey_First(
			applicationKey, orderByComparator);
	}

	/**
	 * Returns the first group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	public static GroupApiKey fetchByApplicationKey_First(
		String applicationKey,
		OrderByComparator<GroupApiKey> orderByComparator) {

		return getPersistence().fetchByApplicationKey_First(
			applicationKey, orderByComparator);
	}

	/**
	 * Returns the last group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	public static GroupApiKey findByApplicationKey_Last(
			String applicationKey,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupApiKeyException {

		return getPersistence().findByApplicationKey_Last(
			applicationKey, orderByComparator);
	}

	/**
	 * Returns the last group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	public static GroupApiKey fetchByApplicationKey_Last(
		String applicationKey,
		OrderByComparator<GroupApiKey> orderByComparator) {

		return getPersistence().fetchByApplicationKey_Last(
			applicationKey, orderByComparator);
	}

	/**
	 * Returns the group api keys before and after the current group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param groupApiKeyPK the primary key of the current group api key
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	public static GroupApiKey[] findByApplicationKey_PrevAndNext(
			GroupApiKeyPK groupApiKeyPK, String applicationKey,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupApiKeyException {

		return getPersistence().findByApplicationKey_PrevAndNext(
			groupApiKeyPK, applicationKey, orderByComparator);
	}

	/**
	 * Removes all the group api keys where applicationKey = &#63; from the database.
	 *
	 * @param applicationKey the application key
	 */
	public static void removeByApplicationKey(String applicationKey) {
		getPersistence().removeByApplicationKey(applicationKey);
	}

	/**
	 * Returns the number of group api keys where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @return the number of matching group api keys
	 */
	public static int countByApplicationKey(String applicationKey) {
		return getPersistence().countByApplicationKey(applicationKey);
	}

	/**
	 * Returns all the group api keys where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @return the matching group api keys
	 */
	public static List<GroupApiKey> findByApplicationSecret(
		String applicationSecret) {

		return getPersistence().findByApplicationSecret(applicationSecret);
	}

	/**
	 * Returns a range of all the group api keys where applicationSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationSecret the application secret
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of matching group api keys
	 */
	public static List<GroupApiKey> findByApplicationSecret(
		String applicationSecret, int start, int end) {

		return getPersistence().findByApplicationSecret(
			applicationSecret, start, end);
	}

	/**
	 * Returns an ordered range of all the group api keys where applicationSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationSecret the application secret
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group api keys
	 */
	public static List<GroupApiKey> findByApplicationSecret(
		String applicationSecret, int start, int end,
		OrderByComparator<GroupApiKey> orderByComparator) {

		return getPersistence().findByApplicationSecret(
			applicationSecret, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the group api keys where applicationSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationSecret the application secret
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group api keys
	 */
	public static List<GroupApiKey> findByApplicationSecret(
		String applicationSecret, int start, int end,
		OrderByComparator<GroupApiKey> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByApplicationSecret(
			applicationSecret, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	public static GroupApiKey findByApplicationSecret_First(
			String applicationSecret,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupApiKeyException {

		return getPersistence().findByApplicationSecret_First(
			applicationSecret, orderByComparator);
	}

	/**
	 * Returns the first group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	public static GroupApiKey fetchByApplicationSecret_First(
		String applicationSecret,
		OrderByComparator<GroupApiKey> orderByComparator) {

		return getPersistence().fetchByApplicationSecret_First(
			applicationSecret, orderByComparator);
	}

	/**
	 * Returns the last group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	public static GroupApiKey findByApplicationSecret_Last(
			String applicationSecret,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupApiKeyException {

		return getPersistence().findByApplicationSecret_Last(
			applicationSecret, orderByComparator);
	}

	/**
	 * Returns the last group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	public static GroupApiKey fetchByApplicationSecret_Last(
		String applicationSecret,
		OrderByComparator<GroupApiKey> orderByComparator) {

		return getPersistence().fetchByApplicationSecret_Last(
			applicationSecret, orderByComparator);
	}

	/**
	 * Returns the group api keys before and after the current group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param groupApiKeyPK the primary key of the current group api key
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	public static GroupApiKey[] findByApplicationSecret_PrevAndNext(
			GroupApiKeyPK groupApiKeyPK, String applicationSecret,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupApiKeyException {

		return getPersistence().findByApplicationSecret_PrevAndNext(
			groupApiKeyPK, applicationSecret, orderByComparator);
	}

	/**
	 * Removes all the group api keys where applicationSecret = &#63; from the database.
	 *
	 * @param applicationSecret the application secret
	 */
	public static void removeByApplicationSecret(String applicationSecret) {
		getPersistence().removeByApplicationSecret(applicationSecret);
	}

	/**
	 * Returns the number of group api keys where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @return the number of matching group api keys
	 */
	public static int countByApplicationSecret(String applicationSecret) {
		return getPersistence().countByApplicationSecret(applicationSecret);
	}

	/**
	 * Caches the group api key in the entity cache if it is enabled.
	 *
	 * @param groupApiKey the group api key
	 */
	public static void cacheResult(GroupApiKey groupApiKey) {
		getPersistence().cacheResult(groupApiKey);
	}

	/**
	 * Caches the group api keys in the entity cache if it is enabled.
	 *
	 * @param groupApiKeys the group api keys
	 */
	public static void cacheResult(List<GroupApiKey> groupApiKeys) {
		getPersistence().cacheResult(groupApiKeys);
	}

	/**
	 * Creates a new group api key with the primary key. Does not add the group api key to the database.
	 *
	 * @param groupApiKeyPK the primary key for the new group api key
	 * @return the new group api key
	 */
	public static GroupApiKey create(GroupApiKeyPK groupApiKeyPK) {
		return getPersistence().create(groupApiKeyPK);
	}

	/**
	 * Removes the group api key with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key that was removed
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	public static GroupApiKey remove(GroupApiKeyPK groupApiKeyPK)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupApiKeyException {

		return getPersistence().remove(groupApiKeyPK);
	}

	public static GroupApiKey updateImpl(GroupApiKey groupApiKey) {
		return getPersistence().updateImpl(groupApiKey);
	}

	/**
	 * Returns the group api key with the primary key or throws a <code>NoSuchGroupApiKeyException</code> if it could not be found.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	public static GroupApiKey findByPrimaryKey(GroupApiKeyPK groupApiKeyPK)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupApiKeyException {

		return getPersistence().findByPrimaryKey(groupApiKeyPK);
	}

	/**
	 * Returns the group api key with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key, or <code>null</code> if a group api key with the primary key could not be found
	 */
	public static GroupApiKey fetchByPrimaryKey(GroupApiKeyPK groupApiKeyPK) {
		return getPersistence().fetchByPrimaryKey(groupApiKeyPK);
	}

	/**
	 * Returns all the group api keys.
	 *
	 * @return the group api keys
	 */
	public static List<GroupApiKey> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of group api keys
	 */
	public static List<GroupApiKey> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of group api keys
	 */
	public static List<GroupApiKey> findAll(
		int start, int end, OrderByComparator<GroupApiKey> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of group api keys
	 */
	public static List<GroupApiKey> findAll(
		int start, int end, OrderByComparator<GroupApiKey> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the group api keys from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of group api keys.
	 *
	 * @return the number of group api keys
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getCompoundPKColumnNames() {
		return getPersistence().getCompoundPKColumnNames();
	}

	public static GroupApiKeyPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(GroupApiKeyPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile GroupApiKeyPersistence _persistence;

}