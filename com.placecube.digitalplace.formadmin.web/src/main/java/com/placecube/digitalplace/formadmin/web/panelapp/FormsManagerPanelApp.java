package com.placecube.digitalplace.formadmin.web.panelapp;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.model.Portlet;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;

@Component(immediate = true, property = { "panel.app.order:Integer=200", "panel.category.key=" + PanelCategoryKeys.SITE_ADMINISTRATION_CONFIGURATION }, service = PanelApp.class)
public class FormsManagerPanelApp extends BasePanelApp {

	@Reference(target = "(javax.portlet.name=" + PortletKeys.FORMS_MANAGER + ")", unbind = "-")
	private Portlet portlet;

	@Override
	public Portlet getPortlet() {
		return portlet;
	}

	@Override
	public String getPortletId() {
		return PortletKeys.FORMS_MANAGER;
	}

}