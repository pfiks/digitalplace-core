package com.placecube.digitalplace.navigation.portlet;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.placecube.digitalplace.navigation.configuration.NavigationPortletInstanceConfiguration;
import com.placecube.digitalplace.navigation.constants.PortletKeys;
import com.placecube.digitalplace.navigation.service.NavigationService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-navigation", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=portlet-dp-navigation", //
		"com.liferay.portlet.display-category=category.cms", //
		"com.liferay.portlet.instanceable=true", //
		"com.liferay.portlet.layout-cacheable=true", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.config-template=/configuration.jsp", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/view.jsp", //
		"javax.portlet.name=" + PortletKeys.NAVIGATION, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=power-user,user" //
}, service = Portlet.class)
public class NavigationPortlet extends MVCPortlet {

	@Reference
	private NavigationService navigationService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			Optional<NavigationPortletInstanceConfiguration> configuration = navigationService.getValidPortletConfiguration(renderRequest, themeDisplay);

			if (configuration.isPresent()) {

				Optional<SiteNavigationMenu> siteNavigationMenu = navigationService.getSiteNavigationMenu(configuration.get());

				if (siteNavigationMenu.isPresent()) {

					Optional<DDMTemplate> widgetTemplate = navigationService.getWidgetTemplate(configuration.get());
					if (widgetTemplate.isPresent()) {
						renderRequest.setAttribute("siteNavigationMenu", siteNavigationMenu.get());
						renderRequest.setAttribute("widgetTemplate", widgetTemplate.get());
					}
				}
			}

			super.render(renderRequest, renderResponse);

		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}