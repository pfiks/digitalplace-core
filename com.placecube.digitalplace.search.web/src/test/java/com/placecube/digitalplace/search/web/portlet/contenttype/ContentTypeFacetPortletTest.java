package com.placecube.digitalplace.search.web.portlet.contenttype;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.contenttype.configuration.ContentTypeFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.contenttype.service.ContentTypeSearchService;
import com.placecube.digitalplace.search.web.service.FacetParsingService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class ContentTypeFacetPortletTest extends PowerMockito {

	private static final String COLOUR = "orange";
	private static final Locale LOCALE = Locale.ENGLISH;
	private static final String REDIRECT = "redirectURLVal";

	@InjectMocks
	private ContentTypeFacetPortlet contentTypeFacetPortlet;

	@Mock
	private ContentTypeFacetPortletInstanceConfiguration mockContentTypeFacetPortletInstanceConfiguration;

	@Mock
	private ContentTypeSearchService mockContentTypeSearchService;

	@Mock
	private FacetParsingService mockFacetParsingService;

	@Mock
	private List<FacetResult> mockFacetResults;

	@Mock
	private Map<String, Integer> mockFacetValues;

	@Mock
	private Map<String, Integer> mockFacetValuesFromSession;

	@Mock
	private PortletSession mockPortletSession;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private Set<String> mockSelectedValues;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockContentTypeSearchService.getConfiguration(mockThemeDisplay)).thenReturn(mockContentTypeFacetPortletInstanceConfiguration);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		contentTypeFacetPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoErrorAndAvailableContentTypesAlreadyInSessionAndThereAreSelectedFilters_ThenConfiguresTheContentTypeDetailsAsRequestAttributesFromTheSession(boolean joinFiltersWithTheSameName) throws Exception {
		mockSearchExecuted(mockFacetValuesFromSession, mockSelectedValues, joinFiltersWithTheSameName);
		when(mockPortletSession.getAttribute("contentTypeFacetPortlet_allAvailableContentTypesInitialFacets")).thenReturn(mockFacetValuesFromSession);

		contentTypeFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableContentTypes", mockFacetResults);
		verify(mockRenderRequest, times(1)).setAttribute("filterColour", COLOUR);
		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", REDIRECT);
		verify(mockPortletSession, never()).setAttribute(eq("contentTypeFacetPortlet_allAvailableContentTypesInitialFacets"), any());
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoErrorAndAvailableContentTypesAlreadyInSessionButThereAreNoSelectedFilters_ThenConfiguresTheContentTypeDetailsAsRequestAttributesFromTheSearchFacets(boolean joinFiltersWithTheSameName)
			throws Exception {
		mockSearchExecuted(mockFacetValues, Collections.emptySet(), joinFiltersWithTheSameName);
		when(mockPortletSession.getAttribute("contentTypeFacetPortlet_allAvailableContentTypesInitialFacets")).thenReturn(mockFacetValuesFromSession);
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.CONTENT_TYPE)).thenReturn(mockFacetValues);

		contentTypeFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableContentTypes", mockFacetResults);
		verify(mockRenderRequest, times(1)).setAttribute("filterColour", COLOUR);
		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", REDIRECT);
		verify(mockPortletSession, times(1)).setAttribute("contentTypeFacetPortlet_allAvailableContentTypesInitialFacets", mockFacetValues);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoErrorAndAvailableContentTypesInSessionAreEmpty_ThenConfiguresTheContentTypeDetailsAsRequestAttributesFromTheSearchFacets(boolean joinFiltersWithTheSameName) throws Exception {
		mockSearchExecuted(mockFacetValues, mockSelectedValues, joinFiltersWithTheSameName);
		when(mockPortletSession.getAttribute("contentTypeFacetPortlet_allAvailableContentTypesInitialFacets")).thenReturn(Collections.emptyMap());
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.CONTENT_TYPE)).thenReturn(mockFacetValues);

		contentTypeFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableContentTypes", mockFacetResults);
		verify(mockRenderRequest, times(1)).setAttribute("filterColour", COLOUR);
		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", REDIRECT);
		verify(mockPortletSession, times(1)).setAttribute("contentTypeFacetPortlet_allAvailableContentTypesInitialFacets", mockFacetValues);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoErrorAndAvailableContentTypesNotAlreadyInSession_ThenConfiguresTheContentTypeDetailsAsRequestAttributesFromTheSearchFacets(boolean joinFiltersWithTheSameName) throws Exception {
		mockSearchExecuted(mockFacetValues, mockSelectedValues, joinFiltersWithTheSameName);
		when(mockPortletSession.getAttribute("contentTypeFacetPortlet_allAvailableContentTypesInitialFacets")).thenReturn(null);
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.CONTENT_TYPE)).thenReturn(mockFacetValues);

		contentTypeFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableContentTypes", mockFacetResults);
		verify(mockRenderRequest, times(1)).setAttribute("filterColour", COLOUR);
		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", REDIRECT);
		verify(mockPortletSession, times(1)).setAttribute("contentTypeFacetPortlet_allAvailableContentTypesInitialFacets", mockFacetValues);
	}

	private void mockSearchExecuted(Map<String, Integer> facetValues, Set<String> selectedValues, boolean joinFiltersWithTheSameName) throws ConfigurationException, SearchException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(LOCALE);
		when(mockContentTypeSearchService.getConfiguration(mockThemeDisplay)).thenReturn(mockContentTypeFacetPortletInstanceConfiguration);
		when(mockContentTypeFacetPortletInstanceConfiguration.filterColour()).thenReturn(COLOUR);
		when(mockContentTypeFacetPortletInstanceConfiguration.joinFiltersWithTheSameName()).thenReturn(joinFiltersWithTheSameName);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockRenderRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockSharedSearchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_CONTENT_TYPES)).thenReturn(selectedValues);
		when(mockFacetParsingService.parseContentTypes(facetValues, selectedValues, LOCALE, joinFiltersWithTheSameName)).thenReturn(mockFacetResults);
		when(mockSharedSearchResponse.getEscapedSearchResultsIteratorURL()).thenReturn(REDIRECT);
	}

}
