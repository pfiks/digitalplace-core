package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.configuration.DDLFieldFilterPortletInstanceConfiguration")
public interface DDLFieldFilterPortletInstanceConfiguration {

	@Meta.AD(required = false)
	long ddmStructureId();

	@Meta.AD(required = false)
	String header();

	@Meta.AD(required = false)
	boolean invisible();

	@Meta.AD(required = false)
	String jsonFilterConfig();

	@Meta.AD(required = false)
	boolean showGroupNameAsFirstOption();

	@Meta.AD(required = false)
	String internalPortletId();
}
