package com.placecube.digitalplace.formadmin.web.constants;

import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isPrivate;
import static java.lang.reflect.Modifier.isStatic;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.junit.Test;

public class FormExportKeysTest {

	@Test
	public void testClassIsAConstantsClass() throws Exception {
		Class<?> classToTest = FormExportKeys.class;

		assertThat(isFinal(classToTest.getModifiers()), equalTo(true));

		assertThat(classToTest.getDeclaredConstructors().length, equalTo(1));
		Constructor<?> constructor = classToTest.getDeclaredConstructor();
		assertThat(isPrivate(constructor.getModifiers()), equalTo(true));
		assertThat(constructor.isAccessible(), equalTo(false));

		constructor.setAccessible(true);
		constructor.newInstance();
		constructor.setAccessible(false);

		for (Method method : classToTest.getMethods()) {
			if (method.isAccessible()) {
				assertThat(isStatic(method.getModifiers()), equalTo(true));
				assertThat(method.getDeclaringClass(), equalTo(classToTest));
			}
		}
	}

	@Test
	public void testConstantValueForAdditionCount() {
		assertThat(FormExportKeys.ADDITION_COUNT, equalTo("addition-count"));
	}

	@Test
	public void testConstantValueForAssetEntryPriority() {
		assertThat(FormExportKeys.ASSET_ENTRY_PRIORITY, equalTo("asset-entry-priority"));
	}

	@Test
	public void testConstantValueForAttachedClassName() {
		assertThat(FormExportKeys.ATTACHED_CLASS_NAME, equalTo("attached-class-name"));
	}

	@Test
	public void testConstantValueForAvailableLocales() {
		assertThat(FormExportKeys.AVAILABLE_LOCALES, equalTo("available-locales"));
	}

	@Test
	public void testConstantValueForBuildNumber() {
		assertThat(FormExportKeys.BUILD_NUMBER, equalTo("build-number"));
	}

	@Test
	public void testConstantValueForClassName() {
		assertThat(FormExportKeys.CLASS_NAME, equalTo("class-name"));
	}

	@Test
	public void testConstantValueForClassPK() {
		assertThat(FormExportKeys.CLASS_PK, equalTo("class-pk"));
	}

	@Test
	public void testConstantValueForCompanyId() {
		assertThat(FormExportKeys.COMPANY_ID, equalTo("company-id"));
	}

	@Test
	public void testConstantValueForCompanyGroupId() {
		assertThat(FormExportKeys.COMPANY_GROUP_ID, equalTo("company-group-id"));
	}

	@Test
	public void testConstantValueForDDMDataProviderInstance() {
		assertThat(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE, equalTo("DDMDataProviderInstance"));
	}

	@Test
	public void testConstantValueForDDMDataProviderInstanceIds() {
		assertThat(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS, equalTo("ddm-data-provider-instance-ids"));
	}

	@Test
	public void testConstantValueForDDMFormAdminPortletDataHandler() {
		assertThat(FormExportKeys.DDM_FORM_ADMIN_PORTLET_DATA_HANDLER, equalTo("DDMFormAdminPortletDataHandler"));
	}

	@Test
	public void testConstantValueForDDMFormInstance() {
		assertThat(FormExportKeys.DDM_FORM_INSTANCE, equalTo("DDMFormInstance"));
	}

	@Test
	public void testConstantValueForDDMFormLayoutPath() {
		assertThat(FormExportKeys.DDM_FORM_LAYOUT_PATH, equalTo("ddm-form-layout-path"));
	}

	@Test
	public void testConstantValueForDDMFormPath() {
		assertThat(FormExportKeys.DDM_FORM_PATH, equalTo("ddm-form-path"));
	}

	@Test
	public void testConstantValueForDDMStructure() {
		assertThat(FormExportKeys.DDM_STRUCTURE, equalTo("DDMStructure"));
	}

	@Test
	public void testConstantValueForExportDate() {
		assertThat(FormExportKeys.EXPORT_DATE, equalTo("export-date"));
	}

	@Test
	public void testConstantValueForGroup() {
		assertThat(FormExportKeys.GROUP, equalTo("group"));
	}

	@Test
	public void testConstantValueForGroupId() {
		assertThat(FormExportKeys.GROUP_ID, equalTo("group-id"));
	}

	@Test
	public void testConstantValueForGroupKey() {
		assertThat(FormExportKeys.GROUP_KEY, equalTo("group-key"));
	}

	@Test
	public void testConstantValueForHeader() {
		assertThat(FormExportKeys.HEADER, equalTo("header"));
	}

	@Test
	public void testConstantValueForLayoutId() {
		assertThat(FormExportKeys.LAYOUT_ID, equalTo("layout-id"));
	}

	@Test
	public void testConstantValueForLiveGroupId() {
		assertThat(FormExportKeys.LIVE_GROUP_ID, equalTo("live-group-id"));
	}

	@Test
	public void testConstantValueForManifestSummary() {
		assertThat(FormExportKeys.MANIFEST_SUMMARY, equalTo("manifest-summary"));
	}

	@Test
	public void testConstantValueForManifestSummaryKey() {
		assertThat(FormExportKeys.MANIFEST_SUMMARY_KEY, equalTo("manifest-summary-key"));
	}

	@Test
	public void testConstantValueForMissing() {
		assertThat(FormExportKeys.MISSING, equalTo("missing"));
	}

	@Test
	public void testConstantValueForMissingReferences() {
		assertThat(FormExportKeys.MISSING_REFERENCES, equalTo("missing-references"));
	}

	@Test
	public void testConstantValueForPath() {
		assertThat(FormExportKeys.PATH, equalTo("path"));
	}

	@Test
	public void testConstantValueForPortlet() {
		assertThat(FormExportKeys.PORTLET, equalTo("portlet"));
	}

	@Test
	public void testConstantValueForPortletConfiguration() {
		assertThat(FormExportKeys.PORTLET_CONFIGURATION, equalTo("portlet-configuration"));
	}

	@Test
	public void testConstantValueForPortletData() {
		assertThat(FormExportKeys.PORTLET_DATA, equalTo("portlet-data"));
	}

	@Test
	public void testConstantValueForPortletId() {
		assertThat(FormExportKeys.PORTLET_ID, equalTo("portlet-id"));
	}

	@Test
	public void testConstantValueForPreloaded() {
		assertThat(FormExportKeys.PRELOADED, equalTo("preloaded"));
	}

	@Test
	public void testConstantValueForPrivateLayout() {
		assertThat(FormExportKeys.PRIVATE_LAYOUT, equalTo("private-layout"));
	}

	@Test
	public void testConstantValueForReference() {
		assertThat(FormExportKeys.REFERENCE, equalTo("reference"));
	}

	@Test
	public void testConstantValueForReferencedClassName() {
		assertThat(FormExportKeys.REFERENCED_CLASS_NAME, equalTo("referenced-class-name"));
	}

	@Test
	public void testConstantValueForReferences() {
		assertThat(FormExportKeys.REFERENCES, equalTo("references"));
	}

	@Test
	public void testConstantValueForRoot() {
		assertThat(FormExportKeys.ROOT, equalTo("root"));
	}

	@Test
	public void testConstantValueForRootPortletId() {
		assertThat(FormExportKeys.ROOT_PORTLET_ID, equalTo("root-portlet-id"));
	}

	@Test
	public void testConstantValueForSchemaVersion() {
		assertThat(FormExportKeys.SCHEMA_VERSION, equalTo("schema-version"));
	}

	@Test
	public void testConstantValueForScopeGroupId() {
		assertThat(FormExportKeys.SCOPE_GROUP_ID, equalTo("scope-group-id"));
	}

	@Test
	public void testConstantValueForScopeLayoutType() {
		assertThat(FormExportKeys.SCOPE_LAYOUT_TYPE, equalTo("scope-layout-type"));
	}

	@Test
	public void testConstantValueForScopeLayoutUuid() {
		assertThat(FormExportKeys.SCOPE_LAYOUT_UUID, equalTo("scope-layout-uuid"));
	}

	@Test
	public void testConstantValueForSelfPath() {
		assertThat(FormExportKeys.SELF_PATH, equalTo("self-path"));
	}

	@Test
	public void testConstantValueForSettingsDDMFormValuesPath() {
		assertThat(FormExportKeys.SETTINGS_DDM_FORM_VALUES_PATH, equalTo("settings-ddm-form-values-path"));
	}

	@Test
	public void testConstantValueForStagedModel() {
		assertThat(FormExportKeys.STAGED_MODEL, equalTo("staged-model"));
	}

	@Test
	public void testConstantValueForStructureKey() {
		assertThat(FormExportKeys.STRUCTURE_KEY, equalTo("structure-key"));
	}

	@Test
	public void testConstantValueForType() {
		assertThat(FormExportKeys.TYPE, equalTo("type"));
	}

	@Test
	public void testConstantValueForUserPersonalSiteGroupId() {
		assertThat(FormExportKeys.USER_PERSONAL_SITE_GROUP_ID, equalTo("user-personal-site-group-id"));
	}

	@Test
	public void testConstantValueForUserUuid() {
		assertThat(FormExportKeys.USER_UUID, equalTo("user-uuid"));
	}

	@Test
	public void testConstantValueForUuid() {
		assertThat(FormExportKeys.UUID, equalTo("uuid"));
	}
}
