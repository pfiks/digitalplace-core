package com.placecube.digitalplace.ddl.model.impl;

import java.util.LinkedList;
import java.util.List;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.placecube.digitalplace.ddl.model.DynamicDataListEntry;
import com.placecube.digitalplace.ddl.model.DynamicDataListValue;

public class DynamicDataListEntryImpl implements DynamicDataListEntry {

	private final DDLRecord ddlRecord;
	private final List<DynamicDataListValue> values;

	DynamicDataListEntryImpl(DDLRecord ddlRecord) {
		this.ddlRecord = ddlRecord;
		values = new LinkedList<>();
	}

	public void addDynamicDataListValue(DynamicDataListValue value) {
		values.add(value);
	}

	@Override
	public DDLRecord getDDLRecord() {
		return ddlRecord;
	}

	@Override
	public long getId() {
		return ddlRecord.getRecordId();
	}

	@Override
	public List<DynamicDataListValue> getValues() {
		return values;
	}

}
