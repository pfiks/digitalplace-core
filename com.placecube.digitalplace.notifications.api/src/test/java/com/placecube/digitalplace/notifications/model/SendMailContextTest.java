package com.placecube.digitalplace.notifications.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import com.placecube.digitalplace.notifications.model.SendMailContext.SendMailContextBuilder;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SendMailContextTest {

	private static final String API_KEY_NAME = "23423322";
	private static final String[] BCC_EMAIL_ADDRESSES = { "test@test.com" };
	private static final String EMAIL_ADDRESS = "emailTo@test.com";
	private static final String FROM_EMAIL_ADDRESS = "emailFrom@test.com";
	private static final String FROM_NAME = "From Name";
	private static final long GROUP_ID = 4333L;
	private static final String REFERENCE = "4353433";
	private static final String TEMPLATE_ID = "TEMPLATE_ID";

	@Mock
	private Map<String, Object> mockPersonalisation;

	@Mock
	private List<File> mockAttachments;

	@Test
	public void build_WhenContextBuilderBuiltWithMinimumParameters_ThenAllSendMailContextParametersAndDefaultValuesAreSet() {
		SendMailContextBuilder builder = new SendMailContextBuilder(TEMPLATE_ID, EMAIL_ADDRESS, mockPersonalisation, REFERENCE, FROM_EMAIL_ADDRESS);
		SendMailContext context = builder.build();

		assertThat(context.getTemplateId(), equalTo(TEMPLATE_ID));
		assertThat(context.getEmailAddress(), equalTo(EMAIL_ADDRESS));
		assertThat(context.getPersonalisation(), sameInstance(mockPersonalisation));
		assertThat(context.getReference(), sameInstance(REFERENCE));
		assertThat(context.getFromEmailAddress(), sameInstance(FROM_EMAIL_ADDRESS));

		assertThat(context.getGroupId(), equalTo(0L));
		assertThat(context.getBccEmailAddresses(), notNullValue());
		assertThat(context.getBccEmailAddresses().length, equalTo(0));
		assertThat(context.getAttachments(), notNullValue());
		assertThat(context.getAttachments().size(), equalTo(0));
		assertThat(context.getApiKeyName(), nullValue());
	}

	@Test
	public void build_WhenPersonalizationIsNull_ThenSetsEmptyHashMapForPersonalisationInSendMailContext() {
		SendMailContextBuilder builder = new SendMailContextBuilder(TEMPLATE_ID, EMAIL_ADDRESS, null, REFERENCE, FROM_EMAIL_ADDRESS);
		SendMailContext context = builder.build();

		assertThat(context.getPersonalisation(), notNullValue());
		assertThat(context.getPersonalisation().size(), equalTo(0));
	}

	@Test
	public void build_WhenContextBuilderBuiltWithFromName_ThenSetsFromNameInSendMailContext() {
		SendMailContextBuilder builder = new SendMailContextBuilder(TEMPLATE_ID, EMAIL_ADDRESS, mockPersonalisation, REFERENCE, FROM_EMAIL_ADDRESS);
		builder.fromName(FROM_NAME);
		SendMailContext context = builder.build();

		assertThat(context.getFromName(), equalTo(FROM_NAME));
	}

	@Test
	public void new_WhenContextBuilderBuiltWithNotNullAttachments_ThenSetsAttachmentsInSendMailContext() {
		SendMailContextBuilder builder = new SendMailContextBuilder(TEMPLATE_ID, EMAIL_ADDRESS, mockPersonalisation, REFERENCE, FROM_EMAIL_ADDRESS);
		builder.attachments(mockAttachments);
		SendMailContext context = builder.build();

		assertThat(context.getAttachments(), sameInstance(mockAttachments));
	}

	@Test
	public void new_WhenContextBuilderBuiltWithNullAttachments_ThenKeepsEmptyAttachmentsListInSendMailContext() {
		SendMailContextBuilder builder = new SendMailContextBuilder(TEMPLATE_ID, EMAIL_ADDRESS, mockPersonalisation, REFERENCE, FROM_EMAIL_ADDRESS);
		builder.attachments(null);
		SendMailContext context = builder.build();

		assertThat(context.getAttachments(), notNullValue());
		assertThat(context.getAttachments().size(), equalTo(0));
	}

	@Test
	public void new_WhenContextBuilderBuiltWithBccEmailAddresses_ThenSetsBccEmailAddressesInSendMailContext() {
		SendMailContextBuilder builder = new SendMailContextBuilder(TEMPLATE_ID, EMAIL_ADDRESS, mockPersonalisation, REFERENCE, FROM_EMAIL_ADDRESS);
		builder.bccEmailAddresses(BCC_EMAIL_ADDRESSES);
		SendMailContext context = builder.build();

		assertThat(context.getBccEmailAddresses(), sameInstance(BCC_EMAIL_ADDRESSES));
	}

	@Test
	public void new_WhenContextBuilderBuiltWithNullBccEmailAddresses_ThenKeepsEmptyAttachmentsListInSendMailContext() {
		SendMailContextBuilder builder = new SendMailContextBuilder(TEMPLATE_ID, EMAIL_ADDRESS, mockPersonalisation, REFERENCE, FROM_EMAIL_ADDRESS);
		builder.bccEmailAddresses(null);
		SendMailContext context = builder.build();

		assertThat(context.getBccEmailAddresses(), notNullValue());
		assertThat(context.getBccEmailAddresses().length, equalTo(0));
	}

	@Test
	public void new_WhenContextBuilderBuiltWithApiKeyName_ThenSetsApiKeyNameInSendMailContext() {
		SendMailContextBuilder builder = new SendMailContextBuilder(TEMPLATE_ID, EMAIL_ADDRESS, mockPersonalisation, REFERENCE, FROM_EMAIL_ADDRESS);
		builder.apiKeyName(API_KEY_NAME);
		SendMailContext context = builder.build();

		assertThat(context.getApiKeyName(), equalTo(API_KEY_NAME));
	}

	@Test
	public void new_WhenContextBuilderBuiltWithGrouId_ThenSetsGroupIdInSendMailContext() {
		SendMailContextBuilder builder = new SendMailContextBuilder(TEMPLATE_ID, EMAIL_ADDRESS, mockPersonalisation, REFERENCE, FROM_EMAIL_ADDRESS);
		builder.groupId(GROUP_ID);
		SendMailContext context = builder.build();

		assertThat(context.getGroupId(), equalTo(GROUP_ID));
	}

}
