package com.placecube.digitalplace.user.account.service.impl;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.powermock.api.mockito.PowerMockito.when;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.PortletRequestModel;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.service.util.UserAuthenticationServiceUtil;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class UserAuthenticationServiceImplTest {

	private static final long COMPANY_ID = 32543L;
	private static final String LANGUAGE_ID = "en_GB";

	@InjectMocks
	private UserAuthenticationServiceImpl userAuthenticationServiceImpl;

	@Mock
	private UserAuthenticationServiceUtil mockUserAuthenticationServiceUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private PortletRequestModel mockPortletRequestModel;

	@Test
	public void getUserAccountPasswordPolicyHtml_WhenCalledWithPortletRequestAndConfiguredPasswordPolicyArticleReturnsContent_ThenReturnsConfiguredArticleContent() throws Exception {
		final String content = "content";

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLanguageId()).thenReturn(LANGUAGE_ID);
		when(mockUserAuthenticationServiceUtil.getPortletRequestModel(mockPortletRequest)).thenReturn(mockPortletRequestModel);
		when(mockUserAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(mockPortletRequestModel, mockThemeDisplay, LANGUAGE_ID)).thenReturn(content);

		String result = userAuthenticationServiceImpl.getUserAccountPasswordPolicyHtml(mockPortletRequest);

		assertThat(result, equalTo(content));
	}

	@Test
	public void getUserAccountPasswordPolicyHtml_WhenCalledWithPortletRequestAndConfiguredPasswordPolicyArticleReturnsEmptyContent_ThenReturnsDefaultPasswordPolicyDescription() throws Exception {
		final String description = "description";

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getLanguageId()).thenReturn(LANGUAGE_ID);
		when(mockUserAuthenticationServiceUtil.getPortletRequestModel(mockPortletRequest)).thenReturn(mockPortletRequestModel);
		when(mockUserAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(mockPortletRequestModel, mockThemeDisplay, LANGUAGE_ID)).thenReturn(StringPool.BLANK);
		when(mockUserAuthenticationServiceUtil.getDefaultPasswordPolicyDescription(COMPANY_ID)).thenReturn(description);

		String result = userAuthenticationServiceImpl.getUserAccountPasswordPolicyHtml(mockPortletRequest);

		assertThat(result, equalTo(description));
	}

	@Test
	public void getUserAccountPasswordPolicyHtml_WhenCalledWithPortletRequestAndConfiguredPasswordPolicyArticleRetrievalFailsWithPortalException_ThenReturnsDefaultPasswordPolicyDescription()
			throws Exception {
		final String description = "description";

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getLanguageId()).thenReturn(LANGUAGE_ID);
		when(mockUserAuthenticationServiceUtil.getPortletRequestModel(mockPortletRequest)).thenReturn(mockPortletRequestModel);
		when(mockUserAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(mockPortletRequestModel, mockThemeDisplay, LANGUAGE_ID)).thenThrow(new PortalException());
		when(mockUserAuthenticationServiceUtil.getDefaultPasswordPolicyDescription(COMPANY_ID)).thenReturn(description);

		String result = userAuthenticationServiceImpl.getUserAccountPasswordPolicyHtml(mockPortletRequest);

		assertThat(result, equalTo(description));
	}

	@Test(expected = PortletException.class)
	public void getUserAccountPasswordPolicyHtml_WhenCalledWithPortletRequestAndConfiguredPasswordPolicyArticleRetrievalFailsWithPortletException_ThenThrowsPorletException() throws Exception {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLanguageId()).thenReturn(LANGUAGE_ID);
		when(mockUserAuthenticationServiceUtil.getPortletRequestModel(mockPortletRequest)).thenReturn(mockPortletRequestModel);
		when(mockUserAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(mockPortletRequestModel, mockThemeDisplay, LANGUAGE_ID)).thenThrow(new PortletException());

		userAuthenticationServiceImpl.getUserAccountPasswordPolicyHtml(mockPortletRequest);
	}

	@Test
	public void getUserAccountPasswordPolicyHtml_WhenCalledWithHttpServletRequestAndConfiguredPasswordPolicyArticleReturnsContent_ThenReturnsConfiguredArticleContent() throws Exception {
		final String content = "content";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLanguageId()).thenReturn(LANGUAGE_ID);
		when(mockUserAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(null, mockThemeDisplay, LANGUAGE_ID)).thenReturn(content);

		String result = userAuthenticationServiceImpl.getUserAccountPasswordPolicyHtml(mockHttpServletRequest);

		assertThat(result, equalTo(content));
	}

	@Test
	public void getUserAccountPasswordPolicyHtml_WhenCalledWithHttpServletRequestAndConfiguredPasswordPolicyArticleReturnsEmptyContent_ThenReturnsDefaultPasswordPolicyDescription() throws Exception {
		final String description = "description";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getLanguageId()).thenReturn(LANGUAGE_ID);
		when(mockUserAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(null, mockThemeDisplay, LANGUAGE_ID)).thenReturn(StringPool.BLANK);
		when(mockUserAuthenticationServiceUtil.getDefaultPasswordPolicyDescription(COMPANY_ID)).thenReturn(description);

		String result = userAuthenticationServiceImpl.getUserAccountPasswordPolicyHtml(mockHttpServletRequest);

		assertThat(result, equalTo(description));
	}

	@Test
	public void getUserAccountPasswordPolicyHtml_WhenCalledWithHttpServletRequestAndConfiguredPasswordPolicyArticleRetrievalFailsWithPortalException_ThenReturnsDefaultPasswordPolicyDescription()
			throws Exception {
		final String description = "description";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getLanguageId()).thenReturn(LANGUAGE_ID);
		when(mockUserAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(null, mockThemeDisplay, LANGUAGE_ID)).thenThrow(new PortalException());
		when(mockUserAuthenticationServiceUtil.getDefaultPasswordPolicyDescription(COMPANY_ID)).thenReturn(description);

		String result = userAuthenticationServiceImpl.getUserAccountPasswordPolicyHtml(mockHttpServletRequest);

		assertThat(result, equalTo(description));
	}

	@Test(expected = PortletException.class)
	public void getUserAccountPasswordPolicyHtml_WhenCalledWithHttpServletRequestAndConfiguredPasswordPolicyArticleRetrievalFailsWithPortletException_ThenThrowsPorletException() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLanguageId()).thenReturn(LANGUAGE_ID);
		when(mockUserAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(null, mockThemeDisplay, LANGUAGE_ID)).thenThrow(new PortletException());

		userAuthenticationServiceImpl.getUserAccountPasswordPolicyHtml(mockHttpServletRequest);
	}
}
