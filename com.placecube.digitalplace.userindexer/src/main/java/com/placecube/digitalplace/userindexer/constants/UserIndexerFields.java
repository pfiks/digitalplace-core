package com.placecube.digitalplace.userindexer.constants;

import com.liferay.petra.string.StringPool;

public final class UserIndexerFields {

	public static final String GROUP_ROLE_IDS = "groupRoleIds";

	public static final String ROLE_IDS = "roleIds";

	public static String getRoleIdsSearchFilter(long roleId) {
		return "(" + ROLE_IDS + ":" + StringPool.QUOTE + +roleId + StringPool.QUOTE + ")";
	}

	public static String getUserGroupRoleIdsSearchFilter(long groupId, long roleId) {
		return "(" + GROUP_ROLE_IDS + ":" + StringPool.QUOTE + getUserGroupRoleSearchValue(groupId, roleId) + StringPool.QUOTE + ")";
	}

	public static String getUserGroupRoleSearchValue(long groupId, long roleId) {
		return groupId + StringPool.DASH + roleId;
	}

	private UserIndexerFields() {
	}
}
