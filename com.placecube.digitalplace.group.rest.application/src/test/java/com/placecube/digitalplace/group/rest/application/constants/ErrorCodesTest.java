package com.placecube.digitalplace.group.rest.application.constants;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

public class ErrorCodesTest extends PowerMockito {

	@Test
	public void getCode_WithInternalError_ThenReturnsTheCode() {
		String result = ErrorCodes.INTERNAL_ERROR.getCode();

		assertThat(result, equalTo("INTERNAL-ERROR"));
	}

	@Test
	public void getCode_WithInvalidSignature_ThenReturnsTheCode() {
		String result = ErrorCodes.INVALID_SIGNATURE.getCode();

		assertThat(result, equalTo("INVALID-SIGNATURE"));
	}

	@Test
	public void getCode_WithInvalidParameters_ThenReturnsTheCode() {
		String result = ErrorCodes.INVALID_PARAMETERS.getCode();

		assertThat(result, equalTo("INVALID-PARAMETERS"));
	}

	@Test
	public void getCode_WithInvalidTimestamp_ThenReturnsTheCode() {
		String result = ErrorCodes.INVALID_TIMESTAMP.getCode();

		assertThat(result, equalTo("INVALID-TIMESTAMP"));
	}

	@Test
	public void getCode_WithMissingHeaders_ThenReturnsTheCode() {
		String result = ErrorCodes.MISSING_HEADERS.getCode();

		assertThat(result, equalTo("MISSING-HEADERS"));
	}

	@Test
	public void getCode_WithInvalidAuthentication_ThenReturnsTheCode() {
		String result = ErrorCodes.INVALID_AUTHENTICATION.getCode();

		assertThat(result, equalTo("INVALID-AUTHENTICATION"));
	}

}
