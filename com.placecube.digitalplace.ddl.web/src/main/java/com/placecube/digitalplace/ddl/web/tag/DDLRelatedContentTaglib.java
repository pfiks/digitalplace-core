package com.placecube.digitalplace.ddl.web.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.ddl.web.context.ServletContextUtil;
import com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.service.DDLRelatedSearchListingsService;

public class DDLRelatedContentTaglib extends IncludeTag {

	private static final String PAGE = "/ddl-related-content/view.jsp";

	private DDLRecord ddlRecord;
	private int maxItems = 3;

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);
		setServletContext(ServletContextUtil.getServletContext());
	}

	@Override
	protected String getPage() {
		return PAGE;
	}

	@Override
	public void setAttributes(HttpServletRequest httpServletRequest) {

		DDLRelatedSearchListingsService ddlRelatedSearchListingsService = ServletContextUtil.getDDLRelatedSearchListingsService();

		ddlRelatedSearchListingsService.configureRequestAttributes(httpServletRequest, ddlRecord, maxItems);
	}

	public void setDdlRecord(DDLRecord ddlRecord) {
		this.ddlRecord = ddlRecord;
	}

	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}
}
