package com.placecube.digitalplace.userprofile.configuration;

import aQute.bnd.annotation.ProviderType;

@ProviderType
public interface UserProfileFieldsConfiguration {

	boolean addressEnabled();

	boolean addressRequired();

	boolean businessPhoneNumberEnabled();

	boolean businessPhoneNumberRequired();

	boolean dateOfBirthEnabled();

	boolean dateOfBirthRequired();

	String[] expandoFieldsSettings();

	String generatedEmailSuffix();

	boolean homePhoneNumberEnabled();

	boolean homePhoneNumberRequired();

	boolean jobTitleEnabled();

	boolean jobTitleRequired();

	boolean mobilePhoneNumberEnabled();

	boolean mobilePhoneNumberRequired();

	boolean twoFactorAuthenticationPromptEnabled();

}
