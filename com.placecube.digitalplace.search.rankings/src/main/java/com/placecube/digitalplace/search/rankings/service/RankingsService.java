package com.placecube.digitalplace.search.rankings.service;

import java.util.List;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.search.engine.adapter.SearchEngineAdapter;
import com.liferay.portal.search.engine.adapter.search.SearchSearchRequest;
import com.liferay.portal.search.engine.adapter.search.SearchSearchResponse;
import com.liferay.portal.search.filter.ComplexQueryPart;
import com.liferay.portal.search.filter.ComplexQueryPartBuilderFactory;
import com.liferay.portal.search.query.IdsQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.query.TermsQuery;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.placecube.digitalplace.search.rankings.model.SearchRanking;
import com.placecube.digitalplace.search.rankings.util.SearchRequestFactory;

@Component(service = RankingsService.class)
public class RankingsService {

	@Reference
	private ComplexQueryPartBuilderFactory complexQueryPartBuilderFactory;

	@Reference
	private Queries queries;

	@Reference
	private SearchEngineAdapter searchEngineAdapter;

	@Reference
	private SearchRequestFactory searchRequestFactory;

	public void addBlockedIdsQuery(SearchRequestBuilder searchRequestBuilder, SearchRanking searchRanking) {

		List<String> blockedIds = searchRanking.getBlockedIds();

		if (!blockedIds.isEmpty()) {
			IdsQuery idsQuery = queries.ids();
			idsQuery.addIds(ArrayUtil.toStringArray(blockedIds));
			ComplexQueryPart blockedIdsComplexQueryPart = complexQueryPartBuilderFactory.builder().query(idsQuery).additive(true).occur(BooleanClauseOccur.MUST_NOT.toString().toLowerCase()).build();
			searchRequestBuilder.addComplexQueryPart(blockedIdsComplexQueryPart);
		}

	}

	public void addPinnedIdsQueries(SearchRequestBuilder searchRequestBuilder, List<Map<String, String>> pinValues) {

		pinValues.stream().forEach(pinValue -> searchRequestBuilder.addComplexQueryPart(getPinQuery(pinValue, pinValues.size())));

	}

	public SearchSearchResponse getSearchRankingsForKeywords(String keywords, long companyId) {

		TermsQuery aliasTermsQuery = queries.terms("queryStrings");

		String[] terms = keywords.split(StringPool.SPACE);
		for (String term : terms) {
			aliasTermsQuery.addValue(term);
		}

		SearchSearchRequest searchSearchRequest = searchRequestFactory.getSearchSearchRequest();
		searchSearchRequest.setQuery(aliasTermsQuery);
		searchSearchRequest.setFetchSource(true);
		searchSearchRequest.setIndexNames(String.format("liferay-%d-search-tuning-rankings", companyId));

		return searchEngineAdapter.execute(searchSearchRequest);
	}

	public boolean isAliasWildcard(String queryString) {

		return queryString.contains(StringPool.STAR);

	}

	private ComplexQueryPart getPinQuery(Map<String, String> pinMap, int size) {

		float position = GetterUtil.getFloat(pinMap.get("position"));
		String id = pinMap.get("uid");

		IdsQuery idsQuery = queries.ids();
		idsQuery.addIds(id);
		idsQuery.setBoost((size - position) * 10000F);

		return complexQueryPartBuilderFactory.builder().query(idsQuery).additive(true).occur(BooleanClauseOccur.SHOULD.toString().toLowerCase()).build();

	}
}
