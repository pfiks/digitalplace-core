/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.appointment.exception.NoSuchAppointmentEntryException;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the appointment entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentEntryUtil
 * @generated
 */
@ProviderType
public interface AppointmentEntryPersistence
	extends BasePersistence<AppointmentEntry> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AppointmentEntryUtil} to access the appointment entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the appointment entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching appointment entries
	 */
	public java.util.List<AppointmentEntry> findByUuid(String uuid);

	/**
	 * Returns a range of all the appointment entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public AppointmentEntry findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public AppointmentEntry fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator);

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public AppointmentEntry findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public AppointmentEntry fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator);

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where uuid = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public AppointmentEntry[] findByUuid_PrevAndNext(
			long appointmentEntryId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Removes all the appointment entries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of appointment entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching appointment entries
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching appointment entries
	 */
	public java.util.List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public AppointmentEntry findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Returns the first appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public AppointmentEntry fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator);

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public AppointmentEntry findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Returns the last appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public AppointmentEntry fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator);

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public AppointmentEntry[] findByUuid_C_PrevAndNext(
			long appointmentEntryId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Removes all the appointment entries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of appointment entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching appointment entries
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @return the matching appointment entries
	 */
	public java.util.List<AppointmentEntry>
		findByCompanyId_EntryClassPK_EntryClassNameId(
			long companyId, long entryClassPK, long entryClassNameId);

	/**
	 * Returns a range of all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry>
		findByCompanyId_EntryClassPK_EntryClassNameId(
			long companyId, long entryClassPK, long entryClassNameId, int start,
			int end);

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry>
		findByCompanyId_EntryClassPK_EntryClassNameId(
			long companyId, long entryClassPK, long entryClassNameId, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator);

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry>
		findByCompanyId_EntryClassPK_EntryClassNameId(
			long companyId, long entryClassPK, long entryClassNameId, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator,
			boolean useFinderCache);

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public AppointmentEntry findByCompanyId_EntryClassPK_EntryClassNameId_First(
			long companyId, long entryClassPK, long entryClassNameId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public AppointmentEntry
		fetchByCompanyId_EntryClassPK_EntryClassNameId_First(
			long companyId, long entryClassPK, long entryClassNameId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator);

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public AppointmentEntry findByCompanyId_EntryClassPK_EntryClassNameId_Last(
			long companyId, long entryClassPK, long entryClassNameId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public AppointmentEntry fetchByCompanyId_EntryClassPK_EntryClassNameId_Last(
		long companyId, long entryClassPK, long entryClassNameId,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator);

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public AppointmentEntry[]
			findByCompanyId_EntryClassPK_EntryClassNameId_PrevAndNext(
				long appointmentEntryId, long companyId, long entryClassPK,
				long entryClassNameId,
				com.liferay.portal.kernel.util.OrderByComparator
					<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Removes all the appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 */
	public void removeByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId);

	/**
	 * Returns the number of appointment entries where companyId = &#63; and entryClassPK = &#63; and entryClassNameId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param entryClassPK the entry class pk
	 * @param entryClassNameId the entry class name ID
	 * @return the number of matching appointment entries
	 */
	public int countByCompanyId_EntryClassPK_EntryClassNameId(
		long companyId, long entryClassPK, long entryClassNameId);

	/**
	 * Returns all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @return the matching appointment entries
	 */
	public java.util.List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId);

	/**
	 * Returns a range of all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId, int start, int end);

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator);

	/**
	 * Returns an ordered range of all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching appointment entries
	 */
	public java.util.List<AppointmentEntry>
		findByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator,
			boolean useFinderCache);

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public AppointmentEntry
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_First(
				long companyId, String serviceId, String connectorClassName,
				String appointmentId,
				com.liferay.portal.kernel.util.OrderByComparator
					<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Returns the first appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public AppointmentEntry
		fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_First(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator);

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry
	 * @throws NoSuchAppointmentEntryException if a matching appointment entry could not be found
	 */
	public AppointmentEntry
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_Last(
				long companyId, String serviceId, String connectorClassName,
				String appointmentId,
				com.liferay.portal.kernel.util.OrderByComparator
					<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Returns the last appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public AppointmentEntry
		fetchByCompanyId_ServiceId_ConnectorClassName_AppointmentId_Last(
			long companyId, String serviceId, String connectorClassName,
			String appointmentId,
			com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
				orderByComparator);

	/**
	 * Returns the appointment entries before and after the current appointment entry in the ordered set where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param appointmentEntryId the primary key of the current appointment entry
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public AppointmentEntry[]
			findByCompanyId_ServiceId_ConnectorClassName_AppointmentId_PrevAndNext(
				long appointmentEntryId, long companyId, String serviceId,
				String connectorClassName, String appointmentId,
				com.liferay.portal.kernel.util.OrderByComparator
					<AppointmentEntry> orderByComparator)
		throws NoSuchAppointmentEntryException;

	/**
	 * Removes all the appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 */
	public void removeByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
		long companyId, String serviceId, String connectorClassName,
		String appointmentId);

	/**
	 * Returns the number of appointment entries where companyId = &#63; and serviceId = &#63; and connectorClassName = &#63; and appointmentId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param serviceId the service ID
	 * @param connectorClassName the connector class name
	 * @param appointmentId the appointment ID
	 * @return the number of matching appointment entries
	 */
	public int countByCompanyId_ServiceId_ConnectorClassName_AppointmentId(
		long companyId, String serviceId, String connectorClassName,
		String appointmentId);

	/**
	 * Caches the appointment entry in the entity cache if it is enabled.
	 *
	 * @param appointmentEntry the appointment entry
	 */
	public void cacheResult(AppointmentEntry appointmentEntry);

	/**
	 * Caches the appointment entries in the entity cache if it is enabled.
	 *
	 * @param appointmentEntries the appointment entries
	 */
	public void cacheResult(
		java.util.List<AppointmentEntry> appointmentEntries);

	/**
	 * Creates a new appointment entry with the primary key. Does not add the appointment entry to the database.
	 *
	 * @param appointmentEntryId the primary key for the new appointment entry
	 * @return the new appointment entry
	 */
	public AppointmentEntry create(long appointmentEntryId);

	/**
	 * Removes the appointment entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry that was removed
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public AppointmentEntry remove(long appointmentEntryId)
		throws NoSuchAppointmentEntryException;

	public AppointmentEntry updateImpl(AppointmentEntry appointmentEntry);

	/**
	 * Returns the appointment entry with the primary key or throws a <code>NoSuchAppointmentEntryException</code> if it could not be found.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry
	 * @throws NoSuchAppointmentEntryException if a appointment entry with the primary key could not be found
	 */
	public AppointmentEntry findByPrimaryKey(long appointmentEntryId)
		throws NoSuchAppointmentEntryException;

	/**
	 * Returns the appointment entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry, or <code>null</code> if a appointment entry with the primary key could not be found
	 */
	public AppointmentEntry fetchByPrimaryKey(long appointmentEntryId);

	/**
	 * Returns all the appointment entries.
	 *
	 * @return the appointment entries
	 */
	public java.util.List<AppointmentEntry> findAll();

	/**
	 * Returns a range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of appointment entries
	 */
	public java.util.List<AppointmentEntry> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of appointment entries
	 */
	public java.util.List<AppointmentEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator);

	/**
	 * Returns an ordered range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of appointment entries
	 */
	public java.util.List<AppointmentEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppointmentEntry>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the appointment entries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of appointment entries.
	 *
	 * @return the number of appointment entries
	 */
	public int countAll();

}