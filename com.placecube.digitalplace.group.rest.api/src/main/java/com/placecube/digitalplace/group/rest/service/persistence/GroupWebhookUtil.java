/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.group.rest.model.GroupWebhook;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the group webhook service. This utility wraps <code>com.placecube.digitalplace.group.rest.service.persistence.impl.GroupWebhookPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GroupWebhookPersistence
 * @generated
 */
public class GroupWebhookUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(GroupWebhook groupWebhook) {
		getPersistence().clearCache(groupWebhook);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, GroupWebhook> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<GroupWebhook> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<GroupWebhook> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<GroupWebhook> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static GroupWebhook update(GroupWebhook groupWebhook) {
		return getPersistence().update(groupWebhook);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static GroupWebhook update(
		GroupWebhook groupWebhook, ServiceContext serviceContext) {

		return getPersistence().update(groupWebhook, serviceContext);
	}

	/**
	 * Returns all the group webhooks where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the group webhooks where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<GroupWebhook> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public static GroupWebhook findByGroupId_First(
			long groupId, OrderByComparator<GroupWebhook> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public static GroupWebhook fetchByGroupId_First(
		long groupId, OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public static GroupWebhook findByGroupId_Last(
			long groupId, OrderByComparator<GroupWebhook> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public static GroupWebhook fetchByGroupId_Last(
		long groupId, OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the group webhooks before and after the current group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param webhookId the primary key of the current group webhook
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public static GroupWebhook[] findByGroupId_PrevAndNext(
			long webhookId, long groupId,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().findByGroupId_PrevAndNext(
			webhookId, groupId, orderByComparator);
	}

	/**
	 * Removes all the group webhooks where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of group webhooks where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching group webhooks
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @return the matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled) {

		return getPersistence().findByGroupId_Enabled(groupId, enabled);
	}

	/**
	 * Returns a range of all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled, int start, int end) {

		return getPersistence().findByGroupId_Enabled(
			groupId, enabled, start, end);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled, int start, int end,
		OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().findByGroupId_Enabled(
			groupId, enabled, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled, int start, int end,
		OrderByComparator<GroupWebhook> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId_Enabled(
			groupId, enabled, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public static GroupWebhook findByGroupId_Enabled_First(
			long groupId, boolean enabled,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().findByGroupId_Enabled_First(
			groupId, enabled, orderByComparator);
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public static GroupWebhook fetchByGroupId_Enabled_First(
		long groupId, boolean enabled,
		OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().fetchByGroupId_Enabled_First(
			groupId, enabled, orderByComparator);
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public static GroupWebhook findByGroupId_Enabled_Last(
			long groupId, boolean enabled,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().findByGroupId_Enabled_Last(
			groupId, enabled, orderByComparator);
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public static GroupWebhook fetchByGroupId_Enabled_Last(
		long groupId, boolean enabled,
		OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().fetchByGroupId_Enabled_Last(
			groupId, enabled, orderByComparator);
	}

	/**
	 * Returns the group webhooks before and after the current group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param webhookId the primary key of the current group webhook
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public static GroupWebhook[] findByGroupId_Enabled_PrevAndNext(
			long webhookId, long groupId, boolean enabled,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().findByGroupId_Enabled_PrevAndNext(
			webhookId, groupId, enabled, orderByComparator);
	}

	/**
	 * Removes all the group webhooks where groupId = &#63; and enabled = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 */
	public static void removeByGroupId_Enabled(long groupId, boolean enabled) {
		getPersistence().removeByGroupId_Enabled(groupId, enabled);
	}

	/**
	 * Returns the number of group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @return the number of matching group webhooks
	 */
	public static int countByGroupId_Enabled(long groupId, boolean enabled) {
		return getPersistence().countByGroupId_Enabled(groupId, enabled);
	}

	/**
	 * Returns all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @return the matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK) {

		return getPersistence().findByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK);
	}

	/**
	 * Returns a range of all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK,
		int start, int end) {

		return getPersistence().findByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK, start, end);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK,
		int start, int end, OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().findByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK, start, end,
			orderByComparator);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group webhooks
	 */
	public static List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK,
		int start, int end, OrderByComparator<GroupWebhook> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public static GroupWebhook findByGroupId_Enabled_ClassName_ClassPK_First(
			long groupId, boolean enabled, String className, long classPK,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().findByGroupId_Enabled_ClassName_ClassPK_First(
			groupId, enabled, className, classPK, orderByComparator);
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public static GroupWebhook fetchByGroupId_Enabled_ClassName_ClassPK_First(
		long groupId, boolean enabled, String className, long classPK,
		OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().fetchByGroupId_Enabled_ClassName_ClassPK_First(
			groupId, enabled, className, classPK, orderByComparator);
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public static GroupWebhook findByGroupId_Enabled_ClassName_ClassPK_Last(
			long groupId, boolean enabled, String className, long classPK,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().findByGroupId_Enabled_ClassName_ClassPK_Last(
			groupId, enabled, className, classPK, orderByComparator);
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public static GroupWebhook fetchByGroupId_Enabled_ClassName_ClassPK_Last(
		long groupId, boolean enabled, String className, long classPK,
		OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().fetchByGroupId_Enabled_ClassName_ClassPK_Last(
			groupId, enabled, className, classPK, orderByComparator);
	}

	/**
	 * Returns the group webhooks before and after the current group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param webhookId the primary key of the current group webhook
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public static GroupWebhook[]
			findByGroupId_Enabled_ClassName_ClassPK_PrevAndNext(
				long webhookId, long groupId, boolean enabled, String className,
				long classPK, OrderByComparator<GroupWebhook> orderByComparator)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().
			findByGroupId_Enabled_ClassName_ClassPK_PrevAndNext(
				webhookId, groupId, enabled, className, classPK,
				orderByComparator);
	}

	/**
	 * Removes all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 */
	public static void removeByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK) {

		getPersistence().removeByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK);
	}

	/**
	 * Returns the number of group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @return the number of matching group webhooks
	 */
	public static int countByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK) {

		return getPersistence().countByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK);
	}

	/**
	 * Caches the group webhook in the entity cache if it is enabled.
	 *
	 * @param groupWebhook the group webhook
	 */
	public static void cacheResult(GroupWebhook groupWebhook) {
		getPersistence().cacheResult(groupWebhook);
	}

	/**
	 * Caches the group webhooks in the entity cache if it is enabled.
	 *
	 * @param groupWebhooks the group webhooks
	 */
	public static void cacheResult(List<GroupWebhook> groupWebhooks) {
		getPersistence().cacheResult(groupWebhooks);
	}

	/**
	 * Creates a new group webhook with the primary key. Does not add the group webhook to the database.
	 *
	 * @param webhookId the primary key for the new group webhook
	 * @return the new group webhook
	 */
	public static GroupWebhook create(long webhookId) {
		return getPersistence().create(webhookId);
	}

	/**
	 * Removes the group webhook with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook that was removed
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public static GroupWebhook remove(long webhookId)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().remove(webhookId);
	}

	public static GroupWebhook updateImpl(GroupWebhook groupWebhook) {
		return getPersistence().updateImpl(groupWebhook);
	}

	/**
	 * Returns the group webhook with the primary key or throws a <code>NoSuchGroupWebhookException</code> if it could not be found.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public static GroupWebhook findByPrimaryKey(long webhookId)
		throws com.placecube.digitalplace.group.rest.exception.
			NoSuchGroupWebhookException {

		return getPersistence().findByPrimaryKey(webhookId);
	}

	/**
	 * Returns the group webhook with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook, or <code>null</code> if a group webhook with the primary key could not be found
	 */
	public static GroupWebhook fetchByPrimaryKey(long webhookId) {
		return getPersistence().fetchByPrimaryKey(webhookId);
	}

	/**
	 * Returns all the group webhooks.
	 *
	 * @return the group webhooks
	 */
	public static List<GroupWebhook> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of group webhooks
	 */
	public static List<GroupWebhook> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of group webhooks
	 */
	public static List<GroupWebhook> findAll(
		int start, int end, OrderByComparator<GroupWebhook> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of group webhooks
	 */
	public static List<GroupWebhook> findAll(
		int start, int end, OrderByComparator<GroupWebhook> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the group webhooks from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of group webhooks.
	 *
	 * @return the number of group webhooks
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static GroupWebhookPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(GroupWebhookPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile GroupWebhookPersistence _persistence;

}