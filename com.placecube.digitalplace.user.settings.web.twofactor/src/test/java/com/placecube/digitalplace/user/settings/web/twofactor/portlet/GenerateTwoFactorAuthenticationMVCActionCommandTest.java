package com.placecube.digitalplace.user.settings.web.twofactor.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.PortalUtil;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, SessionMessages.class })
public class GenerateTwoFactorAuthenticationMVCActionCommandTest {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MutableRenderParameters mockRenderParameters;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	@InjectMocks
	private GenerateTwoFactorAuthenticationMVCActionCommand generateTwoFactorAuthenticationMVCActionCommand;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(PortalUtil.class, SessionMessages.class);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenSetsRequestAttributes() {
		String secretKey = "secretKey";
		when(mockUserTwoFactorAuthenticationLocalService.generateSecretKey()).thenReturn(secretKey);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		generateTwoFactorAuthenticationMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY, secretKey);
		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, true);
		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, true);
		verify(mockRenderParameters, times(1)).setValue(PortletRequestKeys.CURRENT_SECTION_ID, TwoFactorAuthenticationKeys.BUNDLE_ID);
	}

}
