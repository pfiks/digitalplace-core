package com.placecube.digitalplace.ddl.web.constants;

public final class SharedSearchSessionKeys {

	public static final String APPLIED_FILTER = "ddlfieldfilter_applied_filter";

	private SharedSearchSessionKeys() {
	}
}
