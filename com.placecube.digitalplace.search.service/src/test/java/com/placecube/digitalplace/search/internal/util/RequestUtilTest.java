package com.placecube.digitalplace.search.internal.util;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SharedSearchSession.class)
public class RequestUtilTest extends PowerMockito {

	private static final long PLID = 123;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletRequest mockHttpServletRequestOriginal;

	@Mock
	private HttpSession mockHttpSession;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private SharedSearchSession mockSharedSearchSession;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private RequestUtil requestUtil;

	@Before
	public void activateSetup() {
		mockStatic(SharedSearchSession.class);
	}

	@Test
	public void getOrCreateSharedSearchSession_WhenSharedSearchSessionAlreadyInSession_ThenReturnsTheObjectFound() {
		when(mockThemeDisplay.getPlid()).thenReturn(PLID);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute("LIFERAY_SHARED_DIGITALPLACE_SHARED_SEARCH_REQUEST_" + PLID)).thenReturn(mockSharedSearchSession);

		SharedSearchSession result = requestUtil.getOrCreateSharedSearchSession(mockHttpServletRequest, mockThemeDisplay);

		assertThat(result, sameInstance(mockSharedSearchSession));
	}

	@Test
	public void getOrCreateSharedSearchSession_WhenSharedSearchSessionIsNotAlreadyInSession_ThenReturnsTheNewlyCreatedObject() {
		when(mockThemeDisplay.getPlid()).thenReturn(PLID);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute("LIFERAY_SHARED_DIGITALPLACE_SHARED_SEARCH_REQUEST_" + PLID)).thenReturn(null);
		when(SharedSearchSession.init(PLID)).thenReturn(mockSharedSearchSession);

		SharedSearchSession result = requestUtil.getOrCreateSharedSearchSession(mockHttpServletRequest, mockThemeDisplay);

		assertThat(result, sameInstance(mockSharedSearchSession));
	}

	@Test
	public void getOrCreateSharedSearchSession_WhenSharedSearchSessionIsNotAlreadyInSession_ThenSetsTheNewlyCreatedObjectInSession() {
		when(mockThemeDisplay.getPlid()).thenReturn(PLID);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute("LIFERAY_SHARED_DIGITALPLACE_SHARED_SEARCH_REQUEST_" + PLID)).thenReturn(null);
		when(SharedSearchSession.init(PLID)).thenReturn(mockSharedSearchSession);

		requestUtil.getOrCreateSharedSearchSession(mockHttpServletRequest, mockThemeDisplay);

		verify(mockHttpSession, times(1)).setAttribute("LIFERAY_SHARED_DIGITALPLACE_SHARED_SEARCH_REQUEST_" + PLID, mockSharedSearchSession);
	}

	@Test
	public void getOriginalHttpServletRequest_WhenNoError_ThenReturnsTheOriginalHttpServletRequest() {
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequestOriginal);

		HttpServletRequest result = requestUtil.getOriginalHttpServletRequest(mockPortletRequest);

		assertThat(result, sameInstance(mockHttpServletRequestOriginal));
	}

	@Test
	public void getThemeDisplay_WhenNoError_ThenReturnsTheThemeDisplay() {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		ThemeDisplay result = requestUtil.getThemeDisplay(mockPortletRequest);

		assertThat(result, sameInstance(mockThemeDisplay));
	}

	@Test
	public void removeSharedSearchSession_WhenNoError_ThenRemovesTheSharedSearchSessionAttributeFromSession() {
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);

		requestUtil.removeSharedSearchSession(mockHttpServletRequest, PLID);

		verify(mockHttpSession, times(1)).removeAttribute("LIFERAY_SHARED_DIGITALPLACE_SHARED_SEARCH_REQUEST_" + PLID);
	}

	@Test
	public void updateSharedSearchSession_WhenNoError_ThenUpdatesTheAttributeInSession() {
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockSharedSearchSession.getPlid()).thenReturn(PLID);

		requestUtil.updateSharedSearchSession(mockHttpServletRequest, mockSharedSearchSession);

		InOrder inOrder = inOrder(mockHttpSession);
		inOrder.verify(mockHttpSession, times(1)).removeAttribute("LIFERAY_SHARED_DIGITALPLACE_SHARED_SEARCH_REQUEST_" + PLID);
		inOrder.verify(mockHttpSession, times(1)).setAttribute("LIFERAY_SHARED_DIGITALPLACE_SHARED_SEARCH_REQUEST_" + PLID, mockSharedSearchSession);
	}
}
