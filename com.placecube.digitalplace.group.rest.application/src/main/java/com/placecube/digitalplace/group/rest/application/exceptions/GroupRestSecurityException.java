package com.placecube.digitalplace.group.rest.application.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;

@SuppressWarnings("serial")
public class GroupRestSecurityException extends WebApplicationException {

	public GroupRestSecurityException(ErrorCodes errorCode, Status status) {
		super(errorCode.getCode(), status);
	}

}
