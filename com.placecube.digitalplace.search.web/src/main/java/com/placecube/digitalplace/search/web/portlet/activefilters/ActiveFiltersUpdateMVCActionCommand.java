package com.placecube.digitalplace.search.web.portlet.activefilters;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.ACTIVE_FILTERS, "javax.portlet.name=" + PortletKeys.TAG_FILTER, "javax.portlet.name=" + PortletKeys.CATEGORY_FILTER,
		"javax.portlet.name=" + PortletKeys.CONTENT_TYPE_FACET, "mvc.command.name=" + MVCCommandKeys.SEARCH }, service = MVCActionCommand.class)
public class ActiveFiltersUpdateMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String filterName = ParamUtil.getString(actionRequest, "filterName");
		String filterValue = ParamUtil.getString(actionRequest, "filterValue");
		String filterLabel = ParamUtil.getString(actionRequest, "filterLabel");
		String filterColour = ParamUtil.getString(actionRequest, "filterColour");

		String cmd = ParamUtil.getString(actionRequest, "cmd");

		if ("clearAll".equals(cmd)) {

			sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, filterName);

		} else if ("add".equals(cmd)) {

			sharedSearchRequestService.addSharedActiveFilter(actionRequest, filterName, filterValue, filterLabel, filterColour);

		} else if ("remove".equals(cmd)) {

			sharedSearchRequestService.removeSharedActiveFilterValue(actionRequest, filterName, filterValue);

		}

		sharedSearchRequestService.sendRedirectFromParameter(actionRequest, actionResponse, "redirectURL");
	}

}
