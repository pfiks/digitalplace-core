package com.placecube.digitalplace.oauth.service;

import com.liferay.portal.kernel.exception.PortalException;

public interface DigitalPlaceOAuth2Service {

	String getOAuth2Token(long companyId, String clientId, String clientSecret) throws PortalException;
}
