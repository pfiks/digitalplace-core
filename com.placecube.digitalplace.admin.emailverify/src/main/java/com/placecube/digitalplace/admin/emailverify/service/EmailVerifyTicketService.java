package com.placecube.digitalplace.admin.emailverify.service;

import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.TicketConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.TicketLocalService;
import com.liferay.portal.kernel.service.UserLocalService;

@Component(immediate = true, service = EmailVerifyTicketService.class)
public class EmailVerifyTicketService {

	private static final Log LOG = LogFactoryUtil.getLog(EmailVerifyTicketService.class);

	@Reference
	private TicketLocalService ticketLocalService;

	@Reference
	private UserLocalService userLocalService;

	public boolean doesUserHavePendingTickets(long companyId, long selectedUserId) {
		return !getUserTickets(companyId, selectedUserId).isEmpty();
	}

	public void verifyUserEmailAddress(long companyId, long selectedUserId) {
		List<Ticket> userTickets = getUserTickets(companyId, selectedUserId);

		userTickets.forEach(ticket -> {
			try {
				userLocalService.verifyEmailAddress(ticket.getKey());
			} catch (PortalException e) {
				LOG.error("Unable to verify ticket with key: " + ticket.getKey() + " for user with id: " + selectedUserId + " message: " + e.getMessage());
				LOG.debug(e);
			}
		});
	}

	private List<Ticket> getUserTickets(long companyId, long selectedUserId) {
		return ticketLocalService.getTickets(companyId, User.class.getName(), selectedUserId, TicketConstants.TYPE_EMAIL_ADDRESS).stream().filter(ticket -> !ticket.isExpired()).collect(
				Collectors.toList());
	}
}
