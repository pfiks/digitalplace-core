package com.placecube.digitalplace.group.rest.application.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import junit.framework.AssertionFailedError;

public class RandomServiceTest {

	@Test
	public void generateRandomAlphaNumericString_WhenGenerating1000Strings_ThenAllAreUnique() {

		RandomService randomService = new RandomService();

		List<String> randomAlphanumericStrings = new ArrayList<>();

		for (int index = 0; index < 1000; index++) {

			String randomAlphanumericString = randomService.generateRandomAlphanumericString(10);

			if (randomAlphanumericStrings.contains(randomAlphanumericString)) {
				throw new AssertionFailedError("Random alphanumeric string " + randomAlphanumericString + " is not unique");
			}

			randomAlphanumericStrings.add(randomAlphanumericString);

		}

	}

	@Test
	public void generateRandomAlphaNumericString_WhenGenerating1000Strings_ThenAllOnlyContainAlphanumericCharacters() {

		RandomService randomService = new RandomService();
		Pattern regexPattern = Pattern.compile("^[a-zA-Z0-9]*$");

		for (int index = 0; index < 1000; index++) {

			String randomAlphanumericString = randomService.generateRandomAlphanumericString(10);
			Matcher matcher = regexPattern.matcher(randomAlphanumericString);

			assertThat(matcher.matches(), equalTo(true));

		}

	}

	@Test
	public void generateRandomAlphaNumericString_WhenTheRandomStringTargetLengthVaries_ThenRandomStringLengthMatchesTargetLength() {

		RandomService randomService = new RandomService();

		for (int index = 0; index < 1000; index++) {

			int targetStringLength = index + 1;
			String randomAlphanumericString = randomService.generateRandomAlphanumericString(targetStringLength);

			assertThat(randomAlphanumericString.length(), equalTo(targetStringLength));

		}

	}

}
