package com.placecube.digitalplace.appointment.office365.constants;

public final class Office365Constants {

	public static String getCancelEventURL(String userId, String calendarId, String appointmentId) {
		return "https://graph.microsoft.com/v1.0/users/" + userId + "/calendars/" + calendarId + "/events/" + appointmentId + "/cancel";
	}

	public static String getCreateEventURL(String userId, String calendarId) {
		return "https://graph.microsoft.com/v1.0/users/" + userId + "/calendars/" + calendarId + "/events";
	}

	public static String getRetrieveEventURL(String userId, String calendarId, String appointmentId) {
		return "https://graph.microsoft.com/v1.0/users/" + userId + "/calendars/" + calendarId + "/events/" + appointmentId;
	}

	public static String getSearchEventsURL(String userId, String calendarId, String startDateTime, String endDateTime, int entriesToReturn, String valuesToReturn) {
		return "https://graph.microsoft.com/v1.0/users/" + userId + "/calendars/" + calendarId + "/calendarView?$orderby=Start/DateTime&$top=" + entriesToReturn + "&$select=" + valuesToReturn
				+ "&startDateTime=" + startDateTime + "&endDateTime=" + endDateTime;
	}

	public static String getTokenRequestBody(String applicationId, String applicationSecret) {
		return "client_id=" + applicationId + "\n" + "&scope=https%3A%2F%2Fgraph.microsoft.com%2F.default\n" + "&client_secret=" + applicationSecret + "\n" + "&grant_type=client_credentials";
	}

	public static String getTokenRequestURL(String tenantId) {
		return "https://login.microsoftonline.com/" + tenantId + "/oauth2/v2.0/token";
	}

	private Office365Constants() {
	}
}
