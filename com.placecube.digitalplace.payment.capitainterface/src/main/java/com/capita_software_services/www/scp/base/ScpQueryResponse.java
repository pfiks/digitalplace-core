/**
 * ScpQueryResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class ScpQueryResponse  extends com.capita_software_services.www.scp.base.ScpResponse  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.StoreCardResult storeCardResult;

    private com.capita_software_services.www.scp.base.EmailResult emailResult;

    public ScpQueryResponse() {
    }

    public ScpQueryResponse(
           org.apache.axis.types.Token requestId,
           java.lang.String scpReference,
           com.capita_software_services.www.scp.base.TransactionState transactionState,
           com.capita_software_services.www.scp.base.StoreCardResult storeCardResult,
           com.capita_software_services.www.scp.base.EmailResult emailResult) {
        super(
            requestId,
            scpReference,
            transactionState);
        this.storeCardResult = storeCardResult;
        this.emailResult = emailResult;
    }


    /**
     * Gets the storeCardResult value for this ScpQueryResponse.
     *
     * @return storeCardResult
     */
    public com.capita_software_services.www.scp.base.StoreCardResult getStoreCardResult() {
        return storeCardResult;
    }


    /**
     * Sets the storeCardResult value for this ScpQueryResponse.
     *
     * @param storeCardResult
     */
    public void setStoreCardResult(com.capita_software_services.www.scp.base.StoreCardResult storeCardResult) {
        this.storeCardResult = storeCardResult;
    }


    /**
     * Gets the emailResult value for this ScpQueryResponse.
     *
     * @return emailResult
     */
    public com.capita_software_services.www.scp.base.EmailResult getEmailResult() {
        return emailResult;
    }


    /**
     * Sets the emailResult value for this ScpQueryResponse.
     *
     * @param emailResult
     */
    public void setEmailResult(com.capita_software_services.www.scp.base.EmailResult emailResult) {
        this.emailResult = emailResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScpQueryResponse)) return false;
        ScpQueryResponse other = (ScpQueryResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.storeCardResult==null && other.getStoreCardResult()==null) ||
             (this.storeCardResult!=null &&
              this.storeCardResult.equals(other.getStoreCardResult()))) &&
            ((this.emailResult==null && other.getEmailResult()==null) ||
             (this.emailResult!=null &&
              this.emailResult.equals(other.getEmailResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getStoreCardResult() != null) {
            _hashCode += getStoreCardResult().hashCode();
        }
        if (getEmailResult() != null) {
            _hashCode += getEmailResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScpQueryResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "scpQueryResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeCardResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "storeCardResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "storeCardResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "emailResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "emailResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
