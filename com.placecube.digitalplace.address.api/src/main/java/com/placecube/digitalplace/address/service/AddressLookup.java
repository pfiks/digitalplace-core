package com.placecube.digitalplace.address.service;

import java.util.Optional;
import java.util.Set;

import com.placecube.digitalplace.address.model.AddressContext;

public interface AddressLookup {

	boolean enabled(long companyId);

	Set<AddressContext> findByKeyword(long companyId, String keyword, String[] paramsAndValues);

	Set<AddressContext> findByPostcode(long companyId, String postcode, String[] paramsAndValues);

	Optional<AddressContext> findByUprn(long companyId, String uprn, String[] paramsAndValues);

	int getWeight(long companyId);

	boolean national(long companyId);
}
