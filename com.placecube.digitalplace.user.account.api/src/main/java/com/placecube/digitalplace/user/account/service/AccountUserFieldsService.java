package com.placecube.digitalplace.user.account.service;

import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

public interface AccountUserFieldsService {

	void populateContextWithExistingAccountFields(AccountContext accountContext, User user);

	void populateContextWithExistingConfiguredExpandoFields(AccountContext accountContext, User user, UserProfileFieldsConfiguration userFieldsConfiguration);
}
