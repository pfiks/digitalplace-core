package com.placecube.digitalplace.search.web.portlet.searchbar.contributor;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.SearchParameters;

public class SearchBarSharedSearchContributorTest extends PowerMockito {

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@InjectMocks
	private SearchBarSharedSearchContributor searchBarSharedSearchContributor;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenKeywordsFoundInSession_ThenConfiguresTheSearchKeywordsInTheContributorSettings() {
		String expected = "expectedVal";
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SEARCH_KEYWORD)).thenReturn(Optional.of(expected));

		searchBarSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).setSearchKeywords(expected);
	}

	@Test
	public void contribute_WhenKeywordsNotFoundInSession_ThenConfiguresAnEmptyStringAsSearchKeywordsInTheContributorSettings() {
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SEARCH_KEYWORD)).thenReturn(Optional.empty());

		searchBarSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).setSearchKeywords(StringPool.BLANK);
	}
}
