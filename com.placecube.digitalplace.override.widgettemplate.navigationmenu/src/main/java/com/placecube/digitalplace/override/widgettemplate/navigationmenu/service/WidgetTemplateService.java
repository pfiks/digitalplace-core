package com.placecube.digitalplace.override.widgettemplate.navigationmenu.service;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.NavItem;
import com.liferay.portal.kernel.util.Portal;

@Component(immediate = true, service = WidgetTemplateService.class)
public class WidgetTemplateService {

	private static final Log LOG = LogFactoryUtil.getLog(WidgetTemplateService.class);

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private Portal portal;

	public List<DDMTemplate> getNavigationMenuTemplates(long groupId) {
		DynamicQuery dynamicQuery = ddmTemplateLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", groupId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("classNameId", portal.getClassNameId(NavItem.class)));
		return ddmTemplateLocalService.dynamicQuery(dynamicQuery);
	}

	public void removeNoItemsFoundAlert(DDMTemplate ddmTemplate) {
		String regexToFindIfTheStringIsInTheTemplate = ".*(<div class=\"alert alert-info\">)(\\s*)(<@liferay.language key=\"there-are-no-menu-items-to-display\" \\/>)(\\s*)<\\/div>(\\s*).*";
		String script = ddmTemplate.getScript();
		if (script.matches(regexToFindIfTheStringIsInTheTemplate)) {
			String regexToReplaceTheExactString = "(<div class=\"alert alert-info\">)(\\s*)(<@liferay.language key=\"there-are-no-menu-items-to-display\" \\/>)(\\s*)<\\/div>";
			String updatedScript = script.replaceAll(regexToReplaceTheExactString, StringPool.BLANK);
			ddmTemplate.setScript(updatedScript);
			ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
			LOG.info("Updated NavItem template: " + ddmTemplate.getName(ddmTemplate.getDefaultLanguageId()) + ", id: " + ddmTemplate.getTemplateId());
		}
	}

}
