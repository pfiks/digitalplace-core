<%@ include file="init.jsp"%>

<div class="row">
	<div class="col-md-12 ">
		<liferay-journal:journal-article 
		articleId="<%= WebContentArticles.USER_ACCOUNT_CONFIRMATION_RESET_PASSWORD_INVOKED_BY_ADMIN.getArticleId() %>" 
		groupId="${themeDisplay.companyGroupId}" 
		showTitle="false" />	
	</div>
</div>
