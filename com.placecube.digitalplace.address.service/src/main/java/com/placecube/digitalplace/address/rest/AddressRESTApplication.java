package com.placecube.digitalplace.address.rest;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;

@Component(immediate = true, property = { //
		JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/address", //
		JaxrsWhiteboardConstants.JAX_RS_NAME + "=Address.Rest", //
		"oauth2.scopechecker.type=none", //
		"auth.verifier.guest.allowed=true", //
		"liferay.access.control.disable=true" //
}, service = Application.class)
public class AddressRESTApplication extends Application {

	private static final Log LOG = LogFactoryUtil.getLog(AddressRESTApplication.class);

	@Reference
	private AddressLookupService addressLookupService;

	@GET
	@Path("/get-address-by-uprn/{fallbackToNationalLookup}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAddressByUPRN(//
			@PathParam("fallbackToNationalLookup") boolean fallbackToNationalLookup, //
			@QueryParam("uprn") String uprn, //
			@FormParam("paramAndValue") String[] paramsAndValues, //
			@Context HttpServletRequest request) {
		try {

			long companyId = getCompanyId(request);

			LOG.debug("Retrieving addresses for companyId: " + companyId + ", uprn: " + uprn + ", fallbackToNationalLookup: " + fallbackToNationalLookup + ", paramsAndValues: "
					+ Arrays.toString(paramsAndValues));

			Optional<AddressContext> addressContext = addressLookupService.getByUprn(companyId, uprn, paramsAndValues, fallbackToNationalLookup);

			Response.ResponseBuilder responseBuilder = Response.status(Status.OK);

			if (addressContext.isPresent()) {
				String responseJSONString = JSONFactoryUtil.looseSerialize(addressContext.get());
				LOG.debug("Addresse found: " + responseJSONString);
				responseBuilder.entity(responseJSONString);
			}

			return responseBuilder.build();

		} catch (Exception e) {
			return getErrorResponse(e);
		}

	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}

	@GET
	@Path("/search-address-by-keyword/{fallbackToNationalLookup}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchAddressByKeyword(//
			@PathParam("fallbackToNationalLookup") boolean fallbackToNationalLookup, //
			@QueryParam("keyword") String keyword, //
			@FormParam("paramAndValue") String[] paramsAndValues, //
			@Context HttpServletRequest request) {

		try {
			long companyId = getCompanyId(request);
			LOG.debug("Retrieving addresses for companyId: " + companyId + ", keyword: " + keyword + ", fallbackToNationalLookup: " + fallbackToNationalLookup + ", paramsAndValues: "
					+ Arrays.toString(paramsAndValues));

			Set<AddressContext> addressContexts = addressLookupService.getByKeyword(companyId, keyword, paramsAndValues, fallbackToNationalLookup);

			String responseJSONString = JSONFactoryUtil.looseSerialize(addressContexts);

			LOG.debug("Addresses found: " + responseJSONString);
			Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
			responseBuilder.entity(responseJSONString);

			return responseBuilder.build();

		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}

	@GET
	@Path("/search-address-by-postcode/{fallbackToNationalLookup}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchAddressByPostcode(//
			@PathParam("fallbackToNationalLookup") boolean fallbackToNationalLookup, //
			@QueryParam("postcode") String postcode, //
			@FormParam("paramAndValue") String[] paramsAndValues, //
			@Context HttpServletRequest request) {

		try {
			long companyId = getCompanyId(request);
			LOG.debug("Retrieving addresses for companyId: " + companyId + ", postcode: " + postcode + ", fallbackToNationalLookup: " + fallbackToNationalLookup + ", paramsAndValues: "
					+ Arrays.toString(paramsAndValues));

			Set<AddressContext> addressContexts = addressLookupService.getByPostcode(companyId, postcode, paramsAndValues, fallbackToNationalLookup);

			String responseJSONString = JSONFactoryUtil.looseSerialize(addressContexts);

			LOG.debug("Addresses found: " + responseJSONString);

			Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
			responseBuilder.entity(responseJSONString);
			return responseBuilder.build();

		} catch (Exception e) {
			return getErrorResponse(e);
		}

	}

	private long getCompanyId(HttpServletRequest request) {
		return GetterUtil.getLong(request.getAttribute("COMPANY_ID"), 0l);
	}

	private Response getErrorResponse(Exception exception) {
		LOG.error("Unable to retrieve addresses: " + exception);
		Response.ResponseBuilder responseBuilder = Response.status(Status.INTERNAL_SERVER_ERROR);
		responseBuilder.entity(exception.getMessage());
		return responseBuilder.build();
	}

}