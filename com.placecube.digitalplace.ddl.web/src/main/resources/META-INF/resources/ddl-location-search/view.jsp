<%@ include file="../init.jsp"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys"%>
<%@page import="com.placecube.digitalplace.ddl.web.constants.LocationSearchParameters"%>
<%@page import="com.placecube.digitalplace.ddl.web.constants.PortletKeys"%>

<c:choose>

	<c:when test="${invalidConfiguration}">
		<%@ include file="/invalid-configuration.jspf" %>
	</c:when>

	<c:otherwise>

		<portlet:actionURL name="<%=MVCCommandKeys.LOCATION_SEARCH%>" var="searchActionURL" />

		<c:if test="${ not empty destinationPlid }">
			<liferay-portlet:actionURL name="<%=MVCCommandKeys.LOCATION_SEARCH %>" plid="${destinationPlid}" portletName="<%= PortletKeys.DDL_LOCATION_SEARCH_PORTLET %>" var="searchActionURL" />
		</c:if>

		<form action="${searchActionURL}" method="POST" id="<portlet:namespace/>location-search-form" name="<portlet:namespace/>location-search-form">
 			<aui:input type="hidden" value="" name="latitude" id="latitude"/>
 			<aui:input type="hidden" value="" name="longitude" id="longitude"/>
 			<aui:input type="hidden" value="" name="formattedAddress" id="formattedAddress"/>
 			<aui:input type="hidden" value="${geoLocationField}" name="geoLocationField" id="geoLocationField"/>
			<aui:input type="hidden" value="${redirectURL}" name="redirectURL" id="redirectURL"/>

			<div class="row">
				<div class="col-md-12">
					<div class="input-group">
						<label><liferay-ui:message key="search-by-location" />
							<input type="text" id="<portlet:namespace/>autocomplete" name="<portlet:namespace/>autocomplete" class="form-control" value="${formattedAddress}"/>
						</label>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<c:choose>
						<c:when test="${not empty formattedAddress}">
							<a class="btn-link" onclick="clearCurrentLocation()" href="" target="_self"><liferay-ui:message key="remove-location" /></a>
						</c:when>
						<c:otherwise>
							<a class="btn-link" onclick="getCurrentLocation()" href="" target="_self"><liferay-ui:message key="use-current-location" /></a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</form>

	</c:otherwise>
</c:choose>

<aui:script>
		$(document).ready( function () {
			var GOOGLE_API_KEY = "${googleApiKey}";
			var namespace = '<portlet:namespace/>';
			$("#" + namespace + "autocomplete").attr('placeholder', '${placeHolderText}');
			loadScript(GOOGLE_API_KEY, namespace);
		});

		$('#<portlet:namespace/>location-search-form').on('keyup keypress', function(e) {
			var keyCode = e.keyCode || e.which;
			if (keyCode === 13) {
				e.preventDefault();
				return false;
			}
		});

</aui:script>