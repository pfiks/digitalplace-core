package com.placecube.digitalplace.formadmin.web.model;

import com.placecube.digitalplace.form.model.FormDefinition;

public class FormDefinitionResult {

	private boolean installed;

	private FormDefinition formDefinition;

	private FormDefinitionResult(FormDefinition formDefinition, boolean installed) {
		this.installed = installed;
		this.formDefinition = formDefinition;
	}

	public static FormDefinitionResult create(FormDefinition formDefinition, boolean installed) {
		return new FormDefinitionResult(formDefinition, installed);
	}

	public String getName() {
		return formDefinition.getName();
	}

	public String getCategory() {
		return formDefinition.getCategory();
	}

	public String getDescription() {
		return formDefinition.getDescription();
	}

	public String getKey() {
		return formDefinition.getKey();
	}

	public boolean isInstalled() {
		return installed;
	}

}
