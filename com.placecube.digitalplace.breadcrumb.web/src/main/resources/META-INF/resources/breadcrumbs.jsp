<c:if test="${ not empty breadcrumbEntries }">
	<liferay-ddm:template-renderer
		className="<%= BreadcrumbEntry.class.getName() %>"
		displayStyle="${ displayStyle }"
		displayStyleGroupId="${ displayStyleGroupId }"
		entries="${breadcrumbEntries}"
	>
	<ol class="breadcrumb">

		<c:forEach var="breadcrumbEntry" varStatus="loop" items="${ breadcrumbEntries }">

			<li class="breadcrumb-item">

				<c:choose>
					<c:when test="${ empty breadcrumbEntry.getURL() }">

						<span class="${ loop.last ? 'active' : '' }">${ breadcrumbEntry.title }</span>

					</c:when>
					<c:otherwise>

						<a class="breadcrumb-link" href="${ breadcrumbEntry.getURL() }" title="${ breadcrumbEntry.title }">
							<span>${ breadcrumbEntry.title }</span>
						</a>

					</c:otherwise>
				</c:choose>

			</li>

		</c:forEach>

	</ol>
	</liferay-ddm:template-renderer>
</c:if>
