package com.placecube.digitalplace.navigation.constants;

public final class PortletPreferenceKeys {

	public static final String CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME = "categoryNavBuildCategoriesHierarchyUrlPropertyName";

	public static final String CATEGORY_NAV_FILTER_VOCABULARY_ID = "categoryNavFilterVocabularyId";

	public static final String CATEGORY_NAV_ROOT_ENTRY_NAME = "categoryNavRootEntryName";

	public static final String CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME = "categoryNavUrlCategoryPropertyName";

	public static final String CATEGORY_NAV_SHOW_CURRENT_LAYOUT = "categoryNavShowCurrentLayout";

	public static final String CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS = "forceRootForLayoutBreadcrumbs";

	public static final String DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB = "displayOnlyOneAssetCategoryBreadcrumb";

	public static final String DISPLAY_STYLE = "displayStyle";

	public static final String DISPLAY_STYLE_GROUP_ID = "displayStyleGroupId";

	public static final String USE_LAYOUT_URL = "useLayoutUrl";

	private PortletPreferenceKeys() {
		return;
	}

}
