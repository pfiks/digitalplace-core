package com.placecube.digitalplace.user.settings.web.registry;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;
import com.placecube.digitalplace.user.settings.web.model.UserSettingsSection;

public class UserSettingsSectionRegistryTest extends PowerMockito {

	@Mock
	private User mockUser;

	@Mock
	private UserSettingsSection mockUserSettingsSection1;

	@Mock
	private UserSettingsSection mockUserSettingsSection2;

	@Mock
	private UserSettingsSection mockUserSettingsSection3;

	@Mock
	private UserSettingsSection mockUserSettingsSection4;

	@Mock
	private UserSettingsSection mockUserSettingsSection5;

	@InjectMocks
	private UserSettingsSectionRegistry userSettingsSectionRegistry;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getUserSettingsSections_WhenNoError_ThenReturnsTheRegisteredUserSettingsSectionComponentsOrderedByDisplaySectionAscAndOrderAscThatHaveAtLeastOneValue() {
		registerComponent(mockUserSettingsSection1, DisplaySection.ACCOUNT_SETTINGS, 100, true);
		registerComponent(mockUserSettingsSection2, DisplaySection.NOTIFICATIONS, 500, true);
		registerComponent(mockUserSettingsSection3, DisplaySection.ACCOUNT_SETTINGS, 50, true);
		registerComponent(mockUserSettingsSection4, DisplaySection.ACCOUNT_SETTINGS, 70, true);
		registerComponent(mockUserSettingsSection5, DisplaySection.NOTIFICATIONS, 200, false);


		Map<DisplaySection, List<UserSettingsSection>> results = userSettingsSectionRegistry.getUserSettingsSections(mockUser);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get(DisplaySection.ACCOUNT_SETTINGS), contains(mockUserSettingsSection3, mockUserSettingsSection4, mockUserSettingsSection1));
		assertThat(results.get(DisplaySection.NOTIFICATIONS), contains(mockUserSettingsSection2));
		assertThat(results.entrySet().stream().iterator().next().getKey(), equalTo(DisplaySection.NOTIFICATIONS));

		userSettingsSectionRegistry.unsetUserSettingsSection(mockUserSettingsSection3);

		results = userSettingsSectionRegistry.getUserSettingsSections(mockUser);
		assertThat(results.size(), equalTo(2));
		assertThat(results.get(DisplaySection.ACCOUNT_SETTINGS), contains(mockUserSettingsSection4, mockUserSettingsSection1));
		assertThat(results.get(DisplaySection.NOTIFICATIONS), contains(mockUserSettingsSection2));
		assertThat(results.entrySet().stream().iterator().next().getKey(), equalTo(DisplaySection.NOTIFICATIONS));

		userSettingsSectionRegistry.unsetUserSettingsSection(mockUserSettingsSection2);

		results = userSettingsSectionRegistry.getUserSettingsSections(mockUser);
		assertThat(results.size(), equalTo(1));
		assertThat(results.get(DisplaySection.ACCOUNT_SETTINGS), contains(mockUserSettingsSection4, mockUserSettingsSection1));
		assertThat(results.entrySet().stream().iterator().next().getKey(), equalTo(DisplaySection.ACCOUNT_SETTINGS));
	}

	private void registerComponent(UserSettingsSection userSettingsSection, DisplaySection displaySection, int order, boolean displayForUser) {
		when(userSettingsSection.getDisplaySection()).thenReturn(displaySection);
		when(userSettingsSection.getDisplayOrder()).thenReturn(order);
		when(userSettingsSection.displayForUser(mockUser)).thenReturn(displayForUser);
		userSettingsSectionRegistry.setUserSettingsSection(userSettingsSection);
	}

}
