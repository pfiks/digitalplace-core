package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.util;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.util.JournalHelper;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.PortletRequestModel;
import com.liferay.portal.kernel.theme.ThemeDisplay;

@Component(immediate = true, service = {})
public class JournalHelperUtil {

	private static JournalHelper journalHelper;

	public static String diffHtml(long groupId, String articleId, double sourceVersion, double targetVersion, String languageId, PortletRequestModel portletRequestModel, ThemeDisplay themeDisplay)
			throws Exception {

		return journalHelper.diffHtml(groupId, articleId, sourceVersion, targetVersion, languageId, portletRequestModel, themeDisplay);
	}

	public static String getAbsolutePath(PortletRequest portletRequest, long folderId) throws PortalException {

		return journalHelper.getAbsolutePath(portletRequest, folderId);
	}

	public static int getRestrictionType(long folderId) {
		return journalHelper.getRestrictionType(folderId);
	}

	@Reference(unbind = "-")
	protected void setJournalHelper(JournalHelper journalHelperNew) {
		journalHelper = journalHelperNew;
	}

}