package com.placecube.digitalplace.payment.heycentric.service.internal.arp;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricARPConstants;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.model.PaymentResponse;

@Component(immediate = true, service = HeycentricARPResponseParser.class)
public class HeycentricARPResponseParser {

	public PaymentResponse parsePaymentResponse(JSONObject webhookPayload) {
		String paymentResult = webhookPayload.getString(HeycentricPaymentConstants.JSON_PAYLOAD_RESULT);

		if (HeycentricARPConstants.AUTHORISED.equalsIgnoreCase(paymentResult)) {
			String paymentReference = webhookPayload.getString(HeycentricPaymentConstants.JSON_PAYLOAD_RECEIPT);
			PaymentStatus successStatus = PaymentStatus.success();
			successStatus.setPaymentUniqueReferenceNumber(paymentReference);
			return new PaymentResponse(StringPool.BLANK, successStatus);
		} else if (HeycentricARPConstants.INCOMPLETE.equalsIgnoreCase(paymentResult)) {
			return new PaymentResponse(StringPool.BLANK, PaymentStatus.aborted());
		} else {
			return new PaymentResponse(StringPool.BLANK, PaymentStatus.paymentError());
		}
	}
}
