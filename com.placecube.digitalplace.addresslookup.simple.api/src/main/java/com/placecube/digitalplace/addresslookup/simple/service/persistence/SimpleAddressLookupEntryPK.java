/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.service.persistence;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class SimpleAddressLookupEntryPK
	implements Comparable<SimpleAddressLookupEntryPK>, Serializable {

	public long companyId;
	public String uprn;

	public SimpleAddressLookupEntryPK() {
	}

	public SimpleAddressLookupEntryPK(long companyId, String uprn) {
		this.companyId = companyId;
		this.uprn = uprn;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getUprn() {
		return uprn;
	}

	public void setUprn(String uprn) {
		this.uprn = uprn;
	}

	@Override
	public int compareTo(SimpleAddressLookupEntryPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (companyId < pk.companyId) {
			value = -1;
		}
		else if (companyId > pk.companyId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = uprn.compareTo(pk.uprn);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof SimpleAddressLookupEntryPK)) {
			return false;
		}

		SimpleAddressLookupEntryPK pk = (SimpleAddressLookupEntryPK)object;

		if ((companyId == pk.companyId) && uprn.equals(pk.uprn)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, companyId);
		hashCode = HashUtil.hash(hashCode, uprn);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(6);

		sb.append("{");

		sb.append("companyId=");

		sb.append(companyId);
		sb.append(", uprn=");

		sb.append(uprn);

		sb.append("}");

		return sb.toString();
	}

}