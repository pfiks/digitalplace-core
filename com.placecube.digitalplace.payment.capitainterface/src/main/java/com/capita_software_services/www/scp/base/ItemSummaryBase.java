/**
 * ItemSummaryBase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class ItemSummaryBase  implements java.io.Serializable {
    private org.apache.axis.types.Token lineId;

    private org.apache.axis.types.PositiveInteger continuousAuditNumber;

    public ItemSummaryBase() {
    }

    public ItemSummaryBase(
           org.apache.axis.types.Token lineId,
           org.apache.axis.types.PositiveInteger continuousAuditNumber) {
           this.lineId = lineId;
           this.continuousAuditNumber = continuousAuditNumber;
    }


    /**
     * Gets the lineId value for this ItemSummaryBase.
     *
     * @return lineId
     */
    public org.apache.axis.types.Token getLineId() {
        return lineId;
    }


    /**
     * Sets the lineId value for this ItemSummaryBase.
     *
     * @param lineId
     */
    public void setLineId(org.apache.axis.types.Token lineId) {
        this.lineId = lineId;
    }


    /**
     * Gets the continuousAuditNumber value for this ItemSummaryBase.
     *
     * @return continuousAuditNumber
     */
    public org.apache.axis.types.PositiveInteger getContinuousAuditNumber() {
        return continuousAuditNumber;
    }


    /**
     * Sets the continuousAuditNumber value for this ItemSummaryBase.
     *
     * @param continuousAuditNumber
     */
    public void setContinuousAuditNumber(org.apache.axis.types.PositiveInteger continuousAuditNumber) {
        this.continuousAuditNumber = continuousAuditNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ItemSummaryBase)) return false;
        ItemSummaryBase other = (ItemSummaryBase) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.lineId==null && other.getLineId()==null) ||
             (this.lineId!=null &&
              this.lineId.equals(other.getLineId()))) &&
            ((this.continuousAuditNumber==null && other.getContinuousAuditNumber()==null) ||
             (this.continuousAuditNumber!=null &&
              this.continuousAuditNumber.equals(other.getContinuousAuditNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLineId() != null) {
            _hashCode += getLineId().hashCode();
        }
        if (getContinuousAuditNumber() != null) {
            _hashCode += getContinuousAuditNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ItemSummaryBase.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "itemSummaryBase"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lineId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "token"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("continuousAuditNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "continuousAuditNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "positiveInteger"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
