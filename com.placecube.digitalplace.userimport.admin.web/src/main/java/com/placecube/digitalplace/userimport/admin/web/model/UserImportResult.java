package com.placecube.digitalplace.userimport.admin.web.model;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.Validator;

public class UserImportResult {

	private User user;
	private List<String> exceptionMessages;
	private final int index;

	public UserImportResult(int index) {
		this.index = index + 1;
		exceptionMessages = new ArrayList<>();
	}

	public void addExceptionMessage(Exception exception) {
		exceptionMessages.add(exception.getMessage());
	}

	public String getFriendlyImportResultMessage(ResourceBundle resourceBundle) {
		String errorMessage = exceptionMessages.stream().collect(Collectors.joining(StringPool.COMMA_AND_SPACE));
		String indexValue = getIndex();

		String[] args;
		String messageKey;

		if (Validator.isNull(user)) {
			messageKey = "line-import-failed";
			args = new String[] { indexValue, errorMessage };

		} else {

			if (Validator.isNull(errorMessage)) {
				messageKey = "line-import-successfull";
				args = new String[] { indexValue, user.getFullName(),Long.toString(user.getUserId()) };

			} else {
				messageKey = "line-import-successfull-with-warnings";
				args = new String[] { indexValue, user.getFullName(), Long.toString(user.getUserId()), errorMessage };
			}
		}

		if (resourceBundle != null) {
			return LanguageUtil.format(resourceBundle, messageKey, args);
		} else {
			return StringPool.BLANK;
		}
	}

	public String getIndex() {
		return String.valueOf(index);
	}

	public final void setUser(User user) {
		this.user = user;
	}

}
