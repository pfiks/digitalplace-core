package com.placecube.digitalplace.connector.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.osgi.service.tracker.collections.map.ServiceReferenceMapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.placecube.digitalplace.connector.constants.ConnectorConstants;
import com.placecube.digitalplace.connector.model.Connector;
import com.placecube.digitalplace.connector.model.ConnectorRegistryContext;
import com.placecube.digitalplace.connector.service.impl.util.ConnectorRegistryUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceTrackerMapFactory.class, ServiceTrackerCustomizerFactory.class, LanguageUtil.class, ResourceBundleUtil.class })
public class ConnectorRegistryServiceImplTest {

	private interface TestConnector1 extends Connector {
	}

	private interface TestConnector2 extends Connector {
	}

	private static final String CONNECTOR_INTERFACE_NAME = "Connector Interface";
	private static final String CONNECTOR_NAME_1 = "connector-1";
	private static final String CONNECTOR_NAME_2 = "connector-2";

	private static final String LOCALISED_CONNECTOR_NAME_1 = "Connector 1";

	private static final String LOCALISED_CONNECTOR_NAME_2 = "Connector 2";

	@InjectMocks
	private ConnectorRegistryServiceImpl connectorRegistryServiceImpl;

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private TestConnector1 mockConnector1;

	@Mock
	private TestConnector2 mockConnector2;

	@Mock
	private ConnectorRegistryContext mockConnectorRegistryContext;

	@Mock
	private ConnectorRegistryUtil mockConnectorRegistryUtil;

	@Mock
	private ServiceWrapper<Connector> mockConnectorServiceWrapper1;

	@Mock
	private ServiceWrapper<Connector> mockConnectorServiceWrapper2;

	@Mock
	private ResourceBundle mockResourceBundle1;

	@Mock
	private ResourceBundle mockResourceBundle2;

	@Mock
	private ServiceReferenceMapper<String, Connector> mockServiceReferenceMapper;

	@Mock
	private ServiceTrackerCustomizer<Connector, ServiceWrapper<Connector>> mockServiceTrackerCustomizer;

	@Mock
	private ServiceTrackerMap<String, List<ServiceWrapper<Connector>>> mockServiceTrackerMap;

	@Test
	public void createConnectorRegistryContext_WhenNoErrors_ThenReturnsConnectorRegistryContextWithConnectorTypeAndConnectorClassName() {
		final String connectorType = "connectorType";
		final String connectorClassName = "connector.Classname";

		ConnectorRegistryContext result = connectorRegistryServiceImpl.createConnectorRegistryContext(connectorType, connectorClassName);

		assertThat(result, notNullValue());
		assertThat(result.getConnectorType(), equalTo(connectorType));
		assertThat(result.getConnectorClassName(), equalTo(connectorClassName));
	}

	@Test
	public void deactivate_WhenNoErrors_ThenClosesServiceTrackMapper() {
		connectorRegistryServiceImpl.deactivate();

		verify(mockServiceTrackerMap, times(1)).close();
	}

	@Test
	public void getConnector_WhenConnectorServiceIsFound_ThenReturnsConnectorServiceWrapper() {
		when(mockConnectorRegistryContext.getConnectorType()).thenReturn(CONNECTOR_INTERFACE_NAME);
		when(mockConnectorRegistryContext.getConnectorClassName()).thenReturn(mockConnector1.getClass().getName());

		Optional<ServiceWrapper<Connector>> result = connectorRegistryServiceImpl.getConnector(mockConnectorRegistryContext);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockConnectorServiceWrapper1));
	}

	@Test
	public void getConnector_WhenNoConnectorsFound_ThenReturnsEmptyOptional() {
		Optional<ServiceWrapper<Connector>> result = connectorRegistryServiceImpl.getConnector(mockConnectorRegistryContext);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getConnector_WhenTheConnectorIsNotFound_ThenReturnsEmptyOptional() {
		when(mockConnectorRegistryContext.getConnectorType()).thenReturn(CONNECTOR_INTERFACE_NAME);
		when(mockConnectorRegistryContext.getConnectorClassName()).thenReturn("Invalid.Connector");

		Optional<ServiceWrapper<Connector>> result = connectorRegistryServiceImpl.getConnector(mockConnectorRegistryContext);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getConnectorNames_WhenConnectorNamePropertyIsNull_ThenReturnsAMapOfAllTheLocalisedConnectorClassNames() {
		when(ResourceBundleUtil.getBundle("content/Language", Locale.UK, mockConnector1.getClass())).thenReturn(mockResourceBundle1);
		when(ResourceBundleUtil.getBundle("content/Language", Locale.UK, mockConnector2.getClass())).thenReturn(mockResourceBundle2);

		when(LanguageUtil.get(mockResourceBundle1, mockConnector1.getClass().getName())).thenReturn(LOCALISED_CONNECTOR_NAME_1);
		when(LanguageUtil.get(mockResourceBundle2, mockConnector2.getClass().getName())).thenReturn(LOCALISED_CONNECTOR_NAME_2);

		when(mockConnectorServiceWrapper1.getProperties()).thenReturn(new HashMap<>());
		when(mockConnectorServiceWrapper2.getProperties()).thenReturn(new HashMap<>());

		Map<String, String> result = connectorRegistryServiceImpl.getConnectorNames(CONNECTOR_INTERFACE_NAME, Locale.UK);

		Object connectorNames[] = result.values().toArray();

		assertThat(connectorNames.length, equalTo(2));
		assertThat(result.get(mockConnector1.getClass().getName()), equalTo(LOCALISED_CONNECTOR_NAME_1));
		assertThat(result.get(mockConnector2.getClass().getName()), equalTo(LOCALISED_CONNECTOR_NAME_2));
	}

	@Test(expected = IllegalStateException.class)
	public void getConnectorNames_WhenConnectorNamesAreDuplicate_ThenThorwsIllegalStateException() {
		when(mockConnectorServiceWrapper1.getService()).thenReturn(mockConnector1);
		when(mockConnectorServiceWrapper2.getService()).thenReturn(mockConnector1);

		when(ResourceBundleUtil.getBundle("content/Language", Locale.UK, mockConnector1.getClass())).thenReturn(mockResourceBundle1);

		when(LanguageUtil.get(mockResourceBundle1, CONNECTOR_NAME_1)).thenReturn(LOCALISED_CONNECTOR_NAME_1);
		when(LanguageUtil.get(mockResourceBundle1, CONNECTOR_NAME_2)).thenReturn(LOCALISED_CONNECTOR_NAME_2);

		connectorRegistryServiceImpl.getConnectorNames(CONNECTOR_INTERFACE_NAME, Locale.UK);
	}

	@Test
	public void getConnectorNames_WhenNoErrors_ThenReturnsAMapOfAllTheConnectorNamesSorted() {
		when(ResourceBundleUtil.getBundle("content/Language", Locale.UK, mockConnector1.getClass())).thenReturn(mockResourceBundle1);
		when(ResourceBundleUtil.getBundle("content/Language", Locale.UK, mockConnector2.getClass())).thenReturn(mockResourceBundle2);

		when(LanguageUtil.get(mockResourceBundle1, CONNECTOR_NAME_1)).thenReturn(LOCALISED_CONNECTOR_NAME_1);
		when(LanguageUtil.get(mockResourceBundle2, CONNECTOR_NAME_2)).thenReturn(LOCALISED_CONNECTOR_NAME_2);

		Map<String, String> result = connectorRegistryServiceImpl.getConnectorNames(CONNECTOR_INTERFACE_NAME, Locale.UK);

		Object connectorNames[] = result.values().toArray();

		assertThat(connectorNames.length, equalTo(2));
		assertThat(connectorNames[0].toString(), equalTo(LOCALISED_CONNECTOR_NAME_2));
		assertThat(connectorNames[1].toString(), equalTo(LOCALISED_CONNECTOR_NAME_1));

		assertThat(result.get(mockConnector1.getClass().getName()), equalTo(LOCALISED_CONNECTOR_NAME_1));
		assertThat(result.get(mockConnector2.getClass().getName()), equalTo(LOCALISED_CONNECTOR_NAME_2));
	}

	@Test
	public void getConnectors_WhenConnectorsAreFound_ThenReturnsAllTheConnectorsSorted() {

		List<ServiceWrapper<Connector>> result = connectorRegistryServiceImpl.getConnectors(CONNECTOR_INTERFACE_NAME);

		assertThat(result.size(), equalTo(2));
		assertThat(result.get(0), sameInstance(mockConnectorServiceWrapper2));
		assertThat(result.get(1), sameInstance(mockConnectorServiceWrapper1));
	}

	@Test
	public void getConnectors_WhenConnectorsAreNotFound_ThenReturnsEmptyList() {
		when(mockServiceTrackerMap.getService(CONNECTOR_INTERFACE_NAME)).thenReturn(null);

		List<ServiceWrapper<Connector>> result = connectorRegistryServiceImpl.getConnectors(CONNECTOR_INTERFACE_NAME);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Before
	public void setUp() throws Exception {
		mockStatic(ServiceTrackerMapFactory.class, ServiceTrackerCustomizerFactory.class, LanguageUtil.class, ResourceBundleUtil.class);

		mockServiceTrackerMapAndConnectors();
	}

	private void mockServiceTrackerMapAndConnectors() {
		Map<String, Object> connector1ServiceWrapperProperties = new HashMap<>();
		connector1ServiceWrapperProperties.put(ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY, 10);
		connector1ServiceWrapperProperties.put(ConnectorConstants.CONNECTOR_NAME, CONNECTOR_NAME_1);
		when(mockConnectorServiceWrapper1.getProperties()).thenReturn(connector1ServiceWrapperProperties);
		when(mockConnectorServiceWrapper1.getService()).thenReturn(mockConnector1);

		Map<String, Object> connector2ServiceWrapperProperties = new HashMap<>();
		connector2ServiceWrapperProperties.put(ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY, 1);
		connector2ServiceWrapperProperties.put(ConnectorConstants.CONNECTOR_NAME, CONNECTOR_NAME_2);
		when(mockConnectorServiceWrapper2.getProperties()).thenReturn(connector2ServiceWrapperProperties);
		when(mockConnectorServiceWrapper2.getService()).thenReturn(mockConnector2);

		List<ServiceWrapper<Connector>> connectors = new ArrayList<>();
		connectors.add(mockConnectorServiceWrapper1);
		connectors.add(mockConnectorServiceWrapper2);

		when(mockServiceTrackerMap.getService(CONNECTOR_INTERFACE_NAME)).thenReturn(connectors);

		when(mockConnectorRegistryUtil.getInterfaceServiceReferenceMapper()).thenReturn(mockServiceReferenceMapper);
		when(ServiceTrackerCustomizerFactory.<Connector>serviceWrapper(mockBundleContext)).thenReturn(mockServiceTrackerCustomizer);
		when(ServiceTrackerMapFactory.openMultiValueMap(mockBundleContext, Connector.class, null, mockServiceReferenceMapper, mockServiceTrackerCustomizer)).thenReturn(mockServiceTrackerMap);

		connectorRegistryServiceImpl.activate(mockBundleContext);
	}
}
