package com.placecube.digitalplace.payment.govuk.service.commerce.servlet;

import static com.placecube.digitalplace.payment.constants.PaymentConstants.API_AUTH_KEY;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.commerce.model.CommerceOrder;
import com.liferay.commerce.payment.engine.CommercePaymentEngine;
import com.liferay.commerce.payment.engine.CommerceSubscriptionEngine;
import com.liferay.commerce.payment.util.CommercePaymentHttpHelper;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.servlet.PortalSessionThreadLocal;
import com.liferay.portal.kernel.settings.GroupServiceSettingsLocator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.govuk.service.commerce.GovUKCommerceConstants;
import com.placecube.digitalplace.payment.govuk.service.commerce.configuration.GovUKPayGroupServiceConfiguration;
import com.placecube.digitalplace.payment.govuk.service.internal.GovUKPaymentOrderFactory;
import com.placecube.digitalplace.payment.service.PaymentConnector;

@Component(
	property = {
		"osgi.http.whiteboard.context.path=/" + GovUKCommerceConstants.SERVLET_PATH,
		"osgi.http.whiteboard.servlet.name=com.placecube.digitalplace.payment.govuk.service.commerce.servlet.CommercePaymentMethodPayPalServlet",
		"osgi.http.whiteboard.servlet.pattern=/" + GovUKCommerceConstants.SERVLET_PATH + "/*"
	},
	service = Servlet.class
)
public class GovUKPayCommercePaymentMethodServlet extends HttpServlet {

	private static final Log LOG = LogFactoryUtil.getLog(GovUKPayCommercePaymentMethodServlet.class);

	@Reference
	private CommercePaymentEngine commercePaymentEngine;

	@Reference
	private CommercePaymentHttpHelper commercePaymentHttpHelper;

	@Reference
	private CommerceSubscriptionEngine commerceSubscriptionEngine;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private GovUKPaymentOrderFactory govUKPaymentOrderFactory;

	@Reference(target = "(" + PaymentConnector.PAYMENT_CONNECTOR_KEY + "=" + GovUKCommerceConstants.PAYMENT_ENGINE_KEY + ")")
	private PaymentConnector govUKPaymentConnector;

	@Override
	protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

		try {
			if (PortalSessionThreadLocal.getHttpSession() == null) {
				PortalSessionThreadLocal.setHttpSession(httpServletRequest.getSession());
			}
			ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
			serviceContext.setRequest(httpServletRequest);

			CommerceOrder commerceOrder = commercePaymentHttpHelper.getCommerceOrder(httpServletRequest);
			String paymentId = govUKPaymentOrderFactory.getPaymentIdFromSession(httpServletRequest);

			GovUKPayGroupServiceConfiguration configuration = configurationProvider.getConfiguration(
					GovUKPayGroupServiceConfiguration.class, new GroupServiceSettingsLocator(commerceOrder.getGroupId(), GovUKCommerceConstants.SERVICE_NAME));
			serviceContext.setAttribute(API_AUTH_KEY, configuration.keyName());
			PaymentStatus paymentStatus = govUKPaymentConnector.getPaymentStatus("paymentReference", serviceContext);

			if (PaymentStatus.paymentError().getValue().equals(paymentStatus.getValue()) && GovUKCommerceConstants.CANCEL_PAYMENT_RESULT_CODE.equals(paymentStatus.getCode())) {
				commercePaymentEngine.cancelPayment(commerceOrder.getCommerceOrderId(), StringPool.BLANK, httpServletRequest);
			}
			else {
				String orderType = ParamUtil.getString(httpServletRequest, "orderType");

				if (orderType.equals("normal")) {
					commercePaymentEngine.completePayment(commerceOrder.getCommerceOrderId(), paymentId, httpServletRequest);
				} else if (orderType.equals("subscription")) {
					commerceSubscriptionEngine.completeRecurringPayment(commerceOrder.getCommerceOrderId(), paymentId, httpServletRequest);
				}
			}

			String redirect = ParamUtil.getString(httpServletRequest, "redirect");

			httpServletResponse.sendRedirect(redirect);
		}
		catch (Exception exception) {
			LOG.error(exception);
		}
	}

}