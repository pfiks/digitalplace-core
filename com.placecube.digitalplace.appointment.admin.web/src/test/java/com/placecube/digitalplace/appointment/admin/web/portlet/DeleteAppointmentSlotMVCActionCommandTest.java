package com.placecube.digitalplace.appointment.admin.web.portlet;

import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.appointment.admin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.service.AppointmentSlotLocalService;

@RunWith(MockitoJUnitRunner.class)
public class DeleteAppointmentSlotMVCActionCommandTest {

	@InjectMocks
	private DeleteAppointmentSlotMVCActionCommand deleteAppointmentSlotMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private AppointmentSlotLocalService mockAppointmentSlotLocalService;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	private MockedStatic<ParamUtil> mockParamUtil;

	private MockedStatic<PropsUtil> mockPropsUtil;

	@Before
	public void activateSetUp() {
		mockPropsUtil = mockStatic(PropsUtil.class);
		mockParamUtil = mockStatic(ParamUtil.class);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenExceptionDeletingTheEntry_ThenThrowsException() throws Exception {
		long appointmentSlotId = 11;
		mockParamUtil.when(() -> ParamUtil.getLong(mockActionRequest, "appointmentSlotId")).thenReturn(appointmentSlotId);
		when(mockAppointmentSlotLocalService.deleteAppointmentSlot(appointmentSlotId)).thenThrow(new PortalException());

		deleteAppointmentSlotMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenNoErrorAndInvalidId_ThenDoesNotDeleteTheEntry() throws Exception {
		long appointmentSlotId = 0;
		String serviceId = "myServiceId";
		mockParamUtil.when(() -> ParamUtil.getLong(mockActionRequest, "appointmentSlotId")).thenReturn(appointmentSlotId);
		mockParamUtil.when(() -> ParamUtil.getString(mockActionRequest, "serviceId", "")).thenReturn(serviceId);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		deleteAppointmentSlotMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyNoInteractions(mockAppointmentSlotLocalService);
		verify(mockMutableRenderParameters, times(1)).setValue("serviceId", serviceId);
		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_ALL_SLOTS);
	}

	@Test
	public void doProcessAction_WhenNoErrorAndValidId_ThenDeletesTheEntry() throws Exception {
		long appointmentSlotId = 11;
		String serviceId = "myServiceId";
		mockParamUtil.when(() -> ParamUtil.getLong(mockActionRequest, "appointmentSlotId")).thenReturn(appointmentSlotId);
		mockParamUtil.when(() -> ParamUtil.getString(mockActionRequest, "serviceId", "")).thenReturn(serviceId);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		deleteAppointmentSlotMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAppointmentSlotLocalService, times(1)).deleteAppointmentSlot(appointmentSlotId);
		verify(mockMutableRenderParameters, times(1)).setValue("serviceId", serviceId);
		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_ALL_SLOTS);
	}

	@After
	public void teardown() {
		mockPropsUtil.close();
		mockParamUtil.close();
	}

}
