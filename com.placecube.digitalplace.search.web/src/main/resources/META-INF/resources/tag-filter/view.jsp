<%@ include file="/init.jsp"%>

<c:choose>
	
	<c:when test="${invalidConfiguration}">
		<%@ include file="/common/invalid-configuration.jspf" %>
	</c:when>
	
	<c:otherwise>
		<c:set var="sectionTitleKey" value="tags" />
		<c:set var="availableResults" value="${availableTags}" />
		<c:set var="availableShowMoreResults" value="${availableShowMoreTags}" />
		<c:set var="facetFieldName" value="<%= SearchParameters.SELECTED_TAGS %>" />

		<%@ include file="/common/facet-checkbox-view.jspf" %>
	</c:otherwise>
</c:choose>


