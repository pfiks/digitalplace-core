/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.authentication.twofactor.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.authentication.twofactor.exception.NoSuchUserTwoFactorAuthenticationException;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;

import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the user two factor authentication service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTwoFactorAuthenticationUtil
 * @generated
 */
@ProviderType
public interface UserTwoFactorAuthenticationPersistence
	extends BasePersistence<UserTwoFactorAuthentication> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserTwoFactorAuthenticationUtil} to access the user two factor authentication persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the user two factor authentications where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findByUuid(String uuid);

	/**
	 * Returns a range of all the user two factor authentications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @return the range of matching user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the user two factor authentications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator);

	/**
	 * Returns an ordered range of all the user two factor authentications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a matching user two factor authentication could not be found
	 */
	public UserTwoFactorAuthentication findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<UserTwoFactorAuthentication> orderByComparator)
		throws NoSuchUserTwoFactorAuthenticationException;

	/**
	 * Returns the first user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	public UserTwoFactorAuthentication fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator);

	/**
	 * Returns the last user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a matching user two factor authentication could not be found
	 */
	public UserTwoFactorAuthentication findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<UserTwoFactorAuthentication> orderByComparator)
		throws NoSuchUserTwoFactorAuthenticationException;

	/**
	 * Returns the last user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	public UserTwoFactorAuthentication fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator);

	/**
	 * Returns the user two factor authentications before and after the current user two factor authentication in the ordered set where uuid = &#63;.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the current user two factor authentication
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a user two factor authentication with the primary key could not be found
	 */
	public UserTwoFactorAuthentication[] findByUuid_PrevAndNext(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK,
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<UserTwoFactorAuthentication> orderByComparator)
		throws NoSuchUserTwoFactorAuthenticationException;

	/**
	 * Removes all the user two factor authentications where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of user two factor authentications where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching user two factor authentications
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @return the range of matching user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator);

	/**
	 * Returns an ordered range of all the user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a matching user two factor authentication could not be found
	 */
	public UserTwoFactorAuthentication findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<UserTwoFactorAuthentication> orderByComparator)
		throws NoSuchUserTwoFactorAuthenticationException;

	/**
	 * Returns the first user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	public UserTwoFactorAuthentication fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator);

	/**
	 * Returns the last user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a matching user two factor authentication could not be found
	 */
	public UserTwoFactorAuthentication findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<UserTwoFactorAuthentication> orderByComparator)
		throws NoSuchUserTwoFactorAuthenticationException;

	/**
	 * Returns the last user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	public UserTwoFactorAuthentication fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator);

	/**
	 * Returns the user two factor authentications before and after the current user two factor authentication in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the current user two factor authentication
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a user two factor authentication with the primary key could not be found
	 */
	public UserTwoFactorAuthentication[] findByUuid_C_PrevAndNext(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK,
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<UserTwoFactorAuthentication> orderByComparator)
		throws NoSuchUserTwoFactorAuthenticationException;

	/**
	 * Removes all the user two factor authentications where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of user two factor authentications where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching user two factor authentications
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Caches the user two factor authentication in the entity cache if it is enabled.
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 */
	public void cacheResult(
		UserTwoFactorAuthentication userTwoFactorAuthentication);

	/**
	 * Caches the user two factor authentications in the entity cache if it is enabled.
	 *
	 * @param userTwoFactorAuthentications the user two factor authentications
	 */
	public void cacheResult(
		java.util.List<UserTwoFactorAuthentication>
			userTwoFactorAuthentications);

	/**
	 * Creates a new user two factor authentication with the primary key. Does not add the user two factor authentication to the database.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key for the new user two factor authentication
	 * @return the new user two factor authentication
	 */
	public UserTwoFactorAuthentication create(
		UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK);

	/**
	 * Removes the user two factor authentication with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication that was removed
	 * @throws NoSuchUserTwoFactorAuthenticationException if a user two factor authentication with the primary key could not be found
	 */
	public UserTwoFactorAuthentication remove(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK)
		throws NoSuchUserTwoFactorAuthenticationException;

	public UserTwoFactorAuthentication updateImpl(
		UserTwoFactorAuthentication userTwoFactorAuthentication);

	/**
	 * Returns the user two factor authentication with the primary key or throws a <code>NoSuchUserTwoFactorAuthenticationException</code> if it could not be found.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication
	 * @throws NoSuchUserTwoFactorAuthenticationException if a user two factor authentication with the primary key could not be found
	 */
	public UserTwoFactorAuthentication findByPrimaryKey(
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK)
		throws NoSuchUserTwoFactorAuthenticationException;

	/**
	 * Returns the user two factor authentication with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication, or <code>null</code> if a user two factor authentication with the primary key could not be found
	 */
	public UserTwoFactorAuthentication fetchByPrimaryKey(
		UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK);

	/**
	 * Returns all the user two factor authentications.
	 *
	 * @return the user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findAll();

	/**
	 * Returns a range of all the user two factor authentications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @return the range of user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findAll(
		int start, int end);

	/**
	 * Returns an ordered range of all the user two factor authentications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator);

	/**
	 * Returns an ordered range of all the user two factor authentications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of user two factor authentications
	 */
	public java.util.List<UserTwoFactorAuthentication> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<UserTwoFactorAuthentication> orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the user two factor authentications from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of user two factor authentications.
	 *
	 * @return the number of user two factor authentications
	 */
	public int countAll();

	public Set<String> getCompoundPKColumnNames();

}