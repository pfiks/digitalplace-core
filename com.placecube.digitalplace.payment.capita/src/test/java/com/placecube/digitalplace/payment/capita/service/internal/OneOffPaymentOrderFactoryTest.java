package com.placecube.digitalplace.payment.capita.service.internal;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder.CardRequestType;
import com.placecube.digitalplace.payment.model.PaymentContext;

@RunWith(PowerMockRunner.class)
public class OneOffPaymentOrderFactoryTest extends PowerMockito {

	@Mock
	private PaymentContext mockPaymentContext;

	@InjectMocks
	private OneOffPaymentOrderFactory oneOffPaymentOrderFactory;

	@Test
	public void createOneOffPaymentOrder_WhenNoErrorAndIsAdvancedConfiguration_ThenCreateOneOffPaymentOrder() {
		String uprn = "uprn";
		String addressLine1 = "addressLine1";
		String addressLine2 = "addressLine2";
		String addressLine3 = "addressLine3";
		String city = "city";
		String postcode = "postcode";
		AddressContext addressContext = new AddressContext(uprn, addressLine1, addressLine2, addressLine3, "", city, postcode);
		String saleDescription = "SaleDescription";
		String emailAddress = "EmailAddress";
		String amountInMinorUnits = "123";
		String salesReference = "SalesReference";
		String internalReference = "internalReference";
		String returnUrl = "ReturnURL";
		String backUrl = "BackURL";
		String firstName = "Firstname";
		String lastName = "Lastname";
		Map<String, String> advancedConfigMap = new HashMap<>();
		advancedConfigMap.put("customConfig1Key", "customConfig1Value");
		advancedConfigMap.put("customConfig2Key", "customConfig2Value");

		when(mockPaymentContext.getAddress()).thenReturn(addressContext);
		when(mockPaymentContext.getEmailAddress()).thenReturn(emailAddress);
		when(mockPaymentContext.getFirstName()).thenReturn(firstName);
		when(mockPaymentContext.getLastName()).thenReturn(lastName);
		when(mockPaymentContext.getReturnURL()).thenReturn(returnUrl);
		when(mockPaymentContext.getBackURL()).thenReturn(backUrl);
		when(mockPaymentContext.getInternalReference()).thenReturn(internalReference);

		when(mockPaymentContext.getAdvancedConfiguration()).thenReturn(advancedConfigMap);

		when(mockPaymentContext.getAdvancedConfigurationValue(CapitaConstants.AMOUNT_IN_MINOR_UNITS)).thenReturn(amountInMinorUnits);
		when(mockPaymentContext.getAdvancedConfigurationValue(CapitaConstants.SALE_DESCRIPTION)).thenReturn(saleDescription);
		when(mockPaymentContext.getAdvancedConfigurationValue(CapitaConstants.SALE_REFERENCE)).thenReturn(salesReference);

		OneOffPaymentOrder oneOffPaymentOrder = oneOffPaymentOrderFactory.createOneOffPaymentOrder(mockPaymentContext);

		assertEquals(addressLine1, oneOffPaymentOrder.getAddress().getAddressLine1());
		assertEquals(addressLine2, oneOffPaymentOrder.getAddress().getAddressLine2());
		assertEquals(addressLine3, oneOffPaymentOrder.getAddress().getAddressLine3());
		assertEquals(city, oneOffPaymentOrder.getAddress().getCity());
		assertEquals(postcode, oneOffPaymentOrder.getAddress().getPostcode());
		assertEquals(emailAddress, oneOffPaymentOrder.getEmailAddress());
		assertEquals(OneOffPaymentOrder.CardRequestType.PAY_ONLY, oneOffPaymentOrder.getRequestType());
		assertEquals(backUrl, oneOffPaymentOrder.getBackURL());
		assertEquals(returnUrl, oneOffPaymentOrder.getReturnURL());
		assertEquals(Integer.parseInt(amountInMinorUnits), oneOffPaymentOrder.getSaleAmount());
		assertEquals(saleDescription, oneOffPaymentOrder.getSaleDescription());
		assertEquals(salesReference, oneOffPaymentOrder.getSaleReferenceCode());
		assertEquals(internalReference, oneOffPaymentOrder.getInternalReference());
		assertEquals(firstName + " " + lastName, oneOffPaymentOrder.getFullName());
	}

	@Test
	public void createOneOffPaymentOrder_WhenNoErrorAndNoAdvancedConfiguration_ThenCreateOneOffPaymentOrder() {
		String uprn = "uprn";
		String addressLine1 = "addressLine1";
		String addressLine2 = "addressLine2";
		String addressLine3 = "addressLine3";
		String city = "city";
		String postcode = "postcode";
		AddressContext addressContext = new AddressContext(uprn, addressLine1, addressLine2, addressLine3, "", city, postcode);
		String saleDescription = "SaleDescription";
		String itemReference = "ItemReference";
		String accountId = "AccountId";
		String emailAddress = "EmailAddress";
		CardRequestType requestType = OneOffPaymentOrder.CardRequestType.PAY_ONLY;
		int amountInMinorUnits = 100;
		String salesReference = "SalesReference";
		String internalReference = "internalReference";
		String returnUrl = "ReturnURL";
		String backUrl = "BackURL";
		String firstName = "Firstname";
		String lastName = "Lastname";

		when(mockPaymentContext.getAddress()).thenReturn(addressContext);
		when(mockPaymentContext.getEmailAddress()).thenReturn(emailAddress);
		when(mockPaymentContext.getFirstName()).thenReturn(firstName);
		when(mockPaymentContext.getLastName()).thenReturn(lastName);
		when(mockPaymentContext.getReturnURL()).thenReturn(returnUrl);
		when(mockPaymentContext.getBackURL()).thenReturn(backUrl);
		when(mockPaymentContext.getInternalReference()).thenReturn(internalReference);
		when(mockPaymentContext.getAmount()).thenReturn(new BigDecimal(1));
		when(mockPaymentContext.getSalesDescription()).thenReturn(saleDescription);
		when(mockPaymentContext.getSalesReference()).thenReturn(salesReference);
		when(mockPaymentContext.getAdvancedConfiguration()).thenReturn(Collections.emptyMap());
		when(mockPaymentContext.getItemReference()).thenReturn(itemReference);
		when(mockPaymentContext.getAccountId()).thenReturn(accountId);

		OneOffPaymentOrder oneOffPaymentOrder = oneOffPaymentOrderFactory.createOneOffPaymentOrder(mockPaymentContext);

		assertEquals(addressLine1, oneOffPaymentOrder.getAddress().getAddressLine1());
		assertEquals(addressLine2, oneOffPaymentOrder.getAddress().getAddressLine2());
		assertEquals(addressLine3, oneOffPaymentOrder.getAddress().getAddressLine3());
		assertEquals(city, oneOffPaymentOrder.getAddress().getCity());
		assertEquals(postcode, oneOffPaymentOrder.getAddress().getPostcode());
		assertEquals(emailAddress, oneOffPaymentOrder.getEmailAddress());
		assertEquals(requestType, oneOffPaymentOrder.getRequestType());
		assertEquals(amountInMinorUnits, oneOffPaymentOrder.getSaleAmount());
		assertEquals(saleDescription, oneOffPaymentOrder.getSaleDescription());
		assertEquals(salesReference, oneOffPaymentOrder.getSaleReferenceCode());
		assertEquals(backUrl, oneOffPaymentOrder.getBackURL());
		assertEquals(returnUrl, oneOffPaymentOrder.getReturnURL());
		assertEquals(firstName + " " + lastName, oneOffPaymentOrder.getFullName());
	}

}
