package com.placecube.digitalplace.ddl.web.portlet.display;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLDisplayPortletRequestConfigurationService;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLEditorService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-ddl-display", // //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=portlet-ddl-display", //
		"com.liferay.portlet.display-category=category.collaboration", //
		"com.liferay.portlet.header-portlet-javascript=/js/ddl-editor-autofill.js", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/ddl-display/view.jsp", //
		"javax.portlet.init-param.config-template=/ddl-display/configuration.jsp", //
		"javax.portlet.name=" + PortletKeys.DDL_DISPLAY_PORTLET, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.supports.mime-type=text/html", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class DDLDisplayPortlet extends MVCPortlet {

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Reference
	private DDLDisplayPortletRequestConfigurationService ddlDisplayPortletRequestConfigurationService;

	@Reference
	private DDLEditorService ddlEditorService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

		try {

			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			DDLDisplayPortletInstanceConfiguration configuration = ddlConfigurationService.getDDLDisplayPortletInstanceConfiguration(themeDisplay);
			Optional<DDMStructure> ddmStructure = ddlEditorService.getDDMStructure(configuration.ddmStructureId());
			long ddlRecordId = ParamUtil.get(renderRequest, "ddlRecordId", 0L);

			if (ddlRecordId > 0) {

				ddlDisplayPortletRequestConfigurationService.setRenderRequestAttributesUsingDDLRecord(renderRequest, themeDisplay, ddlRecordId);

			} else if (ddmStructure.isPresent()) {

				ddlDisplayPortletRequestConfigurationService.setRenderRequestAttributesUsingConfiguration(renderRequest, themeDisplay, configuration, ddmStructure.get());

			} else {
				renderRequest.setAttribute("invalidConfiguration", true);
				renderRequest.setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
			}

			super.render(renderRequest, renderResponse);

		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}