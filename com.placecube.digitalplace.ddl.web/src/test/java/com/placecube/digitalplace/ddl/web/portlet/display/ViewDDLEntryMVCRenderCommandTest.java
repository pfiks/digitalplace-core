package com.placecube.digitalplace.ddl.web.portlet.display;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.servlet.PortletServlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.service.DynamicDataListTemplateContentService;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLEditorService;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLPermissionService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class ViewDDLEntryMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private ViewDDLEntryMVCRenderCommand viewDDLEntryMVCRenderCommand;

	@Mock
	private DDLEditorService mockDDLEditorService;

	@Mock
	private DDLPermissionService mockDDLPermissionService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DynamicDataListTemplateContentService mockDDLTemplateContentService;

	@Mock
	private HttpServletRequest mockHTTPServletRequest;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenNoError_ThenReturnsViewEntryJSPAndSetsRenderRequestAttributes() throws PortletException, PortalException {
		long ddmStructureId = 1;
		long ddlRecordId = 2;
		long ddlTemplateId = 3;
		String ddlContentForTemplate = "ddlContentForTemplate";
		String backURL = "backURL";

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockRenderRequest.getAttribute(PortletServlet.PORTLET_SERVLET_REQUEST)).thenReturn(mockHTTPServletRequest);
		when(ParamUtil.getLong(mockRenderRequest, "ddmStructureId")).thenReturn(ddmStructureId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordId")).thenReturn(ddlRecordId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlTemplateId")).thenReturn(ddlTemplateId);
		when(mockDDLEditorService.getDDLRecord(ddlRecordId)).thenReturn(Optional.of(mockDDLRecord));
		when(mockDDLTemplateContentService.getTransformedTemplate(mockDDLRecord, ddlTemplateId, mockThemeDisplay, mockHTTPServletRequest)).thenReturn(ddlContentForTemplate);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockDDLPermissionService.hasUpdatePermission(mockPermissionChecker, mockDDLRecord)).thenReturn(true);
		when(mockDDLPermissionService.hasDeletePermission(mockPermissionChecker, mockDDLRecord)).thenReturn(true);
		when(mockDDLEditorService.getBackURL(mockRenderRequest, mockThemeDisplay)).thenReturn(backURL);

		String result = viewDDLEntryMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/ddl-display/view-entry.jsp"));
		verify(mockRenderRequest, times(1)).setAttribute("hasUpdatePermission", true);
		verify(mockRenderRequest, times(1)).setAttribute("hasDeletePermission", true);
		verify(mockRenderRequest, times(1)).setAttribute("ddmStructureId", ddmStructureId);
		verify(mockRenderRequest, times(1)).setAttribute("ddlRecordId", ddlRecordId);
		verify(mockRenderRequest, times(1)).setAttribute("ddlTemplateId", ddlTemplateId);
		verify(mockRenderRequest, times(1)).setAttribute("ddlContentForTemplate", ddlContentForTemplate);
		verify(mockDDLEditorService, times(1)).configureBackLinkURL(mockThemeDisplay, backURL, mockRenderRequest);

	}

	@Test(expected = PortletException.class)
	public void render_WhenError_ThenThrowsPortletException() throws PortletException, PortalException {
		long ddmStructureId = 1;
		long ddlRecordId = 2;
		long ddlTemplateId = 3;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockRenderRequest.getAttribute(PortletServlet.PORTLET_SERVLET_REQUEST)).thenReturn(mockHTTPServletRequest);
		when(ParamUtil.getLong(mockRenderRequest, "ddmStructureId")).thenReturn(ddmStructureId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordId")).thenReturn(ddlRecordId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlTemplateId")).thenReturn(ddlTemplateId);
		when(mockDDLEditorService.getDDLRecord(ddlRecordId)).thenReturn(Optional.of(mockDDLRecord));
		when(mockDDLTemplateContentService.getTransformedTemplate(mockDDLRecord, ddlTemplateId, mockThemeDisplay, mockHTTPServletRequest)).thenThrow(new PortalException(""));

		viewDDLEntryMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

}
