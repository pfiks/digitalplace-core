/**
 * Status.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class Status implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Status(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _SUCCESS = "SUCCESS";
    public static final java.lang.String _INVALID_REQUEST = "INVALID_REQUEST";
    public static final java.lang.String _CARD_DETAILS_REJECTED = "CARD_DETAILS_REJECTED";
    public static final java.lang.String _CANCELLED = "CANCELLED";
    public static final java.lang.String _LOGGED_OUT = "LOGGED_OUT";
    public static final java.lang.String _NOT_ATTEMPTED = "NOT_ATTEMPTED";
    public static final java.lang.String _ERROR = "ERROR";
    public static final Status SUCCESS = new Status(_SUCCESS);
    public static final Status INVALID_REQUEST = new Status(_INVALID_REQUEST);
    public static final Status CARD_DETAILS_REJECTED = new Status(_CARD_DETAILS_REJECTED);
    public static final Status CANCELLED = new Status(_CANCELLED);
    public static final Status LOGGED_OUT = new Status(_LOGGED_OUT);
    public static final Status NOT_ATTEMPTED = new Status(_NOT_ATTEMPTED);
    public static final Status ERROR = new Status(_ERROR);
    public java.lang.String getValue() { return _value_;}
    public static Status fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Status enumeration = (Status)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Status fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Status.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "status"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
