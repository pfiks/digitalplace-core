package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.contributor;

import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.constants.SharedSearchSessionKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.configuration.DDLFieldFilterPortletConfigurationConstants;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.constants.JsonFilterConfigKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model.SearchFilter;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.service.DDLFieldFilterFactoryService;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.service.DDLFieldFilterService;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.DDL_FIELD_FILTER_PORTLET, service = SharedSearchContributor.class)
public class DDLFieldFilterPortletSharedSearchContributor implements SharedSearchContributor {

	private static final Log LOG = LogFactoryUtil.getLog(DDLFieldFilterPortletSharedSearchContributor.class);

	@Reference
	private DDLFieldFilterService ddlFieldFilterService;

	@Reference
	private DDLFieldFilterFactoryService ddlFilterFactoryService;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {

		Optional<PortletPreferences> portletPreferencesOpt = sharedSearchContributorSettings.getPortletPreferences();

		if (!portletPreferencesOpt.isPresent()) {
			throw new SystemException("Invalid portlet configuration");
		}

		PortletPreferences portletPreferences = portletPreferencesOpt.get();

		final String internalPortletId = portletPreferences.getValue(DDLFieldFilterPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK);
		Optional<String> appliedFilterOpt = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SharedSearchSessionKeys.APPLIED_FILTER + "_" + internalPortletId);

		try {
			applyFilterGroups(appliedFilterOpt, portletPreferences, sharedSearchContributorSettings);

		} catch (Exception e) {
			LOG.error("Error contributing field filter portlet settings.", e);
		}

	}

	private void applyFilterGroups(Optional<String> appliedFilterOpt, PortletPreferences portletPreferences, SharedSearchContributorSettings sharedSearchContributorSettings) throws Exception {
		String groupsConfig = portletPreferences.getValue(DDLFieldFilterPortletConfigurationConstants.JSON_FILTER_CONFIG, "[]");
		long ddmStructureId = GetterUtil.getLong(portletPreferences.getValue(DDLFieldFilterPortletConfigurationConstants.STRUCTURE_ID, "0"));

		JSONArray groupsConfigJson = jsonFactory.createJSONArray(groupsConfig);

		for (int i = 0; i < groupsConfigJson.length(); i++) {
			JSONObject filterGroup = groupsConfigJson.getJSONObject(i);

			String currentFilterGroupName = filterGroup.getString(JsonFilterConfigKeys.FILTER_GROUP_NAME);

			Optional<String> selectedFilter = ddlFieldFilterService.getSelectedFilterByGroupName(currentFilterGroupName, appliedFilterOpt);

			if (selectedFilter.isPresent()) {
				String[] groupAndOptionName = selectedFilter.get().split(StringPool.UNDERLINE);

				if (groupAndOptionName.length == 2) {
					applyOptionFilter(groupsConfigJson, currentFilterGroupName, groupAndOptionName[1], ddmStructureId, sharedSearchContributorSettings);
				} else {
					throw new Exception("Invalid selected filter format : " + selectedFilter.get());
				}
			} else {
				String filterPreselectedOption = filterGroup.getString(JsonFilterConfigKeys.FILTER_PRESELECTED_OPTION_NAME);

				if (Validator.isNotNull(filterPreselectedOption)) {
					applyOptionFilter(groupsConfigJson, currentFilterGroupName, filterPreselectedOption, ddmStructureId, sharedSearchContributorSettings);
				}
			}

		}
	}

	private void applyOptionFilter(JSONArray groupsConfigJson, String groupName, String optionName, long ddmStructureId, SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<JSONObject> optionOpt = ddlFieldFilterService.getOptionJsonFromGroupAndOptionName(groupsConfigJson, groupName, optionName);
		if (optionOpt.isPresent()) {
			ThemeDisplay themeDisplay = sharedSearchContributorSettings.getThemeDisplay();

			SearchFilter rangeFilter = ddlFilterFactoryService.getRangeFilterForOption(optionOpt.get(), ddmStructureId, themeDisplay.getLocale());
			sharedSearchContributorSettings.addBooleanClause(rangeFilter.getFilterBooleanClause());
		}

	}

}
