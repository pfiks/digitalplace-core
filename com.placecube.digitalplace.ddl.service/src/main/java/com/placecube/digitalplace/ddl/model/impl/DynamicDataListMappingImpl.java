package com.placecube.digitalplace.ddl.model.impl;

import java.util.Optional;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.placecube.digitalplace.ddl.internal.util.DDMTemplateUtil;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;

public class DynamicDataListMappingImpl implements DynamicDataListMapping {

	private final DDMStructure ddmStructure;
	private final long fullViewDefaultTemplateId;
	private final String iconName;
	private final String indexerContentFieldName;
	private final String indexerTitleFieldName;
	private final String mappingName;
	private final long searchDefaultGeoPointViewTemplateId;
	private final long searchDefaultGridViewTemplateId;
	private final long searchDefaultListViewTemplateId;

	DynamicDataListMappingImpl(String mappingName, DDMStructure ddmStructure, long fullViewDefaultTemplateId, long searchDefaultGeoPointViewTemplateId, long searchDefaultListViewTemplateId,
			long searchDefaultGridViewTemplateId, String indexerTitleFieldName, String indexerContentFieldName, String iconName) {
		this.mappingName = mappingName;
		this.ddmStructure = ddmStructure;
		this.fullViewDefaultTemplateId = fullViewDefaultTemplateId;
		this.searchDefaultGeoPointViewTemplateId = searchDefaultGeoPointViewTemplateId;
		this.searchDefaultListViewTemplateId = searchDefaultListViewTemplateId;
		this.searchDefaultGridViewTemplateId = searchDefaultGridViewTemplateId;
		this.iconName = iconName;
		this.indexerTitleFieldName = indexerTitleFieldName;
		this.indexerContentFieldName = indexerContentFieldName;
	}

	@Override
	public DDMStructure getDDMStructure() {
		return ddmStructure;
	}

	@Override
	public long getDDMStructureId() {
		return ddmStructure.getStructureId();
	}

	@Override
	public Optional<DDMTemplate> getFullViewDefaultTemplate() {
		return DDMTemplateUtil.getTemplateById(ddmStructure, fullViewDefaultTemplateId);
	}

	@Override
	public long getFullViewDefaultTemplateId() {
		return fullViewDefaultTemplateId;
	}

	@Override
	public String getIconName() {
		return iconName;
	}

	@Override
	public String getIndexerContentFieldName() {
		return indexerContentFieldName;
	}

	@Override
	public String getIndexerTitleFieldName() {
		return indexerTitleFieldName;
	}

	@Override
	public String getMappingName() {
		return mappingName;
	}

	@Override
	public Optional<DDMTemplate> getSearchDefaultGeopointViewTemplate() {
		return DDMTemplateUtil.getTemplateById(ddmStructure, searchDefaultGeoPointViewTemplateId);
	}

	@Override
	public long getSearchDefaultGeopointViewTemplateId() {
		return searchDefaultGeoPointViewTemplateId;
	}

	@Override
	public Optional<DDMTemplate> getSearchDefaultGridViewTemplate() {
		return DDMTemplateUtil.getTemplateById(ddmStructure, searchDefaultGridViewTemplateId);
	}

	@Override
	public long getSearchDefaultGridViewTemplateId() {
		return searchDefaultGridViewTemplateId;
	}

	@Override
	public Optional<DDMTemplate> getSearchDefaultListViewTemplate() {
		return DDMTemplateUtil.getTemplateById(ddmStructure, searchDefaultListViewTemplateId);
	}

	@Override
	public long getSearchDefaultListViewTemplateId() {
		return searchDefaultListViewTemplateId;
	}

}
