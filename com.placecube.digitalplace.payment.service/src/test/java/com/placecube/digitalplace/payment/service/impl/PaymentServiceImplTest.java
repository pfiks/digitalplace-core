package com.placecube.digitalplace.payment.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;
import com.placecube.digitalplace.payment.service.PaymentConnector;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PaymentServiceImpl.class, PaymentResponse.class })
public class PaymentServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String PAYMENT_REFERENCE = "PAYMENT_REFERENCE";
	private static final String EXTERNAL_REFERENCE = "EXTERNAL_REFERENCE";

	@Mock
	private AgentReferredPaymentContext mockAgentReferredPaymentContext;

	@Mock
	private PaymentConnector mockPaymentConnector1;

	@Mock
	private PaymentConnector mockPaymentConnector2;

	@Mock
	private PaymentContext mockPaymentContext;

	@Mock
	private PaymentResponse mockPaymentResponse;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private PaymentServiceImpl paymentServiceImpl;

	@Test
	public void getBackURL_WhenThereAreNoConnectors_ThenReturnEmptyOptional() {

		Optional<String> actual = paymentServiceImpl.getBackURL(PAYMENT_REFERENCE, mockServiceContext);

		assertEquals(Optional.empty(), actual);
	}

	@Test
	public void getBackURL_WhenTwoDisabledConnectors_ThenReturnEmptyOptional() {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		Optional<String> actual = paymentServiceImpl.getBackURL(PAYMENT_REFERENCE, mockServiceContext);
		assertEquals(Optional.empty(), actual);

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test
	public void getBackURL_WhenTwoEnabledConnectors_ThenUseFirstConnectorAndReturnOptonalWithBackURL() {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		String backURL = "backURL";

		when(mockPaymentConnector1.getBackURL(PAYMENT_REFERENCE, mockServiceContext)).thenReturn(Optional.of(backURL));

		Optional<String> actual = paymentServiceImpl.getBackURL(PAYMENT_REFERENCE, mockServiceContext);

		assertEquals(backURL, actual.get());
		Mockito.verify(mockPaymentConnector1, Mockito.times(1)).getBackURL(PAYMENT_REFERENCE, mockServiceContext);

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test
	public void getDefaultAdvancedConfiguration_WhenThereAreMultipleEnabledConnectors_ThenUseFirstConnectorAndReturnOptionalWithDefaultAdvancedConfiguration() {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockPaymentConnector1.getDefaultAdvancedConfiguration(COMPANY_ID)).thenReturn(Optional.of("defaultConfig1"));

		Optional<String> result = paymentServiceImpl.getDefaultAdvancedConfiguration(COMPANY_ID);

		assertThat(result.get(), equalTo("defaultConfig1"));

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test
	public void getDefaultAdvancedConfiguration_WhenThereAreNoConnectors_ThenReturnEmptyOptional() {
		Optional<String> result = paymentServiceImpl.getDefaultAdvancedConfiguration(COMPANY_ID);

		assertFalse(result.isPresent());
	}

	@Test
	public void getDefaultAdvancedConfiguration_WhenThereAreNoEnabledConnectors_ThenReturnsEmptyOptional() {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		Optional<String> result = paymentServiceImpl.getDefaultAdvancedConfiguration(COMPANY_ID);

		assertFalse(result.isPresent());

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test
	public void getPaymentStatus_WhenThereAreNoConnectors_ThenReturnNoPaymentConnectorAvailablePaymentSatuss() {
		when(mockPaymentConnector1.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext)).thenReturn(PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE"));

		PaymentStatus expected = PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE");
		PaymentStatus actual = paymentServiceImpl.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		assertEquals(expected, actual);
	}

	@Test
	public void getPaymentStatus_WhenTwoDisabledConnectors_ThenReturnNoPaymentConnectorAvailablePaymentStatus() {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		when(mockPaymentConnector1.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext)).thenReturn(PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE"));

		PaymentStatus expected = PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE");
		PaymentStatus actual = paymentServiceImpl.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		assertEquals(expected, actual);

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test
	public void getPaymentStatus_WhenTwoEnabledConnectors_ThenUseFirstConnectorAndReturnPaymentStatus() {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockPaymentConnector1.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext)).thenReturn(PaymentStatus.success());

		paymentServiceImpl.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		Mockito.verify(mockPaymentConnector1, Mockito.times(1)).getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test
	public void preparePayment_WhenThereAreNoConnectors_ThenReturnNoPaymentConnectorAvailablePaymentResponse() throws Exception {
		whenNew(PaymentResponse.class).withArguments(StringPool.BLANK, PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE")).thenReturn(mockPaymentResponse);

		paymentServiceImpl.preparePayment(mockPaymentContext, mockServiceContext);

		verifyNew(PaymentResponse.class).withArguments(StringPool.BLANK, PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE"));

	}

	@Test
	public void preparePayment_WhenTwoDisabledConnectors_ThenReturnNoPaymentConnectorAvailablePaymentResponse() throws Exception {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		whenNew(PaymentResponse.class).withArguments(StringPool.BLANK, PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE")).thenReturn(mockPaymentResponse);

		paymentServiceImpl.preparePayment(mockPaymentContext, mockServiceContext);

		verifyNew(PaymentResponse.class).withArguments(StringPool.BLANK, PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE"));

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test
	public void preparePayment_WhenTwoEnabledConnectors_ThenUseFirstConnectorAndPreparePayment() {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockPaymentConnector1.preparePayment(mockPaymentContext, mockServiceContext)).thenReturn(mockPaymentResponse);

		PaymentResponse expected = mockPaymentResponse;
		PaymentResponse actual = paymentServiceImpl.preparePayment(mockPaymentContext, mockServiceContext);

		assertEquals(expected, actual);

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test
	public void getAgentReferredPaymentPayload_WhenTwoEnabledConnectors_ThenUseFirstConnectorAndPreparePayment() throws Exception {
		final String response = "response";
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockPaymentConnector1.getAgentReferredPaymentPayload(COMPANY_ID, mockAgentReferredPaymentContext)).thenReturn(response);

		String result = paymentServiceImpl.getAgentReferredPaymentPayload(COMPANY_ID, mockAgentReferredPaymentContext);

		assertThat(result, equalTo(response));

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test(expected = PortalException.class)
	public void getAgentReferredPaymentPayload_WhenThereIsEnabledConnectorAndARPPayloadGenerationFails_ThenUseFirstConnectorAndPreparePayment() throws Exception {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockPaymentConnector1.getAgentReferredPaymentPayload(COMPANY_ID, mockAgentReferredPaymentContext)).thenThrow(new PortalException());

		paymentServiceImpl.getAgentReferredPaymentPayload(COMPANY_ID, mockAgentReferredPaymentContext);

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
	}

	@Test
	public void getAgentReferredPaymentPayload_WhenTwoDisabledConnectors_ThenReturnsEmptyString() throws Exception {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		String result = paymentServiceImpl.getAgentReferredPaymentPayload(COMPANY_ID, mockAgentReferredPaymentContext);

		assertThat(result, equalTo(StringPool.BLANK));

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);

	}

	@Test
	public void processAgentReferredPaymentResponse_WhenTwoEnabledConnectors_ThenUseFirstConnectorAndProcessesARP() {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(true);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(true);

		when(mockPaymentConnector1.processAgentReferredPaymentResponse(EXTERNAL_REFERENCE, mockServiceContext)).thenReturn(mockPaymentResponse);

		PaymentResponse expected = mockPaymentResponse;
		PaymentResponse actual = paymentServiceImpl.processAgentReferredPaymentResponse(EXTERNAL_REFERENCE, mockServiceContext);

		assertEquals(expected, actual);

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}

	@Test
	public void processAgentReferredPaymentResponse_WhenTwoDisabledConnectors_ThenReturnNoPaymentConnectorAvailablePaymentResponse() throws Exception {
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.setPaymentConnector(mockPaymentConnector2);

		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockPaymentConnector1.enabled(COMPANY_ID)).thenReturn(false);
		when(mockPaymentConnector2.enabled(COMPANY_ID)).thenReturn(false);

		whenNew(PaymentResponse.class).withArguments(StringPool.BLANK, PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE")).thenReturn(mockPaymentResponse);

		paymentServiceImpl.processAgentReferredPaymentResponse(EXTERNAL_REFERENCE, mockServiceContext);

		verifyNew(PaymentResponse.class).withArguments(StringPool.BLANK, PaymentStatus.fromValue("NO_PAYMENT_CONNECTOR_AVAILABLE"));

		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector1);
		paymentServiceImpl.unsetPaymentConnector(mockPaymentConnector2);
	}
}
