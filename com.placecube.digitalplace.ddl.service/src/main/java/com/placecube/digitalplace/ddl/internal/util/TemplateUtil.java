package com.placecube.digitalplace.ddl.internal.util;

import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.template.StringTemplateResource;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateResource;
import com.liferay.portal.kernel.util.StringBundler;

@Component(immediate = true, service = TemplateUtil.class)
public class TemplateUtil {

	public Map<String, Object> getContextObjectsInstance() {
		return new HashMap<>();
	}

	public String getTemplateId(long templateId, long companyId, long companyGroupId, long groupId) {
		StringBundler sb = new StringBundler(5);
		sb.append(companyId);
		sb.append(StringPool.POUND);
		if (companyGroupId > 0) {
			sb.append(companyGroupId);
		} else {
			sb.append(groupId);
		}
		sb.append(StringPool.POUND);
		sb.append(templateId);
		return sb.toString();
	}

	public TemplateResource getTemplateResourceInstance(String templateId, String script) {
		return new StringTemplateResource(templateId, script);
	}

	public String getTemplatesPath(long companyId, long groupId, long classNameId) {
		StringBundler sb = new StringBundler(7);
		sb.append(TemplateConstants.TEMPLATE_SEPARATOR);
		sb.append(StringPool.SLASH);
		sb.append(companyId);
		sb.append(StringPool.SLASH);
		sb.append(groupId);
		sb.append(StringPool.SLASH);
		sb.append(classNameId);
		return sb.toString();
	}

	public UnsyncStringWriter getUnsyncStringWriterInstance() {
		return new UnsyncStringWriter();
	}

}
