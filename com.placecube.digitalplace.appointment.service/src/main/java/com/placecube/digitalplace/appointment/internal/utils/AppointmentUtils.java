package com.placecube.digitalplace.appointment.internal.utils;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.TimeZone;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.TimeZoneUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.model.AppointmentSlot;

@Component(immediate = true, service = AppointmentUtils.class)
public class AppointmentUtils {

	public boolean doesAppointmentMatch(String serviceId, String connectorClassName, String appointmentId, AppointmentEntry appointmentEntry) {
		return appointmentId.equals(appointmentEntry.getAppointmentId()) && validateServiceIdAndConnector(appointmentEntry, serviceId, connectorClassName);
	}

	public Optional<LocalDateTime> getCalculationEndDate(LocalDateTime configEnd, LocalDateTime filterStart, LocalDateTime filterEnd, int hour, int minute) {
		if (configEnd.isBefore(filterStart)) {
			return Optional.empty();
		}
		LocalDateTime result = isBeforeOrEqualTo(filterEnd, configEnd) ? filterEnd : configEnd;
		return Optional.of(result.withHour(hour).withMinute(minute).withSecond(0));
	}

	public Optional<LocalDateTime> getCalculationStartDate(LocalDateTime configStart, LocalDateTime filterStart, LocalDateTime maxDate, int hour, int minute) {
		if (filterStart.isAfter(maxDate)) {
			return Optional.empty();
		}

		if (filterStart.isBefore(configStart)) {
			return Optional.of(configStart.withHour(hour).withMinute(minute).withSecond(0));
		}

		return Optional.of(filterStart.withHour(hour).withMinute(minute).withSecond(0));
	}

	public LocalDateTime getLocalDateTime(Date date, int hour, int minute) {
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().withHour(hour).withMinute(minute).withSecond(0);
	}

	public LocalDateTime getLocalDateTime(Date date, LocalDateTime fallback) {
		return Validator.isNotNull(date) ? date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime() : fallback;
	}

	public Locale getLocale(ThemeDisplay themeDisplay) {
		return themeDisplay != null ? themeDisplay.getLocale() : LocaleUtil.getDefault();
	}

	public LocalDateTime getNextDateSlotStart(LocalDateTime calculationDateStart, AppointmentSlot appointmentSlot) {
		return calculationDateStart.plusDays(1).withHour(appointmentSlot.getSlotStartTimeHour()).withMinute(appointmentSlot.getSlotStartTimeMinute());
	}

	public Optional<Date> getParsedDate(String dateValue, Locale locale, String datePattern) throws ParseException {
		if (Validator.isNotNull(dateValue)) {
			if (Validator.isNotNull(datePattern)) {
				return Optional.ofNullable(DateUtil.parseDate(datePattern, dateValue, locale));
			}
			return Optional.of(new Date(GetterUtil.getLong(dateValue, 0l)));
		}
		return Optional.empty();
	}

	public LocalDateTime getSlotEnd(LocalDateTime calculationDateStart, int appointmentDuration, LocalDateTime calculationMaxDate) {
		if (appointmentDuration > 0) {
			return calculationDateStart.plusMinutes(appointmentDuration);
		}
		return calculationDateStart.withHour(calculationMaxDate.getHour()).withMinute(calculationMaxDate.getMinute());
	}

	public TimeZone getTimeZone(ThemeDisplay themeDisplay) {
		return themeDisplay != null ? themeDisplay.getTimeZone() : TimeZoneUtil.getDefault();
	}

	private boolean isBeforeOrEqualTo(LocalDateTime dateToCheck, LocalDateTime dateToCompareItTo) {
		return dateToCheck.isEqual(dateToCompareItTo) || dateToCheck.isBefore(dateToCompareItTo);
	}

	private boolean validateServiceIdAndConnector(AppointmentEntry appointmentEntry, String serviceId, String connectorClassName) {
		return Objects.equals(serviceId, appointmentEntry.getServiceId()) && Objects.equals(connectorClassName, appointmentEntry.getConnectorClassName());
	}
}
