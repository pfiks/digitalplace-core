package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dispatch.model.DispatchTrigger;
import com.liferay.dispatch.service.DispatchTriggerLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceContextThreadLocal.class })
public class SiteInitializerUtilsTest extends PowerMockito {

	@Mock
	private DispatchTrigger mockDispatchTrigger;

	@Mock
	private DispatchTriggerLocalService mockDispatchTriggerLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObjectSettings;

	@Mock
	private Layout mockLayout1;

	@Mock
	private Layout mockLayout2;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServiceContext mockServiceContextOriginal;

	@Mock
	private SiteInitialiserSetupContentService mockSiteInitialiserSetupContentService;

	@Mock
	private TimeZone mockTimeZone;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Mock
	private User mockUser;

	@Captor
	private ArgumentCaptor<UnicodeProperties> settingsCaptor;

	@InjectMocks
	private SiteInitializerUtils siteInitializerUtils;

	@Before
	public void activateSetup() {
		mockStatic(ServiceContextThreadLocal.class);
	}

	@Test
	public void configureAdditionalData_WithGroupIdAndJsonObjectParameters_WhenNoError_ThenConfiguresAssetListEntryUsage() throws PortalException {
		long groupId = 123;
		long userId = 2;
		when(mockJSONObjectSettings.getJSONArray("assetListEntryUsagesReplacements")).thenReturn(mockJSONArray);

		siteInitializerUtils.configureAdditionalData(userId, groupId, mockJSONObjectSettings);

		verify(mockSiteInitialiserSetupContentService, times(1)).configureAssetListEntryUsage(userId, groupId, mockJSONArray);
	}

	@Test
	public void configureAdditionalData_WithGroupIdAndUrlParameters_WhenFileSpecified_ThenConfiguresAssetListEntryUsage() throws PortalException, IOException {
		long groupId = 123;
		long userId = 2;
		Optional<URL> urlData = Optional.of(Thread.currentThread().getContextClassLoader().getResource("test-additional-post-setup-data.json"));
		when(mockGroupLocalService.getGroup(groupId)).thenReturn(mockGroup);
		when(mockSiteInitialiserSetupContentService.getFileContentWithDefaultReplacements(urlData, mockGroup)).thenReturn("myFileContent");
		when(mockJSONFactory.createJSONObject("myFileContent")).thenReturn(mockJSONObjectSettings);
		when(mockJSONObjectSettings.getJSONArray("assetListEntryUsagesReplacements")).thenReturn(mockJSONArray);

		siteInitializerUtils.configureAdditionalData(userId, groupId, urlData);

		verify(mockSiteInitialiserSetupContentService, times(1)).configureAssetListEntryUsage(userId, groupId, mockJSONArray);
	}

	@Test
	public void configureAdditionalData_WithGroupIdAndUrlParameters_WhenNoFileSpecified_ThenNoActionsArePerformed() throws PortalException, IOException {
		long groupId = 123;
		long userId = 2;
		Optional<URL> urlData = Optional.empty();

		siteInitializerUtils.configureAdditionalData(userId, groupId, urlData);

		verifyNoInteractions(mockGroupLocalService);
	}

	@Test
	public void createMissingDispatchTrigger_WhenTriggerAlreadyExists_ThenDoesNotCreateANewone() throws PortalException {
		long userId = 123;
		long companyId = 566;
		String refCode = "refCode123";
		String executorType = "execType123";
		String name = "name123";
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockServiceContext.getCompanyId()).thenReturn(companyId);
		when(mockDispatchTriggerLocalService.fetchDispatchTriggerByExternalReferenceCode(refCode, companyId)).thenReturn(mockDispatchTrigger);

		siteInitializerUtils.createMissingDispatchTrigger(refCode, executorType, name, null, mockServiceContext);

		verify(mockDispatchTriggerLocalService, never()).addDispatchTrigger(anyString(), anyLong(), anyString(), any(), anyString(), anyBoolean());
	}

	@Test
	public void createMissingDispatchTrigger_WhenTriggerDoesNotYetExists_ThenCreatesTheJobTrigger() throws PortalException {
		long userId = 123;
		long companyId = 566;
		String refCode = "refCode123";
		String executorType = "execType123";
		String name = "name123";
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockJSONObjectSettings.toMap()).thenReturn(Map.of("setting1", "value1"));
		when(mockServiceContext.getCompanyId()).thenReturn(companyId);
		when(mockDispatchTriggerLocalService.fetchDispatchTriggerByExternalReferenceCode(refCode, companyId)).thenReturn(null);

		siteInitializerUtils.createMissingDispatchTrigger(refCode, executorType, name, mockJSONObjectSettings, mockServiceContext);

		verify(mockDispatchTriggerLocalService, times(1)).addDispatchTrigger(eq(refCode), eq(userId), eq(executorType), settingsCaptor.capture(), eq(name), eq(false));
		UnicodeProperties settingsParameter = settingsCaptor.getValue();
		assertThat(settingsParameter.get("setting1"), equalTo("value1"));
	}

	@Test(expected = PortalException.class)
	public void createMissingDispatchTrigger_WhenTriggerDoesNotYetExistsAndExceptionCreatingTheTrigger_ThenThrowsPortalException() throws PortalException {
		long userId = 123;
		long companyId = 566;
		String refCode = "refCode123";
		String executorType = "execType123";
		String name = "name123";
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockServiceContext.getCompanyId()).thenReturn(companyId);
		when(mockDispatchTriggerLocalService.fetchDispatchTriggerByExternalReferenceCode(refCode, companyId)).thenReturn(null);
		when(mockDispatchTriggerLocalService.addDispatchTrigger(refCode, userId, executorType, null, name, false)).thenThrow(new PortalException());

		siteInitializerUtils.createMissingDispatchTrigger(refCode, executorType, name, null, mockServiceContext);
	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnsTheConfiguredServiceContext() throws PortalException {
		long groupId = 123l;
		long companyId = 456l;
		long userId = 789l;
		String languageId = "langId";
		when(mockGroupLocalService.getGroup(groupId)).thenReturn(mockGroup);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContextOriginal);
		when(mockServiceContextOriginal.clone()).thenReturn(mockServiceContext);
		when(mockGroup.getCompanyId()).thenReturn(companyId);
		when(mockGroup.getDefaultLanguageId()).thenReturn(languageId);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getTimeZone()).thenReturn(mockTimeZone);

		ServiceContext result = siteInitializerUtils.getServiceContext(mockUser, groupId);

		assertThat(result, sameInstance(mockServiceContext));
		verify(mockServiceContext, times(1)).setAddGroupPermissions(true);
		verify(mockServiceContext, times(1)).setAddGuestPermissions(true);
		verify(mockServiceContext, times(1)).setCompanyId(companyId);
		verify(mockServiceContext, times(1)).setLanguageId(languageId);
		verify(mockServiceContext, times(1)).setScopeGroupId(groupId);
		verify(mockServiceContext, times(1)).setTimeZone(mockTimeZone);
		verify(mockServiceContext, times(1)).setUserId(userId);
	}

	@Test
	public void publishDraftLayout_WhenLayoutIsNull_ThenNoActionIsPerformed() throws PortalException {
		siteInitializerUtils.publishLayout(null);

		verifyZeroInteractions(mockLayoutLocalService);
	}

	@Test
	public void publishDraftLayout_WhenNoError_ThenUpdatesTheLayoutStatusAndTypeSettings() throws PortalException {
		long plid = 123;
		when(mockLayout2.getPlid()).thenReturn(plid);
		when(mockLayoutLocalService.getLayout(plid)).thenReturn(mockLayout1);
		when(mockLayout1.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);

		siteInitializerUtils.publishLayout(mockLayout2);

		InOrder inOrder = inOrder(mockLayout1, mockUnicodeProperties, mockLayoutLocalService);
		inOrder.verify(mockLayout1, times(1)).setStatus(WorkflowConstants.STATUS_APPROVED);
		inOrder.verify(mockUnicodeProperties, times(1)).put("published", Boolean.TRUE.toString());
		inOrder.verify(mockLayout1, times(1)).setTypeSettingsProperties(mockUnicodeProperties);
		inOrder.verify(mockLayoutLocalService, times(1)).updateLayout(mockLayout1);
	}

}
