package com.placecube.digitalplace.announcementsbanner.web.portlet;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.announcementsbanner.web.configuration.AnnouncementsBannerGroupConfiguration;
import com.placecube.digitalplace.announcementsbanner.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.announcementsbanner.web.constants.PortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.ANNOUNCEMENTS_BANNER, "mvc.command.name=" + MVCCommandKeys.GET_ANNOUNCEMENT_CONFIGURATION }, service = MVCResourceCommand.class)
public class GetAnnouncementConfigurationMVCResourceCommand extends BaseMVCResourceCommand {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {
		AnnouncementsBannerGroupConfiguration announcementsBannerGroupConfiguration = configurationProvider.getGroupConfiguration(AnnouncementsBannerGroupConfiguration.class, portal.getScopeGroupId(resourceRequest));
		String announcementsBannerPortletHTMLSelector = announcementsBannerGroupConfiguration.getAnnouncementsBannerPortletHTMLSelector();

		JSONObject jsonObject = jsonFactory.createJSONObject();
		jsonObject.put("announcementsBannerPortletHTMLSelector", announcementsBannerPortletHTMLSelector);
		resourceResponse.getWriter().print(jsonObject);
	}

}
