package com.placecube.digitalplace.search.sortby.mostliked.triggers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.ratings.kernel.model.RatingsStats;
import com.placecube.digitalplace.search.sortby.mostliked.service.MostLikedService;

public class RatingsStatsListenerTest extends PowerMockito {

	@Mock
	private MostLikedService mockMostLikedService;

	@Mock
	private RatingsStats mockRatingsStats;

	@Mock
	private RatingsStats mockRatingsStatsOld;

	@InjectMocks
	private RatingsStatsListener ratingsStatsListener;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void onAfterCreate_WhenNoError_ThenReindexTheScore() {
		String className = "classNameVal";
		long classPK = 12;
		when(mockRatingsStats.getClassName()).thenReturn(className);
		when(mockRatingsStats.getClassPK()).thenReturn(classPK);

		ratingsStatsListener.onAfterCreate(mockRatingsStats);

		verify(mockMostLikedService, times(1)).reindexRatingScore(className, classPK);
	}

	@Test
	public void onAfterRemove_WhenNoError_ThenReindexTheScore() {
		String className = "classNameVal";
		long classPK = 12;
		when(mockRatingsStats.getClassName()).thenReturn(className);
		when(mockRatingsStats.getClassPK()).thenReturn(classPK);

		ratingsStatsListener.onAfterRemove(mockRatingsStats);

		verify(mockMostLikedService, times(1)).reindexRatingScore(className, classPK);
	}

	@Test
	public void onAfterUpdate_WhenNoError_ThenReindexTheScore() {
		String className = "classNameVal";
		long classPK = 12;
		when(mockRatingsStats.getClassName()).thenReturn(className);
		when(mockRatingsStats.getClassPK()).thenReturn(classPK);

		ratingsStatsListener.onAfterUpdate(mockRatingsStatsOld, mockRatingsStats);

		verify(mockMostLikedService, times(1)).reindexRatingScore(className, classPK);
	}

}
