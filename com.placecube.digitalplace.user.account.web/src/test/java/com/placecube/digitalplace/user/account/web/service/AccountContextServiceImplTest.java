package com.placecube.digitalplace.user.account.web.service;

import static com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys.USER_ID;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.common.utils.HtmlUtil;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountAddressService;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextFactory;
import com.placecube.digitalplace.user.account.service.AccountDateOfBirthService;
import com.placecube.digitalplace.user.account.service.AccountEmailAddressService;
import com.placecube.digitalplace.user.account.service.AccountPhoneService;
import com.placecube.digitalplace.user.account.service.AccountUserFieldsService;
import com.placecube.digitalplace.user.account.service.AccountUserService;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;
import com.placecube.digitalplace.userprofile.service.UserProfileFieldsService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, CalendarFactoryUtil.class, com.liferay.portal.kernel.util.HtmlUtil.class, HtmlUtil.class })
public class AccountContextServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;

	private static final long GROUP_ID = 2L;

	@InjectMocks
	private AccountContextServiceImpl accountContextServiceImpl;

	@Mock
	private AccountAddressService mockAccountAddressService;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextFactory mockAccountContextFactory;

	@Mock
	private AccountContext mockAccountContextNewInstance;

	@Mock
	private AccountDateOfBirthService mockAccountDateOfBirthService;

	@Mock
	private AccountEmailAddressService mockAccountEmailAddressService;

	@Mock
	private AccountPhoneService mockAccountPhoneService;

	@Mock
	private AccountUserFieldsService mockAccountUserFieldsService;

	@Mock
	private AccountUserService mockAccountUserService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private ExpandoField mockExpandoField;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletSession mockPortletSession;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserAccountField mockUserAccountField;

	@Mock
	private UserAccountFieldRegistry mockUserAccountFieldRegistry;

	@Mock
	private UserAccountField mockUserAccountFieldTwo;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Mock
	private UserProfileFieldsService mockUserProfileFieldsService;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, CalendarFactoryUtil.class, com.liferay.portal.kernel.util.HtmlUtil.class, HtmlUtil.class);
	}

	@Test
	public void clearContext_WhenNoError_ThenRemovesTheContextFromSession() {
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);

		accountContextServiceImpl.clearContext(mockPortletRequest);

		verify(mockPortletSession, times(1)).removeAttribute(AccountConstants.SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE);
	}

	@Test
	public void configureAccountFields_WhenExistUserAccountFields_ThenConfigureContextValueForThem() {
		List<UserAccountField> userAccountFields = new LinkedList<>();
		userAccountFields.add(mockUserAccountField);
		userAccountFields.add(mockUserAccountFieldTwo);

		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserAccountFieldRegistry.getUserAccountFields(COMPANY_ID)).thenReturn(userAccountFields);

		accountContextServiceImpl.configureAccountFields(mockAccountContext, mockPortletRequest);

		verify(mockUserAccountField, times(1)).configureContextValue(mockAccountContext, mockPortletRequest);
		verify(mockUserAccountFieldTwo, times(1)).configureContextValue(mockAccountContext, mockPortletRequest);
	}

	@Test
	public void configureAdditionalDetails_WhenNoEmailIsOnPage_ThenConfiguresTheEmailFields() throws UserAccountException {
		when(ParamUtil.getString(mockPortletRequest, "emailAddress")).thenReturn("emailValue");
		when(ParamUtil.getString(mockPortletRequest, "confirmEmailAddress")).thenReturn("confirmEmailValue");
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		accountContextServiceImpl.configureAdditionalDetails(mockAccountContext, mockPortletRequest);

		InOrder inOrder = inOrder(mockAccountContext, mockPortletSession);
		inOrder.verify(mockAccountContext, times(1)).setEmailAddress("emailValue");
		inOrder.verify(mockAccountContext, times(1)).setConfirmEmailAddress("confirmEmailValue");
	}

	@Test
	public void configureAdditionalDetails_WhenNoEmailNotOnPage_ThenDoesNotConfigureTheEmailFields() throws UserAccountException {
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		accountContextServiceImpl.configureAdditionalDetails(mockAccountContext, mockPortletRequest);

		InOrder inOrder = inOrder(mockAccountContext);
		inOrder.verify(mockAccountContext, never()).setEmailAddress(ArgumentMatchers.anyString());
		inOrder.verify(mockAccountContext, never()).setConfirmEmailAddress(ArgumentMatchers.anyString());
	}

	@Test
	public void configureAdditionalDetails_WhenNoError_ThenConfiguresAccountFields() throws UserAccountException {
		String jobTitle = "jobTitleVal";
		String homePhoneNumber = "homePhoneNumberVal";
		String mobilePhoneNumber = "mobilePhoneNumberVal";
		String businessPhoneNumber = "businessPhoneNumberVal";

		when(com.liferay.portal.kernel.util.HtmlUtil.escape(ParamUtil.getString(mockPortletRequest, "jobTitle"))).thenReturn(jobTitle);
		when(ParamUtil.getString(mockPortletRequest, "homePhoneNumber")).thenReturn(homePhoneNumber);
		when(ParamUtil.getString(mockPortletRequest, "mobilePhoneNumber")).thenReturn(mobilePhoneNumber);
		when(ParamUtil.getString(mockPortletRequest, "businessPhoneNumber")).thenReturn(businessPhoneNumber);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		accountContextServiceImpl.configureAdditionalDetails(mockAccountContext, mockPortletRequest);

		InOrder inOrder = inOrder(mockAccountContext);
		inOrder.verify(mockAccountContext, times(1)).setJobTitle(jobTitle);
		inOrder.verify(mockAccountContext, times(1)).setHomePhoneNumber(homePhoneNumber);
		inOrder.verify(mockAccountContext, times(1)).setMobilePhoneNumber(mobilePhoneNumber);
		inOrder.verify(mockAccountContext, times(1)).setBusinessPhoneNumber(businessPhoneNumber);
	}

	@Test
	public void configureAdditionalDetails_WhenThereAreExpandoFields_ThenAddTheirValuesToContext() throws UserAccountException, PortalException {
		String expandoFieldName = "expandoFieldNameVal";
		String expandoFieldValue = "expandoFieldValueVal";
		Set<ExpandoField> expandoFields = new HashSet<>();
		expandoFields.add(mockExpandoField);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockExpandoField.getExpandoFieldKey()).thenReturn(expandoFieldName);
		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(expandoFields);
		when(mockUserProfileFieldsService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(com.liferay.portal.kernel.util.HtmlUtil.escape(ParamUtil.getString(mockPortletRequest, "expando--" + expandoFieldName))).thenReturn(expandoFieldValue);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		accountContextServiceImpl.configureAdditionalDetails(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, times(1)).addExpandoField(expandoFieldName, expandoFieldValue);
	}

	@Test
	public void configureAdditionalDetails_WhenThereAreNoExpandoFields_ThenDoNotAddTheirValuesToContext() throws UserAccountException {
		Set<ExpandoField> expandoFields = new HashSet<>();

		when(mockUserProfileFieldsService.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration)).thenReturn(expandoFields);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		accountContextServiceImpl.configureAdditionalDetails(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, never()).addExpandoField(ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
	}

	@Test
	public void configureAdditionalDetails_WhenThereAreNoUserAccountFields_ThenAddTheirValuesToContext() throws UserAccountException {
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockUserAccountField.getExpandoFieldName()).thenReturn("fieldName");
		when(com.liferay.portal.kernel.util.HtmlUtil.escape(ParamUtil.getString(mockPortletRequest, mockUserAccountField.getExpandoFieldName()))).thenReturn("fieldValue");
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		when(mockUserAccountFieldRegistry.getUserAccountFieldsByStep(AccountCreationStep.ADDITIONAL_DETAILS, COMPANY_ID)).thenReturn(createUserAccountFields);

		accountContextServiceImpl.configureAdditionalDetails(mockAccountContext, mockPortletRequest);

		verify(mockUserAccountField, never()).configureContextValue(mockAccountContext, mockPortletRequest);
	}

	@Test
	public void configureAdditionalDetails_WhenThereAreUserAccountFields_ThenAddTheirValuesToContext() throws UserAccountException {
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockUserAccountField.getExpandoFieldName()).thenReturn("fieldName");
		when(com.liferay.portal.kernel.util.HtmlUtil.escape(ParamUtil.getString(mockPortletRequest, mockUserAccountField.getExpandoFieldName()))).thenReturn("fieldValue");
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockUserAccountField);
		when(mockUserAccountFieldRegistry.getUserAccountFieldsByStep(AccountCreationStep.ADDITIONAL_DETAILS, COMPANY_ID)).thenReturn(createUserAccountFields);

		accountContextServiceImpl.configureAdditionalDetails(mockAccountContext, mockPortletRequest);

		verify(mockUserAccountField, times(1)).configureContextValue(mockAccountContext, mockPortletRequest);
	}

	@Test
	public void configureAddressDetails_WhenNoError_ThenAddressAccountUserFieldsAreSet() {
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);

		accountContextServiceImpl.configureAddressDetails(mockAccountContext, mockPortletRequest);

		verify(mockUserAccountFieldRegistry, times(1)).getUserAccountFieldsByStep(AccountCreationStep.ADDRESS_DETAILS, COMPANY_ID);
	}

	@Test
	@Parameters({ "null,null,null", "not null,null,null", "not null,not null,null", "null,not null,null", "null,not null,not null", "null,null,not null", "not null,null,not null" })
	public void configureAddressDetails_WhenNoError_ThenAddressDetailsAreSet(String uprn, String postcode, String fullAddress) {

		when(ParamUtil.getString(mockPortletRequest, "uprn")).thenReturn(uprn);
		when(ParamUtil.getString(mockPortletRequest, "postcode")).thenReturn(postcode);
		when(ParamUtil.getString(mockPortletRequest, "fullAddress")).thenReturn(fullAddress);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);

		accountContextServiceImpl.configureAddressDetails(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, never()).setUprn(ArgumentMatchers.anyString());
		verify(mockAccountContext, never()).setPostcode(ArgumentMatchers.anyString());
		verify(mockAccountContext, never()).setFullAddress(ArgumentMatchers.anyString());
	}

	@Test
	public void configureAddressDetails_WhenNoError_ThenConfiguresTheFields() {

		String uprn = "uprn";
		String postcode = "postcode";
		String fullAddress = "fullAddress";

		when(ParamUtil.getString(mockPortletRequest, "uprn")).thenReturn(uprn);
		when(ParamUtil.getString(mockPortletRequest, "postcode")).thenReturn(postcode);
		when(ParamUtil.getString(mockPortletRequest, "fullAddress")).thenReturn(fullAddress);

		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);

		accountContextServiceImpl.configureAddressDetails(mockAccountContext, mockPortletRequest);

		InOrder inOrder = inOrder(mockAccountContext, mockPortletSession);
		inOrder.verify(mockAccountContext, times(1)).setUprn(uprn);
		inOrder.verify(mockAccountContext, times(1)).setPostcode(postcode);
		inOrder.verify(mockAccountContext, times(1)).setFullAddress(fullAddress);
	}

	@Test
	public void configureAutoGeneratedEmailDetails_WhenEmailAddressIsEmpty_ThenGenerateEmailAddressAndConfigureContext() throws UserAccountException {
		String emailAddress = "john.smith@smith.com";
		when(mockAccountContext.getEmailAddress()).thenReturn("");
		when(mockAccountEmailAddressService.getGeneratedEmailAddress(mockAccountContext)).thenReturn(emailAddress);

		accountContextServiceImpl.configureAutoGeneratedEmailDetails(mockAccountContext);

		verify(mockAccountContext, times(1)).setEmailAddress(emailAddress);
		verify(mockAccountContext, times(1)).setConfirmEmailAddress(emailAddress);
		verify(mockAccountContext, times(1)).setEmailAddressAutoGenerated(true);
	}

	@Test
	public void configureAutoGeneratedEmailDetails_WhenEmailAddressIsNotEmpty_ThenNoActionTaken() throws UserAccountException {
		when(mockAccountContext.getEmailAddress()).thenReturn("email@not-empty.com");

		accountContextServiceImpl.configureAutoGeneratedEmailDetails(mockAccountContext);

		verifyZeroInteractions(mockAccountEmailAddressService);
		verify(mockAccountContext, never()).setEmailAddressAutoGenerated(true);
		verify(mockAccountContext, never()).setConfirmEmailAddress(ArgumentMatchers.anyString());
		verify(mockAccountContext, never()).setEmailAddress(ArgumentMatchers.anyString());
	}

	@Test
	public void configurePasswordDetails_WhenNoError_ThenConfiguresTheFieldsAnd() {
		when(ParamUtil.getString(mockPortletRequest, "password")).thenReturn("passwordValue");
		when(ParamUtil.getString(mockPortletRequest, "confirmPassword")).thenReturn("confirmPasswordValue");
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);

		accountContextServiceImpl.configurePasswordDetails(mockAccountContext, mockPortletRequest);

		InOrder inOrder = inOrder(mockAccountContext, mockPortletSession);
		inOrder.verify(mockAccountContext, times(1)).setPassword1("passwordValue");
		inOrder.verify(mockAccountContext, times(1)).setPassword2("confirmPasswordValue");
	}

	@Test
	public void configurePersonalDetails_WhenEmailAddressIsOnPage_ThenConfiguresTheEmailAddress() throws UserAccountException {
		when(ParamUtil.getString(mockPortletRequest, "emailAddress")).thenReturn("emailAddressValue");
		when(ParamUtil.getString(mockPortletRequest, "confirmEmailAddress")).thenReturn("confirmEmailAddressValue");
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		accountContextServiceImpl.configurePersonalDetails(mockAccountContext, mockPortletRequest);

		InOrder inOrder = inOrder(mockAccountContext, mockPortletSession);
		inOrder.verify(mockAccountContext, times(1)).setEmailAddress("emailAddressValue");
		inOrder.verify(mockAccountContext, times(1)).setConfirmEmailAddress("confirmEmailAddressValue");
	}

	@Test
	public void configurePersonalDetails_WhenEmailAddressNotOnPage_ThenEmailDetailsAreNotPopulated() throws UserAccountException {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		accountContextServiceImpl.configurePersonalDetails(mockAccountContext, mockPortletRequest);

		InOrder inOrder = inOrder(mockAccountContext, mockPortletSession);
		inOrder.verify(mockAccountContext, never()).setEmailAddress(ArgumentMatchers.any());
		inOrder.verify(mockAccountContext, never()).setConfirmEmailAddress(ArgumentMatchers.any());
	}

	@Test
	public void configurePersonalDetails_WhenNoError_ThenConfiguresTheFields() throws UserAccountException {
		String firstNameValue = "firstNameValue";
		String lastNameValue = "lastNameValue";
		when(ParamUtil.getString(mockPortletRequest, "firstName")).thenReturn(firstNameValue);
		when(ParamUtil.getString(mockPortletRequest, "lastName")).thenReturn(lastNameValue);
		when(HtmlUtil.escapeName(firstNameValue)).thenReturn(firstNameValue);
		when(HtmlUtil.escapeName(lastNameValue)).thenReturn(lastNameValue);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		accountContextServiceImpl.configurePersonalDetails(mockAccountContext, mockPortletRequest);

		InOrder inOrder = inOrder(mockAccountContext, mockPortletSession);
		inOrder.verify(mockAccountContext, times(1)).setFirstName("firstNameValue");
		inOrder.verify(mockAccountContext, times(1)).setLastName("lastNameValue");
	}

	@Test
	public void configureSkipPasswordStep_WhenGeneratePasswordWhenLoggedInUserCreateAccountIsTrueAndCreatorSignedInIsTrue_ThenConfigureSkipPasswordToTrue() throws UserAccountException {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.generatePasswordWhenLoggedInUserCreateAccount()).thenReturn(true);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(true);

		accountContextServiceImpl.configureSkipPasswordStep(mockAccountContext);

		verify(mockAccountContext, times(1)).setSkipPasswordStep(true);
		verify(mockAccountContext, never()).setSkipPasswordStep(false);
	}

	@Test
	@Parameters({ "false,false", "false,true", "true,false" })
	public void configureSkipPasswordStep_WhenGivenGeneratePasswordWhenLoggedInUserCreateAccountAndCreatorSignedIn_ThenConfigureSkipPasswordToFalse(
			boolean generatePasswordWhenLoggedInUserCreateAccount, boolean isCreatorSignedIn) throws UserAccountException {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.generatePasswordWhenLoggedInUserCreateAccount()).thenReturn(generatePasswordWhenLoggedInUserCreateAccount);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(isCreatorSignedIn);

		accountContextServiceImpl.configureSkipPasswordStep(mockAccountContext);

		verify(mockAccountContext, times(1)).setSkipPasswordStep(false);
		verify(mockAccountContext, never()).setSkipPasswordStep(true);
	}

	@Test
	public void generateProfileRedirectUrl_WhenNoError_ThenReturnsRedirectUrl() {
		String redirectUrl = "place.com?" + AccountConstants.REDIRECT_URL_PLACEHOLDER;
		String expectedUrl = "place.com?userId=23";
		long userId = 23L;
		when(mockAccountContext.getProfileRedirectUrl()).thenReturn(redirectUrl);
		when(mockAccountContext.getUserId()).thenReturn(userId);
		String result = accountContextServiceImpl.generateProfileRedirectUrl(mockAccountContext);

		assertEquals(result, expectedUrl);
	}

	@Test
	public void generateProfileRedirectUrl_WhenNoRedirectUrlInContext_ThenReturnsBlank() {
		String result = accountContextServiceImpl.generateProfileRedirectUrl(mockAccountContext);

		assertEquals(StringPool.BLANK, result);
	}

	@Test
	public void generateProfileRedirectUrl_WhenRedirectUrlDoesNotContainPlaceholder_ThenReturnsUnchangedUrl() {
		String redirectUrl = "someUrlHere";
		long userId = 23L;
		when(mockAccountContext.getProfileRedirectUrl()).thenReturn(redirectUrl);
		when(mockAccountContext.getUserId()).thenReturn(userId);
		String result = accountContextServiceImpl.generateProfileRedirectUrl(mockAccountContext);

		assertEquals(result, redirectUrl);
	}

	@Test
	public void generateProfileRedirectUrl_WhenUserIdZeroInContext_ThenReturnsBlank() {
		String redirectUrl = "place.com?" + AccountConstants.REDIRECT_URL_PLACEHOLDER;
		long userId = 0L;
		when(mockAccountContext.getProfileRedirectUrl()).thenReturn(redirectUrl);
		when(mockAccountContext.getUserId()).thenReturn(userId);
		String result = accountContextServiceImpl.generateProfileRedirectUrl(mockAccountContext);

		assertEquals(StringPool.BLANK, result);
	}

	@Test(expected = UserAccountException.class)
	public void getContextFromUserDetails_WhenExceptionGettingUser_ThenPortalExceptionIsThrown() throws Exception {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockAccountContextFactory.createAccountContext()).thenReturn(mockAccountContext);
		when(mockUserLocalService.getUserById(anyLong())).thenThrow(new PortalException());

		accountContextServiceImpl.getContextFromUserDetails(mockPortletRequest, mockUserProfileFieldsConfiguration);

	}

	@Test
	public void getContextFromUserDetails_WhenNoErrors_ThenAccountContextIsPoplulated() throws Exception {
		long userId = 12345L;

		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(mockAccountContextFactory.createAccountContext()).thenReturn(mockAccountContext);
		when(mockUserLocalService.getUserById(userId)).thenReturn(mockUser);
		when(ParamUtil.getLong(mockPortletRequest, USER_ID, 0)).thenReturn(userId);

		accountContextServiceImpl.getContextFromUserDetails(mockPortletRequest, mockUserProfileFieldsConfiguration);

		InOrder inOrder = inOrder(mockAccountContext, mockAccountUserService, mockAccountDateOfBirthService, mockAccountAddressService, mockAccountPhoneService, mockAccountUserFieldsService,
				mockPortletSession);
		inOrder.verify(mockAccountContext, times(1)).setUpdate(true);
		inOrder.verify(mockAccountUserService, times(1)).populateContextWithExistingUserDetails(mockAccountContext, mockUser);
		inOrder.verify(mockAccountDateOfBirthService, times(1)).populateContextWithExistingDateOfBirth(mockAccountContext, mockUser);
		inOrder.verify(mockAccountAddressService, times(1)).populateContextWithExistingPrimaryAddress(mockAccountContext, mockUser);
		inOrder.verify(mockAccountPhoneService, times(1)).populateContextWithExistingPhones(mockAccountContext, mockUser);
		inOrder.verify(mockAccountUserFieldsService, times(1)).populateContextWithExistingConfiguredExpandoFields(mockAccountContext, mockUser, mockUserProfileFieldsConfiguration);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateContext_WhenNoErrors_ThenCreatorSignedInIsConfigured(boolean isSignedIn) throws UserAccountException {
		when(mockAccountContextFactory.createAccountContext()).thenReturn(mockAccountContext);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(ParamUtil.getBoolean(mockPortletRequest, RequestKeys.USE_SESSION, true)).thenReturn(true);
		when(mockPortletSession.getAttribute(AccountConstants.SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE)).thenReturn(null);
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(isSignedIn);

		accountContextServiceImpl.getOrCreateContext(mockPortletRequest);

		verify(mockAccountContext, times(1)).setCreatorSignedIn(isSignedIn);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateContext_WhenUseSessionIsFalseAndGivenSignedIn_ThenContextInSessionIsNotUsedAndCreatorSignedInIsConfigured(boolean isSignedIn) throws UserAccountException {
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(ParamUtil.getBoolean(mockPortletRequest, RequestKeys.USE_SESSION, true)).thenReturn(false);
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(isSignedIn);
		when(mockPortletSession.getAttribute(AccountConstants.SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE)).thenReturn(mockAccountContext);

		when(mockAccountContextFactory.createAccountContext()).thenReturn(mockAccountContextNewInstance);

		AccountContext result = accountContextServiceImpl.getOrCreateContext(mockPortletRequest);

		assertNotEquals(mockAccountContext, result);
		verify(mockAccountContextNewInstance, times(1)).setCreatorSignedIn(isSignedIn);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateContext_WhenUseSessionIsTrueAndContextInSessionIsNotNull_ThenContextInSessionIsUsedAndCreatorSignedInIsConfigured(boolean isSignedIn) throws UserAccountException {
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(ParamUtil.getBoolean(mockPortletRequest, RequestKeys.USE_SESSION, true)).thenReturn(true);
		when(mockPortletSession.getAttribute(AccountConstants.SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE)).thenReturn(mockAccountContext);
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(isSignedIn);

		AccountContext result = accountContextServiceImpl.getOrCreateContext(mockPortletRequest);

		assertThat(result, sameInstance(mockAccountContext));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateContext_WhenUseSessionIsTrueAndContextInSessionIsNull_ThenNewContextIsCreatedAndCreatorSignedInIsConfigured(boolean isSignedIn) throws UserAccountException {
		when(mockAccountContextFactory.createAccountContext()).thenReturn(mockAccountContext);
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);
		when(ParamUtil.getBoolean(mockPortletRequest, RequestKeys.USE_SESSION, true)).thenReturn(true);
		when(mockPortletSession.getAttribute(AccountConstants.SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE)).thenReturn(null);
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(isSignedIn);

		AccountContext result = accountContextServiceImpl.getOrCreateContext(mockPortletRequest);

		assertThat(result, sameInstance(mockAccountContext));
	}

	@Test(expected = UserAccountException.class)
	public void isGeneratePasswordWhenLoggedInUserCreateAccount_WhenExceptionGettingConfiguration_ThenPortletExceptionIsThrown() throws UserAccountException {

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenThrow(new UserAccountException());

		accountContextServiceImpl.isGeneratePasswordWhenLoggedInUserCreateAccount(mockAccountContext);

	}

	@Test
	public void isGeneratePasswordWhenLoggedInUserCreateAccount_WhenResetPasswordButtonInConfigurationPageWhenLoggedInUserCreateAccountAndCreatorSignedInAreTrue_ThenReturnTrue()
			throws UserAccountException {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount()).thenReturn(true);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(true);

		boolean result = accountContextServiceImpl.isGeneratePasswordWhenLoggedInUserCreateAccount(mockAccountContext);

		assertTrue(result);
	}

	@Test
	@Parameters({ "false,false", "false,true", "true,false" })
	public void isGeneratePasswordWhenLoggedInUserCreateAccount_WithGivenValues_ThernReturnFalse(boolean resetPasswordButtonInConfigurationPageWhenLoggedInUserCreateAccount, boolean isCreatorSignedIn)
			throws UserAccountException {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount()).thenReturn(resetPasswordButtonInConfigurationPageWhenLoggedInUserCreateAccount);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(isCreatorSignedIn);

		boolean result = accountContextServiceImpl.isGeneratePasswordWhenLoggedInUserCreateAccount(mockAccountContext);

		assertFalse(result);
	}

	@Test
	public void isShowCaptchaOnAccountSecurityStep_WhenAccountSecurityStepIsNotEnabled_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(false);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(false);
		when(mockAccountConfigurationService.isAccountSecurityStepEnabled(mockAccountContext)).thenReturn(false);

		boolean result = accountContextServiceImpl.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnAccountSecurityStep_WhenCaptchaIsEnabledAndCaptchaIsNotRequiredBeforeEmailEntryAndCreatorIsNotSignedInAndAccountSecurityStepIsEnabled_ThenReturnsTrue()
			throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(false);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(false);
		when(mockAccountConfigurationService.isAccountSecurityStepEnabled(mockAccountContext)).thenReturn(true);

		boolean result = accountContextServiceImpl.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isShowCaptchaOnAccountSecurityStep_WhenCaptchaIsNotEnabled_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(false);

		boolean result = accountContextServiceImpl.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnAccountSecurityStep_WhenCaptchaIsRequiredBeforeEmailEntry_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(true);

		boolean result = accountContextServiceImpl.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnAccountSecurityStep_WhenCreatorIsSignedIn_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(false);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(true);

		boolean result = accountContextServiceImpl.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnEmailStep_WhenCaptchaIsEnabledAndCaptchaIsRequiredBeforeEmailEntryAndCreatorIsNotSignedIn_ThenReturnsTrue() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(true);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(false);

		boolean result = accountContextServiceImpl.isShowCaptchaOnEmailStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isShowCaptchaOnEmailStep_WhenCaptchaIsNotEnabled_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(false);

		boolean result = accountContextServiceImpl.isShowCaptchaOnEmailStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnEmailStep_WhenCaptchaIsNotRequiredBeforeEmailEntry_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(false);

		boolean result = accountContextServiceImpl.isShowCaptchaOnEmailStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnEmailStep_WhenCreatorIsSignedIn_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(true);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(true);

		boolean result = accountContextServiceImpl.isShowCaptchaOnEmailStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnPasswordStep_WhenAccountSecurityStepIsEnabled_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(false);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(false);
		when(mockAccountConfigurationService.isAccountSecurityStepEnabled(mockAccountContext)).thenReturn(true);

		boolean result = accountContextServiceImpl.isShowCaptchaOnPasswordStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnPasswordStep_WhenCaptchaIsEnabledAndCaptchaIsNotRequiredBeforeEmailEntryAndCreatorIsNotSignedInAndAccountSecurityStepIsDisabled_ThenReturnsTrue() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(false);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(false);
		when(mockAccountConfigurationService.isAccountSecurityStepEnabled(mockAccountContext)).thenReturn(false);

		boolean result = accountContextServiceImpl.isShowCaptchaOnPasswordStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isShowCaptchaOnPasswordStep_WhenCaptchaIsNotEnabled_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(false);

		boolean result = accountContextServiceImpl.isShowCaptchaOnPasswordStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnPasswordStep_WhenCaptchaIsRequiredBeforeEmailEntry_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(true);

		boolean result = accountContextServiceImpl.isShowCaptchaOnPasswordStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShowCaptchaOnPasswordStep_WhenCreatorIsSignedIn_ThenReturnsFalse() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockPortletRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.isCaptchaEnabled(mockPortletRequest)).thenReturn(true);
		when(mockCreateAccountConfiguration.requireCaptchaBeforeEmailAddressEntry()).thenReturn(false);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(true);

		boolean result = accountContextServiceImpl.isShowCaptchaOnPasswordStep(mockAccountContext, mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void setPhoneNumberToAccountContext_WhenUserDataIsNotPhoneNumber_ThenNoPhoneNumberIsSet() throws UserAccountException {
		String userData = "someothervalue";
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getString(mockPortletRequest, RequestKeys.USER_DATA)).thenReturn(userData);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);

		accountContextServiceImpl.setPhoneNumberToAccountContext(mockPortletRequest, mockAccountContext);

		verify(mockAccountContext, never()).setMobilePhoneNumber(userData);
		verify(mockAccountContext, never()).setBusinessPhoneNumber(userData);
		verify(mockAccountContext, never()).setHomePhoneNumber(userData);

	}

	@Test
	public void setPhoneNumberToAccountContext_WhenUserDataIsPhoneNumberAndBusinessPhoneIsEnabled_ThenSetBusinessPhoneNumber() throws UserAccountException {
		String userData = "1234567898";
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getString(mockPortletRequest, RequestKeys.USER_DATA)).thenReturn(userData);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(true);

		accountContextServiceImpl.setPhoneNumberToAccountContext(mockPortletRequest, mockAccountContext);

		verify(mockAccountContext, times(1)).setBusinessPhoneNumber(userData);
	}

	@Test
	public void setPhoneNumberToAccountContext_WhenUserDataIsPhoneNumberAndHomePhoneIsEnabled_ThenSetHomePhoneNumber() throws UserAccountException {
		String userData = "1234567898";
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getString(mockPortletRequest, RequestKeys.USER_DATA)).thenReturn(userData);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);

		accountContextServiceImpl.setPhoneNumberToAccountContext(mockPortletRequest, mockAccountContext);

		verify(mockAccountContext, times(1)).setHomePhoneNumber(userData);
	}

	@Test
	public void setPhoneNumberToAccountContext_WhenUserDataIsPhoneNumberAndMobilePhoneIsEnabled_ThenSetMobilePhoneNumber() throws UserAccountException {
		String userData = "1234567898";
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getString(mockPortletRequest, RequestKeys.USER_DATA)).thenReturn(userData);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);

		accountContextServiceImpl.setPhoneNumberToAccountContext(mockPortletRequest, mockAccountContext);

		verify(mockAccountContext, times(1)).setMobilePhoneNumber(userData);
	}

	@Test
	public void setPhoneNumberToAccountContext_WhenUserDataIsPhoneNumberAndNoUserPhoneIsEnabled_ThenNoPhoneNumberIsSet() throws UserAccountException {
		String userData = "1234567898";
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(ParamUtil.getString(mockPortletRequest, RequestKeys.USER_DATA)).thenReturn(userData);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(false);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(false);

		accountContextServiceImpl.setPhoneNumberToAccountContext(mockPortletRequest, mockAccountContext);

		verify(mockAccountContext, never()).setMobilePhoneNumber(userData);
		verify(mockAccountContext, never()).setBusinessPhoneNumber(userData);
		verify(mockAccountContext, never()).setHomePhoneNumber(userData);

	}

	@Test
	public void updateContextInSession_WhenNoError_ThenUpdateContextInSession() {
		when(mockPortletRequest.getPortletSession()).thenReturn(mockPortletSession);

		accountContextServiceImpl.updateContextInSession(mockAccountContext, mockPortletRequest);
		verify(mockPortletSession, times(1)).setAttribute(AccountConstants.SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE, mockAccountContext);
	}

}
