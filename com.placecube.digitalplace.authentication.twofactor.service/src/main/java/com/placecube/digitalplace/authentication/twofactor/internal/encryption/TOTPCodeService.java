package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import java.util.Optional;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.client.utils.URIBuilder;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import de.taimos.totp.TOTP;

@Component(immediate = true, service = TOTPCodeService.class)
public class TOTPCodeService {

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	public String buildOTPUrl(String issuer, String emailAddress, String secretKey, String algorithm, int authenticatorCodeLength, int authenticatorCodeDurationInSecs) {
		URIBuilder uri = modelFactoryBuilder.getURIBuilderInstance();
		uri.setScheme("otpauth");
		uri.setHost("totp");
		uri.setPath("/" + modelFactoryBuilder.getFormattedIssuerLabel(issuer, emailAddress));
		uri.setParameter("secret", secretKey);
		uri.setParameter("issuer", issuer);
		uri.setParameter("algorithm", algorithm.replaceAll("Hmac", ""));
		uri.setParameter("digits", String.valueOf(authenticatorCodeLength));
		uri.setParameter("period", String.valueOf(authenticatorCodeDurationInSecs));
		return uri.toString();
	}

	public Optional<String> generateTopCode(String secretKey) {

		Base32 base32 = modelFactoryBuilder.getBase32Instance();
		byte[] bytes = base32.decode(secretKey);
		String hexKey = Hex.encodeHexString(bytes);
		return Optional.of(TOTP.getOTP(hexKey));
	}
}
