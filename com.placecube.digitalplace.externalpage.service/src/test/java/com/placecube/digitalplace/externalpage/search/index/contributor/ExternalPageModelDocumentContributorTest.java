package com.placecube.digitalplace.externalpage.search.index.contributor;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.HtmlParserUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.externalpage.model.ExternalPage;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HtmlParserUtil.class})
@SuppressStaticInitializationFor("com.liferay.portal.kernel.module.util.SystemBundleUtil")
public class ExternalPageModelDocumentContributorTest extends PowerMockito {

	@InjectMocks
	private ExternalPageModelDocumentContributor externalPageModelDocumentContributor;

	@Mock
	private Date mockCreateDate;

	@Mock
	private Document mockDocument;

	@Mock
	private ExternalPage mockExternalPage;

	@Mock
	private Date mockModifiedDate;

	
	@Before
	public void activeSetup() {
		mockStatic(HtmlParserUtil.class);
	}
	
	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheClassNameField() {
		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addKeyword(Field.ENTRY_CLASS_NAME, ExternalPage.class.getName());
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheClassPKField() {
		long expected = 123;
		when(mockExternalPage.getExternalPageId()).thenReturn(expected);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addKeyword(Field.CLASS_PK, expected);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheCompanyIdField() {
		long expected = 123;
		when(mockExternalPage.getCompanyId()).thenReturn(expected);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addKeyword(Field.COMPANY_ID, expected);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheContentField() {
		String expected = "expectedValue";
		String content = "myHtmlContent";
		when(mockExternalPage.getContent()).thenReturn(content);
		when(HtmlParserUtil.extractText(content)).thenReturn(expected);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addText(Field.CONTENT, expected);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheCreateDateField() {
		when(mockExternalPage.getCreateDate()).thenReturn(mockCreateDate);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addDate(Field.CREATE_DATE, mockCreateDate);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheDisplayDateField() {
		when(mockExternalPage.getCreateDate()).thenReturn(mockCreateDate);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addDate(Field.DISPLAY_DATE, mockCreateDate);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheGroupIdField() {
		long expected = 123;
		when(mockExternalPage.getGroupId()).thenReturn(expected);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addKeyword(Field.GROUP_ID, expected);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheModifiedDateField() {
		when(mockExternalPage.getModifiedDate()).thenReturn(mockModifiedDate);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addDate(Field.MODIFIED_DATE, mockModifiedDate);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithThePublishDateField() {
		when(mockExternalPage.getCreateDate()).thenReturn(mockCreateDate);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addDate(Field.PUBLISH_DATE, mockCreateDate);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheScopeGroupIdField() {
		long expected = 123;
		when(mockExternalPage.getGroupId()).thenReturn(expected);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addKeyword(Field.SCOPE_GROUP_ID, expected);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheStatusField() {
		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addKeyword(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheTitleField() {
		String expected = "expectedValue";
		String title = "myHtmlContent";
		when(mockExternalPage.getTitle()).thenReturn(title);
		when(HtmlParserUtil.extractText(title)).thenReturn(expected);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addText(Field.TITLE, expected);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheUIDField() {
		String expected = "uuidValue";
		when(mockExternalPage.getUuid()).thenReturn(expected);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addKeyword(Field.UID, expected);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheURLField() {
		String expected = "expectedValue";
		when(mockExternalPage.getUrl()).thenReturn(expected);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addKeyword(Field.URL, expected);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheDocumentWithTheUserIdField() {
		long expected = 123;
		when(mockExternalPage.getUserId()).thenReturn(expected);

		externalPageModelDocumentContributor.contribute(mockDocument, mockExternalPage);

		verify(mockDocument, times(1)).addKeyword(Field.USER_ID, expected);
	}

}
