package com.placecube.digitalplace.pageimportadmin.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.pageimportadmin.model.PageImportModel;
import com.placecube.digitalplace.pageimportadmin.model.RowImportModel;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.service.ModelCreationFactory;
import com.placecube.journal.model.JournalArticleContext;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ JournalArticleContext.class, LanguageUtil.class })
public class ModelFactoryBuilderTest extends PowerMockito {

	@InjectMocks
	private ModelFactoryBuilder modelFactoryBuilder;

	private static final Locale LOCALE = Locale.CANADA_FRENCH;
	private static final long ID = 10;
	private static final String CATEGORY_NAME = "validName";
	private static final Long CATEGORY_ID = 20l;
	private static final String MESSAGE = "messageValue";
	private static final String TITLE = "titleValue";
	private static final String URL = "urlValue";
	private static final int INDEX = 30;
	private static final long GROUP_ID = 40;
	private static final String PARENT_LAYOUT_URL = "parentLayoutURL";
	private static final String PAGE_TEMPLATE_NAME = "pageTemplateName";
	private static final String STRUCTURE_NAME = "structureName";

	@Mock
	private ContentRetrievalService mockContentRetrievalService;

	@Mock
	private ModelCreationFactory mockModelCreationFactory;

	@Mock
	private Portal mockPortal;

	@Mock
	private RowImportModel mockRowImportModel;

	@Mock
	private Row mockRow;

	@Mock
	private Cell mockCell;

	@Mock
	private RichTextString mockRichTextString;

	@Mock
	private WebContentImportModel mockWebContentImportModel;

	@Mock
	private JournalArticleContext mockJournalArticleContext;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private List<String> mockCategories;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Mock
	private LayoutContext mockLayoutContext;

	@Mock
	private PageImportModel mockPageImportModel;

	@Before
	public void setUp() {
		mockStatic(JournalArticleContext.class, LanguageUtil.class);
	}

	@Test
	public void configureAndValidateCategories_WhenNoCategoriesSpecified_ThenNoChangesMadeToTheRowImportModel() {
		when(mockRowImportModel.getCategoriesNames()).thenReturn(Collections.emptyList());

		modelFactoryBuilder.configureAndValidateCategories(mockRowImportModel, ID, LOCALE);

		verify(mockRowImportModel, never()).addCategoryId(anyLong());
		verify(mockRowImportModel, never()).addWarningMessage(anyString());
	}

	@Test
	public void configureAndValidateCategories_WhenSingleCategoryFoundForTheSpecifiedName_ThenConfiguresTheCategoryId() {
		List<String> categories = new ArrayList<>();
		categories.add(CATEGORY_NAME);
		when(mockRowImportModel.getCategoriesNames()).thenReturn(categories);
		List<Long> categoriesIds = new ArrayList<>();
		categoriesIds.add(CATEGORY_ID);
		when(mockContentRetrievalService.getCategoriesByName(ID, CATEGORY_NAME)).thenReturn(categoriesIds);

		modelFactoryBuilder.configureAndValidateCategories(mockRowImportModel, ID, LOCALE);

		verify(mockRowImportModel, times(1)).addCategoryId(CATEGORY_ID);
		verify(mockRowImportModel, never()).addWarningMessage(anyString());
	}

	@Test
	public void configureAndValidateCategories_WhenMultipleCategoriesFoundForTheSpecifiedName_ThenConfiguresTheWarningMessage() {
		List<String> categories = new ArrayList<>();
		categories.add(CATEGORY_NAME);
		when(mockRowImportModel.getCategoriesNames()).thenReturn(categories);
		List<Long> categoriesIds = new ArrayList<>();
		categoriesIds.add(CATEGORY_ID);
		categoriesIds.add(456l);
		when(mockContentRetrievalService.getCategoriesByName(ID, CATEGORY_NAME)).thenReturn(categoriesIds);
		when(LanguageUtil.format(LOCALE, "too-many-categories-found", CATEGORY_NAME)).thenReturn(MESSAGE);

		modelFactoryBuilder.configureAndValidateCategories(mockRowImportModel, ID, LOCALE);

		verify(mockRowImportModel, never()).addCategoryId(anyLong());
		verify(mockRowImportModel, times(1)).addWarningMessage(MESSAGE);
	}

	@Test
	public void configureAndValidateCategories_WhenNoCategoryFoundForTheSpecifiedName_ThenConfiguresTheWarningMessage() {
		List<String> categories = new ArrayList<>();
		categories.add(CATEGORY_NAME);
		when(mockRowImportModel.getCategoriesNames()).thenReturn(categories);
		when(mockContentRetrievalService.getCategoriesByName(ID, CATEGORY_NAME)).thenReturn(Collections.emptyList());
		when(LanguageUtil.format(LOCALE, "category-not-found", CATEGORY_NAME)).thenReturn(MESSAGE);

		modelFactoryBuilder.configureAndValidateCategories(mockRowImportModel, ID, LOCALE);

		verify(mockRowImportModel, never()).addCategoryId(anyLong());
		verify(mockRowImportModel, times(1)).addWarningMessage(MESSAGE);
	}

	@Test
	public void configurePageTemplate_WhenNoError_ThenUpdatesTheLayoutContextWithTheClassPK() {
		when(mockLayoutPageTemplateEntry.getLayoutPageTemplateEntryId()).thenReturn(ID);

		modelFactoryBuilder.configurePageTemplate(mockLayoutContext, mockLayoutPageTemplateEntry);

		verify(mockLayoutContext, times(1)).setClassPK(ID);
	}

	@Test
	public void configurePageTemplate_WhenNoError_ThenUpdatesTheLayoutContextWithTheClassNameId() {
		when(mockPortal.getClassNameId(LayoutPageTemplateEntry.class)).thenReturn(ID);

		modelFactoryBuilder.configurePageTemplate(mockLayoutContext, mockLayoutPageTemplateEntry);

		verify(mockLayoutContext, times(1)).setClassNameId(ID);
	}

	@Test
	public void getJournalArticleContext_WhenNoError_ThenReturnsJournalArticleContextConfigured() {
		when(JournalArticleContext.init(StringPool.BLANK, TITLE, "<?xml version=\"1.0\"?><root></root>")).thenReturn(mockJournalArticleContext);
		when(mockWebContentImportModel.getUrl()).thenReturn(URL);
		when(mockWebContentImportModel.getTitle()).thenReturn(TITLE);
		when(mockWebContentImportModel.getWebContentStructure()).thenReturn(Optional.of(mockDDMStructure));

		JournalArticleContext result = modelFactoryBuilder.getJournalArticleContext(mockWebContentImportModel);

		assertThat(result, sameInstance(mockJournalArticleContext));
		verify(mockJournalArticleContext, times(1)).setUrlTitle(URL);
		verify(mockJournalArticleContext, times(1)).setDDMStructure(mockDDMStructure);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLayoutContext_WhenNoError_ThenReturnsTheConfiguredLayoutContext(boolean privateLayout) {
		when(mockPageImportModel.getUrl()).thenReturn(URL);
		when(mockPageImportModel.getTitle()).thenReturn(TITLE);
		when(mockPageImportModel.isPrivateLayout()).thenReturn(privateLayout);
		when(mockPageImportModel.getParentPageURL()).thenReturn(PARENT_LAYOUT_URL);
		when(mockModelCreationFactory.createContentPageLayoutContext(TITLE, URL, privateLayout)).thenReturn(mockLayoutContext);

		LayoutContext result = modelFactoryBuilder.getLayoutContext(mockPageImportModel);

		assertThat(result, sameInstance(mockLayoutContext));
		verify(mockLayoutContext, times(1)).setParentLayoutFriendlyURL(PARENT_LAYOUT_URL);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getPageImportModel_WhenNoError_ThenReturnsTheWebContentImportModelConfigured(boolean privateLayout) {
		when(mockContentRetrievalService.getLayout(GROUP_ID, PARENT_LAYOUT_URL, privateLayout)).thenReturn(Optional.of(mockLayout));
		when(mockContentRetrievalService.getPageTemplate(GROUP_ID, PAGE_TEMPLATE_NAME)).thenReturn(Optional.of(mockLayoutPageTemplateEntry));

		PageImportModel result = (PageImportModel) modelFactoryBuilder.getPageImportModel(INDEX, GROUP_ID, TITLE, URL, mockCategories, privateLayout, PARENT_LAYOUT_URL, PAGE_TEMPLATE_NAME);

		assertThat(result.getIndex(), equalTo(INDEX + 1));
		assertThat(result.getGroupId(), equalTo(GROUP_ID));
		assertThat(result.getTitle(), equalTo(TITLE));
		assertThat(result.getUrl(), equalTo(URL));
		assertThat(result.getCategoriesNames(), equalTo(mockCategories));
		assertThat(result.isPrivateLayout(), equalTo(privateLayout));
		assertThat(result.getParentPageURL(), equalTo(PARENT_LAYOUT_URL));
		assertThat(result.getParentLayout(), equalTo(Optional.of(mockLayout)));
		assertThat(result.getPageTemplateName(), equalTo(PAGE_TEMPLATE_NAME));
		assertThat(result.getPageTemplate(), equalTo(Optional.of(mockLayoutPageTemplateEntry)));
	}

	@Test
	public void getWebContentImportModel_WhenNoError_ThenReturnsTheWebContentImportModelConfigured() {
		when(mockContentRetrievalService.getWebContentStructure(GROUP_ID, STRUCTURE_NAME)).thenReturn(Optional.of(mockDDMStructure));

		WebContentImportModel result = (WebContentImportModel) modelFactoryBuilder.getWebContentImportModel(INDEX, GROUP_ID, TITLE, URL, mockCategories, STRUCTURE_NAME);

		assertThat(result.getIndex(), equalTo(INDEX + 1));
		assertThat(result.getGroupId(), equalTo(GROUP_ID));
		assertThat(result.getTitle(), equalTo(TITLE));
		assertThat(result.getUrl(), equalTo(URL));
		assertThat(result.getCategoriesNames(), equalTo(mockCategories));
		assertThat(result.getWebContentStructure(), equalTo(Optional.of(mockDDMStructure)));
	}

	@Test
	public void importAsWebContent_WhenPageTemplateIsValid_ThenReturnsFalse() {
		boolean result = modelFactoryBuilder.importAsWebContent("pageTemplateName", STRUCTURE_NAME);

		assertFalse(result);
	}

	@Test
	public void importAsWebContent_WhenPageTemplateIsInvalidAndStructureIdIsInvalid_ThenReturnsFalse() {
		boolean result = modelFactoryBuilder.importAsWebContent(null, null);

		assertFalse(result);
	}

	@Test
	public void importAsWebContent_WhenPageTemplateIsInvalidAndStructureIdIsValid_ThenReturnsTrue() {
		boolean result = modelFactoryBuilder.importAsWebContent(null, STRUCTURE_NAME);

		assertTrue(result);
	}

	@Test
	public void getStringField_WhenNoError_ThenReturnStringValueofFIeld() throws Exception {
		when(mockRow.getCell(0)).thenReturn(mockCell);
		when(mockCell.getCellTypeEnum()).thenReturn(CellType.STRING);
		when(mockCell.getRichStringCellValue()).thenReturn(mockRichTextString);
		when(mockRichTextString.getString()).thenReturn(TITLE);

		String result = modelFactoryBuilder.getStringField(mockRow, 0);

		assertThat(result, equalTo(TITLE));
	}

	@Test
	public void getStringField_WhenCellIsNull_ThenReturnEmptyString() throws Exception {
		when(mockRow.getCell(0)).thenReturn(null);

		String result = modelFactoryBuilder.getStringField(mockRow, 0);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getEmptyRowImportModel_WhenNoError_ThenReturnsRowImportModel() {
		RowImportModel result = modelFactoryBuilder.getEmptyRowImportModel(INDEX, GROUP_ID);

		assertThat(result.getIndex(), equalTo(INDEX + 1));
		assertThat(result.getGroupId(), equalTo(GROUP_ID));
		assertThat(result.getTitle(), equalTo(null));
		assertThat(result.getUrl(), equalTo(null));
		assertThat(result.getCategoriesNames(), equalTo(null));
	}

}
