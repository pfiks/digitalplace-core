package com.placecube.digitalplace.navigation.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.theme.NavItem;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;
import com.placecube.digitalplace.navigation.configuration.NavigationPortletInstanceConfiguration;

@Component(immediate = true, service = NavigationService.class)
public class NavigationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private Portal portal;

	@Reference
	private SiteNavigationMenuLocalService siteNavigationMenuLocalService;

	public Set<Group> getAvailableGroupsForWidgetTemplateRetrieval(ThemeDisplay themeDisplay) throws PortalException {
		Group scopeGroup = themeDisplay.getScopeGroup();
		long companyId = themeDisplay.getCompanyId();

		Set<Group> results = new HashSet<>();
		results.add(scopeGroup);
		if (!scopeGroup.isGuest()) {
			results.add(groupLocalService.getGroup(companyId, GroupConstants.GUEST));
		}
		results.add(groupLocalService.getCompanyGroup(companyId));
		return results;
	}

	public Set<Group> getGroupsForNavigationMenuRetrieval(Set<Group> groups) {
		return groups.stream().filter(group -> !group.isCompany()).collect(Collectors.toSet());
	}

	public Optional<SiteNavigationMenu> getSiteNavigationMenu(NavigationPortletInstanceConfiguration configuration) {
		long navigationMenuId = configuration.navigationMenuId();
		if (navigationMenuId > 0) {
			return Optional.ofNullable(siteNavigationMenuLocalService.fetchSiteNavigationMenu(navigationMenuId));
		} else {
			long groupId = configuration.navigationMenuGroupId();
			return Optional.ofNullable(siteNavigationMenuLocalService.fetchSiteNavigationMenu(groupId, configuration.navigationType()));
		}
	}

	public Optional<NavigationPortletInstanceConfiguration> getValidPortletConfiguration(RenderRequest renderRequest, ThemeDisplay themeDisplay) throws ConfigurationException {
		String portletId = portal.getPortletId(renderRequest);

		NavigationPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(NavigationPortletInstanceConfiguration.class, themeDisplay.getLayout(), portletId);

		return isValidConfiguration(configuration) ? Optional.of(configuration) : Optional.empty();
	}

	public Optional<DDMTemplate> getWidgetTemplate(NavigationPortletInstanceConfiguration configuration) {
		long templateGroupId = configuration.widgetTemplateGroupId();

		return Optional.ofNullable(ddmTemplateLocalService.fetchTemplate(templateGroupId, portal.getClassNameId(NavItem.class), configuration.widgetTemplateKey()));
	}

	private boolean isValidConfiguration(NavigationPortletInstanceConfiguration configuration) {
		boolean validMenu = configuration.navigationType() > 0 || configuration.navigationMenuId() > 0;
		return validMenu && Validator.isNotNull(configuration.widgetTemplateKey()) && configuration.widgetTemplateGroupId() > 0 && configuration.navigationMenuGroupId() > 0;
	}

}
