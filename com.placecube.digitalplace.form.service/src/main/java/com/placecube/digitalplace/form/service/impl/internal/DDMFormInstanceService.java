package com.placecube.digitalplace.form.service.impl.internal;

import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;

@Component(immediate = true, service = DDMFormInstanceService.class)
public class DDMFormInstanceService {

	private static final String VERSION_DEFAULT = "1.0";

	@Reference
	private CounterLocalService counterLocalService;

	@Reference
	private DDMFormInstanceLocalService ddmFormInstanceLocalService;

	@Reference
	private DDMFormInstanceVersionService ddmFormInstanceVersionService;

	@Reference
	private FormDefinitionDeserializerService formDefinitionDeserializerService;

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private WorkflowDefinitionService workflowDefinitionService;

	public DDMFormInstance addFormInstance(long userId, long groupId, DDMStructure ddmStructure, Map<Locale, String> nameMap, Map<Locale, String> descriptionMap, DDMFormValues settingsDDMFormValues,
			ServiceContext serviceContext) throws PortalException {

		Locale defaultLocale = ddmStructure.getDDMForm().getDefaultLocale();

		User user = userLocalService.getUser(userId);

		long ddmFormInstanceId = counterLocalService.increment();

		DDMFormInstance ddmFormInstance = ddmFormInstanceLocalService.createDDMFormInstance(ddmFormInstanceId);

		ddmFormInstance.setUuid(serviceContext.getUuid());
		ddmFormInstance.setGroupId(groupId);
		ddmFormInstance.setCompanyId(user.getCompanyId());
		ddmFormInstance.setUserId(user.getUserId());
		ddmFormInstance.setUserName(user.getFullName());
		ddmFormInstance.setStructureId(ddmStructure.getStructureId());
		ddmFormInstance.setVersion(VERSION_DEFAULT);
		ddmFormInstance.setNameMap(nameMap, defaultLocale);
		ddmFormInstance.setDescriptionMap(descriptionMap, defaultLocale);
		ddmFormInstance.setSettings(formDefinitionDeserializerService.serialize(settingsDDMFormValues));

		DDMFormInstance updatedDDMFormInstance = ddmFormInstanceLocalService.updateDDMFormInstance(ddmFormInstance);

		workflowDefinitionService.updateWorkflowDefinitionLink(ddmFormInstance, settingsDDMFormValues, serviceContext);

		if (serviceContext.isAddGroupPermissions() || serviceContext.isAddGuestPermissions()) {

			ddmFormInstanceLocalService.addFormInstanceResources(ddmFormInstance, serviceContext.isAddGroupPermissions(), serviceContext.isAddGuestPermissions());
		} else {
			ddmFormInstanceLocalService.addFormInstanceResources(ddmFormInstance, serviceContext.getModelPermissions());
		}

		ddmFormInstanceVersionService.addFormInstanceVersion(ddmStructure, user, ddmFormInstance, VERSION_DEFAULT, serviceContext);

		return updatedDDMFormInstance;
	}

}
