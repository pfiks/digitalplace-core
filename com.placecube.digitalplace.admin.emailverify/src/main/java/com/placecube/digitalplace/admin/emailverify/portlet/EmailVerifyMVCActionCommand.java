package com.placecube.digitalplace.admin.emailverify.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.placecube.digitalplace.admin.emailverify.constants.EmailVerifyMVCCommandKeys;
import com.placecube.digitalplace.admin.emailverify.constants.EmailVerifyPortletKeys;
import com.placecube.digitalplace.admin.emailverify.service.EmailVerifyTicketService;

@Component(property = { "javax.portlet.name=" + EmailVerifyPortletKeys.ADMIN_EMAIL_VERIFY, "mvc.command.name=" + EmailVerifyMVCCommandKeys.VERIFY }, service = MVCActionCommand.class)
public class EmailVerifyMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private EmailVerifyTicketService emailVerifyTicketService;

	@Reference
	private Portal portal;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long selectedUserId = ParamUtil.getLong(actionRequest, "selectedUserId");
		long companyId = portal.getCompanyId(actionRequest);

		emailVerifyTicketService.verifyUserEmailAddress(companyId, selectedUserId);

		LiferayPortletURL usersAdminURL = portal.getLiferayPortletResponse(actionResponse).createLiferayPortletURL(PortletKeys.USERS_ADMIN, PortletRequest.RENDER_PHASE);
		actionResponse.sendRedirect(usersAdminURL.toString());
	}

}
