package com.placecube.digitalplace.group.rest.application.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.UserLocalService;

@Component(immediate = true, service = GroupRestRetrievalService.class)
public class GroupRestRetrievalService {

	private static final Log LOG = LogFactoryUtil.getLog(GroupRestRetrievalService.class);

	@Reference
	private UserLocalService userLocalService;

	public boolean isGuestUser(long userId) {
		try {
			return userLocalService.getUser(userId).isGuestUser();
		} catch (Exception e) {
			LOG.debug("Unable to retrieve user by userId: " + userId + " - " + e.getMessage());
		}
		return true;
	}
}
