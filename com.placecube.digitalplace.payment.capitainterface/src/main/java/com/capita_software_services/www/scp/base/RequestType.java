/**
 * RequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class RequestType implements java.io.Serializable {
    private org.apache.axis.types.Token _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected RequestType(org.apache.axis.types.Token value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final org.apache.axis.types.Token _payOnly = new org.apache.axis.types.Token("payOnly");
    public static final org.apache.axis.types.Token _authoriseOnly = new org.apache.axis.types.Token("authoriseOnly");
    public static final org.apache.axis.types.Token _storeOnly = new org.apache.axis.types.Token("storeOnly");
    public static final org.apache.axis.types.Token _payAndStore = new org.apache.axis.types.Token("payAndStore");
    public static final org.apache.axis.types.Token _payAndAutoStore = new org.apache.axis.types.Token("payAndAutoStore");
    public static final RequestType payOnly = new RequestType(_payOnly);
    public static final RequestType authoriseOnly = new RequestType(_authoriseOnly);
    public static final RequestType storeOnly = new RequestType(_storeOnly);
    public static final RequestType payAndStore = new RequestType(_payAndStore);
    public static final RequestType payAndAutoStore = new RequestType(_payAndAutoStore);
    public org.apache.axis.types.Token getValue() { return _value_;}
    public static RequestType fromValue(org.apache.axis.types.Token value)
          throws java.lang.IllegalArgumentException {
        RequestType enumeration = (RequestType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static RequestType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        try {
            return fromValue(new org.apache.axis.types.Token(value));
        } catch (Exception e) {
            throw new java.lang.IllegalArgumentException();
        }
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_.toString();}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "requestType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
