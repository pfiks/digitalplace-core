package com.placecube.digitalplace.editor.styles.contributor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.editor.configuration.CKEditorConfigurationService;
import com.placecube.digitalplace.editor.configuration.constants.CKEditorCommonToolbarConstants;
import com.placecube.digitalplace.editor.configuration.service.CKEditorCommonToolbarHelper;
import com.placecube.digitalplace.editor.configuration.service.CKEditorCommonToolbarService;
import com.placecube.digitalplace.editor.styles.contributor.constants.ButtonStyle;
import com.placecube.digitalplace.editor.styles.contributor.service.CKEditorStyleService;

@RunWith(PowerMockRunner.class)
public class CKEditorStylesConfigContributorTest extends PowerMockito {

	public static final String BUTTONS = "buttons";

	public static final long COMPANY_ID = 101;

	public static final String STYLES = "Styles";

	@Mock
	private CKEditorConfigurationService mockCKEditorConfigurationService;

	@InjectMocks
	private CKEditorStylesConfigContributor ckEditorStyleConfigContributor;

	@Mock
	private JSONObject jsonObjectBaseStyle;

	@Mock
	private CKEditorCommonToolbarHelper mockCKEditorCommonToolbarHelper;

	@Mock
	private CKEditorCommonToolbarService mockCKEditorCommonToolbarService;

	@Mock
	private CKEditorStyleService mockCKEditorStyleService;

	@Mock
	private Map<String, Object> mockInputEditorTaglibAttributes;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private JSONObject mockPluginAndRolesConfig;

	@Mock
	private RequestBackedPortletURLFactory mockRequestBackedPortletURLFactory;

	@Mock
	private JSONArray mockStyleJSONArray;

	@Mock
	private JSONObject mockStyleJsonObject;

	@Mock
	private JSONArray mockTextButtonStyles;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void populateConfigJSONObject_WhenNoError_ThenSetsDisableNativeSpellCheckerToFalse() {

		when(mockCKEditorStyleService.getStyleSet(mockJsonObject)).thenReturn(Optional.of(mockTextButtonStyles));

		ckEditorStyleConfigContributor.populateConfigJSONObject(mockJsonObject, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockJsonObject, times(1)).put("disableNativeSpellChecker", Boolean.FALSE);
	}

	@Test
	public void populateConfigJSONObject_WhenStylePluginIsEmpty_ThenAddAllButtonStyle() {

		Set<String> userRoleNames = new HashSet<>();
		userRoleNames.add("roleName1");
		userRoleNames.add("roleName2");

		Optional<JSONArray> textButtonStyles = Optional.of(mockTextButtonStyles);

		Set<String> allButtons = Stream.of(ButtonStyle.values()).map(ButtonStyle::name).collect(Collectors.toSet());

		Set<String> btnList = new HashSet<>();

		btnList.addAll(allButtons);

		when(mockCKEditorStyleService.getStyleSet(mockJsonObject)).thenReturn(textButtonStyles);

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockCKEditorConfigurationService.getPluginAndRolesJsonConfiguration(COMPANY_ID)).thenReturn(Optional.of(mockPluginAndRolesConfig));

		when(mockCKEditorCommonToolbarService.getUserRoleName(mockThemeDisplay)).thenReturn(userRoleNames);

		when(mockCKEditorCommonToolbarHelper.getStylePluginJsonArray(Optional.of(mockPluginAndRolesConfig))).thenReturn(Optional.empty());

		when(mockCKEditorStyleService.createStyle(ButtonStyle.valueOf("BASE"))).thenReturn(jsonObjectBaseStyle);

		ckEditorStyleConfigContributor.populateConfigJSONObject(mockJsonObject, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(textButtonStyles.get(), times(1)).put(mockCKEditorStyleService.createStyle(ButtonStyle.valueOf("BASE")));
	}

	@Test
	public void populateConfigJSONObject_WhenStylesNotRetrieved_ThenNotAddCustomStyles() {

		when(mockCKEditorStyleService.getStyleSet(mockJsonObject)).thenReturn(Optional.empty());

		ckEditorStyleConfigContributor.populateConfigJSONObject(mockJsonObject, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(mockCKEditorStyleService, never()).createStyle(any());
	}

	@Test
	public void populateConfigJSONObject_WhenUserHasPermissionButThereIsButton_ThenButtonIsAdded() {

		Set<String> allButtons = Stream.of(ButtonStyle.values()).map(ButtonStyle::name).collect(Collectors.toSet());
		Set<String> btnList = new HashSet<>();

		String buttons = "BASE,DANGER";

		Optional<JSONArray> textButtonStyles = mockCreateStyle();

		btnList.addAll(Arrays.asList(buttons.split(StringPool.COMMA)).stream().filter(allButtons::contains).collect(Collectors.toList()));

		when(mockCKEditorStyleService.createStyle(ButtonStyle.valueOf("BASE"))).thenReturn(jsonObjectBaseStyle);

		ckEditorStyleConfigContributor.populateConfigJSONObject(mockJsonObject, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(textButtonStyles.get(), times(1)).put(mockCKEditorStyleService.createStyle(ButtonStyle.valueOf("BASE")));
	}

	@Test
	public void populateConfigJSONObject_WhenUserHasPermissionButThereIsNoButton_ThenAllButtonsAreAded() {

		Set<String> allButtons = Stream.of(ButtonStyle.values()).map(ButtonStyle::name).collect(Collectors.toSet());
		Set<String> btnList = new HashSet<>();

		Optional<JSONArray> textButtonStyles = mockCreateStyle();

		btnList.addAll(allButtons);

		when(mockCKEditorStyleService.createStyle(ButtonStyle.valueOf("BASE"))).thenReturn(jsonObjectBaseStyle);

		ckEditorStyleConfigContributor.populateConfigJSONObject(mockJsonObject, mockInputEditorTaglibAttributes, mockThemeDisplay, mockRequestBackedPortletURLFactory);

		verify(textButtonStyles.get(), times(1)).put(mockCKEditorStyleService.createStyle(ButtonStyle.valueOf("BASE")));
	}

	private Optional<JSONArray> mockCreateStyle() {

		Set<String> userRoleNames = new HashSet<>();
		userRoleNames.add("roleName1");
		userRoleNames.add("roleName2");

		String roleName = "roleName1,roleName2";
		String buttons = "BASE,DANGER";

		Optional<JSONArray> textButtonStyles = Optional.of(mockTextButtonStyles);
		Optional<JSONObject> pluginAndRolesConfig = Optional.of(mockPluginAndRolesConfig);
		Optional<JSONArray> styleJSONArray = Optional.of(mockStyleJSONArray);

		when(mockCKEditorStyleService.getStyleSet(mockJsonObject)).thenReturn(textButtonStyles);

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		when(mockCKEditorConfigurationService.getPluginAndRolesJsonConfiguration(COMPANY_ID)).thenReturn(pluginAndRolesConfig);

		when(mockCKEditorCommonToolbarService.getUserRoleName(mockThemeDisplay)).thenReturn(userRoleNames);

		when(mockCKEditorCommonToolbarHelper.getStylePluginJsonArray(pluginAndRolesConfig)).thenReturn(styleJSONArray);

		when(styleJSONArray.get().length()).thenReturn(1);

		when(styleJSONArray.get().getJSONObject(0)).thenReturn(mockStyleJsonObject);

		when(mockStyleJsonObject.getString(CKEditorCommonToolbarConstants.BUTTONS)).thenReturn(buttons);

		when(mockStyleJsonObject.getString(CKEditorCommonToolbarConstants.ROLES)).thenReturn(roleName);

		when(mockCKEditorCommonToolbarHelper.hasUserRole(userRoleNames, roleName)).thenReturn(true);

		return textButtonStyles;
	}

}
