package com.placecube.digitalplace.externalpage.search.registry;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.liferay.osgi.service.tracker.collections.internal.ServiceTrackerUtil;
import com.liferay.portal.kernel.module.util.SystemBundleUtil;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, SystemBundleUtil.class, ServiceTrackerUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.module.util.SystemBundleUtil" })
public class ExternalPageSearcherTest extends PowerMockito {

	private ExternalPageSearcher externalPageSearcher;

	@Test
	public void getClassName_ThenReturnsExternalPageClassName() {
		String result = externalPageSearcher.getClassName();

		assertThat(result, equalTo(ExternalPage.class.getName()));
	}

	@Test
	public void newExternalPageSearcherInstance_SetsDefaultSelectedFieldNames() {
		String[] expected = (String[]) Whitebox.getInternalState(externalPageSearcher, "_defaultSelectedFieldNames");

		assertThat(Arrays.asList(expected), contains(Field.ENTRY_CLASS_PK, Field.ENTRY_CLASS_NAME, Field.COMPANY_ID, Field.CONTENT, Field.GROUP_ID, Field.TITLE, Field.UID, Field.URL));
	}

	@Test
	public void newExternalPageSearcherInstance_SetsFilterSearchToFalse() {
		boolean result = externalPageSearcher.isFilterSearch();

		assertFalse(result);
	}

	@Test
	public void newExternalPageSearcherInstance_SetsPermissionAwareToFalse() {
		boolean result = externalPageSearcher.isPermissionAware();

		assertFalse(result);
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, SystemBundleUtil.class, ServiceTrackerUtil.class);

		externalPageSearcher = new ExternalPageSearcher();
	}

}
