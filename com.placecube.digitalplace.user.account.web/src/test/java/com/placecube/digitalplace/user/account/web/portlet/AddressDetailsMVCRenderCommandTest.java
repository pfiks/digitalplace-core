package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

public class AddressDetailsMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private AddressDetailsMVCRenderCommand addressDetailsMVCRenderCommand;

	private final long COMPANY_ID = 1L;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private UserAccountFieldRegistry mockUserAccountFieldRegistry;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Test
	public void render_WhenNoError_ThenReturnsAddressDetailsJSP() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		String result = addressDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/address-details.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetAccountFieldsAsRequestAttribute() throws PortletException, UserAccountException {
		List<UserAccountField> fields = new ArrayList<>();
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserAccountFieldRegistry.getUserAccountFieldsByStep(AccountCreationStep.ADDRESS_DETAILS, COMPANY_ID)).thenReturn(fields);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		addressDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_FIELDS, fields);
	}

	@Test
	public void render_WhenNoError_ThenSetAddressRequiredAsRequestAttribute() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.addressRequired()).thenReturn(true);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		addressDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("addressRequired", true);
	}

	@Test
	public void render_WhenNoError_ThenSetsCreateAccountContextAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		addressDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = inOrder(mockAccountContextService, mockRenderRequest);
		inOrder.verify(mockAccountContextService, times(1)).configureAddressDetails(mockAccountContext, mockRenderRequest);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_CONTEXT, mockAccountContext);
	}

	@Test
	public void render_WhenNoError_ThenSetsIsAddressOutOfLocalAreadEnabledAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(true);

		addressDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.IS_ADDRESS_OUT_OF_LOCAL_AREA_ENABLED, true);
	}

	@Test
	public void render_WhenNoError_ThenUpdateContextInSession() throws UserAccountException, PortletException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(true);

		addressDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockRenderRequest);
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

}
