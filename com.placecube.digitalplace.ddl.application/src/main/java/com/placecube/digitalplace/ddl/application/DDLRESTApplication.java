package com.placecube.digitalplace.ddl.application;

import java.util.Collections;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.placecube.digitalplace.ddl.service.DDLRequestService;

@Component(immediate = true, property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/ddl/services", JaxrsWhiteboardConstants.JAX_RS_NAME + "=DDL.Rest", "oauth2.scopechecker.type=none",
		"auth.verifier.guest.allowed=false", "liferay.access.control.disable=true" }, service = Application.class)
public class DDLRESTApplication extends Application {

	@Reference
	private DDLRequestService ddlRequestService;

	@GET
	@Path("/addLinkedUser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addLinkedUser(@QueryParam("ddlRecordId") Long ddlRecordId, @QueryParam("fieldName") String fieldName, @QueryParam("emailAddress") String emailAddress,
			@Context HttpServletRequest request) throws Exception {

		String response = ddlRequestService.addLinkedUser(getCompanyId(request), ddlRecordId, fieldName, emailAddress);

		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}

	@GET
	@Path("/getDDLRecordValues")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDDLRecordValues(@QueryParam("ddlRecordId") Long ddlRecordId, @Context HttpServletRequest request) throws Exception {

		String response = ddlRequestService.getDDLRecordValues(ddlRecordId, request.getLocale());

		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}

	@GET
	@Path("/getLinkedUsers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLinkedUsers(@QueryParam("ddlRecordId") Long ddlRecordId, @QueryParam("fieldName") String fieldName, @Context HttpServletRequest request) throws Exception {

		String response = ddlRequestService.getLinkedUsers(getCompanyId(request), ddlRecordId, fieldName, request.getLocale());

		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}

	@GET
	@Path("/removeLinkedUser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeLinkedUser(@QueryParam("ddlRecordId") Long ddlRecordId, @QueryParam("fieldName") String fieldName, @QueryParam("emailAddress") String emailAddress,
			@Context HttpServletRequest request) throws Exception {

		String response = ddlRequestService.removeLinkedUser(getCompanyId(request), ddlRecordId, fieldName, emailAddress);

		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}

	private long getCompanyId(HttpServletRequest request) {
		return (Long) request.getAttribute("COMPANY_ID");
	}
}