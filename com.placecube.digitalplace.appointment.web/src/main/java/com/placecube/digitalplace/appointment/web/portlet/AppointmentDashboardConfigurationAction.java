package com.placecube.digitalplace.appointment.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.object.service.ObjectDefinitionLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.web.constants.ConfigurationConstants;
import com.placecube.digitalplace.appointment.web.constants.PortletKeys;
import com.placecube.digitalplace.appointment.web.constants.PortletPreferenceKeys;
import com.placecube.digitalplace.appointmentweb.service.AppointmentsDashboardUtil;

@Component(immediate = true, configurationPid = ConfigurationConstants.CONFIG_PID, configurationPolicy = ConfigurationPolicy.OPTIONAL, property = "javax.portlet.name="
		+ PortletKeys.APPOINTMENTS_DASHBOARD, service = ConfigurationAction.class)
public class AppointmentDashboardConfigurationAction extends DefaultConfigurationAction {

	private static final int APPROVED = 0;

	@Reference
	private AppointmentsDashboardUtil appointmentsDashboardUtil;

	@Reference
	private LayoutPageTemplateEntryLocalService layoutPageTemplateEntryLocalService;

	@Reference
	private ObjectDefinitionLocalService objectDefinitionLocalService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {

		PortletRequest portletRequest = (PortletRequest) httpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST);
		PortletPreferences preferences = portletRequest.getPreferences();
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId = themeDisplay.getCompanyId();

		String serviceId = preferences.getValue(PortletPreferenceKeys.SERVICE_ID, StringPool.BLANK);
		String configuredDPT = preferences.getValue(PortletPreferenceKeys.DISPLAY_PAGE_TEMPLATE_ENTRY_ID, StringPool.BLANK);

		String configuredObjectDefinitionId = preferences.getValue(PortletPreferenceKeys.OBJECT_DEFINTION_ID, StringPool.BLANK);
		String configuredIdColumnLabel = preferences.getValue(PortletPreferenceKeys.ID_COLUMN_LABEL, StringPool.BLANK);
		String configuredFilterColumnLabel = preferences.getValue(PortletPreferenceKeys.FILTER_COLUMN_LABEL, StringPool.BLANK);
		String configuredSummaryLabel = preferences.getValue(PortletPreferenceKeys.SUMMARY_COLUMN_LABEL, StringPool.BLANK);
		String configuredStartDateColumnLabel = preferences.getValue(PortletPreferenceKeys.STARTDATE_COLUMN_LABEL, StringPool.BLANK);
		String configuredODataFilter = preferences.getValue(PortletPreferenceKeys.ODATA_FILTER, StringPool.BLANK);
		String configuredSummaryFieldExpression = preferences.getValue(PortletPreferenceKeys.SUMMARY_FIELD_EXPRESSION, StringPool.BLANK);

		httpServletRequest.setAttribute("configuredServiceId", serviceId);
		httpServletRequest.setAttribute("configuredDPT", configuredDPT);
		httpServletRequest.setAttribute("configuredObjectDefinitionId", configuredObjectDefinitionId);
		httpServletRequest.setAttribute("configuredIdColumnLabel", configuredIdColumnLabel);
		httpServletRequest.setAttribute("configuredFilterColumnLabel", configuredFilterColumnLabel);
		httpServletRequest.setAttribute("configuredSummaryColumnLabel", configuredSummaryLabel);
		httpServletRequest.setAttribute("configuredStartDateColumnLabel", configuredStartDateColumnLabel);
		httpServletRequest.setAttribute("configuredODataFilter", configuredODataFilter);
		httpServletRequest.setAttribute("configuredSummaryFieldExpression", configuredSummaryFieldExpression);
		httpServletRequest.setAttribute("availableServiceIds", appointmentsDashboardUtil.getAvailableServiceIds(companyId));
		httpServletRequest.setAttribute("availableDPTs", layoutPageTemplateEntryLocalService.getLayoutPageTemplateEntries(themeDisplay.getScopeGroupId()));
		httpServletRequest.setAttribute("availableObjectDefinitionIds", objectDefinitionLocalService.getObjectDefinitions(companyId, APPROVED));

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		String serviceId = ParamUtil.getString(actionRequest, PortletPreferenceKeys.SERVICE_ID);
		String dpTemplateEntryId = ParamUtil.getString(actionRequest, PortletPreferenceKeys.DISPLAY_PAGE_TEMPLATE_ENTRY_ID);
		String configuredObjectDefinitionId = ParamUtil.getString(actionRequest, PortletPreferenceKeys.OBJECT_DEFINTION_ID);
		String configuredIdColumnLabel = ParamUtil.getString(actionRequest, PortletPreferenceKeys.ID_COLUMN_LABEL);
		String configuredFilterColumnLabel = ParamUtil.getString(actionRequest, PortletPreferenceKeys.FILTER_COLUMN_LABEL);
		String configuredSummaryLabel = ParamUtil.getString(actionRequest, PortletPreferenceKeys.SUMMARY_COLUMN_LABEL);
		String configuredStartDateColumnLabel = ParamUtil.getString(actionRequest, PortletPreferenceKeys.STARTDATE_COLUMN_LABEL);
		String configuredODataFilter = ParamUtil.getString(actionRequest, PortletPreferenceKeys.ODATA_FILTER);
		String configuredSummaryFieldExpression = ParamUtil.getString(actionRequest, PortletPreferenceKeys.SUMMARY_FIELD_EXPRESSION);

		setPreference(actionRequest, PortletPreferenceKeys.SERVICE_ID, serviceId);
		setPreference(actionRequest, PortletPreferenceKeys.DISPLAY_PAGE_TEMPLATE_ENTRY_ID, dpTemplateEntryId);
		setPreference(actionRequest, PortletPreferenceKeys.OBJECT_DEFINTION_ID, configuredObjectDefinitionId);
		setPreference(actionRequest, PortletPreferenceKeys.ID_COLUMN_LABEL, configuredIdColumnLabel);
		setPreference(actionRequest, PortletPreferenceKeys.FILTER_COLUMN_LABEL, configuredFilterColumnLabel);
		setPreference(actionRequest, PortletPreferenceKeys.SUMMARY_COLUMN_LABEL, configuredSummaryLabel);
		setPreference(actionRequest, PortletPreferenceKeys.STARTDATE_COLUMN_LABEL, configuredStartDateColumnLabel);
		setPreference(actionRequest, PortletPreferenceKeys.ODATA_FILTER, configuredODataFilter);
		setPreference(actionRequest, PortletPreferenceKeys.SUMMARY_FIELD_EXPRESSION, configuredSummaryFieldExpression);

		super.processAction(portletConfig, actionRequest, actionResponse);

	}
}
