package com.placecube.digitalplace.fileupload.liferaydl.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.fileupload.liferaydl.configuration.LiferayDLConnectorConfiguration", localization = "content/Language", name = "fileupload-liferaydl-connector")
public interface LiferayDLConnectorConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean isEnabled();

	@Meta.AD(required = false, deflt = "true", name = "default")
	boolean isDefault();
}
