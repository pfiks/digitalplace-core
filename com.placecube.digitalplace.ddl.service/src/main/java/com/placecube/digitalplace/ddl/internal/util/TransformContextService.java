package com.placecube.digitalplace.ddl.internal.util;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.lists.constants.DDLConstants;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalService;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.comment.CommentManager;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateHandler;
import com.liferay.portal.kernel.template.TemplateHandlerRegistryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.ddl.internal.constants.TemplateKeys;

@Component(immediate = true, service = TransformContextService.class)
public class TransformContextService {

	private static final Log LOG = LogFactoryUtil.getLog(TransformContextService.class);

	private static final String VIEWMODE = "viewMode";

	@Reference
	private CommentManager commentManager;

	@Reference
	private DDLRecordSetLocalService ddlRecordSetLocalService;

	@Reference
	private Portal portal;

	@Reference
	private TemplateUtil templateUtil;

	public Map<String, Object> getContextObjects(DDMTemplate ddmTemplate, DDLRecord ddlRecord, long structureId, ThemeDisplay themeDisplay, HttpServletRequest httpServletRequest) {

		Map<String, Object> contextObjects = templateUtil.getContextObjectsInstance();

		if (ddmTemplate == null) {
			return contextObjects;
		}

		try {
			Locale locale = themeDisplay.getLocale();
			long recordSetId = ddlRecord.getRecordSetId();
			DDLRecordSet recordSet = ddlRecordSetLocalService.getRecordSet(recordSetId);
			contextObjects.put(DDLConstants.RESERVED_RECORD_SET_DESCRIPTION, recordSet.getDescription(locale));
			contextObjects.put(DDLConstants.RESERVED_RECORD_SET_ID, recordSetId);
			contextObjects.put(DDLConstants.RESERVED_RECORD_SET_NAME, recordSet.getName(locale));
		} catch (PortalException e) {
			LOG.debug(e.getMessage(), e);
			LOG.error(e.getMessage());
		}

		contextObjects.put(TemplateKeys.CUR_RECORD, ddlRecord);

		contextObjects.putAll(getDefaultContextObjects(ddmTemplate, structureId, httpServletRequest));

		TemplateHandler templateHandler = TemplateHandlerRegistryUtil.getTemplateHandler(DDLRecordSet.class.getName());
		contextObjects.putAll(templateHandler.getCustomContextObjects());

		return contextObjects;
	}

	public Map<String, Object> getContextObjects(DDMTemplate ddmTemplate, List<AssetEntry> assetEntries, long structureId, RenderRequest renderRequest) {
		Map<String, Object> contextObjects = templateUtil.getContextObjectsInstance();
		if (ddmTemplate == null) {
			return contextObjects;
		}

		contextObjects.put("renderRequest", renderRequest);
		contextObjects.put(TemplateKeys.ENTRIES, assetEntries);

		contextObjects.putAll(getDefaultContextObjects(ddmTemplate, structureId, portal.getHttpServletRequest(renderRequest)));

		return contextObjects;

	}

	private Map<String, Object> getDefaultContextObjects(DDMTemplate ddmTemplate, long structureId, HttpServletRequest httpServletRequest) {
		// Standard Liferay reserved keys
		long templateId = ddmTemplate.getTemplateId();
		Map<String, Object> contextObjects = templateUtil.getContextObjectsInstance();
		contextObjects.put(DDLConstants.RESERVED_DDM_STRUCTURE_ID, structureId);
		contextObjects.put(DDLConstants.RESERVED_DDM_TEMPLATE_ID, templateId);
		contextObjects.put(TemplateConstants.TEMPLATE_ID, templateId);

		// Placecube custom reserved keys
		contextObjects.put("commentManager", commentManager);
		contextObjects.put(VIEWMODE, ParamUtil.getString(httpServletRequest, VIEWMODE, Constants.VIEW));
		contextObjects.put(TemplateConstants.CLASS_NAME_ID, ddmTemplate.getClassNameId());

		return contextObjects;
	}

}
