package com.placecube.digitalplace.search.rankings.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.search.engine.adapter.SearchEngineAdapter;
import com.liferay.portal.search.engine.adapter.search.SearchSearchRequest;
import com.liferay.portal.search.engine.adapter.search.SearchSearchResponse;
import com.liferay.portal.search.filter.ComplexQueryPart;
import com.liferay.portal.search.filter.ComplexQueryPartBuilder;
import com.liferay.portal.search.filter.ComplexQueryPartBuilderFactory;
import com.liferay.portal.search.query.IdsQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.query.TermsQuery;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.placecube.digitalplace.search.rankings.model.SearchRanking;
import com.placecube.digitalplace.search.rankings.util.SearchRequestFactory;

@RunWith(PowerMockRunner.class)
public class RankingsServiceTest {

	@Mock
	private ComplexQueryPart mockComplexQueryPart;

	@Mock
	private ComplexQueryPartBuilder mockComplexQueryPartBuilder;

	@Mock
	private ComplexQueryPartBuilderFactory mockComplexQueryPartBuilderFactory;

	@Mock
	private IdsQuery mockIdsQuery;

	@Mock
	private Queries mockQueries;

	@Mock
	private SearchEngineAdapter mockSearchEngineAdapter;

	@Mock
	private SearchRanking mockSearchRanking;

	@Mock
	private SearchRequestBuilder mockSearchRequestBuilder;

	@Mock
	private SearchRequestFactory mockSearchRequestFactory;

	@Mock
	private SearchSearchRequest mockSearchSearchRequest;

	@Mock
	private TermsQuery mockTermsQuery;

	@InjectMocks
	private RankingsService rankingsService;

	@Mock
	private SearchSearchResponse searchSearchResponse;

	@Test
	public void addBlockedIdsQuery_WhenIdListIsEmpty_ThenNoQueryIsAddedToSearchRequestBuilder() throws ConfigurationException {
		List<String> blockedIds = new ArrayList<>();
		when(mockSearchRanking.getBlockedIds()).thenReturn(blockedIds);

		rankingsService.addBlockedIdsQuery(mockSearchRequestBuilder, mockSearchRanking);

		verify(mockSearchRequestBuilder, never()).addComplexQueryPart(mockComplexQueryPart);
	}

	@Test
	public void addBlockedIdsQuery_WhenIdListIsProvided_ThenAddsIdsToQueryAndAddsQueryToSearchRequestBuilder() throws ConfigurationException {
		List<String> blockedIds = new ArrayList<>();
		blockedIds.add("blocked1");
		blockedIds.add("blocked2");
		when(mockSearchRanking.getBlockedIds()).thenReturn(blockedIds);
		when(mockQueries.ids()).thenReturn(mockIdsQuery);
		when(mockComplexQueryPartBuilderFactory.builder()).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.query(mockIdsQuery)).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.additive(true)).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.occur("must_not")).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.build()).thenReturn(mockComplexQueryPart);

		rankingsService.addBlockedIdsQuery(mockSearchRequestBuilder, mockSearchRanking);

		InOrder inOrder = Mockito.inOrder(mockIdsQuery, mockSearchRequestBuilder);
		inOrder.verify(mockIdsQuery, times(1)).addIds(ArrayUtil.toStringArray(blockedIds));
		inOrder.verify(mockSearchRequestBuilder, times(1)).addComplexQueryPart(mockComplexQueryPart);
	}

	@Test
	public void addPinnedIdsQueries_WhenIdListIsEmpty_ThenNoQueryIsAddedToSearchRequestBuilder() throws ConfigurationException {
		List<Map<String, String>> pinValues = new ArrayList<>();
		when(mockSearchRanking.getPinnedIds()).thenReturn(pinValues);

		rankingsService.addPinnedIdsQueries(mockSearchRequestBuilder, pinValues);

		verifyNoMoreInteractions(mockSearchRequestBuilder);
	}

	@Test
	public void addPinnedIdsQueries_WhenIdListNotEmpty_ThenIdAndBoostSetOnQueryAndAddedToSearchRequestBuilder() throws ConfigurationException {

		Map<String, String> pinnedItem = new HashMap<>();
		pinnedItem.put("position", "1");
		pinnedItem.put("uid", "pin-uid");

		List<Map<String, String>> pinValues = new ArrayList<>();
		pinValues.add(pinnedItem);

		when(mockSearchRanking.getPinnedIds()).thenReturn(pinValues);
		when(mockQueries.ids()).thenReturn(mockIdsQuery);
		when(mockComplexQueryPartBuilderFactory.builder()).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.query(mockIdsQuery)).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.additive(true)).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.occur("should")).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.build()).thenReturn(mockComplexQueryPart);

		rankingsService.addPinnedIdsQueries(mockSearchRequestBuilder, pinValues);

		InOrder inOrder = Mockito.inOrder(mockIdsQuery, mockSearchRequestBuilder);
		inOrder.verify(mockIdsQuery, times(1)).addIds("pin-uid");
		inOrder.verify(mockIdsQuery, times(1)).setBoost((pinValues.size() - 1) * 10000F);
		inOrder.verify(mockSearchRequestBuilder, times(1)).addComplexQueryPart(mockComplexQueryPart);
	}

	@Test
	public void addPinnedIdsQueries_WhenMapInIdListIsEmpty_ThenThrowsNullPointerException() throws ConfigurationException {

		Map<String, String> pinnedItem = new HashMap<>();
		List<Map<String, String>> pinValues = new ArrayList<>();
		pinValues.add(pinnedItem);

		when(mockSearchRanking.getPinnedIds()).thenReturn(pinValues);
		when(mockQueries.ids()).thenReturn(mockIdsQuery);
		when(mockComplexQueryPartBuilderFactory.builder()).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.query(mockIdsQuery)).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.additive(true)).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.occur("should")).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.build()).thenReturn(mockComplexQueryPart);

		rankingsService.addPinnedIdsQueries(mockSearchRequestBuilder, pinValues);

		InOrder inOrder = Mockito.inOrder(mockIdsQuery, mockSearchRequestBuilder);
		inOrder.verify(mockIdsQuery, times(1)).addIds((String[]) null);
		inOrder.verify(mockIdsQuery, times(1)).setBoost((pinValues.size() - 0) * 10000F);
		inOrder.verify(mockSearchRequestBuilder, times(1)).addComplexQueryPart(mockComplexQueryPart);
	}

	@Test(expected = NullPointerException.class)
	public void addPinnedIdsQueries_WhenMapInIdListIsNull_ThenThrowsNullPointerException() throws ConfigurationException {

		Map<String, String> pinnedItem = null;
		List<Map<String, String>> pinValues = new ArrayList<>();
		pinValues.add(pinnedItem);

		when(mockSearchRanking.getPinnedIds()).thenReturn(pinValues);
		when(mockQueries.ids()).thenReturn(mockIdsQuery);
		when(mockComplexQueryPartBuilderFactory.builder()).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.query(mockIdsQuery)).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.additive(true)).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.occur("should")).thenReturn(mockComplexQueryPartBuilder);
		when(mockComplexQueryPartBuilder.build()).thenReturn(mockComplexQueryPart);

		rankingsService.addPinnedIdsQueries(mockSearchRequestBuilder, pinValues);
	}

	@Test
	public void getSearchRankingsForKeywords_WhenKeywordsIsEmpty_ThenQueryIsCreatedAndSearchIsExecuted() throws ConfigurationException {
		long companyId = 342l;
		String keywords = "";

		when(mockSearchRequestFactory.getSearchSearchRequest()).thenReturn(mockSearchSearchRequest);
		when(mockQueries.terms("queryStrings")).thenReturn(mockTermsQuery);

		rankingsService.getSearchRankingsForKeywords(keywords, companyId);

		verify(mockSearchEngineAdapter, times(1)).execute(mockSearchSearchRequest);
	}

	@Test(expected = NullPointerException.class)
	public void getSearchRankingsForKeywords_WhenKeywordsIsNull_ThenThrowsNullPointerException() throws ConfigurationException {
		long companyId = 342l;
		String keywords = null;

		when(mockQueries.terms("queryStrings")).thenReturn(mockTermsQuery);

		rankingsService.getSearchRankingsForKeywords(keywords, companyId);
	}

	@Test
	public void getSearchRankingsForKeywords_WhenKeywordsNotEmptyOrNull_ThenQueryIsCreatedAndSearchIsExecuted() throws ConfigurationException {
		long companyId = 342l;
		String keywords = "ranking-keywords";
		when(mockSearchRequestFactory.getSearchSearchRequest()).thenReturn(mockSearchSearchRequest);
		when(mockQueries.terms("queryStrings")).thenReturn(mockTermsQuery);

		rankingsService.getSearchRankingsForKeywords(keywords, companyId);

		InOrder inOrder = Mockito.inOrder(mockSearchSearchRequest, mockSearchEngineAdapter);
		inOrder.verify(mockSearchSearchRequest, times(1)).setQuery(mockTermsQuery);
		inOrder.verify(mockSearchSearchRequest, times(1)).setFetchSource(true);
		inOrder.verify(mockSearchSearchRequest, times(1)).setIndexNames("liferay-" + companyId + "-search-tuning-rankings");
		inOrder.verify(mockSearchEngineAdapter, times(1)).execute(mockSearchSearchRequest);

	}

	public void isAliasWildcard_WhenAliasContainsAsterisk_ThenReturnsTrue() {
		boolean result = rankingsService.isAliasWildcard("contains*");

		assertTrue(result);
	}

	public void isAliasWildcard_WhenAliasIsEmpty_ThenReturnsFalse() {
		boolean result = rankingsService.isAliasWildcard("");

		assertFalse(result);
	}

	@Test(expected = NullPointerException.class)
	public void isAliasWildcard_WhenAliasIsNull_ThenThrowsNullPointerException() {
		rankingsService.isAliasWildcard(null);
	}

	public void isAliasWildcard_WhenAliasNotContainsAsterisk_ThenReturnsFalse() {
		boolean result = rankingsService.isAliasWildcard("no-asterisk");

		assertFalse(result);
	}

}
