package com.placecube.digitalplace.user.settings.web.registry;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;
import com.placecube.digitalplace.user.settings.web.model.UserSettingsSection;

@Component(immediate = true, service = UserSettingsSectionRegistry.class)
public class UserSettingsSectionRegistry {

	private final static Comparator<UserSettingsSection> COMPARATOR_ON_ORDER_ASC = Comparator.comparingInt(UserSettingsSection::getDisplayOrder);

	private static final Log LOG = LogFactoryUtil.getLog(UserSettingsSectionRegistry.class);

	private Map<DisplaySection, List<UserSettingsSection>> userSettingsSections = new HashMap<>();

	public Map<DisplaySection, List<UserSettingsSection>> getUserSettingsSections(User user) {
		Map<DisplaySection, List<UserSettingsSection>> visibleUserSettingsSections = new HashMap<>();

		for(Map.Entry<DisplaySection, List<UserSettingsSection>> section : userSettingsSections.entrySet()){

			List<UserSettingsSection> sectionsToDisplay = section.getValue().stream().filter(setting -> setting.displayForUser(user)).collect(Collectors.toList());

			if (!sectionsToDisplay.isEmpty()){
				visibleUserSettingsSections.put(section.getKey(), sectionsToDisplay);
			}
		}

		return visibleUserSettingsSections.entrySet().stream().filter(entry -> !entry.getValue().isEmpty()).sorted(Map.Entry.comparingByKey((o1, o2) -> o1.getOrder().compareTo(o2.getOrder())))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setUserSettingsSection(UserSettingsSection userSettingsSection) {
		LOG.debug("Registering UserSettingsSection with bundleId: " + userSettingsSection.getBundleId() + ", displayOrder: " + userSettingsSection.getDisplayOrder() + ", category: "
				+ userSettingsSection.getDisplaySection().getKey());
		DisplaySection section = userSettingsSection.getDisplaySection();

		List<UserSettingsSection> entriesPerSection = userSettingsSections.getOrDefault(section, new LinkedList<UserSettingsSection>());
		entriesPerSection.add(userSettingsSection);
		Collections.sort(entriesPerSection, COMPARATOR_ON_ORDER_ASC);

		userSettingsSections.put(section, entriesPerSection);
	}

	protected void unsetUserSettingsSection(UserSettingsSection userSettingsSection) {
		LOG.debug("Unregistering UserSettingsSection with bundleId: " + userSettingsSection.getBundleId());
		DisplaySection section = userSettingsSection.getDisplaySection();
		List<UserSettingsSection> entriesPerSection = userSettingsSections.getOrDefault(section, new LinkedList<UserSettingsSection>());
		entriesPerSection.remove(userSettingsSection);

		userSettingsSections.put(section, entriesPerSection);
	}

}
