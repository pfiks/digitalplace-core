package com.placecube.digitalplace.payment.mock.web.constants;

public final class ParamKeys {

	public static final String AMOUNT = "amount";

	public static final String RETURN_URL = "returnUrl";

	private ParamKeys() {

	}

}
