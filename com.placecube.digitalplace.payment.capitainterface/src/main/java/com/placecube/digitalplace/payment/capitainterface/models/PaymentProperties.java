package com.placecube.digitalplace.payment.capitainterface.models;

/**
 * Class used to override the default (TEST) properties of the Capita interface
 * Will be used to inject the properties into the PaymentService constructor
 *
 * @author Christine.MacDonald
 *
 */
public class PaymentProperties implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4642818569456684932L;

	/**
	 * The endpoint for the interface
	 */
	private String endpoint;

	/**
	 * encryption key
	 */
	private String hmacKey;

	/**
	 * encryption key id
	 */
	private int hmacKeyId;

	/**
	 * scpId
	 */
	private int scpId;

	/**
	 * Site id
	 */
	private int siteId;

	/**
	 * @return the endpoint
	 */
	public String getEndpoint() {
		return endpoint;
	}

	/**
	 * @return the hmacKey
	 */
	public String getHmacKey() {
		return hmacKey;
	}

	/**
	 * @return the hmacKeyId
	 */
	public int getHmacKeyId() {
		return hmacKeyId;
	}

	/**
	 * @return the scpId
	 */
	public int getScpId() {
		return scpId;
	}

	/**
	 * @return the siteId
	 */
	public int getSiteId() {
		return siteId;
	}

	/**
	 * @param endpoint the endpoint to set
	 */
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	/**
	 * @param hmacKey the hmacKey to set
	 */
	public void setHmacKey(String hmacKey) {
		this.hmacKey = hmacKey;
	}

	/**
	 * @param hmacKeyId the hmacKeyId to set
	 */
	public void setHmacKeyId(int hmacKeyId) {
		this.hmacKeyId = hmacKeyId;
	}

	/**
	 * @param scpId the scpId to set
	 */
	public void setScpId(int scpId) {
		this.scpId = scpId;
	}

	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	@Override
	public String toString() {
		return "PaymentProperties [endpoint=" + endpoint + ", scpId=" + scpId + ", siteId=" + siteId + ", hmacKey=" + hmacKey + ", hmacKeyId=" + hmacKeyId + "]";
	}

}
