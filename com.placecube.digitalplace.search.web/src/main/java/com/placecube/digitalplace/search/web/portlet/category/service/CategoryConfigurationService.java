package com.placecube.digitalplace.search.web.portlet.category.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.web.portlet.category.configuration.CategoryFacetPortletInstanceConfiguration;

@Component(immediate = true, service = CategoryConfigurationService.class)
public class CategoryConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public Optional<CategoryFacetPortletInstanceConfiguration> getValidConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		CategoryFacetPortletInstanceConfiguration portletInstanceConfiguration = configurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, themeDisplay);

		return isValidConfiguration(portletInstanceConfiguration) ? Optional.of(portletInstanceConfiguration) : Optional.empty();
	}

	private boolean isValidConfiguration(CategoryFacetPortletInstanceConfiguration configuration) {
		return configuration.maxCategoriesToShow() > 0;
	}

}
