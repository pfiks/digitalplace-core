package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.settings.LocalizedValuesMap;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.configuration.ExpiredContentFilterSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.constants.ExpiredContentFilterSearchConstants;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.service.ExpiredContentFilterSearchService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ExpiredContentFilterSearchPortletTest extends PowerMockito {

	@InjectMocks
	private ExpiredContentFilterSearchPortlet expiredContentFilterSearchPortlet;

	@Mock
	private ExpiredContentFilterSearchPortletInstanceConfiguration mockConfiguration;

	@Mock
	private ExpiredContentFilterSearchService mockExpiredContentFilterSearchService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private LocalizedValuesMap mockTitleMap;

	@Before
	public void activateSetup() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test(expected = PortletException.class)
	public void render_WhenConfigurationExceptionIsThrown_ThenThrowPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockExpiredContentFilterSearchService.getValidConfiguration(mockThemeDisplay)).thenThrow(new ConfigurationException());

		expiredContentFilterSearchPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoErrorAndInvalidConfiguration_ThenSetInvalidConfigurationRequestAttribute() throws Exception {
		when(mockExpiredContentFilterSearchService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.empty());

		expiredContentFilterSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoErrorAndValidConfiguration_ThenSetRequestAttributes(boolean includeExpiredContent) throws Exception {
		String redirectURL = "redirectURLValue";
		String title = "titleValue";
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockExpiredContentFilterSearchService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getSharedSearchSessionSingleValuedAttribute(ExpiredContentFilterSearchConstants.SHARED_SEARCH_ATTRIBUTE_NAME)).thenReturn(String.valueOf(includeExpiredContent));
		when(mockConfiguration.title()).thenReturn(mockTitleMap);
		when(mockSharedSearchResponse.getEscapedSearchResultsIteratorURL()).thenReturn(redirectURL);
		when(mockExpiredContentFilterSearchService.getTitle(mockThemeDisplay, mockTitleMap)).thenReturn(title);

		expiredContentFilterSearchPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("includeExpiredContent", includeExpiredContent);
		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", redirectURL);
		verify(mockRenderRequest, times(1)).setAttribute("title", title);
	}

	@Test(expected = PortletException.class)
	public void render_WhenSearchExceptionIsThrown_ThenThrowPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockExpiredContentFilterSearchService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		expiredContentFilterSearchPortlet.render(mockRenderRequest, mockRenderResponse);
	}

}
