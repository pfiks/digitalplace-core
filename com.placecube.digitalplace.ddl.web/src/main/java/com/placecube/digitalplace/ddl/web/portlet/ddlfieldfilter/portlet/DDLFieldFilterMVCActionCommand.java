package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.portlet;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.constants.SharedSearchSessionKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.configuration.DDLFieldFilterPortletConfigurationConstants;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.DDL_FIELD_FILTER_PORTLET, "mvc.command.name=" + MVCCommandKeys.FIELD_FILTER_SEARCH }, service = MVCActionCommand.class)
public class DDLFieldFilterMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		String[] appliedFiltersValues = ParamUtil.getParameterValues(actionRequest, "filterOption");
		List<String> appliedFilters = new ArrayList<>();
		for (String filterValue : appliedFiltersValues) {
			if (Validator.isNotNull(filterValue)) {
				appliedFilters.add(filterValue);
			}
		}

		PortletPreferences portletPreferences = actionRequest.getPreferences();
		final String internalPortletId = portletPreferences.getValue(DDLFieldFilterPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK);
		final String filterName = SharedSearchSessionKeys.APPLIED_FILTER + "_" + internalPortletId;

		String appliedFilter = StringUtil.merge(appliedFilters, StringPool.COMMA);
		if (Validator.isNotNull(appliedFilter)) {
			sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, filterName, appliedFilter);
		} else {
			sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, filterName);
		}

		sharedSearchRequestService.sendRedirectFromParameter(actionRequest, actionResponse, "redirectURL");
	}

}
