package com.placecube.digitalplace.payment.govuk.service;

import static com.placecube.digitalplace.payment.constants.PaymentConstants.API_AUTH_KEY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.payment.govuk.configuration.GovUKCompanyConfiguration;
import com.placecube.digitalplace.payment.govuk.service.internal.models.GovUKPaymentProperties;
import com.placecube.digitalplace.payment.service.PaymentService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class GovUKPaymentServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 10;
	private static final String GOVUK_AUTH_KEY = "AUTH_KEY";
	private static final String GOVUK_BASE_URL = "https://publicapi.payments.service.gov.uk";
	private static final String GOVUK_CREATE_PAYMENT_URL = "/v1/payments";

	private static final String GOVUK_PAYMENT_STATUS_URL = "/v1/payments/{PAYMENT_ID}";

	@InjectMocks
	private GovUKPaymentService govUKPaymentService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private GovUKAuthKeyService mockGovUKAuthKeyService;

	@Mock
	private GovUKCompanyConfiguration mockGovUKCompanyConfiguration;

	@Mock(name = "paymentService")
	private PaymentService mockPaymentService;

	@Mock
	private ServiceContext mockServiceContext;

	@Test
	public void getGovUKPaymentProperties_whenPaymentServiceExists_thenReturnExistingProperties() throws Exception {
		final GovUKPaymentProperties govUKPaymentProperties = new GovUKPaymentProperties();
		mockGetCompanyConfiguration();
		mockSetPaymentProperties(govUKPaymentProperties);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getAttribute(API_AUTH_KEY)).thenReturn("apiKey");
		when(mockGovUKAuthKeyService.getApiAuthKey(mockGovUKCompanyConfiguration, "apiKey")).thenReturn(GOVUK_AUTH_KEY);

		GovUKPaymentProperties actual = govUKPaymentService.getGovUKPaymentProperties(mockServiceContext);

		assertNotNull(actual);
		assertEquals(GOVUK_AUTH_KEY, actual.getApiAuthKey());
		assertEquals(GOVUK_BASE_URL, actual.getBaseURL());
		assertEquals(GOVUK_CREATE_PAYMENT_URL, actual.getCreatePaymentURL());
		assertEquals(GOVUK_PAYMENT_STATUS_URL, actual.getPaymentStatusURL());
	}

	@Test(expected = ConfigurationException.class)
	public void isEnabled_whenExceptionRetrievingConfiguration_thenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		govUKPaymentService.isEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEnabled_whenNoError_thenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		mockGetCompanyConfiguration();
		when(mockGovUKCompanyConfiguration.enabled()).thenReturn(expected);

		boolean actual = govUKPaymentService.isEnabled(COMPANY_ID);

		assertEquals(expected, actual);
	}

	private void mockGetCompanyConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(GovUKCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockGovUKCompanyConfiguration);
	}

	private void mockSetPaymentProperties(final GovUKPaymentProperties paymentProperties) throws Exception {
		PowerMockito.whenNew(GovUKPaymentProperties.class).withNoArguments().thenReturn(paymentProperties);
		when(mockGovUKCompanyConfiguration.apiAuthKey()).thenReturn(GOVUK_AUTH_KEY);
		when(mockGovUKCompanyConfiguration.baseURL()).thenReturn(GOVUK_BASE_URL);
		when(mockGovUKCompanyConfiguration.createPaymentURL()).thenReturn(GOVUK_CREATE_PAYMENT_URL);
		when(mockGovUKCompanyConfiguration.getPaymentStatusURL()).thenReturn(GOVUK_PAYMENT_STATUS_URL);
	}

}
