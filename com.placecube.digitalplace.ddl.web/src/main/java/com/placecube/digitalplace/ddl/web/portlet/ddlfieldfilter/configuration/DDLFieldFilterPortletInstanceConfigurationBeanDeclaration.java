package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.configuration;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.settings.definition.ConfigurationBeanDeclaration;

@Component(service = ConfigurationBeanDeclaration.class)
public class DDLFieldFilterPortletInstanceConfigurationBeanDeclaration implements ConfigurationBeanDeclaration {

	@Override
	public Class<?> getConfigurationBeanClass() {
		return DDLFieldFilterPortletInstanceConfiguration.class;
	}

}
