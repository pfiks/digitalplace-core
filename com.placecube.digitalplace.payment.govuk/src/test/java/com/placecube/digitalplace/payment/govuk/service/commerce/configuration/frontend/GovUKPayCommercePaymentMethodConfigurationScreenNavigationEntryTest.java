package com.placecube.digitalplace.payment.govuk.service.commerce.configuration.frontend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.commerce.payment.constants.CommercePaymentScreenNavigationConstants;
import com.liferay.commerce.payment.model.CommercePaymentMethodGroupRel;
import com.liferay.commerce.product.model.CommerceChannel;
import com.liferay.commerce.product.service.CommerceChannelService;
import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.Language;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.settings.ParameterMapSettingsLocator;
import com.liferay.portal.kernel.settings.SettingsLocatorHelperUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.payment.govuk.service.commerce.GovUKCommerceConstants;
import com.placecube.digitalplace.payment.govuk.service.commerce.configuration.GovUKPayGroupServiceConfiguration;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, SettingsLocatorHelperUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil", "com.liferay.portal.kernel.settings.SettingsLocatorHelperUtil" })
public class GovUKPayCommercePaymentMethodConfigurationScreenNavigationEntryTest extends PowerMockito {

	@Mock
	private CommerceChannel mockCommerceChannel;

	@Mock
	private CommerceChannelService mockCommerceChannelService;

	@Mock
	private CommercePaymentMethodGroupRel mockCommercePaymentMethodGroupRel;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private GovUKPayGroupServiceConfiguration mockGovUKPayGroupServiceConfiguration;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSPRenderer mockJspRenderer;

	@Mock
	private Language mockLanguage;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private User mockUser;

	@InjectMocks
	private GovUKPayCommercePaymentMethodConfigurationScreenNavigationEntry govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class, SettingsLocatorHelperUtil.class);
	}

	@Test
	public void getCategoryKey_WhenNoError_ThenReturnsKey() {
		String result = govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.getCategoryKey();

		assertEquals(CommercePaymentScreenNavigationConstants.CATEGORY_KEY_COMMERCE_PAYMENT_METHOD_CONFIGURATION, result);
	}

	@Test
	public void getEntryKey_WhenNoError_ThenReturnsKey() {
		String result = govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.getEntryKey();

		assertEquals(GovUKCommerceConstants.ENTRY_KEY_GOVUK_PAY_COMMERCE_PAYMENT_METHOD_CONFIGURATION, result);
	}

	@Test
	public void getLabel_WhenNoError_ThenReturnsLabel() {
		Locale locale = Locale.UK;
		String label = "label";

		when(mockLanguage.get(locale, CommercePaymentScreenNavigationConstants.CATEGORY_KEY_COMMERCE_PAYMENT_METHOD_CONFIGURATION)).thenReturn(label);

		String result = govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.getLabel(locale);

		assertEquals(label, result);
	}

	@Test
	public void getScreenNavigationKey_WhenNoError_ThenReturnsKey() {
		String result = govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.getScreenNavigationKey();

		assertEquals(CommercePaymentScreenNavigationConstants.SCREEN_NAVIGATION_KEY_COMMERCE_PAYMENT_METHOD, result);
	}

	@Test
	public void isVisible_WhenNoErrorAndPaymentEngineKeyMatchesGovUKPay_ThenReturnsTrue() {
		when(mockCommercePaymentMethodGroupRel.getPaymentIntegrationKey()).thenReturn(GovUKCommerceConstants.PAYMENT_ENGINE_KEY);

		boolean result = govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.isVisible(mockUser, mockCommercePaymentMethodGroupRel);

		assertTrue(result);
	}

	@Test
	public void isVisible_WhenNoErrorAndPaymentEngineKeyDoesNotMatchGovUKPay_ThenReturnsFalse() {
		when(mockCommercePaymentMethodGroupRel.getPaymentIntegrationKey()).thenReturn("someOtherKey");

		boolean result = govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.isVisible(mockUser, mockCommercePaymentMethodGroupRel);

		assertFalse(result);
	}

	@Test
	public void isVisible_WhenNoErrorAndPaymentMethodIsNull_ThenReturnsFalse() {
		boolean result = govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.isVisible(mockUser, null);

		assertFalse(result);
	}

	@Test(expected = IOException.class)
	public void render_WhenExceptionGettingCommerceChannel_ThenThrowIOException() throws IOException, PortalException {
		long commerceChannelId = 123;

		when(ParamUtil.getLong(mockHttpServletRequest, "commerceChannelId")).thenReturn(commerceChannelId);
		when(mockCommerceChannelService.getCommerceChannel(commerceChannelId)).thenThrow(PortalException.class);

		govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = IOException.class)
	public void render_WhenExceptionRenderingJSP_ThenThrowIOException() throws IOException, PortalException {
		long commerceChannelId = 123;

		when(ParamUtil.getLong(mockHttpServletRequest, "commerceChannelId")).thenReturn(commerceChannelId);
		when(mockCommerceChannelService.getCommerceChannel(commerceChannelId)).thenReturn(mockCommerceChannel);
		doThrow(new IOException()).when(mockJspRenderer).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/configuration.jsp");

		govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.render(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void render_WhenNoError_ThenRenderConfigurationJSP() throws IOException, PortalException {
		long commerceChannelId = 123;

		when(ParamUtil.getLong(mockHttpServletRequest, "commerceChannelId")).thenReturn(commerceChannelId);
		when(mockCommerceChannelService.getCommerceChannel(commerceChannelId)).thenReturn(mockCommerceChannel);
		when(mockConfigurationProvider.getConfiguration(eq(GovUKPayGroupServiceConfiguration.class), any(ParameterMapSettingsLocator.class))).thenReturn(mockGovUKPayGroupServiceConfiguration);

		govUKPayCommercePaymentMethodConfigurationScreenNavigationEntry.render(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = inOrder(mockJspRenderer, mockHttpServletRequest);
		inOrder.verify(mockHttpServletRequest, times(1)).setAttribute(GovUKPayGroupServiceConfiguration.class.getName(), mockGovUKPayGroupServiceConfiguration);
		inOrder.verify(mockJspRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/configuration.jsp");
	}

}
