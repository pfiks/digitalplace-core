<%@ include file="/html/common/themes/init.jsp" %>
<liferay-util:buffer var="html">
    <liferay-util:include page="/html/common/themes/body_top.portal.jsp" servletContext="<%= application %>" />
</liferay-util:buffer>

<liferay-util:dynamic-include key="/html/common/themes/body_top.jsp#pre" />

<%
boolean includeBodyTopPre = GetterUtil.getBoolean(request.getAttribute("includeBodyTopPre"), false);
%>

<%= html %>

<c:if test="<%= includeBodyTopPre %>">
	<div id="runtime-portlet-ids-bodytop">
		<% 
		String[] runtimePortletIdsBodyTopPre = GetterUtil.getStringValues(request.getAttribute("runtimePortletIdsBodyTopPre"), new String[0]);
		for (String runtimePortletIdBodyTopPre : runtimePortletIdsBodyTopPre) {
			%>
			<liferay-portlet:runtime portletName="<%= runtimePortletIdBodyTopPre %>"/>
			<%
		}
		%>
	</div>
</c:if>
