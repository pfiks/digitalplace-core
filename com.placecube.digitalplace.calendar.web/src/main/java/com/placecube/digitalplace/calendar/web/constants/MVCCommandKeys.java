package com.placecube.digitalplace.calendar.web.constants;

public class MVCCommandKeys {

	public static final String RESOURCE_LOAD_EVENTS = "/get-calendar-events";

	private MVCCommandKeys() {

	}
}
