package com.placecube.digitalplace.authentication.twofactor.web.service;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;

import java.security.Key;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.encryptor.Encryptor;
import com.liferay.portal.kernel.encryptor.EncryptorException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.security.auth.PrincipalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DigesterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorSessionKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DigesterUtil.class, PropsUtil.class, ParamUtil.class })
public class TwoFactorRequestUtilsTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private Encryptor mockEncryptor;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletRequest mockHttpServletRequestOriginal;

	@Mock
	private HttpSession mockHttpSession;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private Key mockKey;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserTwoFactorAuthentication mockUserTwoFactorAuthentication;

	@InjectMocks
	private TwoFactorRequestUtils twoFactorRequestUtils;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, DigesterUtil.class);
	}

	@Test(expected = PrincipalException.class)
	public void getActionRequest_WhenDigestFromSessionIsNotTheSameAsTheState_ThenThrowsPrincipalException() throws Exception {
		String state = "stateValue";
		String digestedState = "digestedStateValue";
		when(ParamUtil.getString(mockActionRequest, "state")).thenReturn(state);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_DIGEST)).thenReturn("someOtherValue");
		when(DigesterUtil.digest(state)).thenReturn(digestedState);

		twoFactorRequestUtils.getActionRequest(mockHttpServletRequest, mockActionRequest);
	}

	@Test
	public void getActionRequest_WhenDigestFromSessionIsTheSameAsTheState_ThenReturnsAwrappedRequest() throws Exception {
		String state = "stateValue";
		String digestedState = "digestedStateValue";
		String descriptedState = "descriptedStateValue";
		when(ParamUtil.getString(mockActionRequest, "state")).thenReturn(state);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_DIGEST)).thenReturn(digestedState);
		when(DigesterUtil.digest(state)).thenReturn(digestedState);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_KEY)).thenReturn(mockKey);
		when(mockEncryptor.decrypt(mockKey, state)).thenReturn(descriptedState);

		String[] valuesKey2 = new String[] { "a2", "b2" };
		Map<String, Object> stateMapValues = new HashMap<>();
		stateMapValues.put("key1", "a1");
		stateMapValues.put("key2", valuesKey2);
		List<String> listValue = new ArrayList<>();
		listValue.add("valueList1");
		stateMapValues.put("key3", listValue);
		Map<String, Object> stateMap = new HashMap<>();
		stateMap.put("requestParameters", stateMapValues);
		when(mockJSONFactory.looseDeserialize(descriptedState, Map.class)).thenReturn(stateMap);

		ActionRequest result = twoFactorRequestUtils.getActionRequest(mockHttpServletRequest, mockActionRequest);

		assertThat(result.getParameter("key1"), equalTo("a1"));
		assertThat(Collections.list(result.getParameterNames()), containsInAnyOrder("key1", "key2", "key3"));
		assertThat(result.getParameterValues("key2"), equalTo(valuesKey2));
		assertThat(result.getParameterMap(), equalTo(stateMapValues));
	}

	@Test(expected = EncryptorException.class)
	public void getActionRequest_WhenDigestFromSessionIsTheSameAsTheStateButExceptionDecriptingTheState_ThenThrowsEncryptorException() throws Exception {
		String state = "stateValue";
		String digestedState = "digestedStateValue";
		when(ParamUtil.getString(mockActionRequest, "state")).thenReturn(state);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_DIGEST)).thenReturn(digestedState);
		when(DigesterUtil.digest(state)).thenReturn(digestedState);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_KEY)).thenReturn(mockKey);
		when(mockEncryptor.decrypt(mockKey, state)).thenThrow(new EncryptorException());

		twoFactorRequestUtils.getActionRequest(mockHttpServletRequest, mockActionRequest);
	}

	@Test
	public void getActionRequest_WhenStateParameterIsEmpty_ThenReturnsTheOriginalRequestAndNoActionsArePerformed() throws Exception {
		when(ParamUtil.getString(mockActionRequest, "state")).thenReturn("");

		ActionRequest result = twoFactorRequestUtils.getActionRequest(mockHttpServletRequest, mockActionRequest);

		assertThat(result, sameInstance(mockActionRequest));
		verifyZeroInteractions(mockJSONFactory, mockHttpServletRequest);
	}

	@Test
	public void getCompanyId_WhenNoErrors_ThenReturnsRequestCompanyId() {
		final long companyId = 322l;
		when(mockPortal.getCompanyId(mockPortletRequest)).thenReturn(companyId);

		long result = twoFactorRequestUtils.getCompanyId(mockPortletRequest);

		assertThat(result, equalTo(companyId));
	}

	@Test
	public void getEncryptedStateMapJSON_WhenNoError_ThenReturnsAnEncryptedCopyOfTheActionParametersWithoutRedirectParameter() throws EncryptorException {
		String expected = "expectedValue";
		String serializedString = "serializedString";
		Map<String, String[]> paramMap = new HashMap<>();
		paramMap.put("key1", new String[] { "a1", "b1" });
		paramMap.put("key2", new String[] { "a2", "b2" });
		paramMap.put("redirect", new String[] { "redirectValue" });

		when(mockActionRequest.getParameterMap()).thenReturn(paramMap);
		Map<String, Object> expectedParamMapWithoutRedirectToPutInRequestParameters = new HashMap<>(paramMap);
		expectedParamMapWithoutRedirectToPutInRequestParameters.remove("redirect");
		Map<String, Object> mapCopy = new HashMap<>();
		mapCopy.put("requestParameters", expectedParamMapWithoutRedirectToPutInRequestParameters);
		when(mockJSONFactory.looseSerializeDeep(eq(mapCopy))).thenReturn(serializedString);
		when(mockEncryptor.encrypt(mockKey, serializedString)).thenReturn(expected);

		String result = twoFactorRequestUtils.getEncryptedStateMapJSON(mockActionRequest, mockKey);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getOriginalServletRequest_WhenNoError_ThenReturnsTheOriginalServletRequest() {
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequestOriginal);

		HttpServletRequest result = twoFactorRequestUtils.getOriginalServletRequest(mockPortletRequest);

		assertThat(result, sameInstance(mockHttpServletRequestOriginal));
	}

	@Test
	public void getPlid_WhenThemeDisplayIsNotNull_ThenReturnsThePlidValue() {
		long plid = 123;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPlid()).thenReturn(plid);

		long result = twoFactorRequestUtils.getPlid(mockHttpServletRequest);

		assertThat(result, equalTo(plid));
	}

	@Test
	public void getPlid_WhenThemeDisplayIsNull_ThenReturnsZero() {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);

		long result = twoFactorRequestUtils.getPlid(mockHttpServletRequest);

		assertThat(result, equalTo(0l));
	}

	@Test
	public void getTwoFactorMVCCommandName_WhenUserTwoFactorAuthenticationIsNotResetKey_ThenReturnsVerify() {
		when(mockUserTwoFactorAuthentication.getResetKey()).thenReturn(false);

		String result = twoFactorRequestUtils.getTwoFactorMVCCommandName(Optional.of(mockUserTwoFactorAuthentication));

		assertThat(result, equalTo(TwoFactorMVCCommandKeys.VERIFY));
	}

	@Test
	public void getTwoFactorMVCCommandName_WhenUserTwoFactorAuthenticationIsResetKey_ThenReturnsSetup() {
		when(mockUserTwoFactorAuthentication.getResetKey()).thenReturn(true);

		String result = twoFactorRequestUtils.getTwoFactorMVCCommandName(Optional.of(mockUserTwoFactorAuthentication));

		assertThat(result, equalTo(TwoFactorMVCCommandKeys.SETUP));
	}

	@Test
	public void getTwoFactorMVCCommandName_WhenUserTwoFactorAuthenticationNotPresent_ThenReturnsSetup() {
		String result = twoFactorRequestUtils.getTwoFactorMVCCommandName(Optional.empty());

		assertThat(result, equalTo(TwoFactorMVCCommandKeys.SETUP));
	}
}
