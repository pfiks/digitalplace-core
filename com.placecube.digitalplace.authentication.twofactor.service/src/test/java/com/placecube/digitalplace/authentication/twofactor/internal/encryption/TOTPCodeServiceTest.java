package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.client.utils.URIBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import de.taimos.totp.TOTP;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({ "javax.net.ssl.*", "javax.management.*", "javax.crypto.JceSecurity.*", "javax.crypto.*" })
public class TOTPCodeServiceTest extends PowerMockito {

	private static final String ALGORITHM = "SHA1";
	private static final int AUTHENTICATOR_CODE_DURATION_IN_SECS = 30;
	private static final int AUTHENTICATOR_CODE_LENGTH = 6;
	private static final String EMAIL_ADDRESS = "email@address.com";
	private static final String ISSUER = "IssuerValue";
	private static final String SECRET_KEY = "123456789ABCDEFG";

	@Mock
	private Base32 mockBase32;

	@Mock
	private ModelFactoryBuilder mockModelFactoryBuilder;

	@Mock
	private SecretKeySpec mockSecretKeySpec;

	@Mock
	private URIBuilder mockUriBuilder;

	@InjectMocks
	private TOTPCodeService totpCodeService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void buildOTPUrl_WhenNoError_ThenBuildsOTPUrl() {
		String expected = "expectedValue";
		String formattedIssuerLabel = "formattedIssuerLabelValue";
		when(mockModelFactoryBuilder.getURIBuilderInstance()).thenReturn(mockUriBuilder);
		when(mockModelFactoryBuilder.getFormattedIssuerLabel(ISSUER, EMAIL_ADDRESS)).thenReturn(formattedIssuerLabel);
		when(mockUriBuilder.toString()).thenReturn(expected);

		String result = totpCodeService.buildOTPUrl(ISSUER, EMAIL_ADDRESS, SECRET_KEY, ALGORITHM, AUTHENTICATOR_CODE_LENGTH, AUTHENTICATOR_CODE_DURATION_IN_SECS);

		assertThat(result, equalTo(expected));
		InOrder inOrder = inOrder(mockUriBuilder);
		inOrder.verify(mockUriBuilder, times(1)).setScheme("otpauth");
		inOrder.verify(mockUriBuilder, times(1)).setHost("totp");
		inOrder.verify(mockUriBuilder, times(1)).setPath("/" + formattedIssuerLabel);
		inOrder.verify(mockUriBuilder, times(1)).setParameter("secret", SECRET_KEY);
		inOrder.verify(mockUriBuilder, times(1)).setParameter("issuer", ISSUER);
		inOrder.verify(mockUriBuilder, times(1)).setParameter("algorithm", ALGORITHM);
		inOrder.verify(mockUriBuilder, times(1)).setParameter("digits", String.valueOf(AUTHENTICATOR_CODE_LENGTH));
		inOrder.verify(mockUriBuilder, times(1)).setParameter("period", String.valueOf(AUTHENTICATOR_CODE_DURATION_IN_SECS));
	}

	@Test(expected = IllegalArgumentException.class)
	public void generateTopCode_WhenErrorRetrievingTheOneTimePasswordGenerator_ThenReturnsEmptyOptional() throws Exception {

		byte[] optBytes = new byte[0];

		when(mockModelFactoryBuilder.getBase32Instance()).thenReturn(mockBase32);
		when(mockBase32.decode(SECRET_KEY)).thenReturn(optBytes);

		String hexCode = Hex.encodeHexString(optBytes);

		TOTP.getOTP(hexCode);

		totpCodeService.generateTopCode(SECRET_KEY);

		verifyStatic(Hex.class, times(1));
		Hex.encodeHexString(optBytes);
	}

	@Test
	public void generateTopCode_WhenNoError_ThenReturnsTheGeneratesOneTimePassword() throws Exception {
		byte[] optBytes = new byte[3];

		when(mockModelFactoryBuilder.getBase32Instance()).thenReturn(mockBase32);
		when(mockBase32.decode(SECRET_KEY)).thenReturn(optBytes);

		String hexCode = Hex.encodeHexString(optBytes);

		String expectedCode = TOTP.getOTP(hexCode);

		Optional<String> result = totpCodeService.generateTopCode(SECRET_KEY);

		assertThat(result.get(), equalTo(expectedCode));
	}

}
