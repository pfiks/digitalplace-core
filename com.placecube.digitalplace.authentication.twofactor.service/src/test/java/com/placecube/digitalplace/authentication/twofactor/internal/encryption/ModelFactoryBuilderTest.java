package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base32;
import org.apache.http.client.utils.URIBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.security.SecureRandom;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ModelFactoryBuilderTest extends PowerMockito {

	@InjectMocks
	private ModelFactoryBuilder modelFactoryBuilder;

	public static Object[][] provideIllegalGCMParameterSpecInstanceArgs() {
		return new Object[][] { new Object[] { 0, null }, new Object[] { -1, new byte[] {} }, new Object[] { -1, null } };
	}

	public static Object[][] provideIllegalSecretKeySpecInstanceArgs() {
		return new Object[][] { new Object[] { new byte[] {}, null }, { new byte[] {}, "" }, new Object[] { null, null }, new Object[] { null, "" }, new Object[] { null, "AnyString" } };
	}

	@Test(expected = IllegalArgumentException.class)
	public void bytesToString_WhenBytesAreNull_ThenThrowsIllegalArgumentException() {
		modelFactoryBuilder.bytesToString(null);
	}

	@Test
	public void bytesToString_WhenNoError_ThenReturnsTheStringForTheBytes() {
		byte[] bytes = new byte[3];

		String result = modelFactoryBuilder.bytesToString(bytes);

		assertThat(result, equalTo(new String(bytes)));
	}

	@Test
	public void getBase32Instance_WhenNoError_ReturnsBase32Instance() {
		Base32 base32 = modelFactoryBuilder.getBase32Instance();

		assertNotNull(base32);
	}

	@Test
	public void getByteArrayOutputStreamInstance_WhenNoError_ThenReturnsAByteArrayOutputStream() {
		ByteArrayOutputStream result = modelFactoryBuilder.getByteArrayOutputStreamInstance();

		assertNotNull(result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getFormattedIssuerLabel_WhenIssuerContainsColon_ThenThrowsIllegalArgumentException() {
		modelFactoryBuilder.getFormattedIssuerLabel("issuer:invalid", "accountName");
	}

	@Test
	public void getFormattedIssuerLabel_WhenNoError_ThenReturnsTheFormattedValue() {
		String result = modelFactoryBuilder.getFormattedIssuerLabel("issuerValue", "accountNameValue");

		assertThat(result, equalTo("issuerValue:accountNameValue"));
	}

	@Test
	public void getGCMParameterSpecInstance_WhenNoError_ThenReturnsGCMParameterSpecInstance() {
		GCMParameterSpec result = modelFactoryBuilder.getGCMParameterSpecInstance(0, new byte[] {});

		assertNotNull(result);
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "provideIllegalGCMParameterSpecInstanceArgs")
	public void getGCMParameterSpecInstance_WhithWrongArguments_ThrowsIllegalArgmentException(int length, byte[] iv) {
		modelFactoryBuilder.getGCMParameterSpecInstance(length, iv);
	}

	@Test
	public void getSecretKeySpecInstance_ReturnsSecretKeySpecInstance() {
		SecretKeySpec result = modelFactoryBuilder.getSecretKeySpecInstance(new byte[1024], "AES");

		assertNotNull(result);
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "provideIllegalSecretKeySpecInstanceArgs")
	public void getSecretKeySpecInstance_WhithWrongArguments_ThrowsIllegalArgmentException(byte[] key, String algorithm) {
		modelFactoryBuilder.getSecretKeySpecInstance(key, algorithm);
	}

	@Test
	public void getSecureRandomInstance_ReturnsSecureRandomInstance() {
		SecureRandom result = modelFactoryBuilder.getSecureRandomInstance();

		assertNotNull(result);
	}

	@Test
	public void getURIBuilderInstance_WhenNoError_ThenReturnsAURIBuilder() {
		URIBuilder result = modelFactoryBuilder.getURIBuilderInstance();

		assertNotNull(result);
	}

	@Test
	public void instantiateBytes_WhenNoError_ThenReturnsArrayOf20Bytes() {
		byte[] bytes = modelFactoryBuilder.instantiateBytes();

		assertNotNull(bytes);
		assertThat(bytes.length, equalTo(20));
	}

	@Test
	public void now_ThenReturnsNowDate() {
		Date result = modelFactoryBuilder.now();

		assertNotNull(result);
	}
}
