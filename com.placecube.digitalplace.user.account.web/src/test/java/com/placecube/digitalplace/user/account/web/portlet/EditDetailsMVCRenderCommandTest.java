package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, SessionErrors.class })
public class EditDetailsMVCRenderCommandTest extends PowerMockito {

	private final long COMPANY_ID = 1L;

	@InjectMocks
	private EditDetailsMVCRenderCommand editDetailsMVCRenderCommand;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private Set<ExpandoField> mockExpandoFields;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserAccountFieldRegistry mockUserAccountFieldRegistry;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, SessionErrors.class);
	}

	@Test
	public void render_WhenAccountContextAlreadyHasConnectorModelIdSet_ThenDoesNotSetTheValueFromRequest() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);
		when(mockAccountContext.getAccountConnectorModelId()).thenReturn("someValue");

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, never()).setAccountConnectorModelId(anyString());
	}

	@Test
	public void render_WhenAccountContextDoesNotHaveConnectorModelIdSet_ThenSetsTheValueFromRequest() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);
		when(mockAccountContext.getAccountConnectorModelId()).thenReturn(" ");
		when(ParamUtil.getString(mockRenderRequest, AccountConnectorConstant.MODEL_ID)).thenReturn("modelIdFromRequest");

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, times(1)).setAccountConnectorModelId("modelIdFromRequest");
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionGettingContextFromUserDetails_ThenPortletExceptionIsThrown() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenThrow(new UserAccountException());
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		String result = editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/edit-details.jsp"));
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoError_ThenReturnEditUserDetailsJsp() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		String result = editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/edit-details.jsp"));
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoError_ThenSetsAccountFieldsAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		List<UserAccountField> userAccountFields = new ArrayList<>();
		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(COMPANY_ID);
		when(mockUserAccountFieldRegistry.getUserAccountFields(COMPANY_ID)).thenReturn(userAccountFields);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_FIELDS, userAccountFields);
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoError_ThenSetsCreateAccountConfigurationAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_CONFIG, mockCreateAccountConfiguration);
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoError_ThenSetsExpandoFieldsAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getConfiguredExpandoFields(mockAccountContext, mockUserProfileFieldsConfiguration)).thenReturn(mockExpandoFields);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("additionalDetailsExpandoFields", mockExpandoFields);
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoError_ThenSetShowEmailAddressInPersonalDetailsRequestAttributeAsTrue() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.SHOW_EMAIL_ADDRESS_IN_PERSONAL_DETAILS, true);
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoError_ThenSetsIsAddressOutOfLocalAreaAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(true);
		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.IS_ADDRESS_OUT_OF_LOCAL_AREA_ENABLED, true);
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoError_ThenSetsRedirectUrlAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL)).thenReturn("redirect url val");
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.REDIRECT_URL, "redirect url val");
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoError_ThenSetsUserProfileFieldsConfigurationAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.USER_PROFILE_CONFIG, mockUserProfileFieldsConfiguration);
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoErrorAndUseSessionIsFalse_ThenGetAccountContextFromExistingUserAndSetsAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		when(mockAccountContextService.getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(ParamUtil.getBoolean(mockRenderRequest, RequestKeys.USE_SESSION, true)).thenReturn(false);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContextService, times(1)).getContextFromUserDetails(mockRenderRequest, mockUserProfileFieldsConfiguration);
		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_CONTEXT, mockAccountContext);
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoErrorAndUseSessionIsTrue_ThenGetAccountContextFromSessionAndSetsAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(ParamUtil.getBoolean(mockRenderRequest, RequestKeys.USE_SESSION, true)).thenReturn(true);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContextService, times(1)).getOrCreateContext(mockRenderRequest);
		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_CONTEXT, mockAccountContext);
	}

	@Test
	public void render_WhenHasPermissionIsTrueAndNoErrorAndUseSessionIsTrue_ThenUpdateContextInSession() throws UserAccountException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(true);

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(ParamUtil.getBoolean(mockRenderRequest, RequestKeys.USE_SESSION, true)).thenReturn(true);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockRenderRequest);
	}

	@Test
	public void render_WhenHasPermissionsIsFalse_ThenAddsNoPermissionsToSessionErrors() throws PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(false);

		editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "no-permissions");
	}

	@Test
	public void render_WhenHasPermissionsIsFalse_ThenReturnsNoAccessJSP() throws PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(false);

		String result = editDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/no-access.jsp"));
	}

}
