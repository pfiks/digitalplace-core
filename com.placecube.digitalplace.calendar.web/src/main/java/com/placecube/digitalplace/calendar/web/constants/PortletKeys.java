package com.placecube.digitalplace.calendar.web.constants;

public final class PortletKeys {

	public static final String CALENDAR = "com_placecube_digitalplace_calendar_web_CalendarPortlet";

	private PortletKeys() {
	}

}
