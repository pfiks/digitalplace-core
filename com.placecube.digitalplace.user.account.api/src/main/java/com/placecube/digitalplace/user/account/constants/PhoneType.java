package com.placecube.digitalplace.user.account.constants;

public enum PhoneType {

	BUSINESS("business"),

	MOBILE("mobile-phone"),

	PERSONAL("personal");

	private final String type;

	private PhoneType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

}
