package com.placecube.digitalplace.addresslookup.statmap.service.utils;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.json.JSONException;
import com.placecube.digitalplace.address.model.AddressContext;

public class StatMapAddressLookupServiceTest extends PowerMockito {

	@Mock
	private AddressContext mockAddressLookup1;

	@Mock
	private AddressContext mockAddressLookup2;

	@Mock
	private AddressRetrievalService mockAddressRetrievalService;

	@InjectMocks
	private StatMapAddressLookupService statMapAddressLookupService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getAddressContexts_WhenNoError_ThenReturnsSetWithAllValidAddressContexts() throws Exception {
		String searchValue = "mySearchValue";
		String searchTerm = "mySearchTerm";
		String searchEndpointUrl = "mySearchEndpointUrl";
		String recordEndpointUrl = "myRecordEndpointUrl";
		String token = "myToken";
		when(mockAddressRetrievalService.getSearchURL(searchEndpointUrl, token, searchTerm, searchValue)).thenReturn("mySearchURL");
		when(mockAddressRetrievalService.executeCall("mySearchURL")).thenReturn("mySearchResponse");
		Set<String> addressIds = new LinkedHashSet<>();
		addressIds.add("id1");
		addressIds.add("id2");
		addressIds.add("id3");
		when(mockAddressRetrievalService.getAddressIdsFromResponse("mySearchResponse")).thenReturn(addressIds);

		when(mockAddressRetrievalService.getRecordURL(recordEndpointUrl, token, "id1")).thenReturn("id1SearchUrl");
		when(mockAddressRetrievalService.executeCall("id1SearchUrl")).thenReturn("id1Response");
		when(mockAddressRetrievalService.getAddressContextFromResponse("id1Response")).thenReturn(mockAddressLookup1);

		when(mockAddressRetrievalService.getRecordURL(recordEndpointUrl, token, "id2")).thenReturn("id2SearchUrl");
		when(mockAddressRetrievalService.executeCall("id2SearchUrl")).thenReturn("id2Response");
		when(mockAddressRetrievalService.getAddressContextFromResponse("id2Response")).thenThrow(new JSONException());

		when(mockAddressRetrievalService.getRecordURL(recordEndpointUrl, token, "id3")).thenReturn("id3SearchUrl");
		when(mockAddressRetrievalService.executeCall("id3SearchUrl")).thenReturn("id3Response");
		when(mockAddressRetrievalService.getAddressContextFromResponse("id3Response")).thenReturn(mockAddressLookup2);

		Set<AddressContext> results = statMapAddressLookupService.getAddressContexts(searchEndpointUrl, recordEndpointUrl, token, searchTerm, searchValue);

		assertThat(results.size(), equalTo(2));
		assertThat(results, contains(mockAddressLookup1, mockAddressLookup2));
	}

}
