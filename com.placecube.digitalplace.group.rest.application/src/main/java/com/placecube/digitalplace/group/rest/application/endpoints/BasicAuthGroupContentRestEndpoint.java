package com.placecube.digitalplace.group.rest.application.endpoints;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.ddl.model.DynamicDataListEntry;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.digitalplace.group.rest.application.BasicAuthGroupRestApplication;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestException;
import com.placecube.digitalplace.group.rest.application.model.FilterDynamicDataListEntry;
import com.placecube.digitalplace.group.rest.application.model.ModelBuilder;
import com.placecube.digitalplace.group.rest.application.model.RestListResult;

@Path("/group-content")
public class BasicAuthGroupContentRestEndpoint {

	private static final Log LOG = LogFactoryUtil.getLog(BasicAuthGroupContentRestEndpoint.class);

	private final DynamicDataListService dynamicDataListService;
	private final ModelBuilder modelBuilder;

	public BasicAuthGroupContentRestEndpoint(BasicAuthGroupRestApplication restApplication) {
		dynamicDataListService = restApplication.getDynamicDataListService();
		modelBuilder = restApplication.getModelBuilder();
	}

	@GET
	@Path("/dynamic-content/record-set-entries/{recordSetId}")
	@Produces("application/json")
	public Response getDynamicContentDetails(@PathParam("recordSetId") long recordSetId) throws GroupRestException {
		try {
			LOG.debug("Get dynamic content record set entries- recordSetId: " + recordSetId);

			List<DynamicDataListEntry> ddlListEntries = dynamicDataListService.getDDLRecordSetEntries(recordSetId);

			RestListResult result = modelBuilder.initRestListResult();
			for (DynamicDataListEntry ddListEntry : ddlListEntries) {
				result.addResult(FilterDynamicDataListEntry.create(ddListEntry));
			}

			return modelBuilder.getSuccessResponse(result);
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}
	}

}
