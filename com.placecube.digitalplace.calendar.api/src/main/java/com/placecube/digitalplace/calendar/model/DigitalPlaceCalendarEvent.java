package com.placecube.digitalplace.calendar.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DigitalPlaceCalendarEvent {

	protected static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

	private final String title;
	private final String viewURL;
	private final String start;
	private final String end;
	private final String backgroundColour;
	private final String textColour;

	public DigitalPlaceCalendarEvent(String title, String viewURL, String backgroundColour, String textColour, LocalDateTime startDateTime, LocalDateTime endDateTime) {
		this.title = title;
		this.viewURL = viewURL;
		this.backgroundColour = backgroundColour;
		this.textColour = textColour;
		this.start = startDateTime.format(FORMATTER);
		this.end = endDateTime.format(FORMATTER);
	}

	public String getTitle() {
		return title;
	}

	public String getViewURL() {
		return viewURL;
	}

	public String getStart() {
		return start;
	}

	public String getEnd() {
		return end;
	}

	public String getBackgroundColour() {
		return backgroundColour;
	}

	public String getTextColour() {
		return textColour;
	}
}
