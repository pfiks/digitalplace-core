<%@ include file="../init.jsp"%>

<c:if test="${not empty errors}">

	<div class="govuk-error-summary" aria-labelledby="${portletNamespace}error-summary-title" role="alert" tabindex="-1">
		
		<h2 class="govuk-error-summary__title" id="${portletNamespace}error-summary-title">
			<liferay-ui:message key="there-is-a-problem" />
		</h2>
		
		<div class="govuk-error-summary__body">
			<ul class="govuk-list govuk-error-summary__list">
				<c:forEach items="${errors}" var="error">
					<li>
					  <a href="#${portletNamespace}_error_${error.key}"><liferay-ui:message key="${error.value}" /></a>
					</li>
				</c:forEach>
			</ul>
		</div>
		
	</div>

</c:if>
