package com.placecube.digitalplace.search.web.portlet.category.configuration;

import com.placecube.digitalplace.search.web.portlet.category.constants.CategoryFacetDisplayConstants;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.search.web.portlet.category.configuration.CategoryFacetPortletInstanceConfiguration")
public interface CategoryFacetPortletInstanceConfiguration {

	@Meta.AD(required = false)
	boolean alphaSort();

	@Meta.AD(required = false, deflt = "false")
	boolean categoriesQueryAnded();

	@Meta.AD(required = false, deflt = "false")
	boolean checkboxesPanelCollapsedByDefault();

	@Meta.AD(required = false)
	String customCssClass();

	@Meta.AD(required = false)
	String customHeading();

	@Meta.AD(required = false, deflt = "")
	String[] displayCategoryIds();

	@Meta.AD(required = false, deflt = CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_CHECKBOX)
	String displayStyle();

	@Meta.AD(required = false, deflt = "")
	String[] displayVocabularyIds();

	@Meta.AD(required = false)
	String filterColour();

	@Meta.AD(required = false)
	String internalPortletId();

	@Meta.AD(required = false)
	boolean invisible();

	@Meta.AD(required = false, deflt = "10")
	int maxCategoriesToShow();

	@Meta.AD(required = false, deflt = "3")
	int maxTreeViewDepth();

	@Meta.AD(required = false, deflt = "")
	String[] preselectedCategoryIds();

	@Meta.AD(required = false, deflt = "")
	String[] preselectedVocabularyIds();

	@Meta.AD(required = false, deflt = "true")
	boolean showFrequencies();

	@Meta.AD(required = false, deflt = "false")
	boolean showAllCategoriesAlways();

	@Meta.AD(required = false)
	boolean showHeadingAsFirstDropdownOption();
}
