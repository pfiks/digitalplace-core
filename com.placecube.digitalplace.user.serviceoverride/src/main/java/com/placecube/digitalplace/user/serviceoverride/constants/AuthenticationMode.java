package com.placecube.digitalplace.user.serviceoverride.constants;

public enum AuthenticationMode {

	EMAIL_ADDRESS,

	SCREEN_NAME,

	USER_ID

}
