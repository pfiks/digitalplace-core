package com.placecube.digitalplace.user.account.constants;

import java.util.HashMap;
import java.util.Map;

public enum AccountValidatedField {

	BUSINESS_PHONE("businessPhoneNumber", "business-phone-is-required", 75),

	CAPTCHA("captcha", "captcha-verification-failed", 0),

	DATE_OF_BIRTH("dateOfBirth", "dob-is-required", 75),

	EMAIL("emailAddress", "email-address-is-required", 250),

	EMAIL_CONFIRMATION("confirmEmailAddress", "email-address-is-required", 250),

	FIRSTNAME("firstName", "first-name-is-required", 75),

	HOME_PHONE("homePhoneNumber", "home-phone-is-required", 75),

	JOB_TITLE("jobTitle", "job-title-or-role-is-required", 100),

	LASTNAME("lastName", "last-name-is-required", 75),

	MOBILE_PHONE("mobilePhoneNumber", "mobile-phone-is-required", 75),

	PASSWORD("password", "password-is-required", 75),

	PASSWORD_CONFIRM("confirmPassword", "password-is-required", 75),

	TWO_FACTOR_AUTHENTICATION_ENABLED("twoFactorAuthenticationEnabled", "two-factor-authentication-is-required", 0);

	private static final Map<String, AccountValidatedField> accountValidatedFields = new HashMap<>();

	static {

		for (AccountValidatedField accountValidatedField : AccountValidatedField.values()) {
			accountValidatedFields.put(accountValidatedField.getFieldName(), accountValidatedField);
		}
	}

	public static AccountValidatedField getFromFieldName(String fieldName) {
		return accountValidatedFields.get(fieldName);
	}

	private final String fieldName;
	private final String mandatoryErrorKey;
	private final int maxLength;

	private AccountValidatedField(String fieldName, String mandatoryErrorKey, int maxLength) {
		this.fieldName = fieldName;
		this.mandatoryErrorKey = mandatoryErrorKey;
		this.maxLength = maxLength;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getMandatoryErrorKey() {
		return mandatoryErrorKey;
	}

	public int getMaxLength() {
		return maxLength;
	}
}
