package com.placecube.digitalplace.userprofile.model.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.expando.kernel.model.ExpandoColumn;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ExpandoFieldImplTest extends PowerMockito {

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Test
	@Parameters({ "true", "false" })
	public void getLabel_WhenNoError_ThenReturnsTheExpandoColumnDisplayName(boolean required) {
		Locale locale = Locale.CANADA_FRENCH;
		String expected = "expected";
		when(mockExpandoColumn.getDisplayName(locale)).thenReturn(expected);
		ExpandoFieldImpl expandoFieldImpl = new ExpandoFieldImpl(mockExpandoColumn, required);

		String result = expandoFieldImpl.getLabel(locale);

		assertThat(result, equalTo(expected));
	}

}
