toggleBodyFieldFn = function(namespace, methodField, bodySupportedMethods) {
	var A = AUI();
	var value = A.one(methodField).val();
	var bodySupportedMethodsArray = bodySupportedMethods.split(",");
	
	if (bodySupportedMethodsArray.includes(value)) {
		A.one('#' + namespace + 'bodyPanel').show();
	}
	else {
		A.one('#' + namespace + 'bodyPanel').hide();
	}
}