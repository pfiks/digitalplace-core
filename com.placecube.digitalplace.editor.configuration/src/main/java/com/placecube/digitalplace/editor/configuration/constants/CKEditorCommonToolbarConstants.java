package com.placecube.digitalplace.editor.configuration.constants;

public final class CKEditorCommonToolbarConstants {

	public static final String ANCHOR = "Anchor";

	public static final String AUDIO_SELECTOR_PLUGIN_NAME = "AudioSelector";

	public static final String BLOCKQUOTE_PLUGIN_NAME = "Blockquote";

	public static final String BOLD_PLUGIN_NAME = "Bold";

	public static final String BULLETED_LIST_PLUGIN_NAME = "BulletedList";

	public static final String BUTTONS = "buttons";

	public static final String GDS_CONFIRMATION_PLUGIN_NAME = "gdsConfirmationPanel";

	public static final String GDS_DEFAULT_BUTTON_PLUGIN_NAME = "gdsDefaultButton";

	public static final String GDS_DETAILS_PLUGIN_NAME = "gdsDetails";

	public static final String GDS_DISABLED_BUTTON_PLUGIN_NAME = "gdsDisabledButton";

	public static final String GDS_NOTIFICATION_PLUGIN_NAME = "gdsNotificationBanner";

	public static final String GDS_SECONDARY_BUTTON_PLUGIN_NAME = "gdsSecondaryButton";

	public static final String GDS_START_BUTTON_PLUGIN_NAME = "gdsStartButton";

	public static final String GDS_WARNING_BUTTON_PLUGIN_NAME = "gdsWarningButton";

	public static final String GDS_WARNING_PLUGIN_NAME = "gdsWarningPanel";

	public static final String HELP_DIALOG = "HelpDialog";

	public static final String IMAGE_SELECTOR_PLUGIN_NAME = "ImageSelector";

	public static final String ITALIC_PLUGIN_NAME = "Italic";

	public static final String LINK_PLUGIN_NAME = "Link";

	public static final String MAXIMIZE_PLUGIN = "maximize";

	public static final String MAXIMIZE_PLUGIN_NAME = "Maximize";

	public static final String NUMBERED_LIST_PLUGIN_NAME = "NumberedList";

	public static final String PASTE_FROM_WORD_PLUGIN_NAME = "PasteFromWord";

	public static final String PASTE_TEXT_PLUGIN_NAME = "PasteText";

	public static final String REDO_PLUGIN_NAME = "Redo";

	public static final String ROLES = "roles";

	public static final String SCAYT_PLUGIN_NAME = "Scayt";

	public static final String SOURCE_PLUGIN_NAME = "Source";

	public static final String STRIKETHROUGH_PLUGIN_NAME = "Strikethrough";

	public static final String STYLES_PLUGIN_NAME = "Styles";

	public static final String TABLE_PLUGIN_NAME = "Table";

	public static final String UNDERLINE_PLUGIN_NAME = "Underline";

	public static final String UNDO_PLUGIN_NAME = "Undo";

	public static final String UNLINK_PLUGIN_NAME = "Unlink";

	public static final String VIDEO_SELECTOR_PLUGIN_NAME = "VideoSelector";

	private CKEditorCommonToolbarConstants() {
	}
}
