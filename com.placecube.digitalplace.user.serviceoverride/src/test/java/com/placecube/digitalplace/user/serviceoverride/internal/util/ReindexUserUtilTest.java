package com.placecube.digitalplace.user.serviceoverride.internal.util;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.SearchException;

public class ReindexUserUtilTest extends PowerMockito {

	@Mock
	private Indexer<Object> mockIndexer;

	@Mock
	private IndexerRegistry mockIndexerRegistry;

	@Mock
	private User mockUser;

	@InjectMocks
	private ReindexUserUtil reindexUserUtil;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void reindexUser_WithUserIdParameter_WhenUserIdInvalid_ThenNoActionIsPerformed() {
		reindexUserUtil.reindexUser(0l);

		verifyZeroInteractions(mockIndexerRegistry);
	}

	@Test
	public void reindexUser_WithUserIdParameter_WhenUserIdValid_ThenReindexTheUser() throws SearchException {
		long userId = 1;
		when(mockIndexerRegistry.nullSafeGetIndexer(User.class.getName())).thenReturn(mockIndexer);

		reindexUserUtil.reindexUser(userId);

		verify(mockIndexer, times(1)).reindex(User.class.getName(), userId);
	}

	@Test
	public void reindexUser_WithUserIdParameter_WhenUserIdValidAndExceptionReindexingTheUser_ThenNoErrorIsThrown() throws SearchException {
		long userId = 1;
		when(mockIndexerRegistry.nullSafeGetIndexer(User.class.getName())).thenReturn(mockIndexer);
		doThrow(new SearchException()).when(mockIndexer).reindex(User.class.getName(), userId);

		try {
			reindexUserUtil.reindexUser(userId);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void reindexUser_WithUserParameter_WhenUserIsNotNull_ThenReindexTheUser() throws SearchException {
		when(mockIndexerRegistry.nullSafeGetIndexer(User.class.getName())).thenReturn(mockIndexer);

		reindexUserUtil.reindexUser(mockUser);

		verify(mockIndexer, times(1)).reindex(mockUser);
	}

	@Test
	public void reindexUser_WithUserParameter_WhenUserIsNotNullAndExceptionReindexingTheUser_ThenNoErrorIsThrown() throws SearchException {
		when(mockIndexerRegistry.nullSafeGetIndexer(User.class.getName())).thenReturn(mockIndexer);
		doThrow(new SearchException()).when(mockIndexer).reindex(mockUser);

		try {
			reindexUserUtil.reindexUser(mockUser);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void reindexUser_WithUserParameter_WhenUserIsNull_ThenNoActionIsPerformed() {
		reindexUserUtil.reindexUser(null);

		verifyZeroInteractions(mockIndexerRegistry);
	}

}
