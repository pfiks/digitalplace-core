package com.placecube.digitalplace.group.rest.application.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.placecube.digitalplace.ddl.model.DynamicDataListEntry;
import com.placecube.digitalplace.ddl.model.DynamicDataListValue;

@JsonFilter("FilterDynamicDataListEntry")
public class FilterDynamicDataListEntry implements DynamicDataListEntry {

	private DynamicDataListEntry dynamicDataListEntry;

	private FilterDynamicDataListEntry(DynamicDataListEntry dynamicDataListEntry) {
		this.dynamicDataListEntry = dynamicDataListEntry;
	}

	public static FilterDynamicDataListEntry create(DynamicDataListEntry dynamicDataListEntry) {
		return new FilterDynamicDataListEntry(dynamicDataListEntry);
	}

	@Override
	public DDLRecord getDDLRecord() {
		return dynamicDataListEntry.getDDLRecord();
	}

	@Override
	public long getId() {
		return dynamicDataListEntry.getDDLRecord().getRecordId();
	}

	@Override
	public List<DynamicDataListValue> getValues() {
		return dynamicDataListEntry.getValues();
	}
}
