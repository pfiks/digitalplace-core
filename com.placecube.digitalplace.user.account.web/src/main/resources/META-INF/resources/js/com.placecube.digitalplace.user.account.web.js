AUI.add(
	'com-placecube-digitalplace-user-account-web', 
	function(A) {
		
		var CreateAccountWeb = {
			
				disableCopyAndPasteOnField: function(params) {
					var fieldId = params.fieldId;
					try{
						document.getElementById(fieldId).onpaste = function(){
							return false;
						};
					} catch(e){}
				},

				lookupAddress: function(params) {
					try{
						$('#' + params.fieldId).on('change', function(e) {
							var resourceURL = Liferay.PortletURL.createResourceURL();
							resourceURL.setPortletId(params.portletId);
							resourceURL.setResourceId(params.resourceCommandId);
							resourceURL.setParameter('uprn', this.value);

							$.get(
								resourceURL.toString(), 
								{}, 
								function(data) {
									var parsedData = JSON.parse(data);
									$('#' + params.panelId).html(parsedData.fullAddress);	
								}
							);

						});
					} catch(e) {
						console.log(e);
					}

				}

		};

		A.CreateAccountWeb = CreateAccountWeb;
	},
	'',
	{
		requires: [
			'aui-base',
			'liferay-portlet-url'
		]
	}
);
