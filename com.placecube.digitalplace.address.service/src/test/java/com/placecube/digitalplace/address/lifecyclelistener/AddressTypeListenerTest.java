package com.placecube.digitalplace.address.lifecyclelistener;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.ListTypeConstants;
import com.liferay.portal.kernel.service.ListTypeLocalService;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.address.constants.AddressType;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class })
public class AddressTypeListenerTest extends PowerMockito {

	private static final long COMPANY_ID = 123;
	
	@InjectMocks
	private AddressTypeListener addressTypeListener;

	@Mock
	private Company mockCompany;

	@Mock
	private ListType mockListType;

	@Mock
	private ListTypeLocalService mockListTypeLocalService;


	@Before
	public void setUp() {
		mockStatic(PropsUtil.class);
	}

	@Test
	public void portalInstanceRegistered_WhenPrimaryAddressTypeDoesNotExist_ThenCreatePrimaryAddressType() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockListTypeLocalService.getListType(COMPANY_ID, AddressType.PRIMARY.getName(), ListTypeConstants.CONTACT_ADDRESS)).thenReturn(null);

		addressTypeListener.portalInstanceRegistered(mockCompany);

		verify(mockListTypeLocalService, times(1)).addListType(COMPANY_ID, AddressType.PRIMARY.getName(), ListTypeConstants.CONTACT_ADDRESS);
	}

	@Test()
	public void portalInstanceRegistered_WhenPrimaryAddressTypeExists_ThenDoNothing() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockListTypeLocalService.getListType(COMPANY_ID, AddressType.PRIMARY.getName(), ListTypeConstants.CONTACT_ADDRESS)).thenReturn(mockListType);

		addressTypeListener.portalInstanceRegistered(mockCompany);

		verify(mockListTypeLocalService, never()).addListType(COMPANY_ID, AddressType.PRIMARY.getName(), ListTypeConstants.CONTACT_ADDRESS);


	}
}
