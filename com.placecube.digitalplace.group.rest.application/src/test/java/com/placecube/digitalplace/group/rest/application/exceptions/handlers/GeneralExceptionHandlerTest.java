package com.placecube.digitalplace.group.rest.application.exceptions.handlers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.group.rest.application.BasicAuthGroupRestApplication;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.model.ModelBuilder;
import com.placecube.digitalplace.group.rest.application.model.RestErrorMessage;

public class GeneralExceptionHandlerTest extends PowerMockito {

	private GeneralExceptionHandler generalExceptionHandler;

	@Mock
	private BasicAuthGroupRestApplication mockBasicAuthGroupRestApplication;

	@Mock
	private RestErrorMessage mockErrorMessage;

	@Mock
	private ModelBuilder mockModelBuilder;

	@Before
	public void activateSetUp() {
		initMocks(this);

		when(mockBasicAuthGroupRestApplication.getModelBuilder()).thenReturn(mockModelBuilder);
		generalExceptionHandler = new GeneralExceptionHandler(mockBasicAuthGroupRestApplication);
	}

	@Test
	public void toResponse_WhenNoError_ThenReturnsResponseForSystemErrorMessage() {
		Exception exception = new NullPointerException();
		when(mockModelBuilder.initRestErrorMessage(Status.INTERNAL_SERVER_ERROR.getStatusCode(), ErrorCodes.INTERNAL_ERROR.getCode(), getStacktraceAsString(exception))).thenReturn(mockErrorMessage);
		when(mockErrorMessage.getErrorStatus()).thenReturn(Status.ACCEPTED.getStatusCode());

		Response response = generalExceptionHandler.toResponse(exception);

		assertThat(response.getStatus(), equalTo(Status.ACCEPTED.getStatusCode()));
		assertThat(response.getEntity(), sameInstance(mockErrorMessage));
	}

	private String getStacktraceAsString(Throwable exception) {
		StringWriter sw = new StringWriter();
		exception.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

}
