package com.placecube.digitalplace.user.settings.web.model;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;

public interface UserSettingsSection {

	String getBundleId();

	int getDisplayOrder();

	DisplaySection getDisplaySection();

	default String getId() {
		return getBundleId();
	}

	default String getTitle(Locale locale) {
		return AggregatedResourceBundleUtil.get(getTitleKey(), locale, new String[] { "com.placecube.digitalplace.user.settings.web", getBundleId() });
	}

	String getTitleKey();

	default boolean displayForUser(User user) { return true; }

	void render(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
