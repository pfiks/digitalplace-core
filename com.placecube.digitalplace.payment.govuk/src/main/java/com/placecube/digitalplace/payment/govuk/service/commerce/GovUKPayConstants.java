package com.placecube.digitalplace.payment.govuk.service.commerce;

public final class GovUKPayConstants {

	public static final String AGREEMENT_ID_JSON_KEY = "agreement_id";

	public static final String AGREEMENT_ID_EXPANDO_COLUMN_NAME = "agreement-id";

	public static final String PAYMENT_IDEMPOTENCY_DATA_EXPANDO_COLUMN_NAME = "payment-idempotency-data";

	public static final String STATUS_JSON_KEY = "status";

	public static final String STATUS_ACTIVE = "active";

	public static final String STATUS_CREATED = "created";

	private GovUKPayConstants() {
	}
}
