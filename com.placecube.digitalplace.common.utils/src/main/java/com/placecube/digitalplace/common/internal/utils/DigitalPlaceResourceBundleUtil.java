package com.placecube.digitalplace.common.internal.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.liferay.portal.kernel.resource.bundle.AggregateResourceBundleLoader;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoader;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoaderUtil;

public final class DigitalPlaceResourceBundleUtil {

	public static ResourceBundle getAggregatedResourceBundle(Locale locale, String... bundleSymbolicNames) {
		List<ResourceBundleLoader> resourceBundleLoaders = getResourceBundles(bundleSymbolicNames);

		AggregateResourceBundleLoader aggregateResourceBundleLoader = new AggregateResourceBundleLoader(resourceBundleLoaders.toArray(new ResourceBundleLoader[resourceBundleLoaders.size()]));

		return aggregateResourceBundleLoader.loadResourceBundle(locale);
	}

	private static List<ResourceBundleLoader> getResourceBundles(String... bundleSymbolicNames) {
		List<ResourceBundleLoader> resourceBundleLoaders = new ArrayList<>();
		resourceBundleLoaders.add(ResourceBundleLoaderUtil.getPortalResourceBundleLoader());

		for (String bundleSymbolicName : bundleSymbolicNames) {
			resourceBundleLoaders.add(ResourceBundleLoaderUtil.getResourceBundleLoaderByBundleSymbolicName(bundleSymbolicName));
		}
		return resourceBundleLoaders;
	}

}
