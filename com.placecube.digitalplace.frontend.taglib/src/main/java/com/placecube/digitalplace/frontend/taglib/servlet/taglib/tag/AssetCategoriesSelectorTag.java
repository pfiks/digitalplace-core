package com.placecube.digitalplace.frontend.taglib.servlet.taglib.tag;

import com.liferay.asset.kernel.model.AssetCategoryConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HashMapBuilder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.frontend.taglib.servlet.taglib.util.AssetCategoriesSelectorTagUtil;
import com.placecube.digitalplace.frontend.taglib.servlet.taglib.util.ServletContextUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

public class AssetCategoriesSelectorTag extends IncludeTag {

	private static final Log LOG = LogFactoryUtil.getLog(AssetCategoriesSelectorTag.class);

	private static final String PAGE = "/asset_categories_selector/page.jsp";

	private long[] categoryIds;

	private String className;

	private long classTypePK = AssetCategoryConstants.ALL_CLASS_TYPE_PK;

	private long[] groupIds;

	private String hiddenInput = "assetCategoryIds";

	private boolean ignoreRequestValue;

	private boolean showRequiredLabel = true;

	private boolean singleSelect;

	private long[] vocabularyIds;

	public long[] getCategoryIds() {
		return categoryIds;
	}

	public String getClassName() {
		return className;
	}

	public long getClassTypePK() {
		return classTypePK;
	}

	public String getHiddenInput() {
		return hiddenInput;
	}

	public long[] getVocabularyIds() {
		return vocabularyIds;
	}

	public boolean isIgnoreRequestValue() {
		return ignoreRequestValue;
	}

	public boolean isShowRequiredLabel() {
		return showRequiredLabel;
	}

	public boolean isSingleSelect() {
		return singleSelect;
	}

	public void setCategoryIds(long[] categoryIds) {
		this.categoryIds = categoryIds;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setClassTypePK(long classTypePK) {
		this.classTypePK = classTypePK;
	}

	public void setGroupIds(long[] groupIds) {
		this.groupIds = groupIds;
	}

	public void setHiddenInput(String hiddenInput) {
		this.hiddenInput = hiddenInput;
	}

	public void setIgnoreRequestValue(boolean ignoreRequestValue) {
		this.ignoreRequestValue = ignoreRequestValue;
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);

		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setShowRequiredLabel(boolean showRequiredLabel) {
		this.showRequiredLabel = showRequiredLabel;
	}

	public void setSingleSelect(boolean singleSelect) {
		this.singleSelect = singleSelect;
	}

	public void setVocabularyIds(long[] vocabularyIds) {
		this.vocabularyIds = vocabularyIds;
	}

	@Override
	protected void cleanUp() {
		super.cleanUp();

		categoryIds = null;
		className = null;
		classTypePK = AssetCategoryConstants.ALL_CLASS_TYPE_PK;
		groupIds = null;
		hiddenInput = "assetCategoryIds";
		ignoreRequestValue = false;
		showRequiredLabel = true;
		singleSelect = false;
	}

	@Override
	protected String getPage() {
		return PAGE;
	}

	@Override
	protected void setAttributes(HttpServletRequest httpServletRequest) {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String eventName = AssetCategoriesSelectorTagUtil.getEventName();
		String namespace = AssetCategoriesSelectorTagUtil.getNamespace(httpServletRequest);
		try {
			httpServletRequest.setAttribute("liferay-asset:asset-categories-selector:data",
					HashMapBuilder.<String, Object>put("eventName", eventName).put("groupIds", AssetCategoriesSelectorTagUtil.getGroupIds(groupIds, themeDisplay.getScopeGroupId()))
							.put("id", AssetCategoriesSelectorTagUtil.getAssetCategoriesSelectorId(namespace))
							.put("inputName", AssetCategoriesSelectorTagUtil.getHiddenInputName(namespace, hiddenInput))
							.put("portletURL", String.valueOf(AssetCategoriesSelectorTagUtil.getPortletURL(eventName, httpServletRequest)))
							.put("vocabularies", AssetCategoriesSelectorTagUtil.getVocabulariesDescriptors(vocabularyIds, categoryIds, className, classTypePK, showRequiredLabel, singleSelect,
									ignoreRequestValue, hiddenInput, themeDisplay, httpServletRequest))
							.build());
		} catch (Exception exception) {
			LOG.error(exception, exception);
		}
	}
}
