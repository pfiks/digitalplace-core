package com.placecube.digitalplace.payment.mock.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.payment.mock.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.payment.mock.web.constants.ParamKeys;
import com.placecube.digitalplace.payment.mock.web.constants.PortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.MOCK_PAYMENT_PORTLET, "mvc.command.name=" + MVCCommandKeys.REDIRECT_ACTION_COMMAND }, service = MVCActionCommand.class)
public class RedirectMVCActionCommand extends BaseMVCActionCommand {

	private static final String INVALID_CARD_NUMBER = "99";

	@Reference
	private Portal portal;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		boolean paymentSuccess = true;
		if (ParamUtil.getString(actionRequest, "cardNumber", StringPool.BLANK).startsWith(INVALID_CARD_NUMBER)) {
			paymentSuccess = false;
		}

		String returnUrl = ParamUtil.getString(actionRequest, ParamKeys.RETURN_URL, StringPool.BLANK) + "&paymentSuccess=" + paymentSuccess;
		portal.getHttpServletResponse(actionResponse).sendRedirect(returnUrl);
	}

}
