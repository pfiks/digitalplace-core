package com.placecube.digitalplace.user.serviceoverride.internal.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.placecube.digitalplace.user.serviceoverride.service.UserServiceOverrideAction;

@Component(immediate = true, service = UserServiceOverrideActionRegistry.class)
public class UserServiceOverrideActionRegistry {

	private static final class ExecutionOrderComparator implements Comparator<ServiceWrapper<UserServiceOverrideAction>> {

		@Override
		public int compare(ServiceWrapper<UserServiceOverrideAction> serviceWrapper1, ServiceWrapper<UserServiceOverrideAction> serviceWrapper2) {
			Long executionOrder1 = MapUtil.getLong(serviceWrapper1.getProperties(), UserServiceOverrideAction.SERVICE_EXECUTION_PRIORITY);
			Long executionOrder2 = MapUtil.getLong(serviceWrapper2.getProperties(), UserServiceOverrideAction.SERVICE_EXECUTION_PRIORITY);

			return executionOrder1.compareTo(executionOrder2);
		}
	}

	@SuppressWarnings("unused")
	private BundleContext bundleContext;

	private ServiceTrackerMap<String, ServiceWrapper<UserServiceOverrideAction>> userListenerActionMap;

	public List<UserServiceOverrideAction> getUserServiceOverrideActions() {
		List<ServiceWrapper<UserServiceOverrideAction>> allServices = ListUtil.fromCollection(userListenerActionMap.values());

		Collections.sort(allServices, new ExecutionOrderComparator());

		List<UserServiceOverrideAction> results = allServices.stream().map(ServiceWrapper::getService).collect(Collectors.toList());
		return Collections.unmodifiableList(results);
	}

	@Activate
	protected void activate(BundleContext bundleContext) {
		this.bundleContext = bundleContext;

		userListenerActionMap = ServiceTrackerMapFactory.openSingleValueMap(bundleContext, UserServiceOverrideAction.class, UserServiceOverrideAction.SERVICE_ID,
				ServiceTrackerCustomizerFactory.<UserServiceOverrideAction>serviceWrapper(bundleContext));
	}

	@Deactivate
	protected void deactivate() {
		userListenerActionMap.close();
	}

}
