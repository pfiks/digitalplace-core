<%@ page import="com.liferay.portal.kernel.util.Constants" %>

<%@ page import="com.placecube.digitalplace.navigation.constants.ConfigurationConstants" %>
<%@ page import="com.placecube.digitalplace.navigation.constants.PortletPreferenceKeys" %>

<%@ include file="/init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="<%= true %>" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="<%= true %>" var="configurationRenderURL" />

<div class="container-fluid container-fluid-max-xl">

	<aui:form action="<%= configurationActionURL %>" method="post" name="fm">

		<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
		<aui:input name="redirect" type="hidden" value="<%= configurationRenderURL %>" />

		<aui:fieldset label="category-navigation">

			<liferay-ddm:template-selector
				className="<%= BreadcrumbEntry.class.getName() %>"
				displayStyle="${ displayStyle }"
				displayStyleGroupId="${ displayStyleGroupId }"
				showEmptyOption="true"
			/>
			
			<aui:select
				label="filter-vocabulary"
				name="<%= PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID %>"
				value='${ categoryNavFilterVocabularyId }'
			>
				<aui:option value="<%= ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID %>"/>
				<c:forEach var="vocabulary" items="${ categoryNavFilterVocabs }">
					<aui:option value="${ vocabulary.vocabularyId }">${ vocabulary.getTitle(locale) } <c:if test="${vocabulary.getGroupId() == globalGroupId}">(<%= LanguageUtil.get(locale, "global") %>)</c:if></aui:option>
				</c:forEach>
			</aui:select>

			<aui:input
				label="url-category-property-name"
				name="<%= PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME %>"
				type="text"
				value='${ categoryNavUrlCategoryPropertyName }'
			/>

			<aui:input
				label="build-categories-hierarchy-url-property-name"
				name="<%= PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME %>"
				type="text"
				value='${ categoryNavBuildCategoriesHierarchyUrlPropertyName }'
			/>

			<aui:input
				label="root-entry-name"
				name="<%= PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME %>"
				type="text"
				value='${ categoryNavRootEntryName }'
			/>

			<aui:input
				label="force-root-for-layout-breadcrumbs"
				type="checkbox"
				name="<%= PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS %>"
				value="${forceRootForLayoutBreadcrumbs}" />

			<aui:input
				label="show-current-layout"
				type="checkbox"
				name="<%= PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT%>"
				value="${categoryNavShowCurrentLayout}" />

			<aui:input
				label="use-layout-url"
				type="checkbox"
				name="<%= PortletPreferenceKeys.USE_LAYOUT_URL%>"
				value="${useLayoutUrl}" />

			<aui:input
				label="display-only-one-asset-category-breadcrumb"
				type="checkbox"
				name="<%= PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB%>"
				value="${displayOnlyOneAssetCategoryBreadcrumb}" />

			<aui:button-row>
				<aui:button type="submit" />
			</aui:button-row>
		</aui:fieldset>
	</aui:form>

</div>