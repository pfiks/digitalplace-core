package com.placecube.digitalplace.formadmin.web.portlet.command;

import java.io.File;
import java.io.FileInputStream;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.formadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.service.FormExportService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.FORMS_MANAGER, "mvc.command.name=" + MVCCommandKeys.EXPORT_FORM }, service = MVCResourceCommand.class)

public class ExportFormMVCResourceCommand extends BaseMVCResourceCommand {

	@Reference
	private DDMFormInstanceLocalService ddmFormInstanceLocalService;

	@Reference
	private FormExportService formExportService;

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {

		DDMFormInstance ddmFormInstance = ddmFormInstanceLocalService.getDDMFormInstance(ParamUtil.getLong(resourceRequest, PortletRequestKeys.FORM_INSTANCE_ID));

		File file = formExportService.exportForm(ddmFormInstance);
		String formName = ddmFormInstance.getName(resourceRequest.getLocale());
		try (FileInputStream fis = formExportService.getFileInputStream(file)) {
			byte[] byteArray = new byte[(int) file.length()];
			if (fis.read(byteArray) > 0) {
				resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + formName + ".lar");

				PortletResponseUtil.sendFile(resourceRequest, resourceResponse, formName + ".lar", FileUtil.getBytes(file));
			}
		}
	}
}
