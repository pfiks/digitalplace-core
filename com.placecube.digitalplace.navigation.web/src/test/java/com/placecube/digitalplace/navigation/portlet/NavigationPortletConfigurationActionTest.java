package com.placecube.digitalplace.navigation.portlet;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.site.navigation.constants.SiteNavigationConstants;
import com.placecube.digitalplace.navigation.configuration.NavigationPortletInstanceConfiguration;
import com.placecube.digitalplace.navigation.service.NavigationService;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class, PropsUtil.class, ConfigurableUtil.class })
public class NavigationPortletConfigurationActionTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private Set<Group> mockGroupsNavigation;

	@Mock
	private Set<Group> mockGroupsTemplates;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private NavigationPortletInstanceConfiguration mockNavigationPortletInstanceConfiguration;

	@Mock
	private NavigationService mockNavigationService;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletPreferences mockPreferences;

	@Mock
	private Map<Object, Object> mockProperties;

	@InjectMocks
	private NavigationPortletConfigurationAction navigationPortletConfigurationAction;

	@Test
	public void activate_WhenConfigurationFound_ThenDoesNotThrowException() {
		when(ConfigurableUtil.createConfigurable(NavigationPortletInstanceConfiguration.class, mockProperties)).thenReturn(mockNavigationPortletInstanceConfiguration);

		try {
			navigationPortletConfigurationAction.activate(mockProperties);
		} catch (Exception e) {
			fail();
		}
	}

	@Test(expected = IllegalStateException.class)
	public void activate_WhenConfigurationNotFound_ThenThrowsException() {

		when(ConfigurableUtil.createConfigurable(NavigationPortletInstanceConfiguration.class, mockProperties)).thenReturn(null);

		navigationPortletConfigurationAction.activate(mockProperties);
	}

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, ConfigurableUtil.class);
	}

	@Test
	public void include_WhenNoError_ThenAddsNavigationMenuGuestIdAsRequestAttribute() throws Exception {
		long valueFromConfig = 11;
		String expected = "22";
		suppress(methods(DefaultConfigurationAction.class, "include"));
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockPortletRequest.getPreferences()).thenReturn(mockPreferences);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuGroupId()).thenReturn(valueFromConfig);
		when(mockPreferences.getValue("navigationMenuGroupId", String.valueOf(valueFromConfig))).thenReturn(expected);

		navigationPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("navigationMenuGroupId", expected);
	}

	@Test
	public void include_WhenNoError_ThenAddsNavigationMenuIdAsRequestAttribute() throws Exception {
		String expected = "456";
		long valueFromConfig = 123;
		suppress(methods(DefaultConfigurationAction.class, "include"));
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockPortletRequest.getPreferences()).thenReturn(mockPreferences);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(valueFromConfig);
		when(mockPreferences.getValue("navigationMenuId", String.valueOf(valueFromConfig))).thenReturn(expected);

		navigationPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("navigationMenuId", expected);
	}

	@Test
	public void include_WhenNoError_ThenAddsNavigationTypeAsRequestAttribute() throws Exception {
		String expected = "456";
		int valueFromConfig = 123;
		suppress(methods(DefaultConfigurationAction.class, "include"));
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockPortletRequest.getPreferences()).thenReturn(mockPreferences);
		when(mockNavigationPortletInstanceConfiguration.navigationType()).thenReturn(valueFromConfig);
		when(mockPreferences.getValue("navigationType", String.valueOf(valueFromConfig))).thenReturn(expected);

		navigationPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("navigationType", expected);
	}

	@Test
	public void include_WhenNoError_ThenAddsWidgetTemplateFromGuestGroupKeyAsRequestAttribute() throws Exception {
		long valueFromConfig = 11;
		String expected = "22";
		suppress(methods(DefaultConfigurationAction.class, "include"));
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockPortletRequest.getPreferences()).thenReturn(mockPreferences);
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateGroupId()).thenReturn(valueFromConfig);
		when(mockPreferences.getValue("widgetTemplateGroupId", String.valueOf(valueFromConfig))).thenReturn(expected);

		navigationPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("widgetTemplateGroupId", expected);
	}

	@Test
	public void include_WhenNoError_ThenAddsWidgetTemplateKeyAsRequestAttribute() throws Exception {
		String expected = "expectedValue";
		String valueFromConfig = "valueFromConfig";
		suppress(methods(DefaultConfigurationAction.class, "include"));
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockPortletRequest);
		when(mockPortletRequest.getPreferences()).thenReturn(mockPreferences);
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateKey()).thenReturn(valueFromConfig);
		when(mockPreferences.getValue("widgetTemplateKey", valueFromConfig)).thenReturn(expected);

		navigationPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("widgetTemplateKey", expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSetsWidgetTemplateGroupIdPreference() throws Exception {
		long expected = 123;
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		when(ParamUtil.getLong(mockActionRequest, "widgetTemplateGroupId")).thenReturn(expected);
		navigationPortletConfigurationAction = spy(new NavigationPortletConfigurationAction());

		navigationPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(navigationPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "widgetTemplateGroupId", String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsNavigationMenuGroupIdPreference() throws Exception {
		long expected = 123;
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		when(ParamUtil.getLong(mockActionRequest, "navigationMenuGroupId")).thenReturn(expected);
		navigationPortletConfigurationAction = spy(new NavigationPortletConfigurationAction());

		navigationPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(navigationPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "navigationMenuGroupId", String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsNavigationMenuIdPreference() throws Exception {
		long expected = 1234;
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		when(ParamUtil.getLong(mockActionRequest, "navigationMenuId")).thenReturn(expected);
		navigationPortletConfigurationAction = spy(new NavigationPortletConfigurationAction());

		navigationPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(navigationPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "navigationMenuId", String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsNavigationTypePreference() throws Exception {
		int expected = 1234;
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		when(ParamUtil.getInteger(mockActionRequest, "navigationType", SiteNavigationConstants.TYPE_PRIMARY)).thenReturn(expected);
		navigationPortletConfigurationAction = spy(new NavigationPortletConfigurationAction());

		navigationPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(navigationPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "navigationType", String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoErrors_ThenSetsWidgetTemplateKeyPreference() throws Exception {
		String expected = "expectedValue";
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		when(ParamUtil.getString(mockActionRequest, "widgetTemplateKey")).thenReturn(expected);
		navigationPortletConfigurationAction = spy(new NavigationPortletConfigurationAction());

		navigationPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(navigationPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "widgetTemplateKey", String.valueOf(expected));
	}

}
