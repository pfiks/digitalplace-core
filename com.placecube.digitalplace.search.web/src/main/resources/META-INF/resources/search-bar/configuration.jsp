<%@ include file="../init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post">
	
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />


	<liferay-frontend:edit-form-body>
		<dp-frontend:fieldset-group>
			
			<liferay-frontend:fieldset>

				<aui:input
					label="destination-page"
					name="destinationPage"
					type="text"
					value="${ destinationPage }"
				/>
			
			</liferay-frontend:fieldset>

			<liferay-frontend:fieldset>
				<aui:input
					label="search-guest-group"
					name="searchGuestGroup"
					type="toggle-switch"
					value="${ searchGuestGroup }"
				/>
			</liferay-frontend:fieldset>
			
		</dp-frontend:fieldset-group>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>
	
</liferay-frontend:edit-form>
			
