package com.placecube.digitalplace.admin.emailverify.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.TicketConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.TicketLocalService;
import com.liferay.portal.kernel.service.UserLocalService;

@RunWith(PowerMockRunner.class)
public class EmailVerifyTicketServiceTest extends PowerMockito {

	@Mock
	private Ticket mockTicket1;

	@Mock
	private Ticket mockTicket2;

	@Mock
	private Ticket mockTicket3;

	@Mock
	private TicketLocalService mockTicketLocalService;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private EmailVerifyTicketService emailVerifyTicketService;

	@Test
	public void verifyUserEmailAddress_WhenNoError_ThenVerifiesTheUnexpiredUserTickets() throws PortalException {
		long userId = 123;
		long companyId = 321;
		String ticketKey1 = "1";
		String ticketKey3 = "3";
		List<Ticket> tickets = Arrays.asList(mockTicket1, mockTicket2, mockTicket3);

		when(mockTicket2.isExpired()).thenReturn(true);
		when(mockTicket1.getKey()).thenReturn(ticketKey1);
		when(mockTicket3.getKey()).thenReturn(ticketKey3);
		when(mockTicketLocalService.getTickets(companyId, User.class.getName(), userId, TicketConstants.TYPE_EMAIL_ADDRESS)).thenReturn(tickets);

		emailVerifyTicketService.verifyUserEmailAddress(companyId, userId);

		verify(mockUserLocalService, times(1)).verifyEmailAddress(ticketKey1);
		verify(mockUserLocalService, times(1)).verifyEmailAddress(ticketKey3);
		verifyNoMoreInteractions(mockUserLocalService);
	}

	@Test
	public void verifyUserEmailAddress_WhenExceptionVerifyingOneOfTheTickets_ThenVerifiesTheRestOfTheTickets() throws PortalException {
		long userId = 123;
		long companyId = 321;
		String ticketKey1 = "1";
		String ticketKey2 = "2";
		List<Ticket> tickets = Arrays.asList(mockTicket1, mockTicket2);

		when(mockTicket1.getKey()).thenReturn(ticketKey1);
		when(mockTicket2.getKey()).thenReturn(ticketKey2);
		when(mockTicketLocalService.getTickets(companyId, User.class.getName(), userId, TicketConstants.TYPE_EMAIL_ADDRESS)).thenReturn(tickets);
		doThrow(new PortalException()).when(mockUserLocalService).verifyEmailAddress(ticketKey1);

		emailVerifyTicketService.verifyUserEmailAddress(companyId, userId);

		verify(mockUserLocalService, times(1)).verifyEmailAddress(ticketKey1);
		verify(mockUserLocalService, times(1)).verifyEmailAddress(ticketKey2);
		verifyNoMoreInteractions(mockUserLocalService);
	}

	@Test
	public void doesUserHavePendingTickets_WhenUserHasPendingTickets_ThenReturnsTrue() {
		long userId = 123;
		long companyId = 321;
		List<Ticket> tickets = Arrays.asList(mockTicket1, mockTicket2, mockTicket3);

		when(mockTicket2.isExpired()).thenReturn(true);
		when(mockTicketLocalService.getTickets(companyId, User.class.getName(), userId, TicketConstants.TYPE_EMAIL_ADDRESS)).thenReturn(tickets);

		boolean result = emailVerifyTicketService.doesUserHavePendingTickets(companyId, userId);

		assertTrue(result);
	}

	@Test
	public void doesUserHavePendingTickets_WhenUserDoesNotHavePendingTickets_ThenReturnsFalse() {
		long userId = 123;
		long companyId = 321;

		when(mockTicketLocalService.getTickets(companyId, User.class.getName(), userId, TicketConstants.TYPE_EMAIL_ADDRESS)).thenReturn(new ArrayList<>());

		boolean result = emailVerifyTicketService.doesUserHavePendingTickets(companyId, userId);

		assertFalse(result);
	}

}
