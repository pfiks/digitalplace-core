package com.placecube.digitalplace.payment.capita.service.internal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.document.library.kernel.exception.NoSuchFileEntryException;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Repository;
import com.liferay.portal.kernel.portletfilerepository.PortletFileRepository;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.placecube.digitalplace.payment.capita.service.internal.constants.OneOffPaymentOrderConstants;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MimeTypesUtil.class, RestrictionsFactoryUtil.class, PropertyFactoryUtil.class })
public class OneOffPaymentHelperTest {

	private static final long COMPANY_ID = 31232L;

	private static final long GROUP_ID = 3242L;
	private static final long FILE_ENTRY_ID = 984L;
	private static final long FOLDER_ID = 984L;
	private static final long REPOSITORY_ID = 543L;
	private static final long USER_ID = 655L;

	private static final String CONTENT_TYPE = "type";
	private static final String FILE_NAME = "fileName";
	private static final String PAYMENT_REFERENCE = "1111";

	@InjectMocks
	private OneOffPaymentHelper oneOffPaymentHelper;

	@Mock
	private DLFolderLocalService mockDLFolderLocalService;

	@Mock
	private DLFileEntryLocalService mockDLFileEntryLocalService;

	@Mock
	private PortletFileRepository mockPortletFileRepository;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private Criterion mockCriterion;

	@Mock
	private Criterion mockCriterionTwo;

	@Mock
	private DynamicQuery mockDynamicQueryOne;

	@Mock
	private DynamicQuery mockDynamicQueryTwo;

	@Mock
	private DLFileEntry mockDLFileEntry;

	@Mock
	private FileEntry mockFileEntry;

	@Mock
	private FileEntry mockNewFileEntry;

	@Mock
	private File mockFile;

	@Mock
	private Folder mockFolder;

	@Mock
	private Property mockProperty;

	@Mock
	private Repository mockRepository;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activatesetup() {
		mockStatic(MimeTypesUtil.class, RestrictionsFactoryUtil.class, PropertyFactoryUtil.class);
	}

	@Test
	public void addOrReplaceFileEntry_WhenNoErrors_ThenReturnsNewFileEntryForFile() throws PortalException {
		when(MimeTypesUtil.getContentType(mockFile, FILE_NAME)).thenReturn(CONTENT_TYPE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockUserLocalService.getGuestUserId(COMPANY_ID)).thenReturn(USER_ID);
		when(mockFileEntry.getFileEntryId()).thenReturn(FILE_ENTRY_ID);
		when(mockPortletFileRepository.getPortletFileEntry(GROUP_ID, FOLDER_ID, FILE_NAME)).thenReturn(mockFileEntry);

		when(mockPortletFileRepository.addPortletFileEntry(null, GROUP_ID, USER_ID, DDMFormInstance.class.getName(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, DDMFormInstance.class.getName(),
				FOLDER_ID, mockFile, FILE_NAME, CONTENT_TYPE, true)).thenReturn(mockNewFileEntry);

		FileEntry result = oneOffPaymentHelper.addOrReplaceFileEntry(FILE_NAME, mockFile, mockFolder, mockServiceContext);

		assertThat(result, sameInstance(mockNewFileEntry));

		InOrder inOrder = inOrder(mockPortletFileRepository);

		inOrder.verify(mockPortletFileRepository, times(1)).deletePortletFileEntry(FILE_ENTRY_ID);
		inOrder.verify(mockPortletFileRepository, times(1)).addPortletFileEntry(null, GROUP_ID, USER_ID, DDMFormInstance.class.getName(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
				DDMFormInstance.class.getName(), FOLDER_ID, mockFile, FILE_NAME, CONTENT_TYPE, true);
	}

	@Test
	public void addOrReplaceFileEntry_WhenOldFileEntryExists_ThenDeletesOldFileEntryAndCreatesNewOneInOrder() throws PortalException {
		when(MimeTypesUtil.getContentType(mockFile, FILE_NAME)).thenReturn(CONTENT_TYPE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockUserLocalService.getGuestUserId(COMPANY_ID)).thenReturn(USER_ID);
		when(mockFileEntry.getFileEntryId()).thenReturn(FILE_ENTRY_ID);
		when(mockPortletFileRepository.getPortletFileEntry(GROUP_ID, FOLDER_ID, FILE_NAME)).thenReturn(mockFileEntry);

		oneOffPaymentHelper.addOrReplaceFileEntry(FILE_NAME, mockFile, mockFolder, mockServiceContext);

		InOrder inOrder = inOrder(mockPortletFileRepository);

		inOrder.verify(mockPortletFileRepository, times(1)).deletePortletFileEntry(FILE_ENTRY_ID);
		inOrder.verify(mockPortletFileRepository, times(1)).addPortletFileEntry(null, GROUP_ID, USER_ID, DDMFormInstance.class.getName(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
				DDMFormInstance.class.getName(), FOLDER_ID, mockFile, FILE_NAME, CONTENT_TYPE, true);
	}

	@Test
	public void addOrReplaceFileEntry_WhenOldFileEntryDoesNotExist_ThenCreatesNewFileEntry() throws PortalException {
		when(MimeTypesUtil.getContentType(mockFile, FILE_NAME)).thenReturn(CONTENT_TYPE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockUserLocalService.getGuestUserId(COMPANY_ID)).thenReturn(USER_ID);
		when(mockPortletFileRepository.getPortletFileEntry(GROUP_ID, FOLDER_ID, FILE_NAME)).thenThrow(new NoSuchFileEntryException());

		oneOffPaymentHelper.addOrReplaceFileEntry(FILE_NAME, mockFile, mockFolder, mockServiceContext);

		verify(mockPortletFileRepository, times(1)).addPortletFileEntry(null, GROUP_ID, USER_ID, DDMFormInstance.class.getName(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
				DDMFormInstance.class.getName(), FOLDER_ID, mockFile, FILE_NAME, CONTENT_TYPE, true);
	}

	@Test(expected = PortalException.class)
	public void addOrReplaceFileEntry_WhenFileEntryCreationFails_ThenThrowsPortalException() throws PortalException {
		when(MimeTypesUtil.getContentType(mockFile, FILE_NAME)).thenReturn(CONTENT_TYPE);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockUserLocalService.getGuestUserId(COMPANY_ID)).thenReturn(USER_ID);
		when(mockPortletFileRepository.getPortletFileEntry(GROUP_ID, FOLDER_ID, FILE_NAME)).thenThrow(new NoSuchFileEntryException());

		when(mockPortletFileRepository.addPortletFileEntry(null, GROUP_ID, USER_ID, DDMFormInstance.class.getName(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, DDMFormInstance.class.getName(),
				FOLDER_ID, mockFile, FILE_NAME, CONTENT_TYPE, true)).thenThrow(new PortalException());

		oneOffPaymentHelper.addOrReplaceFileEntry(FILE_NAME, mockFile, mockFolder, mockServiceContext);
	}

	@Test
	public void createPaymentTempFile_WhenNoErrors_ThenCreatesFileFromPaymentReference() throws Exception {
		OneOffPaymentOrder payment = new OneOffPaymentOrder();
		payment.setInternalReference("1234");

		File result = oneOffPaymentHelper.createPaymentTempFile(payment);

		assertThat(result.getName().startsWith("1234"), equalTo(true));

		Files.delete(result.toPath());
	}

	@Test
	public void getOrAddPaymentTempFilesFolder_WhenRepositoryAndFolderExist_ThenReturnsFolder() throws Exception {
		when(mockRepository.getRepositoryId()).thenReturn(REPOSITORY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockPortletFileRepository.getPortletRepository(GROUP_ID, DDMFormInstance.class.getName())).thenReturn(mockRepository);
		when(mockPortletFileRepository.getPortletFolder(REPOSITORY_ID, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME)).thenReturn(mockFolder);

		Folder result = oneOffPaymentHelper.getOrAddPaymentTempFilesFolder(mockServiceContext);

		assertThat(result, sameInstance(mockFolder));
	}

	@Test
	public void getOrAddPaymentTempFilesFolder_WhenFolderDoesNotExist_ThenCreatesFolder() throws Exception {
		when(mockRepository.getRepositoryId()).thenReturn(REPOSITORY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockPortletFileRepository.getPortletRepository(GROUP_ID, DDMFormInstance.class.getName())).thenReturn(mockRepository);
		when(mockPortletFileRepository.getPortletFolder(REPOSITORY_ID, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME))
				.thenThrow(new PortalException());
		when(mockPortletFileRepository.addPortletFolder(USER_ID, REPOSITORY_ID, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME, mockServiceContext))
				.thenReturn(mockFolder);

		Folder result = oneOffPaymentHelper.getOrAddPaymentTempFilesFolder(mockServiceContext);

		assertThat(result, sameInstance(mockFolder));

		InOrder inOrder = inOrder(mockPortletFileRepository, mockServiceContext);
		inOrder.verify(mockServiceContext, times(1)).setAddGuestPermissions(true);
		inOrder.verify(mockPortletFileRepository, times(1)).addPortletFolder(USER_ID, REPOSITORY_ID, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME,
				mockServiceContext);

	}

	@Test
	public void getOrAddPaymentTempFilesFolder_WhenRepositoryDoesNotExist_ThenCreatesRepositoryAndReturnsFolder() throws Exception {
		when(mockRepository.getRepositoryId()).thenReturn(REPOSITORY_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockPortletFileRepository.getPortletRepository(GROUP_ID, DDMFormInstance.class.getName())).thenThrow(new PortalException());
		when(mockPortletFileRepository.addPortletRepository(GROUP_ID, DDMFormInstance.class.getName(), mockServiceContext)).thenReturn(mockRepository);
		when(mockPortletFileRepository.getPortletFolder(REPOSITORY_ID, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME)).thenReturn(mockFolder);

		Folder result = oneOffPaymentHelper.getOrAddPaymentTempFilesFolder(mockServiceContext);

		assertThat(result, sameInstance(mockFolder));

		InOrder inOrder = inOrder(mockPortletFileRepository, mockServiceContext);
		inOrder.verify(mockServiceContext, times(1)).setAddGuestPermissions(true);
		inOrder.verify(mockPortletFileRepository, times(1)).addPortletRepository(GROUP_ID, DDMFormInstance.class.getName(), mockServiceContext);
	}

	@Test
	public void getPortletFileEntry_WhenPortletFileEntryCanBeRetrievedByPaymentReference_ThenReturnsFileEntry() throws Exception {
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockFolder.getGroupId()).thenReturn(GROUP_ID);
		when(mockPortletFileRepository.getPortletFileEntry(GROUP_ID, FOLDER_ID, PAYMENT_REFERENCE)).thenReturn(mockFileEntry);

		FileEntry result = oneOffPaymentHelper.getPortletFileEntry(PAYMENT_REFERENCE, mockFolder);

		assertThat(result, sameInstance(mockFileEntry));
	}

	@Test
	public void getPortletFileEntry_WhenPortletFileEntryCannotBeRetrievedByPaymentReference_ThenReturnsFileEntryFromDatabase() throws Exception {
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockFolder.getGroupId()).thenReturn(GROUP_ID);
		when(mockPortletFileRepository.getPortletFileEntry(GROUP_ID, FOLDER_ID, PAYMENT_REFERENCE)).thenThrow(new PortalException());

		when(mockDLFolderLocalService.dynamicQuery()).thenReturn(mockDynamicQueryOne);
		when(RestrictionsFactoryUtil.eq(Field.NAME, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME)).thenReturn(mockCriterion);
		when(PropertyFactoryUtil.forName(Field.FOLDER_ID)).thenReturn(mockProperty);
		when(mockDLFolderLocalService.dynamicQuery(mockDynamicQueryOne)).thenReturn(Arrays.asList(FOLDER_ID));

		when(mockDLFileEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQueryTwo);
		when(mockDLFileEntryLocalService.dynamicQuery(mockDynamicQueryTwo)).thenReturn(Arrays.asList(mockDLFileEntry));
		when(mockDLFileEntry.getFileEntryId()).thenReturn(FILE_ENTRY_ID);

		when(mockPortletFileRepository.getPortletFileEntry(FILE_ENTRY_ID)).thenReturn(mockFileEntry);

		FileEntry result = oneOffPaymentHelper.getPortletFileEntry(PAYMENT_REFERENCE, mockFolder);

		assertThat(result, sameInstance(mockFileEntry));
	}

	@Test
	public void getPortletFileEntry_WhenPortletFileEntryCannotBeRetrievedByPaymentReference_ThenRetrievesFileFromTemporaryFolder() throws Exception {
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockFolder.getGroupId()).thenReturn(GROUP_ID);
		when(mockPortletFileRepository.getPortletFileEntry(GROUP_ID, FOLDER_ID, PAYMENT_REFERENCE)).thenThrow(new PortalException());

		when(mockDLFolderLocalService.dynamicQuery()).thenReturn(mockDynamicQueryOne);
		when(RestrictionsFactoryUtil.eq(Field.NAME, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME)).thenReturn(mockCriterion);
		when(PropertyFactoryUtil.forName(Field.FOLDER_ID)).thenReturn(mockProperty);
		when(mockDLFolderLocalService.dynamicQuery(mockDynamicQueryOne)).thenReturn(Arrays.asList(FOLDER_ID));

		when(mockDLFileEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQueryTwo);
		when(mockDLFileEntryLocalService.dynamicQuery(mockDynamicQueryTwo)).thenReturn(Arrays.asList(mockDLFileEntry));
		when(mockDLFileEntry.getFileEntryId()).thenReturn(FILE_ENTRY_ID);

		oneOffPaymentHelper.getPortletFileEntry(PAYMENT_REFERENCE, mockFolder);

		InOrder inOrder = inOrder(mockDynamicQueryOne, mockDLFolderLocalService);
		inOrder.verify(mockDynamicQueryOne, times(1)).add(mockCriterion);
		inOrder.verify(mockDynamicQueryOne, times(1)).setProjection(mockProperty);
		inOrder.verify(mockDLFolderLocalService, times(1)).dynamicQuery(mockDynamicQueryOne);
	}

	@Test
	public void getPortletFileEntry_WhenPortletFileEntryCannotBeRetrievedByPaymentReference_ThenRetrievesFileFromTemporaryFolderByPaymentReferenceFileName() throws Exception {
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockFolder.getGroupId()).thenReturn(GROUP_ID);
		when(mockFolder.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockPortletFileRepository.getPortletFileEntry(GROUP_ID, FOLDER_ID, PAYMENT_REFERENCE)).thenThrow(new PortalException());

		when(mockDLFolderLocalService.dynamicQuery()).thenReturn(mockDynamicQueryOne);
		when(mockDLFolderLocalService.dynamicQuery(mockDynamicQueryOne)).thenReturn(Arrays.asList(FOLDER_ID));

		when(mockDLFileEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQueryTwo);
		when(RestrictionsFactoryUtil.eq(Field.COMPANY_ID, COMPANY_ID)).thenReturn(mockCriterion);
		when(RestrictionsFactoryUtil.in(Field.FOLDER_ID, Arrays.asList(FOLDER_ID))).thenReturn(mockCriterionTwo);
		when(mockDLFileEntryLocalService.dynamicQuery(mockDynamicQueryTwo)).thenReturn(Arrays.asList(mockDLFileEntry));
		when(mockDLFileEntry.getFileEntryId()).thenReturn(FILE_ENTRY_ID);

		oneOffPaymentHelper.getPortletFileEntry(PAYMENT_REFERENCE, mockFolder);

		InOrder inOrder = inOrder(mockDynamicQueryTwo, mockDLFileEntryLocalService);
		inOrder.verify(mockDynamicQueryTwo, times(1)).add(mockCriterion);
		inOrder.verify(mockDynamicQueryTwo, times(1)).add(mockCriterionTwo);
		inOrder.verify(mockDLFileEntryLocalService, times(1)).dynamicQuery(mockDynamicQueryTwo);
	}

	@Test(expected = PortalException.class)
	public void getPortletFileEntry_WhenPortletFileEntryCannotBeRetrievedNeitherByPaymentReferenceNorFromDataBase_ThenThrowsPortalException() throws Exception {
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockFolder.getGroupId()).thenReturn(GROUP_ID);
		when(mockPortletFileRepository.getPortletFileEntry(GROUP_ID, FOLDER_ID, PAYMENT_REFERENCE)).thenThrow(new PortalException());

		when(mockDLFolderLocalService.dynamicQuery()).thenReturn(mockDynamicQueryOne);
		when(mockDLFolderLocalService.dynamicQuery(mockDynamicQueryOne)).thenReturn(Arrays.asList(FOLDER_ID));

		when(mockDLFileEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQueryTwo);
		when(mockDLFileEntryLocalService.dynamicQuery(mockDynamicQueryTwo)).thenReturn(Collections.emptyList());

		oneOffPaymentHelper.getPortletFileEntry(PAYMENT_REFERENCE, mockFolder);
	}
}
