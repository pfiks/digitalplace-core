package com.placecube.digitalplace.notifications.smsworksnotificationservice.service;

import java.util.Map;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.notifications.model.SendSmsContext;
import com.placecube.digitalplace.notifications.smsworksnotificationservice.configuration.SmsWorksNotificationServiceConfiguration;

@Component(immediate = true, service = SmsWorksNotificationsService.class)
public class SmsWorksNotificationsService {

	private static final int MAX_SENDER_LENGTH = 11;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JSONFactory jsonFactory;

	public String getBody(SendSmsContext sendSmsContext, String content) {
		JSONObject jsonObject = jsonFactory.createJSONObject();
		jsonObject.put("sender", getSmsSenderId(sendSmsContext));
		jsonObject.put("destination", sendSmsContext.getPhoneNumber());
		jsonObject.put("content", content);
		jsonObject.put("schedule", StringPool.BLANK);
		jsonObject.put("tag", sendSmsContext.getReference());
		return jsonObject.toJSONString();
	}

	public SmsWorksNotificationServiceConfiguration getConfiguration(long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(SmsWorksNotificationServiceConfiguration.class, companyId);
	}

	public String getContentWithPlaceholderValues(SendSmsContext sendSmsContext) {
		String text = sendSmsContext.getTemplateId();
		Map<String, Object> variables = sendSmsContext.getPersonalisation();
		for (Map.Entry<String, Object> variable : variables.entrySet()) {
			text = text.replace("$" + variable.getKey() + "$", GetterUtil.getString(variable.getValue()));
		}
		return text;
	}

	public HttpPost getHttpPost(SmsWorksNotificationServiceConfiguration configuration) {
		return new HttpPost(configuration.endpoint());
	}

	public StringEntity getStringEntity(String body) {
		return new StringEntity(body, ContentType.create("application/json"));
	}

	public boolean isSmsEnabled(long companyId) throws ConfigurationException {
		SmsWorksNotificationServiceConfiguration configuration = getConfiguration(companyId);
		return configuration.smsEnabled();
	}

	private String getSmsSenderId(SendSmsContext sendSmsContext) {
		String sender = sendSmsContext.getSmsSenderId();
		if (Validator.isNotNull(sender) && sender.length() > MAX_SENDER_LENGTH) {
			sender = sender.substring(0, MAX_SENDER_LENGTH);
		}
		return sender;
	}
}
