package com.placecube.digitalplace.ddl.web.portlet.ddlsearch;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLSearchListingsPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.model.DDLDisplayEntry;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.service.DDLSearchListingsService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;
import com.placecube.digitalplace.search.shared.constants.SharedSearchDisplayCostants;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchDisplayService;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.dao.search.SearchContainer")
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DDLSearchListingsPortletTest extends PowerMockito {

	public static final String DISPLAY_STYLE_GEO_POINT = "geo-point";

	@InjectMocks
	private DDLSearchListingsPortlet ddlSearchListingsPortlet;

	@Mock
	private DDLConfigurationService mockDDLConfigurationService;

	@Mock
	private List<DDLDisplayEntry> mockDDLDisplayEntryList;

	@Mock
	private DDLSearchListingsPortletInstanceConfiguration mockDDLSearchListingsPortletInstanceConfiguration;

	@Mock
	private DDLSearchListingsService mockDDLSearchListingsService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private Document mockDocument;

	@Mock
	private DynamicDataListMapping mockDynamicDataListMapping;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchContainer mockSearchContainer;

	@Mock
	private SharedSearchDisplayService mockSharedSearchDisplayService;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test
	public void render_WhenDynamicDataListMappingFoundAndDisplayStyleIsGeoPoint_ThenNotConfiguresTheSearchContainer() throws Exception {
		long templateId = 456;
		Document[] searchResults = new Document[] { mockDocument };
		mockSearchExecuted();
		when(mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle()).thenReturn(DISPLAY_STYLE_GEO_POINT);
		when(mockSharedSearchDisplayService.getDisplayStyle(mockRenderRequest, mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle())).thenReturn(DISPLAY_STYLE_GEO_POINT);
		when(mockDynamicDataListMapping.getSearchDefaultGeopointViewTemplateId()).thenReturn(templateId);
		when(mockSharedSearchResponse.getSearchResults()).thenReturn(searchResults);
		when(mockDynamicDataListMapping.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);

		ddlSearchListingsPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq("searchContainer"), any());
		verify(mockRenderRequest, never()).setAttribute(eq("hidePagination"), any());
	}

	@Test
	public void render_WhenDynamicDataListMappingFoundAndDisplayStyleIsGeoPoint_ThenSetsDisplayStyleGeoPointAsRequestAttribute() throws Exception {
		long templateId = 456;
		Document[] searchResults = new Document[] { mockDocument };
		String geoPointContent = "geo-point-content";
		mockSearchExecuted();
		when(mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle()).thenReturn(DISPLAY_STYLE_GEO_POINT);
		when(mockSharedSearchDisplayService.getDisplayStyle(mockRenderRequest, mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle())).thenReturn(DISPLAY_STYLE_GEO_POINT);
		when(mockDynamicDataListMapping.getSearchDefaultGeopointViewTemplateId()).thenReturn(templateId);
		when(mockSharedSearchResponse.getSearchResults()).thenReturn(searchResults);
		when(mockDynamicDataListMapping.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDynamicDataListMapping.getSearchDefaultGeopointViewTemplate()).thenReturn(Optional.of(mockDDMTemplate));
		when(mockDDLSearchListingsService.getTransformedTemplate(searchResults, mockDDMStructure, mockDDMTemplate, mockThemeDisplay, mockRenderRequest)).thenReturn(geoPointContent);

		ddlSearchListingsPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("displayStyleListGeoPoint", true);

	}

	@Test
	public void render_WhenDynamicDataListMappingFoundAndDisplayStyleIsGeoPoint_ThenSetsTransformedGeoPointTemplateAsRequestAttribute() throws Exception {
		long templateId = 456;
		Document[] searchResults = new Document[] { mockDocument };
		String geoPointContent = "geo-point-content";
		mockSearchExecuted();
		when(mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle()).thenReturn(DISPLAY_STYLE_GEO_POINT);
		when(mockSharedSearchDisplayService.getDisplayStyle(mockRenderRequest, mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle())).thenReturn(DISPLAY_STYLE_GEO_POINT);
		when(mockDynamicDataListMapping.getSearchDefaultGeopointViewTemplateId()).thenReturn(templateId);
		when(mockSharedSearchResponse.getSearchResults()).thenReturn(searchResults);
		when(mockDynamicDataListMapping.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDynamicDataListMapping.getSearchDefaultGeopointViewTemplate()).thenReturn(Optional.of(mockDDMTemplate));
		when(mockDDLSearchListingsService.getTransformedTemplate(searchResults, mockDDMStructure, mockDDMTemplate, mockThemeDisplay, mockRenderRequest)).thenReturn(geoPointContent);

		ddlSearchListingsPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("geoPointContent", geoPointContent);

	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenDynamicDataListMappingFoundAndDisplayStyleIsGrid_ThenConfiguresTheSearchContainerWithResultsForTheGridViewTemplateAndTheDisplayStyleAttributes(boolean hidePagination)
			throws Exception {
		final String namespace = PortletKeys.DDL_SEARCH_LISTINGS_PORTLET;
		Document[] searchResults = new Document[] { mockDocument };
		String gridDisplayStyleCss = "gridDisplayStyleCssValue";
		when(mockDDLSearchListingsPortletInstanceConfiguration.gridDisplayStyleCss()).thenReturn(gridDisplayStyleCss);
		mockSearchExecuted();
		when(mockRenderResponse.getNamespace()).thenReturn(namespace);
		when(mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle()).thenReturn(SharedSearchDisplayCostants.DISPLAY_STYLE_LIST);
		when(mockSharedSearchDisplayService.getDisplayStyle(mockRenderRequest, mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle()))
				.thenReturn(SharedSearchDisplayCostants.DISPLAY_STYLE_GRID);
		when(mockDDLSearchListingsPortletInstanceConfiguration.hidePagination()).thenReturn(hidePagination);
		when(mockDynamicDataListMapping.getSearchDefaultGridViewTemplate()).thenReturn(Optional.of(mockDDMTemplate));
		when(mockSharedSearchResponse.getSearchResults()).thenReturn(searchResults);
		when(mockDynamicDataListMapping.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockDDLSearchListingsService.getDDLDisplayEntryResults(searchResults, mockDDMStructure, Optional.of(mockDDMTemplate), mockThemeDisplay, mockHttpServletRequest))
				.thenReturn(mockDDLDisplayEntryList);

		when(mockSharedSearchResponse.getSearchContainerForResults(namespace + "_ddlListingsSearchContainer", mockDDLDisplayEntryList, "no-entries-were-found")).thenReturn(mockSearchContainer);

		ddlSearchListingsPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("gridDisplayStyleCss", gridDisplayStyleCss);
		verify(mockRenderRequest, times(1)).setAttribute("searchContainer", mockSearchContainer);
		verify(mockRenderRequest, times(1)).setAttribute("hidePagination", hidePagination);
		verify(mockRenderRequest, times(1)).setAttribute("displayStyleList", false);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenDynamicDataListMappingFoundAndDisplayStyleIsList_ThenConfiguresTheSearchContainerWithResultsForTheListViewTemplateAndTheDisplayStyleAttributes(boolean hidePagination)
			throws Exception {
		final String namespace = PortletKeys.DDL_SEARCH_LISTINGS_PORTLET;
		Document[] searchResults = new Document[] { mockDocument };
		mockSearchExecuted();
		String gridDisplayStyleCss = "gridDisplayStyleCssValue";
		when(mockDDLSearchListingsPortletInstanceConfiguration.gridDisplayStyleCss()).thenReturn(gridDisplayStyleCss);
		when(mockRenderResponse.getNamespace()).thenReturn(namespace);
		when(mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle()).thenReturn(SharedSearchDisplayCostants.DISPLAY_STYLE_LIST);
		when(mockSharedSearchDisplayService.getDisplayStyle(mockRenderRequest, mockDDLSearchListingsPortletInstanceConfiguration.defaultDisplayStyle()))
				.thenReturn(SharedSearchDisplayCostants.DISPLAY_STYLE_LIST);
		when(mockDDLSearchListingsPortletInstanceConfiguration.hidePagination()).thenReturn(hidePagination);
		when(mockDynamicDataListMapping.getSearchDefaultListViewTemplate()).thenReturn(Optional.of(mockDDMTemplate));
		when(mockSharedSearchResponse.getSearchResults()).thenReturn(searchResults);
		when(mockDynamicDataListMapping.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockDDLSearchListingsService.getDDLDisplayEntryResults(searchResults, mockDDMStructure, Optional.of(mockDDMTemplate), mockThemeDisplay, mockHttpServletRequest))
				.thenReturn(mockDDLDisplayEntryList);
		when(mockSharedSearchResponse.getSearchContainerForResults(namespace + "_ddlListingsSearchContainer", mockDDLDisplayEntryList, "no-entries-were-found")).thenReturn(mockSearchContainer);

		ddlSearchListingsPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("gridDisplayStyleCss", gridDisplayStyleCss);
		verify(mockRenderRequest, times(1)).setAttribute("searchContainer", mockSearchContainer);
		verify(mockRenderRequest, times(1)).setAttribute("hidePagination", hidePagination);
		verify(mockRenderRequest, times(1)).setAttribute("displayStyleList", true);
	}

	@Test(expected = PortletException.class)
	public void render_WhenDynamicDataListMappingFoundAndExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		long companyId = 1;
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLSearchListingsPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockDDLSearchListingsPortletInstanceConfiguration);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockDDLConfigurationService.getDynamicDataListMappingForConfiguredStructure(companyId, mockDDLSearchListingsPortletInstanceConfiguration))
				.thenReturn(Optional.of(mockDynamicDataListMapping));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		ddlSearchListingsPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenDynamicDataListMappingNotFound_ThenSetsInvalidConfigurationTrueAsRequestAttributeAndDoesNotPerformASearch() throws Exception {
		long companyId = 1;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLSearchListingsPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockDDLSearchListingsPortletInstanceConfiguration);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockDDLConfigurationService.getDynamicDataListMappingForConfiguredStructure(companyId, mockDDLSearchListingsPortletInstanceConfiguration)).thenReturn(Optional.empty());

		ddlSearchListingsPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
		verifyZeroInteractions(mockSharedSearchRequestService);
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionRetrievingTheConfiguration_ThenThrowsPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLSearchListingsPortletInstanceConfiguration(mockThemeDisplay)).thenThrow(new ConfigurationException());

		ddlSearchListingsPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	private void mockSearchExecuted() throws ConfigurationException, SearchException {
		long companyId = 1;
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLSearchListingsPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockDDLSearchListingsPortletInstanceConfiguration);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockDDLConfigurationService.getDynamicDataListMappingForConfiguredStructure(companyId, mockDDLSearchListingsPortletInstanceConfiguration))
				.thenReturn(Optional.of(mockDynamicDataListMapping));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
	}
}
