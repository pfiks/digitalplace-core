package com.placecube.digitalplace.appointment.internal.upgrade.steps;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class Upgrade_1_0_1_AppointmentEntryTableIndexesUpdate extends UpgradeProcess {

	public Upgrade_1_0_1_AppointmentEntryTableIndexesUpdate() {
	}

	@Override
	protected void doUpgrade() throws Exception {
		if (hasTable("Placecube_Appointment_AppointmentEntry")) {

			createNewIndex();

			dropLegacyIndexImmediately();
		}
	}

	private void createNewIndex() throws Exception {
		if (!hasIndex("Placecube_Appointment_AppointmentEntry", "IX_F6746F38")) {
			runSQL("create index IX_F6746F38 on Placecube_Appointment_AppointmentEntry (uuid_[$COLUMN_LENGTH:75$]);");
		}
	}

	private void dropLegacyIndexImmediately() throws Exception {
		if (hasIndex("Placecube_Appointment_AppointmentEntry", "IX_CC1661F0")) {
			runSQL(connection, "drop index IX_CC1661F0 on Placecube_Appointment_AppointmentEntry;");
		}
	}

}
