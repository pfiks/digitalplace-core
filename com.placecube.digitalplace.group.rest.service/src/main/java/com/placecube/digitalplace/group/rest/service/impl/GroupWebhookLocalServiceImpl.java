/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.group.rest.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.group.rest.model.GroupWebhook;
import com.placecube.digitalplace.group.rest.service.base.GroupWebhookLocalServiceBaseImpl;

/**
 * The implementation of the group webhook local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.placecube.digitalplace.group.rest.service.GroupWebhookLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GroupWebhookLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=com.placecube.digitalplace.group.rest.model.GroupWebhook",
	service = AopService.class
)
public class GroupWebhookLocalServiceImpl
	extends GroupWebhookLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>com.placecube.digitalplace.group.rest.service.GroupWebhookLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.placecube.digitalplace.group.rest.service.GroupWebhookLocalServiceUtil</code>.
	 */

	/**
	 * Returns all the webhooks configured for the group
	 *
	 * @param groupId the groupId
	 * @param start pagination start
	 * @param end pagination end
	 * @return list of webhooks
	 */
	public List<GroupWebhook> getGroupWebhooks(long groupId, int start, int end) {
		return groupWebhookPersistence.findByGroupId(groupId, start, end);
	}

	/**
	 * Returns the total number of webhooks for the group
	 *
	 * @param groupId the groupId
	 * @return total count
	 */
	public int getGroupWebhooksCount(long groupId) {
		return groupWebhookPersistence.countByGroupId(groupId);
	}

	/**
	 * Returns a list of all the enabled webhooks configured for the specified
	 * className, classPK and event
	 *
	 * @param groupId the groupId
	 * @param className the className to match
	 * @param classPK the classPK to match
	 * @param event the event
	 * @return list of enabled webhooks
	 */
	public List<GroupWebhook> getConfiguredGroupWebhooks(long groupId, String className, long classPK, String event) {
		List<GroupWebhook> webhooksFound = groupWebhookPersistence.findByGroupId_Enabled_ClassName_ClassPK(groupId, true, className, classPK);
		return webhooksFound.stream().filter(webhook -> webhook.getConfiguredEvents().contains(event)).collect(Collectors.toList());
	}

	/**
	 * If a webhook exists for the specified id, then it returns the webhook. If
	 * no webhook is found, then returns a newly created webhoo
	 *
	 * @param webhookId the id to match
	 * @return webhook
	 */
	public GroupWebhook getOrCreateGroupWebhook(long webhookId) {
		try {
			return groupWebhookLocalService.getGroupWebhook(webhookId);
		} catch (PortalException e) {
			return groupWebhookLocalService.createGroupWebhook(counterLocalService.increment(GroupWebhook.class.getName()));
		}
	}
}