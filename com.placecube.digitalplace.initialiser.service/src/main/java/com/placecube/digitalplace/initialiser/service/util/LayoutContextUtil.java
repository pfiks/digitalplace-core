package com.placecube.digitalplace.initialiser.service.util;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.initializer.model.LayoutContext;

@Component(immediate = true, service = LayoutContextUtil.class)
public class LayoutContextUtil {

	@Reference
	private ParserUtil parserUtil;

	public void addCommonPageDetails(Class clazz, JSONObject pageJSONObject, LayoutContext layoutContext) {
		layoutContext.setAddGuestPermissions(pageJSONObject.getBoolean("addGuestPermissions"));
		layoutContext.setTitle(pageJSONObject.getString("pageTitle"));
		layoutContext.setHidden(pageJSONObject.getBoolean("hidden", false));
		layoutContext.setParentLayoutFriendlyURL(pageJSONObject.getString("parentFriendlyURL"));
		layoutContext.setExpandoValues(parserUtil.getExpandoValues(pageJSONObject));

		configureNameMap(layoutContext, pageJSONObject);
		configureTitleMap(layoutContext, pageJSONObject);

		String pageIconPath = pageJSONObject.getString("pageIconPath");
		if (Validator.isNotNull(pageIconPath)) {
			layoutContext.setPageIcon(clazz.getClassLoader().getResourceAsStream(pageIconPath));
		}

		layoutContext.setRolePermissionsToRemove(parserUtil.getPagePermissions(pageJSONObject, "rolePermissionsToRemove"));
		layoutContext.setRolePermissionsToAdd(parserUtil.getPagePermissions(pageJSONObject, "rolePermissionsToAdd"));
	}

	private void configureNameMap(LayoutContext layoutContext, JSONObject pageJSONObject) {
		JSONArray pageNames = pageJSONObject.getJSONArray("localisedPageName");
		if (Validator.isNotNull(pageNames)) {
			for (int i = 0; i < pageNames.length(); i++) {
				JSONObject jsonObject = pageNames.getJSONObject(i);
				Locale fromLanguageId = LocaleUtil.fromLanguageId(jsonObject.getString("languageId"), true, false);
				if (Validator.isNotNull(fromLanguageId)) {
					layoutContext.addLocalisedName(fromLanguageId, jsonObject.getString("value"));
				}
			}
		}
	}

	private void configureTitleMap(LayoutContext layoutContext, JSONObject pageJSONObject) {
		JSONArray pageTitles = pageJSONObject.getJSONArray("localisedPageTitle");
		if (Validator.isNotNull(pageTitles)) {
			for (int i = 0; i < pageTitles.length(); i++) {
				JSONObject jsonObject = pageTitles.getJSONObject(i);
				Locale fromLanguageId = LocaleUtil.fromLanguageId(jsonObject.getString("languageId"), true, false);
				if (Validator.isNotNull(fromLanguageId)) {
					layoutContext.addLocalisedTitle(fromLanguageId, jsonObject.getString("value"));
				}
			}
		}
	}
}
