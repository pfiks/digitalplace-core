package com.placecube.digitalplace.group.rest.application.endpoints;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.group.rest.application.GroupRestApplication;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestException;
import com.placecube.digitalplace.group.rest.application.model.FileAttachment;
import com.placecube.digitalplace.group.rest.application.model.RestContentEntry;
import com.placecube.digitalplace.group.rest.application.model.RestContentEntryBuilder;
import com.placecube.digitalplace.group.rest.application.model.RestContentUser;
import com.placecube.digitalplace.group.rest.application.model.RestListResult;
import com.placecube.digitalplace.group.rest.application.service.GroupContentService;
import com.placecube.digitalplace.group.rest.constants.GroupRestKeyType;
import com.placecube.digitalplace.group.rest.exception.GroupRestKeyException;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RestListResult.class)
public class GroupContentRestEndpointTest extends PowerMockito {

	private static final String API_KEY_VALUE = "API_KEY_VALUE";

	private GroupContentRestEndpoint groupContentRestEndpoint;

	@Mock
	private AssetCategory mockAssetCategoryOne;

	@Mock
	private AssetCategory mockAssetCategoryTwo;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupApiKeyLocalService mockGroupApiKeyLocalService;

	@Mock
	private GroupContentService mockGroupContentService;

	@Mock
	private GroupRestApplication mockGroupRestApplication;

	@Mock
	private RestContentEntryBuilder mockRestContentEntryBuilder;

	@Mock
	private RestContentEntry mockRestContentEntryOne;

	@Mock
	private RestContentEntry mockRestContentEntryTwo;

	@Mock
	private RestListResult mockRestListResult;

	@Mock
	private Set<FileAttachment> mockAttachments;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private DDLRecordSet mockDDLRecordSet1;

	@Mock
	private DDLRecordSet mockDDLRecordSet2;

	@Mock
	private User mockUser;

	@Mock
	private RestContentUser mockRestContentUser;

	@Before
	public void activateSetUp() {
		mockStatic(RestListResult.class);

		when(mockGroupRestApplication.getGroupApiKeyLocalService()).thenReturn(mockGroupApiKeyLocalService);
		when(mockGroupRestApplication.getGroupContentService()).thenReturn(mockGroupContentService);
		when(mockGroupRestApplication.getRestContentEntryBuilder()).thenReturn(mockRestContentEntryBuilder);
		groupContentRestEndpoint = new GroupContentRestEndpoint(mockGroupRestApplication);
	}

	@Test(expected = GroupRestException.class)
	public void addDynamicContentDetails_WhenErrorAddingDDLRecord_ThenThrowsException() throws Exception {

		long userId = 1L;
		long groupId = 2L;
		long ddlRecordSetId = 3L;
		String ddlRecordJSON = "{\"key\":\"value\"}";
		String fileAttachmentsJSON = "myFileAttachments";
		long[] categoryIds = { 11, 22, 33 };
		String[] tagNames = { "Tag 1", "Tag 2", "Tag 3" };
		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenReturn(mockGroup);
		when(mockGroupContentService.getDDLRecordSet(ddlRecordSetId)).thenReturn(mockDDLRecordSet1);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockGroupContentService.getServiceContextAndRunAsUser(mockGroup, userId)).thenReturn(mockServiceContext);
		when(mockGroupContentService.uploadDDLAttachments(mockGroup, fileAttachmentsJSON, mockDDLRecordSet1, mockServiceContext)).thenReturn(mockAttachments);
		when(mockGroupContentService.addDDLRecord(userId, groupId, mockDDLRecordSet1, ddlRecordJSON, mockAttachments, categoryIds, tagNames, mockServiceContext)).thenThrow(new PortalException());

		groupContentRestEndpoint.addDynamicContentDetails(userId, ddlRecordSetId, ddlRecordJSON, fileAttachmentsJSON, categoryIds, tagNames, API_KEY_VALUE);
	}

	@Test(expected = GroupRestException.class)
	public void addDynamicContentDetails_WhenErrorRetrievingDynamicContentFullDetails_ThenThrowsException() throws Exception {

		long userId = 1L;
		long groupId = 2L;
		long ddlRecordSetId = 3L;
		String ddlRecordJSON = "{\"key\":\"value\"}";
		String fileAttachmentsJSON = "myFileAttachments";
		long[] categoryIds = { 11, 22, 33 };
		String[] tagNames = { "Tag 1", "Tag 2", "Tag 3" };

		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockGroupContentService.getDDLRecordSet(ddlRecordSetId)).thenReturn(mockDDLRecordSet1);
		when(mockGroupContentService.getServiceContextAndRunAsUser(mockGroup, userId)).thenReturn(mockServiceContext);
		when(mockGroupContentService.uploadDDLAttachments(mockGroup, fileAttachmentsJSON, mockDDLRecordSet1, mockServiceContext)).thenReturn(mockAttachments);
		when(mockGroupContentService.addDDLRecord(userId, groupId, mockDDLRecordSet1, ddlRecordJSON, mockAttachments, categoryIds, tagNames, mockServiceContext)).thenReturn(mockDDLRecord);
		when(mockRestContentEntryBuilder.getDynamicContentFullDetails(mockDDLRecord)).thenThrow(new PortalException());

		groupContentRestEndpoint.addDynamicContentDetails(userId, ddlRecordSetId, ddlRecordJSON, fileAttachmentsJSON, categoryIds, tagNames, API_KEY_VALUE);

	}

	@Test(expected = GroupRestException.class)
	public void addDynamicContentDetails_WhenErrorRetrievingGroupFromAPIKey_ThenThrowsException() throws Exception {
		long userId = 1L;
		long ddlRecordSetId = 3L;
		String ddlRecordJSON = "{\"key\":\"value\"}";
		String fileAttachmentsJSON = "myFileAttachments";
		long[] categoryIds = { 11, 22, 33 };
		String[] tagNames = { "Tag 1", "Tag 2", "Tag 3" };

		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenThrow(new GroupRestKeyException());

		groupContentRestEndpoint.addDynamicContentDetails(userId, ddlRecordSetId, ddlRecordJSON, fileAttachmentsJSON, categoryIds, tagNames, API_KEY_VALUE);

	}

	@Test
	public void addDynamicContentDetails_WhenNoErrors_ThenCreatesANewDDLRecordAndReturnsOkResponse() throws Exception {
		long userId = 1L;
		long groupId = 2L;
		long ddlRecordSetId = 3L;
		String ddlRecordJSON = "{\"key\":\"value\"}";
		String fileAttachmentsJSON = "myFileAttachments";
		long[] categoryIds = { 11, 22, 33 };
		String[] tagNames = { "Tag 1", "Tag 2", "Tag 3" };
		Response okResponse = Response.ok().build();

		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockGroupContentService.getDDLRecordSet(ddlRecordSetId)).thenReturn(mockDDLRecordSet1);
		when(mockGroupContentService.getServiceContextAndRunAsUser(mockGroup, userId)).thenReturn(mockServiceContext);
		when(mockGroupContentService.uploadDDLAttachments(mockGroup, fileAttachmentsJSON, mockDDLRecordSet1, mockServiceContext)).thenReturn(mockAttachments);
		when(mockGroupContentService.addDDLRecord(userId, groupId, mockDDLRecordSet1, ddlRecordJSON, mockAttachments, categoryIds, tagNames, mockServiceContext)).thenReturn(mockDDLRecord);
		when(mockRestContentEntryBuilder.getDynamicContentFullDetails(mockDDLRecord)).thenReturn(mockRestContentEntryOne);
		when(mockRestContentEntryBuilder.getSuccessResponse(mockRestContentEntryOne)).thenReturn(okResponse);

		Response response = groupContentRestEndpoint.addDynamicContentDetails(userId, ddlRecordSetId, ddlRecordJSON, fileAttachmentsJSON, categoryIds, tagNames, API_KEY_VALUE);

		assertThat(response, equalTo(okResponse));

	}

	@Test(expected = GroupRestException.class)
	public void getDDLRecordSets_WhenExceptionRetrievingGroupFromAPI_ThenThrowsGroupRestException() throws Exception {
		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenThrow(new GroupRestKeyException());

		groupContentRestEndpoint.getDDLRecordSets(API_KEY_VALUE);
	}

	@Test
	public void getDDLRecordSets_WhenNoError_ThenReturnsListWithRecordSets() throws Exception {
		Response expectedResponse = Response.ok().build();
		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenReturn(mockGroup);
		when(mockGroupContentService.getDDLRecordSets(mockGroup)).thenReturn(Arrays.asList(mockDDLRecordSet1, mockDDLRecordSet2));
		when(RestListResult.init()).thenReturn(mockRestListResult);
		when(mockRestContentEntryBuilder.getRestContentEntryForDDLRecordSet(mockDDLRecordSet1)).thenReturn(mockRestContentEntryOne);
		when(mockRestContentEntryBuilder.getRestContentEntryForDDLRecordSet(mockDDLRecordSet2)).thenReturn(mockRestContentEntryTwo);
		when(mockRestContentEntryBuilder.getSuccessResponse(mockRestListResult)).thenReturn(expectedResponse);

		Response result = groupContentRestEndpoint.getDDLRecordSets(API_KEY_VALUE);

		assertThat(result, equalTo(expectedResponse));
		InOrder inOrder = inOrder(mockRestContentEntryBuilder, mockRestListResult);
		inOrder.verify(mockRestListResult, times(1)).addResult(mockRestContentEntryOne);
		inOrder.verify(mockRestListResult, times(1)).addResult(mockRestContentEntryTwo);
		inOrder.verify(mockRestContentEntryBuilder, times(1)).getSuccessResponse(mockRestListResult);
	}

	@Test(expected = GroupRestException.class)
	public void getGroupCategories_WhenException_ThenThrowsGroupRestException() throws Exception {
		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenThrow(new GroupRestKeyException());

		groupContentRestEndpoint.getGroupCategories(API_KEY_VALUE);
	}

	@Test
	public void getGroupCategories_WhenNoError_ThenConfiguresResultsInResponse() throws Exception {
		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenReturn(mockGroup);
		Response expectedResponse = Response.ok().build();

		List<AssetCategory> categories = new LinkedList<>();
		categories.add(mockAssetCategoryOne);
		categories.add(mockAssetCategoryTwo);
		when(mockGroupContentService.getGroupCategories(mockGroup)).thenReturn(categories);
		when(RestListResult.init()).thenReturn(mockRestListResult);
		when(mockRestContentEntryBuilder.getRestContentEntryForAssetCategory(mockAssetCategoryOne)).thenReturn(mockRestContentEntryOne);
		when(mockRestContentEntryBuilder.getRestContentEntryForAssetCategory(mockAssetCategoryTwo)).thenReturn(mockRestContentEntryTwo);
		when(mockRestContentEntryBuilder.getSuccessResponse(mockRestListResult)).thenReturn(expectedResponse);

		groupContentRestEndpoint.getGroupCategories(API_KEY_VALUE);

		InOrder inOrder = inOrder(mockRestListResult, mockRestContentEntryBuilder);
		inOrder.verify(mockRestListResult, times(1)).addResult(mockRestContentEntryOne);
		inOrder.verify(mockRestListResult, times(1)).addResult(mockRestContentEntryTwo);
		inOrder.verify(mockRestContentEntryBuilder, times(1)).getSuccessResponse(mockRestListResult);
	}

	@Test
	public void getGroupCategories_WhenNoError_ThenReturnsResponse() throws Exception {
		Response expectedResponse = Response.ok().build();
		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenReturn(mockGroup);
		when(mockGroupContentService.getGroupCategories(mockGroup)).thenReturn(Collections.emptyList());
		when(RestListResult.init()).thenReturn(mockRestListResult);
		when(mockRestContentEntryBuilder.getSuccessResponse(mockRestListResult)).thenReturn(expectedResponse);

		Response result = groupContentRestEndpoint.getGroupCategories(API_KEY_VALUE);

		assertThat(result, sameInstance(expectedResponse));
	}

	@Test(expected = GroupRestException.class)
	public void getUserDetails_WhenExceptionRetrievingGroupFromAPI_ThenThrowsGroupRestException() throws Exception {
		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenThrow(new GroupRestKeyException());

		groupContentRestEndpoint.getUserDetails(API_KEY_VALUE, "emailValue");
	}

	@Test
	public void getUserDetails_WhenNoError_ThenReturnsTheResponseForTheUserFound() throws Exception {
		Response expectedResponse = Response.ok().build();
		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenReturn(mockGroup);
		when(mockGroupContentService.fetchUser(mockGroup, "emailValue")).thenReturn(mockUser);
		when(mockRestContentEntryBuilder.getRestContentUser(mockGroup, mockUser, "emailValue")).thenReturn(mockRestContentUser);
		when(mockRestContentEntryBuilder.getSuccessResponse(mockRestContentUser)).thenReturn(expectedResponse);

		Response result = groupContentRestEndpoint.getUserDetails(API_KEY_VALUE, "emailValue");

		assertThat(result, equalTo(expectedResponse));
	}

	@Test(expected = GroupRestException.class)
	public void updateDynamicContentAssetEntries_WhenErrorRetrievingGroup_ThenThrowsException() throws GroupRestKeyException, GroupRestException {

		long userId = 123456;
		long contentId = 678910;
		long[] categoryIds = new long[] { 111, 222, 333 };
		String[] tagNames = new String[] { "TAG_1,", "TAG_2", "TAG_3" };

		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenThrow(new GroupRestKeyException());

		groupContentRestEndpoint.updateDynamicContentAssetEntries(userId, contentId, categoryIds, tagNames, API_KEY_VALUE);

	}

	@Test(expected = GroupRestException.class)
	public void updateDynamicContentAssetEntries_WhenErrorUpdatingAssetEntries_ThenThrowsException() throws Exception {
		long userId = 123456;
		long contentId = 678910;
		long[] categoryIds = new long[] { 111, 222, 333 };
		String[] tagNames = new String[] { "TAG_1,", "TAG_2", "TAG_3" };

		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenReturn(mockGroup);
		when(mockRestContentEntryBuilder.getDDLRecord(mockGroup, contentId)).thenThrow(new PortalException());

		groupContentRestEndpoint.updateDynamicContentAssetEntries(userId, contentId, categoryIds, tagNames, API_KEY_VALUE);
	}

	@Test
	public void updateDynamicContentAssetEntries_WhenNoErrors_ThenUpdatesAssetsEntriesAssociatedWithDDLRecord() throws Exception {
		long contentId = 678910;
		long userId = 123456;
		long[] categoryIds = new long[] { 111, 222, 333 };
		String[] tagNames = new String[] { "TAG_1,", "TAG_2", "TAG_3" };

		when(mockGroupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, API_KEY_VALUE)).thenReturn(mockGroup);
		when(mockRestContentEntryBuilder.getDDLRecord(mockGroup, contentId)).thenReturn(mockDDLRecord);
		groupContentRestEndpoint.updateDynamicContentAssetEntries(userId, contentId, categoryIds, tagNames, API_KEY_VALUE);

		verify(mockGroupContentService, times(1)).updateDDLRecordAssetEntries(userId, mockDDLRecord, categoryIds, tagNames);

	}

}
