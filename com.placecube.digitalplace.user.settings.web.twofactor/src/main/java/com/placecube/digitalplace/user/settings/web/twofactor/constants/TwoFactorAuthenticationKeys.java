package com.placecube.digitalplace.user.settings.web.twofactor.constants;

public final class TwoFactorAuthenticationKeys {

	public static final String BUNDLE_ID = "com.placecube.digitalplace.user.settings.web.twofactor";

	public static final int DISPLAY_ORDER = 1000;

	public static final String WEBCONTENT_ARTICLE_ID = "USER-2FA";

	public static final String ENABLE_TWO_FACTOR_AUTHENTICATION = "/usersettings/enable-2fa";

	public static final String GENERATE_TWO_FACTOR_AUTHENTICATION = "/usersettings/generate-2fa";

	public static final String SAVE_TWO_FACTOR_AUTHENTICATION = "/usersettings/save-2fa";

	private TwoFactorAuthenticationKeys() {
	}
}
