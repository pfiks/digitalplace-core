package com.placecube.digitalplace.calendar.web.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarEvent;
import com.placecube.digitalplace.common.portlet.PortletURLService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ JSONFactoryUtil.class })
public class DigitalPlaceCalendarEventParserServiceTest extends PowerMockito  {

	@InjectMocks
	private DigitalPlaceCalendarEventParserService digitalPlaceCalendarEventParserService;

	@Mock
	private DigitalPlaceCalendarEvent mockCalendarEvent1;

	@Mock
	private DigitalPlaceCalendarEvent mockCalendarEvent2;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONObject mockJSONObject1;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private PortletURLService mockPortletURLService;

	@Before
	public void setUp() {
		mockStatic(JSONFactoryUtil.class);
	}

	@Test
	public void parseCalendarEvents_WhenAllDataIsPresent_ThenReturnsParsedCalendarEventsWithAllDataSet() {
		String currentURL = "currentURL";

		String title1 = "title1";
		String title2 = "title2";
		String start1 = "start1";
		String start2 = "start2";
		String backgroundColour1 = "backgroundColour1";
		String backgroundColour2 = "backgroundColour2";
		String textColour1 = "textColour1";
		String textColour2 = "textColour2";
		String end1 = "end1";
		String end2 = "end2";
		String viewURL1 = "viewURL1";
		String viewURL2 = "viewURL2";
		String viewURLWithRedirect1 = "viewURLWithRedirect1";
		String viewURLWithRedirect2 = "viewURLWithRedirect2";

		List<DigitalPlaceCalendarEvent> calendarEvents = Arrays.asList(mockCalendarEvent1, mockCalendarEvent2);
		when(mockCalendarEvent1.getTitle()).thenReturn(title1);
		when(mockCalendarEvent1.getStart()).thenReturn(start1);
		when(mockCalendarEvent1.getBackgroundColour()).thenReturn(backgroundColour1);
		when(mockCalendarEvent1.getTextColour()).thenReturn(textColour1);
		when(mockCalendarEvent1.getEnd()).thenReturn(end1);
		when(mockCalendarEvent1.getViewURL()).thenReturn(viewURL1);
		when(mockPortletURLService.getURLWithRedirectParameter(viewURL1, currentURL)).thenReturn(viewURLWithRedirect1);

		when(mockCalendarEvent2.getTitle()).thenReturn(title2);
		when(mockCalendarEvent2.getStart()).thenReturn(start2);
		when(mockCalendarEvent2.getBackgroundColour()).thenReturn(backgroundColour2);
		when(mockCalendarEvent2.getTextColour()).thenReturn(textColour2);
		when(mockCalendarEvent2.getEnd()).thenReturn(end2);
		when(mockCalendarEvent2.getViewURL()).thenReturn(viewURL2);
		when(mockPortletURLService.getURLWithRedirectParameter(viewURL2, currentURL)).thenReturn(viewURLWithRedirect2);

		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);

		AtomicBoolean wasMethodCalled = new AtomicBoolean(false);
		when(JSONFactoryUtil.createJSONObject()).thenAnswer((Answer<JSONObject>) invocation -> {
			if (wasMethodCalled.get()) {
				return mockJSONObject2;
			} else {
				wasMethodCalled.set(true);
				return mockJSONObject1;
			}
		});

		JSONArray result = digitalPlaceCalendarEventParserService.parseCalendarEvents(calendarEvents, currentURL);
		assertThat(result, sameInstance(mockJSONArray));

		verify(mockJSONArray, times(1)).put(mockJSONObject1);
		verify(mockJSONArray, times(1)).put(mockJSONObject2);

		verify(mockJSONObject1, times(1)).put("title", title1);
		verify(mockJSONObject1, times(1)).put("start", start1);
		verify(mockJSONObject1, times(1)).put("backgroundColor", backgroundColour1);
		verify(mockJSONObject1, times(1)).put("borderColor", backgroundColour1);
		verify(mockJSONObject1, times(1)).put("textColor", textColour1);
		verify(mockJSONObject1, times(1)).put("end", end1);
		verify(mockJSONObject1, times(1)).put("url", viewURLWithRedirect1);

		verify(mockJSONObject2, times(1)).put("title", title2);
		verify(mockJSONObject2, times(1)).put("start", start2);
		verify(mockJSONObject2, times(1)).put("backgroundColor", backgroundColour2);
		verify(mockJSONObject2, times(1)).put("borderColor", backgroundColour2);
		verify(mockJSONObject2, times(1)).put("textColor", textColour2);
		verify(mockJSONObject2, times(1)).put("end", end2);
		verify(mockJSONObject2, times(1)).put("url", viewURLWithRedirect2);
	}

	@Test
	public void parseCalendarEvents_WhenEndDateIsNotDefined_ThenReturnsParsedCalendarEventsWithoutEndDateSet() {
		String currentURL = "currentURL";

		List<DigitalPlaceCalendarEvent> calendarEvents = Collections.singletonList(mockCalendarEvent1);
		when(mockCalendarEvent1.getEnd()).thenReturn(null);
		when(mockCalendarEvent1.getViewURL()).thenReturn("viewURL");

		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject1);

		JSONArray result = digitalPlaceCalendarEventParserService.parseCalendarEvents(calendarEvents, currentURL);
		assertThat(result, sameInstance(mockJSONArray));

		verify(mockJSONArray, times(1)).put(mockJSONObject1);
		verify(mockJSONObject1, never()).put(eq("end"), anyString());
	}

	@Test
	public void parseCalendarEvents_WhenViewURLIsNotDefined_ThenReturnsParsedCalendarEventsWithoutEndDateSet() {
		String currentURL = "currentURL";

		List<DigitalPlaceCalendarEvent> calendarEvents = Collections.singletonList(mockCalendarEvent1);
		when(mockCalendarEvent1.getViewURL()).thenReturn(null);

		when(JSONFactoryUtil.createJSONArray()).thenReturn(mockJSONArray);
		when(JSONFactoryUtil.createJSONObject()).thenReturn(mockJSONObject1);

		JSONArray result = digitalPlaceCalendarEventParserService.parseCalendarEvents(calendarEvents, currentURL);
		assertThat(result, sameInstance(mockJSONArray));

		verify(mockJSONArray, times(1)).put(mockJSONObject1);
		verify(mockJSONObject1, never()).put(eq("url"), anyString());
	}
}
