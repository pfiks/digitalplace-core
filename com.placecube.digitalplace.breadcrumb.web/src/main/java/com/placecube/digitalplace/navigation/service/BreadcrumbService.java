package com.placecube.digitalplace.navigation.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListMergeable;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.site.navigation.taglib.servlet.taglib.util.BreadcrumbUtil;
import com.placecube.digitalplace.navigation.constants.ConfigurationConstants;
import com.placecube.digitalplace.navigation.constants.PortletPreferenceKeys;
import com.placecube.digitalplace.navigation.model.BreadcrumbServiceContext;
import com.placecube.digitalplace.navigation.service.util.BreadcrumbServiceUtil;

@Component(immediate = true, service = BreadcrumbService.class)
public class BreadcrumbService {

	@Reference
	private AssetCategoryService assetCategoryService;

	@Reference
	private AssetVocabularyService assetVocabularyService;

	@Reference
	private BreadcrumbServiceUtil breadcrumbServiceUtil;

	public List<List<BreadcrumbEntry>> getCategoryBreadcrumbEntries(BreadcrumbServiceContext breadcrumbServiceContext, Optional<BreadcrumbEntry> siteLayoutBreadcrumbEntry,
			BreadcrumbEntry displayPageBreadcrumbEntry) throws PortalException {

		List<List<BreadcrumbEntry>> categoryBreadcrumbEntries = new ArrayList<>();
		Optional<AssetEntry> layoutAssetEntryOptional = breadcrumbServiceContext.getLayoutAssetEntry();

		if (layoutAssetEntryOptional.isPresent()) {

			AssetEntry layoutAssetEntry = layoutAssetEntryOptional.get();
			categoryBreadcrumbEntries = getLayoutAssetEntryCategoryBreadcrumbs(layoutAssetEntry, breadcrumbServiceContext, siteLayoutBreadcrumbEntry, displayPageBreadcrumbEntry);

		} else {

			List<BreadcrumbEntry> breadcrumbEntries = getDefaultCategoryBreadcrumbEntries(siteLayoutBreadcrumbEntry, displayPageBreadcrumbEntry);
			categoryBreadcrumbEntries.add(breadcrumbEntries);

		}

		if (breadcrumbServiceContext.isDisplayOnlyOneAssetCategoryBreadcrumb() && !categoryBreadcrumbEntries.isEmpty()) {
			categoryBreadcrumbEntries = categoryBreadcrumbEntries.subList(0, 1);
		}

		return categoryBreadcrumbEntries;

	}

	public BreadcrumbServiceContext getBreadcrumbServiceContext(RenderRequest renderRequest) {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Layout layout = themeDisplay.getLayout();
		Locale locale = themeDisplay.getLocale();

		AssetEntry layoutAssetEntry = (AssetEntry) renderRequest.getAttribute(WebKeys.LAYOUT_ASSET_ENTRY);

		PortletPreferences preferences = renderRequest.getPreferences();
		long categoryNavFilterVocabId = GetterUtil
				.getLong(preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_FILTER_VOCABULARY_ID, ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID));
		String categoryNavUrlCategoryPropertyName = preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME, StringPool.BLANK);
		String categoryNavBuildCategoriesHierarchyUrlPropertyName = preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, StringPool.BLANK);

		ListMergeable<?> pageTitleListMergeable = (ListMergeable<?>) renderRequest.getAttribute(WebKeys.PAGE_TITLE);
		String pageTitle = pageTitleListMergeable != null ? pageTitleListMergeable.mergeToString(StringPool.BLANK) : StringPool.BLANK;

		if (pageTitle.isEmpty()) {
			pageTitle = layout.getTitle(locale);
		}

		boolean useLayoutUrl = GetterUtil.getBoolean(preferences.getValue(PortletPreferenceKeys.USE_LAYOUT_URL, Boolean.FALSE.toString()));
		boolean displayOnlyOneAssetCategoryBreadcrumb = GetterUtil.getBoolean(preferences.getValue(PortletPreferenceKeys.DISPLAY_ONLY_ONE_ASSET_CATEGORY_BREADCRUMB, Boolean.FALSE.toString()));

		BreadcrumbServiceContext breadcrumbServiceContext = new BreadcrumbServiceContext();
		breadcrumbServiceContext.setLayoutAssetEntry(layoutAssetEntry);
		breadcrumbServiceContext.setCategoryNavigationFilterVocabularyId(categoryNavFilterVocabId);
		breadcrumbServiceContext.setCategoryNavigationUrlCategoryPropertyName(categoryNavUrlCategoryPropertyName);
		breadcrumbServiceContext.setCategoryNavigationBuildCategoriesHierarchyUrlPropertyName(categoryNavBuildCategoriesHierarchyUrlPropertyName);
		breadcrumbServiceContext.setLayout(layout);
		breadcrumbServiceContext.setLocale(locale);
		breadcrumbServiceContext.setPageTitle(pageTitle);
		breadcrumbServiceContext.setThemeDisplay(themeDisplay);
		breadcrumbServiceContext.setUseLayoutUrl(useLayoutUrl);
		breadcrumbServiceContext.setDisplayOnlyOneAssetCategoryBreadcrumb(displayOnlyOneAssetCategoryBreadcrumb);

		return breadcrumbServiceContext;

	}

	public BreadcrumbEntry getDisplayPageBreadcrumbEntry(BreadcrumbServiceContext breadcrumbServiceContext) {

		BreadcrumbEntry displayPageBreadcrumbEntry = new BreadcrumbEntry();
		displayPageBreadcrumbEntry.setTitle(breadcrumbServiceContext.getPageTitle());
		displayPageBreadcrumbEntry.setURL(StringPool.BLANK);

		return displayPageBreadcrumbEntry;

	}

	public Optional<BreadcrumbEntry> getSiteLayoutBreadcrumbEntry(ThemeDisplay themeDisplay, String siteLayoutBreadcrumbEntryName) throws Exception {

		Group scopeGroup = themeDisplay.getScopeGroup();
		Optional<BreadcrumbEntry> siteLayoutBreadcrumbEntry;

		if (scopeGroup.isGuest()) {

			siteLayoutBreadcrumbEntry = Optional.ofNullable(BreadcrumbUtil.getGuestGroupBreadcrumbEntry(themeDisplay));

		} else if (breadcrumbServiceUtil.isGuestStagingGroup(scopeGroup)) {

			siteLayoutBreadcrumbEntry = breadcrumbServiceUtil.getLayoutSetGroupBreadcrumbEntry(themeDisplay);

		} else {

			siteLayoutBreadcrumbEntry = Optional.ofNullable(BreadcrumbUtil.getScopeGroupBreadcrumbEntry(themeDisplay));

		}

		if (siteLayoutBreadcrumbEntry.isPresent()) {
			siteLayoutBreadcrumbEntry.get().setTitle(siteLayoutBreadcrumbEntryName);
		}

		return siteLayoutBreadcrumbEntry;

	}

	public List<List<BreadcrumbEntry>> applyCurrentLayoutPreferenceFilterToBreadcrumbTree(List<List<BreadcrumbEntry>> breadcrumbEntriesList, RenderRequest renderRequest) {
		PortletPreferences preferences = renderRequest.getPreferences();
		boolean showCurrentLayout = GetterUtil.getBoolean(preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, Boolean.TRUE.toString()));

		if (!showCurrentLayout) {
			for (List<BreadcrumbEntry> categoryBreadcrumb : breadcrumbEntriesList) {
				filterBreadcrumbCurrentLayout(categoryBreadcrumb, showCurrentLayout);
			}
		}

		return breadcrumbEntriesList;
	}

	public List<BreadcrumbEntry> addRootEntryIfNecessary(List<BreadcrumbEntry> breadcrumbEntries, RenderRequest renderRequest, Optional<BreadcrumbEntry> siteLayoutBreadcrumbEntry) {
		PortletPreferences preferences = renderRequest.getPreferences();
		boolean forceRootForLayoutBreadcrumbs = GetterUtil.getBoolean(preferences.getValue(
				PortletPreferenceKeys.CATEGORY_NAV_FORCE_ROOT_FOR_LAYOUT_BREADCRUMBS, Boolean.FALSE.toString()));

		if (forceRootForLayoutBreadcrumbs && siteLayoutBreadcrumbEntry.isPresent()) {
			breadcrumbEntries.add(0, siteLayoutBreadcrumbEntry.get());
		}

		return breadcrumbEntries;
	}

	public List<BreadcrumbEntry> applyCurrentLayoutPreferenceFilter(List<BreadcrumbEntry> breadcrumbEntries, RenderRequest renderRequest) {
		PortletPreferences preferences = renderRequest.getPreferences();
		boolean showCurrentLayout = GetterUtil.getBoolean(preferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_SHOW_CURRENT_LAYOUT, Boolean.TRUE.toString()));

		return filterBreadcrumbCurrentLayout(breadcrumbEntries, showCurrentLayout);
	}

	private List<BreadcrumbEntry> filterBreadcrumbCurrentLayout(List<BreadcrumbEntry> breadcrumbEntries, boolean showCurrentLayout) {
		if (!showCurrentLayout && breadcrumbEntries.size() > 1) {
			breadcrumbEntries.remove(breadcrumbEntries.size() - 1);
		}

		return breadcrumbEntries;
	}

	private List<BreadcrumbEntry> getCategoryBreadcrumbs(List<AssetCategory> assetCategories, BreadcrumbServiceContext breadcrumbServiceContext, Optional<BreadcrumbEntry> siteLayoutBreadcrumbEntry,
			BreadcrumbEntry displayPageBreadcrumbEntry) {

		Locale locale = breadcrumbServiceContext.getLocale();
		String urlCategoryPropertyName = breadcrumbServiceContext.getCategoryNavUrlCategoryPropertyName();
		String buildCategoriesHierarchyUrlPropertyName = breadcrumbServiceContext.getCategoryNavigationBuildCategoriesHierarchyUrlPropertyName();

		List<BreadcrumbEntry> breadcrumbEntries = new ArrayList<>();

		if (siteLayoutBreadcrumbEntry.isPresent()) {
			breadcrumbEntries.add(siteLayoutBreadcrumbEntry.get());
		}

		for (AssetCategory assetCategory : assetCategories) {

			BreadcrumbEntry categoryBreadcrumbEntry = getBreadcrumbEntry(assetCategory, locale, urlCategoryPropertyName, buildCategoriesHierarchyUrlPropertyName,
					breadcrumbServiceContext.isUseLayoutUrl(), breadcrumbServiceContext.getThemeDisplay().getScopeGroupId());
			breadcrumbEntries.add(categoryBreadcrumbEntry);

		}

		breadcrumbEntries.add(displayPageBreadcrumbEntry);

		return breadcrumbEntries;

	}

	private BreadcrumbEntry getBreadcrumbEntry(AssetCategory assetCategory, Locale locale, String urlCategoryPropertyName, String buildCategoriesHierarchyUrlPropertyName, boolean useLayoutUrl, long scopeGroupId) {

		String title = assetCategory.getTitle(locale);

		String url = assetCategoryService.getAssetCategoryUrl(assetCategory, urlCategoryPropertyName, buildCategoriesHierarchyUrlPropertyName, useLayoutUrl, scopeGroupId);

		BreadcrumbEntry breadcrumbEntry = new BreadcrumbEntry();
		breadcrumbEntry.setTitle(title);
		breadcrumbEntry.setURL(url);

		return breadcrumbEntry;
	}

	private List<BreadcrumbEntry> getDefaultCategoryBreadcrumbEntries(Optional<BreadcrumbEntry> siteLayoutBreadcrumbEntry, BreadcrumbEntry displayPageBreadcrumbEntry) {

		List<BreadcrumbEntry> breadcrumbEntries = new ArrayList<>();

		if (siteLayoutBreadcrumbEntry.isPresent()) {
			breadcrumbEntries.add(siteLayoutBreadcrumbEntry.get());
		}

		breadcrumbEntries.add(displayPageBreadcrumbEntry);

		return breadcrumbEntries;

	}

	private List<List<BreadcrumbEntry>> getLayoutAssetEntryCategoryBreadcrumbs(AssetEntry layoutAssetEntry, BreadcrumbServiceContext breadcrumbServiceContext,
			Optional<BreadcrumbEntry> siteLayoutBreadcrumbEntry, BreadcrumbEntry displayPageBreadcrumbEntry) throws PortalException {

		List<List<BreadcrumbEntry>> categoryBreadcrumbEntries = new ArrayList<>();
		long vocabularyId = getVocabularyId(breadcrumbServiceContext);
		List<AssetCategory> categories = assetCategoryService.getCategories(layoutAssetEntry, vocabularyId);
		List<AssetCategory> deepestCategories = assetCategoryService.getTheDeepestCategories(categories);
		List<List<AssetCategory>> hierarchicalCategories = assetCategoryService.getHierarchicalCategories(deepestCategories, vocabularyId);

		for (List<AssetCategory> categoryHierarchy : hierarchicalCategories) {

			List<BreadcrumbEntry> categoryBreadcrumbs = getCategoryBreadcrumbs(categoryHierarchy, breadcrumbServiceContext, siteLayoutBreadcrumbEntry, displayPageBreadcrumbEntry);
			categoryBreadcrumbEntries.add(new ArrayList<>(categoryBreadcrumbs));

		}

		if (categoryBreadcrumbEntries.isEmpty()) {

			List<BreadcrumbEntry> defaultCategoryBreadcrumbEntries = getDefaultCategoryBreadcrumbEntries(siteLayoutBreadcrumbEntry, displayPageBreadcrumbEntry);
			categoryBreadcrumbEntries.add(defaultCategoryBreadcrumbEntries);

		}

		return categoryBreadcrumbEntries;

	}

	private long getVocabularyId(BreadcrumbServiceContext breadcrumbServiceContext) {

		ThemeDisplay themeDisplay = breadcrumbServiceContext.getThemeDisplay();

		return assetVocabularyService.getVocabularyId(themeDisplay.getScopeGroupId(), String.valueOf(breadcrumbServiceContext.getCategoryNavFilterVocabularyId()));
	}

}
