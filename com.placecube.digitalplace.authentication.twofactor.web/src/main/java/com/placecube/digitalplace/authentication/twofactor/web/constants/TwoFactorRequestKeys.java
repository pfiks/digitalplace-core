package com.placecube.digitalplace.authentication.twofactor.web.constants;

public final class TwoFactorRequestKeys {

	public static final String RESET_BUTTON_ENABLED = "resetButtonEnabled";

	private TwoFactorRequestKeys() {
	}

}
