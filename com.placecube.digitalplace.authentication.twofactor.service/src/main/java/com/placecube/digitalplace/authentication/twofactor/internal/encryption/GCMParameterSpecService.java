package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.crypto.spec.GCMParameterSpec;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = GCMParameterSpecService.class)
public class GCMParameterSpecService {

	static final int IV_LENGTH = 8;

	static final int IV_UUID_PADDING = 8;

	static final String TWO_FACTOR_IV_FILE_PATH = "dependencies/keys/twofactor_iv";

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	public GCMParameterSpec getGCMParameterSpec(String uuid) throws IOException {
		byte[] iv = modelFactoryBuilder.getBytesFromFile(TWO_FACTOR_IV_FILE_PATH);
		byte[] uuidPadding = uuid.substring(uuid.length() - IV_UUID_PADDING).getBytes();
		// Append the last IV_UUID_PADDING bytes of the user UUID to generate an
		// initialisation vector different for all users
		ByteArrayOutputStream outputStream = modelFactoryBuilder.getByteArrayOutputStreamInstance();
		outputStream.write(iv);
		outputStream.write(uuidPadding);
		if (outputStream.toByteArray().length != 16) {
			throw new IllegalStateException("The overall length of Initialisation vector must be 16 bytes");
		}
		return modelFactoryBuilder.getGCMParameterSpecInstance((IV_LENGTH + IV_UUID_PADDING) * 8, outputStream.toByteArray());
	}

}
