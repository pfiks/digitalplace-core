package com.placecube.digitalplace.search.shared.model.impl;

import static com.placecube.digitalplace.search.internal.contants.SharedSearchContextAttributeKeys.SEARCH_BOOLEAN_CLAUSE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.portlet.PortletPreferences;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.RangeTermFilter;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.constants.SearchContextAttributes;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;
import com.placecube.digitalplace.search.internal.util.SearchUtil;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;

public class SharedSearchContributorSettingsImpl implements SharedSearchContributorSettings {

	private static final Log LOG = LogFactoryUtil.getLog(SharedSearchContributorSettingsImpl.class);

	private static final String MAX_TERMS_CONFIG_PROPERTY = "maxTerms";

	private Optional<PortletPreferences> portletPreferences;
	private final SearchContext searchContext;
	private final SearchUtil searchUtil;
	private final SharedSearchSession sharedSearchSession;
	private final ThemeDisplay themeDisplay;

	public static SharedSearchContributorSettingsImpl init(SearchUtil searchUtil, ThemeDisplay themeDisplay, SearchContext searchContext, SharedSearchSession sharedSearchSession) {
		return new SharedSearchContributorSettingsImpl(searchUtil, themeDisplay, searchContext, sharedSearchSession);
	}

	private SharedSearchContributorSettingsImpl(SearchUtil searchUtil, ThemeDisplay themeDisplay, SearchContext searchContext, SharedSearchSession sharedSearchSession) {
		this.searchUtil = searchUtil;
		this.themeDisplay = themeDisplay;
		this.searchContext = searchContext;
		this.sharedSearchSession = sharedSearchSession;
		portletPreferences = Optional.empty();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void addBooleanClause(BooleanClause<Filter> booleanClause) {
		List<BooleanClause<Filter>> existingClauses = Validator.isNull(searchContext.getAttribute(SEARCH_BOOLEAN_CLAUSE)) ? new ArrayList<>()
				: (List<BooleanClause<Filter>>) searchContext.getAttribute(SEARCH_BOOLEAN_CLAUSE);

		existingClauses.add(booleanClause);
		searchContext.setAttribute(SEARCH_BOOLEAN_CLAUSE, (Serializable) existingClauses);
	}

	@Override
	public void addBooleanQuery(BooleanClause<Query> queryToAdd) {
		setBooleanClauses(queryToAdd);
	}

	@Override
	public void addBooleanQuery(String field, String value, BooleanClauseOccur booleanClauseOccur) {
		if (Validator.isNotNull(field) && Validator.isNotNull(value)) {
			BooleanClause<Query> queryToAdd = BooleanClauseFactoryUtil.create(field, value, booleanClauseOccur.getName());
			setBooleanClauses(queryToAdd);
		}
	}

	@Override
	public void addBooleanQueryForMultipleValues(String field, List<String> values, BooleanClauseOccur booleanClauseOccur) {
		if (Validator.isNotNull(field) && Validator.isNotNull(values) && !values.isEmpty()) {
			BooleanQuery mainQuery = searchUtil.createBooleanQuery();
			boolean areValuesAdded = false;
			for (String value : values) {
				if (Validator.isNotNull(value)) {
					BooleanQuery valueQuery = searchUtil.createBooleanQuery();
					valueQuery.addExactTerm(field, value);
					try {
						mainQuery.add(valueQuery, BooleanClauseOccur.SHOULD);
						areValuesAdded = true;
					} catch (ParseException e) {
						LOG.error("Cannot add boolean subquery for value: " + value + " (" + e.getMessage() + ")");
						LOG.debug(e);
					}
				}
			}

			if (areValuesAdded) {
				BooleanClause<Query> queryToAdd = BooleanClauseFactoryUtil.create(mainQuery, booleanClauseOccur.getName());

				setBooleanClauses(queryToAdd);
			}

		}
	}

	@Override
	public void addFacet(String facetFieldName, Optional<FacetConfiguration> facetConfiguration) {
		Facet facet = searchContext.getFacet(facetFieldName);

		if (Objects.nonNull(facet)) {

			FacetConfiguration currentFacetConfiguration = facet.getFacetConfiguration();

			if (facetConfiguration.isPresent() && Objects.nonNull(currentFacetConfiguration)) {

				JSONObject currentData = currentFacetConfiguration.getData();
				JSONObject newData = facetConfiguration.get().getData();

				int currentMaxTerms = currentData.getInt(MAX_TERMS_CONFIG_PROPERTY);
				int maxTermsToAdd = newData.getInt(MAX_TERMS_CONFIG_PROPERTY);

				int facetMultiplier = searchUtil.getFacetMultiplier(themeDisplay.getCompanyId());

				currentData.put(MAX_TERMS_CONFIG_PROPERTY, (currentMaxTerms + maxTermsToAdd) * facetMultiplier);

			} else if (facetConfiguration.isPresent()) {
				facet.setFacetConfiguration(facetConfiguration.get());
			}
		} else {
			facet = searchUtil.createFacet(facetFieldName, searchContext);

			if (facetConfiguration.isPresent()) {
				facet.setFacetConfiguration(facetConfiguration.get());
			}

			searchContext.addFacet(facet);
		}

	}

	@Override
	public void addRangeTermFilterClause(RangeTermFilter rangeTermFilter, BooleanClauseOccur booleanClauseOccur) {
		BooleanClause<Filter> dateFilter = BooleanClauseFactoryUtil.createFilter(searchContext, rangeTermFilter, BooleanClauseOccur.MUST);
		addBooleanClause(dateFilter);
	}

	@Override
	public void addSearchAttribute(String name, boolean value) {
		searchContext.setAttribute(name, value);
	}

	@Override
	public void addSharedSearchSessionSingleValuedAttribute(String attributeName, String attributeValue) {
		sharedSearchSession.addSingleValuedAttribute(attributeName, attributeValue);
	}

	@Override
	public FacetConfiguration createFacetConfiguration(String facetFieldName) {
		FacetConfiguration facetConfiguration = new FacetConfiguration();
		facetConfiguration.setFieldName(facetFieldName);
		return facetConfiguration;
	}

	@Override
	public List<SharedActiveFilter> getActiveFiltersForField(String fieldName) {
		return sharedSearchSession.getActiveFilters().stream().filter(f -> f.getName().equals(fieldName)).collect(Collectors.toList());
	}

	@Override
	public long[] getAssetCategoryIds() {
		return searchContext.getAssetCategoryIds();
	}

	@Override
	public Optional<PortletPreferences> getPortletPreferences() {
		return portletPreferences;
	}

	@Override
	public Optional<String> getSharedSearchSessionSingleValuedAttribute(String name) {
		String sessionValue = sharedSearchSession.getSingleValuedAttribute(name);

		return Validator.isNotNull(sessionValue) ? Optional.of(sessionValue) : Optional.empty();

	}

	@Override
	public ThemeDisplay getThemeDisplay() {
		return themeDisplay;
	}

	@Override
	public void setAllowEmptySearch(boolean allowEmptySearch) {
		searchContext.setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, allowEmptySearch);
	}

	@Override
	public void setAssetTagNames(Set<String> selectedTags) {
		searchContext.setAssetTagNames(selectedTags.toArray(new String[selectedTags.size()]));
	}

	@Override
	public void setEntryClassNames(String... entryClassNames) {
		searchContext.setEntryClassNames(entryClassNames);
	}

	public void setPortletPreferences(Optional<PortletPreferences> portletPreferences) {
		this.portletPreferences = portletPreferences;
	}

	@Override
	public void setSearchGroupIds(long[] groupIds) {
		searchContext.setGroupIds(groupIds);
	}

	@Override
	public void setSearchKeywords(String keywords) {
		searchContext.setKeywords(keywords);
	}

	@Override
	public void setSortBy(Sort... sortsToAdd) {
		if (null != sortsToAdd) {
			searchContext.setSorts(sortsToAdd);
		}
	}

	private void setBooleanClauses(BooleanClause<Query> queryToAdd) {
		BooleanClause<Query>[] booleanClauses = searchContext.getBooleanClauses();
		if (Validator.isNull(booleanClauses)) {
			booleanClauses = new BooleanClause[0];
		}

		searchContext.setBooleanClauses(ArrayUtil.append(booleanClauses, queryToAdd));
	}

}