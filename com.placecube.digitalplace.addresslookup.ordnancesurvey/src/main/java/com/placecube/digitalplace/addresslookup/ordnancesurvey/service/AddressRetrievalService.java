package com.placecube.digitalplace.addresslookup.ordnancesurvey.service;

import java.io.IOException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.ordnancesurvey.constants.OrdnanceSurveyConstants;

@Component(immediate = true, service = AddressRetrievalService.class)
public class AddressRetrievalService {

	private static final Log LOG = LogFactoryUtil.getLog(AddressRetrievalService.class);

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private ResponseParserUtil responseParserUtil;

	public JSONArray executeMethod(String urlToCall) {
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;

		try {

			LOG.debug("Retrieving data for url: " + urlToCall);

			HttpGet method = getMethod(urlToCall);
			client = HttpClients.createDefault();
			response = client.execute(method);

			String responseValue = EntityUtils.toString(response.getEntity());
			return responseParserUtil.getResults(responseValue);

		} catch (Exception e) {
			LOG.error(e);
			return jsonFactory.createJSONArray();
		} finally {
			quietlyCloseResources(client, response);
		}

	}

	public AddressContext getAddressContextResult(JSONObject jsonObject) {
		if (jsonObject.has(OrdnanceSurveyConstants.LPI)) {
			return responseParserUtil.getAddressContextResultForLPI(jsonObject.getJSONObject(OrdnanceSurveyConstants.LPI));
		}
		return responseParserUtil.getAddressContextResultForDPA(jsonObject.getJSONObject(OrdnanceSurveyConstants.DPA));
	}

	private HttpGet getMethod(String url) {
		HttpGet method = new HttpGet(url);
		method.addHeader("Accept", "application/json");
		return method;
	}

	private void quietlyCloseResources(CloseableHttpClient client, CloseableHttpResponse response) {
		try {
			if (response != null) {
				response.close();
			}
		} catch (IOException e) {
			LOG.debug(e);
		}

		try {
			if (client != null) {
				client.close();
			}
		} catch (IOException e) {
			LOG.debug(e);
		}
	}

}
