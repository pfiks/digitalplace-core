package com.placecube.digitalplace.user.account.web.internal.constants;

public final class RequestKeys {

	public static final String ACCOUNT_CONFIG = "accountConfiguration";

	public static final String ACCOUNT_CONTEXT = "accountContext";

	public static final String ACCOUNT_CREATED_WITH_ERRORS = "accountCreatedWithErrors";

	public static final String ACCOUNT_FIELDS = "createUserAccountFields";

	public static final String ACCOUNT_SECURITY_STEP_ENABLED = "accountSecurityStepEnabled";

	public static final String ADDITIONAL_DETAILS_EXPANDO_FIELDS = "additionalDetailsExpandoFields";

	public static final String CAPTCHA_ENABLED = "captchaEnabled";

	public static final String EMAIL_ADDRESS_REQUIRED = "emailAddressRequired";

	public static final String EMAIL_VERIFICATION_ENABLED = "emailVerificationEnabled";

	public static final String IS_ADDRESS_OUT_OF_LOCAL_AREA_ENABLED = "isAddressOutOfLocalAreaEnabled";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String REDIRECT_URL = "redirectUrl";

	public static final String REFERER = "refererGroupId";

	public static final String SHOW_EMAIL_ADDRESS_IN_ADDITIONAL_DETAILS = "showEmailAddressInAdditionalDetails";

	public static final String SHOW_EMAIL_ADDRESS_IN_PERSONAL_DETAILS = "showEmailAddressInPersonalDetails";

	public static final String TWO_FACTOR_AUTHENTICATION_ENABLED_OPTIONS = "twoFactorAuthenticationEnabledOptions";

	public static final String UPRN = "uprn";

	public static final String USE_SESSION = "useSession";

	public static final String USER_DATA = "userData";

	public static final String USER_EMAIL_ADDRESS_TO_SEND_RESET_PASSWORD = "userEmailAddressToSendResetPassword";

	public static final String USER_ID = "userId";

	public static final String USER_PROFILE_CONFIG = "userProfileConfiguration";

	private RequestKeys() {
	}

}
