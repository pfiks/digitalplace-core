package com.placecube.digitalplace.template.contributor.theme;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.template.TemplateContextContributor;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, property = "type=" + TemplateContextContributor.TYPE_THEME, service = TemplateContextContributor.class)
public class ThemeTemplateContextContributor implements TemplateContextContributor {

	private static final String VAR_NAME_BODY_CSS_CLASS = "body_css_class";

	private static final String VAR_NAME_CONTENT_CSS_CLASS = "content_css_class";

	@Override
	public void prepare(Map<String, Object> contextObjects, HttpServletRequest httpServletRequest) {

		Layout layout = (Layout) httpServletRequest.getAttribute(WebKeys.LAYOUT);
		String layoutType = layout.getType();

		switch (layoutType) {
		case LayoutConstants.TYPE_ASSET_DISPLAY:

			updateCssClassContextObjects(contextObjects, "asset-display-page", StringPool.BLANK);
			break;

		case LayoutConstants.TYPE_CONTENT:

			updateCssClassContextObjects(contextObjects, "content-page", StringPool.BLANK);
			break;

		case LayoutConstants.TYPE_PORTLET:

			updateCssClassContextObjects(contextObjects, "portlet-page", "container");
			break;

		default:

			updateCssClassContextObjects(contextObjects, StringPool.BLANK, StringPool.BLANK);
			break;

		}

	}

	private void updateCssClassContextObjects(Map<String, Object> contextObjects, String bodyCssClass, String contentCssClass) {

		contextObjects.put(VAR_NAME_BODY_CSS_CLASS, bodyCssClass);
		contextObjects.put(VAR_NAME_CONTENT_CSS_CLASS, contentCssClass);
	}

}
