package com.placecube.digitalplace.authentication.twofactor.web.useradmins;

import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.users.admin.user.action.contributor.BaseUserActionContributor;
import com.liferay.users.admin.user.action.contributor.UserActionContributor;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorUrlService;

@Component(immediate = true, service = UserActionContributor.class)
public class TwoFactorAuthenticationUserActionContributor extends BaseUserActionContributor {

	@Reference
	private TwoFactorService twoFactorService;

	@Reference
	private TwoFactorUrlService twoFactorUrlService;

	@Override
	public String getConfirmationMessage(PortletRequest portletRequest) {
		return LanguageUtil.get(getResourceBundle(getLocale(portletRequest)), "are-you-sure-you-want-to-reset-two-factor-authenticator");
	}

	@Override
	public String getMessage(PortletRequest portletRequest) {
		return LanguageUtil.get(getResourceBundle(getLocale(portletRequest)), "reset-two-factor-authenticator");
	}

	@Override
	public String getURL(PortletRequest portletRequest, PortletResponse portletResponse, User user, User selUser) {
		return twoFactorUrlService.getResetTwoFactorURL(portletResponse, selUser);
	}

	@Override
	public boolean isShow(PortletRequest portletRequest, User user, User selUser) {
		Optional<AuthenticationTwoFactorCompanyConfiguration> enabledConfiguration = twoFactorService.getEnabledConfiguration(user.getCompanyId());
		return enabledConfiguration.isPresent();
	}

	@Override
	public boolean isShowConfirmationMessage(User selUser) {
		return true;
	}

}
