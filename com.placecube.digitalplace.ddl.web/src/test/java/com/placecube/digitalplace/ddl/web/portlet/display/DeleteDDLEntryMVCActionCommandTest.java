package com.placecube.digitalplace.ddl.web.portlet.display;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLPermissionService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ParamUtil.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class DeleteDDLEntryMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private DeleteDDLEntryMVCActionCommand deleteDDLEntryMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private DDLPermissionService mockDDLPermissionService;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test(expected = PortalException.class)
	public void doProcessAction_WhenExceptionDeletingTheRecord_ThenThrowsPortalException() throws PortalException {
		long recordId = 12;
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, "ddlRecordId")).thenReturn(recordId);
		doThrow(new PortalException()).when(mockDDLRecordLocalService).deleteRecord(recordId);

		deleteDDLEntryMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = PortalException.class)
	public void doProcessAction_WhenExceptionValidatingDeletePermission_ThenThrowsPortalException() throws PortalException {
		long recordId = 12;
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getLong(mockActionRequest, "ddlRecordId")).thenReturn(recordId);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		doThrow(new PortalException()).when(mockDDLPermissionService).validateDeletePermission(mockPermissionChecker, recordId);

		deleteDDLEntryMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenDeletesTheRecord() throws PortalException {
		long recordId = 12;
		when(ParamUtil.getLong(mockActionRequest, "ddlRecordId")).thenReturn(recordId);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);

		deleteDDLEntryMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockDDLRecordLocalService, times(1)).deleteRecord(recordId);
	}
}
