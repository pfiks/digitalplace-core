package com.placecube.digitalplace.ddl.web.portlet.display.service;

import javax.portlet.RenderRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration;

@Component(immediate = true, service = DDLDisplayPortletRequestConfigurationService.class)
public class DDLDisplayPortletRequestConfigurationService {

	@Reference
	private DDLEditorService ddlEditorService;

	@Reference
	private DDLPermissionService ddlPermissionService;

	@Reference
	private DDLRecordService ddlRecordService;

	public void setRenderRequestAttributesUsingConfiguration(RenderRequest renderRequest, ThemeDisplay themeDisplay, DDLDisplayPortletInstanceConfiguration configuration, DDMStructure ddmStructure)
			throws PortalException {

		ServiceContext serviceContext = ddlEditorService.getServiceContextForScopeGroup(themeDisplay);
		DDLRecordSet ddlRecordSet = ddlEditorService.getOrCreateDDLRecordSet(ddmStructure, serviceContext);
		boolean hasAddPermission = ddlPermissionService.hasAddPermission(themeDisplay, ddlRecordSet);

		renderRequest.setAttribute("hasAddPermission", hasAddPermission);
		renderRequest.setAttribute("ddmStructure", ddmStructure);
		renderRequest.setAttribute("ddlRecordSetId", ddlRecordSet.getRecordSetId());
		renderRequest.setAttribute("showLoginOption", configuration.showLoginOption());

	}

	public void setRenderRequestAttributesUsingDDLRecord(RenderRequest renderRequest, ThemeDisplay themeDisplay, long ddlRecordId) throws PortalException {

		DDLRecord ddlRecord = ddlRecordService.getRecord(ddlRecordId);
		DDLRecordSet ddlRecordSet = ddlRecord.getRecordSet();
		DDMStructure ddmStructure = ddlRecordSet.getDDMStructure();
		boolean hasAddPermission = ddlPermissionService.hasAddPermission(themeDisplay, ddlRecordSet);

		renderRequest.setAttribute("hasAddPermission", hasAddPermission);
		renderRequest.setAttribute("ddmStructure", ddmStructure);
		renderRequest.setAttribute("ddlRecordSetId", ddlRecordSet.getRecordSetId());
		renderRequest.setAttribute("showLoginOption", true);

	}

}
