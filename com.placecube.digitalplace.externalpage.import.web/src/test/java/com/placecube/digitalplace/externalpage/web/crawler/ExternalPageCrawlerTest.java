package com.placecube.digitalplace.externalpage.web.crawler;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.net.URISyntaxException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalServiceUtil;
import com.placecube.digitalplace.externalpage.web.constants.CrawlConstants;
import com.placecube.digitalplace.externalpage.web.service.CrawlUtil;

import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.parser.TextParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CrawlUtil.class, ExternalPageLocalServiceUtil.class, CompanyThreadLocal.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@SuppressStaticInitializationFor(value = { "com.placecube.digitalplace.externalpage.service.ExternalPageLocalServiceUtil" })
public class ExternalPageCrawlerTest extends PowerMockito {

	private static final String PAGE_URL = "pageURL";
	private static final String URL = "urlValue";

	@InjectMocks
	private ExternalPageCrawler externalPageCrawler;

	@Mock
	private CrawlController mockCrawlController;

	@Mock
	private List<String> mockCustomData;

	@Mock
	private ExternalPage mockExternalPage;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	@Mock
	private HtmlParseData mockHtmlParseData;

	@Mock
	private Page mockPage;

	@Mock
	private TextParseData mockTextParseData;

	@Mock
	private WebURL mockWebURL;

	@Before
	public void setUp() {
		mockStatic(CrawlUtil.class, ExternalPageLocalServiceUtil.class, CompanyThreadLocal.class);
	}

	@Test
	public void shouldVisit_WhenUrlIsInvalid_ThenReturnsFalse() {
		when(mockWebURL.getURL()).thenReturn(URL);
		when(CrawlUtil.isValidURL(URL.toLowerCase())).thenReturn(false);

		boolean result = externalPageCrawler.shouldVisit(mockPage, mockWebURL);

		assertFalse(result);
	}

	@Test
	public void shouldVisit_WhenUrlIsValidAndDoesNotHaveMatchingDomain_ThenReturnsFalse() throws URISyntaxException {
		when(mockWebURL.getURL()).thenReturn(URL);
		when(CrawlUtil.isValidURL(URL.toLowerCase())).thenReturn(true);
		when(CrawlUtil.getDomain(URL)).thenReturn("domainValue");
		when(CrawlUtil.getPageURL(mockPage)).thenReturn(PAGE_URL);
		when(CrawlUtil.getDomain(PAGE_URL)).thenReturn("differentDomain");

		boolean result = externalPageCrawler.shouldVisit(mockPage, mockWebURL);

		assertFalse(result);
	}

	@Test
	public void shouldVisit_WhenUrlIsValidAndDomainMatches_ThenReturnsTrue() throws URISyntaxException {
		when(mockWebURL.getURL()).thenReturn(URL);
		when(CrawlUtil.isValidURL(URL.toLowerCase())).thenReturn(true);
		when(CrawlUtil.getDomain(URL)).thenReturn("domainValue");
		when(CrawlUtil.getPageURL(mockPage)).thenReturn(PAGE_URL);
		when(CrawlUtil.getDomain(PAGE_URL)).thenReturn("domainValue");

		boolean result = externalPageCrawler.shouldVisit(mockPage, mockWebURL);

		assertTrue(result);
	}

	@Test
	public void shouldVisit_WhenUrlIsValidAndExceptionCheckingForDomain_ThenReturnsFalse() throws URISyntaxException {
		when(mockWebURL.getURL()).thenReturn(URL);
		when(CrawlUtil.isValidURL(URL.toLowerCase())).thenReturn(true);
		when(CrawlUtil.getDomain(URL)).thenReturn("domainValue");
		when(CrawlUtil.getPageURL(mockPage)).thenReturn(PAGE_URL);
		when(CrawlUtil.getDomain(PAGE_URL)).thenThrow(new URISyntaxException("input", "reason"));

		boolean result = externalPageCrawler.shouldVisit(mockPage, mockWebURL);

		assertFalse(result);
	}

	@Test
	public void visit_WhenPageDataIsHTMLAndExceptionRetrievingCompanyId_ThenDoesNotAddorUpdateExternalPage() throws PortalException {
		when(mockPage.getParseData()).thenReturn(mockHtmlParseData);
		when(CrawlUtil.getPageURL(mockPage)).thenReturn(PAGE_URL);
		when(CrawlUtil.getCustomData(mockCrawlController)).thenReturn(mockCustomData);
		when(CrawlUtil.getIdFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_COMPANY_ID)).thenThrow(new PortalException());

		externalPageCrawler.visit(mockPage);

		verifyStatic(ExternalPageLocalServiceUtil.class, never());
		ExternalPageLocalServiceUtil.addOrUpdateExternalPage(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyBoolean());
	}

	@Test
	public void visit_WhenPageDataIsHTMLAndExceptionRetrievingGroupId_ThenDoesNotAddorUpdateExternalPage() throws PortalException {
		when(mockPage.getParseData()).thenReturn(mockHtmlParseData);
		when(CrawlUtil.getPageURL(mockPage)).thenReturn(PAGE_URL);
		when(CrawlUtil.getCustomData(mockCrawlController)).thenReturn(mockCustomData);
		when(CrawlUtil.getIdFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_COMPANY_ID)).thenReturn(123l);
		when(CrawlUtil.getIdFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_GROUP_ID)).thenThrow(new PortalException());

		externalPageCrawler.visit(mockPage);

		verifyStatic(ExternalPageLocalServiceUtil.class, never());
		ExternalPageLocalServiceUtil.addOrUpdateExternalPage(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyBoolean());
	}

	@Test
	public void visit_WhenPageDataIsHTMLAndExceptionRetrievingUserId_ThenDoesNotAddorUpdateExternalPage() throws PortalException {
		when(mockPage.getParseData()).thenReturn(mockHtmlParseData);
		when(CrawlUtil.getPageURL(mockPage)).thenReturn(PAGE_URL);
		when(CrawlUtil.getCustomData(mockCrawlController)).thenReturn(mockCustomData);
		when(CrawlUtil.getIdFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_COMPANY_ID)).thenReturn(123l);
		when(CrawlUtil.getIdFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_GROUP_ID)).thenReturn(56l);
		when(CrawlUtil.getIdFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_USER_ID)).thenThrow(new PortalException());

		externalPageCrawler.visit(mockPage);

		verifyStatic(ExternalPageLocalServiceUtil.class, never());
		ExternalPageLocalServiceUtil.addOrUpdateExternalPage(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyBoolean());
	}

	@Test
	public void visit_WhenPageDataIsHTMLAndNoError_AndIndexParameterIsFalse_ThenDoesNotAddOrUpdateExternalPage() throws PortalException {
		final String content = "contentValue";
		final String title = "titleValue";
		final boolean status = false;

		when(mockPage.getParseData()).thenReturn(mockHtmlParseData);
		when(CrawlUtil.getBooleanFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_INDEX_CONTENT)).thenReturn(false);

		externalPageCrawler.visit(mockPage);

		verifyStatic(ExternalPageLocalServiceUtil.class, never());
		ExternalPageLocalServiceUtil.addOrUpdateExternalPage(123l, 56l, 879l, PAGE_URL, title, content, status);
	}

	@Test
	public void visit_WhenPageDataIsHTMLAndNoError_AndIndexParameterIsTrue_ThenAddsOrUpdatesExternalPage() throws PortalException {
		final String htmlContent = "<p>contentValue</p>";
		final String content = "contentValue";
		final String title = "titleValue";
		final String xpath = "//xpath";
		final boolean status = true;
		final long originalCompanyId = 43242l;
		final long companyId = 123l;

		when(mockPage.getParseData()).thenReturn(mockHtmlParseData);
		when(CrawlUtil.getPageURL(mockPage)).thenReturn(PAGE_URL);
		when(CrawlUtil.getCustomData(mockCrawlController)).thenReturn(mockCustomData);
		when(CrawlUtil.getIdFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_COMPANY_ID)).thenReturn(companyId);
		when(CrawlUtil.getIdFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_GROUP_ID)).thenReturn(56l);
		when(CrawlUtil.getIdFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_USER_ID)).thenReturn(879l);
		when(CrawlUtil.getStringFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_XPATH)).thenReturn(xpath);
		when(CrawlUtil.getStringFromCustomData(mockCustomData, CrawlConstants.CUSTOM_DATA_INDEX_CONTENT)).thenReturn(CrawlConstants.ENTRY_PUBLISHED);
		when(mockHtmlParseData.getTitle()).thenReturn(title);
		when(mockHtmlParseData.getHtml()).thenReturn(htmlContent);
		when(CrawlUtil.extractContent(htmlContent, xpath)).thenReturn(content);
		when(mockExternalPageLocalService.addOrUpdateExternalPage(123l, 56l, 879l, PAGE_URL, title, content, Boolean.TRUE)).thenReturn(mockExternalPage);
		when(CompanyThreadLocal.getCompanyId()).thenReturn(originalCompanyId);
		externalPageCrawler.visit(mockPage);

		verifyStatic(CompanyThreadLocal.class, times(1));
		CompanyThreadLocal.setCompanyId(companyId);

		verifyStatic(ExternalPageLocalServiceUtil.class, times(1));
		ExternalPageLocalServiceUtil.addOrUpdateExternalPage(123l, 56l, 879l, PAGE_URL, title, content, status);

		verifyStatic(CompanyThreadLocal.class, times(1));
		CompanyThreadLocal.setCompanyId(originalCompanyId);

	}

	@Test
	public void visit_WhenPageDataIsNotHTML_ThenNoActionIsPerformed() throws PortalException {
		when(mockPage.getParseData()).thenReturn(mockTextParseData);

		externalPageCrawler.visit(mockPage);

		verifyStatic(CrawlUtil.class, never());
		CrawlUtil.getCustomData(any());

		verifyStatic(CrawlUtil.class, never());
		CrawlUtil.getIdFromCustomData(any(), any());

		verifyStatic(ExternalPageLocalServiceUtil.class, never());
		ExternalPageLocalServiceUtil.addOrUpdateExternalPage(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyBoolean());
	}
}
