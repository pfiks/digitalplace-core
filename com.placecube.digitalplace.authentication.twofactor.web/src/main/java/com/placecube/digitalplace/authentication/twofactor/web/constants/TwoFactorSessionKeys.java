package com.placecube.digitalplace.authentication.twofactor.web.constants;

public final class TwoFactorSessionKeys {

	public static final String TWO_FACTOR_REDIRECT = "TWO_FACTOR_REDIRECT";

	public static final String TWO_FACTOR_USER_ID = "TWO_FACTOR_USER_ID";

	public static final String TWO_FACTOR_VALIDATED_USER_ID = "TWO_FACTOR_VALIDATED_USER_ID";

	public static final String TWO_FACTOR_WEB_DIGEST = "TWO_FACTOR_WEB_DIGEST";

	public static final String TWO_FACTOR_WEB_KEY = "TWO_FACTOR_WEB_KEY";

	private TwoFactorSessionKeys() {
	}
}
