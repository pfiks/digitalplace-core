package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.model.FilterField;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.service.DDLFieldFilterService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.DDL_FIELD_FILTER_PORTLET, service = ConfigurationAction.class)
public class DDLFieldFilterPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Reference
	private DDLFieldFilterService ddlFieldFilterService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	public Map<String, String> getFilterFieldAttributesMap(FilterField filterField) {
		Map<String, String> attributes = new HashMap<>();
		attributes.put("type", filterField.getType());
		return attributes;
	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		DDLFieldFilterPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(DDLFieldFilterPortletInstanceConfiguration.class, themeDisplay);
		List<DDMStructure> availableDDMStructures = ddlConfigurationService.getAvailableDDMStructures(themeDisplay.getScopeGroup());

		httpServletRequest.setAttribute("configuration", configuration);
		httpServletRequest.setAttribute("availableStructures", availableDDMStructures);

		JSONArray jsonFilterConfig;
		if (Validator.isNotNull(configuration.jsonFilterConfig())) {
			jsonFilterConfig = JSONFactoryUtil.createJSONArray(configuration.jsonFilterConfig());
		} else {
			jsonFilterConfig = JSONFactoryUtil.createJSONArray();
		}
		httpServletRequest.setAttribute(DDLFieldFilterPortletConfigurationConstants.JSON_FILTER_CONFIG, jsonFilterConfig);

		long ddmStructureId = configuration.ddmStructureId();

		if (ddmStructureId > 0) {
			DDMStructure ddmStructure = ddmStructureLocalService.fetchDDMStructure(ddmStructureId);
			Set<FilterField> filters = ddlFieldFilterService.getSupportedStructureFilterFields(ddmStructure.getDDMForm().getDDMFormFields());

			Map<String, Map<String, String>> optionsAttributes = filters.stream().collect(Collectors.toMap(FilterField::getFacetFieldName, this::getFilterFieldAttributesMap));

			httpServletRequest.setAttribute("fieldsAttributes", optionsAttributes);
			httpServletRequest.setAttribute("filterFields", filters);
		}

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long ddmStructureId = ParamUtil.getLong(actionRequest, DDLFieldFilterPortletConfigurationConstants.STRUCTURE_ID, 0);
		setPreference(actionRequest, DDLFieldFilterPortletConfigurationConstants.STRUCTURE_ID, String.valueOf(ddmStructureId));

		String jsonFilterConfig = ParamUtil.getString(actionRequest, DDLFieldFilterPortletConfigurationConstants.JSON_FILTER_CONFIG, StringPool.BLANK);
		setPreference(actionRequest, DDLFieldFilterPortletConfigurationConstants.JSON_FILTER_CONFIG, jsonFilterConfig);

		String header = ParamUtil.getString(actionRequest, DDLFieldFilterPortletConfigurationConstants.HEADER, StringPool.BLANK);
		setPreference(actionRequest, DDLFieldFilterPortletConfigurationConstants.HEADER, header);

		boolean invisible = ParamUtil.getBoolean(actionRequest, DDLFieldFilterPortletConfigurationConstants.INVISIBLE);
		setPreference(actionRequest, DDLFieldFilterPortletConfigurationConstants.INVISIBLE, String.valueOf(invisible));

		boolean showGroupNameAsFirstOption = ParamUtil.getBoolean(actionRequest, DDLFieldFilterPortletConfigurationConstants.SHOW_GROUP_NAME_AS_FIRST_OPTION);
		setPreference(actionRequest, DDLFieldFilterPortletConfigurationConstants.SHOW_GROUP_NAME_AS_FIRST_OPTION, String.valueOf(showGroupNameAsFirstOption));

		String internalPortletId = ParamUtil.getString(actionRequest, DDLFieldFilterPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK);

		if (Validator.isNull(internalPortletId)) {
			internalPortletId = StringUtil.randomString(6);
		}

		setPreference(actionRequest, DDLFieldFilterPortletConfigurationConstants.INTERNAL_PORTLET_ID, internalPortletId);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}
}
