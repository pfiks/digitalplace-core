package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceContextThreadLocal.class)
public class SiteInitializerBackupUtilsTest extends PowerMockito {

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServiceContext mockServiceContextCloned;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu1;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu2;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu3;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu4;

	@Mock
	private SiteNavigationMenuLocalService mockSiteNavigationMenuLocalService;

	@InjectMocks
	private SiteInitializerBackupUtils siteInitializerBackupUtils;

	@Before
	public void activateSetup() {
		mockStatic(ServiceContextThreadLocal.class);
	}

	@Test(expected = PortalException.class)
	public void createBackupGroup_WhenException_ThenThrowsPortalException() throws PortalException {
		long userId = 12;
		long groupId = 23;
		Locale locale = Locale.CANADA_FRENCH;
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.clone()).thenReturn(mockServiceContextCloned);
		when(mockServiceContextCloned.getUserId()).thenReturn(userId);
		when(mockServiceContextCloned.getLocale()).thenReturn(locale);
		Map<Locale, String> nameMap = Collections.singletonMap(locale, "backupGroupFor_" + groupId);
		when(mockGroupLocalService.addGroup(userId, GroupConstants.DEFAULT_PARENT_GROUP_ID, Group.class.getName(), 0, GroupConstants.DEFAULT_LIVE_GROUP_ID, nameMap, nameMap,
				GroupConstants.TYPE_SITE_PRIVATE, false, 0, "", true, false, mockServiceContextCloned)).thenThrow(new PortalException());

		siteInitializerBackupUtils.createBackupGroup(groupId);
	}

	@Test
	public void createBackupGroup_WhenNoError_ThenReturnsTheCreatedGroupId() throws PortalException {
		long userId = 12;
		long groupId = 23;
		long expected = 55;
		Locale locale = Locale.CANADA_FRENCH;
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockServiceContext.clone()).thenReturn(mockServiceContextCloned);
		when(mockServiceContextCloned.getUserId()).thenReturn(userId);
		when(mockServiceContextCloned.getLocale()).thenReturn(locale);
		Map<Locale, String> nameMap = Collections.singletonMap(locale, "backupGroupFor_" + groupId);
		when(mockGroupLocalService.addGroup(userId, GroupConstants.DEFAULT_PARENT_GROUP_ID, Group.class.getName(), 0, GroupConstants.DEFAULT_LIVE_GROUP_ID, nameMap, nameMap,
				GroupConstants.TYPE_SITE_PRIVATE, false, 0, "", true, false, mockServiceContextCloned)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(expected);

		long result = siteInitializerBackupUtils.createBackupGroup(groupId);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void hasContentToBackUp_WhenNoSiteMenusFound_ThenReturnsFalse() {
		long groupId = 12;
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenusCount(groupId)).thenReturn(0);

		boolean result = siteInitializerBackupUtils.hasContentToBackUp(groupId);

		assertFalse(result);
	}

	@Test
	public void hasContentToBackUp_WhenSiteMenusFound_ThenReturnsTrue() {
		long groupId = 12;
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenusCount(groupId)).thenReturn(1);

		boolean result = siteInitializerBackupUtils.hasContentToBackUp(groupId);

		assertTrue(result);
	}

	@Test
	public void moveSiteNavigationMenus_WhenNoMenusWithSameName_ThenUpdateMovedMenuToTheNewGroupId() {
		long currentId = 123;
		long updatedId = 234;
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(currentId)).thenReturn(Arrays.asList(mockSiteNavigationMenu1, mockSiteNavigationMenu2));

		String siteNavigationMenu1 = "siteNavigationMenu1";
		when(mockSiteNavigationMenu1.getName()).thenReturn(siteNavigationMenu1);
		when(mockSiteNavigationMenuLocalService.fetchSiteNavigationMenuByName(updatedId, siteNavigationMenu1)).thenReturn(null);
		String siteNavigationMenu2 = "siteNavigationMenu2";
		when(mockSiteNavigationMenu2.getName()).thenReturn(siteNavigationMenu2);
		when(mockSiteNavigationMenuLocalService.fetchSiteNavigationMenuByName(updatedId, siteNavigationMenu2)).thenReturn(null);

		siteInitializerBackupUtils.moveSiteNavigationMenus(currentId, updatedId);

		InOrder inOrder = inOrder(mockSiteNavigationMenu1, mockSiteNavigationMenu2, mockSiteNavigationMenuLocalService);
		inOrder.verify(mockSiteNavigationMenu1, times(1)).setGroupId(updatedId);
		inOrder.verify(mockSiteNavigationMenuLocalService, times(1)).updateSiteNavigationMenu(mockSiteNavigationMenu1);
		inOrder.verify(mockSiteNavigationMenu2, times(1)).setGroupId(updatedId);
		inOrder.verify(mockSiteNavigationMenuLocalService, times(1)).updateSiteNavigationMenu(mockSiteNavigationMenu2);
	}

	@Test
	public void moveSiteNavigationMenus_WhenMenusWithSameName_ThenRenameFetchedMenusAndUpdateMovedMenuToTheNewGroupId() {
		long currentId = 123;
		long updatedId = 234;
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(currentId)).thenReturn(Arrays.asList(mockSiteNavigationMenu1, mockSiteNavigationMenu2));

		String siteNavigationMenu1 = "siteNavigationMenu1";
		when(mockSiteNavigationMenu1.getName()).thenReturn(siteNavigationMenu1);
		when(mockSiteNavigationMenuLocalService.fetchSiteNavigationMenuByName(updatedId, siteNavigationMenu1)).thenReturn(mockSiteNavigationMenu3);
		String siteNavigationMenu2 = "siteNavigationMenu2";
		when(mockSiteNavigationMenu2.getName()).thenReturn(siteNavigationMenu2);
		when(mockSiteNavigationMenuLocalService.fetchSiteNavigationMenuByName(updatedId, siteNavigationMenu2)).thenReturn(mockSiteNavigationMenu4);

		when(mockSiteNavigationMenu3.getName()).thenReturn(siteNavigationMenu1);
		when(mockSiteNavigationMenu4.getName()).thenReturn(siteNavigationMenu2);

		siteInitializerBackupUtils.moveSiteNavigationMenus(currentId, updatedId);

		InOrder inOrder = inOrder(mockSiteNavigationMenu1, mockSiteNavigationMenu2, mockSiteNavigationMenuLocalService, mockSiteNavigationMenu3, mockSiteNavigationMenu4);
		inOrder.verify(mockSiteNavigationMenu3, times(1)).setName(siteNavigationMenu1 + StringPool.SPACE + StringPool.DASH + StringPool.SPACE + new SimpleDateFormat("dd MM yyyy HH:mm").format(new Date()));
		inOrder.verify(mockSiteNavigationMenuLocalService, times(1)).updateSiteNavigationMenu(mockSiteNavigationMenu3);
		inOrder.verify(mockSiteNavigationMenu1, times(1)).setGroupId(updatedId);
		inOrder.verify(mockSiteNavigationMenuLocalService, times(1)).updateSiteNavigationMenu(mockSiteNavigationMenu1);
		inOrder.verify(mockSiteNavigationMenu4, times(1)).setName(siteNavigationMenu2 + StringPool.SPACE + StringPool.DASH + StringPool.SPACE + new SimpleDateFormat("dd MM yyyy HH:mm").format(new Date()));
		inOrder.verify(mockSiteNavigationMenuLocalService, times(1)).updateSiteNavigationMenu(mockSiteNavigationMenu4);
		inOrder.verify(mockSiteNavigationMenu2, times(1)).setGroupId(updatedId);
		inOrder.verify(mockSiteNavigationMenuLocalService, times(1)).updateSiteNavigationMenu(mockSiteNavigationMenu2);
	}

	@Test
	public void removeBackupGroup_WhenExceptionRemovingTheGroup_ThenDoesNotThrowAnyError() throws PortalException {
		long groupId = 123;
		when(mockGroupLocalService.deleteGroup(groupId)).thenThrow(new PortalException());

		try {
			siteInitializerBackupUtils.removeBackupGroup(groupId);
		} catch (Exception e) {
			fail();
		}

	}

	@Test
	public void removeBackupGroup_WhenNoError_ThenRemovesTheGroup() throws PortalException {
		long groupId = 123;

		siteInitializerBackupUtils.removeBackupGroup(groupId);

		verify(mockGroupLocalService, times(1)).deleteGroup(groupId);
	}
}
