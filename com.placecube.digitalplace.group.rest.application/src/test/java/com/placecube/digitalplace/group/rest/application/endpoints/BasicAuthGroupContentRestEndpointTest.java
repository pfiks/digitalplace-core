package com.placecube.digitalplace.group.rest.application.endpoints;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.ddl.model.DynamicDataListEntry;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.digitalplace.group.rest.application.BasicAuthGroupRestApplication;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestException;
import com.placecube.digitalplace.group.rest.application.model.FilterDynamicDataListEntry;
import com.placecube.digitalplace.group.rest.application.model.ModelBuilder;
import com.placecube.digitalplace.group.rest.application.model.RestListResult;

@RunWith(PowerMockRunner.class)
@PrepareForTest(FilterDynamicDataListEntry.class)
public class BasicAuthGroupContentRestEndpointTest extends PowerMockito {

	private BasicAuthGroupContentRestEndpoint basicAuthGroupContentRestEndpoint;

	@Mock
	private BasicAuthGroupRestApplication mockBasicAuthGroupRestApplication;

	@Mock
	private DynamicDataListEntry mockDynamicDataListEntry1;

	@Mock
	private DynamicDataListEntry mockDynamicDataListEntry2;

	@Mock
	private DynamicDataListService mockDynamicDataListService;

	@Mock
	private FilterDynamicDataListEntry mockFilterDynamicDataListEntry1;

	@Mock
	private FilterDynamicDataListEntry mockFilterDynamicDataListEntry2;

	@Mock
	private ModelBuilder mockModelBuilder;

	@Mock
	private RestListResult mockRestListResult;

	@Before
	public void activateSetUp() {
		initMocks(this);
		mockStatic(FilterDynamicDataListEntry.class);

		when(mockBasicAuthGroupRestApplication.getDynamicDataListService()).thenReturn(mockDynamicDataListService);
		when(mockBasicAuthGroupRestApplication.getModelBuilder()).thenReturn(mockModelBuilder);

		basicAuthGroupContentRestEndpoint = new BasicAuthGroupContentRestEndpoint(mockBasicAuthGroupRestApplication);
	}

	@Test(expected = GroupRestException.class)
	public void getDynamicContentDetails_WhenExceptionRetrievingTheData_ThenThrowsGroupRestException() throws Exception {
		long recordSetId = 13;
		when(mockDynamicDataListService.getDDLRecordSetEntries(recordSetId)).thenReturn(null);

		basicAuthGroupContentRestEndpoint.getDynamicContentDetails(recordSetId);
	}

	@Test
	public void getDynamicContentDetails_WhenNoError_ThenConfiguresTheResultsInTheSuccessResponse() throws Exception {
		long recordSetId = 13;
		Response expectedResponse = Response.ok().build();
		List<DynamicDataListEntry> records = new LinkedList<>();
		records.add(mockDynamicDataListEntry1);
		records.add(mockDynamicDataListEntry2);
		when(mockDynamicDataListService.getDDLRecordSetEntries(recordSetId)).thenReturn(records);
		when(mockModelBuilder.initRestListResult()).thenReturn(mockRestListResult);
		when(mockModelBuilder.getSuccessResponse(mockRestListResult)).thenReturn(expectedResponse);
		when(FilterDynamicDataListEntry.create(mockDynamicDataListEntry1)).thenReturn(mockFilterDynamicDataListEntry1);
		when(FilterDynamicDataListEntry.create(mockDynamicDataListEntry2)).thenReturn(mockFilterDynamicDataListEntry2);

		Response result = basicAuthGroupContentRestEndpoint.getDynamicContentDetails(recordSetId);

		assertThat(result, sameInstance(expectedResponse));

		InOrder inOrder = inOrder(mockRestListResult, mockModelBuilder);
		inOrder.verify(mockRestListResult, times(1)).addResult(mockFilterDynamicDataListEntry1);
		inOrder.verify(mockRestListResult, times(1)).addResult(mockFilterDynamicDataListEntry2);
		inOrder.verify(mockModelBuilder, times(1)).getSuccessResponse(mockRestListResult);
	}
}
