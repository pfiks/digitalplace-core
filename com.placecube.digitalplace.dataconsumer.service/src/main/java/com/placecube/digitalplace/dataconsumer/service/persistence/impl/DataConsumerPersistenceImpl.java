/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.dataconsumer.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.placecube.digitalplace.dataconsumer.exception.NoSuchDataConsumerException;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.model.DataConsumerTable;
import com.placecube.digitalplace.dataconsumer.model.impl.DataConsumerImpl;
import com.placecube.digitalplace.dataconsumer.model.impl.DataConsumerModelImpl;
import com.placecube.digitalplace.dataconsumer.service.persistence.DataConsumerPersistence;
import com.placecube.digitalplace.dataconsumer.service.persistence.DataConsumerUtil;
import com.placecube.digitalplace.dataconsumer.service.persistence.impl.constants.Placecube_DataConsumerPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the data consumer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = DataConsumerPersistence.class)
public class DataConsumerPersistenceImpl
	extends BasePersistenceImpl<DataConsumer>
	implements DataConsumerPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>DataConsumerUtil</code> to access the data consumer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		DataConsumerImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the data consumers where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching data consumers
	 */
	@Override
	public List<DataConsumer> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the data consumers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<DataConsumer> list = null;

		if (useFinderCache) {
			list = (List<DataConsumer>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DataConsumer dataConsumer : list) {
					if (!uuid.equals(dataConsumer.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<DataConsumer>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByUuid_First(
			String uuid, OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByUuid_First(uuid, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByUuid_First(
		String uuid, OrderByComparator<DataConsumer> orderByComparator) {

		List<DataConsumer> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByUuid_Last(
			String uuid, OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByUuid_Last(uuid, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByUuid_Last(
		String uuid, OrderByComparator<DataConsumer> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<DataConsumer> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer[] findByUuid_PrevAndNext(
			long dataConsumerId, String uuid,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		uuid = Objects.toString(uuid, "");

		DataConsumer dataConsumer = findByPrimaryKey(dataConsumerId);

		Session session = null;

		try {
			session = openSession();

			DataConsumer[] array = new DataConsumerImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, dataConsumer, uuid, orderByComparator, true);

			array[1] = dataConsumer;

			array[2] = getByUuid_PrevAndNext(
				session, dataConsumer, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected DataConsumer getByUuid_PrevAndNext(
		Session session, DataConsumer dataConsumer, String uuid,
		OrderByComparator<DataConsumer> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(dataConsumer)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<DataConsumer> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the data consumers where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (DataConsumer dataConsumer :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(dataConsumer);
		}
	}

	/**
	 * Returns the number of data consumers where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching data consumers
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_DATACONSUMER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"dataConsumer.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(dataConsumer.uuid IS NULL OR dataConsumer.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the data consumer where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDataConsumerException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByUUID_G(String uuid, long groupId)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByUUID_G(uuid, groupId);

		if (dataConsumer == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchDataConsumerException(sb.toString());
		}

		return dataConsumer;
	}

	/**
	 * Returns the data consumer where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the data consumer where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof DataConsumer) {
			DataConsumer dataConsumer = (DataConsumer)result;

			if (!Objects.equals(uuid, dataConsumer.getUuid()) ||
				(groupId != dataConsumer.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<DataConsumer> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					DataConsumer dataConsumer = list.get(0);

					result = dataConsumer;

					cacheResult(dataConsumer);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (DataConsumer)result;
		}
	}

	/**
	 * Removes the data consumer where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the data consumer that was removed
	 */
	@Override
	public DataConsumer removeByUUID_G(String uuid, long groupId)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = findByUUID_G(uuid, groupId);

		return remove(dataConsumer);
	}

	/**
	 * Returns the number of data consumers where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching data consumers
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_DATACONSUMER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"dataConsumer.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(dataConsumer.uuid IS NULL OR dataConsumer.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"dataConsumer.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching data consumers
	 */
	@Override
	public List<DataConsumer> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<DataConsumer> list = null;

		if (useFinderCache) {
			list = (List<DataConsumer>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DataConsumer dataConsumer : list) {
					if (!uuid.equals(dataConsumer.getUuid()) ||
						(companyId != dataConsumer.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<DataConsumer>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<DataConsumer> orderByComparator) {

		List<DataConsumer> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<DataConsumer> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<DataConsumer> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer[] findByUuid_C_PrevAndNext(
			long dataConsumerId, String uuid, long companyId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		uuid = Objects.toString(uuid, "");

		DataConsumer dataConsumer = findByPrimaryKey(dataConsumerId);

		Session session = null;

		try {
			session = openSession();

			DataConsumer[] array = new DataConsumerImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, dataConsumer, uuid, companyId, orderByComparator,
				true);

			array[1] = dataConsumer;

			array[2] = getByUuid_C_PrevAndNext(
				session, dataConsumer, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected DataConsumer getByUuid_C_PrevAndNext(
		Session session, DataConsumer dataConsumer, String uuid, long companyId,
		OrderByComparator<DataConsumer> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(dataConsumer)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<DataConsumer> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the data consumers where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (DataConsumer dataConsumer :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(dataConsumer);
		}
	}

	/**
	 * Returns the number of data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching data consumers
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_DATACONSUMER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"dataConsumer.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(dataConsumer.uuid IS NULL OR dataConsumer.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"dataConsumer.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByDDMFormInstanceId;
	private FinderPath _finderPathWithoutPaginationFindByDDMFormInstanceId;
	private FinderPath _finderPathCountByDDMFormInstanceId;

	/**
	 * Returns all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @return the matching data consumers
	 */
	@Override
	public List<DataConsumer> findByDDMFormInstanceId(long ddmFormInstanceId) {
		return findByDDMFormInstanceId(
			ddmFormInstanceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId, int start, int end) {

		return findByDDMFormInstanceId(ddmFormInstanceId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return findByDDMFormInstanceId(
			ddmFormInstanceId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByDDMFormInstanceId;
				finderArgs = new Object[] {ddmFormInstanceId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByDDMFormInstanceId;
			finderArgs = new Object[] {
				ddmFormInstanceId, start, end, orderByComparator
			};
		}

		List<DataConsumer> list = null;

		if (useFinderCache) {
			list = (List<DataConsumer>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DataConsumer dataConsumer : list) {
					if (ddmFormInstanceId !=
							dataConsumer.getDdmFormInstanceId()) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

			sb.append(_FINDER_COLUMN_DDMFORMINSTANCEID_DDMFORMINSTANCEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(ddmFormInstanceId);

				list = (List<DataConsumer>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByDDMFormInstanceId_First(
			long ddmFormInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByDDMFormInstanceId_First(
			ddmFormInstanceId, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("ddmFormInstanceId=");
		sb.append(ddmFormInstanceId);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the first data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByDDMFormInstanceId_First(
		long ddmFormInstanceId,
		OrderByComparator<DataConsumer> orderByComparator) {

		List<DataConsumer> list = findByDDMFormInstanceId(
			ddmFormInstanceId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByDDMFormInstanceId_Last(
			long ddmFormInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByDDMFormInstanceId_Last(
			ddmFormInstanceId, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("ddmFormInstanceId=");
		sb.append(ddmFormInstanceId);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the last data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByDDMFormInstanceId_Last(
		long ddmFormInstanceId,
		OrderByComparator<DataConsumer> orderByComparator) {

		int count = countByDDMFormInstanceId(ddmFormInstanceId);

		if (count == 0) {
			return null;
		}

		List<DataConsumer> list = findByDDMFormInstanceId(
			ddmFormInstanceId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer[] findByDDMFormInstanceId_PrevAndNext(
			long dataConsumerId, long ddmFormInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = findByPrimaryKey(dataConsumerId);

		Session session = null;

		try {
			session = openSession();

			DataConsumer[] array = new DataConsumerImpl[3];

			array[0] = getByDDMFormInstanceId_PrevAndNext(
				session, dataConsumer, ddmFormInstanceId, orderByComparator,
				true);

			array[1] = dataConsumer;

			array[2] = getByDDMFormInstanceId_PrevAndNext(
				session, dataConsumer, ddmFormInstanceId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected DataConsumer getByDDMFormInstanceId_PrevAndNext(
		Session session, DataConsumer dataConsumer, long ddmFormInstanceId,
		OrderByComparator<DataConsumer> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

		sb.append(_FINDER_COLUMN_DDMFORMINSTANCEID_DDMFORMINSTANCEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(ddmFormInstanceId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(dataConsumer)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<DataConsumer> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the data consumers where ddmFormInstanceId = &#63; from the database.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 */
	@Override
	public void removeByDDMFormInstanceId(long ddmFormInstanceId) {
		for (DataConsumer dataConsumer :
				findByDDMFormInstanceId(
					ddmFormInstanceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(dataConsumer);
		}
	}

	/**
	 * Returns the number of data consumers where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @return the number of matching data consumers
	 */
	@Override
	public int countByDDMFormInstanceId(long ddmFormInstanceId) {
		FinderPath finderPath = _finderPathCountByDDMFormInstanceId;

		Object[] finderArgs = new Object[] {ddmFormInstanceId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_DATACONSUMER_WHERE);

			sb.append(_FINDER_COLUMN_DDMFORMINSTANCEID_DDMFORMINSTANCEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(ddmFormInstanceId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_DDMFORMINSTANCEID_DDMFORMINSTANCEID_2 =
			"dataConsumer.ddmFormInstanceId = ?";

	private FinderPath _finderPathWithPaginationFindByDDMDataProviderInstanceId;
	private FinderPath
		_finderPathWithoutPaginationFindByDDMDataProviderInstanceId;
	private FinderPath _finderPathCountByDDMDataProviderInstanceId;

	/**
	 * Returns all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @return the matching data consumers
	 */
	@Override
	public List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId) {

		return findByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId, int start, int end) {

		return findByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return findByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByDDMDataProviderInstanceId;
				finderArgs = new Object[] {ddmDataProviderInstanceId};
			}
		}
		else if (useFinderCache) {
			finderPath =
				_finderPathWithPaginationFindByDDMDataProviderInstanceId;
			finderArgs = new Object[] {
				ddmDataProviderInstanceId, start, end, orderByComparator
			};
		}

		List<DataConsumer> list = null;

		if (useFinderCache) {
			list = (List<DataConsumer>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DataConsumer dataConsumer : list) {
					if (ddmDataProviderInstanceId !=
							dataConsumer.getDdmDataProviderInstanceId()) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

			sb.append(
				_FINDER_COLUMN_DDMDATAPROVIDERINSTANCEID_DDMDATAPROVIDERINSTANCEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(ddmDataProviderInstanceId);

				list = (List<DataConsumer>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByDDMDataProviderInstanceId_First(
			long ddmDataProviderInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByDDMDataProviderInstanceId_First(
			ddmDataProviderInstanceId, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("ddmDataProviderInstanceId=");
		sb.append(ddmDataProviderInstanceId);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the first data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByDDMDataProviderInstanceId_First(
		long ddmDataProviderInstanceId,
		OrderByComparator<DataConsumer> orderByComparator) {

		List<DataConsumer> list = findByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByDDMDataProviderInstanceId_Last(
			long ddmDataProviderInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByDDMDataProviderInstanceId_Last(
			ddmDataProviderInstanceId, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("ddmDataProviderInstanceId=");
		sb.append(ddmDataProviderInstanceId);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the last data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByDDMDataProviderInstanceId_Last(
		long ddmDataProviderInstanceId,
		OrderByComparator<DataConsumer> orderByComparator) {

		int count = countByDDMDataProviderInstanceId(ddmDataProviderInstanceId);

		if (count == 0) {
			return null;
		}

		List<DataConsumer> list = findByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer[] findByDDMDataProviderInstanceId_PrevAndNext(
			long dataConsumerId, long ddmDataProviderInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = findByPrimaryKey(dataConsumerId);

		Session session = null;

		try {
			session = openSession();

			DataConsumer[] array = new DataConsumerImpl[3];

			array[0] = getByDDMDataProviderInstanceId_PrevAndNext(
				session, dataConsumer, ddmDataProviderInstanceId,
				orderByComparator, true);

			array[1] = dataConsumer;

			array[2] = getByDDMDataProviderInstanceId_PrevAndNext(
				session, dataConsumer, ddmDataProviderInstanceId,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected DataConsumer getByDDMDataProviderInstanceId_PrevAndNext(
		Session session, DataConsumer dataConsumer,
		long ddmDataProviderInstanceId,
		OrderByComparator<DataConsumer> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

		sb.append(
			_FINDER_COLUMN_DDMDATAPROVIDERINSTANCEID_DDMDATAPROVIDERINSTANCEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(ddmDataProviderInstanceId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(dataConsumer)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<DataConsumer> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the data consumers where ddmDataProviderInstanceId = &#63; from the database.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 */
	@Override
	public void removeByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId) {

		for (DataConsumer dataConsumer :
				findByDDMDataProviderInstanceId(
					ddmDataProviderInstanceId, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(dataConsumer);
		}
	}

	/**
	 * Returns the number of data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @return the number of matching data consumers
	 */
	@Override
	public int countByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId) {

		FinderPath finderPath = _finderPathCountByDDMDataProviderInstanceId;

		Object[] finderArgs = new Object[] {ddmDataProviderInstanceId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_DATACONSUMER_WHERE);

			sb.append(
				_FINDER_COLUMN_DDMDATAPROVIDERINSTANCEID_DDMDATAPROVIDERINSTANCEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(ddmDataProviderInstanceId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_DDMDATAPROVIDERINSTANCEID_DDMDATAPROVIDERINSTANCEID_2 =
			"dataConsumer.ddmDataProviderInstanceId = ?";

	private FinderPath _finderPathWithPaginationFindByCompanyIdAndGroupId;
	private FinderPath _finderPathWithoutPaginationFindByCompanyIdAndGroupId;
	private FinderPath _finderPathCountByCompanyIdAndGroupId;

	/**
	 * Returns all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching data consumers
	 */
	@Override
	public List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId) {

		return findByCompanyIdAndGroupId(
			companyId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId, int start, int end) {

		return findByCompanyIdAndGroupId(companyId, groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return findByCompanyIdAndGroupId(
			companyId, groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	@Override
	public List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByCompanyIdAndGroupId;
				finderArgs = new Object[] {companyId, groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByCompanyIdAndGroupId;
			finderArgs = new Object[] {
				companyId, groupId, start, end, orderByComparator
			};
		}

		List<DataConsumer> list = null;

		if (useFinderCache) {
			list = (List<DataConsumer>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DataConsumer dataConsumer : list) {
					if ((companyId != dataConsumer.getCompanyId()) ||
						(groupId != dataConsumer.getGroupId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYIDANDGROUPID_COMPANYID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDANDGROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				list = (List<DataConsumer>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByCompanyIdAndGroupId_First(
			long companyId, long groupId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByCompanyIdAndGroupId_First(
			companyId, groupId, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the first data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByCompanyIdAndGroupId_First(
		long companyId, long groupId,
		OrderByComparator<DataConsumer> orderByComparator) {

		List<DataConsumer> list = findByCompanyIdAndGroupId(
			companyId, groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer findByCompanyIdAndGroupId_Last(
			long companyId, long groupId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByCompanyIdAndGroupId_Last(
			companyId, groupId, orderByComparator);

		if (dataConsumer != null) {
			return dataConsumer;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("companyId=");
		sb.append(companyId);

		sb.append(", groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchDataConsumerException(sb.toString());
	}

	/**
	 * Returns the last data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	@Override
	public DataConsumer fetchByCompanyIdAndGroupId_Last(
		long companyId, long groupId,
		OrderByComparator<DataConsumer> orderByComparator) {

		int count = countByCompanyIdAndGroupId(companyId, groupId);

		if (count == 0) {
			return null;
		}

		List<DataConsumer> list = findByCompanyIdAndGroupId(
			companyId, groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer[] findByCompanyIdAndGroupId_PrevAndNext(
			long dataConsumerId, long companyId, long groupId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = findByPrimaryKey(dataConsumerId);

		Session session = null;

		try {
			session = openSession();

			DataConsumer[] array = new DataConsumerImpl[3];

			array[0] = getByCompanyIdAndGroupId_PrevAndNext(
				session, dataConsumer, companyId, groupId, orderByComparator,
				true);

			array[1] = dataConsumer;

			array[2] = getByCompanyIdAndGroupId_PrevAndNext(
				session, dataConsumer, companyId, groupId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected DataConsumer getByCompanyIdAndGroupId_PrevAndNext(
		Session session, DataConsumer dataConsumer, long companyId,
		long groupId, OrderByComparator<DataConsumer> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_DATACONSUMER_WHERE);

		sb.append(_FINDER_COLUMN_COMPANYIDANDGROUPID_COMPANYID_2);

		sb.append(_FINDER_COLUMN_COMPANYIDANDGROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(DataConsumerModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(companyId);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(dataConsumer)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<DataConsumer> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the data consumers where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	@Override
	public void removeByCompanyIdAndGroupId(long companyId, long groupId) {
		for (DataConsumer dataConsumer :
				findByCompanyIdAndGroupId(
					companyId, groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(dataConsumer);
		}
	}

	/**
	 * Returns the number of data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching data consumers
	 */
	@Override
	public int countByCompanyIdAndGroupId(long companyId, long groupId) {
		FinderPath finderPath = _finderPathCountByCompanyIdAndGroupId;

		Object[] finderArgs = new Object[] {companyId, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_DATACONSUMER_WHERE);

			sb.append(_FINDER_COLUMN_COMPANYIDANDGROUPID_COMPANYID_2);

			sb.append(_FINDER_COLUMN_COMPANYIDANDGROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(companyId);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDANDGROUPID_COMPANYID_2 =
		"dataConsumer.companyId = ? AND ";

	private static final String _FINDER_COLUMN_COMPANYIDANDGROUPID_GROUPID_2 =
		"dataConsumer.groupId = ?";

	public DataConsumerPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(DataConsumer.class);

		setModelImplClass(DataConsumerImpl.class);
		setModelPKClass(long.class);

		setTable(DataConsumerTable.INSTANCE);
	}

	/**
	 * Caches the data consumer in the entity cache if it is enabled.
	 *
	 * @param dataConsumer the data consumer
	 */
	@Override
	public void cacheResult(DataConsumer dataConsumer) {
		entityCache.putResult(
			DataConsumerImpl.class, dataConsumer.getPrimaryKey(), dataConsumer);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {dataConsumer.getUuid(), dataConsumer.getGroupId()},
			dataConsumer);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the data consumers in the entity cache if it is enabled.
	 *
	 * @param dataConsumers the data consumers
	 */
	@Override
	public void cacheResult(List<DataConsumer> dataConsumers) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (dataConsumers.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (DataConsumer dataConsumer : dataConsumers) {
			if (entityCache.getResult(
					DataConsumerImpl.class, dataConsumer.getPrimaryKey()) ==
						null) {

				cacheResult(dataConsumer);
			}
		}
	}

	/**
	 * Clears the cache for all data consumers.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(DataConsumerImpl.class);

		finderCache.clearCache(DataConsumerImpl.class);
	}

	/**
	 * Clears the cache for the data consumer.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DataConsumer dataConsumer) {
		entityCache.removeResult(DataConsumerImpl.class, dataConsumer);
	}

	@Override
	public void clearCache(List<DataConsumer> dataConsumers) {
		for (DataConsumer dataConsumer : dataConsumers) {
			entityCache.removeResult(DataConsumerImpl.class, dataConsumer);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(DataConsumerImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(DataConsumerImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		DataConsumerModelImpl dataConsumerModelImpl) {

		Object[] args = new Object[] {
			dataConsumerModelImpl.getUuid(), dataConsumerModelImpl.getGroupId()
		};

		finderCache.putResult(_finderPathCountByUUID_G, args, Long.valueOf(1));
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, dataConsumerModelImpl);
	}

	/**
	 * Creates a new data consumer with the primary key. Does not add the data consumer to the database.
	 *
	 * @param dataConsumerId the primary key for the new data consumer
	 * @return the new data consumer
	 */
	@Override
	public DataConsumer create(long dataConsumerId) {
		DataConsumer dataConsumer = new DataConsumerImpl();

		dataConsumer.setNew(true);
		dataConsumer.setPrimaryKey(dataConsumerId);

		String uuid = PortalUUIDUtil.generate();

		dataConsumer.setUuid(uuid);

		dataConsumer.setCompanyId(CompanyThreadLocal.getCompanyId());

		return dataConsumer;
	}

	/**
	 * Removes the data consumer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer that was removed
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer remove(long dataConsumerId)
		throws NoSuchDataConsumerException {

		return remove((Serializable)dataConsumerId);
	}

	/**
	 * Removes the data consumer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the data consumer
	 * @return the data consumer that was removed
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer remove(Serializable primaryKey)
		throws NoSuchDataConsumerException {

		Session session = null;

		try {
			session = openSession();

			DataConsumer dataConsumer = (DataConsumer)session.get(
				DataConsumerImpl.class, primaryKey);

			if (dataConsumer == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDataConsumerException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(dataConsumer);
		}
		catch (NoSuchDataConsumerException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DataConsumer removeImpl(DataConsumer dataConsumer) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(dataConsumer)) {
				dataConsumer = (DataConsumer)session.get(
					DataConsumerImpl.class, dataConsumer.getPrimaryKeyObj());
			}

			if (dataConsumer != null) {
				session.delete(dataConsumer);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (dataConsumer != null) {
			clearCache(dataConsumer);
		}

		return dataConsumer;
	}

	@Override
	public DataConsumer updateImpl(DataConsumer dataConsumer) {
		boolean isNew = dataConsumer.isNew();

		if (!(dataConsumer instanceof DataConsumerModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(dataConsumer.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					dataConsumer);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in dataConsumer proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom DataConsumer implementation " +
					dataConsumer.getClass());
		}

		DataConsumerModelImpl dataConsumerModelImpl =
			(DataConsumerModelImpl)dataConsumer;

		if (Validator.isNull(dataConsumer.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			dataConsumer.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (dataConsumer.getCreateDate() == null)) {
			if (serviceContext == null) {
				dataConsumer.setCreateDate(date);
			}
			else {
				dataConsumer.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!dataConsumerModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				dataConsumer.setModifiedDate(date);
			}
			else {
				dataConsumer.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(dataConsumer);
			}
			else {
				dataConsumer = (DataConsumer)session.merge(dataConsumer);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			DataConsumerImpl.class, dataConsumerModelImpl, false, true);

		cacheUniqueFindersCache(dataConsumerModelImpl);

		if (isNew) {
			dataConsumer.setNew(false);
		}

		dataConsumer.resetOriginalValues();

		return dataConsumer;
	}

	/**
	 * Returns the data consumer with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the data consumer
	 * @return the data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDataConsumerException {

		DataConsumer dataConsumer = fetchByPrimaryKey(primaryKey);

		if (dataConsumer == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDataConsumerException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return dataConsumer;
	}

	/**
	 * Returns the data consumer with the primary key or throws a <code>NoSuchDataConsumerException</code> if it could not be found.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer findByPrimaryKey(long dataConsumerId)
		throws NoSuchDataConsumerException {

		return findByPrimaryKey((Serializable)dataConsumerId);
	}

	/**
	 * Returns the data consumer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer, or <code>null</code> if a data consumer with the primary key could not be found
	 */
	@Override
	public DataConsumer fetchByPrimaryKey(long dataConsumerId) {
		return fetchByPrimaryKey((Serializable)dataConsumerId);
	}

	/**
	 * Returns all the data consumers.
	 *
	 * @return the data consumers
	 */
	@Override
	public List<DataConsumer> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of data consumers
	 */
	@Override
	public List<DataConsumer> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of data consumers
	 */
	@Override
	public List<DataConsumer> findAll(
		int start, int end, OrderByComparator<DataConsumer> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of data consumers
	 */
	@Override
	public List<DataConsumer> findAll(
		int start, int end, OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<DataConsumer> list = null;

		if (useFinderCache) {
			list = (List<DataConsumer>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_DATACONSUMER);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_DATACONSUMER;

				sql = sql.concat(DataConsumerModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<DataConsumer>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the data consumers from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (DataConsumer dataConsumer : findAll()) {
			remove(dataConsumer);
		}
	}

	/**
	 * Returns the number of data consumers.
	 *
	 * @return the number of data consumers
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_DATACONSUMER);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "dataConsumerId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_DATACONSUMER;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return DataConsumerModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the data consumer persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByDDMFormInstanceId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDDMFormInstanceId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"ddmFormInstanceId"}, true);

		_finderPathWithoutPaginationFindByDDMFormInstanceId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByDDMFormInstanceId", new String[] {Long.class.getName()},
			new String[] {"ddmFormInstanceId"}, true);

		_finderPathCountByDDMFormInstanceId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByDDMFormInstanceId", new String[] {Long.class.getName()},
			new String[] {"ddmFormInstanceId"}, false);

		_finderPathWithPaginationFindByDDMDataProviderInstanceId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByDDMDataProviderInstanceId",
				new String[] {
					Long.class.getName(), Integer.class.getName(),
					Integer.class.getName(), OrderByComparator.class.getName()
				},
				new String[] {"ddmDataProviderInstanceId"}, true);

		_finderPathWithoutPaginationFindByDDMDataProviderInstanceId =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByDDMDataProviderInstanceId",
				new String[] {Long.class.getName()},
				new String[] {"ddmDataProviderInstanceId"}, true);

		_finderPathCountByDDMDataProviderInstanceId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByDDMDataProviderInstanceId",
			new String[] {Long.class.getName()},
			new String[] {"ddmDataProviderInstanceId"}, false);

		_finderPathWithPaginationFindByCompanyIdAndGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyIdAndGroupId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"companyId", "groupId"}, true);

		_finderPathWithoutPaginationFindByCompanyIdAndGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCompanyIdAndGroupId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"companyId", "groupId"}, true);

		_finderPathCountByCompanyIdAndGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyIdAndGroupId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"companyId", "groupId"}, false);

		DataConsumerUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		DataConsumerUtil.setPersistence(null);

		entityCache.removeCache(DataConsumerImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_DataConsumerPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_DataConsumerPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_DataConsumerPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_DATACONSUMER =
		"SELECT dataConsumer FROM DataConsumer dataConsumer";

	private static final String _SQL_SELECT_DATACONSUMER_WHERE =
		"SELECT dataConsumer FROM DataConsumer dataConsumer WHERE ";

	private static final String _SQL_COUNT_DATACONSUMER =
		"SELECT COUNT(dataConsumer) FROM DataConsumer dataConsumer";

	private static final String _SQL_COUNT_DATACONSUMER_WHERE =
		"SELECT COUNT(dataConsumer) FROM DataConsumer dataConsumer WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "dataConsumer.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No DataConsumer exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No DataConsumer exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		DataConsumerPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}