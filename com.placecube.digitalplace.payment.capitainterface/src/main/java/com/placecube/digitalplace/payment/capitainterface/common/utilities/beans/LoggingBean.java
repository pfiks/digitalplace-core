package com.placecube.digitalplace.payment.capitainterface.common.utilities.beans;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple bean class to hold all the data that is required to be logged in one single error statement
 *
 *
 */
public class LoggingBean {
    private Service service;
    private SourceSystem sourceSystem;
    private ErrorCode errorCode;
    private Map<DataField, String> diagnosticDataFields;
    private String wsdlOperationName;



    public LoggingBean(Service service, SourceSystem sourceSystem,
            ErrorCode errorCode, Map<DataField, String> diagnosticDataFields, String wsdlOperationName) {
        super();
        this.service = service;
        this.sourceSystem = sourceSystem;
        this.errorCode = errorCode;
        this.diagnosticDataFields = diagnosticDataFields!=null?diagnosticDataFields:new HashMap<DataField, String>();
        this.wsdlOperationName = wsdlOperationName;
    }

    public LoggingBean(Service service, SourceSystem sourceSystem,
            ErrorCode errorCode,
             String wsdlOperationName, DiagnosticData... diagnosticDataFields) {
        super();
        this.service = service;
        this.sourceSystem = sourceSystem;
        this.errorCode = errorCode;
        this.wsdlOperationName = wsdlOperationName;


        //build the Map from the DiagnosticData varargs
        this.diagnosticDataFields = new HashMap<DataField, String>();
        for (DiagnosticData dd : diagnosticDataFields){
            this.diagnosticDataFields.put(dd.getDataField(), dd.getValue());
        }


    }

    /**
     * @return the service
     */
    public Service getService() {
        return service;
    }
    /**
     * @param service the service to set
     */
    public void setService(Service service) {
        this.service = service;
    }
    /**
     * @return the sourceSystem
     */
    public SourceSystem getSourceSystem() {
        return sourceSystem;
    }
    /**
     * @param sourceSystem the sourceSystem to set
     */
    public void setSourceSystem(SourceSystem sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
    /**
     * @return the errorCode
     */
    public ErrorCode getErrorCode() {
        return errorCode;
    }
    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
    /**
     * @return the diagnosticDataFields
     */
    public Map<DataField, String> getDiagnosticDataFields() {
        return diagnosticDataFields;
    }
    /**
     * @param diagnosticDataFields the diagnosticDataFields to set
     */
    public void setDiagnosticDataFields(Map<DataField, String> diagnosticDataFields) {
        this.diagnosticDataFields = diagnosticDataFields;
    }
    public String getWsdlOperationName() {
        return wsdlOperationName;
    }
    public void setWsdlOperationName(String wsdlOperationName) {
        this.wsdlOperationName = wsdlOperationName;
    }



}
