package com.placecube.digitalplace.group.rest.application.filters;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import com.placecube.digitalplace.group.rest.application.GroupRestApplication;
import com.placecube.digitalplace.group.rest.application.service.RequestFiltersService;
import com.placecube.digitalplace.group.rest.application.service.SignatureBuilderService;

@Provider
@PreMatching
public class SignatureCheckerFilter implements ContainerRequestFilter {

	@Context
	private HttpServletRequest httpServletRequest;

	private RequestFiltersService requestFiltersService;
	private SignatureBuilderService signatureBuilderService;

	public SignatureCheckerFilter(GroupRestApplication groupRestApplication) {
		requestFiltersService = groupRestApplication.getRequestFiltersService();
		signatureBuilderService = groupRestApplication.getSignatureBuilderService();
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String signatureHeader = requestFiltersService.getSignatureHeader(requestContext);

		String applicationKey = requestFiltersService.getApiKeyHeader(requestContext);
		String dateHeader = requestFiltersService.getDateHeader(requestContext);
		String contextPath = requestFiltersService.getContextPath(httpServletRequest);
		String path = requestFiltersService.getPath(requestContext);
		String method = requestFiltersService.getMethod(requestContext);
		String body = requestFiltersService.getBody(requestContext);

		String evaluatedSignature = signatureBuilderService.getEvaluatedSignature(applicationKey, dateHeader, contextPath, path, method, body);
		signatureBuilderService.validateSignatureMatch(signatureHeader, evaluatedSignature);
	}

}
