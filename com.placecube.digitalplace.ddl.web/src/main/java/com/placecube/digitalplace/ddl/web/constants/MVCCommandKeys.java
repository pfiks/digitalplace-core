package com.placecube.digitalplace.ddl.web.constants;

public final class MVCCommandKeys {

	public static final String DELETE_ENTRY = "/ddl-web/delete-entry";

	public static final String FIELD_FILTER_SEARCH = "/ddl-web/field-filter-search";

	public static final String LOCATION_SEARCH = "/ddl-web/location-search";

	public static final String UPDATE_ENTRY = "/ddl-web/update-entry";

	public static final String VALIDATE_ENTRY = "/ddl-web/validate-entry";

	public static final String VIEW_ENTRY = "/ddl-web/view-entry";

	private MVCCommandKeys() {
	}

}
