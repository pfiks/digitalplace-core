package com.placecube.digitalplace.calendar.web.portlet;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.ResourceParameters;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.portlet.PortletURLUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarEvent;
import com.placecube.digitalplace.calendar.web.service.DigitalPlaceCalendarEventParserService;
import com.placecube.digitalplace.calendar.web.service.DigitalPlaceCalendarEventService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, PortletURLUtil.class, GetterUtil.class, LocalDateTime.class})
public class LoadEventsResourceCommandTest extends PowerMockito {

	@InjectMocks
	private LoadEventsResourceCommand loadEventsResourceCommand;

	@Mock
	private List<DigitalPlaceCalendarEvent> mockCalendarEvents;

	@Mock
	private DateTimeFormatter mockDateTimeFormatter;

	@Mock
	private DigitalPlaceCalendarEventParserService mockDigitalPlaceCalendarEventParserUtil;

	@Mock
	private DigitalPlaceCalendarEventService mockDigitalPlaceCalendarEventService;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private LocalDateTime mockStartDate;

	@Mock
	private LocalDateTime mockEndDate;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private PrintWriter mockPrintWriter;

	@Mock
	private ResourceParameters mockResourceParameters;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic( PropsUtil.class, PortletURLUtil.class, GetterUtil.class, LocalDateTime.class );
	}

	@Test
	public void serveResource_WhenNoError_ThenPrintWritesCalendarEvents() throws PortletException, IOException, PortalException {
		String currentURL = "currentURL";
		String start = "10.10.2021";
		String end = "11.10.2021";
		String eventsJSON = "json";
		long userId = 123;
		long[] groupIds = new long[] {1,2,3};


		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(PortletURLUtil.getCurrent(mockResourceRequest, mockResourceResponse)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(currentURL);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockDigitalPlaceCalendarEventService.getCalendarScopeGroupIds(mockThemeDisplay)).thenReturn(groupIds);
		when(mockResourceRequest.getResourceParameters()).thenReturn(mockResourceParameters);
		when(GetterUtil.getString(mockResourceParameters.getValue("start"))).thenReturn(start);
		when(GetterUtil.getString(mockResourceParameters.getValue("end"))).thenReturn(end);
		when(mockDigitalPlaceCalendarEventService.getCalendarEventsDateFormatter(mockThemeDisplay)).thenReturn(mockDateTimeFormatter);
		when(mockDigitalPlaceCalendarEventService.parseDate(start, mockDateTimeFormatter)).thenReturn(mockStartDate);
		when(mockDigitalPlaceCalendarEventService.parseDate(end, mockDateTimeFormatter)).thenReturn(mockEndDate);
		when(mockDigitalPlaceCalendarEventService.getCalendarEvents(userId, groupIds, mockStartDate, mockEndDate, mockThemeDisplay)).thenReturn(mockCalendarEvents);
		when(mockDigitalPlaceCalendarEventParserUtil.parseCalendarEvents(mockCalendarEvents, currentURL)).thenReturn(mockJSONArray);
		when(mockResourceResponse.getWriter()).thenReturn(mockPrintWriter);
		when(mockJSONArray.toString()).thenReturn(eventsJSON);

		loadEventsResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verify(mockPrintWriter, times(1)).write(eventsJSON);
	}

	@Test
	public void serveResource_WhenErrorGettingWriter_ThenDoesNotThrowException() throws IOException, PortalException {
		String currentURL = "currentURL";
		String start = "10.10.2021";
		String end = "11.10.2021";
		long userId = 123;
		long[] groupIds = new long[] {1,2,3};

		when(mockResourceRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(PortletURLUtil.getCurrent(mockResourceRequest, mockResourceResponse)).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(currentURL);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(mockDigitalPlaceCalendarEventService.getCalendarScopeGroupIds(mockThemeDisplay)).thenReturn(groupIds);
		when(mockResourceRequest.getResourceParameters()).thenReturn(mockResourceParameters);
		when(GetterUtil.getString(mockResourceParameters.getValue("start"))).thenReturn(start);
		when(GetterUtil.getString(mockResourceParameters.getValue("end"))).thenReturn(end);
		when(mockDigitalPlaceCalendarEventService.getCalendarEventsDateFormatter(mockThemeDisplay)).thenReturn(mockDateTimeFormatter);
		when(mockDigitalPlaceCalendarEventService.parseDate(start, mockDateTimeFormatter)).thenReturn(mockStartDate);
		when(mockDigitalPlaceCalendarEventService.parseDate(end, mockDateTimeFormatter)).thenReturn(mockEndDate);
		when(mockDigitalPlaceCalendarEventService.getCalendarEvents(userId, groupIds, mockStartDate, mockEndDate, mockThemeDisplay)).thenReturn(mockCalendarEvents);
		when(mockDigitalPlaceCalendarEventParserUtil.parseCalendarEvents(mockCalendarEvents, currentURL)).thenReturn(mockJSONArray);
		when(mockResourceResponse.getWriter()).thenThrow(new IOException());

		try {
			loadEventsResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);
		} catch (Exception e) {
			fail();
		}

	}

}
