package com.placecube.digitalplace.addresslookup.ordnancesurvey.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.ordnancesurvey.constants.OrdnanceSurveyConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HttpClients.class, EntityUtils.class })
public class AddressRetrievalServiceTest extends PowerMockito {

	@InjectMocks
	private AddressRetrievalService addressRetrievalService;

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private CloseableHttpClient mockHttpClient;

	@Mock
	private HttpEntity mockHttpEntity;

	@Mock
	private CloseableHttpResponse mockHttpResponse;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject1;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private ResponseParserUtil mockResponseParserUtil;

	@Before
	public void activeSetUp() {
		mockStatic(HttpClients.class, EntityUtils.class);
	}

	@Test
	public void executeMethod_WhenException_ThenReturnsEmptyArray() {
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray);

		JSONArray result = addressRetrievalService.executeMethod("urlToCall");

		assertThat(result, sameInstance(mockJSONArray));
	}

	@Test
	public void executeMethod_WhenNoError_ThenReturnsArrayWithTheResponse() throws Exception {
		when(HttpClients.createDefault()).thenReturn(mockHttpClient);
		when(mockHttpClient.execute(any())).thenReturn(mockHttpResponse);
		when(mockHttpResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn("myResponseString");
		when(mockResponseParserUtil.getResults("myResponseString")).thenReturn(mockJSONArray);

		JSONArray result = addressRetrievalService.executeMethod("urlToCall");

		assertThat(result, sameInstance(mockJSONArray));
		verify(mockHttpResponse, times(1)).close();
		verify(mockHttpClient, times(1)).close();
	}

	@Test
	public void getAddressContextResult_WhenJsonObjectDoesNotHaveLPI_ThenReturnsResultForDPAFormat() {
		when(mockJSONObject1.has(OrdnanceSurveyConstants.LPI)).thenReturn(false);
		when(mockJSONObject1.getJSONObject(OrdnanceSurveyConstants.DPA)).thenReturn(mockJSONObject2);
		when(mockResponseParserUtil.getAddressContextResultForDPA(mockJSONObject2)).thenReturn(mockAddressContext);

		AddressContext result = addressRetrievalService.getAddressContextResult(mockJSONObject1);

		assertThat(result, sameInstance(mockAddressContext));
	}

	@Test
	public void getAddressContextResult_WhenJsonObjectHasLPI_ThenReturnsResultForLPIFormat() {
		when(mockJSONObject1.has(OrdnanceSurveyConstants.LPI)).thenReturn(true);
		when(mockJSONObject1.getJSONObject(OrdnanceSurveyConstants.LPI)).thenReturn(mockJSONObject2);
		when(mockResponseParserUtil.getAddressContextResultForLPI(mockJSONObject2)).thenReturn(mockAddressContext);

		AddressContext result = addressRetrievalService.getAddressContextResult(mockJSONObject1);

		assertThat(result, sameInstance(mockAddressContext));
	}

}
