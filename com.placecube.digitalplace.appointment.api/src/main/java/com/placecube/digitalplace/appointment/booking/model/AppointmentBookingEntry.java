package com.placecube.digitalplace.appointment.booking.model;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

public class AppointmentBookingEntry implements Serializable {

	private static final long serialVersionUID = 1L;

	private long appointmentEntryId;
	private String appointmentId;
	private long classNameId;
	private long classPK;
	private String description;
	private Date endDate;
	private Date startDate;
	private TimeZone timeZone;
	private String title;

	public AppointmentBookingEntry() {

	}

	public long getAppointmentEntryId() {
		return appointmentEntryId;
	}

	public String getAppointmentId() {
		return appointmentId;
	}

	public long getClassNameId() {
		return classNameId;
	}

	public long getClassPK() {
		return classPK;
	}

	public String getDescription() {
		return description;
	}

	public Date getEndDate() {
		return endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public String getTitle() {
		return title;
	}

	public void setAppointmentEntryId(long appointmentEntryId) {
		this.appointmentEntryId = appointmentEntryId;
	}

	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}

	public void setClassNameId(long classNameId) {
		this.classNameId = classNameId;
	}

	public void setClassPK(long classPK) {
		this.classPK = classPK;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
