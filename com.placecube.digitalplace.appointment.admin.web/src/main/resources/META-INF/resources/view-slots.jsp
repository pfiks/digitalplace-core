<%@ include file="init.jsp" %>

<div class="container-fluid container-fluid-max-xl">
	<c:set var="currentTab" value="viewSlots"/>
	<%@ include file="header-tabs.jspf" %>
	
	<c:set var="apointmentSlotUpdateError">
		<%=SessionErrors.get(renderRequest, "appointment-slot-update-error")%>
	</c:set>
	<liferay-ui:error key="appointment-slot-update-error" message="${apointmentSlotUpdateError}" />
	
	<c:choose>
		
		<c:when test="${empty availableServiceIds}">
			<div class="alert alert-info">
				<liferay-ui:message key="no-results-were-found" />
			</div>
		</c:when>
		
		<c:otherwise>
		
			<portlet:renderURL var="loadAppointmentSlotsForServiceIdRenderURL">
				<portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.VIEW_ALL_SLOTS%>" />
			</portlet:renderURL>
			
			<aui:form action="${loadAppointmentSlotsForServiceIdRenderURL}" method="post" name="loadAppointmentSlotsForServiceIdForm" cssClass="mt-2">
				<aui:select  name="serviceId" label="select-service-id" showEmptyOption="true" ignoreRequestValue="true" onChange="this.form.submit();">
					<c:forEach var="serviceIdEntry" items="${availableServiceIds}">
						<aui:option value="${serviceIdEntry}" selected="${serviceId == serviceIdEntry}">
							${serviceIdEntry}
						</aui:option>
					</c:forEach>
				</aui:select>
			</aui:form>
			
			<hr/>
			
			<c:if test="${not empty serviceId}">
				<div class="accordion" id="<portlet:namespace/>actionButtons">
					<div class="card">
						<div class="card-header" id="<portlet:namespace/>appointmentSlotTypeConfigurationHeading">
							<h2 class="mb-0">
								<button class="btn btn-link btn-block text-left" type="button" 
										data-toggle="collapse" data-target="#<portlet:namespace/>appointmentSlotTypeConfigurationContent" 
										aria-expanded="true" aria-controls="<portlet:namespace/>appointmentSlotTypeConfigurationContent">
										
									<liferay-ui:message key="appointment-slot-configurations"/>
								</button>
							</h2>
						</div>
						
						<div id="<portlet:namespace/>appointmentSlotTypeConfigurationContent" class="collapse show" 
							data-parent="#<portlet:namespace/>actionButtons"
							aria-labelledby="<portlet:namespace/>appointmentSlotTypeConfigurationHeading">
							
							<div class="card-body">
							 	<%@ include file="view-appointment-slots-configurations.jspf" %>
							</div>
						</div>
					</div>
					
					<div class="card">
						<div class="card-header" id="<portlet:namespace/>customResourceActionsHeading">
							<h2 class="mb-0">
								<button class="btn btn-link btn-block text-left collapsed" type="button" 
									data-toggle="collapse" data-target="#<portlet:namespace/>customResourceActionsContent" 
									aria-expanded="false" aria-controls="<portlet:namespace/>customResourceActionsContent">
									
									<liferay-ui:message key="appointment-slot-exclusions" />
								</button>
							</h2>
						</div>
						<div id="<portlet:namespace/>customResourceActionsContent" class="collapse" 
							data-parent="#<portlet:namespace/>actionButtons"
							aria-labelledby="<portlet:namespace/>customResourceActionsHeading">
							
							<div class="card-body">
								<%@ include file="view-appointment-slots-exclusions.jspf" %>
							</div>
						</div>
					</div>
				</div>
			</c:if>
						
		</c:otherwise>
	</c:choose>
</div>