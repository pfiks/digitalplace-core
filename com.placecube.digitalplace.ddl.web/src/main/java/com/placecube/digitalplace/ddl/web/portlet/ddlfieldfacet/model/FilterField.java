package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.model;

import java.util.Map;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;

public class FilterField {

	private final String facetFieldName;
	private final String fieldLabel;
	private final Map<String, LocalizedValue> options;
	private final String type;

	private FilterField(String facetFieldName, String fieldLabel, Map<String, LocalizedValue> options, String type) {
		this.facetFieldName = facetFieldName;
		this.fieldLabel = fieldLabel;
		this.options = options;
		this.type = type;
	}

	public static FilterField init(DDMFormField ddmFormField) {
		LocalizedValue label = ddmFormField.getLabel();
		return new FilterField(ddmFormField.getName(), label.getValues().get(label.getDefaultLocale()), ddmFormField.getDDMFormFieldOptions().getOptions(), ddmFormField.getDataType());
	}

	public String getFacetFieldName() {
		return facetFieldName;
	}

	public String getFieldLabel() {
		return fieldLabel;
	}

	public Map<String, LocalizedValue> getOptions() {
		return options;
	}
	
	public String getType() {
		return type;
	}

}
