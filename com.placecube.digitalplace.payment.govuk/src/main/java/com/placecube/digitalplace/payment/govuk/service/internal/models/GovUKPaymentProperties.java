package com.placecube.digitalplace.payment.govuk.service.internal.models;

public class GovUKPaymentProperties {

	private String agreementCancelURL;

	private String agreementStatusURL;

	private String apiAuthKey;

	private String baseURL;

	private String createAgreementURL;

	private String createPaymentURL;

	private String paymentStatusURL;

	public String getAgreementCancelURL() {
		return agreementCancelURL;
	}

	public String getAgreementStatusURL() {
		return agreementStatusURL;
	}

	public String getApiAuthKey() {
		return apiAuthKey;
	}

	public String getBaseURL() {
		return baseURL;
	}

	public String getCreateAgreementURL() {
		return createAgreementURL;
	}

	public String getCreatePaymentURL() {
		return createPaymentURL;
	}

	public String getPaymentStatusURL() {
		return paymentStatusURL;
	}

	public void setAgreementCancelURL(String agreementCancelURL) {
		this.agreementCancelURL = agreementCancelURL;
	}

	public void setAgreementStatusURL(String agreementStatusURL) {
		this.agreementStatusURL = agreementStatusURL;
	}

	public void setApiAuthKey(String apiAuthKey) {
		this.apiAuthKey = apiAuthKey;
	}

	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	public void setCreateAgreementURL(String createAgreementURL) {
		this.createAgreementURL = createAgreementURL;
	}

	public void setCreatePaymentURL(String createPaymentURL) {
		this.createPaymentURL = createPaymentURL;
	}

	public void setPaymentStatusURL(String paymentStatusURL) {
		this.paymentStatusURL = paymentStatusURL;
	}
}
