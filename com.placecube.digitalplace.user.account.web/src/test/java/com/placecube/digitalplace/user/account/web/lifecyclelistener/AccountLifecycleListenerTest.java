package com.placecube.digitalplace.user.account.web.lifecyclelistener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.user.account.constants.WebContentArticles;
import com.placecube.digitalplace.user.account.service.AccountSetupService;

public class AccountLifecycleListenerTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@InjectMocks
	private AccountLifecycleListener accountLifecycleListener;

	@Mock
	private AccountSetupService mockAccountSetupService;

	@Mock
	private Company mockCompany;

	@Mock
	private Group mockGroup;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private ServiceContext mockServiceContext;

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenExceptionAddingFolder_ThenThrowsPortalException() throws Exception {
		mockServiceContextDetails();
		when(mockAccountSetupService.addFolder(mockServiceContext)).thenThrow(new PortalException());

		accountLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = Exception.class)
	public void portalInstanceRegistered_WhenExceptionAddingUserAccountConfirmationWithEmailVerificationArticle_ThenThrowsException() throws Exception {
		mockServiceContextDetails();
		when(mockAccountSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);
		doThrow(new Exception()).when(mockAccountSetupService).addArticle(WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITH_EMAIL_VERIFICATION, mockJournalFolder, mockServiceContext);

		accountLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = Exception.class)
	public void portalInstanceRegistered_WhenExceptionAddingUserAccountConfirmationWithoutEmailVerificationArticle_ThenThrowsException() throws Exception {
		mockServiceContextDetails();
		when(mockAccountSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);
		doThrow(new Exception()).when(mockAccountSetupService).addArticle(WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITHOUT_EMAIL_VERIFICATION, mockJournalFolder, mockServiceContext);

		accountLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = Exception.class)
	public void portalInstanceRegistered_WhenExceptionAddingUserAccountSidePanelInfoArticle_ThenThrowsException() throws Exception {
		mockServiceContextDetails();
		when(mockAccountSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);
		doThrow(new Exception()).when(mockAccountSetupService).addArticle(WebContentArticles.USER_ACCOUNT_SIDE_PANEL_INFO, mockJournalFolder, mockServiceContext);

		accountLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenExceptionRetrievingTheCompanyGroup_ThenThrowsPortalException() throws Exception {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenThrow(new PortalException());

		accountLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesUserAccountConfirmationResetPasswordInvokedByAdminArticle() throws Exception {
		mockServiceContextDetails();
		when(mockAccountSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		accountLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockAccountSetupService, times(1)).addArticle(WebContentArticles.USER_ACCOUNT_CONFIRMATION_RESET_PASSWORD_INVOKED_BY_ADMIN, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesUserAccountConfirmationWithEmailVerificationArticle() throws Exception {
		mockServiceContextDetails();
		when(mockAccountSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		accountLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockAccountSetupService, times(1)).addArticle(WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITH_EMAIL_VERIFICATION, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesUserAccountConfirmationWithoutEmailVerificationArticle() throws Exception {
		mockServiceContextDetails();
		when(mockAccountSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		accountLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockAccountSetupService, times(1)).addArticle(WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITHOUT_EMAIL_VERIFICATION, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesUserAccountSidePanelInfoArticle() throws Exception {
		mockServiceContextDetails();
		when(mockAccountSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		accountLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockAccountSetupService, times(1)).addArticle(WebContentArticles.USER_ACCOUNT_SIDE_PANEL_INFO, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesUserAccountTwoFactorAuthentication() throws Exception {
		mockServiceContextDetails();
		when(mockAccountSetupService.addFolder(mockServiceContext)).thenReturn(mockJournalFolder);

		accountLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockAccountSetupService, times(1)).addArticle(WebContentArticles.USER_ACCOUNT_TWO_FACTOR_AUTHENTICATION, mockJournalFolder, mockServiceContext);
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

	private void mockServiceContextDetails() throws PortalException {
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompany.getGroup()).thenReturn(mockGroup);
		when(mockAccountSetupService.getServiceContext(mockGroup)).thenReturn(mockServiceContext);
	}
}
