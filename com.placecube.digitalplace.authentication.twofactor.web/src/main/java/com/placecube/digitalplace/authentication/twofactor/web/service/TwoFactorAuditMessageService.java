package com.placecube.digitalplace.authentication.twofactor.web.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.audit.AuditMessage;
import com.liferay.portal.kernel.audit.AuditRouter;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.security.audit.event.generators.util.AuditMessageBuilder;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorAuditMessage;

@Component(immediate = true, service = TwoFactorAuditMessageService.class)
public class TwoFactorAuditMessageService {

	private static final Log LOG = LogFactoryUtil.getLog(TwoFactorAuditMessageService.class);

	@Reference
	private AuditRouter auditRouter;

	public void addAuditMessage(long userId, TwoFactorAuditMessage twoFactorAuditMessage) {
		addAuditMessage(CompanyThreadLocal.getCompanyId(), userId, "NotFound", twoFactorAuditMessage);
	}

	public void addAuditMessage(User user, TwoFactorAuditMessage twoFactorAuditMessage) {
		addAuditMessage(user.getCompanyId(), user.getUserId(), user.getScreenName(), twoFactorAuditMessage);
	}

	private void addAuditMessage(long companyId, long userId, String userName, TwoFactorAuditMessage twoFactorAuditMessage) {
		try {
			AuditMessage auditMessage = AuditMessageBuilder.buildAuditMessage(twoFactorAuditMessage.getActionType(), TwoFactorAuditMessageService.class.getName(), userId, null);

			JSONObject additionalInfo = auditMessage.getAdditionalInfo();

			additionalInfo.put("userId", userId);
			additionalInfo.put("date", DateUtil.newDate());
			additionalInfo.put("reason", twoFactorAuditMessage.getMessage());
			auditMessage.setAdditionalInfo(additionalInfo);

			auditRouter.route(auditMessage);
		} catch (Exception e) {
			LOG.warn("Unable to add audit message", e);
		}
	}

}
