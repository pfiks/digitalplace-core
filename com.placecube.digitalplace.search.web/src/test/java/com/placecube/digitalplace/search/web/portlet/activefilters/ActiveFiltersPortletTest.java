package com.placecube.digitalplace.search.web.portlet.activefilters;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class ActiveFiltersPortletTest extends PowerMockito {

	@InjectMocks
	private ActiveFiltersPortlet activeFiltersPortlet;

	@Mock
	private List<SharedActiveFilter> mockActiveFilters;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Before
	public void activateSetup() {
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		activeFiltersPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresTheActiveFitlersAsRequestAttribute() throws Exception {
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getActiveFilters()).thenReturn(mockActiveFilters);

		activeFiltersPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("activeFilters", mockActiveFilters);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresTheIteratorUrlAsRequestAttribute() throws Exception {
		String expected = "expectedVal";
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getEscapedSearchResultsIteratorURL()).thenReturn(expected);

		activeFiltersPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", expected);
	}
}
