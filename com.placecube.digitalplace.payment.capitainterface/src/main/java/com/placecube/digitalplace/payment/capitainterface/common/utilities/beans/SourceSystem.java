package com.placecube.digitalplace.payment.capitainterface.common.utilities.beans;

/**
 * A definition of the various source systems which could be the source of an error.
 *
 */
public enum SourceSystem {
        ESB,
        CAPITA,
        OPENIDM,
        LIFERAY,
        DRUPAL,
        BOOKING_BUG,
        OHMS,
        LAGAN,
        CLICK,
        IWORLD
}

