package com.placecube.digitalplace.externalpage.web.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

import java.io.Serializable;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.background.task.service.BackgroundTaskLocalService;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.externalpage.web.service.CrawlModelBuilder;
import com.placecube.digitalplace.externalpage.web.service.CrawlUtil;
import com.placecube.digitalplace.externalpage.web.service.ExternalPageImportValidator;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, CrawlUtil.class, SessionErrors.class, ServiceContextFactory.class })
public class ExternalPageAddImportMVCActionCommandTest extends PowerMockito {

	private static final String INDEX_ARRAY = "indexArray";
	private static final Integer MAX_PAGES_TO_FECTH = 1;
	private static final String URL = "urlValue";
	private static final String XPATH = "xPathValue";

	@InjectMocks
	private ExternalPageAddImportMVCActionCommand externalPageImportMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private BackgroundTaskLocalService mockBackgroundTaskLocalService;

	@Mock
	private CrawlModelBuilder mockCrawlModelBuilder;

	@Mock
	private ExternalPageImportValidator mockExternalPageImportValidator;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServiceContextFactory mockServiceContextFactory;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	Map<String, Serializable> mockTaskContextMap;

	@Test
	public void doProcessAction_WhenValidationFails_ThenNoActionIsPerformed() {
		when(mockExternalPageImportValidator.validate(mockActionRequest)).thenReturn(false);

		externalPageImportMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyZeroInteractions(mockBackgroundTaskLocalService);
	}

	@Test
	public void doProcessAction_WhenValidationPasses_ThenConfiguresTheBackgroundTask() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockExternalPageImportValidator.validate(mockActionRequest)).thenReturn(true);
		mockControllerConfigs();

		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockExternalPageLocalService.getExternalPagesCount()).thenReturn(2);
		externalPageImportMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		Mockito.verify(mockBackgroundTaskLocalService, times(1)).addBackgroundTask(anyLong(), anyLong(), anyString(), anyString(), any(Map.class), any(ServiceContext.class));

	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class, CrawlUtil.class, SessionErrors.class, ServiceContextFactory.class);
	}

	private void mockControllerConfigs() {
		String indArr[] = new String[] { "1", "3" };

		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.URL)).thenReturn(URL);
		when(ParamUtil.getInteger(mockActionRequest, PortletRequestKeys.MAX_PAGES_TO_FETCH)).thenReturn(MAX_PAGES_TO_FECTH);
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.XPATH)).thenReturn(XPATH);
		when(ParamUtil.getStringValues(mockActionRequest, PortletRequestKeys.INDEX_CONTENT)).thenReturn(indArr);

	}
}
