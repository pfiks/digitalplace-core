package com.placecube.digitalplace.user.serviceoverride.service;

import java.util.Optional;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.user.serviceoverride.model.AuthenticationActionContext;

public interface UserServiceOverrideAction {

	String SERVICE_EXECUTION_PRIORITY = "user.service.override.action.serviceExecutionPriority";

	String SERVICE_ID = "user.service.override.action.serviceId";

	default void executeOnAfterAddUserToGroupActions(long userId, long groupId) {
	}

	default User executeOnAfterCreateActions(User user, Optional<ServiceContext> serviceContext) throws PortalException {
		return user;
	}

	default void executeOnAfterDeleteActions(User user) {
	}

	default void executeOnAfterRemoveUserFromGroupActions(long userId, long groupId, Optional<ServiceContext> serviceContext) {
	}

	default User executeOnAfterUpdateActions(User user, Optional<ServiceContext> serviceContext) throws PortalException {
		return user;
	}

	default void executeAfterUserAuthentication(AuthenticationActionContext authenticationActionContext) throws PortalException {
	}

	default void executeOnAfterVerifyEmailAddress(long userId) {
	}

}
