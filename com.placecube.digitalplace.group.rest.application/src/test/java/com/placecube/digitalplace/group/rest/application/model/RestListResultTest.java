package com.placecube.digitalplace.group.rest.application.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

public class RestListResultTest extends PowerMockito {

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void addResult_WhenNoError_ThenAddsTheResultToTheData() {
		RestListResult result = new RestListResult();
		assertThat(result.getData(), empty());
		result.addResult("ValueOne");
		result.addResult("ValueTwo");

		assertThat(result.getData(), contains("ValueOne", "ValueTwo"));
	}

	@Test
	public void getItemCount_WhenNoError_ThenReturnsTheTotalSizeOfTheData() {
		RestListResult result = new RestListResult();
		assertThat(result.getItemCount(), equalTo(0));
		result.addResult("ValueOne");
		result.addResult("ValueTwo");

		assertThat(result.getItemCount(), equalTo(2));
	}

	@Test
	public void init_WhenNoError_ThenReturnsEntryWithEmptyData() {
		RestListResult result = RestListResult.init();

		assertThat(result.getData(), empty());
	}

}
