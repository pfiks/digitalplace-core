package com.placecube.digitalplace.search.shared.constants;

public class SharedSearchDisplayCostants {

	public static final String DISPLAY_STYLE_GRID = "grid";

	public static final String DISPLAY_STYLE_LIST = "list";

	private SharedSearchDisplayCostants() {

	}
}
