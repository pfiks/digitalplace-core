package com.placecube.digitalplace.oauth.dataprovider.service;

import com.jayway.jsonpath.DocumentContext;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderInputParametersSettings;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderOutputParametersSettings;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderRequest;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderResponse;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderResponseStatus;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceLocalService;
import com.liferay.petra.string.CharPool;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.oauth.dataprovider.settings.DDMRESTOAuth2DataProviderSettings;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = DDMRESTOAuth2DataProviderService.class)
public class DDMRESTOAuth2DataProviderService {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDMDataProviderInstanceLocalService ddmDataProviderInstanceLocalService;

	public Optional<DDMDataProviderInstance> findDDMDataProviderInstance(DDMDataProviderRequest ddmDataProviderRequest) {
		String ddmDataProviderId = ddmDataProviderRequest.getDDMDataProviderId();

		long requestCompanyId = ddmDataProviderRequest.getCompanyId();

		Optional<DDMDataProviderInstance> ddmDataProviderInstance;

		if (requestCompanyId != 0) {
			CompanyThreadLocal.setCompanyId(requestCompanyId);
			ddmDataProviderInstance = Optional.ofNullable(getDDMDataProviderInstance(ddmDataProviderId));
		} else {
			ddmDataProviderInstance = companyLocalService.getCompanies().stream().map(c -> {
				CompanyThreadLocal.setCompanyId(c.getCompanyId());
				return getDDMDataProviderInstance(ddmDataProviderId);
			}).filter(Predicate.not(Objects::isNull)).findFirst();
		}

		return ddmDataProviderInstance;

	}

	public DDMDataProviderResponse createDDMDataProviderResponse(DocumentContext documentContext, DDMDataProviderResponseStatus ddmDataProviderResponseStatus,
			DDMRESTOAuth2DataProviderSettings ddmRESTDataProviderSettings) {

		DDMDataProviderOutputParametersSettings[] outputParameters = ddmRESTDataProviderSettings.outputParameters();

		DDMDataProviderResponse.Builder builder = DDMDataProviderResponse.Builder.newBuilder();

		builder.withStatus(ddmDataProviderResponseStatus);

		if (ArrayUtil.isEmpty(outputParameters)) {
			return builder.build();
		}

		for (DDMDataProviderOutputParametersSettings outputParameter : outputParameters) {

			String id = outputParameter.outputParameterId();
			String type = outputParameter.outputParameterType();
			String path = outputParameter.outputParameterPath();

			if (Objects.equals(type, "text")) {
				builder = builder.withOutput(id, documentContext.read(normalizePath(path), String.class));
			} else if (Objects.equals(type, "number")) {
				builder = builder.withOutput(id, documentContext.read(normalizePath(path), Number.class));
			} else if (Objects.equals(type, "list")) {
				String[] paths = StringUtil.split(path, CharPool.SEMICOLON);

				String normalizedValuePath = normalizePath(paths[0]);

				List<?> values = documentContext.read(normalizedValuePath, List.class);

				if (values == null) {
					continue;
				}

				List<?> keys = values;

				if (paths.length >= 2) {
					keys = documentContext.read(normalizePath(paths[1]));
				}

				List<KeyValuePair> keyValuePairs = new ArrayList<>();

				for (int i = 0; i < values.size(); i++) {
					keyValuePairs.add(new KeyValuePair(String.valueOf(keys.get(i)), String.valueOf(values.get(i))));
				}

				builder = builder.withOutput(id, keyValuePairs);
			}
		}

		return builder.build();
	}

	public KeyStore getKeyStore() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

		keyStore.load(null);

		return keyStore;
	}

	public Map<String, Object> getRequestInputParametersMap(DDMDataProviderRequest ddmDataProviderRequest, DDMRESTOAuth2DataProviderSettings ddmRESTDataProviderSettings) {

		Map<String, Object> requestInputParametersMap = new HashMap<>();

		Map<String, Object> parameters = ddmDataProviderRequest.getParameters();

		for (DDMDataProviderInputParametersSettings ddmDataProviderInputParametersSettings : ddmRESTDataProviderSettings.inputParameters()) {

			String inputParameterName = ddmDataProviderInputParametersSettings.inputParameterName();

			Object value = parameters.get(inputParameterName);

			if (value != null) {
				requestInputParametersMap.put(inputParameterName, value);
			}
		}

		return requestInputParametersMap;
	}

	private DDMDataProviderInstance getDDMDataProviderInstance(String ddmDataProviderInstanceId) {

		DDMDataProviderInstance ddmDataProviderInstance = ddmDataProviderInstanceLocalService.fetchDataProviderInstanceByUuid(ddmDataProviderInstanceId);

		if (ddmDataProviderInstance == null && Validator.isNumber(ddmDataProviderInstanceId)) {
			ddmDataProviderInstance = ddmDataProviderInstanceLocalService.fetchDataProviderInstance(GetterUtil.getLong(ddmDataProviderInstanceId));
		}

		return ddmDataProviderInstance;
	}

	private String normalizePath(String path) {
		if (StringUtil.startsWith(path, StringPool.DOLLAR) || StringUtil.startsWith(path, StringPool.PERIOD) || StringUtil.startsWith(path, StringPool.STAR)) {

			return path;
		}

		return StringPool.PERIOD.concat(path);
	}

}
