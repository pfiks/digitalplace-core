<%@ include file="/init.jsp"%>

<portlet:actionURL name="/ddl-web/field-filter-search" var="applyFilterUrl" />

<c:choose>
	<c:when test="${invalidConfiguration}">
		<%@ include file="/invalid-configuration.jspf" %>
	</c:when>

	<c:when test="${isEditLayoutMode && invisible}">
		<%@ include file="/invisible-portlet-edit-mode.jspf" %>
	</c:when>
	
	<c:when test="${ not invisible }">
		<aui:fieldset cssClass="pl-2 pr-2">
			<aui:form name="ddlFieldFilterPortletFm" action="${ applyFilterUrl }">
				<aui:input type="hidden" name="appliedFilter" value="" />
				<aui:input type="hidden" name="redirectURL" value="${redirectURL}" />

				<c:if test="${not empty configuration.header() }">
					<h3><liferay-ui:message key="${ configuration.header() }"/></h3>
				</c:if>
				
				<c:if test="${ not empty ddlFilterGroups}">
					<c:forEach items="${ ddlFilterGroups }" var="ddlFilterGroup">
						<aui:select cssClass="filter-select" name="filterOption" label="${ showGroupNameAsFirstOption ? '' : ddlFilterGroup.groupName }">
							
							<c:choose>
								<c:when test="${showGroupNameAsFirstOption }">
									<aui:option value="">${ddlFilterGroup.groupName }</aui:option>
								</c:when>
								<c:otherwise>
									<aui:option value=""><liferay-ui:message key="select"/></aui:option>
								</c:otherwise>
							</c:choose>								
							
							<c:forEach items="${ ddlFilterGroup.options }" var="option">
								<c:set var="optionValue" value="${ddlFilterGroup.groupName}_${option}" />
								<aui:option value="${optionValue}" selected="${ appliedFilters.contains(optionValue) }">${ option }</aui:option>
							</c:forEach>
						</aui:select>
					</c:forEach>
				</c:if>
			</aui:form>
		</aui:fieldset>

		<aui:script>

			$('#<portlet:namespace/>filterOption').change(function( event ) {
				$('#<portlet:namespace/>ddlFieldFilterPortletFm').submit();
			});

		</aui:script>
	</c:when>
</c:choose>
