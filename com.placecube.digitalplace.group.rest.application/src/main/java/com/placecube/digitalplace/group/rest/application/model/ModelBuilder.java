package com.placecube.digitalplace.group.rest.application.model;

import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = ModelBuilder.class)
public class ModelBuilder {

	public Response getSuccessResponse(RestListResult result) {
		return Response.ok(result).build();
	}

	public RestErrorMessage initRestErrorMessage(int errorStatus, String errorCode, String stackTrace) {
		return new RestErrorMessage(errorStatus, errorCode, stackTrace);
	}

	public RestListResult initRestListResult() {
		return new RestListResult();
	}
}
