package com.placecube.digitalplace.search.rankings.service;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;

@Component(service = RankingsRegexService.class)
public class RankingsRegexService {

	private static final String END_STRING = StringPool.DOLLAR;

	private static final String START_STRING = StringPool.CARET;

	private static final String WILD_END = "($|\\W.*)";

	private static final String WILD_INNER = "(.*\\W)";

	private static final String WILD_START = "(^|.*\\W)";

	public String getAliasWildcardRegex(String alias) {

		if (Validator.isNull(alias)) {
			return alias;
		}

		String start = START_STRING;
		String end = END_STRING;

		if (alias.startsWith(StringPool.STAR)) {
			alias = alias.substring(1, alias.length());
			start = WILD_START;
		}

		if (alias.endsWith(StringPool.STAR)) {
			alias = alias.substring(0, alias.length() - 1);
			end = WILD_END;
		}

		if (alias.contains(StringPool.STAR)) {
			alias = alias.replace(StringPool.STAR, WILD_INNER);
		}

		return start + alias + end;
	}

}
