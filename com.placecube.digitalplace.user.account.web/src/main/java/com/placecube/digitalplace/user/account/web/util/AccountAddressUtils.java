package com.placecube.digitalplace.user.account.web.util;

import java.util.ArrayList;
import java.util.List;

import com.liferay.petra.string.StringUtil;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;

public final class AccountAddressUtils {

	private AccountAddressUtils() {

	}

	public static String getFullAddress(Address address, String delimiter) {
		return mergeStringAndFilterEmpties(delimiter, address.getStreet1(), address.getStreet2(), address.getStreet3(), address.getCity(), address.getZip());
	}

	public static String getFullAddress(AddressContext addressContext, String delimiter) {
		return mergeStringAndFilterEmpties(delimiter, addressContext.getAddressLine1(), addressContext.getAddressLine2(), addressContext.getAddressLine3(), addressContext.getCity(),
				addressContext.getPostcode());
	}

	public static String mergeStringAndFilterEmpties(String delimeter, String... stringComponents) {
		List<String> stringComponentsList = new ArrayList<>();
		for (String component : stringComponents) {
			if (Validator.isNotNull(component)) {
				stringComponentsList.add(component.trim());
			}
		}

		return StringUtil.merge(stringComponentsList, delimeter);
	}
}
