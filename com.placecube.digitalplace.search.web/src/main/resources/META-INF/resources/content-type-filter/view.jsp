<%@ include file="/init.jsp"%>
	
<c:set var="sectionTitleKey" value="content-type" />
<c:set var="availableResults" value="${availableContentTypes}" />
<c:set var="facetFieldName" value="<%= SearchParameters.SELECTED_CONTENT_TYPES %>" />
<c:set var="hideCounter" value="true" />

<%@ include file="/common/facet-checkbox-view.jspf" %>


