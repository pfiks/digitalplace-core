package com.placecube.digitalplace.authentication.twofactor.web.portlet.override;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.security.Key;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.ActionURL;
import javax.portlet.RenderURL;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.encryptor.Encryptor;
import com.liferay.portal.kernel.encryptor.EncryptorException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.security.auth.PrincipalException;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.DigesterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.security.auth.session.AuthenticatedSessionManagerUtil;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.model.TwoFactorAuthenticationMethod;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorSessionKeys;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorUrlService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, AuthenticatedSessionManagerUtil.class, DigesterUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.security.auth.session.AuthenticatedSessionManagerUtil" })
public class LoginMVCActionCommandOverrideTest extends PowerMockito {

	private static final String ENCRYPTED_STATE = "encryptedStateMapJSONValue";

	private static final String EXPECTED_REDIRECT = "expectedRedirectValue";

	private static final String MVC_COMMAND = "mvcCommandValue";

	private static final long PLID = 555;

	private static final String REDIRECT_VALUE = "redirectValue";

	@InjectMocks
	private LoginMVCActionCommandOverride loginMVCActionCommandOverride;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionRequest mockActionRequestUpdated;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ActionURL mockActionURL;

	@Mock
	private AuthenticationTwoFactorCompanyConfiguration mockAuthenticationTwoFactorCompanyConfiguration;

	@Mock
	private Encryptor mockEncryptor;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpSession mockHttpSession;

	@Mock
	private Key mockKey;

	@Mock
	private LiferayPortletResponse mockLiferayPortletResponse;

	@Mock
	private MVCActionCommand mockOriginalMVCActionCommand;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderURL mockRenderURL;

	@Mock
	private TwoFactorRequestUtils mockTwoFactorRequestUtils;

	@Mock
	private TwoFactorService mockTwoFactorService;

	@Mock
	private TwoFactorUrlService mockTwoFactorUrlService;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private UserTwoFactorAuthentication mockUserTwoFactorAuthentication;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, AuthenticatedSessionManagerUtil.class, DigesterUtil.class);
	}

	@Test(expected = EncryptorException.class)
	public void doProcessAction_WhenEnabledConfigurationFoundAndEncryptorExceptionRetrievingTheAction_ThenThrowsEncryptorException() throws Exception {
		mockConfigurationFound();
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorRequestUtils.getActionRequest(mockHttpServletRequest, mockActionRequest)).thenThrow(new EncryptorException());

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	@Parameters({ "null, passwordVal", "loginVal, null", "null, null" })
	public void doProcessAction_WhenEnabledConfigurationFoundAndInvalidLoginOrPassword_ThenDoesNotProcessTheOriginalActionAndNoOtherActionIsPerformed(String loginValue, String passwordValue)
			throws Exception {
		mockConfigurationFound();
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorRequestUtils.getActionRequest(mockHttpServletRequest, mockActionRequest)).thenReturn(mockActionRequestUpdated);
		when(ParamUtil.getString(mockActionRequestUpdated, "login")).thenReturn(loginValue);
		when(ParamUtil.getString(mockActionRequestUpdated, "password")).thenReturn(passwordValue);

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);

		verifyZeroInteractions(mockOriginalMVCActionCommand, mockTwoFactorUrlService);
	}

	@Test(expected = PrincipalException.class)
	public void doProcessAction_WhenEnabledConfigurationFoundAndPrincipalExceptionRetrievingTheAction_ThenThrowsPrincipalException() throws Exception {
		mockConfigurationFound();
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorRequestUtils.getActionRequest(mockHttpServletRequest, mockActionRequest)).thenThrow(new PrincipalException());

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenEnabledConfigurationFoundAndValidLoginAndPasswordAndAuthenticatedUserIdIsValidAndTwoFactorIsAlreadyVerified_ThenProcessTheOriginalAction() throws Exception {
		long userId = 456;
		mockConfigurationFound();
		mockLoginDetailsForUser(userId);
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);
		when(mockTwoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId)).thenReturn(true);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_REDIRECT)).thenReturn(REDIRECT_VALUE);

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = Mockito.inOrder(mockOriginalMVCActionCommand, mockActionRequestUpdated, mockHttpSession);
		inOrder.verify(mockActionRequestUpdated, times(1)).setAttribute(WebKeys.REDIRECT, REDIRECT_VALUE);
		inOrder.verify(mockHttpSession, times(1)).removeAttribute(TwoFactorSessionKeys.TWO_FACTOR_USER_ID);
		inOrder.verify(mockHttpSession, times(1)).removeAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_DIGEST);
		inOrder.verify(mockHttpSession, times(1)).removeAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_KEY);
		inOrder.verify(mockHttpSession, times(1)).removeAttribute(TwoFactorSessionKeys.TWO_FACTOR_REDIRECT);
		inOrder.verify(mockOriginalMVCActionCommand, times(1)).processAction(mockActionRequestUpdated, mockActionResponse);
		verifyZeroInteractions(mockTwoFactorUrlService);
	}

	@Test(expected = EncryptorException.class)
	public void doProcessAction_WhenEnabledConfigurationFoundAndValidLoginAndPasswordAndAuthenticatedUserIdIsValidAndTwoFactorIsNotAlreadyVerifiedAndTwoFactorEnforcedOrEnabledAndExceptionGeneratingTheKey_ThenThrowsEncryptorException()
			throws Exception {
		long userId = 456;
		mockConfigurationFound();
		mockLoginDetailsForUser(userId);
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);
		when(mockTwoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId)).thenReturn(false);
		when(mockEncryptor.generateKey()).thenThrow(new EncryptorException());
		when(mockTwoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId)).thenReturn(true);

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenEnabledConfigurationFoundAndValidLoginAndPasswordAndAuthenticatedUserIdIsValidAndTwoFactorIsNotAlreadyVerifiedAndTwoFactorIsNotEnforcedOrEnabled_ThenProcessTheOriginalAction()
			throws Exception {
		long userId = 456;
		mockConfigurationFound();
		mockLoginDetailsForUser(userId);
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);
		when(mockTwoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId)).thenReturn(false);
		when(mockTwoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId)).thenReturn(false);

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockOriginalMVCActionCommand, times(1)).processAction(mockActionRequestUpdated, mockActionResponse);
		verifyZeroInteractions(mockTwoFactorUrlService);
	}

	@Test(expected = EncryptorException.class)
	public void doProcessAction_WhenEnabledConfigurationFoundAndValidLoginAndPasswordAndAuthenticatedUserIdIsValidAndTwoFactorIsNotAlreadyVerifiedTwoFactorEnforcedOrEnabledAndAndExceptionRetrievingTheEncryptedStateMap_ThenThrowsEncryptorException()
			throws Exception {
		long userId = 456;
		mockConfigurationFound();
		mockLoginDetailsForUser(userId);
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);
		when(mockTwoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId)).thenReturn(false);
		when(mockEncryptor.generateKey()).thenReturn(mockKey);
		when(mockTwoFactorRequestUtils.getEncryptedStateMapJSON(mockActionRequestUpdated, mockKey)).thenThrow(new EncryptorException());
		when(mockTwoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId)).thenReturn(true);

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenEnabledConfigurationFoundAndValidLoginAndPasswordAndAuthenticatedUserIdIsZero_ThenDoesNotProcessTheOriginalActionAndNoOtherActionIsPerformed() throws Exception {
		mockConfigurationFound();
		mockLoginDetailsForUser(0l);

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);

		verifyZeroInteractions(mockOriginalMVCActionCommand, mockTwoFactorUrlService);
	}

	@Test(expected = PortalException.class)
	public void doProcessAction_WhenEnabledConfigurationFoundAndValidLoginAndPasswordAndExceptionRetrievingAuthenticatedUserId_ThenThrowsPortalException() throws Exception {
		mockConfigurationFound();
		String loginValue = "loginValue";
		String passwordValue = "passwordValue";
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorRequestUtils.getActionRequest(mockHttpServletRequest, mockActionRequest)).thenReturn(mockActionRequestUpdated);
		when(ParamUtil.getString(mockActionRequestUpdated, "login")).thenReturn(loginValue);
		when(ParamUtil.getString(mockActionRequestUpdated, "password")).thenReturn(passwordValue);
		when(AuthenticatedSessionManagerUtil.getAuthenticatedUserId(mockHttpServletRequest, loginValue, passwordValue, null)).thenThrow(new PortalException());

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = WindowStateException.class)
	public void doProcessAction_WhenEnabledConfigurationFoundWithAuthenticationMethodEmailAndValidLoginAndPasswordAndAuthenticatedUserIdIsValidAndTwoFactorIsNotAlreadyVerifiedAndTwoFactorEnforcedOrEnabledAndExceptionRetrievingTheRedirectURL_ThenThrowsWindowStateException()
			throws Exception {
		long userId = 456;
		mockConfigurationFound();
		mockLoginDetailsForUser(userId);
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);
		when(mockTwoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId)).thenReturn(false);
		when(mockEncryptor.generateKey()).thenReturn(mockKey);
		when(mockTwoFactorRequestUtils.getEncryptedStateMapJSON(mockActionRequestUpdated, mockKey)).thenReturn(ENCRYPTED_STATE);
		mockUrls(userId);
		when(mockAuthenticationTwoFactorCompanyConfiguration.authenticationMethod()).thenReturn(TwoFactorAuthenticationMethod.EMAIL);
		when(mockTwoFactorUrlService.getRedirectURL(TwoFactorMVCCommandKeys.VERIFY_BY_EMAIL, PLID, mockActionRequestUpdated, mockActionURL, mockHttpServletRequest, mockRenderURL))
				.thenThrow(new WindowStateException("msg", null));
		when(mockTwoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId)).thenReturn(true);

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = WindowStateException.class)
	public void doProcessAction_WhenEnabledConfigurationFoundWithAuthenticationMethodNotTotpAppAndValidLoginAndPasswordAndAuthenticatedUserIdIsValidAndTwoFactorIsNotAlreadyVerifiedAndTwoFactorEnforcedOrEnabledAndExceptionRetrievingTheRedirectURL_ThenThrowsWindowStateException()
			throws Exception {
		long userId = 456;
		mockConfigurationFound();
		mockLoginDetailsForUser(userId);
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);
		when(mockTwoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId)).thenReturn(false);
		when(mockEncryptor.generateKey()).thenReturn(mockKey);
		when(mockTwoFactorRequestUtils.getEncryptedStateMapJSON(mockActionRequestUpdated, mockKey)).thenReturn(ENCRYPTED_STATE);
		mockUrls(userId);
		when(mockAuthenticationTwoFactorCompanyConfiguration.authenticationMethod()).thenReturn(TwoFactorAuthenticationMethod.TOTP_APP);
		when(mockTwoFactorUrlService.getRedirectURL(MVC_COMMAND, PLID, mockActionRequestUpdated, mockActionURL, mockHttpServletRequest, mockRenderURL))
				.thenThrow(new WindowStateException("msg", null));
		when(mockTwoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId)).thenReturn(true);

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenEnabledConfigurationFoundWithAuthenticationMethodTotpAppAndValidLoginAndPasswordAndAuthenticatedUserIdIsValidAndTwoFactorIsNotAlreadyVerifiedAndTwoFactorEnforcedOrEnabled_ThenDoesNotProcessTheOriginalActionAndRedirectsToTheTwoFactorActionConfiguringTheSession()
			throws Exception {
		long userId = 456;
		mockConfigurationFound();
		mockLoginDetailsForUser(userId);
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);
		when(mockTwoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId)).thenReturn(false);
		when(mockEncryptor.generateKey()).thenReturn(mockKey);
		when(mockTwoFactorRequestUtils.getEncryptedStateMapJSON(mockActionRequestUpdated, mockKey)).thenReturn(ENCRYPTED_STATE);
		mockUrls(userId);
		String digestEncryptedMap = "digestEncryptedMap";
		when(DigesterUtil.digest(ENCRYPTED_STATE)).thenReturn(digestEncryptedMap);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockTwoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId)).thenReturn(true);
		when(mockAuthenticationTwoFactorCompanyConfiguration.authenticationMethod()).thenReturn(TwoFactorAuthenticationMethod.TOTP_APP);

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);

		verifyZeroInteractions(mockOriginalMVCActionCommand);
		verify(mockActionRequestUpdated, times(1)).setAttribute(WebKeys.REDIRECT, EXPECTED_REDIRECT);
		verify(mockHttpSession, times(1)).setAttribute(TwoFactorSessionKeys.TWO_FACTOR_USER_ID, userId);
		verify(mockHttpSession, times(1)).setAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_DIGEST, digestEncryptedMap);
		verify(mockHttpSession, times(1)).setAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_KEY, mockKey);
		verify(mockHttpSession, times(1)).setAttribute(TwoFactorSessionKeys.TWO_FACTOR_REDIRECT, REDIRECT_VALUE);
	}

	@Test
	public void doProcessAction_WhenEnabledConfigurationNotFound_ThenProcessTheOriginalActionAndNoOtherActionIsPerformed() throws Exception {
		long companyId = 123;
		when(mockPortal.getCompanyId(mockActionRequest)).thenReturn(companyId);
		when(mockTwoFactorService.getEnabledConfiguration(companyId)).thenReturn(Optional.empty());

		loginMVCActionCommandOverride.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockOriginalMVCActionCommand, times(1)).processAction(mockActionRequest, mockActionResponse);
		verifyZeroInteractions(mockTwoFactorRequestUtils, mockTwoFactorUrlService);
	}

	private void mockConfigurationFound() {
		long companyId = 123;
		when(mockPortal.getCompanyId(mockActionRequest)).thenReturn(companyId);
		when(mockTwoFactorService.getEnabledConfiguration(companyId)).thenReturn(Optional.of(mockAuthenticationTwoFactorCompanyConfiguration));
	}

	private void mockLoginDetailsForUser(long userId) throws Exception {
		String loginValue = "loginValue";
		String passwordValue = "passwordValue";
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorRequestUtils.getActionRequest(mockHttpServletRequest, mockActionRequest)).thenReturn(mockActionRequestUpdated);
		when(ParamUtil.getString(mockActionRequestUpdated, "login")).thenReturn(loginValue);
		when(ParamUtil.getString(mockActionRequestUpdated, "password")).thenReturn(passwordValue);
		when(AuthenticatedSessionManagerUtil.getAuthenticatedUserId(mockHttpServletRequest, loginValue, passwordValue, null)).thenReturn(userId);
	}

	private void mockUrls(long userId) throws WindowStateException {
		when(mockPortal.getLiferayPortletResponse(mockActionResponse)).thenReturn(mockLiferayPortletResponse);

		when(mockTwoFactorUrlService.getLoginActionURL(mockLiferayPortletResponse, ENCRYPTED_STATE)).thenReturn(mockActionURL);

		when(ParamUtil.getString(mockActionRequestUpdated, "redirect")).thenReturn(REDIRECT_VALUE);
		when(mockTwoFactorUrlService.getReturnToFullPageURL(mockLiferayPortletResponse, REDIRECT_VALUE)).thenReturn(mockRenderURL);

		Optional<UserTwoFactorAuthentication> userTwoFactorAuthentication = Optional.of(mockUserTwoFactorAuthentication);
		when(mockTwoFactorService.getUserTwoFactorAuthentication(userId)).thenReturn(userTwoFactorAuthentication);
		when(mockTwoFactorRequestUtils.getTwoFactorMVCCommandName(userTwoFactorAuthentication)).thenReturn(MVC_COMMAND);

		when(mockTwoFactorRequestUtils.getPlid(mockHttpServletRequest)).thenReturn(PLID);

		when(mockTwoFactorUrlService.getRedirectURL(MVC_COMMAND, PLID, mockActionRequestUpdated, mockActionURL, mockHttpServletRequest, mockRenderURL)).thenReturn(EXPECTED_REDIRECT);
	}

}
