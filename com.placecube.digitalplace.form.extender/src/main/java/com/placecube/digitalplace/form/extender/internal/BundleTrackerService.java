package com.placecube.digitalplace.form.extender.internal;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Component;
import org.osgi.util.tracker.BundleTracker;

import com.placecube.digitalplace.form.extender.FormDefinitionExtender;

@Component(immediate = true, service = BundleTrackerService.class)
public class BundleTrackerService {

	private BundleTracker<?> bundleTracker;

	public void closeBundleTracker() {
		bundleTracker.close();
	}

	public void openBundleTracker(BundleContext bundleContext, FormDefinitionExtender formDefinitionExtender) {

		bundleTracker = new BundleTracker<>(bundleContext, ~Bundle.INSTALLED & ~Bundle.UNINSTALLED, formDefinitionExtender);

		bundleTracker.open();

	}

}
