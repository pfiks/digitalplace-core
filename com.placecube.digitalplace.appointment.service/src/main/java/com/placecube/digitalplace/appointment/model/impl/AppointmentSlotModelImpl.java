/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model.impl;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringUtil;

import com.placecube.digitalplace.appointment.model.AppointmentSlot;
import com.placecube.digitalplace.appointment.model.AppointmentSlotModel;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.sql.Blob;
import java.sql.Types;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * The base model implementation for the AppointmentSlot service. Represents a row in the &quot;Placecube_Appointment_AppointmentSlot&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface <code>AppointmentSlotModel</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link AppointmentSlotImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentSlotImpl
 * @generated
 */
public class AppointmentSlotModelImpl
	extends BaseModelImpl<AppointmentSlot> implements AppointmentSlotModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a appointment slot model instance should use the <code>AppointmentSlot</code> interface instead.
	 */
	public static final String TABLE_NAME =
		"Placecube_Appointment_AppointmentSlot";

	public static final Object[][] TABLE_COLUMNS = {
		{"uuid_", Types.VARCHAR}, {"appointmentSlotId", Types.BIGINT},
		{"companyId", Types.BIGINT}, {"userId", Types.BIGINT},
		{"userName", Types.VARCHAR}, {"createDate", Types.TIMESTAMP},
		{"modifiedDate", Types.TIMESTAMP}, {"slotType", Types.VARCHAR},
		{"name", Types.VARCHAR}, {"serviceId", Types.VARCHAR},
		{"startDate", Types.TIMESTAMP}, {"endDate", Types.TIMESTAMP},
		{"slotStartTimeHour", Types.INTEGER},
		{"slotStartTimeMinute", Types.INTEGER},
		{"slotEndTimeHour", Types.INTEGER},
		{"slotEndTimeMinute", Types.INTEGER},
		{"appointmentDuration", Types.INTEGER},
		{"daysOfWeekBitwise", Types.BIGINT}, {"maxAppointments", Types.INTEGER}
	};

	public static final Map<String, Integer> TABLE_COLUMNS_MAP =
		new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("uuid_", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("appointmentSlotId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("companyId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userName", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("createDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("modifiedDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("slotType", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("name", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("serviceId", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("startDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("endDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("slotStartTimeHour", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("slotStartTimeMinute", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("slotEndTimeHour", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("slotEndTimeMinute", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("appointmentDuration", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("daysOfWeekBitwise", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("maxAppointments", Types.INTEGER);
	}

	public static final String TABLE_SQL_CREATE =
		"create table Placecube_Appointment_AppointmentSlot (uuid_ VARCHAR(75) null,appointmentSlotId LONG not null primary key,companyId LONG,userId LONG,userName VARCHAR(75) null,createDate DATE null,modifiedDate DATE null,slotType VARCHAR(75) null,name STRING null,serviceId STRING null,startDate DATE null,endDate DATE null,slotStartTimeHour INTEGER,slotStartTimeMinute INTEGER,slotEndTimeHour INTEGER,slotEndTimeMinute INTEGER,appointmentDuration INTEGER,daysOfWeekBitwise LONG,maxAppointments INTEGER)";

	public static final String TABLE_SQL_DROP =
		"drop table Placecube_Appointment_AppointmentSlot";

	public static final String ORDER_BY_JPQL =
		" ORDER BY appointmentSlot.appointmentSlotId ASC";

	public static final String ORDER_BY_SQL =
		" ORDER BY Placecube_Appointment_AppointmentSlot.appointmentSlotId ASC";

	public static final String DATA_SOURCE = "liferayDataSource";

	public static final String SESSION_FACTORY = "liferaySessionFactory";

	public static final String TX_MANAGER = "liferayTransactionManager";

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long COMPANYID_COLUMN_BITMASK = 1L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long SERVICEID_COLUMN_BITMASK = 2L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long SLOTTYPE_COLUMN_BITMASK = 4L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long UUID_COLUMN_BITMASK = 8L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *		#getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long APPOINTMENTSLOTID_COLUMN_BITMASK = 16L;

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setEntityCacheEnabled(boolean entityCacheEnabled) {
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setFinderCacheEnabled(boolean finderCacheEnabled) {
	}

	public AppointmentSlotModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _appointmentSlotId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setAppointmentSlotId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _appointmentSlotId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return AppointmentSlot.class;
	}

	@Override
	public String getModelClassName() {
		return AppointmentSlot.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		Map<String, Function<AppointmentSlot, Object>>
			attributeGetterFunctions = getAttributeGetterFunctions();

		for (Map.Entry<String, Function<AppointmentSlot, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<AppointmentSlot, Object> attributeGetterFunction =
				entry.getValue();

			attributes.put(
				attributeName,
				attributeGetterFunction.apply((AppointmentSlot)this));
		}

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Map<String, BiConsumer<AppointmentSlot, Object>>
			attributeSetterBiConsumers = getAttributeSetterBiConsumers();

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			String attributeName = entry.getKey();

			BiConsumer<AppointmentSlot, Object> attributeSetterBiConsumer =
				attributeSetterBiConsumers.get(attributeName);

			if (attributeSetterBiConsumer != null) {
				attributeSetterBiConsumer.accept(
					(AppointmentSlot)this, entry.getValue());
			}
		}
	}

	public Map<String, Function<AppointmentSlot, Object>>
		getAttributeGetterFunctions() {

		return AttributeGetterFunctionsHolder._attributeGetterFunctions;
	}

	public Map<String, BiConsumer<AppointmentSlot, Object>>
		getAttributeSetterBiConsumers() {

		return AttributeSetterBiConsumersHolder._attributeSetterBiConsumers;
	}

	private static class AttributeGetterFunctionsHolder {

		private static final Map<String, Function<AppointmentSlot, Object>>
			_attributeGetterFunctions;

		static {
			Map<String, Function<AppointmentSlot, Object>>
				attributeGetterFunctions =
					new LinkedHashMap
						<String, Function<AppointmentSlot, Object>>();

			attributeGetterFunctions.put("uuid", AppointmentSlot::getUuid);
			attributeGetterFunctions.put(
				"appointmentSlotId", AppointmentSlot::getAppointmentSlotId);
			attributeGetterFunctions.put(
				"companyId", AppointmentSlot::getCompanyId);
			attributeGetterFunctions.put("userId", AppointmentSlot::getUserId);
			attributeGetterFunctions.put(
				"userName", AppointmentSlot::getUserName);
			attributeGetterFunctions.put(
				"createDate", AppointmentSlot::getCreateDate);
			attributeGetterFunctions.put(
				"modifiedDate", AppointmentSlot::getModifiedDate);
			attributeGetterFunctions.put(
				"slotType", AppointmentSlot::getSlotType);
			attributeGetterFunctions.put("name", AppointmentSlot::getName);
			attributeGetterFunctions.put(
				"serviceId", AppointmentSlot::getServiceId);
			attributeGetterFunctions.put(
				"startDate", AppointmentSlot::getStartDate);
			attributeGetterFunctions.put(
				"endDate", AppointmentSlot::getEndDate);
			attributeGetterFunctions.put(
				"slotStartTimeHour", AppointmentSlot::getSlotStartTimeHour);
			attributeGetterFunctions.put(
				"slotStartTimeMinute", AppointmentSlot::getSlotStartTimeMinute);
			attributeGetterFunctions.put(
				"slotEndTimeHour", AppointmentSlot::getSlotEndTimeHour);
			attributeGetterFunctions.put(
				"slotEndTimeMinute", AppointmentSlot::getSlotEndTimeMinute);
			attributeGetterFunctions.put(
				"appointmentDuration", AppointmentSlot::getAppointmentDuration);
			attributeGetterFunctions.put(
				"daysOfWeekBitwise", AppointmentSlot::getDaysOfWeekBitwise);
			attributeGetterFunctions.put(
				"maxAppointments", AppointmentSlot::getMaxAppointments);

			_attributeGetterFunctions = Collections.unmodifiableMap(
				attributeGetterFunctions);
		}

	}

	private static class AttributeSetterBiConsumersHolder {

		private static final Map<String, BiConsumer<AppointmentSlot, Object>>
			_attributeSetterBiConsumers;

		static {
			Map<String, BiConsumer<AppointmentSlot, ?>>
				attributeSetterBiConsumers =
					new LinkedHashMap<String, BiConsumer<AppointmentSlot, ?>>();

			attributeSetterBiConsumers.put(
				"uuid",
				(BiConsumer<AppointmentSlot, String>)AppointmentSlot::setUuid);
			attributeSetterBiConsumers.put(
				"appointmentSlotId",
				(BiConsumer<AppointmentSlot, Long>)
					AppointmentSlot::setAppointmentSlotId);
			attributeSetterBiConsumers.put(
				"companyId",
				(BiConsumer<AppointmentSlot, Long>)
					AppointmentSlot::setCompanyId);
			attributeSetterBiConsumers.put(
				"userId",
				(BiConsumer<AppointmentSlot, Long>)AppointmentSlot::setUserId);
			attributeSetterBiConsumers.put(
				"userName",
				(BiConsumer<AppointmentSlot, String>)
					AppointmentSlot::setUserName);
			attributeSetterBiConsumers.put(
				"createDate",
				(BiConsumer<AppointmentSlot, Date>)
					AppointmentSlot::setCreateDate);
			attributeSetterBiConsumers.put(
				"modifiedDate",
				(BiConsumer<AppointmentSlot, Date>)
					AppointmentSlot::setModifiedDate);
			attributeSetterBiConsumers.put(
				"slotType",
				(BiConsumer<AppointmentSlot, String>)
					AppointmentSlot::setSlotType);
			attributeSetterBiConsumers.put(
				"name",
				(BiConsumer<AppointmentSlot, String>)AppointmentSlot::setName);
			attributeSetterBiConsumers.put(
				"serviceId",
				(BiConsumer<AppointmentSlot, String>)
					AppointmentSlot::setServiceId);
			attributeSetterBiConsumers.put(
				"startDate",
				(BiConsumer<AppointmentSlot, Date>)
					AppointmentSlot::setStartDate);
			attributeSetterBiConsumers.put(
				"endDate",
				(BiConsumer<AppointmentSlot, Date>)AppointmentSlot::setEndDate);
			attributeSetterBiConsumers.put(
				"slotStartTimeHour",
				(BiConsumer<AppointmentSlot, Integer>)
					AppointmentSlot::setSlotStartTimeHour);
			attributeSetterBiConsumers.put(
				"slotStartTimeMinute",
				(BiConsumer<AppointmentSlot, Integer>)
					AppointmentSlot::setSlotStartTimeMinute);
			attributeSetterBiConsumers.put(
				"slotEndTimeHour",
				(BiConsumer<AppointmentSlot, Integer>)
					AppointmentSlot::setSlotEndTimeHour);
			attributeSetterBiConsumers.put(
				"slotEndTimeMinute",
				(BiConsumer<AppointmentSlot, Integer>)
					AppointmentSlot::setSlotEndTimeMinute);
			attributeSetterBiConsumers.put(
				"appointmentDuration",
				(BiConsumer<AppointmentSlot, Integer>)
					AppointmentSlot::setAppointmentDuration);
			attributeSetterBiConsumers.put(
				"daysOfWeekBitwise",
				(BiConsumer<AppointmentSlot, Long>)
					AppointmentSlot::setDaysOfWeekBitwise);
			attributeSetterBiConsumers.put(
				"maxAppointments",
				(BiConsumer<AppointmentSlot, Integer>)
					AppointmentSlot::setMaxAppointments);

			_attributeSetterBiConsumers = Collections.unmodifiableMap(
				(Map)attributeSetterBiConsumers);
		}

	}

	@Override
	public String getUuid() {
		if (_uuid == null) {
			return "";
		}
		else {
			return _uuid;
		}
	}

	@Override
	public void setUuid(String uuid) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_uuid = uuid;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalUuid() {
		return getColumnOriginalValue("uuid_");
	}

	@Override
	public long getAppointmentSlotId() {
		return _appointmentSlotId;
	}

	@Override
	public void setAppointmentSlotId(long appointmentSlotId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_appointmentSlotId = appointmentSlotId;
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_companyId = companyId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalCompanyId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("companyId"));
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() {
		try {
			User user = UserLocalServiceUtil.getUserById(getUserId());

			return user.getUuid();
		}
		catch (PortalException portalException) {
			return "";
		}
	}

	@Override
	public void setUserUuid(String userUuid) {
	}

	@Override
	public String getUserName() {
		if (_userName == null) {
			return "";
		}
		else {
			return _userName;
		}
	}

	@Override
	public void setUserName(String userName) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_userName = userName;
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_createDate = createDate;
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public boolean hasSetModifiedDate() {
		return _setModifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_setModifiedDate = true;

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_modifiedDate = modifiedDate;
	}

	@Override
	public String getSlotType() {
		if (_slotType == null) {
			return "";
		}
		else {
			return _slotType;
		}
	}

	@Override
	public void setSlotType(String slotType) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_slotType = slotType;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalSlotType() {
		return getColumnOriginalValue("slotType");
	}

	@Override
	public String getName() {
		if (_name == null) {
			return "";
		}
		else {
			return _name;
		}
	}

	@Override
	public void setName(String name) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_name = name;
	}

	@Override
	public String getServiceId() {
		if (_serviceId == null) {
			return "";
		}
		else {
			return _serviceId;
		}
	}

	@Override
	public void setServiceId(String serviceId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_serviceId = serviceId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalServiceId() {
		return getColumnOriginalValue("serviceId");
	}

	@Override
	public Date getStartDate() {
		return _startDate;
	}

	@Override
	public void setStartDate(Date startDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_startDate = startDate;
	}

	@Override
	public Date getEndDate() {
		return _endDate;
	}

	@Override
	public void setEndDate(Date endDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_endDate = endDate;
	}

	@Override
	public int getSlotStartTimeHour() {
		return _slotStartTimeHour;
	}

	@Override
	public void setSlotStartTimeHour(int slotStartTimeHour) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_slotStartTimeHour = slotStartTimeHour;
	}

	@Override
	public int getSlotStartTimeMinute() {
		return _slotStartTimeMinute;
	}

	@Override
	public void setSlotStartTimeMinute(int slotStartTimeMinute) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_slotStartTimeMinute = slotStartTimeMinute;
	}

	@Override
	public int getSlotEndTimeHour() {
		return _slotEndTimeHour;
	}

	@Override
	public void setSlotEndTimeHour(int slotEndTimeHour) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_slotEndTimeHour = slotEndTimeHour;
	}

	@Override
	public int getSlotEndTimeMinute() {
		return _slotEndTimeMinute;
	}

	@Override
	public void setSlotEndTimeMinute(int slotEndTimeMinute) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_slotEndTimeMinute = slotEndTimeMinute;
	}

	@Override
	public int getAppointmentDuration() {
		return _appointmentDuration;
	}

	@Override
	public void setAppointmentDuration(int appointmentDuration) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_appointmentDuration = appointmentDuration;
	}

	@Override
	public long getDaysOfWeekBitwise() {
		return _daysOfWeekBitwise;
	}

	@Override
	public void setDaysOfWeekBitwise(long daysOfWeekBitwise) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_daysOfWeekBitwise = daysOfWeekBitwise;
	}

	@Override
	public int getMaxAppointments() {
		return _maxAppointments;
	}

	@Override
	public void setMaxAppointments(int maxAppointments) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_maxAppointments = maxAppointments;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return new StagedModelType(
			PortalUtil.getClassNameId(AppointmentSlot.class.getName()));
	}

	public long getColumnBitmask() {
		if (_columnBitmask > 0) {
			return _columnBitmask;
		}

		if ((_columnOriginalValues == null) ||
			(_columnOriginalValues == Collections.EMPTY_MAP)) {

			return 0;
		}

		for (Map.Entry<String, Object> entry :
				_columnOriginalValues.entrySet()) {

			if (!Objects.equals(
					entry.getValue(), getColumnValue(entry.getKey()))) {

				_columnBitmask |= _columnBitmasks.get(entry.getKey());
			}
		}

		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(
			getCompanyId(), AppointmentSlot.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public AppointmentSlot toEscapedModel() {
		if (_escapedModel == null) {
			Function<InvocationHandler, AppointmentSlot>
				escapedModelProxyProviderFunction =
					EscapedModelProxyProviderFunctionHolder.
						_escapedModelProxyProviderFunction;

			_escapedModel = escapedModelProxyProviderFunction.apply(
				new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		AppointmentSlotImpl appointmentSlotImpl = new AppointmentSlotImpl();

		appointmentSlotImpl.setUuid(getUuid());
		appointmentSlotImpl.setAppointmentSlotId(getAppointmentSlotId());
		appointmentSlotImpl.setCompanyId(getCompanyId());
		appointmentSlotImpl.setUserId(getUserId());
		appointmentSlotImpl.setUserName(getUserName());
		appointmentSlotImpl.setCreateDate(getCreateDate());
		appointmentSlotImpl.setModifiedDate(getModifiedDate());
		appointmentSlotImpl.setSlotType(getSlotType());
		appointmentSlotImpl.setName(getName());
		appointmentSlotImpl.setServiceId(getServiceId());
		appointmentSlotImpl.setStartDate(getStartDate());
		appointmentSlotImpl.setEndDate(getEndDate());
		appointmentSlotImpl.setSlotStartTimeHour(getSlotStartTimeHour());
		appointmentSlotImpl.setSlotStartTimeMinute(getSlotStartTimeMinute());
		appointmentSlotImpl.setSlotEndTimeHour(getSlotEndTimeHour());
		appointmentSlotImpl.setSlotEndTimeMinute(getSlotEndTimeMinute());
		appointmentSlotImpl.setAppointmentDuration(getAppointmentDuration());
		appointmentSlotImpl.setDaysOfWeekBitwise(getDaysOfWeekBitwise());
		appointmentSlotImpl.setMaxAppointments(getMaxAppointments());

		appointmentSlotImpl.resetOriginalValues();

		return appointmentSlotImpl;
	}

	@Override
	public AppointmentSlot cloneWithOriginalValues() {
		AppointmentSlotImpl appointmentSlotImpl = new AppointmentSlotImpl();

		appointmentSlotImpl.setUuid(
			this.<String>getColumnOriginalValue("uuid_"));
		appointmentSlotImpl.setAppointmentSlotId(
			this.<Long>getColumnOriginalValue("appointmentSlotId"));
		appointmentSlotImpl.setCompanyId(
			this.<Long>getColumnOriginalValue("companyId"));
		appointmentSlotImpl.setUserId(
			this.<Long>getColumnOriginalValue("userId"));
		appointmentSlotImpl.setUserName(
			this.<String>getColumnOriginalValue("userName"));
		appointmentSlotImpl.setCreateDate(
			this.<Date>getColumnOriginalValue("createDate"));
		appointmentSlotImpl.setModifiedDate(
			this.<Date>getColumnOriginalValue("modifiedDate"));
		appointmentSlotImpl.setSlotType(
			this.<String>getColumnOriginalValue("slotType"));
		appointmentSlotImpl.setName(
			this.<String>getColumnOriginalValue("name"));
		appointmentSlotImpl.setServiceId(
			this.<String>getColumnOriginalValue("serviceId"));
		appointmentSlotImpl.setStartDate(
			this.<Date>getColumnOriginalValue("startDate"));
		appointmentSlotImpl.setEndDate(
			this.<Date>getColumnOriginalValue("endDate"));
		appointmentSlotImpl.setSlotStartTimeHour(
			this.<Integer>getColumnOriginalValue("slotStartTimeHour"));
		appointmentSlotImpl.setSlotStartTimeMinute(
			this.<Integer>getColumnOriginalValue("slotStartTimeMinute"));
		appointmentSlotImpl.setSlotEndTimeHour(
			this.<Integer>getColumnOriginalValue("slotEndTimeHour"));
		appointmentSlotImpl.setSlotEndTimeMinute(
			this.<Integer>getColumnOriginalValue("slotEndTimeMinute"));
		appointmentSlotImpl.setAppointmentDuration(
			this.<Integer>getColumnOriginalValue("appointmentDuration"));
		appointmentSlotImpl.setDaysOfWeekBitwise(
			this.<Long>getColumnOriginalValue("daysOfWeekBitwise"));
		appointmentSlotImpl.setMaxAppointments(
			this.<Integer>getColumnOriginalValue("maxAppointments"));

		return appointmentSlotImpl;
	}

	@Override
	public int compareTo(AppointmentSlot appointmentSlot) {
		long primaryKey = appointmentSlot.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AppointmentSlot)) {
			return false;
		}

		AppointmentSlot appointmentSlot = (AppointmentSlot)object;

		long primaryKey = appointmentSlot.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isEntityCacheEnabled() {
		return true;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isFinderCacheEnabled() {
		return true;
	}

	@Override
	public void resetOriginalValues() {
		_columnOriginalValues = Collections.emptyMap();

		_setModifiedDate = false;

		_columnBitmask = 0;
	}

	@Override
	public CacheModel<AppointmentSlot> toCacheModel() {
		AppointmentSlotCacheModel appointmentSlotCacheModel =
			new AppointmentSlotCacheModel();

		appointmentSlotCacheModel.uuid = getUuid();

		String uuid = appointmentSlotCacheModel.uuid;

		if ((uuid != null) && (uuid.length() == 0)) {
			appointmentSlotCacheModel.uuid = null;
		}

		appointmentSlotCacheModel.appointmentSlotId = getAppointmentSlotId();

		appointmentSlotCacheModel.companyId = getCompanyId();

		appointmentSlotCacheModel.userId = getUserId();

		appointmentSlotCacheModel.userName = getUserName();

		String userName = appointmentSlotCacheModel.userName;

		if ((userName != null) && (userName.length() == 0)) {
			appointmentSlotCacheModel.userName = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			appointmentSlotCacheModel.createDate = createDate.getTime();
		}
		else {
			appointmentSlotCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			appointmentSlotCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			appointmentSlotCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		appointmentSlotCacheModel.slotType = getSlotType();

		String slotType = appointmentSlotCacheModel.slotType;

		if ((slotType != null) && (slotType.length() == 0)) {
			appointmentSlotCacheModel.slotType = null;
		}

		appointmentSlotCacheModel.name = getName();

		String name = appointmentSlotCacheModel.name;

		if ((name != null) && (name.length() == 0)) {
			appointmentSlotCacheModel.name = null;
		}

		appointmentSlotCacheModel.serviceId = getServiceId();

		String serviceId = appointmentSlotCacheModel.serviceId;

		if ((serviceId != null) && (serviceId.length() == 0)) {
			appointmentSlotCacheModel.serviceId = null;
		}

		Date startDate = getStartDate();

		if (startDate != null) {
			appointmentSlotCacheModel.startDate = startDate.getTime();
		}
		else {
			appointmentSlotCacheModel.startDate = Long.MIN_VALUE;
		}

		Date endDate = getEndDate();

		if (endDate != null) {
			appointmentSlotCacheModel.endDate = endDate.getTime();
		}
		else {
			appointmentSlotCacheModel.endDate = Long.MIN_VALUE;
		}

		appointmentSlotCacheModel.slotStartTimeHour = getSlotStartTimeHour();

		appointmentSlotCacheModel.slotStartTimeMinute =
			getSlotStartTimeMinute();

		appointmentSlotCacheModel.slotEndTimeHour = getSlotEndTimeHour();

		appointmentSlotCacheModel.slotEndTimeMinute = getSlotEndTimeMinute();

		appointmentSlotCacheModel.appointmentDuration =
			getAppointmentDuration();

		appointmentSlotCacheModel.daysOfWeekBitwise = getDaysOfWeekBitwise();

		appointmentSlotCacheModel.maxAppointments = getMaxAppointments();

		return appointmentSlotCacheModel;
	}

	@Override
	public String toString() {
		Map<String, Function<AppointmentSlot, Object>>
			attributeGetterFunctions = getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			(5 * attributeGetterFunctions.size()) + 2);

		sb.append("{");

		for (Map.Entry<String, Function<AppointmentSlot, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<AppointmentSlot, Object> attributeGetterFunction =
				entry.getValue();

			sb.append("\"");
			sb.append(attributeName);
			sb.append("\": ");

			Object value = attributeGetterFunction.apply((AppointmentSlot)this);

			if (value == null) {
				sb.append("null");
			}
			else if (value instanceof Blob || value instanceof Date ||
					 value instanceof Map || value instanceof String) {

				sb.append(
					"\"" + StringUtil.replace(value.toString(), "\"", "'") +
						"\"");
			}
			else {
				sb.append(value);
			}

			sb.append(", ");
		}

		if (sb.index() > 1) {
			sb.setIndex(sb.index() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	private static class EscapedModelProxyProviderFunctionHolder {

		private static final Function<InvocationHandler, AppointmentSlot>
			_escapedModelProxyProviderFunction =
				ProxyUtil.getProxyProviderFunction(
					AppointmentSlot.class, ModelWrapper.class);

	}

	private String _uuid;
	private long _appointmentSlotId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private boolean _setModifiedDate;
	private String _slotType;
	private String _name;
	private String _serviceId;
	private Date _startDate;
	private Date _endDate;
	private int _slotStartTimeHour;
	private int _slotStartTimeMinute;
	private int _slotEndTimeHour;
	private int _slotEndTimeMinute;
	private int _appointmentDuration;
	private long _daysOfWeekBitwise;
	private int _maxAppointments;

	public <T> T getColumnValue(String columnName) {
		columnName = _attributeNames.getOrDefault(columnName, columnName);

		Function<AppointmentSlot, Object> function =
			AttributeGetterFunctionsHolder._attributeGetterFunctions.get(
				columnName);

		if (function == null) {
			throw new IllegalArgumentException(
				"No attribute getter function found for " + columnName);
		}

		return (T)function.apply((AppointmentSlot)this);
	}

	public <T> T getColumnOriginalValue(String columnName) {
		if (_columnOriginalValues == null) {
			return null;
		}

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		return (T)_columnOriginalValues.get(columnName);
	}

	private void _setColumnOriginalValues() {
		_columnOriginalValues = new HashMap<String, Object>();

		_columnOriginalValues.put("uuid_", _uuid);
		_columnOriginalValues.put("appointmentSlotId", _appointmentSlotId);
		_columnOriginalValues.put("companyId", _companyId);
		_columnOriginalValues.put("userId", _userId);
		_columnOriginalValues.put("userName", _userName);
		_columnOriginalValues.put("createDate", _createDate);
		_columnOriginalValues.put("modifiedDate", _modifiedDate);
		_columnOriginalValues.put("slotType", _slotType);
		_columnOriginalValues.put("name", _name);
		_columnOriginalValues.put("serviceId", _serviceId);
		_columnOriginalValues.put("startDate", _startDate);
		_columnOriginalValues.put("endDate", _endDate);
		_columnOriginalValues.put("slotStartTimeHour", _slotStartTimeHour);
		_columnOriginalValues.put("slotStartTimeMinute", _slotStartTimeMinute);
		_columnOriginalValues.put("slotEndTimeHour", _slotEndTimeHour);
		_columnOriginalValues.put("slotEndTimeMinute", _slotEndTimeMinute);
		_columnOriginalValues.put("appointmentDuration", _appointmentDuration);
		_columnOriginalValues.put("daysOfWeekBitwise", _daysOfWeekBitwise);
		_columnOriginalValues.put("maxAppointments", _maxAppointments);
	}

	private static final Map<String, String> _attributeNames;

	static {
		Map<String, String> attributeNames = new HashMap<>();

		attributeNames.put("uuid_", "uuid");

		_attributeNames = Collections.unmodifiableMap(attributeNames);
	}

	private transient Map<String, Object> _columnOriginalValues;

	public static long getColumnBitmask(String columnName) {
		return _columnBitmasks.get(columnName);
	}

	private static final Map<String, Long> _columnBitmasks;

	static {
		Map<String, Long> columnBitmasks = new HashMap<>();

		columnBitmasks.put("uuid_", 1L);

		columnBitmasks.put("appointmentSlotId", 2L);

		columnBitmasks.put("companyId", 4L);

		columnBitmasks.put("userId", 8L);

		columnBitmasks.put("userName", 16L);

		columnBitmasks.put("createDate", 32L);

		columnBitmasks.put("modifiedDate", 64L);

		columnBitmasks.put("slotType", 128L);

		columnBitmasks.put("name", 256L);

		columnBitmasks.put("serviceId", 512L);

		columnBitmasks.put("startDate", 1024L);

		columnBitmasks.put("endDate", 2048L);

		columnBitmasks.put("slotStartTimeHour", 4096L);

		columnBitmasks.put("slotStartTimeMinute", 8192L);

		columnBitmasks.put("slotEndTimeHour", 16384L);

		columnBitmasks.put("slotEndTimeMinute", 32768L);

		columnBitmasks.put("appointmentDuration", 65536L);

		columnBitmasks.put("daysOfWeekBitwise", 131072L);

		columnBitmasks.put("maxAppointments", 262144L);

		_columnBitmasks = Collections.unmodifiableMap(columnBitmasks);
	}

	private long _columnBitmask;
	private AppointmentSlot _escapedModel;

}