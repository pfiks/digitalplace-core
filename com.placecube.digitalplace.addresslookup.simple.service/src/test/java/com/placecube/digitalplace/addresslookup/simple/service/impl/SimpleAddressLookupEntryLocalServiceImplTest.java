package com.placecube.digitalplace.addresslookup.simple.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;
import com.placecube.digitalplace.addresslookup.simple.service.persistence.SimpleAddressLookupEntryPK;
import com.placecube.digitalplace.addresslookup.simple.service.persistence.SimpleAddressLookupEntryPersistence;

public class SimpleAddressLookupEntryLocalServiceImplTest extends PowerMockito {

	@Mock
	private SimpleAddressLookupEntry mockSimpleAddressLookupEntry1;

	@Mock
	private SimpleAddressLookupEntry mockSimpleAddressLookupEntry2;

	@Mock
	private SimpleAddressLookupEntryPersistence mockSimpleAddressLookupEntryPersistence;

	@InjectMocks
	private SimpleAddressLookupEntryLocalServiceImpl simpleAddressLookupEntryLocalServiceImpl;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getByCompanyIdAndPostcode_WhenNoError_ThenReturnsAllTheEntriesFound() {
		long companyId = 1;
		String postcode = "postcodevalue";
		List<SimpleAddressLookupEntry> expected = new ArrayList<>();
		expected.add(mockSimpleAddressLookupEntry1);
		expected.add(mockSimpleAddressLookupEntry2);
		when(mockSimpleAddressLookupEntryPersistence.findByCompanyId_Postcode(companyId, postcode)).thenReturn(expected);

		List<SimpleAddressLookupEntry> results = simpleAddressLookupEntryLocalServiceImpl.getByCompanyIdAndPostcode(companyId, postcode);

		assertThat(results, sameInstance(expected));
	}

	@Test
	public void getByCompanyIdAndUprn_WhenEntryFound_ThenReturnsOptionalWithTheEntry() {
		long companyId = 1;
		String uprn = "uprnValue";
		when(mockSimpleAddressLookupEntryPersistence.fetchByPrimaryKey(new SimpleAddressLookupEntryPK(companyId, uprn))).thenReturn(mockSimpleAddressLookupEntry1);

		Optional<SimpleAddressLookupEntry> result = simpleAddressLookupEntryLocalServiceImpl.getByCompanyIdAndUprn(companyId, uprn);

		assertThat(result.get(), sameInstance(mockSimpleAddressLookupEntry1));
	}

	@Test
	public void getByCompanyIdAndUprn_WhenNoEntryFound_ThenReturnsEmptyOptional() {
		long companyId = 1;
		String uprn = "uprnValue";
		when(mockSimpleAddressLookupEntryPersistence.fetchByPrimaryKey(new SimpleAddressLookupEntryPK(companyId, uprn))).thenReturn(null);

		Optional<SimpleAddressLookupEntry> result = simpleAddressLookupEntryLocalServiceImpl.getByCompanyIdAndUprn(companyId, uprn);

		assertThat(result.isPresent(), equalTo(false));
	}

}
