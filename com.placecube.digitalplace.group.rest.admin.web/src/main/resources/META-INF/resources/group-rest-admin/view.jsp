<%@ include file="../init.jsp" %>

<div class="container-fluid container-fluid-max-xl">
	<div class="row">
		<div class="col-md-12">
			<h1>
				<liferay-ui:message key="api-key" />
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		
			<portlet:actionURL var="refreshAPIKeys" name="<%=MVCCommandKeys.UPDATE_API_KEYS%>" />
			
			<aui:form action="${refreshAPIKeys}" method="post" name="editStructureForm" cssClass="container-fluid container-fluid-max-xl">
				<div class="inline-alert-container lfr-alert-container"></div>
				
				<liferay-ui:error exception="<%= PortalException.class %>" message="there-was-an-unexpected-error.-please-refresh-the-current-page" />
					
				<aui:row>
					<aui:input name="applicationKey" type="text" disabled="true" label="application-key" value="${applicationKey}" />
				</aui:row>
				
				<aui:row>
					<aui:input name="applicationSecret" type="text" disabled="true" label="application-secret" value="${applicationSecret}" />
				</aui:row>
		
				<aui:button-row>
					<button class="btn btn-danger" >
						<liferay-ui:message key="regenerate-api-keys"/>
					</button>
				</aui:button-row>
			</aui:form>
		</div>
	</div>
</div>