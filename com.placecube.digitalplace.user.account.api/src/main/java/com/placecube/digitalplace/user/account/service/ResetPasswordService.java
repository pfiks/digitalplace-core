package com.placecube.digitalplace.user.account.service;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

public interface ResetPasswordService {

	void resetPassword(PortletRequest portletRequest) throws PortletException;
}
