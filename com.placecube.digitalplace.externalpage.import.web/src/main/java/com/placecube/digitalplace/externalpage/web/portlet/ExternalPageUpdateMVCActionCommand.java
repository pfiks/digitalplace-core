package com.placecube.digitalplace.externalpage.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET,
		"mvc.command.name=" + MVCCommandKeys.EXTERNAL_PAGE_UPDATE }, service = MVCActionCommand.class)
public class ExternalPageUpdateMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException {
		long externalPageId = ParamUtil.getLong(actionRequest, "externalPageId");
		String title = ParamUtil.getString(actionRequest, "title");
		String content = ParamUtil.getString(actionRequest, "content");

		if (externalPageId > 0) {
			ExternalPage externalPage = externalPageLocalService.fetchExternalPage(externalPageId);
			externalPage.setContent(content);
			externalPage.setTitle(title);
			externalPageLocalService.updateExternalPage(externalPage);
		} else {
			throw new PortalException("No externalPageId was found in the request");
		}
	}

}
