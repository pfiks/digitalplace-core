/**
 * SurchargeItemDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class SurchargeItemDetails  implements java.io.Serializable {
    private java.lang.String fundCode;

    private java.lang.String reference;

    private com.capita_software_services.www.scp.base.CardSurchargeRate[] surchargeInfo;

    private com.capita_software_services.www.scp.base.TaxSurcharge tax;

    public SurchargeItemDetails() {
    }

    public SurchargeItemDetails(
           java.lang.String fundCode,
           java.lang.String reference,
           com.capita_software_services.www.scp.base.CardSurchargeRate[] surchargeInfo,
           com.capita_software_services.www.scp.base.TaxSurcharge tax) {
           this.fundCode = fundCode;
           this.reference = reference;
           this.surchargeInfo = surchargeInfo;
           this.tax = tax;
    }


    /**
     * Gets the fundCode value for this SurchargeItemDetails.
     *
     * @return fundCode
     */
    public java.lang.String getFundCode() {
        return fundCode;
    }


    /**
     * Sets the fundCode value for this SurchargeItemDetails.
     *
     * @param fundCode
     */
    public void setFundCode(java.lang.String fundCode) {
        this.fundCode = fundCode;
    }


    /**
     * Gets the reference value for this SurchargeItemDetails.
     *
     * @return reference
     */
    public java.lang.String getReference() {
        return reference;
    }


    /**
     * Sets the reference value for this SurchargeItemDetails.
     *
     * @param reference
     */
    public void setReference(java.lang.String reference) {
        this.reference = reference;
    }


    /**
     * Gets the surchargeInfo value for this SurchargeItemDetails.
     *
     * @return surchargeInfo
     */
    public com.capita_software_services.www.scp.base.CardSurchargeRate[] getSurchargeInfo() {
        return surchargeInfo;
    }


    /**
     * Sets the surchargeInfo value for this SurchargeItemDetails.
     *
     * @param surchargeInfo
     */
    public void setSurchargeInfo(com.capita_software_services.www.scp.base.CardSurchargeRate[] surchargeInfo) {
        this.surchargeInfo = surchargeInfo;
    }


    /**
     * Gets the tax value for this SurchargeItemDetails.
     *
     * @return tax
     */
    public com.capita_software_services.www.scp.base.TaxSurcharge getTax() {
        return tax;
    }


    /**
     * Sets the tax value for this SurchargeItemDetails.
     *
     * @param tax
     */
    public void setTax(com.capita_software_services.www.scp.base.TaxSurcharge tax) {
        this.tax = tax;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SurchargeItemDetails)) return false;
        SurchargeItemDetails other = (SurchargeItemDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.fundCode==null && other.getFundCode()==null) ||
             (this.fundCode!=null &&
              this.fundCode.equals(other.getFundCode()))) &&
            ((this.reference==null && other.getReference()==null) ||
             (this.reference!=null &&
              this.reference.equals(other.getReference()))) &&
            ((this.surchargeInfo==null && other.getSurchargeInfo()==null) ||
             (this.surchargeInfo!=null &&
              java.util.Arrays.equals(this.surchargeInfo, other.getSurchargeInfo()))) &&
            ((this.tax==null && other.getTax()==null) ||
             (this.tax!=null &&
              this.tax.equals(other.getTax())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFundCode() != null) {
            _hashCode += getFundCode().hashCode();
        }
        if (getReference() != null) {
            _hashCode += getReference().hashCode();
        }
        if (getSurchargeInfo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSurchargeInfo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSurchargeInfo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTax() != null) {
            _hashCode += getTax().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SurchargeItemDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeItemDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "fundCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "reference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardSurchargeRate"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardSurchargeRate"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "tax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "taxSurcharge"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
