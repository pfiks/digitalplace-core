package com.placecube.digitalplace.ddl.service;

import java.util.List;
import java.util.Optional;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;

public interface DynamicDataListConfigurationService {

	/**
	 * Update the company configuration with the new mapping
	 *
	 * @param ddmStructure the ddmstructure to use in the mapping
	 * @param fullViewTemplate the default full view template
	 * @param listViewTemplate the default list view template
	 * @param gridViewTemplate the default grid view template
	 * @param titleField the title field name
	 * @param contentField the content field name
	 * @param iconName the name of the icon
	 * @throws ConfigurationException exception while updating the configuration
	 */
	void addDynamicDataListMappingConfiguration(DDMStructure ddmStructure, DDMTemplate fullViewTemplate, DDMTemplate listViewTemplate, DDMTemplate gridViewTemplate, String titleField,
			String contentField, String iconName) throws ConfigurationException;

	/**
	 * Returns an optional with the valid dynamic data list mapping for the
	 * specified structure. If no mapping is found or the mapping is invalid,
	 * then an empty optional is returned
	 *
	 * @param ddmStructure the ddm structure
	 * @return optional for the mapping
	 */
	Optional<DynamicDataListMapping> getDynamicDataListMapping(DDMStructure ddmStructure);

	/**
	 * Returns all the valid mappings for Dynamic data lists in the global group
	 * or in the specified group. A mapping is valid if the configured structure
	 * exists.
	 *
	 * @param group the group to retrieve mappings for
	 * @return list of DynamicDataListMapping in global or the group
	 * @throws PortalException
	 */
	List<DynamicDataListMapping> getDynamicDataListMappings(Group group) throws PortalException;

	/**
	 * Returns all the valid mappings for Dynamic data lists. A mapping is valid
	 * if the configured structure exists.
	 *
	 * @param companyId the company id
	 * @return list of DynamicDataListMapping
	 */
	List<DynamicDataListMapping> getDynamicDataListMappings(long companyId);
}
