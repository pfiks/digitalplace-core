package com.placecube.digitalplace.externalpage.web.constants;

public final class CrawlConstants {

	public static final String CUSTOM_DATA_COMPANY_ID = "companyId:";

	public static final String CUSTOM_DATA_GROUP_ID = "groupId:";

	public static final String CUSTOM_DATA_INDEX_CONTENT = "indexContent:";

	public static final String CUSTOM_DATA_USER_ID = "userId:";

	public static final String CUSTOM_DATA_XPATH = "xpath:";

	public static final String ENTRY_ONLY = "entryOnly";

	public static final String ENTRY_PUBLISHED = "entryPublished";

	private CrawlConstants() {
	}

}
