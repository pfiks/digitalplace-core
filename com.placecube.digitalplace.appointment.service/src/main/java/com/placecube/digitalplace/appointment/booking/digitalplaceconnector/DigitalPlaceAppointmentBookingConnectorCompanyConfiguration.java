package com.placecube.digitalplace.appointment.booking.digitalplaceconnector;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.appointment.booking.digitalplaceconnector.DigitalPlaceAppointmentBookingConnectorCompanyConfiguration", localization = "content/Language", name = "digitalplace-appointmentbooking-connector")
public interface DigitalPlaceAppointmentBookingConnectorCompanyConfiguration {

	@Meta.AD(required = false, name = "enabled")
	boolean enabled();

}
