package com.placecube.digitalplace.navigation.constants;

public final class ConfigurationConstants {

	public static final String DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID = "-1";

	public static final String DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_NAME = "Services";

	public static final String DEFAULT_CATEGORY_NAV_ROOT_ENTRY_NAME = "Home";

	public static final String DEFAULT_CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME = "landing-page";

	public static final String DEFAULT_CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME = "build-categories-hierarchy-url";

	public static final String BREADCRUMB_PID = "com.placecube.digitalplace.navigation.configuration.BreadcrumbPortletInstanceConfiguration";

	private ConfigurationConstants() {
		return;
	}

}
