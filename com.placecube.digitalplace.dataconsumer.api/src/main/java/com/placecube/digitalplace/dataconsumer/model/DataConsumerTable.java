/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.dataconsumer.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_DataConsumer_DataConsumer&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see DataConsumer
 * @generated
 */
public class DataConsumerTable extends BaseTable<DataConsumerTable> {

	public static final DataConsumerTable INSTANCE = new DataConsumerTable();

	public final Column<DataConsumerTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, Long> dataConsumerId = createColumn(
		"dataConsumerId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<DataConsumerTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, Long> userId = createColumn(
		"userId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, Date> createDate = createColumn(
		"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, Date> modifiedDate = createColumn(
		"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, Long> ddmFormInstanceId =
		createColumn(
			"ddmFormInstanceId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, Long> ddmDataProviderInstanceId =
		createColumn(
			"ddmDataProviderInstanceId", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, Long> sourceDataProviderId =
		createColumn(
			"sourceDataProviderId", Long.class, Types.BIGINT,
			Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, String> method = createColumn(
		"method", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, String> mapping = createColumn(
		"mapping", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, String> headers = createColumn(
		"headers", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, Integer> chain = createColumn(
		"chain", Integer.class, Types.INTEGER, Column.FLAG_DEFAULT);
	public final Column<DataConsumerTable, String> body = createColumn(
		"body", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);

	private DataConsumerTable() {
		super("Placecube_DataConsumer_DataConsumer", DataConsumerTable::new);
	}

}