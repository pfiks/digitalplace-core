# Editor configurations

### This module contains the configurations for CKeditor

A new configuration item called **Editor** was added to **Content and Data** category,
this configuration allows to choose what button/plugins are added to the CKEditor toolbar.

If empty, all plugins are added to the CK Editor. If a JSON configuration is provided for a plugin, the user needs to have the role.The plugins available are: **Undo**, **Redo**, **Bold**, **Italic**, **Underline**, **Strikethrough**, **Blockquote**, **NumberedList**, **BulletedList**, **Table**, **Link**, **Unlink**, **Styles**, **ImageSelector**, **VideoSelector**, **AudioSelector**, **Source**, **gdsConfirmationPanel**, **gdsDetails**, **gdsNotificationBanner**, **gdsWarningPanel**.The plugin name attribute accepts a list of Role names joined by comma. For Site roles, the user needs to have it on the site the content is edited.As examples; <br>
- Example 1: ` {"Source" : "Site Administrator,Administrator,Content Author"}, ` <br>
- Example 2: ` {"Source" : "Site Administrator,Administrator", "Preview" : "Content Author,User", "CallToAction" : "Content Author"},` <br>
- Example 3: ` {"NumberedList" : "Content Author", "BulletedList" : "Content Author", "Bold" : "Content Author"}`