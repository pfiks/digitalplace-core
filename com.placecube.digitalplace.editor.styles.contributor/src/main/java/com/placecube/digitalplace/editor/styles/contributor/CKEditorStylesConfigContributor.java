package com.placecube.digitalplace.editor.styles.contributor;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.editor.configuration.BaseEditorConfigContributor;
import com.liferay.portal.kernel.editor.configuration.EditorConfigContributor;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.editor.configuration.CKEditorConfigurationService;
import com.placecube.digitalplace.editor.configuration.constants.CKEditorCommonToolbarConstants;
import com.placecube.digitalplace.editor.configuration.service.CKEditorCommonToolbarHelper;
import com.placecube.digitalplace.editor.configuration.service.CKEditorCommonToolbarService;
import com.placecube.digitalplace.editor.styles.contributor.constants.ButtonStyle;
import com.placecube.digitalplace.editor.styles.contributor.service.CKEditorStyleService;

@Component(property = { "editor.name=ckeditor", "editor.name=ckeditor_classic", "service.ranking:Integer=100" }, service = EditorConfigContributor.class)
public class CKEditorStylesConfigContributor extends BaseEditorConfigContributor {

	@Reference
	private CKEditorStyleService ckeditorButtonStyleService;

	@Reference
	private CKEditorCommonToolbarHelper ckEditorCommonToolbarHelper;

	@Reference
	private CKEditorCommonToolbarService ckEditorCommonToolbarService;

	@Reference
	private CKEditorConfigurationService ckEditorConfigurationService;

	@Override
	public void populateConfigJSONObject(JSONObject jsonObject, Map<String, Object> inputEditorTaglibAttributes, ThemeDisplay themeDisplay,
			RequestBackedPortletURLFactory requestBackedPortletURLFactory) {

		Optional<JSONArray> textButtonStyles = ckeditorButtonStyleService.getStyleSet(jsonObject);

		if (textButtonStyles.isPresent()) {

			Set<String> buttonsList = getButtonList(themeDisplay);

			if (!buttonsList.isEmpty()) {
				for (String button : buttonsList) {
					textButtonStyles.get().put(ckeditorButtonStyleService.createStyle(ButtonStyle.valueOf(button)));
				}
			}
		}

		jsonObject.put("disableNativeSpellChecker", Boolean.FALSE);
	}

	private boolean checkUserPermission(Set<String> userRoleNames, JSONObject styleJSONObject, String role) {

		return Validator.isNotNull(styleJSONObject) ? ckEditorCommonToolbarHelper.hasUserRole(userRoleNames, role) : false;

	}

	private Set<String> getButtonList(ThemeDisplay themeDisplay) {

		Optional<JSONObject> pluginAndRolesConfig = ckEditorConfigurationService.getPluginAndRolesJsonConfiguration(themeDisplay.getCompanyId());

		Set<String> userRoleNames = ckEditorCommonToolbarService.getUserRoleName(themeDisplay);

		Optional<JSONArray> styleJSONArray = ckEditorCommonToolbarHelper.getStylePluginJsonArray(pluginAndRolesConfig);

		Set<String> buttonList = new HashSet<>();

		Set<String> allButtons = Stream.of(ButtonStyle.values()).map(ButtonStyle::name).collect(Collectors.toSet());

		if (styleJSONArray.isPresent()) {

			for (int i = 0; i < styleJSONArray.get().length(); i++) {

				JSONObject styleJSONObject = styleJSONArray.get().getJSONObject(i);
				String buttons = styleJSONObject.getString(CKEditorCommonToolbarConstants.BUTTONS);
				String pluginRoles = styleJSONObject.getString(CKEditorCommonToolbarConstants.ROLES);

				if (checkUserPermission(userRoleNames, styleJSONObject, pluginRoles)) {
					if (!buttons.isEmpty()) {
						buttonList.addAll(Arrays.asList(buttons.split(StringPool.COMMA)).stream().filter(allButtons::contains).collect(Collectors.toList()));

					} else if (buttons.isEmpty()) {
						buttonList.addAll(allButtons);
					}
				}

			}

		} else {
			buttonList.addAll(allButtons);
		}
		return buttonList;
	}
}
