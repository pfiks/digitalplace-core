/**
 * StoredCardDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class StoredCardDetails  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.StoredCardKey storedCardKey;

    private com.capita_software_services.www.portal_api.CardDescription cardDescription;

    private com.capita_software_services.www.scp.base.CardType cardType;

    public StoredCardDetails() {
    }

    public StoredCardDetails(
           com.capita_software_services.www.scp.base.StoredCardKey storedCardKey,
           com.capita_software_services.www.portal_api.CardDescription cardDescription,
           com.capita_software_services.www.scp.base.CardType cardType) {
           this.storedCardKey = storedCardKey;
           this.cardDescription = cardDescription;
           this.cardType = cardType;
    }


    /**
     * Gets the storedCardKey value for this StoredCardDetails.
     *
     * @return storedCardKey
     */
    public com.capita_software_services.www.scp.base.StoredCardKey getStoredCardKey() {
        return storedCardKey;
    }


    /**
     * Sets the storedCardKey value for this StoredCardDetails.
     *
     * @param storedCardKey
     */
    public void setStoredCardKey(com.capita_software_services.www.scp.base.StoredCardKey storedCardKey) {
        this.storedCardKey = storedCardKey;
    }


    /**
     * Gets the cardDescription value for this StoredCardDetails.
     *
     * @return cardDescription
     */
    public com.capita_software_services.www.portal_api.CardDescription getCardDescription() {
        return cardDescription;
    }


    /**
     * Sets the cardDescription value for this StoredCardDetails.
     *
     * @param cardDescription
     */
    public void setCardDescription(com.capita_software_services.www.portal_api.CardDescription cardDescription) {
        this.cardDescription = cardDescription;
    }


    /**
     * Gets the cardType value for this StoredCardDetails.
     *
     * @return cardType
     */
    public com.capita_software_services.www.scp.base.CardType getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this StoredCardDetails.
     *
     * @param cardType
     */
    public void setCardType(com.capita_software_services.www.scp.base.CardType cardType) {
        this.cardType = cardType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StoredCardDetails)) return false;
        StoredCardDetails other = (StoredCardDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.storedCardKey==null && other.getStoredCardKey()==null) ||
             (this.storedCardKey!=null &&
              this.storedCardKey.equals(other.getStoredCardKey()))) &&
            ((this.cardDescription==null && other.getCardDescription()==null) ||
             (this.cardDescription!=null &&
              this.cardDescription.equals(other.getCardDescription()))) &&
            ((this.cardType==null && other.getCardType()==null) ||
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStoredCardKey() != null) {
            _hashCode += getStoredCardKey().hashCode();
        }
        if (getCardDescription() != null) {
            _hashCode += getCardDescription().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StoredCardDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "storedCardDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storedCardKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "storedCardKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "storedCardKey"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/portal-api", "cardDescription"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
