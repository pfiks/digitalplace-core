package com.placecube.digitalplace.search.web.constants;

import com.liferay.portal.kernel.search.Field;

public final class SearchFacetConstants {

	public static final String ASSET_CATEGORY_IDS = Field.ASSET_CATEGORY_IDS;

	public static final String ASSET_TAG_NAMES = "assetTagNames.raw";

	public static final String CONTENT_TYPE = "contentType_sortable";

	private SearchFacetConstants() {
	}

}
