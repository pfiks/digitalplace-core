package com.placecube.digitalplace.navigation.portlet;
import java.util.Locale;

import com.liferay.portlet.display.template.BasePortletDisplayTemplateHandler;
import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;
import com.liferay.portal.kernel.template.TemplateHandler;
import com.placecube.digitalplace.navigation.constants.PortletKeys;

import org.osgi.service.component.annotations.Component;

@Component(
	immediate = true, property = "javax.portlet.name=" + PortletKeys.BREADCRUMB,
	service = TemplateHandler.class
)
public class BreadcrumbPortletDisplayTemplateHandler extends BasePortletDisplayTemplateHandler {

	private static final String TEMPLATE_HANDLER_NAME = "Category Breadcrumbs";

	@Override
	public String getClassName() {
		return BreadcrumbEntry.class.getName();
	}

	@Override
	public String getName(Locale locale) {
		return TEMPLATE_HANDLER_NAME;
	}

	@Override
	public String getResourceName() {
		return PortletKeys.BREADCRUMB;
	}

}