package com.placecube.digitalplace.addresslookup.dtf.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.addresslookup.dtf.configuration.DTFCompanyConfiguration", localization = "content/Language", name = "address-lookup-dtf")
public interface DTFCompanyConfiguration {

	@Meta.AD(required = false, deflt = "", name = "dtf-database-class")
	String databaseClass();

	@Meta.AD(required = false, deflt = "", name = "dtf-database-password", type = Meta.Type.Password)
	String databasePassword();

	@Meta.AD(required = false, deflt = "", name = "dtf-database-url")
	String databaseURL();

	@Meta.AD(required = false, deflt = "", name = "dtf-database-user")
	String databaseUser();

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "false", name = "national", description = "national-description")
	boolean national();

	@Meta.AD(required = false, deflt = "1", name = "weight", description = "weight-description")
	Integer weight();

}
