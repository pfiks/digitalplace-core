package com.placecube.digitalplace.user.account.web.service;

import static com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys.USER_ID;

import java.util.Set;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.common.utils.HtmlUtil;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.constants.AccountValidatedField;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountAddressService;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextFactory;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountDateOfBirthService;
import com.placecube.digitalplace.user.account.service.AccountEmailAddressService;
import com.placecube.digitalplace.user.account.service.AccountPhoneService;
import com.placecube.digitalplace.user.account.service.AccountUserFieldsService;
import com.placecube.digitalplace.user.account.service.AccountUserService;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;
import com.placecube.digitalplace.userprofile.service.UserProfileFieldsService;

@Component(immediate = true, service = AccountContextService.class)
public class AccountContextServiceImpl implements AccountContextService {

	private static final Log LOG = LogFactoryUtil.getLog(AccountContextService.class);

	@Reference
	private AccountAddressService accountAddressService;

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextFactory accountContextFactory;

	@Reference
	private AccountDateOfBirthService accountDateOfBirthService;

	@Reference
	private AccountEmailAddressService accountEmailAddressService;

	@Reference
	private AccountPhoneService accountPhoneService;

	@Reference
	private AccountUserFieldsService accountUserFieldsService;

	@Reference
	private AccountUserService accountUserService;

	@Reference
	private UserAccountFieldRegistry userAccountFieldRegistry;

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private UserProfileFieldsService userProfileFieldsService;

	@Override
	public void clearContext(PortletRequest portletRequest) {
		portletRequest.getPortletSession().removeAttribute(AccountConstants.SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE);
	}

	@Override
	public void configureAccountFields(AccountContext accountContext, PortletRequest portletRequest) {
		for (UserAccountField userAccountField : userAccountFieldRegistry.getUserAccountFields(accountContext.getCompanyId())) {
			userAccountField.configureContextValue(accountContext, portletRequest);
		}
	}

	@Override
	public void configureAccountSecurity(AccountContext accountContext, PortletRequest portletRequest) {
		accountContext.setTwoFactorAuthenticationEnabled(ParamUtil.getBoolean(portletRequest, AccountValidatedField.TWO_FACTOR_AUTHENTICATION_ENABLED.getFieldName()));
	}

	@Override
	public void configureAdditionalDetails(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException {
		CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, portletRequest);

		accountContext.setJobTitle(com.liferay.portal.kernel.util.HtmlUtil.escape(ParamUtil.getString(portletRequest, AccountValidatedField.JOB_TITLE.getFieldName())));
		accountContext.setHomePhoneNumber(ParamUtil.getString(portletRequest, AccountValidatedField.HOME_PHONE.getFieldName()));
		accountContext.setMobilePhoneNumber(ParamUtil.getString(portletRequest, AccountValidatedField.MOBILE_PHONE.getFieldName()));
		accountContext.setBusinessPhoneNumber(ParamUtil.getString(portletRequest, AccountValidatedField.BUSINESS_PHONE.getFieldName()));

		if (AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS.equals(accountConfiguration.emailAddressPageLocation())) {
			accountContext.setEmailAddress(ParamUtil.getString(portletRequest, AccountValidatedField.EMAIL.getFieldName()));
			accountContext.setConfirmEmailAddress(ParamUtil.getString(portletRequest, AccountValidatedField.EMAIL_CONFIRMATION.getFieldName()));
		}

		configureAccountFields(AccountCreationStep.ADDITIONAL_DETAILS, accountContext, portletRequest);
		configureExpandoFields(accountContext, portletRequest);
	}

	@Override
	public void configureAddressDetails(AccountContext accountContext, PortletRequest portletRequest) {
		String uprn = ParamUtil.getString(portletRequest, RequestKeys.UPRN);
		String postcode = ParamUtil.getString(portletRequest, "postcode");
		String fullAddress = ParamUtil.getString(portletRequest, "fullAddress");

		if (Validator.isNotNull(uprn) && Validator.isNotNull(postcode) && Validator.isNotNull(fullAddress)) {
			accountContext.setUprn(uprn);
			accountContext.setPostcode(postcode);
			accountContext.setFullAddress(fullAddress);
		}

		configureAccountFields(AccountCreationStep.ADDRESS_DETAILS, accountContext, portletRequest);
	}

	@Override
	public void configureAutoGeneratedEmailDetails(AccountContext accountContext) throws UserAccountException {
		if (Validator.isNull(accountContext.getEmailAddress())) {
			String generatedEmailAddress = accountEmailAddressService.getGeneratedEmailAddress(accountContext);
			accountContext.setEmailAddress(generatedEmailAddress);
			accountContext.setConfirmEmailAddress(generatedEmailAddress);
			accountContext.setEmailAddressAutoGenerated(true);
		}
	}

	@Override
	public void configureExpandoFields(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException {
		try {
			Set<ExpandoField> configuredExpandoFields = userProfileFieldsService.getConfiguredExpandoFields(accountContext.getCompanyId(),
					userProfileFieldsService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId()));
			for (ExpandoField expandoField : configuredExpandoFields) {
				accountContext.addExpandoField(expandoField.getExpandoFieldKey(), com.liferay.portal.kernel.util.HtmlUtil.escape(ParamUtil.getString(portletRequest, "expando--" + expandoField.getExpandoFieldKey())));
			}
		} catch (ConfigurationException e) {
			throw new UserAccountException(e);
		}
	}

	@Override
	public void configurePasswordDetails(AccountContext accountContext, PortletRequest portletRequest) {
		accountContext.setPassword1(ParamUtil.getString(portletRequest, AccountValidatedField.PASSWORD.getFieldName()));
		accountContext.setPassword2(ParamUtil.getString(portletRequest, AccountValidatedField.PASSWORD_CONFIRM.getFieldName()));
	}

	@Override
	public void configurePersonalDetails(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException {
		CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, portletRequest);

		accountContext.setFirstName(HtmlUtil.escapeName(ParamUtil.getString(portletRequest, AccountValidatedField.FIRSTNAME.getFieldName())));
		accountContext.setLastName(HtmlUtil.escapeName(ParamUtil.getString(portletRequest, AccountValidatedField.LASTNAME.getFieldName())));
		accountContext.setBirthDay(ParamUtil.getInteger(portletRequest, "dateOfBirth_day"));
		accountContext.setBirthMonth(ParamUtil.getInteger(portletRequest, "dateOfBirth_month"));
		accountContext.setBirthYear(ParamUtil.getInteger(portletRequest, "dateOfBirth_year"));

		if (AccountConfigurationConstants.PAGE_PERSONAL_DETAILS.equals(accountConfiguration.emailAddressPageLocation())) {
			accountContext.setEmailAddress(ParamUtil.getString(portletRequest, AccountValidatedField.EMAIL.getFieldName()));
			accountContext.setConfirmEmailAddress(ParamUtil.getString(portletRequest, AccountValidatedField.EMAIL_CONFIRMATION.getFieldName()));
		}
	}

	@Override
	public void configureSkipPasswordStep(AccountContext accountContext) throws UserAccountException {
		CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext);
		accountContext.setSkipPasswordStep(accountConfiguration.generatePasswordWhenLoggedInUserCreateAccount() && accountContext.isCreatorSignedIn());
	}

	@Override
	public String generateProfileRedirectUrl(AccountContext accountContext) {
		if (Validator.isNotNull(accountContext.getProfileRedirectUrl()) && accountContext.getUserId() > 0) {
			return accountContext.getProfileRedirectUrl().replace(AccountConstants.REDIRECT_URL_PLACEHOLDER, "userId=" + accountContext.getUserId());
		}
		return StringPool.BLANK;
	}

	@Override
	public AccountContext getContextFromUserDetails(PortletRequest portletRequest, UserProfileFieldsConfiguration userFieldsConfiguration) throws UserAccountException {
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		AccountContext accountContext = accountContextFactory.createAccountContext();
		accountContext.setUpdate(true);
		updateContextCompanyAndGroup(accountContext, themeDisplay);

		long userId = ParamUtil.getLong(portletRequest, USER_ID, 0);
		try {
			User user = userLocalService.getUserById(userId);

			accountUserService.populateContextWithExistingUserDetails(accountContext, user);

			accountDateOfBirthService.populateContextWithExistingDateOfBirth(accountContext, user);

			accountAddressService.populateContextWithExistingPrimaryAddress(accountContext, user);

			accountPhoneService.populateContextWithExistingPhones(accountContext, user);

			accountUserFieldsService.populateContextWithExistingConfiguredExpandoFields(accountContext, user, userFieldsConfiguration);

			accountUserFieldsService.populateContextWithExistingAccountFields(accountContext, user);
		} catch (PortalException e) {
			throw new UserAccountException(e);
		}
		return accountContext;
	}

	@Override
	public AccountContext getOrCreateContext(PortletRequest portletRequest) throws UserAccountException {
		boolean useSession = ParamUtil.getBoolean(portletRequest, RequestKeys.USE_SESSION, true);
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		AccountContext accountContext = null;

		if (useSession) {
			accountContext = (AccountContext) portletRequest.getPortletSession().getAttribute(AccountConstants.SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE);
		}

		if (Validator.isNull(accountContext)) {
			accountContext = accountContextFactory.createAccountContext();
			accountContext.setCreatorSignedIn(themeDisplay.isSignedIn());
		}

		updateContextCompanyAndGroup(accountContext, themeDisplay);

		setPhoneNumberToAccountContext(portletRequest, accountContext);

		return accountContext;
	}

	@Override
	public boolean isGeneratePasswordWhenLoggedInUserCreateAccount(AccountContext accountContext) throws UserAccountException {
		CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext);
		return accountConfiguration.resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount() && accountContext.isCreatorSignedIn();
	}

	@Override
	public boolean isShowCaptchaOnAccountSecurityStep(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException {
		CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, portletRequest);

		return accountConfigurationService.isCaptchaEnabled(portletRequest) && !accountConfiguration.requireCaptchaBeforeEmailAddressEntry() && !accountContext.isCreatorSignedIn()
				&& accountConfigurationService.isAccountSecurityStepEnabled(accountContext);
	}

	@Override
	public boolean isShowCaptchaOnEmailStep(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException {
		CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, portletRequest);

		return accountConfigurationService.isCaptchaEnabled(portletRequest) && accountConfiguration.requireCaptchaBeforeEmailAddressEntry() && !accountContext.isCreatorSignedIn();
	}

	@Override
	public boolean isShowCaptchaOnPasswordStep(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException {
		CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, portletRequest);

		return accountConfigurationService.isCaptchaEnabled(portletRequest) && !accountConfiguration.requireCaptchaBeforeEmailAddressEntry() && !accountContext.isCreatorSignedIn()
				&& !accountConfigurationService.isAccountSecurityStepEnabled(accountContext);
	}

	public void setPhoneNumberToAccountContext(PortletRequest portletRequest, AccountContext accountContext) {

		String userData = ParamUtil.getString(portletRequest, RequestKeys.USER_DATA);
		try {
			UserProfileFieldsConfiguration userFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());

			if (Validator.isPhoneNumber(userData)) {
				if (userFieldsConfiguration.mobilePhoneNumberEnabled()) {
					accountContext.setMobilePhoneNumber(userData);
				} else if (userFieldsConfiguration.businessPhoneNumberEnabled()) {
					accountContext.setBusinessPhoneNumber(userData);
				} else if (userFieldsConfiguration.homePhoneNumberEnabled()) {
					accountContext.setHomePhoneNumber(userData);
				}
			}
		} catch (UserAccountException e) {
			LOG.error(e);
		}
	}

	@Override
	public void updateContextInSession(AccountContext accountContext, PortletRequest portletRequest) {
		portletRequest.getPortletSession().setAttribute(AccountConstants.SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE, accountContext);
	}

	private void configureAccountFields(AccountCreationStep accountCreationStep, AccountContext accountContext, PortletRequest portletRequest) {
		for (UserAccountField userAccountField : userAccountFieldRegistry.getUserAccountFieldsByStep(accountCreationStep, accountContext.getCompanyId())) {
			userAccountField.configureContextValue(accountContext, portletRequest);
		}
	}

	private void updateContextCompanyAndGroup(AccountContext accountContext, ThemeDisplay themeDisplay) {
		accountContext.setCompanyId(themeDisplay.getCompanyId());
		accountContext.setGroupId(themeDisplay.getScopeGroupId());
	}

}
