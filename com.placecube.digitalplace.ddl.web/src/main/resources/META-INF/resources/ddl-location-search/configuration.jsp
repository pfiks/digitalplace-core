<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="../init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post">

	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />

	<liferay-frontend:edit-form-body>
		<div class="sheet">
			<div class="panel-group panel-group-flush">

				<liferay-frontend:fieldset>
					<%@ include file ="../configuration-structure-select.jspf" %>
				</liferay-frontend:fieldset>

				<aui:input
					label="destination-page"
					name="destinationPage"
					type="text"
					value="${ destinationPage }"
				/>
	
				<liferay-frontend:fieldset>
					<aui:input
						label="geo-location-field"
						name="geoLocationField"
						type="text"
						value="${ geoLocationField }"
					/>

				</liferay-frontend:fieldset>
			</div>
		</div>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>
