<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ include file="init.jsp"%>

<liferay-portlet:renderURL var="externalPageURL">
	<portlet:param name="mvcRenderCommandName"
		value="<%=MVCCommandKeys.EXTERNAL_PAGE_VIEW%>" />
</liferay-portlet:renderURL>

<portlet:actionURL name="<%=MVCCommandKeys.EXTERNAL_PAGE_UPDATE%>"
	var="updateExternalImportPage">
</portlet:actionURL>

<aui:form action="<%=updateExternalImportPage%>" cssClass="edit-entry"
	method="post" name="fm">
	<div class="container-fluid container-fluid-max-xl pt-4">
		<div class="sheet">
			<div class="row">
				<div class="col-md-9">
					<aui:input name="externalPageId" type="hidden" value="${externalPage.externalPageId}" >
					</aui:input>
					<aui:input label="Title" name="title" required="true" type="text"
						value="${externalPage.title}" >
						<aui:validator name="maxLength" >100</aui:validator>
					</aui:input>
					<aui:input label="Content" name="content" required="true"
						type="textarea" value="${externalPage.content}" >
					</aui:input>
					<aui:button-row>
						<aui:button cssClass="btn-lg" type="submit" />
						<aui:button cssClass="btn-lg" data-cancel="${externalPageURL}"
							href="${externalPageURL}" type="cancel" />
					</aui:button-row>
				</div>
			</div>
		</div>
	</div>

</aui:form>
