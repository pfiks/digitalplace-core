package com.placecube.digitalplace.pageimportadmin.portlet;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.apache.poi.ss.usermodel.Sheet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.pageimportadmin.constants.MVCCommandKeys;
import com.placecube.digitalplace.pageimportadmin.constants.PortletKeys;
import com.placecube.digitalplace.pageimportadmin.model.PageImportModel;
import com.placecube.digitalplace.pageimportadmin.model.RowImportModel;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;
import com.placecube.digitalplace.pageimportadmin.service.ContentImportService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.PAGE_IMPORT_ADMIN, "mvc.command.name=" + MVCCommandKeys.IMPORT_PAGES }, service = MVCActionCommand.class)
public class PageImportMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(PageImportMVCActionCommand.class);

	@Reference
	private ContentImportService contentImportService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {
		try {
			File file = contentImportService.getFile(actionRequest);
			Optional<Sheet> sheetOpt = contentImportService.getFirstSheet(file);

			if (sheetOpt.isPresent()) {

				Sheet sheet = sheetOpt.get();

				ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
				long groupId = themeDisplay.getScopeGroupId();
				Locale locale = themeDisplay.getLocale();

				boolean privatePage = ParamUtil.getBoolean(actionRequest, "importAsPrivate");
				long assetVocabularyId = ParamUtil.getLong(actionRequest, "assetVocabularyId");

				List<RowImportModel> rowImportModels = new LinkedList<>();

				for (int index = 0; index <= sheet.getLastRowNum(); index++) {

					RowImportModel rowImportModel = contentImportService.getRowContent(sheet, index, groupId, locale, privatePage, assetVocabularyId);

					if (rowImportModel.isWebContent()) {
						contentImportService.createWebContent((WebContentImportModel) rowImportModel, locale, actionRequest);
					} else if (rowImportModel.isPageImport()) {
						contentImportService.createPage((PageImportModel) rowImportModel, locale, actionRequest);
					}

					rowImportModels.add(rowImportModel);
				}

				actionRequest.setAttribute("rowImportModels", rowImportModels);
				
			} else {
				actionRequest.setAttribute("errorMessage", "could-not-find-content-in-file-provided");
				SessionErrors.add(actionRequest, "PageImportError");
			}

		} catch (Exception e) {
			actionRequest.setAttribute("errorMessage", "something-went-wrong");
			SessionErrors.add(actionRequest, "PageImportError");
			LOG.error(e);
		}
	}

}
