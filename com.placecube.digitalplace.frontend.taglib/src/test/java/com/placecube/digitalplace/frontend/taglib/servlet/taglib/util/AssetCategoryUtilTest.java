package com.placecube.digitalplace.frontend.taglib.servlet.taglib.util;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, AssetCategoryLocalServiceUtil.class })
public class AssetCategoryUtilTest extends PowerMockito {

	private static final Locale DEFAULT_LOCALE = Locale.CANADA;

	@Mock
	private AssetCategory mockAssetCategory;

	@Mock
	private AssetCategory mockAssetCategory1;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private AssetVocabulary mockAssetVocabulary1;

	@Mock
	private AssetVocabulary mockAssetVocabulary2;

	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(PortalUtil.class, AssetCategoryLocalServiceUtil.class);
	}

	@Test
	public void filterCategoryIds_WhenCategoryIsNotNull_ThenReturnsCategoryIds() {
		long vocabularyId = 123;
		long categoryId = 421;
		long[] categoryIds = new long[] { categoryId };
		when(mockAssetCategory.getVocabularyId()).thenReturn(vocabularyId);
		when(AssetCategoryLocalServiceUtil.fetchCategory(categoryId)).thenReturn(mockAssetCategory);

		long[] actualResult = AssetCategoryUtil.filterCategoryIds(vocabularyId, categoryIds);

		assertEquals(1, actualResult.length);
	}

	@Test
	public void filterCategoryIds_WhenCategoryIsNull_ThenReturnsEmptyLongArray() {
		long categoryId = 421;
		long[] categoryIds = new long[] { categoryId };

		when(AssetCategoryLocalServiceUtil.fetchCategory(categoryId)).thenReturn(null);

		long[] actualResult = AssetCategoryUtil.filterCategoryIds(0, categoryIds);

		assertEquals(0, actualResult.length);
	}

	@Test
	public void getCategoryIdsTitles_WhenAssetCategoryIsNull_ThenReturnArrayOfCategoryIdsAndTitles() {
		long vocabularyId = 434;
		long catId1 = 123;
		long catId2 = 456;
		long[] categoryIds = new long[] { catId1, catId2 };

		when(AssetCategoryLocalServiceUtil.fetchCategory(catId1)).thenReturn(null);

		String[] categoryIdTitles = AssetCategoryUtil.getCategoryIdsTitles(categoryIds, vocabularyId, mockThemeDisplay);

		assertEquals(StringPool.BLANK, categoryIdTitles[0]);
		assertEquals(StringPool.BLANK, categoryIdTitles[1]);
	}

	@Test
	public void getCategoryIdsTitles_WhenCategoryIdsIsEmpty_ThenReturnsEmptyStringArray() {
		long[] categoryId = new long[] {};
		long vocabularyId = 123;

		String[] actualResult = AssetCategoryUtil.getCategoryIdsTitles(categoryId, vocabularyId, mockThemeDisplay);

		assertEquals(StringPool.BLANK, actualResult[0]);
		assertEquals(StringPool.BLANK, actualResult[1]);
	}

	@Test
	public void getCategoryIdsTitles_WhenCategoryIdsIsNotEmpty_ThenReturnArrayOfCategoryIdsAndTitles() {
		long vocabularyId = 0;
		String catTitle1 = "title1";
		String catTitle2 = "title1";
		long catId1 = 123;
		long catId2 = 456;
		long[] categoryIds = new long[] { catId1, catId2 };

		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(AssetCategoryLocalServiceUtil.fetchCategory(catId1)).thenReturn(mockAssetCategory1);
		when(AssetCategoryLocalServiceUtil.fetchCategory(catId2)).thenReturn(mockAssetCategory2);
		when(mockAssetCategory1.getTitle(DEFAULT_LOCALE)).thenReturn(catTitle1);
		when(mockAssetCategory2.getTitle(DEFAULT_LOCALE)).thenReturn(catTitle2);
		when(mockAssetCategory1.getVocabularyId()).thenReturn(vocabularyId);
		when(mockAssetCategory2.getVocabularyId()).thenReturn(vocabularyId);

		String[] categoryIdTitles = AssetCategoryUtil.getCategoryIdsTitles(categoryIds, vocabularyId, mockThemeDisplay);
		String resultingCategoryIds = categoryIdTitles[0];
		String resultingCategoryIdTitles = categoryIdTitles[1];

		assertEquals(resultingCategoryIds, catId1 + StringPool.COMMA + catId2);
		assertEquals(resultingCategoryIdTitles, catTitle1 + AssetCategoriesSelectorTagUtil.CATEGORY_TITLES_SEPARATOR + catTitle2);
	}

}
