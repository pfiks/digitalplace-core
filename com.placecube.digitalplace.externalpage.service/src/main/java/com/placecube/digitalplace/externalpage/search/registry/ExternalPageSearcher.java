package com.placecube.digitalplace.externalpage.search.registry;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.BaseSearcher;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.externalpage.model.ExternalPage;

@Component(immediate = true, property = "model.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage", service = BaseSearcher.class)
public class ExternalPageSearcher extends BaseSearcher {

	private static final String CLASS_NAME = ExternalPage.class.getName();

	public ExternalPageSearcher() {
		setDefaultSelectedFieldNames(Field.ENTRY_CLASS_PK, Field.ENTRY_CLASS_NAME, Field.COMPANY_ID, Field.CONTENT, Field.GROUP_ID, Field.TITLE, Field.UID, Field.URL);
		setFilterSearch(false);
		setPermissionAware(false);
	}

	@Override
	public String getClassName() {
		return CLASS_NAME;
	}

}