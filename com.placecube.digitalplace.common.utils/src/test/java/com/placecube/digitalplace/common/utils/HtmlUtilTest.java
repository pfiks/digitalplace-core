package com.placecube.digitalplace.common.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ com.liferay.portal.kernel.util.HtmlUtil.class })
public class HtmlUtilTest extends PowerMockito {

	@Before
	public void activateSetup() {
		mockStatic(com.liferay.portal.kernel.util.HtmlUtil.class);
	}

	@Test
	public void escapeName_WhenNoError_ThenEscapeName() {
		String escapedNameByHtmlUtil = "Shaquille O&#39;neal &lt; &gt; &amp; &#34; &#187; &#8211; &#8212; &#8232;";
		String escapedName = "Shaquille O'neal &lt; &gt; &amp; &#34; &#187; &#8211; &#8212; &#8232;";

		String name = "Shaquille O'neal < > & \" » – — \u2028";
		when(com.liferay.portal.kernel.util.HtmlUtil.escape(name)).thenReturn(escapedNameByHtmlUtil);
		String result = HtmlUtil.escapeName(name);
		assertEquals(escapedName, result);
	}

}