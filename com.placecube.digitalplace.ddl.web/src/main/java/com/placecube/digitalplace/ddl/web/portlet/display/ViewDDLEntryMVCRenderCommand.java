package com.placecube.digitalplace.ddl.web.portlet.display;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.servlet.PortletServlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.service.DynamicDataListTemplateContentService;
import com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLEditorService;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLPermissionService;

@Component(property = { "javax.portlet.name=" + PortletKeys.DDL_DISPLAY_PORTLET, "mvc.command.name=" + MVCCommandKeys.VIEW_ENTRY }, service = MVCRenderCommand.class)
public class ViewDDLEntryMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private DDLEditorService ddlEditorService;

	@Reference
	private DDLPermissionService ddlPermissionService;

	@Reference
	private DynamicDataListTemplateContentService dynamicDataListTemplateContentService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			HttpServletRequest httpServletRequest = (HttpServletRequest) renderRequest.getAttribute(PortletServlet.PORTLET_SERVLET_REQUEST);

			long ddmStructureId = ParamUtil.getLong(renderRequest, "ddmStructureId");
			long ddlRecordId = ParamUtil.getLong(renderRequest, "ddlRecordId");
			long ddlTemplateId = ParamUtil.getLong(renderRequest, "ddlTemplateId");

			DDLRecord ddlRecord = ddlEditorService.getDDLRecord(ddlRecordId).orElse(null);

			String ddlContentForTemplate = dynamicDataListTemplateContentService.getTransformedTemplate(ddlRecord, ddlTemplateId, themeDisplay, httpServletRequest);

			PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();

			renderRequest.setAttribute("hasUpdatePermission", ddlPermissionService.hasUpdatePermission(permissionChecker, ddlRecord));
			renderRequest.setAttribute("hasDeletePermission", ddlPermissionService.hasDeletePermission(permissionChecker, ddlRecord));
			renderRequest.setAttribute("ddmStructureId", ddmStructureId);
			renderRequest.setAttribute("ddlRecordId", ddlRecordId);
			renderRequest.setAttribute("ddlTemplateId", ddlTemplateId);
			renderRequest.setAttribute("ddlContentForTemplate", ddlContentForTemplate);

			String backURL = ddlEditorService.getBackURL(renderRequest, themeDisplay);
			ddlEditorService.configureBackLinkURL(themeDisplay, backURL, renderRequest);

			return "/ddl-display/view-entry.jsp";
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}
