/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.dataconsumer.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.dataconsumer.model.DataConsumer;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for DataConsumer. This utility wraps
 * <code>com.placecube.digitalplace.dataconsumer.service.impl.DataConsumerLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see DataConsumerLocalService
 * @generated
 */
public class DataConsumerLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.dataconsumer.service.impl.DataConsumerLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the data consumer to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DataConsumerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dataConsumer the data consumer
	 * @return the data consumer that was added
	 */
	public static DataConsumer addDataConsumer(DataConsumer dataConsumer) {
		return getService().addDataConsumer(dataConsumer);
	}

	public static DataConsumer createDataConsumer() {
		return getService().createDataConsumer();
	}

	/**
	 * Creates a new data consumer with the primary key. Does not add the data consumer to the database.
	 *
	 * @param dataConsumerId the primary key for the new data consumer
	 * @return the new data consumer
	 */
	public static DataConsumer createDataConsumer(long dataConsumerId) {
		return getService().createDataConsumer(dataConsumerId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the data consumer from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DataConsumerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dataConsumer the data consumer
	 * @return the data consumer that was removed
	 */
	public static DataConsumer deleteDataConsumer(DataConsumer dataConsumer) {
		return getService().deleteDataConsumer(dataConsumer);
	}

	/**
	 * Deletes the data consumer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DataConsumerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer that was removed
	 * @throws PortalException if a data consumer with the primary key could not be found
	 */
	public static DataConsumer deleteDataConsumer(long dataConsumerId)
		throws PortalException {

		return getService().deleteDataConsumer(dataConsumerId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.dataconsumer.model.impl.DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.dataconsumer.model.impl.DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static DataConsumer fetchDataConsumer(long dataConsumerId) {
		return getService().fetchDataConsumer(dataConsumerId);
	}

	/**
	 * Returns the data consumer matching the UUID and group.
	 *
	 * @param uuid the data consumer's UUID
	 * @param groupId the primary key of the group
	 * @return the matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchDataConsumerByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchDataConsumerByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the data consumer with the primary key.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer
	 * @throws PortalException if a data consumer with the primary key could not be found
	 */
	public static DataConsumer getDataConsumer(long dataConsumerId)
		throws PortalException {

		return getService().getDataConsumer(dataConsumerId);
	}

	/**
	 * Returns the data consumer matching the UUID and group.
	 *
	 * @param uuid the data consumer's UUID
	 * @param groupId the primary key of the group
	 * @return the matching data consumer
	 * @throws PortalException if a matching data consumer could not be found
	 */
	public static DataConsumer getDataConsumerByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getDataConsumerByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.dataconsumer.model.impl.DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of data consumers
	 */
	public static List<DataConsumer> getDataConsumers(int start, int end) {
		return getService().getDataConsumers(start, end);
	}

	public static List<DataConsumer> getDataConsumersByCompanyIdAndGroupId(
		long companyId, long groupId) {

		return getService().getDataConsumersByCompanyIdAndGroupId(
			companyId, groupId);
	}

	public static List<DataConsumer> getDataConsumersByDDMFormInstanceId(
		long ddmFormInstanceId) {

		return getService().getDataConsumersByDDMFormInstanceId(
			ddmFormInstanceId);
	}

	/**
	 * Returns all the data consumers matching the UUID and company.
	 *
	 * @param uuid the UUID of the data consumers
	 * @param companyId the primary key of the company
	 * @return the matching data consumers, or an empty list if no matches were found
	 */
	public static List<DataConsumer> getDataConsumersByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getDataConsumersByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of data consumers matching the UUID and company.
	 *
	 * @param uuid the UUID of the data consumers
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching data consumers, or an empty list if no matches were found
	 */
	public static List<DataConsumer> getDataConsumersByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getService().getDataConsumersByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of data consumers.
	 *
	 * @return the number of data consumers
	 */
	public static int getDataConsumersCount() {
		return getService().getDataConsumersCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the data consumer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DataConsumerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dataConsumer the data consumer
	 * @return the data consumer that was updated
	 */
	public static DataConsumer updateDataConsumer(DataConsumer dataConsumer) {
		return getService().updateDataConsumer(dataConsumer);
	}

	public static DataConsumerLocalService getService() {
		return _service;
	}

	public static void setService(DataConsumerLocalService service) {
		_service = service;
	}

	private static volatile DataConsumerLocalService _service;

}