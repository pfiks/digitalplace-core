package com.placecube.digitalplace.payment.govuk.service.internal;

import static com.liferay.portal.kernel.util.UnicodeFormatter.bytesToHex;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = GovUKPaymentSecurityUtil.class)
public class GovUKPaymentSecurityUtil {

	public static final String ALGORITHM = "HmacSHA256";

	public String hmacWithJava(String algorithm, String data, String key) throws NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), algorithm);
		Mac mac = Mac.getInstance(algorithm);
		mac.init(secretKeySpec);
		return bytesToHex(mac.doFinal(data.getBytes()));
	}

}
