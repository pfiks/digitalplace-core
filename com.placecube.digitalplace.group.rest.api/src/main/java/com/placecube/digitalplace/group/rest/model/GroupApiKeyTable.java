/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Clob;
import java.sql.Types;

/**
 * The table class for the &quot;Placecube_GroupRest_GroupApiKey&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see GroupApiKey
 * @generated
 */
public class GroupApiKeyTable extends BaseTable<GroupApiKeyTable> {

	public static final GroupApiKeyTable INSTANCE = new GroupApiKeyTable();

	public final Column<GroupApiKeyTable, Long> companyId = createColumn(
		"companyId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<GroupApiKeyTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<GroupApiKeyTable, Clob> applicationKey = createColumn(
		"applicationKey", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);
	public final Column<GroupApiKeyTable, Clob> applicationSecret =
		createColumn(
			"applicationSecret", Clob.class, Types.CLOB, Column.FLAG_DEFAULT);

	private GroupApiKeyTable() {
		super("Placecube_GroupRest_GroupApiKey", GroupApiKeyTable::new);
	}

}