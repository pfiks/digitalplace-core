package com.placecube.digitalplace.calendar.web.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.calendar.web.portlet.configuration.DigitalPlaceCalendarInstanceConfiguration;

public class DigitalPlaceCalendarConfigurationServiceTest extends PowerMockito {

	@InjectMocks
	private DigitalPlaceCalendarConfigurationService digitalPlaceCalendarConfigurationService;

	@Mock
	private DigitalPlaceCalendarInstanceConfiguration mockConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void getDigitalPlaceCalendarConfiguration_WhenNoError_ThenReturnsCalendarConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DigitalPlaceCalendarInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);

		DigitalPlaceCalendarInstanceConfiguration result = digitalPlaceCalendarConfigurationService.getDigitalPlaceCalendarConfiguration(mockThemeDisplay);

		assertThat(result, sameInstance(mockConfiguration));
	}

}
