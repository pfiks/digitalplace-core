package com.placecube.digitalplace.payment.capita.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.payment.capita.service.internal.OneOffPaymentOrderFactory;
import com.placecube.digitalplace.payment.capita.service.internal.OneOffPaymentOrderFileService;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.service.PaymentService;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class CapitaPaymentConnectorTest extends PowerMockito {

	private static final int CAN_NUMBER = 202;
	private static final long COMPANY_ID = 10;
	private static final long USER_ID = 99;
	private static final String ONE_OFF_ORDER_BACK_URL = "ONE_OFF_ORDER_BACK_URL";
	private static final String ONE_OFF_ORDER_REDIRECT_URL = "ONE_OFF_ORDER_REDIRECT_URL";
	private static final String PAYMENT_ERROR = "PAYMENT_ERROR";
	private static final String PAYMENT_REFERENCE = "PAYMENT_REFERENCE";
	private static final String PAYMENT_SETUP_SUCCESSFUL = "PAYMENT_SETUP_SUCCESSFUL";

	@InjectMocks
	private CapitaPaymentConnector capitaPaymentConnector;

	@Mock
	private CapitaPaymentService mockCapitaPaymentService;

	@Mock
	private OneOffPaymentOrder mockOneOffPaymentOrder;

	@Mock
	private OneOffPaymentOrderFactory mockOneOffPaymentOrderFactory;

	@Mock
	private OneOffPaymentOrderFileService mockOneOffPaymentOrderFileService;

	@Mock
	private PaymentContext mockPaymentContext;

	@Mock
	private PaymentService mockPaymentService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockCapitaPaymentService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = capitaPaymentConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockCapitaPaymentService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = capitaPaymentConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getBackURL_WhenPaymentOrderIsNotPresent_ThenReturnEmptyOptional() {

		when(mockOneOffPaymentOrderFileService.loadPaymentTempFile(PAYMENT_REFERENCE, mockServiceContext)).thenReturn(Optional.empty());

		Optional<String> result = capitaPaymentConnector.getBackURL(PAYMENT_REFERENCE, mockServiceContext);

		assertFalse(result.isPresent());
	}

	@Test
	public void getBackURL_WhenPaymentOrderIsPresent_ThenReturnOptionalWithBackURL() {

		when(mockOneOffPaymentOrderFileService.loadPaymentTempFile(PAYMENT_REFERENCE, mockServiceContext)).thenReturn(Optional.of(mockOneOffPaymentOrder));
		when(mockOneOffPaymentOrder.getBackURL()).thenReturn(ONE_OFF_ORDER_BACK_URL);

		Optional<String> result = capitaPaymentConnector.getBackURL(PAYMENT_REFERENCE, mockServiceContext);

		assertThat(result.get(), equalTo(ONE_OFF_ORDER_BACK_URL));
	}

	@Test
	public void getDefaultAdvancedConfiguration_WhenException_ThenReturnsEmptyOptional() throws ConfigurationException {
		when(mockCapitaPaymentService.getDefaultAdvancedConfiguration(COMPANY_ID)).thenThrow(new ConfigurationException());

		Optional<String> result = capitaPaymentConnector.getDefaultAdvancedConfiguration(COMPANY_ID);

		assertFalse(result.isPresent());
	}

	@Test
	public void getDefaultAdvancedConfiguration_WhenNoError_ThenReturnsOptionalWithTheDefaultCOnfiguration() throws ConfigurationException {
		when(mockCapitaPaymentService.getDefaultAdvancedConfiguration(COMPANY_ID)).thenReturn(Optional.of("config1"));

		Optional<String> result = capitaPaymentConnector.getDefaultAdvancedConfiguration(COMPANY_ID);

		assertThat(result.get(), equalTo("config1"));
	}

	@Test
	public void getPaymentStatus_WhenPaymentIsNotSuccessful_ThenReturnStatusFromOneOffPaymentOrder() {
		when(mockOneOffPaymentOrderFileService.loadPaymentTempFile(PAYMENT_REFERENCE, mockServiceContext)).thenReturn(Optional.of(mockOneOffPaymentOrder));
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCapitaPaymentService.getPaymentService(mockServiceContext)).thenReturn(mockPaymentService);
		when(mockPaymentService.invokeQueryServiceOneOffPaymentOrder(mockOneOffPaymentOrder)).thenReturn(false);
		when(mockOneOffPaymentOrder.getStatus()).thenReturn(PaymentStatus.paymentError().getValue());

		PaymentStatus paymentStatus = capitaPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		String expected = PaymentStatus.paymentError().getValue();
		String actual = paymentStatus.getValue();

		assertEquals(expected, actual);
		verify(mockOneOffPaymentOrder, never()).getContinuousAuditNumber();
	}

	@Test
	public void getPaymentStatus_WhenPaymentIsSuccessfull_ThenReturnStatusSuccess() {
		when(mockOneOffPaymentOrderFileService.loadPaymentTempFile(PAYMENT_REFERENCE, mockServiceContext)).thenReturn(Optional.of(mockOneOffPaymentOrder));
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCapitaPaymentService.getPaymentService(mockServiceContext)).thenReturn(mockPaymentService);
		when(mockPaymentService.invokeQueryServiceOneOffPaymentOrder(mockOneOffPaymentOrder)).thenReturn(true);
		when(mockOneOffPaymentOrder.getContinuousAuditNumber()).thenReturn(CAN_NUMBER);

		PaymentStatus paymentStatus = capitaPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		String expected = PaymentStatus.success().getValue();
		String actual = paymentStatus.getValue();

		assertEquals(expected, actual);
		assertThat(paymentStatus.getPaymentUniqueReferenceNumber(), equalTo(String.valueOf(CAN_NUMBER)));
	}

	@Test
	public void getPaymentStatus_WhenPaymentOrderIsNotPresent_ThenReturnStatusAppError() {
		when(mockOneOffPaymentOrderFileService.loadPaymentTempFile(PAYMENT_REFERENCE, mockServiceContext)).thenReturn(Optional.empty());

		PaymentStatus paymentStatus = capitaPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		String expected = PaymentStatus.appError().getValue();
		String actual = paymentStatus.getValue();

		assertEquals(expected, actual);
	}

	@Test
	@Parameters({"true","false"})
	public void preparePayment_WhenPaymentError_ThenReturnPaymentResponseWithStatusPaymentError(boolean isImpersonated) {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCapitaPaymentService.getPaymentService(mockServiceContext)).thenReturn(mockPaymentService);
		when(mockOneOffPaymentOrderFactory.createOneOffPaymentOrder(mockPaymentContext)).thenReturn(mockOneOffPaymentOrder);
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockPaymentContext.getRealUserId(Optional.ofNullable(mockThemeDisplay))).thenReturn(USER_ID);
		when(mockPaymentContext.isImpersonated(Optional.ofNullable(mockThemeDisplay))).thenReturn(isImpersonated);
		when(mockPaymentService.invokePaymentService(mockOneOffPaymentOrder, USER_ID, isImpersonated)).thenReturn(PAYMENT_ERROR);

		PaymentResponse paymentResponse = capitaPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = paymentResponse.getRedirectURL();
		PaymentStatus actualStatus = paymentResponse.getStatus();

		assertEquals(StringPool.BLANK, actualRedirectURL);
		assertEquals(PaymentStatus.paymentError(), actualStatus);
	}

	@Test
	@Parameters({"true","false"})
	public void preparePayment_WhenPaymentSuccessful_ThenReturnPaymentResponseWithStatusPaymentSuccessful(boolean isImpersonated) throws PortalException, IOException {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCapitaPaymentService.getPaymentService(mockServiceContext)).thenReturn(mockPaymentService);
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockOneOffPaymentOrderFactory.createOneOffPaymentOrder(mockPaymentContext)).thenReturn(mockOneOffPaymentOrder);
		when(mockPaymentContext.getRealUserId(Optional.ofNullable(mockThemeDisplay))).thenReturn(USER_ID);
		when(mockPaymentContext.isImpersonated(Optional.ofNullable(mockThemeDisplay))).thenReturn(isImpersonated);
		when(mockPaymentService.invokePaymentService(mockOneOffPaymentOrder, USER_ID, isImpersonated)).thenReturn(PAYMENT_SETUP_SUCCESSFUL);
		when(mockOneOffPaymentOrder.getRedirectUrl()).thenReturn(ONE_OFF_ORDER_REDIRECT_URL);

		PaymentResponse paymentResponse = capitaPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = paymentResponse.getRedirectURL();
		PaymentStatus actualStatus = paymentResponse.getStatus();

		assertEquals(ONE_OFF_ORDER_REDIRECT_URL, actualRedirectURL);
		assertEquals(PaymentStatus.success(), actualStatus);
		Mockito.verify(mockOneOffPaymentOrderFileService, Mockito.times(1)).uploadPaymentTempFile(mockOneOffPaymentOrder, mockServiceContext);

	}
}
