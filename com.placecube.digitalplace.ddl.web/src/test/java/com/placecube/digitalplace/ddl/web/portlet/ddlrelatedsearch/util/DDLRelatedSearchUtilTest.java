package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListTemplateContentService;

public class DDLRelatedSearchUtilTest {

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private DynamicDataListTemplateContentService mockDynamicDataListTemplateContentService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DynamicDataListMapping mockDynamicDataListMapping;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue;

	@Mock
	private Value mockValue;

	@InjectMocks
	private DDLRelatedSearchUtil ddlRelatedSearchUtil;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void getDDLRecordTemplateString_WhenNoExceptionAndFoundMappingAndTemplate_ThenReturnsTemplateStringOptional() throws PortalException {
		long ddlRecordId = 54 ;
		long ddmStructureId = 32;
		String expectedTemplateString = "expectedTemplateString";
		List<DynamicDataListMapping> dynamicDataListMappings = Collections.singletonList(mockDynamicDataListMapping);

		when(mockDDLRecordLocalService.getDDLRecord(ddlRecordId)).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDynamicDataListMapping.getDDMStructureId()).thenReturn(ddmStructureId);
		when(mockDDMStructure.getStructureId()).thenReturn(ddmStructureId);
		when(mockDynamicDataListMapping.getSearchDefaultGridViewTemplate()).thenReturn(Optional.of(mockDDMTemplate));
		when(mockDynamicDataListTemplateContentService.getTransformedTemplate(ddlRecordId, mockDDMStructure, mockDDMTemplate, mockThemeDisplay, mockHttpServletRequest)).thenReturn(expectedTemplateString);

		Optional<String> result = ddlRelatedSearchUtil.getDDLRecordTemplateString(ddlRecordId, dynamicDataListMappings, mockThemeDisplay, mockHttpServletRequest);

		assertThat(result.get(), equalTo(expectedTemplateString));
	}

	@Test
	public void getDDLRecordTemplateString_WhenNoExceptionGettingDDLRecord_ThenReturnsEmptyOptional() throws PortalException {
		long ddlRecordId = 54 ;
		List<DynamicDataListMapping> dynamicDataListMappings = Collections.singletonList(mockDynamicDataListMapping);

		when(mockDDLRecordLocalService.getDDLRecord(ddlRecordId)).thenThrow(PortalException.class);

		Optional<String> result = ddlRelatedSearchUtil.getDDLRecordTemplateString(ddlRecordId, dynamicDataListMappings, mockThemeDisplay, mockHttpServletRequest);

		assertFalse(result.isPresent());
	}

	@Test
	public void getDDLRecordTemplateString_WhenNoExceptionGettingDDLRecordSet_ThenReturnsEmptyOptional() throws PortalException {
		long ddlRecordId = 54 ;
		List<DynamicDataListMapping> dynamicDataListMappings = Collections.singletonList(mockDynamicDataListMapping);

		when(mockDDLRecordLocalService.getDDLRecord(ddlRecordId)).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getRecordSet()).thenThrow(PortalException.class);

		Optional<String> result = ddlRelatedSearchUtil.getDDLRecordTemplateString(ddlRecordId, dynamicDataListMappings, mockThemeDisplay, mockHttpServletRequest);

		assertFalse(result.isPresent());
	}

	@Test
	public void getDDLRecordTemplateString_WhenNoExceptionGettingDDMStructure_ThenReturnsEmptyOptional() throws PortalException {
		long ddlRecordId = 54 ;
		List<DynamicDataListMapping> dynamicDataListMappings = Collections.singletonList(mockDynamicDataListMapping);

		when(mockDDLRecordLocalService.getDDLRecord(ddlRecordId)).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructure()).thenThrow(PortalException.class);

		Optional<String> result = ddlRelatedSearchUtil.getDDLRecordTemplateString(ddlRecordId, dynamicDataListMappings, mockThemeDisplay, mockHttpServletRequest);

		assertFalse(result.isPresent());
	}

	@Test
	public void getDDLRecordTitle_WhenDDLRecordHasTitleFieldCandidate_ThenReturnsFirstCandidateFieldStringValue() throws PortalException {
		List<DDMFormFieldValue> ddmFormFieldValues = new ArrayList<>(Arrays.asList(mockDDMFormFieldValue));
		String recordTitle = "recordTitle";
		Locale locale = new Locale("GB");

		when(mockDDLRecord.getDDMFormFieldValues("Title")).thenThrow(PortalException.class);
		when(mockDDLRecord.getDDMFormFieldValues("title")).thenReturn(ddmFormFieldValues);
		when(mockDDMFormFieldValue.getValue()).thenReturn(mockValue);
		when(mockValue.getDefaultLocale()).thenReturn(locale);
		when(mockValue.getString(locale)).thenReturn(recordTitle);

		String result = ddlRelatedSearchUtil.getDDLRecordTitle(mockDDLRecord);

		assertEquals(recordTitle, result);
	}

	@Test
	public void getDDLRecordTitle_WhenDDLRecordHasNoTitleFieldCandidate_ThenReturnsBlankString() throws PortalException {
		String recordTitle = "recordTitle";
		Locale locale = new Locale("GB");

		when(mockDDLRecord.getDDMFormFieldValues("Title")).thenThrow(PortalException.class);
		when(mockDDLRecord.getDDMFormFieldValues("title")).thenReturn(null);
		when(mockDDMFormFieldValue.getValue()).thenReturn(mockValue);
		when(mockValue.getDefaultLocale()).thenReturn(locale);
		when(mockValue.getString(locale)).thenReturn(recordTitle);

		String result = ddlRelatedSearchUtil.getDDLRecordTitle(mockDDLRecord);

		assertEquals(StringPool.BLANK, result);
	}
}
