package com.placecube.digitalplace.group.rest.application.filters;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.MockitoAnnotations.initMocks;

import java.time.Instant;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.pfiks.common.time.DateFormatter;
import com.placecube.digitalplace.group.rest.application.GroupRestApplication;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;
import com.placecube.digitalplace.group.rest.application.service.RequestFiltersService;

public class DateCheckerFilterTest extends PowerMockito {

	private DateCheckerFilter dateCheckerFilter;

	@Mock
	private GroupRestApplication mockGroupRestApplication;

	@Mock
	private DateFormatter mockDateFormatter;

	@Mock
	private RequestFiltersService mockRequestFiltersService;

	@Mock
	private ContainerRequestContext mockContainerRequestContext;

	@Before
	public void setUp() {
		initMocks(this);

		when(mockGroupRestApplication.getDateFormatter()).thenReturn(mockDateFormatter);
		when(mockGroupRestApplication.getRequestFiltersService()).thenReturn(mockRequestFiltersService);

		dateCheckerFilter = new DateCheckerFilter(mockGroupRestApplication);
	}

	@Test
	public void filter_WhenXDateHeaderHasDateWithin15MinutesFromNow_ThenNoExceptionIsThrown() throws Exception {
		Instant now = Instant.now();
		Instant headerTime = Instant.now().minusSeconds(60 * 14);
		String myDateValue = "myDateValue";
		when(mockDateFormatter.getInstantFromLongFormatString(myDateValue)).thenReturn(headerTime);
		when(mockDateFormatter.getNow()).thenReturn(now);
		when(mockRequestFiltersService.getDateHeader(mockContainerRequestContext)).thenReturn(myDateValue);

		try {
			dateCheckerFilter.filter(mockContainerRequestContext);
		} catch (Exception e) {
			fail();
		}
	}

	@Test(expected = GroupRestSecurityException.class)
	public void filter_WhenXDateHeaderHasDateWithinMoreThanMinutesAgo_ThenThrowsGroupSecurityException() throws Exception {
		Instant now = Instant.now();
		Instant headerTime = Instant.now().minusSeconds(60 * 16);
		String myDateValue = "myDateValueInvalid";
		when(mockDateFormatter.getInstantFromLongFormatString(myDateValue)).thenReturn(headerTime);
		when(mockDateFormatter.getNow()).thenReturn(now);
		when(mockRequestFiltersService.getDateHeader(mockContainerRequestContext)).thenReturn(myDateValue);

		dateCheckerFilter.filter(mockContainerRequestContext);
	}

	@Test
	public void filter_WhenXDateHeaderHasDateWithinMoreThanMinutesAgo_ThenThrowsGroupSecurityExceptionWithInvalidTimeStampMessageAndBadRequestStatus() throws Exception {
		Instant now = Instant.now();
		Instant headerTime = Instant.now().minusSeconds(60 * 16);
		String myDateValue = "myDateValueInvalid";
		when(mockDateFormatter.getInstantFromLongFormatString(myDateValue)).thenReturn(headerTime);
		when(mockDateFormatter.getNow()).thenReturn(now);
		when(mockRequestFiltersService.getDateHeader(mockContainerRequestContext)).thenReturn(myDateValue);

		try {
			dateCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			assertThat(e.getMessage(), equalTo(ErrorCodes.INVALID_TIMESTAMP.getCode()));
			assertThat(e.getResponse().getStatusInfo(), equalTo(Response.Status.BAD_REQUEST));
		}
	}

}
