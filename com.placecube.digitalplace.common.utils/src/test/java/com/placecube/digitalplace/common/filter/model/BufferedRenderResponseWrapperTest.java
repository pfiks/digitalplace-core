package com.placecube.digitalplace.common.filter.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.petra.string.StringPool;

public class BufferedRenderResponseWrapperTest {

	private BufferedRenderResponseWrapper bufferedRenderResponseWrapper;

	@Mock
	private OutputStream mockOutputStream;

	@Mock
	private RenderResponse mockRenderResponse;

	@Test(expected = IllegalStateException.class)
	public void getOutputStream_WhenGetWriterHasAlreadyBeenCalled_ThenThrowsException() throws IOException {

		bufferedRenderResponseWrapper.getWriter();

		bufferedRenderResponseWrapper.getOutputStream();

	}

	@Test
	public void getOutputStream_WhenGetWriterHasNotBeenCalled_ThenReturnsOutputStream() throws IOException {

		when(mockRenderResponse.getPortletOutputStream()).thenReturn(mockOutputStream);

		OutputStream outputStream = bufferedRenderResponseWrapper.getOutputStream();

		assertThat(outputStream, sameInstance(mockOutputStream));

	}

	@Test(expected = IllegalStateException.class)
	public void getWriter_WhenGetOutpuStreamHasAlreadyBeenCalled_ThenThrowsException() throws IOException {

		bufferedRenderResponseWrapper.getOutputStream();

		bufferedRenderResponseWrapper.getWriter();

	}

	@Test
	public void getWriter_WhenGetOutpuStreamHasNotBeenCalled_ThenReturnsWriter() {

		PrintWriter printWriter = bufferedRenderResponseWrapper.getWriter();

		assertThat(printWriter, instanceOf(PrintWriter.class));

	}

	@Before
	public void setUp() {

		initMocks(this);

		bufferedRenderResponseWrapper = new BufferedRenderResponseWrapper(mockRenderResponse);

	}

	@Test
	public void toString_WhenGetPrintWriterHasBeenCalled_ThenReturnsString() {

		String testText = "Test text";
		PrintWriter printWriter = bufferedRenderResponseWrapper.getWriter();
		printWriter.write(testText);

		String string = bufferedRenderResponseWrapper.toString();

		assertThat(string, equalTo(testText));

	}

	@Test
	public void toString_WhenGetPrintWriterHasNotBeenCalled_ThenReturnsBlank() {

		String string = bufferedRenderResponseWrapper.toString();

		assertThat(string, equalTo(StringPool.BLANK));

	}

}