/**
 * AcceptedCards.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class AcceptedCards  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.AcceptedCardType[] includes;

    private com.capita_software_services.www.scp.base.AcceptedCardType[] excludes;

    public AcceptedCards() {
    }

    public AcceptedCards(
           com.capita_software_services.www.scp.base.AcceptedCardType[] includes,
           com.capita_software_services.www.scp.base.AcceptedCardType[] excludes) {
           this.includes = includes;
           this.excludes = excludes;
    }


    /**
     * Gets the includes value for this AcceptedCards.
     *
     * @return includes
     */
    public com.capita_software_services.www.scp.base.AcceptedCardType[] getIncludes() {
        return includes;
    }


    /**
     * Sets the includes value for this AcceptedCards.
     *
     * @param includes
     */
    public void setIncludes(com.capita_software_services.www.scp.base.AcceptedCardType[] includes) {
        this.includes = includes;
    }


    /**
     * Gets the excludes value for this AcceptedCards.
     *
     * @return excludes
     */
    public com.capita_software_services.www.scp.base.AcceptedCardType[] getExcludes() {
        return excludes;
    }


    /**
     * Sets the excludes value for this AcceptedCards.
     *
     * @param excludes
     */
    public void setExcludes(com.capita_software_services.www.scp.base.AcceptedCardType[] excludes) {
        this.excludes = excludes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AcceptedCards)) return false;
        AcceptedCards other = (AcceptedCards) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.includes==null && other.getIncludes()==null) ||
             (this.includes!=null &&
              java.util.Arrays.equals(this.includes, other.getIncludes()))) &&
            ((this.excludes==null && other.getExcludes()==null) ||
             (this.excludes!=null &&
              java.util.Arrays.equals(this.excludes, other.getExcludes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIncludes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIncludes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIncludes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getExcludes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExcludes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExcludes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AcceptedCards.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "acceptedCards"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "includes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "acceptedCardType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "card"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("excludes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "excludes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "acceptedCardType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "card"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
