package com.placecube.digitalplace.payment.govuk.service.commerce;

import static com.placecube.digitalplace.payment.constants.PaymentConstants.API_AUTH_KEY;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.commerce.constants.CommerceOrderPaymentConstants;
import com.liferay.commerce.constants.CommercePaymentMethodConstants;
import com.liferay.commerce.model.CommerceAddress;
import com.liferay.commerce.model.CommerceOrder;
import com.liferay.commerce.model.CommerceOrderItem;
import com.liferay.commerce.model.CommerceSubscriptionEntry;
import com.liferay.commerce.payment.method.CommercePaymentMethod;
import com.liferay.commerce.payment.request.CommercePaymentRequest;
import com.liferay.commerce.payment.result.CommercePaymentResult;
import com.liferay.commerce.service.CommerceOrderLocalService;
import com.liferay.commerce.service.CommerceOrderPaymentLocalService;
import com.liferay.commerce.service.CommerceSubscriptionEntryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.settings.GroupServiceSettingsLocator;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.govuk.service.commerce.configuration.GovUKPayGroupServiceConfiguration;
import com.placecube.digitalplace.payment.govuk.service.internal.GovUKPaymentOrderFactory;
import com.placecube.digitalplace.payment.model.AgreementStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;
import com.placecube.digitalplace.payment.service.PaymentConnector;

@Component(property = "commerce.payment.engine.method.key=" + GovUKCommerceConstants.PAYMENT_ENGINE_KEY, service = CommercePaymentMethod.class)
public class GovUKCommercePaymentMethod implements CommercePaymentMethod {

	private static final Log LOG = LogFactoryUtil.getLog(GovUKCommercePaymentMethod.class);

	private static ServiceContext getServiceContext(CommerceOrder commerceOrder) {
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		if (Validator.isNull(serviceContext)) {
			serviceContext = new ServiceContext();
			serviceContext.setCompanyId(commerceOrder.getCompanyId());
		}
		return serviceContext;
	}

	@Reference
	private CommerceOrderLocalService commerceOrderLocalService;

	@Reference
	private CommerceOrderPaymentLocalService commerceOrderPaymentLocalService;

	@Reference
	private CommerceSubscriptionEntryLocalService commerceSubscriptionEntryLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference(target = "(" + PaymentConnector.PAYMENT_CONNECTOR_KEY + "=" + GovUKCommerceConstants.PAYMENT_ENGINE_KEY + ")")
	private PaymentConnector govUKPaymentConnector;

	@Reference
	private GovUKPaymentOrderFactory govUKPaymentOrderFactory;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public CommercePaymentResult cancelPayment(CommercePaymentRequest commercePaymentRequest) {

		return new CommercePaymentResult(commercePaymentRequest.getTransactionId(), commercePaymentRequest.getCommerceOrderId(), CommerceOrderPaymentConstants.STATUS_CANCELLED, false, null, null,
				Collections.emptyList(), true);

	}

	@Override
	public boolean cancelRecurringPayment(CommercePaymentRequest commercePaymentRequest) throws Exception {
		long commerceOrderId = commercePaymentRequest.getCommerceOrderId();

		CommerceOrder commerceOrder = commerceOrderLocalService.getCommerceOrder(commerceOrderId);
		ServiceContext serviceContext = getServiceContext(commerceOrder);

		GovUKPayGroupServiceConfiguration configuration = configurationProvider.getConfiguration(GovUKPayGroupServiceConfiguration.class,
				new GroupServiceSettingsLocator(commerceOrder.getGroupId(), GovUKCommerceConstants.SERVICE_NAME));
		serviceContext.setAttribute(API_AUTH_KEY, configuration.keyName());

		String agreementId = GetterUtil.getString(commerceOrder.getExpandoBridge().getAttribute(GovUKPayConstants.AGREEMENT_ID_EXPANDO_COLUMN_NAME, false));

		return govUKPaymentConnector.cancelAgreement(agreementId, serviceContext);
	}

	@Override
	public CommercePaymentResult completePayment(CommercePaymentRequest commercePaymentRequest) throws Exception {
		return getCommercePaymentResultAndVerifyPaymentStatus(commercePaymentRequest);
	}

	@Override
	public CommercePaymentResult completeRecurringPayment(CommercePaymentRequest commercePaymentRequest) throws Exception {
		return getCommercePaymentResultAndVerifyPaymentStatus(commercePaymentRequest);
	}

	@Override
	public String getDescription(Locale locale) {
		return getLabel(locale, "commerce-payment-govuk-description");
	}

	@Override
	public String getKey() {
		return GovUKCommerceConstants.PAYMENT_ENGINE_KEY;
	}

	@Override
	public String getName(Locale locale) {
		return getLabel(locale, "commerce-payment-govuk-name");
	}

	@Override
	public int getPaymentType() {
		return CommercePaymentMethodConstants.TYPE_ONLINE_REDIRECT;
	}

	@Override
	public String getServletPath() {
		return GovUKCommerceConstants.SERVLET_PATH;
	}

	@Override
	public boolean getSubscriptionValidity(CommercePaymentRequest commercePaymentRequest) throws Exception {
		long commerceOrderId = commercePaymentRequest.getCommerceOrderId();
		BigDecimal amount = commercePaymentRequest.getAmount();

		CommerceOrder commerceOrder = commerceOrderLocalService.getCommerceOrder(commerceOrderId);
		ServiceContext serviceContext = getServiceContext(commerceOrder);

		String agreementId = GetterUtil.getString(commerceOrder.getExpandoBridge().getAttribute(GovUKPayConstants.AGREEMENT_ID_EXPANDO_COLUMN_NAME, false));

		GovUKPayGroupServiceConfiguration configuration = configurationProvider.getConfiguration(GovUKPayGroupServiceConfiguration.class,
				new GroupServiceSettingsLocator(commerceOrder.getGroupId(), GovUKCommerceConstants.SERVICE_NAME));
		serviceContext.setAttribute(API_AUTH_KEY, configuration.keyName());
		AgreementStatus agreementStatus = govUKPaymentConnector.getAgreementStatus(agreementId, serviceContext);
		String status = agreementStatus.getStatus();

		List<CommerceOrderItem> commerceOrderItems = commerceOrder.getCommerceOrderItems();
		if (commerceOrderItems.size() != 1) {
			throw new PortalException("Subscription order with multiple CommerceOrderItems - this should not be the case. CommerceOrderId: " + commerceOrderId);
		}
		CommerceOrderItem commerceOrderItem = commerceOrderItems.get(0);
		CommerceSubscriptionEntry commerceSubscriptionEntry = commerceSubscriptionEntryLocalService.fetchCommerceSubscriptionEntryByCommerceOrderItemId(commerceOrderItem.getCommerceOrderItemId());
		Date now = new Date();

		String idempotencyDataString = GetterUtil.getString(commerceOrder.getExpandoBridge().getAttribute(GovUKPayConstants.PAYMENT_IDEMPOTENCY_DATA_EXPANDO_COLUMN_NAME, false));
		JSONObject idempotencyDataJSON = JSONFactoryUtil.createJSONObject(idempotencyDataString);
		String idempotencyKey = idempotencyDataJSON.getString("idempotencyKey");
		long lastIdempotencyKeyGenerationDateInMillis = idempotencyDataJSON.getLong("lastIdempotencyKeyGenerationDateInMillis");
		if (lastIdempotencyKeyGenerationDateInMillis > 0) {
			Date lastIdempotencyKeyGenerationDate = DateUtil.newDate(lastIdempotencyKeyGenerationDateInMillis);
			Calendar lastIdempotencyKeyGenerationCalendar = Calendar.getInstance();
			lastIdempotencyKeyGenerationCalendar.setTime(lastIdempotencyKeyGenerationDate);
			Calendar nowCalendar = Calendar.getInstance();
			nowCalendar.setTime(now);

			if (nowCalendar.get(Calendar.DAY_OF_YEAR) > lastIdempotencyKeyGenerationCalendar.get(Calendar.DAY_OF_YEAR)) {
				idempotencyKey = generateNewIdempotencyKey(idempotencyDataJSON, now, commerceOrder);
			}
		} else {
			idempotencyKey = generateNewIdempotencyKey(idempotencyDataJSON, now, commerceOrder);
		}

		if (GovUKPayConstants.STATUS_ACTIVE.equals(status)) {
			if (!now.after(commerceSubscriptionEntry.getNextIterationDate())) {
				return true;
			}
			CommerceAddress billingAddress = commerceOrder.getBillingAddress();
			AddressContext addressContext = new AddressContext(null, billingAddress.getStreet1(), billingAddress.getStreet2(), billingAddress.getStreet3(), StringPool.BLANK, billingAddress.getCity(),
					billingAddress.getZip());
			PaymentContext paymentContext = new PaymentContext("", getSalesDescription(commerceOrder), "", "", commerceOrder.getExternalReferenceCode(), amount, commerceOrder.getUserName(), "", "",
					addressContext, "", "");
			PaymentResponse paymentResponse = govUKPaymentConnector.takeRecurringPayment(paymentContext, agreementId, idempotencyKey, serviceContext);

			if (PaymentStatus.created().getValue().equals(paymentResponse.getStatus().getValue())) {
				commerceOrderPaymentLocalService.addCommerceOrderPayment(commerceOrderId, CommerceOrderPaymentConstants.STATUS_AUTHORIZED, StringPool.BLANK);

				return true;
			}
			if (PaymentStatus.success().getValue().equals(paymentResponse.getStatus().getValue())) {
				LOG.debug("Repeated payment during the same day, avoiding double charge");
				return true;
			}
			commerceOrderPaymentLocalService.addCommerceOrderPayment(commerceOrderId, CommerceOrderPaymentConstants.STATUS_FAILED,
					"Payment failed with active agreement. Cause: " + paymentResponse.getStatus().getResultMessage());
		} else {
			if (now.after(commerceSubscriptionEntry.getNextIterationDate())) {
				commerceOrderPaymentLocalService.addCommerceOrderPayment(commerceOrderId, CommerceOrderPaymentConstants.STATUS_FAILED, "Inactive agreement");
			}
		}

		return false;
	}

	@Override
	public boolean isCancelEnabled() {
		return true;
	}

	@Override
	public boolean isCompleteEnabled() {
		return true;
	}

	@Override
	public boolean isCompleteRecurringEnabled() {
		return true;
	}

	@Override
	public boolean isProcessPaymentEnabled() {
		return true;
	}

	@Override
	public boolean isProcessRecurringEnabled() {
		return true;
	}

	@Override
	public CommercePaymentResult processPayment(CommercePaymentRequest commercePaymentRequest) throws Exception {

		BigDecimal amount = commercePaymentRequest.getAmount();
		String returnUrl = commercePaymentRequest.getReturnUrl();
		String cancelUrl = commercePaymentRequest.getCancelUrl();
		String backUrl = cancelUrl;
		long commerceOrderId = commercePaymentRequest.getCommerceOrderId();
		String transactionId = commercePaymentRequest.getTransactionId();

		CommerceOrder commerceOrder = commerceOrderLocalService.getCommerceOrder(commerceOrderId);
		CommerceAddress billingAddress = commerceOrder.getBillingAddress();

		long userId = commerceOrder.getUserId();
		String email = StringPool.BLANK;
		if (userId > 0) {
			try {
				email = userLocalService.getUser(userId).getEmailAddress();
			} catch (PortalException exception) {
				LOG.error("User with id " + userId + " no longer exist");
			}
		}

		AddressContext addressContext = new AddressContext(null, billingAddress.getStreet1(), billingAddress.getStreet2(), billingAddress.getStreet3(), StringPool.BLANK, billingAddress.getCity(),
				billingAddress.getZip());
		PaymentContext paymentContext = new PaymentContext("", getSalesDescription(commerceOrder), "", "", commerceOrder.getExternalReferenceCode(), amount, commerceOrder.getUserName(), "", email,
				addressContext, returnUrl, backUrl);

		ServiceContext serviceContext = getServiceContext(commerceOrder);
		GovUKPayGroupServiceConfiguration configuration = configurationProvider.getConfiguration(GovUKPayGroupServiceConfiguration.class,
				new GroupServiceSettingsLocator(commerceOrder.getGroupId(), GovUKCommerceConstants.SERVICE_NAME));
		serviceContext.setAttribute(API_AUTH_KEY, configuration.keyName());
		PaymentResponse paymentResponse = govUKPaymentConnector.preparePayment(paymentContext, serviceContext);
		String paymentId = govUKPaymentOrderFactory.getPaymentIdFromSession(serviceContext.getRequest());

		return new CommercePaymentResult(paymentId, commercePaymentRequest.getCommerceOrderId(), CommerceOrderPaymentConstants.STATUS_AUTHORIZED, true, paymentResponse.getRedirectURL(), transactionId,
				Collections.emptyList(), true);

	}

	@Override
	public CommercePaymentResult processRecurringPayment(CommercePaymentRequest commercePaymentRequest) throws Exception {

		BigDecimal amount = commercePaymentRequest.getAmount();
		String returnUrl = commercePaymentRequest.getReturnUrl();
		String cancelUrl = commercePaymentRequest.getCancelUrl();
		String backUrl = cancelUrl;
		long commerceOrderId = commercePaymentRequest.getCommerceOrderId();
		String transactionId = commercePaymentRequest.getTransactionId();

		CommerceOrder commerceOrder = commerceOrderLocalService.getCommerceOrder(commerceOrderId);
		CommerceAddress billingAddress = commerceOrder.getBillingAddress();

		long userId = commerceOrder.getUserId();
		String email = StringPool.BLANK;
		String userExternalRef = StringPool.BLANK;
		if (userId > 0) {
			try {
				User user = userLocalService.getUser(userId);
				email = user.getEmailAddress();
				userExternalRef = user.getExternalReferenceCode();
			} catch (PortalException exception) {
				LOG.error("User with id " + userId + " no longer exist");
			}
		}

		AddressContext addressContext = new AddressContext(null, billingAddress.getStreet1(), billingAddress.getStreet2(), billingAddress.getStreet3(), StringPool.BLANK, billingAddress.getCity(),
				billingAddress.getZip());
		PaymentContext paymentContext = new PaymentContext(userExternalRef, getSalesDescription(commerceOrder), "", "", commerceOrder.getExternalReferenceCode(), amount, commerceOrder.getUserName(),
				"", email, addressContext, returnUrl, backUrl);

		ServiceContext serviceContext = getServiceContext(commerceOrder);
		GovUKPayGroupServiceConfiguration configuration = configurationProvider.getConfiguration(GovUKPayGroupServiceConfiguration.class,
				new GroupServiceSettingsLocator(commerceOrder.getGroupId(), GovUKCommerceConstants.SERVICE_NAME));
		serviceContext.setAttribute(API_AUTH_KEY, configuration.keyName());

		PaymentResponse paymentResponse = govUKPaymentConnector.prepareRecurringPayment(paymentContext, serviceContext);
		if (GovUKPayConstants.STATUS_CREATED.equals(paymentResponse.getAgreementStatus().getStatus())) {
			commerceOrder.getExpandoBridge().setAttribute(GovUKPayConstants.AGREEMENT_ID_EXPANDO_COLUMN_NAME, paymentResponse.getAgreementStatus().getAgreementId());

			String paymentId = govUKPaymentOrderFactory.getPaymentIdFromSession(serviceContext.getRequest());

			return new CommercePaymentResult(paymentId, commercePaymentRequest.getCommerceOrderId(), CommerceOrderPaymentConstants.STATUS_AUTHORIZED, true, paymentResponse.getRedirectURL(),
					transactionId, Collections.emptyList(), true);
		}
		return new CommercePaymentResult(transactionId, commercePaymentRequest.getCommerceOrderId(), CommerceOrderPaymentConstants.STATUS_FAILED, true, null, null, new ArrayList<>(), false);

	}

	private String generateNewIdempotencyKey(JSONObject idempotencyDataJSON, Date now, CommerceOrder commerceOrder) {
		String idempotencyKey = PortalUUIDUtil.generate();
		idempotencyDataJSON.put("idempotencyKey", idempotencyKey);
		idempotencyDataJSON.put("lastIdempotencyKeyGenerationDateInMillis", now.getTime());
		commerceOrder.getExpandoBridge().setAttribute(GovUKPayConstants.PAYMENT_IDEMPOTENCY_DATA_EXPANDO_COLUMN_NAME, idempotencyDataJSON.toJSONString());
		return idempotencyKey;
	}

	private CommercePaymentResult getCommercePaymentResultAndVerifyPaymentStatus(CommercePaymentRequest commercePaymentRequest) throws PortalException {
		CommerceOrder commerceOrder = commerceOrderLocalService.getCommerceOrder(commercePaymentRequest.getCommerceOrderId());
		ServiceContext serviceContext = getServiceContext(commerceOrder);
		GovUKPayGroupServiceConfiguration configuration = configurationProvider.getConfiguration(GovUKPayGroupServiceConfiguration.class,
				new GroupServiceSettingsLocator(commerceOrder.getGroupId(), GovUKCommerceConstants.SERVICE_NAME));
		serviceContext.setAttribute(API_AUTH_KEY, configuration.keyName());

		PaymentStatus paymentStatus = govUKPaymentConnector.getPaymentStatus("paymentReference", serviceContext);
		String paymentId = commercePaymentRequest.getTransactionId();
		if (PaymentStatus.success().getValue().equals(paymentStatus.getValue())) {
			paymentId = govUKPaymentOrderFactory.getPaymentIdFromSession(serviceContext.getRequest());

			return new CommercePaymentResult(paymentId, commercePaymentRequest.getCommerceOrderId(), CommerceOrderPaymentConstants.STATUS_COMPLETED, false, null, null,
					Collections.singletonList("Liferay generated complete message"), true);
		}
		List<String> resultMessages = new ArrayList<>();
		if (Validator.isNotNull(paymentStatus.getResultMessage())) {
			resultMessages.add(paymentStatus.getResultMessage());
		}
		return new CommercePaymentResult(paymentId, commercePaymentRequest.getCommerceOrderId(), CommerceOrderPaymentConstants.STATUS_FAILED, true, null, null, resultMessages, false);
	}

	private String getLabel(Locale locale, String messageKey) {
		ResourceBundle resourceBundle = ResourceBundleUtil.getBundle("content.Language", locale, getClass());
		return LanguageUtil.get(resourceBundle, messageKey);
	}

	private String getSalesDescription(CommerceOrder commerceOrder) {
		return "Order reference code: " + commerceOrder.getExternalReferenceCode();
	}

}
