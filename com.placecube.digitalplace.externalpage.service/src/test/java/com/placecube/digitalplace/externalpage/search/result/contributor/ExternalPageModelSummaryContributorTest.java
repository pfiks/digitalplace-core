package com.placecube.digitalplace.externalpage.search.result.contributor;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Summary;

public class ExternalPageModelSummaryContributorTest extends PowerMockito {

	@InjectMocks
	private ExternalPageModelSummaryContributor externalPageModelSummaryContributor;

	@Mock
	private Document mockDocument;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void getSummary_WhenNoError_ThenReturnsTheConfiguredSummary() {
		String title = "titleValue";
		String content = "contentValue";
		when(mockDocument.get(Field.TITLE)).thenReturn(title);
		when(mockDocument.get(Field.CONTENT)).thenReturn(content);

		Summary result = externalPageModelSummaryContributor.getSummary(mockDocument, Locale.CANADA_FRENCH, "snippet");

		assertThat(result.getTitle(), equalTo(title));
		assertThat(result.getContent(), equalTo(content));
		assertThat(result.getMaxContentLength(), equalTo(200));
	}

}
