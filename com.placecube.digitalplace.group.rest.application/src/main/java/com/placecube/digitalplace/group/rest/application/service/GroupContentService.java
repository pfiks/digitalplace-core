package com.placecube.digitalplace.group.rest.application.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetTag;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.AssetTagLocalService;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.dynamic.data.lists.constants.DDLRecordConstants;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordService;
import com.liferay.dynamic.data.lists.service.DDLRecordSetService;
import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.constants.SearchContextAttributes;
import com.pfiks.common.permission.PermissionUtil;
import com.placecube.digitalplace.ddl.constants.DDLIndexerField;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;
import com.placecube.digitalplace.group.rest.application.model.FileAttachment;

@Component(immediate = true, service = GroupContentService.class)
public class GroupContentService {

	private static final Log LOG = LogFactoryUtil.getLog(GroupContentService.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private AssetTagLocalService assetTagLocalService;

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDLContentParsingService ddlContentParsingService;

	@Reference
	private DDLRecordService ddlRecordService;

	@Reference(target = "(model.class.name=com.liferay.dynamic.data.lists.model.DDLRecordSet)")
	private ModelResourcePermission<DDLRecordSet> ddlRecordSetModelResourcePermission;

	@Reference
	private DDLRecordSetService ddlRecordSetService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private GroupContentSearchHelper groupContentSearchHelper;

	@Reference
	private GroupFileService groupFileService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private PermissionUtil permissionUtil;

	@Reference
	private Portal portal;

	@Reference
	private UserLocalService userLocalService;

	public DDLRecord addDDLRecord(long userId, long groupId, DDLRecordSet ddlRecordSet, String ddlRecordJSON, Set<FileAttachment> uploadedAttachments, long[] categoryIds, String[] tagNames,
			ServiceContext serviceContext) throws Exception {
		try {
			DDMStructure ddmStructure = ddlRecordSet.getDDMStructure();
			DDMForm ddmForm = ddmStructure.getFullHierarchyDDMForm();

			String ddlRecordJSONWithInstanceIds = ddlContentParsingService.addInstanceIdsToDDLRecordJSON(ddlRecordJSON);
			String ddlRecordJSONToUse = ddlContentParsingService.getContentWithAttachmentDetails(ddlRecordJSONWithInstanceIds, uploadedAttachments);

			LOG.info("Adding record with json: " + ddlRecordJSONToUse);

			DDMFormValues ddmFormValues = ddlContentParsingService.deserializeDDLRecordJSON(ddlRecordJSONToUse, ddmForm);

			serviceContext.setAssetCategoryIds(categoryIds);
			serviceContext.setAssetTagNames(tagNames);

			return ddlRecordService.addRecord(groupId, ddlRecordSet.getRecordSetId(), DDLRecordConstants.DISPLAY_INDEX_DEFAULT, ddmFormValues, serviceContext);
		} catch (Exception e) {
			groupFileService.rollbackAttachmentsCreation(uploadedAttachments);
			throw e;
		}
	}

	public void configureSearchContextFiltersAndSorts(SearchContext searchContext, boolean sortByRating, boolean sameFilterConditionsInOr, Long[] categoryIds, String[] tagNames,
			Map<String, List<String>> customFieldsValues) {

		BooleanClauseOccur queryOperator = sameFilterConditionsInOr ? BooleanClauseOccur.SHOULD : BooleanClauseOccur.MUST;

		if (ArrayUtil.isNotEmpty(categoryIds)) {
			List<String> values = Arrays.stream(categoryIds).map(String::valueOf).collect(Collectors.toList());
			groupContentSearchHelper.addBooleanQueryForMultipleValues(searchContext, Field.ASSET_CATEGORY_IDS, values, queryOperator);
		}

		if (ArrayUtil.isNotEmpty(tagNames)) {
			List<String> values = Arrays.asList(tagNames);
			groupContentSearchHelper.addBooleanQueryForMultipleValues(searchContext, Field.ASSET_TAG_NAMES, values, queryOperator);
		}

		if (!customFieldsValues.isEmpty()) {
			for (Entry<String, List<String>> customField : customFieldsValues.entrySet()) {
				groupContentSearchHelper.addBooleanQueryForMultipleValues(searchContext, customField.getKey(), customField.getValue(), queryOperator);
			}
		}

		searchContext.setSorts(groupContentSearchHelper.getSortByOptions(sortByRating));
	}

	public void configureSearchContextWithCreatorUserId(SearchContext searchContext, Long creatorUserId) {
		if (Validator.isNotNull(creatorUserId) && creatorUserId > 0) {
			groupContentSearchHelper.addBooleanQuery(searchContext, Field.USER_ID, String.valueOf(creatorUserId), BooleanClauseOccur.MUST);
		}
	}

	public Hits executeSearch(SearchContext searchContext, Integer startPage, Integer resultsPerPage) throws SearchException {
		int start = groupContentSearchHelper.evaluateStartResultBasedOnCurrentPageAndDelta(startPage, resultsPerPage);
		return groupContentSearchHelper.executeSearch(searchContext, start, resultsPerPage);
	}

	public User fetchUser(Group group, String emailAddress) {
		return userLocalService.fetchUserByEmailAddress(group.getCompanyId(), emailAddress);
	}

	public DDLRecordSet getDDLRecordSet(long ddlRecordSetId) throws PortalException {
		return ddlRecordSetService.getRecordSet(ddlRecordSetId);
	}

	public List<DDLRecordSet> getDDLRecordSets(Group group) {
		return ddlRecordSetService.getRecordSets(new long[] { group.getGroupId() });
	}

	public List<DDMStructure> getDDLStructures(Group group) throws PortalException {
		long globalGroupId = groupLocalService.getCompanyGroup(group.getCompanyId()).getGroupId();
		Long[] groupIds = new Long[] { group.getGroupId(), globalGroupId };
		long classNameId = portal.getClassNameId(DDLRecordSet.class);

		DynamicQuery dynamicQuery = ddmStructureLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", group.getCompanyId()));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("classNameId", classNameId));
		dynamicQuery.add(RestrictionsFactoryUtil.in("groupId", Arrays.asList(groupIds)));

		return ddmStructureLocalService.dynamicQuery(dynamicQuery, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	public List<AssetCategory> getGroupCategories(Group group) {
		DynamicQuery dynamicQuery = assetCategoryLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", group.getGroupId()));
		return assetCategoryLocalService.dynamicQuery(dynamicQuery);
	}

	public List<AssetTag> getGroupContentTags(Group group) {
		return assetTagLocalService.getGroupTags(group.getGroupId());
	}

	public List<AssetVocabulary> getGroupVocabularies(Group group) throws PortalException {
		return assetVocabularyLocalService.getGroupVocabularies(group.getGroupId());
	}

	public String getPortalURL(Group group) throws PortalException {
		Company company = companyLocalService.getCompany(group.getCompanyId());
		return company.getPortalURL(company.getGroupId());
	}

	public SearchContext getSearchContextForAdminDynamicDataListSearch(long adminUserId, Group group, long structureId) {

		SearchContext searchContext = groupContentSearchHelper.getBaseSearchContext(group, adminUserId);

		searchContext.setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, true);

		if (structureId > 0) {
			groupContentSearchHelper.addBooleanQuery(searchContext, DDLIndexerField.DDM_STRUCTURE_ID, String.valueOf(structureId), BooleanClauseOccur.MUST);
		}

		return searchContext;
	}

	public ServiceContext getServiceContextAndRunAsUser(Group group, long userId) throws Exception {
		permissionUtil.runAsUser(userId);
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setUserId(userId);
		serviceContext.setScopeGroupId(group.getGroupId());
		return serviceContext;
	}

	public List<DDMFormField> getStructureFieldsWithPreSetValues(long structureId) throws PortalException {
		List<DDMFormField> ddmFormFields = ddmStructureLocalService.getStructure(structureId).getDDMForm().getDDMFormFields();
		return ddmFormFields.stream().filter(this::validFieldType).collect(Collectors.toList());
	}

	public Map<String, List<String>> parseCustomFieldParameters(long structureId, String[] customFields) {
		Map<String, List<String>> results = new HashMap<>();

		try {
			DDMStructure structure = ddmStructureLocalService.getStructure(structureId);
			String defaultLanguageId = structure.getDefaultLanguageId();
			if (ArrayUtil.isNotEmpty(customFields)) {
				for (String customField : customFields) {
					addCustomFieldToFilters(results, structureId, defaultLanguageId, customField);
				}
			}
		} catch (PortalException e) {
			LOG.warn(e);
		}

		return results;
	}

	public long runAsAdministrator(long companyId) throws Exception {
		permissionUtil.runAsCompanyAdmin(companyId);
		return PermissionThreadLocal.getPermissionChecker().getUserId();
	}

	public void updateDDLRecordAssetEntries(long userId, DDLRecord ddlRecord, long[] categoryIds, String[] tagNames) throws Exception {
		permissionUtil.runAsUser(userId);

		if (!isDDLRecordCreator(userId, ddlRecord) && !hasUpdatePermissionOnDDLRecord(ddlRecord.getRecordSetId())) {
			throw new PortalException("User userId: " + userId + " does not have update permission for ddlRecordId: " + ddlRecord.getRecordId());
		}
		assetEntryLocalService.updateEntry(userId, ddlRecord.getGroupId(), DDLRecord.class.getName(), ddlRecord.getRecordId(), categoryIds, tagNames);
	}

	public Set<FileAttachment> uploadDDLAttachments(Group group, String fileAttachments, DDLRecordSet ddlRecordSet, ServiceContext serviceContext) throws PortalException {
		Set<FileAttachment> results = new LinkedHashSet<>();

		JSONArray jsonArray = ddlContentParsingService.getSafeJSONArray(fileAttachments);

		if (jsonArray.length() > 0) {
			ServiceContext fileServiceContext = (ServiceContext) serviceContext.clone();
			fileServiceContext.setModelPermissions(groupFileService.getModelPermissions(ddlRecordSet));
			fileServiceContext.setAttribute("actionKey", ActionKeys.ADD_ATTACHMENT);

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject fileJsonObject = jsonArray.getJSONObject(i);
				DLFileEntry dlFileEntry = groupFileService.createDLFileEntry(fileJsonObject, fileServiceContext);
				results.add(FileAttachment.init(fileJsonObject.getString("placeholderKey"), dlFileEntry, ddlContentParsingService.getJSONObjectForAttachment(dlFileEntry)));
			}
		}
		return results;
	}

	public void validateParameters(Integer startPage, Integer resultsPerPage, Integer truncateDescription) {
		if (failNegative(startPage) || failNotPositive(resultsPerPage) || failNotPositive(truncateDescription)) {
			throw new GroupRestSecurityException(ErrorCodes.INVALID_PARAMETERS, Response.Status.BAD_REQUEST);
		}
	}

	private void addCustomFieldToFilters(Map<String, List<String>> results, long structureId, String defaultLanguageId, String customField) {
		try {
			int indexOf = customField.indexOf("=");
			String fieldName = customField.substring(0, indexOf);
			String fieldValue = StringUtil.trim(customField.substring(indexOf + 1));
			if (Validator.isNotNull(fieldName) && Validator.isNotNull(fieldValue)) {
				String structureFieldName = "ddm__keyword__" + structureId + "__" + fieldName + "_" + defaultLanguageId;
				List<String> fieldValues = results.getOrDefault(structureFieldName, new ArrayList<>());
				fieldValues.add(fieldValue);
				results.put(structureFieldName, fieldValues);
			}
		} catch (Exception e) {
			LOG.debug(e);
		}
	}

	private boolean failNegative(Integer value) {
		return Validator.isNotNull(value) && value < 0;
	}

	private boolean failNotPositive(Integer value) {
		return Validator.isNotNull(value) && value <= 0;
	}

	private boolean hasUpdatePermissionOnDDLRecord(long ddlRecordSetId) throws PortalException {
		ddlRecordSetModelResourcePermission.check(PermissionThreadLocal.getPermissionChecker(), ddlRecordSetId, ActionKeys.UPDATE);
		return true;
	}

	private boolean isDDLRecordCreator(long userId, DDLRecord ddlRecord) {
		return userId == ddlRecord.getUserId();
	}

	private boolean validFieldType(DDMFormField field) {
		String fieldType = field.getType();
		return DDMFormFieldTypeConstants.CHECKBOX.equals(fieldType) || DDMFormFieldTypeConstants.RADIO.equals(fieldType) || DDMFormFieldTypeConstants.SELECT.equals(fieldType);
	}

}
