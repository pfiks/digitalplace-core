package com.placecube.digitalplace.pageimportadmin.service;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.pageimportadmin.model.PageImportModel;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;

@Component(immediate = true, service = ContentValidationService.class)
public class ContentValidationService {

	@Reference
	private ContentRetrievalService contentRetrievalService;

	public void validatePage(PageImportModel pageImportModel, ServiceContext serviceContext) {
		Locale locale = serviceContext.getLocale();

		if (Validator.isNull(pageImportModel.getTitle())) {
			pageImportModel.addErrorMessage(LanguageUtil.get(locale, "title-is-required"));
		}

		if (contentRetrievalService.getLayout(pageImportModel.getGroupId(), pageImportModel.getUrl(), pageImportModel.isPrivateLayout()).isPresent()) {
			pageImportModel.addErrorMessage(LanguageUtil.get(locale, "page-already-exists"));
		}

		if (Validator.isNotNull(pageImportModel.getPageTemplateName()) && !pageImportModel.getPageTemplate().isPresent()) {
			pageImportModel.addWarningMessage(LanguageUtil.get(locale, "no-pagetemplate-found"));
		}

		if (Validator.isNotNull(pageImportModel.getParentPageURL()) && !pageImportModel.getParentLayout().isPresent()) {
			pageImportModel.addWarningMessage(LanguageUtil.get(locale, "no-parent-layout-found"));
		}
	}

	public void validateWebContent(WebContentImportModel webContentImportModel, ServiceContext serviceContext) {
		Locale locale = serviceContext.getLocale();

		if (Validator.isNull(webContentImportModel.getTitle())) {
			webContentImportModel.addErrorMessage(LanguageUtil.get(locale, "title-is-required"));
		}

		if (!webContentImportModel.getWebContentStructure().isPresent()) {
			webContentImportModel.addErrorMessage(LanguageUtil.get(locale, "no-structure-found"));
		}

		if (contentRetrievalService.doesArticleWithURLTitleExists(webContentImportModel)) {
			webContentImportModel.addErrorMessage(LanguageUtil.get(locale, "webcontent-already-exists"));
		}
	}

}
