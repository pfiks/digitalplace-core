package com.placecube.digitalplace.ddl.web.portlet.display.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.constants.DDLActionKeys;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.role.RoleConstants;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.permission.ModelPermissions;
import com.liferay.portal.kernel.theme.ThemeDisplay;

@Component(immediate = true, service = DDLPermissionService.class)
public class DDLPermissionService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLPermissionService.class);

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference(target = "(model.class.name=com.liferay.dynamic.data.lists.model.DDLRecordSet)")
	private ModelResourcePermission<DDLRecordSet> ddlRecordSetModelResourcePermission;

	public void configureDefaultDDLRecordSetPermissionsInServiceContext(ServiceContext serviceContext) {
		ModelPermissions modelPermissions = serviceContext.getModelPermissions();
		modelPermissions.setResourceName(DDLRecordSet.class.getName());
		modelPermissions.addRolePermissions(RoleConstants.OWNER, DDLActionKeys.ADD_RECORD, ActionKeys.VIEW);
		modelPermissions.addRolePermissions(RoleConstants.SITE_MEMBER, DDLActionKeys.ADD_RECORD, ActionKeys.VIEW);

		serviceContext.setModelPermissions(modelPermissions);
		serviceContext.setAttribute("addRecordSetResources", true);
	}

	public boolean hasAddPermission(ThemeDisplay themeDisplay, DDLRecordSet ddlRecordSet) {
		return hasPermissionOnDDLRecord(themeDisplay.getPermissionChecker(), ddlRecordSet.getRecordSetId(), DDLActionKeys.ADD_RECORD);
	}

	public boolean hasDeletePermission(PermissionChecker permissionChecker, DDLRecord ddlRecord) {
		return isDDLRecordCreator(permissionChecker, ddlRecord) || hasPermissionOnDDLRecord(permissionChecker, ddlRecord.getRecordSetId(), ActionKeys.DELETE);
	}

	public boolean hasUpdatePermission(PermissionChecker permissionChecker, DDLRecord ddlRecord) {
		return isDDLRecordCreator(permissionChecker, ddlRecord) || hasPermissionOnDDLRecord(permissionChecker, ddlRecord.getRecordSetId(), ActionKeys.UPDATE);
	}

	public void validateDeletePermission(PermissionChecker permissionChecker, long recordId) throws PortalException {
		if (!hasDeletePermission(permissionChecker, ddlRecordLocalService.getDDLRecord(recordId))) {
			throw new PortalException();
		}
	}

	public void validateUpdatePermission(PermissionChecker permissionChecker, long ddlRecordId) throws PortalException {
		if (!hasUpdatePermission(permissionChecker, ddlRecordLocalService.getDDLRecord(ddlRecordId))) {
			throw new PortalException();
		}
	}

	private boolean hasPermissionOnDDLRecord(PermissionChecker permissionChecker, long ddlRecordSetId, String actionKey) {
		try {
			ddlRecordSetModelResourcePermission.check(permissionChecker, ddlRecordSetId, actionKey);
			return true;
		} catch (PortalException e) {
			LOG.debug("User does not have " + actionKey + " permission for ddlRecordSetId: " + ddlRecordSetId + " - " + e.getMessage());
			return false;
		}
	}

	private boolean isDDLRecordCreator(PermissionChecker permissionChecker, DDLRecord ddlRecord) {
		return permissionChecker.getUserId() == ddlRecord.getUserId();
	}

}