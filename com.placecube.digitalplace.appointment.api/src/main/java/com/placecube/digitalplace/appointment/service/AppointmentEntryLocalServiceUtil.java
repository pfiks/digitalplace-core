/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.appointment.model.AppointmentEntry;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for AppointmentEntry. This utility wraps
 * <code>com.placecube.digitalplace.appointment.service.impl.AppointmentEntryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentEntryLocalService
 * @generated
 */
public class AppointmentEntryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.appointment.service.impl.AppointmentEntryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the appointment entry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentEntry the appointment entry
	 * @return the appointment entry that was added
	 */
	public static AppointmentEntry addAppointmentEntry(
		AppointmentEntry appointmentEntry) {

		return getService().addAppointmentEntry(appointmentEntry);
	}

	/**
	 * Creates a new appointment entry
	 *
	 * @param companyId the companyId
	 * @param serviceId the serviceId
	 * @param connectorClassName the connector classname
	 * @param appointmentBookingEntry the appointmentBookingEntry
	 * @param externalAppointmentId the externalAppointmentId
	 * @return the created appointment entry
	 */
	public static AppointmentEntry addAppointmentEntry(
		long companyId, String serviceId, String connectorClassName,
		com.placecube.digitalplace.appointment.booking.model.
			AppointmentBookingEntry appointmentBookingEntry,
		String externalAppointmentId) {

		return getService().addAppointmentEntry(
			companyId, serviceId, connectorClassName, appointmentBookingEntry,
			externalAppointmentId);
	}

	/**
	 * Creates a new appointment entry with the primary key. Does not add the appointment entry to the database.
	 *
	 * @param appointmentEntryId the primary key for the new appointment entry
	 * @return the new appointment entry
	 */
	public static AppointmentEntry createAppointmentEntry(
		long appointmentEntryId) {

		return getService().createAppointmentEntry(appointmentEntryId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the appointment entry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentEntry the appointment entry
	 * @return the appointment entry that was removed
	 */
	public static AppointmentEntry deleteAppointmentEntry(
		AppointmentEntry appointmentEntry) {

		return getService().deleteAppointmentEntry(appointmentEntry);
	}

	/**
	 * Deletes the appointment entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry that was removed
	 * @throws PortalException if a appointment entry with the primary key could not be found
	 */
	public static AppointmentEntry deleteAppointmentEntry(
			long appointmentEntryId)
		throws PortalException {

		return getService().deleteAppointmentEntry(appointmentEntryId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static AppointmentEntry fetchAppointmentEntry(
		long appointmentEntryId) {

		return getService().fetchAppointmentEntry(appointmentEntryId);
	}

	/**
	 * Returns the appointment entry with the matching UUID and company.
	 *
	 * @param uuid the appointment entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	public static AppointmentEntry fetchAppointmentEntryByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().fetchAppointmentEntryByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a random generated appointmentId, unique for the
	 * companyId-serviceId-connectorClassName values
	 *
	 * @param companyId the companyId
	 * @param serviceId the serviceId
	 * @param connectorClassName the connector classname
	 * @return the auto-generated id
	 */
	public static String generateExternalAppointmentId(
		long companyId, String serviceId, String connectorClassName) {

		return getService().generateExternalAppointmentId(
			companyId, serviceId, connectorClassName);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of appointment entries
	 */
	public static List<AppointmentEntry> getAppointmentEntries(
		int start, int end) {

		return getService().getAppointmentEntries(start, end);
	}

	public static List<AppointmentEntry> getAppointmentEntries(
		long companyId, long classPK, long classNameId) {

		return getService().getAppointmentEntries(
			companyId, classPK, classNameId);
	}

	/**
	 * Returns the number of appointment entries.
	 *
	 * @return the number of appointment entries
	 */
	public static int getAppointmentEntriesCount() {
		return getService().getAppointmentEntriesCount();
	}

	/**
	 * Returns the appointment entry with the primary key.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry
	 * @throws PortalException if a appointment entry with the primary key could not be found
	 */
	public static AppointmentEntry getAppointmentEntry(long appointmentEntryId)
		throws PortalException {

		return getService().getAppointmentEntry(appointmentEntryId);
	}

	/**
	 * Returns the appointment entry with the matching UUID and company.
	 *
	 * @param uuid the appointment entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching appointment entry
	 * @throws PortalException if a matching appointment entry could not be found
	 */
	public static AppointmentEntry getAppointmentEntryByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException {

		return getService().getAppointmentEntryByUuidAndCompanyId(
			uuid, companyId);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static void removeAppointmentEntry(
		long companyId, String serviceId, String connectorClassName,
		long classPK, long classNameId, String appointmentId) {

		getService().removeAppointmentEntry(
			companyId, serviceId, connectorClassName, classPK, classNameId,
			appointmentId);
	}

	/**
	 * Updates the appointment entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentEntry the appointment entry
	 * @return the appointment entry that was updated
	 */
	public static AppointmentEntry updateAppointmentEntry(
		AppointmentEntry appointmentEntry) {

		return getService().updateAppointmentEntry(appointmentEntry);
	}

	public static AppointmentEntryLocalService getService() {
		return _service;
	}

	public static void setService(AppointmentEntryLocalService service) {
		_service = service;
	}

	private static volatile AppointmentEntryLocalService _service;

}