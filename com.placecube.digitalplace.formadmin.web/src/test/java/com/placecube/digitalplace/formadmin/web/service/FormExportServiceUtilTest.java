package com.placecube.digitalplace.formadmin.web.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstanceLink;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureLayout;
import com.liferay.dynamic.data.mapping.model.DDMStructureVersion;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLayoutLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureVersionLocalService;
import com.liferay.exportimport.kernel.lar.ExportImportPathUtil;
import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.exportimport.kernel.lar.PortletDataHandler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ReleaseInfo;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.placecube.digitalplace.formadmin.web.constants.FormExportKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LanguageUtil.class, ReleaseInfo.class, Time.class, ExportImportPathUtil.class, PropsUtil.class, SAXReaderUtil.class })
public class FormExportServiceUtilTest extends PowerMockito {

	@InjectMocks
	private FormExportServiceUtil formExportServiceUtil;

	@Mock
	private Document mockDocument;

	@Mock
	private DDMDataProviderInstance mockDDMDataProviderInstance;

	@Mock
	private DDMDataProviderInstanceLink ddmDataProviderInstanceLink;

	@Mock
	private DDMDataProviderInstanceLocalService mockDDMDataProviderInstanceLocalService;

	@Mock
	private DDMFormInstance mockDDMFormInstance;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLayout mockDDMStructureLayout;

	@Mock
	private DDMStructureLayoutLocalService mockDDMStructureLayoutLocalService;

	@Mock
	private DDMStructureVersion mockDDMStructureVersion;

	@Mock
	private DDMStructureVersionLocalService mockDDMStructureVersionLocalService;

	@Mock
	private Element element1;

	@Mock
	private Element element2;

	@Mock
	private Element element3;

	@Mock
	private Element element4;

	@Mock
	private Element element5;

	@Mock
	private Element element6;

	@Mock
	private Element element7;

	@Mock
	private Element element8;

	@Mock
	private Element element9;

	@Mock
	private Element element10;

	@Mock
	private Element element11;

	@Mock
	private Element element12;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private Portlet mockPortlet;

	@Mock
	private PortletDataContext mockPortletDataContext;

	@Mock
	private PortletDataHandler mockPortletDataHandler;

	@Before
	public void Setup() {
		mockStatic(LanguageUtil.class, ReleaseInfo.class, Time.class, ExportImportPathUtil.class, PropsUtil.class, SAXReaderUtil.class);
	}

	@Test(expected = PortalException.class)
	public void createManifestXML_WhenErrorFetchingUserPersonalSiteGroup_ThenThrowError() throws PortalException, IOException {
		long groupId = 1L;
		long companyId = 2L;
		String portletId = "portletId";
		int ddmDataProviderInstanceLinksLength = 3;

		when(mockGroupLocalService.fetchCompanyGroup(companyId)).thenReturn(mockGroup);
		when(mockGroupLocalService.fetchUserPersonalSiteGroup(companyId)).thenThrow(new PortalException());

		formExportServiceUtil.createManifestXML(mockPortlet, mockPortletDataContext, groupId, companyId, portletId, ddmDataProviderInstanceLinksLength);
	}

	@Test(expected = IOException.class)
	public void createManifestXML_WhenErrorGettingFormattedString_ThenThrowException() throws PortalException, IOException {
		long groupId = 1L;
		long companyId = 2L;
		String portletId = "portletId";
		int ddmDataProviderInstanceLinksLength = 3;

		when(mockGroupLocalService.fetchCompanyGroup(companyId)).thenReturn(mockGroup);
		when(mockGroupLocalService.fetchUserPersonalSiteGroup(companyId)).thenReturn(mockGroup);

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.ROOT)).thenReturn(element1);

		Set<Locale> locales = new HashSet<>();
		locales.add(Locale.UK);

		when(element1.addElement(FormExportKeys.HEADER)).thenReturn(element2);

		when(mockPortal.getSiteGroupId(groupId)).thenReturn(3L);
		when(LanguageUtil.getAvailableLocales(3L)).thenReturn(locales);
		when(ReleaseInfo.getBuildNumber()).thenReturn(4);
		when(Time.getRFC822()).thenReturn("time");

		when(element1.addElement(FormExportKeys.PORTLET)).thenReturn(element2);

		when(mockPortlet.getPortletId()).thenReturn(portletId);
		when(ExportImportPathUtil.getPortletPath(mockPortletDataContext)).thenReturn("portletPath");

		when(mockPortlet.getPortletDataHandlerInstance()).thenReturn(mockPortletDataHandler);
		when(mockPortletDataHandler.getSchemaVersion()).thenReturn("schemaVersion");

		when(element1.addElement(FormExportKeys.MANIFEST_SUMMARY)).thenReturn(element2);
		when(element2.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element3, element4, element5, element6);

		when(mockDocument.formattedString()).thenThrow(new IOException());

		formExportServiceUtil.createManifestXML(mockPortlet, mockPortletDataContext, groupId, companyId, portletId, ddmDataProviderInstanceLinksLength);
	}

	@Test
	public void createManifestXML_WhenNoError_ThenCreateXML() throws PortalException, IOException {
		long groupId = 1L;
		long companyId = 2L;
		String portletId = "portletId";
		String formattedString = "formattedString";
		int ddmDataProviderInstanceLinksLength = 3;

		when(mockGroupLocalService.fetchCompanyGroup(companyId)).thenReturn(mockGroup);
		when(mockGroupLocalService.fetchUserPersonalSiteGroup(companyId)).thenReturn(mockGroup);

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.ROOT)).thenReturn(element1);

		Set<Locale> locales = new HashSet<>();
		locales.add(Locale.UK);

		when(element1.addElement(FormExportKeys.HEADER)).thenReturn(element2);

		when(mockPortal.getSiteGroupId(groupId)).thenReturn(3L);
		when(LanguageUtil.getAvailableLocales(3L)).thenReturn(locales);
		when(ReleaseInfo.getBuildNumber()).thenReturn(4);
		when(Time.getRFC822()).thenReturn("time");

		when(element1.addElement(FormExportKeys.PORTLET)).thenReturn(element2);

		when(mockPortlet.getPortletId()).thenReturn(portletId);
		when(ExportImportPathUtil.getPortletPath(mockPortletDataContext)).thenReturn("portletPath");

		when(mockPortlet.getPortletDataHandlerInstance()).thenReturn(mockPortletDataHandler);
		when(mockPortletDataHandler.getSchemaVersion()).thenReturn("schemaVersion");

		when(element1.addElement(FormExportKeys.MANIFEST_SUMMARY)).thenReturn(element2);
		when(element2.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element3, element4, element5, element6);

		when(mockDocument.formattedString()).thenReturn(formattedString);

		formExportServiceUtil.createManifestXML(mockPortlet, mockPortletDataContext, groupId, companyId, portletId, ddmDataProviderInstanceLinksLength);
		verify(mockPortletDataContext, times(1)).addZipEntry("manifest.xml", formattedString);
	}

	@Test(expected = PortalException.class)
	public void createDDMFormInstanceXML_WhenAddingClassedModelError_ThenCreateXML() throws PortalException {
		String modelPath = "modelPath";

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.ROOT)).thenReturn(element1);

		when(ExportImportPathUtil.getModelPath(mockDDMFormInstance)).thenReturn(modelPath);

		doThrow(new PortalException()).when(mockPortletDataContext).addClassedModel(element1, modelPath, mockDDMFormInstance);

		formExportServiceUtil.createDDMFormInstanceXML(mockPortletDataContext, mockDDMFormInstance);
	}

	@Test
	public void createDDMFormInstanceXML_WhenNoError_ThenCreateXML() throws PortalException {
		String modelPath = "modelPath";

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.ROOT)).thenReturn(element1);

		when(ExportImportPathUtil.getModelPath(mockDDMFormInstance)).thenReturn(modelPath);

		formExportServiceUtil.createDDMFormInstanceXML(mockPortletDataContext, mockDDMFormInstance);
		verify(mockPortletDataContext, times(1)).addClassedModel(element1, modelPath, mockDDMFormInstance);
	}

	@Test
	public void createDDMFormJSON_WhenNoError_ThenCreateJSON() {
		String definition = "definition";

		when(mockDDMStructure.getDefinition()).thenReturn(definition);

		formExportServiceUtil.createDDMFormJSON(mockPortletDataContext, mockDDMStructure);
		verify(mockPortletDataContext, times(1)).addZipEntry(Mockito.anyString(), Mockito.eq(definition));
	}

	@Test(expected = PortalException.class)
	public void createDDMDataProviderInstanceXML_WhenErrorGettingDataProviderInstance_ThenThrowException() throws PortalException {
		long dataProviderInstanceId = 1L;

		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.ROOT)).thenReturn(element1);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceId);
		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceId)).thenThrow(new PortalException());

		formExportServiceUtil.createDDMDataProviderInstanceXML(ddmDataProviderInstanceLinkList, mockPortletDataContext);
	}

	@Test(expected = PortalException.class)
	public void createDDMDataProviderInstanceXML_WhenErrorAddingClassModel_ThenThrowException() throws PortalException {
		long dataProviderInstanceId = 1L;
		String modelClass = "modelClass";

		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.ROOT)).thenReturn(element1);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceId);
		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceId)).thenReturn(mockDDMDataProviderInstance);

		when(ExportImportPathUtil.getModelPath(mockDDMDataProviderInstance)).thenReturn(modelClass);

		doThrow(new PortalException()).when(mockPortletDataContext).addClassedModel(element1, modelClass, mockDDMDataProviderInstance);

		formExportServiceUtil.createDDMDataProviderInstanceXML(ddmDataProviderInstanceLinkList, mockPortletDataContext);
	}

	@Test
	public void createDDMDataProviderInstanceXML_WhenNoError_ThenCreateXML() throws PortalException {
		long dataProviderInstanceId = 1L;
		String modelClass = "modelClass";

		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.ROOT)).thenReturn(element1);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceId);
		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceId)).thenReturn(mockDDMDataProviderInstance);

		when(ExportImportPathUtil.getModelPath(mockDDMDataProviderInstance)).thenReturn(modelClass);

		formExportServiceUtil.createDDMDataProviderInstanceXML(ddmDataProviderInstanceLinkList, mockPortletDataContext);
		verify(mockPortletDataContext, times(1)).addClassedModel(element1, modelClass, mockDDMDataProviderInstance);
	}

	@Test(expected = PortalException.class)
	public void createDDMFormLayoutJSON_WhenErrorGettingGettingStructureLayout_ThenThrowException() throws PortalException {
		long structureId = 1L;
		long structureVersionId = 2L;

		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructureVersionLocalService.getLatestStructureVersion(structureId)).thenThrow(new PortalException());
		when(mockDDMStructureVersion.getStructureVersionId()).thenReturn(structureVersionId);
		when(mockDDMStructureLayoutLocalService.getStructureLayoutByStructureVersionId(structureVersionId)).thenThrow(new PortalException());

		formExportServiceUtil.createDDMFormLayoutJSON(mockPortletDataContext, mockDDMStructure);
	}

	@Test(expected = PortalException.class)
	public void createDDMFormLayoutJSON_WhenErrorGettingLatestStructureVersion_ThenThrowException() throws PortalException {
		long structureId = 1L;

		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructureVersionLocalService.getLatestStructureVersion(structureId)).thenThrow(new PortalException());

		formExportServiceUtil.createDDMFormLayoutJSON(mockPortletDataContext, mockDDMStructure);
	}

	@Test
	public void createDDMFormLayoutJSON_WhenNoError_ThenCreateJSON() throws PortalException {
		long structureId = 1L;
		long structureVersionId = 2L;
		String definition = "definition";

		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructureVersionLocalService.getLatestStructureVersion(structureId)).thenReturn(mockDDMStructureVersion);
		when(mockDDMStructureVersion.getStructureVersionId()).thenReturn(structureVersionId);
		when(mockDDMStructureLayoutLocalService.getStructureLayoutByStructureVersionId(structureVersionId)).thenReturn(mockDDMStructureLayout);

		when(mockDDMStructureLayout.getDefinition()).thenReturn(definition);

		formExportServiceUtil.createDDMFormLayoutJSON(mockPortletDataContext, mockDDMStructure);
		verify(mockPortletDataContext, times(1)).addZipEntry(Mockito.anyString(), Mockito.eq(definition));
	}

	@Test(expected = PortalException.class)
	public void createDDMStructureXML_WhenError_ThenThrowException() throws PortalException {
		String modelPath = "modelPath";

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.ROOT)).thenReturn(element1);

		when(ExportImportPathUtil.getModelPath(mockDDMStructure)).thenReturn(modelPath);

		doThrow(new PortalException()).when(mockPortletDataContext).addClassedModel(element1, modelPath, mockDDMStructure);

		formExportServiceUtil.createDDMStructureXML(mockPortletDataContext, mockDDMStructure);
	}

	@Test
	public void createDDMStructureXML_WhenNoError_ThenCreateXML() throws PortalException {
		String modelPath = "modelPath";

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.ROOT)).thenReturn(element1);

		when(ExportImportPathUtil.getModelPath(mockDDMStructure)).thenReturn(modelPath);

		formExportServiceUtil.createDDMStructureXML(mockPortletDataContext, mockDDMStructure);
		verify(mockPortletDataContext, times(1)).addClassedModel(element1, modelPath, mockDDMStructure);
	}

	@Test
	public void createSettingsDDMFormValuesJSON_WhenNoError_ThenAddZipEntry() {
		String settingsDDMFormValuesPath = "settingsDDMFormValuesPath";
		String settings = "settings";
		when(ExportImportPathUtil.getModelPath(mockDDMFormInstance, "settings-ddm-form-values.json")).thenReturn(settingsDDMFormValuesPath);
		when(mockDDMFormInstance.getSettings()).thenReturn(settings);

		formExportServiceUtil.createSettingsDDMFormValuesJSON(mockPortletDataContext, mockDDMFormInstance);
		verify(mockPortletDataContext, times(1)).addZipEntry(settingsDDMFormValuesPath, settings);
	}

	@Test(expected = PortalException.class)
	public void addDDMDataProviderInstanceElement_WhenErrorGettingDataProviderInstance_TenThrowException() throws PortalException {
		long dataProviderInstanceID = 1L;
		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(element1.addElement(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE)).thenReturn(element2);
		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceID);

		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceID)).thenThrow(new PortalException());
		formExportServiceUtil.addDDMDataProviderInstanceElement(element1, ddmDataProviderInstanceLinkList);
	}

	@Test
	public void addDDMDataProviderInstanceElement_whenNoErrors_thenAddElements() throws PortalException {
		long dataProviderInstanceID = 1L;
		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(element1.addElement(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE)).thenReturn(element2);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceID);

		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceID)).thenReturn(mockDDMDataProviderInstance);

		when(element2.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element3);

		formExportServiceUtil.addDDMDataProviderInstanceElement(element1, ddmDataProviderInstanceLinkList);

		InOrder inOrder = Mockito.inOrder(element3);
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.UUID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.PATH), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.USER_UUID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(FormExportKeys.ASSET_ENTRY_PRIORITY, "0.0");
	}

	@Test(expected = PortalException.class)
	public void addDDMFormInstanceReferencesElement() throws PortalException {
		long groupId = 1L;

		when(element1.addElement(FormExportKeys.REFERENCES)).thenReturn(element2);

		when(element2.addElement(FormExportKeys.REFERENCE)).thenReturn(element3);

		when(mockDDMStructure.getModelClassName()).thenReturn("ModelClassName");
		when(mockDDMFormInstance.getModelClassName()).thenReturn("ModelClassName");

		when(mockDDMStructure.getGroupId()).thenReturn(2L);
		when(mockGroupLocalService.getGroup(2L)).thenThrow(new PortalException());

		formExportServiceUtil.addDDMFormInstanceReferencesElement(element1, mockDDMStructure, mockDDMFormInstance, groupId);

	}

	@Test
	public void addDDMFormInstanceReferencesElement_WhenNoErrors_ThenAddElements() throws PortalException {
		long groupId = 1L;

		when(element1.addElement(FormExportKeys.REFERENCES)).thenReturn(element2);

		when(element2.addElement(FormExportKeys.REFERENCE)).thenReturn(element3);

		when(mockDDMStructure.getModelClassName()).thenReturn("ModelClassName");
		when(mockDDMFormInstance.getModelClassName()).thenReturn("ModelClassName");

		when(mockDDMStructure.getGroupId()).thenReturn(2L);
		when(mockGroupLocalService.getGroup(2L)).thenReturn(mockGroup);
		when(mockGroup.getGroupKey()).thenReturn("groupKey");

		when(mockDDMStructure.getUuid()).thenReturn(FormExportKeys.UUID);
		when(mockDDMStructure.getStructureKey()).thenReturn("structureKey");

		formExportServiceUtil.addDDMFormInstanceReferencesElement(element1, mockDDMStructure, mockDDMFormInstance, groupId);

		InOrder inOrder = Mockito.inOrder(element3);
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.CLASS_NAME), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.CLASS_PK), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.ATTACHED_CLASS_NAME), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_KEY), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.LIVE_GROUP_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(FormExportKeys.TYPE, PortletDataContext.REFERENCE_TYPE_STRONG);
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.UUID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.COMPANY_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.PRELOADED), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.REFERENCED_CLASS_NAME), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.STRUCTURE_KEY), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.MISSING), Mockito.anyString());

	}

	@Test(expected = PortalException.class)
	public void addDDMStructureReferencesElement_WhenErrorGettingDataProviderInstance_ThenThrowException() throws PortalException {
		long groupId = 1L;
		long dataProviderInstanceID = 2L;
		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(element1.addElement(FormExportKeys.REFERENCES)).thenReturn(element2);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceID);

		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceID)).thenThrow(new PortalException());

		formExportServiceUtil.addDDMStructureReferencesElement(element1, ddmDataProviderInstanceLinkList, groupId);
	}

	@Test(expected = PortalException.class)
	public void addDDMStructureReferencesElement_WhenErrorGettingGroup_ThenThrowException() throws PortalException {
		long groupId = 1L;
		long dataProviderInstanceID = 2L;
		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(element1.addElement(FormExportKeys.REFERENCES)).thenReturn(element2);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceID);

		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceID)).thenReturn(mockDDMDataProviderInstance);

		when(element2.addElement(FormExportKeys.REFERENCE)).thenReturn(element3);

		when(mockDDMDataProviderInstance.getModelClassName()).thenReturn("ModelClassName");

		when(mockDDMDataProviderInstance.getGroupId()).thenReturn(3L);
		when(mockGroupLocalService.getGroup(3L)).thenThrow(new PortalException());

		formExportServiceUtil.addDDMStructureReferencesElement(element1, ddmDataProviderInstanceLinkList, groupId);
	}

	@Test
	public void addDDMStructureReferencesElement_WhenNoErrorsAndAttributeDDMDataProviderInstanceIdsEmpty_ThenAddElements() throws PortalException {
		long groupId = 1L;
		long dataProviderInstanceID = 2L;
		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(element1.addElement(FormExportKeys.REFERENCES)).thenReturn(element2);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceID);

		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceID)).thenReturn(mockDDMDataProviderInstance);

		when(element2.addElement(FormExportKeys.REFERENCE)).thenReturn(element3);

		when(mockDDMDataProviderInstance.getModelClassName()).thenReturn("ModelClassName");

		when(mockDDMDataProviderInstance.getGroupId()).thenReturn(3L);
		when(mockGroupLocalService.getGroup(3L)).thenReturn(mockGroup);
		when(mockGroup.getGroupKey()).thenReturn("groupKey");

		when(mockDDMDataProviderInstance.getUuid()).thenReturn(FormExportKeys.UUID);

		when(element1.attributeValue(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS)).thenReturn("");
		formExportServiceUtil.addDDMStructureReferencesElement(element1, ddmDataProviderInstanceLinkList, groupId);

		InOrder inOrder = Mockito.inOrder(element3, element1);
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.CLASS_NAME), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.CLASS_PK), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_KEY), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.LIVE_GROUP_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(FormExportKeys.TYPE, PortletDataContext.REFERENCE_TYPE_STRONG);
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.UUID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.COMPANY_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.MISSING), Mockito.anyString());
		inOrder.verify(element1, times(1)).addAttribute(Mockito.eq(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS), Mockito.anyString());
	}

	@Test
	public void addDDMStructureReferencesElement_WhenNoErrorsAndAttributeDDMDataProviderInstanceIdsNoEmpty_ThenAddElements() throws PortalException {
		long groupId = 1L;
		long dataProviderInstanceID = 2L;
		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(element1.addElement(FormExportKeys.REFERENCES)).thenReturn(element2);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceID);

		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceID)).thenReturn(mockDDMDataProviderInstance);

		when(element2.addElement(FormExportKeys.REFERENCE)).thenReturn(element3);

		when(mockDDMDataProviderInstance.getModelClassName()).thenReturn("ModelClassName");

		when(mockDDMDataProviderInstance.getGroupId()).thenReturn(3L);
		when(mockGroupLocalService.getGroup(3L)).thenReturn(mockGroup);
		when(mockGroup.getGroupKey()).thenReturn("groupKey");

		when(mockDDMDataProviderInstance.getUuid()).thenReturn(FormExportKeys.UUID);

		when(element1.attributeValue(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS)).thenReturn("instanceId");
		formExportServiceUtil.addDDMStructureReferencesElement(element1, ddmDataProviderInstanceLinkList, groupId);

		InOrder inOrder = Mockito.inOrder(element3, element1);
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.CLASS_NAME), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.CLASS_PK), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_KEY), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.LIVE_GROUP_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(FormExportKeys.TYPE, PortletDataContext.REFERENCE_TYPE_STRONG);
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.UUID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.COMPANY_ID), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.MISSING), Mockito.anyString());
		inOrder.verify(element1, times(1)).addAttribute(Mockito.eq(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS), Mockito.anyString());
	}

	@Test
	public void addDDMFormInstanceStagedModelElement_WhenNoError_ThenAddAttributes() {

		when(element1.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element2);

		formExportServiceUtil.addDDMFormInstanceStagedModelElement(element1, mockDDMFormInstance);

		InOrder inOrder = Mockito.inOrder(element2);
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_ID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.UUID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.SETTINGS_DDM_FORM_VALUES_PATH), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.PATH), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.USER_UUID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(FormExportKeys.ASSET_ENTRY_PRIORITY, "0.0");

	}

	@Test
	public void addDDMStructureStagedModelElement_WhenNoError_ThenAddAttributes() {

		when(element1.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element2);

		when(mockDDMStructure.getUuid()).thenReturn(FormExportKeys.UUID);
		when(mockDDMFormInstance.getModelClassName()).thenReturn("ModelClassName");
		when(mockDDMFormInstance.getUserUuid()).thenReturn("userUuid");

		formExportServiceUtil.addDDMStructureStagedModelElement(element1, mockDDMStructure, mockDDMFormInstance);

		InOrder inOrder = Mockito.inOrder(element2);
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_ID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.UUID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.DDM_FORM_PATH), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.DDM_FORM_LAYOUT_PATH), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.PATH), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.ATTACHED_CLASS_NAME), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.USER_UUID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(FormExportKeys.ASSET_ENTRY_PRIORITY, "0.0");
	}

	@Test
	public void addHeaderElement_WhenNoError_ThenAddAttributes() {
		long companyId = 1L;
		long groupId = 2L;
		String portletId = "portletId";
		Set<Locale> locales = new HashSet<>();
		locales.add(Locale.UK);

		when(element1.addElement(FormExportKeys.HEADER)).thenReturn(element2);

		when(mockPortal.getSiteGroupId(groupId)).thenReturn(3L);
		when(LanguageUtil.getAvailableLocales(3L)).thenReturn(locales);
		when(ReleaseInfo.getBuildNumber()).thenReturn(4);
		when(Time.getRFC822()).thenReturn("time");

		formExportServiceUtil.addHeaderElement(element1, mockGroup, mockGroup, companyId, groupId, portletId);
		InOrder inOrder = Mockito.inOrder(element2);
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.AVAILABLE_LOCALES), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.BUILD_NUMBER), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.EXPORT_DATE), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(FormExportKeys.TYPE, FormExportKeys.PORTLET);
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.COMPANY_ID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.COMPANY_GROUP_ID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.GROUP_ID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.USER_PERSONAL_SITE_GROUP_ID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.PRIVATE_LAYOUT), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.ROOT_PORTLET_ID), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.SCHEMA_VERSION), Mockito.anyString());

	}

	@Test
	public void addManifestSummaryElement_WhenNoError_AndLengthParameterNotZero() {
		int ddmDataProviderInstanceLinksLength = 1;

		when(element1.addElement(FormExportKeys.MANIFEST_SUMMARY)).thenReturn(element2);
		when(element2.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element3, element4, element5, element6);

		formExportServiceUtil.addManifestSummaryElement(element1, ddmDataProviderInstanceLinksLength);

		InOrder inOrder = Mockito.inOrder(element3, element4, element5, element6);
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.MANIFEST_SUMMARY_KEY), Mockito.anyString());
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.ADDITION_COUNT), Mockito.anyString());
		inOrder.verify(element4, times(1)).addAttribute(Mockito.eq(FormExportKeys.MANIFEST_SUMMARY_KEY), Mockito.anyString());
		inOrder.verify(element4, times(1)).addAttribute(FormExportKeys.ADDITION_COUNT, "1");
		inOrder.verify(element5, times(1)).addAttribute(Mockito.eq(FormExportKeys.MANIFEST_SUMMARY_KEY), Mockito.anyString());
		inOrder.verify(element6, times(1)).addAttribute(Mockito.eq(FormExportKeys.MANIFEST_SUMMARY_KEY), Mockito.anyString());
		inOrder.verify(element6, times(1)).addAttribute(FormExportKeys.ADDITION_COUNT, "1");
	}

	@Test
	public void addManifestSummaryElement_WhenNoError_AndLengthParameterZero() {
		int ddmDataProviderInstanceLinksLength = 0;

		when(element1.addElement(FormExportKeys.MANIFEST_SUMMARY)).thenReturn(element2);
		when(element2.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element3, element4, element5, element6);

		formExportServiceUtil.addManifestSummaryElement(element1, ddmDataProviderInstanceLinksLength);

		InOrder inOrder = Mockito.inOrder(element3, element4, element5, element6);
		inOrder.verify(element3, times(1)).addAttribute(Mockito.eq(FormExportKeys.MANIFEST_SUMMARY_KEY), Mockito.anyString());
		inOrder.verify(element3, times(0)).addAttribute(Mockito.eq(FormExportKeys.ADDITION_COUNT), Mockito.anyString());
		inOrder.verify(element4, times(1)).addAttribute(Mockito.eq(FormExportKeys.MANIFEST_SUMMARY_KEY), Mockito.anyString());
		inOrder.verify(element4, times(1)).addAttribute(FormExportKeys.ADDITION_COUNT, "1");
		inOrder.verify(element5, times(1)).addAttribute(Mockito.eq(FormExportKeys.MANIFEST_SUMMARY_KEY), Mockito.anyString());
		inOrder.verify(element6, times(1)).addAttribute(Mockito.eq(FormExportKeys.MANIFEST_SUMMARY_KEY), Mockito.anyString());
		inOrder.verify(element6, times(1)).addAttribute(FormExportKeys.ADDITION_COUNT, "1");
	}

	@Test
	public void addPortletElement_WhenNoError_ThenAddAttributes() {

		when(element1.addElement(FormExportKeys.PORTLET)).thenReturn(element2);

		when(mockPortlet.getPortletId()).thenReturn("portletId");
		when(ExportImportPathUtil.getPortletPath(mockPortletDataContext)).thenReturn("portletPath");

		when(mockPortlet.getPortletDataHandlerInstance()).thenReturn(mockPortletDataHandler);
		when(mockPortletDataHandler.getSchemaVersion()).thenReturn("schemaVersion");
		formExportServiceUtil.addPortletElement(element1, mockPortlet, mockPortletDataContext);

		InOrder inOrder = Mockito.inOrder(element2);
		inOrder.verify(element2, times(1)).addAttribute(FormExportKeys.PORTLET_ID, "portletId");
		inOrder.verify(element2, times(1)).addAttribute(FormExportKeys.LAYOUT_ID, "1");
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.PATH), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.PORTLET_DATA), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(FormExportKeys.SCHEMA_VERSION, "schemaVersion");
		inOrder.verify(element2, times(1)).addAttribute(FormExportKeys.PORTLET_CONFIGURATION, "setup");
	}

	@Test
	public void createPortletDataXml_WhenNoError_ThenCreateDocument() throws PortalException, IOException {
		long groupId = 1L;
		long dataProviderInstanceID = 2L;

		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.DDM_FORM_ADMIN_PORTLET_DATA_HANDLER)).thenReturn(element1);

		when(element1.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element2);
		when(element1.addElement(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE)).thenReturn(element3);
		when(element3.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element4);
		when(element1.addElement(FormExportKeys.DDM_FORM_INSTANCE)).thenReturn(element5);
		when(element5.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element6);
		when(element6.addElement(FormExportKeys.REFERENCES)).thenReturn(element7);
		when(element7.addElement(FormExportKeys.REFERENCE)).thenReturn(element8);

		when(mockDDMStructure.getGroupId()).thenReturn(4L);
		when(mockGroupLocalService.getGroup(4L)).thenReturn(mockGroup);
		when(mockGroup.getGroupKey()).thenReturn("groupKey");

		when(element1.addElement(FormExportKeys.DDM_STRUCTURE)).thenReturn(element9);
		when(element9.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element10);
		when(element10.addElement(FormExportKeys.REFERENCES)).thenReturn(element11);
		when(element11.addElement(FormExportKeys.REFERENCE)).thenReturn(element12);

		when(mockDDMDataProviderInstance.getGroupId()).thenReturn(4L);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceID);
		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceID)).thenReturn(mockDDMDataProviderInstance);

		when(mockDocument.formattedString()).thenReturn("string");

		formExportServiceUtil.createPortletDataXml(mockPortlet, mockPortletDataContext, groupId, mockDDMFormInstance, mockDDMStructure, ddmDataProviderInstanceLinkList);

		verify(mockPortletDataContext, times(1)).addZipEntry(Mockito.anyString(), Mockito.anyString());
	}

	@Test(expected = IOException.class)
	public void createPortletDataXml_WhenErrorGettingStringFromDocument_ThenThrowException() throws PortalException, IOException {
		long groupId = 1L;
		long dataProviderInstanceID = 2L;

		List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList = new ArrayList<>();
		ddmDataProviderInstanceLinkList.add(ddmDataProviderInstanceLink);

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.DDM_FORM_ADMIN_PORTLET_DATA_HANDLER)).thenReturn(element1);

		when(element1.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element2);
		when(element1.addElement(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE)).thenReturn(element3);
		when(element3.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element4);
		when(element1.addElement(FormExportKeys.DDM_FORM_INSTANCE)).thenReturn(element5);
		when(element5.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element6);
		when(element6.addElement(FormExportKeys.REFERENCES)).thenReturn(element7);
		when(element7.addElement(FormExportKeys.REFERENCE)).thenReturn(element8);

		when(mockDDMStructure.getGroupId()).thenReturn(4L);
		when(mockGroupLocalService.getGroup(4L)).thenReturn(mockGroup);
		when(mockGroup.getGroupKey()).thenReturn("groupKey");

		when(element1.addElement(FormExportKeys.DDM_STRUCTURE)).thenReturn(element9);
		when(element9.addElement(FormExportKeys.STAGED_MODEL)).thenReturn(element10);
		when(element10.addElement(FormExportKeys.REFERENCES)).thenReturn(element11);
		when(element11.addElement(FormExportKeys.REFERENCE)).thenReturn(element12);

		when(mockDDMDataProviderInstance.getGroupId()).thenReturn(4L);

		when(ddmDataProviderInstanceLink.getDataProviderInstanceId()).thenReturn(dataProviderInstanceID);
		when(mockDDMDataProviderInstanceLocalService.getDataProviderInstance(dataProviderInstanceID)).thenReturn(mockDDMDataProviderInstance);

		when(mockDocument.formattedString()).thenThrow(new IOException());

		formExportServiceUtil.createPortletDataXml(mockPortlet, mockPortletDataContext, groupId, mockDDMFormInstance, mockDDMStructure, ddmDataProviderInstanceLinkList);

	}

	@Test(expected = IOException.class)
	public void createPortletXML_WhenError_ThenThrowException() throws IOException {
		long groupId = 1L;

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.PORTLET)).thenReturn(element1);

		when(mockPortlet.getPortletId()).thenReturn("portletId");
		when(mockPortlet.getRootPortletId()).thenReturn("rootPortletId");

		when(element1.addElement(FormExportKeys.PORTLET_DATA)).thenReturn(element2);

		when(mockDocument.formattedString()).thenThrow(new IOException());

		formExportServiceUtil.createPortletXML(mockPortlet, groupId, mockPortletDataContext);
	}

	@Test
	public void createPortletXML_WhenNoError_ThenAddElements() throws IOException {
		long groupId = 1L;

		when(SAXReaderUtil.createDocument()).thenReturn(mockDocument);

		when(mockDocument.addElement(FormExportKeys.PORTLET)).thenReturn(element1);

		when(mockPortlet.getPortletId()).thenReturn("portletId");
		when(mockPortlet.getRootPortletId()).thenReturn("rootPortletId");

		when(element1.addElement(FormExportKeys.PORTLET_DATA)).thenReturn(element2);

		when(mockDocument.formattedString()).thenReturn("string");

		formExportServiceUtil.createPortletXML(mockPortlet, groupId, mockPortletDataContext);

		InOrder inOrder = Mockito.inOrder(element1, element2, mockPortletDataContext);
		inOrder.verify(element1, times(1)).addAttribute(FormExportKeys.PORTLET_ID, "portletId");
		inOrder.verify(element1, times(1)).addAttribute(Mockito.eq(FormExportKeys.ROOT_PORTLET_ID), Mockito.anyString());
		inOrder.verify(element1, times(1)).addAttribute(Mockito.eq(FormExportKeys.SCOPE_GROUP_ID), Mockito.anyString());
		inOrder.verify(element1, times(1)).addAttribute(FormExportKeys.SCOPE_LAYOUT_TYPE, StringPool.BLANK);
		inOrder.verify(element1, times(1)).addAttribute(FormExportKeys.SCOPE_LAYOUT_UUID, StringPool.BLANK);
		inOrder.verify(element1, times(1)).addAttribute(Mockito.eq(FormExportKeys.PRIVATE_LAYOUT), Mockito.anyString());
		inOrder.verify(element1, times(1)).addAttribute(Mockito.eq(FormExportKeys.SELF_PATH), Mockito.anyString());
		inOrder.verify(element2, times(1)).addAttribute(Mockito.eq(FormExportKeys.PATH), Mockito.anyString());
		inOrder.verify(mockPortletDataContext, times(1)).addZipEntry(Mockito.anyString(), Mockito.anyString());
	}
}
