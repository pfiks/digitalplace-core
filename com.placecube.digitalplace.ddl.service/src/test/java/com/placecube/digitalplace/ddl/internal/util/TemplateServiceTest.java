package com.placecube.digitalplace.ddl.internal.util;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyZeroInteractions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.mobile.device.Device;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateManagerUtil;
import com.liferay.portal.kernel.template.TemplateResource;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.registry.Filter;
import com.liferay.registry.Registry;
import com.liferay.registry.RegistryUtil;
import com.liferay.registry.ServiceTracker;
import com.liferay.registry.ServiceTrackerCustomizer;
import com.placecube.digitalplace.ddl.internal.constants.TemplateKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PermissionThreadLocal.class, RegistryUtil.class, StringUtil.class, TemplateManagerUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.template.TemplateManagerUtil" })
public class TemplateServiceTest {

	private static final long CLASSNAME_ID = 5L;
	private static final long COMPANY_GROUP_ID = 2L;
	private static final long COMPANY_ID = 1L;
	private static final String LANG_TYPE = "anyLanguage";
	private static final String RANDOM_ID = "anyRandomId";
	private static final long SCOPE_GROUP_ID = 3L;
	private static final String SCRIPT = "anyScript";
	private static final long SITE_GROUP_ID = 4L;
	private static final String TEMPLATE_ID = "anyTemplateId";
	private static final String TEMPLATES_PATH = "anyTemplatesPath";

	@Mock
	private Company mockCompany;

	@Mock
	private Map<String, Object> mockContextObjects;

	@Mock
	private Device mockDevice;

	@Mock
	private Filter mockFilter;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private Registry mockRegistry;

	@Mock
	private ServiceTracker<?, ?> mockServiceTracker;

	@Mock
	private Template mockTemplate;

	@Mock
	private TemplateResource mockTemplateResource;

	@Mock
	private TemplateUtil mockTemplateUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UnsyncStringWriter mockUnsyncStringWriter;

	@InjectMocks
	private TemplateService templateService;

	@Before
	public void activateSetUp() {
		mockStatic(RegistryUtil.class);

		when(RegistryUtil.getRegistry()).thenReturn(mockRegistry);
		when(mockRegistry.getFilter(anyString())).thenReturn(mockFilter);
		when(mockRegistry.trackServices(any(Filter.class), any(ServiceTrackerCustomizer.class))).thenReturn(mockServiceTracker);

		mockStatic(PermissionThreadLocal.class, TemplateManagerUtil.class, StringUtil.class);
	}

	@Test
	public void getTemplate_WhenNoError_ThenReturnsTemplate() throws Exception {
		when(mockTemplateUtil.getTemplateId(1l, COMPANY_ID, COMPANY_GROUP_ID, SCOPE_GROUP_ID)).thenReturn(TEMPLATE_ID);
		when(mockTemplateUtil.getTemplateResourceInstance(TEMPLATE_ID, SCRIPT)).thenReturn(mockTemplateResource);
		when(TemplateManagerUtil.getTemplate(LANG_TYPE, mockTemplateResource, false)).thenReturn(mockTemplate);

		Template result = templateService.getTemplate(1l, COMPANY_ID, COMPANY_GROUP_ID, SCOPE_GROUP_ID, SCRIPT, LANG_TYPE);

		assertThat(result, sameInstance(mockTemplate));
	}

	@Test
	public void initTemplate_WhenNoError_ThenProcessesTemplate() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).processTemplate(mockUnsyncStringWriter);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsCompany() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.COMPANY, mockCompany);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsCompanyId() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.COMPANY_ID, COMPANY_ID);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsContexObjects() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).putAll(mockContextObjects);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsDevice() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.DEVICE, mockDevice);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsGroupId() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.GROUP_ID, SCOPE_GROUP_ID);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsJournalTemplatesPath() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.JOURNAL_TEMPLATES_PATH, TEMPLATES_PATH);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsPermissionChecker() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.PERMISSION_CHECKER, mockPermissionChecker);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsRandomNamespace() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.RANDOM_NAMESPACE, RANDOM_ID + StringPool.UNDERLINE);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsScopeGroupId() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.SCOPE_GROUP_ID, SCOPE_GROUP_ID);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsSiteGroupId() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.SITE_GROUP_ID, SITE_GROUP_ID);
	}

	@Test
	public void initTemplate_WhenNoError_ThenSetsTemplatesPath() throws Exception {
		mockInitVariables();

		templateService.initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);

		verify(mockTemplate).put(TemplateKeys.TEMPLATES_PATH, TEMPLATES_PATH);
	}

	@Test
	public void prepareTemplate_WhenThemeDisplayIsNotNull_ThenPreparesTemplate() {
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);

		templateService.prepareTemplate(mockThemeDisplay, mockTemplate);

		verify(mockTemplate).prepare(mockHttpServletRequest);
	}

	@Test
	public void prepareTemplate_WhenThemeDisplayIsNull_ThenDoesNothing() {
		templateService.prepareTemplate(null, mockTemplate);

		verifyZeroInteractions(mockTemplate);
	}

	private void mockInitVariables() {
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(SITE_GROUP_ID);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockThemeDisplay.getDevice()).thenReturn(mockDevice);
		when(mockContextObjects.get(TemplateConstants.CLASS_NAME_ID)).thenReturn(CLASSNAME_ID);
		when(mockTemplateUtil.getTemplatesPath(COMPANY_ID, SCOPE_GROUP_ID, CLASSNAME_ID)).thenReturn(TEMPLATES_PATH);
		when(PermissionThreadLocal.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(StringUtil.randomId()).thenReturn(RANDOM_ID);
	}
}