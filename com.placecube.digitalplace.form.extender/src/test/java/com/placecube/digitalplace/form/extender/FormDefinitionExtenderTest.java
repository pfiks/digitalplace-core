package com.placecube.digitalplace.form.extender;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Dictionary;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.form.extender.internal.FormDefinitionExtenderService;
import com.placecube.digitalplace.form.model.FormDefinition;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class FormDefinitionExtenderTest {

	@InjectMocks
	private FormDefinitionExtender formDefinitionExtender;

	@Mock
	private FormDefinitionExtenderService mockDefinitionExtenderService;

	@Mock
	private Bundle mockBundle;

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private BundleEvent mockBundleEvent;

	@Mock
	private Dictionary<String, String> mockHeaders;

	@Mock
	private FormDefinition mockFormDefinition;

	@Test
	public void addingBundle_WhenIsValid_ThenFormDefinitionIsRegistered() {

		when(mockBundle.getHeaders()).thenReturn(mockHeaders);
		when(mockDefinitionExtenderService.isValidFormDefinition(mockHeaders)).thenReturn(true);
		when(mockDefinitionExtenderService.hasValidHeaders(mockHeaders)).thenReturn(true);
		when(mockDefinitionExtenderService.createFormDefinition(mockHeaders, mockBundle)).thenReturn(mockFormDefinition);

		formDefinitionExtender.activate(mockBundleContext);

		formDefinitionExtender.addingBundle(mockBundle, mockBundleEvent);

		verify(mockDefinitionExtenderService, times(1)).register(mockFormDefinition, mockBundleContext);

	}

	@Test
	public void addingBundle_WhenIsValid_ThenReturnsFormDefinitionRegistered() {

		when(mockBundle.getHeaders()).thenReturn(mockHeaders);
		when(mockDefinitionExtenderService.isValidFormDefinition(mockHeaders)).thenReturn(true);
		when(mockDefinitionExtenderService.hasValidHeaders(mockHeaders)).thenReturn(true);
		when(mockDefinitionExtenderService.createFormDefinition(mockHeaders, mockBundle)).thenReturn(mockFormDefinition);

		formDefinitionExtender.activate(mockBundleContext);

		FormDefinition result = formDefinitionExtender.addingBundle(mockBundle, mockBundleEvent);

		assertThat(result, sameInstance(mockFormDefinition));

	}

	@Test
	@Parameters({ "true,false", "false,true" })
	public void addingBundle_WhenIsNotValid_ThenNothingIsRegistered(boolean isValidDefinition, boolean validateHeaders) {

		when(mockBundle.getHeaders()).thenReturn(mockHeaders);
		when(mockDefinitionExtenderService.isValidFormDefinition(mockHeaders)).thenReturn(isValidDefinition);
		when(mockDefinitionExtenderService.hasValidHeaders(mockHeaders)).thenReturn(validateHeaders);

		formDefinitionExtender.addingBundle(mockBundle, mockBundleEvent);

		verify(mockDefinitionExtenderService, never()).createFormDefinition(any(Dictionary.class), any(Bundle.class));

		verify(mockDefinitionExtenderService, never()).register(any(FormDefinition.class), any(BundleContext.class));
	}

	@Test
	public void modifiedBundle_WhenNoError_ThenThereIsNoInteractions() {

		formDefinitionExtender.modifiedBundle(mockBundle, mockBundleEvent, mockFormDefinition);

		verifyZeroInteractions(mockBundle, mockBundleEvent, mockFormDefinition);
	}

	@Test
	public void removedBundle_WhenNoError_ThenUnregisterFormDefinition() {

		formDefinitionExtender.removedBundle(mockBundle, mockBundleEvent, mockFormDefinition);

		verify(mockDefinitionExtenderService, times(1)).unregister(mockFormDefinition);

	}

	@Test
	public void activate_WhenNoError_ThenOpenBundleTracker() {

		formDefinitionExtender.activate(mockBundleContext);

		verify(mockDefinitionExtenderService, times(1)).openBundleTracker(mockBundleContext, formDefinitionExtender);
	}

	@Test
	public void deactivate_WheNoError_ThenCloseBundleTracker() {

		formDefinitionExtender.deactivate();

		verify(mockDefinitionExtenderService, times(1)).closeBundleTracker();
	}
}
