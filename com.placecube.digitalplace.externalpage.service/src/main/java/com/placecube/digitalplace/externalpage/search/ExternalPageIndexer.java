package com.placecube.digitalplace.externalpage.search;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlParser;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.model.uid.UIDFactory;
import com.liferay.portal.search.spi.model.result.contributor.ModelSummaryContributor;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;

@Component(enabled = true, immediate = true, service = Indexer.class)
public class ExternalPageIndexer extends BaseIndexer<ExternalPage> {

	public static final String CLASS_NAME = ExternalPage.class.getName();

	@Reference
	private IndexWriterHelper indexWriterHelper;

	@Reference(target = "(indexer.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage)")
	private ModelSummaryContributor modelSummaryContributor;

	@Reference
	private UIDFactory uidFactory;

	@Reference
	private DynamicQueryBatchIndexingActionableFactory dynamicQueryBatchIndexingActionableFactory;

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Reference
	private HtmlParser htmlParser;

	@Override
	public String getClassName() {
		return CLASS_NAME;
	}

	@Override
	protected void doDelete(ExternalPage externalPage) throws Exception {
		super.deleteDocument(externalPage.getCompanyId(), "UID=" + externalPage.getUuid());
	}

	@Override
	protected Document doGetDocument(ExternalPage externalPage) throws Exception {
		Document document = null;
		if (externalPage.getStatus()) {
			document = getBaseModelDocument(CLASS_NAME, externalPage);
			document.addKeyword(Field.UID, externalPage.getUuid());
			document.addKeyword(Field.ENTRY_CLASS_NAME, ExternalPage.class.getName());
			document.addKeyword(Field.CLASS_PK, externalPage.getExternalPageId());
			document.addKeyword(Field.COMPANY_ID, externalPage.getCompanyId());
			document.addKeyword(Field.GROUP_ID, externalPage.getGroupId());
			document.addKeyword(Field.SCOPE_GROUP_ID, externalPage.getGroupId());
			document.addKeyword(Field.USER_ID, externalPage.getUserId());
			document.addKeyword(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
			document.addKeyword(Field.URL, externalPage.getUrl());
			document.addText(Field.TITLE, htmlParser.extractText(externalPage.getTitle()));
			document.addText(Field.CONTENT, htmlParser.extractText(externalPage.getContent()));
			document.addDate(Field.CREATE_DATE, externalPage.getCreateDate());
			document.addDate(Field.DISPLAY_DATE, externalPage.getCreateDate());
			document.addDate(Field.PUBLISH_DATE, externalPage.getCreateDate());
			document.addDate(Field.MODIFIED_DATE, externalPage.getModifiedDate());
		}
		return document;
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletRequest portletRequest, PortletResponse portletResponse) throws Exception {
		Summary summary = new Summary(document.get(Field.TITLE), document.get(Field.CONTENT));
		summary.setMaxContentLength(200);
		return summary;
	}

	@Override
	protected void doReindex(ExternalPage externalPage) throws Exception {
		reindexExternalPage(externalPage);
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		if (ExternalPage.class.getName().equals(className)) {
			ExternalPage externalPage = externalPageLocalService.getExternalPage(classPK);
			reindexExternalPage(externalPage);
		}
	}

	@Override
	protected void doReindex(String[] ids) throws Exception {
		if (ids.length > 0) {
			long companyId = GetterUtil.getLong(ids[0]);

			reindexExternalPage(companyId);
		}
	}

	private void reindexExternalPage(ExternalPage externalPage) throws Exception {
		if (Validator.isNull(externalPage)) {
			return;
		}
		if (externalPage.getStatus()) {
			indexWriterHelper.updateDocument(externalPage.getCompanyId(), getDocument(externalPage));
		} else {
			doDelete(externalPage);
		}
	}

	private void reindexExternalPage(long companyId) throws PortalException {
		IndexableActionableDynamicQuery indexableActionableDynamicQuery = externalPageLocalService.getIndexableActionableDynamicQuery();

		indexableActionableDynamicQuery.setCompanyId(companyId);
		indexableActionableDynamicQuery.setPerformActionMethod((ExternalPage externalPage) -> indexableActionableDynamicQuery.addDocuments(getDocument(externalPage)));

		indexableActionableDynamicQuery.performActions();
	}

}
