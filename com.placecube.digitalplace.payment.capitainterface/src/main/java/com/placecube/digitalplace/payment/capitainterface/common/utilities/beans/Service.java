package com.placecube.digitalplace.payment.capitainterface.common.utilities.beans;

/**
 * A definition of the Liferay Services where an error could surface.
 *
 */
public enum Service {
		PARKING,
		CONCESSIONARY_TRAVEL,
		CASE_MANAGEMENT,
		CUSTOMER_ACCOUNT,
		CONTENT,
		EQUALITIES,
		COMMON_LIBRARY,
		ADDRESS_VERIFICATION,
		REGISTRATIONS,
		REPAIRS,
		STREET_FAULTS,
		WASTE,
		EVIDENCE,
		FILE_MANAGEMENT,
		CONTACT_MAYOR,
		POLLING_STATION_FINDER,
		BUSINESS_RATES,
		TENANT,
		LOCAL_TAX
}



