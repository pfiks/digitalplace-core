package com.placecube.digitalplace.externalpage.internal.upgrade.upgrade_1_1_1;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class ExternalPageTableUpdate extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(ExternalPageTableUpdate.class);

	@Override
	protected void doUpgrade() throws Exception {
		LOG.info("Initialised upgrade process for Placecube_ExternalPage_ExternalPage table");

		if (!hasColumn("Placecube_ExternalPage_ExternalPage", "status")) {
			runSQLTemplateString("alter table Placecube_ExternalPage_ExternalPage add column status BOOLEAN after content;", false);
			LOG.info("Added column status");
		}

		if (!hasIndex("Placecube_ExternalPage_ExternalPage", "IX_F5FC6FD1")) {
			runSQLTemplateString("create index IX_F5FC6FD1 on Placecube_ExternalPage_ExternalPage (externalPageId, status);", false);
			LOG.info("Added index IX_F5FC6FD1");
		}

		LOG.info("Completed upgrade process for Placecube_ExternalPage_ExternalPage table");
	}

}
