package com.placecube.digitalplace.group.rest.exception;

@SuppressWarnings("serial")
public class GroupRestKeyException extends Exception {

	public GroupRestKeyException() {
		super();
	}

	public GroupRestKeyException(Exception e) {
		super(e);
	}

	public GroupRestKeyException(String msg) {
		super(msg);
	}

}
