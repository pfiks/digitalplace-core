package com.placecube.digitalplace.siteinitializeradmin.constants;

public final class PortletRequestKeys {

	public static final String SELECTED_SITE_INITIALIZER = "selectedSiteInitializer";

	public static final String SITE_INITIALIZERS = "siteInitializers";

	private PortletRequestKeys() {
		return;
	}
}