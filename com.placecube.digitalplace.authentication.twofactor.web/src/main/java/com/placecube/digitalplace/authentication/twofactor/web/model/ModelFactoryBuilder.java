package com.placecube.digitalplace.authentication.twofactor.web.model;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.osgi.service.component.annotations.Component;

import com.liferay.mail.kernel.model.MailMessage;

@Component(immediate = true, service = ModelFactoryBuilder.class)
public class ModelFactoryBuilder {

	public InternetAddress createInternetAddress(String address) throws AddressException {
		return new InternetAddress(address);
	}

	public MailMessage createMailMessage() {
		return new MailMessage();
	}
}