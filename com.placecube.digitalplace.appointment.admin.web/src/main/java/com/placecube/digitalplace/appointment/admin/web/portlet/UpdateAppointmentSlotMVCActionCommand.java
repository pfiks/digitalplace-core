package com.placecube.digitalplace.appointment.admin.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.admin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.admin.web.constants.PortletKeys;
import com.placecube.digitalplace.appointment.admin.web.model.AppointmentSlotForm;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.APPOINTMENTS_MANAGER, "mvc.command.name=" + MVCCommandKeys.UPDATE_SLOT }, service = MVCActionCommand.class)
public class UpdateAppointmentSlotMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(UpdateAppointmentSlotMVCActionCommand.class);

	@Reference
	private AppointmentsManagerService appointmentsManagerService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try {
			AppointmentSlotForm appointmentSlotForm = appointmentsManagerService.getAppointmentSlotFormFromRequest(actionRequest);
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

			if (appointmentSlotForm.isNew()) {
				appointmentsManagerService.createNewAppointmentSlot(appointmentSlotForm, themeDisplay.getUser());
			} else {
				appointmentsManagerService.updateAppointmentSlot(appointmentSlotForm, themeDisplay.getTimeZone());
			}

			actionResponse.getRenderParameters().setValue("serviceId", appointmentSlotForm.getServiceId());

		} catch (Exception e) {
			LOG.error(e);

			String stackTrace = ExceptionUtils.getStackTrace(e);
			SessionErrors.add(actionRequest, "appointment-slot-update-error", stackTrace);
		}

		actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_ALL_SLOTS);
	}

}
