package com.placecube.digitalplace.user.account.web.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

@RunWith(PowerMockRunner.class)
public class AccountUserFieldsServiceImplTest extends PowerMockito {

	private final String ACCOUNT_FIELD_KEY = "user-account-key";

	private final String ACCOUNT_FIELD_VALUE = "user-account-value";

	@InjectMocks
	private AccountUserFieldsServiceImpl accountUserFieldsServiceImpl;

	private final long COMPANY_ID = 1L;

	private final String EXPANDO_KEY = "expando-key";

	private final String EXPANDO_VALUE = "expando-value";

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private ExpandoBridge mockExpandoBridge;

	@Mock
	private ExpandoField mockExpandoField;

	@Mock
	private User mockUser;

	@Mock
	private UserAccountField mockUserAccountField;

	@Mock
	private UserAccountFieldRegistry mockUserAccountFieldRegistry;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Before
	public void activateSetup() {
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);
	}

	@Test
	public void populateContextWithExistingAccountFields_WhenNoErrors_ThenSetAccountFieldValuesOnContext() {
		List<UserAccountField> accountFields = new ArrayList<>();
		accountFields.add(mockUserAccountField);

		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserAccountField.getExpandoFieldName()).thenReturn(ACCOUNT_FIELD_KEY);
		when(mockExpandoBridge.getAttribute(ACCOUNT_FIELD_KEY)).thenReturn(ACCOUNT_FIELD_VALUE);
		when(mockUserAccountFieldRegistry.getUserAccountFields(COMPANY_ID)).thenReturn(accountFields);
		accountUserFieldsServiceImpl.populateContextWithExistingAccountFields(mockAccountContext, mockUser);

		verify(mockAccountContext, times(1)).addExpandoField(ACCOUNT_FIELD_KEY, ACCOUNT_FIELD_VALUE);
	}

	@Test
	public void populateContextWithExistingConfiguredExpandoFields_WhenNoErrors_ThenSetExpandoFieldValuesOnContext() {
		Set<ExpandoField> expandoFields = new HashSet<>();
		expandoFields.add(mockExpandoField);

		when(mockExpandoField.getExpandoFieldKey()).thenReturn(EXPANDO_KEY);
		when(mockExpandoBridge.getAttribute(EXPANDO_KEY)).thenReturn(EXPANDO_VALUE);
		when(mockAccountConfigurationService.getConfiguredExpandoFields(mockAccountContext, mockUserProfileFieldsConfiguration)).thenReturn(expandoFields);
		when(mockUser.getExpandoBridge()).thenReturn(mockExpandoBridge);

		accountUserFieldsServiceImpl.populateContextWithExistingConfiguredExpandoFields(mockAccountContext, mockUser, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addExpandoField(EXPANDO_KEY, EXPANDO_VALUE);
	}

}
