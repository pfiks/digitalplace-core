package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch;

import java.util.List;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.AdvancePeopleSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.model.UserSearchField;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service.AdvancePeopleSearchConfigurationService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.ADVANCE_PEOPLE_SEARCH, "mvc.command.name=" + MVCCommandKeys.SEARCH }, service = MVCActionCommand.class)
public class AdvancePeopleSearchMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private AdvancePeopleSearchConfigurationService advancePeopleSearchConfigurationService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String cmd = ParamUtil.getString(actionRequest, "cmd");
		String filterColour = ParamUtil.getString(actionRequest, "filterColour");

		Optional<AdvancePeopleSearchPortletInstanceConfiguration> configurationOptional = advancePeopleSearchConfigurationService.getValidConfiguration(themeDisplay);

		if (configurationOptional.isPresent()) {
			List<UserSearchField> searchFields = advancePeopleSearchConfigurationService
					.getSearchFields(themeDisplay.getCompanyId(), themeDisplay.getLocale(), configurationOptional.get().searchFields());

			if ("clearAll".equals(cmd)) {
				searchFields.forEach(userSearchField -> sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, userSearchField.getField()));
			} else {
				searchFields.forEach(userSearchField -> {
					String value = ParamUtil.getString(actionRequest, userSearchField.getField());
					sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, userSearchField.getField());
					if (Validator.isNotNull(value)) {
						sharedSearchRequestService.addSharedActiveFilter(actionRequest, userSearchField.getField(), value, userSearchField.getLabel(), filterColour);
					}
				});
			}
		}

		sharedSearchRequestService.sendRedirectFromParameter(actionRequest, actionResponse, "redirectURL");
	}
}
