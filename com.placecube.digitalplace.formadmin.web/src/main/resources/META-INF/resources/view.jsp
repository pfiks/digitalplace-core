<%@ include file="init.jsp" %>

<div class="container-fluid container-fluid-max-xl">
	<liferay-ui:search-container
			deltaConfigurable="${ searchContainer.isDeltaConfigurable() }"
			searchContainer="${ searchContainer }"
			total="${ searchContainer.getTotal() }"
			emptyResultsMessage="${ searchContainer.getEmptyResultsMessage() }"
			emptyResultsMessageCssClass="${ searchContainer.getEmptyResultsMessageCssClass() }">
		<liferay-ui:search-container-results results="${ searchContainer.getResults() }" />
	
		<liferay-ui:search-container-row className="com.placecube.digitalplace.formadmin.web.model.FormDefinitionResult" modelVar="formDefinitionResult">
			<liferay-ui:search-container-column-text name="category" value="${ formDefinitionResult.category }"/>
			<liferay-ui:search-container-column-text name="name" value="${ formDefinitionResult.name }"/>
			<liferay-ui:search-container-column-text name="description" value="${ formDefinitionResult.description }"/>
			
			<liferay-ui:search-container-column-text name="status">
			
				<c:choose>
					<c:when test="${ !formDefinitionResult.installed }">
							<portlet:actionURL var="installFormDefinitionURL" name="<%=MVCCommandKeys.INSTALL_FORM_DEFINTION %>">
								<portlet:param name="<%=PortletRequestKeys.FORM_DEFINITION_KEY %>" value="${ formDefinitionResult.key }"/>
							</portlet:actionURL>	
							<aui:button value="install" onClick="${ installFormDefinitionURL }"/>			
					</c:when>
					<c:otherwise>
						<span class="label status approved">
							<liferay-ui:message key="installed"/>
						</span>
					</c:otherwise>
				</c:choose>
			</liferay-ui:search-container-column-text>
			
		</liferay-ui:search-container-row>
	
		<liferay-ui:search-iterator paginate="true" displayStyle="list" markupView="lexicon" searchContainer="${ searchContainer }"  />

	</liferay-ui:search-container>

</div>