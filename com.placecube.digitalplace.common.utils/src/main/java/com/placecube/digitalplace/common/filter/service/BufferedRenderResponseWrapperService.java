package com.placecube.digitalplace.common.filter.service;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.common.filter.model.BufferedRenderResponseWrapper;

import javax.portlet.RenderResponse;

@Component(immediate = true, service = BufferedRenderResponseWrapperService.class)
public class BufferedRenderResponseWrapperService {

	public BufferedRenderResponseWrapper getBufferedRenderResponseWrapper(RenderResponse renderResponse) {

		if (renderResponse == null) {

			throw new IllegalArgumentException("renderResponse can not be null");

		} else {

			return new BufferedRenderResponseWrapper(renderResponse);

		}

	}

}
