package com.placecube.digitalplace.payment.govuk.service.internal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.payment.model.PaymentContext;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({ "javax.net.ssl.*" })
@PrepareForTest({ EntityUtils.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class GovUKPaymentOrderFactoryTest extends PowerMockito {

	private static final String AUTH_KEY = "AUTH_KEY";
	private static final String GOVUK_PAYMENT_ID = "payment_id";
	private static final String GOVUK_PAYMENT_SUCCESS_STATE = "SUCCESS";
	private static final String REQUEST_JSON = "REQUEST_JSON";
	private static final int SUCCESS_STATUS_CODE = 200;
	private static final String URL = "URL";

	@InjectMocks
	private GovUKPaymentOrderFactory govUKPaymentOrderFactory;

	@Mock
	private JSONObject mockBillingAddressJsonObject;

	@Mock
	private JSONObject mockCardHolderDetailsJsonObject;

	@Mock
	private CloseableHttpClient mockCloseableHttpClient;

	@Mock
	private HttpEntity mockHttpEntity;

	@Mock
	private HttpPost mockHttpPost;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpSession mockHttpSession;

	@Mock
	private HttpUriRequest mockHttpUriRequest;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private JSONObject mockLinksJsonObject;

	@Mock
	private JSONObject mockNextURLJsonObject;

	@Mock
	private PaymentContext mockPaymentContext;

	@Mock
	private JSONObject mockPaymentJsonObject;

	@Mock
	private Portal mockPortal;

	@Mock
	private CloseableHttpResponse mockResponse;

	@Mock
	private StatusLine mockStatusLine;

	@Before
	public void activateSetUp() {
		mockStatic(EntityUtils.class);
	}

	@Test
	public void createGovUKPaymentRequest_WhenNoErrors_ThenCreateGovUKPaymentRequest() throws Exception {
		String uprn = "uprn";
		String addressLine1 = "addressLine1";
		String addressLine2 = "addressLine2";
		String addressLine3 = "addressLine3";
		String city = "city";
		String postcode = "postcode";
		AddressContext addressContext = new AddressContext(uprn, addressLine1, addressLine2, addressLine3, StringPool.BLANK, city, postcode);
		String saleDescription = "SaleDescription";
		String itemReference = "ItemReference";
		String emailAddress = "EmailAddress";

		int amountInMinorUnits = 100;
		String returnUrl = "ReturnURL";
		String firstName = "Firstname";
		String lastName = "Lastname";

		when(mockCardHolderDetailsJsonObject.getString("cardholder_name")).thenReturn(firstName + StringPool.SPACE + lastName);

		when(mockBillingAddressJsonObject.getString("line1")).thenReturn(addressLine1);
		when(mockBillingAddressJsonObject.getString("line2")).thenReturn(addressLine2 + StringPool.SPACE + addressLine3);
		when(mockBillingAddressJsonObject.getString("city")).thenReturn(city);
		when(mockBillingAddressJsonObject.getString("country")).thenReturn(uprn);
		when(mockBillingAddressJsonObject.getString("postcode")).thenReturn(postcode);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);
		when(mockJsonObject.getString("email")).thenReturn(emailAddress);
		when(mockJsonObject.getString("description")).thenReturn(saleDescription);
		when(mockJsonObject.getString("reference")).thenReturn(itemReference);
		when(mockJsonObject.getString("return_url")).thenReturn(returnUrl);
		when(mockJsonObject.getInt("amount")).thenReturn(amountInMinorUnits);

		when(mockJsonObject.getJSONObject("prefilled_cardholder_details")).thenReturn(mockCardHolderDetailsJsonObject);
		when(mockCardHolderDetailsJsonObject.getJSONObject("billing_address")).thenReturn(mockBillingAddressJsonObject);

		when(mockPaymentContext.getAmount()).thenReturn(new BigDecimal(1));
		when(mockPaymentContext.getAddress()).thenReturn(addressContext);
		when(mockPaymentContext.getSalesDescription()).thenReturn(saleDescription);
		when(mockPaymentContext.getItemReference()).thenReturn(itemReference);
		when(mockPaymentContext.getEmailAddress()).thenReturn(emailAddress);
		when(mockPaymentContext.getReturnURL()).thenReturn(returnUrl);
		when(mockPaymentContext.getFirstName()).thenReturn(firstName);
		when(mockPaymentContext.getLastName()).thenReturn(lastName);

		JSONObject requestJsonObject = govUKPaymentOrderFactory.createGovUKPaymentRequest(mockPaymentContext);

		JSONObject card_holder_details = requestJsonObject.getJSONObject("prefilled_cardholder_details");

		assertEquals(firstName + StringPool.SPACE + lastName, card_holder_details.getString("cardholder_name"));

		JSONObject billing_address = card_holder_details.getJSONObject("billing_address");

		assertEquals(addressLine1, billing_address.getString("line1"));
		assertEquals(addressLine2 + StringPool.SPACE + addressLine3, billing_address.getString("line2"));
		assertEquals(city, billing_address.getString("city"));
		assertEquals(postcode, billing_address.getString("postcode"));
		assertEquals(uprn, billing_address.getString("country"));

		assertEquals(emailAddress, requestJsonObject.getString("email"));
		assertEquals(saleDescription, requestJsonObject.getString("description"));
		assertEquals(itemReference, requestJsonObject.getString("reference"));
		assertEquals(returnUrl, requestJsonObject.getString("return_url"));
		assertEquals(amountInMinorUnits, requestJsonObject.getInt("amount"));
	}

	@Test(expected = ClientProtocolException.class)
	public void executeRequest_WhenClientProtocolException_ThenThrowException() throws ClientProtocolException, IOException {
		when(mockCloseableHttpClient.execute(mockHttpUriRequest)).thenThrow(ClientProtocolException.class);
		govUKPaymentOrderFactory.executeRequest(mockHttpUriRequest, mockCloseableHttpClient);
	}

	@Test(expected = IOException.class)
	public void executeRequest_WhenIOException_ThenThrowException() throws ClientProtocolException, IOException {
		when(mockCloseableHttpClient.execute(mockHttpUriRequest)).thenThrow(IOException.class);
		govUKPaymentOrderFactory.executeRequest(mockHttpUriRequest, mockCloseableHttpClient);
	}

	@Test
	public void executeRequest_WhenSuccess_ThenReturnCloseableHttpResponse() throws ClientProtocolException, IOException {
		when(mockCloseableHttpClient.execute(mockHttpUriRequest)).thenReturn(mockResponse);
		CloseableHttpResponse closeableHttpResponse = govUKPaymentOrderFactory.executeRequest(mockHttpUriRequest, mockCloseableHttpClient);

		assertEquals(mockResponse, closeableHttpResponse);
	}

	@Test
	public void getHttpClient_WhenCloseableHttpClientCreatedSuccessfully_ThenReturnCloseableHttpClient() {
		CloseableHttpClient closeableHttpClient = govUKPaymentOrderFactory.getHttpClient();
		assertNotNull(closeableHttpClient);
	}

	@Test
	public void getHttpGetMethod_WhenHttpGetObjectCreatedSuccessfully_ThenReturnHttpGet() throws Exception {
		HttpGet httpGet = govUKPaymentOrderFactory.getHttpGetMethod(URL, AUTH_KEY);
		assertNotNull(httpGet);
	}

	@Test
	public void getHttpPostMethod_WhenHttpPostObjectCreatedSuccessfully_ThenReturnHttpPost() throws Exception {
		PowerMockito.whenNew(HttpPost.class).withAnyArguments().thenReturn(mockHttpPost);
		when(mockJsonObject.toJSONString()).thenReturn(REQUEST_JSON);
		HttpPost httpPost = govUKPaymentOrderFactory.getHttpPostMethod(URL, AUTH_KEY, mockJsonObject);
		assertNotNull(httpPost);
	}

	@Test
	public void getPaymentIdFromSession_WhenNullPointerException_ThenReturnBlankPaymentId() {
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(GOVUK_PAYMENT_ID)).thenThrow(new NullPointerException());

		String paymentId = govUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest);

		assertEquals(StringPool.BLANK, paymentId);
	}

	@Test
	public void getPaymentIdFromSession_WhenPaymentIdIsRetrievedFromSessionSuccessfully_ThenReturnPaymentId() {
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(GOVUK_PAYMENT_ID)).thenReturn(GOVUK_PAYMENT_ID);

		String paymentId = govUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest);

		assertEquals(GOVUK_PAYMENT_ID, paymentId);
	}

	@Test
	public void getPaymentURLJSON_WhenJSONException_ThenReturnNull() throws ParseException, IOException, JSONException {
		String result = StringPool.BLANK;
		when(mockResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(result);
		when(mockJsonFactory.createJSONObject(result)).thenThrow(new JSONException());

		JSONObject responseJson = govUKPaymentOrderFactory.getPaymentURLJSON(mockResponse);

		assertNull(responseJson);
	}

	@Test
	public void getPaymentURLJSON_WhenSuccessfullyAddedNextURLAndPaymentIdInJSON_ThenReturnJsonObject() throws ParseException, IOException, JSONException {
		String result = "result";
		when(mockResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(result);
		when(mockJsonFactory.createJSONObject(result)).thenReturn(mockJsonObject);
		when(mockJsonObject.getJSONObject("_links")).thenReturn(mockLinksJsonObject);
		when(mockJsonObject.getString(GOVUK_PAYMENT_ID)).thenReturn(GOVUK_PAYMENT_ID);
		when(mockLinksJsonObject.getJSONObject("next_url")).thenReturn(mockNextURLJsonObject);
		when(mockNextURLJsonObject.getString(GOVUK_PAYMENT_ID)).thenReturn(GOVUK_PAYMENT_ID);

		JSONObject responseJson = govUKPaymentOrderFactory.getPaymentURLJSON(mockResponse);

		assertNotNull(responseJson);
		assertEquals(GOVUK_PAYMENT_ID, responseJson.getString(GOVUK_PAYMENT_ID));
	}

	@Test
	public void getResponseStatus_WhenJSONException_ThenReturnBlankPaymentStatus() throws JSONException, ParseException, IOException {
		String result = StringPool.BLANK;
		when(mockResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(result);
		when(mockJsonFactory.createJSONObject(result)).thenThrow(new JSONException());

		String status = govUKPaymentOrderFactory.getResponseStatus(mockResponse);

		assertEquals(StringPool.BLANK, status);
	}

	@Test
	public void getResponseStatus_WhenPaymentIsSuccessful_ThenReturnPaymentSuccessStatus() throws JSONException, ParseException, IOException {
		String result = "result";
		when(mockResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(result);
		when(mockJsonFactory.createJSONObject(result)).thenReturn(mockJsonObject);
		when(mockJsonObject.getJSONObject("state")).thenReturn(mockPaymentJsonObject);
		when(mockPaymentJsonObject.getString("status")).thenReturn(GOVUK_PAYMENT_SUCCESS_STATE);

		String status = govUKPaymentOrderFactory.getResponseStatus(mockResponse);

		assertNotNull(status);
		assertEquals(GOVUK_PAYMENT_SUCCESS_STATE, status);
	}

	@Test
	public void getResponseStatusCode_WhenNullPointerException_ThenReturnPaymentFailedStatusCode() throws JSONException {
		when(mockResponse.getStatusLine()).thenReturn(mockStatusLine);
		when(mockStatusLine.getStatusCode()).thenThrow(new NullPointerException());

		int statusCode = govUKPaymentOrderFactory.getResponseStatusCode(mockResponse);

		assertNotEquals(SUCCESS_STATUS_CODE, statusCode);
	}

	@Test
	public void getResponseStatusCode_WhenPaymentIsSuccessfull_ThenReturnPaymentSuccessStatusCode() throws JSONException {
		when(mockResponse.getStatusLine()).thenReturn(mockStatusLine);
		when(mockStatusLine.getStatusCode()).thenReturn(SUCCESS_STATUS_CODE);

		int statusCode = govUKPaymentOrderFactory.getResponseStatusCode(mockResponse);

		assertEquals(SUCCESS_STATUS_CODE, statusCode);
	}

	@Test
	public void getResponseStatusJSON_WhenJSONException_ThenReturnEmptyJSONObject() throws JSONException, ParseException, IOException {
		JSONObject result = mockJsonFactory.createJSONObject();
		when(mockResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(StringPool.BLANK);
		when(mockJsonFactory.createJSONObject(StringPool.BLANK)).thenThrow(new JSONException());

		JSONObject status = govUKPaymentOrderFactory.getResponseStatusJSON(mockResponse);

		assertEquals(result, status);
	}

	@Test
	public void getResponseStatusJSON_WhenNoError_ThenReturnStatusJSONObject() throws JSONException, ParseException, IOException {
		when(mockResponse.getEntity()).thenReturn(mockHttpEntity);
		when(EntityUtils.toString(mockHttpEntity)).thenReturn(StringPool.BLANK);
		when(mockJsonFactory.createJSONObject(StringPool.BLANK)).thenReturn(mockJsonObject);
		when(mockJsonObject.getJSONObject("state")).thenReturn(mockPaymentJsonObject);

		JSONObject status = govUKPaymentOrderFactory.getResponseStatusJSON(mockResponse);

		assertNotNull(status);
		assertEquals(mockPaymentJsonObject, status);
	}

	@Test
	public void quietlyCloseResources_WhenExceptionClosingResponse_ThenClosesClient() throws IOException {
		doThrow(new IOException()).when(mockResponse).close();

		govUKPaymentOrderFactory.quietlyCloseResources(mockCloseableHttpClient, mockResponse);

		verify(mockCloseableHttpClient, times(1)).close();
	}

	@Test
	public void quietlyCloseResources_WhenExceptionClosingResponseAndClient_ThenNoErrorIsThrown() throws IOException {
		doThrow(new IOException()).when(mockResponse).close();
		doThrow(new IOException()).when(mockCloseableHttpClient).close();

		try {
			govUKPaymentOrderFactory.quietlyCloseResources(mockCloseableHttpClient, mockResponse);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void quietlyCloseResources_WhenNoError_ThenClosesTheResponseAndTheClient() throws IOException {
		govUKPaymentOrderFactory.quietlyCloseResources(mockCloseableHttpClient, mockResponse);

		verify(mockResponse, times(1)).close();
		verify(mockCloseableHttpClient, times(1)).close();
	}
}
