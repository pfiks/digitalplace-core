package com.placecube.digitalplace.user.account.fields.model;

import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.taglib.servlet.PipingServletResponseFactory;
import com.placecube.digitalplace.user.account.model.AccountContext;

public interface UserAccountField {

	void configureContextValue(AccountContext accountContext, PortletRequest portletRequest);

	default HttpServletResponse createPipingServletResponse(PageContext pageContext) {
		return PipingServletResponseFactory.createPipingServletResponse(pageContext);
	}

	AccountCreationStep getAccountCreationStep();

	String getBundleId();

	Map<String, String> getDependentRequiredFieldNamesAndMessage(String selectedValue, Locale locale);

	int getDisplayOrder();

	String getExpandoFieldName();

	String getRequiredMessage(Locale locale);

	boolean isEnabled(long companyId);

	boolean isRequired(long companyId);

	void render(HttpServletRequest request, HttpServletResponse response) throws PortalException;
}
