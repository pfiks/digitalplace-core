package com.placecube.digitalplace.group.rest.internal.processor;

import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.placecube.digitalplace.group.rest.constants.GroupRestKeyType;
import com.placecube.digitalplace.group.rest.constants.WebhookAction;
import com.placecube.digitalplace.group.rest.constants.WebhookEvent;
import com.placecube.digitalplace.group.rest.model.GroupWebhook;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;
import com.placecube.digitalplace.group.rest.service.GroupWebhookLocalService;

@Component(immediate = true, service = WebhookTriggerService.class)
public class WebhookTriggerService {

	private static final Log LOG = LogFactoryUtil.getLog(WebhookTriggerService.class);

	@Reference
	private GroupWebhookLocalService groupWebhookLocalService;

	@Reference
	private WebhookRequestUtil webhookRequestService;

	@Reference
	private GroupApiKeyLocalService groupApiKeyLocalService;

	public List<GroupWebhook> getConfiguredWebhooks(long groupId, WebhookEvent webhookEvent, long classPK, WebhookAction webhookAction) {
		return groupWebhookLocalService.getConfiguredGroupWebhooks(groupId, webhookEvent.getClassName(), classPK, webhookAction.name());
	}

	public void processWebhook(Group group, long userId, GroupWebhook webhook, WebhookAction webhookAction, long entryId, String entryName, Date entryDate) {
		String payloadURL = webhook.getPayloadURL();

		try {
			String applicationKey = groupApiKeyLocalService.getGroupRestKeyValue(group, GroupRestKeyType.APPLICATION_KEY);

			String body = webhookRequestService.buildRequestBody(group.getGroupId(), userId, webhookAction, entryId, entryName, entryDate);

			String dateHeaderValue = webhookRequestService.buildDateValue();

			String signature = webhookRequestService.buildSignature(group, applicationKey, dateHeaderValue, webhookAction, body, payloadURL);

			HttpPost httpPost = webhookRequestService.getHttpPost(payloadURL);
			HttpEntity stringEntity = webhookRequestService.buildStringEntity(body);
			webhookRequestService.configureHeaders(httpPost, applicationKey, dateHeaderValue, webhookAction, signature);
			webhookRequestService.configureEntity(httpPost, stringEntity);
			webhookRequestService.executePostCall(httpPost);

		} catch (Exception e) {
			LOG.error("Exception executing call to webhook: " + payloadURL, e);
		}
	}

}
