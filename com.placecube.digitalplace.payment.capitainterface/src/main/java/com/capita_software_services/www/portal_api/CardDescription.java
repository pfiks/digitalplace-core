/**
 * CardDescription.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.portal_api;

public class CardDescription implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected CardDescription(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "VISA";
    public static final java.lang.String _value2 = "MASTERCARD";
    public static final java.lang.String _value3 = "AMERICAN EXPRESS";
    public static final java.lang.String _value4 = "LASER";
    public static final java.lang.String _value5 = "DINERS";
    public static final java.lang.String _value6 = "JCBC";
    public static final CardDescription value1 = new CardDescription(_value1);
    public static final CardDescription value2 = new CardDescription(_value2);
    public static final CardDescription value3 = new CardDescription(_value3);
    public static final CardDescription value4 = new CardDescription(_value4);
    public static final CardDescription value5 = new CardDescription(_value5);
    public static final CardDescription value6 = new CardDescription(_value6);
    public java.lang.String getValue() { return _value_;}
    public static CardDescription fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        CardDescription enumeration = (CardDescription)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static CardDescription fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardDescription.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/portal-api", "cardDescription"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
