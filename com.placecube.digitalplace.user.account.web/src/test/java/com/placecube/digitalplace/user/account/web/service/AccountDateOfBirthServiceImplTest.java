package com.placecube.digitalplace.user.account.web.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, CalendarFactoryUtil.class, Calendar.class })
public class AccountDateOfBirthServiceImplTest extends PowerMockito {

	@InjectMocks
	private AccountDateOfBirthServiceImpl accountDateOfBirthServiceImpl;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private Calendar mockCalendar;

	@Mock
	private Contact mockContact;

	@Mock
	private Date mockDate;

	@Mock
	private User mockUser;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, CalendarFactoryUtil.class, Calendar.class);
	}

	@Test
	public void getDateOfBirthFromContext_WhenInValidDateParametersSupplied_ThenDefaultDateOfBirthReturned() {
		int invalidMonth = 13;
		when(mockAccountContext.getBirthYear()).thenReturn(2020);
		when(mockAccountContext.getBirthMonth()).thenReturn(invalidMonth);
		when(mockAccountContext.getBirthDay()).thenReturn(1);

		LocalDate dateOfBirth = accountDateOfBirthServiceImpl.getDateOfBirthFromContext(mockAccountContext);

		assertEquals(1800, dateOfBirth.getYear());
		assertEquals(1, dateOfBirth.getMonthValue());
		assertEquals(1, dateOfBirth.getDayOfMonth());

	}

	@Test
	public void getDateOfBirthFromContext_WhenValidDateParametersSupplied_ThenCorrectDateOfBirthReturned(){
		when(mockAccountContext.getBirthYear()).thenReturn(2020);
		when(mockAccountContext.getBirthMonth()).thenReturn(2);
		when(mockAccountContext.getBirthDay()).thenReturn(1);

		LocalDate dateOfBirth = accountDateOfBirthServiceImpl.getDateOfBirthFromContext(mockAccountContext);

		assertEquals(2020, dateOfBirth.getYear());
		assertEquals(2, dateOfBirth.getMonthValue());
		assertEquals(1, dateOfBirth.getDayOfMonth());

	}

	@Test
	public void getDefaultDateOfBirth_WhenNoErrors_ThenDefaultDateOfBirthReturned() {

		LocalDate dateOfBirth = accountDateOfBirthServiceImpl.getDefaultDateOfBirth();

		assertEquals(1800, dateOfBirth.getYear());
		assertEquals(1, dateOfBirth.getMonthValue());
		assertEquals(1, dateOfBirth.getDayOfMonth());

	}

	@Test
	public void populateContextWithExistingDateOfBirth_WhenNoErrors_ThenSetDateOfBirthOnContext() throws PortalException, UserAccountException {
		when(mockUser.getContact()).thenReturn(mockContact);
		when(CalendarFactoryUtil.getCalendar()).thenReturn(mockCalendar);
		when(mockContact.getBirthday()).thenReturn(mockDate);
		when(mockDate.getTime()).thenReturn(1l);
		int zeroBasedJanuary = 0;
		int oneBasedJanuary = 1;
		int day = 1;
		int year = 2020;
		when(mockCalendar.get(Calendar.DATE)).thenReturn(day);
		when(mockCalendar.get(Calendar.MONTH)).thenReturn(zeroBasedJanuary);
		when(mockCalendar.get(Calendar.YEAR)).thenReturn(year);

		accountDateOfBirthServiceImpl.populateContextWithExistingDateOfBirth(mockAccountContext, mockUser);

		verify(mockCalendar, times(1)).setTimeInMillis(mockDate.getTime());
		verify(mockAccountContext, times(1)).setBirthDay(day);
		verify(mockAccountContext, times(1)).setBirthMonth(oneBasedJanuary);
		verify(mockAccountContext, times(1)).setBirthYear(year);

	}

}
