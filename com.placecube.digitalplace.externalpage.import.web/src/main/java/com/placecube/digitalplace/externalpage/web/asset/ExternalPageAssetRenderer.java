package com.placecube.digitalplace.externalpage.web.asset;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;

import com.liferay.asset.kernel.model.BaseJSPAssetRenderer;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.trash.TrashRenderer;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;

public class ExternalPageAssetRenderer extends BaseJSPAssetRenderer<ExternalPage> implements TrashRenderer {

	private ExternalPage externalPage;

	public ExternalPageAssetRenderer(ExternalPage externalPage) {
		this.externalPage = externalPage;
	}

	@Override
	public ExternalPage getAssetObject() {
		return externalPage;
	}

	@Override
	public String getClassName() {
		return ExternalPage.class.getName();
	}

	@Override
	public long getClassPK() {
		return externalPage.getExternalPageId();
	}

	@Override
	public long getGroupId() {
		return externalPage.getGroupId();
	}

	@Override
	public String getJspPath(HttpServletRequest httpServletRequest, String template) {
		return null;
	}

	@Override
	public String getPortletId() {
		return getAssetRendererFactory().getPortletId();
	}

	@Override
	public String getSummary(PortletRequest portletRequest, PortletResponse portletResponse) {
		String summary = HtmlUtil.escape(externalPage.getContent());
		return StringUtil.shorten(summary, 200);
	}

	@Override
	public String getTitle(Locale locale) {
		return externalPage.getTitle();
	}

	@Override
	public String getType() {
		return getAssetRendererFactory().getType();
	}

	@Override
	public String getUrlTitle() {
		return externalPage.getUrl();
	}

	@Override
	public String getURLView(LiferayPortletResponse liferayPortletResponse, WindowState windowState) {
		return externalPage.getUrl();
	}

	@Override
	public String getURLViewInContext(LiferayPortletRequest liferayPortletRequest, LiferayPortletResponse liferayPortletResponse, String noSuchEntryRedirect) {
		return externalPage.getUrl();
	}

	@Override
	public long getUserId() {
		return externalPage.getUserId();
	}

	@Override
	public String getUserName() {
		return StringPool.BLANK;
	}

	@Override
	public String getUuid() {
		return externalPage.getUuid();
	}

}
