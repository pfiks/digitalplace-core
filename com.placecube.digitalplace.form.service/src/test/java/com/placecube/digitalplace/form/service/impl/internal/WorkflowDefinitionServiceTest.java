package com.placecube.digitalplace.form.service.impl.internal;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceSettings;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.util.DDMFormInstanceFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.WorkflowDefinitionLink;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.WorkflowDefinitionLinkLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DDMFormInstanceFactory.class })
public class WorkflowDefinitionServiceTest {

	@Mock
	private DDMFormInstanceSettings mockDDMFormInstanceSettings;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMFormInstance mockDDMormInstance;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private WorkflowDefinitionLink mockWorkflowDefinitionLink;

	@Mock
	private WorkflowDefinitionLinkLocalService mockWorkflowDefinitionLinkLocalService;

	@InjectMocks
	private WorkflowDefinitionService workflowDefinitionService;

	@Before
	public void setUp() {

		mockStatic(DDMFormInstanceFactory.class);

	}

	@Test(expected = PortalException.class)
	public void updateWorkflowDefinitionLink_WhenErrorUpdatingWorkflowDefinitionLink_ThenPortalExceptionIsThrown() throws PortalException {

		when(DDMFormInstanceFactory.create(DDMFormInstanceSettings.class, mockDDMFormValues)).thenReturn(mockDDMFormInstanceSettings);
		String workflowDefintion = "workflow";
		when(mockDDMFormInstanceSettings.workflowDefinition()).thenReturn(workflowDefintion);

		long userId = 32;
		long companyId = 123;
		long groupId = 49;
		long formInstanceId = 90;
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockServiceContext.getCompanyId()).thenReturn(companyId);
		when(mockDDMormInstance.getGroupId()).thenReturn(groupId);
		when(mockDDMormInstance.getFormInstanceId()).thenReturn(formInstanceId);

		when(mockWorkflowDefinitionLinkLocalService.fetchWorkflowDefinitionLink(companyId, groupId, DDMFormInstance.class.getName(), formInstanceId, 0)).thenReturn(mockWorkflowDefinitionLink);

		doThrow(new PortalException()).when(mockWorkflowDefinitionLinkLocalService).updateWorkflowDefinitionLink(userId, companyId, groupId, DDMFormInstance.class.getName(), formInstanceId, 0,
				workflowDefintion);

		workflowDefinitionService.updateWorkflowDefinitionLink(mockDDMormInstance, mockDDMFormValues, mockServiceContext);

	}

	@Test
	public void updateWorkflowDefinitionLink_WhenThereIsNoWorkflowDefinition_ThenNothingIsUpdated() throws PortalException {

		when(DDMFormInstanceFactory.create(DDMFormInstanceSettings.class, mockDDMFormValues)).thenReturn(mockDDMFormInstanceSettings);
		when(mockDDMFormInstanceSettings.workflowDefinition()).thenReturn("no-workflow");

		workflowDefinitionService.updateWorkflowDefinitionLink(mockDDMormInstance, mockDDMFormValues, mockServiceContext);

		verifyNoMoreInteractions(mockWorkflowDefinitionLinkLocalService);

	}

	@Test
	public void updateWorkflowDefinitionLink_WhenThereIsWorkflowDefinitionAndWorkflowDefinitionLinkDoesNotExist_ThenAddWorkflowDefinitionLink() throws PortalException {

		when(DDMFormInstanceFactory.create(DDMFormInstanceSettings.class, mockDDMFormValues)).thenReturn(mockDDMFormInstanceSettings);
		String workflowDefintion = "workflow";
		when(mockDDMFormInstanceSettings.workflowDefinition()).thenReturn(workflowDefintion);

		long userId = 32;
		long companyId = 123;
		long groupId = 49;
		long formInstanceId = 90;
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockServiceContext.getCompanyId()).thenReturn(companyId);
		when(mockDDMormInstance.getGroupId()).thenReturn(groupId);
		when(mockDDMormInstance.getFormInstanceId()).thenReturn(formInstanceId);

		when(mockWorkflowDefinitionLinkLocalService.fetchWorkflowDefinitionLink(companyId, groupId, DDMFormInstance.class.getName(), formInstanceId, 0)).thenReturn(null);

		workflowDefinitionService.updateWorkflowDefinitionLink(mockDDMormInstance, mockDDMFormValues, mockServiceContext);

		verify(mockWorkflowDefinitionLinkLocalService, times(1)).addWorkflowDefinitionLink(userId, companyId, groupId, DDMFormInstance.class.getName(), formInstanceId, 0, workflowDefintion, 1);

	}

	@Test
	public void updateWorkflowDefinitionLink_WhenThereIsWorkflowDefinitionAndWorkflowDefinitionLinkExists_ThenUpdateWorkflowDefinitionLink() throws PortalException {

		when(DDMFormInstanceFactory.create(DDMFormInstanceSettings.class, mockDDMFormValues)).thenReturn(mockDDMFormInstanceSettings);
		String workflowDefintion = "workflow";
		when(mockDDMFormInstanceSettings.workflowDefinition()).thenReturn(workflowDefintion);

		long userId = 32;
		long companyId = 123;
		long groupId = 49;
		long formInstanceId = 90;
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockServiceContext.getCompanyId()).thenReturn(companyId);
		when(mockDDMormInstance.getGroupId()).thenReturn(groupId);
		when(mockDDMormInstance.getFormInstanceId()).thenReturn(formInstanceId);

		when(mockWorkflowDefinitionLinkLocalService.fetchWorkflowDefinitionLink(companyId, groupId, DDMFormInstance.class.getName(), formInstanceId, 0)).thenReturn(mockWorkflowDefinitionLink);

		workflowDefinitionService.updateWorkflowDefinitionLink(mockDDMormInstance, mockDDMFormValues, mockServiceContext);

		verify(mockWorkflowDefinitionLinkLocalService, times(1)).updateWorkflowDefinitionLink(userId, companyId, groupId, DDMFormInstance.class.getName(), formInstanceId, 0, workflowDefintion);

	}
}
