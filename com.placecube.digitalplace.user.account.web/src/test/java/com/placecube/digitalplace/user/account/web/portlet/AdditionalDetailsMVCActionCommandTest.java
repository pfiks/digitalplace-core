package com.placecube.digitalplace.user.account.web.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PortalUtil.class, SessionMessages.class })
public class AdditionalDetailsMVCActionCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 2L;

	@InjectMocks
	private AdditionalDetailsMVCActionCommand additionalDetailsMVCActionCommand;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private AccountService mockAccountService;

	@Mock
	private AccountValidationService mockAccountValidationService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Before
	public void activateSetUp() {
		initMocks(this);
		mockStatic(PortalUtil.class, SessionMessages.class);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenConfiguresAdditionalDetailsInContext() throws Exception {
		mockBasicDetails();
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountContextService, times(1)).configureAdditionalDetails(mockAccountContext, mockActionRequest);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenConfigureSkipPasswordStepInContext() throws Exception {
		mockBasicDetails();

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountContextService, times(1)).configureSkipPasswordStep(mockAccountContext);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenErrorsAreClearBeforeValidation() throws Exception {
		mockBasicDetails();
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountContextService, mockAccountContext, mockAccountValidationService);
		inOrder.verify(mockAccountContextService, times(1)).configureAdditionalDetails(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountContextService, times(1)).configureSkipPasswordStep(mockAccountContext);
		inOrder.verify(mockAccountContext, times(1)).clearErrors();
		inOrder.verify(mockAccountValidationService, times(1)).validateAdditionalDetails(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);
		inOrder.verify(mockAccountValidationService, times(1)).validateCaptcha(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration,
				AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenSetsPasswordDetailsAsRenderCommandName() throws Exception {
		mockBasicDetails();

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.PASSWORD_DETAILS);
	}

	@Test
	public void doProcessAction_WhenNoErrorAndEmailNotInThisPage_ThenErrorsAreClearBeforeValidation() throws Exception {
		mockBasicDetails();
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountContextService, mockAccountContext, mockAccountValidationService);
		inOrder.verify(mockAccountContextService, times(1)).configureAdditionalDetails(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountContextService, times(1)).configureSkipPasswordStep(mockAccountContext);
		inOrder.verify(mockAccountContext, times(1)).clearErrors();
		inOrder.verify(mockAccountValidationService, times(1)).validateAdditionalDetails(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);
		inOrder.verify(mockAccountValidationService, times(1)).validateCaptcha(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration,
				AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);
	}

	@Test
	public void doProcessAction_WhenNoValidationErrorsExistAndNotSkipPasswordStep_ThenSetsRenderCommandAsPasswordDetails() throws Exception {
		mockBasicDetails();
		when(mockAccountContext.hasErrors()).thenReturn(false);

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.PASSWORD_DETAILS);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoValidationErrorsExistAndSkipPasswordStep_ThenCallCreateAccount(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails();
		when(mockAccountContext.hasErrors()).thenReturn(false);
		when(mockAccountContext.isSkipPasswordStep()).thenReturn(true);
		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(addressOutOfLocalArea);

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountService, times(1)).createUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);
	}

	@Test
	public void doProcessAction_WhenNoValidationErrorsExistAndSkipPasswordStep_ThenSetsRenderCommandAsConfirmationPage() throws Exception {
		mockBasicDetails();
		when(mockAccountContext.hasErrors()).thenReturn(false);
		when(mockAccountContext.isSkipPasswordStep()).thenReturn(true);

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.CONFIRMATION_PAGE);
	}

	@Test
	public void doProcessAction_WhenValidationErrorsExist_ThenSetsRenderCommandAsAdditonalDetails() throws Exception {
		mockBasicDetails();
		when(mockAccountContext.hasErrors()).thenReturn(true);

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.ADDITIONAL_DETAILS);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenUpdateContextInSession() throws Exception {
		mockBasicDetails();
		when(mockAccountContext.hasErrors()).thenReturn(true);

		additionalDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockActionRequest);
	}

	private void mockBasicDetails() throws UserAccountException {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);
	}
}
