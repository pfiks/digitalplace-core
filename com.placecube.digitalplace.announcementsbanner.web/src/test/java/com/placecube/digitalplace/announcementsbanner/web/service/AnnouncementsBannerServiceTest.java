package com.placecube.digitalplace.announcementsbanner.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.announcements.kernel.model.AnnouncementsEntry;
import com.liferay.announcements.kernel.model.AnnouncementsFlagConstants;
import com.liferay.announcements.kernel.service.AnnouncementsEntryLocalService;
import com.liferay.announcements.kernel.service.AnnouncementsFlagLocalService;
import com.liferay.portal.kernel.cookies.CookiesManagerUtil;
import com.liferay.portal.kernel.cookies.constants.CookiesConstants;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AnnouncementsUtil.class, CookiesManagerUtil.class, PortalUtil.class, PropsUtil.class, AnnouncementsBannerService.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.cookies.CookiesManagerUtil" })
public class AnnouncementsBannerServiceTest extends PowerMockito {

	private static final long ANNOUNCEMENT_ID_1 = 11;
	private static final long ANNOUNCEMENT_ID_2 = 22;
	private static final long ANNOUNCEMENT_ID_3 = 33;
	private static final long GROUP_CLASS_NAME_ID = 32L;
	private static final long GROUP_ID = 342;
	private static final long USER_ID = 1;

	private List<AnnouncementsEntry> announcements;

	@InjectMocks
	private AnnouncementsBannerService announcementsBannerService;

	@Captor
	private ArgumentCaptor<LinkedHashMap<Long, long[]>> linkedHashMapCaptor;

	@Mock
	private AnnouncementsEntry mockAnnouncementsEntry1;

	@Mock
	private AnnouncementsEntry mockAnnouncementsEntry2;

	@Mock
	private AnnouncementsEntry mockAnnouncementsEntry3;

	@Mock
	private AnnouncementsEntryLocalService mockAnnouncementsEntryLocalService;

	@Mock
	private AnnouncementsFlagLocalService mockAnnouncementsFlagLocalService;

	@Mock
	private Cookie mockCookie;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Layout mockLayout;

	@Mock
	private Portal mockPortal;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	private LinkedHashMap<Long, long[]> scopes;

	@Test(expected = PortletException.class)
	public void getUnreadAnnouncements_WhenException_ThenThrowsPortletException() throws Exception {
		when(AnnouncementsUtil.getAnnouncementScopes(mockUser)).thenThrow(new PortalException());

		announcementsBannerService.getUnreadAnnouncements(mockHttpServletRequest, mockUser);
	}

	@Test
	public void getUnreadAnnouncements_WhenNoError_ThenRetrievesAnnouncementsWithCurrentLayoutGroupIdInScopes() throws Exception {
		mockUserDetails(false);
		mockAnnouncementsRetrieval();
		mockThemeDisplayAndPortal();

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);

		announcementsBannerService.getUnreadAnnouncements(mockHttpServletRequest, mockUser);

		verify(mockAnnouncementsEntryLocalService, times(1)).getEntries(eq(USER_ID), linkedHashMapCaptor.capture(), eq(false), eq(AnnouncementsFlagConstants.NOT_HIDDEN), eq(QueryUtil.ALL_POS),
				eq(QueryUtil.ALL_POS));

		LinkedHashMap<Long, long[]> capturedScopes = linkedHashMapCaptor.getValue();

		long[] scopeGroupIds = capturedScopes.get(GROUP_CLASS_NAME_ID);

		assertThat(scopeGroupIds, nullValue());
	}

	@Test
	public void getUnreadAnnouncements_WhenNoErrorAndUserIsNotTheDefaultUser_ThenReturnsAnnouncementsRetrieved() throws Exception {
		mockAnnouncementDetail(mockAnnouncementsEntry1, ANNOUNCEMENT_ID_1, false);
		mockAnnouncementDetail(mockAnnouncementsEntry2, ANNOUNCEMENT_ID_2, true);
		mockAnnouncementDetail(mockAnnouncementsEntry3, ANNOUNCEMENT_ID_3, false);
		mockUserDetails(false);
		mockAnnouncementsRetrieval();
		mockThemeDisplayAndPortal();

		List<AnnouncementsEntry> results = announcementsBannerService.getUnreadAnnouncements(mockHttpServletRequest, mockUser);

		assertThat(results, sameInstance(announcements));
	}

	@Test
	public void getUnreadAnnouncements_WhenNoErrorAndUserIsTheDefaultUser_ThenReturnsAnnouncementsRetrievedFilteredBasedOnSessionAttributes() throws Exception {
		mockAnnouncementDetail(mockAnnouncementsEntry1, ANNOUNCEMENT_ID_1, false);
		mockAnnouncementDetail(mockAnnouncementsEntry2, ANNOUNCEMENT_ID_2, true);
		mockAnnouncementDetail(mockAnnouncementsEntry3, ANNOUNCEMENT_ID_3, false);

		mockUserDetails(true);
		mockAnnouncementsRetrieval();
		mockThemeDisplayAndPortal();

		List<AnnouncementsEntry> results = announcementsBannerService.getUnreadAnnouncements(mockHttpServletRequest, mockUser);

		assertThat(results, containsInAnyOrder(mockAnnouncementsEntry1, mockAnnouncementsEntry3));
	}

	@Test
	public void getUnreadAnnouncements_WhenThemeDisplayInRequestIsNull_ThenDoesNotSetCurrentLayoutGroupIdInScopes() throws Exception {
		mockUserDetails(false);
		mockAnnouncementsRetrieval();
		mockThemeDisplayAndPortal();

		announcementsBannerService.getUnreadAnnouncements(mockHttpServletRequest, mockUser);

		verify(mockAnnouncementsEntryLocalService, times(1)).getEntries(eq(USER_ID), linkedHashMapCaptor.capture(), eq(false), eq(AnnouncementsFlagConstants.NOT_HIDDEN), eq(QueryUtil.ALL_POS),
				eq(QueryUtil.ALL_POS));

		LinkedHashMap<Long, long[]> capturedScopes = linkedHashMapCaptor.getValue();

		long[] scopeGroupIds = capturedScopes.get(GROUP_CLASS_NAME_ID);

		assertThat(scopeGroupIds.length, equalTo(1));
		assertThat(scopeGroupIds[0], equalTo(GROUP_ID));
	}

	@Test
	public void markAnnouncementAsRead_WhenUserIsNotTheDefaultUser_ThenAddsTheFlagToHideTheAnnouncement() {
		mockUserDetails(false);

		announcementsBannerService.markAnnouncementAsRead(mockHttpServletRequest, mockHttpServletResponse, mockUser, ANNOUNCEMENT_ID_1);

		verify(mockAnnouncementsFlagLocalService, times(1)).addFlag(USER_ID, ANNOUNCEMENT_ID_1, AnnouncementsFlagConstants.HIDDEN);
	}

	@Test
	public void markAnnouncementAsRead_WhenUserIsTheDefaultUser_ThenAddsAcookieForTheDismissedAnnouncement() throws Exception {
		mockUserDetails(true);

		whenNew(Cookie.class).withArguments(AnnouncementsBannerService.DISMISS_ANNOUNCEMENT_COOKIE_ATTRIBUTE_PREFIX + ANNOUNCEMENT_ID_1, Boolean.TRUE.toString()).thenReturn(mockCookie);

		announcementsBannerService.markAnnouncementAsRead(mockHttpServletRequest, mockHttpServletResponse, mockUser, ANNOUNCEMENT_ID_1);

		verifyStatic(CookiesManagerUtil.class, times(1));
		CookiesManagerUtil.addCookie(CookiesConstants.CONSENT_TYPE_NECESSARY, mockCookie, mockHttpServletRequest, mockHttpServletResponse);

		verifyNew(Cookie.class, times(1)).withArguments(AnnouncementsBannerService.DISMISS_ANNOUNCEMENT_COOKIE_ATTRIBUTE_PREFIX + ANNOUNCEMENT_ID_1, Boolean.TRUE.toString());
		verify(mockCookie, times(1)).setPath("/");
	}

	private void mockAnnouncementDetail(AnnouncementsEntry entry, long announcementId, boolean dismissedInSession) {
		when(entry.getEntryId()).thenReturn(announcementId);
		announcements.add(entry);
		when(CookiesManagerUtil.getCookieValue(AnnouncementsBannerService.DISMISS_ANNOUNCEMENT_COOKIE_ATTRIBUTE_PREFIX + announcementId, mockHttpServletRequest))
				.thenReturn(Boolean.valueOf(dismissedInSession).toString());
	}

	private void mockAnnouncementsRetrieval() throws PortalException {
		when(AnnouncementsUtil.getAnnouncementScopes(mockUser)).thenReturn(scopes);
		when(mockAnnouncementsEntryLocalService.getEntries(USER_ID, scopes, false, AnnouncementsFlagConstants.NOT_HIDDEN, QueryUtil.ALL_POS, QueryUtil.ALL_POS)).thenReturn(announcements);
	}

	private void mockThemeDisplayAndPortal() {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.getGroupId()).thenReturn(GROUP_ID);
		when(mockPortal.getClassNameId(Group.class.getName())).thenReturn(GROUP_CLASS_NAME_ID);
	}

	private void mockUserDetails(boolean defaultUser) {
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.isGuestUser()).thenReturn(defaultUser);
	}

	@Before
	public void setUp() {
		mockStatic(CookiesManagerUtil.class, PortalUtil.class, PropsUtil.class, AnnouncementsUtil.class);
		announcements = new ArrayList<>();
		scopes = new LinkedHashMap<>();
	}

}
