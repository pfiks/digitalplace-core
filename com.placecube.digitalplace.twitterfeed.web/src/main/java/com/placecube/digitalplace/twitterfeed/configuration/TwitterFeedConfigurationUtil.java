package com.placecube.digitalplace.twitterfeed.configuration;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = TwitterFeedConfigurationUtil.class)
public class TwitterFeedConfigurationUtil {

	private static final Log LOG = LogFactoryUtil.getLog(TwitterFeedConfigurationUtil.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	public TwitterFeedPortletInstanceConfiguration getConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		return configurationProvider.getPortletInstanceConfiguration(TwitterFeedPortletInstanceConfiguration.class, themeDisplay);
	}

	public Optional<TwitterFeedPortletInstanceConfiguration> getValidConfiguration(ThemeDisplay themeDisplay) {
		try {
			TwitterFeedPortletInstanceConfiguration configuration = getConfiguration(themeDisplay);
			if (isValidTimelineConfiguration(configuration) || Validator.isNotNull(configuration.twitterEmbedded())) {
				return Optional.of(configuration);
			}
		} catch (ConfigurationException e) {
			LOG.debug(e);
		}

		return Optional.empty();
	}

	private boolean isValidTimelineConfiguration(TwitterFeedPortletInstanceConfiguration configuration) {
		return configuration.feedsToShow() > 0 && Validator.isNotNull(configuration.twitterUsername());
	}
}