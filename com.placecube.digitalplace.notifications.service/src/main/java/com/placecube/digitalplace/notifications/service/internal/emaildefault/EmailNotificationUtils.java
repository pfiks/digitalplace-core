package com.placecube.digitalplace.notifications.service.internal.emaildefault;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.notifications.exception.NotificationsException;

import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = EmailNotificationUtils.class)
public class EmailNotificationUtils {

	static final String BODY_TEXT_PREFIX = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"utf-8\"><title>title</title></head><body>";

	static final String BODY_TEXT_SUFFIX = "</body></html>";

	@Reference
	private JSONFactory jsonFactory;

	public MailMessage createMailMessage() {
		return new MailMessage();
	}

	public String getBodyTextField(String text, Map<String, Object> variables) {
		for (Map.Entry<String, Object> variable : variables.entrySet()) {
			text = replaceAndCleanText(text, variable.getKey(), GetterUtil.getString(variable.getValue()));
		}

		return BODY_TEXT_PREFIX + text + BODY_TEXT_SUFFIX;
	}

	public InternetAddress getInternetAddress(String address, String name) throws NotificationsException {
		try {
			if (Validator.isNotNull(name)) {
				return new InternetAddress(address, name);
			} else {
				return new InternetAddress(address);
			}
		} catch (AddressException | UnsupportedEncodingException e) {
			throw new NotificationsException("Invalid internet address", e);
		}
	}

	public InternetAddress[] getInternetAddress(String[] addresses) throws NotificationsException {

		List<InternetAddress> internetAddresses = new LinkedList<>();
		if (Validator.isNotNull(addresses)) {
			for (String address : addresses) {
				if (Validator.isAddress(address)) {
					internetAddresses.add(getInternetAddress(address, null));

				}
			}
		}

		return internetAddresses.stream().toArray(InternetAddress[]::new);

	}

	public String getSubjectTextField(String text, Map<String, Object> variables) {
		for (Map.Entry<String, Object> variable : variables.entrySet()) {
			text = replaceAndCleanText(text, variable.getKey(), GetterUtil.getString(variable.getValue()));
		}
		return text;
	}

	private String replaceAndCleanText(String text, String variableKey, String variableValue) {
		try {

			if (Validator.isNotNull(variableValue)) {
				JSONObject object = jsonFactory.createJSONObject(variableValue);

				for (String keyJson : object.keySet()) {
					text = text.replace("$" + variableKey + "_" + keyJson + "$", object.getString(keyJson));
				}

			} else {
				text = replacePlaceholderByValue(text, variableKey, variableValue);
			}

		} catch (JSONException | NullPointerException ex) {
			text = replacePlaceholderByValue(text, variableKey, variableValue);
		}
		return text;
	}

	private String replacePlaceholderByValue(String text, String variableKey, String variableValue) {
		String replacedValue = Validator.isNull(variableValue) ? StringPool.BLANK
				: com.liferay.portal.kernel.util.StringUtil.replace(variableValue, new String[] { "[\"", "\"]", "[]" }, new String[] { StringPool.BLANK, StringPool.BLANK, StringPool.BLANK });
		return text.replace("$" + variableKey + "$", replacedValue);
	}
}
