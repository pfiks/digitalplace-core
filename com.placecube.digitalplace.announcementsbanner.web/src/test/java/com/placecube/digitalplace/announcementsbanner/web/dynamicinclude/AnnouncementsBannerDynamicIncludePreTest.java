package com.placecube.digitalplace.announcementsbanner.web.dynamicinclude;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude.DynamicIncludeRegistry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.announcementsbanner.web.constants.PortletKeys;

public class AnnouncementsBannerDynamicIncludePreTest extends PowerMockito {

	@InjectMocks
	private AnnouncementsBannerDynamicIncludePre announcementsBannerDynamicIncludePre;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private DynamicIncludeRegistry mockDynamicIncludeRegistry;

	@Mock
	private Layout mockLayout;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void include_WhenLayoutIsOfTypeControlPanel_ThenDoesNotPerformAnyAction() throws IOException {
		mockBasicDetails(true);

		announcementsBannerDynamicIncludePre.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, never()).setAttribute(anyString(), any());
	}

	@Test
	public void include_WhenLayoutIsNotOfTypeControlPanel_ThenAddsTheAnnouncementBannerToTheRuntimePortletIds() throws IOException {
		mockBasicDetails(false);
		String[] originalPortletIds = new String[] { "id1", "id2" };
		String[] expectedPorltetIds = new String[] { "id1", "id2", PortletKeys.ANNOUNCEMENTS_BANNER };
		when(mockHttpServletRequest.getAttribute("runtimePortletIdsBodyTopPre")).thenReturn(originalPortletIds);

		announcementsBannerDynamicIncludePre.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, times(1)).setAttribute("runtimePortletIdsBodyTopPre", expectedPorltetIds);
	}

	@Test
	public void register_ThenRegistersTheBodyTopPreInclude() {
		announcementsBannerDynamicIncludePre.register(mockDynamicIncludeRegistry);

		verify(mockDynamicIncludeRegistry, times(1)).register("/html/common/themes/body_top.jsp#pre");
	}

	private void mockBasicDetails(boolean typeControlPanel) {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockLayout.isTypeControlPanel()).thenReturn(typeControlPanel);
	}
}
