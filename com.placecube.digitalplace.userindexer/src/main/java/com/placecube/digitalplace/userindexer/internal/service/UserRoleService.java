package com.placecube.digitalplace.userindexer.internal.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.placecube.digitalplace.userindexer.constants.UserIndexerFields;

@Component(immediate = true, service = UserRoleService.class)
public class UserRoleService {

	private static final Log LOG = LogFactoryUtil.getLog(UserRoleService.class);

	@Reference
	private RoleLocalService roleLocalService;

	@Reference
	private UserGroupRoleLocalService userGroupRoleLocalService;

	public List<String> getGroupRoleIdsFieldValues(User user) {

		List<String> groupRoleIds = new ArrayList<>();
		List<UserGroupRole> userGroupRoles = userGroupRoleLocalService.getUserGroupRoles(user.getUserId());
		userGroupRoles.forEach(userGroupRole -> {
			groupRoleIds.add(UserIndexerFields.getUserGroupRoleSearchValue(userGroupRole.getGroupId(), userGroupRole.getRoleId()));
		});

		return groupRoleIds;
	}

	public Set<Long> getInheritedRegularRoleIds(User user) {
		Set<Long> inheritedUserGroupRoles = new HashSet<>();
		user.getUserGroups().forEach(userGroup -> {
			try {
				List<Role> groupRoles = roleLocalService.getGroupRoles(userGroup.getGroupId());
				inheritedUserGroupRoles.addAll(groupRoles.stream().map(entry -> entry.getRoleId()).collect(Collectors.toList()));
			} catch (Exception e) {
				LOG.debug(e);
			}
		});

		return inheritedUserGroupRoles;
	}
}
