package com.placecube.digitalplace.addresslookup.dtf.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.dtf.constants.DTFConstants;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DTFAddressLookupTest extends PowerMockito {

	private static final Long COMPANY_ID = 1l;

	private static final String DATABASE_CLASS = "com.jdbc.mysql";

	private static final String DATABASE_PASSWORD = "password";

	private static final String DATABASE_URL = "url";

	private static final String DATABASE_USER = "user";

	private static final String POSTCODE = "TS11ST";

	private static final String UPRN = "12345678";

	@InjectMocks
	private DTFAddressLookup dtfAddressLookup;

	@Mock
	private AddressContext mockAddressContext1;

	@Mock
	private AddressContext mockAddressContext2;

	@Mock
	private AddressRetrievalService mockAddressRetrievalService;

	@Mock
	private Connection mockConnection;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private ResultSet mockResultSet;

	@Before
	public void activeSetup() {
		initMocks(this);
	}

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockAddressRetrievalService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = dtfAddressLookup.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockAddressRetrievalService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = dtfAddressLookup.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void findByPostcode_WhenErrorGettingConnection_ThenReturnsEmptyAddressContextSet() throws Exception {
		mockConnection();

		when(mockAddressRetrievalService.getConnection(COMPANY_ID, DATABASE_CLASS, DATABASE_URL, DATABASE_USER, DATABASE_PASSWORD)).thenThrow(new SQLException());

		Set<AddressContext> result = dtfAddressLookup.findByPostcode(COMPANY_ID, POSTCODE, null);

		verify(mockAddressRetrievalService, never()).executeQuery(any(), any());
		assertTrue(result.isEmpty());
	}

	@Test
	public void findByPostcode_WhenNoErrorGettingConnectionAndEmptyResultSet_ThenClosesConnectionAndReturnsEmptyAddressContextSet() throws Exception {
		mockConnection();

		when(mockAddressRetrievalService.executeQuery(mockConnection, DTFConstants.FIND_BY_POSTCODE_SQL + POSTCODE + DTFConstants.SUFFIX_SQL)).thenReturn(Optional.empty());

		Set<AddressContext> result = dtfAddressLookup.findByPostcode(COMPANY_ID, POSTCODE, null);

		verify(mockAddressRetrievalService, times(1)).closeConnection(mockConnection);
		assertTrue(result.isEmpty());
	}

	@Test
	public void findByPostcode_WhenNoErrorGettingConnectionAndResultSetPresent_ThenClosesConnectionAndReturnsAddressContextSet() throws Exception {
		mockConnection();

		Set<AddressContext> addressContexts = Stream.of(mockAddressContext1, mockAddressContext2).collect(Collectors.toCollection(HashSet::new));

		when(mockAddressRetrievalService.executeQuery(mockConnection, DTFConstants.FIND_BY_POSTCODE_SQL + POSTCODE + DTFConstants.SUFFIX_SQL)).thenReturn(Optional.of(mockResultSet));
		when(mockAddressRetrievalService.getAddressContexts(mockResultSet)).thenReturn(addressContexts);

		Set<AddressContext> result = dtfAddressLookup.findByPostcode(COMPANY_ID, POSTCODE, null);

		verify(mockAddressRetrievalService, times(1)).closeConnection(mockConnection);
		assertThat(result, containsInAnyOrder(mockAddressContext1, mockAddressContext2));
	}

	@Test
	public void findByUprn_WhenErrorGettingConnection_ThenReturnsEmptyOptional() throws Exception {
		mockConnection();

		when(mockAddressRetrievalService.getConnection(COMPANY_ID, DATABASE_CLASS, DATABASE_URL, DATABASE_USER, DATABASE_PASSWORD)).thenThrow(new SQLException());

		Optional<AddressContext> result = dtfAddressLookup.findByUprn(COMPANY_ID, UPRN, null);

		verify(mockAddressRetrievalService, never()).executeQuery(any(), any());
		assertFalse(result.isPresent());
	}

	@Test
	public void findByUprn_WhenNoErrorGettingConnectionAndEmptyResultSet_ThenClosesConnectionAndReturnsEmptyOptional() throws Exception {
		mockConnection();

		when(mockAddressRetrievalService.executeQuery(mockConnection, DTFConstants.FIND_BY_UPRN_SQL + UPRN + DTFConstants.SUFFIX_SQL)).thenReturn(Optional.empty());

		Optional<AddressContext> result = dtfAddressLookup.findByUprn(COMPANY_ID, UPRN, null);

		verify(mockAddressRetrievalService, times(1)).closeConnection(mockConnection);
		assertFalse(result.isPresent());
	}

	@Test
	public void findByUprn_WhenNoErrorGettingConnectionAndResultSetPresent_ThenClosesConnectionAndReturnsAddressContextOptional() throws Exception {
		mockConnection();

		when(mockAddressRetrievalService.executeQuery(mockConnection, DTFConstants.FIND_BY_UPRN_SQL + UPRN + DTFConstants.SUFFIX_SQL)).thenReturn(Optional.of(mockResultSet));
		when(mockAddressRetrievalService.getAddressContext(mockResultSet)).thenReturn(Optional.of(mockAddressContext1));

		Optional<AddressContext> result = dtfAddressLookup.findByUprn(COMPANY_ID, UPRN, null);

		verify(mockAddressRetrievalService, times(1)).closeConnection(mockConnection);
		assertThat(result.get(), sameInstance(mockAddressContext1));
	}

	@Test
	public void getWeight_WhenException_ThenReturnsZero() throws ConfigurationException {
		when(mockAddressRetrievalService.getWeight(COMPANY_ID)).thenThrow(new ConfigurationException());

		int result = dtfAddressLookup.getWeight(COMPANY_ID);

		assertTrue(result == 0);
	}

	@Test
	public void getWeight_WhenNoError_ThenReturnsTheWeight() throws ConfigurationException {
		int expected = 1;
		when(mockAddressRetrievalService.getWeight(COMPANY_ID)).thenReturn(expected);

		int result = dtfAddressLookup.getWeight(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void national_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockAddressRetrievalService.isNational(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = dtfAddressLookup.national(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void national_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockAddressRetrievalService.isNational(COMPANY_ID)).thenReturn(expected);

		boolean result = dtfAddressLookup.national(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	private void mockConnection() throws Exception {
		when(mockAddressRetrievalService.getDatabaseClass(COMPANY_ID)).thenReturn(DATABASE_CLASS);
		when(mockAddressRetrievalService.getDatabaseURL(COMPANY_ID)).thenReturn(DATABASE_URL);
		when(mockAddressRetrievalService.getDatabaseUser(COMPANY_ID)).thenReturn(DATABASE_USER);
		when(mockAddressRetrievalService.getDatabasePassword(COMPANY_ID)).thenReturn(DATABASE_PASSWORD);

		when(mockAddressRetrievalService.getConnection(COMPANY_ID, DATABASE_CLASS, DATABASE_URL, DATABASE_USER, DATABASE_PASSWORD)).thenReturn(mockConnection);
	}
}