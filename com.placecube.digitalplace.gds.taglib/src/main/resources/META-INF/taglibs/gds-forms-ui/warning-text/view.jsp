<%@ include file="../init.jsp"%>

<c:if test="${not empty messageLabel}" >
	<div class="govuk-warning-text" aria-labelledby="${portletNamespace}warning-text" >
		<span class="govuk-warning-text__icon" aria-hidden="true">!</span>
		<strong class="govuk-warning-text__text">
			<span class="govuk-warning-text__assistive"><liferay-ui:message key="warning"/></span>
			<liferay-ui:message key="${messageLabel}"/>
		</strong>
	</div>
</c:if>