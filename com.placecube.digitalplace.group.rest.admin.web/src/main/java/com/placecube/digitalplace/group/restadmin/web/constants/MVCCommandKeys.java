package com.placecube.digitalplace.group.restadmin.web.constants;

public final class MVCCommandKeys {

	public static final String REMOVE = "/settings/webhooks/remove";

	public static final String UPDATE = "/settings/webhooks/update";

	public static final String UPDATE_API_KEYS = "/group-rest-admin/update-api-keys";

	public static final String VIEW = "/settings/webhooks/view";

	private MVCCommandKeys() {
	}

}
