/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service.impl;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.internal.utils.AppointmentUtils;
import com.placecube.digitalplace.appointment.model.AppointmentSlot;
import com.placecube.digitalplace.appointment.service.base.AppointmentSlotLocalServiceBaseImpl;

/**
 * @author Brian Wing Shun Chan
 */
@Component(property = "model.class.name=com.placecube.digitalplace.appointment.model.AppointmentSlot", service = AopService.class)
public class AppointmentSlotLocalServiceImpl extends AppointmentSlotLocalServiceBaseImpl {

	private static final Log LOG = LogFactoryUtil.getLog(AppointmentSlotLocalServiceImpl.class);

	@Reference
	private AppointmentUtils appointmentUtils;

	@Override
	public int countAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(long companyId, String slotType, String serviceId) {
		return appointmentSlotPersistence.countByCompanyId_SlotType_ServiceId(companyId, slotType, serviceId);
	}

	@Override
	public List<AppointmentBookingSlot> getAppointmentBookingSlots(AppointmentSlot appointmentSlot, Date fromDate, Date toDate) {
		LOG.debug("Processing slot: " + appointmentSlot.getAppointmentSlotId() + ", type:" + appointmentSlot.getSlotType());
		List<AppointmentBookingSlot> results = new ArrayList<>();

		LocalDateTime configStart = appointmentUtils.getLocalDateTime(appointmentSlot.getStartDate(), appointmentSlot.getSlotStartTimeHour(), appointmentSlot.getSlotStartTimeMinute());
		LocalDateTime configEnd = appointmentUtils.getLocalDateTime(appointmentSlot.getEndDate(), appointmentSlot.getSlotEndTimeHour(), appointmentSlot.getSlotEndTimeMinute());

		LocalDateTime filterStart = appointmentUtils.getLocalDateTime(fromDate, configStart);
		LocalDateTime filterEnd = appointmentUtils.getLocalDateTime(toDate, configEnd);

		Optional<LocalDateTime> calculationMaxDateOpt = appointmentUtils.getCalculationEndDate(configEnd, filterStart, filterEnd, appointmentSlot.getSlotEndTimeHour(),
				appointmentSlot.getSlotEndTimeMinute());
		if (calculationMaxDateOpt.isPresent()) {

			LocalDateTime calculationMaxDate = calculationMaxDateOpt.get();

			Optional<LocalDateTime> calculationDateStartOpt = appointmentUtils.getCalculationStartDate(configStart, filterStart, calculationMaxDate, appointmentSlot.getSlotStartTimeHour(),
					appointmentSlot.getSlotStartTimeMinute());
			if (calculationDateStartOpt.isPresent()) {

				LocalDateTime calculationDateStart = calculationDateStartOpt.get();

				Set<DayOfWeek> daysOfWeek = appointmentSlot.getDaysOfWeek();
				int appointmentDuration = appointmentSlot.getAppointmentDuration();
				int maxAppointments = appointmentSlot.getMaxAppointments();

				while (!calculationDateStart.isAfter(calculationMaxDate)) {

					if (daysOfWeek.contains(calculationDateStart.getDayOfWeek())) {

						if (appointmentDuration <= 0) {
							LocalDateTime slotEnd = appointmentUtils.getSlotEnd(calculationDateStart, 0, calculationMaxDate);

							LOG.debug("Adding slot: " + calculationDateStart.toString() + " +" + slotEnd.toString());
							results.add(new AppointmentBookingSlot(calculationDateStart, slotEnd, maxAppointments));

						} else {

							LocalDateTime slotMaxTime = appointmentUtils.getSlotEnd(calculationDateStart, 0, calculationMaxDate);

							LocalDateTime slotEnd = appointmentUtils.getSlotEnd(calculationDateStart, appointmentDuration, calculationMaxDate);

							while (!slotEnd.isAfter(slotMaxTime)) {
								LOG.debug("Adding slot: " + calculationDateStart.toString() + " +" + slotEnd.toString());
								results.add(new AppointmentBookingSlot(calculationDateStart, slotEnd, maxAppointments));

								slotEnd = appointmentUtils.getSlotEnd(slotEnd, appointmentDuration, calculationMaxDate);
								calculationDateStart = calculationDateStart.plusMinutes(appointmentDuration);
							}
						}

					}

					calculationDateStart = appointmentUtils.getNextDateSlotStart(calculationDateStart, appointmentSlot);
				}
			}
		}
		return results;
	}

	@Override
	public List<AppointmentSlot> getAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(long companyId, String slotType, String serviceId, int start, int end) {
		return appointmentSlotPersistence.findByCompanyId_SlotType_ServiceId(companyId, slotType, serviceId, start, end);
	}

}