package com.placecube.digitalplace.externalpage.web.constants;

public final class MVCCommandKeys {

	public static final String EXTERNAL_PAGE_ADD = "/externalPage/add";

	public static final String EXTERNAL_PAGE_CLEAR_ALL = "/externalPage/clearAll";

	public static final String EXTERNAL_PAGE_DELETE = "/externalPage/delete";

	public static final String EXTERNAL_PAGE_EDIT = "/externalPage/edit";

	public static final String EXTERNAL_PAGE_IMPORT = "/externalPage/import";

	public static final String EXTERNAL_PAGE_STATUS = "/externalPage/status";

	public static final String EXTERNAL_PAGE_UPDATE = "/externalPage/update";

	public static final String EXTERNAL_PAGE_VIEW = "/externalPage/view";

	private MVCCommandKeys() {
	}
}
