<%@ include file="../init.jsp"%>

<c:set var="hasError" value="${not empty errorMessage}"/>
<c:set var="radiosCssClass" value="${inline ? 'd-flex align-items-center' : '' }" />

<div class="govuk-form-group ${hasError ? 'govuk-form-group--error' : ''}" id="${portletNamespace}_field_${fieldName}">
	<fieldset class="govuk-fieldset">
		
		<%@ include file="../fieldLegendLabel.jspf" %>
		<%@ include file="../fieldHintMessage.jspf" %>
		<%@ include file="../fieldErrorMessage.jspf" %>
		
		<div class="govuk-radios ${radiosCssClass}">
			<c:forEach items="${fieldOptions}" var="availableOption" varStatus="loop">
				<c:set var="marginLeft" value="${inline && loop.index > 0 ? 'ml-4' : '' }" />
				<c:set var="marginBottom" value="${inline ? 'mb-2' : '' }" />
				
				<div class="govuk-radios__item ${radiosCssClass} ${marginLeft} ${marginBottom}">
					<input class="govuk-radios__input" id="${portletNamespace}${fieldName}-${loop.index}" name="${portletNamespace}${fieldName}" type="radio" value="${availableOption.key}"
						${availableOption.key.equals(fieldValue) ? 'checked' : ''}
					/>
					<label class="govuk-label govuk-radios__label" for="${portletNamespace}${fieldName}-${loop.index}">
						<liferay-ui:message key="${availableOption.value}"/>
					</label>
				</div>
			</c:forEach>
		</div>
	</fieldset>
</div>
