package com.placecube.digitalplace.audit.listener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.PortalPreferenceValue;
import com.liferay.portal.security.audit.event.generators.constants.EventTypes;
import com.placecube.digitalplace.audit.service.AuditService;

public class AuditPortalPreferenceValueListenerTest extends PowerMockito {

	@InjectMocks
	private AuditPortalPreferenceValueListener auditPortalPreferenceValueListener;

	@Mock
	private Map<String, String> mockAdditionalInformationMap;

	@Mock
	private AuditService mockAuditService;

	@Mock
	private PortalPreferenceValue mockPortalPreferenceValue;

	@Mock
	private PortalPreferenceValue mockPortalPreferenceValuePrevious;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void onAfterCreate_WhenNoError_ThenAddsAuditLogEntry() {
		long id = 1;
		when(mockPortalPreferenceValue.getPortalPreferenceValueId()).thenReturn(id);
		when(mockAuditService.getPortalPreferenceValueAuditInfo(mockPortalPreferenceValue)).thenReturn(mockAdditionalInformationMap);

		auditPortalPreferenceValueListener.onAfterCreate(mockPortalPreferenceValue);

		verify(mockAuditService, times(1)).addAuditLogEntry(PortalPreferenceValue.class.getName(), id, EventTypes.ADD, mockAdditionalInformationMap);
	}

	@Test
	public void onAfterRemove_WhenNoError_ThenAddsAuditLogEntry() {
		long id = 1;
		when(mockPortalPreferenceValue.getPortalPreferenceValueId()).thenReturn(id);
		when(mockAuditService.getPortalPreferenceValueAuditInfo(mockPortalPreferenceValue)).thenReturn(mockAdditionalInformationMap);

		auditPortalPreferenceValueListener.onAfterRemove(mockPortalPreferenceValue);

		verify(mockAuditService, times(1)).addAuditLogEntry(PortalPreferenceValue.class.getName(), id, EventTypes.DELETE, mockAdditionalInformationMap);

	}

	@Test
	public void onAfterUpdate_WhenNoError_ThenAddsAuditLogEntry() {
		long id = 1;
		when(mockPortalPreferenceValue.getPortalPreferenceValueId()).thenReturn(id);
		when(mockAuditService.getPortalPreferenceValueAuditInfo(mockPortalPreferenceValue)).thenReturn(mockAdditionalInformationMap);

		auditPortalPreferenceValueListener.onAfterUpdate(mockPortalPreferenceValuePrevious, mockPortalPreferenceValue);

		verify(mockAuditService, times(1)).addAuditLogEntry(PortalPreferenceValue.class.getName(), id, EventTypes.UPDATE, mockAdditionalInformationMap);
	}

}
