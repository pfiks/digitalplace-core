package com.placecube.digitalplace.authentication.twofactor.checker.registry;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.util.ListUtil;
import com.placecube.digitalplace.authentication.twofactor.checker.EnforceTwoFactorAuthenticationChecker;

@Component(immediate = true, service = EnforceTwoFactorAuthenticationCheckerRegistry.class)
public class EnforceTwoFactorAuthenticationCheckerRegistry {

	@SuppressWarnings("unused")
	private BundleContext bundleContext;

	private ServiceTrackerMap<String, ServiceWrapper<EnforceTwoFactorAuthenticationChecker>> enforceTwoFactorAuthenticationCheckerMap;

	public List<EnforceTwoFactorAuthenticationChecker> getEnforceTwoFactorAuthenticationCheckers() {
		List<ServiceWrapper<EnforceTwoFactorAuthenticationChecker>> allCheckers = ListUtil.fromCollection(enforceTwoFactorAuthenticationCheckerMap.values());

		List<EnforceTwoFactorAuthenticationChecker> results = allCheckers.stream().map(ServiceWrapper::getService).collect(Collectors.toList());
		return Collections.unmodifiableList(results);
	}

	@Activate
	protected void activate(BundleContext bundleContext) {
		this.bundleContext = bundleContext;

		enforceTwoFactorAuthenticationCheckerMap = ServiceTrackerMapFactory
				.openSingleValueMap(bundleContext, EnforceTwoFactorAuthenticationChecker.class, EnforceTwoFactorAuthenticationChecker.CHECKER_ID,
						ServiceTrackerCustomizerFactory.serviceWrapper(bundleContext));
	}

	@Deactivate
	protected void deactivate() {
		enforceTwoFactorAuthenticationCheckerMap.close();
	}

}
