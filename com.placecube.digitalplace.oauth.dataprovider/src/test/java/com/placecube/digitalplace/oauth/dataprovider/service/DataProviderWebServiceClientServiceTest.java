package com.placecube.digitalplace.oauth.dataprovider.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.oauth2.provider.model.OAuth2Application;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.json.web.service.client.JSONWebServiceClient;
import com.liferay.portal.json.web.service.client.JSONWebServiceClientFactory;
import com.liferay.portal.json.web.service.client.JSONWebServiceInvocationException;
import com.placecube.digitalplace.oauth.dataprovider.util.URLUtil;
import com.placecube.digitalplace.oauth.service.DigitalPlaceOAuth2Service;

@RunWith(MockitoJUnitRunner.class)
public class DataProviderWebServiceClientServiceTest {

	private static final String ABSOLUTE_URL = "/absoluteUrl";
	private static final String CLIENT_ID = "5433";
	private static final String CLIENT_SECRET = "sdfsdfs-sdfsdf23";
	private static final long COMPANY_ID = 43332l;

	@InjectMocks
	private DataProviderWebServiceClientService dataProviderWebServiceClientService;

	@Captor
	private ArgumentCaptor<Map<String, Object>> mapArgumentCaptor;

	@Mock
	private DDMRESTOAuth2DataProviderService mockDDMRestOAuth2DataProviderUtil;

	@Mock
	private DigitalPlaceOAuth2Service mockDigitalPlaceOAuth2Service;

	@Mock
	private JSONWebServiceClient mockJSONWebServiceClient;

	@Mock
	private JSONWebServiceClientFactory mockJSONWebServiceClientFactory;

	@Mock
	private KeyStore mockKeyStore;

	@Mock
	private OAuth2Application mockOAuth2Application;

	@Mock
	private URI mockURI;

	@Mock
	private URLUtil mockURLUtil;

	@Captor
	private ArgumentCaptor<String[]> stringArrayCaptor;

	@Test(expected = JSONWebServiceInvocationException.class)
	public void callJSONWebService_WhenCallToWebServiceFails_ThenThrowsExceptionAndWebServiceClientIsDestroyed() throws Exception {
		final String token = "ok4hsd32ids2yg";

		mockUri();
		when(mockOAuth2Application.getClientId()).thenReturn(CLIENT_ID);
		when(mockOAuth2Application.getClientSecret()).thenReturn(CLIENT_SECRET);
		when(mockDigitalPlaceOAuth2Service.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET)).thenReturn(token);
		when(mockDDMRestOAuth2DataProviderUtil.getKeyStore()).thenReturn(mockKeyStore);
		when(mockURLUtil.getProxySettingsMap()).thenReturn(new HashMap<>());

		when(mockJSONWebServiceClientFactory.getInstance(any(Map.class), anyBoolean())).thenReturn(mockJSONWebServiceClient);
		when(mockJSONWebServiceClient.doGet(eq(ABSOLUTE_URL), any(String[].class))).thenThrow(new JSONWebServiceInvocationException("error"));

		dataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, new HashMap<>());

		verify(mockJSONWebServiceClient, times(1)).destroy();
	}

	@Test
	public void callJSONWebService_WhenNoErrors_ThenReturnsWebServiceResponse() throws Exception {
		final String token = "ok4hsd32ids2yg";
		final String response = "response";

		mockUri();
		when(mockOAuth2Application.getClientId()).thenReturn(CLIENT_ID);
		when(mockOAuth2Application.getClientSecret()).thenReturn(CLIENT_SECRET);
		when(mockDigitalPlaceOAuth2Service.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET)).thenReturn(token);
		when(mockDDMRestOAuth2DataProviderUtil.getKeyStore()).thenReturn(mockKeyStore);
		when(mockURLUtil.getProxySettingsMap()).thenReturn(new HashMap<>());

		when(mockJSONWebServiceClientFactory.getInstance(any(Map.class), anyBoolean())).thenReturn(mockJSONWebServiceClient);
		when(mockJSONWebServiceClient.doGet(eq(ABSOLUTE_URL), any(String[].class))).thenReturn(response);

		String result = dataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, new HashMap<>());

		assertThat(result, equalTo(response));
	}

	@Test
	public void callJSONWebService_WhenNoErrors_ThenWebServiceClientIsCreatedWithTheRightHeadersAndParameters() throws Exception {
		final String token = "ok4hsd32ids2yg";

		Map<String, Object> proxySettingsMap = new HashMap<>();
		proxySettingsMap.put("setting", "value");

		mockUri();
		when(mockOAuth2Application.getClientId()).thenReturn(CLIENT_ID);
		when(mockOAuth2Application.getClientSecret()).thenReturn(CLIENT_SECRET);
		when(mockDigitalPlaceOAuth2Service.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET)).thenReturn(token);
		when(mockDDMRestOAuth2DataProviderUtil.getKeyStore()).thenReturn(mockKeyStore);
		when(mockURLUtil.getProxySettingsMap()).thenReturn(proxySettingsMap);

		String headers = HttpHeaders.CONTENT_TYPE + "=application/json;" + HttpHeaders.AUTHORIZATION + "=Bearer " + token;

		when(mockJSONWebServiceClientFactory.getInstance(any(Map.class), anyBoolean())).thenReturn(mockJSONWebServiceClient);

		dataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, new HashMap<>());

		verify(mockJSONWebServiceClientFactory, times(1)).getInstance(mapArgumentCaptor.capture(), eq(false));

		Map<String, Object> webServiceParametersMap = mapArgumentCaptor.getValue();

		assertThat(webServiceParametersMap.get("hostName"), equalTo("localhost"));
		assertThat(webServiceParametersMap.get("hostPort"), equalTo(8080));
		assertThat(webServiceParametersMap.get("keyStore"), sameInstance(mockKeyStore));
		assertThat(webServiceParametersMap.get("headers"), equalTo(headers));
		assertThat(webServiceParametersMap.get("protocol"), equalTo("http"));
		assertThat(webServiceParametersMap.get("trustSelfSignedCertificates"), equalTo(true));
		assertThat(webServiceParametersMap.get("setting"), equalTo("value"));

	}

	@Test
	public void callJSONWebService_WhenNoErrors_ThenWebServiceIsCalledWithRightParametersAndWebServiceClientIsDestroyed() throws Exception {
		final String token = "ok4hsd32ids2yg";

		Map<String, String> parameters = new HashMap<>();
		parameters.put("parameter1", "value1");
		parameters.put("parameter2", "value2");

		mockUri();
		when(mockOAuth2Application.getClientId()).thenReturn(CLIENT_ID);
		when(mockOAuth2Application.getClientSecret()).thenReturn(CLIENT_SECRET);
		when(mockDigitalPlaceOAuth2Service.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET)).thenReturn(token);
		when(mockDDMRestOAuth2DataProviderUtil.getKeyStore()).thenReturn(mockKeyStore);
		when(mockURLUtil.getProxySettingsMap()).thenReturn(new HashMap<>());

		when(mockJSONWebServiceClientFactory.getInstance(any(Map.class), anyBoolean())).thenReturn(mockJSONWebServiceClient);

		dataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, parameters);

		InOrder inOrder = inOrder(mockJSONWebServiceClient);

		inOrder.verify(mockJSONWebServiceClient, times(1)).doGet(eq(ABSOLUTE_URL), stringArrayCaptor.capture());
		inOrder.verify(mockJSONWebServiceClient, times(1)).destroy();

		String[] parametersCaptured = stringArrayCaptor.getValue();
		assertThat(parametersCaptured.length, equalTo(4));
		assertThat(parametersCaptured[0], equalTo("parameter2"));
		assertThat(parametersCaptured[1], equalTo("value2"));
		assertThat(parametersCaptured[2], equalTo("parameter1"));
		assertThat(parametersCaptured[3], equalTo("value1"));
	}

	@Test
	public void getAllParametersMap_WhenNoErrors_ThenReturnsMapContainingRequestParametersAndQueryParameters() {
		final String query = "parameter3=value3&parameter4=value4&parameter5";

		Map<String, String> pathInputParametersMap = new HashMap<>();
		pathInputParametersMap.put("parameter1", "value1");

		Map<String, Object> requestParametersMap = new HashMap<>();
		requestParametersMap.put("parameter1", "value1");
		requestParametersMap.put("parameter2", "value2");

		Map<String, String> result = dataProviderWebServiceClientService.getAllParametersMap(pathInputParametersMap, query, requestParametersMap);

		assertThat(result.size(), equalTo(4));
		assertThat(result.get("parameter2"), equalTo("value2"));
		assertThat(result.get("parameter3"), equalTo("value3"));
		assertThat(result.get("parameter4"), equalTo("value4"));
		assertThat(result.get("parameter5"), equalTo(StringPool.BLANK));
	}

	@Test
	public void getPathInputParametersMap_WhenNoErrors_ThenGetsUrlParametersAndReturnsMapWithRequestInputValues() {
		final String url = "http://server:8080/{parameter1}/{parameter2}/{parameter3}";
		Map<String, Object> requestInputParametersMap = new HashMap<>();
		requestInputParametersMap.put("parameter1", "value1");
		requestInputParametersMap.put("parameter2", "value2");

		Map<String, String> result = dataProviderWebServiceClientService.getPathInputParametersMap(requestInputParametersMap, url);

		assertThat(result.size(), equalTo(2));
		assertThat(result.get("parameter1"), equalTo("value1"));
		assertThat(result.get("parameter2"), equalTo("value2"));
	}

	private void mockUri() {
		when(mockURI.getPort()).thenReturn(8080);
		when(mockURI.getScheme()).thenReturn("http");
		when(mockURI.getHost()).thenReturn("localhost:8080");
		when(mockURLUtil.getHostPort(8080, "http")).thenReturn(8080);
		when(mockURLUtil.getHostName("localhost:8080")).thenReturn("localhost");
	}
}
