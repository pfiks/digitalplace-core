package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch;

import static com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.UserProfileFacetPortletConfigurationConstants.FILTER_COLOUR;
import static com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.UserProfileFacetPortletConfigurationConstants.SEARCH_FIELDS;
import static com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.UserProfileFacetPortletConfigurationConstants.TITLE;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.AdvancePeopleSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.displaycontext.AdvancePeopleSearchDisplayContext;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service.AdvancePeopleSearchConfigurationService;

@Component(immediate = true, property = { //
		"com.liferay.fragment.entry.processor.portlet.alias=dp-search-userprofilefacet", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=dp-search-advance-people-search dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=true", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.always-display-default-configuration-icons=true", //
		"javax.portlet.init-param.config-template=/advance-people-search/configuration.jsp", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/advance-people-search/view.jsp", //
		"javax.portlet.name=" + PortletKeys.ADVANCE_PEOPLE_SEARCH, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class AdvancePeopleSearchPortlet extends MVCPortlet {

	@Reference
	private AdvancePeopleSearchConfigurationService advancePeopleSearchConfigurationService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			Optional<AdvancePeopleSearchPortletInstanceConfiguration> configurationOptional = advancePeopleSearchConfigurationService.getValidConfiguration(themeDisplay);

			if (configurationOptional.isPresent()) {
				AdvancePeopleSearchPortletInstanceConfiguration configuration = configurationOptional.get();
				SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

				renderRequest.setAttribute("advancePeopleSearchDisplayContext", AdvancePeopleSearchDisplayContext.initDisplayContext(searchResponse.getActiveFilters()));
				renderRequest.setAttribute(FILTER_COLOUR, configuration.filterColour());
				renderRequest.setAttribute("redirectURL", searchResponse.getEscapedSearchResultsIteratorURL());
				renderRequest.setAttribute(SEARCH_FIELDS, advancePeopleSearchConfigurationService.getSearchFields(themeDisplay.getCompanyId(), themeDisplay.getLocale(), configuration.searchFields()));
				renderRequest.setAttribute(TITLE, advancePeopleSearchConfigurationService.getTitle(themeDisplay, configuration.title()));
			} else {
				renderRequest.setAttribute("invalidConfiguration", true);
			}

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}
}
