/**
 * SimpleSale.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.simple;

public class SimpleSale  extends com.capita_software_services.www.scp.base.SaleBase  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.PostageAndPacking postageAndPacking;

    private com.capita_software_services.www.scp.simple.Surchargeable surchargeable;

    private com.capita_software_services.www.scp.simple.SimpleItem[] items;

    public SimpleSale() {
    }

    public SimpleSale(
           com.capita_software_services.www.scp.base.SummaryData saleSummary,
           com.capita_software_services.www.scp.base.ContactDetails deliveryDetails,
           com.capita_software_services.www.scp.base.LgSaleDetails lgSaleDetails,
           com.capita_software_services.www.scp.base.PostageAndPacking postageAndPacking,
           com.capita_software_services.www.scp.simple.Surchargeable surchargeable,
           com.capita_software_services.www.scp.simple.SimpleItem[] items) {
        super(
            saleSummary,
            deliveryDetails,
            lgSaleDetails);
        this.postageAndPacking = postageAndPacking;
        this.surchargeable = surchargeable;
        this.items = items;
    }


    /**
     * Gets the postageAndPacking value for this SimpleSale.
     *
     * @return postageAndPacking
     */
    public com.capita_software_services.www.scp.base.PostageAndPacking getPostageAndPacking() {
        return postageAndPacking;
    }


    /**
     * Sets the postageAndPacking value for this SimpleSale.
     *
     * @param postageAndPacking
     */
    public void setPostageAndPacking(com.capita_software_services.www.scp.base.PostageAndPacking postageAndPacking) {
        this.postageAndPacking = postageAndPacking;
    }


    /**
     * Gets the surchargeable value for this SimpleSale.
     *
     * @return surchargeable
     */
    public com.capita_software_services.www.scp.simple.Surchargeable getSurchargeable() {
        return surchargeable;
    }


    /**
     * Sets the surchargeable value for this SimpleSale.
     *
     * @param surchargeable
     */
    public void setSurchargeable(com.capita_software_services.www.scp.simple.Surchargeable surchargeable) {
        this.surchargeable = surchargeable;
    }


    /**
     * Gets the items value for this SimpleSale.
     *
     * @return items
     */
    public com.capita_software_services.www.scp.simple.SimpleItem[] getItems() {
        return items;
    }


    /**
     * Sets the items value for this SimpleSale.
     *
     * @param items
     */
    public void setItems(com.capita_software_services.www.scp.simple.SimpleItem[] items) {
        this.items = items;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SimpleSale)) return false;
        SimpleSale other = (SimpleSale) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.postageAndPacking==null && other.getPostageAndPacking()==null) ||
             (this.postageAndPacking!=null &&
              this.postageAndPacking.equals(other.getPostageAndPacking()))) &&
            ((this.surchargeable==null && other.getSurchargeable()==null) ||
             (this.surchargeable!=null &&
              this.surchargeable.equals(other.getSurchargeable()))) &&
            ((this.items==null && other.getItems()==null) ||
             (this.items!=null &&
              java.util.Arrays.equals(this.items, other.getItems())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPostageAndPacking() != null) {
            _hashCode += getPostageAndPacking().hashCode();
        }
        if (getSurchargeable() != null) {
            _hashCode += getSurchargeable().hashCode();
        }
        if (getItems() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItems());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItems(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SimpleSale.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "simpleSale"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postageAndPacking");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "postageAndPacking"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "postageAndPacking"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "surchargeable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "surchargeable"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("items");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "items"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "simpleItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "item"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
