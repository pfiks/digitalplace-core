package com.placecube.digitalplace.frontend.taglib.servlet.taglib.util;

import com.liferay.item.selector.ItemSelector;
import com.liferay.portal.kernel.module.service.Snapshot;


public class ItemSelectorUtil {

	private static final Snapshot<ItemSelector> ITEM_SELECTOR_SNAPSHOT = new Snapshot<>(ItemSelectorUtil.class, ItemSelector.class);

	private ItemSelectorUtil() {
		throw new IllegalStateException("Item Selector Utility class");
	}

	public static ItemSelector getItemSelector() {
		return ITEM_SELECTOR_SNAPSHOT.get();
	}

}