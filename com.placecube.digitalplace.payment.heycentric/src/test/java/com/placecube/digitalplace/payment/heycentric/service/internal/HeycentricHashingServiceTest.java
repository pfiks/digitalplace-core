package com.placecube.digitalplace.payment.heycentric.service.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.payment.heycentric.configuration.HeycentricPaymentCompanyConfiguration;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;

@RunWith(MockitoJUnitRunner.class)
public class HeycentricHashingServiceTest extends Mockito {

	private final String AMOUNT = "25.0";

	@InjectMocks
	private HeycentricHashingService heycentricHashingService;

	@Mock
	private HeycentricPaymentCompanyConfiguration mockHeycentricPaymentCompanyConfiguration;

	@Mock
	private MessageDigest mockMessageDigest;

	private MockedStatic<MessageDigest> mockedStaticMessageDigest;

	@Before
	public void activateSetup() {
		mockedStaticMessageDigest = mockStatic(MessageDigest.class);
	}

	@Test
	public void base64EncodeString_WhenProvidedStringIsNull_ThenReturnBlankString() {
		String expected = StringPool.BLANK;

		String result = heycentricHashingService.base64EncodeString(StringPool.BLANK);

		assertEquals(result, expected);
	}

	@Test
	public void base64EncodeString_WhenProvidedStringNotNull_ThenReturnBase64EncodedResult() {
		String expected = "ISLCoyQlXiYqKCk=";

		String result = heycentricHashingService.base64EncodeString("!\"£$%^&*()");

		assertEquals(result, expected);
	}

	@Test
	public void getHeycentricMD5HashToHex_WhenProvidedRawStringIsNull_ThenReturnBlankString() throws NoSuchAlgorithmException {
		String expected = StringPool.BLANK;

		String result = heycentricHashingService.rawHashToMd5Hex(null);

		assertEquals(result, expected);
	}

	@Test(expected = NoSuchAlgorithmException.class)
	public void rawHashToMd5Hex_WhenAlgorithmIsNotAvailable_ThenThrowsNoSuchAlgorithmException() throws Exception {
		mockedStaticMessageDigest.when(() -> MessageDigest.getInstance("MD5")).thenThrow(new NoSuchAlgorithmException());

		heycentricHashingService.rawHashToMd5Hex("testString");
	}

	@Test
	public void getHeycentricMD5HashToHex_WhenProvidedRawStringNotNull_ThenReturnMD5HashAsHex() throws NoSuchAlgorithmException {
		String rawHashString = "testString";
		String expectedHash = "6162636465666768696a6b6c6d6e6f707172737475767778797a";
		mockedStaticMessageDigest.when(() -> MessageDigest.getInstance("MD5")).thenReturn(mockMessageDigest);
		when(mockMessageDigest.digest(rawHashString.getBytes(StandardCharsets.UTF_8))).thenReturn("abcdefghijklmnopqrstuvwxyz".getBytes(StandardCharsets.UTF_8));

		String result = heycentricHashingService.rawHashToMd5Hex(rawHashString);

		assertEquals(result, expectedHash);
	}

	@Test
	public void getHeycentricRawHashString_When_NoError_Then_CorrectlyFormattedHashReturned() {
		String client = "1234";
		String entity = "4321";
		String paymentType = "PP_TYPE";
		String privateKey = "5467";

		String expected = HeycentricPaymentConstants.HEADER_PARAMETER_CLIENT + "=" + client + "&";
		expected += HeycentricPaymentConstants.HEADER_PARAMETER_ENTITY + "=" + entity + "&";
		expected += HeycentricPaymentConstants.LINE_PARAMETER_PMTTYP + "=" + paymentType + "&";
		expected += HeycentricPaymentConstants.LINE_PARAMETER_AM + "=" + AMOUNT;
		expected += privateKey;

		when(mockHeycentricPaymentCompanyConfiguration.client()).thenReturn(client);
		when(mockHeycentricPaymentCompanyConfiguration.entity()).thenReturn(entity);
		when(mockHeycentricPaymentCompanyConfiguration.pmtTyp()).thenReturn(paymentType);
		when(mockHeycentricPaymentCompanyConfiguration.privateKey()).thenReturn(privateKey);

		String result = heycentricHashingService.getHeycentricRawHashString(mockHeycentricPaymentCompanyConfiguration, AMOUNT);

		assertEquals(result, expected);
	}

	@Test
	public void rawHashToSHA512_WhenValidInput_ThenReturnsExpectedHash() throws NoSuchAlgorithmException {
		String rawHashString = "testString";
		String expectedHash = "6162636465666768696a6b6c6d6e6f707172737475767778797a";
		mockedStaticMessageDigest.when(() -> MessageDigest.getInstance("SHA-512")).thenReturn(mockMessageDigest);
		when(mockMessageDigest.digest(rawHashString.getBytes())).thenReturn("abcdefghijklmnopqrstuvwxyz".getBytes());

		String result = heycentricHashingService.rawHashToSHA512(rawHashString);

		assertThat(result, equalTo(expectedHash));
	}

	@Test
	public void rawHashToSHA512_WhenEmptyInput_ThenReturnsBlank() throws NoSuchAlgorithmException {
		String result = heycentricHashingService.rawHashToSHA512(StringPool.BLANK);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test(expected = NoSuchAlgorithmException.class)
	public void rawHashToSHA512_WhenAlgorithmIsNotAvailable_ThenThrowsNoSuchAlgorithmException() throws Exception {
		mockedStaticMessageDigest.when(() -> MessageDigest.getInstance("SHA-512")).thenThrow(new NoSuchAlgorithmException());

		heycentricHashingService.rawHashToSHA512("testString");
	}

	@After
	public void tearDown() {
		mockedStaticMessageDigest.close();
	}

}
