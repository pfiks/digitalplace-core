package com.placecube.digitalplace.calendar.web.constants;

public final class DigitalPlaceCalendarConfigurationConstants {

	public static final String SCOPE_ALL = "ALL";

	public static final String SCOPE_GROUP = "GROUP";

	private DigitalPlaceCalendarConfigurationConstants() {
	}

}
