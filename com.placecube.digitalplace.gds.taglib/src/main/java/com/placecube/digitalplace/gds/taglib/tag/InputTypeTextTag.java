package com.placecube.digitalplace.gds.taglib.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.gds.taglib.context.ServletContextUtil;

public class InputTypeTextTag extends IncludeTag {

	private String cssClass;
	private String errorMessage;
	private String fieldLabel;
	private String fieldHint;
	private String fieldName;
	private String fieldValue;
	private String portletNamespace;
	private boolean required = false;
	private String type;
	private String inputWrapperCssClass;

	@Override
	protected void cleanUp() {
		super.cleanUp();
		cssClass = null;
		errorMessage = null;
		fieldLabel = null;
		fieldName = null;
		fieldHint = null;
		fieldValue = null;
		portletNamespace = null;
		required = false;
		type = null;
		inputWrapperCssClass = null;
	}

	@Override
	public int doStartTag() {
		setAttributeNamespace("gds-forms-ui:input-text");
		return EVAL_BODY_INCLUDE;
	}

	@Override
	protected String getPage() {
		return "/META-INF/taglibs/gds-forms-ui/input-text/view.jsp";
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute("cssClass", cssClass);
		request.setAttribute("errorMessage", errorMessage);
		request.setAttribute("fieldName", fieldName);
		request.setAttribute("fieldLabel", fieldLabel);
		request.setAttribute("fieldHint", fieldHint);
		request.setAttribute("fieldValue", HtmlUtil.escape(fieldValue));
		request.setAttribute("required", required);
		request.setAttribute("type", Validator.isNotNull(type) ? type : "text");
		request.setAttribute("portletNamespace", portletNamespace);
		request.setAttribute("inputWrapperCssClass", GetterUtil.getString(inputWrapperCssClass));
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public void setFieldHint(String fieldHint) {
		this.fieldHint = fieldHint;
	}

	public void setInputWrapperCssClass(String inputWrapperCssClass) {
		this.inputWrapperCssClass = inputWrapperCssClass;
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);
		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setPortletNamespace(String portletNamespace) {
		this.portletNamespace = portletNamespace;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public void setType(String type) {
		this.type = type;
	}

}
