package com.placecube.digitalplace.group.rest.service.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK;

import org.junit.Before;
import org.junit.Test;

public class GroupApiKeyPKUtilTest {

	private GroupApiKeyPKUtil groupApiKeyPKUtil;

	@Before
	public void activateSetup() {
		groupApiKeyPKUtil = new GroupApiKeyPKUtil();
	}

	@Test
	public void createGroupApiKeyPK_WhenNoErrors_ThenReturnsGroupApiKeyWithProperties() {
		final long companyId = 12332;
		final long groupId = 45332;

		GroupApiKeyPK result = groupApiKeyPKUtil.createGroupApiKeyPK(companyId, groupId);

		assertThat(result, notNullValue());
		assertThat(result.getCompanyId(), equalTo(companyId));
		assertThat(result.getGroupId(), equalTo(groupId));
	}
}
