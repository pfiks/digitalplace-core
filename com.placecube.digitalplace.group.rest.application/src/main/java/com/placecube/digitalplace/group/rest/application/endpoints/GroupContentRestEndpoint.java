package com.placecube.digitalplace.group.rest.application.endpoints;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetTag;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.group.rest.application.GroupRestApplication;
import com.placecube.digitalplace.group.rest.application.constants.RequestHeaders;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestException;
import com.placecube.digitalplace.group.rest.application.model.FileAttachment;
import com.placecube.digitalplace.group.rest.application.model.ModelBuilder;
import com.placecube.digitalplace.group.rest.application.model.RestContentEntry;
import com.placecube.digitalplace.group.rest.application.model.RestContentEntryBuilder;
import com.placecube.digitalplace.group.rest.application.model.RestListResult;
import com.placecube.digitalplace.group.rest.application.service.GroupContentService;
import com.placecube.digitalplace.group.rest.constants.GroupRestKeyType;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;

@Path("/content")
public class GroupContentRestEndpoint {

	private static final Log LOG = LogFactoryUtil.getLog(GroupContentRestEndpoint.class);

	private final GroupApiKeyLocalService groupApiKeyLocalService;

	private final GroupContentService groupContentService;

	private final ModelBuilder modelBuilder;

	private final RestContentEntryBuilder restContentEntryBuilder;

	public GroupContentRestEndpoint(GroupRestApplication restApplication) {
		groupApiKeyLocalService = restApplication.getGroupApiKeyLocalService();
		groupContentService = restApplication.getGroupContentService();
		modelBuilder = restApplication.getModelBuilder();
		restContentEntryBuilder = restApplication.getRestContentEntryBuilder();
	}

	@POST
	@Path("/dynamic-content")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addDynamicContentDetails(@FormParam("userId") long userId, @FormParam("ddlRecordSetId") long ddlRecordSetId, @FormParam("content") String content,
			@FormParam("fileAttachments") String fileAttachments, @FormParam("categoryIds") long[] categoryIds, @FormParam("tagNames") String[] tagNames,
			@HeaderParam(RequestHeaders.X_API_KEY) String apiKey) throws GroupRestException {

		LOG.debug("Update dynamic content asset entries - userId: " + userId + ", ddlRecordSetId: " + ddlRecordSetId + ", categoryIds: " + Arrays.toString(categoryIds) + ", tagNames: "
				+ Arrays.toString(tagNames) + ", content: " + content);

		try {

			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);

			ServiceContext serviceContext = groupContentService.getServiceContextAndRunAsUser(group, userId);

			DDLRecordSet ddlRecordSet = groupContentService.getDDLRecordSet(ddlRecordSetId);

			Set<FileAttachment> uploadedAttachments = groupContentService.uploadDDLAttachments(group, fileAttachments, ddlRecordSet, serviceContext);
			DDLRecord ddlRecord = groupContentService.addDDLRecord(userId, group.getGroupId(), ddlRecordSet, content, uploadedAttachments, categoryIds, tagNames, serviceContext);
			RestContentEntry resultContentEntry = restContentEntryBuilder.getDynamicContentFullDetails(ddlRecord);

			return restContentEntryBuilder.getSuccessResponse(resultContentEntry);

		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}

	}

	@GET
	@Path("/ddl-record-sets")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDDLRecordSets(@HeaderParam(RequestHeaders.X_API_KEY) String apiKey) throws GroupRestException {
		try {
			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);

			List<DDLRecordSet> recordSets = groupContentService.getDDLRecordSets(group);
			RestListResult result = RestListResult.init();
			for (DDLRecordSet recordSet : recordSets) {
				result.addResult(restContentEntryBuilder.getRestContentEntryForDDLRecordSet(recordSet));
			}

			return restContentEntryBuilder.getSuccessResponse(result);
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}
	}

	@GET
	@Path("/ddl-structure/{structureId}/fields")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDDLStructureFieldValues(@PathParam("structureId") long structureId, @HeaderParam(RequestHeaders.X_API_KEY) String apiKey) throws GroupRestException {
		try {
			RestListResult result = RestListResult.init();

			List<DDMFormField> ddmFormFields = groupContentService.getStructureFieldsWithPreSetValues(structureId);

			for (DDMFormField ddmFormField : ddmFormFields) {
				result.addResult(restContentEntryBuilder.getRestContentEntryForDDMFormField(ddmFormField));
			}

			return restContentEntryBuilder.getSuccessResponse(result);
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}
	}

	@GET
	@Path("/ddl-structures")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDDLStructures(@HeaderParam(RequestHeaders.X_API_KEY) String apiKey) throws GroupRestException {
		try {
			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);

			List<DDMStructure> structures = groupContentService.getDDLStructures(group);
			RestListResult result = RestListResult.init();
			for (DDMStructure structure : structures) {
				result.addResult(restContentEntryBuilder.getRestContentEntryForDDMStructure(structure));
			}

			return restContentEntryBuilder.getSuccessResponse(result);
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}
	}

	@GET
	@Path("/dynamic-content/{contentId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDynamicContentDetails(@PathParam("contentId") long contentId, @HeaderParam(RequestHeaders.X_API_KEY) String apiKey) throws GroupRestException {

		LOG.debug("Get dynamic content - contentId: " + contentId);
		try {
			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);
			DDLRecord ddlRecord = restContentEntryBuilder.getDDLRecord(group, contentId);
			RestContentEntry resultContentEntry = restContentEntryBuilder.getDynamicContentFullDetails(ddlRecord);
			return restContentEntryBuilder.getSuccessResponse(resultContentEntry);
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}

	}

	@GET
	@Path("/dynamic-content/{structureId}/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDynamicContentSearch(@PathParam("structureId") long structureId, @HeaderParam(RequestHeaders.X_API_KEY) String apiKey, @FormParam("sortByRating") boolean sortByRating,
			@FormParam("startPage") Integer startPage, @FormParam("resultsPerPage") Integer resultsPerPage, @FormParam("sameFilterConditionsInOr") boolean sameFilterConditionsInOr,
			@FormParam("categoryId") Long[] categoryIds, @FormParam("tagName") String[] tagNames, @FormParam("customField") String[] customFields, @FormParam("creatorUserId") Long creatorUserId,
			@FormParam("truncateDescriptionChars") Integer truncateDescriptionChars, @FormParam("retrieveResultsFullDetails") boolean retrieveResultsFullDetails) throws GroupRestException {

		LOG.debug("Search dynamic content - sortByRating: " + sortByRating + ", startPage: " + startPage + ", resultsPerPage: " + resultsPerPage + ", categoryIds: " + Arrays.toString(categoryIds)
				+ ", truncateDescription: " + truncateDescriptionChars + ", retrieveResultsFullDetails: " + retrieveResultsFullDetails + ", creatorUserId: " + creatorUserId);

		groupContentService.validateParameters(startPage, resultsPerPage, truncateDescriptionChars);

		try {

			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);

			Hits hits = executeDynamicContentSearch(structureId, sortByRating, startPage, resultsPerPage, sameFilterConditionsInOr, creatorUserId, categoryIds, tagNames, customFields, group);

			String portalURL = groupContentService.getPortalURL(group);

			RestListResult result = RestListResult.init();
			for (Document searchResult : hits.getDocs()) {

				Optional<RestContentEntry> resultContentEntryOptional = restContentEntryBuilder.getRestContentEntryForDynamicContent(searchResult, truncateDescriptionChars, portalURL);

				resultContentEntryOptional.ifPresent(restContentEntry -> {

					if (retrieveResultsFullDetails) {
						restContentEntryBuilder.configureFullDetailsInResult(restContentEntry);
					}

					result.addResult(restContentEntry);

				});

			}

			return modelBuilder.getSuccessResponse(result);
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}
	}

	@GET
	@Path("/categories")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupCategories(@HeaderParam(RequestHeaders.X_API_KEY) String apiKey) throws GroupRestException {
		try {
			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);
			List<AssetCategory> categories = groupContentService.getGroupCategories(group);
			RestListResult result = RestListResult.init();

			for (AssetCategory assetCategory : categories) {
				result.addResult(restContentEntryBuilder.getRestContentEntryForAssetCategory(assetCategory));
			}

			return restContentEntryBuilder.getSuccessResponse(result);
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}
	}

	@GET
	@Path("/tags")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupTags(@HeaderParam(RequestHeaders.X_API_KEY) String apiKey) throws GroupRestException {
		try {
			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);

			List<AssetTag> tags = groupContentService.getGroupContentTags(group);
			RestListResult result = RestListResult.init();

			for (AssetTag assetTag : tags) {
				result.addResult(restContentEntryBuilder.getRestContentEntryForAssetTag(assetTag));
			}

			return restContentEntryBuilder.getSuccessResponse(result);
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}
	}

	@GET
	@Path("/vocabularies")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupVocabularies(@HeaderParam(RequestHeaders.X_API_KEY) String apiKey) throws GroupRestException {
		try {
			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);

			List<AssetVocabulary> vocabularies = groupContentService.getGroupVocabularies(group);
			RestListResult result = RestListResult.init();

			for (AssetVocabulary assetVocabulary : vocabularies) {
				result.addResult(restContentEntryBuilder.getRestContentEntryForAssetVocabulary(assetVocabulary));
			}

			return restContentEntryBuilder.getSuccessResponse(result);
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}
	}

	@GET
	@Path("/user-details")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserDetails(@HeaderParam(RequestHeaders.X_API_KEY) String apiKey, @FormParam("emailAddress") String emailAddress) throws GroupRestException {
		try {
			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);

			User user = groupContentService.fetchUser(group, emailAddress);

			return restContentEntryBuilder.getSuccessResponse(restContentEntryBuilder.getRestContentUser(group, user, emailAddress));
		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}
	}

	@PUT
	@Path("/dynamic-content/{contentId}/asset-entries")
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateDynamicContentAssetEntries(@FormParam("userId") long userId, @PathParam("contentId") long contentId, @FormParam("categoryIds") long[] categoryIds,
			@FormParam("tagNames") String[] tagNames, @HeaderParam(RequestHeaders.X_API_KEY) String apiKey) throws GroupRestException {

		LOG.debug("Update dynamic content asset entries - contentId: " + contentId + ", categoryIds: " + Arrays.toString(categoryIds) + ", tagNames: " + Arrays.toString(tagNames));

		try {

			Group group = groupApiKeyLocalService.getGroupByKey(GroupRestKeyType.APPLICATION_KEY, apiKey);

			DDLRecord ddlRecord = restContentEntryBuilder.getDDLRecord(group, contentId);

			groupContentService.updateDDLRecordAssetEntries(userId, ddlRecord, categoryIds, tagNames);

		} catch (Exception e) {
			LOG.warn(e);
			throw new GroupRestException(e);
		}

	}

	private Hits executeDynamicContentSearch(long structureId, boolean sortByRating, Integer startPage, Integer resultsPerPage, boolean sameFilterConditionsInOr, Long creatorUserId,
			Long[] categoryIds, String[] tagNames, String[] customFields, Group group) throws Exception {

		Map<String, List<String>> customFieldsValues = groupContentService.parseCustomFieldParameters(structureId, customFields);

		long adminUserId = groupContentService.runAsAdministrator(group.getCompanyId());
		SearchContext searchContext = groupContentService.getSearchContextForAdminDynamicDataListSearch(adminUserId, group, structureId);
		groupContentService.configureSearchContextFiltersAndSorts(searchContext, sortByRating, sameFilterConditionsInOr, categoryIds, tagNames, customFieldsValues);
		groupContentService.configureSearchContextWithCreatorUserId(searchContext, creatorUserId);
		return groupContentService.executeSearch(searchContext, startPage, resultsPerPage);

	}

}
