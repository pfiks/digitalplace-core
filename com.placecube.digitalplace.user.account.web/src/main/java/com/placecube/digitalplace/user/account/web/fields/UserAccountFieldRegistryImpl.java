package com.placecube.digitalplace.user.account.web.fields;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;

@Component(immediate = true, service = UserAccountFieldRegistry.class)
public class UserAccountFieldRegistryImpl implements UserAccountFieldRegistry {

	private static final Log LOG = LogFactoryUtil.getLog(UserAccountFieldRegistry.class);

	private List<UserAccountField> userAccountFields = new ArrayList<>();

	@Override
	public List<UserAccountField> getUserAccountFields(long companyId) {
		return userAccountFields.stream().filter(f -> f.isEnabled(companyId)).collect(Collectors.toList());
	}

	@Override
	public List<UserAccountField> getUserAccountFieldsByStep(AccountCreationStep accountCreationStep, long companyId) {
		return getUserAccountFields(companyId).stream().filter(f -> f.getAccountCreationStep() == accountCreationStep).collect(Collectors.toList());
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setUserAccountField(UserAccountField userAccountField) {
		LOG.debug("Registering UserAccountField with bundleId: " + userAccountField.getBundleId() + ", displayOrder: " + userAccountField.getDisplayOrder() + ", step: "
				+ userAccountField.getAccountCreationStep());

		userAccountFields.add(userAccountField);
		Collections.sort(userAccountFields, Comparator.comparingInt(UserAccountField::getDisplayOrder));
	}

	protected void unsetUserAccountField(UserAccountField userAccountField) {
		LOG.debug("Unregistering UserAccountField with bundleId: " + userAccountField.getBundleId());

		userAccountFields.remove(userAccountField);
	}

}