/**
 * CardSurchargeRate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class CardSurchargeRate  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.AcceptedCardType surchargeCardType;

    private com.capita_software_services.www.scp.base.SurchargeBasis surchargeBasis;

    public CardSurchargeRate() {
    }

    public CardSurchargeRate(
           com.capita_software_services.www.scp.base.AcceptedCardType surchargeCardType,
           com.capita_software_services.www.scp.base.SurchargeBasis surchargeBasis) {
           this.surchargeCardType = surchargeCardType;
           this.surchargeBasis = surchargeBasis;
    }


    /**
     * Gets the surchargeCardType value for this CardSurchargeRate.
     *
     * @return surchargeCardType
     */
    public com.capita_software_services.www.scp.base.AcceptedCardType getSurchargeCardType() {
        return surchargeCardType;
    }


    /**
     * Sets the surchargeCardType value for this CardSurchargeRate.
     *
     * @param surchargeCardType
     */
    public void setSurchargeCardType(com.capita_software_services.www.scp.base.AcceptedCardType surchargeCardType) {
        this.surchargeCardType = surchargeCardType;
    }


    /**
     * Gets the surchargeBasis value for this CardSurchargeRate.
     *
     * @return surchargeBasis
     */
    public com.capita_software_services.www.scp.base.SurchargeBasis getSurchargeBasis() {
        return surchargeBasis;
    }


    /**
     * Sets the surchargeBasis value for this CardSurchargeRate.
     *
     * @param surchargeBasis
     */
    public void setSurchargeBasis(com.capita_software_services.www.scp.base.SurchargeBasis surchargeBasis) {
        this.surchargeBasis = surchargeBasis;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardSurchargeRate)) return false;
        CardSurchargeRate other = (CardSurchargeRate) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.surchargeCardType==null && other.getSurchargeCardType()==null) ||
             (this.surchargeCardType!=null &&
              this.surchargeCardType.equals(other.getSurchargeCardType()))) &&
            ((this.surchargeBasis==null && other.getSurchargeBasis()==null) ||
             (this.surchargeBasis!=null &&
              this.surchargeBasis.equals(other.getSurchargeBasis())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSurchargeCardType() != null) {
            _hashCode += getSurchargeCardType().hashCode();
        }
        if (getSurchargeBasis() != null) {
            _hashCode += getSurchargeBasis().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardSurchargeRate.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardSurchargeRate"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeCardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeCardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "acceptedCardType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeBasis");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeBasis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeBasis"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
