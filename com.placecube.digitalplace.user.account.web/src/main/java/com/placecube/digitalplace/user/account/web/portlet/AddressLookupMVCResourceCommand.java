package com.placecube.digitalplace.user.account.web.portlet;

import java.util.Optional;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.user.account.web.util.AccountAddressUtils;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.ADDRESS_LOOKUP }, service = MVCResourceCommand.class)
public class AddressLookupMVCResourceCommand implements MVCResourceCommand {

	private static final Log LOG = LogFactoryUtil.getLog(AddressLookupMVCResourceCommand.class);

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private AddressLookupService addressLookupService;

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
		String uprn = ParamUtil.getString(resourceRequest, RequestKeys.UPRN);

		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(resourceRequest);

			Optional<AddressContext> addressContext = addressLookupService.getByUprn(accountContext.getCompanyId(), uprn, new String[0],
					accountConfigurationService.getAccountConfiguration(accountContext, resourceRequest).addressOutOfLocalArea());
			accountContextService.updateContextInSession(accountContext, resourceRequest);

			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

			addressContext.ifPresent(a -> jsonObject.put("fullAddress", AccountAddressUtils.getFullAddress(a, "<br>")));

			resourceResponse.getWriter().print(jsonObject.toJSONString());
			resourceResponse.getWriter().close();

			LOG.debug("Address for UPRN " + uprn + " JSON:" + jsonObject.toJSONString());
			return true;

		} catch (Exception e) {
			LOG.error("Error retrieving address for uprn: " + uprn, e);
			return false;
		}
	}

}