package com.placecube.digitalplace.externalpage.web.cleaner;

import org.htmlcleaner.ContentNode;
import org.htmlcleaner.HtmlNode;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.TagNodeVisitor;

import com.liferay.petra.string.StringPool;

public class LeadingSpaceTagNodeVisitor implements TagNodeVisitor {

	@Override
	public boolean visit(TagNode parentNode, HtmlNode htmlNode) {
		if (htmlNode instanceof ContentNode) {
			ContentNode contentNode = (ContentNode) htmlNode;

			parentNode.insertChildAfter(htmlNode, HtmlCleanerFactory.createContentNode(StringPool.SPACE + contentNode.getContent()));
			parentNode.removeChild(htmlNode);
		}

		return true;
	}
}
