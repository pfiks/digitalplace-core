<%@ include file="init.jsp" %>
<%
String redirect = ParamUtil.getString(request, "redirect");
portletDisplay.setShowBackIcon(true);
portletDisplay.setURLBack(redirect);

String dataProviderName = (String)request.getAttribute(PortletRequestKeys.DATA_PROVIDER_NAME);
dataProviderName = HtmlUtil.stripHtml(dataProviderName);
renderResponse.setTitle(dataProviderName);
%>

<div class="container">

	<portlet:actionURL name="/data-consumer-save" var="mappingUrl">
		<portlet:param name="currentTab" value="data-consumers" />
		<portlet:param name="formInstanceId" value="${ formInstanceId }" />
		<portlet:param name="dataProviderInstanceId" value="${ dataProviderInstanceId }" />
		<portlet:param name="sourceDataProviderId" value="${ sourceDataProviderId }" />
		<portlet:param name="dataConsumerId" value="${ dataConsumerId }" />
		<portlet:param name="chain" value="${ chain }" />
		<portlet:param name="originalDataProviderInstanceId" value="${ originalDataProviderInstanceId }" />
	</portlet:actionURL>

	<aui:form cssClass="container-fluid container-fluid-max-xl" name="fm" action="<%= mappingUrl %>">
		<dp-frontend:fieldset-group>
			<aui:fieldset label="service-details" cssClass="pt-2 pl-3">
				<aui:row>
					<aui:col>
						<aui:input label="service-url" name="url" value="${ url }" type="text" disabled="true" />
					</aui:col>
				</aui:row>
				<aui:row>
					<aui:col>
						<c:set var="bodySupportedMethodsString" value="${bodySupportedMethods }" />
						<aui:select label="method" name="method" required="true" showEmptyOption="true" onChange='<%= "toggleBodyFieldFn('" + renderResponse.getNamespace() + "', this, '" + (String)pageContext.getAttribute("bodySupportedMethodsString") + "');" %>'>
							<c:forEach items="${ methods }" var="method">
								<c:set var="markSelected" value="" />
								<c:if test="${ method == selectedMethod }">
									<c:set var="markSelected" value="selected" />
								</c:if>
								<option value="${ method }" ${ markSelected } label="${ method }" />
							</c:forEach>
						</aui:select>
					</aui:col>
				</aui:row>

			</aui:fieldset>
		</dp-frontend:fieldset-group>
		<dp-frontend:fieldset-group>
			<aui:fieldset label="header-details" cssClass="pt-2 pl-3">
				<div id="auto-fields-container">
					<c:if test="${ empty serviceHeaders }">
						<div class="lfr-form-row lfr-form-row-inline">
							<div class="row">
								<div class="col-md-6">
									<aui:input label="header-name" name="headerName" />
								</div>
								<div class="col-md-6">
									<aui:input label="header-value" name="headerValue" />
								</div>
							</div>
						</div>
					</c:if>
					<c:forEach items="${serviceHeaders}" var="serviceHeader">
						<div class="lfr-form-row lfr-form-row-inline">
							<div class="row">
								<div class="col-md-6">
									<aui:input label="header-name" value="${ serviceHeader.key }" name="headerName" />
								</div>
								<div class="col-md-6">
									<aui:input label="header-value" value="${ serviceHeader.value }" name="headerValue" />
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</aui:fieldset>
		</dp-frontend:fieldset-group>
		<dp-frontend:fieldset-group>
			<aui:row>
				<aui:col width="50">
					<aui:fieldset label="input-parameters" cssClass="pt-2 pl-3">
						<c:forEach items="${inputFields}" var="serviceInputField">
							<aui:row>
								<aui:col>
									<aui:select label="${ serviceInputField.name }"
										name="p_${ serviceInputField.name }" required="${ serviceInputField.required }"
										showEmptyOption="true">

										<c:if test="${ not empty form }">
											<c:forEach items="${ddmFormFields}" var="field">
												<c:set var="markSelected" value="" />
												<c:if
													test="${ field.getName() == mapper.get(serviceInputField.name) }">
													<c:set var="markSelected" value="selected" />
												</c:if>
												<option value="${ field.getName() }" ${ markSelected }
													label="${ field.getFieldReference() }" />
											</c:forEach>
										</c:if>
										<c:if test="${ not empty sourceOutputFields }">

											<c:forEach items="${ sourceOutputFields }" var="field">
												<c:set var="markSelected" value="" />
												<c:if
													test="${ field.getPath() == mapper.get(serviceInputField.name) }">
													<c:set var="markSelected" value="selected" />
												</c:if>
												<option value="${ field.getPath() }" ${ markSelected }
													label="${ field.getPath() }" />
											</c:forEach>

										</c:if>

									</aui:select>
								</aui:col>
							</aui:row>
						</c:forEach>
					</aui:fieldset>
				</aui:col>
				<aui:col width="50">
					<aui:fieldset label="output-parameters" cssClass="pt-2 pl-3">
						<c:forEach items="${outputFields}" var="serviceoutput">
							<aui:row>
								<aui:col>
									${ serviceoutput.path }
								</aui:col>
							</aui:row>
						</c:forEach>
					</aui:fieldset>
				</aui:col>
			</aui:row>
			
			<aui:row id="bodyPanel" cssClass="${not empty selectedMethod && fn:contains(bodySupportedMethods, selectedMethod) ? '' : 'hide' }">
				<aui:col>
					<aui:fieldset label="request-body" helpMessage="request-body-help" cssClass="pt-2 pl-3">
						<aui:row>
							<aui:col>
								<aui:input label="" name="requestBody" type="textarea" value="${requestBody }" cssClass="h-auto" rows="10"/>
							</aui:col>
						</aui:row>
					</aui:fieldset>					
				</aui:col>
			</aui:row>
		</dp-frontend:fieldset-group>
		<aui:button-row>
			<aui:button name="submitButton" type="submit" value="save" />
			<portlet:renderURL var="cancelUrl">
				<portlet:param name="mvcRenderCommandName" value="/select-data-provider" />
				<portlet:param name="currentTab" value="data-consumers" />
				<portlet:param name="formInstanceId" value="${ formInstanceId }" />
				<portlet:param name="dataProviderInstanceId" value="${ dataProviderInstanceId }" />
			</portlet:renderURL>
			<a href="${ cancelUrl }" class="btn btn-secondary">
				<liferay-ui:message key="cancel" />
			</a>
		</aui:button-row>
	</aui:form>
</div>

<aui:script use="liferay-auto-fields">
	new Liferay.AutoFields({
		contentBox: '#auto-fields-container',
		fieldIndexes: '<portlet:namespace />rowIndexes'
	}).render();
	
	Liferay.provide(window, 'toggleBodyField', toggleBodyFieldFn);
</aui:script>