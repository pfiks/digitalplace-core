package com.placecube.digitalplace.search.sortby.mostactive.triggers;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.UserLocalServiceWrapper;

@Component(immediate = true, property = {}, service = ServiceWrapper.class)
public class UserLocalServiceOverride extends UserLocalServiceWrapper {

	@Reference
	private IndexerRegistry indexerRegistry;

	public UserLocalServiceOverride() {
		super(null);
	}

	@Override
	public User updateLastLogin(long userId, String loginIP) throws PortalException {
		User user = super.updateLastLogin(userId, loginIP);

		indexerRegistry.nullSafeGetIndexer(User.class).reindex(user);

		return user;
	}

}
