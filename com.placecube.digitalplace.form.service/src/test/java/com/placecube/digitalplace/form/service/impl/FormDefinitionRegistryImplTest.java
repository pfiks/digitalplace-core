package com.placecube.digitalplace.form.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.placecube.digitalplace.form.exception.FormDefinitionException;
import com.placecube.digitalplace.form.model.FormDefinition;

public class FormDefinitionRegistryImplTest {

	private static final String KEY = "key";

	private FormDefinitionRegistryImpl formDefinitionRegistryImpl;

	@Mock
	private FormDefinition mockFormDefinition;

	@Before
	public void setUp() {

		initMocks(this);

		formDefinitionRegistryImpl = new FormDefinitionRegistryImpl();

	}

	@Test(expected = FormDefinitionException.class)
	public void getFormDefinitionByKey_WhenFormDefinitionDoesNotExistInMap_ThenFormDefinitionExceptionIsThrown() throws FormDefinitionException {

		formDefinitionRegistryImpl.getFormDefinitionByKey("form-definition-does-not-exist");

	}

	@Test
	public void getFormDefinitionByKey_WhenFormDefinitionExistsInMap_ThenReturnsFormDefinition() throws FormDefinitionException {

		when(mockFormDefinition.getKey()).thenReturn(KEY);
		formDefinitionRegistryImpl.setFormDefinition(mockFormDefinition);

		FormDefinition result = formDefinitionRegistryImpl.getFormDefinitionByKey(KEY);

		assertThat(result, sameInstance(mockFormDefinition));

	}

	@Test
	public void setFormDefinition_WhenNoError_ThenPutsFormDefitionInMap() {

		when(mockFormDefinition.getKey()).thenReturn(KEY);

		formDefinitionRegistryImpl.setFormDefinition(mockFormDefinition);

		assertThat(formDefinitionRegistryImpl.getFormNamesAndFormDefinitions().size(), equalTo(1));

	}

	@Test
	public void getFormNamesAndFormDefinitions_WhenThereIsNotFormDefinition_ThenReturnsEmptyMap() {

		assertTrue(formDefinitionRegistryImpl.getFormNamesAndFormDefinitions().isEmpty());

	}

	@Test
	public void getFormNamesAndFormDefinitions_WhenThereIsFormDefinition_ThenReturnsMap() {

		when(mockFormDefinition.getKey()).thenReturn(KEY);
		formDefinitionRegistryImpl.setFormDefinition(mockFormDefinition);

		assertFalse(formDefinitionRegistryImpl.getFormNamesAndFormDefinitions().isEmpty());

	}

	@Test
	public void unsetFormDefinition_WhenNoError_ThenRemoveFormDefinitionFromMap() throws FormDefinitionException {

		when(mockFormDefinition.getKey()).thenReturn(KEY);
		formDefinitionRegistryImpl.setFormDefinition(mockFormDefinition);

		formDefinitionRegistryImpl.unsetFormDefinition(mockFormDefinition);

		try {
			formDefinitionRegistryImpl.getFormDefinitionByKey(KEY);
			assertTrue(false);
		} catch (FormDefinitionException e) {
			assertTrue(true);
		}

	}
}
