package com.placecube.digitalplace.notifications.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendMailContext;
import com.placecube.digitalplace.notifications.model.SendMailContext.SendMailContextBuilder;
import com.placecube.digitalplace.notifications.model.SendSmsContext;
import com.placecube.digitalplace.notifications.service.NotificationMailConnector;
import com.placecube.digitalplace.notifications.service.NotificationSmsConnector;
import com.placecube.digitalplace.notifications.service.internal.emaildefault.DefaultEmailNotificationService;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

public class NotificationServiceImplTest extends PowerMockito {

	private static final Long COMPANY_ID = 11l;

	@Mock
	private DefaultEmailNotificationService mockDefaultEmailNotificationService;

	@Mock
	private NotificationMailConnector mockNotificationMailConnector1;

	@Mock
	private NotificationMailConnector mockNotificationMailConnector2;

	@Mock
	private NotificationSmsConnector mockNotificationSmsConnector1;

	@Mock
	private NotificationSmsConnector mockNotificationSmsConnector2;

	@Mock
	private SendMailContext mockSendMailContext;

	@Mock
	private SendSmsContext mockSendSmsContext;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private NotificationServiceImpl notificationServiceImpl;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void sendMail_WhenAllFieldsValidAndNotificationMailConnectorFound_ThenCallsSendMailFromTheConnector() throws NotificationsException, ConfigurationException {
		when(mockSendMailContext.getTemplateId()).thenReturn("templateValue");
		when(mockSendMailContext.getEmailAddress()).thenReturn("emailValue");
		addNotificationMailConnector(mockNotificationMailConnector1, false);
		addNotificationMailConnector(mockNotificationMailConnector2, true);

		notificationServiceImpl.sendMail(mockSendMailContext, mockServiceContext);

		verify(mockNotificationMailConnector2, times(1)).sendMail(mockSendMailContext, mockServiceContext);
		verifyZeroInteractions(mockDefaultEmailNotificationService);
	}

	@Test(expected = NotificationsException.class)
	public void sendMail_WhenEmailAddressIsNull_ThenThrowsNotificationsException() throws NotificationsException, ConfigurationException {
		when(mockSendMailContext.getTemplateId()).thenReturn("templateValue");
		when(mockSendMailContext.getEmailAddress()).thenReturn(null);

		notificationServiceImpl.sendMail(mockSendMailContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	public void sendMail_WhenTemplateIdIsNull_ThenThrowsNotificationsException() throws NotificationsException, ConfigurationException {
		when(mockSendMailContext.getTemplateId()).thenReturn(null);
		when(mockSendMailContext.getEmailAddress()).thenReturn("emailValue");

		notificationServiceImpl.sendMail(mockSendMailContext, mockServiceContext);
	}

	@Test
	public void sendMail_WhenTemplateIdIsValidAndNoNotificationMailConnectorFound_ThenSendsEmailWithTheDefaultService() throws NotificationsException, ConfigurationException {
		when(mockSendMailContext.getTemplateId()).thenReturn("templateValue");
		when(mockSendMailContext.getEmailAddress()).thenReturn("emailValue");
		addNotificationSmsConnector(mockNotificationSmsConnector1, true);
		addNotificationMailConnector(mockNotificationMailConnector1, false);
		addNotificationMailConnector(mockNotificationMailConnector2, false);

		notificationServiceImpl.sendMail(mockSendMailContext, mockServiceContext);

		verify(mockDefaultEmailNotificationService, times(1)).sendMail(mockSendMailContext, mockServiceContext);
	}

	@Test
	public void sendSms_WhenAllFieldsValidAndNotificationSmsConnectorFound_ThenCallsSendSms() throws NotificationsException, ConfigurationException {
		when(mockSendSmsContext.getTemplateId()).thenReturn("templateValue");
		when(mockSendSmsContext.getPhoneNumber()).thenReturn("phoneValue");
		addNotificationSmsConnector(mockNotificationSmsConnector1, false);
		addNotificationSmsConnector(mockNotificationSmsConnector2, true);

		notificationServiceImpl.sendSms(mockSendSmsContext, mockServiceContext);

		verify(mockNotificationSmsConnector2, times(1)).sendSms(mockSendSmsContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	public void sendSms_WhenPhoneNumberIsNull_ThenThrowsNotificationsException() throws NotificationsException, ConfigurationException {
		when(mockSendSmsContext.getTemplateId()).thenReturn("templateValue");
		when(mockSendSmsContext.getPhoneNumber()).thenReturn(null);

		notificationServiceImpl.sendSms(mockSendSmsContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	public void sendSms_WhenTemplateIdIsNull_ThenThrowsNotificationsException() throws NotificationsException, ConfigurationException {
		when(mockSendSmsContext.getTemplateId()).thenReturn(null);
		when(mockSendSmsContext.getPhoneNumber()).thenReturn("phoneValue");

		notificationServiceImpl.sendSms(mockSendSmsContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	public void sendSms_WhenTemplateIdIsValidAndNoNotificationSmsConnectorFound_ThenThrowsNotificationsException() throws NotificationsException, ConfigurationException {
		when(mockSendSmsContext.getTemplateId()).thenReturn("templateValue");
		when(mockSendSmsContext.getPhoneNumber()).thenReturn("phoneValue");
		addNotificationSmsConnector(mockNotificationSmsConnector1, false);
		addNotificationSmsConnector(mockNotificationSmsConnector2, false);
		addNotificationMailConnector(mockNotificationMailConnector1, true);

		notificationServiceImpl.sendSms(mockSendSmsContext, mockServiceContext);
	}

	@Test
	public void createSendMailContextBuilder_WhenNoErrors_ThenReturnsSendMailContextBuilderWithRightParameters() {
		final String emailAddress = "emailTo@test.com";
		final String fromEmailAddress = "emailFrom@test.com";
		final String reference = "4353433";
		final String templateId = "TEMPLATE_ID";

		Map<String, Object> personalisation = new HashMap<>();
		personalisation.put("key", "value");

		SendMailContextBuilder builder = notificationServiceImpl.createSendMailContextBuilder(templateId, emailAddress, personalisation, reference, fromEmailAddress);

		SendMailContext sendMailContext = builder.build();

		assertThat(sendMailContext.getTemplateId(), equalTo(templateId));
		assertThat(sendMailContext.getEmailAddress(), equalTo(emailAddress));
		assertThat(sendMailContext.getPersonalisation(), sameInstance(personalisation));
		assertThat(sendMailContext.getReference(), sameInstance(reference));
		assertThat(sendMailContext.getFromEmailAddress(), sameInstance(fromEmailAddress));
	}

	private void addNotificationMailConnector(NotificationMailConnector notificationMailConnector, boolean enabled) {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(notificationMailConnector.enabled(COMPANY_ID)).thenReturn(enabled);
		notificationServiceImpl.setNotificationMailConnectors(notificationMailConnector);
	}

	private void addNotificationSmsConnector(NotificationSmsConnector notificationSmsConnector, boolean enabled) {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(notificationSmsConnector.enabled(COMPANY_ID)).thenReturn(enabled);
		notificationServiceImpl.setNotificationSmsConnectors(notificationSmsConnector);
	}
}
