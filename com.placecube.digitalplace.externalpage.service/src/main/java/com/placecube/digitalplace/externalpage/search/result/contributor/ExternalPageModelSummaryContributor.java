
package com.placecube.digitalplace.externalpage.search.result.contributor;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.search.spi.model.result.contributor.ModelSummaryContributor;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage", service = ModelSummaryContributor.class)
public class ExternalPageModelSummaryContributor implements ModelSummaryContributor {

	@Override
	public Summary getSummary(Document document, Locale locale, String snippet) {
		Summary summary = new Summary(document.get(Field.TITLE), document.get(Field.CONTENT));
		summary.setMaxContentLength(200);
		return summary;
	}

}