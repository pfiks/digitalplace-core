package com.placecube.digitalplace.externalpage.web.backgroundtask;

import static org.mockito.Mockito.times;

import com.liferay.portal.kernel.backgroundtask.BackgroundTask;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.pfiks.mail.service.MailService;
import com.placecube.digitalplace.externalpage.web.constants.BackgroundTaskKeys;
import com.placecube.digitalplace.externalpage.web.crawler.ExternalPageCrawler;
import com.placecube.digitalplace.externalpage.web.service.CrawlModelBuilder;
import com.placecube.digitalplace.externalpage.web.service.CrawlUtil;
import com.placecube.digitalplace.externalpage.web.service.ExternalPageImportValidator;

import java.io.Serializable;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, CrawlUtil.class, SessionErrors.class })
public class ExternalPageImportBackgroundTaskExecutorTest extends PowerMockito {

	private static final long COMPANY_ID = 10l;
	private static final String CUSTOM_DATA = "customDataValue";
	private static final String EMAIL_ADDRESS = "support@placecube.com";
	private static final String INDEX_ARRAY = "indexArray";
	private static final Integer MAX_PAGES_TO_FECTH = 1;
	private static final long SCOPED_GROUP_ID = 2301l;

	private static final int TOTAL = 2;
	private static final int TOTAL_URL_COUNT = 1;
	private static final String URL = "urlValue";
	private static final long USER_ID = 101l;
	private static final String USER_NAME = "userValue";
	private static final String XPATH = "xPathValue";

	@InjectMocks
	private ExternalPageImportBackgroundTaskExecutor externalPageImportBackgroundTaskExecutor;

	@Mock
	private BackgroundTask mockBackgroundTask;

	@Mock
	private CrawlConfig mockCrawlConfig;

	@Mock
	private CrawlController mockCrawlController;

	@Mock
	private CrawlModelBuilder mockCrawlModelBuilder;

	@Mock
	private MailService mockEmailService;

	@Mock
	private ExternalPageImportValidator mockExternalPageImportValidator;

	@Mock
	private PageFetcher mockPageFetcher;

	@Mock
	private RobotstxtConfig mockRobotstxtConfig;

	@Mock
	private RobotstxtServer mockRobotstxtServer;

	@Mock
	private Map<String, Serializable> taskContextMap;

	@Test
	public void backGroundTaskExecutor_WhenCrawlCompleted_ThenSendEmailNotification() throws Exception {
		mockControllerConfigs();

		when(mockCrawlModelBuilder.getCrawlController(mockCrawlConfig, mockPageFetcher, mockRobotstxtServer)).thenReturn(mockCrawlController);
		when(CrawlUtil.generateCustomData(XPATH, INDEX_ARRAY, COMPANY_ID, SCOPED_GROUP_ID, USER_ID)).thenReturn(CUSTOM_DATA);

		externalPageImportBackgroundTaskExecutor.execute(mockBackgroundTask);

		InOrder inOrder = Mockito.inOrder(mockCrawlController);
		inOrder.verify(mockCrawlController, times(1)).setCustomData(CUSTOM_DATA);
		inOrder.verify(mockCrawlController, times(1)).addSeed(URL);
		inOrder.verify(mockCrawlController, times(1)).start(ExternalPageCrawler.class, 7);

		Mockito.verify(mockEmailService, times(1)).sendEmail(EMAIL_ADDRESS, USER_NAME, "Site Crawler Status",
				" Total URLs spidered:" + MAX_PAGES_TO_FECTH + "- total new URLs added to external pages results list:" + TOTAL);
	}

	@Test
	public void backGroundTaskExecutor_WhenValidationPasses_AndMaxPagesToFetchIsNotEmpty_ThenConfiguresAndStartsTheCrawlController() throws Exception {
		mockControllerConfigs();
		when(mockCrawlModelBuilder.getCrawlController(mockCrawlConfig, mockPageFetcher, mockRobotstxtServer)).thenReturn(mockCrawlController);
		when(CrawlUtil.generateCustomData(XPATH, INDEX_ARRAY, COMPANY_ID, SCOPED_GROUP_ID, USER_ID)).thenReturn(CUSTOM_DATA);

		externalPageImportBackgroundTaskExecutor.execute(mockBackgroundTask);

		InOrder inOrder = Mockito.inOrder(mockCrawlController);
		inOrder.verify(mockCrawlController, times(1)).setCustomData(CUSTOM_DATA);
		inOrder.verify(mockCrawlController, times(1)).addSeed(URL);
		inOrder.verify(mockCrawlController, times(1)).start(ExternalPageCrawler.class, 7);

		Mockito.verify(mockEmailService, times(1)).sendEmail(EMAIL_ADDRESS, USER_NAME, "Site Crawler Status",
				" Total URLs spidered:" + MAX_PAGES_TO_FECTH + "- total new URLs added to external pages results list:" + TOTAL);

	}

	@Test
	public void backGroundTaskExecutor_WhenValidationPasses_ThenConfiguresAndStartsTheCrawlController() throws Exception {
		mockControllerConfigs();

		when(mockCrawlModelBuilder.getCrawlController(mockCrawlConfig, mockPageFetcher, mockRobotstxtServer)).thenReturn(mockCrawlController);
		when(CrawlUtil.generateCustomData(XPATH, INDEX_ARRAY, COMPANY_ID, SCOPED_GROUP_ID, USER_ID)).thenReturn(CUSTOM_DATA);

		externalPageImportBackgroundTaskExecutor.execute(mockBackgroundTask);

		InOrder inOrder = Mockito.inOrder(mockCrawlController);
		inOrder.verify(mockCrawlController, times(1)).setCustomData(CUSTOM_DATA);
		inOrder.verify(mockCrawlController, times(1)).addSeed(URL);
		inOrder.verify(mockCrawlController, times(1)).start(ExternalPageCrawler.class, 7);

	}

	private void mockControllerConfigs() {

		when(mockBackgroundTask.getTaskContextMap()).thenReturn(taskContextMap);
		when(GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.URL))).thenReturn(URL);

		when(GetterUtil.getInteger(taskContextMap.get(BackgroundTaskKeys.MAX_PAGES_TO_FETCH))).thenReturn(MAX_PAGES_TO_FECTH);
		when(GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.X_PATH))).thenReturn(XPATH);
		when(GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.INDEX_ARRAY))).thenReturn(INDEX_ARRAY);

		when(GetterUtil.getLong(taskContextMap.get(BackgroundTaskKeys.COMPANY_ID))).thenReturn(COMPANY_ID);
		when(GetterUtil.getLong(taskContextMap.get(BackgroundTaskKeys.USER_ID))).thenReturn(USER_ID);
		when(GetterUtil.getLong(taskContextMap.get(BackgroundTaskKeys.SCOPE_GROUP_ID))).thenReturn(SCOPED_GROUP_ID);

		when(GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.USER_EMAIL_ADDRESS))).thenReturn(EMAIL_ADDRESS);
		when(GetterUtil.getString(taskContextMap.get(BackgroundTaskKeys.USER_NAME))).thenReturn(USER_NAME);

		when(GetterUtil.getInteger(taskContextMap.get(BackgroundTaskKeys.TOTAL_URL_COUNT))).thenReturn(TOTAL_URL_COUNT);

		when(mockCrawlModelBuilder.getCrawlConfig()).thenReturn(mockCrawlConfig);
		when(mockCrawlModelBuilder.getPageFetcher(mockCrawlConfig)).thenReturn(mockPageFetcher);
		when(mockCrawlModelBuilder.getRobotstxtConfig()).thenReturn(mockRobotstxtConfig);
		when(mockCrawlModelBuilder.getRobotstxtServer(mockRobotstxtConfig, mockPageFetcher)).thenReturn(mockRobotstxtServer);
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, CrawlUtil.class, SessionErrors.class);
	}

}
