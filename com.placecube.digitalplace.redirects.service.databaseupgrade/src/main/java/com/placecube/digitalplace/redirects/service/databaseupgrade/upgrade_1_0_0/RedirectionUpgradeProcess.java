package com.placecube.digitalplace.redirects.service.databaseupgrade.upgrade_1_0_0;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.upgrade.UpgradeException;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.LoggingTimer;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.redirect.exception.DuplicateRedirectEntrySourceURLException;
import com.liferay.redirect.service.RedirectEntryLocalService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.Date;
import java.util.List;

public class RedirectionUpgradeProcess extends UpgradeProcess {

	private static final Log LOG = LogFactoryUtil.getLog(RedirectionUpgradeProcess.class);

	private static final String SELECT_REDIRECTS_QUERY = "SELECT * FROM Redirect WHERE companyId = COMPANY_ID_PLACEHOLDER;";

	private final RedirectEntryLocalService redirectEntryLocalService;

	private final CompanyLocalService companyLocalService;

	private final GroupLocalService groupLocalService;

	public RedirectionUpgradeProcess(RedirectEntryLocalService redirectEntryLocalService, CompanyLocalService companyLocalService, GroupLocalService groupLocalService) {
		this.redirectEntryLocalService = redirectEntryLocalService;
		this.companyLocalService = companyLocalService;
		this.groupLocalService = groupLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		if (hasTable("Redirect")) {
			long companyId = CompanyThreadLocal.getCompanyId();

			if (companyId > 0) {
				upgradeCompany(companyId);
			} else {
				List<Company> companies = companyLocalService.getCompanies();

				for (Company company : companies) {
					upgradeCompany(company.getCompanyId());
				}
			}
		}
	}

	private void upgradeCompany(long companyId) throws Exception {
		Company company = companyLocalService.getCompany(companyId);

		String queryRedirects = SELECT_REDIRECTS_QUERY.replace("COMPANY_ID_PLACEHOLDER", String.valueOf(companyId));

		try (LoggingTimer loggingTimer = new LoggingTimer();
				PreparedStatement preparedStatement = getConnection().prepareStatement(queryRedirects);
				ResultSet resultSet = preparedStatement.executeQuery()) {

			while (resultSet.next()) {
				processRedirectsForGroup(resultSet, company);

			}
		} catch (SQLSyntaxErrorException e) {
			LOG.info("Skipping upgrade. " + e.getMessage());
		}
	}

	private void processRedirectsForGroup(ResultSet resultSet, Company company) throws SQLException, PortalException {

		long groupId = resultSet.getLong("groupId");

		Group group = groupLocalService.fetchGroup(groupId);

		if (group != null) {

			String destinationURL = sanitizeUrlToFullyFormed(company, group, resultSet.getString("toURL"));
			Date expirationDate = null;
			boolean permanent = resultSet.getBoolean("permanent");
			String sourceURL = sanitizeUrlRemoveLeadingSlashes(resultSet.getString("fromURL"));
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(groupId);
			serviceContext.setCompanyId(company.getCompanyId());
			try {
				redirectEntryLocalService.addRedirectEntry(groupId, destinationURL, expirationDate, permanent, sourceURL, serviceContext);
			} catch (DuplicateRedirectEntrySourceURLException ignored) {
			}
		}

	}

	private String sanitizeUrlRemoveLeadingSlashes(String sourceUrl) {
		if (sourceUrl.startsWith("/")) {
			return sourceUrl.replaceFirst("^//*", "");
		}
		return sourceUrl;
	}

	private String sanitizeUrlToFullyFormed(Company company, Group group, final String toUrl) throws PortalException {
		if (!Validator.isUrl(toUrl)) {
			String fullyFormedUrl = "https://" + company.getVirtualHostname() + toUrl;
			if (!Validator.isUrl(fullyFormedUrl)) {
				throw new UpgradeException("FAILED - Attempt to sanatize URL to fully formed: " + group.getGroupId());
			}
			return fullyFormedUrl;
		}

		return toUrl;
	}

}
