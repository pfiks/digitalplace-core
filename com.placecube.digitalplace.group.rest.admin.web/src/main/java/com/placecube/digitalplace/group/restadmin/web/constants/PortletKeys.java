package com.placecube.digitalplace.group.restadmin.web.constants;

public final class PortletKeys {

	public static final String GROUP_REST_ADMIN = "com_placecube_digitalplace_group_restadmin_web_GroupRestAdminPortlet";

	public static final String GROUP_WEBHOOKS = "com_placecube_digitalplace_group_restadmin_web_GroupWebhooksPortlet";

	private PortletKeys() {
	}

}
