<%@ include file="../init.jsp"%>

<c:choose>
	
	<c:when test="${invalidConfiguration}">
		<%@ include file="/common/invalid-configuration.jspf" %>
	</c:when>
	
	<c:otherwise>
	
		<c:choose>
			<c:when test="${empty destinationSearchBarPortletId}">
				<c:set var="destinationSearchBarPortletNamespace">
					<portlet:namespace/>
				</c:set>
			</c:when>
			<c:otherwise>
				<c:set var="destinationSearchBarPortletNamespace" value="_${destinationSearchBarPortletId}_"/>
			</c:otherwise>
		</c:choose>
		
		<c:if test="${empty destinationSearchBarPortletId}">
			<c:set var="destinationSearchBarPortletId"><portlet:namespace/></c:set>
		</c:if>
		
		<portlet:actionURL name="<%=MVCCommandKeys.SEARCH %>" var="clearResultsURL">
			<portlet:param name="<%= SearchParameters.SEARCH_KEYWORD %>" value=""/>
			<portlet:param name="redirectURL" value="${redirectURL}" />
		</portlet:actionURL>


		<portlet:actionURL name="<%=MVCCommandKeys.SEARCH %>" var="searchActionURL">
			<portlet:param name="redirectURL" value="${redirectURL}" />
		</portlet:actionURL>

		<c:if test="${ not empty destinationPlid }">
			<liferay-portlet:actionURL name="<%=MVCCommandKeys.SEARCH %>" plid="${destinationPlid}" portletName="${destinationSearchBarPortletId}" var="searchActionURL" >
			</liferay-portlet:actionURL>
		</c:if>

		<c:choose>
			<c:when test="${not empty keywords}">
				<clay:management-toolbar
					clearResultsURL="${clearResultsURL}"
					itemsTotal="${totalResults}"
					namespace="${destinationSearchBarPortletNamespace}"
					searchActionURL="${searchActionURL}"
					searchFormName="fm_searchBarForm_${destinationSearchBarPortletNamespace}"
					searchInputName="<%= SearchParameters.SEARCH_KEYWORD %>"
					searchValue="${keywords}"
					selectable="false"
					showSearch="true"
				/>
			</c:when>
			<c:otherwise>
				<clay:management-toolbar
					clearResultsURL="${clearResultsURL}"
					itemsTotal="${totalResults}"
					namespace="${destinationSearchBarPortletNamespace}"
					searchActionURL="${searchActionURL}"
					searchFormName="fm_searchBarForm_${destinationSearchBarPortletNamespace}"
					searchInputName="<%= SearchParameters.SEARCH_KEYWORD %>"
					selectable="false"
					showSearch="true"
				/>
			</c:otherwise>
		</c:choose>

	</c:otherwise>
</c:choose>