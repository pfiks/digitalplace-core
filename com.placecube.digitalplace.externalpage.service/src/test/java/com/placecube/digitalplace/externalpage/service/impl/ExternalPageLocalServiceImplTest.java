package com.placecube.digitalplace.externalpage.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.service.persistence.ExternalPagePersistence;

public class ExternalPageLocalServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 20;
	private static final String CONTENT = "contentValue";
	private static final long GROUP_ID = 10;
	private static final long NEXT_ID = 50;
	private static final long PAGE_ID = 40;
	private static final boolean STATUS = false;
	private static final String TITLE = "titleValue";
	private static final String URL = "urlValue";
	private static final long USER_ID = 30;
	private static final String UUID = "uuidValue";

	@InjectMocks
	private ExternalPageLocalServiceImpl externalPageLocalServiceImpl;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private Date mockCreateDate;

	@Mock
	private ExternalPage mockExternalPage;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	@Mock
	private ExternalPagePersistence mockExternalPagePersistence;

	@Mock
	private ExternalPage mockExternalPageUpdated;

	@Mock
	private Date mockModifiedDate;

	@Test
	public void addOrUpdateExternalPage_WhenPageAlreadyExist_ThenReturnsTheUpdatedExistingPage() throws PortalException {
		when(mockExternalPagePersistence.fetchByGroupIdUrl_First(GROUP_ID, URL, null)).thenReturn(mockExternalPage);
		when(mockExternalPageLocalService.updateExternalPage(mockExternalPage)).thenReturn(mockExternalPageUpdated);

		ExternalPage result = externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, URL, TITLE, CONTENT, STATUS);

		assertThat(result, sameInstance(mockExternalPageUpdated));
	}

	@Test
	public void addOrUpdateExternalPage_WhenPageAlreadyExistAndTitleIsLongerThan100Chars_ThenUpdatesThePageAndItsAssetEntryWithAtruncatedTitle() throws PortalException {
		String longTitle = StringUtil.randomString(101);
		String expectedTitle = StringUtil.shorten(longTitle, 100);
		when(mockExternalPagePersistence.fetchByGroupIdUrl_First(GROUP_ID, URL, null)).thenReturn(mockExternalPage);
		when(mockExternalPageLocalService.updateExternalPage(mockExternalPage)).thenReturn(mockExternalPageUpdated);
		when(mockExternalPageUpdated.getCreateDate()).thenReturn(mockCreateDate);
		when(mockExternalPageUpdated.getModifiedDate()).thenReturn(mockModifiedDate);
		when(mockExternalPageUpdated.getExternalPageId()).thenReturn(PAGE_ID);
		when(mockExternalPageUpdated.getUuid()).thenReturn(UUID);

		externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, URL, longTitle, CONTENT, STATUS);

		InOrder inOrder = Mockito.inOrder(mockExternalPage, mockExternalPageLocalService, mockAssetEntryLocalService);
		inOrder.verify(mockExternalPage, times(1)).setUserId(USER_ID);
		inOrder.verify(mockExternalPage, times(1)).setTitle(expectedTitle);
		inOrder.verify(mockExternalPage, times(1)).setUrl(URL);
		inOrder.verify(mockExternalPage, times(1)).setContent(CONTENT);
		inOrder.verify(mockExternalPageLocalService, times(1)).updateExternalPage(mockExternalPage);
		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(USER_ID, GROUP_ID, mockCreateDate, mockModifiedDate, ExternalPage.class.getName(), PAGE_ID, UUID, 0l, new long[0],
				new String[0], true, true, null, null, null, null, ContentTypes.TEXT_HTML, expectedTitle, CONTENT, expectedTitle, URL, null, 0, 0, 0.0);
	}

	@Test
	public void addOrUpdateExternalPage_WhenPageAlreadyExistAndTitleIsNotLongerThan100Chars_ThenUpdatesThePageAndItsAssetEntry() throws PortalException {
		when(mockExternalPagePersistence.fetchByGroupIdUrl_First(GROUP_ID, URL, null)).thenReturn(mockExternalPage);
		when(mockExternalPageLocalService.updateExternalPage(mockExternalPage)).thenReturn(mockExternalPageUpdated);
		when(mockExternalPageUpdated.getCreateDate()).thenReturn(mockCreateDate);
		when(mockExternalPageUpdated.getModifiedDate()).thenReturn(mockModifiedDate);
		when(mockExternalPageUpdated.getExternalPageId()).thenReturn(PAGE_ID);
		when(mockExternalPageUpdated.getUuid()).thenReturn(UUID);

		externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, URL, TITLE, CONTENT, STATUS);

		InOrder inOrder = Mockito.inOrder(mockExternalPage, mockExternalPageLocalService, mockAssetEntryLocalService);
		inOrder.verify(mockExternalPage, times(1)).setUserId(USER_ID);
		inOrder.verify(mockExternalPage, times(1)).setTitle(TITLE);
		inOrder.verify(mockExternalPage, times(1)).setUrl(URL);
		inOrder.verify(mockExternalPage, times(1)).setContent(CONTENT);
		inOrder.verify(mockExternalPageLocalService, times(1)).updateExternalPage(mockExternalPage);
		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(USER_ID, GROUP_ID, mockCreateDate, mockModifiedDate, ExternalPage.class.getName(), PAGE_ID, UUID, 0l, new long[0],
				new String[0], true, true, null, null, null, null, ContentTypes.TEXT_HTML, TITLE, CONTENT, TITLE, URL, null, 0, 0, 0.0);
	}

	@Test
	public void addOrUpdateExternalPage_WhenPageDoesNotExist_ThenReturnsTheUpdatedNewPage() throws PortalException {
		when(mockExternalPagePersistence.fetchByGroupIdUrl_First(GROUP_ID, URL, null)).thenReturn(null);
		when(mockCounterLocalService.increment(ExternalPage.class.getName(), 1)).thenReturn(NEXT_ID);
		when(mockExternalPageLocalService.createExternalPage(NEXT_ID)).thenReturn(mockExternalPage);
		when(mockExternalPageLocalService.updateExternalPage(mockExternalPage)).thenReturn(mockExternalPageUpdated);

		ExternalPage result = externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, URL, TITLE, CONTENT, STATUS);

		assertThat(result, sameInstance(mockExternalPageUpdated));
	}

	@Test
	public void addOrUpdateExternalPage_WhenPageDoesNotExistAndTitleIsLessThan100Chars_ThenUpdatesTheNewPageAndItsAssetEntry() throws PortalException {
		when(mockExternalPagePersistence.fetchByGroupIdUrl_First(GROUP_ID, URL, null)).thenReturn(null);
		when(mockCounterLocalService.increment(ExternalPage.class.getName(), 1)).thenReturn(NEXT_ID);
		when(mockExternalPageLocalService.createExternalPage(NEXT_ID)).thenReturn(mockExternalPage);
		when(mockExternalPageLocalService.updateExternalPage(mockExternalPage)).thenReturn(mockExternalPageUpdated);
		when(mockExternalPageUpdated.getCreateDate()).thenReturn(mockCreateDate);
		when(mockExternalPageUpdated.getModifiedDate()).thenReturn(mockModifiedDate);
		when(mockExternalPageUpdated.getExternalPageId()).thenReturn(PAGE_ID);
		when(mockExternalPageUpdated.getUuid()).thenReturn(UUID);

		externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, URL, TITLE, CONTENT, STATUS);

		InOrder inOrder = Mockito.inOrder(mockExternalPage, mockExternalPageLocalService, mockAssetEntryLocalService);
		inOrder.verify(mockExternalPage, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockExternalPage, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockExternalPage, times(1)).setUserId(USER_ID);
		inOrder.verify(mockExternalPage, times(1)).setTitle(TITLE);
		inOrder.verify(mockExternalPage, times(1)).setUrl(URL);
		inOrder.verify(mockExternalPage, times(1)).setContent(CONTENT);
		inOrder.verify(mockExternalPageLocalService, times(1)).updateExternalPage(mockExternalPage);
		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(USER_ID, GROUP_ID, mockCreateDate, mockModifiedDate, ExternalPage.class.getName(), PAGE_ID, UUID, 0l, new long[0],
				new String[0], true, true, null, null, null, null, ContentTypes.TEXT_HTML, TITLE, CONTENT, TITLE, URL, null, 0, 0, 0.0);
	}

	@Test
	public void addOrUpdateExternalPage_WhenPageDoesNotExistAndTitleIsMoreThan100Chars_ThenUpdatesTheNewPageAndItsAssetEntryWithAtruncatedTitle() throws PortalException {
		String longTitle = StringUtil.randomString(101);
		String expectedTitle = StringUtil.shorten(longTitle, 100);
		when(mockExternalPagePersistence.fetchByGroupIdUrl_First(GROUP_ID, URL, null)).thenReturn(null);
		when(mockCounterLocalService.increment(ExternalPage.class.getName(), 1)).thenReturn(NEXT_ID);
		when(mockExternalPageLocalService.createExternalPage(NEXT_ID)).thenReturn(mockExternalPage);
		when(mockExternalPageLocalService.updateExternalPage(mockExternalPage)).thenReturn(mockExternalPageUpdated);
		when(mockExternalPageUpdated.getCreateDate()).thenReturn(mockCreateDate);
		when(mockExternalPageUpdated.getModifiedDate()).thenReturn(mockModifiedDate);
		when(mockExternalPageUpdated.getExternalPageId()).thenReturn(PAGE_ID);
		when(mockExternalPageUpdated.getUuid()).thenReturn(UUID);

		externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, URL, longTitle, CONTENT, STATUS);

		InOrder inOrder = Mockito.inOrder(mockExternalPage, mockExternalPageLocalService, mockAssetEntryLocalService);
		inOrder.verify(mockExternalPage, times(1)).setCompanyId(COMPANY_ID);
		inOrder.verify(mockExternalPage, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockExternalPage, times(1)).setUserId(USER_ID);
		inOrder.verify(mockExternalPage, times(1)).setTitle(expectedTitle);
		inOrder.verify(mockExternalPage, times(1)).setUrl(URL);
		inOrder.verify(mockExternalPage, times(1)).setContent(CONTENT);
		inOrder.verify(mockExternalPageLocalService, times(1)).updateExternalPage(mockExternalPage);
		inOrder.verify(mockAssetEntryLocalService, times(1)).updateEntry(USER_ID, GROUP_ID, mockCreateDate, mockModifiedDate, ExternalPage.class.getName(), PAGE_ID, UUID, 0l, new long[0],
				new String[0], true, true, null, null, null, null, ContentTypes.TEXT_HTML, expectedTitle, CONTENT, expectedTitle, URL, null, 0, 0, 0.0);
	}

	@Test(expected = PortalException.class)
	public void addOrUpdateExternalPage_WhenTitleIsEmpty_ThenThrowsPortalException() throws PortalException {
		externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, URL, StringPool.THREE_SPACES, CONTENT, STATUS);
	}

	@Test(expected = PortalException.class)
	public void addOrUpdateExternalPage_WhenTitleIsNull_ThenThrowsPortalException() throws PortalException {
		externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, URL, null, CONTENT, STATUS);
	}

	@Test(expected = PortalException.class)
	public void addOrUpdateExternalPage_WhenURLIsEmpty_ThenThrowsPortalException() throws PortalException {
		externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, StringPool.THREE_SPACES, TITLE, CONTENT, STATUS);
	}

	@Test(expected = PortalException.class)
	public void addOrUpdateExternalPage_WhenURLIsLongerThan200Chars_ThenThrowsPortalException() throws PortalException {
		String longURL = StringUtil.randomString(201);
		externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, longURL, TITLE, CONTENT, STATUS);
	}

	@Test(expected = PortalException.class)
	public void addOrUpdateExternalPage_WhenUrlIsNull_ThenThrowsPortalException() throws PortalException {
		externalPageLocalServiceImpl.addOrUpdateExternalPage(COMPANY_ID, GROUP_ID, USER_ID, null, TITLE, CONTENT, STATUS);
	}

	@Test(expected = PortalException.class)
	public void deleteExternalPageByPrimaryKey_WhenError_ThenThrowsPortalException() throws PortalException {
		long externalPageId = 123L;
		when(mockExternalPageLocalService.deleteExternalPage(externalPageId)).thenThrow(PortalException.class);

		externalPageLocalServiceImpl.deleteExternalPageByPrimaryKey(externalPageId);
	}

	@Test
	public void deleteExternalPageByPrimaryKey_WhenNoError_ThenDeleteExternalPageEntryAndAssetEntry() throws PortalException {
		long externalPageId = 123L;
		when(mockExternalPageLocalService.deleteExternalPage(externalPageId)).thenReturn(mockExternalPage);

		externalPageLocalServiceImpl.deleteExternalPageByPrimaryKey(externalPageId);

		verify(mockExternalPageLocalService, times(1)).deleteExternalPage(externalPageId);
		verify(mockAssetEntryLocalService, times(1)).deleteEntry(ExternalPage.class.getName(), externalPageId);
	}

	@Test
	public void removeExternalPagesByGroupId_WhenGroupIdIsValid_ThenRemoveExternalPagesbyGroupId() {
		externalPageLocalServiceImpl.removeExternalPagesByGroupId(GROUP_ID);
		Mockito.verify(mockExternalPagePersistence, times(1)).removeByGroupId(GROUP_ID);
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

}
