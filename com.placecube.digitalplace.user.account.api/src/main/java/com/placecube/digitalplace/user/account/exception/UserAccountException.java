package com.placecube.digitalplace.user.account.exception;

public class UserAccountException extends Exception{

	public UserAccountException() {super();}

	public UserAccountException(Throwable cause) {
		super(cause);
	}

}
