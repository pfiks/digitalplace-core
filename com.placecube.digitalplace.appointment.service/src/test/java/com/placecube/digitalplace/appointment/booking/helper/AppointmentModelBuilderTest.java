package com.placecube.digitalplace.appointment.booking.helper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.internal.utils.AppointmentUtils;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentModelBuilderTest {

	@InjectMocks
	private AppointmentModelBuilder appointmentModelBuilder;

	@Mock
	private AppointmentEntry mockAppointmentEntry;

	@Mock
	private AppointmentUtils mockAppointmentUtils;

	@Mock
	private Date mockDate1;

	@Mock
	private Date mockDate2;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TimeZone mockTimeZone;

	@Test
	public void getAppointmentBookingEntry_WhenNoError_ThenReturnsTheAppointmentBookingEntry() {
		long classNameId = 1;
		long classPK = 2;
		long appointmentEntryId = 4;
		String appointmentId = "myAppointmentId";
		when(mockAppointmentEntry.getEntryClassNameId()).thenReturn(classNameId);
		when(mockAppointmentEntry.getEntryClassPK()).thenReturn(classPK);
		when(mockAppointmentEntry.getAppointmentEndDate()).thenReturn(mockDate1);
		when(mockAppointmentEntry.getAppointmentStartDate()).thenReturn(mockDate2);
		when(mockAppointmentEntry.getAppointmentEntryId()).thenReturn(appointmentEntryId);
		when(mockAppointmentEntry.getAppointmentId()).thenReturn(appointmentId);

		AppointmentBookingEntry result = appointmentModelBuilder.getAppointmentBookingEntry(mockAppointmentEntry);

		assertThat(result.getAppointmentEntryId(), equalTo(appointmentEntryId));
		assertThat(result.getAppointmentId(), equalTo(appointmentId));
		assertThat(result.getClassNameId(), equalTo(classNameId));
		assertThat(result.getClassPK(), equalTo(classPK));
		assertThat(result.getDescription(), equalTo(null));
		assertThat(result.getEndDate(), equalTo(mockDate1));
		assertThat(result.getStartDate(), equalTo(mockDate2));
		assertThat(result.getTitle(), equalTo(null));
		assertThat(result.getTimeZone(), equalTo(null));
	}

	@Test
	public void getAppointmentBookingEntryFromPayload_WhenNoBodyRequest_ThenReturnsEmptyEntry() throws PortalException, ParseException {
		AppointmentBookingEntry result = appointmentModelBuilder.getAppointmentBookingEntryFromPayload(mockHttpServletRequest, null);

		assertThat(result.getClassNameId(), equalTo(0l));
		assertThat(result.getClassPK(), equalTo(0l));
		assertThat(result.getAppointmentEntryId(), equalTo(0l));
		assertThat(result.getAppointmentId(), equalTo(null));
		assertThat(result.getDescription(), equalTo(null));
		assertThat(result.getTitle(), equalTo(null));
		assertThat(result.getTimeZone(), equalTo(null));
		assertThat(result.getEndDate(), equalTo(null));
		assertThat(result.getStartDate(), equalTo(null));
	}

	@Test
	public void getAppointmentBookingEntryFromPayload_WhenValidBodyRequest_ThenReturnsEntryWithValuesFromJsonPayload() throws PortalException, ParseException {
		Locale locale = Locale.ENGLISH;
		when(mockJSONFactory.createJSONObject("myJsonPayload")).thenReturn(mockJSONObject);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAppointmentUtils.getLocale(mockThemeDisplay)).thenReturn(locale);
		when(mockAppointmentUtils.getTimeZone(mockThemeDisplay)).thenReturn(mockTimeZone);
		when(mockJSONObject.getString("datePattern")).thenReturn("myDatePattern");
		when(mockJSONObject.getLong("classNameId", 0l)).thenReturn(11l);
		when(mockJSONObject.getLong("classPK", 0l)).thenReturn(12l);
		when(mockJSONObject.getString("startDate", "")).thenReturn("myStartDateValue");
		when(mockJSONObject.getString("endDate", "")).thenReturn("myEndDateValue");
		when(mockJSONObject.getString("title", "")).thenReturn("myTitleValue");
		when(mockAppointmentUtils.getParsedDate("myStartDateValue", locale, "myDatePattern")).thenReturn(Optional.of(mockDate1));
		when(mockAppointmentUtils.getParsedDate("myEndDateValue", locale, "myDatePattern")).thenReturn(Optional.of(mockDate2));

		AppointmentBookingEntry result = appointmentModelBuilder.getAppointmentBookingEntryFromPayload(mockHttpServletRequest, "myJsonPayload");

		assertThat(result.getClassNameId(), equalTo(11l));
		assertThat(result.getClassPK(), equalTo(12l));
		assertThat(result.getAppointmentEntryId(), equalTo(0l));
		assertThat(result.getAppointmentId(), equalTo(null));
		assertThat(result.getDescription(), equalTo(null));
		assertThat(result.getTitle(), equalTo("myTitleValue"));
		assertThat(result.getTimeZone(), equalTo(mockTimeZone));
		assertThat(result.getEndDate(), equalTo(mockDate2));
		assertThat(result.getStartDate(), equalTo(mockDate1));
	}

	@Test
	public void getAppointmentContextFromPayload_WhenNoBodyRequest_ThenReturnsEmptyContext() throws JSONException, ParseException {
		AppointmentContext result = appointmentModelBuilder.getAppointmentContextFromPayload(mockHttpServletRequest, null);

		assertThat(result.getClassNameId(), equalTo(0l));
		assertThat(result.getClassPK(), equalTo(0l));
		assertFalse(result.getEndDate().isPresent());
		assertFalse(result.getStartDate().isPresent());
	}

	@Test
	public void getAppointmentContextFromPayload_WhenValidBodyRequest_ThenReturnsContextWithValuesFromJsonPayload() throws JSONException, ParseException {
		Locale locale = Locale.ENGLISH;
		when(mockJSONFactory.createJSONObject("myJsonPayload")).thenReturn(mockJSONObject);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAppointmentUtils.getLocale(mockThemeDisplay)).thenReturn(locale);
		when(mockJSONObject.getString("datePattern")).thenReturn("myDatePattern");
		when(mockJSONObject.getLong("classNameId", 0l)).thenReturn(11l);
		when(mockJSONObject.getLong("classPK", 0l)).thenReturn(12l);
		when(mockJSONObject.getString("startDate", "")).thenReturn("myStartDateValue");
		when(mockJSONObject.getString("endDate", "")).thenReturn("myEndDateValue");
		when(mockAppointmentUtils.getParsedDate("myStartDateValue", locale, "myDatePattern")).thenReturn(Optional.of(mockDate1));
		when(mockAppointmentUtils.getParsedDate("myEndDateValue", locale, "myDatePattern")).thenReturn(Optional.of(mockDate2));

		AppointmentContext result = appointmentModelBuilder.getAppointmentContextFromPayload(mockHttpServletRequest, "myJsonPayload");

		assertThat(result.getClassNameId(), equalTo(11l));
		assertThat(result.getClassPK(), equalTo(12l));
		assertThat(result.getStartDate().get(), sameInstance(mockDate1));
		assertThat(result.getEndDate().get(), sameInstance(mockDate2));
	}

}
