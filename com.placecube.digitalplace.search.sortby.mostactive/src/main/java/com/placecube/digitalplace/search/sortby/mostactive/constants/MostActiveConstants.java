package com.placecube.digitalplace.search.sortby.mostactive.constants;

public final class MostActiveConstants {

	public static final String COUNTER_FIELD_NAME = "dp_mostactive_sortable";

	public static final int DAYS_DELTA = 30;

	public static final String LAST_LOGIN_DATE_FIELD_NAME = "lastLoginDate";

	private MostActiveConstants() {
	}

}
