package com.placecube.digitalplace.search.web.portlet.sortby.constants;

public class SortByDirectionConstants {

	public static final String DIRECTION_DEFAULT = "defaultDirection";
	public static final String DIRECTION_INVERTED = "invertedDirection";

	private SortByDirectionConstants() {
	}
}