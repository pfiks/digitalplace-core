package com.placecube.digitalplace.group.rest.application;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.pfiks.common.time.DateFormatter;
import com.placecube.digitalplace.group.rest.application.endpoints.GroupContentRestEndpoint;
import com.placecube.digitalplace.group.rest.application.exceptions.handlers.GeneralExceptionHandler;
import com.placecube.digitalplace.group.rest.application.exceptions.handlers.GroupRestSecurityExceptionHandler;
import com.placecube.digitalplace.group.rest.application.filters.DateCheckerFilter;
import com.placecube.digitalplace.group.rest.application.filters.HeaderCheckerFilter;
import com.placecube.digitalplace.group.rest.application.filters.SignatureCheckerFilter;
import com.placecube.digitalplace.group.rest.application.model.ModelBuilder;
import com.placecube.digitalplace.group.rest.application.model.RestContentEntryBuilder;
import com.placecube.digitalplace.group.rest.application.service.GroupContentService;
import com.placecube.digitalplace.group.rest.application.service.RequestFiltersService;
import com.placecube.digitalplace.group.rest.application.service.SignatureBuilderService;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;

@ApplicationPath("/groups")
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)
public class GroupRestApplication extends Application {

	@Reference
	private DateFormatter dateFormatter;

	@Reference
	private GroupApiKeyLocalService groupApiKeyLocalService;

	@Reference
	private GroupContentService groupContentService;

	@Reference
	private ModelBuilder modelBuilder;

	@Reference
	private RequestFiltersService requestFiltersService;

	@Reference
	private RestContentEntryBuilder restContentEntryBuilder;

	@Reference
	private SignatureBuilderService signatureBuilderService;

	public DateFormatter getDateFormatter() {
		return dateFormatter;
	}

	public GroupApiKeyLocalService getGroupApiKeyLocalService() {
		return groupApiKeyLocalService;
	}

	public GroupContentService getGroupContentService() {
		return groupContentService;
	}

	public ModelBuilder getModelBuilder() {
		return modelBuilder;
	}

	public RequestFiltersService getRequestFiltersService() {
		return requestFiltersService;
	}

	public RestContentEntryBuilder getRestContentEntryBuilder() {
		return restContentEntryBuilder;
	}

	public SignatureBuilderService getSignatureBuilderService() {
		return signatureBuilderService;
	}

	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new LinkedHashSet<>();

		// Filters
		singletons.add(new HeaderCheckerFilter(this));
		singletons.add(new DateCheckerFilter(this));
		singletons.add(new SignatureCheckerFilter(this));

		// Jackson marshaller for JSON
		singletons.add(new JacksonJsonProvider());

		// Exception handlers
		singletons.add(new GroupRestSecurityExceptionHandler());
		singletons.add(new GeneralExceptionHandler(this));

		// REST endpoints
		singletons.add(new GroupContentRestEndpoint(this));

		return singletons;
	}

}
