package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=/",
		"mvc.command.name=" + MVCCommandKeys.PERSONAL_DETAILS }, service = MVCRenderCommand.class)
public class PersonalDetailsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(renderRequest);

			if (Validator.isNull(accountContext.getProfileRedirectUrl())) {
				accountContext.setProfileRedirectUrl(ParamUtil.getString(renderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK));
			}

			if (Validator.isNull(accountContext.getAccountConnectorModelId())) {
				accountContext.setAccountConnectorModelId(ParamUtil.getString(renderRequest, AccountConnectorConstant.MODEL_ID, StringPool.BLANK));
			}

			UserProfileFieldsConfiguration userFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());

			CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, renderRequest);

			String refererGroupIdUserExpandoFieldName = accountConfiguration.registrationRefererGroupIdUserExpandoFieldName();
			if (Validator.isNotNull(refererGroupIdUserExpandoFieldName)) {
				HttpServletRequest httpServletRequest = portal.getLiferayPortletRequest(renderRequest).getOriginalHttpServletRequest();
				long refererGroupId = ParamUtil.getLong(httpServletRequest, RequestKeys.REFERER);
				if (Validator.isNotNull(refererGroupId)) {
					accountContext.addExpandoField(refererGroupIdUserExpandoFieldName, String.valueOf(refererGroupId));
				}
			}

			boolean showEmailAddress = AccountConfigurationConstants.PAGE_PERSONAL_DETAILS.equals(accountConfiguration.emailAddressPageLocation());

			renderRequest.setAttribute(RequestKeys.SHOW_EMAIL_ADDRESS_IN_PERSONAL_DETAILS, showEmailAddress);
			renderRequest.setAttribute(RequestKeys.ACCOUNT_CONTEXT, accountContext);
			renderRequest.setAttribute(RequestKeys.USER_PROFILE_CONFIG, userFieldsConfiguration);
			renderRequest.setAttribute(RequestKeys.ACCOUNT_CONFIG, accountConfiguration);

			if (showEmailAddress) {
				renderRequest.setAttribute(RequestKeys.CAPTCHA_ENABLED, accountContextService.isShowCaptchaOnEmailStep(accountContext, renderRequest));
			}
			accountContextService.updateContextInSession(accountContext, renderRequest);
		} catch (UserAccountException e) {
			throw new PortletException(e);
		}

		return "/personal-details.jsp";
	}

}
