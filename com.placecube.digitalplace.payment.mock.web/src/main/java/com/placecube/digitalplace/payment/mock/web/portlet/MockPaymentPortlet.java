package com.placecube.digitalplace.payment.mock.web.portlet;

import java.util.Calendar;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.payment.mock.web.constants.ParamKeys;
import com.placecube.digitalplace.payment.mock.web.constants.PortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.display-category=category.hidden", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.restore-current-view=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/view.jsp", //
		"javax.portlet.name=" + PortletKeys.MOCK_PAYMENT_PORTLET, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"portlet.add.default.resource.check.whitelist=" + PortletKeys.MOCK_PAYMENT_PORTLET }, service = Portlet.class)
public class MockPaymentPortlet extends MVCPortlet {

	@Reference
	private Portal portal;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {

			renderRequest.setAttribute("startYear", Calendar.getInstance().get(Calendar.YEAR));
			renderRequest.setAttribute(ParamKeys.AMOUNT, ParamUtil.getString(renderRequest, ParamKeys.AMOUNT));
			renderRequest.setAttribute(ParamKeys.RETURN_URL, ParamUtil.getString(renderRequest, ParamKeys.RETURN_URL));

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}

	}

}
