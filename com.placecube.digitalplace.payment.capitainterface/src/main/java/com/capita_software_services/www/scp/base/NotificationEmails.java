/**
 * NotificationEmails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class NotificationEmails  implements java.io.Serializable {
    private java.lang.String[] email;

    private java.lang.String additionalEmailMessage;

    public NotificationEmails() {
    }

    public NotificationEmails(
           java.lang.String[] email,
           java.lang.String additionalEmailMessage) {
           this.email = email;
           this.additionalEmailMessage = additionalEmailMessage;
    }


    /**
     * Gets the email value for this NotificationEmails.
     *
     * @return email
     */
    public java.lang.String[] getEmail() {
        return email;
    }


    /**
     * Sets the email value for this NotificationEmails.
     *
     * @param email
     */
    public void setEmail(java.lang.String[] email) {
        this.email = email;
    }

    public java.lang.String getEmail(int i) {
        return this.email[i];
    }

    public void setEmail(int i, java.lang.String _value) {
        this.email[i] = _value;
    }


    /**
     * Gets the additionalEmailMessage value for this NotificationEmails.
     *
     * @return additionalEmailMessage
     */
    public java.lang.String getAdditionalEmailMessage() {
        return additionalEmailMessage;
    }


    /**
     * Sets the additionalEmailMessage value for this NotificationEmails.
     *
     * @param additionalEmailMessage
     */
    public void setAdditionalEmailMessage(java.lang.String additionalEmailMessage) {
        this.additionalEmailMessage = additionalEmailMessage;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NotificationEmails)) return false;
        NotificationEmails other = (NotificationEmails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.email==null && other.getEmail()==null) ||
             (this.email!=null &&
              java.util.Arrays.equals(this.email, other.getEmail()))) &&
            ((this.additionalEmailMessage==null && other.getAdditionalEmailMessage()==null) ||
             (this.additionalEmailMessage!=null &&
              this.additionalEmailMessage.equals(other.getAdditionalEmailMessage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEmail() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEmail());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEmail(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAdditionalEmailMessage() != null) {
            _hashCode += getAdditionalEmailMessage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NotificationEmails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "notificationEmails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/portal-api", "email"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalEmailMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "additionalEmailMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
