package com.placecube.digitalplace.appointment.internal.upgrade.steps;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class Upgrade_1_1_0_AppointmentEntryTableUpdates extends UpgradeProcess {

	public Upgrade_1_1_0_AppointmentEntryTableUpdates() {
	}

	@Override
	protected void doUpgrade() throws Exception {
		if (hasTable("Placecube_Appointment_AppointmentEntry")) {

			addMissingColumns();

			createNewIndexes();

			dropLegacyIndexImmediately();
		}
	}

	private void addMissingColumns() throws Exception {
		alterTableAddColumn("Placecube_Appointment_AppointmentEntry", "serviceId", "STRING");
		alterTableAddColumn("Placecube_Appointment_AppointmentEntry", "connectorClassName", "STRING");
		alterTableAddColumn("Placecube_Appointment_AppointmentEntry", "appointmentStartDate", "DATE");
		alterTableAddColumn("Placecube_Appointment_AppointmentEntry", "appointmentEndDate", "DATE");
	}

	private void createNewIndexes() throws Exception {
		if (!hasIndex("Placecube_Appointment_AppointmentEntry", "IX_2DA5010C")) {
			runSQL("create index IX_2DA5010C on Placecube_Appointment_AppointmentEntry (companyId, serviceId[$COLUMN_LENGTH:4000$], connectorClassName[$COLUMN_LENGTH:4000$], appointmentId[$COLUMN_LENGTH:2000000$]);");
		}
		if (!hasIndex("Placecube_Appointment_AppointmentEntry", "IX_EFC3D581")) {
			runSQL("create index IX_EFC3D581 on Placecube_Appointment_AppointmentEntry (companyId, entryClassPK, entryClassNameId);");
		}
	}

	private void dropLegacyIndexImmediately() throws Exception {
		if (hasIndex("Placecube_Appointment_AppointmentEntry", "IX_DD6FD4D1")) {
			runSQL(connection, "drop index IX_DD6FD4D1 on Placecube_Appointment_AppointmentEntry;");
		}
	}

}
