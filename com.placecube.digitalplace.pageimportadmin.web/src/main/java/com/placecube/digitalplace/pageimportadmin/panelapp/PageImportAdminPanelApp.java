package com.placecube.digitalplace.pageimportadmin.panelapp;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.pageimportadmin.constants.PortletKeys;

@Component(immediate = true, property = { "panel.app.order:Integer=500", "panel.category.key=" + PanelCategoryKeys.SITE_ADMINISTRATION_BUILD }, service = PanelApp.class)
public class PageImportAdminPanelApp extends BasePanelApp {

	@Reference(target = "(javax.portlet.name=" + PortletKeys.PAGE_IMPORT_ADMIN + ")", unbind = "-")
	private Portlet portlet;

	@Override
	public Portlet getPortlet() {
		return portlet;
	}

	@Override
	public String getPortletId() {
		return PortletKeys.PAGE_IMPORT_ADMIN;
	}

	@Override
	public boolean isShow(PermissionChecker permissionChecker, Group group) throws PortalException {

		if (isGlobalGroup(group)) {
			return false;
		}

		return super.isShow(permissionChecker, group);
	}

	private boolean isGlobalGroup(Group group) {
		return group.isCompany();
	}
}