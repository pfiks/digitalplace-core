package com.placecube.digitalplace.siteinitializeradmin.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Locale;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.site.initializer.SiteInitializer;
import com.liferay.site.initializer.SiteInitializerRegistry;
import com.placecube.digitalplace.siteinitializeradmin.constants.PortletRequestKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet")
public class SiteInitializerAdminPortletTest {

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SiteInitializer mockSiteInitializer1;

	@Mock
	private SiteInitializer mockSiteInitializer2;

	@Mock
	private SiteInitializer mockSiteInitializer3;

	@Mock
	private SiteInitializerRegistry mockSiteInitializerRegistry;

	@InjectMocks
	private SiteInitializerAdminPortlet siteInitializerAdminPortlet;

	@Before
	public void activateSetUp() throws Exception {
		mockCallToSuper();
	}

	@Test
	public void render_WhenNoError_ThenSetsActiveSiteInitializersAsRequestAttributeSortedByName() throws Exception {
		Long companyId = 11l;
		Locale locale = Locale.ENGLISH;
		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(companyId);
		when(mockSiteInitializerRegistry.getSiteInitializers(companyId, true)).thenReturn(Arrays.asList(mockSiteInitializer1, mockSiteInitializer2, mockSiteInitializer3));
		when(mockPortal.getLocale(mockRenderRequest)).thenReturn(locale);
		when(mockSiteInitializer1.getName(locale)).thenReturn("Beta");
		when(mockSiteInitializer2.getName(locale)).thenReturn("Alpha");
		when(mockSiteInitializer3.getName(locale)).thenReturn("Charlie");

		siteInitializerAdminPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.SITE_INITIALIZERS, Arrays.asList(mockSiteInitializer2, mockSiteInitializer1, mockSiteInitializer3));
	}

	@SuppressWarnings("rawtypes")
	private void mockCallToSuper() throws Exception {
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

}
