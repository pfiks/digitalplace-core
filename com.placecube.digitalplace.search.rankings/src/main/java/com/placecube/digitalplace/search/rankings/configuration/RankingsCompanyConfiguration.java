package com.placecube.digitalplace.search.rankings.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "search", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.search.rankings.configuration.RankingsCompanyConfiguration", localization = "content/Language", name = "digitalplace-search-rankings")
public interface RankingsCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enable-wildcard-rankings", description = "wildcard-search-rankings-description")
	boolean enableWildcardRankings();

}
