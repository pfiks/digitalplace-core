package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class KeywordsFilterUtilTest {

	@Test
	public void removeStopWords() {
		List<String> expected = new ArrayList<>(Collections.singleton("test"));
		List<String> testStrings = new ArrayList<>(Arrays.asList("a", "a", "an", "and", "are", "as", "at", "be", "but", "by", "for", "if", "in", "into", "is", "it", "no", "not", "of", "on", "or", "such", "that",
				"the", "their", "then", "there", "these", "they", "this", "to", "was", "will", "with"));

		testStrings.addAll(expected);

		List<String> result = KeywordsFilterUtil.removeStopWords(testStrings.toArray(new String[0]));

		assertEquals(expected, result);
	}
}