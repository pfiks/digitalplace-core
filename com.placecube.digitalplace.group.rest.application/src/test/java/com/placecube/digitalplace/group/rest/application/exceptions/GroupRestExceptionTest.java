package com.placecube.digitalplace.group.rest.application.exceptions;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.SystemException;

public class GroupRestExceptionTest extends PowerMockito {

	@Test
	public final void newGroupRestException_WhenNoError_ThenIsCreatedWithTheGivenThrowable() {
		Exception exception = new SystemException();

		GroupRestException result = new GroupRestException(exception);

		assertThat(result.getCause(), sameInstance(exception));
	}

}
