package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListTemplateContentService;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.model.DDLDisplayEntry;

@Component(service = DDLRelatedSearchUtil.class, immediate = true)
public class DDLRelatedSearchUtil {

	private static final Log LOG = LogFactoryUtil.getLog(DDLRelatedSearchUtil.class);

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private DynamicDataListTemplateContentService dynamicDataListTemplateContentService;

	public Optional<String> getDDLRecordTemplateString(long ddlRecordId, List<DynamicDataListMapping> dynamicDataListMappings, ThemeDisplay themeDisplay, HttpServletRequest httpServletRequest) {
		try {
			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(ddlRecordId);
			DDMStructure ddmStructure = ddlRecord.getRecordSet().getDDMStructure();

			Optional<DynamicDataListMapping> ddlMappingOptional = dynamicDataListMappings.stream().filter(mapping -> mapping.getDDMStructureId() == ddmStructure.getStructureId()).findFirst();
			Optional<DDMTemplate> ddmTemplateOptional = ddlMappingOptional.flatMap(DynamicDataListMapping::getSearchDefaultGridViewTemplate);

			if (!ddmTemplateOptional.isPresent())
				return Optional.empty();

			return Optional.of(dynamicDataListTemplateContentService.getTransformedTemplate(ddlRecordId, ddmStructure, ddmTemplateOptional.get(), themeDisplay, httpServletRequest));

		} catch (PortalException e) {
			LOG.warn(e.getMessage());
			LOG.debug(e);
			return Optional.empty();
		}
	}

	public DDLDisplayEntry createDDLDisplayEntry(long ddlRecordId, String templateString) {
		return new DDLDisplayEntry(ddlRecordId, templateString);
	}

	public String getDDLRecordTitle(DDLRecord ddlRecord) {
		List<DDMFormFieldValue> ddmTitleCandidateFieldValues = new ArrayList<>();

		ddmTitleCandidateFieldValues.addAll(getDDMFormFieldValues(ddlRecord, "Title"));
		ddmTitleCandidateFieldValues.addAll(getDDMFormFieldValues(ddlRecord, "title"));

		return ddmTitleCandidateFieldValues.stream().findFirst().map(DDMFormFieldValue::getValue).map(value -> value.getString(value.getDefaultLocale())).orElse("");
	}

	private List<DDMFormFieldValue> getDDMFormFieldValues(DDLRecord ddlRecord, String fieldValue) {
		try {
			List<DDMFormFieldValue> ddmFormFieldValues = ddlRecord.getDDMFormFieldValues(fieldValue);
			return Validator.isNotNull(ddmFormFieldValues) ? ddmFormFieldValues : Collections.emptyList();
		} catch (PortalException e) {
			return Collections.emptyList();
		}
	}
}
