package com.placecube.digitalplace.ddl.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.ddl.internal.util.TemplateService;
import com.placecube.digitalplace.ddl.internal.util.TemplateUtil;
import com.placecube.digitalplace.ddl.internal.util.TransformContextService;

public class DynamicDataListTemplateContentServiceImplTest extends PowerMockito {

	private static final long COMPANY_GROUP_ID = 2L;
	private static final long COMPANY_ID = 1L;
	private static final long DDM_STRUCTURE_ID = 789L;
	private static final long EXPECTED_DDL_TEMPLATE_ID = 456L;
	private static final String LANGUAGE = "Language";
	private static final long SCOPE_GROUP_ID = 3L;
	private static final String SCRIPT = "Script";

	@InjectMocks
	private DynamicDataListTemplateContentServiceImpl dynamicDataListTemplateContentServiceImpl;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private Template mockTemplate;

	@Mock
	private TemplateService mockTemplateService;

	@Mock
	private TemplateUtil mockTemplateUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TransformContextService mockTransformContextService;

	@Mock
	private UnsyncStringWriter mockUnsyncStringWriter;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getTransformedTemplate_WithAssetEntryListParameter_WhenNoError_ThenTransformedStringReturned() throws Exception {
		List<AssetEntry> assetEntries = new ArrayList<>();
		assetEntries.add(mockAssetEntry);

		List<DDMTemplate> structureTemplates = Arrays.asList(mockDDMTemplate);
		Map<String, Object> mockContextObjects = Collections.emptyMap();
		String success = "SUCCESS";

		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getTemplates()).thenReturn(structureTemplates);
		mockDDMTemplateValues();
		when(mockTransformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, DDM_STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest)).thenReturn(mockContextObjects);
		mockThemeDisplayValues();
		when(mockTemplateUtil.getUnsyncStringWriterInstance()).thenReturn(mockUnsyncStringWriter);
		when(mockTemplateService.getTemplate(EXPECTED_DDL_TEMPLATE_ID, COMPANY_ID, COMPANY_GROUP_ID, SCOPE_GROUP_ID, SCRIPT, LANGUAGE)).thenReturn(mockTemplate);
		when(mockUnsyncStringWriter.toString()).thenReturn(success);

		String result = dynamicDataListTemplateContentServiceImpl.getTranformedTemplate(assetEntries, mockDDMStructure, mockDDMTemplate, mockThemeDisplay, mockRenderRequest);

		assertEquals(success, result);
	}

	@Test(expected = PortalException.class)
	public void getTransformedTemplate_WithDDLRecordParameter_WhenGettingDDMStructureThrowsPortalException_ThenThrowPortalException() throws Exception {
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenThrow(new PortalException());

		dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(mockDDLRecord, EXPECTED_DDL_TEMPLATE_ID, mockThemeDisplay, mockHttpServletRequest);

	}

	@Test(expected = PortalException.class)
	public void getTransformedTemplate_WithDDLRecordParameterAndStructureAndTemplateParameter_WhenGettingTemplateThrowsTemplateException_ThenThrowPortalException() throws Exception {
		Map<String, Object> mockContextObjects = Collections.emptyMap();
		when(mockDDMStructure.getStructureId()).thenReturn(DDM_STRUCTURE_ID);
		mockDDMTemplateValues();
		when(mockTransformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, DDM_STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest)).thenReturn(mockContextObjects);
		mockThemeDisplayValues();
		when(mockTemplateUtil.getUnsyncStringWriterInstance()).thenReturn(mockUnsyncStringWriter);
		when(mockTemplateService.getTemplate(EXPECTED_DDL_TEMPLATE_ID, COMPANY_ID, COMPANY_GROUP_ID, SCOPE_GROUP_ID, SCRIPT, LANGUAGE)).thenThrow(new TemplateException());

		dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(mockDDLRecord, mockDDMStructure, mockDDMTemplate, mockThemeDisplay, mockHttpServletRequest);
	}

	@Test
	public void getTransformedTemplate_WithDDLRecordParameterAndStructureAndTemplateParameter_WhenValidDDMTemplateIdPassed_ThenTransformedStringReturned() throws Exception {
		Map<String, Object> mockContextObjects = Collections.emptyMap();
		String success = "SUCCESS";
		when(mockDDMStructure.getStructureId()).thenReturn(DDM_STRUCTURE_ID);
		mockDDMTemplateValues();
		when(mockTransformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, DDM_STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest)).thenReturn(mockContextObjects);
		mockThemeDisplayValues();
		when(mockTemplateUtil.getUnsyncStringWriterInstance()).thenReturn(mockUnsyncStringWriter);
		when(mockTemplateService.getTemplate(EXPECTED_DDL_TEMPLATE_ID, COMPANY_ID, COMPANY_GROUP_ID, SCOPE_GROUP_ID, SCRIPT, LANGUAGE)).thenReturn(mockTemplate);
		when(mockUnsyncStringWriter.toString()).thenReturn(success);

		String result = dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(mockDDLRecord, mockDDMStructure, mockDDMTemplate, mockThemeDisplay, mockHttpServletRequest);

		verify(mockTemplateService, times(1)).prepareTemplate(mockThemeDisplay, mockTemplate);
		verify(mockTemplateService, times(1)).initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);
		assertEquals(success, result);
	}

	@Test
	public void getTransformedTemplate_WithDDLRecordParameterAndTemplateIdParameter_WhenDDMStructureHasNoAssociatedTemplates_ThenReturnBlankString() throws Exception {
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getTemplates()).thenReturn(Collections.emptyList());

		String result = dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(mockDDLRecord, EXPECTED_DDL_TEMPLATE_ID, mockThemeDisplay, mockHttpServletRequest);

		assertEquals(result, StringPool.BLANK);
	}

	@Test
	public void getTransformedTemplate_WithDDLRecordParameterAndTemplateIdParameter_WhenDDMStructureHasNoDisplayTemplates_ThenReturnBlankString() throws Exception {
		List<DDMTemplate> structureTemplates = Arrays.asList(mockDDMTemplate);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getTemplates()).thenReturn(structureTemplates);
		when(mockDDMTemplate.getType()).thenReturn(DDMTemplateConstants.TEMPLATE_TYPE_FORM);

		String result = dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(mockDDLRecord, EXPECTED_DDL_TEMPLATE_ID, mockThemeDisplay, mockHttpServletRequest);

		assertEquals(result, StringPool.BLANK);
		verify(mockDDMTemplate, times(1)).getType();
		verify(mockDDMTemplate, never()).getTemplateId();

	}

	@Test
	public void getTransformedTemplate_WithDDLRecordParameterAndTemplateIdParameter_WhenExpectedTemplateIdDoesNotMatchAnyInListOfStructureDisplayTemplates_ThenReturnBlankString() throws Exception {
		List<DDMTemplate> structureTemplates = Arrays.asList(mockDDMTemplate);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getTemplates()).thenReturn(structureTemplates);
		when(mockDDMTemplate.getType()).thenReturn(DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY);
		when(mockDDMTemplate.getTemplateId()).thenReturn(1l);

		String result = dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(mockDDLRecord, EXPECTED_DDL_TEMPLATE_ID, mockThemeDisplay, mockHttpServletRequest);

		assertEquals(result, StringPool.BLANK);
		verify(mockDDMTemplate, times(1)).getType();
		verify(mockDDMTemplate, times(1)).getTemplateId();

	}

	@Test(expected = PortalException.class)
	public void getTransformedTemplate_WithDDLRecordParameterAndTemplateIdParameter_WhenGettingDDLRecordSetThrowsPortalException_ThenThrowPortalException() throws Exception {
		when(mockDDLRecord.getRecordSet()).thenThrow(new PortalException());

		dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(mockDDLRecord, EXPECTED_DDL_TEMPLATE_ID, mockThemeDisplay, mockHttpServletRequest);
	}

	@Test(expected = PortalException.class)
	public void getTransformedTemplate_WithDDLRecordParameterAndTemplateIdParameter_WhenGettingTemplateThrowsTemplateException_ThenThrowPortalException() throws Exception {
		List<DDMTemplate> structureTemplates = Arrays.asList(mockDDMTemplate);
		Map<String, Object> mockContextObjects = Collections.emptyMap();

		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getTemplates()).thenReturn(structureTemplates);
		mockDDMTemplateValues();
		when(mockTransformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, DDM_STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest)).thenReturn(mockContextObjects);
		mockThemeDisplayValues();
		when(mockTemplateUtil.getUnsyncStringWriterInstance()).thenReturn(mockUnsyncStringWriter);
		when(mockTemplateService.getTemplate(EXPECTED_DDL_TEMPLATE_ID, COMPANY_ID, COMPANY_GROUP_ID, SCOPE_GROUP_ID, SCRIPT, LANGUAGE)).thenThrow(new TemplateException());

		dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(mockDDLRecord, EXPECTED_DDL_TEMPLATE_ID, mockThemeDisplay, mockHttpServletRequest);
	}

	@Test
	public void getTransformedTemplate_WithDDLRecordParameterAndTemplateIdParameter_WhenValidDDMTemplateIdPassed_ThenTransformedStringReturned() throws Exception {

		List<DDMTemplate> structureTemplates = Arrays.asList(mockDDMTemplate);
		Map<String, Object> mockContextObjects = Collections.emptyMap();
		String success = "SUCCESS";

		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockDDMStructureLocalService.getDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getTemplates()).thenReturn(structureTemplates);
		mockDDMTemplateValues();
		when(mockTransformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, DDM_STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest)).thenReturn(mockContextObjects);
		mockThemeDisplayValues();
		when(mockTemplateUtil.getUnsyncStringWriterInstance()).thenReturn(mockUnsyncStringWriter);
		when(mockTemplateService.getTemplate(EXPECTED_DDL_TEMPLATE_ID, COMPANY_ID, COMPANY_GROUP_ID, SCOPE_GROUP_ID, SCRIPT, LANGUAGE)).thenReturn(mockTemplate);
		when(mockUnsyncStringWriter.toString()).thenReturn(success);

		String result = dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(mockDDLRecord, EXPECTED_DDL_TEMPLATE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockTemplateService, times(1)).prepareTemplate(mockThemeDisplay, mockTemplate);
		verify(mockTemplateService, times(1)).initTemplate(mockTemplate, mockContextObjects, mockThemeDisplay, mockUnsyncStringWriter);
		assertEquals(success, result);
	}

	@Test(expected = PortalException.class)
	public void getTransformedTemplate_WithRecordIdParameter_WhenErrorGettingRecord_ThenPortalExceptionIsThrown() throws Exception {
		long ddlRecordId = 1l;

		when(mockDDLRecordLocalService.getDDLRecord(ddlRecordId)).thenThrow(new PortalException());

		dynamicDataListTemplateContentServiceImpl.getTransformedTemplate(ddlRecordId, mockDDMStructure, mockDDMTemplate, mockThemeDisplay, mockHttpServletRequest);
	}

	private void mockDDMTemplateValues() {
		when(mockDDMTemplate.getType()).thenReturn(DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY);
		when(mockDDMTemplate.getTemplateId()).thenReturn(EXPECTED_DDL_TEMPLATE_ID);
		when(mockDDMTemplate.getScript()).thenReturn(SCRIPT);
		when(mockDDMTemplate.getLanguage()).thenReturn(LANGUAGE);
	}

	private void mockThemeDisplayValues() {
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getCompanyGroupId()).thenReturn(COMPANY_GROUP_ID);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
	}

}
