package com.placecube.digitalplace.payment.heycentric.service.constants;

public class HeycentricPaymentConstants {

	public static final String BUSINESS_PHONE_TYPE = "business";

	public static final String HASH_PARAMETER = "hash";

	public static final String HASH_PARAMETER_VALUE = "NoHash";

	public static final String HEADER_PARAMETER_AREA = "area";

	public static final String HEADER_PARAMETER_BURL = "burl";

	public static final String HEADER_PARAMETER_CCEMAIL = "ccemail";

	public static final String HEADER_PARAMETER_CLIENT = "client";

	public static final String HEADER_PARAMETER_CMT = "cmt";

	public static final String HEADER_PARAMETER_EMAIL = "email";

	public static final String HEADER_PARAMETER_ENTITY = "entity";

	public static final String HEADER_PARAMETER_EXTREF = "extref";

	public static final String HEADER_PARAMETER_RURL = "rurl";

	public static final String HEADER_PARAMETER_SID = "sid";

	public static final String HEADER_PARAMETER_TILL = "till";

	public static final String JSON_PAYLOAD_ACN = "acn";

	public static final String JSON_PAYLOAD_AGENT_EMAIL = "agentEmail";

	public static final String JSON_PAYLOAD_AGENT_ID = "agentId";

	public static final String JSON_PAYLOAD_BU = "bu";

	public static final String JSON_PAYLOAD_CALLER_EMAIL = "callerEmail";

	public static final String JSON_PAYLOAD_CCEMAIL = "ccEmail";

	public static final String JSON_PAYLOAD_DIGEST = "digest";

	public static final String JSON_PAYLOAD_EMAIL = "Email";

	public static final String JSON_PAYLOAD_ICB = "icb";

	public static final String JSON_PAYLOAD_INMEDIATE_CALLBACK = "immediateCallBack";

	public static final String JSON_PAYLOAD_LINE_ITEMS = "lineitems";

	public static final String JSON_PAYLOAD_LINE_ITEM_AM = "Am";

	public static final String JSON_PAYLOAD_LINE_ITEM_PMTTYP = "PmtTyp";

	public static final String JSON_PAYLOAD_LINE_ITEM_VAL1 = "Val1";

	public static final String JSON_PAYLOAD_LINE_ITEM_VAL2 = "Val2";

	public static final String JSON_PAYLOAD_LOGURL = "logurl";

	public static final String JSON_PAYLOAD_PAYLOG = "payLog";

	public static final String JSON_PAYLOAD_PAYMENT_AMOUNT = "paymentAmount";

	public static final String JSON_PAYLOAD_PAYMENT_CALLER_PHONE_NUMBER = "callerPhoneNumber";

	public static final String JSON_PAYLOAD_PCN = "pcn";

	public static final String JSON_PAYLOAD_RECEIPT = "receipt";

	public static final String JSON_PAYLOAD_RESULT = "result";

	public static final String JSON_PAYLOAD_RETURN_URL = "returnUrl";

	public static final String JSON_PAYLOAD_RURL = "rurl";

	public static final String JSON_PAYLOAD_SID = "sid";

	public static final String JSON_RESPONSE_ENDPOINT = "endpoint";

	public static final String JSON_RESPONSE_REQUEST_PAYLOAD = "requestPayload";

	public static final String LINE_PARAMETER_AM = "am";

	public static final String LINE_PARAMETER_PMTTYP = "pmtTyp";

	public static final String LINE_PARAMETER_VAL1 = "val1";

	public static final String LINE_PARAMETER_VAL1DESC = "val1desc";

	public static final String LINE_PARAMETER_VAL2 = "val2";

	public static final String RESPONSE_PAYMENT_AUTH_CODE_PARAMETER = "AuthCode";

	public static final String RESPONSE_PAYMENT_STATUS_PARAMETER = "Rc";

	public static final String RESPONSE_REC_NO_PARAMETER = "RecNo";

	private HeycentricPaymentConstants() {
	}
}
