package com.placecube.digitalplace.search.web.portlet.category.constants;

public class CategoryFacetDisplayConstants {

	public static final String FACET_DISPLAY_STYLE_CHECKBOX = "checkbox";

	public static final String FACET_DISPLAY_STYLE_DROPDOWN = "dropdown";

	public static final String FACET_DISPLAY_STYLE_TREE = "tree";

	private CategoryFacetDisplayConstants() {

	}
}
