package com.placecube.digitalplace.oauth.dataprovider.service;

import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;

import java.util.Map;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = DataProviderPortalCacheService.class)
public class DataProviderPortalCacheService {

	public String getPortalCacheKey(String ddmDataProviderId, Map<String, String> allParametersMap, String url) {

		StringBundler sb = new StringBundler(4 * allParametersMap.size() + 4);

		sb.append(ddmDataProviderId);
		sb.append(StringPool.AT);
		sb.append(url);
		sb.append(StringPool.QUESTION);

		for (Map.Entry<String, String> entry : allParametersMap.entrySet()) {
			sb.append(entry.getKey());
			sb.append(StringPool.EQUAL);
			sb.append(entry.getValue());
			sb.append(StringPool.AMPERSAND);
		}

		if (!allParametersMap.isEmpty()) {
			sb.setIndex(sb.index() - 1);
		}

		return sb.toString();
	}

}
