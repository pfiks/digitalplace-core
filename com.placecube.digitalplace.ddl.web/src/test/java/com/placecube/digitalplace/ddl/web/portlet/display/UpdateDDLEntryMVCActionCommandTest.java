package com.placecube.digitalplace.ddl.web.portlet.display;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLEditorService;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class UpdateDDLEntryMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private DDLEditorService mockDDLEditorService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDMForm mockDDMForm;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DynamicDataListService mockDynamicDataListService;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UpdateDDLEntryMVCActionCommand updateDDLEntryMVCActionCommand;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenNoException_ThenCallsGetOrCreateDDLRecordAndSendsRedirectToFullViewPage() throws PortalException, IOException {
		long ddlRecordId = 1;
		long ddmStructureId = 2;
		long ddlRecordSetId = 3;
		long ddlTemplateId = 4;
		long ddmFormTemplateId = 5;
		String ddmFormValues = "formValues";
		String expectedRedirect = "redirect";

		when(ParamUtil.getLong(mockActionRequest, "ddlRecordId")).thenReturn(ddlRecordId);
		when(ParamUtil.getLong(mockActionRequest, "ddmStructureId")).thenReturn(ddmStructureId);
		when(ParamUtil.getLong(mockActionRequest, "ddlRecordSetId")).thenReturn(ddlRecordSetId);
		when(ParamUtil.getLong(mockActionRequest, "ddlTemplateId")).thenReturn(ddlTemplateId);

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);

		when(ParamUtil.getLong(mockActionRequest, "ddmFormTemplateId")).thenReturn(ddmFormTemplateId);
		when(ParamUtil.getString(mockActionRequest, "ddmFormValues")).thenReturn(ddmFormValues);
		when(mockDDLEditorService.getDDMFormForUpdate(ddmStructureId, ddmFormTemplateId)).thenReturn(mockDDMForm);
		when(mockDDLEditorService.getDDMFormValuesFromDeserialisedContent(ddmFormValues, mockDDMForm)).thenReturn(mockDDMFormValues);

		when(mockDDLEditorService.getServiceContextForUpdate(mockActionRequest, mockThemeDisplay)).thenReturn(mockServiceContext);
		when(mockDDLEditorService.getOrCreateDDLRecord(ddlRecordId, mockDDMFormValues, ddlRecordSetId, mockServiceContext, mockPermissionChecker)).thenReturn(mockDDLRecord);
		when(mockDynamicDataListService.getViewDDLRecordURL(mockThemeDisplay, mockDDLRecord, ddlTemplateId)).thenReturn(expectedRedirect);

		updateDDLEntryMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionResponse, times(1)).sendRedirect(expectedRedirect);
	}
}