package com.placecube.digitalplace.payment.capita.service;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.xml.rpc.ServiceException;

import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.payment.capita.configuration.CapitaCompanyConfiguration;
import com.placecube.digitalplace.payment.capita.service.internal.constants.CapitaPaymentConstanst;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;
import com.placecube.digitalplace.payment.capitainterface.service.PaymentService;

@Component(configurationPid = CapitaPaymentConstanst.PID, immediate = true, property = Constants.SERVICE_PID + "=" + CapitaPaymentConstanst.PID + ".scoped", service = { ManagedServiceFactory.class,
		CapitaPaymentService.class })
public class CapitaPaymentService implements ManagedServiceFactory {

	private static final Log LOG = LogFactoryUtil.getLog(CapitaPaymentService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	private final Map<Long, PaymentService> paymentServiceMap = new HashMap<>();

	@Override
	public void deleted(String pid) {
	}

	public Optional<String> getDefaultAdvancedConfiguration(long companyId) throws ConfigurationException {
		return Optional.ofNullable(getCapitaCompanyConfiguration(companyId).defaultAdvancedConfiguration());
	}

	@Override
	public String getName() {
		return CapitaPaymentConstanst.PID + ".scoped";
	}

	public PaymentService getPaymentService(ServiceContext serviceContext) {
		return paymentServiceMap.get(serviceContext.getCompanyId());
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		return getCapitaCompanyConfiguration(companyId).enabled();
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> dictionary) throws org.osgi.service.cm.ConfigurationException {
		try {
			long companyId = GetterUtil.getLong(dictionary.get("companyId"), CompanyConstants.SYSTEM);

			if (companyId != CompanyConstants.SYSTEM) {
				paymentServiceMap.put(companyId, initPaymentService(companyId, GetterUtil.getString(dictionary.get("capitaEndpointURL")), GetterUtil.getString(dictionary.get("capitaHMACKey")),
						GetterUtil.getString(dictionary.get("capitaHMACKeyId")), GetterUtil.getString(dictionary.get("capitaSCPId")), GetterUtil.getString(dictionary.get("capitaSiteId"))));
			}
		} catch (PortalException e) {
			LOG.error(e);
		}
	}

	private CapitaCompanyConfiguration getCapitaCompanyConfiguration(long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(CapitaCompanyConfiguration.class, companyId);
	}

	private PaymentService initPaymentService(long companyId, String capitaEndpointURL, String capitaHMACKey, String capitaHMACKeyId, String capitaSCPId, String capitaSiteId) throws PortalException {

		try {

			PaymentProperties properties = new PaymentProperties();

			properties.setEndpoint(capitaEndpointURL);
			properties.setHmacKey(capitaHMACKey);
			properties.setHmacKeyId(GetterUtil.getInteger(capitaHMACKeyId));
			properties.setScpId(GetterUtil.getInteger(capitaSCPId));
			properties.setSiteId(GetterUtil.getInteger(capitaSiteId));

			PaymentService paymentService = new PaymentService(properties);

			LOG.debug("PaymentService created for companyId: " + companyId + ", PaymentProperties: " + properties);

			return paymentService;

		} catch (ServiceException e) {
			throw new PortalException(e);
		}

	}
}
