package com.placecube.digitalplace.editor.configuration.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.editor.configuration.constants.CKEditorCommonToolbarConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Validator.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.util.Validator")

public class CKEditorCommonToolbarHelperTest extends PowerMockito {

	@InjectMocks
	private CKEditorCommonToolbarHelper cKEditorCommonToolbarHelper;

	@Mock
	private JSONArray mockBar;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockPluginAndRolesConfig;

	@Mock
	private JSONArray mockStyleJSONArray;

	@Mock
	private JSONObject mockStyleJSONObject;

	private int pluginsAndRolesLength = 1;
	private String userRoleName1 = "role1";
	private String userRoleName2 = "role2";
	private String pluginRoleNames = userRoleName1 + StringPool.COMMA + userRoleName2;

	@Before
	public void activeSetup() {
		mockStatic(Validator.class);
	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginAndRoleIsNotPresent_ThenCreateBarForPluginAndSkipCheckUserHasPermissionOnPlugin() {
		Optional<JSONObject> optPluginAndRolesConfig = Optional.empty();
		Set<String> userRoleNames = new HashSet<String>();
		userRoleNames.add(userRoleName1);
		userRoleNames.add(userRoleName2);
		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME, CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginAndRolesConfig, userRoleNames, pluginNames);

		verify(mockPluginAndRolesConfig, never()).getString(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME);
		verify(mockPluginAndRolesConfig, never()).getString(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);

		assertThat(result, equalTo(mockBar));
	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginAndRoleIsNull_ThenCreateBarForPluginAndSkipCheckUserHasPermissionOnPlugin() {
		Optional<JSONObject> optPluginAndRolesConfig = Optional.of(mockPluginAndRolesConfig);
		Set<String> userRoleNames = new HashSet<String>();
		userRoleNames.add(userRoleName1);
		userRoleNames.add(userRoleName2);
		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME, CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);
		when(mockPluginAndRolesConfig.length()).thenReturn(pluginsAndRolesLength);
		when(mockPluginAndRolesConfig.getString(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME)).thenReturn(null);
		when(mockPluginAndRolesConfig.getString(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME)).thenReturn(null);
		when(Validator.isNotNull(pluginRoleNames)).thenReturn(false);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginAndRolesConfig, userRoleNames, pluginNames);

		verify(mockPluginAndRolesConfig, times(1)).getString(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME);
		verify(mockPluginAndRolesConfig, times(1)).getString(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);

		assertThat(result, equalTo(mockBar));
	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginAndRoleIsPresentAndLengthIsZero_ThenCreateBarForPluginAndSkipCheckUserHasPermissionOnPlugin() {
		Optional<JSONObject> optPluginAndRolesConfig = Optional.of(mockPluginAndRolesConfig);
		Set<String> userRoleNames = new HashSet<String>();
		userRoleNames.add(userRoleName1);
		userRoleNames.add(userRoleName2);
		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME, CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);
		when(mockPluginAndRolesConfig.length()).thenReturn(0);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginAndRolesConfig, userRoleNames, pluginNames);

		verify(mockPluginAndRolesConfig, never()).getString(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME);
		verify(mockPluginAndRolesConfig, never()).getString(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);

		assertThat(result, equalTo(mockBar));
	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginAndRoleIsPresentAndUserDoesNotHavePermissionOnPlugin_ThenDoNotCheckUserHasPermissionOnPluginAndDoNotCreateBarForPlugin() {
		Optional<JSONObject> optPluginAndRolesConfig = Optional.of(mockPluginAndRolesConfig);
		Set<String> userRoleNames = new HashSet<String>();
		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME, CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);
		when(mockPluginAndRolesConfig.length()).thenReturn(pluginsAndRolesLength);
		when(mockPluginAndRolesConfig.getString(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME)).thenReturn(pluginRoleNames);
		when(mockPluginAndRolesConfig.getString(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME)).thenReturn(pluginRoleNames);
		when(Validator.isNotNull(pluginRoleNames)).thenReturn(true);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginAndRolesConfig, userRoleNames, pluginNames);

		verify(mockBar, never()).put(anyString());

		assertThat(result, equalTo(mockBar));

	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginAndRoleIsPresentAndUserHasPermissionOnPlugin_ThenCreateBarForPluginAndCheckUserHasPermissionOnPlugin() {
		Optional<JSONObject> optPluginAndRolesConfig = Optional.of(mockPluginAndRolesConfig);
		Set<String> userRoleNames = new HashSet<String>();
		userRoleNames.add(userRoleName1);
		userRoleNames.add(userRoleName2);
		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME, CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);
		when(mockPluginAndRolesConfig.length()).thenReturn(pluginsAndRolesLength);
		when(mockPluginAndRolesConfig.getString(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME)).thenReturn(pluginRoleNames);
		when(mockPluginAndRolesConfig.getString(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME)).thenReturn(pluginRoleNames);
		when(Validator.isNotNull(pluginRoleNames)).thenReturn(true);
		when(mockBar.put(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME)).thenReturn(mockBar);
		when(mockBar.put(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME)).thenReturn(mockBar);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginAndRolesConfig, userRoleNames, pluginNames);

		verify(mockPluginAndRolesConfig, times(1)).getString(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME);
		verify(mockPluginAndRolesConfig, times(1)).getString(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.UNDO_PLUGIN_NAME);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.REDO_PLUGIN_NAME);

		assertThat(result, equalTo(mockBar));
	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginNameIsStyleAndConfigIsPresent_ThenCreateBarForPluginAndRolesWithStylePlugin() {
		Optional<JSONObject> optPluginsAndRolesConfig = Optional.of(mockPluginAndRolesConfig);
		Set<String> userRoleNames = new HashSet<String>();
		userRoleNames.add(userRoleName1);
		userRoleNames.add(userRoleName2);
		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(Validator.isNotNull(mockStyleJSONArray)).thenReturn(true);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(mockStyleJSONArray.length()).thenReturn(pluginsAndRolesLength);
		when(mockStyleJSONArray.length()).thenReturn(pluginsAndRolesLength);
		when(mockStyleJSONArray.getJSONObject(anyInt())).thenReturn(mockStyleJSONObject);
		when(mockStyleJSONObject.getString(CKEditorCommonToolbarConstants.ROLES)).thenReturn(pluginRoleNames);
		when(Validator.isNotNull(pluginRoleNames)).thenReturn(true);
		when(mockBar.put(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockBar);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginsAndRolesConfig, userRoleNames, pluginNames);

		verify(mockStyleJSONObject, times(1)).getString(CKEditorCommonToolbarConstants.ROLES);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME);

		assertThat(result, equalTo(mockBar));
	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginNameIsStyleAndConfigIsPresentAndConfigLengthIsZero_ThenCreateBarForPluginAndRolesWithStylePlugin() {
		Optional<JSONObject> optPluginsAndRolesConfig = Optional.of(mockPluginAndRolesConfig);
		Set<String> userRoleNames = new HashSet<String>();
		userRoleNames.add(userRoleName1);
		userRoleNames.add(userRoleName2);
		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(Validator.isNotNull(mockStyleJSONArray)).thenReturn(true);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(mockStyleJSONArray.length()).thenReturn(0);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginsAndRolesConfig, userRoleNames, pluginNames);

		verify(mockStyleJSONObject, never()).getString(CKEditorCommonToolbarConstants.ROLES);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME);

		assertThat(result, equalTo(mockBar));
	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginNameIsStyleAndConfigNotPresent_ThenCreateBarForPluginAndRolesWithStylePlugin() {
		Optional<JSONObject> optPluginsAndRolesConfig = Optional.empty();
		Set<String> userRoleNames = new HashSet<String>();
		userRoleNames.add(userRoleName1);
		userRoleNames.add(userRoleName2);
		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(Validator.isNotNull(mockStyleJSONArray)).thenReturn(false);
		when(mockBar.put(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockBar);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginsAndRolesConfig, userRoleNames, pluginNames);

		verify(mockStyleJSONObject, never()).getString(CKEditorCommonToolbarConstants.ROLES);
		verify(mockBar, times(1)).put(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME);

		assertThat(result, equalTo(mockBar));
	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginNameIsStyleAndHasUserRollIsFalse_ThenCreateBarForPluginAndRolesWithoutStylePlugin() {
		Optional<JSONObject> optPluginsAndRolesConfig = Optional.of(mockPluginAndRolesConfig);
		Set<String> userRoleNames = new HashSet<String>();

		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(Validator.isNotNull(mockStyleJSONArray)).thenReturn(true);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(mockStyleJSONArray.length()).thenReturn(pluginsAndRolesLength);
		when(mockStyleJSONArray.length()).thenReturn(pluginsAndRolesLength);
		when(mockStyleJSONArray.getJSONObject(anyInt())).thenReturn(mockStyleJSONObject);
		when(mockStyleJSONObject.getString(CKEditorCommonToolbarConstants.ROLES)).thenReturn(pluginRoleNames);
		when(Validator.isNotNull(pluginRoleNames)).thenReturn(true);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginsAndRolesConfig, userRoleNames, pluginNames);

		verify(mockStyleJSONObject, times(1)).getString(CKEditorCommonToolbarConstants.ROLES);
		verify(mockBar, never()).put(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME);

		assertThat(result, equalTo(mockBar));
	}

	@Test
	public void createBarForPluginsAndRoles_WhenPluginNameIsStyleAndPluginRoleIsNull_ThenCreateBarForPluginAndRolesWithoutStylePlugin() {
		Optional<JSONObject> optPluginsAndRolesConfig = Optional.of(mockPluginAndRolesConfig);
		Set<String> userRoleNames = new HashSet<String>();
		userRoleNames.add(userRoleName1);
		userRoleNames.add(userRoleName2);
		String[] pluginNames = new String[] { CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME };

		when(mockJSONFactory.createJSONArray()).thenReturn(mockBar);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(Validator.isNotNull(mockStyleJSONArray)).thenReturn(true);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(mockStyleJSONArray.length()).thenReturn(pluginsAndRolesLength);
		when(mockStyleJSONArray.length()).thenReturn(pluginsAndRolesLength);
		when(mockStyleJSONArray.getJSONObject(anyInt())).thenReturn(mockStyleJSONObject);
		when(mockStyleJSONObject.getString(CKEditorCommonToolbarConstants.ROLES)).thenReturn(null);
		when(Validator.isNotNull(pluginRoleNames)).thenReturn(false);

		JSONArray result = cKEditorCommonToolbarHelper.createBarForPluginsAndRoles(optPluginsAndRolesConfig, userRoleNames, pluginNames);

		verify(mockStyleJSONObject, times(1)).getString(CKEditorCommonToolbarConstants.ROLES);
		verify(mockBar, never()).put(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME);

		assertThat(result, equalTo(mockBar));
	}

	@Test
	public void getStylePluginJsonArray_WhenDoesNotHavePluginsAndRolesConfig_ThenReturnOptionalJSONArray() {
		Optional<JSONObject> optPluginsAndRolesConfig = Optional.empty();

		Optional<JSONArray> result = cKEditorCommonToolbarHelper.getStylePluginJsonArray(optPluginsAndRolesConfig);

		assertFalse(result.isPresent());
	}

	@Test
	public void getStylePluginJsonArray_WhenHasPluginsAndRolesConfigWhenDoesNotHavePluginsAndRolesConfig_ThenReturnEmptyOptional() {
		Optional<JSONObject> optPluginsAndRolesConfig = Optional.of(mockPluginAndRolesConfig);

		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);
		when(Validator.isNotNull(mockStyleJSONArray)).thenReturn(true);
		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(mockStyleJSONArray);

		Optional<JSONArray> result = cKEditorCommonToolbarHelper.getStylePluginJsonArray(optPluginsAndRolesConfig);

		assertThat(result.get(), equalTo(mockStyleJSONArray));
	}

	@Test
	public void getStylePluginJsonArray_WhenStyleJSONArrayIsNull_ThenReturnOptionalJSONArray() {

		Optional<JSONObject> optPluginsAndRolesConfig = Optional.of(mockPluginAndRolesConfig);

		when(mockPluginAndRolesConfig.getJSONArray(CKEditorCommonToolbarConstants.STYLES_PLUGIN_NAME)).thenReturn(null);
		when(Validator.isNotNull(mockStyleJSONArray)).thenReturn(false);

		Optional<JSONArray> result = cKEditorCommonToolbarHelper.getStylePluginJsonArray(optPluginsAndRolesConfig);

		assertFalse(result.isPresent());
	}

	@Test
	public void hasUserRole_WhenUserDoesNotHavePluginRole_ThenReturnFalse() {
		Set<String> userRoleNames = new HashSet<String>();

		boolean result = cKEditorCommonToolbarHelper.hasUserRole(userRoleNames, pluginRoleNames);

		assertFalse(result);
	}

	@Test
	public void hasUserRole_WhenUserHasPluginRole_ThenReturnTrue() {
		Set<String> userRoleNames = new HashSet<String>();
		userRoleNames.add(userRoleName1);
		userRoleNames.add(userRoleName2);

		boolean result = cKEditorCommonToolbarHelper.hasUserRole(userRoleNames, pluginRoleNames);

		assertTrue(result);
	}
}
