package com.placecube.digitalplace.appointmentweb.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService;
import com.placecube.digitalplace.appointment.web.constants.CriterionConstants;

@Component(immediate = true, service = AppointmentsRetrievalService.class)
public class AppointmentsRetrievalService {

	@Reference
	private AppointmentEntryLocalService appointmentEntryLocalService;

	public List<AppointmentEntry> getAppointmentEntries(String serviceId, long companyId, long classNameId, Optional<Criterion> dateFilterCriterionOpt, Set<Long> restrictQueryToTheseIds, int start,
			int end) {
		Order orderByStartDate = OrderFactoryUtil.asc(CriterionConstants.START_DATE_FIELD);
		return appointmentEntryLocalService.dynamicQuery(getAppointmentsDynamicQuery(serviceId, companyId, classNameId, orderByStartDate, dateFilterCriterionOpt, restrictQueryToTheseIds), start, end);
	}

	public int getAppointmentEntriesCount(String serviceId, long companyId, long classNameId, Optional<Criterion> dateFilterCriterionOpt, Set<Long> restrictQueryToTheseIds) {
		return (int) appointmentEntryLocalService.dynamicQueryCount(getAppointmentsDynamicQuery(serviceId, companyId, classNameId, null, dateFilterCriterionOpt, restrictQueryToTheseIds));
	}

	public Optional<Criterion> getDateFilterCriterion(String filterType) {
		Criterion dateFilterCriterion = null;
		if (CriterionConstants.DATE_TODAY.equals(filterType)) {
			dateFilterCriterion = getTodaysAppointmentsCriterion();
		} else if (CriterionConstants.DATE_FUTURE.equals(filterType)) {
			dateFilterCriterion = getFutureAppointmentsCriterion();
		}

		return Optional.ofNullable(dateFilterCriterion);
	}

	public Optional<Criterion> getDateRangeCriterion(Calendar fromDate, Calendar toDate) {
		return Optional.of(RestrictionsFactoryUtil.between(CriterionConstants.START_DATE_FIELD, fromDate.getTime(), toDate.getTime()));
	}

	private DynamicQuery getAppointmentsDynamicQuery(String serviceId, long companyId, long classNameId, Order order, Optional<Criterion> dateFilterCriterionOpt, Set<Long> restrictQueryToTheseIds) {
		DynamicQuery appointmentDynamicQuery = appointmentEntryLocalService.dynamicQuery();
		if (!restrictQueryToTheseIds.isEmpty()) {
			appointmentDynamicQuery.add(RestrictionsFactoryUtil.in("entryClassPK", restrictQueryToTheseIds));
		}

		appointmentDynamicQuery.add(RestrictionsFactoryUtil.eq("entryClassNameId", classNameId));
		appointmentDynamicQuery.add(RestrictionsFactoryUtil.eq("serviceId", serviceId));
		appointmentDynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));

		if (Validator.isNotNull(order)) {
			appointmentDynamicQuery.addOrder(order);
		}

		dateFilterCriterionOpt.ifPresent(appointmentDynamicQuery::add);

		return appointmentDynamicQuery;
	}

	private Criterion getFutureAppointmentsCriterion() {
		Calendar today = CalendarFactoryUtil.getCalendar();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		Date startDate = today.getTime();

		return RestrictionsFactoryUtil.gt(CriterionConstants.START_DATE_FIELD, startDate);
	}

	private Criterion getTodaysAppointmentsCriterion() {
		Calendar today = CalendarFactoryUtil.getCalendar();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		Date startDate = today.getTime();

		today.set(Calendar.HOUR_OF_DAY, 23);
		today.set(Calendar.MINUTE, 59);
		today.set(Calendar.SECOND, 59);
		today.set(Calendar.MILLISECOND, 999);
		Date endDate = today.getTime();

		return RestrictionsFactoryUtil.between(CriterionConstants.START_DATE_FIELD, startDate, endDate);
	}
}
