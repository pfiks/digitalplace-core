package com.placecube.digitalplace.address.model;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.address.constants.AddressType;
import com.placecube.digitalplace.address.model.AddressContext.AddressContextBuilder;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class AddressContextTest {

	@Test
	public void getFullAddress_WhenNoErrors_ThenReturnsTheFullAddressDetails() {
		AddressContext addressContext = new AddressContext("uprnValue", "addressLine1Value", "addressLine2Value", "addressLine3Value", "addressLine4Value", "cityValue", "postcodeValue");

		String result = addressContext.getFullAddress();

		assertThat(result, equalTo("addressLine1Value addressLine2Value addressLine3Value addressLine4Value cityValue postcodeValue"));
	}

	@Test
	public void getFullAddress_WhenThereAreNullOrEmptyFields_ThenDoesNotIncludeNullAndEmptyFieldsInFullAddress() {
		AddressContext addressContext = new AddressContext("uprnValue", "addressLine1Value", "addressLine2Value", null, StringPool.BLANK, null, "postcodeValue");

		String result = addressContext.getFullAddress();

		assertThat(result, equalTo("addressLine1Value addressLine2Value postcodeValue"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void isCurrent_WhenNoErrors_ThenReturnsAddressCurrentValue(boolean current) {
		AddressContext addressContext = new AddressContext("uprnValue", "addressLine1Value", "addressLine2Value", "addressLine3Value", "addressLine4Value", "cityValue", "postcodeValue");
		addressContext.setCurrent(current);

		assertThat(addressContext.isCurrent(), equalTo(current));
	}

	@Test
	@Parameters({ "true", "false" })
	public void new_WhenBuiltWithBuilder_ThenSetsPropertiesWithConstructorParameters(boolean isCurrentAddress) {
		AddressContextBuilder builder = new AddressContextBuilder().setUprn("uprnValue").setAddressLine1("addressLine1Value").setAddressLine2("addressLine2Value").setAddressLine3("addressLine3Value")
				.setAddressLine4("addressLine4Value").setCity("cityValue").setPostcode("postcodeValue").setAddressType(AddressType.OTHER).setSource("sourceValue").setCurrent(isCurrentAddress);
		AddressContext addressContext = builder.build();

		assertThat(addressContext.getAddressLine1(), equalTo("addressLine1Value"));
		assertThat(addressContext.getAddressLine2(), equalTo("addressLine2Value"));
		assertThat(addressContext.getAddressLine3(), equalTo("addressLine3Value"));
		assertThat(addressContext.getAddressLine4(), equalTo("addressLine4Value"));
		assertThat(addressContext.getCity(), equalTo("cityValue"));
		assertThat(addressContext.getPostcode(), equalTo("postcodeValue"));
		assertThat(addressContext.getUPRN(), equalTo("uprnValue"));
		assertThat(addressContext.getAddressType(), equalTo(AddressType.OTHER));
		assertThat(addressContext.getSource(), equalTo("sourceValue"));
		assertThat(addressContext.isCurrent(), equalTo(isCurrentAddress));
	}

	@Test
	public void new_WhenBuiltWithConstructor_ThenSetsPropertiesWithConstructorParameters() {
		AddressContext addressContext = new AddressContext("uprnValue", "addressLine1Value", "addressLine2Value", "addressLine3Value", "addressLine4Value", "cityValue", "postcodeValue");

		assertThat(addressContext.getAddressLine1(), equalTo("addressLine1Value"));
		assertThat(addressContext.getAddressLine2(), equalTo("addressLine2Value"));
		assertThat(addressContext.getAddressLine3(), equalTo("addressLine3Value"));
		assertThat(addressContext.getAddressLine4(), equalTo("addressLine4Value"));
		assertThat(addressContext.getCity(), equalTo("cityValue"));
		assertThat(addressContext.getPostcode(), equalTo("postcodeValue"));
		assertThat(addressContext.getUPRN(), equalTo("uprnValue"));
		assertThat(addressContext.getAddressType(), equalTo(AddressType.PERSONAL));
		assertThat(addressContext.getSource(), equalTo(""));
		assertThat(addressContext.isCurrent(), equalTo(true));
	}

}
