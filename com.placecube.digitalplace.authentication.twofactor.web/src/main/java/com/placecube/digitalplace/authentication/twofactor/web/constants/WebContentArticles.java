package com.placecube.digitalplace.authentication.twofactor.web.constants;

public enum WebContentArticles {

	TWO_FACTOR_SETUP("TWO-FACTOR-SETUP", "Two factor setup page"),

	TWO_FACTOR_VERIFY_CODE("TWO-FACTOR-VERIFY-CODE", "Two factor verify code page"),

	TWO_FACTOR_USER_RESET("TWO-FACTOR-AUTHENTICATOR-CODE-RESET", "Two factor user reset code page");

	private final String articleId;
	private final String articleTitle;

	private WebContentArticles(String articleId, String articleTitle) {
		this.articleId = articleId;
		this.articleTitle = articleTitle;
	}

	public String getArticleId() {
		return articleId;
	}

	public String getArticleTitle() {
		return articleTitle;
	}

}
