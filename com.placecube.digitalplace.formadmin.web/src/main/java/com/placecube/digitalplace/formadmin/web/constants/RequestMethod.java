package com.placecube.digitalplace.formadmin.web.constants;

public enum RequestMethod {
	DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT, TRACE;
}