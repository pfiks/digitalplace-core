package com.placecube.digitalplace.search.sortby.mostliked.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.ratings.kernel.model.RatingsStats;
import com.liferay.ratings.kernel.service.RatingsStatsLocalService;
import com.placecube.digitalplace.search.sortby.mostliked.triggers.RatingsStatsListener;

@Component(immediate = true, service = MostLikedService.class)
public class MostLikedService {

	private static final Log LOG = LogFactoryUtil.getLog(RatingsStatsListener.class);

	@Reference
	private IndexerRegistry indexerRegistry;

	@Reference
	private RatingsStatsLocalService ratingsStatsLocalService;

	public double getScore(String className, long classPK) {
		RatingsStats ratingsStats = ratingsStatsLocalService.fetchStats(className, classPK);
		return Validator.isNotNull(ratingsStats) ? ratingsStats.getTotalScore() : 0.0;
	}

	public void reindexRatingScore(String className, long classPK) {
		try {
			LOG.debug("Updated most liked counter for className: " + className + ", classPK: " + classPK);
			Indexer<Object> indexer = indexerRegistry.nullSafeGetIndexer(className);
			indexer.reindex(className, classPK);
		} catch (SearchException e) {
			LOG.warn("Unable to reindex most liked counter " + e.getMessage());
			LOG.debug(e);
		}
	}

}