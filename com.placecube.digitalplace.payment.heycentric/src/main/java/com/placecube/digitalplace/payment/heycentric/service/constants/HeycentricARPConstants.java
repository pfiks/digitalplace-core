package com.placecube.digitalplace.payment.heycentric.service.constants;

public class HeycentricARPConstants {

	public static final String AUTHORISED = "Authorised";

	public static final String DECLINED = "Declined";

	public static final String INCOMPLETE = "Incomplete";

	private HeycentricARPConstants() {

	}
}
