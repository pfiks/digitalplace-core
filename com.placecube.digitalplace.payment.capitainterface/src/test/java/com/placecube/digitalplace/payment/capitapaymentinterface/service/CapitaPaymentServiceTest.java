package com.placecube.digitalplace.payment.capitapaymentinterface.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.capita_software_services.www.scp.base.BillingDetails;
import com.capita_software_services.www.scp.base.CardHolderDetails;
import com.capita_software_services.www.scp.base.Contact;
import com.capita_software_services.www.scp.base.ScpInvokeResponse;
import com.capita_software_services.www.scp.base.TransactionState;
import com.capita_software_services.www.scp.simple.ScpSimpleQueryResponse;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleConfiguration;
import com.placecube.digitalplace.payment.capitainterface.service.CapitaPaymentService;

@Ignore("Disabled due to tests not being compatible with builds on JDK11")
public class CapitaPaymentServiceTest extends PowerMockito {

	@InjectMocks
	private CapitaPaymentService capitaPaymentService;

	@Mock
	private SimpleSaleConfiguration mockSimpleSaleConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void setUp() throws Exception {
		initMocks(this);
	}

	@Test
	public void testCapitaInvoke() {

		OneOffPaymentOrder order = new OneOffPaymentOrder();

		// test a valid request
		order.setRequestType(OneOffPaymentOrder.CardRequestType.PAY_ONLY);

		BillingDetails billing = new BillingDetails();

		// Build the card holders details object
		CardHolderDetails cardHolderDetails = new CardHolderDetails();

		Contact contact = new Contact();
		contact.setEmail("christine123@mailiantor.com");

		cardHolderDetails.setContact(contact);

		billing.setCardHolderDetails(cardHolderDetails);

		order.setReturnURL("scp:close");
		order.setSaleDescription("Christine Test sale");
		order.setSaleAmount(10000);
		order.setSaleReferenceCode("ABC123");

		// RequestType requestType, String returnURL, String backURL, String
		// saleDescription, int saleAmount, List<Map<String,String>>
		// listOfItemDetails, BillingDetails billingDetails

		ScpInvokeResponse response = capitaPaymentService.capitaInvoke(order, billing, 11l, true);
		assertNotNull(response.getScpReference());
		assertNotNull(response.getTransactionState().getValue());
		assertNotNull(response.getInvokeResult().getStatus());
		assertEquals("SUCCESS", response.getInvokeResult().getStatus().getValue());

		// Not passing a returnURL, invalid request
		order.setReturnURL(null);
		response = capitaPaymentService.capitaInvoke(order, billing, 11l, true);
		assertNull(response);

		// Build a valid list of sub items to pass
		ArrayList<Map<String, String>> listOfItemDetails = new ArrayList<>();
		Map<String, String> summary = new HashMap<>();
		summary.put("amountInMinorUnits", "10000");
		summary.put("saleDescription", "item1");
		summary.put("reference", "123");

		listOfItemDetails.add(summary);
		//order.setItemDetails(listOfItemDetails);
		order.setReturnURL("scp:close");

		response = capitaPaymentService.capitaInvoke(order, billing, 11l, true);
		assertNotNull(response.getScpReference());
		assertNotNull(response.getTransactionState().getValue());
		assertNotNull(response.getInvokeResult().getStatus());
		assertEquals("SUCCESS", response.getInvokeResult().getStatus().getValue());

		// Build an invalid list of sub items to pass - the total value of the
		// subitems doesn't match the
		// overall cost we pass
		summary = new HashMap<>();
		summary.put("amountInMinorUnits", "10000");
		summary.put("saleDescription", "item2");
		summary.put("reference", "1233");

		listOfItemDetails.add(summary);

		response = capitaPaymentService.capitaInvoke(order, billing, 11l, true);
		assertNotNull(response.getScpReference());
		assertNotNull(response.getTransactionState().getValue());
		assertNotNull(response.getInvokeResult().getStatus());
		assertNotEquals("SUCCESS", response.getInvokeResult().getStatus().getValue());

	}

	@Test
	public void testCapitaQuery() {

		BillingDetails billing = new BillingDetails();

		// Build the card holders details object
		CardHolderDetails cardHolderDetails = new CardHolderDetails();

		Contact contact = new Contact();
		contact.setEmail("christine123@mailiantor.com");

		cardHolderDetails.setContact(contact);

		billing.setCardHolderDetails(cardHolderDetails);

		OneOffPaymentOrder order = new OneOffPaymentOrder();
		order.setRequestType(OneOffPaymentOrder.CardRequestType.PAY_ONLY);
		order.setReturnURL("scp:close");
		order.setSaleDescription("Christine Test sale");
		order.setSaleAmount(10000);
		order.setSaleReferenceCode("ABC123");

		// RequestType requestType, String returnURL, String backURL, String
		// saleDescription, int saleAmount, List<Map<String,String>>
		// listOfItemDetails, BillingDetails billingDetails
		ScpInvokeResponse response = capitaPaymentService.capitaInvoke(order, billing, 11l, true);

		String scpReference = response.getScpReference();
		ScpSimpleQueryResponse queryResponse = capitaPaymentService.capitaQuery(scpReference, "christine@mailinator.com");
		assertEquals(TransactionState._IN_PROGRESS, queryResponse.getTransactionState().getValue());
		assertEquals(queryResponse.getScpReference(), scpReference);

	}

}
