package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.AdvancePeopleSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.model.UserSearchField;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service.AdvancePeopleSearchConfigurationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class AdvancePeopleSearchMVCActionCommandTest extends PowerMockito {

	private static final String CMD = "cmd";

	private static final String CONFIGURED_SEARCH_FIELDS = "[]";

	private static final String FIELD1 = "field1";

	private static final String FIELD2 = "field2";

	private static final String LABEL1 = "label1";

	private static final String LABEL2 = "label2";

	private static final Locale LOCALE = Locale.UK;

	private static final String REDIRECT_URL = "redirectURL";

	@InjectMocks
	private AdvancePeopleSearchMVCActionCommand advancePeopleSearchMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private AdvancePeopleSearchConfigurationService mockAdvancePeopleSearchConfigurationService;

	@Mock
	private AdvancePeopleSearchPortletInstanceConfiguration mockAdvancePeopleSearchPortletInstanceConfiguration;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test(expected = ConfigurationException.class)
	public void doProcessAction_WhenConfigurationExceptionIsThrown_ThenThrowsException() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockActionRequest, CMD)).thenReturn(StringPool.BLANK);
		when(mockAdvancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay)).thenThrow(new ConfigurationException());

		advancePeopleSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenConfigurationIsNotValid_ThenRedirects() throws Exception {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockActionRequest, CMD)).thenReturn(StringPool.BLANK);

		advancePeopleSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, REDIRECT_URL);
	}

	@Test
	public void doProcessAction_WhenConfigurationIsValidAndCommandIsClearAll_ThenRemovesSharedFiltersAndRedirects() throws Exception {
		long companyId = 123;
		List<UserSearchField> searchFields = new ArrayList<>();
		searchFields.add(new UserSearchField("field1", "label1"));
		searchFields.add(new UserSearchField("field2", "label2"));

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockActionRequest, CMD)).thenReturn("clearAll");
		when(mockAdvancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(java.util.Optional.of(mockAdvancePeopleSearchPortletInstanceConfiguration));
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getLocale()).thenReturn(LOCALE);
		when(mockAdvancePeopleSearchPortletInstanceConfiguration.searchFields()).thenReturn(CONFIGURED_SEARCH_FIELDS);
		when(mockAdvancePeopleSearchConfigurationService.getSearchFields(companyId, LOCALE, CONFIGURED_SEARCH_FIELDS)).thenReturn(searchFields);

		advancePeopleSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		for (UserSearchField userSearchField : searchFields) {
			verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, userSearchField.getField());
		}

		verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, REDIRECT_URL);
	}

	@Test
	public void doProcessAction_WhenConfigurationIsValidAndCommandIsNotClearAll_ThenAddsSharedFiltersAndRedirects() throws Exception {
		long companyId = 123;
		String filterColour = "filterColour";
		String value = "value";
		List<UserSearchField> searchFields = new ArrayList<>();
		searchFields.add(new UserSearchField(FIELD1, LABEL1));
		searchFields.add(new UserSearchField(FIELD2, LABEL2));

		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockActionRequest, CMD)).thenReturn(StringPool.BLANK);
		when(ParamUtil.getString(mockActionRequest, filterColour)).thenReturn(filterColour);
		when(mockAdvancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(java.util.Optional.of(mockAdvancePeopleSearchPortletInstanceConfiguration));
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getLocale()).thenReturn(LOCALE);
		when(mockAdvancePeopleSearchPortletInstanceConfiguration.searchFields()).thenReturn(CONFIGURED_SEARCH_FIELDS);
		when(mockAdvancePeopleSearchConfigurationService.getSearchFields(companyId, LOCALE, CONFIGURED_SEARCH_FIELDS)).thenReturn(searchFields);
		when(ParamUtil.getString(mockActionRequest, FIELD1)).thenReturn(value);

		advancePeopleSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		for (UserSearchField userSearchField : searchFields) {
			verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, userSearchField.getField());
		}

		verify(mockSharedSearchRequestService, times(1)).addSharedActiveFilter(mockActionRequest, FIELD1, value, LABEL1, filterColour);
		verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, REDIRECT_URL);
	}

}
