<%@ include file="init.jsp"%>

<div class="row">

	<div class="col-md-12"> 

		<portlet:actionURL name="<%=MVCCommandKeys.EDIT_DETAILS%>" var="editDetailsURL" />

		<gds-forms-ui:errorSummary portletNamespace="${portletNamespace}" errors="${accountContext.getErrors()}"/>

		<aui:form action="${ editDetailsURL }" method="post" name="updateAccountForm">
			<aui:input type="hidden" name="userId" value="${ accountContext.userId }" />
			<aui:input type="hidden" name="redirectUrl" value="${ redirectUrl }" />

			<%@ include file="/fragments/creation-errors.jspf" %>

			<%@ include file="/fragments/personal-fields.jspf" %>
			
			<%@ include file="/fragments/additional-fields.jspf" %>

			<div class="row">
				<div class="col-md-12">
					<label class="govuk-label">
						<liferay-ui:message key="address"/>
					</label>
					<div class="govuk-inset-text">
						<p id="<portlet:namespace />addressPanel">
							<c:choose>
								<c:when test="${not empty accountContext.fullAddress}">
									${accountContext.fullAddress}
								</c:when>
								<c:otherwise>
									<liferay-ui:message key="no-current-address"/>
								</c:otherwise>
							</c:choose>
						</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<dp-address:postcode-lookup required="false" fallbackToNationalLookup="${ isAddressOutOfLocalAreaEnabled }"/>
				</div>
			</div>

			<portlet:renderURL var="updateAddressUrl">
				<portlet:param name="<%=RequestKeys.MVC_RENDER_COMMAND_NAME%>" value="<%=MVCCommandKeys.ADDRESS_DETAILS%>"/>
			</portlet:renderURL>

			<aui:button-row cssClass="pull-right">
				<aui:button type="cancel" value="cancel" />
				<aui:button type="submit" cssClass="btn btn-primary" value="update" icon="glyphicon glyphicon-chevron-right" iconAlign="right"/>
			</aui:button-row>

		</aui:form>
		
	</div>
</div>

<aui:script use="com-placecube-digitalplace-user-account-web">

		A.CreateAccountWeb.lookupAddress({
			fieldId: '<portlet:namespace/>uprn',
			panelId: '<portlet:namespace/>addressPanel',
			portletId: '<%=AccountPortletKeys.CREATE_ACCOUNT%>',
			resourceCommandId: '<%= MVCCommandKeys.ADDRESS_LOOKUP %>'
		});

</aui:script>

