package com.placecube.digitalplace.ddl.web.portlet.display;

import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLCategorisationService;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLEditorService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@Component(property = { "javax.portlet.name=" + PortletKeys.DDL_DISPLAY_PORTLET, "mvc.command.name=" + MVCCommandKeys.UPDATE_ENTRY }, service = MVCRenderCommand.class)
public class UpdateDDLEntryMVCRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(UpdateDDLEntryMVCRenderCommand.class);
	@Reference
	private DDLCategorisationService ddlCategorisationService;

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Reference
	private DDLEditorService ddlEditorService;

	@Reference
	private DynamicDataListService dynamicDataListService;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			long ddmStructureId = ParamUtil.getLong(renderRequest, "ddmStructureId");

			Optional<DDMStructure> structure = ddlEditorService.getDDMStructure(ddmStructureId);

			if (!structure.isPresent()) {
				throw new PortalException("Structure not found");
			}
			long ddlRecordId = ParamUtil.getLong(renderRequest, "ddlRecordId");
			long ddlRecordSetId = ParamUtil.getLong(renderRequest, "ddlRecordSetId");

			DDMStructure ddmStructure = structure.get();
			Optional<DDLRecord> ddlRecord = ddlEditorService.getDDLRecord(ddlRecordId);

			long classPK = ddmStructure.getStructureId();
			long classNameId = portal.getClassNameId(DDMStructure.class);
			Optional<DDMTemplate> ddmTemplate = ddlEditorService.getFormTemplate(ddmStructure);
			if (ddmTemplate.isPresent()) {
				classPK = ddmTemplate.get().getTemplateId();
				classNameId = portal.getClassNameId(DDMTemplate.class);
				renderRequest.setAttribute("ddmFormTemplateId", classPK);
			}
			renderRequest.setAttribute("classNameId", classNameId);
			renderRequest.setAttribute("classPK", classPK);
			renderRequest.setAttribute("ddmStructure", ddmStructure);

			renderRequest.setAttribute("ddlRecordSetId", ddlRecordSetId);
			renderRequest.setAttribute("groupIds", new long[] { themeDisplay.getScopeGroupId() });
			renderRequest.setAttribute("ddlRecordId", ddlRecordId);

			addCustomConfigurationAttributes(renderRequest, themeDisplay);

			if (ddlRecord.isPresent()) {
				renderRequest.setAttribute("ddmFormValues", ddlRecord.get().getDDMFormValues());
				configurePageTitle(renderRequest, themeDisplay, ddmStructure, "edit-x-in-x");
				configureBackURLForRecord(renderRequest, themeDisplay, ddlRecord.get());
			} else {
				renderRequest.setAttribute("ddlAutofillFieldsJSON", ddlEditorService.getFieldsToAutofill(ParamUtil.getStringValues(renderRequest, "ddlAutofillFields")));
				configureBackURLForAddAction(renderRequest, themeDisplay);
				configurePageTitle(renderRequest, themeDisplay, ddmStructure, "add-x-in-x");
			}

			return "/ddl-display/update-entry.jsp";

		} catch (Exception e) {
			LOG.debug(e.getMessage(), e);
			throw new PortletException(e);
		}
	}

	private void addCustomConfigurationAttributes(RenderRequest renderRequest, ThemeDisplay themeDisplay) throws ConfigurationException {
		DDLDisplayPortletInstanceConfiguration configuration = ddlConfigurationService.getDDLDisplayPortletInstanceConfiguration(themeDisplay);

		renderRequest.setAttribute("enableTranslations", configuration.enableTranslations());

		Set<AssetCategory> mandatoryCategories = ddlCategorisationService.getMandatoryCategories(ddlConfigurationService.getLongValuesAsList(configuration.mandatoryCategoryIds()));
		Set<AssetVocabulary> mandatoryVocabularies = ddlCategorisationService.getMandatoryVocabularies(ddlConfigurationService.getLongValuesAsList(configuration.mandatoryVocabularyIds()));

		renderRequest.setAttribute("requiredVocabularyIdsJsonArray", ddlCategorisationService.getVocabularyIdsJSON(mandatoryCategories, mandatoryVocabularies));
		renderRequest.setAttribute("requiredCategorisationNames", ddlCategorisationService.getRequiredCategorisationMessage(themeDisplay.getLocale(), mandatoryCategories, mandatoryVocabularies));
	}

	private void configureBackURLForAddAction(RenderRequest renderRequest, ThemeDisplay themeDisplay) {
		String backURL = ddlEditorService.getBackURL(renderRequest, themeDisplay);

		ddlEditorService.configureBackLinkURL(themeDisplay, backURL, renderRequest);
	}

	private void configureBackURLForRecord(RenderRequest renderRequest, ThemeDisplay themeDisplay, DDLRecord ddlRecord) {
		String backURL = ddlEditorService.getRedirectParamValue(renderRequest);

		if (Validator.isNull(backURL)) {
			backURL = dynamicDataListService.getViewDDLRecordURL(themeDisplay, ddlRecord, ParamUtil.getLong(renderRequest, "ddlTemplateId"));
		}

		ddlEditorService.configureBackLinkURL(themeDisplay, backURL, renderRequest);
	}

	private void configurePageTitle(RenderRequest renderRequest, ThemeDisplay themeDisplay, DDMStructure ddmStructure, String pageMessageTitleKey) {
		Locale locale = themeDisplay.getLocale();
		String[] messageArgs = new String[] { ddmStructure.getName(locale), themeDisplay.getScopeGroup().getName(locale) };
		String pageMessageTitle = LanguageUtil.format(portal.getHttpServletRequest(renderRequest), pageMessageTitleKey, messageArgs);
		renderRequest.setAttribute("pageMessageTitle", pageMessageTitle);
	}

}
