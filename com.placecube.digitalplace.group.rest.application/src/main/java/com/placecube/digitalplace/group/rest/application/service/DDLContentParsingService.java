package com.placecube.digitalplace.group.rest.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.text.StringEscapeUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.document.library.util.DLURLHelperUtil;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializer;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializerDeserializeRequest;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializerDeserializeResponse;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.group.rest.application.model.FileAttachment;
import com.placecube.digitalplace.group.rest.application.model.RestContentEntryField;

@Component(immediate = true, service = DDLContentParsingService.class)
public class DDLContentParsingService {

	private static final String DDL_RECORD_JSON_KEY_FIELD_VALUES = "fieldValues";

	private static final String DDL_RECORD_JSON_KEY_INSTANCE_ID = "instanceId";

	private static final int INSTANCE_ID_CHARACTER_LENGTH = 7;

	private static final Log LOG = LogFactoryUtil.getLog(DDLContentParsingService.class);

	@Reference(target = "(ddm.form.values.deserializer.type=json)")
	private DDMFormValuesDeserializer ddmFormValuesJsonDeserializer;

	@Reference
	private DLAppLocalService dlAppLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private RandomService randomService;

	public String addInstanceIdsToDDLRecordJSON(String ddlRecordJSON) throws JSONException {

		JSONObject ddlRecordJSONObject = jsonFactory.createJSONObject(ddlRecordJSON);
		JSONArray fieldValuesJSONArray = ddlRecordJSONObject.getJSONArray(DDL_RECORD_JSON_KEY_FIELD_VALUES);

		for (int index = 0; index < fieldValuesJSONArray.length(); index++) {

			JSONObject fieldValueJSONObject = fieldValuesJSONArray.getJSONObject(index);

			if (Validator.isNull(fieldValueJSONObject.getString(DDL_RECORD_JSON_KEY_INSTANCE_ID))) {

				String instanceId = randomService.generateRandomAlphanumericString(INSTANCE_ID_CHARACTER_LENGTH);
				fieldValueJSONObject.put(DDL_RECORD_JSON_KEY_INSTANCE_ID, instanceId);

			}

		}

		return ddlRecordJSONObject.toString();

	}

	public DDMFormValues deserializeDDLRecordJSON(String ddlRecordJSON, DDMForm ddmForm) {

		DDMFormValuesDeserializerDeserializeRequest.Builder builder = DDMFormValuesDeserializerDeserializeRequest.Builder.newBuilder(ddlRecordJSON, ddmForm);
		DDMFormValuesDeserializerDeserializeRequest ddmFormValuesDeserializerDeserializeRequest = builder.build();
		DDMFormValuesDeserializerDeserializeResponse ddmFormValuesDeserializerDeserializeResponse = ddmFormValuesJsonDeserializer.deserialize(ddmFormValuesDeserializerDeserializeRequest);

		return ddmFormValuesDeserializerDeserializeResponse.getDDMFormValues();

	}

	public String getContentWithAttachmentDetails(String ddlRecordJson, Set<FileAttachment> uploadedAttachments) {
		for (FileAttachment fileAttachment : uploadedAttachments) {
			ddlRecordJson = StringUtil.replace(ddlRecordJson, fileAttachment.getPlaceholderKey(), fileAttachment.getJsonValue());
		}
		return ddlRecordJson;
	}

	public String getFieldLabel(DDMFormField ddmFormField) {
		LocalizedValue label = ddmFormField.getLabel();
		return label.getString(label.getDefaultLocale());
	}

	public String getJSONObjectForAttachment(DLFileEntry dlFileEntry) {
		JSONObject attachmentJson = jsonFactory.createJSONObject();
		attachmentJson.put("classPK", dlFileEntry.getFileEntryId());
		attachmentJson.put("groupId", String.valueOf(dlFileEntry.getGroupId()));
		attachmentJson.put("title", dlFileEntry.getTitle());
		attachmentJson.put("type", "document");
		attachmentJson.put("uuid", dlFileEntry.getUuid());
		return StringEscapeUtils.escapeJava(attachmentJson.toJSONString());
	}

	public RestContentEntryField getRestContentEntryField(DDMFormField ddmFormField, String fieldName, DDMFormFieldValue ddmFormFieldValue) {

		Value value = ddmFormFieldValue.getValue();

		if (!Validator.isNotNull(value)) {
			return new RestContentEntryField(fieldName);
		}
		String viewURL = null;
		String fieldValue = StringPool.BLANK;

		Locale locale = value.getDefaultLocale();
		String baseValue = value.getString(locale);

		if (isValidValue(baseValue)) {
			fieldValue = baseValue;

			String fieldType = ddmFormField.getType();

			if ("ddm-text-html".equals(fieldType)) {
				fieldValue = HtmlUtil.stripHtml(baseValue);

			} else if ("select".equals(fieldType)) {
				fieldValue = getSelectFieldValue(ddmFormField, fieldValue, locale, baseValue);

			} else if ("ddm-documentlibrary".equals(fieldType)) {
				try {
					JSONObject documentLibraryValues = jsonFactory.createJSONObject(baseValue);
					long fileEntryId = documentLibraryValues.getLong("fileEntryId", 0L);
					if (fileEntryId > 0) {
						FileEntry fileEntry = dlAppLocalService.getFileEntry(fileEntryId);
						fieldValue = fileEntry.getTitle();
						viewURL = DLURLHelperUtil.getDownloadURL(fileEntry, fileEntry.getFileVersion(), null, StringPool.BLANK, true, true);
					}
				} catch (Exception e) {
					LOG.warn("Unable to get document url - " + e.getMessage());
					LOG.debug(e);
				}

			} else if ("radio".equals(fieldType)) {
				fieldValue = getOptionLabel(ddmFormField, locale, fieldValue);

			}
		}

		return new RestContentEntryField(fieldName, fieldValue.trim(), viewURL);
	}

	public JSONArray getSafeJSONArray(String value) throws JSONException {
		if (Validator.isNotNull(value)) {
			return jsonFactory.createJSONArray(value);
		}
		return jsonFactory.createJSONArray();
	}

	private String getOptionLabel(DDMFormField ddmFormField, Locale locale, String valueString) {
		DDMFormFieldOptions ddmFormFieldOptions = ddmFormField.getDDMFormFieldOptions();
		LocalizedValue optionLabels = ddmFormFieldOptions.getOptionLabels(valueString);
		return optionLabels.getString(locale);
	}

	private String getSelectFieldValue(DDMFormField ddmFormField, String fieldValue, Locale locale, String baseValue) {
		String valueForField = getValueWithoutBrackets(baseValue);
		if (ddmFormField.isMultiple()) {
			List<String> optionValues = new ArrayList<>();
			String[] values = StringUtil.split(valueForField, StringPool.COMMA);
			for (String val : values) {
				optionValues.add(getOptionLabel(ddmFormField, locale, getValueWithoutWrappingQuotes(val)));
			}
			fieldValue = String.join(StringPool.COMMA_AND_SPACE, optionValues);
		} else {
			fieldValue = getOptionLabel(ddmFormField, locale, valueForField);
		}
		return fieldValue;
	}

	private String getValueWithoutBrackets(String valueString) {
		valueString = StringUtil.replaceFirst(valueString, "[\"", StringPool.BLANK);
		return StringUtil.replaceLast(valueString, "\"]", StringPool.BLANK);
	}

	private String getValueWithoutWrappingQuotes(String valueString) {
		valueString = StringUtil.replaceFirst(valueString, "\"", StringPool.BLANK);
		return StringUtil.replaceLast(valueString, "\"", StringPool.BLANK);
	}

	private boolean isValidValue(String valueString) {
		return Validator.isNotNull(valueString) && !"[]".equals(valueString);
	}

}
