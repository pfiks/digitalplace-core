AUI.add(
	'com-placecube-digitalplace-search-web',
	
	function(A) {
	
		var DigitalPlaceSearchWeb = {
		
			submitFormOnChange: function(fieldSelector, formName) {
				try{
					$(fieldSelector).on('change', function(e) {
						document.forms[formName].submit();
					});
				} catch(e) {
					console.log(e);
				}
			}
		};
		
		A.DigitalPlaceSearchWeb = DigitalPlaceSearchWeb;
	},
	'',
	{
		requires: [
			'aui-base'
		]
	}
);
