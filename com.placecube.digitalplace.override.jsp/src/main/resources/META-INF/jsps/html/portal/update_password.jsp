<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-util:dynamic-include key="/html/portal/update_password.jsp#pre" />

<liferay-util:buffer var="html">
	<liferay-util:include page="/html/portal/update_password.portal.jsp" servletContext="<%= application %>" />
</liferay-util:buffer>

<%
	String passwordPolicy = GetterUtil.getString(request.getAttribute("passwordPolicy"), "");

	int formTagStartIndex = html.lastIndexOf("<form"); 
	
	StringBuilder stringBuilder = new StringBuilder();
	
	if (formTagStartIndex > -1 && Validator.isNotNull(passwordPolicy)) {
		
		int formTagEndIndex = html.indexOf('>', formTagStartIndex);
		int closeFormTagIndex = html.indexOf("</form>", formTagEndIndex); 
		
		stringBuilder.append(html.substring(0, formTagEndIndex + 1));
		stringBuilder.append("<div class=\"row\"><div class=\"col-md-6\">");
		
		stringBuilder.append(html.substring(formTagEndIndex + 2, closeFormTagIndex));
		stringBuilder.append("</div>");
		stringBuilder.append("<div class=\"col-md-6\">");
		stringBuilder.append(passwordPolicy);
		stringBuilder.append("</div></div>");
		stringBuilder.append(html.substring(closeFormTagIndex));
	}
	else {
		stringBuilder.append(html);
	}
%>

<%= stringBuilder.toString() %>