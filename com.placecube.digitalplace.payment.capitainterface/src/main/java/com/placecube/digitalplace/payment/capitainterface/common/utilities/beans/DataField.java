package com.placecube.digitalplace.payment.capitainterface.common.utilities.beans;

/**
 * A definition of the allowed diagnostic data fields which can be logged
 *
 */
public enum DataField {

    UPRN,
    USER_ID,
    CAPITA_REF_NO,
    CAPITA_TX_REF_NO,
    CAPITA_STATUS,
    DRUPAL_PAGE_TYPE,
    DRUPAL_INFOCHUNK_ID,
    DRUPAL_PAGE_ID,
    MISSING_MANDATORY_FIELD,
    PERMIT_APPLICATION_NUMBER,
    VEHICLE_REG_NO,
    PERMIT_NUMBERS,
    ESB_ERROR_CODE,
    DATE,
    FILE_PATH,
    SOURCE_SYSTEM,
    REFERENCE_NUMBER,
    XML_STRING,
    POSTCODE,
    RESPONSE_STRING,
    RESPONSE_CODE,
    DESCRIPTION,
    ACTION_URL,
    URL,
    CONTENT_KEY,
    AMOUNT,
    TEMPLATE_NAME,
    FLOW_NAME,
    FUND_CODE,
    ATTRIBUTE_NAME,
    BOOKING_BUG_REQUEST_INFO,
    LIFERAY_PORTLET_ERROR,
    REPAIRS_JOB_INFO,
    REQUEST_STRING,
	TRANSACTION_ID;

}
