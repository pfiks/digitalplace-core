package com.placecube.digitalplace.exportimport.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.exportimport.configuration.ExportImportServiceConfiguration;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil;
import com.liferay.portal.kernel.util.HashMapDictionary;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.configuration.module.configuration.ConfigurationProviderUtil" })
@PrepareForTest({ ExportImportServiceImpl.class, ConfigurationProviderUtil.class })
public class ExportImportServiceImplTest extends PowerMockito {

	@InjectMocks
	private ExportImportServiceImpl exportImportStagingConfigurationServiceImpl;

	@Mock
	private ExportImportServiceConfiguration mockExportImportServiceConfiguration;

	@Mock
	private HashMapDictionary<String, Object> mockProperties;

	@Before
	public void activateSetup() {
		mockStatic(ConfigurationProviderUtil.class);
	}

	@Test(expected = PortalException.class)
	public void disableValidateLayoutReferences_WhenConfigurationExceptionThrown_ThenThrowPortalException() throws Exception {
		long companyId = 1;
		whenNew(HashMapDictionary.class).withNoArguments().thenReturn(mockProperties);
		PowerMockito.doThrow(new ConfigurationException()).when(ConfigurationProviderUtil.class, "saveCompanyConfiguration", ExportImportServiceConfiguration.class, companyId, mockProperties);

		exportImportStagingConfigurationServiceImpl.disableValidateLayoutReferences(companyId);
	}

	@Test
	public void disableValidateLayoutReferences_WhenNoError_ThenDisableValidateLayoutReferences() throws Exception {
		long companyId = 1;
		whenNew(HashMapDictionary.class).withNoArguments().thenReturn(mockProperties);
		exportImportStagingConfigurationServiceImpl.disableValidateLayoutReferences(companyId);

		verifyStatic(ConfigurationProviderUtil.class);
		ConfigurationProviderUtil.saveCompanyConfiguration(ExportImportServiceConfiguration.class, companyId, mockProperties);
	}

	@Test(expected = PortalException.class)
	public void enableValidateLayoutReferences_WhenConfigurationExceptionThrown_ThenThrowPortalException() throws Exception {
		long companyId = 1;
		whenNew(HashMapDictionary.class).withNoArguments().thenReturn(mockProperties);
		PowerMockito.doThrow(new ConfigurationException()).when(ConfigurationProviderUtil.class, "saveCompanyConfiguration", ExportImportServiceConfiguration.class, companyId, mockProperties);

		exportImportStagingConfigurationServiceImpl.enableValidateLayoutReferences(companyId);
	}

	@Test
	public void enableValidateLayoutReferences_WhenNoError_ThenEnableValidateLayoutReferences() throws Exception {
		long companyId = 1;
		whenNew(HashMapDictionary.class).withNoArguments().thenReturn(mockProperties);
		exportImportStagingConfigurationServiceImpl.enableValidateLayoutReferences(companyId);

		verifyStatic(ConfigurationProviderUtil.class);
		ConfigurationProviderUtil.saveCompanyConfiguration(ExportImportServiceConfiguration.class, companyId, mockProperties);
	}
}
