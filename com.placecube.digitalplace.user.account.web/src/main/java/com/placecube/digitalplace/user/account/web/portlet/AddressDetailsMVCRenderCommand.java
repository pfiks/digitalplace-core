package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.ADDRESS_DETAILS }, service = MVCRenderCommand.class)
public class AddressDetailsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private UserAccountFieldRegistry userAccountFieldRegistry;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(renderRequest);

			accountContextService.configureAddressDetails(accountContext, renderRequest);

			renderRequest.setAttribute("addressRequired", accountConfigurationService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId()).addressRequired());
			renderRequest.setAttribute(RequestKeys.ACCOUNT_CONTEXT, accountContext);
			renderRequest.setAttribute(RequestKeys.ACCOUNT_FIELDS, userAccountFieldRegistry.getUserAccountFieldsByStep(AccountCreationStep.ADDRESS_DETAILS, accountContext.getCompanyId()));
			renderRequest.setAttribute(RequestKeys.IS_ADDRESS_OUT_OF_LOCAL_AREA_ENABLED, accountConfigurationService.getAccountConfiguration(accountContext, renderRequest).addressOutOfLocalArea());
			accountContextService.updateContextInSession(accountContext, renderRequest);
		} catch (UserAccountException e) {
			throw new PortletException(e);
		}

		return "/address-details.jsp";
	}

}
