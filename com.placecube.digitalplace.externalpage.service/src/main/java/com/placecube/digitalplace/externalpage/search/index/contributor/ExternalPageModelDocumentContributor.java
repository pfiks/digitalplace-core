package com.placecube.digitalplace.externalpage.search.index.contributor;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.HtmlParserUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.externalpage.model.ExternalPage;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage", service = ModelDocumentContributor.class)
public class ExternalPageModelDocumentContributor implements ModelDocumentContributor<ExternalPage> {

	private static final Log LOG = LogFactoryUtil.getLog(ExternalPageModelDocumentContributor.class);

	@Override
	public void contribute(Document document, ExternalPage externalPage) {
		document.addKeyword(Field.UID, externalPage.getUuid());
		document.addKeyword(Field.ENTRY_CLASS_NAME, ExternalPage.class.getName());
		document.addKeyword(Field.CLASS_PK, externalPage.getExternalPageId());
		document.addKeyword(Field.COMPANY_ID, externalPage.getCompanyId());
		document.addKeyword(Field.GROUP_ID, externalPage.getGroupId());
		document.addKeyword(Field.SCOPE_GROUP_ID, externalPage.getGroupId());
		document.addKeyword(Field.USER_ID, externalPage.getUserId());
		document.addKeyword(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
		document.addKeyword(Field.URL, externalPage.getUrl());
		document.addText(Field.TITLE, HtmlParserUtil.extractText(externalPage.getTitle()));
		document.addText(Field.CONTENT, HtmlParserUtil.extractText(externalPage.getContent()));
		document.addDate(Field.CREATE_DATE, externalPage.getCreateDate());
		document.addDate(Field.DISPLAY_DATE, externalPage.getCreateDate());
		document.addDate(Field.PUBLISH_DATE, externalPage.getCreateDate());
		document.addDate(Field.MODIFIED_DATE, externalPage.getModifiedDate());

		LOG.debug("Indexing externalPage: " + document);
	}

}
