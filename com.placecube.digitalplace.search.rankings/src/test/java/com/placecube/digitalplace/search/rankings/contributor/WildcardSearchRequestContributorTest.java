package com.placecube.digitalplace.search.rankings.contributor;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.search.document.Document;
import com.liferay.portal.search.engine.adapter.search.SearchSearchResponse;
import com.liferay.portal.search.hits.SearchHit;
import com.liferay.portal.search.hits.SearchHits;
import com.liferay.portal.search.searcher.SearchRequest;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.SearchRequestBuilderFactory;
import com.placecube.digitalplace.search.rankings.model.SearchRanking;
import com.placecube.digitalplace.search.rankings.service.RankingsConfigurationService;
import com.placecube.digitalplace.search.rankings.service.RankingsRegexService;
import com.placecube.digitalplace.search.rankings.service.RankingsService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SearchRanking.class })
public class WildcardSearchRequestContributorTest extends PowerMockito {

	private Locale locale = LocaleUtil.UK;

	@Mock
	private Document mockDocument;

	@Mock
	private SearchRequest mockReturnSearchRequest;

	@Mock
	private RankingsConfigurationService mockRankingsConfigurationService;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private Consumer<SearchContext> mockSearchContextConsumer;

	@Mock
	private SearchHit mockSearchHit;

	@Mock
	private SearchHits mockSearchHits;

	@Mock
	private SearchRanking mockSearchRanking;

	@Mock
	private RankingsService mockRankingsService;

	@Mock
	private RankingsRegexService mockRankingsRegexService;

	@Mock
	private SearchRequest mockSearchRequest;

	@Mock
	private SearchRequestBuilder mockSearchRequestBuilder;

	@Mock
	private SearchRequestBuilderFactory mockSearchRequestBuilderFactory;

	@Mock
	private SearchSearchResponse mockSearchSearchResponse;

	@InjectMocks
	private WildcardSearchRequestContributor wildcardSearchRequestContributor;

	@Before
	public void setUp() {
		mockStatic(SearchRanking.class);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void contribute_WhenWildCardRankingIsEnabledAndRankingsWereFoundAndAliasTypeIsExactAliasAndMatchIsFound_ThenPinnedIdsAndBlockedIdsAreAddedToTheSearchBuilder() {
		boolean wildCardRankingsEnabled = true;
		boolean isAliasWildcard = false;
		long companyId = 342l;
		String alias = "alias";
		String keywords = "alias";

		List<SearchHit> searchHits = new ArrayList<>();
		searchHits.add(mockSearchHit);

		List<String> aliases = new ArrayList<>();
		aliases.add(alias);

		List pinnedIds = new ArrayList<>();

		when(mockSearchRequestBuilderFactory.builder(mockSearchRequest)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchContext.getCompanyId()).thenReturn(companyId);
		when(mockSearchContext.getLocale()).thenReturn(locale);
		doAnswer(invocation -> {
			Consumer<SearchContext> consumer = invocation.getArgument(0);
			consumer.accept(mockSearchContext);
			return null;
		}).when(mockSearchRequestBuilder).withSearchContext(any(Consumer.class));
		when(mockSearchRequest.getQueryString()).thenReturn(keywords);
		when(mockRankingsConfigurationService.isEnableWildcardRankings(companyId)).thenReturn(wildCardRankingsEnabled);
		when(mockRankingsService.getSearchRankingsForKeywords(keywords, companyId)).thenReturn(mockSearchSearchResponse);
		when(mockSearchSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(searchHits);
		when(mockSearchHit.getDocument()).thenReturn(mockDocument);
		when(SearchRanking.fromSearchDocument(mockDocument)).thenReturn(mockSearchRanking);
		when(mockSearchRanking.isDisabled()).thenReturn(false);
		when(mockSearchRanking.getAliases()).thenReturn(aliases);
		when(mockRankingsService.isAliasWildcard(alias)).thenReturn(isAliasWildcard);
		when(mockSearchRanking.getPinnedIds()).thenReturn(pinnedIds);

		wildcardSearchRequestContributor.contribute(mockSearchRequest);

		InOrder inOrder = Mockito.inOrder(mockRankingsService);
		inOrder.verify(mockRankingsService, times(1)).addPinnedIdsQueries(mockSearchRequestBuilder, pinnedIds);
		inOrder.verify(mockRankingsService, times(1)).addBlockedIdsQuery(mockSearchRequestBuilder, mockSearchRanking);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void contribute_WhenWildCardRankingIsEnabledAndAliasAndKeywordsMatchWithDifferentCasing_ThenAMatchIsFoundAndPinnedIdsAndBlockedIdsAreAddedToTheSearchBuilder() {
		boolean wildCardRankingsEnabled = true;
		boolean isAliasWildcard = false;
		long companyId = 342l;
		String alias = "AliAS";
		String keywords = "ALIAS";

		List<SearchHit> searchHits = new ArrayList<>();
		searchHits.add(mockSearchHit);

		List<String> aliases = new ArrayList<>();
		aliases.add(alias);

		List pinnedIds = new ArrayList<>();

		when(mockSearchRequestBuilderFactory.builder(mockSearchRequest)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchContext.getCompanyId()).thenReturn(companyId);
		when(mockSearchContext.getLocale()).thenReturn(locale);
		doAnswer(invocation -> {
			Consumer<SearchContext> consumer = invocation.getArgument(0);
			consumer.accept(mockSearchContext);
			return null;
		}).when(mockSearchRequestBuilder).withSearchContext(any(Consumer.class));
		when(mockSearchRequest.getQueryString()).thenReturn(keywords);
		when(mockRankingsConfigurationService.isEnableWildcardRankings(companyId)).thenReturn(wildCardRankingsEnabled);
		when(mockRankingsService.getSearchRankingsForKeywords(keywords.toLowerCase(locale), companyId)).thenReturn(mockSearchSearchResponse);
		when(mockSearchSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(searchHits);
		when(mockSearchHit.getDocument()).thenReturn(mockDocument);
		when(SearchRanking.fromSearchDocument(mockDocument)).thenReturn(mockSearchRanking);
		when(mockSearchRanking.isDisabled()).thenReturn(false);
		when(mockSearchRanking.getAliases()).thenReturn(aliases);
		when(mockRankingsService.isAliasWildcard(alias)).thenReturn(isAliasWildcard);
		when(mockSearchRanking.getPinnedIds()).thenReturn(pinnedIds);

		wildcardSearchRequestContributor.contribute(mockSearchRequest);

		InOrder inOrder = Mockito.inOrder(mockRankingsService);
		inOrder.verify(mockRankingsService, times(1)).addPinnedIdsQueries(mockSearchRequestBuilder, pinnedIds);
		inOrder.verify(mockRankingsService, times(1)).addBlockedIdsQuery(mockSearchRequestBuilder, mockSearchRanking);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void contribute_WhenWildCardRankingIsEnabledAndRankingsWereFoundAndAliasTypeIsExactAliasAndNoMatchIsFound_ThenPinnedIdsAndBlockedIdsNotAddedToTheSearchBuilder() {
		boolean wildCardRankingsEnabled = true;
		boolean isAliasWildcard = false;
		long companyId = 342l;
		String alias = "alias";
		String keywords = "alias no match";

		List<SearchHit> searchHits = new ArrayList<>();
		searchHits.add(mockSearchHit);

		List<String> aliases = new ArrayList<>();
		aliases.add(alias);

		List pinnedIds = new ArrayList<>();

		when(mockSearchRequestBuilderFactory.builder(mockSearchRequest)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchContext.getCompanyId()).thenReturn(companyId);
		when(mockSearchContext.getLocale()).thenReturn(locale);
		doAnswer(invocation -> {
			Consumer<SearchContext> consumer = invocation.getArgument(0);
			consumer.accept(mockSearchContext);
			return null;
		}).when(mockSearchRequestBuilder).withSearchContext(any(Consumer.class));
		when(mockSearchRequest.getQueryString()).thenReturn(keywords);
		when(mockRankingsConfigurationService.isEnableWildcardRankings(companyId)).thenReturn(wildCardRankingsEnabled);
		when(mockRankingsService.getSearchRankingsForKeywords(keywords, companyId)).thenReturn(mockSearchSearchResponse);
		when(mockSearchSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(searchHits);
		when(mockSearchHit.getDocument()).thenReturn(mockDocument);
		when(SearchRanking.fromSearchDocument(mockDocument)).thenReturn(mockSearchRanking);
		when(mockSearchRanking.isDisabled()).thenReturn(false);
		when(mockSearchRanking.getAliases()).thenReturn(aliases);
		when(mockRankingsService.isAliasWildcard(alias)).thenReturn(isAliasWildcard);
		when(mockSearchRanking.getPinnedIds()).thenReturn(pinnedIds);

		wildcardSearchRequestContributor.contribute(mockSearchRequest);

		InOrder inOrder = Mockito.inOrder(mockRankingsService);
		inOrder.verify(mockRankingsService, never()).addPinnedIdsQueries(mockSearchRequestBuilder, pinnedIds);
		inOrder.verify(mockRankingsService, never()).addBlockedIdsQuery(mockSearchRequestBuilder, mockSearchRanking);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void contribute_WhenWildCardRankingIsEnabledAndRankingsWereFoundAndAliasTypeIsWildAndMatchIsFound_ThenPinnedIdsAndBlockedIdsAreAddedToTheSearchBuilder() {
		boolean wildCardRankingsEnabled = true;
		boolean isAliasWildcard = true;
		long companyId = 342l;
		String alias = "*alias";
		String keywords = "alias";
		String regexResult = "alias";

		List<SearchHit> searchHits = new ArrayList<>();
		searchHits.add(mockSearchHit);

		List<String> aliases = new ArrayList<>();
		aliases.add(alias);

		List pinnedIds = new ArrayList<>();

		when(mockSearchRequestBuilderFactory.builder(mockSearchRequest)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchContext.getCompanyId()).thenReturn(companyId);
		when(mockSearchContext.getLocale()).thenReturn(locale);
		doAnswer(invocation -> {
			Consumer<SearchContext> consumer = invocation.getArgument(0);
			consumer.accept(mockSearchContext);
			return null;
		}).when(mockSearchRequestBuilder).withSearchContext(any(Consumer.class));
		when(mockSearchRequest.getQueryString()).thenReturn(keywords);
		when(mockRankingsConfigurationService.isEnableWildcardRankings(companyId)).thenReturn(wildCardRankingsEnabled);
		when(mockRankingsService.getSearchRankingsForKeywords(keywords, companyId)).thenReturn(mockSearchSearchResponse);
		when(mockSearchSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(searchHits);
		when(mockSearchHit.getDocument()).thenReturn(mockDocument);
		when(SearchRanking.fromSearchDocument(mockDocument)).thenReturn(mockSearchRanking);
		when(mockSearchRanking.isDisabled()).thenReturn(false);
		when(mockSearchRanking.getAliases()).thenReturn(aliases);
		when(mockRankingsService.isAliasWildcard(alias)).thenReturn(isAliasWildcard);
		when(mockSearchRanking.getPinnedIds()).thenReturn(pinnedIds);
		when(mockRankingsRegexService.getAliasWildcardRegex(alias)).thenReturn(keywords);
		when(mockRankingsRegexService.getAliasWildcardRegex(alias)).thenReturn(regexResult);

		wildcardSearchRequestContributor.contribute(mockSearchRequest);

		InOrder inOrder = Mockito.inOrder(mockRankingsService);
		inOrder.verify(mockRankingsService, times(1)).addPinnedIdsQueries(mockSearchRequestBuilder, pinnedIds);
		inOrder.verify(mockRankingsService, times(1)).addBlockedIdsQuery(mockSearchRequestBuilder, mockSearchRanking);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void contribute_WhenWildCardRankingIsEnabledAndRankingsWereFoundAndAliasTypeIsWildAndNoMatchIsFound_ThenNoPinnedOrBlockedIdsAreAddedToTheSearchBuilder() {
		boolean wildCardRankingsEnabled = true;
		boolean isAliasWildcard = true;
		long companyId = 342l;
		String alias = "*alias";
		String keywords = "keywords";
		String regexResult = "no-match";

		List<SearchHit> searchHits = new ArrayList<>();
		searchHits.add(mockSearchHit);

		List<String> aliases = new ArrayList<>();
		aliases.add(alias);

		List pinnedIds = new ArrayList<>();

		when(mockSearchRequestBuilderFactory.builder(mockSearchRequest)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchContext.getCompanyId()).thenReturn(companyId);
		when(mockSearchContext.getLocale()).thenReturn(locale);
		doAnswer(invocation -> {
			Consumer<SearchContext> consumer = invocation.getArgument(0);
			consumer.accept(mockSearchContext);
			return null;
		}).when(mockSearchRequestBuilder).withSearchContext(any(Consumer.class));
		when(mockSearchRequest.getQueryString()).thenReturn(keywords);
		when(mockRankingsConfigurationService.isEnableWildcardRankings(companyId)).thenReturn(wildCardRankingsEnabled);
		when(mockRankingsService.getSearchRankingsForKeywords(keywords, companyId)).thenReturn(mockSearchSearchResponse);
		when(mockSearchSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(searchHits);
		when(mockSearchHit.getDocument()).thenReturn(mockDocument);
		when(SearchRanking.fromSearchDocument(mockDocument)).thenReturn(mockSearchRanking);
		when(mockSearchRanking.isDisabled()).thenReturn(false);
		when(mockSearchRanking.getAliases()).thenReturn(aliases);
		when(mockRankingsService.isAliasWildcard(alias)).thenReturn(isAliasWildcard);
		when(mockSearchRanking.getPinnedIds()).thenReturn(pinnedIds);
		when(mockRankingsRegexService.getAliasWildcardRegex(alias)).thenReturn(regexResult);

		wildcardSearchRequestContributor.contribute(mockSearchRequest);

		InOrder inOrder = Mockito.inOrder(mockRankingsService);
		inOrder.verify(mockRankingsService, never()).addPinnedIdsQueries(mockSearchRequestBuilder, pinnedIds);
		inOrder.verify(mockRankingsService, never()).addBlockedIdsQuery(mockSearchRequestBuilder, mockSearchRanking);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void contribute_WhenWildCardRankingIsEnabledAndNoRankingsAreFound_ThenNoPinnedOrBlockedIdsAreAddedToTheSearchBuilder() {
		boolean wildCardRankingsEnabled = true;
		long companyId = 342l;
		String alias = "*alias";
		String keywords = "alias";

		List<SearchHit> searchHits = new ArrayList<>();

		List<String> aliases = new ArrayList<>();
		aliases.add(alias);

		List pinnedIds = new ArrayList<>();

		when(mockSearchRequestBuilderFactory.builder(mockSearchRequest)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchContext.getCompanyId()).thenReturn(companyId);
		when(mockSearchContext.getLocale()).thenReturn(locale);
		doAnswer(invocation -> {
			Consumer<SearchContext> consumer = invocation.getArgument(0);
			consumer.accept(mockSearchContext);
			return null;
		}).when(mockSearchRequestBuilder).withSearchContext(any(Consumer.class));
		when(mockSearchRequest.getQueryString()).thenReturn(keywords);
		when(mockRankingsConfigurationService.isEnableWildcardRankings(companyId)).thenReturn(wildCardRankingsEnabled);
		when(mockRankingsService.getSearchRankingsForKeywords(keywords, companyId)).thenReturn(mockSearchSearchResponse);
		when(mockSearchSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(searchHits);

		wildcardSearchRequestContributor.contribute(mockSearchRequest);

		InOrder inOrder = Mockito.inOrder(mockRankingsService);
		inOrder.verify(mockRankingsService, never()).addPinnedIdsQueries(mockSearchRequestBuilder, pinnedIds);
		inOrder.verify(mockRankingsService, never()).addBlockedIdsQuery(mockSearchRequestBuilder, mockSearchRanking);

	}

	@SuppressWarnings({ "unchecked" })
	@Test
	public void contribute_WhenWildCardRankingIsEnabledAndRankingsWereFoundAndRankingIsDisabled_ThenRankingAliasesAreNotProcessed() {
		boolean wildCardRankingsEnabled = true;
		long companyId = 342l;
		String alias = "*alias";
		String keywords = "alias";

		List<SearchHit> searchHits = new ArrayList<>();
		searchHits.add(mockSearchHit);

		List<String> aliases = new ArrayList<>();
		aliases.add(alias);

		when(mockSearchRequestBuilderFactory.builder(mockSearchRequest)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchContext.getCompanyId()).thenReturn(companyId);
		when(mockSearchContext.getLocale()).thenReturn(locale);
		doAnswer(invocation -> {
			Consumer<SearchContext> consumer = invocation.getArgument(0);
			consumer.accept(mockSearchContext);
			return null;
		}).when(mockSearchRequestBuilder).withSearchContext(any(Consumer.class));
		when(mockSearchRequest.getQueryString()).thenReturn(keywords);
		when(mockRankingsConfigurationService.isEnableWildcardRankings(companyId)).thenReturn(wildCardRankingsEnabled);
		when(mockRankingsService.getSearchRankingsForKeywords(keywords, companyId)).thenReturn(mockSearchSearchResponse);
		when(mockSearchSearchResponse.getSearchHits()).thenReturn(mockSearchHits);
		when(mockSearchHits.getSearchHits()).thenReturn(searchHits);
		when(mockSearchHit.getDocument()).thenReturn(mockDocument);
		when(SearchRanking.fromSearchDocument(mockDocument)).thenReturn(mockSearchRanking);
		when(mockSearchRanking.isDisabled()).thenReturn(true);

		wildcardSearchRequestContributor.contribute(mockSearchRequest);

		verify(mockSearchRanking, never()).getAliases();

	}

	@SuppressWarnings({ "unchecked" })
	@Test
	public void contribute_WhenContributeCompletes_ThenANewSearchRequestIsBuiltAndReturned() {
		long companyId = 342l;

		List<SearchHit> searchHits = new ArrayList<>();
		searchHits.add(mockSearchHit);

		when(mockSearchRequestBuilderFactory.builder(mockSearchRequest)).thenReturn(mockSearchRequestBuilder);
		when(mockSearchContext.getCompanyId()).thenReturn(companyId);
		when(mockSearchContext.getLocale()).thenReturn(locale);
		doAnswer(invocation -> {
			Consumer<SearchContext> consumer = invocation.getArgument(0);
			consumer.accept(mockSearchContext);
			return null;
		}).when(mockSearchRequestBuilder).withSearchContext(any(Consumer.class));
		when(mockSearchRequestBuilder.build()).thenReturn(mockReturnSearchRequest);

		SearchRequest result = wildcardSearchRequestContributor.contribute(mockSearchRequest);

		assertThat(result, equalTo(mockReturnSearchRequest));

	}

}
