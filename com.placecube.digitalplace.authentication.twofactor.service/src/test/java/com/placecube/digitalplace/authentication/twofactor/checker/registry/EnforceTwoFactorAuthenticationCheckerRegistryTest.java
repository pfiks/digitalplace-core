package com.placecube.digitalplace.authentication.twofactor.checker.registry;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.reflect.internal.WhiteboxImpl;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.placecube.digitalplace.authentication.twofactor.checker.EnforceTwoFactorAuthenticationChecker;

public class EnforceTwoFactorAuthenticationCheckerRegistryTest extends PowerMockito {

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private ServiceWrapper<EnforceTwoFactorAuthenticationChecker> mockServiceWrapper1;

	@Mock
	private ServiceWrapper<EnforceTwoFactorAuthenticationChecker> mockServiceWrapper2;

	@Mock
	private ServiceWrapper<EnforceTwoFactorAuthenticationChecker> mockServiceWrapper3;

	@Mock
	private EnforceTwoFactorAuthenticationChecker mockEnforceTwoFactorAuthenticationChecker1;

	@Mock
	private EnforceTwoFactorAuthenticationChecker mockEnforceTwoFactorAuthenticationChecker2;

	@Mock
	private EnforceTwoFactorAuthenticationChecker mockEnforceTwoFactorAuthenticationChecker3;

	@InjectMocks
	private EnforceTwoFactorAuthenticationCheckerRegistry enforceTwoFactorAuthenticationCheckerRegistry;

	@Before
	public void activateSetup() {
		initMocks(this);
		enforceTwoFactorAuthenticationCheckerRegistry = spy(new EnforceTwoFactorAuthenticationCheckerRegistry());
	}

	@Test
	public void getEnforceTwoFactorAuthenticationCheckers_WhenNoError_ThenReturnsTheCheckers() {
		configureInternalMap();

		when(mockServiceWrapper1.getService()).thenReturn(mockEnforceTwoFactorAuthenticationChecker1);
		when(mockServiceWrapper2.getService()).thenReturn(mockEnforceTwoFactorAuthenticationChecker2);
		when(mockServiceWrapper3.getService()).thenReturn(mockEnforceTwoFactorAuthenticationChecker3);

		List<EnforceTwoFactorAuthenticationChecker> results = enforceTwoFactorAuthenticationCheckerRegistry.getEnforceTwoFactorAuthenticationCheckers();

		assertThat(results, contains(mockEnforceTwoFactorAuthenticationChecker1, mockEnforceTwoFactorAuthenticationChecker2, mockEnforceTwoFactorAuthenticationChecker3));
	}

	private void configureInternalMap() {
		ServiceTrackerMap<String, ServiceWrapper<EnforceTwoFactorAuthenticationChecker>> enforceTwoFactorAuthenticationCheckerMap = new ServiceTrackerMap<String, ServiceWrapper<EnforceTwoFactorAuthenticationChecker>>() {

			@Override
			public void close() {
			}

			@Override
			public boolean containsKey(String key) {
				return false;
			}

			@Override
			public ServiceWrapper<EnforceTwoFactorAuthenticationChecker> getService(String key) {
				return null;
			}

			@Override
			public Set<String> keySet() {
				return null;
			}

			@Override
			public Collection<ServiceWrapper<EnforceTwoFactorAuthenticationChecker>> values() {
				Collection<ServiceWrapper<EnforceTwoFactorAuthenticationChecker>> checkers = new ArrayList<>();
				checkers.add(mockServiceWrapper1);
				checkers.add(mockServiceWrapper2);
				checkers.add(mockServiceWrapper3);
				return checkers;
			}
		};
		WhiteboxImpl.setInternalState(enforceTwoFactorAuthenticationCheckerRegistry, "enforceTwoFactorAuthenticationCheckerMap", enforceTwoFactorAuthenticationCheckerMap);
	}

}
