/**
 * ScpInvokeResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class ScpInvokeResponse  extends com.capita_software_services.www.scp.base.ScpResponse  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.ScpInvokeResponseInvokeResult invokeResult;

    public ScpInvokeResponse() {
    }

    public ScpInvokeResponse(
           org.apache.axis.types.Token requestId,
           java.lang.String scpReference,
           com.capita_software_services.www.scp.base.TransactionState transactionState,
           com.capita_software_services.www.scp.base.ScpInvokeResponseInvokeResult invokeResult) {
        super(
            requestId,
            scpReference,
            transactionState);
        this.invokeResult = invokeResult;
    }


    /**
     * Gets the invokeResult value for this ScpInvokeResponse.
     *
     * @return invokeResult
     */
    public com.capita_software_services.www.scp.base.ScpInvokeResponseInvokeResult getInvokeResult() {
        return invokeResult;
    }


    /**
     * Sets the invokeResult value for this ScpInvokeResponse.
     *
     * @param invokeResult
     */
    public void setInvokeResult(com.capita_software_services.www.scp.base.ScpInvokeResponseInvokeResult invokeResult) {
        this.invokeResult = invokeResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScpInvokeResponse)) return false;
        ScpInvokeResponse other = (ScpInvokeResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.invokeResult==null && other.getInvokeResult()==null) ||
             (this.invokeResult!=null &&
              this.invokeResult.equals(other.getInvokeResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getInvokeResult() != null) {
            _hashCode += getInvokeResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScpInvokeResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "scpInvokeResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invokeResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "invokeResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", ">scpInvokeResponse>invokeResult"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }




}
