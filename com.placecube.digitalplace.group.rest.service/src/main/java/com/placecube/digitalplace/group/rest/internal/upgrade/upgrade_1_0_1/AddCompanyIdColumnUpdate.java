package com.placecube.digitalplace.group.rest.internal.upgrade.upgrade_1_0_1;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class AddCompanyIdColumnUpdate extends UpgradeProcess {

	public AddCompanyIdColumnUpdate() {
	}

	@Override
	protected void doUpgrade() throws Exception {
		if (!hasColumn("Placecube_GroupRest_GroupApiKey", "companyId")) {
			alterTableAddColumn("Placecube_GroupRest_GroupApiKey", "companyId", "LONG");

			runSQL("update Placecube_GroupRest_GroupApiKey a INNER JOIN Group_ b ON a.groupId = b.groupId set a.companyId = b.companyId");
		}
	}

}
