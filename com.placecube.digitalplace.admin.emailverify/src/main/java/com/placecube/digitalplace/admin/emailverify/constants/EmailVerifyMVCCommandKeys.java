package com.placecube.digitalplace.admin.emailverify.constants;

public final class EmailVerifyMVCCommandKeys {

	public static final String VERIFY = "/email/verify";

	private EmailVerifyMVCCommandKeys() {
	}

}
