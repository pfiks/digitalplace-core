package com.placecube.digitalplace.formadmin.web.portlet.command;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.model.FormDefinitionResult;
import com.placecube.digitalplace.formadmin.web.service.FormDefinitionListService;
import com.placecube.digitalplace.formadmin.web.service.SearchContainerService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.FORMS_MANAGER, "mvc.command.name=/", "mvc.command.name=/forms" }, service = MVCRenderCommand.class)
public class FormsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private FormDefinitionListService formDefinitionListService;

	@Reference
	private SearchContainerService searchContainerService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		List<FormDefinitionResult> formDefinitionResults = formDefinitionListService.getFormDefinitions(renderRequest);

		SearchContainer<FormDefinitionResult> searchContainer = searchContainerService.getSearchContainer(renderRequest, renderResponse, "no-form-definitions-were-found");
		searchContainer.setId("formsSearchContainer");
		searchContainer.setResultsAndTotal(formDefinitionResults);

		renderRequest.setAttribute(PortletRequestKeys.SEARCH_CONTAINER, searchContainer);

		return "/view.jsp";
	}

}
