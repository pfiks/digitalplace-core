package com.placecube.digitalplace.authentication.twofactor.checker;

import com.liferay.portal.kernel.model.User;

public interface EnforceTwoFactorAuthenticationChecker {

	String CHECKER_ID = "com.placecube.digitalplace.authentication.twofactor.checker";

	boolean checkIfTwoFactorAuthenticationShouldBeEnforced(User user);
}
