package com.placecube.digitalplace.ddl.internal.configuration.tracker;

import java.util.List;

import com.placecube.digitalplace.ddl.internal.configuration.DynamicDataListCompanyConfiguration;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;

public interface DynamicDataListCompanyConfigurationTracker {

	List<DynamicDataListMapping> getConfiguredDynamicDataListMappingsForCompany(long companyId);

	DynamicDataListCompanyConfiguration getDynamicDataListCompanyConfiguration(long companyId);

}
