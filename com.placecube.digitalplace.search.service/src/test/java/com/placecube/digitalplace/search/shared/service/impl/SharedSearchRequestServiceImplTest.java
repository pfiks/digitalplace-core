package com.placecube.digitalplace.search.shared.service.impl;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;
import com.placecube.digitalplace.search.internal.util.RequestUtil;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest(ParamUtil.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class SharedSearchRequestServiceImplTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletRequest mockOriginalHttpServletRequest;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private RequestUtil mockRequestUtil;

	@Mock
	private SharedSearchSession mockSharedSearchSession;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Portal mockPortal;

	@InjectMocks
	private SharedSearchRequestServiceImpl sharedSearchRequestServiceImpl;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void addSharedActiveFilter_WhenCalledWithPortletRequest_ThenAddsTheFilterInTheSharedSearchSession() {
		String filterName = "filterName";
		String filterValue = "filterValue";
		String filterLabel = "filterLabel";
		String filterColour = "filterColour";
		when(mockRequestUtil.getThemeDisplay(mockPortletRequest)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockOriginalHttpServletRequest);
		when(mockRequestUtil.getOrCreateSharedSearchSession(mockOriginalHttpServletRequest, mockThemeDisplay)).thenReturn(mockSharedSearchSession);

		sharedSearchRequestServiceImpl.addSharedActiveFilter(mockPortletRequest, filterName, filterValue, filterLabel, filterColour);

		InOrder inOrder = inOrder(mockSharedSearchSession, mockRequestUtil);
		inOrder.verify(mockSharedSearchSession, times(1)).addActiveFilter(filterName, filterValue, filterLabel, filterColour);
		inOrder.verify(mockRequestUtil, times(1)).updateSharedSearchSession(mockOriginalHttpServletRequest, mockSharedSearchSession);
	}

	@Test
	public void addSharedActiveFilter_WhenCalledWithThemeDisplay_ThenAddsTheFilterInTheSharedSearchSession() {
		String filterName = "filterName";
		String filterValue = "filterValue";
		String filterLabel = "filterLabel";
		String filterColour = "filterColour";
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockOriginalHttpServletRequest);
		when(mockRequestUtil.getOrCreateSharedSearchSession(mockOriginalHttpServletRequest, mockThemeDisplay)).thenReturn(mockSharedSearchSession);

		sharedSearchRequestServiceImpl.addSharedActiveFilter(mockThemeDisplay, filterName, filterValue, filterLabel, filterColour);

		InOrder inOrder = inOrder(mockSharedSearchSession, mockRequestUtil);
		inOrder.verify(mockSharedSearchSession, times(1)).addActiveFilter(filterName, filterValue, filterLabel, filterColour);
		inOrder.verify(mockRequestUtil, times(1)).updateSharedSearchSession(mockOriginalHttpServletRequest, mockSharedSearchSession);
	}

	@Test
	public void clearSharedSearchSession_ThenRemovesTheSharedSearchSession() {
		long plid = 11;
		when(mockRequestUtil.getThemeDisplay(mockPortletRequest)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPlid()).thenReturn(plid);
		when(mockRequestUtil.getOriginalHttpServletRequest(mockPortletRequest)).thenReturn(mockOriginalHttpServletRequest);

		sharedSearchRequestServiceImpl.clearSharedSearchSession(mockPortletRequest);

		verify(mockRequestUtil, times(1)).removeSharedSearchSession(mockOriginalHttpServletRequest, plid);
	}

	@Test
	public void removeSharedActiveFilterValuer_WhenNoError_ThenRemovedTheFilterValueInTheSharedSearchSession() {
		String filterName = "filterName";
		String filterValue = "filterValue";

		when(mockRequestUtil.getThemeDisplay(mockPortletRequest)).thenReturn(mockThemeDisplay);
		when(mockRequestUtil.getOriginalHttpServletRequest(mockPortletRequest)).thenReturn(mockOriginalHttpServletRequest);
		when(mockRequestUtil.getOrCreateSharedSearchSession(mockOriginalHttpServletRequest, mockThemeDisplay)).thenReturn(mockSharedSearchSession);

		sharedSearchRequestServiceImpl.removeSharedActiveFilterValue(mockPortletRequest, filterName, filterValue);

		InOrder inOrder = inOrder(mockSharedSearchSession, mockRequestUtil);
		inOrder.verify(mockSharedSearchSession, times(1)).removeActiveFilterValue(filterName, filterValue);
		inOrder.verify(mockRequestUtil, times(1)).updateSharedSearchSession(mockOriginalHttpServletRequest, mockSharedSearchSession);
	}

	@Test
	public void removeSharedSearchSessionAttribute_WhenNoError_ThenRemovesTheAttributeFromTheSharedSearchSession() {
		String attributeName = "attributeName";
		when(mockRequestUtil.getThemeDisplay(mockPortletRequest)).thenReturn(mockThemeDisplay);
		when(mockRequestUtil.getOriginalHttpServletRequest(mockPortletRequest)).thenReturn(mockOriginalHttpServletRequest);
		when(mockRequestUtil.getOrCreateSharedSearchSession(mockOriginalHttpServletRequest, mockThemeDisplay)).thenReturn(mockSharedSearchSession);

		sharedSearchRequestServiceImpl.removeSharedSearchSessionAttribute(mockPortletRequest, attributeName);

		InOrder inOrder = inOrder(mockSharedSearchSession, mockRequestUtil);
		inOrder.verify(mockSharedSearchSession, times(1)).removeAttribute(attributeName);
		inOrder.verify(mockRequestUtil, times(1)).updateSharedSearchSession(mockOriginalHttpServletRequest, mockSharedSearchSession);
	}

	@Test
	public void sendRedirectFromParameter_WhenRedirectValueIsFound_ThenSendsTheRedirect() throws IOException {
		String paramName = "paramName";
		String redirectValue = "redirectValue";
		when(ParamUtil.getString(mockActionRequest, paramName)).thenReturn(redirectValue);

		sharedSearchRequestServiceImpl.sendRedirectFromParameter(mockActionRequest, mockActionResponse, paramName);

		verify(mockActionResponse, times(1)).sendRedirect(redirectValue);
	}

	@Test(expected = IOException.class)
	public void sendRedirectFromParameter_WhenRedirectValueIsFoundAndExceptionExecutingTheRedirect_ThenThrowsIOException() throws IOException {
		String paramName = "paramName";
		String redirectValue = "redirectValue";
		when(ParamUtil.getString(mockActionRequest, paramName)).thenReturn(redirectValue);
		doThrow(new IOException()).when(mockActionResponse).sendRedirect(redirectValue);

		sharedSearchRequestServiceImpl.sendRedirectFromParameter(mockActionRequest, mockActionResponse, paramName);
	}

	@Test
	public void sendRedirectFromParameter_WhenRedirectValueIsNotFound_ThenNoRedirectIsPerformed() throws IOException {
		String paramName = "paramName";
		when(ParamUtil.getString(mockActionRequest, paramName)).thenReturn("");

		sharedSearchRequestServiceImpl.sendRedirectFromParameter(mockActionRequest, mockActionResponse, paramName);

		verifyZeroInteractions(mockActionResponse);
	}
}
