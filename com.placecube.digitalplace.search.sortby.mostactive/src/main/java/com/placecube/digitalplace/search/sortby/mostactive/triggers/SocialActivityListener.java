package com.placecube.digitalplace.search.sortby.mostactive.triggers;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.social.kernel.model.SocialActivity;
import com.placecube.digitalplace.search.sortby.mostactive.service.MostActiveService;

@Component(immediate = true, service = ModelListener.class)
public class SocialActivityListener extends BaseModelListener<SocialActivity> {

	private static final Log LOG = LogFactoryUtil.getLog(SocialActivityListener.class);

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private IndexerRegistry indexerRegistry;

	@Reference
	private MostActiveService mostActiveService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void onAfterCreate(SocialActivity socialActivity) {
		updateActivityCount(socialActivity);
	}

	@Override
	public void onAfterRemove(SocialActivity socialActivity) {
		updateActivityCount(socialActivity);
	}

	private void updateActivityCount(SocialActivity socialActivity) {
		try {
			long userId = socialActivity.getUserId();
			User user = userLocalService.getUser(userId);
			long activitiesCount = mostActiveService.getActivitiesCount(user);
			Indexer<User> indexer = indexerRegistry.getIndexer(User.class);
			if (Validator.isNotNull(indexer)) {
				mostActiveService.reindexSocialActivityCount(user, activitiesCount, indexer.isCommitImmediately());
			}
			LOG.debug("Updated most active counter for userId: " + userId);
		} catch (Exception e) {
			LOG.debug(e);
			LOG.warn("Unable to update most active counter for user " + e.getMessage());
		}

		try {
			long groupId = socialActivity.getGroupId();
			Group group = groupLocalService.getGroup(groupId);
			Indexer<Group> indexer = indexerRegistry.getIndexer(Group.class);
			if (!group.isOrganization() && Validator.isNotNull(indexer)) {
				long groupActivitiesCount = mostActiveService.getActivitiesCount(group);
				mostActiveService.reindexSocialActivityCount(group, groupActivitiesCount, indexer.isCommitImmediately());
				LOG.debug("Updated most active counter for groupId: " + groupId);
			}
		} catch (Exception e) {
			LOG.debug(e);
			LOG.warn("Unable to update most active counter for group " + e.getMessage());
		}
	}

}
