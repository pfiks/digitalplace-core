package com.placecube.digitalplace.frontend.taglib.servlet.taglib.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;

public class AssetsUtil {

	/**
	 * Gets a list of {@link AssetVocabulary} entries, which has at least one category assigned, based on vocabulary ids.
	 * @param vocabularyIds ids of vocabularies
	 * @return list of non empty vocabularies.
	 */
	public static List<AssetVocabulary> getNonEmptyVocabulariesByIds(long[] vocabularyIds) {
		List<AssetVocabulary> vocabularyList = new ArrayList<>();
		for (long vocabularyId : vocabularyIds) {
			AssetVocabulary assetVocabulary = AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(vocabularyId);
			if (Validator.isNotNull(assetVocabulary) && assetVocabulary.getCategoriesCount() > 0) {
				vocabularyList.add(assetVocabulary);
			}
		}
		return vocabularyList;
	}

	/**
	 * Gets group name of the vocabulary based on scope group.
	 * @param assetVocabulary vocabulary.
	 * @param themeDisplay themeDisplay.
	 * @return vocabulary name if group of the vocabulary is different than scopes group, blank string otherwise (or if exception occurred).
	 */
	public static String getVocabularyGroupName(AssetVocabulary assetVocabulary, ThemeDisplay themeDisplay) {
		String vocabularyGroupName = StringPool.BLANK;
		try {
			if (assetVocabulary.getGroupId() != themeDisplay.getSiteGroupId()) {
				Group vocabularyGroup = GroupLocalServiceUtil.getGroup(assetVocabulary.getGroupId());

				vocabularyGroupName = vocabularyGroup.getDescriptiveName(themeDisplay.getLocale());
			}
		} catch (PortalException e) {
			vocabularyGroupName = StringPool.BLANK;
		}
		return vocabularyGroupName;
	}

	/**
	 * Checks if Asset Vocabulary is required for a given asset type.
	 * @param assetVocabulary vocabulary.
	 * @param className class name.
	 * @param classTypePK primary key of class type.
	 * @return true if vocabulary is required for a given asset type, false otherwise.
	 */
	public static boolean isVocabularyRequired(AssetVocabulary assetVocabulary, String className, long classTypePK) {
		return assetVocabulary.isRequired(PortalUtil.getClassNameId(className), classTypePK);
	}

	/**
	 * Gets a list of selected items in a form of title - id (label - value) map.
	 * @param selectedCategoryIds selected category ids in a form of a string divided by the categoryIdsSeparator.
	 * @param selectedCategoryIdTitles selected category titles in a form of a string divided by the categoryTitlesSeparator.
	 * @param categoryIdsSeparator separator for the selectedCategoryIds.
	 * @param categoryTitlesSeparator separator for the selectedCategoryIdTitles.
	 * @return a list of selected items.
	 */
	public static List<HashMap<String, Object>> getSelectedItemsList(String selectedCategoryIds, String selectedCategoryIdTitles, String categoryIdsSeparator, String categoryTitlesSeparator) {
		List<HashMap<String, Object>> selectedItems = new ArrayList<>();
		if (!selectedCategoryIds.isEmpty()) {
			String[] categoryIds = selectedCategoryIds.split(categoryIdsSeparator);
			String[] categoryTitles = selectedCategoryIdTitles.split(categoryTitlesSeparator);

			for (int i = 0; i < categoryIds.length; i++) {
				HashMap<String, Object> category = new HashMap<>();

				selectedItems.add(category);

				category.put("label", categoryTitles[i]);
				category.put("value", categoryIds[i]);
			}
		}

		return selectedItems;
	}
}
