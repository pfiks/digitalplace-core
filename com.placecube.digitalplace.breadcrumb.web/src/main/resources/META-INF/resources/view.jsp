<%@ include file="/init.jsp" %>

<c:set var="breadcrumbEntries" value="${ layoutBreadcrumbEntries }" scope="page" />

<%@ include file="/breadcrumbs.jsp" %>

<c:forEach var="breadcrumbEntries" varStatus="loop" items="${ categoryBreadcrumbEntries }">
	<%@ include file="/breadcrumbs.jsp" %>
</c:forEach>
