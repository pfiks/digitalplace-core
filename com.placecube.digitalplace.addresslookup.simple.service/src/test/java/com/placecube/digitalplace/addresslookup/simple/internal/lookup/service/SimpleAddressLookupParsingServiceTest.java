package com.placecube.digitalplace.addresslookup.simple.internal.lookup.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SimpleAddressLookupParsingServiceTest extends PowerMockito {

	@Mock
	private SimpleAddressLookupEntry mockSimpleAddressLookupEntry1;

	@Mock
	private SimpleAddressLookupEntry mockSimpleAddressLookupEntry2;

	@InjectMocks
	private SimpleAddressLookupParsingService simpleAddressLookupParsingService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getAddressContext_WhenNoError_ThenReturnsTheAddressContextWithTheDetailsFromTheSimpleAddressLookupEntry() {
		when(mockSimpleAddressLookupEntry1.getUprn()).thenReturn("uprnvalue");
		when(mockSimpleAddressLookupEntry1.getAddressLine1()).thenReturn("addressline1value");
		when(mockSimpleAddressLookupEntry1.getAddressLine2()).thenReturn("addressline2value");
		when(mockSimpleAddressLookupEntry1.getAddressLine3()).thenReturn("addressline3value");
		when(mockSimpleAddressLookupEntry1.getCity()).thenReturn("cityvalue");
		when(mockSimpleAddressLookupEntry1.getPostcode()).thenReturn("postcodevalue");

		AddressContext result = simpleAddressLookupParsingService.getAddressContext(mockSimpleAddressLookupEntry1);

		assertThat(result.getUPRN(), equalTo("uprnvalue"));
		assertThat(result.getAddressLine1(), equalTo("addressline1value"));
		assertThat(result.getAddressLine2(), equalTo("addressline2value"));
		assertThat(result.getAddressLine3(), equalTo("addressline3value"));
		assertThat(result.getCity(), equalTo("cityvalue"));
		assertThat(result.getPostcode(), equalTo("postcodevalue"));
	}

	@Test
	public void getAddressContexts_WhenNoError_ThenReturnsTheAddressContextsWithTheDetailsFromTheSimpleAddressLookupEntries() {
		when(mockSimpleAddressLookupEntry1.getUprn()).thenReturn("uprnvalue1");
		when(mockSimpleAddressLookupEntry1.getAddressLine1()).thenReturn("addressline1value1");
		when(mockSimpleAddressLookupEntry1.getAddressLine2()).thenReturn("addressline2value1");
		when(mockSimpleAddressLookupEntry1.getAddressLine3()).thenReturn("addressline3value1");
		when(mockSimpleAddressLookupEntry1.getCity()).thenReturn("cityvalue1");
		when(mockSimpleAddressLookupEntry1.getPostcode()).thenReturn("postcodevalue1");

		when(mockSimpleAddressLookupEntry2.getUprn()).thenReturn("uprnvalue2");
		when(mockSimpleAddressLookupEntry2.getAddressLine1()).thenReturn("addressline1value2");
		when(mockSimpleAddressLookupEntry2.getAddressLine2()).thenReturn("addressline2value2");
		when(mockSimpleAddressLookupEntry2.getAddressLine3()).thenReturn("addressline3value2");
		when(mockSimpleAddressLookupEntry2.getCity()).thenReturn("cityvalue2");
		when(mockSimpleAddressLookupEntry2.getPostcode()).thenReturn("postcodevalue2");

		List<SimpleAddressLookupEntry> entries = new LinkedList<>();
		entries.add(mockSimpleAddressLookupEntry1);
		entries.add(mockSimpleAddressLookupEntry2);

		Set<AddressContext> results = simpleAddressLookupParsingService.getAddressContexts(entries);

		assertThat(results.size(), equalTo(2));

		Iterator<AddressContext> iterator = results.iterator();
		AddressContext firstResult = iterator.next();
		AddressContext secondResult = iterator.next();

		assertThat(firstResult.getUPRN(), equalTo("uprnvalue1"));
		assertThat(firstResult.getAddressLine1(), equalTo("addressline1value1"));
		assertThat(firstResult.getAddressLine2(), equalTo("addressline2value1"));
		assertThat(firstResult.getAddressLine3(), equalTo("addressline3value1"));
		assertThat(firstResult.getCity(), equalTo("cityvalue1"));
		assertThat(firstResult.getPostcode(), equalTo("postcodevalue1"));

		assertThat(secondResult.getUPRN(), equalTo("uprnvalue2"));
		assertThat(secondResult.getAddressLine1(), equalTo("addressline1value2"));
		assertThat(secondResult.getAddressLine2(), equalTo("addressline2value2"));
		assertThat(secondResult.getAddressLine3(), equalTo("addressline3value2"));
		assertThat(secondResult.getCity(), equalTo("cityvalue2"));
		assertThat(secondResult.getPostcode(), equalTo("postcodevalue2"));
	}
}
