package com.placecube.digitalplace.dataconsumer.utils;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.URLStringEncoder;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ URLEncoder.class })
public class DataConsumerWebClientUtilsTest extends PowerMockito {

	private DataConsumerWebClientUtils dataConsumerWebClientUtils;

	@Mock
	private HttpURLConnection mockHttpURLConnection;

	@Mock
	private OutputStream mockOutputStream;

	@Mock
	private URLEncoder mockURLEncoder;

	@Mock
	private URLStringEncoder mockURLStringEncoder;

	@Test
	public void appendBody_WhenJSONBodyIsNotNull_ThenSetsDoOutputWritesDataToConnectionOutputStreamAndFlushesOutputInOrder() throws Exception {
		final String jsonBody = "{jsonData}";

		when(mockHttpURLConnection.getOutputStream()).thenReturn(mockOutputStream);

		dataConsumerWebClientUtils.appendBody(mockHttpURLConnection, jsonBody);

		InOrder inOrder = inOrder(mockOutputStream, mockHttpURLConnection);

		inOrder.verify(mockHttpURLConnection, times(1)).setDoOutput(true);
		inOrder.verify(mockOutputStream, times(1)).write(jsonBody.getBytes());
		inOrder.verify(mockOutputStream, times(1)).flush();
	}

	@Test
	public void appendBody_WhenJSONBodyIsNull_ThenDoesNotWriteOutput() throws Exception {
		dataConsumerWebClientUtils.appendBody(mockHttpURLConnection, null);

		verify(mockHttpURLConnection, never()).setDoOutput(anyBoolean());
		verify(mockOutputStream, never()).write(any(byte[].class));
		verify(mockOutputStream, never()).flush();
	}

	@Test
	public void getEncodedParameter_WhenParameterIsNullOrBlank_ThenReturnBlankValue() throws Exception {

		String result = dataConsumerWebClientUtils.getEncodedParameter(StringPool.BLANK);

		assertEquals(StringPool.BLANK, result);
	}

	@Test
	public void getEncodedParameter_WhenParameterIsPresent_ThenReturnEncodedValue() throws Exception {
		String parameter = "Test parameter";
		String expectedResult = "Test+parameter";

		String result = dataConsumerWebClientUtils.getEncodedParameter(parameter);

		assertEquals(expectedResult, result);
	}

	@Test
	public void getEncodedParameter_WhenParameterIsPresentAndHasDashChar_ThenReturnEncodedValue() throws Exception {
		String parameter = "Test-parameter";
		String expectedResult = "Test-parameter";

		String result = dataConsumerWebClientUtils.getEncodedParameter(parameter);

		assertEquals(expectedResult, result);
	}

	@Test
	public void getParamString_WhenParametersMapIsEmpty_ThenReturnsEmptyString() {
		String result = dataConsumerWebClientUtils.getParamString(new HashMap<>());

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getParamString_WhenParametersMapIsNotEmpty_ThenReturnsUrlParametersString() {
		Map<String, String> parameterMap = new HashMap<>();
		parameterMap.put("param1", "value1");
		parameterMap.put("param2", "value2");

		String result = dataConsumerWebClientUtils.getParamString(parameterMap);

		assertThat(result, equalTo("?param1=value1&param2=value2"));
	}

	@Test
	@Parameters({ "POST", "PUT", "PATCH" })
	public void isHttpMethodRequiresBody_WhenHttpMethodIsPostPutOrPatch_ThenReturnsTrue(String httpMethod) throws Exception {

		boolean result = dataConsumerWebClientUtils.isHttpMethodRequiresBody(httpMethod);

		assertTrue(result);
	}

	@Test
	public void isHttpMethodRequiresBody_WhenHttpMethodNotPostPutOrPatch_ThenReturnsFalse() throws Exception {

		boolean result = dataConsumerWebClientUtils.isHttpMethodRequiresBody("GET");

		assertFalse(result);
	}

	@Test
	public void openConnection_WhenUrlDoesContainProtocol_ThenOpensConnection() throws Exception {
		final String url = "https://www.google.com";
		HttpURLConnection result = dataConsumerWebClientUtils.openConnection(url);

		assertThat(result.getURL().toString(), equalTo(url));
	}

	@Test
	public void openConnection_WhenUrlDoesNotContainProtocol_ThenOpensConnectionWithHttpProtocol() throws Exception {
		final String url = "www.google.com";
		HttpURLConnection result = dataConsumerWebClientUtils.openConnection(url);

		assertThat(result.getURL().toString(), equalTo("http://" + url));
	}

	@Test(expected = IOException.class)
	public void readConnectionOutput_WhenErrorResponse_ThenThrowIoException() throws IOException {
		String response = " Error response returned from rest service ";

		when(mockHttpURLConnection.getResponseCode()).thenReturn(HttpURLConnection.HTTP_INTERNAL_ERROR);
		when(mockHttpURLConnection.getErrorStream()).thenReturn(new ByteArrayInputStream(response.getBytes()));

		dataConsumerWebClientUtils.readConnectionOutput(mockHttpURLConnection);
	}

	@Test
	public void readConnectionOutput_WhenOkResponseAndNoErrors_ThenReturnsConcatenatedTrimmedInputLines() throws IOException {
		String response = " This is a test \n And a new line ";

		when(mockHttpURLConnection.getResponseCode()).thenReturn(HttpURLConnection.HTTP_OK);
		when(mockHttpURLConnection.getInputStream()).thenReturn(new ByteArrayInputStream(response.getBytes()));

		String result = dataConsumerWebClientUtils.readConnectionOutput(mockHttpURLConnection);

		assertThat(result, equalTo("This is a test And a new line"));
	}

	@Before
	public void setUp() {
		mockStatic(URLEncoder.class);
		dataConsumerWebClientUtils = new DataConsumerWebClientUtils();
	}

}
