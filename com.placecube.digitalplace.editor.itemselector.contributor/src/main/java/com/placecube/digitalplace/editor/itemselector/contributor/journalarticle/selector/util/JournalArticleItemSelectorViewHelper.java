package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector.util;

import java.util.Date;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.journal.constants.JournalArticleConstants;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.dao.search.ResultRow;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portlet.url.builder.PortletURLBuilder;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HashMapBuilder;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.servlet.taglib.clay.JournalArticleItemSelectorVerticalCard;

@Component(service = JournalArticleItemSelectorViewHelper.class)
public class JournalArticleItemSelectorViewHelper {

	public void addResultRowClass(ResultRow row, String cssClass) {
		row.setCssClass(cssClass + " " + row.getCssClass());
	}

	public PortletURL buildFolderRowUrl(PortletURL portletURL, JournalFolder journalFolder) {
		return PortletURLBuilder.create(portletURL).setParameter("folderId", journalFolder.getFolderId()).setParameter("groupId", journalFolder.getGroupId()).buildPortletURL();
	}

	public String getArticleAuthorName(JournalArticle journalArticle) {
		return PortalUtil.getUserName(journalArticle);
	}

	public String getFolderAuthorName(JournalFolder journalFolder) {
		return PortalUtil.getUserName(journalFolder);
	}

	public JournalArticleItemSelectorVerticalCard getJournalArticleItemSelectorVerticalCard(JournalArticle journalArticle, RenderRequest renderRequest) {
		return new JournalArticleItemSelectorVerticalCard(journalArticle, renderRequest);
	}

	public String getTimeDescription(HttpServletRequest request, Date date) {
		return LanguageUtil.getTimeDescription(request, System.currentTimeMillis() - date.getTime(), true);
	}

	public boolean isDescriptiveView(String displayStyle) {
		return "descriptive".equals(displayStyle);
	}

	public boolean isIconView(String displayStyle) {
		return "icon".equals(displayStyle);
	}

	public boolean isRowObjectInstanceOfJournalFolder(ResultRow row) {
		return row.getObject() instanceof JournalFolder;
	}

	public void setJournalArticleRowData(ResultRow row, JournalArticle journalArticle, ThemeDisplay themeDisplay) throws PortalException {
		String articleFriendlyUrl = JournalArticleConstants.CANONICAL_URL_SEPARATOR + journalArticle.getUrlTitle(themeDisplay.getLocale());

		row.setData(HashMapBuilder.<String, Object>put("value", articleFriendlyUrl).build());
	}

	public String shortenText(String text, int length) {
		return StringUtil.shorten(text, length);
	}

	public String[] toArray(String... arguments) {
		return arguments;
	}

}
