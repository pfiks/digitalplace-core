package com.capita_software_services.www.scp.simple;

public class ScpProxy implements com.capita_software_services.www.scp.simple.Scp {
  private String _endpoint = null;
  private com.capita_software_services.www.scp.simple.Scp scp = null;

  public ScpProxy() {
    _initScpProxy();
  }

  public ScpProxy(String endpoint) {
    _endpoint = endpoint;
    _initScpProxy();
  }

  private void _initScpProxy() {
    try {
      scp = (new com.capita_software_services.www.scp.simple.ScpServiceLocator()).getscpSoap11();
      if (scp != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)scp)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)scp)._getProperty("javax.xml.rpc.service.endpoint.address");
      }

    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }

  public String getEndpoint() {
    return _endpoint;
  }

  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (scp != null)
      ((javax.xml.rpc.Stub)scp)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

  }

  public com.capita_software_services.www.scp.simple.Scp getScp() {
    if (scp == null)
      _initScpProxy();
    return scp;
  }

  public com.capita_software_services.www.scp.simple.ScpSimpleQueryResponse scpSimpleQuery(com.capita_software_services.www.scp.base.ScpQueryRequest scpSimpleQueryRequest) throws java.rmi.RemoteException{
    if (scp == null)
      _initScpProxy();
    return scp.scpSimpleQuery(scpSimpleQueryRequest);
  }

  public com.capita_software_services.www.scp.base.ScpInvokeResponse scpSimpleInvoke(com.capita_software_services.www.scp.simple.ScpSimpleInvokeRequest scpSimpleInvokeRequest) throws java.rmi.RemoteException{
    if (scp == null)
      _initScpProxy();
    return scp.scpSimpleInvoke(scpSimpleInvokeRequest);
  }

  public com.capita_software_services.www.scp.base.ScpVersionResponse scpVersion(com.capita_software_services.www.scp.base.ScpVersionRequest scpVersionRequest) throws java.rmi.RemoteException{
    if (scp == null)
      _initScpProxy();
    return scp.scpVersion(scpVersionRequest);
  }


}