package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AccountSecurityMVCRenderCommandTest {

	@InjectMocks
	private AccountSecurityMVCRenderCommand accountSecurityMVCRenderCommand;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Captor
	private ArgumentCaptor<Map<String, String>> stringMapCaptor;

	@Test
	public void render_WhenNoError_ThenReturnsAccountSecurityPage() throws PortletException {
		String result = accountSecurityMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/account-security.jsp"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoError_ThenSetsCaptchaEnabledRequestAttribute(boolean captchaEnabled) throws UserAccountException, PortletException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContextService.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockRenderRequest)).thenReturn(captchaEnabled);

		accountSecurityMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.CAPTCHA_ENABLED, captchaEnabled);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoError_ThenShowCaptchaOnAccountSecurityStepRequestAttribute(boolean showCaptchaOnAccountSecurityStep) throws UserAccountException, PortletException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContextService.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockRenderRequest)).thenReturn(showCaptchaOnAccountSecurityStep);

		accountSecurityMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.CAPTCHA_ENABLED, showCaptchaOnAccountSecurityStep);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoError_ThenUpdateContextInSession(boolean showCaptchaOnAccountSecurityStep) throws UserAccountException, PortletException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContextService.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockRenderRequest)).thenReturn(showCaptchaOnAccountSecurityStep);

		accountSecurityMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockRenderRequest);
	}

	@Test
	public void render_WhenNoError_ThenSetsOrderedTwoFactorAuthenticationOptionsAttributeInRequest() throws PortletException {

		accountSecurityMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(eq(RequestKeys.TWO_FACTOR_AUTHENTICATION_ENABLED_OPTIONS), stringMapCaptor.capture());

		Map<String, String> twoFactorAuthenticationEnabledOptions = stringMapCaptor.getValue();

		assertThat(twoFactorAuthenticationEnabledOptions.size(), equalTo(2));

		String[] values = twoFactorAuthenticationEnabledOptions.values().toArray(new String[twoFactorAuthenticationEnabledOptions.size()]);

		assertThat(values[0], equalTo(AccountConstants.I_WANT_TO_SETUP_2FA_LABEL));
		assertThat(values[1], equalTo(AccountConstants.I_DONT_WANT_TO_SETUP_2FA_LABEL));

		assertThat(twoFactorAuthenticationEnabledOptions.get(Boolean.TRUE.toString()), equalTo(AccountConstants.I_WANT_TO_SETUP_2FA_LABEL));
		assertThat(twoFactorAuthenticationEnabledOptions.get(Boolean.FALSE.toString()), equalTo(AccountConstants.I_DONT_WANT_TO_SETUP_2FA_LABEL));
	}
}
