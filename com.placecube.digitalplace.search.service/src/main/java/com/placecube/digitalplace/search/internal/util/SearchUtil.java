package com.placecube.digitalplace.search.internal.util;

import java.util.List;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcher;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcherManager;
import com.liferay.portal.kernel.search.facet.util.FacetFactory;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.search.internal.service.SearchConfigurationService;
import com.placecube.digitalplace.search.shared.constants.SharedSearchAttributeKeys;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;

@Component(immediate = true, service = SearchUtil.class)
public class SearchUtil {

	@Reference
	private FacetedSearcherManager facetedSearcherManager;

	@Reference
	private FacetFactory facetFactory;

	@Reference
	private SearchConfigurationService searchConfigurationService;

	@Reference
	private SearchURLUtil searchURLUtil;

	public BooleanQuery createBooleanQuery() {
		return new BooleanQueryImpl();
	}

	public Facet createFacet(String name, SearchContext searchContext) {
		Facet facet = facetFactory.newInstance(searchContext);
		facet.setFieldName(name);
		return facet;
	}

	public Hits executeSearch(SearchContext searchContext, SearchContainer<Object> searchContainer) throws SearchException {
		searchContext.setStart(searchContainer.getStart());
		searchContext.setEnd(searchContainer.getEnd());

		FacetedSearcher facetedSearcher = facetedSearcherManager.createFacetedSearcher();
		return facetedSearcher.search(searchContext);
	}

	public SearchContainer<Object> getBaseSearchContainer(RenderRequest renderRequest, HttpServletRequest originalServletRequest, SharedSearchContributorSettings sharedSearchContributorSettings) {

		int delta = ParamUtil.getInteger(originalServletRequest, SearchContainer.DEFAULT_DELTA_PARAM, 0);
		if (delta == 0) {
			String deltaSetting = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SharedSearchAttributeKeys.SEARCH_CONTAINER_DELTA)
					.orElse(String.valueOf(SearchContainer.DEFAULT_DELTA));
			delta = GetterUtil.getInteger(deltaSetting);
		}

		int cur = ParamUtil.getInteger(originalServletRequest, SearchContainer.DEFAULT_CUR_PARAM, 0);

		DisplayTerms displayTerms = null;
		DisplayTerms searchTerms = null;
		List<String> headerNames = null;
		String emptyResultsMessage = null;
		String urlString = searchURLUtil.getCompleteOriginalURL(originalServletRequest);
		PortletURL iteratorURL = searchURLUtil.getNullPortletURL(urlString);

		return SearchContainerFactory.createSearchContainer(renderRequest, displayTerms, searchTerms, SearchContainer.DEFAULT_CUR_PARAM, cur, delta, iteratorURL, headerNames, emptyResultsMessage);
	}

	public SearchContext getBaseSearchContext(ThemeDisplay themeDisplay) {
		SearchContext searchContext = new SearchContext();

		searchContext.setCompanyId(themeDisplay.getCompanyId());
		searchContext.setLayout(themeDisplay.getLayout());
		searchContext.setLocale(themeDisplay.getLocale());
		searchContext.setTimeZone(themeDisplay.getTimeZone());
		searchContext.setUserId(themeDisplay.getUserId());

		return searchContext;
	}

	public int getFacetMultiplier(long companyId) {
		int facetMultiplier = searchConfigurationService.getConfiguredFacetMultiplier(companyId);

		return facetMultiplier < 1 ? 1 : facetMultiplier;
	}
}
