package com.placecube.digitalplace.formadmin.web.panelapp;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.panelapp.FormsManagerPanelApp;

public class FormsManagerPanelAppTest {

	private FormsManagerPanelApp formsManagerPanelApp;

	@Before
	public void setUp() {
		formsManagerPanelApp = new FormsManagerPanelApp();
	}

	@Test
	public void getPortletId_ThenReturnsThePortletId() {
		String result = formsManagerPanelApp.getPortletId();

		assertThat(result, equalTo(PortletKeys.FORMS_MANAGER));
	}

}
