package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.common.portlet.PortletURLService;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration.DDLFieldFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.constants.DDLFieldFacetDisplayConstants;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetDDMFormFieldService;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetParserService;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetRequestService;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-ddl-search-fieldfacet", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=dp-search-facet-category dp-search-facet dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.header-portlet-javascript=/ddl-search-field-facet/js/field-facet.js", //
		"com.liferay.portlet.instanceable=true", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/ddl-search-field-facet/view.jsp", //
		"javax.portlet.init-param.config-template=/ddl-search-field-facet/configuration.jsp", //
		"javax.portlet.name=" + PortletKeys.DDL_FIELD_FACET_PORTLET, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class DDLFieldFacetPortlet extends MVCPortlet {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DDLFieldFacetDDMFormFieldService ddlFieldFacetDDMFormFieldService;

	@Reference
	private DDLFieldFacetParserService ddlFieldFacetParserService;

	@Reference
	private DDLFieldFacetRequestService ddlFieldFacetRequestService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private Portal portal;

	@Reference
	private PortletURLService portletURLService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			DDLFieldFacetPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(DDLFieldFacetPortletInstanceConfiguration.class, themeDisplay);

			if (configuration.invisible()) {
				renderRequest.setAttribute("invisible", true);
				renderRequest.setAttribute("isEditLayoutMode", portletURLService.isEditLayoutMode(renderRequest));

				super.render(renderRequest, renderResponse);
				return;
			}

			long ddmStructureId = configuration.ddmStructureId();
			String ddmFieldName = configuration.ddmFieldName();
			DDMStructure ddmStructure = ddmStructureLocalService.fetchDDMStructure(ddmStructureId);

			if (ddmStructure != null && ddmFieldName != null && !ddmFieldName.equals(StringPool.BLANK)) {
				SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

				final String displayStyle = configuration.displayStyle();
				String filterColour = configuration.filterColour();
				String indexedDDMFieldName = ddlFieldFacetParserService.getIndexedFieldName(ddmStructure, ddmFieldName);
				boolean isDropDownDisplayStyle = DDLFieldFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN.equals(displayStyle);

				String filterTitle = ddmStructure.getFieldLabel(ddmFieldName, renderRequest.getLocale());

				DDMFormField ddmFormField = ddmStructure.getDDMFormField(ddmFieldName);

				Set<String> selectedValues = searchResponse.getActiveValuesForFilter(indexedDDMFieldName);
				Map<String, Integer> facetValues = searchResponse.getFacetValues(indexedDDMFieldName);

				List<FacetResult> availableFieldValues = null;

				if (ddlFieldFacetDDMFormFieldService.fieldTypeAllowsOptions(ddmFormField)) {
					availableFieldValues = getAvailableFieldFacetResultsFromFieldOptions(facetValues, selectedValues, ddmFormField, isDropDownDisplayStyle);
				} else {
					availableFieldValues = getAvailableFieldFacetResultsFromFacetValues(facetValues, selectedValues, configuration, isDropDownDisplayStyle, renderRequest);
				}

				sortValues(availableFieldValues, renderRequest.getLocale(), configuration.alphaSort());

				if (isDropDownDisplayStyle) {
					addFirstSelectedValueToRequest(availableFieldValues, renderRequest);

					renderRequest.setAttribute("showHeadingAsFirstDropdownOption", configuration.showHeadingAsFirstDropdownOption());
				}

				PortletPreferences portletPreferences = renderRequest.getPreferences();
				boolean isFieldNameInUrl = ddlFieldFacetRequestService.isConfiguredFieldNameValueInUrl(themeDisplay, portletPreferences);

				renderRequest.setAttribute("availableFieldValues", availableFieldValues);
				renderRequest.setAttribute("customHeading", configuration.customHeading());
				renderRequest.setAttribute("displayStyle", displayStyle);
				renderRequest.setAttribute("facetFieldName", indexedDDMFieldName);
				renderRequest.setAttribute("filterTitle", filterTitle);
				renderRequest.setAttribute("filterColour", filterColour);

				renderRequest.setAttribute("redirectURL", isFieldNameInUrl ? ddlFieldFacetRequestService.getEscapedRedirectUrlWithoutFieldUrlParameter(searchResponse, portletPreferences)
						: searchResponse.getEscapedSearchResultsIteratorURL());
			} else {
				renderRequest.setAttribute("invalidConfiguration", true);
				renderRequest.setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
			}
			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

	private void addFirstSelectedValueToRequest(List<FacetResult> availableFieldValues, RenderRequest renderRequest) {
		Optional<FacetResult> firstSelected = availableFieldValues.stream().filter(FacetResult::isSelected).findFirst();

		if (firstSelected.isPresent()) {
			renderRequest.setAttribute("selectedFacetValue", firstSelected.get().getValue());
		}
	}

	private List<FacetResult> getAvailableFieldFacetResultsFromFacetValues(Map<String, Integer> facetValues, Set<String> selectedValues, DDLFieldFacetPortletInstanceConfiguration configuration,
			boolean showAll, RenderRequest renderRequest) {

		if (showAll) {
			return ddlFieldFacetParserService.parseAllFacetValues(facetValues, selectedValues, configuration.isFieldNameDDLRecordLink(), configuration.ddlRecordLinkFieldName(),
					configuration.ddmLinkStructureId(), portal.getHttpServletRequest(renderRequest));
		}
		return ddlFieldFacetParserService.parseFacetsWithFacetValues(facetValues, selectedValues, configuration.isFieldNameDDLRecordLink(), configuration.ddlRecordLinkFieldName(),
				renderRequest.getLocale());
	}

	private List<FacetResult> getAvailableFieldFacetResultsFromFieldOptions(Map<String, Integer> facetValues, Set<String> selectedValues, DDMFormField ddmFormField, boolean showAll) {
		DDMFormFieldOptions ddmFormFieldOptions = ddmFormField.getDDMFormFieldOptions();

		if (showAll) {
			return ddlFieldFacetParserService.parseAllFieldOptions(facetValues, selectedValues, ddmFormFieldOptions);
		}
		return ddlFieldFacetParserService.parseFacetsWithFieldOptions(facetValues, selectedValues, ddmFormFieldOptions);
	}

	private void sortValues(List<FacetResult> fieldValues, Locale locale, boolean alphaSort) {
		if (alphaSort) {
			fieldValues.sort(Comparator.comparing(fr -> fr.getLabel(locale)));
		}
	}

}
