package com.placecube.digitalplace.ddl.internal.util;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.lists.constants.DDLConstants;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalService;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.comment.CommentManager;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.template.TemplateHandler;
import com.liferay.portal.kernel.template.TemplateHandlerRegistryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyFactory;
import com.liferay.registry.Filter;
import com.liferay.registry.Registry;
import com.liferay.registry.RegistryUtil;
import com.liferay.registry.ServiceTracker;
import com.liferay.registry.ServiceTrackerCustomizer;
import com.placecube.digitalplace.ddl.internal.constants.TemplateKeys;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RegistryUtil.class, ProxyFactory.class, ParamUtil.class, PropsUtil.class, TemplateHandlerRegistryUtil.class })
@SuppressStaticInitializationFor({"com.liferay.portal.kernel.template.TemplateHandlerRegistryUtil"})
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class TransformContextServiceTest {

	private final static long CLASS_NAME_ID = 4L;
	private static final Locale LOCALE = Locale.UK;
	private static final String RECORD_SET_DESCRIPTION = "AnyRecordSetDescription";
	private final static long RECORD_SET_ID = 1L;
	private static final String RECORD_SET_NAME = "AnyRecordSetName";
	private final static long STRUCTURE_ID = 2L;
	private final static long TEMPLATE_ID = 3L;
	private static final String TEMPLATE_LANGUAGE = "AnyTemplateLanguage";

	@Mock
	private CommentManager mockCommentManager;

	@Mock
	private Map<String, Object> mockContextObjects;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private DDLRecordSetLocalService mockDDLRecordSetLocalService;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private Filter mockFilter;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Portal mockPortal;

	@Mock
	private Registry mockRegistry;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ServiceTracker<?, ?> mockServiceTracker;

	@Mock
	private TemplateHandler mockTemplateHandler;

	@Mock
	private TemplateUtil mockTemplateUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private TransformContextService transformContextService;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, RegistryUtil.class, ProxyFactory.class, TemplateHandlerRegistryUtil.class);

		when(RegistryUtil.getRegistry()).thenReturn(mockRegistry);
		when(mockRegistry.getFilter(anyString())).thenReturn(mockFilter);
		when(mockRegistry.trackServices(any(Filter.class), any(ServiceTrackerCustomizer.class))).thenReturn(mockServiceTracker);
	}

	@Test
	public void getContextObjects_WhenErrorOnGettingRecordSet_TheDoesNotPutAnyReservedRecordSetValuesIntoContextObjects() throws PortalException {
		mockParams(true);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects, never()).put(eq(DDLConstants.RESERVED_RECORD_SET_DESCRIPTION), any());
		verify(mockContextObjects, never()).put(eq(DDLConstants.RESERVED_RECORD_SET_ID), any());
		verify(mockContextObjects, never()).put(eq(DDLConstants.RESERVED_RECORD_SET_NAME), any());
	}

	@Test
	public void getContextObjects_WhenNoError_ThenPutsReservedRecordSetDescriptionIntoContextObjects() throws PortalException {
		mockParams(false);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put(DDLConstants.RESERVED_RECORD_SET_DESCRIPTION, RECORD_SET_DESCRIPTION);
	}

	@Test
	public void getContextObjects_WhenNoError_ThenPutsReservedRecordSetIdIntoContextObjects() throws PortalException {
		mockParams(false);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put(DDLConstants.RESERVED_RECORD_SET_ID, RECORD_SET_ID);
	}

	@Test
	public void getContextObjects_WhenNoError_ThenPutsReservedRecordSetNameIntoContextObjects() throws PortalException {
		mockParams(false);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put(DDLConstants.RESERVED_RECORD_SET_NAME, RECORD_SET_NAME);
	}

	@Test
	public void getContextObjects_WhenNoError_TheReturnsContextObjects() throws PortalException {
		mockParams(false);

		Map<String, Object> result = transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		assertThat(result, sameInstance(mockContextObjects));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsClassNameIdIntoContextObjects(boolean throwPortalException) throws PortalException {
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put(TemplateConstants.CLASS_NAME_ID, CLASS_NAME_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsCommentManagerIntoContextObjects(boolean throwPortalException) throws PortalException {
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put("commentManager", mockCommentManager);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsReservedDDLRecordIntoContextObjects(boolean throwPortalException) throws PortalException {
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put(TemplateKeys.CUR_RECORD, mockDDLRecord);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsReservedDDMStructureIdIntoContextObjects(boolean throwPortalException) throws PortalException {
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put(DDLConstants.RESERVED_DDM_STRUCTURE_ID, STRUCTURE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsReservedDDMTeamplteIdIntoContextObjects(boolean throwPortalException) throws PortalException {
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put(DDLConstants.RESERVED_DDM_TEMPLATE_ID, TEMPLATE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsTemplateIdIntoContextObjects(boolean throwPortalException) throws PortalException {
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put(TemplateConstants.TEMPLATE_ID, TEMPLATE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WhenNoErrorOrErrorOnGettingRecordSetAndViewModeIsInTheRequest_ThenPutsTheRequestViewModeIntoContextObjects(boolean throwPortalException) throws PortalException {
		mockParams(throwPortalException);
		String requestViewMode = "AnyRequestViewMode";
		when(ParamUtil.getString(mockHttpServletRequest, "viewMode", Constants.VIEW)).thenReturn(requestViewMode);

		transformContextService.getContextObjects(mockDDMTemplate, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		verify(mockContextObjects).put("viewMode", requestViewMode);
	}

	@Test
	public void getContextObjects_WhenNullTemplatePassed_ThenUnmodifiedContextObjectsMapIsReturned() {

		Map<String, Object> contextObjects = Collections.emptyMap();
		when(mockTemplateUtil.getContextObjectsInstance()).thenReturn(contextObjects);

		Map<String, Object> result = transformContextService.getContextObjects(null, mockDDLRecord, STRUCTURE_ID, mockThemeDisplay, mockHttpServletRequest);

		assertThat(result, sameInstance(contextObjects));
		assertTrue(result.isEmpty());
	}

	@Test
	public void getContextObjects_WithAssetEntriesParameter_WhenErrorOnGettingRecordSet_TheDoesNotPutAnyReservedRecordSetValuesIntoContextObjects() throws PortalException {
		List<AssetEntry> assetEntries = new ArrayList<>();
		mockParams(true);

		transformContextService.getContextObjects(mockDDMTemplate, assetEntries, STRUCTURE_ID, mockRenderRequest);

		verify(mockContextObjects, never()).put(eq(DDLConstants.RESERVED_RECORD_SET_DESCRIPTION), any());
		verify(mockContextObjects, never()).put(eq(DDLConstants.RESERVED_RECORD_SET_ID), any());
		verify(mockContextObjects, never()).put(eq(DDLConstants.RESERVED_RECORD_SET_NAME), any());
	}

	@Test
	public void getContextObjects_WithAssetEntriesParameter_WhenNoError_TheReturnsContextObjects() throws PortalException {
		List<AssetEntry> assetEntries = new ArrayList<>();
		mockParams(false);

		Map<String, Object> result = transformContextService.getContextObjects(mockDDMTemplate, assetEntries, STRUCTURE_ID, mockRenderRequest);

		assertThat(result, sameInstance(mockContextObjects));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WithAssetEntriesParameter_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsClassNameIdIntoContextObjects(boolean throwPortalException) throws PortalException {
		List<AssetEntry> assetEntries = new ArrayList<>();
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, assetEntries, STRUCTURE_ID, mockRenderRequest);

		verify(mockContextObjects).put(TemplateConstants.CLASS_NAME_ID, CLASS_NAME_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WithAssetEntriesParameter_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsCommentManagerIntoContextObjects(boolean throwPortalException) throws PortalException {
		List<AssetEntry> assetEntries = new ArrayList<>();
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, assetEntries, STRUCTURE_ID, mockRenderRequest);

		verify(mockContextObjects).put("commentManager", mockCommentManager);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WithAssetEntriesParameter_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsReservedDDMStructureIdIntoContextObjects(boolean throwPortalException) throws PortalException {
		List<AssetEntry> assetEntries = new ArrayList<>();
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, assetEntries, STRUCTURE_ID, mockRenderRequest);

		verify(mockContextObjects).put(DDLConstants.RESERVED_DDM_STRUCTURE_ID, STRUCTURE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WithAssetEntriesParameter_WhenNoErrorOrErrorOnGettingRecordSet_ThenPutsTemplateIdIntoContextObjects(boolean throwPortalException) throws PortalException {
		List<AssetEntry> assetEntries = new ArrayList<>();
		mockParams(throwPortalException);

		transformContextService.getContextObjects(mockDDMTemplate, assetEntries, STRUCTURE_ID, mockRenderRequest);

		verify(mockContextObjects).put(TemplateConstants.TEMPLATE_ID, TEMPLATE_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContextObjects_WithAssetEntriesParameter_WhenNoErrorOrErrorOnGettingRecordSetAndViewModeIsInTheRequest_ThenPutsTheRequestViewModeIntoContextObjects(boolean throwPortalException)
			throws PortalException {
		List<AssetEntry> assetEntries = new ArrayList<>();
		mockParams(throwPortalException);
		String requestViewMode = "AnyRequestViewMode";
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "viewMode", Constants.VIEW)).thenReturn(requestViewMode);

		transformContextService.getContextObjects(mockDDMTemplate, assetEntries, STRUCTURE_ID, mockRenderRequest);

		verify(mockContextObjects).put("viewMode", requestViewMode);
	}

	@Test
	public void getContextObjects_WithAssetEntriesParameter_WhenNullTemplatePassed_ThenUnmodifiedContextObjectsMapIsReturned() {
		List<AssetEntry> assetEntries = new ArrayList<>();
		Map<String, Object> contextObjects = Collections.emptyMap();
		when(mockTemplateUtil.getContextObjectsInstance()).thenReturn(contextObjects);

		Map<String, Object> result = transformContextService.getContextObjects(null, assetEntries, STRUCTURE_ID, mockRenderRequest);

		assertThat(result, sameInstance(contextObjects));
		assertTrue(result.isEmpty());
	}

	@Test
	public void getContextObjects_WithAssetEntriesParameter_WhenTemplatePassed_ThenSetAssetEntriesInContextObjectsMap() throws PortalException {
		List<AssetEntry> assetEntries = new ArrayList<>();
		mockParams(false);
		when(mockTemplateUtil.getContextObjectsInstance()).thenReturn(mockContextObjects);

		transformContextService.getContextObjects(mockDDMTemplate, assetEntries, STRUCTURE_ID, mockRenderRequest);

		verify(mockContextObjects, times(1)).put(TemplateKeys.ENTRIES, assetEntries);
	}

	@Test
	public void getContextObjects_WithAssetEntriesParameter_WhenTemplatePassed_ThenSetRenderRequestInContextObjectsMap() throws PortalException {
		List<AssetEntry> assetEntries = new ArrayList<>();
		mockParams(false);
		when(mockTemplateUtil.getContextObjectsInstance()).thenReturn(mockContextObjects);

		transformContextService.getContextObjects(mockDDMTemplate, assetEntries, STRUCTURE_ID, mockRenderRequest);

		verify(mockContextObjects, times(1)).put("renderRequest", mockRenderRequest);
	}

	private void mockParams(boolean throwPortalException) throws PortalException {
		when(mockTemplateUtil.getContextObjectsInstance()).thenReturn(mockContextObjects);
		when(mockDDLRecord.getRecordSetId()).thenReturn(RECORD_SET_ID);
		if (throwPortalException) {
			when(mockDDLRecordSetLocalService.getRecordSet(RECORD_SET_ID)).thenThrow(new PortalException());
		} else {
			when(mockDDLRecordSetLocalService.getRecordSet(RECORD_SET_ID)).thenReturn(mockDDLRecordSet);
			when(mockThemeDisplay.getLocale()).thenReturn(LOCALE);
			when(mockDDLRecordSet.getDescription(LOCALE)).thenReturn(RECORD_SET_DESCRIPTION);
			when(mockDDLRecordSet.getName(LOCALE)).thenReturn(RECORD_SET_NAME);
		}
		when(mockDDMTemplate.getTemplateId()).thenReturn(TEMPLATE_ID);
		when(mockDDMTemplate.getClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockDDMTemplate.getLanguage()).thenReturn(TEMPLATE_LANGUAGE);
		when(mockThemeDisplay.getResponse()).thenReturn(mockHttpServletResponse);
		when(TemplateHandlerRegistryUtil.getTemplateHandler(DDLRecordSet.class.getName())).thenReturn(mockTemplateHandler);
	}

}
