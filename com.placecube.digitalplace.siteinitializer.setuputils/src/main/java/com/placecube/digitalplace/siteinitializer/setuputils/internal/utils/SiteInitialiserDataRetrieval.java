package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.list.model.AssetListEntryUsage;
import com.liferay.asset.list.model.AssetListEntryUsageTable;
import com.liferay.asset.list.service.AssetListEntryUsageLocalService;
import com.liferay.info.collection.provider.InfoCollectionProvider;
import com.liferay.layout.util.structure.CollectionStyledLayoutStructureItem;
import com.liferay.petra.sql.dsl.DSLQueryFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.util.Portal;

@Component(immediate = true, service = SiteInitialiserDataRetrieval.class)
public class SiteInitialiserDataRetrieval {

	@Reference
	private AssetListEntryUsageLocalService assetListEntryUsageLocalService;

	@Reference
	private Portal portal;

	public Optional<AssetListEntryUsage> findAssetListEntryUsage(Layout layout, String assetListEntryUsageKey) {
		List<AssetListEntryUsage> results = new ArrayList<>(assetListEntryUsageLocalService.dslQuery(//
				DSLQueryFactoryUtil.select(AssetListEntryUsageTable.INSTANCE)//
						.from(AssetListEntryUsageTable.INSTANCE)//
						.where(AssetListEntryUsageTable.INSTANCE.companyId.eq(layout.getCompanyId())//
								.and(AssetListEntryUsageTable.INSTANCE.groupId.eq(layout.getGroupId()))//
								.and(AssetListEntryUsageTable.INSTANCE.classNameId.eq(portal.getClassNameId(InfoCollectionProvider.class)))//
								.and(AssetListEntryUsageTable.INSTANCE.plid.eq(layout.getPlid()))//
								.and(AssetListEntryUsageTable.INSTANCE.key.eq(assetListEntryUsageKey))//
								.and(AssetListEntryUsageTable.INSTANCE.containerType.eq(portal.getClassNameId(CollectionStyledLayoutStructureItem.class))))));

		return results.stream().findFirst();
	}

}
