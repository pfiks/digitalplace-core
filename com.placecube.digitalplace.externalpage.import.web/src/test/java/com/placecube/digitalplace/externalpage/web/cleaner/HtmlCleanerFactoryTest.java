package com.placecube.digitalplace.externalpage.web.cleaner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.htmlcleaner.ContentNode;
import org.htmlcleaner.HtmlCleaner;
import org.junit.Test;

public class HtmlCleanerFactoryTest {

	@Test
	public void createHtmlCleaner_WhenNoErrors_ThenReturnsNewHtmlCleanerInstance() {
		assertThat(HtmlCleanerFactory.createHtmlCleaner() instanceof HtmlCleaner, equalTo(true));
	}

	@Test
	public void createLeadingSpaceTagNodeVisitor_WhenNoErrors_ThenReturnsNewLeadingSpaceTagNodeVisitor() {
		assertThat(HtmlCleanerFactory.createLeadingSpaceTagNodeVisitor() instanceof LeadingSpaceTagNodeVisitor, equalTo(true));
	}

	@Test
	public void createContentNode_WhenNoErrors_ThenReturnsNewContentNodeInstance_WithContent() {
		final String content = "content";

		ContentNode node = HtmlCleanerFactory.createContentNode(content);

		assertThat(node instanceof ContentNode, equalTo(true));
		assertThat(node.getContent(), equalTo(content));
	}
}
