package com.placecube.digitalplace.announcementsbanner.web.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.portlet.PortletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.announcements.kernel.model.AnnouncementsEntry;
import com.liferay.announcements.kernel.model.AnnouncementsFlagConstants;
import com.liferay.announcements.kernel.service.AnnouncementsEntryLocalService;
import com.liferay.announcements.kernel.service.AnnouncementsFlagLocalService;
import com.liferay.portal.kernel.cookies.CookiesManagerUtil;
import com.liferay.portal.kernel.cookies.constants.CookiesConstants;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;

@Component(immediate = true, service = AnnouncementsBannerService.class)
public class AnnouncementsBannerService {

	static final String DISMISS_ANNOUNCEMENT_COOKIE_ATTRIBUTE_PREFIX = "dismiss-announcement-";

	private static final Log LOG = LogFactoryUtil.getLog(AnnouncementsBannerService.class);

	@Reference
	private AnnouncementsEntryLocalService announcementsEntryLocalService;

	@Reference
	private AnnouncementsFlagLocalService announcementsFlagLocalService;

	@Reference
	private Portal portal;

	public List<AnnouncementsEntry> getUnreadAnnouncements(HttpServletRequest httpServletRequest, User user) throws PortletException {
		try {
			LinkedHashMap<Long, long[]> scopes = AnnouncementsUtil.getAnnouncementScopes(user);
			setCurrentGroupScope(scopes, httpServletRequest);

			List<AnnouncementsEntry> entries = announcementsEntryLocalService.getEntries(user.getUserId(), scopes, false, AnnouncementsFlagConstants.NOT_HIDDEN, QueryUtil.ALL_POS, QueryUtil.ALL_POS);

			if (user.isGuestUser()) {
				return entries.stream().filter(entry -> unreadAnnouncement(entry, httpServletRequest)).collect(Collectors.toList());
			}

			return entries;
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

	public void markAnnouncementAsRead(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, User user, long announcementsEntryId) {
		if (user.isGuestUser()) {
			LOG.debug("Adding session attribute to dismiss announcement " + announcementsEntryId);
			Cookie announcementReadCookie = new Cookie(DISMISS_ANNOUNCEMENT_COOKIE_ATTRIBUTE_PREFIX + announcementsEntryId, Boolean.TRUE.toString());
			announcementReadCookie.setPath("/");
			CookiesManagerUtil.addCookie(CookiesConstants.CONSENT_TYPE_NECESSARY, announcementReadCookie, httpServletRequest, httpServletResponse);
		} else {
			LOG.debug("Marking announcement " + announcementsEntryId + " as hidden for userId: " + user.getUserId());
			announcementsFlagLocalService.addFlag(user.getUserId(), announcementsEntryId, AnnouncementsFlagConstants.HIDDEN);
		}
	}

	private void setCurrentGroupScope(LinkedHashMap<Long, long[]> scopes, HttpServletRequest httpServletRequest) {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		if (themeDisplay != null) {
			scopes.put(portal.getClassNameId(Group.class.getName()), new long[] { themeDisplay.getLayout().getGroupId() });
		}
	}

	private boolean unreadAnnouncement(AnnouncementsEntry entry, HttpServletRequest httpServletRequest) {
		return !GetterUtil.getBoolean(CookiesManagerUtil.getCookieValue(DISMISS_ANNOUNCEMENT_COOKIE_ATTRIBUTE_PREFIX + entry.getEntryId(), httpServletRequest), false);
	}

}
