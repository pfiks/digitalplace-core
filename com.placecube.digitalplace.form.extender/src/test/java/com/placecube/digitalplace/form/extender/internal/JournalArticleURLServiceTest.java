package com.placecube.digitalplace.form.extender.internal;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.Bundle;

public class JournalArticleURLServiceTest {

	@InjectMocks
	private JournalArticleURLService journalArticleURLService;

	@Mock
	private Bundle mockBundle;

	@Mock
	private HttpURLConnection mockConnection;

	@Before
	public void setup() {
		initMocks(this);
	}

	@Test
	public void getJournalArticleURLs_WhenNoArticles_ThenReturnEmptyList() {

		when(mockBundle.findEntries("webcontent", "*.xml", true)).thenReturn(null);

		List<URL> result = journalArticleURLService.getJournalArticleURLs(mockBundle);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getJournalArticleURLs_WhenArticleFound_ThenReturnListContainingArticle() throws MalformedURLException, IOException {

		URL ddmForm = new URL(null, "file://webcontent/test.xml", createURLStreamHandler());
		List<URL> urls = new ArrayList<URL>();
		urls.add(ddmForm);
		Enumeration<URL> enuJournalArticleURLs = Collections.enumeration(urls);

		when(mockBundle.findEntries("webcontent", "*.xml", true)).thenReturn(enuJournalArticleURLs);

		List<URL> result = journalArticleURLService.getJournalArticleURLs(mockBundle);

		assertThat(result.get(0), sameInstance(ddmForm));
	}

	private URLStreamHandler createURLStreamHandler() throws IOException {

		URLStreamHandler stubURLStreamHandler = new URLStreamHandler() {

			@Override
			protected URLConnection openConnection(URL u) throws IOException {
				return mockConnection;
			}
		};

		return stubURLStreamHandler;
	}
}