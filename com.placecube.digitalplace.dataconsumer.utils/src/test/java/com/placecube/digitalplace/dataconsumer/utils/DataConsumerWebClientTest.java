package com.placecube.digitalplace.dataconsumer.utils;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.dataconsumer.service.DataTransformationService;

@RunWith(MockitoJUnitRunner.class)
public class DataConsumerWebClientTest {

	private static final String GET_METHOD = "GET";

	private static final String HEADER_NAME = "headerName";

	private static final String HEADER_VALUE = "headerValue";

	private static final String PARAM_STRING = "?param1=value1&param2=value2";

	private static final String POST_METHOD = "POST";

	private static final String URL = "http://www.site.com/url";

	@InjectMocks
	private DataConsumerWebClient dataConsumerWebClient;

	@Mock
	private DataConsumerWebClientUtils mockDataConsumerWebClientUtils;

	@Mock
	private DataTransformationService mockDataTransformationService;

	@Mock
	private Map<String, String> mockHeadersMap;

	@Mock
	private HttpURLConnection mockHttpURLConnection;

	@Mock
	private Map<String, String> mockParametersMap;

	@Test
	public void execute_WhenMethodIsNotRequiredAndURLContainsQuestionMark_ThenOpensConnectionWithTheURLWithParametersReplaced() throws IOException {
		String urlWithQueryParamPlaceholders = "http://www.site.com/url?param1={param1}&param2={param2}";
		String valueOne = "value1";
		String valueTwo = "value2";

		Map<String, String> parametersMap = new HashMap<>();
		parametersMap.put("param1", valueOne);
		parametersMap.put("param2", valueTwo);

		String urlWithParametersReplaced = "http://www.site.com/url?param1=value1&param2=value2";

		when(mockDataConsumerWebClientUtils.getEncodedParameter(valueOne)).thenReturn(valueOne);
		when(mockDataConsumerWebClientUtils.getEncodedParameter(valueTwo)).thenReturn(valueTwo);
		when(mockDataConsumerWebClientUtils.openConnection(urlWithParametersReplaced)).thenReturn(mockHttpURLConnection);

		dataConsumerWebClient.execute(urlWithQueryParamPlaceholders, GET_METHOD, new HashMap<>(), parametersMap, StringPool.BLANK);

		InOrder inOrder = inOrder(mockDataConsumerWebClientUtils, mockHttpURLConnection);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).openConnection(urlWithParametersReplaced);
		inOrder.verify(mockHttpURLConnection, times(1)).setRequestMethod(GET_METHOD);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).appendBody(mockHttpURLConnection, null);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).readConnectionOutput(mockHttpURLConnection);
	}

	@Test
	public void execute_WhenMethodIsNotRequiredAndURLDoesNotContainQuestionMark_ThenOpensConnectionWithTheParametrisedURL() throws IOException {
		String urlWithPathParamPlaceholders = "http://www.site.com/url/{param1}/{param2}";
		String valueOne = "value1";
		String valueTwo = "test parameter";
		String encodedValueTwo = "test+parameter";

		Map<String, String> parametersMap = new HashMap<>();
		parametersMap.put("param1", valueOne);
		parametersMap.put("param2", valueTwo);

		String urlWithParametersReplaced = "http://www.site.com/url/value1/test+parameter";

		when(mockDataConsumerWebClientUtils.getParamString(parametersMap)).thenReturn(PARAM_STRING);
		when(mockDataConsumerWebClientUtils.getEncodedParameter(valueOne)).thenReturn(valueOne);
		when(mockDataConsumerWebClientUtils.getEncodedParameter(valueTwo)).thenReturn(encodedValueTwo);
		when(mockDataConsumerWebClientUtils.openConnection(urlWithParametersReplaced + PARAM_STRING)).thenReturn(mockHttpURLConnection);

		dataConsumerWebClient.execute(urlWithPathParamPlaceholders, GET_METHOD, new HashMap<>(), parametersMap, StringPool.BLANK);

		InOrder inOrder = inOrder(mockDataConsumerWebClientUtils, mockHttpURLConnection);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).openConnection(urlWithParametersReplaced + PARAM_STRING);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).appendBody(mockHttpURLConnection, null);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).readConnectionOutput(mockHttpURLConnection);
	}

	@Test
	public void execute_WhenMethodIsRequired_ThenOpensConnectionWithTheURLAndAppendsBody() throws IOException {
		String body = "body";

		when(mockDataConsumerWebClientUtils.isHttpMethodRequiresBody(POST_METHOD)).thenReturn(true);
		when(mockDataConsumerWebClientUtils.openConnection(URL)).thenReturn(mockHttpURLConnection);

		dataConsumerWebClient.execute(URL, POST_METHOD, mockHeadersMap, mockParametersMap, body);

		InOrder inOrder = inOrder(mockDataConsumerWebClientUtils, mockHttpURLConnection);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).openConnection(URL);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).appendBody(mockHttpURLConnection, body);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).readConnectionOutput(mockHttpURLConnection);
	}

	@Test
	public void execute_WhenNoErrors_ThenReturnsConnectionOutput() throws IOException {
		final String response = "response";

		when(mockDataConsumerWebClientUtils.getParamString(mockParametersMap)).thenReturn(PARAM_STRING);
		when(mockDataConsumerWebClientUtils.openConnection(URL + PARAM_STRING)).thenReturn(mockHttpURLConnection);
		when(mockDataConsumerWebClientUtils.readConnectionOutput(mockHttpURLConnection)).thenReturn(response);

		String result = dataConsumerWebClient.execute(URL, GET_METHOD, new HashMap<>(), mockParametersMap, StringPool.BLANK);

		assertThat(result, equalTo(response));
	}

	@Test
	public void execute_WhenNoErrors_ThenSetsRequestMethodAndHeadersAndReadsConnectionOutputAndDisconnects() throws IOException {
		Map<String, String> headersMap = new HashMap<>();
		headersMap.put(HEADER_NAME, HEADER_VALUE);

		when(mockDataConsumerWebClientUtils.getParamString(mockParametersMap)).thenReturn(PARAM_STRING);
		when(mockDataConsumerWebClientUtils.openConnection(URL + PARAM_STRING)).thenReturn(mockHttpURLConnection);
		when(mockDataConsumerWebClientUtils.isHttpMethodRequiresBody(GET_METHOD)).thenReturn(false);

		dataConsumerWebClient.execute(URL, GET_METHOD, headersMap, mockParametersMap, StringPool.BLANK);

		InOrder inOrder = inOrder(mockDataConsumerWebClientUtils, mockHttpURLConnection);

		inOrder.verify(mockHttpURLConnection, times(1)).setRequestProperty(HEADER_NAME, HEADER_VALUE);
		inOrder.verify(mockHttpURLConnection, times(1)).setRequestMethod(GET_METHOD);
		inOrder.verify(mockDataConsumerWebClientUtils, times(1)).readConnectionOutput(mockHttpURLConnection);
		inOrder.verify(mockHttpURLConnection, times(1)).disconnect();
	}

}
