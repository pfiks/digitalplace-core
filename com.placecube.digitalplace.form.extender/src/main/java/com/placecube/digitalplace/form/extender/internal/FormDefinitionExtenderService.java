package com.placecube.digitalplace.form.extender.internal;

import java.net.URL;
import java.util.Dictionary;
import java.util.List;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.HashMapDictionary;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.form.extender.FormDefinitionExtender;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.digitalplace.form.service.FormDefinitionService;

@Component(immediate = true, service = { FormDefinitionExtenderService.class })
public class FormDefinitionExtenderService {

	private static final Log LOG = LogFactoryUtil.getLog(FormDefinitionExtenderService.class);

	@Reference
	private BundleTrackerService bundleTrackerService;

	@Reference
	private FormDefinitionService formDefinitionService;

	@Reference
	private RegistrationService registrationService;

	@Reference
	private JournalArticleURLService journalArticleURLService;

	@Reference
	private DataProviderURLService dataProviderURLService;


	public FormDefinition createFormDefinition(Dictionary<String, String> headers, Bundle bundle) {

		String formCategory = getFormCategory(headers);
		String formDescription = getFormDescription(headers);
		String formName = getFormName(headers);
		String formKey = getFormKey(headers);
		String journalArticleFolderName = getJournalArticleFolderName(headers);

		URL ddmForm = getDDFormJsonFile(bundle);
		URL ddmFormLayout = getDDFormLayoutJsonFile(bundle);
		URL settingsDDMFormValues = getSettingsDDMFormValuesJsonFile(bundle);
		List<URL> journalArticleURLs = journalArticleURLService.getJournalArticleURLs(bundle);
		List<URL> dataProviderDDMFormValuesURLs = dataProviderURLService.getDataProviderDDMFormValuesURLs(bundle);

		return formDefinitionService.create(formName, formDescription, formCategory, formKey, journalArticleFolderName, ddmForm, ddmFormLayout, settingsDDMFormValues, journalArticleURLs, dataProviderDDMFormValuesURLs);

	}

	public boolean isValidFormDefinition(Dictionary<String, String> headers) {

		String formCategory = getFormCategory(headers);
		String formDescription = getFormDescription(headers);
		String formName = getFormName(headers);

		return Validator.isNull(formCategory) || Validator.isNull(formName) || Validator.isNull(formDescription) ? false : true;

	}

	public void register(FormDefinition formDefinition, BundleContext bundleContext) {

		ServiceRegistration<FormDefinition> serviceRegistration = bundleContext.registerService(FormDefinition.class, formDefinition, new HashMapDictionary<>());

		registrationService.registerService(formDefinition, serviceRegistration);

	}

	public void unregister(FormDefinition formDefinition) {

		registrationService.unregisterService(formDefinition);

	}

	public void openBundleTracker(BundleContext bundleContext, FormDefinitionExtender formDefinitionExtender) {

		bundleTrackerService.openBundleTracker(bundleContext, formDefinitionExtender);

	}

	public void closeBundleTracker() {

		bundleTrackerService.closeBundleTracker();

	}

	public boolean hasValidHeaders(Dictionary<String, String> headers) {

		boolean valid = true;

		String formKey = getFormKey(headers);
		if (Validator.isNull(formKey)) {
			valid = false;
		} else if (getFormKey(headers).length() > 75) {
			LOG.warn("The Digital-Place-Form-Key header's length is greater than 75, the form definition won't be registered.");
			valid = false;
		}

		return valid;
	}

	private URL getDDFormJsonFile(Bundle bundle) {
		return bundle.getEntry("ddm-form.json");
	}

	private URL getDDFormLayoutJsonFile(Bundle bundle) {
		return bundle.getEntry("ddm-form-layout.json");
	}

	private URL getSettingsDDMFormValuesJsonFile(Bundle bundle) {
		return bundle.getEntry("settings-ddm-form-values.json");
	}

	private String getFormCategory(Dictionary<String, String> headers) {
		return headers.get("Digital-Place-Form-Category");
	}

	private String getFormDescription(Dictionary<String, String> headers) {
		return headers.get("Digital-Place-Form-Description");
	}

	private String getFormKey(Dictionary<String, String> headers) {
		return headers.get("Digital-Place-Form-Key");
	}

	private String getFormName(Dictionary<String, String> headers) {
		return headers.get("Digital-Place-Form-Name");
	}

	private String getJournalArticleFolderName(Dictionary<String, String> headers) {
		return headers.get("Digital-Place-Journal-Article-Folder-Name");
	}

}
