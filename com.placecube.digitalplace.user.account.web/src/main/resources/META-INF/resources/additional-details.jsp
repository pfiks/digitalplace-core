<%@ include file="init.jsp"%>

<c:set var="progressBarValue" value="60"/>
<%@ include file="/fragments/progress-bar.jspf" %>

<div class="row">
	<%@ include file="/fragments/side-bar.jspf" %>
	
	<div class="col-md-8"> 
			
		<portlet:actionURL name="<%=MVCCommandKeys.ADDITIONAL_DETAILS%>" var="additionalDetailsURL" />

		<gds-forms-ui:errorSummary portletNamespace="${portletNamespace}" errors="${accountContext.getErrors()}"/>

		<aui:form action="${additionalDetailsURL}" method="post" name="createAccountForm">
			<%@ include file="/fragments/creation-errors.jspf" %>
			
			<%@ include file="/fragments/additional-fields.jspf" %>
			
			<aui:button-row>
				<div class="pull-right">
					<aui:button type="submit" value="next" cssClass="btn btn-primary" icon="glyphicon glyphicon-chevron-right" iconAlign="right"/>
				</div>
				<div class="pull-left">
					<portlet:renderURL var="backURL">
						<portlet:param name="mvcRenderCommandName" value="${additionalDetailsPreviousStep}" />
					</portlet:renderURL>
					<aui:button type="button" value="previous" cssClass="btn btn-default" href="${backURL}" icon="glyphicon glyphicon-chevron-left" iconAlign="left"/>
				</div>
			</aui:button-row>
		</aui:form>
	</div>
</div>
