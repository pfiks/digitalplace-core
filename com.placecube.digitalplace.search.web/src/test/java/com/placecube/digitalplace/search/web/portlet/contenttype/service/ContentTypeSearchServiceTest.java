package com.placecube.digitalplace.search.web.portlet.contenttype.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.portlet.contenttype.configuration.ContentTypeFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.contenttype.constants.ContentTypeConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AggregatedResourceBundleUtil.class, ContentTypeConstants.class })
public class ContentTypeSearchServiceTest extends PowerMockito {

	@InjectMocks
	private ContentTypeSearchService contentTypeSearchService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private ContentTypeFacetPortletInstanceConfiguration mockContentTypeFacetPortletInstanceConfiguration;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private DDLRecordSet mockDDLRecordSet;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private Document mockDocument;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(AggregatedResourceBundleUtil.class, ContentTypeConstants.class);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenExceptionRetrievingTheConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {

		when(mockConfigurationProvider.getPortletInstanceConfiguration(ContentTypeFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		contentTypeSearchService.getConfiguration(mockThemeDisplay);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsTheConfiguration() throws ConfigurationException {

		when(mockConfigurationProvider.getPortletInstanceConfiguration(ContentTypeFacetPortletInstanceConfiguration.class, mockThemeDisplay))
				.thenReturn(mockContentTypeFacetPortletInstanceConfiguration);

		ContentTypeFacetPortletInstanceConfiguration result = contentTypeSearchService.getConfiguration(mockThemeDisplay);

		assertThat(result, sameInstance(mockContentTypeFacetPortletInstanceConfiguration));
	}

	@Test
	public void getContentTypeSearchField_WhenNoError_ThenReturnsTheSearchFieldForNameContentTypeWithTheSpecificiedClassNameValue() {
		String className = "classNameValue";

		Field result = contentTypeSearchService.getContentTypeSearchField(className);

		assertThat(result.getName(), equalTo(SearchFacetConstants.CONTENT_TYPE));
		assertThat(result.getValue(), equalTo(className));
	}

	@Test
	public void getDDLRecordContentTypeValue_WhenNoError_ThenReturnsTheDDLClassNameWithTheStructureId() {
		String structureId = "structureIdValue";

		String result = contentTypeSearchService.getDDLRecordContentTypeValue(structureId);

		assertThat(result, equalTo(ContentTypeConstants.DDL_RECORD_CLASS_NAME + "_" + structureId));
	}

	@Test
	public void getDDLRecordSetIdFromDocument_WhenDocumentHasStructureIdField_ThenReturnsTheFieldFromTheDocument() throws PortalException {
		String expected = "expectedValue";
		when(mockDocument.get("ddmStructureId")).thenReturn(expected);

		String result = contentTypeSearchService.getDDLStructureIdFromDocument(mockDocument);

		assertThat(result, equalTo(expected));
		verifyZeroInteractions(mockDDLRecordLocalService);
	}

	@Test
	public void getDDLStructureIdFromDocument_WhenDocumentDoesNotHaveStructureIdField_ThenReturnsTheFieldFromTheDDLRecordRetrieved() throws PortalException {
		long entryClassPK = 123;
		long expected = 456;
		when(mockDocument.get("ddmStructureId")).thenReturn(null);
		when(mockDocument.get("entryClassPK")).thenReturn(String.valueOf(entryClassPK));
		when(mockDDLRecordLocalService.getDDLRecord(entryClassPK)).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getRecordSet()).thenReturn(mockDDLRecordSet);
		when(mockDDLRecordSet.getDDMStructureId()).thenReturn(expected);

		String result = contentTypeSearchService.getDDLStructureIdFromDocument(mockDocument);

		assertThat(result, equalTo(String.valueOf(expected)));
	}

	@Test
	public void getLabelMapForContentType_WhenContentTypeValueDoesNotStartWithDDLPrefix_ThenReturnsTheLocalisedMessageKeyForTheClassName() throws PortalException {
		Locale locale = Locale.ENGLISH;
		String expected = "expected";
		String contentTypeValue = "contentTypeValue";
		String messageKey = "messageKeyValue";
		when(ContentTypeConstants.getMessageKeyForClassName(contentTypeValue)).thenReturn(messageKey);
		when(AggregatedResourceBundleUtil.get(messageKey, locale, "com.placecube.digitalplace.search.web")).thenReturn(expected);

		Map<Locale, String> result = contentTypeSearchService.getLabelMapForContentType(locale, contentTypeValue);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(locale), equalTo(expected));
	}

	@Test
	public void getLabelMapForContentType_WhenContentTypeValueStartsWithDDLPrefixAndExceptionRetrievingTheStructure_ThenReturnsTheLocalisedMessageKey() throws PortalException {
		long structureId = 123;
		Locale locale = Locale.ENGLISH;
		String expected = "expected";
		when(mockDDMStructureLocalService.getDDMStructure(structureId)).thenThrow(new PortalException());
		when(AggregatedResourceBundleUtil.get("content-type-ddl", locale, "com.placecube.digitalplace.search.web")).thenReturn(expected);

		Map<Locale, String> result = contentTypeSearchService.getLabelMapForContentType(locale, ContentTypeConstants.DDL_RECORD_CLASS_NAME + StringPool.UNDERLINE + structureId);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(locale), equalTo(expected + StringPool.BLANK + structureId));
	}

	@Test
	public void getLabelMapForContentType_WhenContentTypeValueStartsWithDDLPrefixAndNoErrorRetrievingTheStructure_ThenReturnsTheStructureName() throws PortalException {
		long structureId = 123;
		Locale locale = Locale.ENGLISH;
		String expected = "expected";
		when(mockDDMStructureLocalService.getDDMStructure(structureId)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getName(locale)).thenReturn(expected);

		Map<Locale, String> result = contentTypeSearchService.getLabelMapForContentType(locale, ContentTypeConstants.DDL_RECORD_CLASS_NAME + StringPool.UNDERLINE + structureId);

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(locale), equalTo(expected));
	}
}
