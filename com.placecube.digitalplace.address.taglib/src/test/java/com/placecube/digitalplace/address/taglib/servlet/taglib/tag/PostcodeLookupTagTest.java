package com.placecube.digitalplace.address.taglib.servlet.taglib.tag;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.address.taglib.servlet.taglib.context.ServletContextUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalClassLoaderUtil.class, PropsUtil.class, ServletContextUtil.class, HtmlUtil.class })
public class PostcodeLookupTagTest {

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private PageContext mockPageContext;

	@Mock
	private ServletContext mockServletContext;

	/* Can't use annotation as liferay classes need to be mockStatic first */
	private PostcodeLookupTag postcodeLookupTag;

	@Test
	public void getPage_ReturnsPostcodeLookupPage() {
		String result = postcodeLookupTag.getPage();
		assertThat(result, equalTo("/postcode_lookup/page.jsp"));
	}

	@Test
	public void setAttributes_WhenNoError_ThenSetTheAttributesInRequest() {

		when(HtmlUtil.escape("this is the postcode")).thenReturn("this is the postcode");

		postcodeLookupTag.setFullAddress("this the full address");
		postcodeLookupTag.setAddressLine1("this is the addressLine1");
		postcodeLookupTag.setAddressLine2("this is the addressLine2");
		postcodeLookupTag.setAddressLine3("this is the addressLine3");
		postcodeLookupTag.setAddressLine4("this is the addressLine4");
		postcodeLookupTag.setCity("this is the city");
		postcodeLookupTag.setPostcode("this is the postcode");
		postcodeLookupTag.setUprn("this is the uprn");
		postcodeLookupTag.setDisplaySelected(true);
		postcodeLookupTag.setSelectedCallback("this is the selected callback");
		postcodeLookupTag.setUnselectedCallback("this is the unselected callback");

		postcodeLookupTag.setAttributes(mockHttpServletRequest);

		verify(mockHttpServletRequest, times(1)).setAttribute("fullAddress", "this the full address");
		verify(mockHttpServletRequest, times(1)).setAttribute("addressLine1", "this is the addressLine1");
		verify(mockHttpServletRequest, times(1)).setAttribute("addressLine2", "this is the addressLine2");
		verify(mockHttpServletRequest, times(1)).setAttribute("addressLine3", "this is the addressLine3");
		verify(mockHttpServletRequest, times(1)).setAttribute("addressLine4", "this is the addressLine4");
		verify(mockHttpServletRequest, times(1)).setAttribute("city", "this is the city");
		verify(mockHttpServletRequest, times(1)).setAttribute("postcode", "this is the postcode");
		verify(mockHttpServletRequest, times(1)).setAttribute("uprn", "this is the uprn");
		verify(mockHttpServletRequest, times(1)).setAttribute("displaySelected", true);
		verify(mockHttpServletRequest, times(1)).setAttribute("selectedCallback", "this is the selected callback");
		verify(mockHttpServletRequest, times(1)).setAttribute("unselectedCallback", "this is the unselected callback");

	}

	@Test
	public void setPageContext_WhenNoError_ThenSetPageContext() {
		when(PortalClassLoaderUtil.getClassLoader()).thenReturn(this.getClass().getClassLoader());

		postcodeLookupTag.setPageContext(mockPageContext);

		verify(postcodeLookupTag, times(1)).setPageContext(mockPageContext);
	}

	@Test
	public void setPageContext_WhenNoError_ThenSetServletContext() {
		when(PortalClassLoaderUtil.getClassLoader()).thenReturn(this.getClass().getClassLoader());

		postcodeLookupTag.setPageContext(mockPageContext);

		verify(postcodeLookupTag, times(1)).setServletContext(mockServletContext);
	}

	@Before
	public void setUp() {
		mockStatic(PortalClassLoaderUtil.class, PropsUtil.class, ServletContextUtil.class, HtmlUtil.class);

		initMocks(this);

		when(mockPageContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(ServletContextUtil.getServletContext()).thenReturn(mockServletContext);

		postcodeLookupTag = spy(new PostcodeLookupTag());
	}
}
