package com.placecube.digitalplace.user.account.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.placecube.digitalplace.common.email.EmailService;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

public class AccountEmailAddressServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 456L;

	private static final String EMAIL_ADDRESS_VERIFICATION_URL = "/email-address-verification";

	private static final String FIRST_NAME = "John";

	private static final long GROUP_ID = 12345L;

	private static final String LAST_NAME = "Smith";

	private static final String SUFFIX = "testdomain.com";

	@InjectMocks
	private AccountEmailAddressServiceImpl accountEmailAddressServiceImpl;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private EmailService mockEmailService;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = UserAccountException.class)
	public void getEmailAddressVerifiedLayoutPlid_WhenAccountConfigurationRetrievalFails_ThenThrowsPortletException() throws Exception {
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenThrow(new UserAccountException());

		accountEmailAddressServiceImpl.getEmailAddressVerifiedLayoutPlid(mockAccountContext);
	}

	@Test
	public void getEmailAddressVerifiedLayoutPlid_WhenEmailAddressVerifiedLayoutDoesNotExist_ThenReturnsEmptyOptional() throws Exception {
		when(mockAccountContext.getGroupId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressVerificationURL()).thenReturn(EMAIL_ADDRESS_VERIFICATION_URL);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(COMPANY_ID, false, EMAIL_ADDRESS_VERIFICATION_URL)).thenReturn(null);

		Optional<Long> result = accountEmailAddressServiceImpl.getEmailAddressVerifiedLayoutPlid(mockAccountContext);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getEmailAddressVerifiedLayoutPlid_WhenEmailAddressVerifiedLayoutExists_ThenReturnsOptionalOfPlid() throws Exception {
		final long plid = 44345L;

		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressVerificationURL()).thenReturn(EMAIL_ADDRESS_VERIFICATION_URL);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, false, EMAIL_ADDRESS_VERIFICATION_URL)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(plid);

		Optional<Long> result = accountEmailAddressServiceImpl.getEmailAddressVerifiedLayoutPlid(mockAccountContext);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), equalTo(plid));
	}

	@Test(expected = UserAccountException.class)
	public void getGeneratedEmailAddress_WhenExceptionRetrievingConfiguration_ThenThrowsPortalException() throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenThrow(new UserAccountException());

		accountEmailAddressServiceImpl.getGeneratedEmailAddress(mockAccountContext);
	}

	@Test
	public void getGeneratedEmailAddress_WhenNoErrors_ThenReturnsTheGeneratedEmailAddress() throws Exception {
		String expected = "expectedValue";

		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.generatedEmailSuffix()).thenReturn(SUFFIX);
		when(mockAccountContext.getFirstName()).thenReturn(FIRST_NAME);
		when(mockAccountContext.getLastName()).thenReturn(LAST_NAME);
		when(mockEmailService.getGeneratedEmailAddress(FIRST_NAME, LAST_NAME, SUFFIX)).thenReturn(expected);

		String result = accountEmailAddressServiceImpl.getGeneratedEmailAddress(mockAccountContext);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void isEmailAutoGenerated_WhenContainsAutoGeneratedSuffix_ThenReturnsTrue() throws Exception {
		String testEmail = "email@" + SUFFIX;
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.generatedEmailSuffix()).thenReturn(SUFFIX);

		boolean result = accountEmailAddressServiceImpl.isEmailAutoGenerated(testEmail, mockAccountContext);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isEmailAutoGenerated_WhenDoesNotContainAutoGeneratedSuffix_ThenReturnsFalse() throws Exception {
		String testEmail = "email@somethingelse";
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.generatedEmailSuffix()).thenReturn(SUFFIX);

		boolean result = accountEmailAddressServiceImpl.isEmailAutoGenerated(testEmail, mockAccountContext);

		assertThat(result, equalTo(false));
	}

	@Test(expected = UserAccountException.class)
	public void isEmailAutoGenerated_WhenExceptionRetrievingConfiguration_ThenThrowsPortalException() throws Exception {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenThrow(new UserAccountException());

		accountEmailAddressServiceImpl.isEmailAutoGenerated("testEmail", mockAccountContext);

	}

	@Test
	public void isEmailAutoGenerated_WhenNoSuffixConfigured_ThenReturnsFalse() throws Exception {
		String testEmail = "email@" + SUFFIX;
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockUserProfileFieldsConfiguration.generatedEmailSuffix()).thenReturn(null);

		boolean result = accountEmailAddressServiceImpl.isEmailAutoGenerated(testEmail, mockAccountContext);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailAutoGenerated_WhenPassedInEmailIsNull_ThenReturnsFalse() throws Exception {
		String testEmail = null;
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		boolean result = accountEmailAddressServiceImpl.isEmailAutoGenerated(testEmail, mockAccountContext);

		assertThat(result, equalTo(false));
	}

}
