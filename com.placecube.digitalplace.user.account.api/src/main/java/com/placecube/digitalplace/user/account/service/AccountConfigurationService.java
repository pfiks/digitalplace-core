package com.placecube.digitalplace.user.account.service;

import java.util.Set;

import javax.portlet.PortletRequest;

import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

public interface AccountConfigurationService {

	CreateAccountConfiguration getAccountConfiguration(AccountContext accountContext) throws UserAccountException;

	CreateAccountConfiguration getAccountConfiguration(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException;

	String getAdditionalDetailsPreviousStep(UserProfileFieldsConfiguration configuration);

	String getAddressNextStep(AccountContext accountContext) throws UserAccountException;

	UserProfileFieldsConfiguration getCompanyUserProfileFieldsConfiguration(long companyId) throws UserAccountException;

	Set<ExpandoField> getConfiguredExpandoFields(AccountContext accountContext, UserProfileFieldsConfiguration configuration);

	boolean getEmailVerificationEnabled(AccountContext accountContext);

	String getPasswordPreviousStep(AccountContext accountContext, UserProfileFieldsConfiguration configuration);

	String getPersonalDetailsNextStep(AccountContext accountContext) throws UserAccountException;

	boolean isAccountSecurityStepEnabled(AccountContext accountContext) throws UserAccountException;

	boolean isCaptchaEnabled(PortletRequest portletRequest);

}
