package com.placecube.digitalplace.search.sortby.mostliked.triggers;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.ratings.kernel.model.RatingsStats;
import com.placecube.digitalplace.search.sortby.mostliked.service.MostLikedService;

@Component(immediate = true, service = ModelListener.class)
public class RatingsStatsListener extends BaseModelListener<RatingsStats> {

	@Reference
	private MostLikedService mostLikedService;

	@Override
	public void onAfterCreate(RatingsStats ratingsStats) {
		updateScore(ratingsStats);
	}

	@Override
	public void onAfterRemove(RatingsStats ratingsStats) {
		updateScore(ratingsStats);
	}

	@Override
	public void onAfterUpdate(RatingsStats originalModel, RatingsStats model) {
		updateScore(model);
	}

	private void updateScore(RatingsStats ratingsStats) {
		String className = ratingsStats.getClassName();
		long classPK = ratingsStats.getClassPK();

		mostLikedService.reindexRatingScore(className, classPK);
	}

}
