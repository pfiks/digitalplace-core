package com.placecube.digitalplace.companyinitializeradmin.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.companyinitializeradmin.constants.MVCCommandKeys;
import com.placecube.digitalplace.companyinitializeradmin.constants.PortletKeys;
import com.placecube.digitalplace.companyinitializeradmin.constants.PortletRequestKeys;
import com.placecube.digitalplace.companyinitializeradmin.initializer.internal.service.CompanyInitializerRegistry;
import com.placecube.digitalplace.initialiser.CompanyInitializer;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.COMPANY_INITIALIZER_ADMIN,
		"mvc.command.name=" + MVCCommandKeys.RUN_COMPANY_INITIALIZER }, service = MVCActionCommand.class)
public class RunCompanyInitializerMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(RunCompanyInitializerMVCActionCommand.class);

	@Reference
	private CompanyInitializerRegistry companyInitializerRegistry;

	@Reference
	private Portal portal;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {
		try {
			String companyInitializerKey = ParamUtil.getString(actionRequest, PortletRequestKeys.SELECTED_COMPANY_INITIALIZER);
			CompanyInitializer companyInitializer = companyInitializerRegistry.getCompanyInitializer(companyInitializerKey);

			if (Validator.isNotNull(companyInitializer)) {
				companyInitializer.initialize(portal.getCompany(actionRequest));
			}
		} catch (Exception e) {
			LOG.error(e);
			throw new PortletException(e);
		}
	}

}
