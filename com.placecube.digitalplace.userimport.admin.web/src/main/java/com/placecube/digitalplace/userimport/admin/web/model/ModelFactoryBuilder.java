package com.placecube.digitalplace.userimport.admin.web.model;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.liferay.mail.kernel.model.MailMessage;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = ModelFactoryBuilder.class)
public class ModelFactoryBuilder {

    public InternetAddress createInternetAddress(String address) throws AddressException {
        return new InternetAddress(address);
    }

    public MailMessage createMailMessage() {
        return new MailMessage();
    }
}