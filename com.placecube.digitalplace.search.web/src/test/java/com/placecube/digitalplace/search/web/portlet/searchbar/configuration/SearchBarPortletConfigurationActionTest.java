package com.placecube.digitalplace.search.web.portlet.searchbar.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class SearchBarPortletConfigurationActionTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private SearchBarPortletInstanceConfiguration mockConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SearchBarPortletConfigurationAction searchBarPortletConfigurationAction;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void include_WhenNoError_ThenSetsRequestAttributes() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String expectedURL = "/friendly-url";
		boolean expectedSearchGuest = false;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(SearchBarPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.destinationPage()).thenReturn(expectedURL);
		when(mockConfiguration.searchGuestGroup()).thenReturn(expectedSearchGuest);

		searchBarPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(SearchBarPortletConfigurationConstants.DESTINATION_PAGE, expectedURL);
		verify(mockHttpServletRequest, times(1)).setAttribute(SearchBarPortletConfigurationConstants.SEARCH_GUEST_GROUP, expectedSearchGuest);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesPreferences() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String expectedURL = "/friendly-url";
		String expectedSearchGuest = "false";
		when(ParamUtil.getString(mockActionRequest, SearchBarPortletConfigurationConstants.DESTINATION_PAGE, StringPool.BLANK)).thenReturn(expectedURL);
		when(ParamUtil.getString(mockActionRequest, SearchBarPortletConfigurationConstants.SEARCH_GUEST_GROUP, StringPool.FALSE)).thenReturn(expectedSearchGuest);
		searchBarPortletConfigurationAction = spy(new SearchBarPortletConfigurationAction());

		searchBarPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(searchBarPortletConfigurationAction, times(1)).setPreference(mockActionRequest, SearchBarPortletConfigurationConstants.DESTINATION_PAGE, expectedURL);
		verify(searchBarPortletConfigurationAction, times(1)).setPreference(mockActionRequest, SearchBarPortletConfigurationConstants.SEARCH_GUEST_GROUP, expectedSearchGuest);

	}
}
