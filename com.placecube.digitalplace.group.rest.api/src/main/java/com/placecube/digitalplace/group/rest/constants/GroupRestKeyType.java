package com.placecube.digitalplace.group.rest.constants;

public enum GroupRestKeyType {

	APPLICATION_KEY("group-rest-application-key"),

	APPLICATION_SECRET("group-rest-application-secret");

	private final String value;

	private GroupRestKeyType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
