package com.placecube.digitalplace.appointment.booking.registry;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.appointment.booking.connector.AppointmentBookingConnector;
import com.placecube.digitalplace.appointment.booking.registry.AppointmentsRegistry;

public class AppointmentsRegistryTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@InjectMocks
	private AppointmentsRegistry appointmentsRegistry;

	@Mock
	private AppointmentBookingConnector mockAppointmentConnector1;

	@Mock
	private AppointmentBookingConnector mockAppointmentConnector2;

	@Mock
	private AppointmentBookingConnector mockAppointmentConnector3;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getEnabledAppointmentConnectors_WhenNoError_ThenReturnsTheCurrentlyRegisteredConnectorsThatAreEnabled() {
		assertTrue(appointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID).isEmpty());

		appointmentsRegistry.setAppointmentConnector(mockAppointmentConnector1);
		when(mockAppointmentConnector1.isEnabled(COMPANY_ID)).thenReturn(true);
		assertThat(appointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID), contains(mockAppointmentConnector1));

		appointmentsRegistry.setAppointmentConnector(mockAppointmentConnector2);
		when(mockAppointmentConnector2.isEnabled(COMPANY_ID)).thenReturn(true);
		assertThat(appointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID), contains(mockAppointmentConnector1, mockAppointmentConnector2));

		appointmentsRegistry.setAppointmentConnector(mockAppointmentConnector3);
		when(mockAppointmentConnector3.isEnabled(COMPANY_ID)).thenReturn(false);
		assertThat(appointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID), contains(mockAppointmentConnector1, mockAppointmentConnector2));

		appointmentsRegistry.unsetAppointmentConnector(mockAppointmentConnector1);
		assertThat(appointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID), contains(mockAppointmentConnector2));
	}

}
