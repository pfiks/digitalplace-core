package com.placecube.digitalplace.externalpage.internal.upgrade.upgrade_1_1_2;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class ExternalPageIndexUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {

		if (hasIndex("Placecube_ExternalPage_ExternalPage", "IX_F5FC6FD1")) {
			runSQLTemplateString("drop index IX_F5FC6FD1 on Placecube_ExternalPage_ExternalPage;", false);
		}
		if (hasIndex("Placecube_ExternalPage_ExternalPage", "IX_57279D76")) {
			runSQLTemplateString("drop index IX_57279D76 on Placecube_ExternalPage_ExternalPage;", false);
		}
		if (hasIndex("Placecube_ExternalPage_ExternalPage", "IX_EDC50978")) {
			runSQLTemplateString("drop index IX_EDC50978 on Placecube_ExternalPage_ExternalPage;", false);
		}

		if (!hasIndex("Placecube_ExternalPage_ExternalPage", "IX_867491D8")) {
			runSQLTemplateString("create index IX_8D1C15F7 on Placecube_ExternalPage_ExternalPage (groupId, url[$COLUMN_LENGTH:200$]);", false);
		}
		if (!hasIndex("Placecube_ExternalPage_ExternalPage", "IX_1AFD5F7C")) {
			runSQLTemplateString("create index IX_7B152B72 on Placecube_ExternalPage_ExternalPage (uuid_[$COLUMN_LENGTH:75$]);", false);
		}

	}

}
