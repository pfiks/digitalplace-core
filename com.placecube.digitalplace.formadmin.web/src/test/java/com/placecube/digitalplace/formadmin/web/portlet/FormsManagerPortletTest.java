package com.placecube.digitalplace.formadmin.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Locale;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.frontend.taglib.clay.servlet.taglib.util.NavigationItem;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.formadmin.web.constants.FormsManagerTabs;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.tabs.FormsManagerNavigationProvider;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, MVCPortlet.class })
public class FormsManagerPortletTest extends PowerMockito {

	public FormsManagerPortletTest() {

	}

	@Mock
	protected FormsManagerNavigationProvider mockFormsManagerNavigationProvider;

	@InjectMocks
	private FormsManagerPortlet formsManagerPortlet;

	@Mock
	private List<NavigationItem> mockNavigationItems;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test
	public void render_WhenNoError_ThenAddsNavigationItemsAsRequestAttribute() throws Exception {
		String currentTab = "currentTabValue";
		Locale locale = Locale.CANADA_FRENCH;
		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.CURRENT_TAB, FormsManagerTabs.FORMS.getNavigationKey())).thenReturn(currentTab);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		when(mockRenderRequest.getLocale()).thenReturn(locale);
		when(mockFormsManagerNavigationProvider.getViewNavigationItems(mockPortletURL, currentTab, locale)).thenReturn(mockNavigationItems);

		formsManagerPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.NAVIGATION_ITEMS, mockNavigationItems);
	}

}
