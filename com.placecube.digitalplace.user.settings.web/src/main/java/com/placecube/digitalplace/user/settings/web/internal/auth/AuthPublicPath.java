package com.placecube.digitalplace.user.settings.web.internal.auth;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, property = { "auth.public.path=/my-settings", }, service = Object.class)
public class AuthPublicPath {

}