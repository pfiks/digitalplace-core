package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;

@Component(immediate = true, service = SiteInitializerBackupUtils.class)
public class SiteInitializerBackupUtils {

	private static final Log LOG = LogFactoryUtil.getLog(SiteInitializerBackupUtils.class);

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private SiteNavigationMenuLocalService siteNavigationMenuLocalService;

	public long createBackupGroup(long currentGroupId) throws PortalException {
		ServiceContext serviceContext = (ServiceContext) ServiceContextThreadLocal.getServiceContext().clone();

		Map<Locale, String> nameMap = Collections.singletonMap(serviceContext.getLocale(), "backupGroupFor_" + currentGroupId);

		Group group = groupLocalService.addGroup(serviceContext.getUserId(), GroupConstants.DEFAULT_PARENT_GROUP_ID, Group.class.getName(), 0, GroupConstants.DEFAULT_LIVE_GROUP_ID, nameMap, nameMap,
				GroupConstants.TYPE_SITE_PRIVATE, false, 0, "", true, false, serviceContext);

		return group.getGroupId();
	}

	public boolean hasContentToBackUp(long groupId) {
		return siteNavigationMenuLocalService.getSiteNavigationMenusCount(groupId) > 0;
	}

	public void moveSiteNavigationMenus(long originalGroupId, long updatedGroupId) {
		List<SiteNavigationMenu> siteNavigationMenus = siteNavigationMenuLocalService.getSiteNavigationMenus(originalGroupId);

		for (SiteNavigationMenu siteNavigationMenu : siteNavigationMenus) {
			SiteNavigationMenu siteNavigationMenuWithTheSameName = siteNavigationMenuLocalService.fetchSiteNavigationMenuByName(updatedGroupId, siteNavigationMenu.getName());

			if (Validator.isNotNull(siteNavigationMenuWithTheSameName)) {
				siteNavigationMenuWithTheSameName.setName(siteNavigationMenuWithTheSameName.getName() + StringPool.SPACE + StringPool.DASH + StringPool.SPACE + new SimpleDateFormat("dd MM yyyy HH:mm").format(new Date()));
				siteNavigationMenuLocalService.updateSiteNavigationMenu(siteNavigationMenuWithTheSameName);
				LOG.info("Renamed duplicated menu with SiteNavigationMenuId: " + siteNavigationMenuWithTheSameName.getSiteNavigationMenuId());
			}

			siteNavigationMenu.setGroupId(updatedGroupId);
			siteNavigationMenuLocalService.updateSiteNavigationMenu(siteNavigationMenu);
			LOG.info("Copying siteNavigationMenuId: " + siteNavigationMenu.getSiteNavigationMenuId() + " in groupId: " + updatedGroupId);
		}
	}

	public void removeBackupGroup(long backupGroupId) {
		try {
			groupLocalService.deleteGroup(backupGroupId);
		} catch (Exception e) {
			LOG.error("Unable to delete backup groupId: " + backupGroupId, e);
		}
	}

}
