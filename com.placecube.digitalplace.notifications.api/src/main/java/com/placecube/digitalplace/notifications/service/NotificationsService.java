package com.placecube.digitalplace.notifications.service;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendMailContext;
import com.placecube.digitalplace.notifications.model.SendMailContext.SendMailContextBuilder;
import com.placecube.digitalplace.notifications.model.SendSmsContext;

import java.util.Map;

public interface NotificationsService {

	/**
	 * Creates and returns a SendMailContextBuilder
	 *
	 * @param templateId the template id
	 * @param emailAddress the destination email address
	 * @param personalisation a map with parameters to personalise the email
	 * @param reference the reference
	 * @param fromEmailAddress the sender email address
	 * @return a SendMailContextBuilder
	 */
	SendMailContextBuilder createSendMailContextBuilder(String templateId, String emailAddress, Map<String, Object> personalisation, String reference, String fromEmailAddress);

	/**
	 * Sends an E-Mail to the Users E-Mail Address.
	 *
	 * @param sendMailContext Context object containing required parameters.
	 * @param serviceContext Provider of additional fields.
	 * @throws NotificationsException If an error occurs sending the Email.
	 * @throws ConfigurationException If an error occurs retrieving
	 *             Configuration.
	 */
	void sendMail(SendMailContext sendMailContext, ServiceContext serviceContext) throws NotificationsException, ConfigurationException;

	/**
	 * Sends an SMS Message to the Users Phone Number (Not currently supported).
	 *
	 * @param sendSmsContext Context object containing required parameters.
	 * @param serviceContext Provider of additional fields.
	 * @throws NotificationsException If an error occurs sending the SMS.
	 *             * @throws ConfigurationException If an error occurs
	 *             retrieving Configuration.
	 * @throws ConfigurationException If an error occurs retrieving
	 *             Configuration.
	 */
	void sendSms(SendSmsContext sendSmsContext, ServiceContext serviceContext) throws NotificationsException, ConfigurationException;

}
