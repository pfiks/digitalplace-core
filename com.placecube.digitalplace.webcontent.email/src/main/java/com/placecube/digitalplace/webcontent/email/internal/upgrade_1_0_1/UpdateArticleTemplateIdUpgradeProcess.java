package com.placecube.digitalplace.webcontent.email.internal.upgrade_1_0_1;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.webcontent.email.constants.EmailConstants;



public class UpdateArticleTemplateIdUpgradeProcess extends UpgradeProcess {

	private DDMStructureLocalService ddmStructureLocalService;

	public UpdateArticleTemplateIdUpgradeProcess(DDMStructureLocalService ddmStructureLocalService){
		this.ddmStructureLocalService = ddmStructureLocalService;
	}
	@Override
	protected void doUpgrade() throws Exception {

		DynamicQuery dynamicQuery = ddmStructureLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("structureKey", EmailConstants.STRUCTURE_KEY));

		List<DDMStructure> ddmStructures = ddmStructureLocalService.dynamicQuery(dynamicQuery);

		for (DDMStructure ddmStructure : ddmStructures) {

			runSQL("UPDATE JournalArticle SET DDMTemplateKey = \'" + EmailConstants.TEMPLATE_KEY + "\' WHERE DDMStructureId = " + ddmStructure.getStructureId() + " AND (DDMTemplateKey IS NULL OR DDMTemplateKey = '')");

		}

	}

}
