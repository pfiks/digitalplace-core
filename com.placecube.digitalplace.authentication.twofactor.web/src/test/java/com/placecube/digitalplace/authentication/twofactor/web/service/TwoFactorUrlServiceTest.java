package com.placecube.digitalplace.authentication.twofactor.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionURL;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderURL;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorPortletKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class TwoFactorUrlServiceTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionURL mockActionURL;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LiferayPortletResponse mockLiferayPortletResponse;

	@Mock
	private LiferayPortletURL mockLiferayPortletURL;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletResponse mockPortletResponse;

	@Mock
	private PortletURLFactory mockPortletURLFactory;

	@Mock
	private RenderURL mockRenderURL;

	@Mock
	private User mockUser;

	@Mock
	private UserTwoFactorAuthentication mockUserTwoFactorAuthentication;

	@InjectMocks
	private TwoFactorUrlService twoFactorUrlService;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void getLoginActionURL_WhenNoError_ThenReturnsTheConfiguredActionURL() {
		String encryptedStateMapJSON = "encryptedStateMapJSONValue";
		when(mockLiferayPortletResponse.createActionURL()).thenReturn(mockActionURL);

		ActionURL result = twoFactorUrlService.getLoginActionURL(mockLiferayPortletResponse, encryptedStateMapJSON);

		assertThat(result, sameInstance(mockActionURL));
		verify(mockActionURL, times(1)).setParameter(ActionRequest.ACTION_NAME, "/login/login");
		verify(mockActionURL, times(1)).setParameter("state", encryptedStateMapJSON);
	}

	@Test
	public void getRedirectURL_WhenPortletIdIsFastLogin_ThenReturnsTheRedirectURLWithTheOriginalWindowState() throws Exception {
		WindowState windowState = WindowState.MINIMIZED;
		String actionURLValue = "actionURLValue";
		String returnToFullPageRenderURLValue = "returnToFullPageRenderURLValue";
		String expectedValue = "expectedValue";
		long plid = 12;
		String mvcRenderCommandName = "mvcRenderCommandNameValue";
		when(mockPortletURLFactory.create(mockHttpServletRequest, TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION, plid, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(ParamUtil.getString(mockHttpServletRequest, "p_p_id")).thenReturn(PortletKeys.FAST_LOGIN);
		when(mockActionURL.toString()).thenReturn(actionURLValue);
		when(mockActionRequest.getWindowState()).thenReturn(windowState);
		when(mockRenderURL.toString()).thenReturn(returnToFullPageRenderURLValue);
		when(mockLiferayPortletURL.toString()).thenReturn(expectedValue);

		String result = twoFactorUrlService.getRedirectURL(mvcRenderCommandName, plid, mockActionRequest, mockActionURL, mockHttpServletRequest, mockRenderURL);

		assertThat(result, equalTo(expectedValue));
		InOrder inOrder = inOrder(mockLiferayPortletURL);
		inOrder.verify(mockLiferayPortletURL, times(1)).setWindowState(windowState);
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter("saveLastPath", Boolean.FALSE.toString());
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter("mvcRenderCommandName", mvcRenderCommandName);
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter("redirect", actionURLValue);
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter("returnToFullPageURL", returnToFullPageRenderURLValue);
	}

	@Test
	public void getRedirectURL_WhenPortletIdIsNotFastLogin_ThenReturnsTheRedirectURLWithTheWindowStateMaximized() throws Exception {
		String actionURLValue = "actionURLValue";
		String returnToFullPageRenderURLValue = "returnToFullPageRenderURLValue";
		String expectedValue = "expectedValue";
		long plid = 12;
		String mvcRenderCommandName = "mvcRenderCommandNameValue";
		when(mockPortletURLFactory.create(mockHttpServletRequest, TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION, plid, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(ParamUtil.getString(mockHttpServletRequest, "p_p_id")).thenReturn("someOtherPortlet");
		when(mockActionURL.toString()).thenReturn(actionURLValue);
		when(mockRenderURL.toString()).thenReturn(returnToFullPageRenderURLValue);
		when(mockLiferayPortletURL.toString()).thenReturn(expectedValue);

		String result = twoFactorUrlService.getRedirectURL(mvcRenderCommandName, plid, mockActionRequest, mockActionURL, mockHttpServletRequest, mockRenderURL);

		assertThat(result, equalTo(expectedValue));

		InOrder inOrder = inOrder(mockLiferayPortletURL);
		inOrder.verify(mockLiferayPortletURL, times(1)).setWindowState(WindowState.MAXIMIZED);
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter("saveLastPath", Boolean.FALSE.toString());
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter("mvcRenderCommandName", mvcRenderCommandName);
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter("redirect", actionURLValue);
		inOrder.verify(mockLiferayPortletURL, times(1)).setParameter("returnToFullPageURL", returnToFullPageRenderURLValue);
	}

	@Test
	public void getResetTwoFactorURL_WhenNoError_ThenReturnsTheConfiguredResetTwoFactorURL() {
		String urlValue = "thisIsMyUrl&with" + PortletKeys.USERS_ADMIN + "andSomeOther" + PortletKeys.USERS_ADMIN + "andOtherStuff";
		String expectedValue = "thisIsMyUrl&with" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION + "andSomeOther" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION + "andOtherStuff";
		long userId = 123;
		when(mockPortal.getLiferayPortletResponse(mockPortletResponse)).thenReturn(mockLiferayPortletResponse);
		when(mockLiferayPortletResponse.createActionURL()).thenReturn(mockActionURL);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockActionURL.toString()).thenReturn(urlValue);

		String result = twoFactorUrlService.getResetTwoFactorURL(mockPortletResponse, mockUser);

		assertThat(result, equalTo(expectedValue));
		InOrder inOrder = inOrder(mockActionURL);
		inOrder.verify(mockActionURL, times(1)).setParameter("p_p_id", TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION);
		inOrder.verify(mockActionURL, times(1)).setParameter(ActionRequest.ACTION_NAME, TwoFactorMVCCommandKeys.RESET);
		inOrder.verify(mockActionURL, times(1)).setParameter("selectedUserId", String.valueOf(userId));
	}

	@Test
	public void getReturnToFullPageURL_WhenNoRedirectSpecified_ThenReturnsTheRenderURL() {
		when(mockLiferayPortletResponse.createRenderURL()).thenReturn(mockRenderURL);

		RenderURL result = twoFactorUrlService.getReturnToFullPageURL(mockLiferayPortletResponse, "");

		assertThat(result, sameInstance(mockRenderURL));
	}

	@Test
	public void getReturnToFullPageURL_WhenRedirectSpecified_ThenReturnsTheRenderURLWithTheRedirect() {
		String redirect = "redirectVal";
		when(mockLiferayPortletResponse.createRenderURL()).thenReturn(mockRenderURL);

		RenderURL result = twoFactorUrlService.getReturnToFullPageURL(mockLiferayPortletResponse, redirect);

		assertThat(result, sameInstance(mockRenderURL));
		verify(mockRenderURL, times(1)).setParameter("redirect", redirect);
	}
}
