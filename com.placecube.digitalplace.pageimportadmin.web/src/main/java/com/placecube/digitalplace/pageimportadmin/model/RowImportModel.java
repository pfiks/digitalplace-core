package com.placecube.digitalplace.pageimportadmin.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RowImportModel {

	private final int index;
	private final long groupId;
	private final String title;
	private final String url;
	private final List<Long> categoryIds;
	private final List<String> categoriesNames;
	private final Set<String> errorMessages;
	private final Set<String> warningMessages;

	public RowImportModel(long groupId, int index, String title, String url, List<String> categoriesNames) {
		this.groupId = groupId;
		this.index = index + 1;
		this.title = title;
		this.url = url;
		this.categoriesNames = categoriesNames;
		categoryIds = new ArrayList<>();
		errorMessages = new HashSet<>();
		warningMessages = new HashSet<>();
	}

	public void addCategoryId(Long categoryId) {
		categoryIds.add(categoryId);
	}

	public void addErrorMessage(String message) {
		errorMessages.add(message);
	}

	public void addWarningMessage(String message) {
		warningMessages.add(message);
	}

	public List<String> getCategoriesNames() {
		return categoriesNames;
	}

	public List<Long> getCategoryIds() {
		return categoryIds;
	}

	public Set<String> getErrorMessages() {
		return errorMessages;
	}

	public long getGroupId() {
		return groupId;
	}

	public int getIndex() {
		return index;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

	public Set<String> getWarningMessages() {
		return warningMessages;
	}

	public boolean isValid() {
		return errorMessages.isEmpty();
	}

	public boolean isWebContent() {
		return this instanceof WebContentImportModel;
	}

	public boolean isPageImport() {
		return this instanceof PageImportModel;
	}

}
