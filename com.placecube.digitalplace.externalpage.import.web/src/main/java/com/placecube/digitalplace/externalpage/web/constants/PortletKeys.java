package com.placecube.digitalplace.externalpage.web.constants;

public final class PortletKeys {

	public static final String EXTERNAL_PAGE_IMPORT_PORTLET = "com_placecube_digitalplace_externalpage_portlet_ExternalPageImportPortlet";

	private PortletKeys() {
	}
}
