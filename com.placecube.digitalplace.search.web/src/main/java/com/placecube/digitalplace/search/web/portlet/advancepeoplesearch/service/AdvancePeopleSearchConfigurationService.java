package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.settings.LocalizedValuesMap;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.AdvancePeopleSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.model.UserSearchField;

@Component(immediate = true, service = AdvancePeopleSearchConfigurationService.class)
public class AdvancePeopleSearchConfigurationService {

	private static final Log LOG = LogFactoryUtil.getLog(AdvancePeopleSearchConfigurationService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private ExpandoColumnLocalService expandoColumnLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private LayoutLocalService layoutLocalService;

	public List<UserSearchField> getSearchFields(long companyId, Locale locale, String configuredSearchFields) {
		List<UserSearchField> searchFields = new LinkedList<>();

		if (Validator.isNotNull(configuredSearchFields)) {
			try {
				JSONArray searchFieldsMapping = jsonFactory.createJSONArray(configuredSearchFields);
				if (searchFieldsMapping != null) {
					for (int i = 0; i < searchFieldsMapping.length(); i++) {
						UserSearchField userSearchField = parseSearchFieldConfiguration(companyId, locale, searchFieldsMapping.getJSONObject(i));
						if (Validator.isNotNull(userSearchField)) {
							searchFields.add(userSearchField);
						}
					}
				}
			} catch (JSONException e) {
				LOG.warn("Unable to parse search fields configuration: " + configuredSearchFields + " - " + e.getMessage());
			}
		}

		return searchFields;
	}

	public String getTitle(ThemeDisplay themeDisplay, LocalizedValuesMap titleLocalizedValuesMap) {
		String title = titleLocalizedValuesMap.get(themeDisplay.getLocale());
		if (Validator.isNull(title)) {
			title = titleLocalizedValuesMap.get(themeDisplay.getSiteDefaultLocale());
		}
		return title;
	}

	public Optional<AdvancePeopleSearchPortletInstanceConfiguration> getValidConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		AdvancePeopleSearchPortletInstanceConfiguration portletInstanceConfiguration = configurationProvider.getPortletInstanceConfiguration(AdvancePeopleSearchPortletInstanceConfiguration.class,
				themeDisplay);

		return isValidConfiguration(portletInstanceConfiguration, themeDisplay.getSiteDefaultLocale(), themeDisplay.getCompanyId()) ? Optional.of(portletInstanceConfiguration) : Optional.empty();

	}

	private String getLabelFromExpandoField(long companyId, Locale locale, String name) {
		ExpandoColumn expandoColumn = expandoColumnLocalService.getColumn(companyId, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME, name);
		return expandoColumn != null ? expandoColumn.getDisplayName(locale) : StringPool.BLANK;
	}

	private boolean isValidConfiguration(AdvancePeopleSearchPortletInstanceConfiguration portletInstanceConfiguration, Locale locale, long companyId) {
		List<UserSearchField> configuredSearchFields = getSearchFields(companyId, locale, portletInstanceConfiguration.searchFields());
		return Validator.isNotNull(portletInstanceConfiguration.title().get(locale)) && !configuredSearchFields.isEmpty();
	}

	private UserSearchField parseSearchFieldConfiguration(long companyId, Locale locale, JSONObject jsonObject) {
		UserSearchField userSearchField = null;
		String label = jsonObject.getString("label");
		String field = jsonObject.getString("field");
		if (field.startsWith("expando")) {
			label = getLabelFromExpandoField(companyId, locale, label);
		}
		if (Validator.isNotNull(label)) {
			userSearchField = new UserSearchField(field, label);
		}
		return userSearchField;
	}
}
