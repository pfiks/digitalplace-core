package com.placecube.digitalplace.ddl.model.impl;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.ddl.internal.configuration.DynamicDataListCompanyConfigurationConstants;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.model.DynamicDataListValue;

@Component(immediate = true, service = ModelBuilder.class)
public class ModelBuilder {

	private static final Log LOG = LogFactoryUtil.getLog(ModelBuilder.class);

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private JSONFactory jsonFactory;

	public Optional<DynamicDataListMapping> getDynamicDataListMapping(String mapping) {
		try {
			JSONObject jsonObject = jsonFactory.createJSONObject(mapping);

			long ddlStructureId = jsonObject.getLong(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_STRUCTURE_ID);
			if (ddlStructureId > 0) {
				DDMStructure ddmStructure = ddmStructureLocalService.getDDMStructure(ddlStructureId);
				String mappingName = jsonObject.getString(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_MAPPING_NAME,
						ddmStructure.getName(ddmStructure.getDefaultLanguageId()));

				long fullViewDefaultTemplateId = jsonObject.getLong(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_FULL_VIEW_TEMPLATE_ID);
				long searchDefaultGeoPointViewTemplateId = jsonObject.getLong(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_GEOPOINT_TEMPLATE_ID);
				long searchDefaultListViewTemplateId = jsonObject.getLong(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_LIST_TEMPLATE_ID);
				long searchDefaultGridViewTemplateId = jsonObject.getLong(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_GRID_TEMPLATE_ID);

				String iconName = jsonObject.getString(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_ICON_NAME);
				String indexerTitleFieldName = jsonObject.getString(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_INDEX_TITLE);
				String indexerContentFieldName = jsonObject.getString(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_INDEX_CONTENT);

				return Optional.of(new DynamicDataListMappingImpl(mappingName, ddmStructure, fullViewDefaultTemplateId, searchDefaultGeoPointViewTemplateId,
						searchDefaultListViewTemplateId, searchDefaultGridViewTemplateId, indexerTitleFieldName, indexerContentFieldName, iconName));
			}
		} catch (PortalException e) {
			LOG.warn("Unable to retrieve dynamic data list mapping from configured json: " + mapping + " - " + e.getMessage());
			LOG.debug(e);
		}

		return Optional.empty();
	}

	public DynamicDataListEntryImpl initialiseDynamicDataListEntry(DDLRecord ddlRecord) {
		return new DynamicDataListEntryImpl(ddlRecord);
	}

	public DynamicDataListValue initialiseDynamicDataListValue(String fieldName, String fieldValue, String fieldAdditionalDetails) {
		return new DynamicDataListValueImpl(fieldName, fieldValue, fieldAdditionalDetails);
	}

}
