/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.dataconsumer.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.dataconsumer.model.DataConsumer;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the data consumer service. This utility wraps <code>com.placecube.digitalplace.dataconsumer.service.persistence.impl.DataConsumerPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DataConsumerPersistence
 * @generated
 */
public class DataConsumerUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(DataConsumer dataConsumer) {
		getPersistence().clearCache(dataConsumer);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, DataConsumer> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DataConsumer> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DataConsumer> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DataConsumer> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static DataConsumer update(DataConsumer dataConsumer) {
		return getPersistence().update(dataConsumer);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static DataConsumer update(
		DataConsumer dataConsumer, ServiceContext serviceContext) {

		return getPersistence().update(dataConsumer, serviceContext);
	}

	/**
	 * Returns all the data consumers where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching data consumers
	 */
	public static List<DataConsumer> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the data consumers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public static List<DataConsumer> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByUuid_First(
			String uuid, OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByUuid_First(
		String uuid, OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByUuid_Last(
			String uuid, OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByUuid_Last(
		String uuid, OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public static DataConsumer[] findByUuid_PrevAndNext(
			long dataConsumerId, String uuid,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByUuid_PrevAndNext(
			dataConsumerId, uuid, orderByComparator);
	}

	/**
	 * Removes all the data consumers where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of data consumers where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching data consumers
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the data consumer where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDataConsumerException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the data consumer where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the data consumer where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the data consumer where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the data consumer that was removed
	 */
	public static DataConsumer removeByUUID_G(String uuid, long groupId)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of data consumers where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching data consumers
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching data consumers
	 */
	public static List<DataConsumer> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public static List<DataConsumer> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public static DataConsumer[] findByUuid_C_PrevAndNext(
			long dataConsumerId, String uuid, long companyId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByUuid_C_PrevAndNext(
			dataConsumerId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the data consumers where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching data consumers
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @return the matching data consumers
	 */
	public static List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId) {

		return getPersistence().findByDDMFormInstanceId(ddmFormInstanceId);
	}

	/**
	 * Returns a range of all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public static List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId, int start, int end) {

		return getPersistence().findByDDMFormInstanceId(
			ddmFormInstanceId, start, end);
	}

	/**
	 * Returns an ordered range of all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().findByDDMFormInstanceId(
			ddmFormInstanceId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByDDMFormInstanceId(
			ddmFormInstanceId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByDDMFormInstanceId_First(
			long ddmFormInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByDDMFormInstanceId_First(
			ddmFormInstanceId, orderByComparator);
	}

	/**
	 * Returns the first data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByDDMFormInstanceId_First(
		long ddmFormInstanceId,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByDDMFormInstanceId_First(
			ddmFormInstanceId, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByDDMFormInstanceId_Last(
			long ddmFormInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByDDMFormInstanceId_Last(
			ddmFormInstanceId, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByDDMFormInstanceId_Last(
		long ddmFormInstanceId,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByDDMFormInstanceId_Last(
			ddmFormInstanceId, orderByComparator);
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public static DataConsumer[] findByDDMFormInstanceId_PrevAndNext(
			long dataConsumerId, long ddmFormInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByDDMFormInstanceId_PrevAndNext(
			dataConsumerId, ddmFormInstanceId, orderByComparator);
	}

	/**
	 * Removes all the data consumers where ddmFormInstanceId = &#63; from the database.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 */
	public static void removeByDDMFormInstanceId(long ddmFormInstanceId) {
		getPersistence().removeByDDMFormInstanceId(ddmFormInstanceId);
	}

	/**
	 * Returns the number of data consumers where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @return the number of matching data consumers
	 */
	public static int countByDDMFormInstanceId(long ddmFormInstanceId) {
		return getPersistence().countByDDMFormInstanceId(ddmFormInstanceId);
	}

	/**
	 * Returns all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @return the matching data consumers
	 */
	public static List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId) {

		return getPersistence().findByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId);
	}

	/**
	 * Returns a range of all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public static List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId, int start, int end) {

		return getPersistence().findByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId, start, end);
	}

	/**
	 * Returns an ordered range of all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().findByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByDDMDataProviderInstanceId_First(
			long ddmDataProviderInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByDDMDataProviderInstanceId_First(
			ddmDataProviderInstanceId, orderByComparator);
	}

	/**
	 * Returns the first data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByDDMDataProviderInstanceId_First(
		long ddmDataProviderInstanceId,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByDDMDataProviderInstanceId_First(
			ddmDataProviderInstanceId, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByDDMDataProviderInstanceId_Last(
			long ddmDataProviderInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByDDMDataProviderInstanceId_Last(
			ddmDataProviderInstanceId, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByDDMDataProviderInstanceId_Last(
		long ddmDataProviderInstanceId,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByDDMDataProviderInstanceId_Last(
			ddmDataProviderInstanceId, orderByComparator);
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public static DataConsumer[] findByDDMDataProviderInstanceId_PrevAndNext(
			long dataConsumerId, long ddmDataProviderInstanceId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByDDMDataProviderInstanceId_PrevAndNext(
			dataConsumerId, ddmDataProviderInstanceId, orderByComparator);
	}

	/**
	 * Removes all the data consumers where ddmDataProviderInstanceId = &#63; from the database.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 */
	public static void removeByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId) {

		getPersistence().removeByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId);
	}

	/**
	 * Returns the number of data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @return the number of matching data consumers
	 */
	public static int countByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId) {

		return getPersistence().countByDDMDataProviderInstanceId(
			ddmDataProviderInstanceId);
	}

	/**
	 * Returns all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching data consumers
	 */
	public static List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId) {

		return getPersistence().findByCompanyIdAndGroupId(companyId, groupId);
	}

	/**
	 * Returns a range of all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public static List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId, int start, int end) {

		return getPersistence().findByCompanyIdAndGroupId(
			companyId, groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().findByCompanyIdAndGroupId(
			companyId, groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public static List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId, int start, int end,
		OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByCompanyIdAndGroupId(
			companyId, groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByCompanyIdAndGroupId_First(
			long companyId, long groupId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByCompanyIdAndGroupId_First(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the first data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByCompanyIdAndGroupId_First(
		long companyId, long groupId,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByCompanyIdAndGroupId_First(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public static DataConsumer findByCompanyIdAndGroupId_Last(
			long companyId, long groupId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByCompanyIdAndGroupId_Last(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the last data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public static DataConsumer fetchByCompanyIdAndGroupId_Last(
		long companyId, long groupId,
		OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().fetchByCompanyIdAndGroupId_Last(
			companyId, groupId, orderByComparator);
	}

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public static DataConsumer[] findByCompanyIdAndGroupId_PrevAndNext(
			long dataConsumerId, long companyId, long groupId,
			OrderByComparator<DataConsumer> orderByComparator)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByCompanyIdAndGroupId_PrevAndNext(
			dataConsumerId, companyId, groupId, orderByComparator);
	}

	/**
	 * Removes all the data consumers where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	public static void removeByCompanyIdAndGroupId(
		long companyId, long groupId) {

		getPersistence().removeByCompanyIdAndGroupId(companyId, groupId);
	}

	/**
	 * Returns the number of data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching data consumers
	 */
	public static int countByCompanyIdAndGroupId(long companyId, long groupId) {
		return getPersistence().countByCompanyIdAndGroupId(companyId, groupId);
	}

	/**
	 * Caches the data consumer in the entity cache if it is enabled.
	 *
	 * @param dataConsumer the data consumer
	 */
	public static void cacheResult(DataConsumer dataConsumer) {
		getPersistence().cacheResult(dataConsumer);
	}

	/**
	 * Caches the data consumers in the entity cache if it is enabled.
	 *
	 * @param dataConsumers the data consumers
	 */
	public static void cacheResult(List<DataConsumer> dataConsumers) {
		getPersistence().cacheResult(dataConsumers);
	}

	/**
	 * Creates a new data consumer with the primary key. Does not add the data consumer to the database.
	 *
	 * @param dataConsumerId the primary key for the new data consumer
	 * @return the new data consumer
	 */
	public static DataConsumer create(long dataConsumerId) {
		return getPersistence().create(dataConsumerId);
	}

	/**
	 * Removes the data consumer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer that was removed
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public static DataConsumer remove(long dataConsumerId)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().remove(dataConsumerId);
	}

	public static DataConsumer updateImpl(DataConsumer dataConsumer) {
		return getPersistence().updateImpl(dataConsumer);
	}

	/**
	 * Returns the data consumer with the primary key or throws a <code>NoSuchDataConsumerException</code> if it could not be found.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public static DataConsumer findByPrimaryKey(long dataConsumerId)
		throws com.placecube.digitalplace.dataconsumer.exception.
			NoSuchDataConsumerException {

		return getPersistence().findByPrimaryKey(dataConsumerId);
	}

	/**
	 * Returns the data consumer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer, or <code>null</code> if a data consumer with the primary key could not be found
	 */
	public static DataConsumer fetchByPrimaryKey(long dataConsumerId) {
		return getPersistence().fetchByPrimaryKey(dataConsumerId);
	}

	/**
	 * Returns all the data consumers.
	 *
	 * @return the data consumers
	 */
	public static List<DataConsumer> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of data consumers
	 */
	public static List<DataConsumer> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of data consumers
	 */
	public static List<DataConsumer> findAll(
		int start, int end, OrderByComparator<DataConsumer> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of data consumers
	 */
	public static List<DataConsumer> findAll(
		int start, int end, OrderByComparator<DataConsumer> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the data consumers from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of data consumers.
	 *
	 * @return the number of data consumers
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static DataConsumerPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(DataConsumerPersistence persistence) {
		_persistence = persistence;
	}

	private static volatile DataConsumerPersistence _persistence;

}