package com.placecube.digitalplace.search.internal.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.search.configuration.SearchCompanyConfiguration;

@Component(immediate = true, service = SearchConfigurationService.class)
public class SearchConfigurationService {

	private static final Log LOG = LogFactoryUtil.getLog(SearchConfigurationService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	public int getConfiguredFacetMultiplier(long companyId) {
		try {
			SearchCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(SearchCompanyConfiguration.class, companyId);
			return configuration.facetMultiplier();

		} catch (ConfigurationException e) {
			LOG.error("Error retrieving configured facet multipler: " + e.getMessage());
			LOG.debug(e);

			return 1;
		}

	}
}
