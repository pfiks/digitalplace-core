/**
 * PostageAndPacking.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class PostageAndPacking  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.SummaryData pnpSummary;

    private com.capita_software_services.www.scp.base.TaxItem tax;

    private com.capita_software_services.www.scp.base.LgPnpDetails lgPnpDetails;

    public PostageAndPacking() {
    }

    public PostageAndPacking(
           com.capita_software_services.www.scp.base.SummaryData pnpSummary,
           com.capita_software_services.www.scp.base.TaxItem tax,
           com.capita_software_services.www.scp.base.LgPnpDetails lgPnpDetails) {
           this.pnpSummary = pnpSummary;
           this.tax = tax;
           this.lgPnpDetails = lgPnpDetails;
    }


    /**
     * Gets the pnpSummary value for this PostageAndPacking.
     *
     * @return pnpSummary
     */
    public com.capita_software_services.www.scp.base.SummaryData getPnpSummary() {
        return pnpSummary;
    }


    /**
     * Sets the pnpSummary value for this PostageAndPacking.
     *
     * @param pnpSummary
     */
    public void setPnpSummary(com.capita_software_services.www.scp.base.SummaryData pnpSummary) {
        this.pnpSummary = pnpSummary;
    }


    /**
     * Gets the tax value for this PostageAndPacking.
     *
     * @return tax
     */
    public com.capita_software_services.www.scp.base.TaxItem getTax() {
        return tax;
    }


    /**
     * Sets the tax value for this PostageAndPacking.
     *
     * @param tax
     */
    public void setTax(com.capita_software_services.www.scp.base.TaxItem tax) {
        this.tax = tax;
    }


    /**
     * Gets the lgPnpDetails value for this PostageAndPacking.
     *
     * @return lgPnpDetails
     */
    public com.capita_software_services.www.scp.base.LgPnpDetails getLgPnpDetails() {
        return lgPnpDetails;
    }


    /**
     * Sets the lgPnpDetails value for this PostageAndPacking.
     *
     * @param lgPnpDetails
     */
    public void setLgPnpDetails(com.capita_software_services.www.scp.base.LgPnpDetails lgPnpDetails) {
        this.lgPnpDetails = lgPnpDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PostageAndPacking)) return false;
        PostageAndPacking other = (PostageAndPacking) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.pnpSummary==null && other.getPnpSummary()==null) ||
             (this.pnpSummary!=null &&
              this.pnpSummary.equals(other.getPnpSummary()))) &&
            ((this.tax==null && other.getTax()==null) ||
             (this.tax!=null &&
              this.tax.equals(other.getTax()))) &&
            ((this.lgPnpDetails==null && other.getLgPnpDetails()==null) ||
             (this.lgPnpDetails!=null &&
              this.lgPnpDetails.equals(other.getLgPnpDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPnpSummary() != null) {
            _hashCode += getPnpSummary().hashCode();
        }
        if (getTax() != null) {
            _hashCode += getTax().hashCode();
        }
        if (getLgPnpDetails() != null) {
            _hashCode += getLgPnpDetails().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PostageAndPacking.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "postageAndPacking"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pnpSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "pnpSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "summaryData"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "tax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "taxItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lgPnpDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lgPnpDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lgPnpDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
