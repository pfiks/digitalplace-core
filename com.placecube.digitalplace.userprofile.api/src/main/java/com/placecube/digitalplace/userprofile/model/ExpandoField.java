package com.placecube.digitalplace.userprofile.model;

import java.util.Locale;

public interface ExpandoField {

	long getExpandoColumnId();

	String getExpandoFieldKey();

	String getLabel(Locale locale);

	boolean isRequired();
}
