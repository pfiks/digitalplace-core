package com.placecube.digitalplace.userprofile.service.impl;

import java.util.LinkedHashSet;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfigurationFactory;
import com.placecube.digitalplace.userprofile.model.ExpandoField;
import com.placecube.digitalplace.userprofile.model.impl.ExpandoFieldImpl;
import com.placecube.digitalplace.userprofile.service.UserProfileFieldsService;

@Component(immediate = true, service = UserProfileFieldsService.class)
public class UserProfileFieldsServiceImpl implements UserProfileFieldsService {

	@Reference
	private ExpandoColumnLocalService expandoColumnLocalService;

	@Reference
	private UserProfileFieldsConfigurationFactory userProfileFieldsConfigurationFactory;

	@Override
	public UserProfileFieldsConfiguration getCompanyUserProfileFieldsConfiguration(long companyId) throws ConfigurationException {
		return userProfileFieldsConfigurationFactory.getCompanyUserProfileFieldsConfiguration(companyId);
	}

	@Override
	public Set<ExpandoField> getConfiguredExpandoFields(long companyId, String[] configuredFields) {
		Set<ExpandoField> results = new LinkedHashSet<>();

		if (ArrayUtil.isNotEmpty(configuredFields)) {
			for (String expandoFieldSetting : configuredFields) {
				addExpandoToResults(companyId, results, expandoFieldSetting);
			}
		}

		return results;
	}

	@Override
	public Set<ExpandoField> getConfiguredExpandoFields(long companyId, UserProfileFieldsConfiguration configuration) {
		return getConfiguredExpandoFields(companyId, configuration.expandoFieldsSettings());
	}

	private void addExpandoToResults(long companyId, Set<ExpandoField> results, String expandoFieldSetting) {
		String[] values = StringUtil.split(expandoFieldSetting, StringPool.EQUAL);
		if (ArrayUtil.isNotEmpty(values)) {
			String expandoFieldName = values[0];
			ExpandoColumn expandoColumn = expandoColumnLocalService.getColumn(companyId, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME, expandoFieldName);
			if (Validator.isNotNull(expandoColumn)) {
				results.add(new ExpandoFieldImpl(expandoColumn, GetterUtil.getBoolean(values[1])));
			}
		}
	}

}