package com.placecube.digitalplace.externalpage.search.index.contributor;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.executor.PortalExecutorManager;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;

@RunWith(PowerMockRunner.class)
public class ExternalPageModelIndexerWriterContributorTest extends PowerMockito {

	@InjectMocks
	private ExternalPageModelIndexerWriterContributor externalPageModelIndexerWriterContributor;

	@Mock
	private BatchIndexingActionable mockBatchIndexingActionable;

	@Mock
	private Document mockDocument;

	@Mock
	private DynamicQueryBatchIndexingActionableFactory mockDynamicQueryBatchIndexingActionableFactory;

	@Mock
	private ExternalPage mockExternalPage;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	private IndexableActionableDynamicQuery mockIndexableActionableDynamicQuery;

	@Mock
	private IndexWriterHelper mockIndexWriterHelper;

	@Mock
	private PortalExecutorManager mockPortalExecutorManager;

	@Test
	public void getBatchIndexingActionable_WhenNoError_ThenReturnsTheBatchIndexingActionable() {
		when(mockExternalPageLocalService.getIndexableActionableDynamicQuery()).thenReturn(mockIndexableActionableDynamicQuery);
		when(mockDynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(mockIndexableActionableDynamicQuery)).thenReturn(mockBatchIndexingActionable);

		BatchIndexingActionable result = externalPageModelIndexerWriterContributor.getBatchIndexingActionable();

		assertThat(result, sameInstance(mockBatchIndexingActionable));
	}

	@Test
	public void getCompanyId_WhenNoError_ThenReturnsTheExternalPageCompanyId() {
		Long companyId = 123l;
		when(mockExternalPage.getCompanyId()).thenReturn(companyId);

		long result = externalPageModelIndexerWriterContributor.getCompanyId(mockExternalPage);

		assertThat(result, equalTo(companyId));
	}

	@Test
	public void getIndexerWriterMode_WhenNoError_ThenReturnsIndexerWriterModeUpdate() {
		IndexerWriterMode result = externalPageModelIndexerWriterContributor.getIndexerWriterMode(mockExternalPage);

		assertThat(result, equalTo(IndexerWriterMode.UPDATE));
	}

	@Before
	public void setUp() {
		mockIndexableActionableDynamicQuery = mock(IndexableActionableDynamicQuery.class);
	}

}
