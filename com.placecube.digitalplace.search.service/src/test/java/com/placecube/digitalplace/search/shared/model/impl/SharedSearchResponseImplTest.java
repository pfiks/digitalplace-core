package com.placecube.digitalplace.search.shared.model.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.search.facet.collector.FacetCollector;
import com.liferay.portal.kernel.search.facet.collector.TermCollector;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;
import com.placecube.digitalplace.search.internal.util.SearchContainerFactory;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.dao.search.SearchContainer", "com.liferay.portal.kernel.util.ParamUtil" })
@PrepareForTest({ HtmlUtil.class, PropsUtil.class, ParamUtil.class, PortalUtil.class, SearchContainerFactory.class })
public class SharedSearchResponseImplTest extends PowerMockito {

	private static final int CUR = 1;
	private static final int DELTA = 10;
	private static final String ITERATOR_URL = "urlValue";
	private static final String ITERATOR_URL_ESCAPED = "urlEscapedValue";

	@Mock
	private DisplayTerms mockDisplayTermsOne;

	@Mock
	private DisplayTerms mockDisplayTermsTwo;

	@Mock
	private Facet mockFacet;

	@Mock
	private FacetCollector mockFacetCollector;

	@Mock
	private Hits mockHits;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private SearchContainer mockSearchContainer;

	@Mock
	private SearchContainer mockSearchContainerResults;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SharedActiveFilter mockSharedActiveFilter;

	@Mock
	private SharedActiveFilter mockSharedActiveFilter2;

	@Mock
	private SharedSearchSession mockSharedSearchSession;

	@Mock
	private List<String> mockStringList;

	@Mock
	private TermCollector mockTermCollector1;

	@Mock
	private TermCollector mockTermCollector2;

	@Mock
	private TermCollector mockTermCollector3;

	private SharedSearchResponseImpl sharedSearchResponseImpl;

	@Before
	public void activateSetup() {
		mockStatic(HtmlUtil.class, ParamUtil.class, PortalUtil.class, SearchContainerFactory.class);

		String urlValue = "urlValue";
		when(mockSearchContainer.getIteratorURL()).thenReturn(mockPortletURL);
		when(mockPortletURL.toString()).thenReturn(ITERATOR_URL);
		when(HtmlUtil.escape(urlValue)).thenReturn(ITERATOR_URL_ESCAPED);
		sharedSearchResponseImpl = SharedSearchResponseImpl.init(mockHits, mockSearchContext, mockSearchContainer, mockSharedSearchSession);
	}

	@Test
	public void getActiveFilters_WhenNoError_ThenReturnsActiveFiltersFromSharedSession() {
		List<SharedActiveFilter> mockFilterList = new ArrayList<>();
		mockFilterList.add(mockSharedActiveFilter);
		mockFilterList.add(mockSharedActiveFilter2);

		when(mockSharedSearchSession.getActiveFilters()).thenReturn(mockFilterList);

		List<SharedActiveFilter> result = sharedSearchResponseImpl.getActiveFilters();

		assertThat(result, sameInstance(mockFilterList));
	}

	@Test
	public void getActiveValuesForFilter_WhenMutipleFiltersExist_ThenReturnActiveFilterValuesForFieldName() {
		String fieldName = "attributeNameVal";
		String value = "v1";

		List<SharedActiveFilter> mockFilterList = new ArrayList<>();
		mockFilterList.add(mockSharedActiveFilter);
		mockFilterList.add(mockSharedActiveFilter2);

		when(mockSharedSearchSession.getActiveFilters()).thenReturn(mockFilterList);
		when(mockSharedActiveFilter.getName()).thenReturn(fieldName);
		when(mockSharedActiveFilter2.getName()).thenReturn("different");
		when(mockSharedActiveFilter.getValue()).thenReturn(value);

		Set<String> results = sharedSearchResponseImpl.getActiveValuesForFilter(fieldName);

		assertThat(results.size(), equalTo(1));
		assertTrue(results.contains(value));
	}

	@Test
	public void getActiveValuesForFilter_WhenNoFiltersExist_ThenReturnEmptyList() {
		String fieldName = "attributeNameVal";
		Set<String> results = sharedSearchResponseImpl.getActiveValuesForFilter(fieldName);
		assertThat(results.size(), equalTo(0));
	}

	@Test
	public void getEscapedSearchResultsIteratorURL_WhenNoError_ThenReturnsTheSearchContainerIteratorUrlEscaped() {

		String result = sharedSearchResponseImpl.getEscapedSearchResultsIteratorURL();

		assertThat(result, equalTo(ITERATOR_URL_ESCAPED));
	}

	@Test
	public void getFacetValues_WhenFacetFound_ThenReturnsMapWithTheFacetValuesAndTheirFrequencies() {
		String facetName = "facetNameValue";
		when(mockSearchContext.getFacet(facetName)).thenReturn(mockFacet);
		when(mockFacet.getFacetCollector()).thenReturn(mockFacetCollector);

		List<TermCollector> termCollectors = new ArrayList<>();
		termCollectors.add(mockTermCollector1);
		termCollectors.add(mockTermCollector2);
		termCollectors.add(mockTermCollector3);
		when(mockFacetCollector.getTermCollectors()).thenReturn(termCollectors);

		when(mockTermCollector1.getTerm()).thenReturn("termOne");
		when(mockTermCollector1.getFrequency()).thenReturn(77);

		when(mockTermCollector2.getTerm()).thenReturn("termTwo");
		when(mockTermCollector2.getFrequency()).thenReturn(44);

		when(mockTermCollector3.getTerm()).thenReturn(StringPool.BLANK);

		Map<String, Integer> results = sharedSearchResponseImpl.getFacetValues(facetName);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get("termOne"), equalTo(77));
		assertThat(results.get("termTwo"), equalTo(44));
	}

	@Test
	public void getFacetValues_WhenFacetNotFound_ThenReturnsEmptyMap() {
		String facetName = "facetNameValue";
		when(mockSearchContext.getFacet(facetName)).thenReturn(null);

		Map<String, Integer> results = sharedSearchResponseImpl.getFacetValues(facetName);

		assertTrue(results.isEmpty());
	}

	@Test
	public void getIteratorURL_WhenNoError_ThenReturnsTheSearchContainerIteratorUrl() {
		String result = sharedSearchResponseImpl.getIteratorURL();

		assertThat(result, equalTo(ITERATOR_URL));
	}

	@Test
	public void getKeywords_WhenNoError_ThenReturnsTheSearchContextKeywords() {
		String expected = "exp";
		when(mockSearchContext.getKeywords()).thenReturn(expected);

		String result = sharedSearchResponseImpl.getKeywords();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSearchContainerForResults_WhenNoError_ThenReturnsAcopyOfTheDefaultSearchContainerConfiguredWithTheSpecifiedDetails() throws Throwable {
		String searchContainerId = "searchContainerIdValue";
		String noResultsMessage = "noResultsMessageValue";
		int total = 123;
		List<String> results = Collections.singletonList("one");

		List<String> headers = Collections.singletonList("headerOne");

		mockSearchContainer(headers);
		when(mockHits.getLength()).thenReturn(total);

		when(SearchContainerFactory.createSearchContainer(mockPortletRequest, mockDisplayTermsOne, mockDisplayTermsTwo, SearchContainer.DEFAULT_CUR_PARAM, CUR, DELTA, mockPortletURL, headers,
				noResultsMessage)).thenReturn(mockSearchContainerResults);

		SearchContainer result = sharedSearchResponseImpl.getSearchContainerForResults(searchContainerId, results, noResultsMessage);

		assertThat(result, sameInstance(mockSearchContainerResults));

		verify(mockSearchContainerResults, times(1)).setId(searchContainerId);
		verify(mockSearchContainerResults, times(1)).setResultsAndTotal(any(), eq(total));

	}

	@Test
	public void getSearchResults_WhenNoError_ThenReturnsTheSearchResultsDocuments() {
		Document[] expected = new Document[3];
		when(mockHits.getDocs()).thenReturn(expected);

		Document[] result = sharedSearchResponseImpl.getSearchResults();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSharedSearchSessionSingleValuedAttribute_WhenNoError_ThenReturnsTheSingleValuedAttribute() {
		String name = "nameValue";
		String expected = "exp";
		when(mockSharedSearchSession.getSingleValuedAttribute(name)).thenReturn(expected);

		String result = sharedSearchResponseImpl.getSharedSearchSessionSingleValuedAttribute(name);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getTotalResults_WhenNoError_ThenReturnsTheTotalNumberOfResults() {
		int expected = 34;
		when(mockHits.getLength()).thenReturn(expected);

		int result = sharedSearchResponseImpl.getTotalResults();

		assertThat(result, equalTo(expected));
	}

	private void mockSearchContainer(List<String> headers) {
		when(mockSearchContainer.getPortletRequest()).thenReturn(mockPortletRequest);
		when(mockSearchContainer.getDisplayTerms()).thenReturn(mockDisplayTermsOne);
		when(mockSearchContainer.getSearchTerms()).thenReturn(mockDisplayTermsTwo);
		when(mockSearchContainer.getCurParam()).thenReturn(SearchContainer.DEFAULT_CUR_PARAM);
		when(mockSearchContainer.getCur()).thenReturn(CUR);
		when(mockSearchContainer.getDelta()).thenReturn(DELTA);
		when(mockSearchContainer.getHeaderNames()).thenReturn(headers);
	}
}
