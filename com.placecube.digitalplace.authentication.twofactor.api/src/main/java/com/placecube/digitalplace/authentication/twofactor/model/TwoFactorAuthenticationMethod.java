package com.placecube.digitalplace.authentication.twofactor.model;

public final class TwoFactorAuthenticationMethod {

	public static final String EMAIL = "EMAIL";

	public static final String TOTP_APP = "TOTP (app)";

	private TwoFactorAuthenticationMethod() {
	}
}
