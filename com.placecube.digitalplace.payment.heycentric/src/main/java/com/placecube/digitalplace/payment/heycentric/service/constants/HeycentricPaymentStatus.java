package com.placecube.digitalplace.payment.heycentric.service.constants;

public class HeycentricPaymentStatus {

	public static final String NOT_SUCCESSFUL = "C";

	public static final String SUCCESSFUL = "A";

	public static final String UNKNOWN = "U";

	private HeycentricPaymentStatus() {
	}
}
