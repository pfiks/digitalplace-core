package com.placecube.digitalplace.companyinitializeradmin.portlet;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.companyinitializeradmin.constants.PortletRequestKeys;
import com.placecube.digitalplace.companyinitializeradmin.initializer.internal.service.CompanyInitializerRegistry;
import com.placecube.digitalplace.initialiser.CompanyInitializer;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class RunCompanyInitializerMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private Company mockCompany;

	@Mock
	private CompanyInitializer mockCompanyInitializer;

	@Mock
	private CompanyInitializerRegistry mockCompanyInitializerRegistry;

	@Mock
	private Portal mockPortal;

	@InjectMocks
	private RunCompanyInitializerMVCActionCommand runCompanyInitializerMVCActionCommand;

	@Test
	public void doProcessAction_WhenCompanyInitializerFound_ThenCallsInitializeOnTheCompany() throws Exception {
		String companyInitializerKey = "companyInitializerKeyValue";

		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.SELECTED_COMPANY_INITIALIZER)).thenReturn(companyInitializerKey);
		when(mockCompanyInitializerRegistry.getCompanyInitializer(companyInitializerKey)).thenReturn(mockCompanyInitializer);
		when(mockPortal.getCompany(mockActionRequest)).thenReturn(mockCompany);

		runCompanyInitializerMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockCompanyInitializer, times(1)).initialize(mockCompany);
	}

	@Test(expected = PortletException.class)
	public void doProcessAction_WhenCompanyInitializerFoundAndExceptionExecutingInitializeCode_ThenThrowsPortletException() throws Exception {
		String companyInitializerKey = "companyInitializerKeyValue";

		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.SELECTED_COMPANY_INITIALIZER)).thenReturn(companyInitializerKey);
		when(mockCompanyInitializerRegistry.getCompanyInitializer(companyInitializerKey)).thenReturn(mockCompanyInitializer);
		when(mockPortal.getCompany(mockActionRequest)).thenReturn(mockCompany);
		doThrow(new Exception()).when(mockCompanyInitializer).initialize(mockCompany);

		runCompanyInitializerMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = PortletException.class)
	public void doProcessAction_WhenCompanyInitializerFoundAndExceptionRetrievingCompany_ThenThrowsPortletException() throws Exception {
		String companyInitializerKey = "companyInitializerKeyValue";
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.SELECTED_COMPANY_INITIALIZER)).thenReturn(companyInitializerKey);
		when(mockCompanyInitializerRegistry.getCompanyInitializer(companyInitializerKey)).thenReturn(mockCompanyInitializer);
		when(mockPortal.getCompany(mockActionRequest)).thenThrow(new PortalException());

		runCompanyInitializerMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenCompanyInitializerNotFound_ThenDoesNotThrowAnyError() throws PortletException {
		String companyInitializerKey = "companyInitializerKeyValue";
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.SELECTED_COMPANY_INITIALIZER)).thenReturn(companyInitializerKey);
		when(mockCompanyInitializerRegistry.getCompanyInitializer(companyInitializerKey)).thenReturn(null);
		try {
			runCompanyInitializerMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
		} catch (Exception e) {
			fail();
		}
	}

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
		when(PropsUtil.get(PropsKeys.UNICODE_TEXT_NORMALIZER_FORM)).thenReturn("test");
	}
}
