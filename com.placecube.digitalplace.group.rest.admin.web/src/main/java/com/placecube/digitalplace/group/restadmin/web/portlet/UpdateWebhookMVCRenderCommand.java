package com.placecube.digitalplace.group.restadmin.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.group.rest.constants.WebhookEvent;
import com.placecube.digitalplace.group.restadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.ViewKeys;
import com.placecube.digitalplace.group.restadmin.web.model.WebhookFormModel;
import com.placecube.digitalplace.group.restadmin.web.service.GroupWebhookManagementService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.GROUP_WEBHOOKS, "mvc.command.name=" + MVCCommandKeys.UPDATE }, service = MVCRenderCommand.class)
public class UpdateWebhookMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private GroupWebhookManagementService webhookManagementService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			long webhookId = ParamUtil.getLong(renderRequest, "webhookId");

			WebhookFormModel webhookFormModel;

			if (webhookId > 0) {
				webhookFormModel = webhookManagementService.getWebhookFormModel(webhookId);
			} else {
				String webhookClassName = ParamUtil.getString(renderRequest, "webhookClassName");
				webhookFormModel = webhookManagementService.getWebhookFormModel(webhookClassName);
			}
			renderRequest.setAttribute("webhookFormModel", webhookFormModel);
			renderRequest.setAttribute("webhookAvailableEventActions", WebhookEvent.getActionsForClassName(webhookFormModel.getClassName()));

			return ViewKeys.UPDATE_GROUP_WEBHOOK_JSP;
		} catch (PortalException e) {
			throw new PortletException(e);
		}
	}

}
