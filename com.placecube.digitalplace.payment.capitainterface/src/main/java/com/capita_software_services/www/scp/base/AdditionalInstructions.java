/**
 * AdditionalInstructions.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class AdditionalInstructions  implements java.io.Serializable {
    private java.lang.String merchantCode;

    private java.lang.String countryCode;

    private java.lang.String currencyCode;

    private com.capita_software_services.www.scp.base.AcceptedCards acceptedCards;

    private java.lang.String language;

    private com.capita_software_services.www.scp.base.StageIndicator stageIndicator;

    private com.capita_software_services.www.scp.base.ResponseInterface responseInterface;

    private java.lang.String cardholderID;

    private org.apache.axis.types.PositiveInteger integrator;

    private java.lang.String styleCode;

    private java.lang.String frameworkCode;

    private java.lang.String systemCode;

    public AdditionalInstructions() {
    }

    public AdditionalInstructions(
           java.lang.String merchantCode,
           java.lang.String countryCode,
           java.lang.String currencyCode,
           com.capita_software_services.www.scp.base.AcceptedCards acceptedCards,
           java.lang.String language,
           com.capita_software_services.www.scp.base.StageIndicator stageIndicator,
           com.capita_software_services.www.scp.base.ResponseInterface responseInterface,
           java.lang.String cardholderID,
           org.apache.axis.types.PositiveInteger integrator,
           java.lang.String styleCode,
           java.lang.String frameworkCode,
           java.lang.String systemCode) {
           this.merchantCode = merchantCode;
           this.countryCode = countryCode;
           this.currencyCode = currencyCode;
           this.acceptedCards = acceptedCards;
           this.language = language;
           this.stageIndicator = stageIndicator;
           this.responseInterface = responseInterface;
           this.cardholderID = cardholderID;
           this.integrator = integrator;
           this.styleCode = styleCode;
           this.frameworkCode = frameworkCode;
           this.systemCode = systemCode;
    }


    /**
     * Gets the merchantCode value for this AdditionalInstructions.
     *
     * @return merchantCode
     */
    public java.lang.String getMerchantCode() {
        return merchantCode;
    }


    /**
     * Sets the merchantCode value for this AdditionalInstructions.
     *
     * @param merchantCode
     */
    public void setMerchantCode(java.lang.String merchantCode) {
        this.merchantCode = merchantCode;
    }


    /**
     * Gets the countryCode value for this AdditionalInstructions.
     *
     * @return countryCode
     */
    public java.lang.String getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this AdditionalInstructions.
     *
     * @param countryCode
     */
    public void setCountryCode(java.lang.String countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * Gets the currencyCode value for this AdditionalInstructions.
     *
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this AdditionalInstructions.
     *
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the acceptedCards value for this AdditionalInstructions.
     *
     * @return acceptedCards
     */
    public com.capita_software_services.www.scp.base.AcceptedCards getAcceptedCards() {
        return acceptedCards;
    }


    /**
     * Sets the acceptedCards value for this AdditionalInstructions.
     *
     * @param acceptedCards
     */
    public void setAcceptedCards(com.capita_software_services.www.scp.base.AcceptedCards acceptedCards) {
        this.acceptedCards = acceptedCards;
    }


    /**
     * Gets the language value for this AdditionalInstructions.
     *
     * @return language
     */
    public java.lang.String getLanguage() {
        return language;
    }


    /**
     * Sets the language value for this AdditionalInstructions.
     *
     * @param language
     */
    public void setLanguage(java.lang.String language) {
        this.language = language;
    }


    /**
     * Gets the stageIndicator value for this AdditionalInstructions.
     *
     * @return stageIndicator
     */
    public com.capita_software_services.www.scp.base.StageIndicator getStageIndicator() {
        return stageIndicator;
    }


    /**
     * Sets the stageIndicator value for this AdditionalInstructions.
     *
     * @param stageIndicator
     */
    public void setStageIndicator(com.capita_software_services.www.scp.base.StageIndicator stageIndicator) {
        this.stageIndicator = stageIndicator;
    }


    /**
     * Gets the responseInterface value for this AdditionalInstructions.
     *
     * @return responseInterface
     */
    public com.capita_software_services.www.scp.base.ResponseInterface getResponseInterface() {
        return responseInterface;
    }


    /**
     * Sets the responseInterface value for this AdditionalInstructions.
     *
     * @param responseInterface
     */
    public void setResponseInterface(com.capita_software_services.www.scp.base.ResponseInterface responseInterface) {
        this.responseInterface = responseInterface;
    }


    /**
     * Gets the cardholderID value for this AdditionalInstructions.
     *
     * @return cardholderID
     */
    public java.lang.String getCardholderID() {
        return cardholderID;
    }


    /**
     * Sets the cardholderID value for this AdditionalInstructions.
     *
     * @param cardholderID
     */
    public void setCardholderID(java.lang.String cardholderID) {
        this.cardholderID = cardholderID;
    }


    /**
     * Gets the integrator value for this AdditionalInstructions.
     *
     * @return integrator
     */
    public org.apache.axis.types.PositiveInteger getIntegrator() {
        return integrator;
    }


    /**
     * Sets the integrator value for this AdditionalInstructions.
     *
     * @param integrator
     */
    public void setIntegrator(org.apache.axis.types.PositiveInteger integrator) {
        this.integrator = integrator;
    }


    /**
     * Gets the styleCode value for this AdditionalInstructions.
     *
     * @return styleCode
     */
    public java.lang.String getStyleCode() {
        return styleCode;
    }


    /**
     * Sets the styleCode value for this AdditionalInstructions.
     *
     * @param styleCode
     */
    public void setStyleCode(java.lang.String styleCode) {
        this.styleCode = styleCode;
    }


    /**
     * Gets the frameworkCode value for this AdditionalInstructions.
     *
     * @return frameworkCode
     */
    public java.lang.String getFrameworkCode() {
        return frameworkCode;
    }


    /**
     * Sets the frameworkCode value for this AdditionalInstructions.
     *
     * @param frameworkCode
     */
    public void setFrameworkCode(java.lang.String frameworkCode) {
        this.frameworkCode = frameworkCode;
    }


    /**
     * Gets the systemCode value for this AdditionalInstructions.
     *
     * @return systemCode
     */
    public java.lang.String getSystemCode() {
        return systemCode;
    }


    /**
     * Sets the systemCode value for this AdditionalInstructions.
     *
     * @param systemCode
     */
    public void setSystemCode(java.lang.String systemCode) {
        this.systemCode = systemCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdditionalInstructions)) return false;
        AdditionalInstructions other = (AdditionalInstructions) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.merchantCode==null && other.getMerchantCode()==null) ||
             (this.merchantCode!=null &&
              this.merchantCode.equals(other.getMerchantCode()))) &&
            ((this.countryCode==null && other.getCountryCode()==null) ||
             (this.countryCode!=null &&
              this.countryCode.equals(other.getCountryCode()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) ||
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.acceptedCards==null && other.getAcceptedCards()==null) ||
             (this.acceptedCards!=null &&
              this.acceptedCards.equals(other.getAcceptedCards()))) &&
            ((this.language==null && other.getLanguage()==null) ||
             (this.language!=null &&
              this.language.equals(other.getLanguage()))) &&
            ((this.stageIndicator==null && other.getStageIndicator()==null) ||
             (this.stageIndicator!=null &&
              this.stageIndicator.equals(other.getStageIndicator()))) &&
            ((this.responseInterface==null && other.getResponseInterface()==null) ||
             (this.responseInterface!=null &&
              this.responseInterface.equals(other.getResponseInterface()))) &&
            ((this.cardholderID==null && other.getCardholderID()==null) ||
             (this.cardholderID!=null &&
              this.cardholderID.equals(other.getCardholderID()))) &&
            ((this.integrator==null && other.getIntegrator()==null) ||
             (this.integrator!=null &&
              this.integrator.equals(other.getIntegrator()))) &&
            ((this.styleCode==null && other.getStyleCode()==null) ||
             (this.styleCode!=null &&
              this.styleCode.equals(other.getStyleCode()))) &&
            ((this.frameworkCode==null && other.getFrameworkCode()==null) ||
             (this.frameworkCode!=null &&
              this.frameworkCode.equals(other.getFrameworkCode()))) &&
            ((this.systemCode==null && other.getSystemCode()==null) ||
             (this.systemCode!=null &&
              this.systemCode.equals(other.getSystemCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMerchantCode() != null) {
            _hashCode += getMerchantCode().hashCode();
        }
        if (getCountryCode() != null) {
            _hashCode += getCountryCode().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getAcceptedCards() != null) {
            _hashCode += getAcceptedCards().hashCode();
        }
        if (getLanguage() != null) {
            _hashCode += getLanguage().hashCode();
        }
        if (getStageIndicator() != null) {
            _hashCode += getStageIndicator().hashCode();
        }
        if (getResponseInterface() != null) {
            _hashCode += getResponseInterface().hashCode();
        }
        if (getCardholderID() != null) {
            _hashCode += getCardholderID().hashCode();
        }
        if (getIntegrator() != null) {
            _hashCode += getIntegrator().hashCode();
        }
        if (getStyleCode() != null) {
            _hashCode += getStyleCode().hashCode();
        }
        if (getFrameworkCode() != null) {
            _hashCode += getFrameworkCode().hashCode();
        }
        if (getSystemCode() != null) {
            _hashCode += getSystemCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdditionalInstructions.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "additionalInstructions"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchantCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "merchantCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "countryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "currencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acceptedCards");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "acceptedCards"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "acceptedCards"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("language");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "language"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stageIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "stageIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "stageIndicator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseInterface");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "responseInterface"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "responseInterface"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardholderID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardholderID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("integrator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "integrator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "positiveInteger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("styleCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "styleCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("frameworkCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "frameworkCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "systemCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
