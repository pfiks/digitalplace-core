package com.placecube.digitalplace.form.service.impl.internal;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceSettings;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.util.DDMFormInstanceFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.WorkflowDefinitionLink;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.WorkflowDefinitionLinkLocalService;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = WorkflowDefinitionService.class)
public class WorkflowDefinitionService {

	@Reference
	private WorkflowDefinitionLinkLocalService workflowDefinitionLinkLocalService;

	public void updateWorkflowDefinitionLink(DDMFormInstance ddmFormInstance, DDMFormValues settingsDDMFormValues, ServiceContext serviceContext) throws PortalException {

		String workflowDefinition = getWorkflowDefinition(settingsDDMFormValues);

		if (!"no-workflow".equalsIgnoreCase(workflowDefinition)) {

			WorkflowDefinitionLink workflowDefinitionLink = workflowDefinitionLinkLocalService.fetchWorkflowDefinitionLink(serviceContext.getCompanyId(), ddmFormInstance.getGroupId(),
					DDMFormInstance.class.getName(), ddmFormInstance.getFormInstanceId(), 0);

			if (Validator.isNull(workflowDefinitionLink)) {
				workflowDefinitionLinkLocalService.addWorkflowDefinitionLink(serviceContext.getUserId(), serviceContext.getCompanyId(), ddmFormInstance.getGroupId(), DDMFormInstance.class.getName(),
						ddmFormInstance.getFormInstanceId(), 0, workflowDefinition, 1);

			} else {
				workflowDefinitionLinkLocalService.updateWorkflowDefinitionLink(serviceContext.getUserId(), serviceContext.getCompanyId(), ddmFormInstance.getGroupId(),
						DDMFormInstance.class.getName(), ddmFormInstance.getFormInstanceId(), 0, workflowDefinition);
			}
		}
	}

	private String getWorkflowDefinition(DDMFormValues ddmFormValues) {

		DDMFormInstanceSettings ddmFormInstanceSettings = DDMFormInstanceFactory.create(DDMFormInstanceSettings.class, ddmFormValues);

		return ddmFormInstanceSettings.workflowDefinition();
	}

}
