package com.placecube.digitalplace.ddl.web.portlet.display.service;

import java.util.List;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.dynamic.data.lists.constants.DDLRecordConstants;
import com.liferay.dynamic.data.lists.constants.DDLRecordSetConstants;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.lists.service.DDLRecordService;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalService;
import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.io.DDMFormDeserializer;
import com.liferay.dynamic.data.mapping.io.DDMFormDeserializerDeserializeRequest;
import com.liferay.dynamic.data.mapping.io.DDMFormDeserializerDeserializeResponse;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializer;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializerDeserializeRequest;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializerDeserializeResponse;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;

@Component(immediate = true, service = DDLEditorService.class)
public class DDLEditorService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLEditorService.class);
	private static final String REDIRECT = "redirect";

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private DDLPermissionService ddlPermissionService;

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private DDLRecordService ddlRecordService;

	@Reference
	private DDLRecordSetLocalService ddlRecordSetLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDMTemplateService ddmTemplateService;

	@Reference(target = "(ddm.form.deserializer.type=json)")
	private DDMFormDeserializer jsonDDMFormDeserializer;

	@Reference(target = "(ddm.form.values.deserializer.type=json)")
	private DDMFormValuesDeserializer jsonDDMFormValuesDeserializer;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	@Reference
	private PortletURLFactory portletURLFactory;

	public void configureBackLinkURL(ThemeDisplay themeDisplay, String url, RenderRequest renderRequest) {
		PortletDisplay portletDisplay = themeDisplay.getPortletDisplay();
		portletDisplay.setShowBackIcon(true);
		portletDisplay.setURLBack(url);
		renderRequest.setAttribute(REDIRECT, url);
	}

	public String getBackToDDLDisplayPortletURL(PortletRequest portletRequest, ThemeDisplay themeDisplay) {
		PortletURL liferayPortletURL = portletURLFactory.create(portletRequest, PortletKeys.DDL_DISPLAY_PORTLET, themeDisplay.getPlid(), PortletRequest.RENDER_PHASE);
		return liferayPortletURL.toString();
	}

	public String getBackURL(PortletRequest renderRequest, ThemeDisplay themeDisplay) {
		String redirect = getRedirectParamValue(renderRequest);

		if (Validator.isNotNull(redirect)) {
			return redirect;
		}
		return getBackToDDLDisplayPortletURL(renderRequest, themeDisplay);
	}

	public Optional<DDLRecord> getDDLRecord(long ddlRecordId) {
		try {
			return Optional.of(ddlRecordService.getRecord(ddlRecordId));
		} catch (PortalException e) {
			LOG.debug(e);
			return Optional.empty();
		}
	}

	public DDMForm getDDMFormForUpdate(long ddmStructureId, long ddmFormTemplateId) throws PortalException {

		if (ddmFormTemplateId > 0) {

			DDMTemplate ddmTemplate = ddmTemplateService.getTemplate(ddmFormTemplateId);
			DDMFormDeserializerDeserializeRequest.Builder builder = DDMFormDeserializerDeserializeRequest.Builder.newBuilder(ddmTemplate.getScript());
			DDMFormDeserializerDeserializeResponse ddmFormDeserializerDeserializeResponse = jsonDDMFormDeserializer.deserialize(builder.build());
			return ddmFormDeserializerDeserializeResponse.getDDMForm();

		}
		DDMStructure ddmStructure = ddmStructureLocalService.getDDMStructure(ddmStructureId);
		return ddmStructure.getFullHierarchyDDMForm();
	}

	public DDMFormValues getDDMFormValuesFromDeserialisedContent(String content, DDMForm ddmForm) {
		DDMFormValuesDeserializerDeserializeRequest.Builder builder = DDMFormValuesDeserializerDeserializeRequest.Builder.newBuilder(content, ddmForm);

		DDMFormValuesDeserializerDeserializeResponse ddmFormValuesDeserializerDeserializeResponse = jsonDDMFormValuesDeserializer.deserialize(builder.build());

		return ddmFormValuesDeserializerDeserializeResponse.getDDMFormValues();
	}

	public Optional<DDMStructure> getDDMStructure(long ddmStructureId) {
		return Optional.ofNullable(ddmStructureLocalService.fetchDDMStructure(ddmStructureId));
	}

	public String getFieldsToAutofill(String[] autofillFields) {
		if (!ArrayUtil.isEmpty(autofillFields)) {
			JSONArray result = jsonFactory.createJSONArray();
			for (String entry : autofillFields) {
				if (Validator.isNotNull(entry) && entry.contains("=")) {
					int indexOf = entry.indexOf("=");
					String fieldName = entry.substring(0, indexOf);
					String fieldValue = entry.substring(indexOf + 1);
					if (Validator.isNotNull(fieldName) && Validator.isNotNull(fieldValue)) {
						JSONObject entryJson = jsonFactory.createJSONObject();
						entryJson.put("fieldName", fieldName);
						entryJson.put("fieldValue", fieldValue);
						result.put(entryJson);
					}
				}
			}
			if (result.length() > 0) {
				return result.toJSONString();
			}
		}
		return StringPool.BLANK;
	}

	public Optional<DDMTemplate> getFormTemplate(DDMStructure ddmStructure) {
		return ddmStructure.getTemplates().stream().filter(tpl -> DDMTemplateConstants.TEMPLATE_TYPE_FORM.equalsIgnoreCase(tpl.getType())).findFirst();
	}

	public DDLRecord getOrCreateDDLRecord(long ddlRecordId, DDMFormValues ddmFormValues, long ddlRecordSetId, ServiceContext serviceContext, PermissionChecker permissionChecker)
			throws PortalException {

		DDLRecord record = null;
		if (ddlRecordId > 0) {
			ddlPermissionService.validateUpdatePermission(permissionChecker, ddlRecordId);
			record = ddlRecordLocalService.updateRecord(permissionChecker.getUserId(), ddlRecordId, true, DDLRecordConstants.DISPLAY_INDEX_DEFAULT, ddmFormValues, serviceContext);
		} else {
			record = ddlRecordService.addRecord(serviceContext.getScopeGroupId(), ddlRecordSetId, DDLRecordConstants.DISPLAY_INDEX_DEFAULT, ddmFormValues, serviceContext);
		}

		assetEntryLocalService.updateEntry(record.getUserId(), record.getGroupId(), DDLRecord.class.getName(), record.getRecordId(), serviceContext.getAssetCategoryIds(),
				serviceContext.getAssetTagNames());
		return record;
	}

	public DDLRecordSet getOrCreateDDLRecordSet(DDMStructure ddmStructure, ServiceContext serviceContext) throws PortalException {
		long groupId = serviceContext.getScopeGroupId();
		long structureId = ddmStructure.getStructureId();

		List<DDLRecordSet> recordSets = ddlRecordSetLocalService.getRecordSets(groupId);

		Optional<DDLRecordSet> ddlRecordSetOpt = recordSets.stream().filter(recordSet -> recordSet.getDDMStructureId() == structureId).findFirst();

		if (ddlRecordSetOpt.isPresent()) {
			return ddlRecordSetOpt.get();

		}
		ddlPermissionService.configureDefaultDDLRecordSetPermissionsInServiceContext(serviceContext);

		return ddlRecordSetLocalService.addRecordSet(serviceContext.getUserId(), groupId, structureId, null, ddmStructure.getNameMap(), ddmStructure.getDescriptionMap(),
				DDLRecordSetConstants.MIN_DISPLAY_ROWS_DEFAULT, DDLRecordSetConstants.SCOPE_DYNAMIC_DATA_LISTS, serviceContext);
	}

	public String getRedirectParamValue(PortletRequest portletRequest) {
		String redirect = ParamUtil.getString(portletRequest, REDIRECT);
		if (Validator.isNull(redirect)) {
			HttpServletRequest httpServletRequest = portal.getHttpServletRequest(portletRequest);
			HttpServletRequest originalServletRequest = portal.getOriginalServletRequest(httpServletRequest);
			redirect = originalServletRequest.getParameter(REDIRECT);
		}

		return GetterUtil.getString(redirect);
	}

	public ServiceContext getServiceContextForScopeGroup(ThemeDisplay themeDisplay) {
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		serviceContext.setScopeGroupId(themeDisplay.getScopeGroupId());
		return serviceContext;
	}

	public ServiceContext getServiceContextForUpdate(ActionRequest actionRequest, ThemeDisplay themeDisplay) throws PortalException {
		ServiceContext serviceContext = ServiceContextFactory.getInstance(DDLRecord.class.getName(), actionRequest);
		serviceContext.setScopeGroupId(themeDisplay.getScopeGroupId());
		return serviceContext;
	}

}