package com.placecube.digitalplace.user.account.web.service;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Calendar;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountDateOfBirthService;

@Component(immediate = true, service = AccountDateOfBirthService.class)
public class AccountDateOfBirthServiceImpl implements AccountDateOfBirthService {

	@Override
	public LocalDate getDateOfBirthFromContext(AccountContext accountContext) {
		try {
			return LocalDate.of(accountContext.getBirthYear(), accountContext.getBirthMonth(), accountContext.getBirthDay());
		} catch (DateTimeException e) {
			return getDefaultDateOfBirth();
		}
	}

	@Override
	public LocalDate getDefaultDateOfBirth() {
		return LocalDate.of(1800, 1, 1);
	}

	@Override
	public void populateContextWithExistingDateOfBirth(AccountContext accountContext, User user) throws UserAccountException {
		try{
			Contact contact = user.getContact();
			Calendar birthdayCalendar = CalendarFactoryUtil.getCalendar();
			birthdayCalendar.setTime(contact.getBirthday());

			accountContext.setBirthDay(birthdayCalendar.get(Calendar.DATE));
			accountContext.setBirthMonth(birthdayCalendar.get(Calendar.MONTH) + 1);
			accountContext.setBirthYear(birthdayCalendar.get(Calendar.YEAR));
		}catch (PortalException e) {
			throw new UserAccountException(e);
		}

	}

}
