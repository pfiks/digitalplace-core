package com.placecube.digitalplace.appointment.web.constants;

public final class PortletKeys {

	public static final String APPOINTMENTS_DASHBOARD = "com_placecube_digitalplace_appointment_web_AppointmentDashboard";

	private PortletKeys() {
	}
}