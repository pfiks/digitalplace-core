package com.placecube.digitalplace.userimport.admin.web.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.userimport.admin.web.constants.NotificationArticle;
import com.placecube.digitalplace.userimport.admin.web.service.UserImportNotificationService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class UserImportLifecycleListener extends BasePortalInstanceLifecycleListener {

	@Reference
	private ServiceContextFactory serviceContextFactory;

	@Reference
	private UserImportNotificationService userImportNotificationService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {

		ServiceContext serviceContext = serviceContextFactory.createServiceContext(company.getGroup().getGroupId(), company.getGuestUser().getUserId());

		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);

		JournalFolder journalFolder = userImportNotificationService.getOrCreateFolder(serviceContext);
		DDMStructure emailStructure = userImportNotificationService.getEmailStructure(serviceContext);

		userImportNotificationService.createMissingArticle(NotificationArticle.IMPORT_COMPLETED, journalFolder, emailStructure, false, serviceContext);
		userImportNotificationService.createMissingArticle(NotificationArticle.IMPORT_FAILED, journalFolder, emailStructure, false, serviceContext);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		// Implementation not required
	}
}
