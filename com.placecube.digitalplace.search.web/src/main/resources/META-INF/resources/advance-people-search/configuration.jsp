<%@ include file="../init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post">

    <aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
    <aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />

    <liferay-frontend:edit-form-body>
        <dp-frontend:fieldset-group>

            <liferay-frontend:fieldset>

                <aui:field-wrapper label="title">
                    <liferay-ui:input-localized
                            defaultLanguageId="<%= LocaleUtil.toLanguageId(themeDisplay.getSiteDefaultLocale()) %>"
                            name="title"
                            xml="${title}"
                    />
                </aui:field-wrapper>

                <aui:input
                        label="filter-colour-label"
                        name="filterColour"
                        type="text"
                        value="${ filterColour }"
                />

                <aui:field-wrapper label="search-fields-label">

                    <aui:input
                            label=""
                            name="searchFields"
                            type="textarea"
                            value="${searchFields}"
                    />

                    <div class="form-text">
                        <liferay-ui:message key="search-fields-label-description" />
                    </div>

                </aui:field-wrapper>

                <aui:input name="applyPrivacy" label="apply-privacy-to-fields" type="toggle-switch" value="${applyPrivacy}" />

            </liferay-frontend:fieldset>

        </dp-frontend:fieldset-group>
    </liferay-frontend:edit-form-body>

    <liferay-frontend:edit-form-footer>
        <aui:button type="submit" />

        <aui:button type="cancel" />
    </liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>