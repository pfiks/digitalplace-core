package com.placecube.digitalplace.address.service.impl;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookup;

@Component(immediate = true, service = AddressLookupHelper.class)
public class AddressLookupHelper {

	private static final Log LOG = LogFactoryUtil.getLog(AddressLookupHelper.class);

	public Set<AddressContext> fetchKeyword(long companyId, String keyword, String[] paramsAndValues, List<AddressLookup> localAddressLookupList) {

		Set<AddressContext> addressContexts = Collections.emptySet();

		for (Iterator<AddressLookup> iterator = localAddressLookupList.iterator(); iterator.hasNext() && addressContexts.isEmpty();) {
			addressContexts = iterator.next().findByKeyword(companyId, keyword, paramsAndValues);
		}

		return addressContexts;
	}

	public Set<AddressContext> fetchPostcode(long companyId, String postcode, String[] paramsAndValues, List<AddressLookup> localAddressLookupList) {

		Set<AddressContext> addressContexts = Collections.emptySet();

		for (Iterator<AddressLookup> iterator = localAddressLookupList.iterator(); iterator.hasNext() && addressContexts.isEmpty();) {
			addressContexts = iterator.next().findByPostcode(companyId, postcode, paramsAndValues);
		}

		return addressContexts;
	}

	public Optional<AddressContext> fetchUprn(long companyId, String uprn, String[] paramsAndValues, List<AddressLookup> addressLookupList) {

		for (AddressLookup addressLookup : addressLookupList) {
			Optional<AddressContext> addressContext = addressLookup.findByUprn(companyId, uprn, paramsAndValues);
			if (addressContext.isPresent()) {
				LOG.debug("addressContext found for uprn: " + uprn + " using connector: " + addressLookup.getClass().getName());
				return addressContext;
			}
		}

		return Optional.empty();
	}

	public List<AddressLookup> getLocalAddressLookup(long companyId, Set<AddressLookup> addressLookups) {

		return filter(companyId, addressLookups, false);

	}

	public List<AddressLookup> getNationalAddressLookup(long companyId, Set<AddressLookup> addressLookups) {

		return filter(companyId, addressLookups, true);

	}

	public void validateLookups(List<AddressLookup> addressLookups) throws PortalException {

		if (addressLookups.isEmpty()) {
			throw new PortalException("No address lookup configured");
		}

	}

	private List<AddressLookup> filter(long companyId, Set<AddressLookup> addressLookups, boolean national) {
		List<AddressLookup> addressLookUpList = addressLookups.stream().filter(entry -> entry.enabled(companyId) && entry.national(companyId) == national).collect(Collectors.toList());
		Collections.sort(addressLookUpList, (e1, e2) -> Integer.compare(e1.getWeight(companyId), e2.getWeight(companyId)));
		return addressLookUpList;
	}

}
