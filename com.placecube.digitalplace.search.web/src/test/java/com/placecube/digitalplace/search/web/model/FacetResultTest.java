package com.placecube.digitalplace.search.web.model;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.util.LocaleUtil;
import com.placecube.digitalplace.search.shared.model.FacetResult;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest(LocaleUtil.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class FacetResultTest extends PowerMockito {

	@Before
	public void activateSetup() {
		mockStatic(LocaleUtil.class);
	}

	@Test
	@Parameters({ "true", "false" })
	public void init_WithValueAndFrequencyAndSelectedAndLabelMapParameters_ThenInitialisesTheFacetResultWithTheSpecifiedValues(boolean selected) {

		Map<Locale, String> labelMap = new LinkedHashMap<>();
		labelMap.put(Locale.CANADA_FRENCH, "canadaFrenchValue");
		labelMap.put(Locale.ITALIAN, "italianValue");

		String value = "facetValue";
		int frequency = 11;

		FacetResult result = FacetResult.init(value, frequency, selected, labelMap);

		assertThat(result.getValue(), equalTo(value));
		assertThat(result.getFrequency(), equalTo(frequency));
		assertThat(result.getLabel(Locale.ITALIAN), equalTo("italianValue"));
		assertThat(result.getLabel(Locale.CANADA_FRENCH), equalTo("canadaFrenchValue"));
		assertThat(result.getLabel(Locale.CHINESE), equalTo("canadaFrenchValue"));
		assertThat(result.isSelected(), equalTo(selected));
	}

	@Test
	@Parameters({ "true", "false" })
	public void init_WithValueAndFrequencyAndSelectedParameters_ThenInitialisesTheFacetResultWithTheSpecifiedValuesAndTheValueAsDefaultLabel(boolean selected) {
		Locale locale = Locale.CANADA_FRENCH;
		when(LocaleUtil.getDefault()).thenReturn(locale);

		String value = "facetValue";
		int frequency = 11;

		FacetResult result = FacetResult.init(value, frequency, selected);

		assertThat(result.getValue(), equalTo(value));
		assertThat(result.getFrequency(), equalTo(frequency));
		assertThat(result.getLabel(Locale.ITALIAN), equalTo(value));
		assertThat(result.isSelected(), equalTo(selected));
	}
}
