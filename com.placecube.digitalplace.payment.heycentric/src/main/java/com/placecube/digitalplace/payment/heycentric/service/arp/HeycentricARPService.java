package com.placecube.digitalplace.payment.heycentric.service.arp;

import java.security.GeneralSecurityException;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.heycentric.configuration.HeycentricPaymentCompanyConfiguration;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.heycentric.service.internal.arp.HeycentricARPLineService;
import com.placecube.digitalplace.payment.heycentric.service.internal.arp.HeycentricARPResponseParser;
import com.placecube.digitalplace.payment.heycentric.service.internal.arp.HeycentricARPSecurityUtil;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;

@Component(immediate = true, service = HeycentricARPService.class)
public class HeycentricARPService {

	private static final Log LOG = LogFactoryUtil.getLog(HeycentricARPService.class);

	@Reference
	private Portal portal;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private HeycentricARPLineService heycentricARPLineService;

	@Reference
	private HeycentricARPSecurityUtil heycentricARPSecurityUtil;

	@Reference
	private HeycentricARPResponseParser heycentricARPResponseParser;

	public PaymentResponse processAgentReferredPaymentResponse(HttpServletRequest request, String externalReferenceCode) throws PortalException, GeneralSecurityException {
		String payLog = ParamUtil.getString(request, HeycentricPaymentConstants.JSON_PAYLOAD_PAYLOG);

		if (Validator.isNotNull(payLog)) {
			HeycentricPaymentCompanyConfiguration heycentricPaymentCompanyConfiguration = configurationProvider.getCompanyConfiguration(HeycentricPaymentCompanyConfiguration.class,
					portal.getCompanyId(request));

			JSONObject webhookPayload = jsonFactory.createJSONObject(payLog);

			String payLoadDigest = webhookPayload.getString(HeycentricPaymentConstants.JSON_PAYLOAD_DIGEST);
			String sid = webhookPayload.getString(HeycentricPaymentConstants.JSON_PAYLOAD_SID);
			String agentId = webhookPayload.getString(HeycentricPaymentConstants.JSON_PAYLOAD_AGENT_ID);
			String secret = heycentricPaymentCompanyConfiguration.arpSecret();

			if (heycentricARPSecurityUtil.isValidData(externalReferenceCode, sid, agentId, secret, payLoadDigest)) {
				return heycentricARPResponseParser.parsePaymentResponse(webhookPayload);
			} else {
				LOG.error("Invalid payment data received, payLog: " + payLog);
				return getErrorPaymentResponse("Invalid payment data received");
			}
		} else {
			LOG.error("Invalid payment data received. No payLog present in response");
			return getErrorPaymentResponse("Invalid payment data received");
		}
	}

	public JSONObject getAgentReferredPaymentRequestPayload(AgentReferredPaymentContext agentReferredPaymentContext, HeycentricPaymentCompanyConfiguration heycentricPaymentCompanyConfiguration) throws GeneralSecurityException {
		JSONObject arpRequestPayload = jsonFactory.createJSONObject();

		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_BU, heycentricPaymentCompanyConfiguration.arpExternalSystemName());
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_SID, agentReferredPaymentContext.getSid());
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_PCN, agentReferredPaymentContext.getCallerPhoneNumber());
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_EMAIL, agentReferredPaymentContext.getCallerEmail());
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_CCEMAIL, heycentricPaymentCompanyConfiguration.arpGroupReceiptEmail());
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_ICB, Boolean.toString(agentReferredPaymentContext.isImmediateCallBack()));

		String returnUrl = agentReferredPaymentContext.getReturnUrl();
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_RURL, returnUrl);
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_LOGURL, agentReferredPaymentContext.getLogUrl());

		User user = agentReferredPaymentContext.getUser();
		long userId = user.getUserId();

		String digest = heycentricARPSecurityUtil.generateDigest(returnUrl, userId, heycentricPaymentCompanyConfiguration.arpSecret());
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_DIGEST, digest);

		String agentEmail = user.getEmailAddress();
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_AGENT_ID, userId);
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_AGENT_EMAIL, agentEmail);
		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_ACN, agentReferredPaymentContext.getAgentPhoneNumber());

		arpRequestPayload.put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEMS, heycentricARPLineService.agentReferredPaymentLinesToJSON(agentReferredPaymentContext));

		return arpRequestPayload;
	}

	private PaymentResponse getErrorPaymentResponse(String errorMessage) {
		return new PaymentResponse(StringPool.BLANK, PaymentStatus.paymentError(StringPool.BLANK, errorMessage));
	}

}
