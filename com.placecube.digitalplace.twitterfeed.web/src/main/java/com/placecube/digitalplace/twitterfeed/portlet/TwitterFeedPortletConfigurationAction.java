package com.placecube.digitalplace.twitterfeed.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.twitterfeed.configuration.TwitterFeedConfigurationUtil;
import com.placecube.digitalplace.twitterfeed.constants.PortletKeys;
import com.placecube.digitalplace.twitterfeed.constants.PortletRequestKeys;

@Component(configurationPid = "TwitterFeedPortletInstanceConfiguration", configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true, property = {
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.name=" + PortletKeys.TWITTER }, service = ConfigurationAction.class)
public class TwitterFeedPortletConfigurationAction extends DefaultConfigurationAction {

	private static final Log LOG = LogFactoryUtil.getLog(TwitterFeedPortletConfigurationAction.class);

	@Reference
	private TwitterFeedConfigurationUtil twitterFeedConfigurationUtil;

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Long feedsToShow = ParamUtil.getLong(actionRequest, PortletRequestKeys.FEEDS_TO_SHOW, 0);
		String twitterUsername = ParamUtil.getString(actionRequest, PortletRequestKeys.TWITTER_USERNAME, StringPool.BLANK);
		String twitterEmbedded = ParamUtil.getString(actionRequest, PortletRequestKeys.TWITTER_EMBEDDED, StringPool.BLANK);
		LOG.debug("Updating portlet preferences - TwitterFeedPortletConfigurationAction: " + feedsToShow + ", twitterUsername: " + twitterUsername);

		setPreference(actionRequest, PortletRequestKeys.FEEDS_TO_SHOW, String.valueOf(feedsToShow));
		setPreference(actionRequest, PortletRequestKeys.TWITTER_USERNAME, twitterUsername);
		setPreference(actionRequest, PortletRequestKeys.TWITTER_EMBEDDED, twitterEmbedded);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		httpServletRequest.setAttribute(PortletRequestKeys.CONFIGURATION, twitterFeedConfigurationUtil.getConfiguration(themeDisplay));
		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}



}
