package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.settings.LocalizedValuesMap;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, LocaleUtil.class, LocalizationUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class AdvancePeopleSearchPortletConfigurationActionTest extends PowerMockito {

	private static final String CONFIGURED_SEARCH_FIELDS = "[]";

	private static final String FILTER_COLOUR = "filterColour";

	private static final Locale LOCALE = Locale.UK;

	@InjectMocks
	private AdvancePeopleSearchPortletConfigurationAction advancePeopleSearchPortletConfigurationAction;

	@Mock
	private AdvancePeopleSearchPortletInstanceConfiguration configuration;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private PortletPreferences mockPreferences;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private LocalizedValuesMap mockTitleMap;

	@Mock
	private Map<Locale, String> mockTitleMapValues;

	@Before
	public void activateSetup() {
		mockStatic(LocaleUtil.class, LocalizationUtil.class);
	}

	@Test(expected = ConfigurationException.class)
	public void include_WhenConfigurationExceptionIsThrown_ThenThrowsException() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(AdvancePeopleSearchPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		advancePeopleSearchPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void include_WhenNoError_ThenSetsConfigurationsAsRequestAttributes() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));

		String title = "titleXmlValue";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(AdvancePeopleSearchPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(configuration);
		when(configuration.applyPrivacy()).thenReturn(Boolean.FALSE);
		when(configuration.filterColour()).thenReturn(FILTER_COLOUR);
		when(configuration.searchFields()).thenReturn(CONFIGURED_SEARCH_FIELDS);
		when(configuration.title()).thenReturn(mockTitleMap);
		when(configuration.title().getValues()).thenReturn(mockTitleMapValues);
		when(LocaleUtil.getSiteDefault()).thenReturn(LOCALE);
		when(LocaleUtil.toLanguageId(LOCALE)).thenReturn("en-gb");
		when(LocalizationUtil.updateLocalization(mockTitleMapValues, StringPool.BLANK, "title", LocaleUtil.toLanguageId(LocaleUtil.getSiteDefault()))).thenReturn(title);

		advancePeopleSearchPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("applyPrivacy", Boolean.FALSE);
		verify(mockHttpServletRequest, times(1)).setAttribute("filterColour", FILTER_COLOUR);
		verify(mockHttpServletRequest, times(1)).setAttribute("searchFields", CONFIGURED_SEARCH_FIELDS);
		verify(mockHttpServletRequest, times(1)).setAttribute("title", title);
	}

	@Test
	public void processAction_WhenNoError_Then() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		advancePeopleSearchPortletConfigurationAction = spy(new AdvancePeopleSearchPortletConfigurationAction());

		when(ParamUtil.getString(mockActionRequest, FILTER_COLOUR, StringPool.BLANK)).thenReturn(FILTER_COLOUR);
		when(ParamUtil.getString(mockActionRequest, "searchFields")).thenReturn(CONFIGURED_SEARCH_FIELDS);
		when(mockActionRequest.getPreferences()).thenReturn(mockPreferences);

		advancePeopleSearchPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(advancePeopleSearchPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "applyPrivacy", String.valueOf(Boolean.FALSE));
		verify(advancePeopleSearchPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "filterColour", FILTER_COLOUR);
		verify(advancePeopleSearchPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "searchFields", CONFIGURED_SEARCH_FIELDS);
		verifyStatic(LocalizationUtil.class, times(1));
		LocalizationUtil.setLocalizedPreferencesValues(mockActionRequest, mockPreferences, "title");
	}

}
