/**
 * Card.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class Card  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.CardDetails cardDetails;

    private com.capita_software_services.www.scp.base.StoredCardKey storedCardKey;

    private java.lang.String paymentGroupCode;

    public Card() {
    }

    public Card(
           com.capita_software_services.www.scp.base.CardDetails cardDetails,
           com.capita_software_services.www.scp.base.StoredCardKey storedCardKey,
           java.lang.String paymentGroupCode) {
           this.cardDetails = cardDetails;
           this.storedCardKey = storedCardKey;
           this.paymentGroupCode = paymentGroupCode;
    }


    /**
     * Gets the cardDetails value for this Card.
     *
     * @return cardDetails
     */
    public com.capita_software_services.www.scp.base.CardDetails getCardDetails() {
        return cardDetails;
    }


    /**
     * Sets the cardDetails value for this Card.
     *
     * @param cardDetails
     */
    public void setCardDetails(com.capita_software_services.www.scp.base.CardDetails cardDetails) {
        this.cardDetails = cardDetails;
    }


    /**
     * Gets the storedCardKey value for this Card.
     *
     * @return storedCardKey
     */
    public com.capita_software_services.www.scp.base.StoredCardKey getStoredCardKey() {
        return storedCardKey;
    }


    /**
     * Sets the storedCardKey value for this Card.
     *
     * @param storedCardKey
     */
    public void setStoredCardKey(com.capita_software_services.www.scp.base.StoredCardKey storedCardKey) {
        this.storedCardKey = storedCardKey;
    }


    /**
     * Gets the paymentGroupCode value for this Card.
     *
     * @return paymentGroupCode
     */
    public java.lang.String getPaymentGroupCode() {
        return paymentGroupCode;
    }


    /**
     * Sets the paymentGroupCode value for this Card.
     *
     * @param paymentGroupCode
     */
    public void setPaymentGroupCode(java.lang.String paymentGroupCode) {
        this.paymentGroupCode = paymentGroupCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Card)) return false;
        Card other = (Card) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.cardDetails==null && other.getCardDetails()==null) ||
             (this.cardDetails!=null &&
              this.cardDetails.equals(other.getCardDetails()))) &&
            ((this.storedCardKey==null && other.getStoredCardKey()==null) ||
             (this.storedCardKey!=null &&
              this.storedCardKey.equals(other.getStoredCardKey()))) &&
            ((this.paymentGroupCode==null && other.getPaymentGroupCode()==null) ||
             (this.paymentGroupCode!=null &&
              this.paymentGroupCode.equals(other.getPaymentGroupCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardDetails() != null) {
            _hashCode += getCardDetails().hashCode();
        }
        if (getStoredCardKey() != null) {
            _hashCode += getStoredCardKey().hashCode();
        }
        if (getPaymentGroupCode() != null) {
            _hashCode += getPaymentGroupCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Card.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "card"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storedCardKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "storedCardKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "storedCardKey"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentGroupCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "paymentGroupCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
