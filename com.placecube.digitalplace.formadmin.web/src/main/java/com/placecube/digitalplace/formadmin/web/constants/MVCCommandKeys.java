package com.placecube.digitalplace.formadmin.web.constants;

public final class MVCCommandKeys {

	public static final String DATA_CONSUMER_DELETE = "/data-consumer-delete";

	public static final String DATA_CONSUMER_SAVE = "/data-consumer-save";

	public static final String DATA_CONSUMERS = "/data-consumers";

	public static final String DATA_CONSUMERS_MAPPING = "/data-consumer-mapping";

	public static final String EXPORT_FORM = "/export-form";

	public static final String INSTALL_FORM_DEFINTION = "/install-form-definition";

	public static final String SELECT_DATA_PROVIDER = "/select-data-provider";

	private MVCCommandKeys() {
	}
}