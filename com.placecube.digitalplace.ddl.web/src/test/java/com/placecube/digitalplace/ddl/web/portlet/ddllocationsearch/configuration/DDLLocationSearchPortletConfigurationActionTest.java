package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLSearchListingsConfigurationConstants;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class DDLLocationSearchPortletConfigurationActionTest extends PowerMockito {

	private static final String DESTINATION_PAGE = "Destination";

	private static final String GEO_LOCATION_FIELD = "geo_location_field";

	@InjectMocks
	private DDLLocationSearchPortletConfigurationAction dDLLocationSearchPortletConfigurationAction;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private DDLConfigurationService mockDDLConfigurationService;

	@Mock
	private DDLLocationSearchPortletInstanceConfiguration mockDDLLocationSearchPortletInstanceConfiguration;

	@Mock
	private List<DDMStructure> mockDDMStructures;

	@Mock
	private Group mockGroup;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheAvailableStructuresAndDestinationPageAndGeoLocationFieldAsRequestAttributes() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLLocationSearchPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockDDLLocationSearchPortletInstanceConfiguration);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDDLConfigurationService.getAvailableDDMStructures(mockGroup)).thenReturn(mockDDMStructures);

		when(mockDDLLocationSearchPortletInstanceConfiguration.destinationPage()).thenReturn(DESTINATION_PAGE);
		when(mockDDLLocationSearchPortletInstanceConfiguration.geoLocationField()).thenReturn(GEO_LOCATION_FIELD);

		dDLLocationSearchPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("availableStructures", mockDDMStructures);
		verify(mockHttpServletRequest, times(1)).setAttribute(DDLSearchListingsConfigurationConstants.DESTINATION_PAGE, mockDDLLocationSearchPortletInstanceConfiguration.destinationPage());
		verify(mockHttpServletRequest, times(1)).setAttribute(DDLSearchListingsConfigurationConstants.GEOLOCATION_FIELD, mockDDLLocationSearchPortletInstanceConfiguration.geoLocationField());
	}

	@Test
	public void include_WhenNoError_ThenSetsTheConfigurationAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLLocationSearchPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockDDLLocationSearchPortletInstanceConfiguration);

		dDLLocationSearchPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("configuration", mockDDLLocationSearchPortletInstanceConfiguration);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesDestinationAndDDMStructureIdAndGeoLocationFieldAsConfigurationPreferences() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		long ddmStructureId = 34;
		String destinationExpected = "/friendly-url";
		String geoLocationExpected = "location";
		when(ParamUtil.getLong(mockActionRequest, "ddmStructureId", 0)).thenReturn(ddmStructureId);
		when(ParamUtil.getString(mockActionRequest, DDLSearchListingsConfigurationConstants.DESTINATION_PAGE, StringPool.BLANK)).thenReturn(destinationExpected);
		when(ParamUtil.getString(mockActionRequest, DDLSearchListingsConfigurationConstants.GEOLOCATION_FIELD, StringPool.BLANK)).thenReturn(geoLocationExpected);
		dDLLocationSearchPortletConfigurationAction = spy(new DDLLocationSearchPortletConfigurationAction());

		dDLLocationSearchPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(dDLLocationSearchPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "ddmStructureId", String.valueOf(ddmStructureId));
		verify(dDLLocationSearchPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLSearchListingsConfigurationConstants.DESTINATION_PAGE, destinationExpected);
		verify(dDLLocationSearchPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLSearchListingsConfigurationConstants.GEOLOCATION_FIELD, geoLocationExpected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesDestinationPageInPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String expected = "/friendly-url";
		when(ParamUtil.getString(mockActionRequest, DDLSearchListingsConfigurationConstants.DESTINATION_PAGE, StringPool.BLANK)).thenReturn(expected);
		dDLLocationSearchPortletConfigurationAction = spy(new DDLLocationSearchPortletConfigurationAction());

		dDLLocationSearchPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(dDLLocationSearchPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLSearchListingsConfigurationConstants.DESTINATION_PAGE, expected);
	}

}