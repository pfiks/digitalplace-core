package com.placecube.digitalplace.search.internal.model;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SharedSearchSessionTest extends PowerMockito {

	private static final String ATTRIBUTE_NAME_1 = "attrName1";
	private static final String ATTRIBUTE_NAME_2 = "attrName2";
	private static final String COLOUR = "Orange";
	private static final String FILTER_NAME = "filter1";
	private static final long PLID = 10;
	private static final String VALUE_A = "valueA";
	private static final String VALUE_B = "valueB";

	@Test
	@Parameters({ "filterName,filterValue,", "filterName,,filterLabel", ",filterValue,filterLabel", "filterName,," })
	public void addActiveFilter_WhenAttributeAreInvalid_ThenNoFiltersAreAdded(String filterName, String filterValue, String filterLabel) {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addActiveFilter(filterName, filterValue, filterLabel, null);

		List<SharedActiveFilter> result = sharedSearchSession.getActiveFilters();
		assertThat(result.size(), equalTo(0));
	}

	@Test
	public void addActiveFilter_WhenAttributeNameAndValueAndLabelAndColourAreValid_ThenAddsActiveFilterIfNotAlreadyPresent() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addActiveFilter(FILTER_NAME, VALUE_A, ATTRIBUTE_NAME_1, COLOUR);
		sharedSearchSession.addActiveFilter(FILTER_NAME, VALUE_A, ATTRIBUTE_NAME_1, COLOUR);

		List<SharedActiveFilter> result = sharedSearchSession.getActiveFilters();

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(0).getName(), equalTo(FILTER_NAME));
		assertThat(result.get(0).getValue(), equalTo(VALUE_A));
		assertThat(result.get(0).getLabel(), equalTo(ATTRIBUTE_NAME_1));
		assertThat(result.get(0).getColour(), equalTo(COLOUR));
	}

	@Test
	public void addActiveFilter_WhenAttributeNameAndValueAndLabelAreValid_ThenAddsActiveFilterIfNotAlreadyPresent() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addActiveFilter(FILTER_NAME, VALUE_A, ATTRIBUTE_NAME_1, null);
		sharedSearchSession.addActiveFilter(FILTER_NAME, VALUE_A, ATTRIBUTE_NAME_1, null);

		List<SharedActiveFilter> result = sharedSearchSession.getActiveFilters();

		assertThat(result.size(), equalTo(1));
		assertThat(result.get(0).getName(), equalTo(FILTER_NAME));
		assertThat(result.get(0).getValue(), equalTo(VALUE_A));
		assertThat(result.get(0).getLabel(), equalTo(ATTRIBUTE_NAME_1));
		assertThat(result.get(0).getColour(), equalTo(null));
	}

	@Test
	public void addSingleValuedAttribute_WhenAttributeNameAndValueAreValid_ThenAddsTheValueToTheSpecifiedSingleValuedAttribute() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addSingleValuedAttribute(ATTRIBUTE_NAME_1, VALUE_A);
		sharedSearchSession.addSingleValuedAttribute(ATTRIBUTE_NAME_1, VALUE_B);

		assertThat(sharedSearchSession.getSingleValuedAttribute(ATTRIBUTE_NAME_1), equalTo(VALUE_B));
	}

	@Test
	@Parameters({ "attrName,null", "attrName,", "null,attrValue", ",attrValue" })
	public void addSingleValuedAttribute_WhenAttributeNameOrAttributeValueAreInvalid_ThenNoChangesAreMade(String attributeName, String attributeValue) {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addSingleValuedAttribute(attributeName, attributeValue);

		assertThat(sharedSearchSession.getSingleValuedAttribute(attributeName), equalTo(StringPool.BLANK));
		assertThat(sharedSearchSession.getSingleValuedAttribute(null), equalTo(StringPool.BLANK));
		assertThat(sharedSearchSession.getSingleValuedAttribute(""), equalTo(StringPool.BLANK));
	}

	@Test
	public void getSingleValuedAttribute_WhenSingleValuedAttributeHasBeenConfigured_ThenReturnsTheAttributeValue() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addSingleValuedAttribute(ATTRIBUTE_NAME_1, VALUE_A);
		sharedSearchSession.addSingleValuedAttribute(ATTRIBUTE_NAME_1, VALUE_B);

		assertThat(sharedSearchSession.getSingleValuedAttribute(ATTRIBUTE_NAME_1), equalTo(VALUE_B));
	}

	@Test
	public void getSingleValuedAttribute_WhenSingleValuedAttributeHasNotBeenConfigured_ThenReturnsAnEmptyString() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addSingleValuedAttribute(ATTRIBUTE_NAME_1, VALUE_A);

		assertThat(sharedSearchSession.getSingleValuedAttribute(ATTRIBUTE_NAME_2), equalTo(StringPool.BLANK));
	}

	@Test
	public void init_WhenNoError_ThenReturnsTheObjectWithThePlidConfigured() {
		SharedSearchSession result = SharedSearchSession.init(PLID);

		assertThat(result.getPlid(), equalTo(PLID));
	}

	@Test
	public void removeActiveFilter_WhenFilterToRemoveDoesNotExist_ThenDoesnotRemoveAnyFilters() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addActiveFilter(FILTER_NAME, VALUE_A, ATTRIBUTE_NAME_1, COLOUR);

		sharedSearchSession.removeActiveFilter("Different");

		List<SharedActiveFilter> result = sharedSearchSession.getActiveFilters();

		assertThat(result.size(), equalTo(1));
	}

	@Test
	public void removeActiveFilter_WhenFilterToRemoveExists_ThenRemovesActiveFilter() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addActiveFilter(FILTER_NAME, VALUE_A, ATTRIBUTE_NAME_1, COLOUR);
		sharedSearchSession.addActiveFilter(FILTER_NAME, VALUE_B, ATTRIBUTE_NAME_1, COLOUR);

		sharedSearchSession.removeActiveFilter(FILTER_NAME);

		List<SharedActiveFilter> result = sharedSearchSession.getActiveFilters();

		assertThat(result.size(), equalTo(0));
	}

	@Test
	public void removeActiveFilterValue_WhenFilterAndMultipleValuesExists_ThenRemoveTheOneValue() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addActiveFilter(FILTER_NAME, VALUE_A, ATTRIBUTE_NAME_1, COLOUR);
		sharedSearchSession.addActiveFilter(FILTER_NAME, VALUE_B, ATTRIBUTE_NAME_1, COLOUR);

		sharedSearchSession.removeActiveFilterValue(FILTER_NAME, VALUE_B);

		List<SharedActiveFilter> result = sharedSearchSession.getActiveFilters();

		assertThat(result.size(), equalTo(1));
	}

	@Test
	public void removeAttribute_WhenNoError_ThenRemovesTheAttribute() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addSingleValuedAttribute(ATTRIBUTE_NAME_1, VALUE_B);

		assertThat(sharedSearchSession.getSingleValuedAttribute(ATTRIBUTE_NAME_1), equalTo(VALUE_B));

		sharedSearchSession.removeAttribute(ATTRIBUTE_NAME_1);

		assertThat(sharedSearchSession.getSingleValuedAttribute(ATTRIBUTE_NAME_1), equalTo(StringPool.BLANK));
	}

	@Test
	public void removeSingleValuedAttribute_WhenNoError_ThenRemovesTheAttributeFromTheSingleValuedList() {
		SharedSearchSession sharedSearchSession = SharedSearchSession.init(PLID);
		sharedSearchSession.addSingleValuedAttribute(ATTRIBUTE_NAME_1, VALUE_B);

		assertThat(sharedSearchSession.getSingleValuedAttribute(ATTRIBUTE_NAME_1), equalTo(VALUE_B));

		sharedSearchSession.removeSingleValuedAttribute(ATTRIBUTE_NAME_1);

		assertThat(sharedSearchSession.getSingleValuedAttribute(ATTRIBUTE_NAME_1), equalTo(StringPool.BLANK));
	}

}
