package com.placecube.digitalplace.search.web.portlet.category.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.web.portlet.category.util.CategoryFacetTreeUtil;

public class CategoryFacetTreeServiceTest extends PowerMockito {

	private static final long INVALID_CATEGORY_ID = 0;
	private static final long ROOT_CATEGORY_1_ID = 1;
	private static final long ROOT_CATEGORY_2_ID = 2;
	private static final long CHILD_CATEGORY_1_ID = 3;
	private static final long CHILD_CATEGORY_2_ID = 4;
	private static final long GRANDCHILD_CATEGORY_1_ID = 5;

	private static final Locale LOCALE = Locale.CANADA;

	@InjectMocks
	private CategoryFacetTreeService categoryFacetTreeService;

	@Mock
	private CategoryFacetTreeUtil mockCategoryFacetTreeUtil;

	@Mock
	private AssetCategory mockRootCategory1;

	@Mock
	private AssetCategory mockRootCategory2;

	@Mock
	private AssetCategory mockChildCategory1;

	@Mock
	private AssetCategory mockChildCategory2;

	@Mock
	private AssetCategory mockGrandChildCategory;

	@Mock
	private FacetResult mockRootFacetResult1;

	@Mock
	private FacetResult mockRootFacetResult2;

	@Mock
	private FacetResult mockChildFacetResult1;

	@Mock
	private FacetResult mockChildFacetResult2;

	@Mock
	private FacetResult mockGrandChildFacetResult;

	@Mock
	private FacetResult mockInvalidFacetResult;

	@Mock
	private JSONArray mockTreeJSON;

	@Mock
	private JSONArray mockPrunedTreeJSON;

	@Mock
	private JSONArray mockInvalidJSONArray;

	@Mock
	private JSONFactory mockJsonFactory;

	@Before
	public void setUp() {
		initMocks(this);

		when(mockRootFacetResult1.getValue()).thenReturn(String.valueOf(ROOT_CATEGORY_1_ID));
		when(mockRootFacetResult2.getValue()).thenReturn(String.valueOf(ROOT_CATEGORY_2_ID));
		when(mockChildFacetResult1.getValue()).thenReturn(String.valueOf(CHILD_CATEGORY_1_ID));
		when(mockChildFacetResult2.getValue()).thenReturn(String.valueOf(CHILD_CATEGORY_2_ID));
		when(mockGrandChildFacetResult.getValue()).thenReturn(String.valueOf(GRANDCHILD_CATEGORY_1_ID));

		when(mockCategoryFacetTreeUtil.fetchCategoryById(ROOT_CATEGORY_1_ID)).thenReturn(Optional.of(mockRootCategory1));
		when(mockCategoryFacetTreeUtil.fetchCategoryById(ROOT_CATEGORY_2_ID)).thenReturn(Optional.of(mockRootCategory2));
		when(mockCategoryFacetTreeUtil.fetchCategoryById(CHILD_CATEGORY_1_ID)).thenReturn(Optional.of(mockChildCategory1));
		when(mockCategoryFacetTreeUtil.fetchCategoryById(CHILD_CATEGORY_2_ID)).thenReturn(Optional.of(mockChildCategory2));
		when(mockCategoryFacetTreeUtil.fetchCategoryById(GRANDCHILD_CATEGORY_1_ID)).thenReturn(Optional.of(mockGrandChildCategory));

		AtomicInteger arrayCounter = new AtomicInteger();
		when(mockJsonFactory.createJSONArray()).thenAnswer((Answer<JSONArray>) invocation -> {
			int counterValue = arrayCounter.get();
			JSONArray result;
			switch (counterValue) {
			case 0:
				result = mockTreeJSON;
				break;
			case 1:
				result = mockPrunedTreeJSON;
				break;
			default:
				return mockInvalidJSONArray;
			}
			arrayCounter.incrementAndGet();
			return result;
		});
	}

	@Test
	public void toJSONTree_WhenNoErrorAndAllFacetResultsAreValid_ThenBuildsAndPrunesJSONTreeAndReturnsJSONTree() {
		int maxDepth = 100;
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		Set<Long> categoriesToPrune = new HashSet<>();

		List<FacetResult> facetResults = Arrays.asList(mockChildFacetResult1, mockRootFacetResult2, mockChildFacetResult2, mockGrandChildFacetResult, mockRootFacetResult1);
		Set<String> activeValuesForFilter = Collections.emptySet();

		JSONArray jsonTree = categoryFacetTreeService.toJSONTree(facetResults, activeValuesForFilter, maxDepth, LOCALE);

		assertThat(jsonTree, sameInstance(mockPrunedTreeJSON));

		InOrder inOrder = inOrder(mockCategoryFacetTreeUtil);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockRootFacetResult1), mockRootCategory1, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockRootFacetResult2), mockRootCategory2, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockChildFacetResult1), mockChildCategory1, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockChildFacetResult2), mockChildCategory2, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockGrandChildFacetResult), mockGrandChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockCategoryFacetTreeUtil, times(1)).pruneCategoryTree(mockPrunedTreeJSON, mockTreeJSON, categoriesToPrune);
	}

	@Test
	public void toJSONTree_WhenNoErrorButFacetResultIsNull_ThenOmitsThisFacetAndBuildsTreeFromOtherCategories() {
		int maxDepth = 100;
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		Set<Long> categoriesToPrune = new HashSet<>();

		List<FacetResult> facetResults = Arrays.asList(mockInvalidFacetResult, mockChildFacetResult1, mockRootFacetResult2, mockChildFacetResult2, mockGrandChildFacetResult, mockRootFacetResult1);
		Set<String> activeValuesForFilter = Collections.emptySet();

		when(mockInvalidFacetResult.getValue()).thenReturn(null);

		JSONArray jsonTree = categoryFacetTreeService.toJSONTree(facetResults, activeValuesForFilter, maxDepth, LOCALE);

		assertThat(jsonTree, sameInstance(mockPrunedTreeJSON));

		InOrder inOrder = inOrder(mockCategoryFacetTreeUtil);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockRootFacetResult1), mockRootCategory1, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockRootFacetResult2), mockRootCategory2, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockChildFacetResult1), mockChildCategory1, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockChildFacetResult2), mockChildCategory2, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockGrandChildFacetResult), mockGrandChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockCategoryFacetTreeUtil, times(1)).pruneCategoryTree(mockPrunedTreeJSON, mockTreeJSON, categoriesToPrune);
	}

	@Test
	public void toJSONTree_WhenNoErrorButAssetCategoryDoesNotExistForFacet_ThenOmitsThisAssetCategoryAndBuildsTreeFromOtherCategories() {
		int maxDepth = 100;
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		Set<Long> categoriesToPrune = new HashSet<>();

		List<FacetResult> facetResults = Arrays.asList(mockInvalidFacetResult, mockChildFacetResult1, mockInvalidFacetResult, mockRootFacetResult2, mockChildFacetResult2, mockGrandChildFacetResult, mockRootFacetResult1);
		Set<String> activeValuesForFilter = Collections.emptySet();

		when(mockInvalidFacetResult.getValue()).thenReturn(String.valueOf(INVALID_CATEGORY_ID));
		when(mockCategoryFacetTreeUtil.fetchCategoryById(INVALID_CATEGORY_ID)).thenReturn(Optional.empty());

		JSONArray jsonTree = categoryFacetTreeService.toJSONTree(facetResults, activeValuesForFilter, maxDepth, LOCALE);

		assertThat(jsonTree, sameInstance(mockPrunedTreeJSON));

		InOrder inOrder = inOrder(mockCategoryFacetTreeUtil);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockRootFacetResult1), mockRootCategory1, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockRootFacetResult2), mockRootCategory2, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockChildFacetResult1), mockChildCategory1, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockChildFacetResult2), mockChildCategory2, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);
		inOrder.verify(mockCategoryFacetTreeUtil, times(1))
				.buildCategoryTreeBranch(Optional.of(mockGrandChildFacetResult), mockGrandChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockCategoryFacetTreeUtil, times(1)).pruneCategoryTree(mockPrunedTreeJSON, mockTreeJSON, categoriesToPrune);
	}
}