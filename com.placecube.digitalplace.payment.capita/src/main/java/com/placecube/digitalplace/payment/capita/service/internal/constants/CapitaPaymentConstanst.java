package com.placecube.digitalplace.payment.capita.service.internal.constants;

public class CapitaPaymentConstanst {

	public static final String PID = "com.placecube.digitalplace.payment.capita.configuration.CapitaCompanyConfiguration";

	private CapitaPaymentConstanst() {
	}
}
