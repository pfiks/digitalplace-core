package com.placecube.digitalplace.appointment.admin.web.model;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang3.time.DateUtils;

import com.liferay.petra.string.StringPool;
import com.pfiks.common.time.model.DateTimeSelection;
import com.placecube.digitalplace.appointment.booking.constants.AppointmentSlotType;
import com.placecube.digitalplace.appointment.model.AppointmentSlot;

public class AppointmentSlotForm {

	private int appointmentDuration;
	private final long appointmentSlotId;
	private final Set<DayOfWeek> daysOfWeek;
	private long daysOfWeekBitwiseValue;
	private DateTimeSelection endDateTime;
	private int maxAppointments;
	private String name;
	private final String serviceId;
	private final String slotType;
	private DateTimeSelection startDateTime;

	public AppointmentSlotForm(AppointmentSlot appointmentSlot, DateTimeSelection startDateTime, DateTimeSelection endDateTime) {
		appointmentSlotId = appointmentSlot.getAppointmentSlotId();
		name = appointmentSlot.getName();
		serviceId = appointmentSlot.getServiceId();
		slotType = appointmentSlot.getSlotType();
		daysOfWeek = appointmentSlot.getDaysOfWeek();
		maxAppointments = appointmentSlot.getMaxAppointments();
		appointmentDuration = appointmentSlot.getAppointmentDuration();
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
	}

	public AppointmentSlotForm(String slotType, String serviceId, DateTimeSelection startDateTime, DateTimeSelection endDateTime) {
		appointmentSlotId = 0l;
		this.slotType = slotType;
		this.serviceId = serviceId;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		daysOfWeek = Collections.emptySet();
	}

	public int getAppointmentDuration() {
		return appointmentDuration;
	}

	public long getAppointmentSlotId() {
		return appointmentSlotId;
	}

	public long getDaysOfWeekBitwiseValue() {
		return daysOfWeekBitwiseValue;
	}

	public DateTimeSelection getEndDateTime() {
		return endDateTime;
	}

	public String getFormattedTimes(TimeZone timeZone) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
		return simpleDateFormat.format(startDateTime.getDate(timeZone)).concat(StringPool.SPACE).concat(StringPool.DASH).concat(StringPool.SPACE)
				.concat(simpleDateFormat.format(endDateTime.getDate(timeZone)));
	}

	public int getMaxAppointments() {
		return maxAppointments;
	}

	public String getName() {
		return name;
	}

	public String getServiceId() {
		return serviceId;
	}

	public Date getSlotEndDate(TimeZone timeZone) {
		return DateUtils.truncate(endDateTime.getDate(timeZone), Calendar.DATE);
	}

	public int getSlotEndTimeHour() {
		return endDateTime.getHour();
	}

	public int getSlotEndTimeMinute() {
		return endDateTime.getMinute();
	}

	public Date getSlotStartDate(TimeZone timeZone) {
		return DateUtils.truncate(startDateTime.getDate(timeZone), Calendar.DATE);
	}

	public int getSlotStartTimeHour() {
		return startDateTime.getHour();
	}

	public int getSlotStartTimeMinute() {
		return startDateTime.getMinute();
	}

	public String getSlotType() {
		return slotType;
	}

	public DateTimeSelection getStartDateTime() {
		return startDateTime;
	}

	public boolean isDayOfWeekSelected(int dayOfWeekToCheck) {
		return daysOfWeek.contains(DayOfWeek.of(dayOfWeekToCheck));
	}

	public boolean isNew() {
		return appointmentSlotId <= 0;
	}

	public boolean isSlotTypeConfiguration() {
		return AppointmentSlotType.CONFIGURATION.name().equals(slotType);
	}

	public void setAppointmentDuration(int appointmentDuration) {
		this.appointmentDuration = appointmentDuration;
	}

	public void setDaysOfWeekBitwiseValue(long daysOfWeekBitwiseValue) {
		this.daysOfWeekBitwiseValue = daysOfWeekBitwiseValue;
	}

	public void setEndDateTime(DateTimeSelection endDateTime) {
		this.endDateTime = endDateTime;
	}

	public void setMaxAppointments(int maxAppointments) {
		this.maxAppointments = maxAppointments;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStartDateTime(DateTimeSelection startDateTime) {
		this.startDateTime = startDateTime;
	}

}
