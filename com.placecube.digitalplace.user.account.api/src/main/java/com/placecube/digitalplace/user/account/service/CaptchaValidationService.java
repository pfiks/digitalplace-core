package com.placecube.digitalplace.user.account.service;

import javax.portlet.PortletRequest;

import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.constants.AccountValidatedField;

public interface CaptchaValidationService {
	void validateCaptcha(AccountContext accountContext, AccountValidatedField captchaField, PortletRequest portletRequest);

}
