package com.placecube.digitalplace.template.contributor.freemarker;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.util.DLURLHelper;
import com.liferay.document.library.video.external.shortcut.resolver.DLVideoExternalShortcutResolver;
import com.liferay.portal.kernel.language.Language;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.template.TemplateContextContributor;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.category.service.CategoryRetrievalService;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, property = "type=" + TemplateContextContributor.TYPE_GLOBAL, service = TemplateContextContributor.class)
public class DigitalPlaceFreemarkerTemplateContextContributor implements TemplateContextContributor {

	public static final String DIGITALPLACE_ADDRESS_LOOKUP_SERVICE = "digitalplace_addressLookupService";
	public static final String DIGITALPLACE_CATEGORY_RETRIEVAL_SERVICE = "digitalplace_categoryRetrievalService";
	public static final String DIGITALPLACE_CONSTANTS_VIEW = "digitalplace_constantsView";
	public static final String DIGITALPLACE_DL_URL_HELPER = "digitalplace_dlUrlHelper";
	public static final String DIGITALPLACE_DL_VIDEO_EXTERNAL_SHORTCUT_RESOLVER = "digitalplace_dlVideoExternalShortcutResolver";
	public static final String DIGITALPLACE_DYNAMIC_DATA_LIST_SERVICE = "digitalplace_dynamicDataListService";
	public static final String DIGITALPLACE_JOURNAL_ARTICLE_RETRIEVAL_SERVICE = "digitalplace_journalArticleRetrievalService";
	public static final String DIGITALPLACE_LANGUAGE = "digitalplace_language";
	public static final String DIGITALPLACE_SERVICE_CONTEXT = "digitalplace_serviceContext";
	public static final String DIGITALPLACE_WORKFLOW_STATUS_ANY = "digitalplace_workflowStatusAny";
	public static final String DIGITALPLACE_WORKFLOW_STATUS_APPROVED = "digitalplace_workflowStatusApproved";

	@Reference
	private AddressLookupService addressLookupService;

	@Reference
	private CategoryRetrievalService categoryRetrievalService;

	@Reference
	private DLURLHelper dlurlHelper;

	@Reference
	private DLVideoExternalShortcutResolver dlVideoExternalShortcutResolver;

	@Reference
	private DynamicDataListService dynamicDataListService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private Language language;

	@Override
	public void prepare(Map<String, Object> contextObjects, HttpServletRequest httpServletRequest) {
		contextObjects.put(DIGITALPLACE_ADDRESS_LOOKUP_SERVICE, addressLookupService);
		contextObjects.put(DIGITALPLACE_DL_URL_HELPER, dlurlHelper);
		contextObjects.put(DIGITALPLACE_DL_VIDEO_EXTERNAL_SHORTCUT_RESOLVER, dlVideoExternalShortcutResolver);
		contextObjects.put(DIGITALPLACE_JOURNAL_ARTICLE_RETRIEVAL_SERVICE, journalArticleRetrievalService);
		contextObjects.put(DIGITALPLACE_CATEGORY_RETRIEVAL_SERVICE, categoryRetrievalService);
		contextObjects.put(DIGITALPLACE_DYNAMIC_DATA_LIST_SERVICE, dynamicDataListService);
		contextObjects.put(DIGITALPLACE_LANGUAGE, language);
		contextObjects.put(DIGITALPLACE_SERVICE_CONTEXT, ServiceContextThreadLocal.getServiceContext());

		contextObjects.put(DIGITALPLACE_CONSTANTS_VIEW, Constants.VIEW);
		contextObjects.put(DIGITALPLACE_WORKFLOW_STATUS_ANY, WorkflowConstants.STATUS_ANY);
		contextObjects.put(DIGITALPLACE_WORKFLOW_STATUS_APPROVED, WorkflowConstants.STATUS_APPROVED);
	}

}
