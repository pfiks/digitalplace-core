package com.placecube.digitalplace.navigation.service.util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;

public class BreadcrumbEntryFactoryTest {

	private BreadcrumbEntryFactory breadcrumbEntryFactory;

	@Before
	public void setUp() {
		breadcrumbEntryFactory = new BreadcrumbEntryFactory();
	}

	@Test
	public void createBreadcrumbEntry_WhenNoErrors_ThenReturnsBreadcrumbEntry_AndSetsTitleAndUrl() {
		final String title = "title";
		final String url = "/url";

		BreadcrumbEntry result = breadcrumbEntryFactory.createBreadcrumbEntry(title, url);

		assertThat(result instanceof BreadcrumbEntry, equalTo(true));
		assertThat(result.getTitle(), equalTo(title));
		assertThat(result.getURL(), equalTo(url));
	}
}
