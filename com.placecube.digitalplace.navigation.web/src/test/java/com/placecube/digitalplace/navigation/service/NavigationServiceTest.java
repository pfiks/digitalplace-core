package com.placecube.digitalplace.navigation.service;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.portlet.RenderRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.theme.NavItem;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;
import com.placecube.digitalplace.navigation.configuration.NavigationPortletInstanceConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class NavigationServiceTest extends PowerMockito {

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Mock
	private Group mockGroup1;

	@Mock
	private Group mockGroup2;

	@Mock
	private Group mockGroup3;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Layout mockLayout;

	@Mock
	private NavigationPortletInstanceConfiguration mockNavigationPortletInstanceConfiguration;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Mock
	private SiteNavigationMenuLocalService mockSiteNavigationMenuLocalService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private NavigationService navigationService;

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getAvailableGroupsForWidgetTemplateRetrieval_WhenExceptionRetrievingTheGlobalGroup_ThenThrowsPortalException(boolean scopeGroupGuest) throws PortalException {
		long companyId = 123;
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup1);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroup1.isGuest()).thenReturn(scopeGroupGuest);
		when(mockGroupLocalService.getGroup(companyId, GroupConstants.GUEST)).thenReturn(mockGroup2);
		when(mockGroupLocalService.getCompanyGroup(companyId)).thenThrow(new PortalException());

		navigationService.getAvailableGroupsForWidgetTemplateRetrieval(mockThemeDisplay);
	}

	@Test
	public void getAvailableGroupsForWidgetTemplateRetrieval_WhenScopeGroupIsNotTheGuestGroup_ThenReturnsTheScopeGroupAndTheGuestGroupAndTheGlobalGroup() throws PortalException {
		long companyId = 123;
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup1);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroup1.isGuest()).thenReturn(false);
		when(mockGroupLocalService.getGroup(companyId, GroupConstants.GUEST)).thenReturn(mockGroup2);
		when(mockGroupLocalService.getCompanyGroup(companyId)).thenReturn(mockGroup3);

		Set<Group> results = navigationService.getAvailableGroupsForWidgetTemplateRetrieval(mockThemeDisplay);

		assertThat(results, containsInAnyOrder(mockGroup1, mockGroup2, mockGroup3));
	}

	@Test(expected = PortalException.class)
	public void getAvailableGroupsForWidgetTemplateRetrieval_WhenScopeGroupIsNotTheGuestGroupAndExceptionRetrievingTheGuestGroup_ThenThrowsPortalException() throws PortalException {
		long companyId = 123;
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup1);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroup1.isGuest()).thenReturn(false);
		when(mockGroupLocalService.getGroup(companyId, GroupConstants.GUEST)).thenThrow(new PortalException());

		navigationService.getAvailableGroupsForWidgetTemplateRetrieval(mockThemeDisplay);
	}

	@Test
	public void getAvailableGroupsForWidgetTemplateRetrieval_WhenScopeGroupIsTheGuestGroup_ThenReturnsTheGuestGroupAndTheGlobalGroup() throws PortalException {
		long companyId = 123;
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup1);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroup1.isGuest()).thenReturn(true);
		when(mockGroupLocalService.getCompanyGroup(companyId)).thenReturn(mockGroup2);

		Set<Group> results = navigationService.getAvailableGroupsForWidgetTemplateRetrieval(mockThemeDisplay);

		assertThat(results, containsInAnyOrder(mockGroup1, mockGroup2));
	}

	@Test
	public void getGroupsForNavigationMenuRetrieval_WhenNoError_ThenReturnsAllTheGroupsThatAreNotCompany() {
		Set<Group> groups = new HashSet<>();
		groups.add(mockGroup1);
		groups.add(mockGroup2);
		groups.add(mockGroup3);
		when(mockGroup1.isCompany()).thenReturn(false);
		when(mockGroup2.isCompany()).thenReturn(true);
		when(mockGroup3.isCompany()).thenReturn(false);

		Set<Group> results = navigationService.getGroupsForNavigationMenuRetrieval(groups);

		assertThat(results, containsInAnyOrder(mockGroup1, mockGroup3));
	}

	@Test
	@Parameters({ "-1", "0" })
	public void getSiteNavigationMenu_WhenConfigurationHasInvalidNavigationMenuId_ThenReturnsOptionalWithTheMenuRetrievedBasedOnTheNavigationMenuType(long navigationMenuId) {
		long groupId = 11;
		int type = 234;
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(navigationMenuId);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuGroupId()).thenReturn(groupId);
		when(mockNavigationPortletInstanceConfiguration.navigationType()).thenReturn(type);
		when(mockSiteNavigationMenuLocalService.fetchSiteNavigationMenu(groupId, type)).thenReturn(mockSiteNavigationMenu);

		Optional<SiteNavigationMenu> result = navigationService.getSiteNavigationMenu(mockNavigationPortletInstanceConfiguration);

		assertThat(result.get(), sameInstance(mockSiteNavigationMenu));
	}

	@Test
	@Parameters({ "-1", "0" })
	public void getSiteNavigationMenu_WhenConfigurationHasInvalidNavigationMenuIdAndNoMenuFoundBasedOnTheNavigationMenuType_ThenReturnsEmptyOptional(long navigationMenuId) {
		long groupId = 11;
		int type = 234;
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(navigationMenuId);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuGroupId()).thenReturn(groupId);
		when(mockNavigationPortletInstanceConfiguration.navigationType()).thenReturn(type);
		when(mockSiteNavigationMenuLocalService.fetchSiteNavigationMenu(groupId, type)).thenReturn(null);

		Optional<SiteNavigationMenu> result = navigationService.getSiteNavigationMenu(mockNavigationPortletInstanceConfiguration);

		assertFalse(result.isPresent());
	}

	@Test
	public void getSiteNavigationMenu_WhenConfigurationHasValidNavigationMenuIdAndNavigationMenuFound_ThenReturnsOptionalWithTheMenuRetrievedFromTheId() {
		long navigationMenuId = 1;
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(navigationMenuId);
		when(mockSiteNavigationMenuLocalService.fetchSiteNavigationMenu(navigationMenuId)).thenReturn(mockSiteNavigationMenu);

		Optional<SiteNavigationMenu> result = navigationService.getSiteNavigationMenu(mockNavigationPortletInstanceConfiguration);

		assertThat(result.get(), sameInstance(mockSiteNavigationMenu));
	}

	@Test
	public void getSiteNavigationMenu_WhenConfigurationHasValidNavigationMenuIdAndNavigationMenuNotFound_ThenReturnsEmptyOptional() {
		long navigationMenuId = 1;
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(navigationMenuId);
		when(mockSiteNavigationMenuLocalService.fetchSiteNavigationMenu(navigationMenuId)).thenReturn(null);

		Optional<SiteNavigationMenu> result = navigationService.getSiteNavigationMenu(mockNavigationPortletInstanceConfiguration);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters({ "-1,1", "0,1", "1,0", "1,-1" })
	public void getValidPortletConfiguration_WhenConfigurationHasValidValues_ThenReturnsOptionalWithTheConfiguration(int navType, long navMenuId) throws ConfigurationException {
		String portletId = "portletIdVal";
		when(mockPortal.getPortletId(mockRenderRequest)).thenReturn(portletId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(NavigationPortletInstanceConfiguration.class, mockLayout, portletId)).thenReturn(mockNavigationPortletInstanceConfiguration);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(navMenuId);
		when(mockNavigationPortletInstanceConfiguration.navigationType()).thenReturn(navType);
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateKey()).thenReturn("templateKeyValue");
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateGroupId()).thenReturn(1l);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuGroupId()).thenReturn(2l);

		Optional<NavigationPortletInstanceConfiguration> result = navigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay);

		assertThat(result.get(), sameInstance(mockNavigationPortletInstanceConfiguration));
	}

	@Test(expected = ConfigurationException.class)
	public void getValidPortletConfiguration_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		String portletId = "portletIdVal";
		when(mockPortal.getPortletId(mockRenderRequest)).thenReturn(portletId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(NavigationPortletInstanceConfiguration.class, mockLayout, portletId)).thenThrow(new ConfigurationException());

		navigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay);
	}

	@Test
	@Parameters({ "-1,-1", "0,0", "-1,0", "0,-1" })
	public void getValidPortletConfiguration_WhenNavigationTypeAndNavigationMenuIdAreBothLessThanZero_ThenReturnsEmptyOptional(int navType, long navMenuId) throws ConfigurationException {
		String portletId = "portletIdVal";
		when(mockPortal.getPortletId(mockRenderRequest)).thenReturn(portletId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(NavigationPortletInstanceConfiguration.class, mockLayout, portletId)).thenReturn(mockNavigationPortletInstanceConfiguration);
		when(mockNavigationPortletInstanceConfiguration.navigationType()).thenReturn(navType);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(navMenuId);

		Optional<NavigationPortletInstanceConfiguration> result = navigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters({ "-1,1", "0,1", "1,0", "1,-1" })
	public void getValidPortletConfiguration_WhenNavigationTypeOrNavigationMenuIdAreGreaterThanZeroAndTemplateKeyIsEmpty_ThenReturnsEmptyOptional(int navType, long navMenuId)
			throws ConfigurationException {
		String portletId = "portletIdVal";
		when(mockPortal.getPortletId(mockRenderRequest)).thenReturn(portletId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(NavigationPortletInstanceConfiguration.class, mockLayout, portletId)).thenReturn(mockNavigationPortletInstanceConfiguration);
		when(mockNavigationPortletInstanceConfiguration.navigationType()).thenReturn(navType);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(navMenuId);
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateKey()).thenReturn(StringPool.BLANK);

		Optional<NavigationPortletInstanceConfiguration> result = navigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters({ "-1,1,0", "0,1,-1", "1,0,0", "1,-1,-1" })
	public void getValidPortletConfiguration_WhenNavigationTypeOrNavigationMenuIdAreGreaterThanZeroAndTemplateKeyIsValidAndWidgetTemplateIdIsInvalid_ThenReturnsEmptyOptional(int navType,
			long navMenuId, long templateGroupId) throws ConfigurationException {
		String portletId = "portletIdVal";
		when(mockPortal.getPortletId(mockRenderRequest)).thenReturn(portletId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(NavigationPortletInstanceConfiguration.class, mockLayout, portletId)).thenReturn(mockNavigationPortletInstanceConfiguration);
		when(mockNavigationPortletInstanceConfiguration.navigationType()).thenReturn(navType);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(navMenuId);
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateKey()).thenReturn("templateKeyValue");
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateGroupId()).thenReturn(templateGroupId);

		Optional<NavigationPortletInstanceConfiguration> result = navigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters({ "-1,1,0", "0,1,-1", "1,0,0", "1,-1,-1" })
	public void getValidPortletConfiguration_WhenNavigationTypeOrNavigationMenuIdAreGreaterThanZeroAndTemplateKeyIsValidAndWidgetTemplateIdIsValidAndNavigationMenuIdIsInvalid_ThenReturnsEmptyOptional(
			int navType, long navMenuId, long menuGroupId) throws ConfigurationException {
		String portletId = "portletIdVal";
		when(mockPortal.getPortletId(mockRenderRequest)).thenReturn(portletId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(NavigationPortletInstanceConfiguration.class, mockLayout, portletId)).thenReturn(mockNavigationPortletInstanceConfiguration);
		when(mockNavigationPortletInstanceConfiguration.navigationType()).thenReturn(navType);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuId()).thenReturn(navMenuId);
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateKey()).thenReturn("templateKeyValue");
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateGroupId()).thenReturn(1l);
		when(mockNavigationPortletInstanceConfiguration.navigationMenuGroupId()).thenReturn(menuGroupId);

		Optional<NavigationPortletInstanceConfiguration> result = navigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay);

		assertFalse(result.isPresent());
	}

	@Test
	public void getWidgetTemplate_WhenTemplateIsFound_ThenReturnsOptionalWithTheWidget() {
		long groupId = 11;
		long classNameId = 4556;
		String widgetKey = "widgetKeyValue";
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateGroupId()).thenReturn(groupId);
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateKey()).thenReturn(widgetKey);
		when(mockPortal.getClassNameId(NavItem.class)).thenReturn(classNameId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, widgetKey)).thenReturn(mockDDMTemplate);

		Optional<DDMTemplate> result = navigationService.getWidgetTemplate(mockNavigationPortletInstanceConfiguration);

		assertThat(result.get(), sameInstance(mockDDMTemplate));
	}

	@Test
	public void getWidgetTemplate_WhenTemplateIsNotFound_ThenReturnsEmptyOptional() {
		long groupId = 11;
		long classNameId = 4556;
		String widgetKey = "widgetKeyValue";
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateGroupId()).thenReturn(groupId);
		when(mockNavigationPortletInstanceConfiguration.widgetTemplateKey()).thenReturn(widgetKey);
		when(mockPortal.getClassNameId(NavItem.class)).thenReturn(classNameId);
		when(mockDDMTemplateLocalService.fetchTemplate(groupId, classNameId, widgetKey)).thenReturn(null);

		Optional<DDMTemplate> result = navigationService.getWidgetTemplate(mockNavigationPortletInstanceConfiguration);

		assertFalse(result.isPresent());
	}

}
