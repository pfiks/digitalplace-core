package com.placecube.digitalplace.form.service;

import java.util.Map;

import com.placecube.digitalplace.form.exception.FormDefinitionException;
import com.placecube.digitalplace.form.model.FormDefinition;

public interface FormDefinitionRegistry {

	Map<String, FormDefinition> getFormNamesAndFormDefinitions();

	FormDefinition getFormDefinitionByKey(String formKey) throws FormDefinitionException;
}
