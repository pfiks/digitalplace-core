package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.selector;
import com.liferay.item.selector.BaseItemSelectorCriterion;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
public class JournalArticleItemSelectorCriterion extends BaseItemSelectorCriterion {

	private String itemSubtype;
	private String itemType;
	private String[] mimeTypes;
	private int status = WorkflowConstants.STATUS_APPROVED;

	public String getItemSubtype() {
		return itemSubtype;
	}

	public String getItemType() {
		return itemType;
	}

	public String[] getMimeTypes() {
		return mimeTypes;
	}

	public int getStatus() {
		return status;
	}

	public void setItemSubtype(String itemSubtypeNew) {
		itemSubtype = itemSubtypeNew;
	}

	public void setItemType(String itemTypeNew) {
		itemType = itemTypeNew;
	}

	public void setMimeTypes(String[] mimeTypesNew) {
		mimeTypes = mimeTypesNew;
	}

	public void setStatus(int statusNew) {
		status = statusNew;
	}


}
