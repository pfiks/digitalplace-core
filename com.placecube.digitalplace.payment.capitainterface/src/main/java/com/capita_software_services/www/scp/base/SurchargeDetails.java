/**
 * SurchargeDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class SurchargeDetails  implements java.io.Serializable {
    private java.lang.String fundCode;

    private java.lang.String reference;

    private int amountInMinorUnits;

    private com.capita_software_services.www.scp.base.SurchargeBasis surchargeBasis;

    private org.apache.axis.types.PositiveInteger continuousAuditNumber;

    private int vatAmountInMinorUnits;

    public SurchargeDetails() {
    }

    public SurchargeDetails(
           java.lang.String fundCode,
           java.lang.String reference,
           int amountInMinorUnits,
           com.capita_software_services.www.scp.base.SurchargeBasis surchargeBasis,
           org.apache.axis.types.PositiveInteger continuousAuditNumber,
           int vatAmountInMinorUnits) {
           this.fundCode = fundCode;
           this.reference = reference;
           this.amountInMinorUnits = amountInMinorUnits;
           this.surchargeBasis = surchargeBasis;
           this.continuousAuditNumber = continuousAuditNumber;
           this.vatAmountInMinorUnits = vatAmountInMinorUnits;
    }


    /**
     * Gets the fundCode value for this SurchargeDetails.
     *
     * @return fundCode
     */
    public java.lang.String getFundCode() {
        return fundCode;
    }


    /**
     * Sets the fundCode value for this SurchargeDetails.
     *
     * @param fundCode
     */
    public void setFundCode(java.lang.String fundCode) {
        this.fundCode = fundCode;
    }


    /**
     * Gets the reference value for this SurchargeDetails.
     *
     * @return reference
     */
    public java.lang.String getReference() {
        return reference;
    }


    /**
     * Sets the reference value for this SurchargeDetails.
     *
     * @param reference
     */
    public void setReference(java.lang.String reference) {
        this.reference = reference;
    }


    /**
     * Gets the amountInMinorUnits value for this SurchargeDetails.
     *
     * @return amountInMinorUnits
     */
    public int getAmountInMinorUnits() {
        return amountInMinorUnits;
    }


    /**
     * Sets the amountInMinorUnits value for this SurchargeDetails.
     *
     * @param amountInMinorUnits
     */
    public void setAmountInMinorUnits(int amountInMinorUnits) {
        this.amountInMinorUnits = amountInMinorUnits;
    }


    /**
     * Gets the surchargeBasis value for this SurchargeDetails.
     *
     * @return surchargeBasis
     */
    public com.capita_software_services.www.scp.base.SurchargeBasis getSurchargeBasis() {
        return surchargeBasis;
    }


    /**
     * Sets the surchargeBasis value for this SurchargeDetails.
     *
     * @param surchargeBasis
     */
    public void setSurchargeBasis(com.capita_software_services.www.scp.base.SurchargeBasis surchargeBasis) {
        this.surchargeBasis = surchargeBasis;
    }


    /**
     * Gets the continuousAuditNumber value for this SurchargeDetails.
     *
     * @return continuousAuditNumber
     */
    public org.apache.axis.types.PositiveInteger getContinuousAuditNumber() {
        return continuousAuditNumber;
    }


    /**
     * Sets the continuousAuditNumber value for this SurchargeDetails.
     *
     * @param continuousAuditNumber
     */
    public void setContinuousAuditNumber(org.apache.axis.types.PositiveInteger continuousAuditNumber) {
        this.continuousAuditNumber = continuousAuditNumber;
    }


    /**
     * Gets the vatAmountInMinorUnits value for this SurchargeDetails.
     *
     * @return vatAmountInMinorUnits
     */
    public int getVatAmountInMinorUnits() {
        return vatAmountInMinorUnits;
    }


    /**
     * Sets the vatAmountInMinorUnits value for this SurchargeDetails.
     *
     * @param vatAmountInMinorUnits
     */
    public void setVatAmountInMinorUnits(int vatAmountInMinorUnits) {
        this.vatAmountInMinorUnits = vatAmountInMinorUnits;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SurchargeDetails)) return false;
        SurchargeDetails other = (SurchargeDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.fundCode==null && other.getFundCode()==null) ||
             (this.fundCode!=null &&
              this.fundCode.equals(other.getFundCode()))) &&
            ((this.reference==null && other.getReference()==null) ||
             (this.reference!=null &&
              this.reference.equals(other.getReference()))) &&
            this.amountInMinorUnits == other.getAmountInMinorUnits() &&
            ((this.surchargeBasis==null && other.getSurchargeBasis()==null) ||
             (this.surchargeBasis!=null &&
              this.surchargeBasis.equals(other.getSurchargeBasis()))) &&
            ((this.continuousAuditNumber==null && other.getContinuousAuditNumber()==null) ||
             (this.continuousAuditNumber!=null &&
              this.continuousAuditNumber.equals(other.getContinuousAuditNumber()))) &&
            this.vatAmountInMinorUnits == other.getVatAmountInMinorUnits();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFundCode() != null) {
            _hashCode += getFundCode().hashCode();
        }
        if (getReference() != null) {
            _hashCode += getReference().hashCode();
        }
        _hashCode += getAmountInMinorUnits();
        if (getSurchargeBasis() != null) {
            _hashCode += getSurchargeBasis().hashCode();
        }
        if (getContinuousAuditNumber() != null) {
            _hashCode += getContinuousAuditNumber().hashCode();
        }
        _hashCode += getVatAmountInMinorUnits();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SurchargeDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "fundCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "reference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amountInMinorUnits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "amountInMinorUnits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surchargeBasis");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeBasis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "surchargeBasis"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("continuousAuditNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "continuousAuditNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "positiveInteger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vatAmountInMinorUnits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "vatAmountInMinorUnits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
