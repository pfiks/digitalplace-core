package com.placecube.digitalplace.session.timeout.dynamicinclude;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.placecube.digitalplace.session.timeout.web.service.SessionTimeoutPropertyService;

public class SessionTimeoutDynamicIncludeTest {

	private SessionTimeoutPropertyService sessionTimeoutPropertyService = new SessionTimeoutPropertyService();

	@Test
	public void getValidTimeout_WhenConfiguredValueIsLessThan2_ThenReturnZero() {
		int configuredValue = 1;

		int result = sessionTimeoutPropertyService.getValidTimeout(configuredValue);

		assertThat(result, equalTo(0));
	}

	@Test
	public void getValidTimeout_WhenConfiguredValueIsMoreThan720_ThenReturnZero() {
		int configuredValue = 721;

		int result = sessionTimeoutPropertyService.getValidTimeout(configuredValue);

		assertThat(result, equalTo(0));
	}

	@Test
	public void getValidTimeout_WhenConfiguredValueIsMoreThan2AndLessThan720_ThenReturnConfiguredValue() {
		int configuredValue = 719;

		int result = sessionTimeoutPropertyService.getValidTimeout(configuredValue);

		assertThat(result, equalTo(configuredValue));
	}

}
