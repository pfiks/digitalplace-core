package com.placecube.digitalplace.notifications.service;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendSmsContext;

public interface NotificationSmsConnector {

	boolean enabled(long companyId);

	void sendSms(SendSmsContext sendSmsContext, ServiceContext serviceContext) throws NotificationsException, ConfigurationException;
}
