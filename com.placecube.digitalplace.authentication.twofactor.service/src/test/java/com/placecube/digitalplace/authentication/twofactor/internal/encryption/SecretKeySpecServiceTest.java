package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.crypto.spec.SecretKeySpec;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

public class SecretKeySpecServiceTest extends PowerMockito {

	private static final String UUID = "237e8c17-6f94-56c5-f15a-fa46ed5f53f3";

	private byte[] binaryKey = new byte[SecretKeySpecService.SECURITY_KEY_UUID_PADDING];

	@Mock
	private ByteArrayOutputStream mockByteArrayOutputStream;

	@Mock
	private ModelFactoryBuilder mockModelFactoryBuilder;

	@Mock
	private SecretKeySpec mockSecretKeySpec;

	@InjectMocks
	private SecretKeySpecService secretKeySpecService;

	private byte[] securityKey = new byte[SecretKeySpecService.SECURITY_KEY_UUID_PADDING + SecretKeySpecService.SECURITY_KEY_UUID_PADDING];
	private byte[] securityKeyWrongLength = new byte[SecretKeySpecService.SECURITY_KEY_UUID_PADDING + SecretKeySpecService.SECURITY_KEY_UUID_PADDING + 1];

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = IOException.class)
	public void getSecretKeySpec_WhenErrorOnCombiningKeys_ThenThrowsIOException() throws IOException {
		mockByteArrayOutputStream();
		doThrow(new IOException()).when(mockByteArrayOutputStream).write(any());

		secretKeySpecService.getSecretKeySpec(UUID);
	}

	@Test(expected = IOException.class)
	public void getSecretKeySpec_WhenErrorReadingBytesFromPath_ThenThrowsIOException() throws IOException {
		when(mockModelFactoryBuilder.getBytesFromFile(SecretKeySpecService.SECRET_KEY_PATH)).thenThrow(new IOException());

		secretKeySpecService.getSecretKeySpec(UUID);
	}

	@Test(expected = IllegalStateException.class)
	public void getSecretKeySpec_WhenIVHasWrongLength_ThenThrowsIllegalStateException() throws IOException {
		mockByteArrayOutputStream();
		when(mockByteArrayOutputStream.toByteArray()).thenReturn(securityKeyWrongLength);

		secretKeySpecService.getSecretKeySpec(UUID);
	}

	@Test
	public void getSecretKeySpec_WhenNoError_ThenCombinesBinaryKeyAndFirstCharactersOfUUID() throws IOException {
		mockByteArrayOutputStream();
		when(mockByteArrayOutputStream.toByteArray()).thenReturn(securityKey);

		secretKeySpecService.getSecretKeySpec(UUID);

		InOrder inOrder = Mockito.inOrder(mockByteArrayOutputStream);
		inOrder.verify(mockByteArrayOutputStream, times(1)).write(binaryKey);
		inOrder.verify(mockByteArrayOutputStream, times(1)).write(UUID.substring(0, SecretKeySpecService.SECURITY_KEY_UUID_PADDING).getBytes());
	}

	@Test
	public void getSecretKeySpec_WhenNoError_ThenReturnsSecretKeySpec() throws IOException {
		mockByteArrayOutputStream();
		when(mockByteArrayOutputStream.toByteArray()).thenReturn(securityKey);
		when(mockModelFactoryBuilder.getSecretKeySpecInstance(securityKey, "AES")).thenReturn(mockSecretKeySpec);

		SecretKeySpec result = secretKeySpecService.getSecretKeySpec(UUID);

		assertThat(result, sameInstance(mockSecretKeySpec));
	}

	private void mockByteArrayOutputStream() throws IOException {
		when(mockModelFactoryBuilder.getBytesFromFile(SecretKeySpecService.SECRET_KEY_PATH)).thenReturn(binaryKey);
		when(mockModelFactoryBuilder.getByteArrayOutputStreamInstance()).thenReturn(mockByteArrayOutputStream);
	}

}
