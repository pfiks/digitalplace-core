package com.placecube.digitalplace.search.sortby.mostactive.model.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.sortby.mostactive.constants.MostActiveConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AggregatedResourceBundleUtil.class, SortFactoryUtil.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.search.SortFactoryUtil")
public class SortByMostActiveTest extends PowerMockito {

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Mock
	private Sort mockSortCounter;

	@Mock
	private Sort mockSortDate;

	@InjectMocks
	private SortByMostActive sortByMostActive;

	@Before
	public void activateSetup() {
		mockStatic(SortFactoryUtil.class, AggregatedResourceBundleUtil.class);
	}

	@Test
	public void getId_WhenNoError_ThenReturnsTheId() {
		String result = sortByMostActive.getId();

		assertThat(result, equalTo("sortByMostActive"));
	}

	@Test
	public void getLabel_WhenNoError_ThenReturnsLabel() {
		String expected = "exp";
		Locale locale = Locale.FRENCH;
		when(AggregatedResourceBundleUtil.get("most-active", locale, "com.placecube.digitalplace.search.sortby.mostactive")).thenReturn(expected);

		String result = sortByMostActive.getLabel(locale);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSort_WhenUseOppositeSortDirectionIsFalse_ThenReturnsTheSortsWithSortOrderTrue() {
		when(SortFactoryUtil.create(MostActiveConstants.COUNTER_FIELD_NAME, true)).thenReturn(mockSortCounter);
		when(SortFactoryUtil.create(MostActiveConstants.LAST_LOGIN_DATE_FIELD_NAME + "_sortable", true)).thenReturn(mockSortDate);

		Sort[] results = sortByMostActive.getSorts(mockSharedSearchContributorSettings, false);

		assertThat(results.length, equalTo(2));
		assertThat(results[0], equalTo(mockSortCounter));
		assertThat(results[1], equalTo(mockSortDate));
	}

	@Test
	public void getSort_WhenUseOppositeSortDirectionIsTrue_ThenReturnsTheSortsWithSortOrderFalse() {
		when(SortFactoryUtil.create(MostActiveConstants.COUNTER_FIELD_NAME, false)).thenReturn(mockSortCounter);
		when(SortFactoryUtil.create(MostActiveConstants.LAST_LOGIN_DATE_FIELD_NAME + "_sortable", false)).thenReturn(mockSortDate);

		Sort[] results = sortByMostActive.getSorts(mockSharedSearchContributorSettings, true);

		assertThat(results.length, equalTo(2));
		assertThat(results[0], equalTo(mockSortCounter));
		assertThat(results[1], equalTo(mockSortDate));
	}
}
