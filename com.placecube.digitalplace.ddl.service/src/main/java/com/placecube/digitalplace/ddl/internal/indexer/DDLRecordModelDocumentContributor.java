package com.placecube.digitalplace.ddl.internal.indexer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.storage.Field;
import com.liferay.dynamic.data.mapping.storage.Fields;
import com.liferay.dynamic.data.mapping.util.DDMFormValuesToFieldsConverter;
import com.liferay.dynamic.data.mapping.util.DDMIndexer;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlParser;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.ddl.constants.DDLIndexerField;
import com.placecube.digitalplace.ddl.internal.constants.DDLFormFieldTypeConstants;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;

@Component(immediate = true, property = "indexer.class.name=com.liferay.dynamic.data.lists.model.DDLRecord", service = ModelDocumentContributor.class)
public class DDLRecordModelDocumentContributor implements ModelDocumentContributor<DDLRecord> {

	private static final String GEOLOCATION_SORTABLE = "_geoLocationsortable";

	private static final Log LOG = LogFactoryUtil.getLog(DDLRecordModelDocumentContributor.class);

	private static final ZoneId UTC_ZONE = ZoneId.of("UTC");

	@Reference
	private DDMFormValuesToFieldsConverter ddmFormValuesToFieldsConverter;

	@Reference
	private DDMIndexer ddmIndexer;

	@Reference
	private DynamicDataListConfigurationService dynamicDataListConfigurationService;

	@Reference
	private HtmlParser htmlParser;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	public void contribute(Document document, DDLRecord ddlRecord) {
		try {
			DDMStructure ddmStructure = ddlRecord.getRecordSet().getDDMStructure();

			document.addKeyword(DDLIndexerField.DDM_STRUCTURE_ID, ddmStructure.getStructureId());
			document.addKeyword(DDLIndexerField.DDM_STRUCTURE_KEY, ddmStructure.getStructureKey());
			DDMFormValues ddmFormValues = ddlRecord.getDDMFormValues();
			Fields fields = ddmFormValuesToFieldsConverter.convert(ddmStructure, ddmFormValues);

			Set<Locale> locales = ddmFormValues.getAvailableLocales();

			Optional<DynamicDataListMapping> optDynamicDataListMapping = dynamicDataListConfigurationService.getDynamicDataListMapping(ddmStructure);
			if (optDynamicDataListMapping.isPresent()) {

				DynamicDataListMapping dynamicDataListMapping = optDynamicDataListMapping.get();

				String indexerTitleFieldName = dynamicDataListMapping.getIndexerTitleFieldName();
				Field field = fields.get(indexerTitleFieldName);
				addTextField(document, com.liferay.portal.kernel.search.Field.TITLE, locales, field);

				String indexerContentFieldName = dynamicDataListMapping.getIndexerContentFieldName();
				addTextField(document, com.liferay.portal.kernel.search.Field.CONTENT, locales, fields.get(indexerContentFieldName));

			}

			fields.forEach(field -> processField(document, field, ddmStructure, locales));

		} catch (PortalException e) {
			LOG.warn("Unable to process DDLRecord model contributor - " + e.getMessage());
			LOG.debug(e);
		}
	}

	private void addDateField(Document document, Field field) {
		if (Validator.isNotNull(field.getValue())) {

			String stringValue = String.valueOf(field.getValue());

			document.addDate(field.getName(), getDateFromDateString(stringValue));
		}
	}

	private void addGeoLocationSortableField(Document document, Field field, DDMStructure ddmStructure, Set<Locale> locales) throws JSONException {

		for (Locale locale : locales) {

			String name = ddmIndexer.encodeName(ddmStructure.getStructureId(), field.getName(), locale);

			String valueString = String.valueOf(field.getValue(locale));

			JSONObject jsonObject = jsonFactory.createJSONObject(valueString);

			double latitude = jsonObject.getDouble("latitude", 0);
			double longitude = jsonObject.getDouble("longitude", 0);

			document.addGeoLocation(name.concat("_geolocation") + GEOLOCATION_SORTABLE, latitude, longitude);
		}
	}

	private void addTextField(Document document, String fieldName, Set<Locale> locales, Field field) {
		if (Validator.isNotNull(field)) {

			Map<Locale, String> localisedValues = new HashMap<>();

			for (Locale locale : locales) {
				String value = getStringValue(field, locale);
				localisedValues.put(locale, value);
			}

			document.addLocalizedText(fieldName, localisedValues);

			String defaultLanguageValue = getStringValue(field, field.getDefaultLocale());
			document.addText(fieldName, defaultLanguageValue);
		}
	}

	private Date getDateFromDateString(String dateString) {
		try {
			LocalDate localDate = LocalDate.parse(dateString);
			return Date.from(localDate.atStartOfDay(UTC_ZONE).toInstant());
		} catch (DateTimeParseException e) {
			LocalDateTime localDateTime = LocalDateTime.parse(dateString);
			return Date.from(localDateTime.atZone(UTC_ZONE).toInstant());
		}
	}

	private String getStringValue(Field field, Locale locale) {
		String value = GetterUtil.getString(field.getValue(locale), StringPool.BLANK);
		return htmlParser.extractText(value);
	}

	private boolean isDateField(String fieldType) {
		return DDLFormFieldTypeConstants.DDM_DATE.equals(fieldType) || DDMFormFieldTypeConstants.DATE.equals(fieldType);
	}

	private void processField(Document document, Field field, DDMStructure ddmStructure, Set<Locale> locales) {
		try {
			String type = field.getType();

			if (type.contains(DDMFormFieldTypeConstants.GEOLOCATION)) {
				addGeoLocationSortableField(document, field, ddmStructure, locales);
			} else if (isDateField(type) && !field.isRepeatable()) {
				addDateField(document, field);
			}
		} catch (Exception e) {
			LOG.warn("Unable to process field " + field.getName() + " in DDLRecordModelDocumentContributor - " + e.getMessage());
			LOG.debug(e);
		}
	}

}
