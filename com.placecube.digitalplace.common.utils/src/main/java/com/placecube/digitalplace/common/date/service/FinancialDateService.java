package com.placecube.digitalplace.common.date.service;

import java.time.LocalDate;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = FinancialDateService.class)
public class FinancialDateService extends DateService {

	public static final int FINANCIAL_YEAR_START_DAY = 1;

	public static final int FINANCIAL_YEAR_START_MONTH = 4;

	public XMLGregorianCalendar getFinancialYearStartCalendar(int year) throws DatatypeConfigurationException {
		LocalDate financialYearStartDate = getFinancialYearStartLocalDate(year);
		return toXMLGregorianCalendar(financialYearStartDate);
	}

	public LocalDate getFinancialYearStartLocalDate(int year) {
		return LocalDate.of(year, FINANCIAL_YEAR_START_MONTH, FINANCIAL_YEAR_START_DAY);
	}

	public LocalDate getFinancialYearEndLocalDate(int year) {
		return LocalDate.of(year + 1, FINANCIAL_YEAR_START_MONTH, FINANCIAL_YEAR_START_DAY).minusDays(1);
	}
}
