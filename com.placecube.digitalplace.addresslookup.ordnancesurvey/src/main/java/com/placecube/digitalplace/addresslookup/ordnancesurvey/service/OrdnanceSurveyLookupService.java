package com.placecube.digitalplace.addresslookup.ordnancesurvey.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.ordnancesurvey.constants.OrdnanceSurveyConstants;

@Component(immediate = true, service = OrdnanceSurveyLookupService.class)
public class OrdnanceSurveyLookupService {

	@Reference
	private AddressRetrievalService addressRetrievalService;

	public Map<String, String> getAdditionalParametersAndValuesMap(String[] paramsAndValues) {
		Map<String, String> results = new HashMap<>();
		if (ArrayUtil.isNotEmpty(paramsAndValues)) {
			for (String paramValue : paramsAndValues) {
				if (Validator.isNotNull(paramValue) && paramValue.contains("=")) {
					int indexOf = paramValue.indexOf("=");
					String fieldName = paramValue.substring(0, indexOf);
					String fieldValue = StringUtil.trim(paramValue.substring(indexOf + 1));
					if (Validator.isNotNull(fieldName) && Validator.isNotNull(fieldValue)) {
						results.put(fieldName, fieldValue);
					}
				}
			}
		}
		return results;
	}

	public Set<AddressContext> getResults(String urlToCall) {
		Map<String, AddressContext> results = new LinkedHashMap<>();

		JSONArray resultsJsonArray = addressRetrievalService.executeMethod(urlToCall);
		for (int i = 0; i < resultsJsonArray.length(); i++) {
			AddressContext addressLookup = addressRetrievalService.getAddressContextResult(resultsJsonArray.getJSONObject(i));
			if (!results.containsKey(addressLookup.getUPRN()) || OrdnanceSurveyConstants.DPA.equals(addressLookup.getSource())) {
				results.put(addressLookup.getUPRN(), addressLookup);
			}
		}

		return new LinkedHashSet<>(results.values());
	}

	public String getURL(String baseEndpointURL, String defaultParameterName, String defaultParameterValue, Map<String, String> additionalParameterValues, String applicationKey) {
		List<NameValuePair> nvps = new ArrayList<>();
		nvps.add(new BasicNameValuePair(defaultParameterName, defaultParameterValue));
		nvps.add(new BasicNameValuePair("key", applicationKey));
		for (Entry<String, String> entry : additionalParameterValues.entrySet()) {
			nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
		}

		StringBuilder requestUrl = new StringBuilder(baseEndpointURL);
		requestUrl.append(StringPool.QUESTION);
		requestUrl.append(URLEncodedUtils.format(nvps, StandardCharsets.UTF_8.toString()));

		return requestUrl.toString();
	}

}
