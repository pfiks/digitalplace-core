package com.placecube.digitalplace.search.web.portlet.sortby.model.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AggregatedResourceBundleUtil.class, SortFactoryUtil.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.search.SortFactoryUtil")
public class SortByCreateDateTest extends PowerMockito {

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Mock
	private Sort mockSort;

	@InjectMocks
	private SortByCreateDate sortByCreateDate;

	@Before
	public void activateSetup() {
		mockStatic(SortFactoryUtil.class, AggregatedResourceBundleUtil.class);
	}

	@Test
	public void getId_WhenNoError_ThenReturnsTheId() {
		String result = sortByCreateDate.getId();

		assertThat(result, equalTo("sortByNewest"));
	}

	@Test
	public void getLabel_WhenNoError_ThenReturnsLabel() {
		String expected = "exp";
		Locale locale = Locale.FRENCH;
		when(AggregatedResourceBundleUtil.get("newest", locale, "com.placecube.digitalplace.search.web")).thenReturn(expected);

		String result = sortByCreateDate.getLabel(locale);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSort_WhenUseOppositeSortDirectionIsFalse_ThenReturnsTheSortsWithSortOrderTrue() {
		when(SortFactoryUtil.create("createDate_sortable", true)).thenReturn(mockSort);

		Sort[] results = sortByCreateDate.getSorts(mockSharedSearchContributorSettings, false);

		assertThat(results.length, equalTo(1));
		assertThat(results[0], equalTo(mockSort));
	}

	@Test
	public void getSort_WhenUseOppositeSortDirectionIsTrue_ThenReturnsTheSortsWithSortOrderFalse() {
		when(SortFactoryUtil.create("createDate_sortable", false)).thenReturn(mockSort);

		Sort[] results = sortByCreateDate.getSorts(mockSharedSearchContributorSettings, true);

		assertThat(results.length, equalTo(1));
		assertThat(results[0], equalTo(mockSort));
	}

}
