package com.placecube.digitalplace.override.jsp.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "instance-configuration", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.override.jsp.configuration.CompanyStyleConfiguration", localization = "content/Language", name = "company-style")
public interface CompanyStyleConfiguration {

	@Meta.AD(required = false, name = "company-css", description = "company-css-help")
	String companyCSS();

	@Meta.AD(required = false, name = "company-favicon", description = "company-favicon-help")
	String companyFaviconURL();

}
