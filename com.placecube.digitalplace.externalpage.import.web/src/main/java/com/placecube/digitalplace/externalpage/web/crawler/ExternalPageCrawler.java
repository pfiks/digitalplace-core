package com.placecube.digitalplace.externalpage.web.crawler;

import java.net.URISyntaxException;
import java.util.List;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalServiceUtil;
import com.placecube.digitalplace.externalpage.web.constants.CrawlConstants;
import com.placecube.digitalplace.externalpage.web.service.CrawlUtil;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.parser.ParseData;
import edu.uci.ics.crawler4j.url.WebURL;

public class ExternalPageCrawler extends WebCrawler {

	private static final Log LOG = LogFactoryUtil.getLog(ExternalPageCrawler.class);

	/**
	 * This method receives two parameters. The first parameter is the page in
	 * which we have discovered this new url and the second parameter is the new
	 * url. You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic).
	 */
	@Override
	public boolean shouldVisit(Page referringPage, WebURL url) {
		try {
			String urlToImport = url.getURL();
			return CrawlUtil.isValidURL(urlToImport.toLowerCase()) && matchingDomain(referringPage, urlToImport);
		} catch (Exception e) {
			LOG.debug(e);
			LOG.error("Unable to visit page " + e.getMessage());
			return false;
		}
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */
	@Override
	public void visit(Page page) {
		ParseData parseData = page.getParseData();
		if (parseData instanceof HtmlParseData) {
			String url = CrawlUtil.getPageURL(page);
			try {
				List<String> customData = CrawlUtil.getCustomData(getMyController());
				long companyId = CrawlUtil.getIdFromCustomData(customData, CrawlConstants.CUSTOM_DATA_COMPANY_ID);
				long groupId = CrawlUtil.getIdFromCustomData(customData, CrawlConstants.CUSTOM_DATA_GROUP_ID);
				long userId = CrawlUtil.getIdFromCustomData(customData, CrawlConstants.CUSTOM_DATA_USER_ID);
				String xpath = CrawlUtil.getStringFromCustomData(customData, CrawlConstants.CUSTOM_DATA_XPATH);
				String crawlAction = CrawlUtil.getStringFromCustomData(customData, CrawlConstants.CUSTOM_DATA_INDEX_CONTENT);
				boolean indexContent = crawlAction.equalsIgnoreCase(CrawlConstants.ENTRY_PUBLISHED);
				HtmlParseData htmlParseData = (HtmlParseData) parseData;

				String content = CrawlUtil.extractContent(htmlParseData.getHtml(), xpath);

				if (crawlAction.equalsIgnoreCase(CrawlConstants.ENTRY_PUBLISHED) || crawlAction.equalsIgnoreCase(CrawlConstants.ENTRY_ONLY)) {
					long currentCompanyId = CompanyThreadLocal.getCompanyId();
					CompanyThreadLocal.setCompanyId(companyId);
					ExternalPageLocalServiceUtil.addOrUpdateExternalPage(companyId, groupId, userId, url, htmlParseData.getTitle(), content, indexContent);
					CompanyThreadLocal.setCompanyId(currentCompanyId);
				} else {
					StringBuilder messageBuilder = new StringBuilder("Start test import content from " + url);

					if (Validator.isNotNull(xpath)) {
						messageBuilder.append(" with xpath: " + xpath);
					}

					LOG.info(messageBuilder.toString());
					LOG.info(content);
					LOG.info("End test imported external page");
				}

			} catch (Exception e) {
				LOG.debug(e);
				LOG.error("Exception importing external page from url: " + url + " - " + e.getMessage());
			}
		}
	}

	private boolean matchingDomain(Page referringPage, String urlToImport) throws URISyntaxException {
		return CrawlUtil.getDomain(urlToImport).startsWith(CrawlUtil.getDomain(CrawlUtil.getPageURL(referringPage)));
	}

}
