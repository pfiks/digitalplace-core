package com.placecube.digitalplace.externalpage.web.portlet;

import java.util.List;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.GroupProvider;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;
import com.placecube.digitalplace.externalpage.web.constants.ViewKeys;
import com.placecube.digitalplace.externalpage.web.display.context.ExternalManagementToolbarDisplayContext;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET, "mvc.command.name=/",
		"mvc.command.name=" + MVCCommandKeys.EXTERNAL_PAGE_VIEW }, service = MVCRenderCommand.class)
public class ExternalPageViewListMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Reference
	private GroupProvider groupProvider;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {

		addExternalPageListAttributes(renderRequest, renderResponse);

		addManagementToolbarAttributes(renderRequest, renderResponse);
		return ViewKeys.VIEW;
	}

	private void addExternalPageListAttributes(RenderRequest renderRequest, RenderResponse renderResponse) {

		PortletURL iteratorURL = renderResponse.createRenderURL();
		SearchContainer<ExternalPage> searchContainer = new SearchContainer<>(renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM, SearchContainer.DEFAULT_DELTA, iteratorURL, null,
				"no-entries-were-found");

		HttpServletRequest httpServletRequest = portal.getHttpServletRequest(renderRequest);
		Group group = groupProvider.getGroup(httpServletRequest);

		int total = externalPageLocalService.getExternalPagesCount(group.getGroupId());

		List<ExternalPage> externalList = externalPageLocalService.getExternalPages(group.getGroupId(), searchContainer.getStart(), searchContainer.getEnd());
		searchContainer.setResultsAndTotal(() -> externalList, total);

		renderRequest.setAttribute("searchContainers", searchContainer);
	}

	private void addManagementToolbarAttributes(RenderRequest renderRequest, RenderResponse renderResponse) {

		LiferayPortletRequest liferayPortletRequest = portal.getLiferayPortletRequest(renderRequest);

		LiferayPortletResponse liferayPortletResponse = portal.getLiferayPortletResponse(renderResponse);

		ExternalManagementToolbarDisplayContext externalManagementToolbarDisplayContext = new ExternalManagementToolbarDisplayContext(liferayPortletRequest, liferayPortletResponse,
				portal.getHttpServletRequest(renderRequest));

		renderRequest.setAttribute("externalManagementToolbarDisplayContext", externalManagementToolbarDisplayContext);

	}

}
