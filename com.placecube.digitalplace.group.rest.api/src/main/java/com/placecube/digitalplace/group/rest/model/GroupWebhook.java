/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the GroupWebhook service. Represents a row in the &quot;Placecube_GroupRest_GroupWebhook&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see GroupWebhookModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.group.rest.model.impl.GroupWebhookImpl"
)
@ProviderType
public interface GroupWebhook extends GroupWebhookModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.group.rest.model.impl.GroupWebhookImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<GroupWebhook, Long> WEBHOOK_ID_ACCESSOR =
		new Accessor<GroupWebhook, Long>() {

			@Override
			public Long get(GroupWebhook groupWebhook) {
				return groupWebhook.getWebhookId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<GroupWebhook> getTypeClass() {
				return GroupWebhook.class;
			}

		};

	public java.util.List<String> getConfiguredEvents();

}