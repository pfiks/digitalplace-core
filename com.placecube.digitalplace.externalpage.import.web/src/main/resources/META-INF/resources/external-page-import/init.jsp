<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/clay" prefix="clay" %>
<%@ taglib uri="http://liferay.com/tld/frontend" prefix="liferay-frontend" %>
<%@ taglib uri="http://liferay.com/tld/item-selector" prefix="liferay-item-selector"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://placecube.com/digitalplace/tld/frontend" prefix="dp-frontend" %>
 
<%@ page import="javax.portlet.WindowState"%>
 

<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
<%@page import="com.placecube.digitalplace.externalpage.model.ExternalPage"%>
<%@page import="com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys"%>
<%@page import="com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys"%>
<%@page import="com.placecube.digitalplace.externalpage.web.constants.ValidationKeys"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil" %>

<liferay-frontend:defineObjects />
<liferay-theme:defineObjects />
<portlet:defineObjects />
