package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.display;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.dao.search.ResultRow;
import com.liferay.portal.kernel.dao.search.ResultRowSplitter;
import com.liferay.portal.kernel.dao.search.ResultRowSplitterEntry;

import java.util.ArrayList;
import java.util.List;

public class JournalResultRowSplitter implements ResultRowSplitter {

    @Override
    public List<ResultRowSplitterEntry> split(List<ResultRow> resultRows) {
        List<ResultRowSplitterEntry> resultRowSplitterEntries =
                new ArrayList<>();

        List<ResultRow> journalArticleResultRows = new ArrayList<>();
        List<ResultRow> journalFolderResultRows = new ArrayList<>();

        for (ResultRow resultRow : resultRows) {
            Object object = resultRow.getObject();

            if (object instanceof JournalFolder) {
                journalFolderResultRows.add(resultRow);
            }
            else {
                journalArticleResultRows.add(resultRow);
            }
        }

        if (!journalFolderResultRows.isEmpty()) {
            resultRowSplitterEntries.add(
                    new ResultRowSplitterEntry("folders", journalFolderResultRows));
        }

        if (!journalArticleResultRows.isEmpty()) {
            resultRowSplitterEntries.add(
                    new ResultRowSplitterEntry(
                            "web-content", journalArticleResultRows));
        }

        return resultRowSplitterEntries;
    }

}