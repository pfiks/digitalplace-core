package com.placecube.digitalplace.group.rest.application.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.liferay.petra.string.StringPool;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestContentEntryField {

	private final String name;
	private final String value;
	private final String viewURL;

	public RestContentEntryField(String name) {
		this.name = name;
		value = StringPool.BLANK;
		viewURL = null;
	}

	public RestContentEntryField(String name, String value) {
		this.name = name;
		this.value = value;
		viewURL = null;
	}

	public RestContentEntryField(String name, String value, String viewURL) {
		this.name = name;
		this.value = value;
		this.viewURL = viewURL;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public String getViewURL() {
		return viewURL;
	}

}
