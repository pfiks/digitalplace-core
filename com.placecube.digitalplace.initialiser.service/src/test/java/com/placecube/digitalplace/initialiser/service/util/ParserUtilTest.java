package com.placecube.digitalplace.initialiser.service.util;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.initialiser.service.constants.ConfigurationKeys;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.ModelCreationFactory;

public class ParserUtilTest extends PowerMockito {

	private static final long GROUP_ID = 99999;

	@InjectMocks
	private ParserUtil parserUtil;

	@Mock
	private AssetListEntry mockAssetListEntry1;

	@Mock
	private AssetListEntry mockAssetListEntry2;

	@Mock
	private AssetListEntryInitializer mockAssetListEntryInitializer;

	@Mock
	private FragmentContext mockFragmentContext;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONArray mockJSONArray2;

	@Mock
	private JSONObject mockJSONObject1;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private JSONObject mockJSONObject3;

	@Mock
	private JSONObject mockJSONObject4;

	@Mock
	private ModelCreationFactory mockModelCreationFactory;

	@Mock
	private PortletContext mockPortletContext;

	@Mock
	private Map<String, String> mockPrefs;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void configureMultivaluedPreferences_WhenMultivaluedPrefsAreNull_ThenNoActionsArePerformed() {
		when(mockJSONObject1.getJSONArray("portletMultivaluedPreferences")).thenReturn(null);

		parserUtil.configureMultivaluedPreferences(mockPortletContext, mockJSONObject1, ConfigurationKeys.DEFAULT);

		verifyZeroInteractions(mockPortletContext);
	}

	@Test
	public void configureMultivaluedPreferences_WhenMultivaluedPrefsAreEmpty_ThenNoActionsArePerformed() {
		when(mockJSONArray.length()).thenReturn(0);
		when(mockJSONObject1.getJSONArray("portletMultivaluedPreferences")).thenReturn(mockJSONArray);

		parserUtil.configureMultivaluedPreferences(mockPortletContext, mockJSONObject1, ConfigurationKeys.DEFAULT);

		verifyZeroInteractions(mockPortletContext);
	}

	@Test
	public void configureMultivaluedPreferences_WhenMultivaluedPrefsHaveValues_ThenAddsEachMultivaluedPrefToTheContext() {
		when(mockJSONArray.length()).thenReturn(2);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject2);
		when(mockJSONArray.getJSONObject(1)).thenReturn(mockJSONObject3);
		when(mockJSONObject1.getJSONArray("portletMultivaluedPreferences")).thenReturn(mockJSONArray);
		when(mockJSONObject2.getString("name")).thenReturn("prefName1");
		when(mockJSONObject2.getJSONArray("values")).thenReturn(mockJSONArray2);
		when(mockJSONArray2.length()).thenReturn(1);
		when(mockJSONArray2.getJSONObject(0)).thenReturn(mockJSONObject4);
		when(mockJSONArray2.getJSONObject(1)).thenReturn(null);
		when(mockJSONObject4.getString("value")).thenReturn("value1");

		parserUtil.configureMultivaluedPreferences(mockPortletContext, mockJSONObject1, ConfigurationKeys.DEFAULT);

		verify(mockPortletContext, times(1)).addMultivaluedPreference("prefName1", new String[] { "value1" });
	}

	@Test
	public void configurePortletPreferences_CalledWithContentPlaceholders_WhenPreferencesContainsKeyAssetListEntryId_ThenReplacesItsValueWithTheContentSetValueAndAddsEachPreference() {
		Map<String, String> contentSetPlaceholders = new HashMap<>();
		contentSetPlaceholders.put("someOtherKey", "valueDiffernet");
		contentSetPlaceholders.put("valToReplace", "val2");
		Map<String, String> portletPreferences = new HashMap<>();
		portletPreferences.put("key1", "val1");
		portletPreferences.put("assetListEntryId", "valToReplace");
		portletPreferences.put("key3", "val3");

		parserUtil.configurePortletPreferences(mockPortletContext, portletPreferences, contentSetPlaceholders);

		verify(mockPortletContext, times(1)).addPreference("key1", "val1");
		verify(mockPortletContext, times(1)).addPreference("assetListEntryId", "val2");
		verify(mockPortletContext, times(1)).addPreference("key3", "val3");
	}

	@Test
	public void configurePortletPreferences_CalledWithContentPlaceholders_WhenPreferencesDoNotContainKeyAssetListEntryId_ThenAddsEachPreferenceAsTheyAre() {
		Map<String, String> contentSetPlaceholders = new HashMap<>();
		contentSetPlaceholders.put("someOtherKey", "valueDiffernet");
		contentSetPlaceholders.put("valToReplace", "val2");
		Map<String, String> portletPreferences = new HashMap<>();
		portletPreferences.put("key1", "val1");
		portletPreferences.put("key3", "val3");

		parserUtil.configurePortletPreferences(mockPortletContext, portletPreferences, contentSetPlaceholders);

		verify(mockPortletContext, times(1)).addPreference("key1", "val1");
		verify(mockPortletContext, times(1)).addPreference("key3", "val3");
	}

	@Test
	public void configurePortletPreference_WhenNoError_ThenAddsEachPreferenceAsTheyAre() {
		Map<String, String> portletPreferences = new HashMap<>();
		portletPreferences.put("key1", "val1");
		portletPreferences.put("key3", "val3");

		parserUtil.configurePortletPreferences(mockPortletContext, portletPreferences);

		verify(mockPortletContext, times(1)).addPreference("key1", "val1");
		verify(mockPortletContext, times(1)).addPreference("key3", "val3");
	}

	@Test
	public void getContentSets_WhenArrayIsNull_ThenReturnsEmptyMap() {
		Map<String, String> results = parserUtil.getContentSets(GROUP_ID, null);

		assertThat(results.isEmpty(), equalTo(true));
	}

	@Test
	public void getContentSets_WhenNoError_ThenReturnsMapWithContentSetPlaceholderNameAndId() {
		when(mockJSONArray.length()).thenReturn(2);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject1);
		when(mockJSONArray.getJSONObject(1)).thenReturn(mockJSONObject2);
		mockContentSetDetails(mockJSONObject1, "placeholder1", "value1", mockAssetListEntry1, 12);
		mockContentSetDetails(mockJSONObject2, "placeholder2", "value2", mockAssetListEntry2, 33);

		Map<String, String> results = parserUtil.getContentSets(GROUP_ID, mockJSONArray);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get("placeholder1"), equalTo("12"));
		assertThat(results.get("placeholder2"), equalTo("33"));
	}

	@Test
	public void getExpandoValues_WhenExpandoFieldsAreNull_ThenReturnsEmptyMap() {
		when(mockJSONObject1.getJSONArray("expandoFields")).thenReturn(null);

		Map<String, Serializable> results = parserUtil.getExpandoValues(mockJSONObject1);

		assertTrue(results.isEmpty());
	}

	@Test
	public void getExpandoValues_WhenExpandoFieldsAreSpecified_ThenReturnsMapWithTheExpandoFieldNameAndFieldTypeValue() {
		when(mockJSONObject1.getJSONArray("expandoFields")).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(2);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject2);
		when(mockJSONObject2.getString("name")).thenReturn("expandoName1");
		when(mockJSONObject2.getString("value")).thenReturn("one;two;three");
		when(mockJSONObject2.getInt("type")).thenReturn(ExpandoColumnConstants.STRING_ARRAY);

		when(mockJSONArray.getJSONObject(1)).thenReturn(mockJSONObject1);
		when(mockJSONObject1.getString("name")).thenReturn("expandoName2");
		when(mockJSONObject1.getString("value")).thenReturn("one;two");
		when(mockJSONObject1.getInt("type")).thenReturn(ExpandoColumnConstants.STRING);

		Map<String, Serializable> results = parserUtil.getExpandoValues(mockJSONObject1);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get("expandoName1"), equalTo(new String[] { "one", "two", "three" }));
		assertThat(results.get("expandoName2"), equalTo("one;two"));
	}

	@Test
	public void getFragment_WhenNoError_ThenReturnsFragment() {
		when(mockJSONObject1.getString("id")).thenReturn("idValue");
		when(mockJSONObject1.getString("placeholder")).thenReturn("placeholderValue");
		when(mockJSONObject1.getString("preferences")).thenReturn("preferencesValue");
		when(mockModelCreationFactory.createFragmentContext("idValue", "placeholderValue", "preferencesValue")).thenReturn(mockFragmentContext);

		FragmentContext result = parserUtil.getFragment(mockJSONObject1);

		assertThat(result, sameInstance(mockFragmentContext));
	}

	@Test
	public void getNameValueMap_WhenJsonArrayIsNull_ThenReturnsEmptyMap() {
		Map<String, String> prefsPlaceholderValues = new HashMap<>();
		prefsPlaceholderValues.put("valueOne", "replacedValueOne");

		Map<String, String> results = parserUtil.getNameValueMap(null, prefsPlaceholderValues);

		assertThat(results.isEmpty(), equalTo(true));
	}

	@Test
	public void getNameValueMap_WhenNoError_ThenReturnsMapWithArrayEntries() {
		when(mockJSONArray.length()).thenReturn(2);
		mockJSONArrayEntry(0, mockJSONObject2, "keyOne", "valueOne");
		mockJSONArrayEntry(1, mockJSONObject3, "keyTwo", "valueTwo");
		Map<String, String> prefsPlaceholderValues = new HashMap<>();
		prefsPlaceholderValues.put("valueOne", "replacedValueOne");

		Map<String, String> results = parserUtil.getNameValueMap(mockJSONArray, prefsPlaceholderValues);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get("keyOne"), equalTo("replacedValueOne"));
		assertThat(results.get("keyTwo"), equalTo("valueTwo"));
	}

	@Test
	public void getPagePermissions_WhenJSONArrayIsNull_ThenReturnsEmptyMap() {
		String key = "keyVal";
		when(mockJSONObject1.getJSONArray(key)).thenReturn(null);

		Map<String, String[]> results = parserUtil.getPagePermissions(mockJSONObject1, key);

		assertThat(results.isEmpty(), equalTo(true));
	}

	@Test
	public void getPagePermissions_WhenNoError_ThenReturnsAmapWithRoleNamesAndActionIds() {
		String key = "keyVal";
		when(mockJSONObject1.getJSONArray(key)).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(2);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject2);
		when(mockJSONObject2.getString("roleName")).thenReturn("roleName1");
		when(mockJSONObject2.getString("actionIds")).thenReturn("actionId1a,actionId1b");
		when(mockJSONArray.getJSONObject(1)).thenReturn(mockJSONObject3);
		when(mockJSONObject3.getString("roleName")).thenReturn("roleName2");
		when(mockJSONObject3.getString("actionIds")).thenReturn("actionId2");

		Map<String, String[]> results = parserUtil.getPagePermissions(mockJSONObject1, key);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get("roleName1"), equalTo(new String[] { "actionId1a", "actionId1b" }));
		assertThat(results.get("roleName2"), equalTo(new String[] { "actionId2" }));
	}

	@Test
	public void getPortletPreferencesWithPlaceholders_WhenNoError_ThenReturnsMapForThePortletPreferences() {
		String keyOne = "keyOne";
		String placeholderOne = "placeholder1";
		String placeholderValueToUseOne = "valueToUse1";
		String keyTwo = "keyTwo";
		String valueTwo = "valueTwo";
		when(mockJSONObject1.getJSONArray("portletPreferences")).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(2);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject2);
		when(mockJSONArray.getJSONObject(1)).thenReturn(mockJSONObject3);
		when(mockJSONObject2.getString("name")).thenReturn(keyOne);
		when(mockJSONObject2.getString("value")).thenReturn(placeholderOne);
		when(mockJSONObject3.getString("name")).thenReturn(keyTwo);
		when(mockJSONObject3.getString("value")).thenReturn(valueTwo);

		Map<String, String> prefsPlaceholderValues = new HashMap<>();
		prefsPlaceholderValues.put(placeholderOne, placeholderValueToUseOne);

		Map<String, String> results = parserUtil.getPortletPreferencesWithPlaceholders(mockJSONObject1, prefsPlaceholderValues, ConfigurationKeys.DEFAULT);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get(keyOne), equalTo(placeholderValueToUseOne));
		assertThat(results.get(keyTwo), equalTo(valueTwo));
	}

	@Test
	public void getPortletPreferencesWithPlaceholders_WhenNoPreferencesSpecified_ThenReturnsEmptyMap() {
		when(mockJSONObject1.getJSONArray("portletPreferences")).thenReturn(null);

		Map<String, String> prefsPlaceholderValues = new HashMap<>();
		prefsPlaceholderValues.put("placeholderOne", "placeholderValueToUseOne");

		Map<String, String> results = parserUtil.getPortletPreferencesWithPlaceholders(mockJSONObject1, prefsPlaceholderValues, ConfigurationKeys.DEFAULT);

		assertThat(results.size(), equalTo(0));
	}

	@Test
	public void getPortletWithPlaceholder_WhenNoError_ThenReturnsPortlet() {
		String portletId = "portletIdValue";
		String placeholder = "placeholderValue";
		String customInstanceId = "customInstanceIdValue";
		when(mockJSONObject1.getString("portletId")).thenReturn(portletId);
		when(mockJSONObject1.getString("placeholder")).thenReturn(placeholder);
		when(mockJSONObject1.getString("customInstanceId", StringPool.BLANK)).thenReturn(customInstanceId);
		when(mockModelCreationFactory.createPortletContext(portletId, placeholder, customInstanceId)).thenReturn(mockPortletContext);

		PortletContext result = parserUtil.getPortletWithPlaceholder(mockJSONObject1, ConfigurationKeys.DEFAULT);

		assertThat(result, sameInstance(mockPortletContext));
	}

	@Test
	public void getPortletWithPreferences_WhenNoError_ThenReturnsPortlet() {
		String portletId = "portletIdValue";
		String customInstanceId = "customInstanceIdValue";
		when(mockJSONObject1.getString("portletId")).thenReturn(portletId);
		when(mockJSONObject1.getString("customInstanceId", StringPool.BLANK)).thenReturn(customInstanceId);
		when(mockModelCreationFactory.createPortletContext(portletId, mockPrefs, customInstanceId)).thenReturn(mockPortletContext);

		PortletContext result = parserUtil.getPortletWithPreferences(mockJSONObject1, mockPrefs, ConfigurationKeys.DEFAULT);

		assertThat(result, sameInstance(mockPortletContext));
	}

	private void mockContentSetDetails(JSONObject jsonObject, String name, String value, AssetListEntry assetListEntry, long id) {
		when(jsonObject.getString("name")).thenReturn(name);
		when(jsonObject.getString("value")).thenReturn(value);
		when(mockAssetListEntryInitializer.getContentSet(GROUP_ID, value)).thenReturn(Optional.of(assetListEntry));
		when(assetListEntry.getAssetListEntryId()).thenReturn(id);
	}

	private void mockJSONArrayEntry(int index, JSONObject jsonObject, String name, String value) {
		when(mockJSONArray.getJSONObject(index)).thenReturn(jsonObject);
		when(jsonObject.getString("name")).thenReturn(name);
		when(jsonObject.getString("value")).thenReturn(value);
	}

}
