package com.placecube.digitalplace.externalpage.search.index.contributor;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.search.batch.BatchIndexingActionable;
import com.liferay.portal.search.batch.DynamicQueryBatchIndexingActionableFactory;
import com.liferay.portal.search.spi.model.index.contributor.ModelIndexerWriterContributor;
import com.liferay.portal.search.spi.model.index.contributor.helper.IndexerWriterMode;
import com.liferay.portal.search.spi.model.index.contributor.helper.ModelIndexerWriterDocumentHelper;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage", service = ModelIndexerWriterContributor.class)
public class ExternalPageModelIndexerWriterContributor implements ModelIndexerWriterContributor<ExternalPage> {

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Reference
	private DynamicQueryBatchIndexingActionableFactory dynamicQueryBatchIndexingActionableFactory;

	@Override
	public void customize(BatchIndexingActionable batchIndexingActionable, ModelIndexerWriterDocumentHelper modelIndexerWriterDocumentHelper) {
		batchIndexingActionable.setPerformActionMethod((ExternalPage externalPage) -> batchIndexingActionable.addDocuments(modelIndexerWriterDocumentHelper.getDocument(externalPage)));
	}

	@Override
	public BatchIndexingActionable getBatchIndexingActionable() {
		return dynamicQueryBatchIndexingActionableFactory.getBatchIndexingActionable(externalPageLocalService.getIndexableActionableDynamicQuery());
	}

	@Override
	public long getCompanyId(ExternalPage externalPage) {
		return externalPage.getCompanyId();
	}

	@Override
	public IndexerWriterMode getIndexerWriterMode(ExternalPage externalPage) {
		return IndexerWriterMode.UPDATE;
	}

}
