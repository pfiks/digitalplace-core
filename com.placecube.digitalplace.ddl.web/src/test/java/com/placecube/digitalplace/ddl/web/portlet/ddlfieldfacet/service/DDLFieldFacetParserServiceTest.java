package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.model.FilterField;
import com.placecube.digitalplace.search.shared.model.FacetResult;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ FilterField.class, FacetResult.class, DDLFieldFacetParserService.class })
public class DDLFieldFacetParserServiceTest extends PowerMockito {

	private static final String DDM_FIELD_NAME = "FieldOne";

	private static final Locale LOCALE = Locale.ENGLISH;

	@InjectMocks
	private DDLFieldFacetParserService ddlFieldFacetParserService;

	@Mock
	private DDMFormField mockDDMFormField1;

	@Mock
	private DDMFormField mockDDMFormField2;

	@Mock
	private DDMFormField mockDDMFormField3;

	@Mock
	private DDLFieldFacetRecordService mockDDLFieldFacetRecordService;

	@Mock
	private DDLFieldFacetDDMFormFieldService mockDDLFieldFacetDDMFormFieldService;

	@Mock
	private DDMFormFieldOptions mockDDMFormFieldOptions;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private FacetResult mockFacetResultOne;

	@Mock
	private FacetResult mockFacetResultTwo;

	@Mock
	private FilterField mockFilterField1;

	@Mock
	private FilterField mockFilterField2;

	@Mock
	private LocalizedValue mockLocalizedValueOne;

	@Mock
	private LocalizedValue mockLocalizedValueTwo;

	@Mock
	private Set<String> mockSelectedValues;

	@Mock
	private Map<Locale, String> mockValuesOne;

	@Mock
	private Map<Locale, String> mockValuesTwo;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private DDLRecord mockDDLRecord;

	@Captor
	private ArgumentCaptor<Map<Locale, String>> labelsMapCaptor;

	@Before
	public void activateSetup() {
		mockStatic(FilterField.class, FacetResult.class);
	}

	@Test
	public void getIndexedFieldName_WhenNoError_ThenReturnIntexedFieldNameInFightFormat() {
		Long structureId = 11l;
		String defaultLanguageId = "en_GB";
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructure.getDefaultLanguageId()).thenReturn(defaultLanguageId);
		String expected = "ddm__keyword__11__FieldOne_en_GB";

		String result = ddlFieldFacetParserService.getIndexedFieldName(mockDDMStructure, DDM_FIELD_NAME);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void parseFacetsWithFieldOptions_WhenNoError_ThenFacetsParsed() {
		String facetValue = "v1";
		int counter = 2;

		Map<String, Integer> facetValues = new HashMap<>();
		facetValues.put(facetValue, counter);

		when(mockSelectedValues.contains(facetValue)).thenReturn(true);
		when(mockDDMFormFieldOptions.getOptionLabels(facetValue)).thenReturn(mockLocalizedValueOne);
		when(mockLocalizedValueOne.getValues()).thenReturn(mockValuesOne);

		when(FacetResult.init(facetValue, counter, true, mockValuesOne)).thenReturn(mockFacetResultOne);

		List<FacetResult> result = ddlFieldFacetParserService.parseFacetsWithFieldOptions(facetValues, mockSelectedValues, mockDDMFormFieldOptions);

		assertThat(result.size(), equalTo(1));
		assertTrue(result.contains(mockFacetResultOne));
	}

	@Test
	public void parseFacetsWithFieldOptions_WhenNoFacetValues_ThenReturnEmptyResult() {
		Map<String, Integer> facetValues = new HashMap<>();

		List<FacetResult> result = ddlFieldFacetParserService.parseFacetsWithFieldOptions(facetValues, null, null);

		assertThat(result.size(), equalTo(0));
	}

	@Test
	public void parseFacetsWithFieldOptions_WhenNoLabelsForDDMField_ThenFacetsParsed() {
		String facetValue = "v1";
		int counter = 2;

		Map<String, Integer> facetValues = new HashMap<>();
		facetValues.put(facetValue, counter);

		when(mockSelectedValues.contains(facetValue)).thenReturn(true);
		when(mockDDMFormFieldOptions.getOptionLabels(facetValue)).thenReturn(null);

		when(FacetResult.init(anyString(), anyInt(), anyBoolean(), any(Map.class))).thenReturn(mockFacetResultOne);

		List<FacetResult> result = ddlFieldFacetParserService.parseFacetsWithFieldOptions(facetValues, mockSelectedValues, mockDDMFormFieldOptions);

		assertThat(result.size(), equalTo(1));
		assertTrue(result.contains(mockFacetResultOne));
	}

	@Test
	public void parseAllFieldOptions_WhenNoError_ThenReturnsFacetResultsForAllFieldOptions() {
		String facetValueOne = "v1";
		int counterOne = 2;

		String facetValueTwo = "v2";
		int counterTwo = 1;

		Map<String, Integer> facetValues = new HashMap<>();
		facetValues.put(facetValueOne, counterOne);
		facetValues.put(facetValueTwo, counterTwo);

		when(mockSelectedValues.contains(facetValueOne)).thenReturn(true);
		when(mockDDMFormFieldOptions.getOptionsValues()).thenReturn(facetValues.keySet());

		when(mockDDMFormFieldOptions.getOptionLabels(facetValueOne)).thenReturn(mockLocalizedValueOne);
		when(mockLocalizedValueOne.getValues()).thenReturn(mockValuesOne);

		when(mockDDMFormFieldOptions.getOptionLabels(facetValueTwo)).thenReturn(mockLocalizedValueTwo);
		when(mockLocalizedValueTwo.getValues()).thenReturn(mockValuesTwo);

		when(FacetResult.init(facetValueOne, counterOne, true, mockValuesOne)).thenReturn(mockFacetResultOne);
		when(FacetResult.init(facetValueTwo, counterTwo, false, mockValuesTwo)).thenReturn(mockFacetResultTwo);

		List<FacetResult> result = ddlFieldFacetParserService.parseAllFieldOptions(facetValues, mockSelectedValues, mockDDMFormFieldOptions);

		assertThat(result.size(), equalTo(2));
		assertTrue(result.contains(mockFacetResultOne));
		assertTrue(result.contains(mockFacetResultTwo));
	}

	@Test
	public void parseAllFieldOptions_WhenNoFieldOptions_ThenReturnEmptyResult() {
		Map<String, Integer> facetValues = new HashMap<>();

		when(mockDDMFormFieldOptions.getOptionsValues()).thenReturn(new HashSet<>());
		List<FacetResult> result = ddlFieldFacetParserService.parseAllFieldOptions(facetValues, mockSelectedValues, mockDDMFormFieldOptions);

		assertThat(result.size(), equalTo(0));
	}

	@Test
	public void parseAllFieldOptions_WhenThereAreNoOptionsLabels_ThenReturnsFacetResultsWithFacetValueLabelForUKLocale() {
		String facetValueOne = "v1";
		int counterOne = 2;

		Map<String, Integer> facetValues = new HashMap<>();
		facetValues.put(facetValueOne, counterOne);

		when(mockSelectedValues.contains(facetValueOne)).thenReturn(true);
		when(mockDDMFormFieldOptions.getOptionsValues()).thenReturn(facetValues.keySet());

		when(mockDDMFormFieldOptions.getOptionLabels(facetValueOne)).thenReturn(null);

		ddlFieldFacetParserService.parseAllFieldOptions(facetValues, mockSelectedValues, mockDDMFormFieldOptions);

		verifyStatic(FacetResult.class);
		FacetResult.init(eq(facetValueOne), eq(counterOne), eq(true), labelsMapCaptor.capture());

		Map<Locale, String> capturedMap = labelsMapCaptor.getValue();

		assertThat(capturedMap.size(), equalTo(1));
		assertThat(capturedMap.get(Locale.UK), equalTo(facetValueOne));
	}

	@Test
	public void parseAllFieldOptions_WhenTheresNoFacetForFieldOption_ThenReturnsFacetResultsForFieldOptionWithZeroFrequency() {
		final String optionWithoutFacet = "option1";
		final String facetValueOne = "v1";
		final int counterOne = 2;

		Map<String, Integer> facetValues = new HashMap<>();
		facetValues.put(facetValueOne, counterOne);

		Set<String> fieldOptions = new HashSet<>();
		fieldOptions.add(facetValueOne);
		fieldOptions.add(optionWithoutFacet);
		
		when(mockSelectedValues.contains(facetValueOne)).thenReturn(true);
		when(mockDDMFormFieldOptions.getOptionsValues()).thenReturn(fieldOptions);

		when(mockDDMFormFieldOptions.getOptionLabels(facetValueOne)).thenReturn(mockLocalizedValueOne);
		when(mockLocalizedValueOne.getValues()).thenReturn(mockValuesOne);

		when(mockDDMFormFieldOptions.getOptionLabels(optionWithoutFacet)).thenReturn(mockLocalizedValueTwo);
		when(mockLocalizedValueTwo.getValues()).thenReturn(mockValuesTwo);

		when(FacetResult.init(facetValueOne, counterOne, true, mockValuesOne)).thenReturn(mockFacetResultOne);
		when(FacetResult.init(optionWithoutFacet, 0, false, mockValuesTwo)).thenReturn(mockFacetResultTwo);

		List<FacetResult> result = ddlFieldFacetParserService.parseAllFieldOptions(facetValues, mockSelectedValues, mockDDMFormFieldOptions);

		assertThat(result.size(), equalTo(2));
		assertTrue(result.contains(mockFacetResultOne));
		assertTrue(result.contains(mockFacetResultTwo));
	}
	
	@Test
	public void parseFacetsWithFacetValues_WhenThereAreSelectedValuesAndFieldIsNotADDLRecordLink_ThenReturnsSetContainingParsedFacetResultWithFacetValueLabel() throws Exception {
		String facetValue = "v1";
		int counter = 2;

		DDLFieldFacetParserService spiedDDLFieldFacetParserService = spy(ddlFieldFacetParserService);

		Map<String, Integer> facetValues = new HashMap<>();
		facetValues.put(facetValue, counter);

		when(mockSelectedValues.contains(facetValue)).thenReturn(true);
		doReturn(mockValuesOne).when(spiedDDLFieldFacetParserService, "getSingleValuedLocalizedMap", LOCALE, facetValue);

		when(FacetResult.init(facetValue, counter, true, mockValuesOne)).thenReturn(mockFacetResultOne);

		List<FacetResult> result = spiedDDLFieldFacetParserService.parseFacetsWithFacetValues(facetValues, mockSelectedValues, false, StringPool.BLANK, LOCALE);

		assertThat(result.size(), equalTo(1));
		assertTrue(result.contains(mockFacetResultOne));
	}

	@Test
	public void parseFacetsWithFacetValues_WhenThereAreSelectedValuesAndFieldIsADDLRecordLink_ThenReturnsSetContainingParsedFacetResultWithDDLRecordLinkFieldLabel() throws Exception {
		String facetValue = "234";
		String linkFieldName = "linkFieldName";
		String linkFieldValue = "value";
		int counter = 2;

		DDLFieldFacetParserService spiedDDLFieldFacetParserService = spy(ddlFieldFacetParserService);

		Map<String, Integer> facetValues = new HashMap<>();
		facetValues.put(facetValue, counter);

		when(mockSelectedValues.contains(facetValue)).thenReturn(true);
		when(mockDDLFieldFacetRecordService.getDDLRecordFieldValue(Long.valueOf(facetValue), linkFieldName, LOCALE)).thenReturn(linkFieldValue);
		doReturn(mockValuesOne).when(spiedDDLFieldFacetParserService, "getSingleValuedLocalizedMap", LOCALE, linkFieldValue);

		when(FacetResult.init(facetValue, counter, true, mockValuesOne)).thenReturn(mockFacetResultOne);

		List<FacetResult> result = spiedDDLFieldFacetParserService.parseFacetsWithFacetValues(facetValues, mockSelectedValues, true, linkFieldName, LOCALE);

		assertThat(result.size(), equalTo(1));
		assertTrue(result.contains(mockFacetResultOne));
	}

	@Test
	public void parseFacetsWithFacetValues_WhenThereAreNoSelectedValues_ThenReturnEmptyResult() {
		Map<String, Integer> facetValues = new HashMap<>();

		List<FacetResult> result = ddlFieldFacetParserService.parseFacetsWithFacetValues(facetValues, null, false, StringPool.BLANK, LOCALE);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void parseAllFacetValues_WhenIsDDLRecordLinkAndLinkStructureIdIsGreaterThanZeroAndThereAreRecords_ThenReturnsFacetResultsForAllDDLRecordsInStructure() throws Exception {
		final String ddlRecordLinkFieldName = "linkFieldName";
		final String linkFieldValue = "value";
		final long ddmLinkStructureId = 123L;
		String facetValue = "152";
		int counter = 2;

		Map<String, Integer> facetValues = new HashMap<>();
		facetValues.put(facetValue, counter);

		DDLFieldFacetParserService spiedDDLFieldFacetParserService = spy(ddlFieldFacetParserService);

		when(mockHttpServletRequest.getLocale()).thenReturn(LOCALE);
		when(mockDDLFieldFacetRecordService.searchDDLRecords(ddmLinkStructureId, mockHttpServletRequest)).thenReturn(Arrays.asList(new DDLRecord[] { mockDDLRecord }));
		when(mockDDLRecord.getRecordId()).thenReturn(Long.parseLong(facetValue));
		when(mockDDLFieldFacetDDMFormFieldService.getDDMFormFieldStringValue(mockDDLRecord, ddlRecordLinkFieldName, LOCALE)).thenReturn(linkFieldValue);
		when(mockSelectedValues.contains(facetValue)).thenReturn(true);
		doReturn(mockValuesOne).when(spiedDDLFieldFacetParserService, "getSingleValuedLocalizedMap", LOCALE, linkFieldValue);

		when(FacetResult.init(facetValue, counter, true, mockValuesOne)).thenReturn(mockFacetResultOne);

		List<FacetResult> result = spiedDDLFieldFacetParserService.parseAllFacetValues(facetValues, mockSelectedValues, true, ddlRecordLinkFieldName, ddmLinkStructureId, mockHttpServletRequest);

		assertThat(result.size(), equalTo(1));
		assertTrue(result.contains(mockFacetResultOne));
	}

	@Test
	public void parseAllFacetValues_WhenIsDDLRecordLinkAndLinkStructureIdIsGreaterThanZeroAndThereAreRecordsValueIsNotInFacetsList_ThenReturnsFacetResultsWithZeroOccurrences() throws Exception {
		final String ddlRecordLinkFieldName = "linkFieldName";
		final String linkFieldValue = "value";
		final long ddmLinkStructureId = 123L;
		String facetValue = "152";

		DDLFieldFacetParserService spiedDDLFieldFacetParserService = spy(ddlFieldFacetParserService);

		when(mockHttpServletRequest.getLocale()).thenReturn(LOCALE);
		when(mockDDLFieldFacetRecordService.searchDDLRecords(ddmLinkStructureId, mockHttpServletRequest)).thenReturn(Arrays.asList(new DDLRecord[] { mockDDLRecord }));
		when(mockDDLRecord.getRecordId()).thenReturn(Long.parseLong(facetValue));
		when(mockDDLFieldFacetDDMFormFieldService.getDDMFormFieldStringValue(mockDDLRecord, ddlRecordLinkFieldName, LOCALE)).thenReturn(linkFieldValue);
		when(mockSelectedValues.contains(facetValue)).thenReturn(true);
		doReturn(mockValuesOne).when(spiedDDLFieldFacetParserService, "getSingleValuedLocalizedMap", LOCALE, linkFieldValue);

		when(FacetResult.init(facetValue, 0, true, mockValuesOne)).thenReturn(mockFacetResultOne);

		List<FacetResult> result = spiedDDLFieldFacetParserService.parseAllFacetValues(new HashMap<>(), mockSelectedValues, true, ddlRecordLinkFieldName, ddmLinkStructureId, mockHttpServletRequest);

		assertThat(result.size(), equalTo(1));
		assertTrue(result.contains(mockFacetResultOne));
	}

	@Test
	public void parseAllFacetValues_WhenIsDDLRecordLinkAndLinkStructureIdIsGreaterThanZeroAndSearchFails_ThenReturnsEmptyFacetResultsList() throws Exception {
		final String ddlRecordLinkFieldName = "linkFieldName";
		final long ddmLinkStructureId = 123L;

		when(mockDDLFieldFacetRecordService.searchDDLRecords(ddmLinkStructureId, mockHttpServletRequest)).thenThrow(new PortalException());

		List<FacetResult> result = ddlFieldFacetParserService.parseAllFacetValues(new HashMap<>(), mockSelectedValues, true, ddlRecordLinkFieldName, ddmLinkStructureId, mockHttpServletRequest);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void parseAllFacetValues_WhenIsNotDDLRecordLink_ThenReturnsEmptyFacetResultsList() throws Exception {
		final String ddlRecordLinkFieldName = "linkFieldName";
		final long ddmLinkStructureId = 123L;

		List<FacetResult> result = ddlFieldFacetParserService.parseAllFacetValues(new HashMap<>(), mockSelectedValues, false, ddlRecordLinkFieldName, ddmLinkStructureId, mockHttpServletRequest);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void parseAllFacetValues_WhenDDMLinkStructureIdIsZero_ThenReturnsEmptyFacetResultsList() throws Exception {
		final String ddlRecordLinkFieldName = "linkFieldName";

		List<FacetResult> result = ddlFieldFacetParserService.parseAllFacetValues(new HashMap<>(), mockSelectedValues, true, ddlRecordLinkFieldName, 0L, mockHttpServletRequest);

		assertThat(result.isEmpty(), equalTo(true));
	}

	@Test
	public void parseToFilterFields_WhenAllFieldsAreInvalid_ThenReturnEmptyResult() {

		List<DDMFormField> ddmFormFields = new ArrayList<>();
		ddmFormFields.add(mockDDMFormField1);
		ddmFormFields.add(mockDDMFormField2);

		when(mockDDMFormField1.isRepeatable()).thenReturn(true);

		when(mockDDMFormField2.isRepeatable()).thenReturn(true);

		when(FilterField.init(mockDDMFormField1)).thenReturn(mockFilterField1);
		when(FilterField.init(mockDDMFormField2)).thenReturn(mockFilterField2);

		Set<FilterField> result = ddlFieldFacetParserService.parseToFilterFields(ddmFormFields);

		assertThat(result.size(), equalTo(0));
	}

	@Test
	public void parseToFilterFields_WhenAllFieldsAreValid_ThenParseAllFields() {

		List<DDMFormField> ddmFormFields = new ArrayList<>();
		ddmFormFields.add(mockDDMFormField1);
		ddmFormFields.add(mockDDMFormField2);

		when(mockDDMFormField1.isRepeatable()).thenReturn(false);
		when(mockDDMFormField2.isRepeatable()).thenReturn(false);

		when(FilterField.init(mockDDMFormField1)).thenReturn(mockFilterField1);
		when(FilterField.init(mockDDMFormField2)).thenReturn(mockFilterField2);

		Set<FilterField> result = ddlFieldFacetParserService.parseToFilterFields(ddmFormFields);

		assertThat(result.size(), equalTo(2));
		assertTrue(result.contains(mockFilterField1));
		assertTrue(result.contains(mockFilterField2));
	}

	@Test
	public void parseToFilterFields_WhenSomeFieldsAreInvalid_ThenOnlyParseValidFields() {

		List<DDMFormField> ddmFormFields = new ArrayList<>();
		ddmFormFields.add(mockDDMFormField1);
		ddmFormFields.add(mockDDMFormField2);

		when(mockDDMFormField1.isRepeatable()).thenReturn(false);
		when(mockDDMFormField2.isRepeatable()).thenReturn(true);

		when(FilterField.init(mockDDMFormField1)).thenReturn(mockFilterField1);
		when(FilterField.init(mockDDMFormField2)).thenReturn(mockFilterField2);

		Set<FilterField> result = ddlFieldFacetParserService.parseToFilterFields(ddmFormFields);

		assertThat(result.size(), equalTo(1));
		assertTrue(result.contains(mockFilterField1));
	}

}
