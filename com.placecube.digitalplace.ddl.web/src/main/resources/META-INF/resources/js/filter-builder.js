AUI.add(
	'com-placecube-field-filter-builder',
	function (A) {

		var groupTemplate;

		var optionTemplate;

		var hiddenFormField;

		var ns;

		var FieldFilterBuilder = {

			init: function (params) {
				groupTemplate = params.groupTemplate;
				optionTemplate = params.optionTemplate;
				hiddenFormField = params.hiddenFormField;
				ns = params.ns;

				FieldFilterBuilder.buildForm(params.jsonFilterConfig);

				$('#' + ns + 'addFilterGroupBtn').click(function (e) {
					FieldFilterBuilder.createFilterGroup();
				});

				$('#' + ns + 'filterGroups').on("click", "button.add-filter", function (event) {
					FieldFilterBuilder.createFilterFromClick(event.currentTarget);
				});

				$('#' + ns + 'filterGroups').on("click", "button.add-option", function (event) {
					FieldFilterBuilder.createFilterOptionFromClick(event.currentTarget);
				});

				$('#' + ns + 'filterGroups').on("click", "a.remove-group", function (event) {
					FieldFilterBuilder.removeItem(event, event.currentTarget, '.filter-group');
				});

				$('#' + ns + 'filterGroups').on("click", "a.remove-option", function (event) {
					FieldFilterBuilder.removeItem(event, event.currentTarget, '.filter-option');
				});

				$('#' + ns + 'filterGroups').on("click", "a.remove-filter", function (event) {
					FieldFilterBuilder.removeItem(event, event.currentTarget, '.field-filter');
				});
				
				$('#' + ns + 'filterGroups').on("change", ".filter-field-selector", function (event) {
					FieldFilterBuilder.updateAvailableInputs(event.currentTarget);
				});
			},

			createFilterGroup: function () {

				var templateHtml = groupTemplate.html().trim();

				var newFilterGroup = document.createElement('div');
				$(newFilterGroup).addClass('filter-group');
				newFilterGroup.innerHTML = templateHtml;

				var filterGroups = document.getElementById(ns + 'filterGroups');
				filterGroups.appendChild(newFilterGroup);
				return newFilterGroup;
			},

			createFilterOptionFromClick: function (element) {
				var filterValuesDiv = $(element).siblings('.filter-options').first().get(0);
				return FieldFilterBuilder.createFilterOption(filterValuesDiv);
			},

			createFilterOption: function (filterValuesDiv) {

				var templateHtml = optionTemplate.html().trim();

				var newFilterOption = document.createElement('div');
				$(newFilterOption).addClass('filter-option');
				newFilterOption.innerHTML = templateHtml;

				filterValuesDiv.appendChild(newFilterOption);
				return newFilterOption;

			},

			createFilterFromClick: function (element) {

				var filterValuesDiv = $(element).siblings('.filters').first().get(0);

				return FieldFilterBuilder.createFilter(filterValuesDiv);
			},

			createFilter: function (filterValuesDiv) {

				var templateHtml = $('#' + ns + 'field-filter-template').html().trim();

				var newFilterValue = document.createElement('div');
				$(newFilterValue).addClass('field-filter');
				newFilterValue.innerHTML = templateHtml;

				filterValuesDiv.appendChild(newFilterValue);
				return newFilterValue;
			},

			formToJson: function () {

				var groups = [];

				$('.filter-group').each(function () {
					var group = {};
					group.filterGroupName = $(this).find('[name="' + ns + 'filterGroupName"]').val();

					if (!group.filterGroupName) {
						return;
					}

					group.filterPreselectedOptionName = $(this).find('[name="' + ns + 'filterPreselectedOptionName"]').val();

					var options = [];
					group.options = options;
					groups.push(group);
					$(this).find('.filter-option').each(function () {

						var option = {};
						var filters = [];
						option.filters = filters;

						option.filterOptionName = $(this).find('[name="' + ns + 'filterOptionName"]').val();
						if (!option.filterOptionName) {
							return;
						}
						
						$(this).find('.field-filter').each(function () {
							var filter = {};
							
							filter.fieldName = $(this).find('[name="' + ns + 'fieldName"]').val();
							filter.filterOperator = $(this).find('[name="' + ns + 'filterOperator"]').val();
							filter.fieldValue = $(this).find('[name="' + ns + 'fieldValue"]:not(:disabled)').val();
							filter.numDays = $(this).find('[name="' + ns + 'numDays"]').val();
							filter.fieldType = $(this).find('[name="' + ns + 'fieldType"]').val();
							
							if (filter.fieldName && filter.filterOperator) {
								filters.push(filter);
							}
						});

						if (filters.length > 0) {
							options.push(option);
						} 
						
					});
				});


				hiddenFormField.val(JSON.stringify(groups));
			},

			removeItem: function (event, element, selector) {
				event.preventDefault();
				var nodeToRemove = $(element).parents(selector).first().get(0);
				nodeToRemove.parentNode.removeChild(nodeToRemove);
			},

			updateAvailableInputs: function (target) {
				var optionSelectedValue = $(target).val();
			
				if (optionSelectedValue != "") {
					var selectedElement = $(target).find("option[value=" + optionSelectedValue + "]");
					var tableRow = $(target).closest("tr");
					
					var type = selectedElement.attr('data-type');
					
					tableRow.find("input.field-type").val(type);
					
					FieldFilterBuilder.showAndHideFilterFields(tableRow, type);
				}
			},
			
			showAndHideFilterFields: function (filterElement, type) {
				if (type == "date") {
					filterElement.find(".date-field-wrapper").show();
					filterElement.find(".date-field-wrapper input").prop('disabled', false);
					filterElement.find(".num-days-cell").show();
					filterElement.find(".number-field-wrapper").hide();
					filterElement.find(".number-field-wrapper input").prop('disabled', true);
				}
				else {
					filterElement.find(".number-field-wrapper").show();
					filterElement.find(".number-field-wrapper input").prop('disabled', false);
					filterElement.find(".date-field-wrapper").hide();
					filterElement.find(".date-field-wrapper input").prop('disabled', true);
					filterElement.find(".num-days-cell").hide();
				}
			},

			buildForm: function (jsonConfigData) {

				var existingFilterGroups = JSON.parse(jsonConfigData);

				for (var i = 0; i < existingFilterGroups.length; i++) {
					var filterGroup = existingFilterGroups[i];
					var options = filterGroup.options;

					var filterGroupElement = FieldFilterBuilder.createFilterGroup();
					$(filterGroupElement).find('[name="' + ns + 'filterGroupName"]').val(filterGroup.filterGroupName);
					$(filterGroupElement).find('[name="' + ns + 'filterPreselectedOptionName"]').val(filterGroup.filterPreselectedOptionName);

					for (var x = 0; x < options.length; x++) {
						var option = options[x];
						var filterOptionElement = FieldFilterBuilder.createFilterOption($(filterGroupElement).find('.filter-options').get(0));
						$(filterOptionElement).find('[name="' + ns + 'filterOptionName"]').val(option.filterOptionName);

						var filters = option.filters;
						var filtersElement = ($(filterOptionElement).find('.filters').get(0));
						for (var y = 0; y < filters.length; y++) {
							var filter = filters[y];
							var filterElement = FieldFilterBuilder.createFilter(filtersElement);
							$(filterElement).find('[name="' + ns + 'fieldName"]').val(filter.fieldName);
							$(filterElement).find('[name="' + ns + 'filterOperator"]').val(filter.filterOperator);
							$(filterElement).find('[name="' + ns + 'fieldValue"]').val(filter.fieldValue);
							$(filterElement).find('[name="' + ns + 'numDays"]').val(filter.numDays);
							$(filterElement).find('[name="' + ns + 'fieldType"]').val(filter.fieldType);
							
							FieldFilterBuilder.showAndHideFilterFields($(filterElement), filter.fieldType);
						}

					}

				}
			}
		};

		A.FieldFilterBuilder = FieldFilterBuilder;

	},
	'', {
	requires: [
		'aui-base'
	]
});