package com.placecube.digitalplace.user.serviceoverride.internal.util;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = ReindexUserUtil.class)
public class ReindexUserUtil {

	private static final Log LOG = LogFactoryUtil.getLog(ReindexUserUtil.class);

	@Reference
	private IndexerRegistry indexerRegistry;

	public void reindexUser(long userId) {
		try {
			if (userId > 0) {
				indexerRegistry.nullSafeGetIndexer(User.class.getName()).reindex(User.class.getName(), userId);
			}
		} catch (SearchException e) {
			LOG.warn("Unable to reindex user - " + e.getMessage());
			LOG.debug(e);
		}
	}

	public void reindexUser(User user) {
		try {
			if (Validator.isNotNull(user)) {
				indexerRegistry.nullSafeGetIndexer(User.class.getName()).reindex(user);
			}
		} catch (SearchException e) {
			LOG.warn("Unable to reindex user - " + e.getMessage());
			LOG.debug(e);
		}
	}

}
