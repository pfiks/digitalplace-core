package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.contributor;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.filter.RangeTermFilter;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.service.ExpiredContentFilterSearchService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.EXPIRED_CONTENT_FILTER_SEARCH, service = SharedSearchContributor.class)
public class ExpiredContentFilterSearchPortletSharedSearchContributor implements SharedSearchContributor {

	@Reference
	private ExpiredContentFilterSearchService expiredContentFilterSearchService;

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<PortletPreferences> portletPreferences = sharedSearchContributorSettings.getPortletPreferences();

		if (portletPreferences.isPresent()) {
			boolean includeExpiredContent = expiredContentFilterSearchService.getIncludeExpiredContentValue(sharedSearchContributorSettings);
			String dateParamName = portletPreferences.get().getValue("dateParameterName", StringPool.BLANK);
			if (!includeExpiredContent && Validator.isNotNull(dateParamName)) {
				String lowerTerm = getTodayAtStartOfDay();
				String upperTerm = getMaxDate();

				RangeTermFilter rangeTermFilter = new RangeTermFilter(dateParamName, true, true, lowerTerm, upperTerm);
				sharedSearchContributorSettings.addRangeTermFilterClause(rangeTermFilter, BooleanClauseOccur.MUST);
			}
		}

	}

	private String getMaxDate() {
		return String.valueOf(LocalDateTime.now().plusYears(200).toInstant(ZoneOffset.UTC).toEpochMilli());
	}

	private String getTodayAtStartOfDay() {
		return String.valueOf(Instant.now().truncatedTo(ChronoUnit.DAYS).toEpochMilli());
	}

}
