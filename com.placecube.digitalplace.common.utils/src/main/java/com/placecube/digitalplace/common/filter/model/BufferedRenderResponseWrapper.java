package com.placecube.digitalplace.common.filter.model;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.portlet.RenderResponse;
import javax.portlet.filter.RenderResponseWrapper;

import com.liferay.petra.string.StringPool;

public class BufferedRenderResponseWrapper extends RenderResponseWrapper {

	private CharArrayWriter charArrayWriter;

	private boolean getOutputStreamCalled;

	private boolean getWriterCalled;

	private PrintWriter printWriter;

	public BufferedRenderResponseWrapper(RenderResponse renderResponse) {

		super(renderResponse);

		charArrayWriter = new CharArrayWriter();

	}

	public OutputStream getOutputStream() throws IOException {

		if (getWriterCalled) {
			throw new IllegalStateException("getWriter already called");
		}

		getOutputStreamCalled = true;

		return super.getPortletOutputStream();
	}

	@Override
	public PrintWriter getWriter() {

		if (printWriter != null) {
			return printWriter;
		}

		if (getOutputStreamCalled) {
			throw new IllegalStateException("getOutputStream already called");
		}

		getWriterCalled = true;

		printWriter = new PrintWriter(charArrayWriter);

		return printWriter;
	}

	@Override
	public String toString() {

		String string;

		if (printWriter != null) {
			string = charArrayWriter.toString();
		} else {
			string = StringPool.BLANK;
		}

		return string;

	}

}
