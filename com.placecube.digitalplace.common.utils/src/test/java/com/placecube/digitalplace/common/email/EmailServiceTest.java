package com.placecube.digitalplace.common.email;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.CalendarFactoryUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalendarFactoryUtil.class)
public class EmailServiceTest extends PowerMockito {

	@InjectMocks
	private EmailService emailService;

	@Mock
	private Calendar mockCalendar;

	@Before
	public void activateSetup() {
		mockStatic(CalendarFactoryUtil.class);
	}

	@Test
	public void getGeneratedEmailAddress_WhenNoErrorAndDomainAlreadyContainsAt_ThenReturnsTheEmailAddressGeneratedUsingAlphaCharsOnly() throws Exception {
		String domain = "@domainValue";
		long timeInMillis = 1234l;
		when(CalendarFactoryUtil.getCalendar()).thenReturn(mockCalendar);
		when(mockCalendar.getTimeInMillis()).thenReturn(timeInMillis);

		String result = emailService.getGeneratedEmailAddress("John *&^^%$-+£!", "Smith *&^ ^%$-+£!", domain);

		assertThat(result, equalTo("John.Smith." + timeInMillis + domain));
	}

	@Test
	public void getGeneratedEmailAddress_WhenNoErrorAndDomainDoesNotContainAt_ThenReturnsTheEmailAddressGeneratedUsingAlphaCharsOnlyAddingAtToTheDomain() throws Exception {
		String domain = "domainValue";
		long timeInMillis = 1234l;
		when(CalendarFactoryUtil.getCalendar()).thenReturn(mockCalendar);
		when(mockCalendar.getTimeInMillis()).thenReturn(timeInMillis);

		String result = emailService.getGeneratedEmailAddress("John *&^^%$-+£!", "Smith *&^ ^%$-+£!", domain);

		assertThat(result, equalTo("John.Smith." + timeInMillis + "@" + domain));
	}

}
