package com.placecube.digitalplace.payment.capitainterface.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class SimpleSaleItemConfiguration implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String, String> simpleSaleItemDetails = new HashMap<>();

	private SimpleSaleItemConfiguration() {
		super();
	}

	public static SimpleSaleItemConfiguration newSimpleSaleItemConfiguration() {
		return new SimpleSaleItemConfiguration();
	}

	public String getSaleItemDetail(String itemKey) {
		return simpleSaleItemDetails.get(itemKey);
	}

	public void putSaleItemDetail(String itemKey, String itemValue) {
		simpleSaleItemDetails.put(itemKey, itemValue);
	}

}
