package com.placecube.digitalplace.payment.constants;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.liferay.petra.string.StringPool;

public class PaymentStatus implements Serializable {

	private static final String ABORTED = "ABORTED";

	private static final String APP_ERROR = "APP_ERROR";

	private static final String CREATED = "CREATED";

	private static final String PAYMENT_ERROR = "PAYMENT_ERROR";

	private static final long serialVersionUID = 1L;

	private static Map<String, PaymentStatus> statuses = new HashMap<>();

	private static final String SUCCESS = "SUCCESS";

	public static PaymentStatus aborted() {
		return fromValue(ABORTED);
	}

	public static PaymentStatus appError() {
		return fromValue(APP_ERROR);
	}

	public static PaymentStatus created() {
		return fromValue(CREATED);
	}

	public static PaymentStatus fromValue(String value) {

		if (!statuses.containsKey(value)) {
			statuses.put(value, new PaymentStatus(value));
		}

		return statuses.get(value);
	}

	public static PaymentStatus paymentError() {
		return fromValue(PAYMENT_ERROR);
	}

	public static PaymentStatus paymentError(String code, String resultMessage) {
		return new PaymentStatus(PAYMENT_ERROR, code, resultMessage);
	}

	public static PaymentStatus success() {
		return fromValue(SUCCESS);
	}

	private String code;

	private String paymentUniqueReferenceNumber;

	private String resultMessage;

	private String value;

	private PaymentStatus(String value) {
		this.value = value;
		code = StringPool.BLANK;
		resultMessage = StringPool.BLANK;
	}

	private PaymentStatus(String value, String code, String resultMessage) {
		this.value = value;
		this.code = code;
		this.resultMessage = resultMessage;
	}

	public String getCode() {
		return code;
	}

	public String getPaymentUniqueReferenceNumber() {
		return paymentUniqueReferenceNumber;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public String getValue() {
		return value;
	}

	public void setPaymentUniqueReferenceNumber(String paymentUniqueReferenceNumber) {
		this.paymentUniqueReferenceNumber = paymentUniqueReferenceNumber;
	}

	@Override
	public String toString() {
		return "PaymentStatus [code=" + code + ", paymentUniqueReferenceNumber=" + paymentUniqueReferenceNumber + ", resultMessage=" + resultMessage + ", value=" + value + "]";
	}
}
