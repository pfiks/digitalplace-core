package com.placecube.digitalplace.externalpage.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import javax.portlet.PortletRequest;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.externalpage.web.constants.ValidationKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SessionErrors.class, XPathFactory.class })
public class ExternalPageImportValidatorTest {

	private ExternalPageImportValidator externalPageImportValidator;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private XPathFactory mockXPathFactory;

	@Mock
	private XPath mockXPath;

	@Mock
	private XPathExpression mockXPathExpression;

	@Before
	public void setUp() {
		externalPageImportValidator = new ExternalPageImportValidator();

		mockStatic(PropsUtil.class, ParamUtil.class, SessionErrors.class, XPathFactory.class);
	}

	@Test
	public void validate_WhenUrlIsEmpty_ThenAddsRequiredUrlSessionError() {
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.URL)).thenReturn(StringPool.BLANK);
		externalPageImportValidator.validate(mockPortletRequest);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, ValidationKeys.REQUIRED_URL);
	}

	@Test
	public void validate_WhenUrlIsNotEmpty_ThenDoesNotAddRequiredUrlSessionError() {
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.URL)).thenReturn("url");
		externalPageImportValidator.validate(mockPortletRequest);

		verifyStatic(SessionErrors.class, never());
		SessionErrors.add(mockPortletRequest, ValidationKeys.REQUIRED_URL);
	}

	@Test
	public void validate_WhenXpathIsEmpty_ThenDoestNotAddInvalidXpathSessionError() {
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.XPATH)).thenReturn(StringPool.BLANK);
		externalPageImportValidator.validate(mockPortletRequest);

		verifyStatic(SessionErrors.class, never());
		SessionErrors.add(mockPortletRequest, ValidationKeys.INVALID_XPATH);
	}

	@Test
	public void validate_WhenXpathIsNotEmpty_AndXpathIsValid_ThenDoestNotAddInvalidXpathSessionError() throws Exception {
		final String xpath = "/xpath";
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.XPATH)).thenReturn(xpath);
		when(XPathFactory.newInstance()).thenReturn(mockXPathFactory);
		when(mockXPathFactory.newXPath()).thenReturn(mockXPath);
		when(mockXPath.compile(xpath)).thenReturn(mockXPathExpression);

		externalPageImportValidator.validate(mockPortletRequest);

		verifyStatic(SessionErrors.class, never());
		SessionErrors.add(mockPortletRequest, ValidationKeys.INVALID_XPATH);
	}

	@Test
	public void validate_WhenXpathIsNotEmpty_AndXpathIsInvalid_ThenAddsInvalidXpathSessionError() throws Exception {
		final String xpath = "/xpath";
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.XPATH)).thenReturn(xpath);
		when(XPathFactory.newInstance()).thenReturn(mockXPathFactory);
		when(mockXPathFactory.newXPath()).thenReturn(mockXPath);
		when(mockXPath.compile(xpath)).thenThrow(new XPathExpressionException("invalid xpath"));

		externalPageImportValidator.validate(mockPortletRequest);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockPortletRequest, ValidationKeys.INVALID_XPATH);
	}

	@Test
	public void validate_WhenUrlIsEmpty_ThenReturnsFalse() {
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.URL)).thenReturn(StringPool.BLANK);

		assertThat(externalPageImportValidator.validate(mockPortletRequest), equalTo(false));
	}

	@Test
	public void validate_WhenUrlIsNotEmpty_AndXpathIsNotEmpty_AndXpathIsInvalid_ThenReturnsFalse() throws Exception {
		final String xpath = "/xpath";
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.XPATH)).thenReturn(xpath);
		when(XPathFactory.newInstance()).thenReturn(mockXPathFactory);
		when(mockXPathFactory.newXPath()).thenReturn(mockXPath);
		when(mockXPath.compile(xpath)).thenThrow(new XPathExpressionException("invalid xpath"));
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.URL)).thenReturn("url");

		assertThat(externalPageImportValidator.validate(mockPortletRequest), equalTo(false));
	}

	@Test
	public void validate_WhenUrlIsNotEmpty_AndXpathIsNotEmpty_AndXpathIsValid_ThenReturnsTrue() throws Exception {
		final String xpath = "/xpath";
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.XPATH)).thenReturn(xpath);
		when(XPathFactory.newInstance()).thenReturn(mockXPathFactory);
		when(mockXPathFactory.newXPath()).thenReturn(mockXPath);
		when(mockXPath.compile(xpath)).thenReturn(mockXPathExpression);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.URL)).thenReturn("url");

		assertThat(externalPageImportValidator.validate(mockPortletRequest), equalTo(true));
	}

	@Test
	public void validate_WhenUrlIsNotEmpty_AndXpathIsEmpty_ThenReturnsTrue() {
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.XPATH)).thenReturn(StringPool.BLANK);
		when(ParamUtil.getString(mockPortletRequest, PortletRequestKeys.URL)).thenReturn("url");

		assertThat(externalPageImportValidator.validate(mockPortletRequest), equalTo(true));

	}
}
