package com.placecube.digitalplace.search.sortby.mostliked.constants;

public final class MostLikedConstants {

	public static final String COUNTER_FIELD_NAME = "dp_mostliked_sortable";

	private MostLikedConstants() {
	}

}
