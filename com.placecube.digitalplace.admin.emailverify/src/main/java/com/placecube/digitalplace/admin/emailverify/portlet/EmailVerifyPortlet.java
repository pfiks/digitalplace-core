package com.placecube.digitalplace.admin.emailverify.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.admin.emailverify.constants.EmailVerifyPortletKeys;

@Component(immediate = true, property = { "javax.portlet.version=3.0", "com.liferay.portlet.css-class-wrapper=portlet-email-verify",
		"com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.instanceable=false", "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,administrator", "com.liferay.portlet.add-default-resource=true", "javax.portlet.name=" + EmailVerifyPortletKeys.ADMIN_EMAIL_VERIFY,
		"portlet.add.default.resource.check.whitelist=" + EmailVerifyPortletKeys.ADMIN_EMAIL_VERIFY }, service = Portlet.class)
public class EmailVerifyPortlet extends MVCPortlet {

}
