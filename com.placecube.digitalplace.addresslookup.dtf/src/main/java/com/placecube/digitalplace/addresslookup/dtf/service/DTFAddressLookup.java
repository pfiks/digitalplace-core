package com.placecube.digitalplace.addresslookup.dtf.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookup;
import com.placecube.digitalplace.addresslookup.dtf.constants.DTFConstants;

@Component(immediate = true, service = AddressLookup.class)
public class DTFAddressLookup implements AddressLookup {

	private static final Log LOG = LogFactoryUtil.getLog(DTFAddressLookup.class);

	@Reference
	private AddressRetrievalService addressRetrievalService;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	public boolean enabled(long companyId) {
		try {
			return addressRetrievalService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Set<AddressContext> findByKeyword(long companyId, String keyword, String[] paramsAndValues) {
		throw new UnsupportedOperationException("Unsupported method");
	}

	@Override
	public Set<AddressContext> findByPostcode(long companyId, String postcode, String[] paramsAndValues) {

		Set<AddressContext> addressContexts = Collections.emptySet();
		Optional<Connection> conn = getConnection(companyId);

		if (conn.isPresent()) {
			Optional<ResultSet> rs = addressRetrievalService.executeQuery(conn.get(), DTFConstants.FIND_BY_POSTCODE_SQL + postcode.replaceAll("\\s", "") + DTFConstants.SUFFIX_SQL);
			if (rs.isPresent()) {
				addressContexts = addressRetrievalService.getAddressContexts(rs.get());
			}
			addressRetrievalService.closeConnection(conn.get());
		}

		return addressContexts;
	}

	@Override
	public Optional<AddressContext> findByUprn(long companyId, String uprn, String[] paramsAndValues) {

		Optional<AddressContext> addressContext = Optional.empty();
		Optional<Connection> conn = getConnection(companyId);

		if (conn.isPresent()) {
			Optional<ResultSet> rs = addressRetrievalService.executeQuery(conn.get(), DTFConstants.FIND_BY_UPRN_SQL + uprn + DTFConstants.SUFFIX_SQL);
			if (rs.isPresent()) {
				addressContext = addressRetrievalService.getAddressContext(rs.get());
			}
			addressRetrievalService.closeConnection(conn.get());
		}

		return addressContext;
	}

	@Override
	public int getWeight(long companyId) {
		try {
			return addressRetrievalService.getWeight(companyId);
		} catch (ConfigurationException e) {
			LOG.error(e);
			return 0;
		}
	}

	@Override
	public boolean national(long companyId) {
		try {
			return addressRetrievalService.isNational(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	private Optional<Connection> getConnection(long companyId) {
		try {
			String driverClass = addressRetrievalService.getDatabaseClass(companyId);
			String url = addressRetrievalService.getDatabaseURL(companyId);
			String user = addressRetrievalService.getDatabaseUser(companyId);
			String password = addressRetrievalService.getDatabasePassword(companyId);
			Connection conn = addressRetrievalService.getConnection(companyId, driverClass, url, user, password);
			return Optional.of(conn);
		} catch (ConfigurationException | ClassNotFoundException | SQLException e) {
			LOG.error(e);
			return Optional.empty();
		}
	}
}
