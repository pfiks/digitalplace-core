<%@include file="init.jsp" %>

<c:set var="ddmStructureId" value="${ddmStructure.getStructureId()}" />

<aui:row>
	<aui:col span="12">
		<h1>
			<liferay-ui:message key="${pageMessageTitle}" />
		</h1>
	</aui:col>
</aui:row>

<portlet:resourceURL id="<%=MVCCommandKeys.VALIDATE_ENTRY %>" var="validateEntryResourceURL" />

<portlet:actionURL name="<%=MVCCommandKeys.UPDATE_ENTRY %>" var="updateEntryActionURL">
	<portlet:param name="ddlRecordId" value="${ddlRecordId}" />
	<portlet:param name="ddmStructureId" value="${ddmStructureId}" />
	<portlet:param name="ddlRecordSetId" value="${ddlRecordSetId}" />
	<portlet:param name="ddmFormTemplateId" value="${ddmFormTemplateId}" />
	<portlet:param name="ddlTemplateId"	value="${ddlTemplateId}" />
</portlet:actionURL>

<aui:form action="${updateEntryActionURL}" method="post" name="ddlUpdateForm" enctype="multipart/form-data" onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "saveEntry();" %>'>
	
    <aui:fieldset cssClass="input-container">
        <div class="inline-alert-container lfr-alert-container"></div>
        
		<liferay-ddm:html
			classNameId="${classNameId}"
			classPK="${classPK}"
			ddmFormValues="${ddmFormValues}"
			defaultEditLocale="${locale}"
			defaultLocale="${locale}"
			groupId="${themeDisplay.getScopeGroupId()}"
			localizable="${enableTranslations}"
			requestedLocale="${locale}"
		/>
       
       <%@include file="update-entry-categorisation.jspf" %>
       
        <aui:button-row>
            <aui:button name="submit" type="submit" />
            <aui:button name="cancel" type="cancel" href="${ redirect }" />
        </aui:button-row>
    </aui:fieldset>
</aui:form>

<c:if test="${not empty ddlAutofillFieldsJSON}">
	<aui:script use="com-placecube-ddl-editor-autofill">
		A.DDLEditorAutofill.init('${ddlAutofillFieldsJSON}', '<portlet:namespace/>');
	</aui:script>
</c:if>

<aui:script>
	function <portlet:namespace />saveEntry() {
		var selectedCategories = [];
		var portletNamespace = '<portlet:namespace />';
		   	
		var vocabularyIds = '${requiredVocabularyIdsJsonArray}';
		$.each(JSON.parse('${requiredVocabularyIdsJsonArray}'), function(key, value){
			var selectedCategoriesForVocab = $('#<portlet:namespace/>assetCategoryIds_'+value).val();
			selectedCategories.push(selectedCategoriesForVocab);
		});
		   	
		$.ajax({
			url: '${validateEntryResourceURL}',
			dataType: "json",
			data: portletNamespace + 'selectedAssetCategoryIds' + '=' + selectedCategories,
			success: function(data) {
				if(data.success === 'true'){
					submitForm(document.<portlet:namespace />ddlUpdateForm);
				} else{
					$('#<portlet:namespace/>requiredCategoriesMessage').removeClass('alert-warning').addClass('alert-danger');
				}
			}
		});
	}
</aui:script>