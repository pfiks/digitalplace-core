package com.placecube.digitalplace.fileupload.service;

import java.io.InputStream;

import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.fileupload.exception.FileUploadException;

public interface FileUploadConnector {

	public String getId();

	public boolean isDefault(long companyId);

	public boolean isEnabled(long companyId);

	public void uploadFile(String name, String path, InputStream file, ServiceContext serviceContext) throws FileUploadException;
}
