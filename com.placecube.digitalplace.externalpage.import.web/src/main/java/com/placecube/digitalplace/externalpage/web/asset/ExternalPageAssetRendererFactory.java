package com.placecube.digitalplace.externalpage.web.asset;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.model.BaseAssetRendererFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET, service = AssetRendererFactory.class)
public class ExternalPageAssetRendererFactory extends BaseAssetRendererFactory<ExternalPage> {

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.externalpage.import.web)")
	private ServletContext servletContext;

	@Override
	public AssetRenderer<ExternalPage> getAssetRenderer(long classPK, int type) throws PortalException {
		ExternalPageAssetRenderer externalPageAssetRenderer = new ExternalPageAssetRenderer(externalPageLocalService.getExternalPage(classPK));
		externalPageAssetRenderer.setAssetRendererType(type);
		externalPageAssetRenderer.setServletContext(servletContext);
		return externalPageAssetRenderer;
	}

	@Override
	public String getClassName() {
		return ExternalPage.class.getName();
	}

	@Override
	public String getPortletId() {
		return PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET;
	}

	@Override
	public String getType() {
		return ExternalPage.class.getName();
	}

	@Override
	public boolean hasPermission(PermissionChecker permissionChecker, long classPK, String actionId) throws Exception {
		return true;
	}

	@Override
	public boolean isActive(long companyId) {
		return true;
	}

	@Override
	public boolean isSearchable() {
		return true;
	}

}
