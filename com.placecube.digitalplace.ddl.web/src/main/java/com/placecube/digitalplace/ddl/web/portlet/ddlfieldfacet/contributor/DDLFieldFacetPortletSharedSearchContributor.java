package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.contributor;

import java.util.List;
import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration.DDLFieldFacetPortletConfigurationConstants;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetParserService;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetRequestService;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.DDL_FIELD_FACET_PORTLET, service = SharedSearchContributor.class)
public class DDLFieldFacetPortletSharedSearchContributor implements SharedSearchContributor {

	@Reference
	private DDLFieldFacetParserService ddlFieldFacetParserService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDLFieldFacetRequestService ddlFieldFacetRequestService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<PortletPreferences> portletPreferences = sharedSearchContributorSettings.getPortletPreferences();
		if (portletPreferences.isPresent()) {
			PortletPreferences prefs = portletPreferences.get();
			long structureId = GetterUtil.getLong(prefs.getValue(DDLFieldFacetPortletConfigurationConstants.STRUCTURE_ID, StringPool.BLANK));
			if (structureId > 0) {
				String fieldName = prefs.getValue(DDLFieldFacetPortletConfigurationConstants.FIELD_NAME, StringPool.BLANK);
				DDMStructure ddmStructure = ddmStructureLocalService.fetchDDMStructure(structureId);

				String facetFieldName = ddlFieldFacetParserService.getIndexedFieldName(ddmStructure, fieldName);
				final String preselectedValue = prefs.getValue(DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, StringPool.BLANK);
				final String filterColour = prefs.getValue(DDLFieldFacetPortletConfigurationConstants.FILTER_COLOUR, StringPool.BLANK);

				addRequestParameterActiveFilter(sharedSearchContributorSettings.getThemeDisplay(), facetFieldName, fieldName, preselectedValue, filterColour);

				addFieldFacet(sharedSearchContributorSettings, facetFieldName);
				addSelectedValuesToQuery(sharedSearchContributorSettings, facetFieldName);

				addPreSelectedValueToQuery(sharedSearchContributorSettings, facetFieldName, preselectedValue);
			}
		} else {
			throw new SystemException("Invalid portlet configuration");
		}

	}

	private void addFieldFacet(SharedSearchContributorSettings sharedSearchContributorSettings, String facetFieldName) {
		FacetConfiguration facetConfiguration = sharedSearchContributorSettings.createFacetConfiguration(facetFieldName);
		facetConfiguration.setOrder("OrderHitsDesc");
		facetConfiguration.setStatic(false);
		facetConfiguration.setWeight(1.3d);
		JSONObject data = facetConfiguration.getData();
		data.put("frequencyThreshold", 1);

		sharedSearchContributorSettings.addFacet(facetFieldName, Optional.of(facetConfiguration));
	}

	private void addPreSelectedValueToQuery(SharedSearchContributorSettings sharedSearchContributorSettings, String facetFieldName, String value) {
		if (!Validator.isBlank(value)) {
			sharedSearchContributorSettings.addBooleanQuery(facetFieldName, value, BooleanClauseOccur.MUST);
		}
	}

	private void addRequestParameterActiveFilter(ThemeDisplay themeDisplay, String facetFieldName, String fieldName, String preselectedValue, String filterColour) {
		String value = ddlFieldFacetRequestService.getConfiguredFieldNameValueFromUrl(themeDisplay, fieldName, preselectedValue);

		if (Validator.isNotNull(value)) {
			sharedSearchRequestService.addSharedActiveFilter(themeDisplay, facetFieldName, value, value, filterColour);
		}
	}

	private void addSelectedValuesToQuery(SharedSearchContributorSettings sharedSearchContributorSettings, String facetFieldName) {
		List<SharedActiveFilter> filters = sharedSearchContributorSettings.getActiveFiltersForField(facetFieldName);

		filters.forEach(f -> sharedSearchContributorSettings.addBooleanQuery(facetFieldName, f.getValue(), BooleanClauseOccur.MUST));
	}
}
