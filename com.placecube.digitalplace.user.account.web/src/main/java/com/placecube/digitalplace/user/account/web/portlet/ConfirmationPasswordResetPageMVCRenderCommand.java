package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT,
		"mvc.command.name=" + MVCCommandKeys.CONFIRMATION_PASSWORD_RESET_PAGE }, service = MVCRenderCommand.class)
public class ConfirmationPasswordResetPageMVCRenderCommand implements MVCRenderCommand {

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {
		return "/confirmation-password-reset-page.jsp";
	}

}
