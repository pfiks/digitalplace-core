package com.placecube.digitalplace.group.rest.application.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetTag;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetTagLocalService;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.digitalplace.group.rest.application.service.DDLContentParsingService;

@Component(immediate = true, service = RestContentEntryBuilder.class)
public class RestContentEntryBuilder {

	private static final Log LOG = LogFactoryUtil.getLog(RestContentEntryBuilder.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private AssetTagLocalService assetTagLocalService;

	@Reference
	private DDLContentParsingService ddlContentParsingService;

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private DynamicDataListService dynamicDataListService;

	public Optional<RestContentEntry> configureFullDetailsInResult(RestContentEntry restContentEntry) {
		try {
			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(restContentEntry.getId());
			configureValuesAndCategoriesAndTags(restContentEntry, ddlRecord);
			return Optional.of(restContentEntry);
		} catch (Exception e) {
			LOG.warn("Unable to get dynamic content: " + e.getMessage());
			LOG.debug(e);
			return Optional.empty();
		}
	}

	public DDLRecord getDDLRecord(Group group, long recordId) throws PortalException {
		DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(recordId);
		if (ddlRecord.getGroupId() == group.getGroupId()) {
			return ddlRecord;
		}
		throw new PortalException("Group API key invalid for DDL Record");
	}

	public RestContentEntry getDynamicContentFullDetails(DDLRecord ddlRecord) throws PortalException {
		RestContentEntry restContentEntry = new RestContentEntry(ddlRecord.getRecordId());

		configureValuesAndCategoriesAndTags(restContentEntry, ddlRecord);

		return restContentEntry;
	}

	public RestContentEntry getRestContentEntryForAssetCategory(AssetCategory assetCategory) {
		return RestContentEntry.init(assetCategory.getCategoryId(), assetCategory.getTitle(assetCategory.getDefaultLanguageId()));
	}

	public RestContentEntry getRestContentEntryForAssetTag(AssetTag assetCategory) {
		return RestContentEntry.init(assetCategory.getTagId(), assetCategory.getName());
	}

	public RestContentEntry getRestContentEntryForAssetVocabulary(AssetVocabulary assetVocabulary) {
		List<RestContentEntry> categories = assetVocabulary.getCategories().stream().map(this::getRestContentEntryForAssetCategory).collect(Collectors.toList());

		return new RestContentEntry(assetVocabulary.getVocabularyId(), assetVocabulary.getTitle(assetVocabulary.getDefaultLanguageId()), categories);
	}

	public RestContentEntry getRestContentEntryForDDLRecordSet(DDLRecordSet recordSet) {
		return RestContentEntry.init(recordSet.getRecordSetId(), recordSet.getName(recordSet.getDefaultLanguageId()));
	}

	public RestContentEntry getRestContentEntryForDDMFormField(DDMFormField ddmFormField) {
		Map<String, LocalizedValue> options = ddmFormField.getDDMFormFieldOptions().getOptions();

		List<RestContentEntryField> restContentEntryFields = new ArrayList<>();

		if (options.isEmpty() && DDMFormFieldTypeConstants.CHECKBOX.equals(ddmFormField.getType())) {
			restContentEntryFields.add(new RestContentEntryField("true", "true"));
			restContentEntryFields.add(new RestContentEntryField("false", "false"));
		} else {
			for (Entry<String, LocalizedValue> option : options.entrySet()) {
				String optionVal = option.getKey();
				LocalizedValue value = option.getValue();
				String optionLabel = value.getString(value.getDefaultLocale());
				restContentEntryFields.add(new RestContentEntryField(optionLabel, optionVal));
			}
		}
		return new RestContentEntry(ddmFormField.getName(), restContentEntryFields);
	}

	public RestContentEntry getRestContentEntryForDDMStructure(DDMStructure structure) {
		return RestContentEntry.init(structure.getStructureId(), structure.getName(structure.getDefaultLanguageId()));
	}

	public Optional<RestContentEntry> getRestContentEntryForDynamicContent(Document searchResult, Integer truncateDescription, String portalURL) {
		try {
			long ddlRecordId = GetterUtil.getLong(searchResult.get(Field.ENTRY_CLASS_PK), -1L);

			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(ddlRecordId);

			String title = GetterUtil.getString(searchResult.get(Field.TITLE), StringPool.BLANK);
			String description = getDescription(searchResult, truncateDescription);

			String viewURL = getViewURL(portalURL, ddlRecord);

			return Optional.of(RestContentEntry.init(ddlRecordId, title, description, viewURL));
		} catch (Exception e) {
			LOG.warn("Unable to get dynamic content: " + e.getMessage());
			LOG.debug(e);
			return Optional.empty();
		}
	}

	public RestContentUser getRestContentUser(Group group, User user, String email) {
		if (Validator.isNotNull(user)) {
			return new RestContentUser(user, group);
		}
		return new RestContentUser(email);
	}

	public Response getSuccessResponse(RestContentEntry result) {
		return Response.ok(result).build();
	}

	public Response getSuccessResponse(RestContentUser result) {
		return Response.ok(result).build();
	}

	public Response getSuccessResponse(RestListResult result) {
		return Response.ok(result).build();
	}

	private void configureValuesAndCategoriesAndTags(RestContentEntry restContentEntry, DDLRecord ddlRecord) throws PortalException {
		long ddlRecordId = restContentEntry.getId();

		List<RestContentEntryField> values = new ArrayList<>();

		List<DDMFormFieldValue> ddmFormFieldValues = ddlRecord.getDDMFormValues().getDDMFormFieldValues();

		for (DDMFormFieldValue ddmFormFieldValue : ddmFormFieldValues) {
			DDMFormField ddmFormField = ddmFormFieldValue.getDDMFormField();
			if (Validator.isNotNull(ddmFormField)) {

				String fieldName = ddlContentParsingService.getFieldLabel(ddmFormField);

				RestContentEntryField restContentEntryField = ddlContentParsingService.getRestContentEntryField(ddmFormField, fieldName, ddmFormFieldValue);
				values.add(restContentEntryField);
			}
		}

		String className = DDLRecord.class.getName();

		List<RestContentEntry> categories = assetCategoryLocalService.getCategories(className, ddlRecordId).stream().map(this::getRestContentEntryForAssetCategory).collect(Collectors.toList());

		List<RestContentEntry> tags = assetTagLocalService.getTags(className, ddlRecordId).stream().map(this::getRestContentEntryForAssetTag).collect(Collectors.toList());

		restContentEntry.setValues(values);
		restContentEntry.setCategories(categories);
		restContentEntry.setTags(tags);
	}

	private String getDescription(Document searchResult, Integer truncateDescription) {
		String fullDescription = HtmlUtil.stripHtml(GetterUtil.getString(searchResult.get(Field.CONTENT), StringPool.BLANK)).trim();
		return Validator.isNotNull(truncateDescription) && fullDescription.length() > truncateDescription ? fullDescription.substring(0, truncateDescription).concat(StringPool.DOUBLE_PERIOD)
				: fullDescription;
	}

	private String getViewURL(String portalURL, DDLRecord ddlRecord) {
		try {
			return dynamicDataListService.getViewDDLRecordURL(portalURL, ddlRecord, true, 0L).orElse(StringPool.BLANK);
		} catch (Exception e) {
			// This is a workaround as the first call to the method fails as
			// there's no authenticated user
			return dynamicDataListService.getViewDDLRecordURL(portalURL, ddlRecord, true, 0L).orElse(StringPool.BLANK);
		}
	}

}
