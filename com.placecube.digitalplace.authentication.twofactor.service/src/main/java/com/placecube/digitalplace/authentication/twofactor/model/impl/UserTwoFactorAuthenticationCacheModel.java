/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.authentication.twofactor.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.persistence.UserTwoFactorAuthenticationPK;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserTwoFactorAuthentication in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class UserTwoFactorAuthenticationCacheModel
	implements CacheModel<UserTwoFactorAuthentication>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof UserTwoFactorAuthenticationCacheModel)) {
			return false;
		}

		UserTwoFactorAuthenticationCacheModel
			userTwoFactorAuthenticationCacheModel =
				(UserTwoFactorAuthenticationCacheModel)object;

		if (userTwoFactorAuthenticationPK.equals(
				userTwoFactorAuthenticationCacheModel.
					userTwoFactorAuthenticationPK)) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, userTwoFactorAuthenticationPK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", secretKey=");
		sb.append(secretKey);
		sb.append(", resetKey=");
		sb.append(resetKey);
		sb.append(", enabled=");
		sb.append(enabled);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserTwoFactorAuthentication toEntityModel() {
		UserTwoFactorAuthenticationImpl userTwoFactorAuthenticationImpl =
			new UserTwoFactorAuthenticationImpl();

		if (uuid == null) {
			userTwoFactorAuthenticationImpl.setUuid("");
		}
		else {
			userTwoFactorAuthenticationImpl.setUuid(uuid);
		}

		userTwoFactorAuthenticationImpl.setCompanyId(companyId);
		userTwoFactorAuthenticationImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			userTwoFactorAuthenticationImpl.setCreateDate(null);
		}
		else {
			userTwoFactorAuthenticationImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			userTwoFactorAuthenticationImpl.setModifiedDate(null);
		}
		else {
			userTwoFactorAuthenticationImpl.setModifiedDate(
				new Date(modifiedDate));
		}

		if (secretKey == null) {
			userTwoFactorAuthenticationImpl.setSecretKey("");
		}
		else {
			userTwoFactorAuthenticationImpl.setSecretKey(secretKey);
		}

		userTwoFactorAuthenticationImpl.setResetKey(resetKey);
		userTwoFactorAuthenticationImpl.setEnabled(enabled);

		userTwoFactorAuthenticationImpl.resetOriginalValues();

		return userTwoFactorAuthenticationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		secretKey = objectInput.readUTF();

		resetKey = objectInput.readBoolean();

		enabled = objectInput.readBoolean();

		userTwoFactorAuthenticationPK = new UserTwoFactorAuthenticationPK(
			companyId, userId);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (secretKey == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(secretKey);
		}

		objectOutput.writeBoolean(resetKey);

		objectOutput.writeBoolean(enabled);
	}

	public String uuid;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public String secretKey;
	public boolean resetKey;
	public boolean enabled;
	public transient UserTwoFactorAuthenticationPK
		userTwoFactorAuthenticationPK;

}