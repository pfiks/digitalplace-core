package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.user.account.web.util.AccountPermissionUtils;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.EDIT_ACCOUNT, "mvc.command.name=/",
		"mvc.command.name=" + MVCCommandKeys.EDIT_DETAILS }, service = MVCRenderCommand.class)
public class EditDetailsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private UserAccountFieldRegistry userAccountFieldRegistry;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		if (!AccountPermissionUtils.hasEditPermission(renderRequest)) {

			SessionErrors.add(renderRequest, "no-permissions");
			return "/no-access.jsp";

		}
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			AccountContext accountContext;
			UserProfileFieldsConfiguration userFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(themeDisplay.getCompanyId());

			if (ParamUtil.getBoolean(renderRequest, RequestKeys.USE_SESSION, true)) {
				accountContext = accountContextService.getOrCreateContext(renderRequest);
			} else {
				accountContext = accountContextService.getContextFromUserDetails(renderRequest, userFieldsConfiguration);
			}

			if (Validator.isNull(accountContext.getAccountConnectorModelId())) {
				accountContext.setAccountConnectorModelId(ParamUtil.getString(renderRequest, AccountConnectorConstant.MODEL_ID));
			}

			CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, renderRequest);

			renderRequest.setAttribute(RequestKeys.ACCOUNT_CONTEXT, accountContext);
			renderRequest.setAttribute(RequestKeys.SHOW_EMAIL_ADDRESS_IN_PERSONAL_DETAILS, true);
			renderRequest.setAttribute(RequestKeys.ADDITIONAL_DETAILS_EXPANDO_FIELDS, accountConfigurationService.getConfiguredExpandoFields(accountContext, userFieldsConfiguration));
			renderRequest.setAttribute(RequestKeys.ACCOUNT_FIELDS, userAccountFieldRegistry.getUserAccountFields(accountContext.getCompanyId()));
			renderRequest.setAttribute(RequestKeys.REDIRECT_URL, ParamUtil.getString(renderRequest, RequestKeys.REDIRECT_URL));
			renderRequest.setAttribute(RequestKeys.USER_PROFILE_CONFIG, userFieldsConfiguration);
			renderRequest.setAttribute(RequestKeys.ACCOUNT_CONFIG, accountConfiguration);
			renderRequest.setAttribute(RequestKeys.IS_ADDRESS_OUT_OF_LOCAL_AREA_ENABLED, accountConfiguration.addressOutOfLocalArea());
			accountContextService.updateContextInSession(accountContext, renderRequest);
		} catch (UserAccountException e) {
			throw new PortletException(e);
		}
		return "/edit-details.jsp";
	}
}
