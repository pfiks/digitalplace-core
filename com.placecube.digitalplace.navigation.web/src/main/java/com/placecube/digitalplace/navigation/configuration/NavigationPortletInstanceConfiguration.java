package com.placecube.digitalplace.navigation.configuration;

import com.placecube.digitalplace.navigation.constants.ConfigurationConstants;

import aQute.bnd.annotation.metatype.Meta;
import aQute.bnd.annotation.metatype.Meta.Type;

@Meta.OCD(id = ConfigurationConstants.NAVIGATION_PID)
public interface NavigationPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "0", type = Type.Long)
	long navigationMenuGroupId();

	@Meta.AD(required = false, deflt = "0", type = Type.Long)
	long navigationMenuId();

	@Meta.AD(required = false, deflt = "1", type = Type.Integer)
	int navigationType();

	@Meta.AD(required = false, deflt = "0", type = Type.Long)
	long widgetTemplateGroupId();

	@Meta.AD(required = false, deflt = "", type = Type.String)
	String widgetTemplateKey();

}
