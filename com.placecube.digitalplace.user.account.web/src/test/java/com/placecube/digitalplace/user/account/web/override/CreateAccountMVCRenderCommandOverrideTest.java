package com.placecube.digitalplace.user.account.web.override;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

public class CreateAccountMVCRenderCommandOverrideTest extends PowerMockito {

	private static final Long COMPANY_ID = 22L;
	private static final String CREATE_ACCOUNT_URL = "createAccountConfiguredURLValue";
	private static final String DEFAULT_RENDER_COMMAND_RESULT = "expectedValue";
	private static final String GROUP_URL = "GROUP_URL";
	private static final Long GUEST_GROUP_ID = 11L;
	private static final String LAYOUT_URL = "LAYOUT_URL";
	private static final long SCOPE_GROUP_ID = 33;

	@InjectMocks
	private CreateAccountMVCRenderCommandOverride createAccountMVCRenderCommandOverride;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Group mockGuestGroup;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private MVCRenderCommand mockMVCRenderCommand;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private Group mockScopeGroup;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetUp() {
		initMocks(this);
		createAccountMVCRenderCommandOverride.setMvcActionCommand(mockMVCRenderCommand);
	}

	@Test
	public void render_WhenExceptionRetrievingTheConfiguration_ThenReturnsTheDefaultRenderCommandAndNoRedirectIsPerformed() throws Exception {
		mockBasicDetails();
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenThrow(new UserAccountException());
		when(mockMVCRenderCommand.render(mockRenderRequest, mockRenderResponse)).thenReturn(DEFAULT_RENDER_COMMAND_RESULT);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(DEFAULT_RENDER_COMMAND_RESULT));
		verify(mockPortal, never()).getHttpServletRequest(mockRenderRequest);
	}

	@Test
	public void render_WhenScopeGroupDoesNotHaveLayoutAndExceptionRetrievingGuestGroup_ThenReturnsTheDefaultRenderCommandAndNoRedirectIsPerformed() throws Exception {
		mockBasicDetails();
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(SCOPE_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(null);
		when(mockGroupLocalService.getGroup(COMPANY_ID, GroupConstants.GUEST)).thenThrow(new PortalException());
		when(mockMVCRenderCommand.render(mockRenderRequest, mockRenderResponse)).thenReturn(DEFAULT_RENDER_COMMAND_RESULT);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(DEFAULT_RENDER_COMMAND_RESULT));
		verify(mockPortal, never()).getHttpServletResponse(mockRenderResponse);
	}

	@Test
	public void render_WhenScopeGroupDoesNotHaveLayoutAndExceptionRetrievingTheGuestGroupLayout_ThenReturnsTheDefaultRenderCommandAndNoRedirectIsPerformed() throws Exception {
		mockBasicDetails();
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(SCOPE_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(null);
		mockGuestGroupRetrieval();
		when(mockLayoutLocalService.getFriendlyURLLayout(GUEST_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenThrow(new PortalException());
		when(mockMVCRenderCommand.render(mockRenderRequest, mockRenderResponse)).thenReturn(DEFAULT_RENDER_COMMAND_RESULT);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(DEFAULT_RENDER_COMMAND_RESULT));
		verify(mockPortal, never()).getHttpServletResponse(mockRenderResponse);
	}

	@Test
	public void render_WhenScopeGroupDoesNotHaveLayoutAndIsGuestAndExceptionSendingRedirectToTheGuestGroupLayout_ThenReturnsTheDefaultRenderCommand() throws Exception {
		mockBasicDetails();
		when(mockScopeGroup.isGuest()).thenReturn(true);
		mockGuestGroupRetrieval();
		when(mockLayoutLocalService.getFriendlyURLLayout(GUEST_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(mockLayout);
		mockURLs(mockGuestGroup);
		doThrow(new IOException()).when(mockHttpServletResponse).sendRedirect(GROUP_URL + LAYOUT_URL);
		when(mockMVCRenderCommand.render(mockRenderRequest, mockRenderResponse)).thenReturn(DEFAULT_RENDER_COMMAND_RESULT);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(DEFAULT_RENDER_COMMAND_RESULT));
	}

	@Test
	public void render_WhenScopeGroupDoesNotHaveLayoutAndIsGuestAndNoErrorWithTheGuestGroupDetails_ThenSendsTheRedirectToTheGuestGroupLayout() throws Exception {
		mockBasicDetails();
		mockGuestGroupRetrieval();
		when(mockScopeGroup.isGuest()).thenReturn(true);
		when(mockLayoutLocalService.getFriendlyURLLayout(GUEST_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(mockLayout);
		mockURLs(mockGuestGroup);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(StringPool.BLANK));
		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockRenderRequest);
		verify(mockHttpServletResponse, times(1)).sendRedirect(GROUP_URL + LAYOUT_URL);
	}

	@Test
	public void render_WhenScopeGroupDoesNotHaveLayoutAndIsNotGuestAndExceptionSendingRedirectToTheGuestGroupLayout_ThenReturnsTheDefaultRenderCommand() throws Exception {
		mockBasicDetails();
		when(mockScopeGroup.isGuest()).thenReturn(false);
		mockGuestGroupRetrieval();
		when(mockLayoutLocalService.getFriendlyURLLayout(GUEST_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(mockLayout);
		mockURLs(mockGuestGroup);
		doThrow(new IOException()).when(mockHttpServletResponse).sendRedirect(GROUP_URL + LAYOUT_URL + "?" + RequestKeys.REFERER + "=" + SCOPE_GROUP_ID);
		when(mockMVCRenderCommand.render(mockRenderRequest, mockRenderResponse)).thenReturn(DEFAULT_RENDER_COMMAND_RESULT);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(DEFAULT_RENDER_COMMAND_RESULT));
	}

	@Test
	public void render_WhenScopeGroupDoesNotHaveLayoutAndIsNotGuestAndNoErrorWithTheGuestGroupDetails_ThenSendsTheRedirectToTheGuestGroupLayout() throws Exception {
		mockBasicDetails();
		mockGuestGroupRetrieval();
		when(mockScopeGroup.isGuest()).thenReturn(false);
		when(mockLayoutLocalService.getFriendlyURLLayout(GUEST_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(mockLayout);
		mockURLs(mockGuestGroup);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(StringPool.BLANK));
		verify(mockHttpServletResponse, times(1)).sendRedirect(GROUP_URL + LAYOUT_URL + "?" + RequestKeys.REFERER + "=" + SCOPE_GROUP_ID);
	}

	@Test
	public void render_WhenScopeGroupHasLayoutAndIsGuest_ThenSendsTheRedirectToTheScopeGroupLayout() throws Exception {
		mockBasicDetails();
		when(mockScopeGroup.isGuest()).thenReturn(true);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(SCOPE_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(mockLayout);
		mockURLs(mockScopeGroup);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(StringPool.BLANK));
		verify(mockHttpServletResponse, times(1)).sendRedirect(GROUP_URL + LAYOUT_URL);
	}

	@Test
	public void render_WhenScopeGroupHasLayoutAndIsGuestAndExceptionSendingRedirectToTheGroupLayout_ThenReturnsTheDefaultRenderCommand() throws Exception {
		mockBasicDetails();
		when(mockScopeGroup.isGuest()).thenReturn(true);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(SCOPE_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(mockLayout);
		mockURLs(mockScopeGroup);
		doThrow(new IOException()).when(mockHttpServletResponse).sendRedirect(GROUP_URL + LAYOUT_URL);
		when(mockMVCRenderCommand.render(mockRenderRequest, mockRenderResponse)).thenReturn(DEFAULT_RENDER_COMMAND_RESULT);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(DEFAULT_RENDER_COMMAND_RESULT));
	}

	@Test
	public void render_WhenScopeGroupHasLayoutAndIsNotGuest_ThenSendsTheRedirectToTheScopeGroupLayout() throws Exception {
		mockBasicDetails();
		when(mockScopeGroup.isGuest()).thenReturn(false);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(SCOPE_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(mockLayout);
		mockURLs(mockScopeGroup);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(StringPool.BLANK));
		verify(mockHttpServletResponse, times(1)).sendRedirect(GROUP_URL + LAYOUT_URL + "?" + RequestKeys.REFERER + "=" + SCOPE_GROUP_ID);
	}

	@Test
	public void render_WhenScopeGroupHasLayoutAndIsNotGuestAndExceptionSendingRedirectToTheGroupLayout_ThenReturnsTheDefaultRenderCommand() throws Exception {
		mockBasicDetails();
		when(mockScopeGroup.isGuest()).thenReturn(false);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(SCOPE_GROUP_ID, false, CREATE_ACCOUNT_URL)).thenReturn(mockLayout);
		mockURLs(mockScopeGroup);
		doThrow(new IOException()).when(mockHttpServletResponse).sendRedirect(GROUP_URL + LAYOUT_URL + "?" + RequestKeys.REFERER + "=" + SCOPE_GROUP_ID);
		when(mockMVCRenderCommand.render(mockRenderRequest, mockRenderResponse)).thenReturn(DEFAULT_RENDER_COMMAND_RESULT);

		String result = createAccountMVCRenderCommandOverride.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(DEFAULT_RENDER_COMMAND_RESULT));
	}

	private void mockBasicDetails() throws UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockScopeGroup);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockScopeGroup.getGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.createAccountURL()).thenReturn(CREATE_ACCOUNT_URL);
	}

	private void mockGuestGroupRetrieval() throws PortalException {
		when(mockGroupLocalService.getGroup(COMPANY_ID, GroupConstants.GUEST)).thenReturn(mockGuestGroup);
		when(mockGuestGroup.getGroupId()).thenReturn(GUEST_GROUP_ID);
	}

	private void mockURLs(Group group) {
		when(group.getDisplayURL(mockThemeDisplay)).thenReturn(GROUP_URL);
		when(mockLayout.getFriendlyURL()).thenReturn(LAYOUT_URL);
		when(mockPortal.getHttpServletResponse(mockRenderResponse)).thenReturn(mockHttpServletResponse);
	}

}
