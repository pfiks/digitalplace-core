package com.placecube.digitalplace.common.email;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = EmailService.class)
public class EmailService {

	private static final String NON_ALPHA_REGEX_PATTERN = "[^a-zA-Z0-9]";

	/**
	 * Returns an auto-generated email in the format
	 * firstName.lastName.currentTimestamp@emailDomain
	 * 
	 * @param firstName the firstname
	 * @param lastName the lastname
	 * @param emailDomain the domain to use
	 * @return generated email
	 */
	public String getGeneratedEmailAddress(String firstName, String lastName, String emailDomain) {
		String domainPrefix = emailDomain.contains(StringPool.AT) ? StringPool.BLANK : StringPool.AT;
		return stripNonAlphaChars(firstName) + StringPool.PERIOD + stripNonAlphaChars(lastName) + StringPool.PERIOD + CalendarFactoryUtil.getCalendar().getTimeInMillis() + domainPrefix + emailDomain;

	}

	private String stripNonAlphaChars(String sourceValue) {
		if (Validator.isNotNull(sourceValue)) {
			return sourceValue.replaceAll(NON_ALPHA_REGEX_PATTERN, StringPool.BLANK);
		}

		return sourceValue;
	}

}
