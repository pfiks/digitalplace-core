package com.placecube.digitalplace.common.portlet;

import java.net.URI;
import java.net.URISyntaxException;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = PortletURLService.class)
public class PortletURLService {

	private static final Log LOG = LogFactoryUtil.getLog(PortletURLService.class);

	@Reference
	private Portal portal;

	public boolean isEditLayoutMode(PortletRequest portletRequest) {
		HttpServletRequest originalHttpServletRequest = portal.getOriginalServletRequest(portal.getHttpServletRequest(portletRequest));

		String layoutMode = ParamUtil.getString(originalHttpServletRequest, "p_l_mode", Constants.VIEW);

		return Constants.EDIT.equals(layoutMode);
	}

	public String getURLWithRedirectParameter(String viewURL, String currentURL) {
		if (Validator.isNotNull(currentURL)) {
			try {
				URI uri = new URI(viewURL);
				StringBuilder result = new StringBuilder();
				result.append(viewURL);
				if (Validator.isNotNull(uri.getQuery())) {
					result.append(StringPool.AMPERSAND);
				} else {
					result.append(StringPool.QUESTION);
				}
				result.append("redirect=");
				result.append(PortalUtil.escapeRedirect(currentURL));
				return result.toString();
			} catch (URISyntaxException e) {
				LOG.debug("Cannot construct URI from view URL - " + e.getMessage());
				return viewURL;
			}

		} else {
			return viewURL;
		}
	}
}
