package com.placecube.digitalplace.search.web.portlet.contenttype.constants;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.liferay.blogs.model.BlogsEntry;
import com.liferay.calendar.model.CalendarBooking;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.knowledge.base.model.KBArticle;
import com.liferay.message.boards.model.MBMessage;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.wiki.model.WikiPage;

public class ContentTypeConstantsTest {

	@Test
	public void getMessageKeyForClassName_WhenClassNameIsBlogsEntry_ThenReturnsTheContentTypeLabel() {
		String result = ContentTypeConstants.getMessageKeyForClassName(BlogsEntry.class.getName());

		assertThat(result, equalTo("content-type-blogs"));
	}

	@Test
	public void getMessageKeyForClassName_WhenClassNameIsCalendarBooking_ThenReturnsTheContentTypeLabel() {
		String result = ContentTypeConstants.getMessageKeyForClassName(CalendarBooking.class.getName());

		assertThat(result, equalTo("content-type-events"));
	}

	@Test
	public void getMessageKeyForClassName_WhenClassNameIsDLFileEntry_ThenReturnsTheContentTypeLabel() {
		String result = ContentTypeConstants.getMessageKeyForClassName(DLFileEntry.class.getName());

		assertThat(result, equalTo("content-type-documents"));
	}

	@Test
	public void getMessageKeyForClassName_WhenClassNameIsGroup_ThenReturnsTheContentTypeLabel() {
		String result = ContentTypeConstants.getMessageKeyForClassName(Group.class.getName());

		assertThat(result, equalTo("content-type-groups"));
	}

	@Test
	public void getMessageKeyForClassName_WhenClassNameIsKBArticle_ThenReturnsTheContentTypeLabel() {
		String result = ContentTypeConstants.getMessageKeyForClassName(KBArticle.class.getName());

		assertThat(result, equalTo("content-type-kb-articles"));
	}

	@Test
	public void getMessageKeyForClassName_WhenClassNameIsMBMessage_ThenReturnsTheContentTypeLabel() {
		String result = ContentTypeConstants.getMessageKeyForClassName(MBMessage.class.getName());

		assertThat(result, equalTo("content-type-discussions"));
	}

	@Test
	public void getMessageKeyForClassName_WhenClassNameIsNotManaged_ThenReturnsTheObjectName() {
		String result = ContentTypeConstants.getMessageKeyForClassName("com.something.else.ObjectName");

		assertThat(result, equalTo("ObjectName"));
	}

	@Test
	public void getMessageKeyForClassName_WhenClassNameIsUser_ThenReturnsTheContentTypeLabel() {
		String result = ContentTypeConstants.getMessageKeyForClassName(User.class.getName());

		assertThat(result, equalTo("content-type-users"));
	}

	@Test
	public void getMessageKeyForClassName_WhenClassNameIsWikiPage_ThenReturnsTheContentTypeLabel() {
		String result = ContentTypeConstants.getMessageKeyForClassName(WikiPage.class.getName());

		assertThat(result, equalTo("content-type-wiki-pages"));
	}

}
