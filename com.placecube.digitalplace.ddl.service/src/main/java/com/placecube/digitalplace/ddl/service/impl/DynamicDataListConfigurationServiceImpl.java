package com.placecube.digitalplace.ddl.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.HashMapDictionary;
import com.placecube.digitalplace.ddl.internal.configuration.DynamicDataListCompanyConfiguration;
import com.placecube.digitalplace.ddl.internal.configuration.tracker.DynamicDataListCompanyConfigurationTracker;
import com.placecube.digitalplace.ddl.internal.util.ConfigurationParsingUtil;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;

@Component(immediate = true, service = DynamicDataListConfigurationService.class)
public class DynamicDataListConfigurationServiceImpl implements DynamicDataListConfigurationService {

	private static final Log LOG = LogFactoryUtil.getLog(DynamicDataListConfigurationServiceImpl.class);

	@Reference
	private ConfigurationParsingUtil configurationParsingUtil;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DynamicDataListCompanyConfigurationTracker dynamicDataListCompanyConfigurationTracker;

	@Reference
	private GroupLocalService groupLocalService;

	@Override
	public void addDynamicDataListMappingConfiguration(DDMStructure ddmStructure, DDMTemplate fullViewTemplate, DDMTemplate listViewTemplate, DDMTemplate gridViewTemplate, String titleField,
			String contentField, String iconName) throws ConfigurationException {

		if (ddmStructure.getClassName().equalsIgnoreCase(DDLRecordSet.class.getName())) {
			if (!getDynamicDataListMapping(ddmStructure).isPresent()) {
				long companyId = ddmStructure.getCompanyId();
				DynamicDataListCompanyConfiguration configuration = dynamicDataListCompanyConfigurationTracker.getDynamicDataListCompanyConfiguration(companyId);
				String[] dynamicDataListMappings = configuration.dynamicDataListMappings();
				if (null == dynamicDataListMappings) {
					dynamicDataListMappings = new String[0];
				}

				String mapping = configurationParsingUtil.getMappingJsonString(ddmStructure, fullViewTemplate, listViewTemplate, gridViewTemplate, titleField, contentField, iconName);
				dynamicDataListMappings = ArrayUtil.append(dynamicDataListMappings, mapping);

				HashMapDictionary<String, Object> properties = new HashMapDictionary<>();
				properties.put("dynamicDataListMappings", dynamicDataListMappings);
				configurationProvider.saveCompanyConfiguration(DynamicDataListCompanyConfiguration.class, companyId, properties);
			} else {
				LOG.debug("Skipping structure configuration as it is already configured - " + ddmStructure.getStructureId());
			}
		} else {
			throw new ConfigurationException("Cannot configure structure as it is not a DDLRecordSet - ddmStructureId: " + ddmStructure.getStructureId());
		}
	}

	@Override
	public Optional<DynamicDataListMapping> getDynamicDataListMapping(DDMStructure ddmStructure) {
		List<DynamicDataListMapping> mappings = dynamicDataListCompanyConfigurationTracker.getConfiguredDynamicDataListMappingsForCompany(ddmStructure.getCompanyId());

		return mappings.stream().filter(mapping -> mapping.getDDMStructureId() == ddmStructure.getStructureId()).findFirst();
	}

	@Override
	public List<DynamicDataListMapping> getDynamicDataListMappings(Group group) throws PortalException {
		long globalGroupId = groupLocalService.getCompanyGroup(group.getCompanyId()).getGroupId();
		long groupId = group.getGroupId();
		return getDynamicDataListMappings(group.getCompanyId()).stream().filter(entry -> entry.getDDMStructure().getGroupId() == groupId || entry.getDDMStructure().getGroupId() == globalGroupId)
				.collect(Collectors.toList());
	}

	@Override
	public List<DynamicDataListMapping> getDynamicDataListMappings(long companyId) {
		return dynamicDataListCompanyConfigurationTracker.getConfiguredDynamicDataListMappingsForCompany(companyId);
	}

}
