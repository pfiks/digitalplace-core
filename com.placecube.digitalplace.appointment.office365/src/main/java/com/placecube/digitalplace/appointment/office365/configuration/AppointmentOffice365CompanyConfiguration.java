package com.placecube.digitalplace.appointment.office365.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.appointment.office365.configuration.AppointmentOffice365CompanyConfiguration", localization = "content/Language", name = "appointment-office365")
public interface AppointmentOffice365CompanyConfiguration {

	@Meta.AD(required = false, name = "application-id")
	String applicationId();

	@Meta.AD(required = false, name = "application-secret")
	String applicationSecret();

	@Meta.AD(required = false, name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, name = "serviceid-user-calendars", description = "serviceid-user-calendars-description")
	String[] serviceIdUserCalendars();

	@Meta.AD(required = false, name = "tenant-id")
	String tenantId();

}
