package com.placecube.digitalplace.announcementsbanner.web.portlet;

import static org.mockito.Mockito.times;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.announcementsbanner.web.configuration.AnnouncementsBannerGroupConfiguration;

@RunWith(PowerMockRunner.class)
public class GetAnnouncementConfigurationMVCResourceCommandTest extends PowerMockito {

	private final long GROUP_ID = 1L;

	private final String HTML_SELECTOR = "HTMLSelector";

	@Mock
	private AnnouncementsBannerGroupConfiguration mockAnnouncementsBannerGroupConfiguration;

	@Mock
	private ConfigurationProvider configurationProvider;

	@InjectMocks
	private GetAnnouncementConfigurationMVCResourceCommand getAnnouncementConfigurationMVCResourceCommand;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private Portal mockPortal;

	@Mock
	private PrintWriter mockPrintWriter;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Test(expected = ConfigurationException.class)
	public void doServeResource_WhenErrorGettingGroupConfiguration_ThenThrowConfigurationException() throws Exception {
		when(mockPortal.getScopeGroupId(mockResourceRequest)).thenReturn(GROUP_ID);
		when(configurationProvider.getGroupConfiguration(AnnouncementsBannerGroupConfiguration.class, GROUP_ID)).thenThrow(new ConfigurationException());

		getAnnouncementConfigurationMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test(expected = IOException.class)
	public void doServeResource_WhenErrorGettingWriter_ThenThrowIOException() throws Exception {
		when(mockPortal.getScopeGroupId(mockResourceRequest)).thenReturn(GROUP_ID);
		when(configurationProvider.getGroupConfiguration(AnnouncementsBannerGroupConfiguration.class, GROUP_ID)).thenReturn(mockAnnouncementsBannerGroupConfiguration);

		when(mockAnnouncementsBannerGroupConfiguration.getAnnouncementsBannerPortletHTMLSelector()).thenReturn(HTML_SELECTOR);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockResourceResponse.getWriter()).thenThrow(new IOException());

		getAnnouncementConfigurationMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test
	public void doServeResource_WhenNoError_ThenReturnHTMLSelector() throws Exception {
		when(mockPortal.getScopeGroupId(mockResourceRequest)).thenReturn(GROUP_ID);
		when(configurationProvider.getGroupConfiguration(AnnouncementsBannerGroupConfiguration.class, GROUP_ID)).thenReturn(mockAnnouncementsBannerGroupConfiguration);

		when(mockAnnouncementsBannerGroupConfiguration.getAnnouncementsBannerPortletHTMLSelector()).thenReturn(HTML_SELECTOR);

		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockResourceResponse.getWriter()).thenReturn(mockPrintWriter);

		getAnnouncementConfigurationMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		InOrder inOrder = Mockito.inOrder(mockJsonObject, mockPrintWriter);
		inOrder.verify(mockJsonObject, times(1)).put("announcementsBannerPortletHTMLSelector", HTML_SELECTOR);
		inOrder.verify(mockPrintWriter, times(1)).print(mockJsonObject);
	}

}
