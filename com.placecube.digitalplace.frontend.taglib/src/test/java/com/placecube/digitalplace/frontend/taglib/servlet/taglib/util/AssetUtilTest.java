package com.placecube.digitalplace.frontend.taglib.servlet.taglib.util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AssetVocabularyLocalServiceUtil.class, GroupLocalServiceUtil.class, PortalUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil" , "com.liferay.portal.kernel.util.Validator",
		"com.liferay.portal.kernel.service.GroupLocalServiceUtil"})
public class AssetUtilTest extends PowerMockito {

	private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
	private static final long GROUP_ID_1 = 123;
	private static final long GROUP_ID_2 = 456;
	private static final long NON_EXISTING_VOCABULARY_ID = 789;
	private static final long VOCABULARY_ID_1 = 741;
	private static final long VOCABULARY_ID_2 = 852;

	@Mock
	private AssetVocabulary mockAssetVocabulary1;

	@Mock
	private AssetVocabulary mockAssetVocabulary2;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Group mockGroup;


	@Before
	public void activateSetup() {
		mockStatic(AssetVocabularyLocalServiceUtil.class, GroupLocalServiceUtil.class, PortalUtil.class);
	}

	@Test
	public void getNonEmptyVocabulariesByIds_WhenVocabularyIdsSizeMoreThan0AndAllVocabulariesExistAndHaveCategories_ThenReturnListOfAllAssetVocabularies() {
		long[] vocabularyIds = new long[]{VOCABULARY_ID_1, VOCABULARY_ID_2};

		when(AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(VOCABULARY_ID_1)).thenReturn(mockAssetVocabulary1);
		when(AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(VOCABULARY_ID_2)).thenReturn(mockAssetVocabulary2);
		when(mockAssetVocabulary1.getCategoriesCount()).thenReturn(3);
		when(mockAssetVocabulary2.getCategoriesCount()).thenReturn(3);

		List<AssetVocabulary> assetVocabularies = AssetsUtil.getNonEmptyVocabulariesByIds(vocabularyIds);

		assertThat(assetVocabularies.size(), equalTo(2));
		assertThat(assetVocabularies.get(0), equalTo(mockAssetVocabulary1));
		assertThat(assetVocabularies.get(1), equalTo(mockAssetVocabulary2));
	}

	@Test
	public void getNonEmptyVocabulariesByIds_WhenVocabularyIdsSizeMoreThan0AllVocabulariesExistButNotAllHaveCategories_ThenReturnListOfAssetVocabulariesWithCategoriesOnly() {
		long[] vocabularyIds = new long[]{VOCABULARY_ID_1, VOCABULARY_ID_2};

		when(AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(VOCABULARY_ID_1)).thenReturn(mockAssetVocabulary1);
		when(AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(VOCABULARY_ID_2)).thenReturn(mockAssetVocabulary2);
		when(mockAssetVocabulary1.getCategoriesCount()).thenReturn(3);
		when(mockAssetVocabulary2.getCategoriesCount()).thenReturn(0);

		List<AssetVocabulary> assetVocabularies = AssetsUtil.getNonEmptyVocabulariesByIds(vocabularyIds);

		assertThat(assetVocabularies.size(), equalTo(1));
		assertThat(assetVocabularies.get(0), equalTo(mockAssetVocabulary1));
	}

	@Test
	public void getNonEmptyVocabulariesByIds_WhenVocabularyIdsSizeMoreThan0ButNotAllVocabulariesExist_ThenReturnListOfExistingVocabularies() {
		long[] vocabularyIds = new long[]{NON_EXISTING_VOCABULARY_ID, VOCABULARY_ID_1};

		when(AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(VOCABULARY_ID_1)).thenReturn(mockAssetVocabulary1);
		when(AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(NON_EXISTING_VOCABULARY_ID)).thenReturn(null);
		when(mockAssetVocabulary1.getCategoriesCount()).thenReturn(3);

		List<AssetVocabulary> assetVocabularies = AssetsUtil.getNonEmptyVocabulariesByIds(vocabularyIds);

		assertThat(assetVocabularies.size(), equalTo(1));
		assertThat(assetVocabularies.get(0), equalTo(mockAssetVocabulary1));
	}

	@Test
	public void getNonEmptyVocabulariesByIds_WhenVocabularyIdsSizeIs0_ThenReturnEmptyList() {
		long[] vocabularyIds = new long[]{};

		List<AssetVocabulary> assetVocabularies = AssetsUtil.getNonEmptyVocabulariesByIds(vocabularyIds);

		assertThat(assetVocabularies.size(), equalTo(0));
	}

	@Test
	public void getVocabularyGroupName_WhenNoExceptionAndVocabularyIdDifferentThanSiteGroupId_ThenReturnGroupName() throws PortalException {
		String expectedGroupName = "group name";
		when(mockAssetVocabulary1.getGroupId()).thenReturn(GROUP_ID_1);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID_2);
		when(GroupLocalServiceUtil.getGroup(GROUP_ID_1)).thenReturn(mockGroup);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockGroup.getDescriptiveName(DEFAULT_LOCALE)).thenReturn(expectedGroupName);

		String groupName = AssetsUtil.getVocabularyGroupName(mockAssetVocabulary1, mockThemeDisplay);

		assertThat(groupName, equalTo(expectedGroupName));

	}

	@Test
	public void getVocabularyGroupName_WhenNoExceptionAndVocabularyIdTheSameAsSiteGroupId_ThenReturnBlankString() throws PortalException {
		when(mockAssetVocabulary1.getGroupId()).thenReturn(GROUP_ID_1);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID_1);

		String groupName = AssetsUtil.getVocabularyGroupName(mockAssetVocabulary1, mockThemeDisplay);

		assertThat(groupName, equalTo(StringPool.BLANK));
	}

	@Test
	public void getVocabularyGroupName_WhenExceptionDuringGroupRetrieval_ThenReturnBlankString() throws PortalException {
		when(mockAssetVocabulary1.getGroupId()).thenReturn(GROUP_ID_1);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID_2);
		when(GroupLocalServiceUtil.getGroup(GROUP_ID_1)).thenThrow(new PortalException());

		String groupName = AssetsUtil.getVocabularyGroupName(mockAssetVocabulary1, mockThemeDisplay);

		assertThat(groupName, equalTo(StringPool.BLANK));
	}

	@Test
	public void getVocabularyGroupName_WhenExceptionDuringGettingTheDescriptiveName_ThenReturnBlankString() throws PortalException {
		when(mockAssetVocabulary1.getGroupId()).thenReturn(GROUP_ID_1);
		when(mockThemeDisplay.getSiteGroupId()).thenReturn(GROUP_ID_2);
		when(GroupLocalServiceUtil.getGroup(GROUP_ID_1)).thenReturn(mockGroup);
		when(mockThemeDisplay.getLocale()).thenReturn(DEFAULT_LOCALE);
		when(mockGroup.getDescriptiveName(DEFAULT_LOCALE)).thenThrow(new PortalException());

		String groupName = AssetsUtil.getVocabularyGroupName(mockAssetVocabulary1, mockThemeDisplay);

		assertThat(groupName, equalTo(StringPool.BLANK));
	}

	@Test
	public void isVocabularyRequired_WhenVocabularyIsRequiredForAGivenClass_ThenReturnTrue() {
		String className = "class name";
		long classNameId = 123;
		long classTypePk = 456;
		when(PortalUtil.getClassNameId(className)).thenReturn(classNameId);
		when(mockAssetVocabulary1.isRequired(classNameId, classTypePk)).thenReturn(true);

		boolean isVocabularyRequired = AssetsUtil.isVocabularyRequired(mockAssetVocabulary1, className, classTypePk);

		assertThat(isVocabularyRequired, equalTo(true));

	}

	@Test
	public void isVocabularyRequired_WhenVocabularyIsNotRequired_ThenReturnFalse() {
		String className = "class name";
		long classNameId = 123;
		long classTypePk = 456;
		when(PortalUtil.getClassNameId(className)).thenReturn(classNameId);
		when(mockAssetVocabulary1.isRequired(classNameId, classTypePk)).thenReturn(false);

		boolean isVocabularyRequired = AssetsUtil.isVocabularyRequired(mockAssetVocabulary1, className, classTypePk);

		assertThat(isVocabularyRequired, equalTo(false));
	}

	@Test
	public void getSelectedItemsList_WhenCategoryIdsNotEmpty_ThenReturnListOfLabelValueMapsForAddCategories() {
		String categoryIdsSeparator = StringPool.COMMA;
		String categoryTitlesSeparator = "_CATEGORY_";
		StringBuilder selectedCategoryIds = new StringBuilder();
		StringBuilder selectedCategoryIdTitles = new StringBuilder();

		int numberOfCategories = 3;
		String idPrefix = "00";
		String titlePrefix = "title";
		for (int i = 0; i < numberOfCategories; i++) {
			if (i == 0) {
				selectedCategoryIds.append(idPrefix).append(i);
				selectedCategoryIdTitles.append(titlePrefix).append(i);
			} else {
				selectedCategoryIds.append(categoryIdsSeparator).append(idPrefix).append(i);
				selectedCategoryIdTitles.append(categoryTitlesSeparator).append(titlePrefix).append(i);
			}

		}


		List<HashMap<String, Object>> selectedItems = AssetsUtil.getSelectedItemsList(selectedCategoryIds.toString(), selectedCategoryIdTitles.toString(),
				categoryIdsSeparator, categoryTitlesSeparator);

		assertThat(selectedItems.size(), equalTo(numberOfCategories));
		for (int i = 0; i < numberOfCategories; i++) {
			HashMap<String, Object> labelValueMap = selectedItems.get(i);
			assertThat(labelValueMap.get("label"), equalTo(titlePrefix + i));
			assertThat(labelValueMap.get("value"), equalTo(idPrefix + i));
		}
	}

	@Test
	public void getSelectedItemsList_WhenCategoryIdsIsEmpty_ThenReturnEmptyList() {
		String categoryIdsSeparator = StringPool.COMMA;
		String categoryTitlesSeparator = "_CATEGORY_";
		String selectedCategoryIds = StringPool.BLANK;
		String selectedCategoryIdTitles = StringPool.BLANK;

		List<HashMap<String, Object>> selectedItems = AssetsUtil.getSelectedItemsList(selectedCategoryIds, selectedCategoryIdTitles,
				categoryIdsSeparator, categoryTitlesSeparator);

		assertThat(selectedItems.size(), equalTo(0));
	}
}
