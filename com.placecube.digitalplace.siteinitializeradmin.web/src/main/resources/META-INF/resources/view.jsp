<%@ include file="/init.jsp" %>

<div class="container-fluid container-fluid-max-xl">
	<c:choose>
		<c:when test="${not empty siteInitializers}">
			
			<div class="alert alert-info">
				<liferay-ui:message key="select-a-site-initializer"/>
			</div>
			
			<portlet:actionURL name="<%= MVCCommandKeys.RUN_SITE_INITIALIZER %>" var="runSiteInitializerURL"/>
			
			<aui:form action="${runSiteInitializerURL}" name="runSiteInitializer" onSubmit='<%= "event.preventDefault();" %>'>
				<aui:select name="<%=PortletRequestKeys.SELECTED_SITE_INITIALIZER %>" showEmptyOption="true" label="">
					<c:forEach items="${siteInitializers}" var="entry">
						<aui:option value="${entry.getKey()}" label="${entry.getName(locale)}" />
					</c:forEach>
				</aui:select>
				
				<aui:button type="button" value="execute" onClick="checkSaveEntry()"/>
				
			</aui:form>
			
			<aui:script>
				function checkSaveEntry() {
					if(confirm('<liferay-ui:message key="are-you-sure-you-want-to-run-the-site-initializer"/>')){
						submitForm(document.<portlet:namespace />runSiteInitializer);
					}
				}
			</aui:script>
			
		</c:when>
		<c:otherwise>
			<div class="alert alert-warning">
				<liferay-ui:message key="no-entries-were-found"/>
			</div>
		</c:otherwise>
	</c:choose>
</div>