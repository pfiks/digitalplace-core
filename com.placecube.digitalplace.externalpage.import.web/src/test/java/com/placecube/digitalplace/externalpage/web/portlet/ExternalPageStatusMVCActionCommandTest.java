package com.placecube.digitalplace.externalpage.web.portlet;

import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;

import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SessionErrors.class, PropsUtil.class, ParamUtil.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.util.ParamUtil")
public class ExternalPageStatusMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private ExternalPageStatusMVCActionCommand externalPageStatusMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ExternalPage mockExternalPage;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Before
	public void activeSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, SessionErrors.class);
	}

	@Test
	public void doProcessAction_WhenStatusIsFalse_ThenUnPublishExternalPage() throws PortalException {
		long externalPageId = 123;
		boolean status = false;

		when(ParamUtil.getBoolean(mockActionRequest, PortletRequestKeys.STATUS)).thenReturn(status);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.EXTERNAL_PAGE_ID)).thenReturn(externalPageId);
		when(mockExternalPageLocalService.getExternalPageImport(externalPageId)).thenReturn(Optional.of(mockExternalPage));
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		externalPageStatusMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockExternalPageLocalService, times(1)).unPublishExternalPage(mockExternalPage, status);

	}

	@Test
	public void doProcessAction_WhenStatusIsTrue_ThenPublishExternalPage() throws PortalException {
		long externalPageId = 123;
		boolean status = true;

		when(ParamUtil.getBoolean(mockActionRequest, PortletRequestKeys.STATUS)).thenReturn(status);
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.EXTERNAL_PAGE_ID)).thenReturn(externalPageId);
		when(mockExternalPageLocalService.getExternalPageImport(externalPageId)).thenReturn(Optional.of(mockExternalPage));
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		externalPageStatusMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockExternalPageLocalService, times(1)).publishExternalPage(mockExternalPage, status);

	}

}
