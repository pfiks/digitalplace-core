package com.placecube.digitalplace.externalpage.web.panelapp;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.model.Portlet;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;

@Component(immediate = true, property = { "panel.app.order:Integer=1000", "panel.category.key=" + PanelCategoryKeys.SITE_ADMINISTRATION_CONTENT }, service = PanelApp.class)
public class ExternalPageImportPanelApp extends BasePanelApp {

	@Reference(target = "(javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET + ")", unbind = "-")
	private Portlet portlet;

	@Override
	public Portlet getPortlet() {
		return portlet;
	}

	@Override
	public String getPortletId() {
		return PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET;
	}

}
