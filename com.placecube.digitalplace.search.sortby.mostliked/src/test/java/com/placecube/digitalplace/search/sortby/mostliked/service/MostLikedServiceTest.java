package com.placecube.digitalplace.search.sortby.mostliked.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistry;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.ratings.kernel.model.RatingsStats;
import com.liferay.ratings.kernel.service.RatingsStatsLocalService;

public class MostLikedServiceTest extends PowerMockito {

	@Mock
	private Document mockDocument;

	@Mock
	private Indexer mockIndexer;

	@Mock
	private IndexerRegistry mockIndexerRegistry;

	@Mock
	private RatingsStats mockRatingsStats;

	@Mock
	private RatingsStatsLocalService mockRatingsStatsLocalService;

	@InjectMocks
	private MostLikedService mostActiveService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getScore_WhenNoStatsFound_ThenReturnsZero() {
		long classPK = 123;
		String className = "classNameVal";
		when(mockRatingsStatsLocalService.fetchStats(className, classPK)).thenReturn(null);

		double result = mostActiveService.getScore(className, classPK);

		assertThat(result, equalTo(0.0));
	}

	@Test
	public void getScore_WhenStatsFound_ThenReturnsTheStatsTotalScore() {
		long classPK = 123;
		String className = "classNameVal";
		double expected = 1.25;
		when(mockRatingsStatsLocalService.fetchStats(className, classPK)).thenReturn(mockRatingsStats);
		when(mockRatingsStats.getTotalScore()).thenReturn(expected);
		double result = mostActiveService.getScore(className, classPK);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void reindexRatingScore_WhenExceptionExecutingTheDocumentReindex_ThenNoErrorIsThrown() throws SearchException {
		long classPK = 123;
		String className = "classNameVal";
		when(mockIndexerRegistry.nullSafeGetIndexer(className)).thenReturn(mockIndexer);
		doThrow(new SearchException()).when(mockIndexer).reindex(className, classPK);

		try {
			mostActiveService.reindexRatingScore(className, classPK);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void reindexRatingScore_WhenNoError_ThenExecutesTheDocumentReindex() throws SearchException {
		long classPK = 123;
		String className = "classNameVal";
		when(mockIndexerRegistry.nullSafeGetIndexer(className)).thenReturn(mockIndexer);

		mostActiveService.reindexRatingScore(className, classPK);

		verify(mockIndexer, times(1)).reindex(className, classPK);
	}
}
