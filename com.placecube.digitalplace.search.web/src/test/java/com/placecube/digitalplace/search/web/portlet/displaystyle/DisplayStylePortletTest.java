package com.placecube.digitalplace.search.web.portlet.displaystyle;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchDisplayService;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DisplayStylePortletTest extends PowerMockito {

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SharedSearchDisplayService mockSharedSearchDisplayService;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@InjectMocks
	private DisplayStylePortlet displayStylePortlet;

	@Before
	public void activateSetup() {
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		displayStylePortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresTheDisplayStyleGridURLAsRequestAttribute() throws Exception {
		String expected = "expectedVal";
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchDisplayService.getDisplayStyleGridURL(mockSharedSearchResponse)).thenReturn(expected);

		displayStylePortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("displayStyleGridURL", expected);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresTheDisplayStyleListURLAsRequestAttribute() throws Exception {
		String expected = "expectedVal";
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchDisplayService.getDisplayStyleListURL(mockSharedSearchResponse)).thenReturn(expected);

		displayStylePortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("displayStyleListURL", expected);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoError_ThenConfiguresTheIsDisplayListSelectedAsRequestAttribute(boolean expected) throws Exception {
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchDisplayService.isDisplayStyleList(mockRenderRequest)).thenReturn(expected);

		displayStylePortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("isDisplayListSelected", expected);
	}
}
