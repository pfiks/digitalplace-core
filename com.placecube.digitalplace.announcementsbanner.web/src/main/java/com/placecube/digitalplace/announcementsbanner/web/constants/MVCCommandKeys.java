package com.placecube.digitalplace.announcementsbanner.web.constants;

public final class MVCCommandKeys {

	public static final String GET_ANNOUNCEMENT_CONFIGURATION = "/announcements-banner/get-configuration";

	public static final String READ_ANNOUNCEMENT = "/announcements-banner/read";

	private MVCCommandKeys() {
		return;
	}
}