package com.placecube.digitalplace.ddl.web.portlet.display;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLCategorisationService;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLEditorService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, LanguageUtil.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.util.ParamUtil")
public class UpdateDDLEntryMVCRenderCommandTest extends PowerMockito {

	@Mock
	private Set<AssetCategory> mockAssetCategories;

	@Mock
	private Set<AssetVocabulary> mockAssetVocabularies;

	@Mock
	private DDLDisplayPortletInstanceConfiguration mockConfiguration;

	@Mock
	private DDLCategorisationService mockDDLCategorisationService;

	@Mock
	private DDLConfigurationService mockDDLConfigurationService;

	@Mock
	private DDLEditorService mockDDLEditorService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DynamicDataListService mockDynamicDataListService;

	@Mock
	private Group mockGroup;

	@Mock
	private HttpServletRequest mockHTTPServletRequest;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private UpdateDDLEntryMVCRenderCommand updateDDLEntryMVCRenderCommand;

	@Before
	public void activeSetUp() {
		mockStatic(ParamUtil.class, LanguageUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenError_ThenThrowPortletException() throws PortletException {
		long ddmStructureId = 1;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLEditorService.getDDMStructure(ddmStructureId)).thenThrow(NullPointerException.class);

		updateDDLEntryMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoErrorAndDDLRecordIsNotPresent_ThenSetsAttributesAndBackURLBasedOnAddRecordAction() throws PortalException, PortletException {
		long ddmStructureId = 1;
		long ddlRecordId = 2;
		long ddlRecordSetId = 3;
		long scopeGroupId = 7;
		boolean enableTranslations = true;
		String structureName = "structureName";
		String groupName = "groupName";
		String pageMessageTitle = "pageMessageTitle";
		String backURL = "backURL";
		String[] categoryIds = new String[] { "123" };
		String[] vocabularyIds = new String[] { "456" };
		String[] messageArgs = new String[] { structureName, groupName };
		List<Long> categoryIdsParsed = Collections.singletonList(123L);
		List<Long> vocabularyIdsParsed = Collections.singletonList(456L);
		Locale locale = Locale.CANADA;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLEditorService.getDDMStructure(ddmStructureId)).thenReturn(Optional.of(mockDDMStructure));
		when(ParamUtil.getLong(mockRenderRequest, "ddmStructureId")).thenReturn(ddmStructureId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordId")).thenReturn(ddlRecordId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordSetId")).thenReturn(ddlRecordSetId);
		when(mockDDLEditorService.getDDLRecord(ddlRecordId)).thenReturn(Optional.empty());
		when(mockDDLEditorService.getFormTemplate(mockDDMStructure)).thenReturn(Optional.of(mockDDMTemplate));
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockDDLConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.enableTranslations()).thenReturn(enableTranslations);
		when(mockConfiguration.mandatoryCategoryIds()).thenReturn(categoryIds);
		when(mockConfiguration.mandatoryVocabularyIds()).thenReturn(vocabularyIds);
		when(mockDDLConfigurationService.getLongValuesAsList(categoryIds)).thenReturn(categoryIdsParsed);
		when(mockDDLConfigurationService.getLongValuesAsList(vocabularyIds)).thenReturn(vocabularyIdsParsed);
		when(mockDDLCategorisationService.getMandatoryCategories(categoryIdsParsed)).thenReturn(mockAssetCategories);
		when(mockDDLCategorisationService.getMandatoryVocabularies(vocabularyIdsParsed)).thenReturn(mockAssetVocabularies);
		when(mockDDLCategorisationService.getVocabularyIdsJSON(mockAssetCategories, mockAssetVocabularies)).thenReturn(mockJSONArray);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockDDMStructure.getName(locale)).thenReturn(structureName);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.getName(locale)).thenReturn(groupName);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHTTPServletRequest);
		when(LanguageUtil.format(mockHTTPServletRequest, "add-x-in-x", messageArgs)).thenReturn(pageMessageTitle);
		when(mockDDLEditorService.getBackURL(mockRenderRequest, mockThemeDisplay)).thenReturn(backURL);
		String[] ddlAutofillFields = new String[] { "one", "two" };
		when(ParamUtil.getStringValues(mockRenderRequest, "ddlAutofillFields")).thenReturn(ddlAutofillFields);
		when(mockDDLEditorService.getFieldsToAutofill(ddlAutofillFields)).thenReturn("expectedAutofillFields");

		updateDDLEntryMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq("ddmFormValues"), any());
		verify(mockRenderRequest, times(1)).setAttribute("pageMessageTitle", pageMessageTitle);
		verify(mockRenderRequest, times(1)).setAttribute("ddlAutofillFieldsJSON", "expectedAutofillFields");
		verify(mockDDLEditorService, times(1)).configureBackLinkURL(mockThemeDisplay, backURL, mockRenderRequest);

	}

	@Test
	public void render_WhenNoErrorAndStructureAndTemplateAndRecordAndRedirectArePresent_ThenReturnsUpdateEntryJSPAndSetsAllRequestAttributesAndSetsRedirectAsBackURL()
			throws PortalException, PortletException {
		long ddmStructureId = 1;
		long ddlRecordId = 2;
		long ddlRecordSetId = 3;
		long ddmStructureClassNameId = 4;
		long ddmTemplateClassNameId = 5;
		long ddmTemplateId = 6;
		long scopeGroupId = 7;
		long ddlTemplateId = 8;
		boolean enableTranslations = true;
		String requiredCategorisationNames = "requiredCategorisationNames";
		String structureName = "structureName";
		String groupName = "groupName";
		String pageMessageTitle = "pageMessageTitle";
		String backURL = "backURL";
		String[] categoryIds = new String[] { "123" };
		String[] vocabularyIds = new String[] { "456" };
		String[] messageArgs = new String[] { structureName, groupName };
		List<Long> categoryIdsParsed = Collections.singletonList(123L);
		List<Long> vocabularyIdsParsed = Collections.singletonList(456L);
		Locale locale = Locale.CANADA;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLEditorService.getDDMStructure(ddmStructureId)).thenReturn(Optional.of(mockDDMStructure));
		when(ParamUtil.getLong(mockRenderRequest, "ddmStructureId")).thenReturn(ddmStructureId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordId")).thenReturn(ddlRecordId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordSetId")).thenReturn(ddlRecordSetId);
		when(mockDDLEditorService.getDDLRecord(ddlRecordId)).thenReturn(Optional.of(mockDDLRecord));
		when(mockDDMStructure.getStructureId()).thenReturn(ddmStructureId);
		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(ddmStructureClassNameId);
		when(mockDDLEditorService.getFormTemplate(mockDDMStructure)).thenReturn(Optional.of(mockDDMTemplate));
		when(mockPortal.getClassNameId(DDMTemplate.class)).thenReturn(ddmTemplateClassNameId);
		when(mockDDMTemplate.getTemplateId()).thenReturn(ddmTemplateId);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockDDLConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.enableTranslations()).thenReturn(enableTranslations);
		when(mockConfiguration.mandatoryCategoryIds()).thenReturn(categoryIds);
		when(mockConfiguration.mandatoryVocabularyIds()).thenReturn(vocabularyIds);
		when(mockDDLConfigurationService.getLongValuesAsList(categoryIds)).thenReturn(categoryIdsParsed);
		when(mockDDLConfigurationService.getLongValuesAsList(vocabularyIds)).thenReturn(vocabularyIdsParsed);
		when(mockDDLCategorisationService.getMandatoryCategories(categoryIdsParsed)).thenReturn(mockAssetCategories);
		when(mockDDLCategorisationService.getMandatoryVocabularies(vocabularyIdsParsed)).thenReturn(mockAssetVocabularies);
		when(mockDDLCategorisationService.getVocabularyIdsJSON(mockAssetCategories, mockAssetVocabularies)).thenReturn(mockJSONArray);
		when(mockDDLCategorisationService.getRequiredCategorisationMessage(locale, mockAssetCategories, mockAssetVocabularies)).thenReturn(requiredCategorisationNames);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockDDLRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockDDMStructure.getName(locale)).thenReturn(structureName);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.getName(locale)).thenReturn(groupName);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHTTPServletRequest);
		when(LanguageUtil.format(mockHTTPServletRequest, "edit-x-in-x", messageArgs)).thenReturn(pageMessageTitle);
		when(mockDDLEditorService.getRedirectParamValue(mockRenderRequest)).thenReturn(null);
		when(ParamUtil.getLong(mockRenderRequest, "ddlTemplateId")).thenReturn(ddlTemplateId);
		when(mockDynamicDataListService.getViewDDLRecordURL(mockThemeDisplay, mockDDLRecord, ddlTemplateId)).thenReturn(backURL);

		String result = updateDDLEntryMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/ddl-display/update-entry.jsp"));
		verify(mockRenderRequest, times(1)).setAttribute("ddmFormTemplateId", ddmTemplateId);
		verify(mockRenderRequest, times(1)).setAttribute("classNameId", ddmTemplateClassNameId);
		verify(mockRenderRequest, times(1)).setAttribute("classPK", ddmTemplateId);
		verify(mockRenderRequest, times(1)).setAttribute("ddmStructure", mockDDMStructure);
		verify(mockRenderRequest, times(1)).setAttribute("ddlRecordSetId", ddlRecordSetId);
		verify(mockRenderRequest, times(1)).setAttribute("groupIds", new long[] { scopeGroupId });
		verify(mockRenderRequest, times(1)).setAttribute("ddlRecordId", ddlRecordId);
		verify(mockRenderRequest, times(1)).setAttribute("enableTranslations", enableTranslations);
		verify(mockRenderRequest, times(1)).setAttribute("requiredVocabularyIdsJsonArray", mockJSONArray);
		verify(mockRenderRequest, times(1)).setAttribute("requiredCategorisationNames", requiredCategorisationNames);
		verify(mockRenderRequest, times(1)).setAttribute("ddmFormValues", mockDDMFormValues);
		verify(mockRenderRequest, times(1)).setAttribute("pageMessageTitle", pageMessageTitle);
		verify(mockDDLEditorService, times(1)).configureBackLinkURL(mockThemeDisplay, backURL, mockRenderRequest);
		verify(mockRenderRequest, never()).setAttribute(eq("ddlAutofillFieldsJSON"), any());
	}

	@Test
	public void render_WhenNoErrorAndStructureAndTemplateAndRecordAndRedirectIsNotPresent_ThenSetsViewURLAsBackURL() throws PortalException, PortletException {
		long ddmStructureId = 1;
		long ddlRecordId = 2;
		long ddlRecordSetId = 3;
		long scopeGroupId = 7;
		long ddlTemplateId = 8;
		String backURL = "backURL";
		String[] categoryIds = new String[] { "123" };
		String[] vocabularyIds = new String[] { "456" };
		List<Long> categoryIdsParsed = Collections.singletonList(123L);
		List<Long> vocabularyIdsParsed = Collections.singletonList(456L);
		Locale locale = Locale.CANADA;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLEditorService.getDDMStructure(ddmStructureId)).thenReturn(Optional.of(mockDDMStructure));
		when(ParamUtil.getLong(mockRenderRequest, "ddmStructureId")).thenReturn(ddmStructureId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordId")).thenReturn(ddlRecordId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordSetId")).thenReturn(ddlRecordSetId);
		when(mockDDLEditorService.getDDLRecord(ddlRecordId)).thenReturn(Optional.of(mockDDLRecord));
		when(mockDDLEditorService.getFormTemplate(mockDDMStructure)).thenReturn(Optional.of(mockDDMTemplate));
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(scopeGroupId);
		when(mockDDLConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.mandatoryCategoryIds()).thenReturn(categoryIds);
		when(mockConfiguration.mandatoryVocabularyIds()).thenReturn(vocabularyIds);
		when(mockDDLConfigurationService.getLongValuesAsList(categoryIds)).thenReturn(categoryIdsParsed);
		when(mockDDLConfigurationService.getLongValuesAsList(vocabularyIds)).thenReturn(vocabularyIdsParsed);
		when(mockDDLCategorisationService.getMandatoryCategories(categoryIdsParsed)).thenReturn(mockAssetCategories);
		when(mockDDLCategorisationService.getMandatoryVocabularies(vocabularyIdsParsed)).thenReturn(mockAssetVocabularies);
		when(mockDDLCategorisationService.getVocabularyIdsJSON(mockAssetCategories, mockAssetVocabularies)).thenReturn(mockJSONArray);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockDDLRecord.getDDMFormValues()).thenReturn(mockDDMFormValues);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHTTPServletRequest);
		when(mockDDLEditorService.getRedirectParamValue(mockRenderRequest)).thenReturn(null);
		when(ParamUtil.getLong(mockRenderRequest, "ddlTemplateId")).thenReturn(ddlTemplateId);
		when(mockDynamicDataListService.getViewDDLRecordURL(mockThemeDisplay, mockDDLRecord, ddlTemplateId)).thenReturn(backURL);

		updateDDLEntryMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockDDLEditorService, times(1)).configureBackLinkURL(mockThemeDisplay, backURL, mockRenderRequest);
	}

	@Test
	public void render_WhenNoErrorAndTemplateIsNotPresent_ThenSetsRequestAttributesBasedOnStructure() throws PortalException, PortletException {
		long ddmStructureId = 1;
		long ddlRecordId = 2;
		long ddlRecordSetId = 3;
		long ddmStructureClassNameId = 4;
		String[] categoryIds = new String[] { "123" };
		String[] vocabularyIds = new String[] { "456" };
		List<Long> categoryIdsParsed = Collections.singletonList(123L);
		List<Long> vocabularyIdsParsed = Collections.singletonList(456L);
		Locale locale = Locale.CANADA;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLEditorService.getDDMStructure(ddmStructureId)).thenReturn(Optional.of(mockDDMStructure));
		when(ParamUtil.getLong(mockRenderRequest, "ddmStructureId")).thenReturn(ddmStructureId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordId")).thenReturn(ddlRecordId);
		when(ParamUtil.getLong(mockRenderRequest, "ddlRecordSetId")).thenReturn(ddlRecordSetId);
		when(mockDDLEditorService.getDDLRecord(ddlRecordId)).thenReturn(Optional.of(mockDDLRecord));
		when(mockDDMStructure.getStructureId()).thenReturn(ddmStructureId);
		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(ddmStructureClassNameId);
		when(mockDDLEditorService.getFormTemplate(mockDDMStructure)).thenReturn(Optional.empty());
		when(mockDDLConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.mandatoryCategoryIds()).thenReturn(categoryIds);
		when(mockConfiguration.mandatoryVocabularyIds()).thenReturn(vocabularyIds);
		when(mockDDLConfigurationService.getLongValuesAsList(categoryIds)).thenReturn(categoryIdsParsed);
		when(mockDDLConfigurationService.getLongValuesAsList(vocabularyIds)).thenReturn(vocabularyIdsParsed);
		when(mockDDLCategorisationService.getMandatoryCategories(categoryIdsParsed)).thenReturn(mockAssetCategories);
		when(mockDDLCategorisationService.getMandatoryVocabularies(vocabularyIdsParsed)).thenReturn(mockAssetVocabularies);
		when(mockDDLCategorisationService.getVocabularyIdsJSON(mockAssetCategories, mockAssetVocabularies)).thenReturn(mockJSONArray);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHTTPServletRequest);

		updateDDLEntryMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq("ddmFormTemplateId"), anyLong());
		verify(mockRenderRequest, times(1)).setAttribute("classNameId", ddmStructureClassNameId);
		verify(mockRenderRequest, times(1)).setAttribute("classPK", ddmStructureId);

	}

	@Test(expected = PortletException.class)
	public void render_WhenStructureIsNotPresent_ThenThrowPortletException() throws PortletException {
		long ddmStructureId = 1;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLEditorService.getDDMStructure(ddmStructureId)).thenReturn(Optional.empty());

		updateDDLEntryMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

}
