package com.placecube.digitalplace.oauth.dataprovider;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderException;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderInstanceSettings;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderRequest;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderResponse;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderResponseStatus;
import com.liferay.dynamic.data.mapping.data.provider.configuration.DDMDataProviderConfiguration;
import com.liferay.dynamic.data.mapping.data.provider.settings.DDMDataProviderSettingsProvider;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.oauth2.provider.model.OAuth2Application;
import com.liferay.oauth2.provider.service.OAuth2ApplicationLocalService;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.json.web.service.client.JSONWebServiceException;
import com.liferay.portal.kernel.cache.MultiVMPool;
import com.liferay.portal.kernel.cache.PortalCache;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.placecube.digitalplace.oauth.dataprovider.service.DDMRESTOAuth2DataProviderService;
import com.placecube.digitalplace.oauth.dataprovider.service.DataProviderPortalCacheService;
import com.placecube.digitalplace.oauth.dataprovider.service.DataProviderWebServiceClientService;
import com.placecube.digitalplace.oauth.dataprovider.settings.DDMRESTOAuth2DataProviderSettings;
import com.placecube.digitalplace.oauth.dataprovider.util.URLUtil;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DDMRESTOAuth2DataProviderTest {

	private static final long COMPANY_ID = 43332l;
	private static final long THREAD_LOCAL_COMPANY_ID = 321127;
	private static final String SETTINGS_URL = "/url";
	private static final String BUILT_URL = "/built-url";
	private static final String DATA_PROVIDER_INSTANCE_ID = "4325-234322";
	private static final String QUERY = "query";
	private static final String ABSOLUTE_URL = "/absoluteUrl";
	private static final String PORTAL_CACHE_KEY = "87666573";
	private static final String OAUTH_APP_EXTERNAL_REFERENCE_CODE = "8732gdsfds7-sad2342-safda92";

	@InjectMocks
	private DDMRESTOAuth2DataProvider ddmRESTOAuth2DataProvider;

	@Mock
	private DDMDataProviderInstanceSettings mockDDMDataProviderInstanceSettings;

	@Mock
	private DDMDataProviderSettingsProvider mockDDMDataProviderSettingsProvider;

	@Mock
	private URLUtil mockURLUtil;

	@Mock
	private MultiVMPool mockMultiVMPool;

	@Mock
	private OAuth2ApplicationLocalService mockOAuth2ApplicationLocalService;

	@Mock
	private OAuth2Application mockOAuth2Application;

	@Mock
	private DDMRESTOAuth2DataProviderService mockDDMRestOAuth2DataProviderService;

	@Mock
	private DataProviderWebServiceClientService mockDataProviderWebServiceClientService;

	@Mock
	private DataProviderPortalCacheService mockDataProviderPortalCacheService;

	@Mock
	private Map<String, Object> mockObjectMap;

	@Mock
	private Map<String, String> mockStringMap;

	@Mock
	private DDMDataProviderConfiguration mockDDMDataProviderConfiguration;

	@Mock
	private PortalCache mockPortalCache;

	@Mock
	private DDMDataProviderInstance mockDDMDataProviderInstance;

	@Mock
	private DDMRESTOAuth2DataProviderSettings mockDDMRESTOAuth2DataProviderSettings;

	@Mock
	private DocumentContext mockDocumentContext;

	@Mock
	private DDMDataProviderResponse.Builder mockNonStaticDDMDataProviderResponseBuilder;

	@Mock
	private DDMDataProviderRequest mockDDMDataProviderRequest;

	@Mock
	private DDMDataProviderResponse mockDDMDataProviderResponse;

	@Mock
	private URI mockURI;

	private MockedStatic<CompanyThreadLocal> mockCompanyThreadLocal;

	private MockedStatic<ConfigurableUtil> mockConfigurableUtil;

	private MockedStatic<DDMDataProviderResponse.Builder> mockDDMDataProviderResponseBuilder;

	private MockedStatic<JsonPath> mockJsonPath;

	@Before
	public void activateSetup() {
		mockCompanyThreadLocal = mockStatic(CompanyThreadLocal.class);
		mockConfigurableUtil = mockStatic(ConfigurableUtil.class);
		mockDDMDataProviderResponseBuilder = mockStatic(DDMDataProviderResponse.Builder.class);
		mockJsonPath = mockStatic(JsonPath.class);

		when(mockMultiVMPool.getPortalCache(DDMRESTOAuth2DataProvider.class.getName())).thenReturn(mockPortalCache);
		mockConfigurableUtil.when(() -> ConfigurableUtil.createConfigurable(DDMDataProviderConfiguration.class, mockObjectMap)).thenReturn(mockDDMDataProviderConfiguration);

		ddmRESTOAuth2DataProvider.activate(mockObjectMap);
	}

	@After
	public void teardown() {
		mockCompanyThreadLocal.close();
		mockConfigurableUtil.close();
		mockDDMDataProviderResponseBuilder.close();
		mockJsonPath.close();
	}

	@Test
	public void getSettings_WhenNoErrors_ThenReturnsDDMDataProviderInstanceSettings() {
		final Class settingsClass = DDMRESTOAuth2DataProviderSettings.class;

		when(mockDDMDataProviderSettingsProvider.getSettings()).thenReturn(settingsClass);

		Class result = ddmRESTOAuth2DataProvider.getSettings();

		assertThat(result, equalTo(settingsClass));
	}

	@Test
	public void deactivate_WhenNoErrors_ThenRemovesPortalCache() {
		ddmRESTOAuth2DataProvider.deactivate();

		verify(mockMultiVMPool, times(1)).removePortalCache(DDMRESTOAuth2DataProvider.class.getName());
	}

	@Test
	public void getData_WhenDDMDataProviderInstanceDoesNotExist_ThenReturnsServiceUnavailableResponse() throws Exception {
		mockCompanyThreadLocal.when(() -> CompanyThreadLocal.getCompanyId()).thenReturn(THREAD_LOCAL_COMPANY_ID);
		mockDDMDataProviderResponseBuilder.when(() -> DDMDataProviderResponse.Builder.newBuilder()).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);

		when(mockDDMRestOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest)).thenReturn(Optional.empty());
		when(mockNonStaticDDMDataProviderResponseBuilder.withStatus(DDMDataProviderResponseStatus.SERVICE_UNAVAILABLE)).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);
		when(mockNonStaticDDMDataProviderResponseBuilder.build()).thenReturn(mockDDMDataProviderResponse);

		InOrder inOrder = inOrder(mockNonStaticDDMDataProviderResponseBuilder, CompanyThreadLocal.class);

		DDMDataProviderResponse result = ddmRESTOAuth2DataProvider.getData(mockDDMDataProviderRequest);

		assertThat(result, sameInstance(mockDDMDataProviderResponse));

		inOrder.verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).withStatus(DDMDataProviderResponseStatus.SERVICE_UNAVAILABLE);
		inOrder.verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).build();
		inOrder.verify(mockCompanyThreadLocal, () -> CompanyThreadLocal.setCompanyId(THREAD_LOCAL_COMPANY_ID));
	}

	@Test
	public void getData_WhenResponseIsNotInCacheAndDataRetrievalFails_ThenEmptyResponseWithUnavailableStatusIsReturned() throws Exception {
		mockJsonPath.when(() -> JsonPath.parse("{}")).thenReturn(mockDocumentContext);

		when(mockDDMRestOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest)).thenReturn(Optional.of(mockDDMDataProviderInstance));
		when(mockDDMDataProviderInstance.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMDataProviderInstanceSettings.getSettings(mockDDMDataProviderInstance, DDMRESTOAuth2DataProviderSettings.class)).thenReturn(mockDDMRESTOAuth2DataProviderSettings);

		when(mockDDMRestOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings)).thenReturn(mockObjectMap);
		when(mockDDMRESTOAuth2DataProviderSettings.url()).thenReturn(SETTINGS_URL);
		when(mockDataProviderWebServiceClientService.getPathInputParametersMap(mockObjectMap, SETTINGS_URL)).thenReturn(mockStringMap);
		when(mockURLUtil.buildURL(mockStringMap, SETTINGS_URL)).thenReturn(BUILT_URL);
		when(mockURLUtil.buildURI(BUILT_URL)).thenReturn(mockURI);
		mockURI(QUERY);
		when(mockDataProviderWebServiceClientService.getAllParametersMap(mockStringMap, QUERY, mockObjectMap)).thenReturn(mockStringMap);
		when(mockURLUtil.getAbsoluteURL(QUERY, BUILT_URL)).thenReturn(ABSOLUTE_URL);
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_INSTANCE_ID);
		when(mockDataProviderPortalCacheService.getPortalCacheKey(DATA_PROVIDER_INSTANCE_ID, mockStringMap, ABSOLUTE_URL)).thenReturn(PORTAL_CACHE_KEY);
		when(mockPortalCache.get(PORTAL_CACHE_KEY)).thenReturn(null);
		when(mockDDMDataProviderConfiguration.trustSelfSignedCertificates()).thenReturn(true);
		when(mockDDMRESTOAuth2DataProviderSettings.oauthApplicationExternalReferenceCode()).thenReturn(OAUTH_APP_EXTERNAL_REFERENCE_CODE);
		when(mockOAuth2ApplicationLocalService.getOAuth2ApplicationByExternalReferenceCode(OAUTH_APP_EXTERNAL_REFERENCE_CODE, COMPANY_ID)).thenReturn(mockOAuth2Application);
		when(mockDataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, mockStringMap)).thenThrow(new JSONWebServiceException("error"));
		when(mockDDMRestOAuth2DataProviderService.createDDMDataProviderResponse(mockDocumentContext, DDMDataProviderResponseStatus.SERVICE_UNAVAILABLE, mockDDMRESTOAuth2DataProviderSettings))
				.thenReturn(mockDDMDataProviderResponse);

		DDMDataProviderResponse result = ddmRESTOAuth2DataProvider.getData(mockDDMDataProviderRequest);

		assertThat(result, sameInstance(mockDDMDataProviderResponse));
	}

	@Test
	public void getData_WhenResponseIsNotInCacheAndDataRetrievalIsSuccessful_ThenReturnsSuccessResponseWithJSONBody() throws Exception {
		when(mockDDMRestOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest)).thenReturn(Optional.of(mockDDMDataProviderInstance));
		when(mockDDMDataProviderInstance.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMDataProviderInstanceSettings.getSettings(mockDDMDataProviderInstance, DDMRESTOAuth2DataProviderSettings.class)).thenReturn(mockDDMRESTOAuth2DataProviderSettings);

		when(mockDDMRestOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings)).thenReturn(mockObjectMap);
		when(mockDDMRESTOAuth2DataProviderSettings.url()).thenReturn(SETTINGS_URL);
		when(mockDataProviderWebServiceClientService.getPathInputParametersMap(mockObjectMap, SETTINGS_URL)).thenReturn(mockStringMap);
		when(mockURLUtil.buildURL(mockStringMap, SETTINGS_URL)).thenReturn(BUILT_URL);
		when(mockURLUtil.buildURI(BUILT_URL)).thenReturn(mockURI);
		mockURI(QUERY);
		when(mockDataProviderWebServiceClientService.getAllParametersMap(mockStringMap, QUERY, mockObjectMap)).thenReturn(mockStringMap);
		when(mockURLUtil.getAbsoluteURL(QUERY, BUILT_URL)).thenReturn(ABSOLUTE_URL);
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_INSTANCE_ID);
		when(mockDataProviderPortalCacheService.getPortalCacheKey(DATA_PROVIDER_INSTANCE_ID, mockStringMap, ABSOLUTE_URL)).thenReturn(PORTAL_CACHE_KEY);
		when(mockPortalCache.get(PORTAL_CACHE_KEY)).thenReturn(null);
		when(mockDDMDataProviderConfiguration.trustSelfSignedCertificates()).thenReturn(true);
		when(mockDDMRESTOAuth2DataProviderSettings.oauthApplicationExternalReferenceCode()).thenReturn(OAUTH_APP_EXTERNAL_REFERENCE_CODE);
		when(mockOAuth2ApplicationLocalService.getOAuth2ApplicationByExternalReferenceCode(OAUTH_APP_EXTERNAL_REFERENCE_CODE, COMPANY_ID)).thenReturn(mockOAuth2Application);
		when(mockDataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, mockStringMap)).thenReturn("response");

		String sanitisedResponse = IOUtils.toString(new BOMInputStream(new ByteArrayInputStream("response".getBytes())), StandardCharsets.UTF_8);

		mockJsonPath.when(() -> JsonPath.parse(sanitisedResponse)).thenReturn(mockDocumentContext);

		when(mockDDMRestOAuth2DataProviderService.createDDMDataProviderResponse(mockDocumentContext, DDMDataProviderResponseStatus.OK, mockDDMRESTOAuth2DataProviderSettings))
				.thenReturn(mockDDMDataProviderResponse);

		DDMDataProviderResponse result = ddmRESTOAuth2DataProvider.getData(mockDDMDataProviderRequest);

		assertThat(result, sameInstance(mockDDMDataProviderResponse));
	}

	@Test
	public void getData_WhenResponseIsNotInCacheAndDataRetrievalIsSuccessfulAndIsCacheable_ThenSetsResponseInCache() throws Exception {
		when(mockDDMRestOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest)).thenReturn(Optional.of(mockDDMDataProviderInstance));
		when(mockDDMDataProviderInstance.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMDataProviderInstanceSettings.getSettings(mockDDMDataProviderInstance, DDMRESTOAuth2DataProviderSettings.class)).thenReturn(mockDDMRESTOAuth2DataProviderSettings);

		when(mockDDMRestOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings)).thenReturn(mockObjectMap);
		when(mockDDMRESTOAuth2DataProviderSettings.url()).thenReturn(SETTINGS_URL);
		when(mockDataProviderWebServiceClientService.getPathInputParametersMap(mockObjectMap, SETTINGS_URL)).thenReturn(mockStringMap);
		when(mockURLUtil.buildURL(mockStringMap, SETTINGS_URL)).thenReturn(BUILT_URL);
		when(mockURLUtil.buildURI(BUILT_URL)).thenReturn(mockURI);
		mockURI(QUERY);
		when(mockDataProviderWebServiceClientService.getAllParametersMap(mockStringMap, QUERY, mockObjectMap)).thenReturn(mockStringMap);
		when(mockURLUtil.getAbsoluteURL(QUERY, BUILT_URL)).thenReturn(ABSOLUTE_URL);
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_INSTANCE_ID);
		when(mockDataProviderPortalCacheService.getPortalCacheKey(DATA_PROVIDER_INSTANCE_ID, mockStringMap, ABSOLUTE_URL)).thenReturn(PORTAL_CACHE_KEY);
		when(mockPortalCache.get(PORTAL_CACHE_KEY)).thenReturn(null);
		when(mockDDMDataProviderConfiguration.trustSelfSignedCertificates()).thenReturn(true);
		when(mockDDMRESTOAuth2DataProviderSettings.oauthApplicationExternalReferenceCode()).thenReturn(OAUTH_APP_EXTERNAL_REFERENCE_CODE);
		when(mockOAuth2ApplicationLocalService.getOAuth2ApplicationByExternalReferenceCode(OAUTH_APP_EXTERNAL_REFERENCE_CODE, COMPANY_ID)).thenReturn(mockOAuth2Application);
		when(mockDataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, mockStringMap)).thenReturn("response");
		when(mockDDMRESTOAuth2DataProviderSettings.cacheable()).thenReturn(true);

		String sanitisedResponse = IOUtils.toString(new BOMInputStream(new ByteArrayInputStream("response".getBytes())), StandardCharsets.UTF_8);

		mockJsonPath.when(() -> JsonPath.parse(sanitisedResponse)).thenReturn(mockDocumentContext);

		when(mockDDMRestOAuth2DataProviderService.createDDMDataProviderResponse(mockDocumentContext, DDMDataProviderResponseStatus.OK, mockDDMRESTOAuth2DataProviderSettings))
				.thenReturn(mockDDMDataProviderResponse);

		DDMDataProviderResponse result = ddmRESTOAuth2DataProvider.getData(mockDDMDataProviderRequest);

		assertThat(result, sameInstance(mockDDMDataProviderResponse));

		verify(mockPortalCache, times(1)).put(PORTAL_CACHE_KEY, mockDDMDataProviderResponse);
	}

	@Test
	public void getData_WhenResponseIsNotInCacheAndDataRetrievalIsSuccessfulAndIsNotCacheable_ThenDoesNotSetResponseInCache() throws Exception {
		when(mockDDMRestOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest)).thenReturn(Optional.of(mockDDMDataProviderInstance));
		when(mockDDMDataProviderInstance.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMDataProviderInstanceSettings.getSettings(mockDDMDataProviderInstance, DDMRESTOAuth2DataProviderSettings.class)).thenReturn(mockDDMRESTOAuth2DataProviderSettings);

		when(mockDDMRestOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings)).thenReturn(mockObjectMap);
		when(mockDDMRESTOAuth2DataProviderSettings.url()).thenReturn(SETTINGS_URL);
		when(mockDataProviderWebServiceClientService.getPathInputParametersMap(mockObjectMap, SETTINGS_URL)).thenReturn(mockStringMap);
		when(mockURLUtil.buildURL(mockStringMap, SETTINGS_URL)).thenReturn(BUILT_URL);
		when(mockURLUtil.buildURI(BUILT_URL)).thenReturn(mockURI);
		mockURI(QUERY);
		when(mockDataProviderWebServiceClientService.getAllParametersMap(mockStringMap, QUERY, mockObjectMap)).thenReturn(mockStringMap);
		when(mockURLUtil.getAbsoluteURL(QUERY, BUILT_URL)).thenReturn(ABSOLUTE_URL);
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_INSTANCE_ID);
		when(mockDataProviderPortalCacheService.getPortalCacheKey(DATA_PROVIDER_INSTANCE_ID, mockStringMap, ABSOLUTE_URL)).thenReturn(PORTAL_CACHE_KEY);
		when(mockPortalCache.get(PORTAL_CACHE_KEY)).thenReturn(null);
		when(mockDDMDataProviderConfiguration.trustSelfSignedCertificates()).thenReturn(true);
		when(mockDDMRESTOAuth2DataProviderSettings.oauthApplicationExternalReferenceCode()).thenReturn(OAUTH_APP_EXTERNAL_REFERENCE_CODE);
		when(mockOAuth2ApplicationLocalService.getOAuth2ApplicationByExternalReferenceCode(OAUTH_APP_EXTERNAL_REFERENCE_CODE, COMPANY_ID)).thenReturn(mockOAuth2Application);
		when(mockDataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, mockStringMap)).thenReturn("response");
		when(mockDDMRESTOAuth2DataProviderSettings.cacheable()).thenReturn(false);

		ddmRESTOAuth2DataProvider.getData(mockDDMDataProviderRequest);

		verify(mockPortalCache, never()).put(anyString(), any(DDMDataProviderResponse.class));
	}

	@Test
	public void getData_WhenResponseIsInCacheAndIsCacheable_ThenReturnsResponseInCache() throws Exception {
		when(mockDDMRestOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest)).thenReturn(Optional.of(mockDDMDataProviderInstance));
		when(mockDDMDataProviderInstance.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMDataProviderInstanceSettings.getSettings(mockDDMDataProviderInstance, DDMRESTOAuth2DataProviderSettings.class)).thenReturn(mockDDMRESTOAuth2DataProviderSettings);

		when(mockDDMRestOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings)).thenReturn(mockObjectMap);
		when(mockDDMRESTOAuth2DataProviderSettings.url()).thenReturn(SETTINGS_URL);
		when(mockDataProviderWebServiceClientService.getPathInputParametersMap(mockObjectMap, SETTINGS_URL)).thenReturn(mockStringMap);
		when(mockURLUtil.buildURL(mockStringMap, SETTINGS_URL)).thenReturn(BUILT_URL);
		when(mockURLUtil.buildURI(BUILT_URL)).thenReturn(mockURI);
		mockURI(QUERY);
		when(mockDataProviderWebServiceClientService.getAllParametersMap(mockStringMap, QUERY, mockObjectMap)).thenReturn(mockStringMap);
		when(mockURLUtil.getAbsoluteURL(QUERY, BUILT_URL)).thenReturn(ABSOLUTE_URL);
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_INSTANCE_ID);
		when(mockDataProviderPortalCacheService.getPortalCacheKey(DATA_PROVIDER_INSTANCE_ID, mockStringMap, ABSOLUTE_URL)).thenReturn(PORTAL_CACHE_KEY);
		when(mockPortalCache.get(PORTAL_CACHE_KEY)).thenReturn(mockDDMDataProviderResponse);
		when(mockDDMRESTOAuth2DataProviderSettings.cacheable()).thenReturn(true);

		DDMDataProviderResponse result = ddmRESTOAuth2DataProvider.getData(mockDDMDataProviderRequest);

		assertThat(result, sameInstance(mockDDMDataProviderResponse));
	}

	@Test
	public void getData_WhenResponseIsInCacheAndIsNotCacheable_ThenRetrievesWebServiceResponse() throws Exception {
		when(mockDDMRestOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest)).thenReturn(Optional.of(mockDDMDataProviderInstance));
		when(mockDDMDataProviderInstance.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMDataProviderInstanceSettings.getSettings(mockDDMDataProviderInstance, DDMRESTOAuth2DataProviderSettings.class)).thenReturn(mockDDMRESTOAuth2DataProviderSettings);

		when(mockDDMRestOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings)).thenReturn(mockObjectMap);
		when(mockDDMRESTOAuth2DataProviderSettings.url()).thenReturn(SETTINGS_URL);
		when(mockDataProviderWebServiceClientService.getPathInputParametersMap(mockObjectMap, SETTINGS_URL)).thenReturn(mockStringMap);
		when(mockURLUtil.buildURL(mockStringMap, SETTINGS_URL)).thenReturn(BUILT_URL);
		when(mockURLUtil.buildURI(BUILT_URL)).thenReturn(mockURI);
		mockURI(QUERY);
		when(mockDataProviderWebServiceClientService.getAllParametersMap(mockStringMap, QUERY, mockObjectMap)).thenReturn(mockStringMap);
		when(mockURLUtil.getAbsoluteURL(QUERY, BUILT_URL)).thenReturn(ABSOLUTE_URL);
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_INSTANCE_ID);
		when(mockDataProviderPortalCacheService.getPortalCacheKey(DATA_PROVIDER_INSTANCE_ID, mockStringMap, ABSOLUTE_URL)).thenReturn(PORTAL_CACHE_KEY);
		when(mockPortalCache.get(PORTAL_CACHE_KEY)).thenReturn(mockDDMDataProviderResponse);
		when(mockDDMRESTOAuth2DataProviderSettings.cacheable()).thenReturn(false);
		when(mockDDMDataProviderConfiguration.trustSelfSignedCertificates()).thenReturn(true);
		when(mockDDMRESTOAuth2DataProviderSettings.oauthApplicationExternalReferenceCode()).thenReturn(OAUTH_APP_EXTERNAL_REFERENCE_CODE);
		when(mockOAuth2ApplicationLocalService.getOAuth2ApplicationByExternalReferenceCode(OAUTH_APP_EXTERNAL_REFERENCE_CODE, COMPANY_ID)).thenReturn(mockOAuth2Application);
		when(mockDataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, mockStringMap)).thenReturn("response");

		String sanitisedResponse = IOUtils.toString(new BOMInputStream(new ByteArrayInputStream("response".getBytes())), StandardCharsets.UTF_8);

		mockJsonPath.when(() -> JsonPath.parse(sanitisedResponse)).thenReturn(mockDocumentContext);

		when(mockDDMRestOAuth2DataProviderService.createDDMDataProviderResponse(mockDocumentContext, DDMDataProviderResponseStatus.OK, mockDDMRESTOAuth2DataProviderSettings))
				.thenReturn(mockDDMDataProviderResponse);

		DDMDataProviderResponse result = ddmRESTOAuth2DataProvider.getData(mockDDMDataProviderRequest);

		assertThat(result, sameInstance(mockDDMDataProviderResponse));
	}

	@Test
	public void getData_WhenNoErrors_ThenResetsCompanyIdInCompanyThreadLocal() throws Exception {
		mockCompanyThreadLocal.when(() -> CompanyThreadLocal.getCompanyId()).thenReturn(THREAD_LOCAL_COMPANY_ID);

		when(mockDDMRestOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest)).thenReturn(Optional.of(mockDDMDataProviderInstance));
		when(mockDDMDataProviderInstance.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMDataProviderInstanceSettings.getSettings(mockDDMDataProviderInstance, DDMRESTOAuth2DataProviderSettings.class)).thenReturn(mockDDMRESTOAuth2DataProviderSettings);

		when(mockDDMRestOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings)).thenReturn(mockObjectMap);
		when(mockDDMRESTOAuth2DataProviderSettings.url()).thenReturn(SETTINGS_URL);
		when(mockDataProviderWebServiceClientService.getPathInputParametersMap(mockObjectMap, SETTINGS_URL)).thenReturn(mockStringMap);
		when(mockURLUtil.buildURL(mockStringMap, SETTINGS_URL)).thenReturn(BUILT_URL);
		when(mockURLUtil.buildURI(BUILT_URL)).thenReturn(mockURI);
		mockURI(QUERY);
		when(mockDataProviderWebServiceClientService.getAllParametersMap(mockStringMap, QUERY, mockObjectMap)).thenReturn(mockStringMap);
		when(mockURLUtil.getAbsoluteURL(QUERY, BUILT_URL)).thenReturn(ABSOLUTE_URL);
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_INSTANCE_ID);
		when(mockDataProviderPortalCacheService.getPortalCacheKey(DATA_PROVIDER_INSTANCE_ID, mockStringMap, ABSOLUTE_URL)).thenReturn(PORTAL_CACHE_KEY);
		when(mockPortalCache.get(PORTAL_CACHE_KEY)).thenReturn(null);
		when(mockDDMDataProviderConfiguration.trustSelfSignedCertificates()).thenReturn(true);
		when(mockDDMRESTOAuth2DataProviderSettings.oauthApplicationExternalReferenceCode()).thenReturn(OAUTH_APP_EXTERNAL_REFERENCE_CODE);
		when(mockOAuth2ApplicationLocalService.getOAuth2ApplicationByExternalReferenceCode(OAUTH_APP_EXTERNAL_REFERENCE_CODE, COMPANY_ID)).thenReturn(mockOAuth2Application);
		when(mockDataProviderWebServiceClientService.callJSONWebService(COMPANY_ID, mockOAuth2Application, mockURI, ABSOLUTE_URL, true, mockStringMap)).thenReturn("response");

		String sanitisedResponse = IOUtils.toString(new BOMInputStream(new ByteArrayInputStream("response".getBytes())), StandardCharsets.UTF_8);
		mockJsonPath.when(() -> JsonPath.parse(sanitisedResponse)).thenReturn(mockDocumentContext);

		ddmRESTOAuth2DataProvider.getData(mockDDMDataProviderRequest);

		mockCompanyThreadLocal.verify(() -> CompanyThreadLocal.setCompanyId(THREAD_LOCAL_COMPANY_ID));
	}

	@Test(expected = DDMDataProviderException.class)
	public void getData_WhenCallToConfiguredEndpointFails_ThenThrowsDDMDataProviderException() throws Exception {
		mockCompanyThreadLocal.when(() -> CompanyThreadLocal.getCompanyId()).thenReturn(THREAD_LOCAL_COMPANY_ID);

		when(mockDDMRestOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest)).thenReturn(Optional.of(mockDDMDataProviderInstance));
		when(mockDDMDataProviderInstance.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMDataProviderInstanceSettings.getSettings(mockDDMDataProviderInstance, DDMRESTOAuth2DataProviderSettings.class)).thenReturn(mockDDMRESTOAuth2DataProviderSettings);
		when(mockDDMRestOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings)).thenReturn(mockObjectMap);
		when(mockDDMRESTOAuth2DataProviderSettings.url()).thenReturn(SETTINGS_URL);
		when(mockDataProviderWebServiceClientService.getPathInputParametersMap(mockObjectMap, SETTINGS_URL)).thenReturn(mockStringMap);
		when(mockURLUtil.buildURL(mockStringMap, SETTINGS_URL)).thenReturn(BUILT_URL);
		when(mockURLUtil.buildURI(BUILT_URL)).thenThrow(new URISyntaxException(BUILT_URL, "error"));

		ddmRESTOAuth2DataProvider.getData(mockDDMDataProviderRequest);

		mockCompanyThreadLocal.verify(() -> CompanyThreadLocal.setCompanyId(THREAD_LOCAL_COMPANY_ID));

	}

	private void mockURI(String query) {
		when(mockURI.getQuery()).thenReturn(query);
	}
}
