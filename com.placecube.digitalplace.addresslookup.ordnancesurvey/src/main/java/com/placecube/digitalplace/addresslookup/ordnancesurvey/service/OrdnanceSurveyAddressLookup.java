package com.placecube.digitalplace.addresslookup.ordnancesurvey.service;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookup;
import com.placecube.digitalplace.addresslookup.ordnancesurvey.configuration.OrdnanceSurveyCompanyConfiguration;

@Component(immediate = true, service = AddressLookup.class)
public class OrdnanceSurveyAddressLookup implements AddressLookup {

	private static final Log LOG = LogFactoryUtil.getLog(OrdnanceSurveyAddressLookup.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private OrdnanceSurveyLookupService ordnanceSurveyLookupService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return getConfiguration(companyId).enabled();
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Set<AddressContext> findByKeyword(long companyId, String keyword, String[] paramsAndValues) {
		try {
			OrdnanceSurveyCompanyConfiguration configuration = getConfiguration(companyId);

			String urlToCall = ordnanceSurveyLookupService.getURL(configuration.findEndpointURL(), "query", keyword, ordnanceSurveyLookupService.getAdditionalParametersAndValuesMap(paramsAndValues),
					configuration.applicationKey());

			return ordnanceSurveyLookupService.getResults(urlToCall);

		} catch (ConfigurationException e) {
			LOG.error(e);
			return Collections.emptySet();
		}
	}

	@Override
	public Set<AddressContext> findByPostcode(long companyId, String postcode, String[] paramsAndValues) {
		try {
			OrdnanceSurveyCompanyConfiguration configuration = getConfiguration(companyId);

			String urlToCall = ordnanceSurveyLookupService.getURL(configuration.postcodeEndpointURL(), "postcode", postcode,
					ordnanceSurveyLookupService.getAdditionalParametersAndValuesMap(paramsAndValues), configuration.applicationKey());

			return ordnanceSurveyLookupService.getResults(urlToCall);

		} catch (ConfigurationException e) {
			LOG.error(e);
			return Collections.emptySet();
		}
	}

	@Override
	public Optional<AddressContext> findByUprn(long companyId, String uprn, String[] paramsAndValues) {
		try {

			OrdnanceSurveyCompanyConfiguration configuration = getConfiguration(companyId);

			String urlToCall = ordnanceSurveyLookupService.getURL(configuration.uprnEndpointURL(), "uprn", uprn, ordnanceSurveyLookupService.getAdditionalParametersAndValuesMap(paramsAndValues),
					configuration.applicationKey());

			Set<AddressContext> addressLookups = ordnanceSurveyLookupService.getResults(urlToCall);
			if (!addressLookups.isEmpty()) {
				return Optional.of(addressLookups.iterator().next());
			}

		} catch (ConfigurationException e) {
			LOG.error(e);
		}

		return Optional.empty();
	}

	@Override
	public int getWeight(long companyId) {
		try {
			return getConfiguration(companyId).weight();
		} catch (Exception e) {
			LOG.error(e);
			return 0;
		}
	}

	@Override
	public boolean national(long companyId) {
		try {
			return getConfiguration(companyId).national();
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	private OrdnanceSurveyCompanyConfiguration getConfiguration(Long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, companyId);
	}

}
