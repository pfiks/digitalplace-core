package com.placecube.digitalplace.externalpage.web.service;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.SystemProperties;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

@Component(immediate = true, service = CrawlModelBuilder.class)
public class CrawlModelBuilder {

	public CrawlConfig getCrawlConfig() {
		CrawlConfig crawlConfig = new CrawlConfig();
		crawlConfig.setCrawlStorageFolder(SystemProperties.get(SystemProperties.TMP_DIR));
		return crawlConfig;
	}

	public CrawlController getCrawlController(CrawlConfig crawlConfig, PageFetcher pageFetcher, RobotstxtServer robotstxtServer) throws Exception {
		return new CrawlController(crawlConfig, pageFetcher, robotstxtServer);
	}

	public PageFetcher getPageFetcher(CrawlConfig config) {
		return new PageFetcher(config);
	}

	public RobotstxtConfig getRobotstxtConfig() {
		RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		robotstxtConfig.setEnabled(false);
		return robotstxtConfig;
	}

	public RobotstxtServer getRobotstxtServer(RobotstxtConfig robotstxtConfig, PageFetcher pageFetcher) {
		return new RobotstxtServer(robotstxtConfig, pageFetcher);
	}

}
