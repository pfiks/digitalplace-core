package com.placecube.digitalplace.siteinitializeradmin.constants;

public final class MVCCommandKeys {

	public static final String RUN_SITE_INITIALIZER = "run-site-initializer";

	private MVCCommandKeys() {
		return;
	}
}