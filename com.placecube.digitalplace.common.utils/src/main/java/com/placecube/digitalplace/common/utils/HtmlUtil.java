package com.placecube.digitalplace.common.utils;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

public class HtmlUtil {

	public static String escapeName(String name) {
		return StringUtil.replace(com.liferay.portal.kernel.util.HtmlUtil.escape(name), "&#39;", StringPool.APOSTROPHE);
	}

}