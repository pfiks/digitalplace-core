package com.placecube.digitalplace.group.rest.application.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;

public class GroupContentSearchHelperTest extends PowerMockito {

	@InjectMocks
	private GroupContentSearchHelper groupContentSearchHelper;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void evaluateStartResultBasedOnCurrentPageAndDelta_WhenResultsPerPageNotSet_ThenReturnsTheStartResultsCalculatingOneResultPerPage() {
		Integer startPage = 3;
		Integer resultsPerPage = null;

		int result = groupContentSearchHelper.evaluateStartResultBasedOnCurrentPageAndDelta(startPage, resultsPerPage);

		assertThat(result, equalTo(2));
	}

	@Test
	public void evaluateStartResultBasedOnCurrentPageAndDelta_WhenStartPageIsInvalid_ThenReturnsZeroAsStartResultsNumber() {
		Integer startPage = 0;
		Integer resultsPerPage = 4;

		int result = groupContentSearchHelper.evaluateStartResultBasedOnCurrentPageAndDelta(startPage, resultsPerPage);

		assertThat(result, equalTo(0));
	}

	@Test
	public void evaluateStartResultBasedOnCurrentPageAndDelta_WhenStartPageNotSet_ThenReturnsZeroAsStartResultsNumber() {
		Integer startPage = null;
		Integer resultsPerPage = 4;

		int result = groupContentSearchHelper.evaluateStartResultBasedOnCurrentPageAndDelta(startPage, resultsPerPage);

		assertThat(result, equalTo(0));
	}

	@Test
	public void evaluateStartResultBasedOnCurrentPageAndDelta_WhenValuesAreSet_ThenReturnsTheEvaluatedStartResults() {
		Integer startPage = 3;
		Integer resultsPerPage = 4;

		int result = groupContentSearchHelper.evaluateStartResultBasedOnCurrentPageAndDelta(startPage, resultsPerPage);

		assertThat(result, equalTo(8));
	}
}
