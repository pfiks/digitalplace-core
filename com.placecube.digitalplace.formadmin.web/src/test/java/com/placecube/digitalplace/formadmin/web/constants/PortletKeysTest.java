package com.placecube.digitalplace.formadmin.web.constants;

import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isPrivate;
import static java.lang.reflect.Modifier.isStatic;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.junit.Test;

import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;

public class PortletKeysTest {

	@Test
	public void testConstantValueForFormsManagerAdmin() {
		assertThat(PortletKeys.FORMS_MANAGER, equalTo("com_placecube_digitalplace_formadmin_FormsManagerPortlet"));
	}

	@Test
	public void testClassIsAConstantsClass() throws Exception {
		Class<?> classToTest = PortletKeys.class;

		assertThat(isFinal(classToTest.getModifiers()), equalTo(true));

		assertThat(classToTest.getDeclaredConstructors().length, equalTo(1));
		Constructor<?> constructor = classToTest.getDeclaredConstructor();
		assertThat(isPrivate(constructor.getModifiers()), equalTo(true));
		assertThat(constructor.isAccessible(), equalTo(false));

		constructor.setAccessible(true);
		constructor.newInstance();
		constructor.setAccessible(false);

		for (Method method : classToTest.getMethods()) {
			if (method.isAccessible()) {
				assertThat(isStatic(method.getModifiers()), equalTo(true));
				assertThat(method.getDeclaringClass(), equalTo(classToTest));
			}
		}
	}
}
