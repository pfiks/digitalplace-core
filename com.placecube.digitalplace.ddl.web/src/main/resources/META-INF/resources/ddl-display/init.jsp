<%@ include file="/init.jsp"%>
<%@ taglib uri="http://liferay.com/tld/ddm" prefix="liferay-ddm" %>
<%@ taglib uri="http://placecube.com/digitalplace/tld/frontend" prefix="dp-frontend" %>

<%@page import="com.liferay.dynamic.data.lists.model.DDLRecord"%>
<%@page import="com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys"%>
<%@page import="javax.portlet.WindowState"%>

<c:set var="windowStateMaximized" value="<%= WindowState.MAXIMIZED %>" />
<c:set var="windowStateNormal" value="<%= WindowState.NORMAL %>" />

<c:set var="portletNamespace">
	<portlet:namespace />
</c:set>
