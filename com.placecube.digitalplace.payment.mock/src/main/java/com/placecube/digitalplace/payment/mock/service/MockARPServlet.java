package com.placecube.digitalplace.payment.mock.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;

@Component(property = { "osgi.http.whiteboard.context.path=/", "osgi.http.whiteboard.servlet.pattern=" + "/mock-arp-payment/process-payment" }, service = Servlet.class)
public class MockARPServlet extends HttpServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Reference
	private JSONFactory jsonFactory;

	private static final Log LOG = LogFactoryUtil.getLog(MockARPServlet.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String payRequest = ParamUtil.getString(request, "payRequest");

		try {
			JSONObject payloadJSON = jsonFactory.createJSONObject(payRequest);

			String logUrl = payloadJSON.getString("logUrl");
			String returnUrl = payloadJSON.getString("returnUrl");

			HttpClient client = HttpClients.createDefault();
			HttpPost method = new HttpPost(new URI(logUrl));
			HttpResponse httpResponse = client.execute(method);

			if (HttpStatus.SC_OK != httpResponse.getStatusLine().getStatusCode()) {
				LOG.error("Call to " + logUrl + " returned response status: " + httpResponse.getStatusLine().getStatusCode());
			}

			response.sendRedirect(returnUrl);

		} catch (JSONException | URISyntaxException e) {
			LOG.error("Could not process mock arp payment request: " + e.toString());
		}

	}

}
