package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SiteInitialiserSetupContentServiceTest extends PowerMockito {

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONObject mockJSONObject1;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private SiteInitialiserSetupFragmentsService mockSiteInitialiserSetupFragmentsService;

	@InjectMocks
	private SiteInitialiserSetupContentService siteInitialiserSetupContentService;

	@Test
	public void configureAssetListEntryUsage_WhenArrayIsNull_ThenNoActionIsPerformed() throws PortalException {
		long groupId = 123;
		long userId = 2;

		siteInitialiserSetupContentService.configureAssetListEntryUsage(userId, groupId, null);

		verifyZeroInteractions(mockLayoutLocalService, mockSiteInitialiserSetupFragmentsService);
	}

	@Test
	@Parameters({ "true", "false" })
	public void configureAssetListEntryUsage_WhenNoError_ThenConfiguresTheFragmentEntryLinksInLayout(boolean isPrivateLayout) throws Exception {
		long groupId = 123;
		long userId = 2;
		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject1);
		when(mockJSONObject1.getString("layoutUrl")).thenReturn("layoutUrlValue");
		when(mockJSONObject1.getString("assetListEntryUsageKey")).thenReturn("assetListEntryUsageKeyValue");
		when(mockJSONObject1.getString("targetCollectionPlaceholderValue")).thenReturn("targetCollectionPlaceholderValueValue");
		when(mockJSONObject1.getBoolean("privateLayout")).thenReturn(isPrivateLayout);
		when(mockLayoutLocalService.getLayoutByFriendlyURL(groupId, isPrivateLayout, "layoutUrlValue")).thenReturn(mockLayout);

		siteInitialiserSetupContentService.configureAssetListEntryUsage(userId, groupId, mockJSONArray);

		verify(mockSiteInitialiserSetupFragmentsService, times(1)).updateFragmentEntryLinksInLayout(userId, mockLayout, "assetListEntryUsageKeyValue", "targetCollectionPlaceholderValueValue");
	}

}
