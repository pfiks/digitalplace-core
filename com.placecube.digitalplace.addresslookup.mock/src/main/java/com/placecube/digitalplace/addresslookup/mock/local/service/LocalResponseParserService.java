package com.placecube.digitalplace.addresslookup.mock.local.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;

@Component(immediate = true, service = LocalResponseParserService.class)
public class LocalResponseParserService {

	@Reference
	private JSONFactory jsonFactory;

	public AddressContext getAddressContext(JSONObject jsonObject, String addressLine1, String addressLine2, String addressLine3) {
		String uprn = jsonObject.getString("UPRN");
		String city = jsonObject.getString("POST_TOWN");
		String postcode = jsonObject.getString("POSTCODE");
		return new AddressContext(uprn, addressLine1, addressLine2, addressLine3, StringPool.BLANK, city, postcode);
	}

	public String getAddressLine1(String buildingDetails, String buildingNumber) {
		return Validator.isNotNull(buildingDetails) ? buildingDetails : buildingNumber;
	}

	public String getAddressLine2(String buildingDetails, String buildingNumber, String streetName) {
		return Validator.isNotNull(buildingDetails) ? buildingNumber + StringPool.SPACE + streetName : streetName;
	}

	public String getAddressLine3(JSONObject jsonObject) {
		String dependentLocality = jsonObject.getString("DEPENDENT_LOCALITY", StringPool.BLANK);
		String doubleDependentLocality = jsonObject.getString("DOUBLE_DEPENDENT_LOCALITY", StringPool.BLANK);
		return String.join(StringPool.SPACE, dependentLocality, doubleDependentLocality);
	}

	public String getBuildingDetails(JSONObject jsonObject) {
		String subBuildingName = jsonObject.getString("SUB_BUILDING_NAME", StringPool.BLANK);
		String buildingName = jsonObject.getString("BUILDING_NAME", StringPool.BLANK);
		return String.join(StringPool.SPACE, subBuildingName, buildingName);
	}

	public JSONObject getDPA(int digit, String postcode) {

		JSONObject dpa = jsonFactory.createJSONObject();
		dpa.put("SUB_BUILDING_NAME", "SBN_" + digit);
		dpa.put("BUILDING_NAME", "BN_" + digit);
		dpa.put("BUILDING_NUMBER", "BNum_" + digit);
		dpa.put("THOROUGHFARE_NAME", "TN_" + digit);
		dpa.put("DEPENDENT_THOROUGHFARE_NAME", "DTN_" + digit);
		dpa.put("DEPENDENT_LOCALITY", "DL_" + digit);
		dpa.put("DOUBLE_DEPENDENT_LOCALITY", "DDL_" + digit);
		dpa.put("UPRN", String.valueOf(digit));
		dpa.put("POST_TOWN", "T_" + digit);
		dpa.put("POSTCODE", postcode);

		JSONObject dpaResponse = jsonFactory.createJSONObject();
		dpaResponse.put("DPA", dpa);

		return dpaResponse;
	}

	public JSONArray getResults(String postcode) throws JSONException {

		JSONArray jsonArray = jsonFactory.createJSONArray();

		if (postcode.startsWith("ZZ") || postcode.startsWith("NN")) {
			return jsonArray;
		}

		String[] array = postcode.split(StringPool.SPACE);
		int number = GetterUtil.getInteger(array[1].substring(0, 1));

		for (int i = 1; i <= number && i <= 5; i++) {

			jsonArray.put(getDPA(i, postcode));

		}

		return jsonArray;
	}

	public String getStreeetName(JSONObject jsonObject) {
		String thoroughfareName = jsonObject.getString("THOROUGHFARE_NAME", StringPool.BLANK);
		String dependentThoroughfareName = jsonObject.getString("DEPENDENT_THOROUGHFARE_NAME", StringPool.BLANK);
		return String.join(StringPool.SPACE, thoroughfareName, dependentThoroughfareName);
	}

}
