package com.placecube.digitalplace.search.rankings.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.search.document.Document;

public class SearchRanking {

	private List<String> aliases;

	private List<String> blockedIds;

	private boolean disabled = false;

	private List<?> pinnedIds;

	private SearchRanking(Document document) {
		aliases = document.getStrings("queryStrings");
		blockedIds = document.getStrings("blocks");
		disabled = GetterUtil.getBoolean(document.getString("inactive"), false);
		pinnedIds = document.getValues("pins");
	}

	public static SearchRanking fromSearchDocument(Document document) {
		return new SearchRanking(document);
	}

	public List<String> getAliases() {
		return aliases;
	}

	public List<String> getBlockedIds() {
		return blockedIds;
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getPinnedIds() {
		return ListUtil.isNotEmpty(pinnedIds) ? ((List<Map<String, String>>) pinnedIds) : new ArrayList<>();
	}

	public boolean isDisabled() {
		return disabled;
	}

}
