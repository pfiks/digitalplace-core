package com.placecube.digitalplace.search.web.portlet.sortby.indexer;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.DocumentContributor;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.web.portlet.contenttype.constants.ContentTypeConstants;
import com.placecube.digitalplace.search.web.portlet.sortby.service.SortByService;

@SuppressWarnings("rawtypes")
@Component(immediate = true, service = DocumentContributor.class)
public class SortableTitleModelDocumentContributor implements DocumentContributor {

	@Reference
	private SortByService sortByService;

	@Override
	public void contribute(Document document, BaseModel baseModel) {
		String sortableTitle = document.get(Field.getSortableFieldName(Field.TITLE));

		if (Validator.isNull(sortableTitle)) {

			String className = document.get(Field.ENTRY_CLASS_NAME);

			if (ContentTypeConstants.CALENDAR_BOOKING_CLASS_NAME.equalsIgnoreCase(className)) {
				document.addKeyword(Field.TITLE, sortByService.getTitleForCalendarBooking(document));

			} else if (ContentTypeConstants.MB_MESSAGE_CLASS_NAME.equalsIgnoreCase(className)) {
				document.addKeyword(Field.TITLE, sortByService.getTitleForMBMessage(document));
			}

		}

	}

}