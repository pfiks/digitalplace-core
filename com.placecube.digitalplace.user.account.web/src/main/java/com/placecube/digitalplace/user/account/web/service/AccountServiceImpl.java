package com.placecube.digitalplace.user.account.web.service;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.entry.rel.service.AssetEntryAssetCategoryRelLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserConstants;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.common.permission.PermissionUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.address.service.AddressTypeConfigurationService;
import com.placecube.digitalplace.user.account.constants.PhoneType;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountDateOfBirthService;
import com.placecube.digitalplace.user.account.service.AccountEmailAddressService;
import com.placecube.digitalplace.user.account.service.AccountPhoneService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;

@Component(immediate = true, service = AccountService.class)
public class AccountServiceImpl implements AccountService {

	private static final String ACCOUNT_CREATED_WITH_ERRORS = "accountCreatedWithErrors";

	private static final Log LOG = LogFactoryUtil.getLog(AccountServiceImpl.class);

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private AccountDateOfBirthService accountDateOfBirthService;

	@Reference
	private AccountEmailAddressService accountEmailAddressService;

	@Reference
	private AccountPhoneService accountPhoneService;

	@Reference
	private AddressLookupService addressLookupService;

	@Reference
	private AddressTypeConfigurationService addressTypeConfigurationService;

	@Reference
	private AssetEntryAssetCategoryRelLocalService assetCategoryRelLocalService;

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private PermissionUtil permissionUtil;

	@Reference
	private UserAccountFieldRegistry userAccountFieldRegistry;

	@Reference
	private UserGroupRoleLocalService userGroupRoleLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void checkAccountCreationEnabled(PortletRequest portletRequest) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		if (!themeDisplay.getCompany().isStrangers()) {
			throw new PortletException();
		}
	}

	@Override
	public void createUserAccount(PortletRequest portletRequest, AccountContext accountContext, boolean fallbackToNationalLookup) throws UserAccountException {
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Optional<Long> emailVerifiedPlid = accountEmailAddressService.getEmailAddressVerifiedLayoutPlid(accountContext);

		serviceContext.setPlid(emailVerifiedPlid.orElse(LayoutConstants.DEFAULT_PLID));

		serviceContext.setAssetCategoryIds(accountContext.getCategoryIds());

		LocalDate dateOfBirth = accountDateOfBirthService.getDateOfBirthFromContext(accountContext);

		accountContextService.configureAutoGeneratedEmailDetails(accountContext);

		addCustomFieldsToContext(accountContext, serviceContext);

		runAsCompanyAdmin(serviceContext);
		try {
			User user = userLocalService.addUserWithWorkflow(0, accountContext.getCompanyId(), accountContext.isSkipPasswordStep(), accountContext.getPassword1(), accountContext.getPassword2(), true,
					null, accountContext.getEmailAddress(), serviceContext.getLocale(), accountContext.getFirstName(), null, accountContext.getLastName(), 0, 0, true, dateOfBirth.getMonthValue() - 1,
					dateOfBirth.getDayOfMonth(), dateOfBirth.getYear(), accountContext.getJobTitle(), UserConstants.TYPE_REGULAR, new long[0], new long[0], new long[0],
					accountContext.getUserGroupIds(), accountContext.sendEmail(), serviceContext);

			performPostCreateUpdateTasks(portletRequest, accountContext, fallbackToNationalLookup, serviceContext, user);

			permissionUtil.runAsUser(serviceContext.getUserId());
		} catch (Exception e) {
			throw new UserAccountException(e);
		}
	}

	@Override
	public void updateUserAccount(PortletRequest portletRequest, AccountContext accountContext, boolean fallbackToNationalLookup) throws UserAccountException {
		try {
			ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

			LocalDate dateOfBirth = accountDateOfBirthService.getDateOfBirthFromContext(accountContext);

			User user = userLocalService.getUserById(accountContext.getUserId());
			Contact contact = user.getContact();
			String oldPassword = user.getPassword();

			List<UserGroupRole> userGroupRoles = userGroupRoleLocalService.getUserGroupRoles(user.getUserId());

			if (Validator.isNull(accountContext.getEmailAddress())) {
				accountContextService.configureAutoGeneratedEmailDetails(accountContext);
			}

			if (accountContext.isUpdateUserCategories()) {
				configureAssetCategoriesInServiceContext(serviceContext, accountContext, user.getUserId());
			}

			addCustomFieldsToContext(accountContext, serviceContext);

			runAsCompanyAdmin(serviceContext);

			user = userLocalService.updateUser(user.getUserId(), oldPassword, accountContext.getPassword1(), accountContext.getPassword2(), user.getPasswordReset(), user.getReminderQueryQuestion(),
					user.getReminderQueryAnswer(), user.getScreenName(), accountContext.getEmailAddress(), false, null, user.getLanguageId(), user.getTimeZoneId(), user.getGreeting(),
					user.getComments(), accountContext.getFirstName(), user.getMiddleName(), accountContext.getLastName(), contact.getPrefixListTypeId(), contact.getSuffixListTypeId(), user.getMale(),
					dateOfBirth.getMonthValue() - 1, dateOfBirth.getDayOfMonth(), dateOfBirth.getYear(), contact.getSmsSn(), contact.getFacebookSn(), contact.getJabberSn(), contact.getSkypeSn(),
					contact.getTwitterSn(), accountContext.getJobTitle(), user.getGroupIds(), user.getOrganizationIds(), user.getRoleIds(), userGroupRoles, user.getUserGroupIds(), serviceContext);

			performPostCreateUpdateTasks(portletRequest, accountContext, fallbackToNationalLookup, serviceContext, user);
		} catch (PortalException e) {
			throw new UserAccountException(e);
		}
	}

	private void addCustomFieldsToContext(AccountContext accountContext, ServiceContext serviceContext) {
		Map<String, Serializable> customFields = serviceContext.getExpandoBridgeAttributes();
		customFields.putAll(accountContext.getExpandoFields());
		serviceContext.setExpandoBridgeAttributes(customFields);
	}

	private void configureAddress(PortletRequest portletRequest, AccountContext accountContext, boolean fallbackToNationalLookup, ServiceContext serviceContext, User user) {
		try {
			long companyId = serviceContext.getCompanyId();
			Optional<AddressContext> addressContextOpt = addressLookupService.getByUprn(companyId, accountContext.getUprn(), new String[0], fallbackToNationalLookup);
			boolean isNoFixedAbode = isNoFixedAbode(accountContext);

			AddressContext addressContext = null;
			if (!isNoFixedAbode && addressContextOpt.isPresent()) {
				addressContext = addressContextOpt.get();
			} else if (isNoFixedAbode && !isAlreadyNoCurrentAddress(accountContext)) {
				addressContext = addressLookupService.createAddressContext("", AccountConstants.NO_CURRENT_ADDRESS, "", "", AccountConstants.NO_CURRENT_ADDRESS, "", "");
			}
			if (Validator.isNotNull(addressContext)) {
				addressContext.setAddressType(addressTypeConfigurationService.getDefaultAddressType(companyId));
				addressLookupService.saveAddress(user, addressContext, serviceContext);
			}
		} catch (Exception e) {
			LOG.warn("Exception configuring user address - " + e.getMessage());
			SessionErrors.add(portletRequest, ACCOUNT_CREATED_WITH_ERRORS);
		}
	}

	private void configureAssetCategoriesInServiceContext(ServiceContext serviceContext, AccountContext accountContext, long userId) {
		AssetEntry assetEntry = assetEntryLocalService.fetchEntry(User.class.getName(), userId);
		if (Validator.isNotNull(assetEntry)) {
			for (AssetCategory assetCategory : assetEntry.getCategories()) {
				assetCategoryRelLocalService.deleteAssetEntryAssetCategoryRel(assetEntry.getEntryId(), assetCategory.getCategoryId());
			}
		}
		serviceContext.setAssetCategoryIds(accountContext.getCategoryIds());
	}

	private void configureCreateUserAccountFields(PortletRequest portletRequest, AccountContext accountContext, User user) {
		for (UserAccountField createUserAccountField : userAccountFieldRegistry.getUserAccountFields(accountContext.getCompanyId())) {
			try {
				ExpandoBridge expandoBridge = user.getExpandoBridge();

				expandoBridge.setAttribute(createUserAccountField.getExpandoFieldName(), accountContext.getExpandoValue(createUserAccountField.getExpandoFieldName()), false);
			} catch (Exception e) {
				LOG.warn("Exception configuring CreateUserAccountField: " + createUserAccountField.getClass().getSimpleName() + " - " + e.getMessage());
				SessionErrors.add(portletRequest, ACCOUNT_CREATED_WITH_ERRORS);
			}
		}
	}

	private void configurePhoneNumber(PortletRequest portletRequest, String phoneNumber, PhoneType type, User user, ServiceContext serviceContext) {
		try {

			accountPhoneService.addPhone(user, phoneNumber, type, serviceContext);

		} catch (Exception e) {
			LOG.warn("Exception configuring user phone number - " + e.getMessage());
			SessionErrors.add(portletRequest, ACCOUNT_CREATED_WITH_ERRORS);
		}
	}

	private boolean isAlreadyNoCurrentAddress(AccountContext accountContext) {
		return Validator.isNotNull(accountContext.getFullAddress()) ? accountContext.getFullAddress().contains(AccountConstants.NO_CURRENT_ADDRESS) : false;
	}

	private boolean isNoFixedAbode(AccountContext accountContext) {
		String expandoValue = GetterUtil.getString(accountContext.getExpandoValue("address-info-status"), StringPool.BLANK);
		return "no-fixed-abode".equalsIgnoreCase(expandoValue);
	}

	private void performPostCreateUpdateTasks(PortletRequest portletRequest, AccountContext accountContext, boolean fallbackToNationalLookup, ServiceContext serviceContext, User user) {

		accountContext.setUserId(user.getUserId());
		configureAddress(portletRequest, accountContext, fallbackToNationalLookup, serviceContext, user);
		configurePhoneNumber(portletRequest, accountContext.getBusinessPhoneNumber(), PhoneType.BUSINESS, user, serviceContext);
		configurePhoneNumber(portletRequest, accountContext.getMobilePhoneNumber(), PhoneType.MOBILE, user, serviceContext);
		configurePhoneNumber(portletRequest, accountContext.getHomePhoneNumber(), PhoneType.PERSONAL, user, serviceContext);
		configureCreateUserAccountFields(portletRequest, accountContext, user);
	}

	private void runAsCompanyAdmin(ServiceContext serviceContext) throws UserAccountException {

		try {
			permissionUtil.runAsCompanyAdmin(serviceContext.getCompanyId());
		} catch (Exception e) {
			throw new UserAccountException(e);
		}
	}
}
