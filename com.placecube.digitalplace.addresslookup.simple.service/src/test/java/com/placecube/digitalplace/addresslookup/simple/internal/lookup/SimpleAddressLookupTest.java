package com.placecube.digitalplace.addresslookup.simple.internal.lookup;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.simple.internal.lookup.configuration.SimpleAddressLookupCompanyConfiguration;
import com.placecube.digitalplace.addresslookup.simple.internal.lookup.service.SimpleAddressLookupParsingService;
import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;
import com.placecube.digitalplace.addresslookup.simple.service.SimpleAddressLookupEntryLocalService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SimpleAddressLookupTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@Mock
	private AddressContext mockAddressContext1;

	@Mock
	private AddressContext mockAddressContext2;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private SimpleAddressLookupCompanyConfiguration mockSimpleAddressLookupCompanyConfiguration;

	@Mock
	private SimpleAddressLookupEntry mockSimpleAddressLookupEntry1;

	@Mock
	private SimpleAddressLookupEntryLocalService mockSimpleAddressLookupEntryLocalService;

	@Mock
	private SimpleAddressLookupParsingService mockSimpleAddressLookupParsingService;

	@InjectMocks
	private SimpleAddressLookup simpleAddressLookup;

	@Test
	public void enabled_WhenExceptionRetrievingTheConfiguration_ThenReturnsFalse() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(SimpleAddressLookupCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = simpleAddressLookup.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(SimpleAddressLookupCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockSimpleAddressLookupCompanyConfiguration);
		when(mockSimpleAddressLookupCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = simpleAddressLookup.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void findByPostcode_WhenNoError_ThenReturnsTheEntriesFoundForThePostcodeParsedAsAddressContexts() {
		String postcode = "postcodevalue";
		List<SimpleAddressLookupEntry> entries = Collections.singletonList(mockSimpleAddressLookupEntry1);
		when(mockSimpleAddressLookupEntryLocalService.getByCompanyIdAndPostcode(COMPANY_ID, postcode)).thenReturn(entries);
		Set<AddressContext> expected = new HashSet<>();
		expected.add(mockAddressContext1);
		expected.add(mockAddressContext2);
		when(mockSimpleAddressLookupParsingService.getAddressContexts(entries)).thenReturn(expected);

		Set<AddressContext> results = simpleAddressLookup.findByPostcode(COMPANY_ID, postcode, null);

		assertThat(results, sameInstance(expected));
	}

	@Test
	public void findByUprn_WhenEntryFound_ThenReturnsOptioanlWithTheAddressContextForTheEntry() {
		String uprn = "123";
		when(mockSimpleAddressLookupEntryLocalService.getByCompanyIdAndUprn(COMPANY_ID, uprn)).thenReturn(Optional.of(mockSimpleAddressLookupEntry1));
		when(mockSimpleAddressLookupParsingService.getAddressContext(mockSimpleAddressLookupEntry1)).thenReturn(mockAddressContext1);

		Optional<AddressContext> result = simpleAddressLookup.findByUprn(COMPANY_ID, uprn, null);

		assertThat(result.get(), sameInstance(mockAddressContext1));
	}

	@Test
	public void findByUprn_WhenNoEntryFound_ThenReturnsEmptyOptioanl() {
		String uprn = "123";
		when(mockSimpleAddressLookupEntryLocalService.getByCompanyIdAndUprn(COMPANY_ID, uprn)).thenReturn(Optional.empty());

		Optional<AddressContext> result = simpleAddressLookup.findByUprn(COMPANY_ID, uprn, null);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getWeight_WhenExceptionRetrievingTheConfiguration_ThenReturnsZero() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(SimpleAddressLookupCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		int result = simpleAddressLookup.getWeight(COMPANY_ID);

		assertThat(result, equalTo(0));
	}

	@Test
	public void getWeight_WhenNoError_ThenReturnsIfTheConfigurationTheWeight() throws ConfigurationException {
		int expected = 5;
		when(mockConfigurationProvider.getCompanyConfiguration(SimpleAddressLookupCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockSimpleAddressLookupCompanyConfiguration);
		when(mockSimpleAddressLookupCompanyConfiguration.weight()).thenReturn(expected);

		int result = simpleAddressLookup.getWeight(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void national_WhenExceptionRetrievingTheConfiguration_ThenReturnsFalse() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(SimpleAddressLookupCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = simpleAddressLookup.national(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void national_WhenNoError_ThenReturnsIfTheConfigurationIsNational(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(SimpleAddressLookupCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockSimpleAddressLookupCompanyConfiguration);
		when(mockSimpleAddressLookupCompanyConfiguration.national()).thenReturn(expected);

		boolean result = simpleAddressLookup.national(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

}
