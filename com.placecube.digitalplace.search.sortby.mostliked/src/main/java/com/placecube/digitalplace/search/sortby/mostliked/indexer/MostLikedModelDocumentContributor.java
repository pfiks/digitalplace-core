package com.placecube.digitalplace.search.sortby.mostliked.indexer;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.DocumentContributor;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.search.sortby.mostliked.constants.MostLikedConstants;
import com.placecube.digitalplace.search.sortby.mostliked.service.MostLikedService;

@Component(immediate = true, service = DocumentContributor.class)
public class MostLikedModelDocumentContributor implements DocumentContributor {

	@Reference
	private MostLikedService mostLikedService;

	@Override
	public void contribute(Document document, BaseModel baseModel) {
		String className = document.get(Field.ENTRY_CLASS_NAME);
		long classPK = GetterUtil.getLong(document.get(Field.ENTRY_CLASS_PK));

		document.addKeyword(MostLikedConstants.COUNTER_FIELD_NAME, mostLikedService.getScore(className, classPK));
	}

}