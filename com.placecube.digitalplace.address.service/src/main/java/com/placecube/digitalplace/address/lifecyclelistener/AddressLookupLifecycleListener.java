package com.placecube.digitalplace.address.lifecyclelistener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.module.framework.ModuleServiceLifecycle;
import com.pfiks.expando.creator.ExpandoColumnCreator;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class AddressLookupLifecycleListener extends BasePortalInstanceLifecycleListener {

	private static final Log LOG = LogFactoryUtil.getLog(AddressLookupLifecycleListener.class);

	@Reference(target = "(expandocolumn.creator=user.uprn)")
	private ExpandoColumnCreator expandoColumnCreator;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		LOG.debug("Initialising address service for companyId: " + company.getCompanyId());

		expandoColumnCreator.create(company);
	}

	@Reference(target = ModuleServiceLifecycle.PORTAL_INITIALIZED, unbind = "-")
	protected void setModuleServiceLifecycle(ModuleServiceLifecycle moduleServiceLifecycle) {
		return;
	}

}
