package com.placecube.digitalplace.appointment.admin.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.appointment.admin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.admin.web.constants.PortletKeys;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.APPOINTMENTS_MANAGER, "mvc.command.name=" + MVCCommandKeys.CANCEL_BOOKED_APPOINTMENT }, service = MVCActionCommand.class)
public class CancelBookedAppointmentMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private AppointmentBookingService appointmentBookingService;

	@Reference
	private AppointmentsManagerService appointmentsManagerService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		AppointmentBookingEntry appointmentEntry = appointmentsManagerService.getAppointmentBookingEntryFromRequest(actionRequest);

		appointmentBookingService.cancelAppointment(ParamUtil.getLong(actionRequest, "companyId"), ParamUtil.getString(actionRequest, "serviceId", StringPool.BLANK), appointmentEntry);

		actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_BOOKED_APPOINTMENTS);
	}

}
