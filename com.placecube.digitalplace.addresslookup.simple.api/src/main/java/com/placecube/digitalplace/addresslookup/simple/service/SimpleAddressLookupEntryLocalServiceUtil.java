/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for SimpleAddressLookupEntry. This utility wraps
 * <code>com.placecube.digitalplace.addresslookup.simple.service.impl.SimpleAddressLookupEntryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see SimpleAddressLookupEntryLocalService
 * @generated
 */
public class SimpleAddressLookupEntryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.addresslookup.simple.service.impl.SimpleAddressLookupEntryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the simple address lookup entry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 * @return the simple address lookup entry that was added
	 */
	public static SimpleAddressLookupEntry addSimpleAddressLookupEntry(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		return getService().addSimpleAddressLookupEntry(
			simpleAddressLookupEntry);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new simple address lookup entry with the primary key. Does not add the simple address lookup entry to the database.
	 *
	 * @param simpleAddressLookupEntryPK the primary key for the new simple address lookup entry
	 * @return the new simple address lookup entry
	 */
	public static SimpleAddressLookupEntry createSimpleAddressLookupEntry(
		com.placecube.digitalplace.addresslookup.simple.service.persistence.
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK) {

		return getService().createSimpleAddressLookupEntry(
			simpleAddressLookupEntryPK);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the simple address lookup entry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 */
	public static SimpleAddressLookupEntry deleteSimpleAddressLookupEntry(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		return getService().deleteSimpleAddressLookupEntry(
			simpleAddressLookupEntry);
	}

	/**
	 * Deletes the simple address lookup entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 * @throws PortalException if a simple address lookup entry with the primary key could not be found
	 */
	public static SimpleAddressLookupEntry deleteSimpleAddressLookupEntry(
			com.placecube.digitalplace.addresslookup.simple.service.persistence.
				SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws PortalException {

		return getService().deleteSimpleAddressLookupEntry(
			simpleAddressLookupEntryPK);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static SimpleAddressLookupEntry fetchSimpleAddressLookupEntry(
		com.placecube.digitalplace.addresslookup.simple.service.persistence.
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK) {

		return getService().fetchSimpleAddressLookupEntry(
			simpleAddressLookupEntryPK);
	}

	/**
	 * Returns the simple address lookup entry with the matching UUID and company.
	 *
	 * @param uuid the simple address lookup entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry
		fetchSimpleAddressLookupEntryByUuidAndCompanyId(
			String uuid, long companyId) {

		return getService().fetchSimpleAddressLookupEntryByUuidAndCompanyId(
			uuid, companyId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static List<SimpleAddressLookupEntry> getByCompanyIdAndPostcode(
		long companyId, String postcode) {

		return getService().getByCompanyIdAndPostcode(companyId, postcode);
	}

	public static java.util.Optional<SimpleAddressLookupEntry>
		getByCompanyIdAndUprn(long companyId, String uprn) {

		return getService().getByCompanyIdAndUprn(companyId, uprn);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> getSimpleAddressLookupEntries(
		int start, int end) {

		return getService().getSimpleAddressLookupEntries(start, end);
	}

	/**
	 * Returns the number of simple address lookup entries.
	 *
	 * @return the number of simple address lookup entries
	 */
	public static int getSimpleAddressLookupEntriesCount() {
		return getService().getSimpleAddressLookupEntriesCount();
	}

	/**
	 * Returns the simple address lookup entry with the primary key.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry
	 * @throws PortalException if a simple address lookup entry with the primary key could not be found
	 */
	public static SimpleAddressLookupEntry getSimpleAddressLookupEntry(
			com.placecube.digitalplace.addresslookup.simple.service.persistence.
				SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws PortalException {

		return getService().getSimpleAddressLookupEntry(
			simpleAddressLookupEntryPK);
	}

	/**
	 * Returns the simple address lookup entry with the matching UUID and company.
	 *
	 * @param uuid the simple address lookup entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching simple address lookup entry
	 * @throws PortalException if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry
			getSimpleAddressLookupEntryByUuidAndCompanyId(
				String uuid, long companyId)
		throws PortalException {

		return getService().getSimpleAddressLookupEntryByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Updates the simple address lookup entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect SimpleAddressLookupEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 * @return the simple address lookup entry that was updated
	 */
	public static SimpleAddressLookupEntry updateSimpleAddressLookupEntry(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		return getService().updateSimpleAddressLookupEntry(
			simpleAddressLookupEntry);
	}

	public static SimpleAddressLookupEntryLocalService getService() {
		return _service;
	}

	public static void setService(
		SimpleAddressLookupEntryLocalService service) {

		_service = service;
	}

	private static volatile SimpleAddressLookupEntryLocalService _service;

}