/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AppointmentSlot}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentSlot
 * @generated
 */
public class AppointmentSlotWrapper
	extends BaseModelWrapper<AppointmentSlot>
	implements AppointmentSlot, ModelWrapper<AppointmentSlot> {

	public AppointmentSlotWrapper(AppointmentSlot appointmentSlot) {
		super(appointmentSlot);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("appointmentSlotId", getAppointmentSlotId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("slotType", getSlotType());
		attributes.put("name", getName());
		attributes.put("serviceId", getServiceId());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("slotStartTimeHour", getSlotStartTimeHour());
		attributes.put("slotStartTimeMinute", getSlotStartTimeMinute());
		attributes.put("slotEndTimeHour", getSlotEndTimeHour());
		attributes.put("slotEndTimeMinute", getSlotEndTimeMinute());
		attributes.put("appointmentDuration", getAppointmentDuration());
		attributes.put("daysOfWeekBitwise", getDaysOfWeekBitwise());
		attributes.put("maxAppointments", getMaxAppointments());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long appointmentSlotId = (Long)attributes.get("appointmentSlotId");

		if (appointmentSlotId != null) {
			setAppointmentSlotId(appointmentSlotId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String slotType = (String)attributes.get("slotType");

		if (slotType != null) {
			setSlotType(slotType);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String serviceId = (String)attributes.get("serviceId");

		if (serviceId != null) {
			setServiceId(serviceId);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		Integer slotStartTimeHour = (Integer)attributes.get(
			"slotStartTimeHour");

		if (slotStartTimeHour != null) {
			setSlotStartTimeHour(slotStartTimeHour);
		}

		Integer slotStartTimeMinute = (Integer)attributes.get(
			"slotStartTimeMinute");

		if (slotStartTimeMinute != null) {
			setSlotStartTimeMinute(slotStartTimeMinute);
		}

		Integer slotEndTimeHour = (Integer)attributes.get("slotEndTimeHour");

		if (slotEndTimeHour != null) {
			setSlotEndTimeHour(slotEndTimeHour);
		}

		Integer slotEndTimeMinute = (Integer)attributes.get(
			"slotEndTimeMinute");

		if (slotEndTimeMinute != null) {
			setSlotEndTimeMinute(slotEndTimeMinute);
		}

		Integer appointmentDuration = (Integer)attributes.get(
			"appointmentDuration");

		if (appointmentDuration != null) {
			setAppointmentDuration(appointmentDuration);
		}

		Long daysOfWeekBitwise = (Long)attributes.get("daysOfWeekBitwise");

		if (daysOfWeekBitwise != null) {
			setDaysOfWeekBitwise(daysOfWeekBitwise);
		}

		Integer maxAppointments = (Integer)attributes.get("maxAppointments");

		if (maxAppointments != null) {
			setMaxAppointments(maxAppointments);
		}
	}

	@Override
	public AppointmentSlot cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the appointment duration of this appointment slot.
	 *
	 * @return the appointment duration of this appointment slot
	 */
	@Override
	public int getAppointmentDuration() {
		return model.getAppointmentDuration();
	}

	/**
	 * Returns the appointment slot ID of this appointment slot.
	 *
	 * @return the appointment slot ID of this appointment slot
	 */
	@Override
	public long getAppointmentSlotId() {
		return model.getAppointmentSlotId();
	}

	/**
	 * Returns the company ID of this appointment slot.
	 *
	 * @return the company ID of this appointment slot
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this appointment slot.
	 *
	 * @return the create date of this appointment slot
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public Date getDateWithEndTime(java.time.LocalDate localDate) {
		return model.getDateWithEndTime(localDate);
	}

	@Override
	public Date getDateWithStartTime(java.time.LocalDate localDate) {
		return model.getDateWithStartTime(localDate);
	}

	@Override
	public java.util.Set<java.time.DayOfWeek> getDaysOfWeek() {
		return model.getDaysOfWeek();
	}

	/**
	 * Returns the days of week bitwise of this appointment slot.
	 *
	 * @return the days of week bitwise of this appointment slot
	 */
	@Override
	public long getDaysOfWeekBitwise() {
		return model.getDaysOfWeekBitwise();
	}

	/**
	 * Returns the end date of this appointment slot.
	 *
	 * @return the end date of this appointment slot
	 */
	@Override
	public Date getEndDate() {
		return model.getEndDate();
	}

	/**
	 * Returns the max appointments of this appointment slot.
	 *
	 * @return the max appointments of this appointment slot
	 */
	@Override
	public int getMaxAppointments() {
		return model.getMaxAppointments();
	}

	/**
	 * Returns the modified date of this appointment slot.
	 *
	 * @return the modified date of this appointment slot
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the name of this appointment slot.
	 *
	 * @return the name of this appointment slot
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the primary key of this appointment slot.
	 *
	 * @return the primary key of this appointment slot
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the service ID of this appointment slot.
	 *
	 * @return the service ID of this appointment slot
	 */
	@Override
	public String getServiceId() {
		return model.getServiceId();
	}

	/**
	 * Returns the slot end time hour of this appointment slot.
	 *
	 * @return the slot end time hour of this appointment slot
	 */
	@Override
	public int getSlotEndTimeHour() {
		return model.getSlotEndTimeHour();
	}

	/**
	 * Returns the slot end time minute of this appointment slot.
	 *
	 * @return the slot end time minute of this appointment slot
	 */
	@Override
	public int getSlotEndTimeMinute() {
		return model.getSlotEndTimeMinute();
	}

	/**
	 * Returns the slot start time hour of this appointment slot.
	 *
	 * @return the slot start time hour of this appointment slot
	 */
	@Override
	public int getSlotStartTimeHour() {
		return model.getSlotStartTimeHour();
	}

	/**
	 * Returns the slot start time minute of this appointment slot.
	 *
	 * @return the slot start time minute of this appointment slot
	 */
	@Override
	public int getSlotStartTimeMinute() {
		return model.getSlotStartTimeMinute();
	}

	/**
	 * Returns the slot type of this appointment slot.
	 *
	 * @return the slot type of this appointment slot
	 */
	@Override
	public String getSlotType() {
		return model.getSlotType();
	}

	/**
	 * Returns the start date of this appointment slot.
	 *
	 * @return the start date of this appointment slot
	 */
	@Override
	public Date getStartDate() {
		return model.getStartDate();
	}

	/**
	 * Returns the user ID of this appointment slot.
	 *
	 * @return the user ID of this appointment slot
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this appointment slot.
	 *
	 * @return the user name of this appointment slot
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this appointment slot.
	 *
	 * @return the user uuid of this appointment slot
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this appointment slot.
	 *
	 * @return the uuid of this appointment slot
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the appointment duration of this appointment slot.
	 *
	 * @param appointmentDuration the appointment duration of this appointment slot
	 */
	@Override
	public void setAppointmentDuration(int appointmentDuration) {
		model.setAppointmentDuration(appointmentDuration);
	}

	/**
	 * Sets the appointment slot ID of this appointment slot.
	 *
	 * @param appointmentSlotId the appointment slot ID of this appointment slot
	 */
	@Override
	public void setAppointmentSlotId(long appointmentSlotId) {
		model.setAppointmentSlotId(appointmentSlotId);
	}

	/**
	 * Sets the company ID of this appointment slot.
	 *
	 * @param companyId the company ID of this appointment slot
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this appointment slot.
	 *
	 * @param createDate the create date of this appointment slot
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the days of week bitwise of this appointment slot.
	 *
	 * @param daysOfWeekBitwise the days of week bitwise of this appointment slot
	 */
	@Override
	public void setDaysOfWeekBitwise(long daysOfWeekBitwise) {
		model.setDaysOfWeekBitwise(daysOfWeekBitwise);
	}

	/**
	 * Sets the end date of this appointment slot.
	 *
	 * @param endDate the end date of this appointment slot
	 */
	@Override
	public void setEndDate(Date endDate) {
		model.setEndDate(endDate);
	}

	/**
	 * Sets the max appointments of this appointment slot.
	 *
	 * @param maxAppointments the max appointments of this appointment slot
	 */
	@Override
	public void setMaxAppointments(int maxAppointments) {
		model.setMaxAppointments(maxAppointments);
	}

	/**
	 * Sets the modified date of this appointment slot.
	 *
	 * @param modifiedDate the modified date of this appointment slot
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the name of this appointment slot.
	 *
	 * @param name the name of this appointment slot
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the primary key of this appointment slot.
	 *
	 * @param primaryKey the primary key of this appointment slot
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the service ID of this appointment slot.
	 *
	 * @param serviceId the service ID of this appointment slot
	 */
	@Override
	public void setServiceId(String serviceId) {
		model.setServiceId(serviceId);
	}

	/**
	 * Sets the slot end time hour of this appointment slot.
	 *
	 * @param slotEndTimeHour the slot end time hour of this appointment slot
	 */
	@Override
	public void setSlotEndTimeHour(int slotEndTimeHour) {
		model.setSlotEndTimeHour(slotEndTimeHour);
	}

	/**
	 * Sets the slot end time minute of this appointment slot.
	 *
	 * @param slotEndTimeMinute the slot end time minute of this appointment slot
	 */
	@Override
	public void setSlotEndTimeMinute(int slotEndTimeMinute) {
		model.setSlotEndTimeMinute(slotEndTimeMinute);
	}

	/**
	 * Sets the slot start time hour of this appointment slot.
	 *
	 * @param slotStartTimeHour the slot start time hour of this appointment slot
	 */
	@Override
	public void setSlotStartTimeHour(int slotStartTimeHour) {
		model.setSlotStartTimeHour(slotStartTimeHour);
	}

	/**
	 * Sets the slot start time minute of this appointment slot.
	 *
	 * @param slotStartTimeMinute the slot start time minute of this appointment slot
	 */
	@Override
	public void setSlotStartTimeMinute(int slotStartTimeMinute) {
		model.setSlotStartTimeMinute(slotStartTimeMinute);
	}

	/**
	 * Sets the slot type of this appointment slot.
	 *
	 * @param slotType the slot type of this appointment slot
	 */
	@Override
	public void setSlotType(String slotType) {
		model.setSlotType(slotType);
	}

	/**
	 * Sets the start date of this appointment slot.
	 *
	 * @param startDate the start date of this appointment slot
	 */
	@Override
	public void setStartDate(Date startDate) {
		model.setStartDate(startDate);
	}

	/**
	 * Sets the user ID of this appointment slot.
	 *
	 * @param userId the user ID of this appointment slot
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this appointment slot.
	 *
	 * @param userName the user name of this appointment slot
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this appointment slot.
	 *
	 * @param userUuid the user uuid of this appointment slot
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this appointment slot.
	 *
	 * @param uuid the uuid of this appointment slot
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected AppointmentSlotWrapper wrap(AppointmentSlot appointmentSlot) {
		return new AppointmentSlotWrapper(appointmentSlot);
	}

}