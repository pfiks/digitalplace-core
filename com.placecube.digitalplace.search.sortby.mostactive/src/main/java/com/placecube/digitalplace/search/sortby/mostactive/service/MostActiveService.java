package com.placecube.digitalplace.search.sortby.mostactive.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.search.index.UpdateDocumentIndexWriter;
import com.liferay.portal.search.indexer.BaseModelDocumentFactory;
import com.liferay.social.kernel.model.SocialActivityConstants;
import com.liferay.social.kernel.service.SocialActivityLocalService;
import com.placecube.digitalplace.search.sortby.mostactive.constants.MostActiveConstants;

@Component(immediate = true, service = MostActiveService.class)
public class MostActiveService {

	@Reference
	private BaseModelDocumentFactory baseModelDocumentFactory;

	@Reference
	private SocialActivityLocalService socialActivityLocalService;

	@Reference
	private UpdateDocumentIndexWriter updateDocumentIndexWriter;

	public long getActivitiesCount(Group group) {
		DynamicQuery dynamicQuery = getBaseDynamicQuery(group.getCompanyId());
		dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", group.getGroupId()));

		return socialActivityLocalService.dynamicQueryCount(dynamicQuery);
	}

	public long getActivitiesCount(User user) {
		DynamicQuery dynamicQuery = getBaseDynamicQuery(user.getCompanyId());
		dynamicQuery.add(RestrictionsFactoryUtil.eq("userId", user.getUserId()));

		return socialActivityLocalService.dynamicQueryCount(dynamicQuery);
	}

	public void reindexSocialActivityCount(Group group, long count, boolean isCommitImmediately) {
		Document document = baseModelDocumentFactory.createDocument(group);

		document.addKeyword(MostActiveConstants.COUNTER_FIELD_NAME, count);

		updateDocumentIndexWriter.updateDocumentPartially(group.getCompanyId(), document, isCommitImmediately);
	}

	public void reindexSocialActivityCount(User user, long count, boolean isCommitImmediately) {
		Document document = baseModelDocumentFactory.createDocument(user);

		document.addKeyword(MostActiveConstants.COUNTER_FIELD_NAME, count);

		updateDocumentIndexWriter.updateDocumentPartially(user.getCompanyId(), document, isCommitImmediately);
	}

	private DynamicQuery getBaseDynamicQuery(long companyId) {
		Instant date = Instant.now().minus(MostActiveConstants.DAYS_DELTA, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS);
		long nowMinusDays = date.toEpochMilli();

		DynamicQuery dynamicQuery = socialActivityLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("mirrorActivityId", 0L));
		dynamicQuery.add(RestrictionsFactoryUtil.gt("createDate", nowMinusDays));
		dynamicQuery.add(RestrictionsFactoryUtil.ne("type", SocialActivityConstants.TYPE_RESTORE_FROM_TRASH));
		dynamicQuery.add(RestrictionsFactoryUtil.ne("type", SocialActivityConstants.TYPE_MOVE_TO_TRASH));
		return dynamicQuery;
	}

}