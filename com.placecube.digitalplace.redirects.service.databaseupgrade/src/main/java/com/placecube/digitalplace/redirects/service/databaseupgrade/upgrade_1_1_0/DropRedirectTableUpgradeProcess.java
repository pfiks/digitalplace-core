package com.placecube.digitalplace.redirects.service.databaseupgrade.upgrade_1_1_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class DropRedirectTableUpgradeProcess extends UpgradeProcess {

    @Override
    protected void doUpgrade() throws Exception {
        runSQLTemplateString("drop table Redirect", false);
    }

}
