create table Placecube_AddressLookupSimple_SimpleAddressLookupEntry (
	uuid_ VARCHAR(75) null,
	companyId LONG not null,
	uprn VARCHAR(75) not null,
	addressLine1 STRING null,
	addressLine2 STRING null,
	addressLine3 STRING null,
	city STRING null,
	postcode VARCHAR(75) null,
	primary key (companyId, uprn)
);