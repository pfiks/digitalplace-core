package com.placecube.digitalplace.appointment.web.configuration;

import com.placecube.digitalplace.appointment.web.constants.ConfigurationConstants;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = ConfigurationConstants.CONFIG_PID)
public interface AppointmentDashboardPortletInstanceConfiguration {

	@Meta.AD(required = false)
	long displayPageTemplateEntryId();

	@Meta.AD(required = false, name = "dpappointment-filter-column-label")
	String filterColumnLabel();

	@Meta.AD(required = false, name = "dpappointment-id-column-label")
	String idColumnLabel();

	@Meta.AD(required = false, name = "dpappointment-object-definition-id")
	long objectDefinitionId();

	@Meta.AD(required = false, name = "dpappointment-odata-filter")
	String odataFilter();

	@Meta.AD(required = false, name = "dpappointment-service-id")
	String serviceId();

	@Meta.AD(required = false, name = "dpappointment-start-date-column-label")
	String startDateColumnLabel();

	@Meta.AD(required = false, name = "dpappointment-summary-column-label")
	String summaryColumnLabel();

	@Meta.AD(required = false, name = "dpappointment-summary-field-expression")
	String summaryFieldExpression();

}
