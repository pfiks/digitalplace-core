/**
 * Credentials.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uk.co.capita_software.support.selfservice;

public class Credentials  implements java.io.Serializable {
    private uk.co.capita_software.support.selfservice.Subject subject;

    private uk.co.capita_software.support.selfservice.RequestIdentification requestIdentification;

    private uk.co.capita_software.support.selfservice.Signature signature;

    public Credentials() {
    }

    public Credentials(
           uk.co.capita_software.support.selfservice.Subject subject,
           uk.co.capita_software.support.selfservice.RequestIdentification requestIdentification,
           uk.co.capita_software.support.selfservice.Signature signature) {
           this.subject = subject;
           this.requestIdentification = requestIdentification;
           this.signature = signature;
    }


    /**
     * Gets the subject value for this Credentials.
     *
     * @return subject
     */
    public uk.co.capita_software.support.selfservice.Subject getSubject() {
        return subject;
    }


    /**
     * Sets the subject value for this Credentials.
     *
     * @param subject
     */
    public void setSubject(uk.co.capita_software.support.selfservice.Subject subject) {
        this.subject = subject;
    }


    /**
     * Gets the requestIdentification value for this Credentials.
     *
     * @return requestIdentification
     */
    public uk.co.capita_software.support.selfservice.RequestIdentification getRequestIdentification() {
        return requestIdentification;
    }


    /**
     * Sets the requestIdentification value for this Credentials.
     *
     * @param requestIdentification
     */
    public void setRequestIdentification(uk.co.capita_software.support.selfservice.RequestIdentification requestIdentification) {
        this.requestIdentification = requestIdentification;
    }


    /**
     * Gets the signature value for this Credentials.
     *
     * @return signature
     */
    public uk.co.capita_software.support.selfservice.Signature getSignature() {
        return signature;
    }


    /**
     * Sets the signature value for this Credentials.
     *
     * @param signature
     */
    public void setSignature(uk.co.capita_software.support.selfservice.Signature signature) {
        this.signature = signature;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Credentials)) return false;
        Credentials other = (Credentials) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.subject==null && other.getSubject()==null) ||
             (this.subject!=null &&
              this.subject.equals(other.getSubject()))) &&
            ((this.requestIdentification==null && other.getRequestIdentification()==null) ||
             (this.requestIdentification!=null &&
              this.requestIdentification.equals(other.getRequestIdentification()))) &&
            ((this.signature==null && other.getSignature()==null) ||
             (this.signature!=null &&
              this.signature.equals(other.getSignature())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSubject() != null) {
            _hashCode += getSubject().hashCode();
        }
        if (getRequestIdentification() != null) {
            _hashCode += getRequestIdentification().hashCode();
        }
        if (getSignature() != null) {
            _hashCode += getSignature().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Credentials.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "credentials"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subject");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "subject"));
        elemField.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "subject"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestIdentification");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "requestIdentification"));
        elemField.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "requestIdentification"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("signature");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "signature"));
        elemField.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "signature"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

    @Override
    public String toString() {
        return "Credentials [subject=" + subject + ", requestIdentification="
                + requestIdentification + ", signature=" + signature + "]";
    }



}
