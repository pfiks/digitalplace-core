package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.common.portlet.PortletURLService;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.constants.SharedSearchSessionKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.configuration.DDLFieldFilterPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.model.DDLFilterGroup;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.service.DDLFieldFilterGroupService;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.service.DDLFieldFilterService;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=ddl-search-field-filter", //
		"com.liferay.portlet.css-class-wrapper=ddl-search-field-filter dp-search-filter dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=true", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.use-default-template=true", //
		"com.liferay.portlet.header-portlet-javascript=/js/filter-builder.js", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/ddl-search-field-filter/view.jsp", //
		"javax.portlet.init-param.config-template=/ddl-search-field-filter/configuration.jsp", //
		"javax.portlet.name=" + PortletKeys.DDL_FIELD_FILTER_PORTLET, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class DDLFieldFilterPortlet extends MVCPortlet {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DDLFieldFilterService ddlFieldFilterService;

	@Reference
	private DDLFieldFilterGroupService ddlFilterGroupFactory;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private PortletURLService portletURLService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			DDLFieldFilterPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(DDLFieldFilterPortletInstanceConfiguration.class, themeDisplay);

			if (Validator.isNull(configuration.ddmStructureId()) || Validator.isNull(configuration.jsonFilterConfig())) {
				renderRequest.setAttribute("invalidConfiguration", true);
				renderRequest.setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
			} else {
				if (configuration.invisible()) {
					renderRequest.setAttribute("invisible", true);
					renderRequest.setAttribute("isEditLayoutMode", portletURLService.isEditLayoutMode(renderRequest));
				} else {
					List<DDLFilterGroup> ddlFilterGroups = new ArrayList<>();

					JSONArray jsonFilterGroups = jsonFactory.createJSONArray(configuration.jsonFilterConfig());
					ddlFilterGroups = ddlFilterGroupFactory.getFilterGroups(jsonFilterGroups);

					SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

					String appliedFilter = searchResponse.getSharedSearchSessionSingleValuedAttribute(SharedSearchSessionKeys.APPLIED_FILTER + "_" + configuration.internalPortletId());
					if (Validator.isNotNull(appliedFilter)) {
						List<String> appliedFilters = ddlFieldFilterService.getAppliedFilterNames(appliedFilter);
						renderRequest.setAttribute("appliedFilters", appliedFilters);
					}

					renderRequest.setAttribute("configuration", configuration);
					renderRequest.setAttribute("ddlFilterGroups", ddlFilterGroups);
					renderRequest.setAttribute("redirectURL", searchResponse.getEscapedSearchResultsIteratorURL());
					renderRequest.setAttribute("showGroupNameAsFirstOption", configuration.showGroupNameAsFirstOption());
				}
			}

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}
