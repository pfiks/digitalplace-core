package com.placecube.digitalplace.payment.model;

import java.util.List;

import com.liferay.portal.kernel.model.User;

public class AgentReferredPaymentContext {

	private String callerPhoneNumber;
	private String callerEmail;
	private String receiptEmail;
	private boolean immediateCallBack;
	private String agentPhoneNumber;
	private String returnUrl;
	private String sid;
	private String logUrl;
	private User user;
	private List<AgentReferredPaymentLine> paymentLines;

	public String getCallerPhoneNumber() {
		return callerPhoneNumber;
	}

	public void setCallerPhoneNumber(String callerPhoneNumber) {
		this.callerPhoneNumber = callerPhoneNumber;
	}

	public String getCallerEmail() {
		return callerEmail;
	}

	public void setCallerEmail(String callerEmail) {
		this.callerEmail = callerEmail;
	}

	public String getReceiptEmail() {
		return receiptEmail;
	}

	public void setReceiptEmail(String receiptEmail) {
		this.receiptEmail = receiptEmail;
	}

	public boolean isImmediateCallBack() {
		return immediateCallBack;
	}

	public void setImmediateCallBack(boolean immediateCallBack) {
		this.immediateCallBack = immediateCallBack;
	}

	public String getAgentPhoneNumber() {
		return agentPhoneNumber;
	}

	public void setAgentPhoneNumber(String phoneNumber) {
		agentPhoneNumber = phoneNumber;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getLogUrl() {
		return logUrl;
	}

	public void setLogUrl(String logUrl) {
		this.logUrl = logUrl;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<AgentReferredPaymentLine> getPaymentLines() {
		return paymentLines;
	}

	public void setPaymentLines(List<AgentReferredPaymentLine> paymentLines) {
		this.paymentLines = paymentLines;
	}

}
