package com.placecube.digitalplace.ddl.web.service;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;

@Component(immediate = true, service = DDLURLService.class)
public class DDLURLService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLURLService.class);

	@Reference
	private DDLRecordSetLocalService ddlRecordSetLocalService;

	@Reference
	private PortletURLFactory portletURLFactory;

	public String getEditRecordURL(ThemeDisplay themeDisplay, long ddlRecordSetId, String... autofillParams) throws PortalException {
		try {
			DDLRecordSet ddlRecordSet = ddlRecordSetLocalService.getDDLRecordSet(ddlRecordSetId);
			LiferayPortletURL liferayPortletURL = portletURLFactory.create(themeDisplay.getRequest(), PortletKeys.DDL_DISPLAY_PORTLET, PortletRequest.RENDER_PHASE);
			liferayPortletURL.setWindowState(LiferayWindowState.MAXIMIZED);
			MutableRenderParameters renderParameters = liferayPortletURL.getRenderParameters();
			renderParameters.setValue("mvcRenderCommandName", MVCCommandKeys.UPDATE_ENTRY);
			renderParameters.setValue("ddlRecordSetId", String.valueOf(ddlRecordSetId));
			renderParameters.setValue("ddmStructureId", String.valueOf(ddlRecordSet.getDDMStructureId()));
			for (String params : autofillParams) {
				renderParameters.setValue("ddlAutofillFields", params);
			}
			return liferayPortletURL.toString();
		} catch (Exception e) {
			LOG.error(e);
			throw new PortalException("Unable to create edit record URL", e);
		}
	}
}