package com.placecube.digitalplace.addresslookup.dtf.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.dtf.constants.DTFConstants;

@RunWith(PowerMockRunner.class)
public class ResponseParserServiceTest extends PowerMockito {

	private static final String POSTCODE = "TS1 1ST";

	private static final String POSTTOWN = "Test";

	private static final String PAODESC = "Flat 1";

	private static final String PAONUMBER = "30";

	private static final String SAODESC = "Burrows House";

	private static final Integer SAONUMBER = 20;

	private static final String STREET = "Test Street";

	private static final String UPRN = "123456";

	@InjectMocks
	private ResponseParserService responseParserService;

	@Mock
	private ResultSet mockResultSet;

	@Test(expected = SQLException.class)
	public void getAddressContextFromResultSet_WhenErrorAccessingResultSet_ThenThrowsSQLException() throws Exception {

		when(mockResultSet.getString(DTFConstants.FIELD_NAME_UPRN)).thenThrow(new SQLException());

		responseParserService.getAddressContextFromResultSet(mockResultSet);
	}

	@Test
	public void getAddressContextFromResultSet_WhenSaoDescBlankAndSaoNumberZeroAndPaoDescBlank_ThenSetsAddressLine1ToPaoNumberAndAddressLine2ToStreet() throws Exception {

		mockResults();

		when(mockResultSet.getString(DTFConstants.FIELD_NAME_SAODESC)).thenReturn(StringPool.BLANK);
		when(mockResultSet.getInt(DTFConstants.FIELD_NAME_SAONUMBER)).thenReturn(0);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_PAODESC)).thenReturn(StringPool.BLANK);

		AddressContext result = responseParserService.getAddressContextFromResultSet(mockResultSet);

		assertThat(result.getAddressLine1(), equalTo(PAONUMBER));
		assertThat(result.getAddressLine2(), equalTo(STREET));
		testAddressContext(result);
	}

	@Test
	public void getAddressContextFromResultSet_WhenSaoDescBlankAndSaoNumberZeroAndPaoDescNotBlank_ThenSetsAddressLine1ToPaoDescAndAddressLine2ToPaoNumberAndStreet() throws Exception {

		mockResults();

		when(mockResultSet.getString(DTFConstants.FIELD_NAME_SAODESC)).thenReturn(StringPool.BLANK);
		when(mockResultSet.getInt(DTFConstants.FIELD_NAME_SAONUMBER)).thenReturn(0);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_PAODESC)).thenReturn(PAODESC);

		AddressContext result = responseParserService.getAddressContextFromResultSet(mockResultSet);

		assertThat(result.getAddressLine1(), equalTo(PAODESC));
		assertThat(result.getAddressLine2(), equalTo(PAONUMBER + " " + STREET));
		testAddressContext(result);
	}

	@Test
	public void getAddressContextFromResultSet_WhenSaoDescBlankAndSaoNumberNotZeroAndPaoDescNotBlank_ThenSetsAddressLine1ToSaoNumberPlusPaoDescAndAddressLine2ToPaoNumberPlusStreet() throws Exception {

		mockResults();

		when(mockResultSet.getString(DTFConstants.FIELD_NAME_SAODESC)).thenReturn(StringPool.BLANK);
		when(mockResultSet.getInt(DTFConstants.FIELD_NAME_SAONUMBER)).thenReturn(SAONUMBER);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_PAODESC)).thenReturn(PAODESC);

		AddressContext result = responseParserService.getAddressContextFromResultSet(mockResultSet);

		assertThat(result.getAddressLine1(), equalTo(SAONUMBER + " " + PAODESC));
		assertThat(result.getAddressLine2(), equalTo(PAONUMBER + " " + STREET));
		testAddressContext(result);
	}

	@Test
	public void getAddressContextFromResultSet_WhenSaoDescNotBlankAndPaoDescNotBlank_ThenSetsAddressLine1ToSaoDescPlusPaoDescAndAddressLine2ToPaoNumberAndStreet() throws Exception {

		mockResults();

		when(mockResultSet.getString(DTFConstants.FIELD_NAME_SAODESC)).thenReturn(SAODESC);
		when(mockResultSet.getInt(DTFConstants.FIELD_NAME_SAONUMBER)).thenReturn(SAONUMBER);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_PAODESC)).thenReturn(PAODESC);

		AddressContext result = responseParserService.getAddressContextFromResultSet(mockResultSet);

		assertThat(result.getAddressLine1(), equalTo(SAODESC + " " + PAODESC));
		assertThat(result.getAddressLine2(), equalTo(PAONUMBER + " " + STREET));
		testAddressContext(result);
	}

	@Test
	public void getAddressContextFromResultSet_WhenSaoDescBlankAndSaoNumberZeroAndPaoDescBlankAndStreetIsNull_ThenSetsAddressLine2ToEmptyString() throws Exception {

		mockResults();

		when(mockResultSet.getString(DTFConstants.FIELD_NAME_SAODESC)).thenReturn(StringPool.BLANK);
		when(mockResultSet.getInt(DTFConstants.FIELD_NAME_SAONUMBER)).thenReturn(0);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_PAODESC)).thenReturn(StringPool.BLANK);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_STREET)).thenReturn(null);

		AddressContext result = responseParserService.getAddressContextFromResultSet(mockResultSet);

		assertThat(result.getAddressLine2(), equalTo(StringPool.BLANK));
	}

	@Test
	public void getAddressContextFromResultSet_WhenSaoDescNotBlankAndPaoDescNotBlankAndStreetIsBlank_ThenSetsAddressLine2ToPaoNumber() throws Exception {

		mockResults();

		when(mockResultSet.getString(DTFConstants.FIELD_NAME_SAODESC)).thenReturn(SAODESC);
		when(mockResultSet.getInt(DTFConstants.FIELD_NAME_SAONUMBER)).thenReturn(SAONUMBER);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_PAODESC)).thenReturn(PAODESC);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_STREET)).thenReturn(StringPool.BLANK);

		AddressContext result = responseParserService.getAddressContextFromResultSet(mockResultSet);

		assertThat(result.getAddressLine2(), equalTo(PAONUMBER));
	}

	private void mockResults() throws Exception {
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_UPRN)).thenReturn(UPRN);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_PAONUMBER)).thenReturn(PAONUMBER);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_STREET)).thenReturn(STREET);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_POSTTOWN)).thenReturn(POSTTOWN);
		when(mockResultSet.getString(DTFConstants.FIELD_NAME_POSTCODE)).thenReturn(POSTCODE);
	}

	private void testAddressContext(AddressContext result) {
		assertThat(result.getAddressLine3(), equalTo(StringPool.BLANK));
		assertThat(result.getCity(), equalTo(POSTTOWN));
		assertThat(result.getPostcode(), equalTo(POSTCODE));
		assertThat(result.getUPRN(), equalTo(UPRN));
	}
}