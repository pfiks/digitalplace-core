package com.placecube.digitalplace.ddl.service;

import java.util.List;
import java.util.Optional;

import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.ddl.model.DynamicDataListEntry;

public interface DynamicDataListService {

	/**
	 * Returns all the DDL entries for the specified list. For each entry the id
	 * and its field values are returned.
	 *
	 * @param ddlRecordSetId the ddl record set list id
	 * @return list of DDL entries
	 */
	List<DynamicDataListEntry> getDDLRecordSetEntries(long ddlRecordSetId);

	/**
	 * Returns a list of DDLRecords belonging to the structure linked to the
	 * structureId and sorted by fieldName
	 *
	 * @param httpServletRequest the request
	 * @param fieldName the sorting field name
	 * @param structureId the ddl structure id
	 * @return a sorted list of DDLRecords
	 */
	List<DDLRecord> getSortedDDLRecordsByFieldName(HttpServletRequest httpServletRequest, String fieldName, String structureId);

	/**
	 * Returns the DDL record friendly URL for the specified view template ID.
	 * If no template ID is specified, then it will use the configured default
	 * full view template ID instead. The returned friendly URL is composed of
	 * the group path and layout friendly URL where the first instance of DDL
	 * display portlet is found. It will also have window state set to maximized
	 * for display of the DDL record.
	 *
	 * @param portalURL the portalURL
	 * @param ddlRecord the DDL record
	 * @param viewDDLTemplateId the view template ID
	 * @param privateLayoutPreference whether to look for the display portlet
	 *            within private layouts first
	 * @return the DDL record friendly URL
	 */
	Optional<String> getViewDDLRecordURL(String portalURL, DDLRecord ddlRecord, boolean privateLayoutPreference, long viewDDLTemplateId);

	/**
	 * Returns the DDL record friendly URL for the specified view template ID.
	 * If no template ID is specified, then it will use the configured default
	 * full view template ID instead. The returned friendly URL is composed of
	 * the group path and layout friendly URL where the first instance of DDL
	 * display portlet is found.
	 *
	 * @param portalURL the portalURL
	 * @param ddlRecord the DDL record
	 * @param viewDDLTemplateId the view template id
	 * @param privateLayoutPreference whether to look for the display portlet
	 *            within private layouts first
	 * @param windowState the window state to display the DDL record in
	 * @return the DDL record friendly URL
	 */
	Optional<String> getViewDDLRecordURL(String portalURL, DDLRecord ddlRecord, long viewDDLTemplateId, boolean privateLayoutPreference, WindowState windowState);

	/**
	 * Returns the DDL record friendly URL for the specified view template ID.
	 * If no template ID is specified, then it will use the configured default
	 * full view template ID instead. The returned friendly URL is built on the
	 * current URL with window state set to maximized for display of the DDL
	 * record.
	 *
	 * @param themeDisplay the themeDisplay
	 * @param ddlRecord the DDL record
	 * @param viewDDLTemplateId the view template ID
	 * @return the DDL record friendly URL
	 */
	String getViewDDLRecordURL(ThemeDisplay themeDisplay, DDLRecord ddlRecord, long viewDDLTemplateId);

	/**
	 * Returns the DDL record friendly URL for the specified view template ID.
	 * If no template ID is specified, then it will use the configured default
	 * full view template ID instead. The returned friendly URL is built on the
	 * current URL.
	 *
	 * @param themeDisplay the themeDisplay
	 * @param ddlRecord the ddl record
	 * @param viewDDLTemplateId the view template ID
	 * @param windowState the window state to display the DDL record in
	 * @return the DDL record friendly URL
	 */
	String getViewDDLRecordURL(ThemeDisplay themeDisplay, DDLRecord ddlRecord, long viewDDLTemplateId, String windowState);

	/**
	 * Returns the DDL record friendly URL for the specified view template ID.
	 * If no template ID is specified, then it will use the configured default
	 * full view template ID instead. The returned friendly URL is built on the
	 * current URL with window state set to maximized for display of the DDL
	 * record.
	 *
	 * @param themeDisplay the themeDisplay
	 * @param ddlRecord the DDL record
	 * @param viewDDLTemplateId the view template ID
	 * @param windowState the window state to display the DDL record in
	 * @return the DDL record friendly URL
	 */
	String getViewDDLRecordURL(ThemeDisplay themeDisplay, DDLRecord ddlRecord, long viewDDLTemplateId, WindowState windowState);
}
