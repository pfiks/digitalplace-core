<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="/init.jsp" %>

<style>
	.sheet-lg {
		max-width: none;
	}
</style>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />
<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post" name="ddlFieldFilterForm" >
	<aui:input name="internalPortletId" type="hidden" value="${ configuration.internalPortletId() }" />

	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />
	<aui:input name="jsonFilterConfig" type="hidden" value="" />

	<liferay-frontend:edit-form-body >
		<dp-frontend:fieldset-group>

			<liferay-frontend:fieldset>

				<aui:input type="text" name="header" label="header" value="${ configuration.header() }" />

				<aui:input name="invisible" label="invisible" type="toggle-switch" value="${ configuration.invisible() }" />
				
				<aui:input name="showGroupNameAsFirstOption" label="show-group-name-as-first-option" type="toggle-switch" value="${ configuration.showGroupNameAsFirstOption() }" />
			    
			    <%@ include file ="../configuration-structure-select.jspf" %>

				<%@ include file="fragments/field_filters.jsp" %>

            </liferay-frontend:fieldset>
		</dp-frontend:fieldset-group>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" value="save" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>

<aui:script>

	$('#<portlet:namespace/>ddmStructureId').change(function( event ) {
		$('#<portlet:namespace/>ddlFieldFilterForm').submit();
	});

</aui:script>
