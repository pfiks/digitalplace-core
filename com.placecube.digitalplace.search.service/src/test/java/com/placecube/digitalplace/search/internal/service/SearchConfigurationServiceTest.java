package com.placecube.digitalplace.search.internal.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.search.configuration.SearchCompanyConfiguration;

@RunWith(MockitoJUnitRunner.class)
public class SearchConfigurationServiceTest {

	@InjectMocks
	private SearchConfigurationService searchConfigurationService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private SearchCompanyConfiguration mockSearchCompanyConfiguration;

	@Test
	public void getConfiguredFacetMultiplier_WhenNoErrors_ThenReturnsConfiguredFacetMultiplier() throws ConfigurationException {
		final long companyId = 342L;
		final int facetMultiplier = 3;

		when(mockConfigurationProvider.getCompanyConfiguration(SearchCompanyConfiguration.class, companyId)).thenReturn(mockSearchCompanyConfiguration);
		when(mockSearchCompanyConfiguration.facetMultiplier()).thenReturn(facetMultiplier);

		int result = searchConfigurationService.getConfiguredFacetMultiplier(companyId);

		assertThat(result, equalTo(facetMultiplier));
	}

	@Test
	public void getConfiguredFacetMultiplier_WhenConfigurationRetrievalFails_ThenReturnsOne() throws ConfigurationException {
		final long companyId = 342L;

		when(mockConfigurationProvider.getCompanyConfiguration(SearchCompanyConfiguration.class, companyId)).thenThrow(new ConfigurationException());

		int result = searchConfigurationService.getConfiguredFacetMultiplier(companyId);

		assertThat(result, equalTo(1));
	}
}
