package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class PersonalDetailsMVCRenderCommandTest extends PowerMockito {

	private static final long GROUP_ID = 100;

	private static final String REDIRECT_URL = "SOME_URL";

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LiferayPortletRequest mockLiferayPortletRequest;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@InjectMocks
	private PersonalDetailsMVCRenderCommand personalDetailsMVCRenderCommand;

	@Before
	public void activateSetUp() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_whenAccountContextAlreadyHasModelIdConfigured_ThenDoesNotSetModelIdAgainAndUseAccountGroupConfiguration() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.registrationRefererGroupIdUserExpandoFieldName()).thenReturn("");
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn("");
		when(mockAccountContext.getAccountConnectorModelId()).thenReturn("someModelIdValue");

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, never()).setAccountConnectorModelId(anyString());
		verify(mockAccountConfigurationService, times(1)).getAccountConfiguration(mockAccountContext, mockRenderRequest);
	}

	@Test
	public void render_whenAccountContextAlreadyHasProfileRedirectUrlConfigured_ThenDoesNotSetProfileRedirectUrlAgain() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.registrationRefererGroupIdUserExpandoFieldName()).thenReturn("");
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn("");
		when(mockAccountContext.getProfileRedirectUrl()).thenReturn("someRedirectValue");

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, never()).setProfileRedirectUrl(anyString());
	}

	@Test
	public void render_whenAccountContextDoesNotHaveModelIdConfigured_ThenSetsModelIdFromRequest() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.registrationRefererGroupIdUserExpandoFieldName()).thenReturn("");
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn("");
		when(mockAccountContext.getAccountConnectorModelId()).thenReturn(" ");
		when(ParamUtil.getString(mockRenderRequest, AccountConnectorConstant.MODEL_ID, StringPool.BLANK)).thenReturn("modelIdValue");

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, times(1)).setAccountConnectorModelId("modelIdValue");
	}

	@Test
	public void render_whenAccountContextDoesNotHaveProfileRedirectUrlConfigured_ThenSetsProfileRedirectUrlFromRequest() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.registrationRefererGroupIdUserExpandoFieldName()).thenReturn("");
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn("");
		when(mockAccountContext.getProfileRedirectUrl()).thenReturn(" ");
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK)).thenReturn(REDIRECT_URL);

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, times(1)).setProfileRedirectUrl(REDIRECT_URL);
	}

	@Test
	public void render_WhenEmailAddressPageLocationIsNotPersonalDetailsPage_ThenSetsShowEmailAddressInRequestToFalse() throws UserAccountException, PortletException {
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK)).thenReturn(REDIRECT_URL);
		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockLiferayPortletRequest.getOriginalHttpServletRequest()).thenReturn(mockHttpServletRequest);

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.SHOW_EMAIL_ADDRESS_IN_PERSONAL_DETAILS, false);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenEmailAddressPageLocationIsPersonalDetailsPage_ThenSetShowCaptchaOnEmailStepAttributeInRequest(boolean showCaptchaOnEmailStep) throws UserAccountException, PortletException {
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK)).thenReturn(REDIRECT_URL);
		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockLiferayPortletRequest.getOriginalHttpServletRequest()).thenReturn(mockHttpServletRequest);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
		when(mockAccountContextService.isShowCaptchaOnEmailStep(mockAccountContext, mockRenderRequest)).thenReturn(showCaptchaOnEmailStep);

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.CAPTCHA_ENABLED, showCaptchaOnEmailStep);
	}

	@Test
	public void render_WhenEmailAddressPageLocationIsPersonalDetailsPage_ThenSetsShowEmailAddressInRequestToTrue() throws UserAccountException, PortletException {
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK)).thenReturn(REDIRECT_URL);
		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockLiferayPortletRequest.getOriginalHttpServletRequest()).thenReturn(mockHttpServletRequest);

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.SHOW_EMAIL_ADDRESS_IN_PERSONAL_DETAILS, true);
	}

	@Test
	public void render_WhenNoError_ThenReturnsPersonalDetailsJSP() throws UserAccountException, PortletException {
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK)).thenReturn(REDIRECT_URL);
		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockLiferayPortletRequest.getOriginalHttpServletRequest()).thenReturn(mockHttpServletRequest);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);

		String result = personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/personal-details.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetsCreateAccountContextAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK)).thenReturn(REDIRECT_URL);
		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockLiferayPortletRequest.getOriginalHttpServletRequest()).thenReturn(mockHttpServletRequest);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, times(1)).setProfileRedirectUrl(REDIRECT_URL);
		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_CONTEXT, mockAccountContext);
		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockRenderRequest);
	}

	@Test
	public void render_WhenNoErrorAndDoesNotHaveRefererGroupIdSetInRequestAndHaveCompanyConfigurationForExpandoFieldName_ThenDoesNotAddRefererGroupIdExpandoFieldValue()
			throws UserAccountException, PortletException {
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK)).thenReturn(REDIRECT_URL);
		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockLiferayPortletRequest.getOriginalHttpServletRequest()).thenReturn(mockHttpServletRequest);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockCreateAccountConfiguration.registrationRefererGroupIdUserExpandoFieldName()).thenReturn("name");

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, never()).addExpandoField(anyString(), anyString());
	}

	@Test
	public void render_WhenNoErrorAndHasRefererGroupIdSetInRequestAndDoesNotHaveCompanyConfigurationForExpandoFieldName_ThenDoesNotAddRefererGroupIdExpandoFieldValue()
			throws UserAccountException, PortletException {
		long refererGroupId = 123;

		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK)).thenReturn(REDIRECT_URL);
		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockLiferayPortletRequest.getOriginalHttpServletRequest()).thenReturn(mockHttpServletRequest);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(ParamUtil.getLong(mockHttpServletRequest, RequestKeys.REFERER)).thenReturn(refererGroupId);

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, never()).addExpandoField(anyString(), anyString());
	}

	@Test
	public void render_WhenNoErrorAndHasRefererGroupIdSetInRequestAndHaveCompanyConfigurationForExpandoFieldName_ThenAddsRefererGroupIdExpandoFieldValue()
			throws UserAccountException, PortletException {
		long refererGroupId = 123;
		String expandoFieldName = "name";

		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(ParamUtil.getString(mockRenderRequest, RequestKeys.REDIRECT_URL, StringPool.BLANK)).thenReturn(REDIRECT_URL);
		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockLiferayPortletRequest.getOriginalHttpServletRequest()).thenReturn(mockHttpServletRequest);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockCreateAccountConfiguration.registrationRefererGroupIdUserExpandoFieldName()).thenReturn(expandoFieldName);
		when(ParamUtil.getLong(mockHttpServletRequest, RequestKeys.REFERER)).thenReturn(refererGroupId);

		personalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContext, times(1)).addExpandoField(expandoFieldName, String.valueOf(refererGroupId));
	}
}
