package com.placecube.digitalplace.user.account.web.constants;

public final class AccountConstants {

	public static final String BUNDLE_SYMBOLIC_NAME = "com.placecube.digitalplace.user.account.web";

	public static final String CREATE_EDIT_MODAL_ID = "UserAccountModal";

	public static final String I_DONT_WANT_TO_SETUP_2FA_LABEL = "no-I-dont-want-to-set-up-2fa";

	public static final String I_WANT_TO_SETUP_2FA_LABEL = "yes-I-want-to-set-up-2fa";

	public static final String NO_CURRENT_ADDRESS = "No Current Address";

	public static final String REDIRECT_URL_PLACEHOLDER = "userId=0";

	public static final String SESSION_CREATE_ACCOUNT_CONTEXT_ATTRIBUTE = "sessionAccountContext";

	public static final String USER_CREATE_ACCOUNT_CONNECTOR_NAME = "digital-place-user-create-account-connector";

	private AccountConstants() {

	}

}
