package com.placecube.digitalplace.search.web.portlet.searchbar.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.layout.service.LayoutRetrievalService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.searchbar.configuration.SearchBarPortletInstanceConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SearchBarServiceTest extends PowerMockito {

	@Mock
	private SearchBarPortletInstanceConfiguration mockConfiguration;

	@Mock
	private Layout mockCurrentPageLayout;

	@Mock
	private Layout mockDestinationLayout;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private LayoutRetrievalService mockLayoutRetrievalService;

	@Mock
	private Portlet mockPortlet1;

	@Mock
	private Portlet mockPortlet2;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SearchBarService searchBarService;

	@Test
	public void getDestinationLayout_WhenIsGuestGroupAndCurrentLayoutIsPrivateAndPrivateLayoutIsFound_ThenReturnsPrivateLayoutByUrlInTheGuestGroup() throws PortalException {
		long companyId = 1;
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockConfiguration.searchGuestGroup()).thenReturn(true);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroupLocalService.getGroup(companyId, GroupConstants.GUEST)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(true);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, true, friendlyUrl)).thenReturn(mockDestinationLayout);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertThat(result.get(), sameInstance(mockDestinationLayout));
	}

	@Test
	public void getDestinationLayout_WhenIsGuestGroupAndCurrentLayoutIsPrivateAndPrivateLayoutNotFoundButPublicLayoutIsFound_ThenReturnsPublicLayoutByUrlInTheGuestGroup() throws PortalException {
		long companyId = 1;
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockConfiguration.searchGuestGroup()).thenReturn(true);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroupLocalService.getGroup(companyId, GroupConstants.GUEST)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(true);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, true, friendlyUrl)).thenReturn(null);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, false, friendlyUrl)).thenReturn(mockDestinationLayout);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertThat(result.get(), sameInstance(mockDestinationLayout));
	}

	@Test
	public void getDestinationLayout_WhenIsGuestGroupAndCurrentLayoutIsPublicAndPublicLayoutIsFound_ThenReturnsPublicLayoutByUrlInTheGuestGroup() throws PortalException {
		long companyId = 1;
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockConfiguration.searchGuestGroup()).thenReturn(true);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroupLocalService.getGroup(companyId, GroupConstants.GUEST)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(false);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, false, friendlyUrl)).thenReturn(mockDestinationLayout);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertThat(result.get(), sameInstance(mockDestinationLayout));
	}

	@Test
	public void getDestinationLayout_WhenIsGuestGroupAndCurrentLayoutIsPublicAndPublicLayoutNotFoundButPrivateLayoutIsFound_ThenReturnsPrivateLayoutByUrlInTheGuestGroup() throws PortalException {
		long companyId = 1;
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.searchGuestGroup()).thenReturn(true);
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroupLocalService.getGroup(companyId, GroupConstants.GUEST)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(false);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, false, friendlyUrl)).thenReturn(null);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, true, friendlyUrl)).thenReturn(mockDestinationLayout);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertThat(result.get(), sameInstance(mockDestinationLayout));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getDestinationLayout_WhenIsGuestGroupAndNoPublicNorPrivateLayoutFound_ThenReturnsEmptyOptional(boolean isPrivate) throws PortalException {
		long companyId = 1;
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.searchGuestGroup()).thenReturn(true);
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockGroupLocalService.getGroup(companyId, GroupConstants.GUEST)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(isPrivate);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, isPrivate, friendlyUrl)).thenReturn(null);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, !isPrivate, friendlyUrl)).thenReturn(null);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertFalse(result.isPresent());
	}

	@Test
	public void getDestinationLayout_WhenIsNotGuestGroupAndCurrentLayoutIsPrivateAndPrivateLayoutIsFound_ThenReturnsPrivateLayoutByUrlInTheScopeGroup() throws PortalException {
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockConfiguration.searchGuestGroup()).thenReturn(false);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(true);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, true, friendlyUrl)).thenReturn(mockDestinationLayout);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertThat(result.get(), sameInstance(mockDestinationLayout));
	}

	@Test
	public void getDestinationLayout_WhenIsNotGuestGroupAndCurrentLayoutIsPrivateAndPrivateLayoutNotFoundButPublicLayoutIsFound_ThenReturnsPublicLayoutByUrlInTheScopeGroup() throws PortalException {
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockConfiguration.searchGuestGroup()).thenReturn(false);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(true);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, true, friendlyUrl)).thenReturn(null);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, false, friendlyUrl)).thenReturn(mockDestinationLayout);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertThat(result.get(), sameInstance(mockDestinationLayout));
	}

	@Test
	public void getDestinationLayout_WhenIsNotGuestGroupAndCurrentLayoutIsPublicAndPublicLayoutIsFound_ThenReturnsPublicLayoutByUrlInTheScopeGroup() throws PortalException {
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockConfiguration.searchGuestGroup()).thenReturn(false);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(false);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, false, friendlyUrl)).thenReturn(mockDestinationLayout);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertThat(result.get(), sameInstance(mockDestinationLayout));
	}

	@Test
	public void getDestinationLayout_WhenIsNotGuestGroupAndCurrentLayoutIsPublicAndPublicLayoutNotFoundButPrivateLayoutIsFound_ThenReturnsPrivateLayoutByUrlInTheScopeGroup() throws PortalException {
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.searchGuestGroup()).thenReturn(false);
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(false);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, false, friendlyUrl)).thenReturn(null);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, true, friendlyUrl)).thenReturn(mockDestinationLayout);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertThat(result.get(), sameInstance(mockDestinationLayout));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getDestinationLayout_WhenIsNotGuestGroupAndNoPublicNorPrivateLayoutFound_ThenReturnsEmptyOptional(boolean isPrivate) throws PortalException {
		long groupId = 2;
		String friendlyUrl = "myFriendlyURLValue";
		when(mockConfiguration.searchGuestGroup()).thenReturn(false);
		when(mockConfiguration.destinationPage()).thenReturn(friendlyUrl);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockCurrentPageLayout);
		when(mockCurrentPageLayout.isPrivateLayout()).thenReturn(isPrivate);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, isPrivate, friendlyUrl)).thenReturn(null);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, !isPrivate, friendlyUrl)).thenReturn(null);

		Optional<Layout> result = searchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration);

		assertFalse(result.isPresent());
	}

	@Test
	public void getDestinationSearchBarPortletIdOnLayout_WhenPortletFound_ThenReturnsThePortletId() {
		when(mockLayoutRetrievalService.getAllPortletsOnLayout(mockDestinationLayout)).thenReturn(Arrays.asList(mockPortlet1, mockPortlet2));
		when(mockPortlet1.getPortletId()).thenReturn("aa");
		when(mockPortlet2.getPortletId()).thenReturn("_" + PortletKeys.SEARCH_BAR + "_instance");

		String result = searchBarService.getDestinationSearchBarPortletIdOnLayout(mockDestinationLayout);

		assertThat(result, equalTo("_" + PortletKeys.SEARCH_BAR + "_instance"));
	}

	@Test
	public void getDestinationSearchBarPortletIdOnLayout_WhenPortletNotFound_ThenReturnsEmptyString() {
		when(mockLayoutRetrievalService.getAllPortletsOnLayout(mockDestinationLayout)).thenReturn(Arrays.asList(mockPortlet1, mockPortlet2));
		when(mockPortlet1.getPortletId()).thenReturn("aa");
		when(mockPortlet2.getPortletId()).thenReturn("bb");

		String result = searchBarService.getDestinationSearchBarPortletIdOnLayout(mockDestinationLayout);

		assertThat(result, equalTo(StringPool.BLANK));
	}
}
