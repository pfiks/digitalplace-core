package com.placecube.digitalplace.formadmin.web.portlet.command;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceService;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.service.DataConsumerLocalService;
import com.placecube.digitalplace.formadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.service.DataConsumerListService;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.FORMS_MANAGER, "mvc.command.name=" + MVCCommandKeys.SELECT_DATA_PROVIDER }, service = MVCRenderCommand.class)
public class SelectDataProviderMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private DataConsumerListService dataConsumerListService;

	@Reference
	private DataConsumerLocalService dataConsumerLocalService;

	@Reference
	private DDMDataProviderInstanceService ddmDataProviderInstanceService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		long formInstanceId = ParamUtil.getLong(renderRequest, PortletRequestKeys.FORM_INSTANCE_ID);
		String formInstanceName = dataConsumerListService.getFormInstanceName(formInstanceId, renderRequest.getLocale());

		List<DataConsumer> dataConsumers = dataConsumerLocalService.getDataConsumersByDDMFormInstanceId(formInstanceId);

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long[] ids = { themeDisplay.getScopeGroupId() };

		List<DDMDataProviderInstance> dataProviders = ddmDataProviderInstanceService.search(themeDisplay.getCompanyId(), ids, "", -1, -1, null);
		renderRequest.setAttribute(PortletRequestKeys.DATA_PROVIDERS, dataProviders);
		renderRequest.setAttribute(PortletRequestKeys.FORM_INSTANCE_ID, formInstanceId);
		renderRequest.setAttribute(PortletRequestKeys.FORM_INSTANCE_NAME, formInstanceName);
		if (!dataConsumers.isEmpty()) {
			renderRequest.setAttribute(PortletRequestKeys.DATA_CONSUMERS, dataConsumers);
		}

		return "/select-data-provider.jsp";
	}

}
