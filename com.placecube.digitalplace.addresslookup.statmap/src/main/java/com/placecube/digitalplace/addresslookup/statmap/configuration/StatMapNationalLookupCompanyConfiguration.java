package com.placecube.digitalplace.addresslookup.statmap.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.addresslookup.statmap.configuration.StatMapNationalLookupCompanyConfiguration", localization = "content/Language", name = "address-lookup-statmap-national-lookup", description = "address-lookup-statmap-national-lookup-description")
public interface StatMapNationalLookupCompanyConfiguration {

	@Meta.AD(required = false, deflt = "https://s1.statmap.co.uk/gazetteers/GAZETTEER_NAME_PLACEHOLDER", name = "statmap-base-record-endpoint", description = "statmap-base-record-endpoint-description")
	String baseRecordEndpoint();

	@Meta.AD(required = false, deflt = "https://s1.statmap.co.uk/gazetteers/GAZETTEER_NAME_PLACEHOLDER/suggest", name = "statmap-base-search-endpoint", description = "statmap-base-search-endpoint-description")
	String baseSearchEndpoint();

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "", name = "statmap-token")
	String token();

	@Meta.AD(required = false, deflt = "1", name = "weight", description = "weight-description")
	Integer weight();

}
