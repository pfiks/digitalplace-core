package com.placecube.digitalplace.search.web.portlet.sortby.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.calendar.model.CalendarBooking;
import com.liferay.calendar.service.CalendarBookingLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.message.boards.model.MBMessage;
import com.liferay.message.boards.service.MBMessageLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.model.SortByOption;
import com.placecube.digitalplace.search.shared.service.SortByOptionsRegistry;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.constants.SortByDirectionConstants;
import com.placecube.digitalplace.search.web.portlet.sortby.model.impl.SortByScore;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.placecube.digitalplace.search.web.portlet.sortby.model.impl.SortByScore" })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SortByServiceTest extends PowerMockito {

	@Mock
	private CalendarBooking mockCalendarBooking;

	@Mock
	private CalendarBookingLocalService mockCalendarBookingLocalService;

	@Mock
	private DDMFormField mockDDMFormFieldIndexbaledOne;

	@Mock
	private DDMFormField mockDDMFormFieldIndexbaledTwo;

	@Mock
	private DDMFormField mockDDMFormFieldNotIndexbaled;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMStructure mockDDMStructureOne;

	@Mock
	private DDMStructure mockDDMStructureTwo;

	@Mock
	private Document mockDocument;

	@Mock
	private DynamicDataListConfigurationService mockDynamicDataListConfigurationService;

	@Mock
	private DynamicDataListMapping mockDynamicDataListMappingOne;

	@Mock
	private DynamicDataListMapping mockDynamicDataListMappingTwo;

	@Mock
	private MBMessage mockMBMessage;

	@Mock
	private MBMessageLocalService mockMBMessageLocalService;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Mock
	private Sort mockSort1;

	@Mock
	private Sort mockSort2;

	@Mock
	private SortByOption mockSortByOption1;

	@Mock
	private SortByOption mockSortByOption2;

	@Mock
	private SortByOptionsRegistry mockSortByOptionsRegistry;

	@Mock
	private Sort mockSortByScore;

	@Mock
	private SortByScore mockSortByScoreOption;

	@InjectMocks
	private SortByService sortByService;

	@Test
	public void addChainedSortByOptions_WhenAvailableSortByOptions_ThenSortByOptionsAreAddedToSorts() {
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByOption2);
		when(mockSortByOption1.getId()).thenReturn("sortField1");
		when(mockSortByOption2.getId()).thenReturn("sortField2");

		sortByService.addChainedSortByOptions(availableSortByOptions, currentSorts);

		assertThat(currentSorts.size(), equalTo(2));
		assertThat(currentSorts.values(), contains(mockSortByOption1, mockSortByOption2));

	}

	@Test
	public void addChainedSortByOptions_WhenSortByScoreOptionIsPresentInAvailableSortByOptions_ThenSortByScoreIsNotAddedToSorts() {
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByScoreOption);
		availableSortByOptions.add(mockSortByOption2);
		when(mockSortByOption1.getId()).thenReturn("sortField1");
		when(mockSortByOption1.getId()).thenReturn("scoreSort");
		when(mockSortByOption2.getId()).thenReturn("sortField2");

		sortByService.addChainedSortByOptions(availableSortByOptions, currentSorts);

		assertThat(currentSorts.size(), equalTo(2));
		assertThat(currentSorts.values(), contains(mockSortByOption1, mockSortByOption2));

	}

	@Test
	public void addDefaultSortByOption_WhenCurrentSortsAreEmptyAndAvailableSortByOptionsAreNotEmpty_ThenReturnsTheFirstAvailableSortByOption() {
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();

		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByOption2);

		sortByService.addDefaultSortByOption(availableSortByOptions, currentSorts);

		assertThat(currentSorts.values(), contains(mockSortByOption1));
	}

	@Test
	public void addDefaultSortByOption_WhenCurrentSortsAreEmptyAndAvailableSortByOptionsContainsScoreOptionFirst_ThenReturnsTheFirstAvailableSortByOptionThatIsNotScoreOption() {
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();

		availableSortByOptions.add(mockSortByScoreOption);
		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByOption2);

		sortByService.addDefaultSortByOption(availableSortByOptions, currentSorts);

		assertThat(currentSorts.values(), contains(mockSortByOption1));
	}

	@Test
	public void addDefaultSortByOption_WhenCurrentSortsAreEmptyAndNoAvailableOptions_ThenNoChangesAreMade() {
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();

		sortByService.addDefaultSortByOption(availableSortByOptions, currentSorts);

		assertThat(currentSorts.size(), equalTo(0));
	}

	@Test
	public void addScoreSortByOption_WhenNoError_ThenAddsTheScoreSortToTheCurrentSorts() {
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();

		sortByService.addScoreSortByOption(currentSorts);

		assertThat(currentSorts.values(), contains(mockSortByScoreOption));
	}

	@Test
	public void addSelectedSortByOption_WhenSelectedSortByIsNotSpecified_ThenNoChangesAreMade() {
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByOption2);

		sortByService.addSelectedSortByOption(availableSortByOptions, currentSorts, Optional.empty());

		assertThat(currentSorts.size(), equalTo(0));
	}

	@Test
	public void addSelectedSortByOption_WhenSelectedSortByIsSpecifiedAndFoundWithinTheAvailableOptions_ThenTheSelectedSortByIsAddedToSorts() {
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByOption2);
		when(mockSortByOption1.getId()).thenReturn("sortField1");
		when(mockSortByOption2.getId()).thenReturn("sortField2");

		sortByService.addSelectedSortByOption(availableSortByOptions, currentSorts, Optional.of("sortField2"));

		assertThat(currentSorts.size(), equalTo(1));
		assertThat(currentSorts.values(), contains(mockSortByOption2));
	}

	@Test
	public void addSelectedSortByOption_WhenSelectedSortByIsSpecifiedButNotFoundWithinTheAvailableOptions_ThenNoChangesAreMade() {
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByOption2);
		when(mockSortByOption1.getId()).thenReturn("sortField1");
		when(mockSortByOption2.getId()).thenReturn("sortField2");

		sortByService.addSelectedSortByOption(availableSortByOptions, currentSorts, Optional.of("invalidSort"));

		assertThat(currentSorts.size(), equalTo(0));
	}

	@Test
	public void getAvailableDDMFormFieldsIndexabled_WhenDDMStructureExist_ThenReturnsListOfIndexableDDMFormFields() {
		long ddmStructureId = 10;
		List<DDMFormField> ddmFormFields = new LinkedList<>();
		ddmFormFields.add(mockDDMFormFieldIndexbaledOne);
		ddmFormFields.add(mockDDMFormFieldNotIndexbaled);
		ddmFormFields.add(mockDDMFormFieldIndexbaledTwo);

		when(mockDDMStructureLocalService.fetchDDMStructure(ddmStructureId)).thenReturn(mockDDMStructureOne);
		when(mockDDMStructureOne.getDDMFormFields(false)).thenReturn(ddmFormFields);
		when(mockDDMFormFieldIndexbaledOne.getIndexType()).thenReturn("keyword");
		when(mockDDMFormFieldIndexbaledTwo.getIndexType()).thenReturn("keyword");
		when(mockDDMFormFieldNotIndexbaled.getIndexType()).thenReturn("xyz_not_indexabled");

		List<DDMFormField> results = sortByService.getAvailableDDMFormFieldsIndexabled(ddmStructureId);

		assertThat(results.size(), equalTo(2));
		assertThat(results, contains(mockDDMFormFieldIndexbaledOne, mockDDMFormFieldIndexbaledTwo));
	}

	@Test
	public void getAvailableDDMFormFieldsIndexabled_WhenDDMStructureNotExist_ThenReturnsEmptyList() {
		long ddmStructureId = 10;
		when(mockDDMStructureLocalService.fetchDDMStructure(ddmStructureId)).thenReturn(null);

		List<DDMFormField> results = sortByService.getAvailableDDMFormFieldsIndexabled(ddmStructureId);

		assertTrue(results.isEmpty());
	}

	@Test
	public void getAvailableDDMStructures_WhenNoError_ThenReturnsDDMStructures() {
		long companyId = 10;
		List<DynamicDataListMapping> dynamicDataListMappings = new LinkedList<>();
		dynamicDataListMappings.add(mockDynamicDataListMappingOne);
		dynamicDataListMappings.add(mockDynamicDataListMappingTwo);

		when(mockDynamicDataListConfigurationService.getDynamicDataListMappings(companyId)).thenReturn(dynamicDataListMappings);
		when(mockDynamicDataListMappingOne.getDDMStructure()).thenReturn(mockDDMStructureOne);
		when(mockDynamicDataListMappingTwo.getDDMStructure()).thenReturn(mockDDMStructureTwo);

		List<DDMStructure> results = sortByService.getAvailableDDMStructures(companyId);

		assertThat(results.size(), equalTo(2));
		assertThat(results, contains(mockDDMStructureOne, mockDDMStructureTwo));
	}

	@Test
	public void getAvailableSortByOptions_WhenSearchKeywordIsBlank_ThenReturnsListWithAvailablSortByOptions() {
		String[] enabledSortByOptions = new String[] { "enabledSortByOption1", "anotherOption" };
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByOption2);
		when(mockSortByOptionsRegistry.getAvailableSortByOptionsForIds(enabledSortByOptions)).thenReturn(availableSortByOptions);

		List<SortByOption> results = sortByService.getAvailableSortByOptions(enabledSortByOptions, StringPool.THREE_SPACES);

		assertThat(results, contains(mockSortByOption1, mockSortByOption2));
	}

	@Test
	public void getAvailableSortByOptions_WhenSearchKeywordIsSpecified_ThenReturnsListWithSortByScoreAndTheAvailableOptions() {
		String[] enabledSortByOptions = new String[] { "enabledSortByOption1", "anotherOption" };
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByOption2);
		when(mockSortByOptionsRegistry.getAvailableSortByOptionsForIds(enabledSortByOptions)).thenReturn(availableSortByOptions);

		List<SortByOption> results = sortByService.getAvailableSortByOptions(enabledSortByOptions, "someValue");

		assertThat(results, contains(mockSortByScoreOption, mockSortByOption1, mockSortByOption2));
	}

	@Test
	public void getAvailableSortByOptionsKeyValuePair_WhenAvailableSortByOptionsDifferentThenEnabledSortByOption_ThenReturnsAvailableSortByOptionsKeyValuePair() {
		String[] enabledSortByOptions = new String[] { "enabledSortByOption1", "enabledSortByOption2", "mockSortByScoreOption" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		sortByOptions.add(mockSortByOption1);
		sortByOptions.add(mockSortByOption2);
		sortByOptions.add(mockSortByScoreOption);

		when(mockSortByOptionsRegistry.getAllAvailableSortByOptions()).thenReturn(sortByOptions);

		when(mockSortByOption1.getId()).thenReturn("availableSortByOption1");
		when(mockSortByOption1.getLabel(Locale.ENGLISH)).thenReturn("availableSortByOption1Label");

		when(mockSortByOption2.getId()).thenReturn("availabledSortByOption2");
		when(mockSortByOption2.getLabel(Locale.ENGLISH)).thenReturn("availableSortByOption2Label");

		when(mockSortByScoreOption.getId()).thenReturn("mockSortByScoreOption");
		when(mockSortByScoreOption.getLabel(Locale.ENGLISH)).thenReturn("mockSortByScoreOptionLabel");

		List<KeyValuePair> results = sortByService.getAvailableSortByOptionsKeyValuePair(Arrays.asList(enabledSortByOptions), Locale.ENGLISH);

		assertThat(results, contains(new KeyValuePair("availableSortByOption1", "availabledSortByOption1Label"), new KeyValuePair("availabledSortByOption2", "availableSortByOption2Label")));
	}

	@Test
	public void getEnabledSortByOptionsKeyValuePair_WhenEnabledSortByOptionsExist_ThenReturnsEnabledSortByOptionsKeyValuePair() {
		String[] enabledSortByOptions = new String[] { "enabledSortByOption1", "anotherOption" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		sortByOptions.add(mockSortByOption1);
		sortByOptions.add(mockSortByOption2);

		when(mockSortByOptionsRegistry.getAvailableSortByOptionsForIds(enabledSortByOptions)).thenReturn(sortByOptions);

		when(mockSortByOption1.getId()).thenReturn("enabledSortByOption1");
		when(mockSortByOption1.getLabel(Locale.ENGLISH)).thenReturn("enabledSortByOption1Label");

		when(mockSortByOption2.getId()).thenReturn("enabledSortByOption2");
		when(mockSortByOption2.getLabel(Locale.ENGLISH)).thenReturn("enabledSortByOption2Label");

		List<KeyValuePair> results = sortByService.getEnabledSortByOptionsKeyValuePair(enabledSortByOptions, Locale.ENGLISH);

		assertThat(results, contains(new KeyValuePair("enabledSortByOption1", "enabledSortByOption1Label"), new KeyValuePair("enabledSortByOption2", "enabledSortByOption2Label")));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSortsForSortByOptions_WhenNoSortByOptionsInMap_ThenReturnsEmptyArrayOfSorts(boolean useOppositeSortDirection) {
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();

		Sort[] sorts = sortByService.getSortsForSortByOptions(mockSharedSearchContributorSettings, currentSorts.values(), useOppositeSortDirection);

		assertThat(sorts, equalTo(new Sort[] {}));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSortsForSortByOptions_WhenSortByOptionsInMap_ThenReturnsArrayOfSorts(boolean useOppositeSortDirection) {
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		List<SortByOption> availableSortByOptions = new LinkedList<>();
		availableSortByOptions.add(mockSortByOption1);
		availableSortByOptions.add(mockSortByOption2);
		when(mockSortByOption1.getId()).thenReturn("sortField1");
		when(mockSortByOption1.getSorts(mockSharedSearchContributorSettings, useOppositeSortDirection)).thenReturn(new Sort[] { mockSort1 });
		when(mockSortByOption2.getId()).thenReturn("sortField2");
		when(mockSortByOption2.getSorts(mockSharedSearchContributorSettings, useOppositeSortDirection)).thenReturn(new Sort[] { mockSort2 });

		currentSorts.put(mockSortByOption1.getId(), mockSortByOption1);
		currentSorts.put(mockSortByOption2.getId(), mockSortByOption2);

		Sort[] sorts = sortByService.getSortsForSortByOptions(mockSharedSearchContributorSettings, currentSorts.values(), useOppositeSortDirection);

		assertThat(sorts, equalTo(new Sort[] { mockSort1, mockSort2 }));
	}

	@Test
	public void getTitleForCalendarBooking_WhenExceptionRetrievingEvent_ThenReturnsEmptyString() throws PortalException {
		when(mockDocument.get("entryClassPK")).thenReturn("123");
		when(mockCalendarBookingLocalService.getCalendarBooking(123L)).thenThrow(new PortalException());

		String result = sortByService.getTitleForCalendarBooking(mockDocument);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getTitleForCalendarBooking_WhenNoError_ThenReturnsTheTitleForTheEvent() throws PortalException {
		String expected = "expectedValue";
		when(mockDocument.get("entryClassPK")).thenReturn("123");
		when(mockCalendarBookingLocalService.getCalendarBooking(123L)).thenReturn(mockCalendarBooking);
		when(mockCalendarBooking.getDefaultLanguageId()).thenReturn("langId");
		when(mockCalendarBooking.getTitle("langId")).thenReturn(expected);

		String result = sortByService.getTitleForCalendarBooking(mockDocument);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getTitleForMBMessage_WhenExceptionRetrievingMBMessage_ThenReturnsEmptyString() throws PortalException {
		when(mockDocument.get("entryClassPK")).thenReturn("123");
		when(mockMBMessageLocalService.getMBMessage(123L)).thenThrow(new PortalException());

		String result = sortByService.getTitleForMBMessage(mockDocument);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getTitleForMBMessage_WhenNoError_ThenReturnsTheTitleForTheMBMessage() throws PortalException {
		String expected = "expectedValue";
		when(mockDocument.get("entryClassPK")).thenReturn("123");
		when(mockMBMessageLocalService.getMBMessage(123L)).thenReturn(mockMBMessage);
		when(mockMBMessage.getSubject()).thenReturn(expected);

		String result = sortByService.getTitleForMBMessage(mockDocument);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void useOppositeSortByDirection_WhenSelectedSortByNotPresent_ThenReturnsFalse() {
		boolean result = sortByService.useOppositeSortByDirection(Optional.empty(), mockSharedSearchContributorSettings);

		assertFalse(result);
	}

	@Test
	public void useOppositeSortByDirection_WhenSelectedSortByPresentAndNoSortByDirectionFound_ThenReturnsFalse() {
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY_DIRECTION)).thenReturn(Optional.empty());

		boolean result = sortByService.useOppositeSortByDirection(Optional.of("sortByValue"), mockSharedSearchContributorSettings);

		assertFalse(result);
	}

	@Test
	public void useOppositeSortByDirection_WhenSelectedSortByPresentAndSortByDirectionIsInverted_ThenReturnsTrue() {
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY_DIRECTION))
				.thenReturn(Optional.of(SortByDirectionConstants.DIRECTION_INVERTED));

		boolean result = sortByService.useOppositeSortByDirection(Optional.of("sortByValue"), mockSharedSearchContributorSettings);

		assertTrue(result);
	}

	@Test
	public void useOppositeSortByDirection_WhenSelectedSortByPresentAndSortByDirectionNotInverted_ThenReturnsFalse() {
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY_DIRECTION)).thenReturn(Optional.of("someSortByDirection"));

		boolean result = sortByService.useOppositeSortByDirection(Optional.of("sortByValue"), mockSharedSearchContributorSettings);

		assertFalse(result);
	}

}
