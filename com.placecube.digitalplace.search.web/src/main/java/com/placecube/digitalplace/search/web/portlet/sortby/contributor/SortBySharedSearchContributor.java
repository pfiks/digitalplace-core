package com.placecube.digitalplace.search.web.portlet.sortby.contributor;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.model.SortByOption;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.service.SortByService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.SORT_BY, service = SharedSearchContributor.class)
public class SortBySharedSearchContributor implements SharedSearchContributor {

	@Reference
	private SortByService sortByService;

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {

		Optional<PortletPreferences> portletPreferences = sharedSearchContributorSettings.getPortletPreferences();
		if (portletPreferences.isPresent()) {
			String searchKeyword = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SEARCH_KEYWORD).orElse(StringPool.BLANK);

			Optional<String> selectedSortBy = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY);

			boolean useOppositeSortDirection = sortByService.useOppositeSortByDirection(selectedSortBy, sharedSearchContributorSettings);

			boolean chainSortsEnabled = GetterUtil.getBoolean(portletPreferences.get().getValue("chainSortByOptions", "false"));
			List<SortByOption> availableSortOptions = getAvailableConfiguredSortByOptions(portletPreferences.get(), searchKeyword);

			Sort[] sortsToApply;
			if (chainSortsEnabled) {
				sortsToApply = getChainedSortsToApply(availableSortOptions, sharedSearchContributorSettings, selectedSortBy, useOppositeSortDirection, searchKeyword);
			} else {
				sortsToApply = getNonChainedSortsToApply(availableSortOptions, sharedSearchContributorSettings, selectedSortBy, useOppositeSortDirection);
			}

			sharedSearchContributorSettings.setSortBy(sortsToApply);

		} else {
			throw new SystemException("Invalid sort by portlet prefs");
		}
	}

	private List<SortByOption> getAvailableConfiguredSortByOptions(PortletPreferences portletPreferences, String searchKeyword) {
		String[] enabledSortByOptions = portletPreferences.getValues("enabledSortByOptions", new String[0]);
		return sortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword);
	}

	private Sort[] getChainedSortsToApply(List<SortByOption> availableSortOptions, SharedSearchContributorSettings sharedSearchContributorSettings, Optional<String> selectedSortBy,
			boolean useOppositeSortDirection, String keyword) {
		Map<String, SortByOption> sortsToApply = new LinkedHashMap<>();

		if (selectedSortBy.isPresent()) {
			sortByService.addSelectedSortByOption(availableSortOptions, sortsToApply, selectedSortBy);
		}

		if (Validator.isNotNull(keyword)) {
			sortByService.addScoreSortByOption(sortsToApply);
		}

		sortByService.addChainedSortByOptions(availableSortOptions, sortsToApply);

		return sortByService.getSortsForSortByOptions(sharedSearchContributorSettings, sortsToApply.values(), useOppositeSortDirection);
	}

	private Sort[] getNonChainedSortsToApply(List<SortByOption> availableSortOptions, SharedSearchContributorSettings sharedSearchContributorSettings, Optional<String> selectedSortBy,
			boolean useOppositeSortDirection) {

		Map<String, SortByOption> sortsToApply = new LinkedHashMap<>();

		sortByService.addSelectedSortByOption(availableSortOptions, sortsToApply, selectedSortBy);
		sortByService.addDefaultSortByOption(availableSortOptions, sortsToApply);
		sortByService.addScoreSortByOption(sortsToApply);

		return sortByService.getSortsForSortByOptions(sharedSearchContributorSettings, sortsToApply.values(), useOppositeSortDirection);

	}
}