package com.placecube.digitalplace.search.shared.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.placecube.digitalplace.search.shared.model.SortByOption;
import com.placecube.digitalplace.search.shared.service.SortByOptionsRegistry;

@Component(immediate = true, service = SortByOptionsRegistry.class)
public class SortByOptionsRegistryImpl implements SortByOptionsRegistry {

	private static final Log LOG = LogFactoryUtil.getLog(SortByOptionsRegistry.class);

	private final Map<String, SortByOption> sortByOptions = new ConcurrentHashMap<>();

	@Override
	public List<SortByOption> getAllAvailableSortByOptions() {
		return sortByOptions.values().stream().collect(Collectors.toList());
	}

	@Override
	public List<SortByOption> getAvailableSortByOptionsForIds(String[] fieldNamesToMatch) {
		List<SortByOption> availableSortByOptionsForIds = new ArrayList<>();
		if (ArrayUtil.isNotEmpty(fieldNamesToMatch)) {
			for (String fieldNameToMatch : fieldNamesToMatch) {
				Optional<SortByOption> sortByOption = sortByOptions.values().stream().filter(a -> a.getId().equals(fieldNameToMatch)).findFirst();
				if (sortByOption.isPresent()) {
					availableSortByOptionsForIds.add(sortByOption.get());
				}
			}
		}
		return availableSortByOptionsForIds;
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC, unbind = "unregisterSortByOption")
	protected void registerSortByOption(SortByOption report) {
		sortByOptions.put(report.getId(), report);
		LOG.debug("Added sort by option " + report.getId());
	}

	protected void unregisterSortByOption(SortByOption report) {
		sortByOptions.remove(report.getId());
		LOG.debug("Removed sort by option " + report.getId());
	}
}
