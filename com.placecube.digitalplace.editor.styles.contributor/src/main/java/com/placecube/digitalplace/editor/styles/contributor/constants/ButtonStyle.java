package com.placecube.digitalplace.editor.styles.contributor.constants;

public enum ButtonStyle {

	BASE("btn btn-nm btn-base", "Base - Call to action button", "span"), //
	DANGER("btn btn-nm btn-danger", "Danger", "span"), //
	INSET("govuk-inset-text", "GOV.UK inset text", "div"), //
	PRIMARY_BASE("btn btn-nm btn-primary", "Primary - Call to action button", "span"), //
	SMALL("btn btn-nm btn-sm", "Small", "span"), //
	SUCCESS("btn btn-nm btn-success", "Success", "span"), //
	TAG("govuk-tag", "GDS Tag", "strong");

	private String data;
	private String element;
	private String name;
	private String value;

	private ButtonStyle(String value, String name, String element) {
		this.value = value;
		this.name = name;
		this.element = element;
	}

	private ButtonStyle(String value, String name, String element, String data) {
		this.value = value;
		this.name = name;
		this.element = element;
		this.data = data;
	}

	public String getData() {
		return data;
	}

	public String getElement() {
		return element;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}
}
