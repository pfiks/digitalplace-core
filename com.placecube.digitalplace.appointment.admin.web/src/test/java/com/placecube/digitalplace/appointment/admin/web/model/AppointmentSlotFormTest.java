package com.placecube.digitalplace.appointment.admin.web.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Date;
import java.util.TimeZone;

import org.junit.Test;

import com.pfiks.common.time.model.DateTimeSelection;

public class AppointmentSlotFormTest {

	@Test
	public void getFormattedTimes_WhenNoError_ThenReturnsTheStartAndEndTimeFormatted() {
		DateTimeSelection startDateTime = new DateTimeSelection();
		startDateTime.setHour(10);
		startDateTime.setMinute(9);

		DateTimeSelection endDateTime = new DateTimeSelection();
		endDateTime.setHour(17);
		endDateTime.setMinute(15);

		AppointmentSlotForm appointmentSlotForm = new AppointmentSlotForm("type", "serviceId", startDateTime, endDateTime);

		String result = appointmentSlotForm.getFormattedTimes(TimeZone.getDefault());

		assertThat(result, equalTo("10:09 - 17:15"));
	}

	@Test
	public void getSlotEndDate_WhenNoError_ThenReturnsTheEndDateTruncatedToDays() {
		DateTimeSelection endDateTime = new DateTimeSelection();
		endDateTime.setHour(17);
		endDateTime.setMinute(15);

		AppointmentSlotForm appointmentSlotForm = new AppointmentSlotForm("type", "serviceId", null, endDateTime);

		Date result = appointmentSlotForm.getSlotEndDate(TimeZone.getDefault());

		assertThat(result.getHours(), equalTo(0));
		assertThat(result.getMinutes(), equalTo(0));
	}

	@Test
	public void getSlotStartDate_WhenNoError_ThenReturnsTheStartDateTruncatedToDays() {
		DateTimeSelection startDateTime = new DateTimeSelection();
		startDateTime.setHour(10);
		startDateTime.setMinute(9);

		AppointmentSlotForm appointmentSlotForm = new AppointmentSlotForm("type", "serviceId", startDateTime, null);

		Date result = appointmentSlotForm.getSlotStartDate(TimeZone.getDefault());

		assertThat(result.getHours(), equalTo(0));
		assertThat(result.getMinutes(), equalTo(0));
	}

}
