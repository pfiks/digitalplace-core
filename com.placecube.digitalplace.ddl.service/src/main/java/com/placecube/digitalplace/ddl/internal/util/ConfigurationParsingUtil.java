package com.placecube.digitalplace.ddl.internal.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.internal.configuration.DynamicDataListCompanyConfiguration;
import com.placecube.digitalplace.ddl.internal.configuration.DynamicDataListCompanyConfigurationConstants;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.model.impl.ModelBuilder;

@Component(immediate = true, service = ConfigurationParsingUtil.class)
public class ConfigurationParsingUtil {

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private ModelBuilder modelBuilder;

	public List<DynamicDataListMapping> getDynamicDataListMappings(DynamicDataListCompanyConfiguration configuration, long companyId) {

		long originalCompanyId = CompanyThreadLocal.getCompanyId();

		CompanyThreadLocal.setCompanyId(companyId);

		List<DynamicDataListMapping> results = new ArrayList<>();

		String[] configuredMappings = configuration.dynamicDataListMappings();
		if (ArrayUtil.isNotEmpty(configuredMappings)) {

			for (String mapping : configuredMappings) {
				addMappingToResults(results, mapping);
			}

		}

		CompanyThreadLocal.setCompanyId(originalCompanyId);

		return results;
	}

	public String getMappingJsonString(DDMStructure ddmStructure, DDMTemplate fullViewTemplate, DDMTemplate geoPointViewTemplate, DDMTemplate listViewTemplate, DDMTemplate gridViewTemplate,
			String titleField, String contentField, String iconName) {
		JSONObject mapping = jsonFactory.createJSONObject();
		mapping.put(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_MAPPING_NAME, ddmStructure.getName(ddmStructure.getDefaultLanguageId()));
		mapping.put(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_STRUCTURE_ID, ddmStructure.getStructureId());
		mapping.put(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_FULL_VIEW_TEMPLATE_ID, fullViewTemplate.getTemplateId());
		mapping.put(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_GEOPOINT_TEMPLATE_ID, Validator.isNotNull(geoPointViewTemplate) ? geoPointViewTemplate.getTemplateId() : 0l);
		mapping.put(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_LIST_TEMPLATE_ID, listViewTemplate.getTemplateId());
		mapping.put(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_GRID_TEMPLATE_ID, gridViewTemplate.getTemplateId());
		mapping.put(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_INDEX_TITLE, titleField);
		mapping.put(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_INDEX_CONTENT, contentField);
		mapping.put(DynamicDataListCompanyConfigurationConstants.CONFIG_FIELD_ICON_NAME, iconName);
		return mapping.toJSONString();
	}

	public String getMappingJsonString(DDMStructure ddmStructure, DDMTemplate fullViewTemplate, DDMTemplate listViewTemplate, DDMTemplate gridViewTemplate, String titleField, String contentField,
			String iconName) {
		return getMappingJsonString(ddmStructure, fullViewTemplate, null, listViewTemplate, gridViewTemplate, titleField, contentField, iconName);
	}

	private void addMappingToResults(List<DynamicDataListMapping> results, String mapping) {
		Optional<DynamicDataListMapping> dynamicDataListMapping = modelBuilder.getDynamicDataListMapping(mapping);
		if (dynamicDataListMapping.isPresent()) {
			results.add(dynamicDataListMapping.get());
		}
	}

}
