package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.PASSWORD_DETAILS }, service = MVCActionCommand.class)
public class PasswordDetailsMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(PasswordDetailsMVCActionCommand.class);

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private AccountService accountService;

	@Reference
	private AccountValidationService accountValidationService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(actionRequest);
			accountContextService.configurePasswordDetails(accountContext, actionRequest);

			accountContext.clearErrors();
			accountValidationService.validatePasswordForm(accountContext, actionRequest);

			if (accountContext.hasErrors()) {
				actionResponse.getRenderParameters().setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.PASSWORD_DETAILS);
			} else if (accountConfigurationService.isAccountSecurityStepEnabled(accountContext)) {
				actionResponse.getRenderParameters().setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.ACCOUNT_SECURITY);
			} else {
				accountService.createUserAccount(actionRequest, accountContext, accountConfigurationService.getAccountConfiguration(accountContext, actionRequest).addressOutOfLocalArea());
				actionResponse.getRenderParameters().setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.CONFIRMATION_PAGE);
			}
			accountContextService.updateContextInSession(accountContext, actionRequest);

		} catch (UserAccountException e) {
			LOG.error("Unable to create user account", e);

			SessionErrors.add(actionRequest, e.getClass(), e);

			actionResponse.getRenderParameters().setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.PASSWORD_DETAILS);
		}

		hideDefaultErrorMessage(actionRequest);
	}

}
