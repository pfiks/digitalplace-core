<%@ include file="init.jsp"%>

<c:set var="progressBarValue" value="50"/>
<%@ include file="/fragments/progress-bar.jspf" %>

<div class="row">
	<%@ include file="/fragments/side-bar.jspf" %>
	
	<div class="col-md-8"> 

		<portlet:actionURL name="<%= MVCCommandKeys.ADDRESS_DETAILS %>" var="addressDetailsURL" />

		<gds-forms-ui:errorSummary portletNamespace="${portletNamespace}" errors="${accountContext.getErrors()}"/>

		<aui:form action="${addressDetailsURL}" method="post" name="createAccountForm">

			<dp-address:postcode-lookup fullAddress="${ accountContext.fullAddress }" postcode="${ accountContext.postcode }" uprn="${ accountContext.uprn }" required="${ addressRequired }" fallbackToNationalLookup="${ isAddressOutOfLocalAreaEnabled }"/>

			<div class="row">
				<%@ include file="/fragments/account-fields.jspf" %>
			</div>

			<aui:button-row>
				<div class="pull-right">
					<aui:button type="submit" value="next" cssClass="btn btn-primary" icon="glyphicon glyphicon-chevron-right" iconAlign="right"/>
				</div>
				<div class="pull-left">
					<portlet:renderURL var="backURL">
						<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.PERSONAL_DETAILS%>" />
					</portlet:renderURL>
					<aui:button type="button" value="previous" cssClass="btn btn-default" href="${backURL}" icon="glyphicon glyphicon-chevron-left" iconAlign="left"/>
				</div>								
			</aui:button-row>
		</aui:form>
	</div>
</div>
