package com.placecube.digitalplace.user.account.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE)
@Meta.OCD(id = "com.placecube.digitalplace.user.account.configuration.AccountPortletInstanceConfiguration", localization = "content/Language", name = "widget-create-account")
public interface AccountPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "true", name = "email-address-required")
	boolean emailAddressRequired();
}
