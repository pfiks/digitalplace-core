package com.placecube.digitalplace.search.internal.indexer;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.spi.model.query.contributor.QueryPreFilterContributor;
import com.placecube.digitalplace.search.internal.contants.SharedSearchContextAttributeKeys;

@Component(immediate = true, service = QueryPreFilterContributor.class)
public class BooleanClauseQueryPreFilterContributor implements QueryPreFilterContributor {

	@Override
	public void contribute(BooleanFilter booleanFilter, SearchContext searchContext) {

		@SuppressWarnings("unchecked")
		List<BooleanClause<Filter>> searchBooleanClauses = (List<BooleanClause<Filter>>) searchContext.getAttribute(SharedSearchContextAttributeKeys.SEARCH_BOOLEAN_CLAUSE);
		if (Validator.isNotNull(searchBooleanClauses)) {
			searchBooleanClauses.forEach(f -> booleanFilter.add(f.getClause(), f.getBooleanClauseOccur()));
		}

	}
}
