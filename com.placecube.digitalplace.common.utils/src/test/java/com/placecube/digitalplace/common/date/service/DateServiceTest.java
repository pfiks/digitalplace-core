package com.placecube.digitalplace.common.date.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({ "com.sun.org.apache.xerces.*", "javax.xml.*", "org.xml.*", "javax.management.*" })
public class DateServiceTest {

	private static final ZoneId ZONE_ID = ZoneId.of("GMT");

	private DateService dateService;

	@Before
	public void setUp() {
		dateService = new DateService();
	}

	@Test
	public void localDateToDate_WhenNoErrors_ThenReturnsZoneDate() {
		LocalDate testLocalDate = LocalDate.now();

		Date expectedDate = Date.from(testLocalDate.atStartOfDay(ZONE_ID).toInstant());

		Date result = dateService.localDateToDate(testLocalDate, ZONE_ID);

		assertThat(result.getTime(), equalTo(expectedDate.getTime()));
	}

	@Test
	public void toXMLGregorianCalendar_WhenCalledWithLocalDateArgument_ThenReturnsXMLGregorianCalendarForDate() throws Exception {
		LocalDate testLocalDate = LocalDate.now();

		XMLGregorianCalendar result = dateService.toXMLGregorianCalendar(testLocalDate);
		LocalDate resultLocalDate = LocalDate.of(result.getYear(), result.getMonth(), result.getDay());

		assertThat(resultLocalDate.getYear(), equalTo(testLocalDate.getYear()));
		assertThat(resultLocalDate.getMonth(), equalTo(testLocalDate.getMonth()));
		assertThat(resultLocalDate.getDayOfMonth(), equalTo(testLocalDate.getDayOfMonth()));
	}

	@Test
	public void toXMLGregorianCalendar_WhenCalledWithDateArgument_ThenReturnsXMLGregorianCalendarForDate() throws Exception {
		LocalDate testLocalDate = LocalDate.now();

		Date date = Date.from(testLocalDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

		XMLGregorianCalendar result = dateService.toXMLGregorianCalendar(date);

		LocalDate resultLocalDate = LocalDate.of(result.getYear(), result.getMonth(), result.getDay());

		assertThat(resultLocalDate.getYear(), equalTo(testLocalDate.getYear()));
		assertThat(resultLocalDate.getMonth(), equalTo(testLocalDate.getMonth()));
		assertThat(resultLocalDate.getDayOfMonth(), equalTo(testLocalDate.getDayOfMonth()));
	}
}
