<%@ page import="com.placecube.digitalplace.twitterfeed.constants.PortletRequestKeys" %>
<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ include file="init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL"/>
<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL"/>

<liferay-frontend:edit-form action="${configurationActionURL}" method="post" name="fm">

	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>"/>
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}"/>

	<liferay-frontend:edit-form-body>
		<dp-frontend:fieldset-group>
			<liferay-frontend:fieldset>
				<aui:input type="textarea" name="<%=PortletRequestKeys.TWITTER_EMBEDDED %>" value="${configuration.twitterEmbedded()}" label="embedded"/>
				
				<aui:input type="text" name="<%=PortletRequestKeys.TWITTER_USERNAME %>" value="${configuration.twitterUsername()}" label="screen-name"/>
				
				<aui:input type="number" name="<%=PortletRequestKeys.FEEDS_TO_SHOW %>" value="${configuration.feedsToShow()}" min="0" label="number-of-items-to-display"/>
			</liferay-frontend:fieldset>
		</dp-frontend:fieldset-group>
	</liferay-frontend:edit-form-body>
	<liferay-frontend:edit-form-footer>
		<aui:button type="submit"/>

		<aui:button type="cancel"/>
	</liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>