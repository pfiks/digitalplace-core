package com.placecube.digitalplace.ddl.web.context;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.service.DDLRelatedSearchListingsService;

@Component(immediate = true)
public class ServletContextUtil {

	private static ServletContextUtil instance;

	@Reference
	protected DDLRelatedSearchListingsService ddlRelatedSearchListingsService;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.ddl.web)", unbind = "-")
	protected ServletContext servletContext;

	public static final ServletContext getServletContext() {
		return instance.servletContext;
	}
	public static DDLRelatedSearchListingsService getDDLRelatedSearchListingsService() {
		return instance.ddlRelatedSearchListingsService;
	}

	@Activate
	protected void activate() {
		instance = this;
	}

	@Deactivate
	protected void deactivate() {
		instance = null;
	}

}