/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link AppointmentEntryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentEntryLocalService
 * @generated
 */
public class AppointmentEntryLocalServiceWrapper
	implements AppointmentEntryLocalService,
			   ServiceWrapper<AppointmentEntryLocalService> {

	public AppointmentEntryLocalServiceWrapper() {
		this(null);
	}

	public AppointmentEntryLocalServiceWrapper(
		AppointmentEntryLocalService appointmentEntryLocalService) {

		_appointmentEntryLocalService = appointmentEntryLocalService;
	}

	/**
	 * Adds the appointment entry to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentEntry the appointment entry
	 * @return the appointment entry that was added
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
		addAppointmentEntry(
			com.placecube.digitalplace.appointment.model.AppointmentEntry
				appointmentEntry) {

		return _appointmentEntryLocalService.addAppointmentEntry(
			appointmentEntry);
	}

	/**
	 * Creates a new appointment entry
	 *
	 * @param companyId the companyId
	 * @param serviceId the serviceId
	 * @param connectorClassName the connector classname
	 * @param appointmentBookingEntry the appointmentBookingEntry
	 * @param externalAppointmentId the externalAppointmentId
	 * @return the created appointment entry
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
		addAppointmentEntry(
			long companyId, String serviceId, String connectorClassName,
			com.placecube.digitalplace.appointment.booking.model.
				AppointmentBookingEntry appointmentBookingEntry,
			String externalAppointmentId) {

		return _appointmentEntryLocalService.addAppointmentEntry(
			companyId, serviceId, connectorClassName, appointmentBookingEntry,
			externalAppointmentId);
	}

	/**
	 * Creates a new appointment entry with the primary key. Does not add the appointment entry to the database.
	 *
	 * @param appointmentEntryId the primary key for the new appointment entry
	 * @return the new appointment entry
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
		createAppointmentEntry(long appointmentEntryId) {

		return _appointmentEntryLocalService.createAppointmentEntry(
			appointmentEntryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentEntryLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Deletes the appointment entry from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentEntry the appointment entry
	 * @return the appointment entry that was removed
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
		deleteAppointmentEntry(
			com.placecube.digitalplace.appointment.model.AppointmentEntry
				appointmentEntry) {

		return _appointmentEntryLocalService.deleteAppointmentEntry(
			appointmentEntry);
	}

	/**
	 * Deletes the appointment entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry that was removed
	 * @throws PortalException if a appointment entry with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
			deleteAppointmentEntry(long appointmentEntryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentEntryLocalService.deleteAppointmentEntry(
			appointmentEntryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentEntryLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _appointmentEntryLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _appointmentEntryLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _appointmentEntryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _appointmentEntryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _appointmentEntryLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _appointmentEntryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _appointmentEntryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _appointmentEntryLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
		fetchAppointmentEntry(long appointmentEntryId) {

		return _appointmentEntryLocalService.fetchAppointmentEntry(
			appointmentEntryId);
	}

	/**
	 * Returns the appointment entry with the matching UUID and company.
	 *
	 * @param uuid the appointment entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching appointment entry, or <code>null</code> if a matching appointment entry could not be found
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
		fetchAppointmentEntryByUuidAndCompanyId(String uuid, long companyId) {

		return _appointmentEntryLocalService.
			fetchAppointmentEntryByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a random generated appointmentId, unique for the
	 * companyId-serviceId-connectorClassName values
	 *
	 * @param companyId the companyId
	 * @param serviceId the serviceId
	 * @param connectorClassName the connector classname
	 * @return the auto-generated id
	 */
	@Override
	public String generateExternalAppointmentId(
		long companyId, String serviceId, String connectorClassName) {

		return _appointmentEntryLocalService.generateExternalAppointmentId(
			companyId, serviceId, connectorClassName);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _appointmentEntryLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the appointment entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment entries
	 * @param end the upper bound of the range of appointment entries (not inclusive)
	 * @return the range of appointment entries
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.appointment.model.AppointmentEntry>
			getAppointmentEntries(int start, int end) {

		return _appointmentEntryLocalService.getAppointmentEntries(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.appointment.model.AppointmentEntry>
			getAppointmentEntries(
				long companyId, long classPK, long classNameId) {

		return _appointmentEntryLocalService.getAppointmentEntries(
			companyId, classPK, classNameId);
	}

	/**
	 * Returns the number of appointment entries.
	 *
	 * @return the number of appointment entries
	 */
	@Override
	public int getAppointmentEntriesCount() {
		return _appointmentEntryLocalService.getAppointmentEntriesCount();
	}

	/**
	 * Returns the appointment entry with the primary key.
	 *
	 * @param appointmentEntryId the primary key of the appointment entry
	 * @return the appointment entry
	 * @throws PortalException if a appointment entry with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
			getAppointmentEntry(long appointmentEntryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentEntryLocalService.getAppointmentEntry(
			appointmentEntryId);
	}

	/**
	 * Returns the appointment entry with the matching UUID and company.
	 *
	 * @param uuid the appointment entry's UUID
	 * @param companyId the primary key of the company
	 * @return the matching appointment entry
	 * @throws PortalException if a matching appointment entry could not be found
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
			getAppointmentEntryByUuidAndCompanyId(String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentEntryLocalService.
			getAppointmentEntryByUuidAndCompanyId(uuid, companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _appointmentEntryLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _appointmentEntryLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentEntryLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public void removeAppointmentEntry(
		long companyId, String serviceId, String connectorClassName,
		long classPK, long classNameId, String appointmentId) {

		_appointmentEntryLocalService.removeAppointmentEntry(
			companyId, serviceId, connectorClassName, classPK, classNameId,
			appointmentId);
	}

	/**
	 * Updates the appointment entry in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentEntryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentEntry the appointment entry
	 * @return the appointment entry that was updated
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentEntry
		updateAppointmentEntry(
			com.placecube.digitalplace.appointment.model.AppointmentEntry
				appointmentEntry) {

		return _appointmentEntryLocalService.updateAppointmentEntry(
			appointmentEntry);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _appointmentEntryLocalService.getBasePersistence();
	}

	@Override
	public AppointmentEntryLocalService getWrappedService() {
		return _appointmentEntryLocalService;
	}

	@Override
	public void setWrappedService(
		AppointmentEntryLocalService appointmentEntryLocalService) {

		_appointmentEntryLocalService = appointmentEntryLocalService;
	}

	private AppointmentEntryLocalService _appointmentEntryLocalService;

}