package com.placecube.digitalplace.search.web.portlet.contenttype.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.search.web.portlet.contenttype.configuration.ContentTypeFacetPortletInstanceConfiguration")
public interface ContentTypeFacetPortletInstanceConfiguration {

	@Meta.AD(required = false)
	String filterColour();

	@Meta.AD(required = false, deflt = "false")
	boolean joinFiltersWithTheSameName();

}
