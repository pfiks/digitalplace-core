package com.placecube.digitalplace.ddl.web.portlet.ddlsearch;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLSearchListingsPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.model.DDLDisplayEntry;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.service.DDLSearchListingsService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;
import com.placecube.digitalplace.search.shared.constants.SharedSearchDisplayCostants;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchDisplayService;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-ddl-search-listings", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=portlet-ddl-listings portlet-ddl-search-listings", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=true", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-request-attributes=false", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.restore-current-view=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/ddl-search/view.jsp", //
		"javax.portlet.init-param.config-template=/ddl-search/configuration.jsp", //
		"javax.portlet.name=" + PortletKeys.DDL_SEARCH_LISTINGS_PORTLET, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class DDLSearchListingsPortlet extends MVCPortlet {

	public static final String DISPLAY_STYLE_GEO_POINT = "geo-point";

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Reference
	private DDLSearchListingsService ddlSearchListingsService;

	@Reference
	private Portal portal;

	@Reference
	private SharedSearchDisplayService sharedSearchDisplayService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			DDLSearchListingsPortletInstanceConfiguration configuration = ddlConfigurationService.getDDLSearchListingsPortletInstanceConfiguration(themeDisplay);
			Optional<DynamicDataListMapping> dynamicDataListMapping = ddlConfigurationService.getDynamicDataListMappingForConfiguredStructure(themeDisplay.getCompanyId(), configuration);

			if (dynamicDataListMapping.isPresent()) {

				SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);
				DynamicDataListMapping mapping = dynamicDataListMapping.get();

				String displayStyle = sharedSearchDisplayService.getDisplayStyle(renderRequest, configuration.defaultDisplayStyle());
				boolean displayStyleListGeoPoint = DISPLAY_STYLE_GEO_POINT.equals(displayStyle);

				if (displayStyleListGeoPoint) {
					Optional<DDMTemplate> resultDisplayTemplateOpt = mapping.getSearchDefaultGeopointViewTemplate();
					if (resultDisplayTemplateOpt.isPresent()) {
						String geoPointContent = ddlSearchListingsService.getTransformedTemplate(searchResponse.getSearchResults(), mapping.getDDMStructure(), resultDisplayTemplateOpt.get(),
								themeDisplay, renderRequest);
						renderRequest.setAttribute("geoPointContent", geoPointContent);
						renderRequest.setAttribute("displayStyleListGeoPoint", displayStyleListGeoPoint);
					}
				} else {
					boolean displayStyleList = SharedSearchDisplayCostants.DISPLAY_STYLE_LIST.equals(displayStyle);
					Optional<DDMTemplate> resultDisplayTemplate = displayStyleList ? mapping.getSearchDefaultListViewTemplate() : mapping.getSearchDefaultGridViewTemplate();

					List<DDLDisplayEntry> results = ddlSearchListingsService.getDDLDisplayEntryResults(searchResponse.getSearchResults(), mapping.getDDMStructure(), resultDisplayTemplate,
							themeDisplay, portal.getHttpServletRequest(renderRequest));

					SearchContainer<DDLRecord> searchContainer = searchResponse.getSearchContainerForResults(renderResponse.getNamespace() + "_ddlListingsSearchContainer", results,
							"no-entries-were-found");

					renderRequest.setAttribute("gridDisplayStyleCss", GetterUtil.getString(configuration.gridDisplayStyleCss(), "col-md-4"));
					renderRequest.setAttribute("hidePagination", configuration.hidePagination());
					renderRequest.setAttribute("searchContainer", searchContainer);
					renderRequest.setAttribute("displayStyleList", displayStyleList);
				}

			} else {
				renderRequest.setAttribute("invalidConfiguration", true);
				renderRequest.setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
			}

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}