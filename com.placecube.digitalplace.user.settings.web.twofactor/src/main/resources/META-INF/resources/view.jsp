<%@ include file="init.jsp" %>

<h3 class="portlet-section-title">
	<liferay-ui:message key="two-factor-authentication" />
</h3>

<portlet:actionURL name="${ GENERATE_TWO_FACTOR_AUTHENTICATION }" var="generateTwoFactorAuthentication" />
<portlet:actionURL name="${ ENABLE_TWO_FACTOR_AUTHENTICATION }" var="enableTwoFactorAuthentication" />
<portlet:actionURL name="${ SAVE_TWO_FACTOR_AUTHENTICATION }" var="saveTwoFactorAuthentication" />

<div class="2fa-settings">

	<div class="portlet-helptext-section mb-4 mt-2">
		<liferay-journal:journal-article
			articleId="<%= TwoFactorAuthenticationKeys.WEBCONTENT_ARTICLE_ID %>"
			groupId="${themeDisplay.getCompanyGroupId()}"
			showTitle="false" />
	</div>

	<div class="portlet-section-content">
		<c:if test="${!twoFactorAuthenticationEnforced}">
			<aui:form action="${ enableTwoFactorAuthentication }" method="post" name="enableTwoFactorForm">
				<aui:input name="${ TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM}"
					label="enable-two-factor" helpMessage="enable-two-factor-help" type="toggle-switch"
					value="${twoFactorAuthenticationDisplayForm}" onChange="submitEnableTwoFactorForm()" />
			</aui:form>

			<aui:script>
				function submitEnableTwoFactorForm() {
					document.querySelector('form[id$=enableTwoFactorForm]').submit();
				}
			</aui:script>
		</c:if>

		<c:if test="${twoFactorAuthenticationDisplayForm}">
			<aui:form action="${ saveTwoFactorAuthentication }" method="post" name="saveTwoFactorAuthenticationForm">
				<div class="inline-alert-container lfr-alert-container"></div>
				<c:if test="${secretKeyGenerated}">
					<liferay-ui:error key="twoFactorVerificationFailed" message="code-not-verified" />
					<aui:input name="${SECRET_KEY}" type="hidden" value="${ secretKey }" />
					<aui:input label="authenticator-code" name="${ TWO_FACTOR_VERIFICATION_CODE }" type="input">
						<aui:validator name="required" />
						<aui:validator name="digits"></aui:validator>
						<aui:validator name="minLength">6</aui:validator>
						<aui:validator name="maxLength">8</aui:validator>
					</aui:input>
				</c:if>

				<div class="form-group">
					<aui-label for="secret-key"><liferay-ui:message key="secret-key" /></aui-label>
					<p style="word-break: break-word">${ secretKey }</p>

					<img alt="qrcode" src="${ qrUrl }" class="" width="166" height="166">

					<a href="${generateTwoFactorAuthentication}"><liferay-ui:message key="regenerate-secret-key" /></a>
				</div>

				<c:if test="${secretKeyGenerated}">
					<aui:button-row>
						<aui:button type="submit" value="verify-code" cssClass="btn-primary" />
					</aui:button-row>
				</c:if>
			</aui:form>
		</c:if>
	</div>
</div>
