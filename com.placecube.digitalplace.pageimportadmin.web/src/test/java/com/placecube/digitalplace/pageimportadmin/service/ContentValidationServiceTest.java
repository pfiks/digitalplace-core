package com.placecube.digitalplace.pageimportadmin.service;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.pageimportadmin.model.PageImportModel;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ LanguageUtil.class })
public class ContentValidationServiceTest extends PowerMockito {

	@InjectMocks
	private ContentValidationService contentValidationService;

	private static final Locale LOCALE = Locale.ENGLISH;
	private static final String MESSAGE = "message";
	private static final String LAYOUT_URL = "layoutURL";
	private static final long GROUP_ID = 10;
	private static final String PAGE_TEMLATE_NAME = "templateName";
	private static final String PARENT_URL = "parentLayout";
	private static final String TITLE = "titleValue";

	@Mock
	private ContentRetrievalService mockContentRetrievalService;

	@Mock
	private PageImportModel mockPageImportModel;

	@Mock
	private WebContentImportModel mockWebContentImportModel;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private Layout mockLayout;

	@Mock
	private Layout mockParentLayout;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Before
	public void setUp() {
		mockStatic(LanguageUtil.class);
	}

	@Test
	@Parameters({ "true", "false" })
	public void validatePage_WhenPageTitleIsNull_ThenAddsTitleRequiredAsErrorMessage(boolean privateLayout) {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockPageImportModel.getTitle()).thenReturn(null);
		mockLayoutDetails(privateLayout, false);
		when(LanguageUtil.get(LOCALE, "title-is-required")).thenReturn(MESSAGE);

		contentValidationService.validatePage(mockPageImportModel, mockServiceContext);

		verify(mockPageImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void validatePage_WhenLayoutIsFound_ThenAddsPageAlreadyExistsAsErrorMessage(boolean privateLayout) {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockPageImportModel.getTitle()).thenReturn(TITLE);
		mockLayoutDetails(privateLayout, true);
		when(LanguageUtil.get(LOCALE, "page-already-exists")).thenReturn(MESSAGE);

		contentValidationService.validatePage(mockPageImportModel, mockServiceContext);

		verify(mockPageImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void validatePage_WhenPageTemplateNameIsSetAndNoPageTemplateIsFound_ThenAddsNoPageTemplateFoundAsWarningMessage(boolean privateLayout) {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockPageImportModel.getTitle()).thenReturn(TITLE);
		mockLayoutDetails(privateLayout, false);
		when(mockPageImportModel.getPageTemplateName()).thenReturn(PAGE_TEMLATE_NAME);
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.empty());
		when(LanguageUtil.get(LOCALE, "no-pagetemplate-found")).thenReturn(MESSAGE);

		contentValidationService.validatePage(mockPageImportModel, mockServiceContext);

		verify(mockPageImportModel, times(1)).addWarningMessage(MESSAGE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void validatePage_WhenParentPageURLIsSetAndNoParentPageIsFound_ThenAddsNoParentLayoutFoundAsWarningMessage(boolean privateLayout) {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockPageImportModel.getTitle()).thenReturn(TITLE);
		mockLayoutDetails(privateLayout, false);
		when(mockPageImportModel.getParentPageURL()).thenReturn(PARENT_URL);
		when(mockPageImportModel.getParentLayout()).thenReturn(Optional.empty());
		when(LanguageUtil.get(LOCALE, "no-parent-layout-found")).thenReturn(MESSAGE);

		contentValidationService.validatePage(mockPageImportModel, mockServiceContext);

		verify(mockPageImportModel, times(1)).addWarningMessage(MESSAGE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void validatePage_WhenTitleIsValidAndLayoutNotFoundAndPageTemplateNotSpecifiedAndParentURLNotSpecified_ThenDoesNotAddAnyErrorMessage(boolean privateLayout) {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockPageImportModel.getTitle()).thenReturn(TITLE);
		mockLayoutDetails(privateLayout, false);

		contentValidationService.validatePage(mockPageImportModel, mockServiceContext);

		verify(mockPageImportModel, never()).addErrorMessage(anyString());
		verify(mockPageImportModel, never()).addWarningMessage(anyString());
	}

	@Test
	@Parameters({ "true", "false" })
	public void validatePage_WhenTitleIsValidAndLayoutNotFoundAndPageTemplateSpecifiedAndPageTemplateFoundAndParentURLNotSpecified_ThenDoesNotAddAnyErrorMessage(boolean privateLayout) {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockPageImportModel.getTitle()).thenReturn(TITLE);
		mockLayoutDetails(privateLayout, false);
		when(mockPageImportModel.getPageTemplateName()).thenReturn(PAGE_TEMLATE_NAME);
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.of(mockLayoutPageTemplateEntry));

		contentValidationService.validatePage(mockPageImportModel, mockServiceContext);

		verify(mockPageImportModel, never()).addErrorMessage(anyString());
		verify(mockPageImportModel, never()).addWarningMessage(anyString());
	}

	@Test
	@Parameters({ "true", "false" })
	public void validatePage_WhenTitleIsValidAndLayoutNotFoundAndPageTemplateNotSpecifiedAndParentURLSpecifiedAndParentLayoutFound_ThenDoesNotAddAnyErrorMessage(boolean privateLayout) {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockPageImportModel.getTitle()).thenReturn(TITLE);
		mockLayoutDetails(privateLayout, false);
		when(mockPageImportModel.getParentPageURL()).thenReturn(PARENT_URL);
		when(mockPageImportModel.getParentLayout()).thenReturn(Optional.of(mockParentLayout));

		contentValidationService.validatePage(mockPageImportModel, mockServiceContext);

		verify(mockPageImportModel, never()).addErrorMessage(anyString());
		verify(mockPageImportModel, never()).addWarningMessage(anyString());
	}

	@Test
	@Parameters({ "true", "false" })
	public void validatePage_WhenTitleIsValidAndLayoutNotFoundAndPageTemplateSpecifiedAndPageTemplateFoundAndParentURLSpecifiedAndParentLayoutFound_ThenDoesNotAddAnyErrorMessage(
			boolean privateLayout) {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockPageImportModel.getTitle()).thenReturn(TITLE);
		mockLayoutDetails(privateLayout, false);
		when(mockPageImportModel.getPageTemplateName()).thenReturn(PAGE_TEMLATE_NAME);
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.of(mockLayoutPageTemplateEntry));
		when(mockPageImportModel.getParentPageURL()).thenReturn(PARENT_URL);
		when(mockPageImportModel.getParentLayout()).thenReturn(Optional.of(mockParentLayout));

		contentValidationService.validatePage(mockPageImportModel, mockServiceContext);

		verify(mockPageImportModel, never()).addErrorMessage(anyString());
		verify(mockPageImportModel, never()).addWarningMessage(anyString());
	}

	@Test
	public void validateWebContent_WhenTitleIsNull_ThenAddsTitleRequiredAsErrorMessage() {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockWebContentImportModel.getTitle()).thenReturn(null);
		when(LanguageUtil.get(LOCALE, "title-is-required")).thenReturn(MESSAGE);
		when(mockWebContentImportModel.getWebContentStructure()).thenReturn(Optional.of(mockDDMStructure));

		contentValidationService.validateWebContent(mockWebContentImportModel, mockServiceContext);

		verify(mockWebContentImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void validateWebContent_WhenWebContentStructureIsNotFound_ThenAddsNoStructureFoundAsErrorMessage() {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockWebContentImportModel.getTitle()).thenReturn(TITLE);
		when(mockWebContentImportModel.getWebContentStructure()).thenReturn(Optional.empty());
		when(LanguageUtil.get(LOCALE, "no-structure-found")).thenReturn(MESSAGE);

		contentValidationService.validateWebContent(mockWebContentImportModel, mockServiceContext);

		verify(mockWebContentImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void validateWebContent_WhenArticleWithURLTitleAlreadyExists_ThenAddsWebContentAlreadyExistsAsErrorMessage() {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockWebContentImportModel.getTitle()).thenReturn(TITLE);
		when(mockWebContentImportModel.getWebContentStructure()).thenReturn(Optional.of(mockDDMStructure));
		when(mockContentRetrievalService.doesArticleWithURLTitleExists(mockWebContentImportModel)).thenReturn(true);
		when(LanguageUtil.get(LOCALE, "webcontent-already-exists")).thenReturn(MESSAGE);

		contentValidationService.validateWebContent(mockWebContentImportModel, mockServiceContext);

		verify(mockWebContentImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void validateWebContent_WhenTitleIsValidAndStructureIsFoundAndArticleWithURLTitleDoesNotAlreadyExists_ThenDoesNotAddAnyErrorOrWarningMessages() {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockWebContentImportModel.getTitle()).thenReturn(TITLE);
		when(mockWebContentImportModel.getWebContentStructure()).thenReturn(Optional.of(mockDDMStructure));
		when(mockContentRetrievalService.doesArticleWithURLTitleExists(mockWebContentImportModel)).thenReturn(false);

		contentValidationService.validateWebContent(mockWebContentImportModel, mockServiceContext);

		verify(mockWebContentImportModel, never()).addErrorMessage(anyString());
		verify(mockWebContentImportModel, never()).addWarningMessage(anyString());
	}

	private void mockLayoutDetails(boolean privateLayout, boolean layoutFound) {
		when(mockPageImportModel.getGroupId()).thenReturn(GROUP_ID);
		when(mockPageImportModel.getUrl()).thenReturn(LAYOUT_URL);
		when(mockPageImportModel.isPrivateLayout()).thenReturn(privateLayout);
		if (layoutFound) {
			when(mockContentRetrievalService.getLayout(GROUP_ID, LAYOUT_URL, privateLayout)).thenReturn(Optional.of(mockLayout));
		} else {
			when(mockContentRetrievalService.getLayout(GROUP_ID, LAYOUT_URL, privateLayout)).thenReturn(Optional.empty());
		}
	}
}
