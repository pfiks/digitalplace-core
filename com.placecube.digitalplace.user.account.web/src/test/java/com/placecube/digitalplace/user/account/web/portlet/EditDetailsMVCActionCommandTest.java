package com.placecube.digitalplace.user.account.web.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ SessionMessages.class, PortalUtil.class, ParamUtil.class, PropsUtil.class, SessionErrors.class })
public class EditDetailsMVCActionCommandTest extends PowerMockito {

	private final long COMPANY_ID = 2L;

	@InjectMocks
	private EditDetailsMVCActionCommand editDetailsMVCActionCommand;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private AccountService mockAccountService;

	@Mock
	private AccountValidationService mockAccountValidationService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Before
	public void activateSetUp() {
		mockStatic(SessionMessages.class, PortalUtil.class, PropsUtil.class, ParamUtil.class, SessionErrors.class);
	}

	@Test(expected = UserAccountException.class)
	public void doProcessAction_WhenHasPermissionIsTrueAndAdditionalDetailsValidationThrowsException_ThenPortalExceptionIsThrown() throws Exception {
		mockBasicSetup(true);
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);
		doThrow(new UserAccountException()).when(mockAccountValidationService).validateAdditionalDetails(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration,
				mockUserProfileFieldsConfiguration);

		editDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenHasPermissionIsTrueAndHasErrorsIsFalse_ThenSetsRenderParameters(boolean addressOutOfLocalArea) throws Exception {
		mockBasicSetup(true);
		String redirectUrl = "redirect url";
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(addressOutOfLocalArea);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContext.hasErrors()).thenReturn(false);
		when(ParamUtil.getString(mockActionRequest, RequestKeys.REDIRECT_URL)).thenReturn(redirectUrl);

		editDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountContextService, mockAccountContext, mockAccountValidationService, mockAccountService, mockMutableRenderParameters);
		inOrder.verify(mockAccountContextService, times(1)).configurePersonalDetails(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountContextService, times(1)).configureAdditionalDetails(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountContextService, times(1)).configureAddressDetails(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountContextService, times(1)).configureAccountFields(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountContextService, times(1)).configureExpandoFields(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountContext, times(1)).clearErrors();
		inOrder.verify(mockAccountValidationService, times(1)).validatePersonalDetails(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);
		inOrder.verify(mockAccountValidationService, times(1)).validateAdditionalDetails(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);
		inOrder.verify(mockAccountService, times(1)).updateUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.REDIRECT_URL, redirectUrl);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.CLOSE_POPUP);

	}

	@Test
	public void doProcessAction_WhenHasPermissionIsTrueAndHasErrorsIsTrue_ThenSetMVCRenderCommandNameWithEditDetails() throws Exception {
		mockBasicSetup(true);
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountContext.hasErrors()).thenReturn(true);

		editDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.EDIT_DETAILS);
		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockActionRequest);

	}

	@Test(expected = UserAccountException.class)
	public void doProcessAction_WhenHasPermissionIsTrueAndPersonalDetailsValidationThrowsException_ThenPortalExceptionIsThrown() throws Exception {
		mockBasicSetup(true);
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		doThrow(new UserAccountException()).when(mockAccountValidationService).validatePersonalDetails(mockAccountContext, mockActionRequest, mockCreateAccountConfiguration,
				mockUserProfileFieldsConfiguration);

		editDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}

	@Test
	public void doProcessAction_WhenHasPermissionsIsFalse_ThenAddNoPermissionsToSessionErrors() throws Exception {
		mockBasicSetup(false);

		editDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "no-permissions");
	}

	@Test
	public void doProcessAction_WhenHasPermissionsIsFalse_ThenSetMVCRenderCommandNameWithEditDetails() throws Exception {
		mockBasicSetup(false);

		editDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.EDIT_DETAILS);
	}

	private void mockBasicSetup(boolean editPermitted) {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPermissionChecker()).thenReturn(mockPermissionChecker);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockPermissionChecker.hasPermission(null, User.class.getName(), User.class.getName(), ActionKeys.VIEW)).thenReturn(editPermitted);
	}

}
