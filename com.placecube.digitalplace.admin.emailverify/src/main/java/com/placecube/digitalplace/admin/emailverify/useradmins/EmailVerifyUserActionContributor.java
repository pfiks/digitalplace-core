package com.placecube.digitalplace.admin.emailverify.useradmins;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.users.admin.user.action.contributor.BaseUserActionContributor;
import com.liferay.users.admin.user.action.contributor.UserActionContributor;
import com.placecube.digitalplace.admin.emailverify.service.EmailVerifyTicketService;
import com.placecube.digitalplace.admin.emailverify.service.EmailVerifyUrlService;

@Component(immediate = true, service = UserActionContributor.class)
public class EmailVerifyUserActionContributor extends BaseUserActionContributor {

	@Reference
	private EmailVerifyTicketService emailVerifyTicketService;

	@Reference
	private EmailVerifyUrlService emailVerifyUrlService;

	@Override
	public String getConfirmationMessage(PortletRequest portletRequest) {
		return LanguageUtil.get(getResourceBundle(getLocale(portletRequest)), "are-you-sure-you-want-to-verify-users-email-address");
	}

	@Override
	public String getMessage(PortletRequest portletRequest) {
		return LanguageUtil.get(getResourceBundle(getLocale(portletRequest)), "verify-email-address");
	}

	@Override
	public String getURL(PortletRequest portletRequest, PortletResponse portletResponse, User user, User selUser) {
		return emailVerifyUrlService.getVerifyEmailURL(portletResponse, selUser);
	}

	@Override
	public boolean isShow(PortletRequest portletRequest, User user, User selUser) {
		return emailVerifyTicketService.doesUserHavePendingTickets(selUser.getCompanyId(), selUser.getUserId());
	}

	@Override
	public boolean isShowConfirmationMessage(User selUser) {
		return true;
	}

}
