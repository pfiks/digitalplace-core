/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.addresslookup.simple.service.impl;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.aop.AopService;
import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;
import com.placecube.digitalplace.addresslookup.simple.service.base.SimpleAddressLookupEntryLocalServiceBaseImpl;
import com.placecube.digitalplace.addresslookup.simple.service.persistence.SimpleAddressLookupEntryPK;

/**
 * @author Brian Wing Shun Chan
 */
@Component(property = "model.class.name=com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry", service = AopService.class)
public class SimpleAddressLookupEntryLocalServiceImpl extends SimpleAddressLookupEntryLocalServiceBaseImpl {

	@Override
	public List<SimpleAddressLookupEntry> getByCompanyIdAndPostcode(long companyId, String postcode) {
		return simpleAddressLookupEntryPersistence.findByCompanyId_Postcode(companyId, postcode);
	}

	@Override
	public Optional<SimpleAddressLookupEntry> getByCompanyIdAndUprn(long companyId, String uprn) {
		SimpleAddressLookupEntryPK pk = new SimpleAddressLookupEntryPK(companyId, uprn);
		return Optional.ofNullable(simpleAddressLookupEntryPersistence.fetchByPrimaryKey(pk));
	}

}