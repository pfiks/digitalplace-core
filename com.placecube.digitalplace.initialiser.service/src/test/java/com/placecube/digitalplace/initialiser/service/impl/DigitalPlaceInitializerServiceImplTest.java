package com.placecube.digitalplace.initialiser.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.UserActiveException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.initialiser.service.constants.ConfigurationKeys;
import com.placecube.digitalplace.initialiser.service.util.PageCreationUtil;
import com.placecube.digitalplace.initialiser.service.util.PageTemplateCreationUtil;
import com.placecube.digitalplace.initialiser.service.util.ParserUtil;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.LayoutInitializer;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@SuppressWarnings("rawtypes")
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DigitalPlaceInitializerServiceImplTest extends PowerMockito {

	private static final String FILE_PATH = "myFilePath";
	private static final String FRIENDLY_URL = "/my-page";
	private static final long GROUP_ID = 456;
	private static final long ID = 123;
	private static final String PAGE_TEMPLATE_NAME = "template";
	private static final String PAGE_TITLE = "myPage";

	@InjectMocks
	private DigitalPlaceInitializerServiceImpl digitalPlaceInitializerServiceImpl;

	@Mock
	private Set<FragmentContext> mockFragmentContextSet;

	@Mock
	private JSONArray mockJSONArray1;

	@Mock
	private JSONArray mockJSONArray2;

	@Mock
	private JSONArray mockJSONArray3;

	@Mock
	private JSONArray mockJSONArrayPageTypeSettings;

	@Mock
	private JSONObject mockJSONObjectPage;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutContext mockLayoutContext;

	@Mock
	private LayoutInitializer mockLayoutInitializer;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Mock
	private LayoutPageTemplateEntryLocalService mockLayoutPageTemplateEntryLocalService;

	@Mock
	private Layout mockLayoutUpdated;

	@Mock
	private PageCreationUtil mockPageCreationUtil;

	@Mock
	private PageTemplateCreationUtil mockPageTemplateCreationUtil;

	@Mock
	private Map<String, String> mockPageTypeSettings;

	@Mock
	private ParserUtil mockParserUtil;

	@Mock
	private Map<String, String[]> mockPermissionsMap;

	@Mock
	private Set<PortletContext> mockPortletContextSet;

	@Mock
	private Map<Integer, Set<PortletContext>> mockPortletsMap;

	@Mock
	private Map<String, String> mockPrefsPlaceholders;

	@Mock
	private ServiceContext mockServiceContext;

	@Test(expected = PortalException.class)
	public void configurePageAsFirstLayout_WhenException_ThenThrowsPortalException() throws PortalException {
		when(mockLayoutLocalService.updatePriority(mockLayout, 0)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.configurePageAsFirstLayout(mockLayout);
	}

	@Test
	public void configurePageAsFirstLayout_WhenNoError_ThenReturnsTheUpdatedLayoutWithPriorityZero() throws PortalException {
		when(mockLayoutLocalService.updatePriority(mockLayout, 0)).thenReturn(mockLayoutUpdated);

		Layout result = digitalPlaceInitializerServiceImpl.configurePageAsFirstLayout(mockLayout);

		assertThat(result, sameInstance(mockLayoutUpdated));
	}

	@Test(expected = PortalException.class)
	public void initializeContentPage_WhenExceptionCreatingTheLayout_ThenThrowsPortalException() throws PortalException {
		when(mockServiceContext.getScopeGroupId()).thenReturn(ID);
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockJSONObjectPage.getJSONArray("fragments")).thenReturn(mockJSONArray1);
		when(mockJSONObjectPage.getJSONArray("portlets")).thenReturn(mockJSONArray2);
		when(mockJSONObjectPage.getJSONArray("contentSetPlaceholders")).thenReturn(mockJSONArray3);
		when(mockPageCreationUtil.getFragments(mockJSONArray1)).thenReturn(mockFragmentContextSet);
		when(mockPageCreationUtil.getPortlets(ID, mockJSONArray2, mockJSONArray3, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletContextSet);
		when(mockJSONObjectPage.getJSONArray("pageTypeSettings")).thenReturn(mockJSONArrayPageTypeSettings);
		when(mockParserUtil.getNameValueMap(mockJSONArrayPageTypeSettings, mockPrefsPlaceholders)).thenReturn(mockPageTypeSettings);
		when(mockPageCreationUtil.getContentPageToCreate(classForPath, mockJSONObjectPage, mockFragmentContextSet, mockPortletContextSet, mockPageTypeSettings)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreateContentLayout(mockLayoutContext, mockServiceContext)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeContentPage(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializeContentPage_WhenExceptionRetrievingJSONObject_ThenThrowsPortalException() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeContentPage(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);
	}

	@Test
	public void initializeContentPage_WhenNoError_ThenCreatesTheContentLayout() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockServiceContext.getScopeGroupId()).thenReturn(ID);
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockJSONObjectPage.getJSONArray("fragments")).thenReturn(mockJSONArray1);
		when(mockJSONObjectPage.getJSONArray("portlets")).thenReturn(mockJSONArray2);
		when(mockJSONObjectPage.getJSONArray("contentSetPlaceholders")).thenReturn(mockJSONArray3);
		when(mockPageCreationUtil.getFragments(mockJSONArray1)).thenReturn(mockFragmentContextSet);
		when(mockPageCreationUtil.getPortlets(ID, mockJSONArray2, mockJSONArray3, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletContextSet);
		when(mockJSONObjectPage.getJSONArray("pageTypeSettings")).thenReturn(mockJSONArrayPageTypeSettings);
		when(mockParserUtil.getNameValueMap(mockJSONArrayPageTypeSettings, mockPrefsPlaceholders)).thenReturn(mockPageTypeSettings);
		when(mockPageCreationUtil.getContentPageToCreate(classForPath, mockJSONObjectPage, mockFragmentContextSet, mockPortletContextSet, mockPageTypeSettings)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreateContentLayout(mockLayoutContext, mockServiceContext)).thenReturn(mockLayout);

		Layout result = digitalPlaceInitializerServiceImpl.initializeContentPage(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);

		assertThat(result, sameInstance(mockLayout));
	}

	@Test(expected = PortalException.class)
	public void initializeContentPage_WithJSONObject_WhenExceptionCreatingTheLayout_ThenThrowsPortalException() throws PortalException {
		when(mockServiceContext.getScopeGroupId()).thenReturn(ID);
		Class classForPath = UserActiveException.class.getClass();
		when(mockJSONObjectPage.getJSONArray("fragments")).thenReturn(mockJSONArray1);
		when(mockJSONObjectPage.getJSONArray("portlets")).thenReturn(mockJSONArray2);
		when(mockJSONObjectPage.getJSONArray("contentSetPlaceholders")).thenReturn(mockJSONArray3);
		when(mockPageCreationUtil.getFragments(mockJSONArray1)).thenReturn(mockFragmentContextSet);
		when(mockPageCreationUtil.getPortlets(ID, mockJSONArray2, mockJSONArray3, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletContextSet);
		when(mockJSONObjectPage.getJSONArray("pageTypeSettings")).thenReturn(mockJSONArrayPageTypeSettings);
		when(mockParserUtil.getNameValueMap(mockJSONArrayPageTypeSettings, mockPrefsPlaceholders)).thenReturn(mockPageTypeSettings);
		when(mockPageCreationUtil.getContentPageToCreate(classForPath, mockJSONObjectPage, mockFragmentContextSet, mockPortletContextSet, mockPageTypeSettings)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreateContentLayout(mockLayoutContext, mockServiceContext)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeContentPage(classForPath, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext);
	}

	@Test
	public void initializeContentPage_WithJSONObject_WhenNoError_ThenCreatesTheContentLayout() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockServiceContext.getScopeGroupId()).thenReturn(ID);
		when(mockJSONObjectPage.getJSONArray("fragments")).thenReturn(mockJSONArray1);
		when(mockJSONObjectPage.getJSONArray("portlets")).thenReturn(mockJSONArray2);
		when(mockJSONObjectPage.getJSONArray("contentSetPlaceholders")).thenReturn(mockJSONArray3);
		when(mockPageCreationUtil.getFragments(mockJSONArray1)).thenReturn(mockFragmentContextSet);
		when(mockPageCreationUtil.getPortlets(ID, mockJSONArray2, mockJSONArray3, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletContextSet);
		when(mockJSONObjectPage.getJSONArray("pageTypeSettings")).thenReturn(mockJSONArrayPageTypeSettings);
		when(mockParserUtil.getNameValueMap(mockJSONArrayPageTypeSettings, mockPrefsPlaceholders)).thenReturn(mockPageTypeSettings);
		when(mockPageCreationUtil.getContentPageToCreate(classForPath, mockJSONObjectPage, mockFragmentContextSet, mockPortletContextSet, mockPageTypeSettings)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreateContentLayout(mockLayoutContext, mockServiceContext)).thenReturn(mockLayout);

		Layout result = digitalPlaceInitializerServiceImpl.initializeContentPage(classForPath, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext);

		assertThat(result, sameInstance(mockLayout));
	}

	@Test(expected = PortalException.class)
	public void initializeLinkToLayoutPage_WhenExceptionCreatingTheLayout_ThenThrowsPortalException() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockPageCreationUtil.getLinkToLayoutPageToCreate(classForPath, mockJSONObjectPage)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreateLinkToPageLayout(mockLayoutContext, mockServiceContext)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeLinkToLayoutPage(classForPath, FILE_PATH, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializeLinkToLayoutPage_WhenExceptionRetrievingJSONObject_ThenThrowsPortalException() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeLinkToLayoutPage(classForPath, FILE_PATH, mockServiceContext);
	}

	@Test
	public void initializeLinkToLayoutPage_WhenNoError_ThenCreatesTheLinkToLayoutPage() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockPageCreationUtil.getLinkToLayoutPageToCreate(classForPath, mockJSONObjectPage)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreateLinkToPageLayout(mockLayoutContext, mockServiceContext)).thenReturn(mockLayout);

		Layout result = digitalPlaceInitializerServiceImpl.initializeLinkToLayoutPage(classForPath, FILE_PATH, mockServiceContext);

		assertThat(result, sameInstance(mockLayout));
	}

	@Test
	@Parameters({ "true", "false" })
	public void initializePageFromPageTemplate_WithPageTitleAndFriendlyUrlAndPrivateAndPageTemplateEntryAndPermissionsMapAndServiceContext_WhenNoError_ThenReturnsThePageTemplate(boolean isPrivate)
			throws PortalException {
		when(mockPageCreationUtil.getPageFromPageTemplateToCreate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext)).thenReturn(mockLayout);

		Layout result = digitalPlaceInitializerServiceImpl.initializePageFromPageTemplate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry, mockPermissionsMap, mockServiceContext);

		assertThat(result, equalTo(mockLayout));
		InOrder inOrder = inOrder(mockLayoutInitializer, mockLayoutContext);
		inOrder.verify(mockLayoutContext, times(1)).setRolePermissionsToAdd(mockPermissionsMap);
		inOrder.verify(mockLayoutInitializer, times(1)).getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void initializePageFromPageTemplate_WithPageTitleAndFriendlyUrlAndPrivateAndPageTemplateEntryAndPermissionsMapServiceContext_WhenErrorOnGettingThePageTemplate_ThenThrowsPortalException(
			boolean isPrivate) throws PortalException {
		when(mockPageCreationUtil.getPageFromPageTemplateToCreate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext)).thenThrow(new PortalException(""));

		digitalPlaceInitializerServiceImpl.initializePageFromPageTemplate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry, mockPermissionsMap, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void initializePageFromPageTemplate_WithPageTitleAndFriendlyUrlAndPrivateAndPageTemplateEntryAndServiceContext_WhenErrorOnGettingThePageTemplate_ThenThrowsPortalException(boolean isPrivate)
			throws PortalException {
		when(mockPageCreationUtil.getPageFromPageTemplateToCreate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext)).thenThrow(new PortalException(""));

		digitalPlaceInitializerServiceImpl.initializePageFromPageTemplate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void initializePageFromPageTemplate_WithPageTitleAndFriendlyUrlAndPrivateAndPageTemplateEntryAndServiceContext_WhenNoError_ThenReturnsThePageTemplate(boolean isPrivate)
			throws PortalException {
		when(mockPageCreationUtil.getPageFromPageTemplateToCreate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext)).thenReturn(mockLayout);

		Layout result = digitalPlaceInitializerServiceImpl.initializePageFromPageTemplate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry, mockServiceContext);

		assertThat(result, equalTo(mockLayout));
		InOrder inOrder = inOrder(mockLayoutInitializer, mockLayoutContext);
		inOrder.verify(mockLayoutContext, times(1)).setRolePermissionsToAdd(Collections.emptyMap());
		inOrder.verify(mockLayoutInitializer, times(1)).getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void initializePageFromPageTemplate_WithPageTitleMapAndFriendlyURLAndPrivateAndPageTemplateEntryAndServiceContextParameters_WhenErrorOnGettingThePageTemplate_ThenThrowsPortalException(
			boolean isPrivate) throws PortalException {
		Map<Locale, String> pageTitleMap = new LinkedHashMap<>();
		pageTitleMap.put(Locale.ENGLISH, PAGE_TITLE);
		pageTitleMap.put(Locale.CANADA_FRENCH, "anotherValue");
		when(mockPageCreationUtil.getPageFromPageTemplateToCreate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext)).thenThrow(new PortalException(""));
		digitalPlaceInitializerServiceImpl.initializePageFromPageTemplate(pageTitleMap, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void initializePageFromPageTemplate_WithPageTitleMapAndFriendlyURLAndPrivateAndPageTemplateEntryAndServiceContextParameters_WhenNoError_ThenReturnsThePageTemplateCreatedWithTheLocalisedNameAndTitle(
			boolean isPrivate) throws PortalException {
		Map<Locale, String> pageTitleMap = new LinkedHashMap<>();
		pageTitleMap.put(Locale.ENGLISH, PAGE_TITLE);
		pageTitleMap.put(Locale.CANADA_FRENCH, "anotherValue");
		when(mockPageCreationUtil.getPageFromPageTemplateToCreate(PAGE_TITLE, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext)).thenReturn(mockLayout);
		Layout result = digitalPlaceInitializerServiceImpl.initializePageFromPageTemplate(pageTitleMap, FRIENDLY_URL, isPrivate, mockLayoutPageTemplateEntry, mockServiceContext);
		assertThat(result, equalTo(mockLayout));
		InOrder inOrder = inOrder(mockLayoutInitializer, mockLayoutContext);
		inOrder.verify(mockLayoutContext, times(1)).addLocalisedName(Locale.ENGLISH, PAGE_TITLE);
		inOrder.verify(mockLayoutContext, times(1)).addLocalisedTitle(Locale.ENGLISH, PAGE_TITLE);
		inOrder.verify(mockLayoutContext, times(1)).addLocalisedName(Locale.CANADA_FRENCH, "anotherValue");
		inOrder.verify(mockLayoutContext, times(1)).addLocalisedTitle(Locale.CANADA_FRENCH, "anotherValue");
		inOrder.verify(mockLayoutInitializer, times(1)).getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true,false", "false,true" })
	public void initializePageTemplate_WhenErrorOnCreatingPageTemplate_ThenThrowsPortalException(boolean displayPage, boolean widgetPage) throws PortalException {
		Class classForPath = this.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockJSONObjectPage.getString("pageTemplateName")).thenReturn(PAGE_TEMPLATE_NAME);
		when(mockLayoutPageTemplateEntryLocalService.fetchLayoutPageTemplateEntry(GROUP_ID, PAGE_TEMPLATE_NAME)).thenReturn(null);
		when(mockPageCreationUtil.getPageTemplateToCreate(mockJSONObjectPage)).thenReturn(mockLayoutContext);
		when(mockJSONObjectPage.getBoolean("displayPageTemplate")).thenReturn(displayPage);
		when(mockJSONObjectPage.getBoolean("widgetPageTemplate")).thenReturn(widgetPage);
		if (displayPage) {
			when(mockPageTemplateCreationUtil.createDisplayPageTemplate(mockLayoutContext, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext)).thenThrow(new PortalException(""));
		} else if (widgetPage) {
			when(mockPageTemplateCreationUtil.createWidgetPageTemplate(mockLayoutContext, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext)).thenThrow(new PortalException(""));
		} else {
			when(mockPageTemplateCreationUtil.createContentPageTemplate(mockLayoutContext, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext)).thenThrow(new PortalException(""));
		}

		digitalPlaceInitializerServiceImpl.initializePageTemplate(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializePageTemplate_WhenErrorOnGettingTemplateJSONObject_ThenThrowsPortalException() throws PortalException {
		Class classForPath = this.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenThrow(new PortalException(""));

		digitalPlaceInitializerServiceImpl.initializePageTemplate(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);
	}

	@Test()
	public void initializePageTemplate_WhenLayoutPageTemplateEntryExists_ThenReturnsFetchedTemplate() throws PortalException {
		Class classForPath = this.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockJSONObjectPage.getString("pageTemplateName")).thenReturn(PAGE_TEMPLATE_NAME);
		when(mockLayoutPageTemplateEntryLocalService.fetchLayoutPageTemplateEntry(GROUP_ID, PAGE_TEMPLATE_NAME)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = digitalPlaceInitializerServiceImpl.initializePageTemplate(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);

		assertThat(result, equalTo(mockLayoutPageTemplateEntry));
	}

	@Test
	public void initializePageTemplate_WhenNoErrorAndContentPageTemplate_ThenReturnsCreatedContentPageTemplate() throws PortalException {
		Class classForPath = this.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockJSONObjectPage.getString("pageTemplateName")).thenReturn(PAGE_TEMPLATE_NAME);
		when(mockLayoutPageTemplateEntryLocalService.fetchLayoutPageTemplateEntry(GROUP_ID, PAGE_TEMPLATE_NAME)).thenReturn(null);
		when(mockPageCreationUtil.getPageTemplateToCreate(mockJSONObjectPage)).thenReturn(mockLayoutContext);
		when(mockJSONObjectPage.getBoolean("displayPageTemplate")).thenReturn(false);
		when(mockJSONObjectPage.getBoolean("widgetPageTemplate")).thenReturn(false);
		when(mockPageTemplateCreationUtil.createContentPageTemplate(mockLayoutContext, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = digitalPlaceInitializerServiceImpl.initializePageTemplate(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);

		assertThat(result, equalTo(mockLayoutPageTemplateEntry));

	}

	@Test
	public void initializePageTemplate_WhenNoErrorAndDisplayPageTemplate_ThenReturnsCreatedDisplayPageTemplate() throws PortalException {
		Class classForPath = this.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockJSONObjectPage.getString("pageTemplateName")).thenReturn(PAGE_TEMPLATE_NAME);
		when(mockLayoutPageTemplateEntryLocalService.fetchLayoutPageTemplateEntry(GROUP_ID, PAGE_TEMPLATE_NAME)).thenReturn(null);
		when(mockPageCreationUtil.getPageTemplateToCreate(mockJSONObjectPage)).thenReturn(mockLayoutContext);
		when(mockJSONObjectPage.getBoolean("displayPageTemplate")).thenReturn(true);
		when(mockJSONObjectPage.getBoolean("widgetPageTemplate")).thenReturn(false);
		when(mockPageTemplateCreationUtil.createDisplayPageTemplate(mockLayoutContext, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = digitalPlaceInitializerServiceImpl.initializePageTemplate(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);

		assertThat(result, equalTo(mockLayoutPageTemplateEntry));

	}

	@Test
	public void initializePageTemplate_WhenNoErrorAndWidgetPageTemplate_ThenReturnsCreatedWidgetPageTemplate() throws PortalException {
		Class classForPath = this.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockJSONObjectPage.getString("pageTemplateName")).thenReturn(PAGE_TEMPLATE_NAME);
		when(mockLayoutPageTemplateEntryLocalService.fetchLayoutPageTemplateEntry(GROUP_ID, PAGE_TEMPLATE_NAME)).thenReturn(null);
		when(mockPageCreationUtil.getPageTemplateToCreate(mockJSONObjectPage)).thenReturn(mockLayoutContext);
		when(mockJSONObjectPage.getBoolean("displayPageTemplate")).thenReturn(false);
		when(mockJSONObjectPage.getBoolean("widgetPageTemplate")).thenReturn(true);
		when(mockPageTemplateCreationUtil.createWidgetPageTemplate(mockLayoutContext, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = digitalPlaceInitializerServiceImpl.initializePageTemplate(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);

		assertThat(result, equalTo(mockLayoutPageTemplateEntry));

	}

	@Test(expected = PortalException.class)
	public void initializeWidgetPage_WithClassAndFilePathAndPlaceholdersAndServiceContextParameters_WhenExceptionCreatingTheLayout_ThenThrowsPortalException() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockJSONObjectPage.getJSONArray("columns")).thenReturn(mockJSONArray1);
		when(mockPageCreationUtil.getColumns(mockJSONArray1, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletsMap);
		when(mockPageCreationUtil.getWidgetPageToCreate(classForPath, mockJSONObjectPage, mockPortletsMap)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeWidgetPage(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializeWidgetPage_WithClassAndFilePathAndPlaceholdersAndServiceContextParameters_WhenExceptionRetrievingJSONObject_ThenThrowsPortalException() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeWidgetPage(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);
	}

	@Test
	public void initializeWidgetPage_WithClassAndFilePathAndPlaceholdersAndServiceContextParameters_WhenNoError_ThenCreatesThePortletLayout() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockJSONObjectPage.getJSONArray("columns")).thenReturn(mockJSONArray1);
		when(mockPageCreationUtil.getColumns(mockJSONArray1, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletsMap);
		when(mockPageCreationUtil.getWidgetPageToCreate(classForPath, mockJSONObjectPage, mockPortletsMap)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext)).thenReturn(mockLayout);

		Layout result = digitalPlaceInitializerServiceImpl.initializeWidgetPage(classForPath, FILE_PATH, mockPrefsPlaceholders, mockServiceContext);

		assertThat(result, sameInstance(mockLayout));
	}

	@Test(expected = PortalException.class)
	public void initializeWidgetPage_WithClassAndFilePathAndServiceContextParameter_WhenExceptionCreatingTheLayout_ThenThrowsPortalException() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockJSONObjectPage.getJSONArray("columns")).thenReturn(mockJSONArray1);
		when(mockPageCreationUtil.getColumns(mockJSONArray1, Collections.emptyMap(), ConfigurationKeys.DEFAULT)).thenReturn(mockPortletsMap);
		when(mockPageCreationUtil.getWidgetPageToCreate(classForPath, mockJSONObjectPage, mockPortletsMap)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeWidgetPage(classForPath, FILE_PATH, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void initializeWidgetPage_WithClassAndFilePathAndServiceContextParameters_WhenExceptionRetrievingJSONObject_ThenThrowsPortalException() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeWidgetPage(classForPath, FILE_PATH, mockServiceContext);
	}

	@Test
	public void initializeWidgetPage_WithClassAndFilePathAndServiceContextParameters_WhenNoError_ThenCreatesThePortletLayout() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockPageCreationUtil.getJSONObject(classForPath, FILE_PATH)).thenReturn(mockJSONObjectPage);
		when(mockJSONObjectPage.getJSONArray("columns")).thenReturn(mockJSONArray1);
		when(mockPageCreationUtil.getColumns(mockJSONArray1, Collections.emptyMap(), ConfigurationKeys.DEFAULT)).thenReturn(mockPortletsMap);
		when(mockPageCreationUtil.getWidgetPageToCreate(classForPath, mockJSONObjectPage, mockPortletsMap)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext)).thenReturn(mockLayout);

		Layout result = digitalPlaceInitializerServiceImpl.initializeWidgetPage(classForPath, FILE_PATH, mockServiceContext);

		assertThat(result, sameInstance(mockLayout));
	}

	@Test(expected = PortalException.class)
	public void initializeWidgetPage_WithClassAndJsonFileAndPlaceholdersAndServiceContextParameters_WhenExceptionCreatingTheLayout_ThenThrowsPortalException() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockJSONObjectPage.getJSONArray("columns")).thenReturn(mockJSONArray1);
		when(mockPageCreationUtil.getColumns(mockJSONArray1, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletsMap);
		when(mockPageCreationUtil.getWidgetPageToCreate(classForPath, mockJSONObjectPage, mockPortletsMap)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext)).thenThrow(new PortalException());

		digitalPlaceInitializerServiceImpl.initializeWidgetPage(classForPath, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext);
	}

	@Test
	public void initializeWidgetPage_WithClassAndJsonFileAndPlaceholdersAndServiceContextParameters_WhenNoError_ThenCreatesThePortletLayout() throws PortalException {
		Class classForPath = UserActiveException.class.getClass();
		when(mockJSONObjectPage.getJSONArray("columns")).thenReturn(mockJSONArray1);
		when(mockPageCreationUtil.getColumns(mockJSONArray1, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletsMap);
		when(mockPageCreationUtil.getWidgetPageToCreate(classForPath, mockJSONObjectPage, mockPortletsMap)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext)).thenReturn(mockLayout);

		Layout result = digitalPlaceInitializerServiceImpl.initializeWidgetPage(classForPath, mockJSONObjectPage, mockPrefsPlaceholders, mockServiceContext);

		assertThat(result, sameInstance(mockLayout));
	}

}
