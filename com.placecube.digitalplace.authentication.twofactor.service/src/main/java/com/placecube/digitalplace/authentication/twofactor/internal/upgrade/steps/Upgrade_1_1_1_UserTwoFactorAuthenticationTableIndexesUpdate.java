package com.placecube.digitalplace.authentication.twofactor.internal.upgrade.steps;

import java.io.IOException;
import java.sql.SQLException;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class Upgrade_1_1_1_UserTwoFactorAuthenticationTableIndexesUpdate extends UpgradeProcess {

	public Upgrade_1_1_1_UserTwoFactorAuthenticationTableIndexesUpdate() {
	}

	@Override
	protected void doUpgrade() throws Exception {
		if (hasTable("Placecube_Auth_TwoFactor_UserTwoFactorAuthentication")) {

			createNewIndex();

			dropLegacyIndexImmediately();
		}
	}

	private void createNewIndex() throws Exception, IOException, SQLException {
		if (!hasIndex("Placecube_Auth_TwoFactor_UserTwoFactorAuthentication", "IX_C3C4CDDE")) {
			runSQL("create index IX_C3C4CDDE on Placecube_Auth_TwoFactor_UserTwoFactorAuthentication (uuid_[$COLUMN_LENGTH:75$]);");
		}
	}

	private void dropLegacyIndexImmediately() throws Exception {
		if (hasIndex("Placecube_Auth_TwoFactor_UserTwoFactorAuthentication", "IX_C4B07F8A")) {
			runSQL(connection, "drop index IX_C4B07F8A on Placecube_Auth_TwoFactor_UserTwoFactorAuthentication;");
		}
	}

}
