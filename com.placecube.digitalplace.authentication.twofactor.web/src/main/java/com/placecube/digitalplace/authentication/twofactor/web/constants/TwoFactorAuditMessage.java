package com.placecube.digitalplace.authentication.twofactor.web.constants;

public enum TwoFactorAuditMessage {

	INVALID_AUTH_CODE("VERIFICATION_FAILURE", "Invalid two factor auth code"),

	INVALID_SESSION("NOT_VERIFIED", "Empty session"),

	USER_NOT_FOUND("VERIFICATION_FAILURE", "User not found"),

	USER_NOT_THE_SAME("NOT_VERIFIED", "Not the same user"),

	USER_NOT_YET_VERIFIED("NOT_VERIFIED", "Not yet verified");

	private final String actionType;
	private final String message;

	private TwoFactorAuditMessage(String actionType, String message) {
		this.actionType = actionType;
		this.message = message;
	}

	public String getActionType() {
		return actionType;
	}

	public String getMessage() {
		return message;
	}

}
