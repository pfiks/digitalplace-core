package com.placecube.digitalplace.search.sortby.mostactive.model.impl;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.model.SortByOption;
import com.placecube.digitalplace.search.sortby.mostactive.constants.MostActiveConstants;

@Component(immediate = true, service = { SortByMostActive.class, SortByOption.class })
public class SortByMostActive implements SortByOption {

	private static boolean DEFAULT_SORT_DIRECTION_REVERSE = true;

	@Override
	public String getId() {
		return "sortByMostActive";
	}

	@Override
	public String getLabel(Locale locale) {
		return AggregatedResourceBundleUtil.get("most-active", locale, "com.placecube.digitalplace.search.sortby.mostactive");
	}

	@Override
	public Sort[] getSorts(SharedSearchContributorSettings sharedSearchContributorSettings, boolean useOppositeSortDirection) {
		boolean sortDirection = useOppositeSortDirection ? !DEFAULT_SORT_DIRECTION_REVERSE : DEFAULT_SORT_DIRECTION_REVERSE;
		Sort sortOnCounter = SortFactoryUtil.create(MostActiveConstants.COUNTER_FIELD_NAME, sortDirection);
		Sort sortOnDate = SortFactoryUtil.create(MostActiveConstants.LAST_LOGIN_DATE_FIELD_NAME + "_sortable", sortDirection);
		return new Sort[] { sortOnCounter, sortOnDate };
	}

}
