package com.placecube.digitalplace.user.account.web.connector;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.connector.constants.ConnectorConstants;
import com.placecube.digitalplace.connector.model.Connector;
import com.placecube.digitalplace.user.account.connector.UserCreateAccountConnector;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;

@Component(enabled = true, immediate = true, property = { //
		ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY + "=1", //
		ConnectorConstants.CONNECTOR_TYPE + "=" + UserCreateAccountConnector.USER_CREATE_ACCOUNT_CONNECTOR_TYPE, //
		ConnectorConstants.CONNECTOR_NAME + "=" + AccountConstants.USER_CREATE_ACCOUNT_CONNECTOR_NAME //
}, service = Connector.class)
public class DigitalPlaceCreateAccountConnector implements UserCreateAccountConnector {

	public static final String MODEL_ID = "digitalPlaceUserCreateAccountModel";

	@Reference
	private JSONFactory jsonFactory;

	@Override
	public String getCreateAccountMVCRenderCommandName() {
		return MVCCommandKeys.PERSONAL_DETAILS;
	}

	@Override
	public String getModelId() {
		return MODEL_ID;
	}

	@Override
	public String getPortletId() {
		return AccountPortletKeys.CREATE_ACCOUNT;
	}

	@Override
	public JSONObject getRequestParameters() {
		return jsonFactory.createJSONObject();
	}

}
