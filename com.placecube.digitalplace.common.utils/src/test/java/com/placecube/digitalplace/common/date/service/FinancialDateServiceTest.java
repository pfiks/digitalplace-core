package com.placecube.digitalplace.common.date.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.time.LocalDate;

import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Before;
import org.junit.Test;

public class FinancialDateServiceTest {

	private static final int YEAR = 2020;

	private FinancialDateService financialDateService;

	@Before
	public void setUp() {
		financialDateService = new FinancialDateService();
	}

	@Test
	public void getFinancialYearStartCalendar_WhenNoErrors_ThenReturnsFinancialYearStartCalendar() throws Exception {
		XMLGregorianCalendar result = financialDateService.getFinancialYearStartCalendar(YEAR);

		assertThat(result.getDay(), equalTo(FinancialDateService.FINANCIAL_YEAR_START_DAY));
		assertThat(result.getMonth(), equalTo(FinancialDateService.FINANCIAL_YEAR_START_MONTH));
		assertThat(result.getYear(), equalTo(YEAR));
	}

	@Test
	public void getFinancialYearStartLocalDate_WhenNoErrors_ThenReturnsFinancialYearStartDate() {
		LocalDate result = financialDateService.getFinancialYearStartLocalDate(YEAR);

		assertThat(result.getDayOfMonth(), equalTo(FinancialDateService.FINANCIAL_YEAR_START_DAY));
		assertThat(result.getMonthValue(), equalTo(FinancialDateService.FINANCIAL_YEAR_START_MONTH));
		assertThat(result.getYear(), equalTo(YEAR));
	}

	@Test
	public void getFinancialYearEndLocalDate_WhenNoErrors_ThenReturnsFinancialYearEndDate() {
		LocalDate result = financialDateService.getFinancialYearEndLocalDate(YEAR);

		LocalDate expected = LocalDate.of(YEAR + 1, FinancialDateService.FINANCIAL_YEAR_START_MONTH, FinancialDateService.FINANCIAL_YEAR_START_DAY).minusDays(1);

		assertThat(result.getDayOfMonth(), equalTo(expected.getDayOfMonth()));
		assertThat(result.getMonthValue(), equalTo(expected.getMonthValue()));
		assertThat(result.getYear(), equalTo(expected.getYear()));
	}

}