package com.placecube.digitalplace.gds.taglib.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.gds.taglib.context.ServletContextUtil;

public class WarningTextTag extends IncludeTag {

	private String messageLabel;
	private String portletNamespace;

	@Override
	public int doStartTag() {
		setAttributeNamespace("gds-forms-ui:warningText");
		return EVAL_BODY_INCLUDE;
	}

	public void setMessageLabel(String messageLabel) {
		this.messageLabel = messageLabel;
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);
		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setPortletNamespace(String portletNamespace) {
		this.portletNamespace = portletNamespace;
	}

	@Override
	protected void cleanUp() {
		super.cleanUp();
		portletNamespace = null;
		messageLabel = null;
	}

	@Override
	protected String getPage() {
		return "/META-INF/taglibs/gds-forms-ui/warning-text/view.jsp";
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute("portletNamespace", portletNamespace);
		request.setAttribute("messageLabel", messageLabel);
	}

}
