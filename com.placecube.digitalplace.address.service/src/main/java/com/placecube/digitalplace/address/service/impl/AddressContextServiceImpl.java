/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.address.service.impl;

import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.address.service.base.AddressContextServiceBaseImpl;

/**
 * The implementation of the address context remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.address.service.AddressContextService</code>
 * interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AddressContextServiceBaseImpl
 */
@Component(property = { "json.web.service.context.name=placecube_digitalplace", "json.web.service.context.path=AddressContext" }, service = AopService.class)
public class AddressContextServiceImpl extends AddressContextServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use
	 * <code>com.placecube.digitalplace.address.service.
	 * AddressContextServiceUtil</code> to access the address context remote
	 * service.
	 */
	@Reference
	private AddressLookupService addressLookupService;

	@AccessControlled(guestAccessEnabled = true)
	@Override
	public Set<AddressContext> searchAddressByPostcode(long companyId, String postcode, boolean fallbackToNationalLookup) throws PortalException {

		return addressLookupService.getByPostcode(companyId, postcode, new String[0], fallbackToNationalLookup);
	}
}