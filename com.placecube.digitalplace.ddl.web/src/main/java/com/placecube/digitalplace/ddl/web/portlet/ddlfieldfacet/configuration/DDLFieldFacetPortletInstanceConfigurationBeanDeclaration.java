package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.settings.definition.ConfigurationBeanDeclaration;

@Component(service = ConfigurationBeanDeclaration.class)
public class DDLFieldFacetPortletInstanceConfigurationBeanDeclaration implements ConfigurationBeanDeclaration {

	@Override
	public Class<?> getConfigurationBeanClass() {
		return DDLFieldFacetPortletInstanceConfiguration.class;
	}

}
