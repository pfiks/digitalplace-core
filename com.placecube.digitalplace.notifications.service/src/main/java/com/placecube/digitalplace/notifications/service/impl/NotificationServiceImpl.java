package com.placecube.digitalplace.notifications.service.impl;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendMailContext;
import com.placecube.digitalplace.notifications.model.SendMailContext.SendMailContextBuilder;
import com.placecube.digitalplace.notifications.model.SendSmsContext;
import com.placecube.digitalplace.notifications.service.NotificationMailConnector;
import com.placecube.digitalplace.notifications.service.NotificationSmsConnector;
import com.placecube.digitalplace.notifications.service.NotificationsService;
import com.placecube.digitalplace.notifications.service.internal.emaildefault.DefaultEmailNotificationService;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

@Component(immediate = true, service = NotificationsService.class)
public class NotificationServiceImpl implements NotificationsService {

	@Reference
	private DefaultEmailNotificationService defaultEmailNotificationService;

	private Set<NotificationMailConnector> notificationMailConnectors = new LinkedHashSet<>();

	private Set<NotificationSmsConnector> notificationSmsConnectors = new LinkedHashSet<>();

	@Override
	public SendMailContextBuilder createSendMailContextBuilder(String templateId, String emailAddress, Map<String, Object> personalisation, String reference, String fromEmailAddress) {
		return new SendMailContextBuilder(templateId, emailAddress, personalisation, reference, fromEmailAddress);
	}

	@Override
	public void sendMail(SendMailContext sendMailContext, ServiceContext serviceContext) throws NotificationsException, ConfigurationException {
		if (Validator.isNotNull(sendMailContext.getTemplateId()) && Validator.isNotNull(sendMailContext.getEmailAddress())) {

			Optional<NotificationMailConnector> mailConnector = notificationMailConnectors.stream().filter(entry -> entry.enabled(serviceContext.getCompanyId())).findFirst();
			if (mailConnector.isPresent()) {
				mailConnector.get().sendMail(sendMailContext, serviceContext);
			} else {
				defaultEmailNotificationService.sendMail(sendMailContext, serviceContext);
			}
		} else {
			throw new NotificationsException("templateID or emailAddress is empty or null");
		}
	}

	@Override
	public void sendSms(SendSmsContext sendSmsContext, ServiceContext serviceContext) throws NotificationsException, ConfigurationException {
		if (Validator.isNotNull(sendSmsContext.getTemplateId()) && Validator.isNotNull(sendSmsContext.getPhoneNumber())) {
			getNotificationSmsConnector(serviceContext.getCompanyId()).sendSms(sendSmsContext, serviceContext);
		} else {
			throw new NotificationsException("templateID or phoneNumber is empty or null");
		}
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setNotificationMailConnectors(NotificationMailConnector notificationMailConnector) {
		notificationMailConnectors.add(notificationMailConnector);
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setNotificationSmsConnectors(NotificationSmsConnector notificationSmsConnector) {
		notificationSmsConnectors.add(notificationSmsConnector);
	}

	protected void unsetNotificationMailConnectors(NotificationMailConnector notificationMailConnector) {
		notificationMailConnectors.remove(notificationMailConnector);
	}

	protected void unsetNotificationSmsConnectors(NotificationSmsConnector notificationSmsConnector) {
		notificationSmsConnectors.remove(notificationSmsConnector);
	}

	private NotificationSmsConnector getNotificationSmsConnector(long companyId) throws NotificationsException {
		Optional<NotificationSmsConnector> notificationsConnector = notificationSmsConnectors.stream().filter(entry -> entry.enabled(companyId)).findFirst();
		if (notificationsConnector.isPresent()) {
			return notificationsConnector.get();
		} else {
			throw new NotificationsException("No notifications sms connector configured");
		}
	}
}
