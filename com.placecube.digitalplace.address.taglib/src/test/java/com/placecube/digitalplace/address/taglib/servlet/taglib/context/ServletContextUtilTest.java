package com.placecube.digitalplace.address.taglib.servlet.taglib.context;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.servlet.ServletContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class ServletContextUtilTest {

	@InjectMocks
	private ServletContextUtil servletContextUtil = new ServletContextUtil();

	@Mock
	private ServletContext mockServletContext;

	@Before
	public void setUp() {
		initMocks(this);

	}

	@Test
	public void getServletContext_ReturnsTheServletContext() {
		servletContextUtil.activate();

		assertThat(ServletContextUtil.getServletContext(), equalTo(mockServletContext));
	}

}
