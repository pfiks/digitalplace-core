package com.placecube.digitalplace.addresslookup.ordnancesurvey.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.addresslookup.ordnancesurvey.configuration.OrdnanceSurveyCompanyConfiguration", localization = "content/Language", name = "address-lookup-ordnance-survey")
public interface OrdnanceSurveyCompanyConfiguration {

	@Meta.AD(required = false, deflt = "", name = "key")
	String applicationKey();

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "https://api.os.uk/search/places/v1/find", name = "find-endpoint-url")
	String findEndpointURL();

	@Meta.AD(required = false, deflt = "false", name = "national", description = "national-description")
	boolean national();

	@Meta.AD(required = false, deflt = "https://api.os.uk/search/places/v1/postcode", name = "postcode-endpoint-url")
	String postcodeEndpointURL();

	@Meta.AD(required = false, deflt = "https://api.os.uk/search/places/v1/uprn", name = "uprn-endpoint-url")
	String uprnEndpointURL();

	@Meta.AD(required = false, deflt = "1", name = "weight", description = "weight-description")
	Integer weight();

}
