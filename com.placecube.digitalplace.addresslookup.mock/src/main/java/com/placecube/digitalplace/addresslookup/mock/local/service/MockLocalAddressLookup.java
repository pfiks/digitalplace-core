package com.placecube.digitalplace.addresslookup.mock.local.service;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookup;

@Component(immediate = true, service = AddressLookup.class)
public class MockLocalAddressLookup implements AddressLookup {

	private static final Log LOG = LogFactoryUtil.getLog(MockLocalAddressLookup.class);

	@Reference
	private LocalAddressRetrievalService localAddressRetrievalService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return localAddressRetrievalService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Set<AddressContext> findByKeyword(long companyId, String keyword, String[] paramsAndValues) {
		try {
			JSONArray resultsJsonArray = localAddressRetrievalService.getResults(keyword);

			return getAddressContexts(resultsJsonArray);

		} catch (Exception e) {
			LOG.error(e);
			return Collections.emptySet();

		}
	}

	@Override
	public Set<AddressContext> findByPostcode(long companyId, String postcode, String[] paramsAndValues) {
		try {
			JSONArray resultsJsonArray = localAddressRetrievalService.getResults(postcode);

			Set<AddressContext> addressLookups = getAddressContexts(resultsJsonArray);
			if (!addressLookups.isEmpty()) {
				addressLookups.add(new AddressContext("77", "A77 AddressLine1Value", "A77 AddressLine2Value", "A77 AddressLine3Value", "A77 AddressLine4Value", "A77 CityValue", postcode));
				addressLookups.add(new AddressContext("88", "B88 AddressLine1Value", "B88 AddressLine2Value", "", "", "B88 CityValue", postcode));
				addressLookups.add(new AddressContext("99", "C99 AddressLine1Value", "", "", "", "C99 CityValue", postcode));
			}

			return addressLookups;

		} catch (Exception e) {
			LOG.error(e);
			return Collections.emptySet();

		}
	}

	@Override
	public Optional<AddressContext> findByUprn(long companyId, String uprn, String[] paramsAndValues) {

		if (Pattern.matches("^[1-5]*$", uprn)) {
			return Optional.of(localAddressRetrievalService.getAddressContext(uprn));
		}
		return Optional.empty();

	}

	@Override
	public int getWeight(long companyId) {
		try {
			return localAddressRetrievalService.getWeight(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return 0;
		}
	}

	@Override
	public boolean national(long companyId) {
		try {
			return localAddressRetrievalService.isNational(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	private Set<AddressContext> getAddressContexts(JSONArray resultsJsonArray) {
		Set<AddressContext> addressLookups = new LinkedHashSet<>();
		if (Validator.isNotNull(resultsJsonArray)) {
			for (int i = 0; i < resultsJsonArray.length(); i++) {
				JSONObject jsonObject = resultsJsonArray.getJSONObject(i);
				addressLookups.add(localAddressRetrievalService.getAddressLookup(jsonObject));
			}
		}
		return addressLookups;
	}

}
