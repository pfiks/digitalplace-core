package com.placecube.digitalplace.companyinitializeradmin.initializer.internal.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.reflect.internal.WhiteboxImpl;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.placecube.digitalplace.initialiser.CompanyInitializer;

public class CompanyInitializerRegistryTest extends PowerMockito {

	private static final String MOCK_COMPANY_INITILIZER_1_KEY = "mockCompanyInitilizer1Key";

	@InjectMocks
	private CompanyInitializerRegistry companyInitializerRegistry;

	private ServiceTrackerMap<String, ServiceWrapper<CompanyInitializer>> companyInitilizersMap;

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private CompanyInitializer mockCompanyInitilizer1;

	@Mock
	private CompanyInitializer mockCompanyInitilizer2;

	@Mock
	private CompanyInitializer mockCompanyInitilizer3;

	@Mock
	private ServiceWrapper<CompanyInitializer> mockServiceWrapper1;

	@Mock
	private ServiceWrapper<CompanyInitializer> mockServiceWrapper2;

	@Mock
	private ServiceWrapper<CompanyInitializer> mockServiceWrapper3;

	@Before
	public void activateSetup() {
		initMocks(this);
		companyInitializerRegistry = spy(new CompanyInitializerRegistry());
	}

	@Test
	public void getCompanyInitializer_WhenKeyIsEmpty_ThenReturnNull() {
		CompanyInitializer result = companyInitializerRegistry.getCompanyInitializer("");

		assertNull(result);
	}

	@Test
	public void getCompanyInitializer_WhenKeyIsNotValidForService_ThenReturnNull() {
		configureInternalMap();

		CompanyInitializer result = companyInitializerRegistry.getCompanyInitializer("notValidKeyForService");

		assertNull(result);
	}

	@Test
	public void getCompanyInitializer_WhenKeyIsNull_ThenReturnNull() {
		CompanyInitializer result = companyInitializerRegistry.getCompanyInitializer(null);

		assertNull(result);
	}

	@Test
	public void getCompanyInitializer_WhenKeyIsValidForService_ThenReturnCompanyInitalizer() {
		configureInternalMap();

		when(mockServiceWrapper1.getService()).thenReturn(mockCompanyInitilizer1);

		CompanyInitializer result = companyInitializerRegistry.getCompanyInitializer(MOCK_COMPANY_INITILIZER_1_KEY);

		assertThat(result, equalTo(mockCompanyInitilizer1));
	}

	@Test
	public void getCompanyInitializers_WhenNoError_ThenReturnsTheCompanyInitilizers() {
		configureInternalMap();

		when(mockServiceWrapper1.getService()).thenReturn(mockCompanyInitilizer1);
		when(mockServiceWrapper2.getService()).thenReturn(mockCompanyInitilizer2);
		when(mockServiceWrapper3.getService()).thenReturn(mockCompanyInitilizer3);

		List<CompanyInitializer> results = companyInitializerRegistry.getCompanyInitializers();

		assertThat(results, contains(mockCompanyInitilizer1, mockCompanyInitilizer2, mockCompanyInitilizer3));
	}

	private void configureInternalMap() {

		companyInitilizersMap = new ServiceTrackerMap<String, ServiceWrapper<CompanyInitializer>>() {

			@Override
			public void close() {
			}

			@Override
			public boolean containsKey(String key) {
				return false;
			}

			@Override
			public ServiceWrapper<CompanyInitializer> getService(String key) {
				if (MOCK_COMPANY_INITILIZER_1_KEY.equals(key)) {
					return mockServiceWrapper1;
				}
				return null;
			}

			@Override
			public Set<String> keySet() {
				return null;
			}

			@Override
			public Collection<ServiceWrapper<CompanyInitializer>> values() {
				Collection<ServiceWrapper<CompanyInitializer>> companyInitializers = new ArrayList<>();
				companyInitializers.add(mockServiceWrapper1);
				companyInitializers.add(mockServiceWrapper2);
				companyInitializers.add(mockServiceWrapper3);
				return companyInitializers;
			}
		};
		WhiteboxImpl.setInternalState(companyInitializerRegistry, "companyInitilizersMap", companyInitilizersMap);
	}

}
