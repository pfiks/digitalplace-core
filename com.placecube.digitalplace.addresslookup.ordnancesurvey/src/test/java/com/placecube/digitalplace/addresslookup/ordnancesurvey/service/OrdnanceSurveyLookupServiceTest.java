package com.placecube.digitalplace.addresslookup.ordnancesurvey.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.ordnancesurvey.constants.OrdnanceSurveyConstants;

public class OrdnanceSurveyLookupServiceTest extends PowerMockito {

	@Mock
	private AddressContext mockAddressContext1;

	@Mock
	private AddressContext mockAddressContext2;

	@Mock
	private AddressContext mockAddressContext3;

	@Mock
	private AddressContext mockAddressContext4;

	@Mock
	private AddressRetrievalService mockAddressRetrievalService;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONObject mockJSONObject1;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private JSONObject mockJSONObject3;

	@Mock
	private JSONObject mockJSONObject4;

	@InjectMocks
	private OrdnanceSurveyLookupService ordnanceSurveyLookupService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getAdditionalParametersAndValuesMap_WhenNoError_ThenReturnsMapWithValidParametersAndValues() {
		String[] paramsAndValues = new String[] { "key1=value1", "key2=value2", "key3=", "=value4", "someRandomValue", "" };

		Map<String, String> results = ordnanceSurveyLookupService.getAdditionalParametersAndValuesMap(paramsAndValues);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get("key1"), equalTo("value1"));
		assertThat(results.get("key2"), equalTo("value2"));
	}

	@Test
	public void getAdditionalParametersAndValuesMap_WhenNoParamsAndValues_ThenReturnsEmptyMap() {
		Map<String, String> results = ordnanceSurveyLookupService.getAdditionalParametersAndValuesMap(null);

		assertThat(results.size(), equalTo(0));
	}

	@Test
	public void getResults_WhenNoError_ThenReturnsSetWithResultsPreferringTheDPAResults() {
		when(mockAddressRetrievalService.executeMethod("myUrl")).thenReturn(mockJSONArray);
		when(mockJSONArray.length()).thenReturn(4);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject1);
		when(mockJSONArray.getJSONObject(1)).thenReturn(mockJSONObject2);
		when(mockJSONArray.getJSONObject(2)).thenReturn(mockJSONObject3);
		when(mockJSONArray.getJSONObject(3)).thenReturn(mockJSONObject4);
		when(mockAddressRetrievalService.getAddressContextResult(mockJSONObject1)).thenReturn(mockAddressContext1);
		when(mockAddressRetrievalService.getAddressContextResult(mockJSONObject2)).thenReturn(mockAddressContext2);
		when(mockAddressRetrievalService.getAddressContextResult(mockJSONObject3)).thenReturn(mockAddressContext3);
		when(mockAddressRetrievalService.getAddressContextResult(mockJSONObject4)).thenReturn(mockAddressContext4);
		when(mockAddressContext1.getUPRN()).thenReturn("uprn1");
		when(mockAddressContext2.getUPRN()).thenReturn("uprn2");
		when(mockAddressContext3.getUPRN()).thenReturn("uprn1");
		when(mockAddressContext3.getSource()).thenReturn(OrdnanceSurveyConstants.DPA);
		when(mockAddressContext4.getUPRN()).thenReturn("uprn2");
		when(mockAddressContext4.getSource()).thenReturn("someOtherValue");

		Set<AddressContext> results = ordnanceSurveyLookupService.getResults("myUrl");

		assertThat(results.size(), equalTo(2));
		assertThat(results, contains(mockAddressContext3, mockAddressContext2));
	}

	@Test
	public void getURL_WhenNoError_ThenReturnsTheFullUrlWIthParametersSet() {
		Map<String, String> params = new LinkedHashMap<>();
		params.put("key1", "value1");
		params.put("key2", "value2");

		String result = ordnanceSurveyLookupService.getURL("baseEndpoint", "defaultParam1", "defaultValue1", params, "appKeyValue");

		assertThat(result, equalTo("baseEndpoint?defaultParam1=defaultValue1&key=appKeyValue&key1=value1&key2=value2"));
	}

}
