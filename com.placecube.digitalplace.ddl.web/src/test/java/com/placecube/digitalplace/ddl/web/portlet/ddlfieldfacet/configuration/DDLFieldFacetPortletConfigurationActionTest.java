package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.model.FilterField;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service.DDLFieldFacetParserService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class DDLFieldFacetPortletConfigurationActionTest extends PowerMockito {

	private static final long DDM_STRUCTURE_ID = 2L;

	@InjectMocks
	private DDLFieldFacetPortletConfigurationAction ddlFieldFacetPortletConfigurationAction;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private List<DDMStructure> mockAvailableDDMStructures;

	@Mock
	private Set<FilterField> mockAvailableFields;

	@Mock
	private DDLFieldFacetPortletInstanceConfiguration mockConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private DDLConfigurationService mockDDLConfigurationService;

	@Mock
	private DDLFieldFacetParserService mockDDLFieldFacetParserService;

	@Mock
	private DDMForm mockDDMForm;

	@Mock
	private List<DDMFormField> mockDDMFormFields;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void include_WhenDDMStructureIdNotSet_ThenReturnOnlyAvailableStructures() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DDLFieldFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDDLConfigurationService.getAvailableDDMStructures(mockGroup)).thenReturn(mockAvailableDDMStructures);

		when(mockConfiguration.ddmStructureId()).thenReturn(0L);

		ddlFieldFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("configuration", mockConfiguration);
		verify(mockHttpServletRequest, times(1)).setAttribute("availableStructures", mockAvailableDDMStructures);
		verify(mockDDMStructureLocalService, times(0)).fetchDDMStructure(anyLong());
	}

	@Test
	public void include_WhenDDMStructureIdSet_ThenReturnAvailableStructuresAndFieldOption() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DDLFieldFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDDLConfigurationService.getAvailableDDMStructures(mockGroup)).thenReturn(mockAvailableDDMStructures);

		when(mockConfiguration.ddmStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockDDMStructureLocalService.fetchDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);

		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDDMFormFields()).thenReturn(mockDDMFormFields);
		when(mockDDLFieldFacetParserService.parseToFilterFields(mockDDMFormFields)).thenReturn(mockAvailableFields);

		ddlFieldFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("configuration", mockConfiguration);
		verify(mockHttpServletRequest, times(1)).setAttribute("availableStructures", mockAvailableDDMStructures);
		verify(mockHttpServletRequest, times(1)).setAttribute("availableFields", mockAvailableFields);

	}

	@Test
	public void include_WhenLinkDDMStructureIdIsNotSet_ThenDoesNotSetAvailableLinkStructuresInRequest() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DDLFieldFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDDLConfigurationService.getAvailableDDMStructures(mockGroup)).thenReturn(mockAvailableDDMStructures);

		when(mockConfiguration.ddmLinkStructureId()).thenReturn(0L);

		ddlFieldFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, never()).setAttribute(eq("availableLinkFields"), any(List.class));
	}

	@Test
	public void include_WhenLinkDDMStructureIdIsSet_ThenSetsAvailableLinkStructuresInRequest() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(DDLFieldFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDDLConfigurationService.getAvailableDDMStructures(mockGroup)).thenReturn(mockAvailableDDMStructures);
		when(mockConfiguration.ddmLinkStructureId()).thenReturn(DDM_STRUCTURE_ID);
		when(mockDDMStructureLocalService.fetchDDMStructure(DDM_STRUCTURE_ID)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMForm.getDDMFormFields()).thenReturn(mockDDMFormFields);
		when(mockDDLFieldFacetParserService.parseToFilterFields(mockDDMFormFields)).thenReturn(mockAvailableFields);

		ddlFieldFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("availableLinkFields", mockAvailableFields);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesPreferences() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));

		String expectedFieldName = "thisField";
		String expectedFilterColour = "Orange";
		String expectedPreselectedValue = "preselected";
		boolean expectedInvisible = true;
		boolean alphaSort = true;
		String customHeading = "customHeading";
		String ddlRecordLinkFieldName = "ddlRecordLinkFieldName";
		long ddmLinkStructureId = 2323L;
		String displayStyle = "displayStyle";
		boolean isFieldNameDDLRecordLink = true;
		boolean showHeadingAsFirstDropdownOption = true;

		when(ParamUtil.getLong(mockActionRequest, "ddmStructureId", 0)).thenReturn(DDM_STRUCTURE_ID);
		when(ParamUtil.getString(mockActionRequest, "filterField", StringPool.BLANK)).thenReturn(expectedFieldName);
		when(ParamUtil.getString(mockActionRequest, "filterColour", StringPool.BLANK)).thenReturn(expectedFilterColour);
		when(ParamUtil.getBoolean(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.INVISIBLE)).thenReturn(expectedInvisible);
		when(ParamUtil.getString(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, StringPool.BLANK)).thenReturn(expectedPreselectedValue);
		when(ParamUtil.getBoolean(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.ALPHA_SORT)).thenReturn(alphaSort);
		when(ParamUtil.getString(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.CUSTOM_HEADING, StringPool.BLANK)).thenReturn(customHeading);
		when(ParamUtil.getBoolean(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.IS_FIELD_NAME_DDL_RECORD_LINK)).thenReturn(isFieldNameDDLRecordLink);
		when(ParamUtil.getLong(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.DDM_LINK_STRUCTURE_ID, 0)).thenReturn(ddmLinkStructureId);
		when(ParamUtil.getString(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.DDL_RECORD_LINK_FIELD_NAME, StringPool.BLANK)).thenReturn(ddlRecordLinkFieldName);
		when(ParamUtil.getString(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.DISPLAY_STYLE, StringPool.BLANK)).thenReturn(displayStyle);
		when(ParamUtil.getBoolean(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION)).thenReturn(showHeadingAsFirstDropdownOption);

		ddlFieldFacetPortletConfigurationAction = spy(new DDLFieldFacetPortletConfigurationAction());

		ddlFieldFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "ddmStructureId", String.valueOf(DDM_STRUCTURE_ID));
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "ddmFieldName", expectedFieldName);
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "filterColour", expectedFilterColour);
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, expectedPreselectedValue);
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.INVISIBLE, String.valueOf(expectedInvisible));
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.ALPHA_SORT, String.valueOf(alphaSort));
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.CUSTOM_HEADING, customHeading);
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.IS_FIELD_NAME_DDL_RECORD_LINK,
				String.valueOf(isFieldNameDDLRecordLink));
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.DDM_LINK_STRUCTURE_ID,
				String.valueOf(ddmLinkStructureId));
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.DDL_RECORD_LINK_FIELD_NAME, ddlRecordLinkFieldName);
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.DISPLAY_STYLE, displayStyle);
		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION,
				String.valueOf(showHeadingAsFirstDropdownOption));
	}

	@Test
	public void processAction_WhenNoValuesPreSelected_ThenInvisibleSetToFalse() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));

		boolean expectedInvisible = false;

		when(ParamUtil.getString(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, StringPool.BLANK)).thenReturn(StringPool.BLANK);

		ddlFieldFacetPortletConfigurationAction = spy(new DDLFieldFacetPortletConfigurationAction());

		ddlFieldFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(ddlFieldFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLFieldFacetPortletConfigurationConstants.INVISIBLE, String.valueOf(expectedInvisible));

	}
}
