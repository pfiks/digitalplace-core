package com.placecube.digitalplace.webcontent.email.internal.upgrade_1_0_3;

import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.placecube.digitalplace.webcontent.email.constants.EmailDDMTemplate;

public class EmailWebContentTemplateStaticUtilUpgrade extends UpgradeProcess {

	private final DDMTemplateLocalService ddmTemplateLocalService;

	private static final Log LOG = LogFactoryUtil.getLog(EmailWebContentTemplateStaticUtilUpgrade.class);

	public EmailWebContentTemplateStaticUtilUpgrade(DDMTemplateLocalService ddmTemplateLocalService) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();
		upgradeWidgetTemplate(companyId, EmailDDMTemplate.EMAIL);
	}

	private void upgradeWidgetTemplate(long companyId, EmailDDMTemplate template) {
		DynamicQuery dynamicQuery = ddmTemplateLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("templateKey", template.getKey()));

		List<DDMTemplate> ddmTemplates = ddmTemplateLocalService.dynamicQuery(dynamicQuery);

		if (ddmTemplates.isEmpty()) {
			LOG.warn("Template " + template.getKey() + " not found in company " + companyId);
		} else {
			ddmTemplates.forEach(this::updateTemplate);
		}

	}

	private void updateTemplate(DDMTemplate ddmTemplate) {
		String newScript = getAmendedScript(ddmTemplate.getScript());
		ddmTemplate.setScript(newScript);
		ddmTemplateLocalService.updateDDMTemplate(ddmTemplate);
	}

	private String getAmendedScript(String script) {
		return script.replace("<#assign serviceContext = staticUtil[\"com.liferay.portal.kernel.service.ServiceContextThreadLocal\"].getServiceContext()>", StringPool.BLANK)
				.replaceAll("\\bserviceContext\\b", "digitalplace_serviceContext");
	}

}