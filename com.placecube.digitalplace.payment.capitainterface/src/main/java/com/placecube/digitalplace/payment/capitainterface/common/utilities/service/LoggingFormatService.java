package com.placecube.digitalplace.payment.capitainterface.common.utilities.service;

import java.util.Map;
import java.util.Map.Entry;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.DataField;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.DiagnosticData;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.ErrorCode;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.LoggingBean;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.Service;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.SourceSystem;

/**
 * Service class to manage the format of error logging
 *
 */
public class LoggingFormatService {

	private static final String SEPARATOR = "|";
	private static final Log LOG = LogFactoryUtil.getLog(LoggingFormatService.class);

	private LoggingFormatService() {

	}

	/**
	 * This method maps the esbErrorCode to the correct ErrorCode enum value. If
	 * no ESB code exists with this code in the ErrorCode definition, E0009 is
	 * returned.
	 *
	 * @param esbErrorCode the esbErrorCode String value
	 * @return the error code
	 */
	public static ErrorCode generateESBErrorCode(String esbErrorCode) {

		ErrorCode esbCode = ErrorCode.E0009;
		try {
			String transcribedCode = esbErrorCode.replace("-", "_");
			esbCode = ErrorCode.valueOf("E" + transcribedCode);
		} catch (java.lang.IllegalArgumentException e) {
			LOG.error(LoggingFormatService
					.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.ESB, esbCode, "getCurrentPermits", new DiagnosticData(DataField.ESB_ERROR_CODE, esbErrorCode))), e);

		}

		return esbCode;
	}

	/**
	 * This method returns a String representation of the LoggingBean, in the
	 * standard error logging format: Liferay Service where error
	 * occurred|Source system of error|Error code|WSDL operation if
	 * applicable|Diagnostic data where Diagnostic data is in format:
	 * FIELDNAME1: value1 FIELDNAME2: value2 etc
	 *
	 * @param loggingBean a LoggingBean
	 * @return a String representation of the LoggingBean
	 */
	public static String formatLoggingBean(LoggingBean loggingBean) {

		String loggingString = "";

		if (loggingBean.getService() != null) {
			loggingString += loggingBean.getService();
		}

		loggingString += SEPARATOR;

		if (loggingBean.getSourceSystem() != null) {
			loggingString += loggingBean.getSourceSystem();
		}

		loggingString += SEPARATOR;

		if (loggingBean.getErrorCode() != null) {
			loggingString += loggingBean.getErrorCode().getCode();
		}

		loggingString += SEPARATOR;

		if (Validator.isNotNull(loggingBean.getWsdlOperationName())) {
			loggingString += loggingBean.getWsdlOperationName();
		}

		loggingString += SEPARATOR;

		Map<DataField, String> dataFields = loggingBean.getDiagnosticDataFields();
		for (Entry<DataField, String> mapEntry : dataFields.entrySet()) {
			loggingString += (mapEntry.getKey() + ": " + mapEntry.getValue() + " ");
		}

		return loggingString;
	}

}
