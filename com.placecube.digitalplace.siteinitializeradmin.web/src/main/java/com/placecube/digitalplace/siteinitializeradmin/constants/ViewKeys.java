package com.placecube.digitalplace.siteinitializeradmin.constants;

public final class ViewKeys {

	public static final String VIEW = "/view.jsp";

	private ViewKeys() {
		return;
	}
}