package com.placecube.digitalplace.search.web.portlet.contenttype.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.contenttype.service.ContentTypeSearchService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.CONTENT_TYPE_FACET, service = ConfigurationAction.class)
public class ContentTypeFacetPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private ContentTypeSearchService contentTypeSearchService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		ContentTypeFacetPortletInstanceConfiguration config = contentTypeSearchService.getConfiguration(themeDisplay);

		httpServletRequest.setAttribute(ContentTypeFacetPortletConfigurationConstants.FILTER_COLOUR, config.filterColour());

		httpServletRequest.setAttribute(ContentTypeFacetPortletConfigurationConstants.JOIN_FILTERS_WITH_THE_SAME_NAME, config.joinFiltersWithTheSameName());

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		String filterColour = ParamUtil.getString(actionRequest, ContentTypeFacetPortletConfigurationConstants.FILTER_COLOUR, StringPool.BLANK);
		boolean joinFiltersWithTheSameName = ParamUtil.getBoolean(actionRequest, ContentTypeFacetPortletConfigurationConstants.JOIN_FILTERS_WITH_THE_SAME_NAME);

		setPreference(actionRequest, ContentTypeFacetPortletConfigurationConstants.FILTER_COLOUR, filterColour);
		setPreference(actionRequest, ContentTypeFacetPortletConfigurationConstants.JOIN_FILTERS_WITH_THE_SAME_NAME, String.valueOf(joinFiltersWithTheSameName));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
