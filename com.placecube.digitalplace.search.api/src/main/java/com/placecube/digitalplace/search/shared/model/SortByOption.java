package com.placecube.digitalplace.search.shared.model;

import java.util.Locale;

import javax.portlet.PortletPreferences;

import com.liferay.portal.kernel.search.Sort;

public interface SortByOption {

	String getId();

	String getLabel(Locale locale);

	default String getLabel(Locale locale, PortletPreferences portletPreferences) {
		return getLabel(locale);
	}

	default Sort[] getSorts(SharedSearchContributorSettings sharedSearchContributorSettings) {
		return getSorts(sharedSearchContributorSettings, false);
	}

	Sort[] getSorts(SharedSearchContributorSettings sharedSearchContributorSettings, boolean useOppositeSortDirection);
}
