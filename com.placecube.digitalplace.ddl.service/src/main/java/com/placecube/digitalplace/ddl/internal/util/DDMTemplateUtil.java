package com.placecube.digitalplace.ddl.internal.util;

import java.util.Optional;

import com.liferay.dynamic.data.mapping.constants.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;

public class DDMTemplateUtil {

	public static Optional<DDMTemplate> getTemplateById(DDMStructure ddmStructure, long templateId) {
		if (templateId > 0) {
			for (DDMTemplate ddmTemplate : ddmStructure.getTemplates()) {
				if (DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY.equals(ddmTemplate.getType()) && templateId == ddmTemplate.getTemplateId()) {
					return Optional.of(ddmTemplate);
				}
			}
		}
		return Optional.empty();
	}
}
