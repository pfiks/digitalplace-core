package com.placecube.digitalplace.dataconsumer.model;

public interface DataProviderOutputParameter {

	String getName();

	String getPath();

	String getType();

}
