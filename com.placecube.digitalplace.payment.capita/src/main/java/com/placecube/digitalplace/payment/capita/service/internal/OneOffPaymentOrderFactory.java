package com.placecube.digitalplace.payment.capita.service.internal;

import java.math.BigDecimal;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleConfiguration;
import com.placecube.digitalplace.payment.model.PaymentContext;

@Component(immediate = true, service = OneOffPaymentOrderFactory.class)
public class OneOffPaymentOrderFactory {

	public OneOffPaymentOrder createOneOffPaymentOrder(PaymentContext paymentContext) {
		OneOffPaymentOrder oneOffOrder = new OneOffPaymentOrder();

		oneOffOrder.setAddress(paymentContext.getAddress());
		oneOffOrder.setEmailAddress(paymentContext.getEmailAddress());
		oneOffOrder.setFullName(paymentContext.getFirstName() + " " + paymentContext.getLastName());
		oneOffOrder.setRequestType(OneOffPaymentOrder.CardRequestType.PAY_ONLY);
		oneOffOrder.setBackURL(paymentContext.getBackURL());
		oneOffOrder.setReturnURL(paymentContext.getReturnURL());
		oneOffOrder.setInternalReference(paymentContext.getInternalReference());

		int amountInMinorUnits;
		String saleDescription;
		String saleReference;

		boolean advancedConfiguration = isAdvancedConfiguration(paymentContext);
		if (advancedConfiguration) {
			amountInMinorUnits = GetterUtil.getInteger(paymentContext.getAdvancedConfigurationValue(CapitaConstants.AMOUNT_IN_MINOR_UNITS));
			saleDescription = GetterUtil.getString(paymentContext.getAdvancedConfigurationValue(CapitaConstants.SALE_DESCRIPTION));
			saleReference = GetterUtil.getString(paymentContext.getAdvancedConfigurationValue(CapitaConstants.SALE_REFERENCE));
		} else {
			amountInMinorUnits = convertToPence(paymentContext);
			saleDescription = GetterUtil.getString(paymentContext.getSalesDescription());
			saleReference = paymentContext.getSalesReference();
		}

		oneOffOrder.setSaleDescription(saleDescription);
		oneOffOrder.setSaleReferenceCode(saleReference);
		oneOffOrder.setSaleAmount(amountInMinorUnits);
		oneOffOrder.setSimpleSaleConfiguration(SimpleSaleConfiguration.newSimpleSaleConfiguration(paymentContext, amountInMinorUnits, advancedConfiguration));

		return oneOffOrder;
	}

	private int convertToPence(PaymentContext paymentContext) {
		return paymentContext.getAmount().multiply(new BigDecimal(100)).intValue();
	}

	private boolean isAdvancedConfiguration(PaymentContext paymentContext) {
		return !paymentContext.getAdvancedConfiguration().isEmpty();
	}

}