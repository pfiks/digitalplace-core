package com.placecube.digitalplace.audit.listener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.PortletPreferences;
import com.liferay.portal.security.audit.event.generators.constants.EventTypes;
import com.placecube.digitalplace.audit.service.AuditService;

public class AuditPortletPreferencesListenerTest extends PowerMockito {

	@InjectMocks
	private AuditPortletPreferencesListener auditPortletPreferencesListener;

	@Mock
	private Map<String, String> mockAdditionalInformationMap;

	@Mock
	private AuditService mockAuditService;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletPreferences mockPortletPreferencesPrevious;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void onAfterCreate_WhenNoError_ThenAddsAuditLogEntry() {
		long id = 1;
		when(mockPortletPreferences.getPortletPreferencesId()).thenReturn(id);
		when(mockAuditService.getPortletPreferencesAuditInfo(mockPortletPreferences)).thenReturn(mockAdditionalInformationMap);

		auditPortletPreferencesListener.onAfterCreate(mockPortletPreferences);

		verify(mockAuditService, times(1)).addAuditLogEntry(PortletPreferences.class.getName(), id, EventTypes.ADD, mockAdditionalInformationMap);
	}

	@Test
	public void onAfterRemove_WhenNoError_ThenAddsAuditLogEntry() {
		long id = 1;
		when(mockPortletPreferences.getPortletPreferencesId()).thenReturn(id);
		when(mockAuditService.getPortletPreferencesAuditInfo(mockPortletPreferences)).thenReturn(mockAdditionalInformationMap);

		auditPortletPreferencesListener.onAfterRemove(mockPortletPreferences);

		verify(mockAuditService, times(1)).addAuditLogEntry(PortletPreferences.class.getName(), id, EventTypes.DELETE, mockAdditionalInformationMap);

	}

	@Test
	public void onAfterUpdate_WhenNoError_ThenAddsAuditLogEntry() {
		long id = 1;
		when(mockPortletPreferences.getPortletPreferencesId()).thenReturn(id);
		when(mockAuditService.getPortletPreferencesAuditInfo(mockPortletPreferences)).thenReturn(mockAdditionalInformationMap);

		auditPortletPreferencesListener.onAfterUpdate(mockPortletPreferencesPrevious, mockPortletPreferences);

		verify(mockAuditService, times(1)).addAuditLogEntry(PortletPreferences.class.getName(), id, EventTypes.UPDATE, mockAdditionalInformationMap);
	}
}
