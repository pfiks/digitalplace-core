package com.placecube.digitalplace.search.internal.util;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcher;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcherManager;
import com.liferay.portal.kernel.search.facet.util.FacetFactory;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.search.internal.service.SearchConfigurationService;
import com.placecube.digitalplace.search.shared.constants.SharedSearchAttributeKeys;
import com.placecube.digitalplace.search.shared.model.impl.SharedSearchContributorSettingsImpl;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class SearchUtilTest extends PowerMockito {

	@Mock
	private Facet mockFacet;

	@Mock
	private FacetedSearcher mockFacetedSearcher;

	@Mock
	private FacetedSearcherManager mockFacetedSearcherManager;

	@Mock
	private FacetFactory mockFacetFactory;

	@Mock
	private Hits mockHits;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Layout mockLayout;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private SearchConfigurationService mockSearchConfigurationService;

	private SearchContainer mockSearchContainer;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchURLUtil mockSearchHttpUtil;

	@Mock
	private SharedSearchContributorSettingsImpl mockSharedSearchContributorSettingsImpl;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TimeZone mockTimeZone;

	@InjectMocks
	private SearchUtil searchUtil;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void createBooleanQuery_WhenNoError_ThenCreatesANewBooleanQueryImpl() {
		BooleanQuery result = searchUtil.createBooleanQuery();

		assertThat(result.getClass(), equalTo(BooleanQueryImpl.class));
	}

	@Test
	public void createFacet_WhenNoError_ThenReturnsAnewFacetConfiguredWithTheSpecifiedName() {
		String name = "nameVal";
		when(mockFacetFactory.newInstance(mockSearchContext)).thenReturn(mockFacet);

		Facet result = searchUtil.createFacet(name, mockSearchContext);

		assertThat(result, sameInstance(mockFacet));
		verify(mockFacet, times(1)).setFieldName(name);
	}

	@Test
	public void executeSearch_WhenNoError_ThenConfiguresTheStartAndEndInTheSearchContextBeforeExecutingThesearch() throws SearchException {
		mockSearchContainer = mock(SearchContainer.class);
		int start = 11;
		int end = 23;
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		when(mockSearchContainer.getStart()).thenReturn(start);
		when(mockSearchContainer.getEnd()).thenReturn(end);

		searchUtil.executeSearch(mockSearchContext, mockSearchContainer);

		InOrder inOrder = inOrder(mockSearchContext, mockFacetedSearcherManager);
		inOrder.verify(mockSearchContext, times(1)).setStart(start);
		inOrder.verify(mockSearchContext, times(1)).setEnd(end);
		inOrder.verify(mockFacetedSearcherManager, times(1)).createFacetedSearcher();
	}

	@Test
	public void executeSearch_WhenNoError_ThenReturnsTheSearchResults() throws SearchException {
		mockSearchContainer = mock(SearchContainer.class);
		when(mockFacetedSearcherManager.createFacetedSearcher()).thenReturn(mockFacetedSearcher);
		when(mockFacetedSearcher.search(mockSearchContext)).thenReturn(mockHits);

		Hits results = searchUtil.executeSearch(mockSearchContext, mockSearchContainer);

		assertThat(results, sameInstance(mockHits));
	}

	@Test
	public void getBaseSearchContainer_WhenNoError_ThenReturnsTheSearchContainer() {
		int cur = 11;
		int delta = 23;
		String urlValue = "urlValue";
		when(ParamUtil.getInteger(mockHttpServletRequest, SearchContainer.DEFAULT_CUR_PARAM, 0)).thenReturn(cur);
		when(ParamUtil.getInteger(mockHttpServletRequest, SearchContainer.DEFAULT_DELTA_PARAM, 0)).thenReturn(delta);
		when(ParamUtil.getInteger(mockRenderRequest, SearchContainer.DEFAULT_DELTA_PARAM, delta)).thenReturn(delta);
		when(mockSearchHttpUtil.getCompleteOriginalURL(mockHttpServletRequest)).thenReturn(urlValue);
		when(mockSearchHttpUtil.getNullPortletURL(urlValue)).thenReturn(mockPortletURL);

		SearchContainer<Object> result = searchUtil.getBaseSearchContainer(mockRenderRequest, mockHttpServletRequest, mockSharedSearchContributorSettingsImpl);

		assertThat(result.getPortletRequest(), sameInstance(mockRenderRequest));
		assertNull(result.getDisplayTerms());
		assertNull(result.getSearchTerms());
		assertThat(result.getCurParam(), equalTo(SearchContainer.DEFAULT_CUR_PARAM));
		assertThat(result.getCur(), equalTo(cur));
		assertThat(result.getDelta(), equalTo(delta));
		assertThat(result.getIteratorURL(), sameInstance(mockPortletURL));
		assertNull(result.getHeaderNames());
		assertNull(result.getEmptyResultsMessage());
	}

	@Test
	public void getBaseSearchContainer_WhenNoErrorAndRequestHasDeltaParameter_ThenSetsRequestDeltaParameterOnSearchContainer() {

		int delta = 23;
		String urlValue = "urlValue";

		when(ParamUtil.getInteger(mockHttpServletRequest, SearchContainer.DEFAULT_DELTA_PARAM, 0)).thenReturn(delta);
		when(ParamUtil.getInteger(mockRenderRequest, SearchContainer.DEFAULT_DELTA_PARAM, delta)).thenReturn(delta);
		when(mockSearchHttpUtil.getCompleteOriginalURL(mockHttpServletRequest)).thenReturn(urlValue);
		when(mockSearchHttpUtil.getNullPortletURL(urlValue)).thenReturn(mockPortletURL);

		SearchContainer<Object> result = searchUtil.getBaseSearchContainer(mockRenderRequest, mockHttpServletRequest, mockSharedSearchContributorSettingsImpl);

		assertThat(result.getDelta(), equalTo(delta));
	}

	@Test
	public void getBaseSearchContainer_WhenNoErrorAndRequestNotHasDeltaParameterAndHasSharedSessiontDeltaAttribute_ThenSetsSharedSessiontDeltaAttributeOnSearchContainer() {

		int sessionDelta = 23;
		String urlValue = "urlValue";

		when(ParamUtil.getInteger(mockHttpServletRequest, SearchContainer.DEFAULT_DELTA_PARAM, 0)).thenReturn(0);
		when(mockSharedSearchContributorSettingsImpl.getSharedSearchSessionSingleValuedAttribute(SharedSearchAttributeKeys.SEARCH_CONTAINER_DELTA))
				.thenReturn(Optional.of(String.valueOf(sessionDelta)));
		when(ParamUtil.getInteger(mockRenderRequest, SearchContainer.DEFAULT_DELTA_PARAM, sessionDelta)).thenReturn(sessionDelta);

		when(mockSearchHttpUtil.getCompleteOriginalURL(mockHttpServletRequest)).thenReturn(urlValue);
		when(mockSearchHttpUtil.getNullPortletURL(urlValue)).thenReturn(mockPortletURL);

		SearchContainer<Object> result = searchUtil.getBaseSearchContainer(mockRenderRequest, mockHttpServletRequest, mockSharedSearchContributorSettingsImpl);

		assertThat(result.getDelta(), equalTo(sessionDelta));
	}

	@Test
	public void getBaseSearchContainer_WhenNoErrorAndRequestNotHasDeltaParameterAndNotHasSharedSessiontDeltaAttribute_ThenSetsDefaultDeltaValueOnSearchContainer() {

		String urlValue = "urlValue";

		when(ParamUtil.getInteger(mockHttpServletRequest, SearchContainer.DEFAULT_DELTA_PARAM, 0)).thenReturn(0);
		when(mockSharedSearchContributorSettingsImpl.getSharedSearchSessionSingleValuedAttribute(SharedSearchAttributeKeys.SEARCH_CONTAINER_DELTA)).thenReturn(Optional.empty());

		when(mockSearchHttpUtil.getCompleteOriginalURL(mockHttpServletRequest)).thenReturn(urlValue);
		when(mockSearchHttpUtil.getNullPortletURL(urlValue)).thenReturn(mockPortletURL);

		SearchContainer<Object> result = searchUtil.getBaseSearchContainer(mockRenderRequest, mockHttpServletRequest, mockSharedSearchContributorSettingsImpl);

		assertThat(result.getDelta(), equalTo(SearchContainer.DEFAULT_DELTA));
	}

	@Test
	public void getBaseSearchContext_WhenNoError_ThenReturnsTheConfiguredSearchContext() {
		long companyId = 112;
		long userId = 44;
		Locale locale = Locale.CANADA_FRENCH;
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockThemeDisplay.getTimeZone()).thenReturn(mockTimeZone);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);

		SearchContext result = searchUtil.getBaseSearchContext(mockThemeDisplay);

		assertThat(result.getCompanyId(), equalTo(companyId));
		assertThat(result.getLayout(), sameInstance(mockLayout));
		assertThat(result.getLocale(), sameInstance(locale));
		assertThat(result.getTimeZone(), sameInstance(mockTimeZone));
		assertThat(result.getUserId(), equalTo(userId));
	}

	@Test
	public void getFacetMultiplier_WhenConfiguredFacetMultiplierIsGreaterThanOne_ThenReturnsConfiguredFacetMultiplier() {
		long companyId = 2323L;
		int facetMultiplier = 3;

		when(mockSearchConfigurationService.getConfiguredFacetMultiplier(companyId)).thenReturn(facetMultiplier);

		int result = searchUtil.getFacetMultiplier(companyId);

		assertThat(result, equalTo(facetMultiplier));
	}

	@Test
	public void getFacetMultiplier_WhenConfiguredFacetMultiplierIsSmallerThanOne_ThenReturnsOne() {
		long companyId = 2323L;

		when(mockSearchConfigurationService.getConfiguredFacetMultiplier(companyId)).thenReturn(0);

		int result = searchUtil.getFacetMultiplier(companyId);

		assertThat(result, equalTo(1));
	}

}
