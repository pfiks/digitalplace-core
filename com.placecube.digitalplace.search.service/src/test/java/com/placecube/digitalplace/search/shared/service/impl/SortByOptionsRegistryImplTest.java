package com.placecube.digitalplace.search.shared.service.impl;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.search.shared.model.SortByOption;

public class SortByOptionsRegistryImplTest extends PowerMockito {

	@Mock
	private SortByOption mockSortByOption1;

	@Mock
	private SortByOption mockSortByOption2;

	@Mock
	private SortByOption mockSortByOption3;

	@InjectMocks
	private SortByOptionsRegistryImpl sortByOptionsRegistryImpl;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getAllAvailableSortByOptions_WhenNoError_ThenReturnsAllTheRegisterdOptions() {
		mockSortOptionDetails(mockSortByOption1, "field1");
		mockSortOptionDetails(mockSortByOption2, "field2");
		mockSortOptionDetails(mockSortByOption3, "field3");

		List<SortByOption> results = sortByOptionsRegistryImpl.getAllAvailableSortByOptions();
		assertThat(results, containsInAnyOrder(mockSortByOption1, mockSortByOption2, mockSortByOption3));

		sortByOptionsRegistryImpl.unregisterSortByOption(mockSortByOption1);

		results = sortByOptionsRegistryImpl.getAllAvailableSortByOptions();
		assertThat(results, containsInAnyOrder(mockSortByOption2, mockSortByOption3));
	}

	@Test
	public void getAvailableSortByOptionsForFields_WhenFieldNamesToMatchHasNotValidValues_ThenReturnsEmptyList() {
		mockSortOptionDetails(mockSortByOption1, "field1");
		mockSortOptionDetails(mockSortByOption2, "field2");
		mockSortOptionDetails(mockSortByOption3, "field3");

		List<SortByOption> results = sortByOptionsRegistryImpl.getAvailableSortByOptionsForIds(new String[] { null, "notValidFieldName", StringPool.BLANK });
		assertTrue(results.isEmpty());
	}

	@Test
	public void getAvailableSortByOptionsForFields_WhenFieldNamesToMatchHasValidValues_ThenReturnsListWithSortByOptionsWithMatchingFieldNames() {
		mockSortOptionDetails(mockSortByOption1, "field1");
		mockSortOptionDetails(mockSortByOption2, "field2");
		mockSortOptionDetails(mockSortByOption3, "field3");

		List<SortByOption> results = sortByOptionsRegistryImpl.getAvailableSortByOptionsForIds(new String[] { "field1", "field3" });
		assertThat(results, containsInAnyOrder(mockSortByOption1, mockSortByOption3));

		sortByOptionsRegistryImpl.unregisterSortByOption(mockSortByOption1);

		results = sortByOptionsRegistryImpl.getAvailableSortByOptionsForIds(new String[] { "field1", "field3" });
		assertThat(results, containsInAnyOrder(mockSortByOption3));
	}

	@Test
	public void getAvailableSortByOptionsForFields_WhenFieldNamesToMatchIsEmpty_ThenReturnsEmptyList() {
		List<SortByOption> results = sortByOptionsRegistryImpl.getAvailableSortByOptionsForIds(new String[0]);

		assertTrue(results.isEmpty());
	}

	@Test
	public void getAvailableSortByOptionsForFields_WhenFieldNamesToMatchIsNull_ThenReturnsEmptyList() {
		List<SortByOption> results = sortByOptionsRegistryImpl.getAvailableSortByOptionsForIds(null);

		assertTrue(results.isEmpty());
	}

	private void mockSortOptionDetails(SortByOption sortByOption, String fieldName) {
		when(sortByOption.getId()).thenReturn(fieldName);
		sortByOptionsRegistryImpl.registerSortByOption(sortByOption);
	}

}
