/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link GroupApiKeyLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see GroupApiKeyLocalService
 * @generated
 */
public class GroupApiKeyLocalServiceWrapper
	implements GroupApiKeyLocalService,
			   ServiceWrapper<GroupApiKeyLocalService> {

	public GroupApiKeyLocalServiceWrapper() {
		this(null);
	}

	public GroupApiKeyLocalServiceWrapper(
		GroupApiKeyLocalService groupApiKeyLocalService) {

		_groupApiKeyLocalService = groupApiKeyLocalService;
	}

	/**
	 * Adds the group api key to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupApiKeyLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupApiKey the group api key
	 * @return the group api key that was added
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupApiKey
		addGroupApiKey(
			com.placecube.digitalplace.group.rest.model.GroupApiKey
				groupApiKey) {

		return _groupApiKeyLocalService.addGroupApiKey(groupApiKey);
	}

	/**
	 * Creates a new group api key with the primary key. Does not add the group api key to the database.
	 *
	 * @param groupApiKeyPK the primary key for the new group api key
	 * @return the new group api key
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupApiKey
		createGroupApiKey(
			com.placecube.digitalplace.group.rest.service.persistence.
				GroupApiKeyPK groupApiKeyPK) {

		return _groupApiKeyLocalService.createGroupApiKey(groupApiKeyPK);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupApiKeyLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the group api key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupApiKeyLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupApiKey the group api key
	 * @return the group api key that was removed
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupApiKey
		deleteGroupApiKey(
			com.placecube.digitalplace.group.rest.model.GroupApiKey
				groupApiKey) {

		return _groupApiKeyLocalService.deleteGroupApiKey(groupApiKey);
	}

	/**
	 * Deletes the group api key with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupApiKeyLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key that was removed
	 * @throws PortalException if a group api key with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupApiKey
			deleteGroupApiKey(
				com.placecube.digitalplace.group.rest.service.persistence.
					GroupApiKeyPK groupApiKeyPK)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupApiKeyLocalService.deleteGroupApiKey(groupApiKeyPK);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupApiKeyLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _groupApiKeyLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _groupApiKeyLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _groupApiKeyLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _groupApiKeyLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _groupApiKeyLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _groupApiKeyLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _groupApiKeyLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _groupApiKeyLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.group.rest.model.GroupApiKey
		fetchGroupApiKey(
			com.placecube.digitalplace.group.rest.service.persistence.
				GroupApiKeyPK groupApiKeyPK) {

		return _groupApiKeyLocalService.fetchGroupApiKey(groupApiKeyPK);
	}

	@Override
	public String generateKey(
			com.placecube.digitalplace.group.rest.constants.GroupRestKeyType
				keyType)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		return _groupApiKeyLocalService.generateKey(keyType);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _groupApiKeyLocalService.getActionableDynamicQuery();
	}

	@Override
	public String getEncodedTextWithGroupKey(
			com.liferay.portal.kernel.model.Group group, String textToEncode)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		return _groupApiKeyLocalService.getEncodedTextWithGroupKey(
			group, textToEncode);
	}

	@Override
	public String getEncodedTextWithKey(
			String applicationKey, String textToEncode)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		return _groupApiKeyLocalService.getEncodedTextWithKey(
			applicationKey, textToEncode);
	}

	/**
	 * Returns the group api key with the primary key.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key
	 * @throws PortalException if a group api key with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupApiKey
			getGroupApiKey(
				com.placecube.digitalplace.group.rest.service.persistence.
					GroupApiKeyPK groupApiKeyPK)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupApiKeyLocalService.getGroupApiKey(groupApiKeyPK);
	}

	/**
	 * Returns a range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of group api keys
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.group.rest.model.GroupApiKey>
			getGroupApiKeys(int start, int end) {

		return _groupApiKeyLocalService.getGroupApiKeys(start, end);
	}

	/**
	 * Returns the number of group api keys.
	 *
	 * @return the number of group api keys
	 */
	@Override
	public int getGroupApiKeysCount() {
		return _groupApiKeyLocalService.getGroupApiKeysCount();
	}

	@Override
	public String getGroupApplicationKey(long companyId, long groupId) {
		return _groupApiKeyLocalService.getGroupApplicationKey(
			companyId, groupId);
	}

	@Override
	public String getGroupApplicationSecret(long companyId, long groupId) {
		return _groupApiKeyLocalService.getGroupApplicationSecret(
			companyId, groupId);
	}

	@Override
	public com.liferay.portal.kernel.model.Group getGroupByKey(
			com.placecube.digitalplace.group.rest.constants.GroupRestKeyType
				keyType,
			String keyValue)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		return _groupApiKeyLocalService.getGroupByKey(keyType, keyValue);
	}

	@Override
	public String getGroupRestKeyValue(
		com.liferay.portal.kernel.model.Group group,
		com.placecube.digitalplace.group.rest.constants.GroupRestKeyType
			keyType) {

		return _groupApiKeyLocalService.getGroupRestKeyValue(group, keyType);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _groupApiKeyLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _groupApiKeyLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _groupApiKeyLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public void regenerateGroupApiKey(
			com.liferay.portal.kernel.model.Group group)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		_groupApiKeyLocalService.regenerateGroupApiKey(group);
	}

	/**
	 * Updates the group api key in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupApiKeyLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupApiKey the group api key
	 * @return the group api key that was updated
	 */
	@Override
	public com.placecube.digitalplace.group.rest.model.GroupApiKey
		updateGroupApiKey(
			com.placecube.digitalplace.group.rest.model.GroupApiKey
				groupApiKey) {

		return _groupApiKeyLocalService.updateGroupApiKey(groupApiKey);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _groupApiKeyLocalService.getBasePersistence();
	}

	@Override
	public GroupApiKeyLocalService getWrappedService() {
		return _groupApiKeyLocalService;
	}

	@Override
	public void setWrappedService(
		GroupApiKeyLocalService groupApiKeyLocalService) {

		_groupApiKeyLocalService = groupApiKeyLocalService;
	}

	private GroupApiKeyLocalService _groupApiKeyLocalService;

}