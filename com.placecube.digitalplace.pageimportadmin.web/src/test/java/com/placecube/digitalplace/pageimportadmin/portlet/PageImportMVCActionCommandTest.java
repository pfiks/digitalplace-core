package com.placecube.digitalplace.pageimportadmin.portlet;

import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.apache.poi.ss.usermodel.Sheet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.document.library.kernel.antivirus.AntivirusScannerException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.pageimportadmin.model.PageImportModel;
import com.placecube.digitalplace.pageimportadmin.model.RowImportModel;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;
import com.placecube.digitalplace.pageimportadmin.service.ContentImportService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ SessionErrors.class, ParamUtil.class, PropsUtil.class })
public class PageImportMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private PageImportMVCActionCommand pageImportMVCActionCommand;

	private static final long GROUP_ID = 1;
	private static final Locale LOCALE = Locale.FRENCH;

	@Mock
	private ContentImportService mockContentImportService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private File mockFile;

	@Mock
	private PageImportModel mockPageImportModel;

	@Mock
	private WebContentImportModel mockWebContentImportModel;

	@Mock
	private RowImportModel mockRowImportModel;

	@Mock
	private Sheet mockSheet;

	@Before
	public void setUp() {
		mockStatic(PropsUtil.class, SessionErrors.class, ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenRetrievingFirstReturnsEmptyOptional_ThenSetsSessionErrorFileContentMissing() throws Exception {
		mockThemeDisplayDetails();
		when(mockContentImportService.getFile(mockActionRequest)).thenReturn(mockFile);
		when(mockContentImportService.getFirstSheet(mockFile)).thenReturn(Optional.empty());

		pageImportMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "PageImportError");

		verify(mockContentImportService, never()).createPage(any(PageImportModel.class), any(Locale.class), any(ActionRequest.class));
		verify(mockContentImportService, never()).createWebContent(any(WebContentImportModel.class), any(Locale.class), any(ActionRequest.class));
		verify(mockActionRequest, times(1)).setAttribute("errorMessage", "could-not-find-content-in-file-provided");
	}

	@Test
	public void doProcessAction_WhenRetrievingFileThrowsException_ThenSetsSessionError() throws Exception {
		mockThemeDisplayDetails();
		when(mockContentImportService.getFile(mockActionRequest)).thenThrow(new AntivirusScannerException(new Exception()));

		pageImportMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "PageImportError");

		verify(mockContentImportService, never()).createPage(any(PageImportModel.class), any(Locale.class), any(ActionRequest.class));
		verify(mockContentImportService, never()).createWebContent(any(WebContentImportModel.class), any(Locale.class), any(ActionRequest.class));
		verify(mockActionRequest, times(1)).setAttribute("errorMessage", "something-went-wrong");
	}

	@Test
	public void doProcessAction_WhenRetrievingFirstSheetThrowsPortletException_ThenSetsSessionErrorSomethingWentWrong() throws Exception {
		mockThemeDisplayDetails();
		when(mockContentImportService.getFile(mockActionRequest)).thenReturn(mockFile);
		when(mockContentImportService.getFirstSheet(mockFile)).thenThrow(new PortletException());

		pageImportMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "PageImportError");

		verify(mockContentImportService, never()).createPage(any(PageImportModel.class), any(Locale.class), any(ActionRequest.class));
		verify(mockContentImportService, never()).createWebContent(any(WebContentImportModel.class), any(Locale.class), any(ActionRequest.class));
		verify(mockActionRequest, times(1)).setAttribute("errorMessage", "something-went-wrong");
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoError_ThenImportsTheItems(boolean privateLayout) throws Exception {
		long assetVocabularyId = 123l;
		mockThemeDisplayDetails();
		when(ParamUtil.getBoolean(mockActionRequest, "importAsPrivate")).thenReturn(privateLayout);
		when(ParamUtil.getLong(mockActionRequest, "assetVocabularyId")).thenReturn(assetVocabularyId);
		when(mockContentImportService.getFile(mockActionRequest)).thenReturn(mockFile);
		when(mockContentImportService.getFirstSheet(mockFile)).thenReturn(Optional.of(mockSheet));
		List<RowImportModel> rowImportModels = new LinkedList<>();
		rowImportModels.add(mockPageImportModel);
		rowImportModels.add(mockWebContentImportModel);
		when(mockSheet.getLastRowNum()).thenReturn(1);
		when(mockContentImportService.getRowContent(mockSheet, 0, GROUP_ID, LOCALE, privateLayout, assetVocabularyId)).thenReturn(mockPageImportModel);
		when(mockContentImportService.getRowContent(mockSheet, 1, GROUP_ID, LOCALE, privateLayout, assetVocabularyId)).thenReturn(mockWebContentImportModel);
		when(mockWebContentImportModel.isPageImport()).thenReturn(false);
		when(mockWebContentImportModel.isWebContent()).thenReturn(true);
		when(mockPageImportModel.isPageImport()).thenReturn(true);
		when(mockPageImportModel.isWebContent()).thenReturn(false);

		pageImportMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockContentImportService, times(1)).createPage(mockPageImportModel, LOCALE, mockActionRequest);
		verify(mockContentImportService, times(1)).createWebContent(mockWebContentImportModel, LOCALE, mockActionRequest);
		verify(mockActionRequest, times(1)).setAttribute("rowImportModels", rowImportModels);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenRowContentReturnsEmptyModel_ThenDoesNotImportEmptyItemsWithErrors(boolean privateLayout) throws Exception {
		long assetVocabularyId = 123l;
		mockThemeDisplayDetails();
		when(ParamUtil.getBoolean(mockActionRequest, "importAsPrivate")).thenReturn(privateLayout);
		when(ParamUtil.getLong(mockActionRequest, "assetVocabularyId")).thenReturn(assetVocabularyId);
		when(mockContentImportService.getFile(mockActionRequest)).thenReturn(mockFile);
		when(mockContentImportService.getFirstSheet(mockFile)).thenReturn(Optional.of(mockSheet));
		List<RowImportModel> rowImportModels = new LinkedList<>();
		rowImportModels.add(mockRowImportModel);
		rowImportModels.add(mockRowImportModel);
		when(mockSheet.getLastRowNum()).thenReturn(1);
		when(mockContentImportService.getRowContent(mockSheet, 0, GROUP_ID, LOCALE, privateLayout, assetVocabularyId)).thenReturn(mockRowImportModel);
		when(mockContentImportService.getRowContent(mockSheet, 1, GROUP_ID, LOCALE, privateLayout, assetVocabularyId)).thenReturn(mockRowImportModel);
		when(mockWebContentImportModel.isWebContent()).thenReturn(false);
		when(mockWebContentImportModel.isPageImport()).thenReturn(false);

		pageImportMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockContentImportService, never()).createPage(any() , any() , any() );
		verify(mockContentImportService, never()).createWebContent(any() , any() , any());
		verify(mockActionRequest, times(1)).setAttribute("rowImportModels", rowImportModels);
	}

	private void mockThemeDisplayDetails() {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockThemeDisplay.getLocale()).thenReturn(LOCALE);
	}
}
