package com.placecube.digitalplace.search.web.portlet.searchbar.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

@Component(immediate = true, property = "javax.portlet.name=" + com.placecube.digitalplace.search.web.constants.PortletKeys.SEARCH_BAR, service = ConfigurationAction.class)
public class SearchBarPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		SearchBarPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(SearchBarPortletInstanceConfiguration.class, themeDisplay);

		httpServletRequest.setAttribute(SearchBarPortletConfigurationConstants.DESTINATION_PAGE, configuration.destinationPage());
		httpServletRequest.setAttribute(SearchBarPortletConfigurationConstants.SEARCH_GUEST_GROUP, configuration.searchGuestGroup());

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String destinationPage = ParamUtil.getString(actionRequest, SearchBarPortletConfigurationConstants.DESTINATION_PAGE, StringPool.BLANK);
		String searchGuestGroup = ParamUtil.getString(actionRequest, SearchBarPortletConfigurationConstants.SEARCH_GUEST_GROUP, StringPool.FALSE);

		setPreference(actionRequest, SearchBarPortletConfigurationConstants.DESTINATION_PAGE, destinationPage);
		setPreference(actionRequest, SearchBarPortletConfigurationConstants.SEARCH_GUEST_GROUP, searchGuestGroup);
		super.processAction(portletConfig, actionRequest, actionResponse);
	}
}