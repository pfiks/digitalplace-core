package com.placecube.digitalplace.payment.capita.service.internal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portletfilerepository.PortletFileRepository;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;

@Component(immediate = true, service = OneOffPaymentOrderFileService.class)
public class OneOffPaymentOrderFileService {

	private static final Log LOG = LogFactoryUtil.getLog(OneOffPaymentOrderFileService.class);

	@Reference
	private OneOffPaymentHelper oneOffPaymentHelper;

	public Optional<OneOffPaymentOrder> loadPaymentTempFile(String paymentReference, ServiceContext serviceContext) {

		OneOffPaymentOrder order = null;

		if (Validator.isNotNull(paymentReference)) {
			try {
				Folder folder = oneOffPaymentHelper.getOrAddPaymentTempFilesFolder(serviceContext);
				FileEntry fileEntry = oneOffPaymentHelper.getPortletFileEntry(paymentReference, folder);

				InputStream fin = fileEntry.getContentStream();
				ObjectInputStream ois = new ObjectInputStream(fin);

				order = (OneOffPaymentOrder) ois.readObject();

				LOG.debug("Loaded file with fileEntryId: " + fileEntry.getFileEntryId() + " from folder with folderId: " + folder.getFolderId());

			} catch (Exception e) {
				LOG.error(e);
				LOG.debug("Stored order not found for paymentReference:" + paymentReference);
			}
		}
		return Optional.ofNullable(order);

	}

	public void uploadPaymentTempFile(OneOffPaymentOrder oneOffPaymentOrder, ServiceContext serviceContext) throws IOException, PortalException {

		File tempFile = oneOffPaymentHelper.createPaymentTempFile(oneOffPaymentOrder);

		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(tempFile))) {

			oos.writeObject(oneOffPaymentOrder);
			Folder folder = oneOffPaymentHelper.getOrAddPaymentTempFilesFolder(serviceContext);

			FileEntry fileEntry = oneOffPaymentHelper.addOrReplaceFileEntry(oneOffPaymentOrder.getInternalReference(), tempFile, folder, serviceContext);

			LOG.debug("Added file with fileEntryId: " + fileEntry.getFileEntryId() + " to folder with folderId: " + folder.getFolderId());

		} finally {
			FileUtil.delete(tempFile);
		}

	}

}
