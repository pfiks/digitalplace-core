package com.placecube.digitalplace.userindexer.internal.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.placecube.digitalplace.userindexer.constants.UserIndexerFields;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserIndexerFields.class)
public class UserRoleServiceTest extends PowerMockito {

	@InjectMocks
	private UserRoleService userRoleService;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private UserGroupRoleLocalService mockUserGroupRoleLocalService;

	@Mock
	private User mockUser;

	@Mock
	private UserGroupRole mockUserGroupRole;

	@Mock
	private UserGroup mockUserGroup;

	@Mock
	private UserGroup mockUserGroup2;

	@Mock
	private Role mockRole;

	@Before
	public void setUp() {
		mockStatic(UserIndexerFields.class);
	}

	@Test
	public void getGroupRoleIdsFieldValues_WhenUserHasNotGroupRole_ThenReturnsEmptyList() {

		long userId = 1;
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(userId)).thenReturn(Collections.emptyList());

		List<String> result = userRoleService.getGroupRoleIdsFieldValues(mockUser);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getGroupRoleIdsFieldValues_WhenUserHasGroupRoles_ThenReturnsUserGroupRoleSearchValues() {

		long userId = 1;
		when(mockUser.getUserId()).thenReturn(userId);
		List<UserGroupRole> userGroupRoles = new ArrayList<>();
		long groupId = 2;
		long roleId = 3;
		when(mockUserGroupRole.getGroupId()).thenReturn(groupId);
		when(mockUserGroupRole.getRoleId()).thenReturn(roleId);
		userGroupRoles.add(mockUserGroupRole);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(userId)).thenReturn(userGroupRoles);
		String userGroupRoleSearchValue = groupId + StringPool.DASH + roleId;
		when(UserIndexerFields.getUserGroupRoleSearchValue(groupId, roleId)).thenReturn(userGroupRoleSearchValue);

		List<String> result = userRoleService.getGroupRoleIdsFieldValues(mockUser);

		assertThat(result, contains(userGroupRoleSearchValue));
	}

	@Test
	public void getInheritedRegularRoleIds_WhenUserHasNotUserGroups_ThenReturnsEmptySet() {

		when(mockUser.getUserGroups()).thenReturn(Collections.emptyList());

		Set<Long> result = userRoleService.getInheritedRegularRoleIds(mockUser);

		assertTrue(result.isEmpty());

	}

	@Test
	public void getInheritedRegularRoleIds_WhenUserHasUserGroups_ThenReturnsRoleIds() throws Exception {

		List<UserGroup> userGroups = new ArrayList<>();
		when(mockUser.getUserGroups()).thenReturn(userGroups);
		userGroups.add(mockUserGroup);
		long groupId = 2;
		when(mockUserGroup.getGroupId()).thenReturn(groupId);

		List<Role> roles = new ArrayList<>();
		roles.add(mockRole);
		long roleId = 3;
		when(mockRole.getRoleId()).thenReturn(roleId);
		when(mockRoleLocalService.getGroupRoles(groupId)).thenReturn(roles);

		Set<Long> result = userRoleService.getInheritedRegularRoleIds(mockUser);

		assertThat(result, contains(roleId));

	}

	@Test
	public void getInheritedRegularRoleIds_WhenExceptionGettingGroupId_ThenNoExceptionIsThrown() throws Exception {

		List<UserGroup> userGroups = new ArrayList<>();
		when(mockUser.getUserGroups()).thenReturn(userGroups);
		userGroups.add(mockUserGroup);
		long groupId = 2;
		when(mockUserGroup.getGroupId()).thenReturn(groupId);
		userGroups.add(mockUserGroup2);
		when(mockUserGroup2.getGroupId()).thenThrow(new PortalException());

		List<Role> roles = new ArrayList<>();
		roles.add(mockRole);
		long roleId = 3;
		when(mockRole.getRoleId()).thenReturn(roleId);
		when(mockRoleLocalService.getGroupRoles(groupId)).thenReturn(roles);

		try {
			Set<Long> result = userRoleService.getInheritedRegularRoleIds(mockUser);
			assertThat(result.size(), equalTo(1));
			assertThat(result, contains(roleId));

		} catch (Exception e) {
			fail();
		}

	}
}
