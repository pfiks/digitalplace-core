package com.placecube.digitalplace.oauth.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.liferay.oauth2.provider.service.OAuth2ApplicationLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.placecube.digitalplace.oauth.util.URLUtil;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DigitalPlaceOAuth2ServiceImplTest {

	private static final long COMPANY_ID = 43332l;
	private static final String CLIENT_ID = "5433";
	private static final String CLIENT_SECRET = "sdfsdfs-sdfsdf23";

	@InjectMocks
	private DigitalPlaceOAuth2ServiceImpl digitalPlaceOAuth2ServiceImpl;

	@Mock
	private CompanyLocalService mockCompanyLocalService;

	@Mock
	private OAuth2ApplicationLocalService mockOAuth2ApplicationLocalService;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private Company mockCompany;

	@Mock
	private URL mockURL;

	@Mock
	private HttpURLConnection mockHttpURLConnection;

	@Mock
	private URLUtil mockURLUtil;

	@Mock
	private JSONObject mockJSONObject;

	@Test
	public void getOAuth2Token_WhenNoErrors_ThenReturnsAuthorisationToken() throws Exception {
		final String response = "response";
		final String portalURL = "http://portal:8080";
		final String token = "323uy3262df";

		when(mockCompanyLocalService.getCompany(COMPANY_ID)).thenReturn(mockCompany);
		when(mockCompany.getPortalURL(0)).thenReturn(portalURL);

		final String oauth2AuthorizationUrl = portalURL + "/o/oauth2/token";

		final String urlParameters = "grant_type=client_credentials" + "&client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET;

		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		when(mockURLUtil.buildURL(oauth2AuthorizationUrl)).thenReturn(mockURL);
		when(mockURLUtil.getConnection(mockURL, postDataLength)).thenReturn(mockHttpURLConnection);
		when(mockHttpURLConnection.getResponseCode()).thenReturn(200);
		when(mockURLUtil.getResponse(mockHttpURLConnection)).thenReturn(response);
		when(mockJSONFactory.createJSONObject(response)).thenReturn(mockJSONObject);
		when(mockJSONObject.get("access_token")).thenReturn(token);

		String result = digitalPlaceOAuth2ServiceImpl.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET);

		assertThat(result, equalTo(token));
	}

	@Test
	public void getOAuth2Token_WhenNoErrors_ThenWritesPostDataGetsResponseAndClosesConnectionInOrder() throws Exception {
		final String response = "response";
		final String portalURL = "http://portal:8080";
		final String token = "323uy3262df";

		when(mockCompanyLocalService.getCompany(COMPANY_ID)).thenReturn(mockCompany);
		when(mockCompany.getPortalURL(0)).thenReturn(portalURL);

		final String oauth2AuthorizationUrl = portalURL + "/o/oauth2/token";

		final String urlParameters = "grant_type=client_credentials" + "&client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET;

		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		when(mockURLUtil.buildURL(oauth2AuthorizationUrl)).thenReturn(mockURL);
		when(mockURLUtil.getConnection(mockURL, postDataLength)).thenReturn(mockHttpURLConnection);
		when(mockHttpURLConnection.getResponseCode()).thenReturn(200);
		when(mockURLUtil.getResponse(mockHttpURLConnection)).thenReturn(response);
		when(mockJSONFactory.createJSONObject(response)).thenReturn(mockJSONObject);
		when(mockJSONObject.get("access_token")).thenReturn(token);

		digitalPlaceOAuth2ServiceImpl.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET);

		InOrder inOrder = inOrder(mockURLUtil, mockHttpURLConnection);

		inOrder.verify(mockURLUtil, times(1)).writePostData(mockHttpURLConnection, postData);
		inOrder.verify(mockURLUtil, times(1)).getResponse(mockHttpURLConnection);
		inOrder.verify(mockHttpURLConnection, times(1)).disconnect();
	}

	@Test(expected = PortalException.class)
	public void getOAuth2Token_WhenResponseIsNot200_ThenThrowsPortalException() throws Exception {
		final String portalURL = "http://portal:8080";

		when(mockCompanyLocalService.getCompany(COMPANY_ID)).thenReturn(mockCompany);
		when(mockCompany.getPortalURL(0)).thenReturn(portalURL);

		final String oauth2AuthorizationUrl = portalURL + "/o/oauth2/token";

		final String urlParameters = "grant_type=client_credentials" + "&client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET;

		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		when(mockURLUtil.buildURL(oauth2AuthorizationUrl)).thenReturn(mockURL);
		when(mockURLUtil.getConnection(mockURL, postDataLength)).thenReturn(mockHttpURLConnection);
		when(mockHttpURLConnection.getResponseCode()).thenReturn(500);

		digitalPlaceOAuth2ServiceImpl.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET);
	}

	@Test(expected = PortalException.class)
	public void getOAuth2Token_WhenUrlIsNotValid_ThenThrowsPortalException() throws Exception {
		final String portalURL = "http://portal:8080";

		when(mockCompanyLocalService.getCompany(COMPANY_ID)).thenReturn(mockCompany);
		when(mockCompany.getPortalURL(0)).thenReturn(portalURL);

		final String oauth2AuthorizationUrl = portalURL + "/o/oauth2/token";

		when(mockURLUtil.buildURL(oauth2AuthorizationUrl)).thenThrow(new MalformedURLException());

		digitalPlaceOAuth2ServiceImpl.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET);
	}

	@Test(expected = PortalException.class)
	public void getOAuth2Token_WhenConnectionFails_ThenThrowsPortalException() throws Exception {
		final String portalURL = "http://portal:8080";

		when(mockCompanyLocalService.getCompany(COMPANY_ID)).thenReturn(mockCompany);
		when(mockCompany.getPortalURL(0)).thenReturn(portalURL);

		final String oauth2AuthorizationUrl = portalURL + "/o/oauth2/token";

		final String urlParameters = "grant_type=client_credentials" + "&client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET;

		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		when(mockURLUtil.buildURL(oauth2AuthorizationUrl)).thenReturn(mockURL);
		when(mockURLUtil.getConnection(mockURL, postDataLength)).thenThrow(new IOException());

		digitalPlaceOAuth2ServiceImpl.getOAuth2Token(COMPANY_ID, CLIENT_ID, CLIENT_SECRET);
	}

}
