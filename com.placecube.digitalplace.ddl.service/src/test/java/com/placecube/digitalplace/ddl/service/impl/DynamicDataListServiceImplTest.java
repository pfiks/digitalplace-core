package com.placecube.digitalplace.ddl.service.impl;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.placecube.digitalplace.ddl.internal.util.DynamicDataListEntryRetrievalUtil;
import com.placecube.digitalplace.ddl.model.DynamicDataListEntry;

public class DynamicDataListServiceImplTest extends PowerMockito {

	@InjectMocks
	private DynamicDataListServiceImpl dynamicDataListServiceImpl;

	@Mock
	private DDLRecord mockDDLRecord1;

	@Mock
	private DDLRecord mockDDLRecord2;

	@Mock
	private DDLRecord mockDDLRecord3;

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private DynamicDataListEntry mockDynamicDataListEntry1;

	@Mock
	private DynamicDataListEntry mockDynamicDataListEntry2;

	@Mock
	private DynamicDataListEntryRetrievalUtil mockDynamicDataListEntryRetrievalUtil;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getDDLRecordSetEntries_WhenNoError_ThenReturnsListWithTheValidRecords() {
		long recordSetId = 12;
		List<DDLRecord> records = new ArrayList<>();
		records.add(mockDDLRecord1);
		records.add(mockDDLRecord2);
		records.add(mockDDLRecord3);
		when(mockDDLRecordLocalService.getRecords(recordSetId)).thenReturn(records);
		when(mockDynamicDataListEntryRetrievalUtil.getDynamicDataListEntry(mockDDLRecord1)).thenReturn(Optional.of(mockDynamicDataListEntry1));
		when(mockDynamicDataListEntryRetrievalUtil.getDynamicDataListEntry(mockDDLRecord2)).thenReturn(Optional.empty());
		when(mockDynamicDataListEntryRetrievalUtil.getDynamicDataListEntry(mockDDLRecord3)).thenReturn(Optional.of(mockDynamicDataListEntry2));

		List<DynamicDataListEntry> results = dynamicDataListServiceImpl.getDDLRecordSetEntries(recordSetId);

		assertThat(results, containsInAnyOrder(mockDynamicDataListEntry1, mockDynamicDataListEntry2));
	}

}
