package com.placecube.digitalplace.search.web.portlet.tag.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.web.portlet.tag.service.TagConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + com.placecube.digitalplace.search.web.constants.PortletKeys.TAG_FILTER, service = ConfigurationAction.class)
public class TagFacetPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private TagConfigurationService tagConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		TagFacetPortletInstanceConfiguration config = configurationProvider.getPortletInstanceConfiguration(TagFacetPortletInstanceConfiguration.class, themeDisplay);
		httpServletRequest.setAttribute(TagFacetPortletConfigurationConstants.MAX_TAGS_TO_SHOW, config.maxTagsToShow());
		httpServletRequest.setAttribute(TagFacetPortletConfigurationConstants.MAX_MORE_TAGS_TO_SHOW, config.maxShowMoreTagsToShow());
		httpServletRequest.setAttribute(TagFacetPortletConfigurationConstants.FILTER_COLOUR, config.filterColour());
		httpServletRequest.setAttribute(TagFacetPortletConfigurationConstants.TAGS_QUERY_ANDED, config.tagsQueryAnded());

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		int maxTagsToShow = ParamUtil.getInteger(actionRequest, TagFacetPortletConfigurationConstants.MAX_TAGS_TO_SHOW);
		setPreference(actionRequest, TagFacetPortletConfigurationConstants.MAX_TAGS_TO_SHOW, String.valueOf(maxTagsToShow));

		int maxShowMoreTagsToShow = ParamUtil.getInteger(actionRequest, TagFacetPortletConfigurationConstants.MAX_MORE_TAGS_TO_SHOW);
		setPreference(actionRequest, TagFacetPortletConfigurationConstants.MAX_MORE_TAGS_TO_SHOW, String.valueOf(maxShowMoreTagsToShow));

		String filterColour = ParamUtil.getString(actionRequest, TagFacetPortletConfigurationConstants.FILTER_COLOUR, StringPool.BLANK);
		setPreference(actionRequest, TagFacetPortletConfigurationConstants.FILTER_COLOUR, filterColour);

		boolean tagsQueryAnded = ParamUtil.getBoolean(actionRequest, TagFacetPortletConfigurationConstants.TAGS_QUERY_ANDED, false);
		setPreference(actionRequest, TagFacetPortletConfigurationConstants.TAGS_QUERY_ANDED, String.valueOf(tagsQueryAnded));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
