/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the SimpleAddressLookupEntry service. Represents a row in the &quot;Placecube_AddressLookupSimple_SimpleAddressLookupEntry&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see SimpleAddressLookupEntryModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryImpl"
)
@ProviderType
public interface SimpleAddressLookupEntry
	extends PersistedModel, SimpleAddressLookupEntryModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.addresslookup.simple.model.impl.SimpleAddressLookupEntryImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<SimpleAddressLookupEntry, Long>
		COMPANY_ID_ACCESSOR = new Accessor<SimpleAddressLookupEntry, Long>() {

			@Override
			public Long get(SimpleAddressLookupEntry simpleAddressLookupEntry) {
				return simpleAddressLookupEntry.getCompanyId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<SimpleAddressLookupEntry> getTypeClass() {
				return SimpleAddressLookupEntry.class;
			}

		};
	public static final Accessor<SimpleAddressLookupEntry, String>
		UPRN_ACCESSOR = new Accessor<SimpleAddressLookupEntry, String>() {

			@Override
			public String get(
				SimpleAddressLookupEntry simpleAddressLookupEntry) {

				return simpleAddressLookupEntry.getUprn();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<SimpleAddressLookupEntry> getTypeClass() {
				return SimpleAddressLookupEntry.class;
			}

		};

}