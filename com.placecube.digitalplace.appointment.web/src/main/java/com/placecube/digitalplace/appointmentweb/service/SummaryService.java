package com.placecube.digitalplace.appointmentweb.service;

import java.io.Serializable;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;

@Component(immediate = true, service = SummaryService.class)
public class SummaryService {

	private static final Pattern PATTERN = Pattern.compile("\\$(\\w+)\\$");

	public String replacePlaceholders(String expression, Map<String, Serializable> objectEntryValues) {
		Matcher matcher = PATTERN.matcher(expression);
		StringBuffer result = new StringBuffer();

		while (matcher.find()) {
			String key = matcher.group(1);
			Serializable value = objectEntryValues.get(key);
			if (value != null) {
				matcher.appendReplacement(result, Matcher.quoteReplacement(value.toString()));
			} else {
				matcher.appendReplacement(result, Matcher.quoteReplacement(StringPool.BLANK));
			}
		}

		matcher.appendTail(result);
		return result.toString();
	}
}
