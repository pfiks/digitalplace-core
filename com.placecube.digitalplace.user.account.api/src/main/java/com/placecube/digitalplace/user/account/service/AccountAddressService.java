package com.placecube.digitalplace.user.account.service;

import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.account.model.AccountContext;

public interface AccountAddressService {

	void populateContextWithExistingPrimaryAddress(AccountContext accountContext, User user);
}
