AUI.add(
	'com-placecube-ddl-editor-autofill',
	function (A) {
		var DDLEditorAutofill = {
			init: function (ddlAutofillFields, namespace) {
				const fieldsToAutofill = JSON.parse(ddlAutofillFields);
				fieldsToAutofill.forEach((item) => {
					const fieldName = item.fieldName;
					const inputElement = document.querySelector('input[id*="'+namespace+fieldName+'"]');
					if (inputElement) {
						$('div[data-fieldname="'+fieldName+'"]').hide();
						inputElement.value = item.fieldValue;
					}
				});
			}
		};
		A.DDLEditorAutofill = DDLEditorAutofill;
	},
	'', {
	requires: [
		'aui-base'
	]
});