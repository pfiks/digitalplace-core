package com.placecube.digitalplace.userimport.admin.web.constants;

public final class MVCCommandKeys {

	public static final String IMPORT_DATA = "/userimport/import-data";

	private MVCCommandKeys() {
	}

}
