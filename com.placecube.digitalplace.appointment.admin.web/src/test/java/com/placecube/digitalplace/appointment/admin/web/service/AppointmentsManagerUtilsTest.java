package com.placecube.digitalplace.appointment.admin.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.portlet.PortletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.pfiks.common.time.DateTimeSelectionService;
import com.pfiks.common.time.model.DateTimeSelection;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentsManagerUtilsTest {

	private static final Integer END_DAY = 9;
	private static final Integer END_HOUR = 2;
	private static final Integer END_MINUTE = 5;
	private static final Integer END_MONTH = 2;
	private static final Integer END_YEAR = 2017;
	private static final Integer START_DAY = 5;
	private static final Integer START_HOUR = 7;
	private static final Integer START_MINUTE = 22;
	private static final Integer START_MONTH = 4;
	private static final Integer START_YEAR = 2016;

	@InjectMocks
	private AppointmentsManagerUtils appointmentsManagerUtils;

	@Mock
	private Date mockDate;

	@Mock
	private DateTimeSelection mockDateTimeSelection;

	@Mock
	private DateTimeSelectionService mockDateTimeSelectionService;

	private MockedStatic<ParamUtil> mockParamUtil;

	@Mock
	private PortletRequest mockPortletRequest;

	private MockedStatic<PropsUtil> mockPropsUtil;

	@Mock
	private TimeZone mockTimeZone;

	@Before
	public void activateSetUp() {
		mockPropsUtil = mockStatic(PropsUtil.class);
		mockParamUtil = mockStatic(ParamUtil.class);
	}

	@Test
	public void getAppointmentContext_WhenNoError_ThenReturnsTheAppointmentContext() {

		Calendar startCal = Calendar.getInstance();
		startCal.set(2024, 5, 14, 10, 20);
		DateTimeSelection startDateTime = new DateTimeSelection(startCal);

		Calendar endCal = Calendar.getInstance();
		endCal.set(2025, 3, 7, 9, 10);
		DateTimeSelection endDateTime = new DateTimeSelection(endCal);

		AppointmentContext result = appointmentsManagerUtils.getAppointmentContext(startDateTime, endDateTime);

		Calendar startDateResult = Calendar.getInstance();
		startDateResult.setTime(result.getStartDate().get());
		assertThat(startDateResult.get(Calendar.YEAR), equalTo(startCal.get(Calendar.YEAR)));
		assertThat(startDateResult.get(Calendar.MONTH), equalTo(startCal.get(Calendar.MONTH)));
		assertThat(startDateResult.get(Calendar.DAY_OF_MONTH), equalTo(14));
		assertThat(startDateResult.get(Calendar.HOUR_OF_DAY), equalTo(0));
		assertThat(startDateResult.get(Calendar.MINUTE), equalTo(0));

		Calendar endDateResult = Calendar.getInstance();
		endDateResult.setTime(result.getEndDate().get());
		assertThat(endDateResult.get(Calendar.YEAR), equalTo(endCal.get(Calendar.YEAR)));
		assertThat(endDateResult.get(Calendar.MONTH), equalTo(endCal.get(Calendar.MONTH)));
		assertThat(endDateResult.get(Calendar.DAY_OF_MONTH), equalTo(7));
		assertThat(endDateResult.get(Calendar.HOUR_OF_DAY), equalTo(23));
		assertThat(endDateResult.get(Calendar.MINUTE), equalTo(59));

		assertFalse(result.isExactDateMatch());
	}

	@Test
	public void getDateTimeSelection_WhenNoError_ThenReturnsTheDateWithTheSpecifiedHourAndMinuteValues() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2021, 11, 25, 5, 15);
		Date date = calendar.getTime();

		int hour = 10;
		int minute = 30;

		DateTimeSelection result = appointmentsManagerUtils.getDateTimeSelection(date, hour, minute);

		assertThat(result.getYear(), equalTo(2021));
		assertThat(result.getMonth(), equalTo(11));
		assertThat(result.getDay(), equalTo(25));
		assertThat(result.getHour(), equalTo(hour));
		assertThat(result.getMinute(), equalTo(minute));
	}

	@Test
	public void getDaysOfWeekBitwiseValue_WhenInvalidDays_ThenReturnsZero() {
		long result = appointmentsManagerUtils.getDaysOfWeekBitwiseValue();

		assertThat(result, equalTo(0l));
	}

	@Test
	public void getDaysOfWeekBitwiseValue_WhenNoErrorAndMultipleDays_ThenReturnsTheTotalBitwiseValueForTheDays() {
		// Test for multiple days
		long result = appointmentsManagerUtils.getDaysOfWeekBitwiseValue(1, 2);

		assertThat(result, equalTo(3l));

		result = appointmentsManagerUtils.getDaysOfWeekBitwiseValue(1, 3);

		assertThat(result, equalTo(5l));

		result = appointmentsManagerUtils.getDaysOfWeekBitwiseValue(1, 2, 3, 4, 5, 6, 7);

		assertThat(result, equalTo(127l));
	}

	@Test
	public void getDaysOfWeekBitwiseValue_WhenNoErrorAndSingleDayValue_ThenReturnsTheBitwiseValueForTheDay() {
		assertEquals(1, appointmentsManagerUtils.getDaysOfWeekBitwiseValue(1)); // Monday
		assertEquals(2, appointmentsManagerUtils.getDaysOfWeekBitwiseValue(2)); // Tuesday
		assertEquals(4, appointmentsManagerUtils.getDaysOfWeekBitwiseValue(3)); // Wednesday
		assertEquals(8, appointmentsManagerUtils.getDaysOfWeekBitwiseValue(4)); // Thursday
		assertEquals(16, appointmentsManagerUtils.getDaysOfWeekBitwiseValue(5)); // Friday
		assertEquals(32, appointmentsManagerUtils.getDaysOfWeekBitwiseValue(6)); // Saturday
		assertEquals(64, appointmentsManagerUtils.getDaysOfWeekBitwiseValue(7)); // Sunday
	}

	@Test
	public void getEndDateTimeFromRequest_WhenAM_ThenReturnsDateTimeSelectionWithHourFromRequest() {
		mockEndDateTime(END_HOUR, Calendar.AM, END_YEAR, END_MONTH, END_DAY, END_MINUTE);

		DateTimeSelection result = appointmentsManagerUtils.getEndDateTimeFromRequest(mockPortletRequest);

		verifyDateTime(result, END_YEAR, END_MONTH, END_DAY, Calendar.AM, END_HOUR, END_MINUTE);
	}

	@Test
	public void getEndDateTimeFromRequest_WhenPM_ThenReturnsDateTimeSelectionWithHourFromRequestPlus12() {
		mockEndDateTime(END_HOUR, Calendar.PM, END_YEAR, END_MONTH, END_DAY, END_MINUTE);

		DateTimeSelection result = appointmentsManagerUtils.getEndDateTimeFromRequest(mockPortletRequest);

		verifyDateTime(result, END_YEAR, END_MONTH, END_DAY, Calendar.PM, END_HOUR + 12, END_MINUTE);
	}

	@Test
	public void getEndDateTimeSelection_WhenNoError_ThenReturnsTheDateTimeSelectionWithTwoHoursAdded() {
		when(mockDateTimeSelectionService.getDateTimeSelectionWithHours(mockTimeZone, 2)).thenReturn(mockDateTimeSelection);

		DateTimeSelection result = appointmentsManagerUtils.getEndDateTimeSelection(mockTimeZone);

		assertThat(result, sameInstance(mockDateTimeSelection));
	}

	@Test
	public void getStartDateTimeFromRequest_WhenAM_ThenReturnsDateTimeSelectionWithHourFromRequest() {
		mockStartDateTime(START_HOUR, Calendar.AM, START_YEAR, START_MONTH, START_DAY, START_MINUTE);

		DateTimeSelection result = appointmentsManagerUtils.getStartDateTimeFromRequest(mockPortletRequest);

		verifyDateTime(result, START_YEAR, START_MONTH, START_DAY, Calendar.AM, START_HOUR, START_MINUTE);
	}

	@Test
	public void getStartDateTimeFromRequest_WhenPM_ThenReturnsDateTimeSelectionWithHourFromRequestPlus12() {
		mockStartDateTime(START_HOUR, Calendar.PM, START_YEAR, START_MONTH, START_DAY, START_MINUTE);

		DateTimeSelection result = appointmentsManagerUtils.getStartDateTimeFromRequest(mockPortletRequest);

		verifyDateTime(result, START_YEAR, START_MONTH, START_DAY, Calendar.PM, START_HOUR + 12, START_MINUTE);
	}

	@Test
	public void getStartDateTimeSelection_WhenNoError_ThenReturnsTheDateTimeSelectionWithOneHourAdded() {
		when(mockDateTimeSelectionService.getDateTimeSelectionWithHours(mockTimeZone, 1)).thenReturn(mockDateTimeSelection);

		DateTimeSelection result = appointmentsManagerUtils.getStartDateTimeSelection(mockTimeZone);

		assertThat(result, sameInstance(mockDateTimeSelection));
	}

	@After
	public void teardown() {
		mockPropsUtil.close();
		mockParamUtil.close();
	}

	private void mockEndDateTime(Integer endHour, Integer endAMorPM, Integer endYear, Integer endMonth, Integer endDay, Integer endMinute) {
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "endHour")).thenReturn(endHour);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "endAMorPM")).thenReturn(endAMorPM);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "endYear")).thenReturn(endYear);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "endMonth")).thenReturn(endMonth);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "endDay")).thenReturn(endDay);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "endMinute")).thenReturn(endMinute);
	}

	private void mockStartDateTime(Integer startHour, Integer startAMorPM, Integer startYear, Integer startMonth, Integer startDay, Integer startMinute) {
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "startHour")).thenReturn(startHour);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "startAMorPM")).thenReturn(startAMorPM);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "startYear")).thenReturn(startYear);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "startMonth")).thenReturn(startMonth);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "startDay")).thenReturn(startDay);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, "startMinute")).thenReturn(startMinute);
	}

	private void verifyDateTime(DateTimeSelection dateTimeToCheck, Integer year, Integer month, Integer day, Integer amORpm, Integer hour, Integer minute) {
		assertThat(dateTimeToCheck.getAmORpm(), equalTo(amORpm));
		assertThat(dateTimeToCheck.getYear(), equalTo(year));
		assertThat(dateTimeToCheck.getMonth(), equalTo(month));
		assertThat(dateTimeToCheck.getDay(), equalTo(day));
		assertThat(dateTimeToCheck.getHour(), equalTo(hour));
		assertThat(dateTimeToCheck.getMinute(), equalTo(minute));
	}

}
