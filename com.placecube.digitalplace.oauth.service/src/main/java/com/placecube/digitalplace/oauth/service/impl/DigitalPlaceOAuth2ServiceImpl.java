package com.placecube.digitalplace.oauth.service.impl;

import com.liferay.oauth2.provider.service.OAuth2ApplicationLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.placecube.digitalplace.oauth.service.DigitalPlaceOAuth2Service;
import com.placecube.digitalplace.oauth.util.URLUtil;

import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = DigitalPlaceOAuth2Service.class)
public class DigitalPlaceOAuth2ServiceImpl implements DigitalPlaceOAuth2Service {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private OAuth2ApplicationLocalService oAuth2ApplicationLocalService;

	@Reference
	private URLUtil urlUtil;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	public String getOAuth2Token(long companyId, String clientId, String clientSecret) throws PortalException {
		Company company = companyLocalService.getCompany(companyId);
		String portalUrl = company.getPortalURL(0);

		final String oauth2AuthorizationUrl = portalUrl + "/o/oauth2/token";

		final String urlParameters = "grant_type=client_credentials" + //
				"&client_id=" + clientId + //
				"&client_secret=" + clientSecret;

		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		try {
			URL url = urlUtil.buildURL(oauth2AuthorizationUrl);

			HttpURLConnection connection = urlUtil.getConnection(url, postDataLength);
			urlUtil.writePostData(connection, postData);

			if (connection.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + connection.getResponseCode());
			}

			String response = urlUtil.getResponse(connection);

			connection.disconnect();

			JSONObject responseObject = jsonFactory.createJSONObject(response);

			return responseObject.get("access_token").toString();
		} catch (Exception e) {
			throw new PortalException("OAuth2 token could not be retrieved: " + e.toString(), e);
		}
	}

}
