package com.placecube.digitalplace.navigation.service.util;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;

@Component(immediate = true, service = BreadcrumbServiceUtil.class)
public class BreadcrumbServiceUtil {

	@Reference
	private BreadcrumbEntryFactory breadcrumbEntryFactory;

	public Optional<BreadcrumbEntry> getLayoutSetGroupBreadcrumbEntry(ThemeDisplay themeDisplay) throws PortalException {

		Layout layout = themeDisplay.getLayout();

		LayoutSet layoutSet = layout.getLayoutSet();

		Group group = layoutSet.getGroup();

		int layoutsPageCount = 0;

		if (layoutSet.isPrivateLayout()) {
			layoutsPageCount = group.getPrivateLayoutsPageCount();
		} else {
			layoutsPageCount = group.getPublicLayoutsPageCount();
		}

		if (layoutsPageCount > 0) {
			String layoutSetFriendlyURL = PortalUtil.getLayoutSetFriendlyURL(layoutSet, themeDisplay);

			if (themeDisplay.isAddSessionIdToURL()) {
				layoutSetFriendlyURL = PortalUtil.getURLWithSessionId(layoutSetFriendlyURL, themeDisplay.getSessionId());
			}

			String breadcrumbTitle = group.getDescriptiveName(themeDisplay.getLocale());
			return Optional.of(breadcrumbEntryFactory.createBreadcrumbEntry(breadcrumbTitle, layoutSetFriendlyURL));
		}

		return Optional.empty();
	}

	public boolean isGuestStagingGroup(Group group) {
		if (group.isGuest()) {
			return false;
		}

		if (group.isStaged()) {
			Group liveGroup = group.getLiveGroup();

			if (liveGroup != null) {
				String groupKey = liveGroup.getGroupKey();

				if (groupKey.equals(GroupConstants.GUEST)) {
					return true;
				}
			}
		}

		return false;
	}
}
