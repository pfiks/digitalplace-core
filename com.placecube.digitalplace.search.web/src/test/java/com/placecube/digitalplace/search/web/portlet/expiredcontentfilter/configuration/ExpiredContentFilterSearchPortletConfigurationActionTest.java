package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.settings.LocalizedValuesMap;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, LocaleUtil.class, LocalizationUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class ExpiredContentFilterSearchPortletConfigurationActionTest extends PowerMockito {

	private static final String DATE_PARAM_NAME = "sampleName";

	private static final Locale LOCALE = Locale.UK;

	private static final String TITLE_XML = "sampleTitle";

	@InjectMocks
	private ExpiredContentFilterSearchPortletConfigurationAction expiredContentFilterSearchPortletConfigurationAction;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private ExpiredContentFilterSearchPortletInstanceConfiguration mockExpiredContentFilterSearchPortletInstanceConfiguration;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private PortletPreferences mockPreferences;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private LocalizedValuesMap mockTitleMap;

	@Mock
	private Map<Locale, String> mockTitleMapValues;

	@Before
	public void activateSetup() {
		mockStatic(LocaleUtil.class, LocalizationUtil.class);
	}

	@Test(expected = ConfigurationException.class)
	public void include_WhenConfigurationExceptionIsThrown_ThenThrowsException() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(ExpiredContentFilterSearchPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		expiredContentFilterSearchPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void include_WhenNoError_ThenSetsRequestAttributes() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(ExpiredContentFilterSearchPortletInstanceConfiguration.class, mockThemeDisplay))
				.thenReturn(mockExpiredContentFilterSearchPortletInstanceConfiguration);
		when(mockExpiredContentFilterSearchPortletInstanceConfiguration.dateParameterName()).thenReturn(DATE_PARAM_NAME);
		when(mockExpiredContentFilterSearchPortletInstanceConfiguration.title()).thenReturn(mockTitleMap);
		when(mockExpiredContentFilterSearchPortletInstanceConfiguration.title().getValues()).thenReturn(mockTitleMapValues);
		when(LocaleUtil.getSiteDefault()).thenReturn(LOCALE);
		when(LocaleUtil.toLanguageId(LOCALE)).thenReturn("en-gb");
		when(LocalizationUtil.updateLocalization(mockTitleMapValues, StringPool.BLANK, "title", LocaleUtil.toLanguageId(LocaleUtil.getSiteDefault()))).thenReturn(TITLE_XML);

		expiredContentFilterSearchPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("dateParameterName", DATE_PARAM_NAME);
		verify(mockHttpServletRequest, times(1)).setAttribute("title", TITLE_XML);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesThePreferences() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		expiredContentFilterSearchPortletConfigurationAction = spy(new ExpiredContentFilterSearchPortletConfigurationAction());

		when(ParamUtil.getString(mockActionRequest, "dateParameterName")).thenReturn(DATE_PARAM_NAME);
		when(mockActionRequest.getPreferences()).thenReturn(mockPreferences);

		expiredContentFilterSearchPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(expiredContentFilterSearchPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "dateParameterName", DATE_PARAM_NAME);
		verifyStatic(LocalizationUtil.class, times(1));
		LocalizationUtil.setLocalizedPreferencesValues(mockActionRequest, mockPreferences, "title");
	}
}
