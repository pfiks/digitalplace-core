package com.placecube.digitalplace.user.account.connector;

import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.connector.model.Connector;

public interface UserCreateAccountConnector extends Connector {

	String USER_CREATE_ACCOUNT_CONNECTOR_TYPE = "UserCreateAccountConnector";

	String getCreateAccountMVCRenderCommandName();

	String getModelId();

	String getPortletId();

	JSONObject getRequestParameters();
}
