package com.placecube.digitalplace.appointment.booking.helper;

import java.text.ParseException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.internal.utils.AppointmentUtils;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;

@Component(immediate = true, service = AppointmentModelBuilder.class)
public class AppointmentModelBuilder {

	private static final String JSON_FIELD_CLASS_NAME_ID = "classNameId";
	private static final String JSON_FIELD_CLASS_PK = "classPK";
	private static final String JSON_FIELD_DATE_PATTERN = "datePattern";
	private static final String JSON_FIELD_END_DATE = "endDate";
	private static final String JSON_FIELD_START_DATE = "startDate";
	private static final String JSON_FIELD_TITLE = "title";

	@Reference
	private AppointmentUtils appointmentUtils;

	@Reference
	private JSONFactory jsonFactory;

	public AppointmentBookingEntry getAppointmentBookingEntry(AppointmentEntry appointmentEntry) {
		AppointmentBookingEntry result = new AppointmentBookingEntry();

		result.setClassNameId(appointmentEntry.getEntryClassNameId());
		result.setClassPK(appointmentEntry.getEntryClassPK());
		result.setEndDate(appointmentEntry.getAppointmentEndDate());
		result.setStartDate(appointmentEntry.getAppointmentStartDate());
		result.setAppointmentEntryId(appointmentEntry.getAppointmentEntryId());
		result.setAppointmentId(appointmentEntry.getAppointmentId());

		return result;
	}

	public AppointmentBookingEntry getAppointmentBookingEntryFromPayload(HttpServletRequest httpServletRequest, String bodyRequest) throws PortalException, ParseException {
		AppointmentBookingEntry result = new AppointmentBookingEntry();

		if (Validator.isNotNull(bodyRequest)) {
			JSONObject jsonObject = jsonFactory.createJSONObject(bodyRequest);

			ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Locale locale = appointmentUtils.getLocale(themeDisplay);
			String datePattern = jsonObject.getString(JSON_FIELD_DATE_PATTERN);

			result.setTimeZone(appointmentUtils.getTimeZone(themeDisplay));
			result.setClassNameId(jsonObject.getLong(JSON_FIELD_CLASS_NAME_ID, 0l));
			result.setClassPK(jsonObject.getLong(JSON_FIELD_CLASS_PK, 0l));

			result.setEndDate(
					appointmentUtils.getParsedDate(jsonObject.getString(JSON_FIELD_END_DATE, StringPool.BLANK), locale, datePattern).orElseThrow(() -> new PortalException("End date is required")));
			result.setStartDate(appointmentUtils.getParsedDate(jsonObject.getString(JSON_FIELD_START_DATE, StringPool.BLANK), locale, datePattern)
					.orElseThrow(() -> new PortalException("Start date is required")));
			result.setTitle(jsonObject.getString(JSON_FIELD_TITLE, StringPool.BLANK));
		}
		return result;
	}

	public AppointmentContext getAppointmentContextFromPayload(HttpServletRequest httpServletRequest, String bodyRequest) throws JSONException, ParseException {
		AppointmentContext result = new AppointmentContext();

		if (Validator.isNotNull(bodyRequest)) {
			JSONObject jsonObject = jsonFactory.createJSONObject(bodyRequest);

			ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Locale locale = appointmentUtils.getLocale(themeDisplay);
			String datePattern = jsonObject.getString(JSON_FIELD_DATE_PATTERN);

			result.setClassNameId(jsonObject.getLong(JSON_FIELD_CLASS_NAME_ID, 0l));
			result.setClassPK(jsonObject.getLong(JSON_FIELD_CLASS_PK, 0l));
			result.setEndDate(appointmentUtils.getParsedDate(jsonObject.getString(JSON_FIELD_END_DATE, StringPool.BLANK), locale, datePattern));
			result.setStartDate(appointmentUtils.getParsedDate(jsonObject.getString(JSON_FIELD_START_DATE, StringPool.BLANK), locale, datePattern));
		}

		return result;
	}

}
