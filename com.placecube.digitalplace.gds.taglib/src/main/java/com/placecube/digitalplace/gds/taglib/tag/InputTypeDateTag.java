package com.placecube.digitalplace.gds.taglib.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.gds.taglib.context.ServletContextUtil;

public class InputTypeDateTag extends IncludeTag {

	private String dayLabel;
	private int dayValue;
	private String errorMessage;
	private String fieldHint;
	private String fieldLabel;
	private String fieldLabelSize = "l";
	private String fieldName;
	private boolean hideDay;
	private boolean hideMonth;
	private String monthLabel;
	private int monthValue;
	private String portletNamespace;
	private boolean required = false;
	private String yearLabel;
	private int yearValue;

	@Override
	public int doStartTag() {
		setAttributeNamespace("gds-forms-ui:input-date");
		return EVAL_BODY_INCLUDE;
	}

	public void setDayLabel(String dayLabel) {
		this.dayLabel = dayLabel;
	}

	public void setDayValue(int dayValue) {
		this.dayValue = dayValue;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setFieldHint(String fieldHint) {
		this.fieldHint = fieldHint;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public void setFieldLabelSize(String fieldLabelSize) {
		this.fieldLabelSize = fieldLabelSize;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setHideDay(boolean hideDay) {
		this.hideDay = hideDay;
	}

	public void setHideMonth(boolean hideMonth) {
		this.hideMonth = hideMonth;
	}

	public void setMonthLabel(String monthLabel) {
		this.monthLabel = monthLabel;
	}

	public void setMonthValue(int monthValue) {
		this.monthValue = monthValue;
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);
		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setPortletNamespace(String portletNamespace) {
		this.portletNamespace = portletNamespace;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public void setYearLabel(String yearLabel) {
		this.yearLabel = yearLabel;
	}

	public void setYearValue(int yearValue) {
		this.yearValue = yearValue;
	}

	@Override
	protected void cleanUp() {
		super.cleanUp();
		dayValue = 0;
		dayLabel = null;
		errorMessage = null;
		fieldHint = null;
		fieldLabel = null;
		fieldLabelSize = null;
		fieldName = null;
		hideDay = false;
		hideMonth = false;
		monthLabel = null;
		monthValue = 0;
		required = false;
		portletNamespace = null;
		yearLabel = null;
		yearValue = 0;

	}

	@Override
	protected String getPage() {
		return "/META-INF/taglibs/gds-forms-ui/date/view.jsp";
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute("dayLabel", dayLabel == null ? "day" : dayLabel);
		request.setAttribute("dayValue", dayValue > 0 ? dayValue : "");
		request.setAttribute("errorMessage", errorMessage);
		request.setAttribute("fieldHint", fieldHint);
		request.setAttribute("fieldLabel", fieldLabel);
		request.setAttribute("fieldLabelSize", fieldLabelSize);
		request.setAttribute("fieldName", fieldName);
		request.setAttribute("hideDay", hideDay);
		request.setAttribute("hideMonth", hideMonth);
		request.setAttribute("monthLabel", monthLabel == null ? "month" : monthLabel);
		request.setAttribute("monthValue", monthValue > 0 ? monthValue : "");
		request.setAttribute("required", required);
		request.setAttribute("portletNamespace", portletNamespace);
		request.setAttribute("yearLabel", yearLabel == null ? "year" : yearLabel);
		request.setAttribute("yearValue", yearValue > 0 ? yearValue : "");

	}

}
