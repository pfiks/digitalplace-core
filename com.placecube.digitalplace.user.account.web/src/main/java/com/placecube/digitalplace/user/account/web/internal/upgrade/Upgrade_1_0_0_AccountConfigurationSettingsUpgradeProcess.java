package com.placecube.digitalplace.user.account.web.internal.upgrade;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.jdbc.AutoBatchPreparedStatementUtil;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.user.account.configuration.AccountCompanyConfiguration;
import com.placecube.digitalplace.user.account.configuration.AccountGroupConfiguration;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsCompanyConfiguration;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.apache.felix.cm.file.ConfigurationHandler;

public class Upgrade_1_0_0_AccountConfigurationSettingsUpgradeProcess extends UpgradeProcess {

	private static final String EMAIL_ADDRESS_REQUIRED = "emailAddressRequired";

	private static final String USER_PROFILE_FIELDS_GROUP_CCONFIGURATION_PID = "com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsGroupConfiguration";

	private CompanyLocalService companyLocalService;

	private ConfigurationProvider configurationProvider;

	private GroupLocalService groupLocalService;

	public Upgrade_1_0_0_AccountConfigurationSettingsUpgradeProcess(CompanyLocalService companyLocalService, ConfigurationProvider configurationProvider, GroupLocalService groupLocalService) {
		this.companyLocalService = companyLocalService;
		this.configurationProvider = configurationProvider;
		this.groupLocalService = groupLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {
		long companyId = CompanyThreadLocal.getCompanyId();

		if (companyId > 0) {
			upgradeCompany(companyId);
		} else {
			List<Company> companies = companyLocalService.getCompanies();

			for (Company company : companies) {
				upgradeCompany(company.getCompanyId());
			}
		}
	}

	private void upgradeCompany(long companyId) throws Exception {
		List<Group> groups = groupLocalService.getGroups(companyId, GroupConstants.ANY_PARENT_GROUP_ID, true);
		updateCompanyConfiguration(companyId);
		for (Group group : groups) {
			addGroupConfiguration(group.getGroupId());
		}
	}

	private void addGroupConfiguration(long groupId) throws IOException, SQLException, ConfigurationException {

		try (PreparedStatement preparedStatement = connection.prepareStatement("select configurationId, dictionary  from Configuration_ where " + "configurationId like ? and dictionary like ?");
				PreparedStatement preparedDeleteStatement = AutoBatchPreparedStatementUtil.concurrentAutoBatch(connection, "DELETE FROM Configuration_ WHERE configurationId = ?")) {

			preparedStatement.setString(1, "%" + USER_PROFILE_FIELDS_GROUP_CCONFIGURATION_PID + "%");
			preparedStatement.setString(2, "%groupId=L\"" + groupId + "\"%");

			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				if (resultSet.next()) {
					String configurationid = resultSet.getString("configurationId");
					String dictionaryString = resultSet.getString("dictionary");

					Dictionary<String, Object> dictionary = ConfigurationHandler.read(new UnsyncByteArrayInputStream(dictionaryString.getBytes(StringPool.UTF8)));

					if (Validator.isNotNull(dictionary)) {
						configurationProvider.saveGroupConfiguration(AccountGroupConfiguration.class, groupId, dictionary);

						preparedDeleteStatement.setString(1, configurationid);

						preparedDeleteStatement.addBatch();
					}
				}
			}
			preparedDeleteStatement.executeBatch();
		}
	}

	private void updateCompanyConfiguration(long companyId) throws IOException, SQLException, ConfigurationException {

		try (PreparedStatement preparedStatement = connection.prepareStatement("select dictionary from Configuration_ where " + "configurationId like ? and dictionary like ?")) {

			preparedStatement.setString(1, "%" + UserProfileFieldsCompanyConfiguration.class.getName() + "%");
			preparedStatement.setString(2, "%companyId=L\"" + companyId + "\"%");

			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				if (resultSet.next()) {
					String dictionaryString = resultSet.getString("dictionary");

					Dictionary<String, Object> properties = new Hashtable<>();

					Dictionary<String, Object> dictionary = ConfigurationHandler.read(new UnsyncByteArrayInputStream(dictionaryString.getBytes(StringPool.UTF8)));

					if (Validator.isNotNull(dictionary) && Validator.isNotNull(dictionary.get(EMAIL_ADDRESS_REQUIRED))) {
						AccountCompanyConfiguration accountCompanyConfiguration = configurationProvider.getCompanyConfiguration(AccountCompanyConfiguration.class, companyId);

						properties.put("addressOutOfLocalArea", accountCompanyConfiguration.addressOutOfLocalArea());
						properties.put("createAccountURL", accountCompanyConfiguration.createAccountURL());
						properties.put("emailAddressPageLocation", accountCompanyConfiguration.emailAddressPageLocation());
						properties.put(EMAIL_ADDRESS_REQUIRED, dictionary.get(EMAIL_ADDRESS_REQUIRED));
						properties.put("emailAddressVerificationURL", accountCompanyConfiguration.emailAddressVerificationURL());
						properties.put("generatePasswordWhenLoggedInUserCreateAccount", accountCompanyConfiguration.generatePasswordWhenLoggedInUserCreateAccount());
						properties.put("registrationRefererGroupIdUserExpandoFieldName", accountCompanyConfiguration.registrationRefererGroupIdUserExpandoFieldName());
						properties.put("requireCaptchaBeforeEmailAddressEntry", accountCompanyConfiguration.requireCaptchaBeforeEmailAddressEntry());
						properties.put("resetPasswordArticleId", accountCompanyConfiguration.resetPasswordArticleId());
						properties.put("resetPasswordButtonIncConfirmationPageWhenLoggedInUserCreateAccount",
								accountCompanyConfiguration.resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount());

						configurationProvider.saveCompanyConfiguration(AccountCompanyConfiguration.class, companyId, properties);

						dictionary.remove(EMAIL_ADDRESS_REQUIRED);

						configurationProvider.saveCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, companyId, dictionary);
					}
				}
			}
		}
	}
}
