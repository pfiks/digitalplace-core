package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.constants;

public final class ExpiredContentFilterSearchConstants {

	public static final String CONFIG_DATE_PARAMETER_NAME = "dateParameterName";

	public static final String SHARED_SEARCH_ATTRIBUTE_NAME = "expiredContentFilterSearchIncludeExpiredContent";

	private ExpiredContentFilterSearchConstants() {
	}
}
