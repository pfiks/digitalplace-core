package com.placecube.digitalplace.form.service;

import java.net.URL;
import java.util.List;

import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.form.exception.FormDefinitionException;
import com.placecube.digitalplace.form.model.FormDefinition;

public interface FormDefinitionService {

	public void addFormDefinition(FormDefinition formDefinition, String jsonDDMFormDefinition, String jsonDDMFormLayout, String jsonSettingsDDMFormValues, ServiceContext serviceContext)
			throws FormDefinitionException;

	public void addFormDefinition(FormDefinition formDefinition, ServiceContext serviceContext) throws FormDefinitionException;

	FormDefinition create(String name, String description, String key);

	FormDefinition create(String name, String description, String category, String key, String journalArticleFolderName, URL ddmForm, URL ddmFormLayout, URL settingsDDMFormValues,
			List<URL> journalArticleURLs, List<URL> dataProviderDDMFormValues);

	List<FormDefinition> sort(List<FormDefinition> formDefinitions);

}
