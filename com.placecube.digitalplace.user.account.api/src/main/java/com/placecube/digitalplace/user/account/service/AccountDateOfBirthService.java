package com.placecube.digitalplace.user.account.service;

import java.time.LocalDate;

import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;

public interface AccountDateOfBirthService {

	LocalDate getDateOfBirthFromContext(AccountContext accountContext);

	LocalDate getDefaultDateOfBirth();

	void populateContextWithExistingDateOfBirth(AccountContext accountContext, User user) throws UserAccountException;
}
