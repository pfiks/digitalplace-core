package com.placecube.digitalplace.session.timeout.web.config;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "instance-configuration", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.session.timeout.web.config.SessionTimeoutCompanyConfiguration", localization = "content/Language", name = "session-timeout")
public interface SessionTimeoutCompanyConfiguration {

	@Meta.AD(required = false, deflt = "15", name = "timeout-in-minutes", description = "timeout-in-minutes-description")
	int timeoutInMinutes();

}
