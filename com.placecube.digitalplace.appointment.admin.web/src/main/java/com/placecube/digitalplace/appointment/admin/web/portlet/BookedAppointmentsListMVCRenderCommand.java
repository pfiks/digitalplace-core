package com.placecube.digitalplace.appointment.admin.web.portlet;

import java.util.TimeZone;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.common.time.model.DateTimeSelection;
import com.placecube.digitalplace.appointment.admin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.admin.web.constants.PortletKeys;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;

@Component(immediate = true, property = { //
		"javax.portlet.name=" + PortletKeys.APPOINTMENTS_MANAGER, //
		"mvc.command.name=" + MVCCommandKeys.VIEW_BOOKED_APPOINTMENTS //
}, service = MVCRenderCommand.class)
public class BookedAppointmentsListMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AppointmentsManagerService appointmentsManagerService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		TimeZone timeZone = themeDisplay.getTimeZone();

		long companyId = themeDisplay.getCompanyId();
		boolean searchExecuted = ParamUtil.getBoolean(renderRequest, "searchExecuted", false);

		DateTimeSelection filterStartDateTime = appointmentsManagerService.getFilterStartDate(searchExecuted, renderRequest, timeZone);
		DateTimeSelection filterEndDateTime = appointmentsManagerService.getFilterEndDate(searchExecuted, renderRequest, timeZone);

		if (searchExecuted) {
			String serviceId = ParamUtil.getString(renderRequest, "serviceId", StringPool.BLANK);
			renderRequest.setAttribute("bookedAppointments", appointmentsManagerService.getBookedAppointments(companyId, serviceId, filterStartDateTime, filterEndDateTime));
			renderRequest.setAttribute("serviceId", serviceId);
		}

		renderRequest.setAttribute("filterStartDateTime", filterStartDateTime);
		renderRequest.setAttribute("filterEndDateTime", filterEndDateTime);
		renderRequest.setAttribute("availableServiceIds", appointmentsManagerService.getAvailableServiceIds(companyId));

		return "/view-appointments.jsp";
	}

}
