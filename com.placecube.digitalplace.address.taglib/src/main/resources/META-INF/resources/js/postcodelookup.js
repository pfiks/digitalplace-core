AUI.add(

	'com-placecube-digitalplace-address-postcodelookup',

	function (A) {

		var NS = 'ns';
		var ENTRY_EMPTY_OPTION = '<option value="">{numberOfAddresses} addresses found for {postcode}</option>';

		var ENTRY_OPTION_TEMPLATE = '<option {selectedAddress} value="{addressId}" data-addressLine1="{addressLine1Value}" data-addressLine2="{addressLine2Value}" data-addressLine3="{addressLine3Value}" data-addressLine4="{addressLine4Value}" data-city="{cityValue}">{addressValue}</option>';

		var SELECTED_CALLBACK = 'selectedCallback';

		var UNSELECTED_CALLBACK = 'unselectedCallback';

		var ADDRESS_FORM_CONTAINER_ID = 'addressFormContainerId';
		var ADDRESS_LOOKUP_RESET_ID = 'addressLookupResetId';
		var BUTTON_ADDRESS_LOOKUP_ID = 'btnAddressLookupId';
		var FULL_ADDRESS_ID = 'fullAddressId';
		var POST_CODE_FIELD_CONTAINER_ID = 'postcodeFieldContainerId';
		var POSTCODE_ID = 'postcodeId';
		var POST_CODE_LABEL_ID = 'postcodeLabelId';
		var UPRN_ID = 'uprnId';

		A.PostcodeLookup = A.Component.create({

			NAME: 'com-placecube-digitalplace-address-postcodelookup',

			EXTENDS: A.Component,

			ATTRS: {
				ns: {},
				selectedCallback: {},
				unselectedCallback: {},
				fallbackToNationalLookup: {},
				addressFormContainerId: {},
				addressLookupResetId: {},
				btnAddressLookupId: {},
				fullAddressId: {},
				postcodeFieldContainerId: {},
				postcodeId: {},
				postcodeLabelId: {},
				uprnId: {}
			},
			prototype: {


				render: function () {
					var instance = this;
					instance._init();
				},

				_init: function () {

					var instance = this;

					instance.set(ADDRESS_FORM_CONTAINER_ID, '#' + instance.get(NS) + 'addressFormContainer');
					instance.set(ADDRESS_LOOKUP_RESET_ID, '#' + instance.get(NS) + 'addressLookupReset');
					instance.set(BUTTON_ADDRESS_LOOKUP_ID, '#' + instance.get(NS) + 'btnAddressLookup');
					instance.set(FULL_ADDRESS_ID, '#' + instance.get(NS) + 'fullAddress');
					instance.set(POST_CODE_FIELD_CONTAINER_ID, '#' + instance.get(NS) + 'postcodeFieldContainer');
					instance.set(POSTCODE_ID, '#' + instance.get(NS) + 'postcode');
					instance.set(POST_CODE_LABEL_ID, '#' + instance.get(NS) + 'postcodeLabel');
					instance.set(UPRN_ID, '#' + instance.get(NS) + 'uprn');

					$(instance.get(BUTTON_ADDRESS_LOOKUP_ID)).on('click', function (e) {
						var postcodeValue = $(instance.get(POSTCODE_ID)).val();
						instance.lookupAddress(postcodeValue);
					});

					$(instance.get(ADDRESS_LOOKUP_RESET_ID)).on('click', function (e) {
						e.preventDefault();
						instance._resetAddressLookup();
						instance._callback(UNSELECTED_CALLBACK);
					});

					$(instance.get(UPRN_ID)).on('change', function (e) {
						instance._displaySelectedAddress(Liferay.Util.escape($(this).find("option:selected").text()));
						if ($(this).prop('selectedIndex') > 0) {
							instance._callback(SELECTED_CALLBACK);
						} else {
							instance._callback(UNSELECTED_CALLBACK);
						}
					});

				},

				lookupAddress: function (postcodeValue) {

					var instance = this;

					var uprnId = instance.get(UPRN_ID);
					var postcodeId = instance.get(POSTCODE_ID);
					var postcodeLabelId = instance.get(POST_CODE_LABEL_ID);
					var btnAddressLookupId = instance.get(BUTTON_ADDRESS_LOOKUP_ID);
					var addressFormContainerId = instance.get(ADDRESS_FORM_CONTAINER_ID);
					var addressLookupResetId = instance.get(ADDRESS_LOOKUP_RESET_ID);

					var isValidPostcode = postcodeValue.match(/[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z]{2}/gi);
					if (isValidPostcode != null) {
						postcodeValue = Liferay.Util.escape(postcodeValue);
						Liferay.Service(
							'/placecube_digitalplace.addresscontext/search-address-by-postcode',
							{
								companyId: Liferay.ThemeDisplay.getCompanyId(),
								postcode: postcodeValue,
								fallbackToNationalLookup: instance.get('fallbackToNationalLookup')
							},
							function (data) {

								var addressesFound = data;

								instance._displaySelectedAddress('');
								$(uprnId).html('');
								instance._clearErrorMessage();

								if (addressesFound.length === 0) {
									$(addressFormContainerId).addClass('hide');
									instance._configurePostcodeErrorMessage(Liferay.Language.get('no-address-found-for-postcode'));
								} else {
									$(postcodeId).addClass('hide');
									$(postcodeLabelId).html(postcodeValue);
									$(postcodeLabelId).removeClass('hide');
									$(btnAddressLookupId).addClass('hide');

									$(addressFormContainerId).removeClass('hide');
									$(addressLookupResetId).removeClass('hide');

									var emptyOption = A.Lang.sub(
										ENTRY_EMPTY_OPTION,
										{
											numberOfAddresses: addressesFound.length,
											postcode: postcodeValue
										}
									);
									$(uprnId).append(emptyOption);

									$.each(addressesFound, function (key, value) {
										var entryValue = A.Lang.sub(
											ENTRY_OPTION_TEMPLATE,
											{
												addressId: value.UPRN,
												addressValue: value.fullAddress,
												addressLine1Value: value.addressLine1,
												addressLine2Value: value.addressLine2,
												addressLine3Value: value.addressLine3,
												addressLine4Value: value.addressLine4,
												cityValue: value.city,
												selectedAddress: ''
											}
										);
										$(uprnId).append(entryValue);
									});
								}
							}
						);

					} else {
						instance._displaySelectedAddress('');
						$(uprnId).html('');
						instance._clearErrorMessage();

						$(addressFormContainerId).addClass('hide');
						instance._configurePostcodeErrorMessage(Liferay.Language.get('invalid-address-format'));
					}
				},

				_callback: function (callback) {
					var instance = this;
					if (instance.get(callback)) {
						eval(instance.get(callback));
					}
				},

				_clearErrorMessage: function () {
					var instance = this;

					var postcodeFieldContainerId = instance.get(POST_CODE_FIELD_CONTAINER_ID);
					$(postcodeFieldContainerId + ' .form-group.input-text-wrapper').removeClass('has-success has-error');
					$(postcodeFieldContainerId + ' .field.form-control').removeClass('error-field');
					$(postcodeFieldContainerId + ' .form-group.input-text-wrapper .form-validator-stack').remove();
				},

				_configurePostcodeErrorMessage: function (errorMessage) {
					var instance = this;

					var postcodeFieldContainerId = instance.get(POST_CODE_FIELD_CONTAINER_ID);
					$(postcodeFieldContainerId + ' .form-group.input-text-wrapper').addClass('has-error');
					$(postcodeFieldContainerId + ' .field.form-control').addClass('error-field');
					$(postcodeFieldContainerId + ' .form-group.input-text-wrapper').append('<div class="form-validator-stack help-block"><div role="alert" class="required">' + errorMessage + '</div></div>');
				},

				_displaySelectedAddress: function (fullAddressSelected) {
					var instance = this;
					var fullAddressDetailsId = '#' + instance.get(NS) + 'fullAddressDetails';
					var addressDisplayContainerId = '#' + instance.get(NS) + 'addressDisplayContainer';

					if (fullAddressSelected !== undefined && fullAddressSelected.length > 0) {
						$(instance.get(FULL_ADDRESS_ID)).val(fullAddressSelected);
					}

					if ($(addressDisplayContainerId).length) {
						if (fullAddressSelected !== undefined && fullAddressSelected.length > 0) {
							$(fullAddressDetailsId).html(fullAddressSelected);
							$(addressDisplayContainerId).removeClass('hide');
						} else {
							$(fullAddressDetailsId).html('');
							$(addressDisplayContainerId).addClass('hide');
						}
					}
				},

				_resetAddressLookup: function () {
					var instance = this;

					$(instance.get(ADDRESS_FORM_CONTAINER_ID)).addClass('hide');
					$(instance.get(ADDRESS_LOOKUP_RESET_ID)).addClass('hide');
					$(instance.get(FULL_ADDRESS_ID)).val('');
					$(instance.get(UPRN_ID)).html('');

					$(instance.get(POST_CODE_FIELD_CONTAINER_ID) + ' .field.form-control').val('');
					$(instance.get(POST_CODE_LABEL_ID)).addClass('hide');
					$(instance.get(POSTCODE_ID)).removeClass('hide');
					$(instance.get(BUTTON_ADDRESS_LOOKUP_ID)).removeClass('hide');
				}
			}
		});
	},
	'',
	{
		requires: [
			'aui-base',
			'liferay-util-window'
		]
	}
);
