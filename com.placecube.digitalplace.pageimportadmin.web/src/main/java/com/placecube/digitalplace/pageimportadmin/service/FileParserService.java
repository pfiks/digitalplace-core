package com.placecube.digitalplace.pageimportadmin.service;

import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.pageimportadmin.model.RowImportModel;

@Component(immediate = true, service = FileParserService.class)
public class FileParserService {

	static final int ROW_INDEX_TITLE = 0;
	static final int ROW_INDEX_URL = 1;
	static final int ROW_INDEX_PARENT_PAGE_URL = 2;
	static final int ROW_INDEX_PAGE_TEMPLATE_NAME = 3;
	static final int ROW_INDEX_WEBCONTENT_STRUCTURE_NAME = 4;
	static final int ROW_INDEX_CATEGORIES = 5;

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	public RowImportModel getRowContent(Sheet sheet, int index, long groupId, boolean privateLayout) {
		Row row = sheet.getRow(index);

		if (row != null) {
			String title = modelFactoryBuilder.getStringField(row, ROW_INDEX_TITLE);
			String url = modelFactoryBuilder.getStringField(row, ROW_INDEX_URL);
			String parentPageURL = modelFactoryBuilder.getStringField(row, ROW_INDEX_PARENT_PAGE_URL);
			String pageTemplateName = modelFactoryBuilder.getStringField(row, ROW_INDEX_PAGE_TEMPLATE_NAME);
			String webContentStructureName = modelFactoryBuilder.getStringField(row, ROW_INDEX_WEBCONTENT_STRUCTURE_NAME);
			List<String> categories = getMultivaluedField(row, ROW_INDEX_CATEGORIES);

			if (modelFactoryBuilder.importAsWebContent(pageTemplateName, webContentStructureName)) {
				return modelFactoryBuilder.getWebContentImportModel(index, groupId, title, url, categories, webContentStructureName);
			} else {
				return modelFactoryBuilder.getPageImportModel(index, groupId, title, url, categories, privateLayout, parentPageURL, pageTemplateName);
			}

		} else {
			RowImportModel emptyRowImportModel = modelFactoryBuilder.getEmptyRowImportModel(index, groupId);
			emptyRowImportModel.addErrorMessage("Unable to parse empty row.");
			return emptyRowImportModel;
		}

	}

	private List<String> getMultivaluedField(Row row, int index) {
		String value = modelFactoryBuilder.getStringField(row, index);
		return Arrays.asList(StringUtil.split(value, StringPool.SEMICOLON));
	}

}
