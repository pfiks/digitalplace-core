package com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLSearchListingsPortletInstanceConfiguration")
public interface DDLSearchListingsPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = "0")
	long ddmStructureId();

	@Meta.AD(required = false, deflt = "")
	String defaultDisplayStyle();

	@Meta.AD(required = false, deflt = "col-md-4")
	String gridDisplayStyleCss();

	@Meta.AD(required = false, deflt = "false")
	boolean hidePagination();

	@Meta.AD(required = false, deflt = "20")
	int numberOfResultsPerPage();
}
