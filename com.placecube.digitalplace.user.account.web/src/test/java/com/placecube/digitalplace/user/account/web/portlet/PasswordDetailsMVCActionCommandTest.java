package com.placecube.digitalplace.user.account.web.portlet;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ SessionMessages.class, SessionErrors.class, PortalUtil.class })
public class PasswordDetailsMVCActionCommandTest extends PowerMockito {

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private AccountService mockAccountService;

	@Mock
	private AccountValidationService mockAccountValidationService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private PasswordDetailsMVCActionCommand passwordDetailsMVCActionCommand;

	@Before
	public void activateSetup() {
		mockStatic(SessionMessages.class, SessionErrors.class, PortalUtil.class);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenExceptionCreatingUser_ThenAddsExceptionToSessionErrors(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);
		UserAccountException exception = new UserAccountException();
		doThrow(exception).when(mockAccountService).createUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);

		passwordDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, exception.getClass(), exception);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenExceptionCreatingUser_ThenHidesDefaultErrorMessage(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);
		String portletId = "portletIdValue";
		when(PortalUtil.getPortletId(mockActionRequest)).thenReturn(portletId);
		doThrow(new UserAccountException()).when(mockAccountService).createUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);

		passwordDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionMessages.class, times(1));
		SessionMessages.add(mockActionRequest, portletId + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenExceptionCreatingUser_ThenSetsPasswordDetailsAsRenderCommandName(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);
		doThrow(new UserAccountException()).when(mockAccountService).createUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);

		passwordDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.PASSWORD_DETAILS);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoError_ThenCreatesTheAccount(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);
		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(addressOutOfLocalArea);

		passwordDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountService, mockAccountContextService);
		inOrder.verify(mockAccountContextService, times(1)).configurePasswordDetails(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountService, times(1)).createUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoError_ThenSetsConfirmationPageAsRenderCommandName(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);

		passwordDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.CONFIRMATION_PAGE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoValidationErrorExists_ThenRenderCommandSetToPasswordDetails(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);
		when(mockAccountContext.hasErrors()).thenReturn(true);

		passwordDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.PASSWORD_DETAILS);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenValidationBegins_ThenExistingErrorsHaveBeenCleared(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);

		passwordDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountContext, mockAccountValidationService);
		inOrder.verify(mockAccountContext, times(1)).clearErrors();
		inOrder.verify(mockAccountValidationService, times(1)).validatePasswordForm(mockAccountContext, mockActionRequest);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenValidationBegins_ThenUpdateContextInSession(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);

		passwordDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockActionRequest);
	}

	private void mockBasicDetails(boolean addressOutOfLocalArea) throws UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(addressOutOfLocalArea);
	}
}
