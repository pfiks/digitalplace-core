package com.placecube.digitalplace.session.timeout.dynamicinclude;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.servlet.taglib.BaseJSPDynamicInclude;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.session.timeout.web.config.SessionTimeoutCompanyConfiguration;
import com.placecube.digitalplace.session.timeout.web.service.SessionTimeoutPropertyService;

@Component(immediate = true, service = DynamicInclude.class)
public class SessionTimeoutDynamicInclude extends BaseJSPDynamicInclude {

	private static final Log LOG = LogFactoryUtil.getLog(SessionTimeoutDynamicInclude.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private SessionTimeoutPropertyService sessionTimeoutPropertyService;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.session.timeout.web)", unbind = "-")
	private ServletContext servletContext;

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String key) throws IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			if (Validator.isNotNull(themeDisplay)) {
				SessionTimeoutCompanyConfiguration sessionTimeoutCompanyConfiguration = configurationProvider.getCompanyConfiguration(SessionTimeoutCompanyConfiguration.class,
						themeDisplay.getCompanyId());
				int configuredTimeout = sessionTimeoutPropertyService.getValidTimeout(sessionTimeoutCompanyConfiguration.timeoutInMinutes());
				httpServletRequest.setAttribute("customSessionLengthInMins", configuredTimeout);
				httpServletRequest.setAttribute("systemTimeout", sessionTimeoutPropertyService.getSystemConfiguredTimeout());
				httpServletRequest.setAttribute("isSignedIn", themeDisplay.isSignedIn());

			}
		} catch (ConfigurationException e) {
			LOG.error("Error getting session timeout configuration", e);
		}

		super.include(httpServletRequest, httpServletResponse, key);
	}

	@Override
	public void register(DynamicIncludeRegistry dynamicIncludeRegistry) {
		dynamicIncludeRegistry.register("/html/common/themes/bottom.jsp#post");
	}

	@Override
	protected String getJspPath() {
		return "/dynamic_include/bottom.jsp";
	}

	@Override
	protected Log getLog() {
		return LOG;
	}

	@Override
	protected ServletContext getServletContext() {
		return servletContext;
	}

}
