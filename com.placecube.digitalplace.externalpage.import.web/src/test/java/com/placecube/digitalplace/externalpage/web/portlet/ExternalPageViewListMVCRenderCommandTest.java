package com.placecube.digitalplace.externalpage.web.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.application.list.GroupProvider;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.display.context.ExternalManagementToolbarDisplayContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, ExternalPageViewListMVCRenderCommand.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.util.ParamUtil")
public class ExternalPageViewListMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private ExternalPageViewListMVCRenderCommand externalPageViewListMVCRenderCommand;

	@Mock
	private ExternalManagementToolbarDisplayContext mockExternalManagementToolbarDisplayContext;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupProvider mockGroupProvider;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LiferayPortletRequest mockLiferayPortletRequest;

	@Mock
	private LiferayPortletResponse mockLiferayPortletResponse;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	private SearchContainer<ExternalPage> mockSearchContainer;

	@Before
	public void activeSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenNoError_ThenSetExternalManagementToolbarAttribute() throws Exception {
		long groupId = 1L;
		int total = 20;

		List<ExternalPage> mockPageList = new ArrayList<>();
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);

		mockSearchContainer = mock(SearchContainer.class);
		whenNew(SearchContainer.class)
				.withArguments(mockRenderRequest, null, null, mockSearchContainer.DEFAULT_CUR_PARAM, mockSearchContainer.DEFAULT_DELTA, mockPortletURL, null, "no-entries-were-found")
				.thenReturn(mockSearchContainer);

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockGroupProvider.getGroup(mockHttpServletRequest)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockExternalPageLocalService.getExternalPagesCount(groupId)).thenReturn(total);
		when(mockSearchContainer.getStart()).thenReturn(0);
		when(mockSearchContainer.getEnd()).thenReturn(20);
		when(mockExternalPageLocalService.getExternalPages(groupId, 0, 20)).thenReturn(mockPageList);

		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockPortal.getLiferayPortletResponse(mockRenderResponse)).thenReturn(mockLiferayPortletResponse);
		whenNew(ExternalManagementToolbarDisplayContext.class).withArguments(mockLiferayPortletRequest, mockLiferayPortletResponse, mockPortal.getHttpServletRequest(mockRenderRequest))
				.thenReturn(mockExternalManagementToolbarDisplayContext);

		externalPageViewListMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("externalManagementToolbarDisplayContext", mockExternalManagementToolbarDisplayContext);

	}

	@Test
	public void render_WhenNoError_ThenSetSearchContainerAttribute() throws Exception {
		long groupId = 1L;
		int total = 20;

		List<ExternalPage> mockPageList = new ArrayList<>();
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);

		mockSearchContainer = mock(SearchContainer.class);
		whenNew(SearchContainer.class)
				.withArguments(mockRenderRequest, null, null, mockSearchContainer.DEFAULT_CUR_PARAM, mockSearchContainer.DEFAULT_DELTA, mockPortletURL, null, "no-entries-were-found")
				.thenReturn(mockSearchContainer);

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockGroupProvider.getGroup(mockHttpServletRequest)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);

		when(mockExternalPageLocalService.getExternalPagesCount(groupId)).thenReturn(total);

		when(mockSearchContainer.getStart()).thenReturn(0);
		when(mockSearchContainer.getEnd()).thenReturn(20);
		when(mockExternalPageLocalService.getExternalPages(groupId, 0, 20)).thenReturn(mockPageList);

		when(mockPortal.getLiferayPortletRequest(mockRenderRequest)).thenReturn(mockLiferayPortletRequest);
		when(mockPortal.getLiferayPortletResponse(mockRenderResponse)).thenReturn(mockLiferayPortletResponse);
		whenNew(ExternalManagementToolbarDisplayContext.class).withArguments(mockLiferayPortletRequest, mockLiferayPortletResponse, mockPortal.getHttpServletRequest(mockRenderRequest))
				.thenReturn(mockExternalManagementToolbarDisplayContext);

		externalPageViewListMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockSearchContainer, times(1)).setResultsAndTotal(any(), eq(total));
		verify(mockRenderRequest, times(1)).setAttribute("searchContainers", mockSearchContainer);

	}

}
