package com.placecube.digitalplace.payment.capitapaymentinterface.models;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleConfiguration;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleItemConfiguration;
import com.placecube.digitalplace.payment.model.PaymentContext;

@RunWith(PowerMockRunner.class)
public class SimpleSaleConfigurationTest extends PowerMockito {

	private static int TOTAL_SALE_AMOUNT_IN_MINOR_UNITS = 999;

	private SimpleSaleConfiguration simpleSaleConfiguration;

	@Mock
	private PaymentContext mockPaymentContext;

	@Test
	public void getPaymentContext_WhenNoError_ThenReturnThePaymentContextProvidedInTheConstructor() {
		boolean useAdvancedConfig = false;
		simpleSaleConfiguration = SimpleSaleConfiguration.newSimpleSaleConfiguration(mockPaymentContext, TOTAL_SALE_AMOUNT_IN_MINOR_UNITS, useAdvancedConfig);

		PaymentContext paymentContext = simpleSaleConfiguration.getPaymentContext();

		assertEquals(mockPaymentContext, paymentContext);
	}

	@Test
	public void getSimpleSaleItemConfigurations_WhenUseAdvancedConfig_ThenSalesItemsGeneratedFromAdvancedConfig() {
		boolean useAdvancedConfig = true;
		int numberOfSalesItems = 2;
		simpleSaleConfiguration = SimpleSaleConfiguration.newSimpleSaleConfiguration(mockPaymentContext, TOTAL_SALE_AMOUNT_IN_MINOR_UNITS, useAdvancedConfig);
		when(mockPaymentContext.getAdvancedConfiguration()).thenReturn(getAdvancedConfig(numberOfSalesItems));

		List<SimpleSaleItemConfiguration> simpleSaleItemConfigurations = simpleSaleConfiguration.getSimpleSaleItemConfigurations();

		int itemCounter = 0;
		List<String> itemSettingKeys = getItemSettingsKeys();
		for (SimpleSaleItemConfiguration simpleSaleItemConfiguration : simpleSaleItemConfigurations) {
			for (String itemSettingKey : itemSettingKeys) {
				assertEquals(getSettingValueString(itemSettingKey, itemCounter), simpleSaleItemConfiguration.getSaleItemDetail(itemSettingKey));
			}
			itemCounter++;
		}
		verify(mockPaymentContext, times(1)).getAdvancedConfiguration();
	}

	@Test
	public void getSimpleSaleItemConfigurations_WhenNotUseAdvancedConfig_ThenStandardSingleSalesItemIsGenerated() {
		boolean useAdvancedConfig = false;
		String salesDescription = "salesDescription";
		String itemReference = "itemReference";
		String accountId = "accountId";
		simpleSaleConfiguration = SimpleSaleConfiguration.newSimpleSaleConfiguration(mockPaymentContext, TOTAL_SALE_AMOUNT_IN_MINOR_UNITS, useAdvancedConfig);
		when(mockPaymentContext.getSalesDescription()).thenReturn(salesDescription);
		when(mockPaymentContext.getItemReference()).thenReturn(itemReference);
		when(mockPaymentContext.getAccountId()).thenReturn(accountId);

		List<SimpleSaleItemConfiguration> simpleSaleItemConfigurations = simpleSaleConfiguration.getSimpleSaleItemConfigurations();
		SimpleSaleItemConfiguration simpleSaleItemConfiguration = simpleSaleItemConfigurations.get(0);

		assertEquals(1, simpleSaleItemConfigurations.size());
		assertEquals(String.valueOf(TOTAL_SALE_AMOUNT_IN_MINOR_UNITS), simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_AMOUNT_IN_MINOR_UNITS));
		assertEquals(salesDescription, simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_DESCRIPTION));
		assertEquals(itemReference, simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_REFERENCE));
		assertEquals(accountId, simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_FUND_CODE));
		assertEquals(StringPool.BLANK, simpleSaleItemConfiguration.getSaleItemDetail("adminEmailAddresses"));
		verify(mockPaymentContext, never()).getAdvancedConfiguration();
	}

	private Map<String, String> getAdvancedConfig(int numberOfItems) {
		Map<String, String> advancedConfig = new HashMap<>();
		for (int i = 0; i < numberOfItems; i++) {
			populateOneAdvancedItem(advancedConfig, i);
		}

		return advancedConfig;
	}

	private List<String> getItemSettingsKeys() {
		List<String> itemSettingKeys = new ArrayList<>();
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_DESCRIPTION);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_AMOUNT_IN_MINOR_UNITS);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_FUND_CODE);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_FORENAME);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_SURNAME);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_TITLE);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_NARRATIVE);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_REFERENCE);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_TAX_VAT_VAT_AMOUNT_IN_MINOR_UNITS);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_TAX_VAT_VAT_CODE);
		itemSettingKeys.add(CapitaConstants.SALE_ITEM_TAX_VAT_VAT_RATE);

		return itemSettingKeys;
	}

	private String getSettingValueString(String itemSettingKey, int itemNumber) {
		return String.format(itemSettingKey + "_VAL_%s", itemNumber);
	}

	private String getSettingKeyString(String itemSettingKey, int itemNumber) {
		return String.format("simpleSale.items[%s].%s", itemNumber, itemSettingKey);
	}

	private void populateOneAdvancedItem(Map<String, String> advancedConfig, int itemNumber) {

		List<String> itemSettingKeys = getItemSettingsKeys();
		for (String itemSettingKey : itemSettingKeys) {
			advancedConfig.put(getSettingKeyString(itemSettingKey, itemNumber), getSettingValueString(itemSettingKey, itemNumber));
		}

	}

}
