package com.placecube.digitalplace.user.settings.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.displaycontext.UserSettingsDisplayContext;
import com.placecube.digitalplace.user.settings.web.model.UserSettingsSection;
import com.placecube.digitalplace.user.settings.web.registry.UserSettingsSectionRegistry;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ UserSettingsDisplayContext.class, MVCPortlet.class, PropsUtil.class, ParamUtil.class })
public class UserSettingsPortletTest extends PowerMockito {

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserSettingsDisplayContext mockUserSettingsDisplayContext;

	@Mock
	private UserSettingsSectionRegistry mockUserSettingsSectionRegistry;

	@Mock
	private Map<DisplaySection, List<UserSettingsSection>> mockUserSettingsSections;

	@InjectMocks
	private UserSettingsPortlet userSettingsPortlet;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, UserSettingsDisplayContext.class, ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenUserIsNotSignedIn_ThenThrowsPortletException() throws IOException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(false);

		userSettingsPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenUserIsSignedIn_ThenAddsPortletURLAsRequestAttribute() throws IOException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		suppress(methods(MVCPortlet.class, "render"));
		String currentSectionId = "currentIdVal";
		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.CURRENT_SECTION_ID)).thenReturn(currentSectionId);

		userSettingsPortlet.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = Mockito.inOrder(mockRenderRequest, mockPortletURL, mockMutableRenderParameters);
		inOrder.verify(mockMutableRenderParameters, times(1)).setValue(PortletRequestKeys.CURRENT_SECTION_ID, currentSectionId);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("portletURL", mockPortletURL);
	}

	@Test
	public void render_WhenUserIsSignedIn_ThenAddsUserSettingsDisplayContextAsRequestAttribute() throws IOException, PortletException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockUserSettingsSectionRegistry.getUserSettingsSections(mockUser)).thenReturn(mockUserSettingsSections);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(UserSettingsDisplayContext.initDisplayContext(mockUserSettingsSections)).thenReturn(mockUserSettingsDisplayContext);
		suppress(methods(MVCPortlet.class, "render"));

		userSettingsPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("userSettingsDisplayContext", mockUserSettingsDisplayContext);
	}

}
