package com.placecube.digitalplace.userindexer.constants;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.liferay.petra.string.StringPool;

public class UserIndexerFieldsTest {

	@Test
	public void getRoleIdsSearchFilter_WhenNoError_ThenReturnsFilterWithRoleId() {

		int roleId = 1;
		String expected = "(" + UserIndexerFields.ROLE_IDS + ":" + StringPool.QUOTE + +roleId + StringPool.QUOTE + ")";

		String result = UserIndexerFields.getRoleIdsSearchFilter(roleId);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void getUserGroupRoleIdsSearchFilter_WhenNoError_ThenReturnsFilterWithGroupIdAndRoleId() {

		int groupId = 2;
		int roleId = 1;

		String expected = "(" + UserIndexerFields.GROUP_ROLE_IDS + ":" + StringPool.QUOTE + groupId + StringPool.DASH + roleId + StringPool.QUOTE + ")";

		String result = UserIndexerFields.getUserGroupRoleIdsSearchFilter(groupId, roleId);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void getUserGroupRoleSearchValue_WhenNoError_ThenReturnsSearchValueWithGroupIdAndRoleId() {

		int groupId = 2;
		int roleId = 1;

		String expected = groupId + StringPool.DASH + roleId;

		String result = UserIndexerFields.getUserGroupRoleSearchValue(groupId, roleId);

		assertThat(result, equalTo(expected));

	}
}
