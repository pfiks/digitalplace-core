package com.placecube.digitalplace.payment.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.address.model.AddressContext;

public class PaymentContext implements Serializable {

	public static class PaymentContextBuilder {

		private String accountId;
		private AddressContext address;
		private Map<String, String> advancedConfiguration;
		private BigDecimal amount;
		private String backURL;
		private String emailAddress;
		private String firstName;
		private String internalReference;
		private boolean isImpersonated = false;
		private String itemReference;
		private String lastName;
		private long realUserId;
		private boolean resumeOnPaymentCancelOrError = false;
		private String returnURL;
		private String salesDescription;
		private String salesReference;

		public PaymentContextBuilder() {
		}

		public PaymentContext build() {
			return new PaymentContext(this);
		}

		public PaymentContextBuilder setAccountId(String accountId) {
			this.accountId = accountId;
			return this;
		}

		public PaymentContextBuilder setAddress(AddressContext address) {
			this.address = address;
			return this;
		}

		public PaymentContextBuilder setAdvancedConfiguration(Map<String, String> advancedConfiguration) {
			this.advancedConfiguration = advancedConfiguration;
			return this;
		}

		public PaymentContextBuilder setAmount(BigDecimal amount) {
			this.amount = amount;
			return this;
		}

		public PaymentContextBuilder setBackURL(String backURL) {
			this.backURL = backURL;
			return this;
		}

		public PaymentContextBuilder setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
			return this;
		}

		public PaymentContextBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public PaymentContextBuilder setImpersonated(boolean isImpersonated) {
			this.isImpersonated = isImpersonated;
			return this;
		}

		public PaymentContextBuilder setInternalReference(String internalReference) {
			this.internalReference = internalReference;
			return this;
		}

		public PaymentContextBuilder setItemReference(String itemReference) {
			this.itemReference = itemReference;
			return this;
		}

		public PaymentContextBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public PaymentContextBuilder setRealUserId(long realUserId) {
			this.realUserId = realUserId;
			return this;
		}

		public PaymentContextBuilder setResumeOnPaymentCancelOrError(boolean resumeOnPaymentCancelOrError) {
			this.resumeOnPaymentCancelOrError = resumeOnPaymentCancelOrError;
			return this;
		}

		public PaymentContextBuilder setReturnURL(String returnURL) {
			this.returnURL = returnURL;
			return this;
		}

		public PaymentContextBuilder setSalesDescription(String salesDescription) {
			this.salesDescription = salesDescription;
			return this;
		}

		public PaymentContextBuilder setSalesReference(String salesReference) {
			this.salesReference = salesReference;
			return this;
		}

	}

	private static final long serialVersionUID = 1L;

	private String accountId;
	private AddressContext address;
	private Map<String, String> advancedConfiguration;
	private BigDecimal amount;
	private String backURL;
	private String emailAddress;
	private String firstName;
	private String internalReference;
	private boolean isImpersonated = false;
	private String itemReference;
	private String lastName;
	private long realUserId;
	private boolean resumeOnPaymentCancelOrError = false;
	private String returnURL;
	private String salesDescription;
	private String salesReference;

	public PaymentContext(Map<String, String> advancedConfiguration, String firstName, String lastName, String emailAddress, String internalReference, AddressContext address, String returnUrl,
			String backURL) {
		this.advancedConfiguration = advancedConfiguration;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailAddress = emailAddress;
		this.internalReference = internalReference;
		this.address = address;
		returnURL = returnUrl;
		this.backURL = backURL;
	}

	public PaymentContext(String accountId, String salesDescription, String salesReference, String internalReference, String itemReference, BigDecimal amount, String firstName, String lastName,
			String emailAddress, AddressContext address, String returnUrl, String backURL) {
		this.accountId = accountId;
		this.salesDescription = salesDescription;
		this.salesReference = salesReference;
		this.internalReference = internalReference;
		this.itemReference = itemReference;
		this.amount = amount;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailAddress = emailAddress;
		this.address = address;
		returnURL = returnUrl;
		this.backURL = backURL;
		advancedConfiguration = new HashMap<>();
	}

	private PaymentContext(PaymentContextBuilder paymentContextBuilder) {
		accountId = paymentContextBuilder.accountId;
		address = paymentContextBuilder.address;
		advancedConfiguration = paymentContextBuilder.advancedConfiguration;
		amount = paymentContextBuilder.amount;
		backURL = paymentContextBuilder.backURL;
		emailAddress = paymentContextBuilder.emailAddress;
		firstName = paymentContextBuilder.firstName;
		internalReference = paymentContextBuilder.internalReference;
		itemReference = paymentContextBuilder.itemReference;
		lastName = paymentContextBuilder.lastName;
		resumeOnPaymentCancelOrError = paymentContextBuilder.resumeOnPaymentCancelOrError;
		returnURL = paymentContextBuilder.returnURL;
		salesDescription = paymentContextBuilder.salesDescription;
		salesReference = paymentContextBuilder.salesReference;
		realUserId = paymentContextBuilder.realUserId;
		isImpersonated = paymentContextBuilder.isImpersonated;
	}

	public String getAccountId() {
		return accountId;
	}

	public AddressContext getAddress() {
		return address;
	}

	public Map<String, String> getAdvancedConfiguration() {
		return advancedConfiguration;
	}

	public String getAdvancedConfigurationValue(String key) {
		return advancedConfiguration.getOrDefault(key, StringPool.BLANK);
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getBackURL() {
		return backURL;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getInternalReference() {
		return internalReference;
	}

	public String getItemReference() {
		return itemReference;
	}

	public String getLastName() {
		return lastName;
	}

	public long getRealUserId(Optional<ThemeDisplay> themeDisplay) {
		if (realUserId <= 0 && themeDisplay.isPresent()) {
			return themeDisplay.get().getRealUserId();
		}
		return realUserId;
	}

	public String getReturnURL() {
		return returnURL;
	}

	public String getSalesDescription() {
		return salesDescription;
	}

	public String getSalesReference() {
		return salesReference;
	}

	public boolean isImpersonated(Optional<ThemeDisplay> themeDisplay) {
		if (realUserId <= 0 && themeDisplay.isPresent()) {
			return themeDisplay.get().isImpersonated();
		}
		return isImpersonated;
	}

	public boolean isResumeOnPaymentCancelOrError() {
		return resumeOnPaymentCancelOrError;
	}

	public void setResumeOnPaymentCancelOrError(boolean resumeOnPaymentCancelOrError) {
		this.resumeOnPaymentCancelOrError = resumeOnPaymentCancelOrError;
	}

	@Override
	public String toString() {
		return "PaymentContext [accountId=" + accountId + ", salesDescription=" + salesDescription + ", salesReference=" + salesReference + ", internalReference=" + internalReference
				+ ", itemReference=" + itemReference + ", amount=" + amount + ", firstName=" + firstName + ", lastName=" + lastName + ", emailAddress=" + emailAddress + ", address=" + address
				+ ", returnURL=" + returnURL + ", backURL=" + backURL + "]";
	}
}
