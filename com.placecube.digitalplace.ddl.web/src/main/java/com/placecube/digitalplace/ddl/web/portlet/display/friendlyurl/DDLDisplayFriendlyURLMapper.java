package com.placecube.digitalplace.ddl.web.portlet.display.friendlyurl;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.DefaultFriendlyURLMapper;
import com.liferay.portal.kernel.portlet.FriendlyURLMapper;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;

@Component(property = { "com.liferay.portlet.friendly-url-routes=META-INF/friendly-url-routes/routes.xml", "javax.portlet.name=" + PortletKeys.DDL_DISPLAY_PORTLET }, service = FriendlyURLMapper.class)
public class DDLDisplayFriendlyURLMapper extends DefaultFriendlyURLMapper {

	private static final String MAPPING = "ddl_display";

	@Override
	public String getMapping() {
		return MAPPING;
	}

}
