package com.placecube.digitalplace.companyinitializeradmin.constants;

public final class PortletRequestKeys {

	public static final String COMPANY_INITIALIZERS = "companyInitializers";

	public static final String SELECTED_COMPANY_INITIALIZER = "selectedCompanyInitializer";

	private PortletRequestKeys() {
	}
}