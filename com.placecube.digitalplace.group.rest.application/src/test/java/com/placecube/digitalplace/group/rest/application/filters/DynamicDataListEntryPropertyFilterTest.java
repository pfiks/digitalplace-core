package com.placecube.digitalplace.group.rest.application.filters;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.PropertyWriter;

public class DynamicDataListEntryPropertyFilterTest extends PowerMockito {

	private DynamicDataListEntryPropertyFilter dynamicDataListEntryPropertyFilter;

	@Mock
	private BeanPropertyWriter mockBeanPropertyWriter;

	@Mock
	private JsonGenerator mockJgen;

	@Mock
	private Object mockPojo;

	@Mock
	private PropertyWriter mockPropertyWriter;

	@Mock
	private SerializerProvider mockSerializerProvider;

	@Before
	public void activeSetup() {

		initMocks(this);

		dynamicDataListEntryPropertyFilter = new DynamicDataListEntryPropertyFilter();
	}

	@Test
	public void include_WithBeanPropertyWriter_WhenNoError_ThenReturnsTrue() {
		assertTrue(dynamicDataListEntryPropertyFilter.include(mockBeanPropertyWriter));
	}

	@Test
	public void include_WithPropertyWriter_WhenNoError_ThenReturnsTrue() {
		assertTrue(dynamicDataListEntryPropertyFilter.include(mockPropertyWriter));
	}

	@Test
	public void serializeAsField_WhenNameIsDDLRecord_ThenFieldIsNotSerialized() throws Exception {

		when(mockPropertyWriter.getName()).thenReturn("ddlrecord");

		dynamicDataListEntryPropertyFilter.serializeAsField(mockPojo, mockJgen, mockSerializerProvider, mockPropertyWriter);

		verify(mockPropertyWriter, never()).serializeAsField(mockPojo, mockJgen, mockSerializerProvider);
	}

	@Test
	public void serializeAsField_WhenNameIsNotDDLRecord_ThenSerializeAsField() throws Exception {

		when(mockPropertyWriter.getName()).thenReturn("field");

		dynamicDataListEntryPropertyFilter.serializeAsField(mockPojo, mockJgen, mockSerializerProvider, mockPropertyWriter);

		verify(mockPropertyWriter, times(1)).serializeAsField(mockPojo, mockJgen, mockSerializerProvider);
	}

}
