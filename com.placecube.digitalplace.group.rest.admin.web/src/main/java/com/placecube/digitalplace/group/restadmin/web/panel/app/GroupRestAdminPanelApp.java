package com.placecube.digitalplace.group.restadmin.web.panel.app;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletKeys;

@Component(immediate = true, property = { "panel.app.order:Integer=1000", "panel.category.key=" + PanelCategoryKeys.SITE_ADMINISTRATION_CONFIGURATION, }, service = PanelApp.class)
public class GroupRestAdminPanelApp extends BasePanelApp {

	@Reference(target = "(javax.portlet.name=" + PortletKeys.GROUP_REST_ADMIN + ")", unbind = "-")
	private Portlet portlet;

	@Override
	public Portlet getPortlet() {
		return portlet;
	}

	@Override
	public String getPortletId() {
		return PortletKeys.GROUP_REST_ADMIN;
	}

	@Override
	public boolean isShow(PermissionChecker permissionChecker, Group group) {
		return !group.isCompany() && !group.isGuest() && group.isSite() && permissionChecker.isCompanyAdmin(group.getCompanyId());
	}

}
