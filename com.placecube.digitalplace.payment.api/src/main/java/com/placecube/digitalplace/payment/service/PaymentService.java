package com.placecube.digitalplace.payment.service;

import java.util.Optional;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.AgreementStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;

public interface PaymentService {

	boolean cancelAgreement(String agreementId, ServiceContext serviceContext);

	AgreementStatus createAgreement(PaymentContext paymentContext, ServiceContext serviceContext);

	AgreementStatus getAgreementStatus(String agreementId, ServiceContext serviceContext);

	Optional<String> getBackURL(String paymentReference, ServiceContext serviceContext);

	Optional<String> getDefaultAdvancedConfiguration(long companyId);

	PaymentStatus getPaymentStatus(String paymentReference, ServiceContext serviceContext);

	PaymentStatus getRecurringPaymentStatus(String paymentId, ServiceContext serviceContext);

	PaymentResponse preparePayment(PaymentContext paymentContext, ServiceContext serviceContext);

	PaymentResponse prepareRecurringPayment(PaymentContext paymentContext, ServiceContext serviceContext);

	PaymentResponse takeRecurringPayment(PaymentContext paymentContext, String agreementId, String idempotencyKey, ServiceContext serviceContext);

	String getAgentReferredPaymentPayload(long companyId, AgentReferredPaymentContext agentReferredPaymentContext) throws PortalException;

	PaymentResponse processAgentReferredPaymentResponse(String externalReferenceCode, ServiceContext serviceContext);

}