<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="/init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post" name="fm">

	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />

	<liferay-frontend:edit-form-body>
		<dp-frontend:fieldset-group>

			<liferay-frontend:fieldset label="portlet-settings">
				<aui:input
					label="display-portlet"
					name="displayPortlet"
					type="checkbox"
					checked="${configuration.displayPortlet()}"
				/>
				<aui:input
					label="chain-sort-by-options"
					name="chainSortByOptions"
					type="checkbox"
					checked="${configuration.chainSortByOptions()}"
				/>
                <aui:input
                    label="sort-direction-changeable"
                    name="sortDirectionChangeable"
                    type="checkbox"
                    checked="${configuration.sortDirectionChangeable()}"
                />
			</liferay-frontend:fieldset>

			<aui:input name="enabledSortByOptions" type="hidden" />
			<liferay-ui:input-move-boxes
				leftBoxName="availableValues"
				leftList="${availableSortByOptionsKeyValuePair}"
				leftTitle="available"
				rightBoxName="selectedValues"
				rightList="${ enabledSortByOptionsKeyValuePair }"
				rightTitle="selected"
				rightReorder="true"
			/>

		</dp-frontend:fieldset-group>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>



<aui:script use="liferay-util-list-fields">
A.one('#<portlet:namespace/>fm').on('submit', function(event) {
	var selectedValues = Liferay.Util.listSelect('#<portlet:namespace/>selectedValues');
	A.one('#<portlet:namespace/>enabledSortByOptions').val(selectedValues);
	submitForm('#<portlet:namespace/>fm');
});
</aui:script>