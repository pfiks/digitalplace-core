/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.group.rest.exception.NoSuchGroupApiKeyException;
import com.placecube.digitalplace.group.rest.model.GroupApiKey;

import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the group api key service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GroupApiKeyUtil
 * @generated
 */
@ProviderType
public interface GroupApiKeyPersistence extends BasePersistence<GroupApiKey> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link GroupApiKeyUtil} to access the group api key persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the group api keys where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @return the matching group api keys
	 */
	public java.util.List<GroupApiKey> findByApplicationKey(
		String applicationKey);

	/**
	 * Returns a range of all the group api keys where applicationKey = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationKey the application key
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of matching group api keys
	 */
	public java.util.List<GroupApiKey> findByApplicationKey(
		String applicationKey, int start, int end);

	/**
	 * Returns an ordered range of all the group api keys where applicationKey = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationKey the application key
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group api keys
	 */
	public java.util.List<GroupApiKey> findByApplicationKey(
		String applicationKey, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator);

	/**
	 * Returns an ordered range of all the group api keys where applicationKey = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationKey the application key
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group api keys
	 */
	public java.util.List<GroupApiKey> findByApplicationKey(
		String applicationKey, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	public GroupApiKey findByApplicationKey_First(
			String applicationKey,
			com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
				orderByComparator)
		throws NoSuchGroupApiKeyException;

	/**
	 * Returns the first group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	public GroupApiKey fetchByApplicationKey_First(
		String applicationKey,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator);

	/**
	 * Returns the last group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	public GroupApiKey findByApplicationKey_Last(
			String applicationKey,
			com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
				orderByComparator)
		throws NoSuchGroupApiKeyException;

	/**
	 * Returns the last group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	public GroupApiKey fetchByApplicationKey_Last(
		String applicationKey,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator);

	/**
	 * Returns the group api keys before and after the current group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param groupApiKeyPK the primary key of the current group api key
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	public GroupApiKey[] findByApplicationKey_PrevAndNext(
			GroupApiKeyPK groupApiKeyPK, String applicationKey,
			com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
				orderByComparator)
		throws NoSuchGroupApiKeyException;

	/**
	 * Removes all the group api keys where applicationKey = &#63; from the database.
	 *
	 * @param applicationKey the application key
	 */
	public void removeByApplicationKey(String applicationKey);

	/**
	 * Returns the number of group api keys where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @return the number of matching group api keys
	 */
	public int countByApplicationKey(String applicationKey);

	/**
	 * Returns all the group api keys where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @return the matching group api keys
	 */
	public java.util.List<GroupApiKey> findByApplicationSecret(
		String applicationSecret);

	/**
	 * Returns a range of all the group api keys where applicationSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationSecret the application secret
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of matching group api keys
	 */
	public java.util.List<GroupApiKey> findByApplicationSecret(
		String applicationSecret, int start, int end);

	/**
	 * Returns an ordered range of all the group api keys where applicationSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationSecret the application secret
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group api keys
	 */
	public java.util.List<GroupApiKey> findByApplicationSecret(
		String applicationSecret, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator);

	/**
	 * Returns an ordered range of all the group api keys where applicationSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationSecret the application secret
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group api keys
	 */
	public java.util.List<GroupApiKey> findByApplicationSecret(
		String applicationSecret, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	public GroupApiKey findByApplicationSecret_First(
			String applicationSecret,
			com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
				orderByComparator)
		throws NoSuchGroupApiKeyException;

	/**
	 * Returns the first group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	public GroupApiKey fetchByApplicationSecret_First(
		String applicationSecret,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator);

	/**
	 * Returns the last group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	public GroupApiKey findByApplicationSecret_Last(
			String applicationSecret,
			com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
				orderByComparator)
		throws NoSuchGroupApiKeyException;

	/**
	 * Returns the last group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	public GroupApiKey fetchByApplicationSecret_Last(
		String applicationSecret,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator);

	/**
	 * Returns the group api keys before and after the current group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param groupApiKeyPK the primary key of the current group api key
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	public GroupApiKey[] findByApplicationSecret_PrevAndNext(
			GroupApiKeyPK groupApiKeyPK, String applicationSecret,
			com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
				orderByComparator)
		throws NoSuchGroupApiKeyException;

	/**
	 * Removes all the group api keys where applicationSecret = &#63; from the database.
	 *
	 * @param applicationSecret the application secret
	 */
	public void removeByApplicationSecret(String applicationSecret);

	/**
	 * Returns the number of group api keys where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @return the number of matching group api keys
	 */
	public int countByApplicationSecret(String applicationSecret);

	/**
	 * Caches the group api key in the entity cache if it is enabled.
	 *
	 * @param groupApiKey the group api key
	 */
	public void cacheResult(GroupApiKey groupApiKey);

	/**
	 * Caches the group api keys in the entity cache if it is enabled.
	 *
	 * @param groupApiKeys the group api keys
	 */
	public void cacheResult(java.util.List<GroupApiKey> groupApiKeys);

	/**
	 * Creates a new group api key with the primary key. Does not add the group api key to the database.
	 *
	 * @param groupApiKeyPK the primary key for the new group api key
	 * @return the new group api key
	 */
	public GroupApiKey create(GroupApiKeyPK groupApiKeyPK);

	/**
	 * Removes the group api key with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key that was removed
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	public GroupApiKey remove(GroupApiKeyPK groupApiKeyPK)
		throws NoSuchGroupApiKeyException;

	public GroupApiKey updateImpl(GroupApiKey groupApiKey);

	/**
	 * Returns the group api key with the primary key or throws a <code>NoSuchGroupApiKeyException</code> if it could not be found.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	public GroupApiKey findByPrimaryKey(GroupApiKeyPK groupApiKeyPK)
		throws NoSuchGroupApiKeyException;

	/**
	 * Returns the group api key with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key, or <code>null</code> if a group api key with the primary key could not be found
	 */
	public GroupApiKey fetchByPrimaryKey(GroupApiKeyPK groupApiKeyPK);

	/**
	 * Returns all the group api keys.
	 *
	 * @return the group api keys
	 */
	public java.util.List<GroupApiKey> findAll();

	/**
	 * Returns a range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of group api keys
	 */
	public java.util.List<GroupApiKey> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of group api keys
	 */
	public java.util.List<GroupApiKey> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator);

	/**
	 * Returns an ordered range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of group api keys
	 */
	public java.util.List<GroupApiKey> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupApiKey>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the group api keys from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of group api keys.
	 *
	 * @return the number of group api keys
	 */
	public int countAll();

	public Set<String> getCompoundPKColumnNames();

}