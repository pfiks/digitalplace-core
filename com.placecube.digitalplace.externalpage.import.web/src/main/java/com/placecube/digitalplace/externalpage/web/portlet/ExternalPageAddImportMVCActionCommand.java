package com.placecube.digitalplace.externalpage.web.portlet;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.background.task.service.BackgroundTaskLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.backgroundtask.ExternalPageImportBackgroundTaskExecutor;
import com.placecube.digitalplace.externalpage.web.constants.BackgroundTaskKeys;
import com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.externalpage.web.service.ExternalPageImportValidator;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET,
		"mvc.command.name=" + MVCCommandKeys.EXTERNAL_PAGE_IMPORT }, service = MVCActionCommand.class)
public class ExternalPageAddImportMVCActionCommand extends BaseMVCActionCommand {

	private static final Log LOG = LogFactoryUtil.getLog(ExternalPageAddImportMVCActionCommand.class);

	@Reference
	private BackgroundTaskLocalService backgroundTaskLocalService;

	@Reference
	private ExternalPageImportValidator externalPageImportValidator;

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		if (externalPageImportValidator.validate(actionRequest)) {
			String url = ParamUtil.getString(actionRequest, PortletRequestKeys.URL);
			Integer maxPagesToFetch = ParamUtil.getInteger(actionRequest, PortletRequestKeys.MAX_PAGES_TO_FETCH, 1);
			String xpath = ParamUtil.getString(actionRequest, PortletRequestKeys.XPATH);
			String indexArray[] = ParamUtil.getStringValues(actionRequest, PortletRequestKeys.INDEX_CONTENT);
			LOG.info("Starting crawler for url: " + url + " at: " + Calendar.getInstance().getTime());

			try {
				User user = themeDisplay.getUser();
				ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
				long companyId = themeDisplay.getCompanyId();
				long userId = themeDisplay.getUserId();
				long scopeGroupId = themeDisplay.getScopeGroupId();
				Map<String, Serializable> taskContextMap = new HashMap<>();
				taskContextMap.put(BackgroundTaskKeys.URL, url);
				taskContextMap.put(BackgroundTaskKeys.X_PATH, xpath);
				taskContextMap.put(BackgroundTaskKeys.USER_ID, userId);
				taskContextMap.put(BackgroundTaskKeys.COMPANY_ID, companyId);
				taskContextMap.put(BackgroundTaskKeys.SCOPE_GROUP_ID, scopeGroupId);
				taskContextMap.put(BackgroundTaskKeys.INDEX_ARRAY, indexArray[0]);
				taskContextMap.put(BackgroundTaskKeys.MAX_PAGES_TO_FETCH, maxPagesToFetch);
				taskContextMap.put(BackgroundTaskKeys.USER_EMAIL_ADDRESS, user.getEmailAddress());
				taskContextMap.put(BackgroundTaskKeys.USER_NAME, user.getFullName());
				taskContextMap.put(BackgroundTaskKeys.TOTAL_URL_COUNT, externalPageLocalService.getExternalPagesCount());

				LOG.info("Creating background task for external page import. UserId: " + userId + ", groupId: " + scopeGroupId + ", companyId: " + companyId + ", url: " + url + ", indexArray: "
						+ indexArray[0] + ", maxPages: " + maxPagesToFetch + ", xpath: " + xpath);

				backgroundTaskLocalService.addBackgroundTask(userId, scopeGroupId, BackgroundTaskKeys.TASK_NAME, ExternalPageImportBackgroundTaskExecutor.class.getName(), taskContextMap,
						serviceContext);

			} catch (Exception e) {
				LOG.error("Error starting instantiating crawler " + e.getMessage(), e);
				SessionErrors.add(actionRequest, "externalPageImportError");
			}
		}
	}

}
