package com.placecube.digitalplace.authentication.twofactor.web.service;

import com.liferay.portal.kernel.encryptor.Encryptor;
import com.liferay.portal.kernel.encryptor.EncryptorException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.security.auth.PrincipalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Accessor;
import com.liferay.portal.kernel.util.DigesterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorSessionKeys;

import java.security.Key;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.portlet.filter.ActionRequestWrapper;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@SuppressWarnings({ "unchecked", "deprecation" })
@Component(immediate = true, service = TwoFactorRequestUtils.class)
public class TwoFactorRequestUtils {

	private static final Accessor<Object, String> STRING_ACCESSOR = new Accessor<Object, String>() {

		@Override
		public String get(Object object) {
			return String.valueOf(object);
		}

		@Override
		public Class<String> getAttributeClass() {
			return String.class;
		}

		@Override
		public Class<Object> getTypeClass() {
			return Object.class;
		}

	};

	@Reference
	private Encryptor encryptor;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	public ActionRequest getActionRequest(HttpServletRequest httpServletRequest, ActionRequest actionRequest) throws EncryptorException, PrincipalException {
		String state = ParamUtil.getString(actionRequest, "state");
		if (Validator.isNull(state)) {
			return actionRequest;
		}
		HttpSession httpSession = httpServletRequest.getSession();

		String webDigest = (String) httpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_DIGEST);

		if (!StringUtil.equals(DigesterUtil.digest(state), webDigest)) {
			throw new PrincipalException("User sent unverified state");
		}

		Key webKey = (Key) httpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_WEB_KEY);

		Map<String, Object> stateMap = jsonFactory.looseDeserialize(encryptor.decrypt(webKey, state), Map.class);

		Map<String, Object> requestParameters = getRequestParametersMap(stateMap);

		return new ActionRequestWrapper(actionRequest) {

			@Override
			public String getParameter(String name) {
				return MapUtil.getString(requestParameters, name, null);
			}

			@SuppressWarnings("rawtypes")
			@Override
			public Map<String, String[]> getParameterMap() {
				return new HashMap(requestParameters);
			}

			@Override
			public Enumeration<String> getParameterNames() {
				return Collections.enumeration(requestParameters.keySet());
			}

			@Override
			public String[] getParameterValues(String name) {
				return (String[]) requestParameters.get(name);
			}

		};
	}

	public String getEncryptedStateMapJSON(ActionRequest actionRequest, Key key) throws EncryptorException {
		Map<String, Object> paramMap = new HashMap<>();

		Map<String, Object> paramMapWithoutRedirect = new HashMap<>(actionRequest.getParameterMap());
		paramMapWithoutRedirect.remove("redirect");

		paramMap.put("requestParameters", paramMapWithoutRedirect);

		String looseSerializeDeep = jsonFactory.looseSerializeDeep(paramMap);
		return encryptor.encrypt(key, looseSerializeDeep);
	}

	public HttpServletRequest getOriginalServletRequest(PortletRequest portletRequest) {
		return portal.getOriginalServletRequest(portal.getHttpServletRequest(portletRequest));
	}

	public long getPlid(HttpServletRequest httpServletRequest) {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		if (themeDisplay != null) {
			return themeDisplay.getPlid();
		}
		return 0;
	}

	public long getCompanyId(PortletRequest portletRequest) {
		return portal.getCompanyId(portletRequest);
	}

	public String getTwoFactorMVCCommandName(Optional<UserTwoFactorAuthentication> userTwoFactorAuthentication) {
		return userTwoFactorAuthentication.isPresent() && !userTwoFactorAuthentication.get().getResetKey() ? TwoFactorMVCCommandKeys.VERIFY : TwoFactorMVCCommandKeys.SETUP;
	}

	private Map<String, Object> getRequestParametersMap(Map<String, Object> stateMap) {
		Map<String, Object> requestParameters = (Map<String, Object>) stateMap.get("requestParameters");
		for (Map.Entry<String, Object> entry : requestParameters.entrySet()) {
			if (entry.getValue() instanceof List) {
				entry.setValue(ListUtil.toArray((List<?>) entry.getValue(), STRING_ACCESSOR));
			}
		}
		return requestParameters;
	}
}
