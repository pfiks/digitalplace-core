package com.placecube.digitalplace.search.shared.model;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.portlet.PortletPreferences;

import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.search.filter.RangeTermFilter;
import com.liferay.portal.kernel.theme.ThemeDisplay;

/**
 * Used to help in configuring the search context in a shared search request
 *
 */
public interface SharedSearchContributorSettings {

	void addBooleanClause(BooleanClause<Filter> booleanClause);

	void addBooleanQuery(BooleanClause<Query> queryToAdd);

	void addBooleanQuery(String field, String value, BooleanClauseOccur booleanClauseOccur);

	void addBooleanQueryForMultipleValues(String field, List<String> values, BooleanClauseOccur booleanClauseOccur);

	void addFacet(String facetFieldName, Optional<FacetConfiguration> facetConfiguration);

	void addRangeTermFilterClause(RangeTermFilter rangeTermFilter, BooleanClauseOccur booleanClauseOccur);

	void addSearchAttribute(String name, boolean value);

	void addSharedSearchSessionSingleValuedAttribute(String attributeName, String attributeValue);

	FacetConfiguration createFacetConfiguration(String facetFieldName);

	List<SharedActiveFilter> getActiveFiltersForField(String fieldName);

	long[] getAssetCategoryIds();

	Optional<PortletPreferences> getPortletPreferences();

	Optional<String> getSharedSearchSessionSingleValuedAttribute(String name);

	ThemeDisplay getThemeDisplay();

	void setAllowEmptySearch(boolean allowEmptySearch);

	void setAssetTagNames(Set<String> selectedTags);

	void setEntryClassNames(String... entryClassNames);

	void setSearchGroupIds(long[] groupIds);

	void setSearchKeywords(String keywords);

	void setSortBy(Sort... sortsToAdd);

}
