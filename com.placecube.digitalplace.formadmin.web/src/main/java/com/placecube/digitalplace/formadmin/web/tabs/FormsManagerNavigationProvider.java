package com.placecube.digitalplace.formadmin.web.tabs;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.portlet.PortletURL;

import org.osgi.service.component.annotations.Component;

import com.liferay.frontend.taglib.clay.servlet.taglib.util.NavigationItem;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.formadmin.web.constants.FormsManagerTabs;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;

@Component(immediate = true, service = FormsManagerNavigationProvider.class)
public class FormsManagerNavigationProvider {

	public List<NavigationItem> getViewNavigationItems(PortletURL portletURL, String currentTab, Locale locale) {

		return Arrays.asList(FormsManagerTabs.values()).stream().map(t -> getNavigationItem(portletURL, currentTab, locale, t.getNavigationKey())).collect(Collectors.toList());
	}

	private NavigationItem getNavigationItem(PortletURL portletURL, String currentTab, Locale locale, String itemKey) {
		NavigationItem tab = new NavigationItem();

		tab.setActive(Objects.equals(currentTab, itemKey));
		tab.setHref(portletURL, PortletRequestKeys.CURRENT_TAB, itemKey, "mvcRenderCommandName", "/" + itemKey);
		tab.setLabel(AggregatedResourceBundleUtil.get(itemKey, locale, "com.placecube.digitalplace.formadmin.web"));

		return tab;
	}
}