package com.placecube.digitalplace.userprofile.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsCompanyConfiguration", localization = "content/Language", name = "user-profile")
public interface UserProfileFieldsCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "address-enabled")
	boolean addressEnabled();

	@Meta.AD(required = false, deflt = "true", name = "address-required")
	boolean addressRequired();

	@Meta.AD(required = false, deflt = "false", name = "business-phone-number-enabled")
	boolean businessPhoneNumberEnabled();

	@Meta.AD(required = false, deflt = "false", name = "business-phone-number-required")
	boolean businessPhoneNumberRequired();

	@Meta.AD(required = false, deflt = "false", name = "date-of-birth-enabled")
	boolean dateOfBirthEnabled();

	@Meta.AD(required = false, deflt = "false", name = "date-of-birth-required")
	boolean dateOfBirthRequired();

	@Meta.AD(required = false, name = "expando-fields-settings", description = "expando-fields-settings-help")
	String[] expandoFieldsSettings();

	@Meta.AD(required = false, name = "email-address-generation-suffix", description = "email-address-generation-suffix-help")
	String generatedEmailSuffix();

	@Meta.AD(required = false, deflt = "false", name = "home-phone-number-enabled")
	boolean homePhoneNumberEnabled();

	@Meta.AD(required = false, deflt = "false", name = "home-phone-number-required")
	boolean homePhoneNumberRequired();

	@Meta.AD(required = false, deflt = "false", name = "job-title-enabled")
	boolean jobTitleEnabled();

	@Meta.AD(required = false, deflt = "false", name = "job-title-required")
	boolean jobTitleRequired();

	@Meta.AD(required = false, deflt = "false", name = "mobile-phone-number-enabled")
	boolean mobilePhoneNumberEnabled();

	@Meta.AD(required = false, deflt = "false", name = "mobile-phone-number-required")
	boolean mobilePhoneNumberRequired();

	@Meta.AD(required = false, deflt = "false", name = "two-factor-authentication-prompt-enabled")
	boolean twoFactorAuthenticationPromptEnabled();

}
