package com.placecube.digitalplace.pageimportadmin.constants;

public final class MVCCommandKeys {

	public static final String IMPORT_PAGES = "importPages";

	private MVCCommandKeys() {
		return;
	}
}