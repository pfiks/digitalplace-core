package com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.settings.definition.ConfigurationBeanDeclaration;

@Component(service = ConfigurationBeanDeclaration.class)
public class DDLSearchListingsPortletInstanceConfigurationBeanDeclaration implements ConfigurationBeanDeclaration {

	@Override
	public Class<?> getConfigurationBeanClass() {
		return DDLSearchListingsPortletInstanceConfiguration.class;
	}

}
