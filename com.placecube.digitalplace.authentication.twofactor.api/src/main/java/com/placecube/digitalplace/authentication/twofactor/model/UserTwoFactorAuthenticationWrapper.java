/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.authentication.twofactor.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UserTwoFactorAuthentication}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTwoFactorAuthentication
 * @generated
 */
public class UserTwoFactorAuthenticationWrapper
	extends BaseModelWrapper<UserTwoFactorAuthentication>
	implements ModelWrapper<UserTwoFactorAuthentication>,
			   UserTwoFactorAuthentication {

	public UserTwoFactorAuthenticationWrapper(
		UserTwoFactorAuthentication userTwoFactorAuthentication) {

		super(userTwoFactorAuthentication);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("secretKey", getSecretKey());
		attributes.put("resetKey", getResetKey());
		attributes.put("enabled", getEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String secretKey = (String)attributes.get("secretKey");

		if (secretKey != null) {
			setSecretKey(secretKey);
		}

		Boolean resetKey = (Boolean)attributes.get("resetKey");

		if (resetKey != null) {
			setResetKey(resetKey);
		}

		Boolean enabled = (Boolean)attributes.get("enabled");

		if (enabled != null) {
			setEnabled(enabled);
		}
	}

	@Override
	public UserTwoFactorAuthentication cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the company ID of this user two factor authentication.
	 *
	 * @return the company ID of this user two factor authentication
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this user two factor authentication.
	 *
	 * @return the create date of this user two factor authentication
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the enabled of this user two factor authentication.
	 *
	 * @return the enabled of this user two factor authentication
	 */
	@Override
	public Boolean getEnabled() {
		return model.getEnabled();
	}

	/**
	 * Returns the modified date of this user two factor authentication.
	 *
	 * @return the modified date of this user two factor authentication
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this user two factor authentication.
	 *
	 * @return the primary key of this user two factor authentication
	 */
	@Override
	public
		com.placecube.digitalplace.authentication.twofactor.service.persistence.
			UserTwoFactorAuthenticationPK getPrimaryKey() {

		return model.getPrimaryKey();
	}

	/**
	 * Returns the reset key of this user two factor authentication.
	 *
	 * @return the reset key of this user two factor authentication
	 */
	@Override
	public Boolean getResetKey() {
		return model.getResetKey();
	}

	/**
	 * Returns the secret key of this user two factor authentication.
	 *
	 * @return the secret key of this user two factor authentication
	 */
	@Override
	public String getSecretKey() {
		return model.getSecretKey();
	}

	/**
	 * Returns the user ID of this user two factor authentication.
	 *
	 * @return the user ID of this user two factor authentication
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user uuid of this user two factor authentication.
	 *
	 * @return the user uuid of this user two factor authentication
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this user two factor authentication.
	 *
	 * @return the uuid of this user two factor authentication
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this user two factor authentication.
	 *
	 * @param companyId the company ID of this user two factor authentication
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this user two factor authentication.
	 *
	 * @param createDate the create date of this user two factor authentication
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the enabled of this user two factor authentication.
	 *
	 * @param enabled the enabled of this user two factor authentication
	 */
	@Override
	public void setEnabled(Boolean enabled) {
		model.setEnabled(enabled);
	}

	/**
	 * Sets the modified date of this user two factor authentication.
	 *
	 * @param modifiedDate the modified date of this user two factor authentication
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this user two factor authentication.
	 *
	 * @param primaryKey the primary key of this user two factor authentication
	 */
	@Override
	public void setPrimaryKey(
		com.placecube.digitalplace.authentication.twofactor.service.persistence.
			UserTwoFactorAuthenticationPK primaryKey) {

		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the reset key of this user two factor authentication.
	 *
	 * @param resetKey the reset key of this user two factor authentication
	 */
	@Override
	public void setResetKey(Boolean resetKey) {
		model.setResetKey(resetKey);
	}

	/**
	 * Sets the secret key of this user two factor authentication.
	 *
	 * @param secretKey the secret key of this user two factor authentication
	 */
	@Override
	public void setSecretKey(String secretKey) {
		model.setSecretKey(secretKey);
	}

	/**
	 * Sets the user ID of this user two factor authentication.
	 *
	 * @param userId the user ID of this user two factor authentication
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user uuid of this user two factor authentication.
	 *
	 * @param userUuid the user uuid of this user two factor authentication
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this user two factor authentication.
	 *
	 * @param uuid the uuid of this user two factor authentication
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected UserTwoFactorAuthenticationWrapper wrap(
		UserTwoFactorAuthentication userTwoFactorAuthentication) {

		return new UserTwoFactorAuthenticationWrapper(
			userTwoFactorAuthentication);
	}

}