package com.placecube.digitalplace.payment.capitainterface.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.model.PaymentContext;

public class SimpleSaleConfiguration implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Pattern OVERRIDE_SETTING_PATTERN = Pattern.compile("(simpleSale.items\\[[0-9]\\]\\.)+");
	private int totalSaleAmountInMinorUnits;
	private Map<String, SimpleSaleItemConfiguration> simpleSaleItemConfigurationTracker = new HashMap<>();
	private PaymentContext paymentContext;
	private List<SimpleSaleItemConfiguration> simpleSaleItemConfigurations;
	private boolean useAdvancedConfiguration;

	private SimpleSaleConfiguration(PaymentContext paymentContext, int totalSaleAmountInMinorUnits, boolean useAdvancedConfiguration) {
		this.paymentContext = paymentContext;
		this.totalSaleAmountInMinorUnits = totalSaleAmountInMinorUnits;
		this.useAdvancedConfiguration = useAdvancedConfiguration;
	}

	public static SimpleSaleConfiguration newSimpleSaleConfiguration(PaymentContext paymentContext, int amountInMinorUnits, boolean useAdvancedConfiguration) {
		return new SimpleSaleConfiguration(paymentContext, amountInMinorUnits, useAdvancedConfiguration);
	}

	public List<SimpleSaleItemConfiguration> getSimpleSaleItemConfigurations() {
		if (Validator.isNull(simpleSaleItemConfigurations)) {
			if (useAdvancedConfiguration) {
				simpleSaleItemConfigurations = getOverrideConfigSimpleSaleItemConfigurations(paymentContext.getAdvancedConfiguration());
			} else {
				simpleSaleItemConfigurations = getStandardConfigSimpleSaleItemConfigurations();
			}
		}

		return simpleSaleItemConfigurations;

	}

	public PaymentContext getPaymentContext() {
		return paymentContext;
	}

	public String toString() {
		return "OneOffPaymentOrder [totalSaleAmountInMinorUnits=" + totalSaleAmountInMinorUnits + ", paymentContext=" + paymentContext + ", simpleSaleItemConfigurationTracker="
				+ simpleSaleItemConfigurationTracker + ", simpleSaleItemConfigurations=" + simpleSaleItemConfigurations + "]";
	}

	private void addArraySetting(Map.Entry<String, String> entry) {
		String settingKey = entry.getKey();
		Matcher matcher = OVERRIDE_SETTING_PATTERN.matcher(settingKey);
		if (matcher.find()) {
			String itemId = matcher.group().replaceAll("[^0-9]", StringPool.BLANK);
			String itemSetting = settingKey.replace(matcher.group(), StringPool.BLANK);
			SimpleSaleItemConfiguration simpleSaleItem = getSimpleSaleItemForArrayId(itemId, simpleSaleItemConfigurationTracker);
			simpleSaleItem.putSaleItemDetail(itemSetting, entry.getValue());
		}
	}

	private List<SimpleSaleItemConfiguration> getOverrideConfigSimpleSaleItemConfigurations(Map<String, String> jobItemConfiguration) {

		List<SimpleSaleItemConfiguration> simpleSaleItems = new ArrayList<>();
		for (Map.Entry<String, String> advancedSettingEntry : jobItemConfiguration.entrySet()) {
			addArraySetting(advancedSettingEntry);
		}

		simpleSaleItems.addAll(simpleSaleItemConfigurationTracker.values());
		return simpleSaleItems;
	}

	private SimpleSaleItemConfiguration getSimpleSaleItemForArrayId(String itemId, Map<String, SimpleSaleItemConfiguration> jobItemsArrayTracker) {
		return jobItemsArrayTracker.computeIfAbsent(itemId, simpleSaleItem -> {
			SimpleSaleItemConfiguration initialSimpleSaleItemConfiguration = SimpleSaleItemConfiguration.newSimpleSaleItemConfiguration();
			initialSimpleSaleItemConfiguration.putSaleItemDetail("adminEmailAddresses", StringPool.BLANK);
			return initialSimpleSaleItemConfiguration;
		});
	}

	private List<SimpleSaleItemConfiguration> getStandardConfigSimpleSaleItemConfigurations() {
		List<SimpleSaleItemConfiguration> simpleSaleItems = new ArrayList<>();
		SimpleSaleItemConfiguration simpleSaleItem = SimpleSaleItemConfiguration.newSimpleSaleItemConfiguration();

		simpleSaleItem.putSaleItemDetail(CapitaConstants.SALE_ITEM_AMOUNT_IN_MINOR_UNITS, String.valueOf(totalSaleAmountInMinorUnits));
		simpleSaleItem.putSaleItemDetail(CapitaConstants.SALE_ITEM_DESCRIPTION, GetterUtil.getString(paymentContext.getSalesDescription()));
		simpleSaleItem.putSaleItemDetail(CapitaConstants.SALE_ITEM_REFERENCE, GetterUtil.getString(paymentContext.getItemReference()));
		simpleSaleItem.putSaleItemDetail(CapitaConstants.SALE_ITEM_FUND_CODE, paymentContext.getAccountId());
		simpleSaleItem.putSaleItemDetail("adminEmailAddresses", StringPool.BLANK);
		simpleSaleItems.add(simpleSaleItem);

		return simpleSaleItems;
	}

}
