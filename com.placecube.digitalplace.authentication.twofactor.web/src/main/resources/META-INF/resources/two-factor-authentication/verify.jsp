<%@ include file="init.jsp" %>

<div class="two-factor-authenticator two-factor-authenticator-verify">

	<portlet:actionURL name="<%=TwoFactorMVCCommandKeys.VERIFY %>" var="verifyURL">
		<portlet:param name="mvcRenderCommandName" value="<%=TwoFactorMVCCommandKeys.VERIFY %>" />
		<portlet:param name="saveLastPath" value="false" />
		<portlet:param name="redirect" value="${redirect}" />
	</portlet:actionURL>

	<portlet:renderURL var="userResetTwoFactorURL">
		<portlet:param name="mvcRenderCommandName" value="<%= TwoFactorMVCCommandKeys.USER_RESET %>" />
		<portlet:param name="redirect" value="${redirect}" />
	</portlet:renderURL>

	<aui:form action="${verifyURL}" cssClass="container-fluid container-fluid-max-xl sign-in-form" data-senna-off="true" method="post" name="verifyTwoFactorAuthenticationForm">

		<liferay-journal:journal-article
			articleId="<%=WebContentArticles.TWO_FACTOR_VERIFY_CODE.getArticleId() %>"
			groupId="${themeDisplay.getCompanyGroupId()}"
			showTitle="false" />

		<liferay-ui:error key="twoFactorVerificationFailed" message="two-factor-authentication-has-failed" />

		<aui:input autocomplete="off" label="authenticator-app-code" name="twoFactorAuthCode" required="true" />

		<aui:button-row>
			<aui:button type="submit" value="verify-code" />
		</aui:button-row>

		<c:if test="${resetButtonEnabled}">
			<a href="${userResetTwoFactorURL}"><liferay-ui:message key="reset" /></a>
		</c:if>
		
	</aui:form>

</div>