package com.placecube.digitalplace.common.internal.utils;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;

import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.resource.bundle.AggregateResourceBundleLoader;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoader;
import com.liferay.portal.kernel.resource.bundle.ResourceBundleLoaderUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ResourceBundleLoaderUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.resource.bundle.ResourceBundleLoaderUtil" })
public class DigitalPlaceResourceBundleUtilTest extends PowerMockito {

	@InjectMocks
	private DigitalPlaceResourceBundleUtil digitalPlaceResourceBundleUtil;

	@Mock
	private ResourceBundle mockAggregatedResourceBundle;

	@Mock
	private AggregateResourceBundleLoader mockAggregateResourceBundleLoader;

	@Mock
	private ResourceBundleLoader mockPortalResourceBundleLoader;

	@Mock
	private ResourceBundleLoader mockResourceBundleLoader1;

	@Mock
	private ResourceBundleLoader mockResourceBundleLoader2;

	@Before
	public void activateSetup() {
		mockStatic(ResourceBundleLoaderUtil.class);
	}

	@Test
	public void getAggregatedResourceBundle_WhenNoError_ThenReturnsTheAggregatedResourceBundleForAllTheBundleSymbolicNamesSpecified() throws Exception {
		String[] symbolicNames = new String[] { "name1", "name2" };
		Locale locale = Locale.CANADA_FRENCH;
		when(ResourceBundleLoaderUtil.getPortalResourceBundleLoader()).thenReturn(mockPortalResourceBundleLoader);
		when(ResourceBundleLoaderUtil.getResourceBundleLoaderByBundleSymbolicName("name1")).thenReturn(mockResourceBundleLoader1);
		when(ResourceBundleLoaderUtil.getResourceBundleLoaderByBundleSymbolicName("name2")).thenReturn(mockResourceBundleLoader2);
		when(mockResourceBundleLoader1.loadResourceBundle(locale)).thenReturn(mockAggregatedResourceBundle);
		whenNew(AggregateResourceBundleLoader.class).withArguments(eq(new ResourceBundleLoader[] { mockPortalResourceBundleLoader, mockResourceBundleLoader1, mockResourceBundleLoader2 }))
				.thenReturn(mockAggregateResourceBundleLoader);
		when(mockAggregateResourceBundleLoader.loadResourceBundle(locale)).thenReturn(mockAggregatedResourceBundle);

		ResourceBundle result = DigitalPlaceResourceBundleUtil.getAggregatedResourceBundle(locale, symbolicNames);

		assertThat(result, sameInstance(mockAggregatedResourceBundle));
	}

}
