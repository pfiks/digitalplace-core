package com.placecube.digitalplace.override.struts.portal.action;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.override.struts.service.EmailVerificationService;

import java.io.IOException;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SessionMessages.class, SessionErrors.class, PropsUtil.class, ParamUtil.class, PortletURLFactoryUtil.class })
public class VerifyEmailOverrideActionTest {

	@InjectMocks
	private VerifyEmailOverrideAction verifyEmailOverrideAction;

	@Mock
	private EmailVerificationService mockEmailVerificationService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private LiferayPortletURL mockLiferayPortletURL;

	@Mock
	private Portal mockPortal;

	@Before
	public void setUp() {
		mockStatic(SessionMessages.class, SessionErrors.class, PropsUtil.class, ParamUtil.class, PortletURLFactoryUtil.class);
	}

	@Test
	public void execute_WhenCmdCommandIsEmpty_ThenReturnsVerifyEmailAddressPage() throws Exception {

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn(StringPool.BLANK);

		String result = verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		assertThat(result, equalTo("portal.verify_email_address"));

	}

	@Test
	public void execute_WhenUserIsSignedInAndCmdCommandIsSend_ThenReturnsVerifyEmailAddressPage() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn(Constants.SEND);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		String result = verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		assertThat(result, equalTo("portal.verify_email_address"));
	}

	@Test
	public void execute_WhenUserIsSignedInAndCmdCommandIsSend_ThenSendsVerificationEmail() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn(Constants.SEND);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockEmailVerificationService, times(1)).sendEmailAddressVerification(mockHttpServletRequest, mockThemeDisplay);
	}

	@Test
	public void execute_WhenUserIsSignedInAndCmdCommandIsSend_ThenAddsSuccessMessage() throws Exception {
		final String message = "message";

		when(ParamUtil.getString(mockHttpServletRequest, "successMessage")).thenReturn(message);
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn(Constants.SEND);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		verifyStatic(SessionMessages.class);

		SessionMessages.add(mockHttpServletRequest, "requestProcessed", message);
	}

	@Test(expected = PortalException.class)
	public void execute_WhenVerificationEmailDeliveryFails_ThenThrowsPortalException() throws Exception {

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn(Constants.SEND);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		doThrow(new PortalException()).when(mockEmailVerificationService).sendEmailAddressVerification(mockHttpServletRequest, mockThemeDisplay);

		verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test
	public void execute_WhenCmdCommandExistsAndUserIsNotSignedIn_ThenVerifiesEmailAddress() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn("verify");
		when(mockThemeDisplay.isSignedIn()).thenReturn(false);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, PortletKeys.LOGIN, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);

		verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockEmailVerificationService, times(1)).verifyEmailAddress(mockHttpServletRequest);
	}

	@Test
	public void execute_WhenCmdCommandExistsAndUserIsNotSignedIn_ThenRedirectsToLoginPortletUrl() throws Exception {
		final String url = "/login";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn("verify");
		when(mockThemeDisplay.isSignedIn()).thenReturn(false);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, PortletKeys.LOGIN, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.toString()).thenReturn(url);

		verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletResponse, times(1)).sendRedirect(url);
	}

	@Test
	public void execute_WhenCmdCommandExistsAndUserIsNotSignedIn_ThenReturnsNull() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn("verify");
		when(mockThemeDisplay.isSignedIn()).thenReturn(false);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, PortletKeys.LOGIN, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);

		String result = verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		assertThat(result, nullValue());
	}

	@Test
	public void execute_WhenCmdCommandIsNotSendAndUserIsSignedIn_ThenVerifiesEmailAddress() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn("verify");
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockEmailVerificationService, times(1)).verifyEmailAddress(mockHttpServletRequest);
	}

	@Test
	public void execute_WhenCmdCommandIsNotSendAndUserIsSignedIn_ThenRetursnCommonRefererPage() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn("verify");
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		String result = verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		assertThat(result, equalTo("/common/referer_jsp.jsp"));
	}

	@Test
	public void execute_WhenEmailVerificationFails_ThenAddsSessionErrorAndReturnsToVerifyEmailAddressPage() throws Exception {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn("verify");
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);

		doThrow(new PortalException()).when(mockEmailVerificationService).verifyEmailAddress(mockHttpServletRequest);

		String result = verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		assertThat(result, equalTo("portal.verify_email_address"));

		verifyStatic(SessionErrors.class);

		SessionErrors.add(mockHttpServletRequest, PortalException.class);
	}

	@Test
	public void execute_WhenRedirectionToLoginPortletFails_ThenSendsErrorToPortalLevelAndReturnsNull() throws Exception {
		final String url = "/login";
		final IOException e = new IOException();

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(ParamUtil.getString(mockHttpServletRequest, Constants.CMD)).thenReturn("verify");
		when(mockThemeDisplay.isSignedIn()).thenReturn(false);
		when(PortletURLFactoryUtil.create(mockHttpServletRequest, PortletKeys.LOGIN, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.toString()).thenReturn(url);
		doThrow(e).when(mockHttpServletResponse).sendRedirect(url);

		String result = verifyEmailOverrideAction.execute(mockHttpServletRequest, mockHttpServletResponse);

		assertThat(result, nullValue());

		verify(mockPortal, times(1)).sendError(e, mockHttpServletRequest, mockHttpServletResponse);
	}
}