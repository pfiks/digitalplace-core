/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.group.rest.service.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.group.rest.constants.GroupRestKeyType;
import com.placecube.digitalplace.group.rest.exception.GroupRestKeyException;
import com.placecube.digitalplace.group.rest.model.GroupApiKey;
import com.placecube.digitalplace.group.rest.service.base.GroupApiKeyLocalServiceBaseImpl;
import com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK;
import com.placecube.digitalplace.group.rest.service.util.APIKeyUtil;
import com.placecube.digitalplace.group.rest.service.util.GroupApiKeyPKUtil;

import java.security.Key;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * The implementation of the group api key local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GroupApiKeyLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.group.rest.model.GroupApiKey", service = AopService.class)
public class GroupApiKeyLocalServiceImpl extends GroupApiKeyLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.group.rest.service.
	 * GroupApiKeyLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.group.rest.service.
	 * GroupApiKeyLocalServiceUtil</code>.
	 */

	@Reference
	private APIKeyUtil apiKeyUtil;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private GroupApiKeyPKUtil groupApiKeyPKUtil;

	@Override
	public void regenerateGroupApiKey(Group group) throws GroupRestKeyException {
		GroupApiKeyPK groupApiKeyPK = groupApiKeyPKUtil.createGroupApiKeyPK(group.getCompanyId(), group.getGroupId());
		GroupApiKey groupApiKey = fetchGroupApiKey(groupApiKeyPK);

		if (Validator.isNull(groupApiKey)) {
			groupApiKey = createGroupApiKey(groupApiKeyPK);
		}

		String apiKey = generateKey(GroupRestKeyType.APPLICATION_KEY);
		String apiSecret = generateKey(GroupRestKeyType.APPLICATION_SECRET);
		groupApiKey.setApplicationKey(apiKey);
		groupApiKey.setApplicationSecret(apiSecret);

		if (isValidGroup(group) && !apiKey.isEmpty() && !apiSecret.isEmpty()) {
			updateGroupApiKey(groupApiKey);
		}
	}

	@Override
	public String getGroupApplicationKey(long companyId, long groupId) {
		GroupApiKey groupApiKey = fetchGroupApiKey(groupApiKeyPKUtil.createGroupApiKeyPK(companyId, groupId));
		return Validator.isNull(groupApiKey) ? StringPool.BLANK : groupApiKey.getApplicationKey();
	}

	@Override
	public String getGroupApplicationSecret(long companyId, long groupId) {
		GroupApiKey groupApiKey = fetchGroupApiKey(groupApiKeyPKUtil.createGroupApiKeyPK(companyId, groupId));
		return Validator.isNull(groupApiKey) ? StringPool.BLANK : groupApiKey.getApplicationSecret();
	}

	@Override
	public Group getGroupByKey(GroupRestKeyType keyType, String keyValue) throws GroupRestKeyException {
		if (Validator.isNotNull(keyValue)) {
			List<Group> groups = getGroupsByKey(keyType, keyValue);
			if (groups.size() == 1) {
				return groups.get(0);
			}
		}
		throw new GroupRestKeyException("Unable to find single group by key. KeyType: " + keyType.getValue() + ", keyValue: " + keyValue);
	}

	@Override
	public String getEncodedTextWithKey(String applicationKey, String textToEncode) throws GroupRestKeyException {
		try {
			Group group = getGroupByKey(GroupRestKeyType.APPLICATION_KEY, applicationKey);
			String applicationSecret = getGroupRestKeyValue(group, GroupRestKeyType.APPLICATION_SECRET);
			Key networkKey = apiKeyUtil.getKey(applicationSecret);

			Mac mac = apiKeyUtil.getMacForKey(networkKey);
			byte[] bytes = apiKeyUtil.getBytes(textToEncode);
			return apiKeyUtil.getEncodedText(mac, bytes);
		} catch (Exception e) {
			throw new GroupRestKeyException(e);
		}
	}

	@Override
	public String getEncodedTextWithGroupKey(Group group, String textToEncode) throws GroupRestKeyException {
		try {
			String applicationSecret = getGroupRestKeyValue(group, GroupRestKeyType.APPLICATION_SECRET);
			Key key = apiKeyUtil.getKey(applicationSecret);

			Mac mac = apiKeyUtil.getMacForKey(key);
			byte[] bytes = apiKeyUtil.getBytes(textToEncode);
			return apiKeyUtil.getEncodedText(mac, bytes);
		} catch (Exception e) {
			throw new GroupRestKeyException(e);
		}
	}

	private boolean isValidGroup(Group group) {
		return Validator.isNotNull(group) && !group.isCompany() && !group.isGuest() && group.isSite();
	}

	@Override
	public String generateKey(GroupRestKeyType keyType) throws GroupRestKeyException {
		try {
			KeyGenerator keyGenerator = apiKeyUtil.getKeyGenerator();
			String generatedKey = generateKey(keyGenerator);
			boolean isKeyUnique = isKeyUnique(keyType, generatedKey);
			while (!isKeyUnique) {
				generatedKey = generateKey(keyGenerator);
				isKeyUnique = isKeyUnique(keyType, generatedKey);
			}
			return generatedKey;
		} catch (Exception e) {
			throw new GroupRestKeyException(e);
		}
	}

	protected boolean isKeyUnique(GroupRestKeyType keyType, String value) {
		if (keyType.getValue().equals(GroupRestKeyType.APPLICATION_KEY.getValue())) {
			return groupApiKeyPersistence.countByApplicationKey(value) == 0;
		} else {
			return groupApiKeyPersistence.countByApplicationSecret(value) == 0;
		}
	}

	protected List<Group> getGroupsByKey(GroupRestKeyType keyType, String keyValue) {
		List<GroupApiKey> apiKeys;

		if (keyType.getValue().equals(GroupRestKeyType.APPLICATION_KEY.getValue())) {
			apiKeys = groupApiKeyPersistence.findByApplicationKey(keyValue);
		} else {
			apiKeys = groupApiKeyPersistence.findByApplicationSecret(keyValue);
		}

		return apiKeys.stream().map(apiKey -> groupLocalService.fetchGroup(apiKey.getGroupId())).filter(Objects::nonNull).collect(Collectors.toList());
	}

	@Override
	public String getGroupRestKeyValue(Group group, GroupRestKeyType keyType) {
		long groupId = group.getGroupId();
		GroupApiKey groupApiKey = groupApiKeyPersistence.fetchByPrimaryKey(groupId);

		if (Validator.isNotNull(groupApiKey)) {
			if (keyType.getValue().equals(GroupRestKeyType.APPLICATION_KEY.getValue())) {
				return groupApiKey.getApplicationKey();
			} else {
				return groupApiKey.getApplicationSecret();
			}
		}

		return StringPool.BLANK;
	}

	private String generateKey(KeyGenerator keyGenerator) {
		SecretKey secretKey = apiKeyUtil.generateSecretKey(keyGenerator);
		return apiKeyUtil.getEncodedSecretKey(secretKey);
	}
}