package com.placecube.digitalplace.userimport.admin.web.constants;

public enum UserProfileFields {

	EMAIL_ADDRESS("emailAddress", "email-address", true),

	USER_PASSWORD("userPassword", "password", false),

	FIRST_NAME("firstName", "first-name", true),

	LAST_NAME("lastName", "last-name", true),

	JOB_TITLE("jobTitle", "job-title", false),

	EMAIL_ADDRESS_VERIFIED("emailAddressVerified", "email-address-verified", false),

	CREATE_DATE("createDate", "create-date", false),
	
	BIRTHDAY("birthday", "birthday", false),

	ADDRESS_STREET_1("addressStreet1", "street-1", false),

	ADDRESS_STREET_2("addressStreet2", "street-2", false),

	ADDRESS_STREET_3("addressStreet3", "street-3", false),

	CITY("city", "city", false),

	POSTCODE("postcode", "post-code", false),

	PHONE_PERSONAL("personal", "personal-phone", false),

	PHONE_BUSINESS("business", "business-phone", false),

	PHONE_MOBILE("mobile", "mobile-phone", false);

	private final String label;

	private final String name;

	private final boolean required;

	private UserProfileFields(String name, String label, boolean required) {
		this.name = name;
		this.label = label;
		this.required = required;
	}

	public final String getLabel() {
		return label;
	}

	public final String getName() {
		return name;
	}

	public final boolean isRequired() {
		return required;
	}

}
