package com.placecube.digitalplace.search.web.portlet.contenttype.contributor;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.CONTENT_TYPE_FACET, service = SharedSearchContributor.class)
public class ContentTypeFacetPortletSharedSearchContributor implements SharedSearchContributor {

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {
		addFacet(sharedSearchContributorSettings);

		addSelectedValues(sharedSearchContributorSettings);
	}

	private void addFacet(SharedSearchContributorSettings sharedSearchContributorSettings) {
		String facetFieldName = SearchFacetConstants.CONTENT_TYPE;

		FacetConfiguration facetConfiguration = sharedSearchContributorSettings.createFacetConfiguration(facetFieldName);
		facetConfiguration.setOrder("OrderHitsDesc");
		facetConfiguration.setStatic(false);
		facetConfiguration.setWeight(1.4d);

		JSONObject data = facetConfiguration.getData();
		data.put("maxTerms", 100);
		data.put("frequencyThreshold", 1);

		sharedSearchContributorSettings.addFacet(facetFieldName, Optional.of(facetConfiguration));
	}

	private void addSelectedValues(SharedSearchContributorSettings sharedSearchContributorSettings) {
		List<SharedActiveFilter> activeFilters = sharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_CONTENT_TYPES);
		List<String> selectedContentTypes = activeFilters.stream().map(filter -> Arrays.asList(filter.getValue().split(StringPool.COMMA))).flatMap(List::stream).collect(Collectors.toList());
		sharedSearchContributorSettings.addBooleanQueryForMultipleValues(SearchFacetConstants.CONTENT_TYPE, selectedContentTypes, BooleanClauseOccur.MUST);
	}

}