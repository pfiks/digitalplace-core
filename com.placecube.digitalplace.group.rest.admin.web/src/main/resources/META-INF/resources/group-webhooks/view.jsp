<%@ include file="../init.jsp" %>

<div class="container-fluid container-fluid-max-xl">
	<div class="row">
		<div class="col-md-12">

			<div class="text-right">

				<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle" type="button" id="webhooksDropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<liferay-ui:message key="add" /> +
					</button>

					<div class="dropdown-menu" aria-labelledby="webhooksDropdownMenuButton">
						<c:forEach items="${webhookEvents}" var="webhookEvent">

							<portlet:renderURL var="addWebhookURL">
								<<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE %>" />
								<portlet:param name="webhookClassName" value="${webhookEvent.getClassName()}" />
							</portlet:renderURL>

							<a class="dropdown-item" href="${addWebhookURL}"><liferay-ui:message key="${webhookEvent.getLabel()}" /></a>

						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<liferay-ui:search-container searchContainer="${ searchContainer }"
				total="${ searchContainer.total }"
				compactEmptyResultsMessage="true"
				emptyResultsMessage="${ searchContainer.emptyResultsMessage }"
				emptyResultsMessageCssClass="${ searchContainer.emptyResultsMessageCssClass }">

				<liferay-ui:search-container-results results="${ searchContainer.results }" />

				<liferay-ui:search-container-row className="com.placecube.digitalplace.group.rest.model.GroupWebhook" keyProperty="webhookId" modelVar="webhook">
					<liferay-ui:search-container-column-text title="name" align="left" colspan="8">
						${webhook.name}
					</liferay-ui:search-container-column-text>

					<liferay-ui:search-container-column-text title="status" colspan="2">
						<liferay-ui:message key="${webhook.enabled ? 'enabled' : 'disabled'}" />
					</liferay-ui:search-container-column-text>

					<liferay-ui:search-container-column-text title="actions" align="right" colspan="2">

						<liferay-ui:icon-menu direction="left-side" markupView="lexicon">
							<portlet:renderURL var="editWebhookURL">
								<portlet:param name="webhookId" value="${webhook.webhookId }" />
								<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE %>" />
							</portlet:renderURL>

							<liferay-ui:icon label="true" message="edit" url="${ editWebhookURL }" />

							<portlet:actionURL name="<%= MVCCommandKeys.REMOVE %>" var="removeWebhookActionURL">
								<portlet:param name="webhookId" value="${ webhook.webhookId }" />
								<portlet:param name="mvcRenderCommandName" value="${ currentPage }" />
							</portlet:actionURL>

							<c:set var="confirmRemoveWebhook">
								<liferay-ui:message key="remove-webhook-confirmation" arguments="${ webhook.name }" />
							</c:set>
							<liferay-ui:icon-delete url="${ removeWebhookActionURL }" message="remove" confirmation="${ confirmRemoveWebhook }" />
						</liferay-ui:icon-menu>

					</liferay-ui:search-container-column-text>

				</liferay-ui:search-container-row>

				<liferay-ui:search-iterator displayStyle="descriptive" markupView="lexicon" searchContainer="${ searchContainer }" />

			</liferay-ui:search-container>
		</div>
	</div>
</div>