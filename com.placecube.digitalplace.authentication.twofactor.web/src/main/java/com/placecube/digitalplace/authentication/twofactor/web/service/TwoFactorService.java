package com.placecube.digitalplace.authentication.twofactor.web.service;

import java.util.Objects;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorAuditMessage;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorSessionKeys;

@Component(immediate = true, service = TwoFactorService.class)
public class TwoFactorService {

	private static final Log LOG = LogFactoryUtil.getLog(TwoFactorService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private TwoFactorAuditMessageService twoFactorAuditMessageService;

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private UserTwoFactorAuthenticationLocalService userTwoFactorAuthenticationLocalService;

	public void configureTwoFactorAuthenticationForUser(User user, String secretKey) {
		if (Validator.isNotNull(secretKey)) {
			userTwoFactorAuthenticationLocalService.configureTwoFactorAuthenticationForUser(user, secretKey);
		}
	}

	public Optional<AuthenticationTwoFactorCompanyConfiguration> getEnabledConfiguration(long companyId) {
		try {
			AuthenticationTwoFactorCompanyConfiguration companyConfiguration = configurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, companyId);
			if (companyConfiguration.enabled()) {
				return Optional.of(companyConfiguration);
			}
		} catch (ConfigurationException e) {
			LOG.error(e);
		}
		return Optional.empty();
	}

	public String getQRBarCodeURL(User user, String secretKey) throws PortletException {
		try {
			return userTwoFactorAuthenticationLocalService.getQRBarCodeURL(user.getCompanyId(), user.getEmailAddress(), secretKey);
		} catch (PortalException e) {
			throw new PortletException(e);
		}
	}

	public String getSecretKey(PortletRequest portletRequest) {
		String secretKey = GetterUtil.getString(portletRequest.getAttribute("twoFactorSecretKey"));
		return Validator.isNull(secretKey) ? userTwoFactorAuthenticationLocalService.generateSecretKey() : secretKey;
	}

	public Optional<User> getTwoFactorUser(PortletRequest portletRequest, HttpServletRequest httpServletRequest) {
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		User user;
		if (themeDisplay.isSignedIn()) {
			user = themeDisplay.getUser();
		} else {
			HttpSession httpSession = httpServletRequest.getSession();
			long userId = GetterUtil.getLong(httpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_USER_ID));
			user = userLocalService.fetchUser(userId);
		}

		return Optional.ofNullable(user);
	}

	public Optional<UserTwoFactorAuthentication> getUserTwoFactorAuthentication(long userId) {
		User user = userLocalService.fetchUser(userId);
		if (Validator.isNotNull(user)) {
			return userTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(user);
		}
		return Optional.empty();
	}

	public boolean isTwoFactorAlreadyVerified(HttpServletRequest httpServletRequest, User user, long userId) {
		if (user == null) {
			LOG.warn("Two factor user not found for userId: " + userId);

			twoFactorAuditMessageService.addAuditMessage(userId, TwoFactorAuditMessage.USER_NOT_FOUND);

			return false;
		}

		HttpSession httpSession = httpServletRequest.getSession(false);
		if (httpSession == null) {
			twoFactorAuditMessageService.addAuditMessage(user, TwoFactorAuditMessage.INVALID_SESSION);

			return false;
		}

		Object validatedUserId = httpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_VALIDATED_USER_ID);

		if (validatedUserId == null) {

			twoFactorAuditMessageService.addAuditMessage(user, TwoFactorAuditMessage.USER_NOT_YET_VERIFIED);

			return false;
		}

		if (!Objects.equals(validatedUserId, userId)) {
			twoFactorAuditMessageService.addAuditMessage(user, TwoFactorAuditMessage.USER_NOT_THE_SAME);

			return false;
		}

		return true;
	}

	public boolean isValidTwoFactorCode(User user, String twoFactorAuthCode, HttpServletRequest httpServletRequest) throws PortalException {
		if (Validator.isNull(twoFactorAuthCode)) {
			return false;
		}

		if (userTwoFactorAuthenticationLocalService.isValidTwoFactorAuthentication(user, twoFactorAuthCode)) {
			HttpSession httpSession = httpServletRequest.getSession();
			httpSession.setAttribute(TwoFactorSessionKeys.TWO_FACTOR_VALIDATED_USER_ID, user.getUserId());
			return true;

		} else {
			twoFactorAuditMessageService.addAuditMessage(user, TwoFactorAuditMessage.INVALID_AUTH_CODE);
			return false;
		}

	}

	public boolean isTwoFactorAuthenticationEnforcedOrEnabled(User user, long userId) {
		if (user == null) {
			return false;
		}
		Optional<UserTwoFactorAuthentication> userTwoFactorAuthenticationOptional = userTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(user);

		boolean enforced = userTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(user);

		return enforced || (userTwoFactorAuthenticationOptional.isPresent() && userTwoFactorAuthenticationOptional.get().getEnabled());
	}

	public boolean verifyAndEnableTwoFactorAuthentication(User user, String authenticationCode, String secretKey) throws PortalException {
		return userTwoFactorAuthenticationLocalService.verifyAndEnableUserTwoFactorAuthentication(user, authenticationCode, secretKey);
	}

	public User setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail(User user, ActionRequest actionRequest) throws PortalException {
		user.setEmailAddressVerified(false);
		user = userLocalService.updateUser(user);
		userLocalService.sendEmailAddressVerification(user, user.getEmailAddress(), ServiceContextFactory.getInstance(actionRequest));
		return user;
	}

	public void setTwoFactorValidatedUserIdInHttpSession(HttpServletRequest httpServletRequest, User user) {
		HttpSession httpSession = httpServletRequest.getSession();
		httpSession.setAttribute(TwoFactorSessionKeys.TWO_FACTOR_VALIDATED_USER_ID, user.getUserId());
	}

}
