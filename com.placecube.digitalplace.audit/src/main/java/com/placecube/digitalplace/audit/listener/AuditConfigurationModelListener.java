package com.placecube.digitalplace.audit.listener;

import java.util.Dictionary;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.persistence.listener.ConfigurationModelListener;
import com.liferay.portal.security.audit.event.generators.constants.EventTypes;
import com.placecube.digitalplace.audit.service.AuditService;

@Component(property = "model.class.name=*", service = ConfigurationModelListener.class)
public class AuditConfigurationModelListener implements ConfigurationModelListener {

	@Reference
	private AuditService auditService;

	@Override
	public void onAfterDelete(String pid) {
		auditService.addAuditLogEntry(ConfigurationModelListener.class.getName(), 0l, EventTypes.DELETE, auditService.getConfigurationAuditInfo(pid));
	}

	@Override
	public void onAfterSave(String pid, Dictionary<String, Object> properties) {
		auditService.addAuditLogEntry(ConfigurationModelListener.class.getName(), 0l, EventTypes.UPDATE, auditService.getConfigurationAuditInfo(pid, properties));
	}

}
