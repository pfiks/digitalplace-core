package com.placecube.digitalplace.externalpage.web.constants;

public final class ViewKeys {

	public static final String ADD = "/external-page-import/create_external_page.jsp";

	public static final String VIEW = "/external-page-import/view.jsp";

	public static final String EDIT = "/external-page-import/edit_external_page.jsp";

	private ViewKeys() {
	}

}
