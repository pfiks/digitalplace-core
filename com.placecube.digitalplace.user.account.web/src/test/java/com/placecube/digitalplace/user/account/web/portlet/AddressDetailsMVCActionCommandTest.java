package com.placecube.digitalplace.user.account.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

public class AddressDetailsMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private AddressDetailsMVCActionCommand addressDetailsMVCActionCommand;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private AccountService mockAccountService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void doProcessAction_WhenAccountContextHasErrors_ThenRenderAddressDetails() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);

		when(mockAccountContext.hasErrors()).thenReturn(true);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		addressDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.ADDRESS_DETAILS);
	}

	@Test
	public void doProcessAction_WhenAccountContextNoErrorsAndAddressNextStep_ThenRenderNextStepPage() throws Exception {
		String nextStep = "nextStep";

		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);

		when(mockAccountContext.hasErrors()).thenReturn(false);

		when(mockAccountConfigurationService.getAddressNextStep(mockAccountContext)).thenReturn(nextStep);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		addressDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, nextStep);
	}

	@Test
	public void doProcessAction_WhenAccountContextNoErrorsAndNoAddressNextStep_ThenRenderConfirmationPage() throws Exception {
		String nextStep = "";

		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);

		when(mockAccountContext.hasErrors()).thenReturn(false);

		when(mockAccountConfigurationService.getAddressNextStep(mockAccountContext)).thenReturn(nextStep);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(true);

		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		addressDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountService, times(1)).createUserAccount(mockActionRequest, mockAccountContext, true);
		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.CONFIRMATION_PAGE);
		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockActionRequest);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenErrorConfiguringSkipPasswordStep_ThenThrowException() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		doThrow(new UserAccountException()).when(mockAccountContextService).configureSkipPasswordStep(mockAccountContext);

		addressDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenErrorGettingAccountConfiguration_ThenThrowException() throws Exception {
		String nextStep = "";

		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);

		when(mockAccountContext.hasErrors()).thenReturn(false);

		when(mockAccountConfigurationService.getAddressNextStep(mockAccountContext)).thenReturn(nextStep);

		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenThrow(new UserAccountException());

		addressDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenErrorGettingAddressNextStep_ThenThrowException() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);

		when(mockAccountContext.hasErrors()).thenReturn(false);

		when(mockAccountConfigurationService.getAddressNextStep(mockAccountContext)).thenThrow(new UserAccountException());

		addressDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = Exception.class)
	public void doProcessAction_WhenErrorGettingOrCreatingAccountContext_ThenThrowException() throws Exception {
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenThrow(new UserAccountException());

		addressDetailsMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

}
