package com.placecube.digitalplace.search.web.portlet.tag.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.web.portlet.tag.configuration.TagFacetPortletInstanceConfiguration;

@Component(immediate = true, service = TagConfigurationService.class)
public class TagConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public Optional<TagFacetPortletInstanceConfiguration> getValidConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		TagFacetPortletInstanceConfiguration portletInstanceConfiguration = configurationProvider.getPortletInstanceConfiguration(TagFacetPortletInstanceConfiguration.class, themeDisplay);

		return isValidConfiguration(portletInstanceConfiguration) ? Optional.of(portletInstanceConfiguration) : Optional.empty();
	}

	private boolean isValidConfiguration(TagFacetPortletInstanceConfiguration tagFacetPortletInstanceConfiguration) {
		return tagFacetPortletInstanceConfiguration.maxShowMoreTagsToShow() >= 0 && tagFacetPortletInstanceConfiguration.maxTagsToShow() > 0;
	}

}
