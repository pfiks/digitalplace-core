package com.placecube.digitalplace.payment.capitainterface.common.utilities.beans;

/**
 * Simple bean to hold the name (dataField) and the value of a piece of diagnostic data
 *
 */
public class DiagnosticData {

    private DataField dataField;
    private String value;


    public DiagnosticData(DataField dataField, String value) {
        super();
        this.dataField = dataField;
        this.value = value;
    }
    /**
     * @return the dataField
     */
    public DataField getDataField() {
        return dataField;
    }
    /**
     * @param dataField the dataField to set
     */
    public void setDataField(DataField dataField) {
        this.dataField = dataField;
    }
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }





}
