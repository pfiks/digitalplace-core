package com.placecube.digitalplace.search.web.portlet.tag.contributor;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.tag.configuration.TagFacetPortletConfigurationConstants;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.TAG_FILTER, service = SharedSearchContributor.class)
public class TagFacetPortletSharedSearchContributor implements SharedSearchContributor {

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<PortletPreferences> prefs = sharedSearchContributorSettings.getPortletPreferences();
		if (prefs.isPresent()) {
			PortletPreferences portletPreferences = prefs.get();
			addTagFacet(sharedSearchContributorSettings, portletPreferences);
			addSelectedTagsQuery(sharedSearchContributorSettings, portletPreferences);
		} else {
			throw new SystemException("Preferences not found");
		}

	}

	private void addTagFacet(SharedSearchContributorSettings sharedSearchContributorSettings, PortletPreferences portletPreferences) {

		int maxTagsToShow = GetterUtil.getInteger(portletPreferences.getValue(TagFacetPortletConfigurationConstants.MAX_TAGS_TO_SHOW, "10"));
		int maxShowMoreTagsToShow = GetterUtil.getInteger(portletPreferences.getValue(TagFacetPortletConfigurationConstants.MAX_MORE_TAGS_TO_SHOW, "10"));
		int maxTerms = maxTagsToShow + maxShowMoreTagsToShow;
		String facetFieldName = SearchFacetConstants.ASSET_TAG_NAMES;

		FacetConfiguration facetConfiguration = sharedSearchContributorSettings.createFacetConfiguration(facetFieldName);
		facetConfiguration.setOrder("OrderHitsDesc");
		facetConfiguration.setStatic(false);
		facetConfiguration.setWeight(1.4d);

		JSONObject data = facetConfiguration.getData();
		data.put("maxTerms", maxTerms);
		data.put("frequencyThreshold", 1);

		sharedSearchContributorSettings.addFacet(facetFieldName, Optional.of(facetConfiguration));

	}

	private void addSelectedTagsQuery(SharedSearchContributorSettings sharedSearchContributorSettings, PortletPreferences portletPreferences) {

		boolean isTagsQueryAnded = GetterUtil.getBoolean(portletPreferences.getValue(TagFacetPortletConfigurationConstants.TAGS_QUERY_ANDED, "false"));
		List<SharedActiveFilter> activeTagFilters = sharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_TAGS);
		Set<String> selectedTags = activeTagFilters.stream().map(SharedActiveFilter::getValue).collect(Collectors.toSet());

		if (isTagsQueryAnded) {
			for (String selectedTag : selectedTags) {
				sharedSearchContributorSettings.addBooleanQuery(SearchFacetConstants.ASSET_TAG_NAMES, selectedTag, BooleanClauseOccur.MUST);
			}
		} else {
			sharedSearchContributorSettings.setAssetTagNames(selectedTags);
		}

	}

}