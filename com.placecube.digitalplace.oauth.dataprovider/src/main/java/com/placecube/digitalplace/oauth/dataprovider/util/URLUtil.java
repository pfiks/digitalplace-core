package com.placecube.digitalplace.oauth.dataprovider.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.Http;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.SystemProperties;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = URLUtil.class)
public class URLUtil {

	private static final Log LOG = LogFactoryUtil.getLog(URLUtil.class);

	public URI buildURI(String url) throws URISyntaxException {
		return new URI(url);
	}

	public String buildURL(Map<String, String> pathInputParametersMap, String url) {

		for (Map.Entry<String, String> urlInputParameter : pathInputParametersMap.entrySet()) {

			url = StringUtil.replaceFirst(url, String.format("{%s}", urlInputParameter.getKey()), HtmlUtil.escapeURL(urlInputParameter.getValue()));
		}

		return url;
	}

	public String getAbsoluteURL(String query, String url) {
		if (query != null) {
			return StringUtil.replaceLast(url, StringPool.QUESTION + query, StringPool.BLANK);
		}

		return url;
	}

	public String getHostName(String host) {
		if (StringUtil.startsWith(host, "www.")) {
			return host.substring(4);
		}

		return host;
	}

	public int getHostPort(int port, String scheme) {
		if (port == -1) {
			if (StringUtil.equals(scheme, Http.HTTPS)) {
				return Http.HTTPS_PORT;
			}

			return Http.HTTP_PORT;
		}

		return port;
	}

	public Map<String, Object> getProxySettingsMap() {
		Map<String, Object> proxySettingsMap = new HashMap<>();

		try {
			String proxyHost = SystemProperties.get("http.proxyHost");
			String proxyPort = SystemProperties.get("http.proxyPort");

			if (Validator.isNotNull(proxyHost) && Validator.isNotNull(proxyPort)) {

				proxySettingsMap.put("proxyHostName", proxyHost);
				proxySettingsMap.put("proxyHostPort", GetterUtil.getInteger(proxyPort));
			}
		} catch (Exception exception) {
			proxySettingsMap.clear();

			if (LOG.isWarnEnabled()) {
				LOG.warn("Unable to get proxy settings from system properties", exception);
			}
		}

		return proxySettingsMap;
	}
}
