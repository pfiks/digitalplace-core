package com.placecube.digitalplace.externalpage.web.service;

import javax.portlet.PortletRequest;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.externalpage.web.constants.ValidationKeys;

@Component(immediate = true, service = ExternalPageImportValidator.class)
public class ExternalPageImportValidator {

	public boolean validate(PortletRequest portletRequest) {
		boolean valid = true;

		String xpath = ParamUtil.getString(portletRequest, PortletRequestKeys.XPATH);

		if (Validator.isNotNull(xpath)) {
			try {
				XPathFactory factory = XPathFactory.newInstance();
				XPath xpathObject = factory.newXPath();
				xpathObject.compile(xpath);
			} catch (XPathExpressionException e) {
				valid = false;
				SessionErrors.add(portletRequest, ValidationKeys.INVALID_XPATH);
			}
		}

		if (Validator.isNull(ParamUtil.getString(portletRequest, PortletRequestKeys.URL))) {
			valid = false;
			SessionErrors.add(portletRequest, ValidationKeys.REQUIRED_URL);
		}

		return valid;
	}
}
