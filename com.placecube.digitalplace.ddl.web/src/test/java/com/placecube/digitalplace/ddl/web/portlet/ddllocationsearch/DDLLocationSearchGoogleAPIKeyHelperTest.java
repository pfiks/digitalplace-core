package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PrefsPropsUtil.class, GetterUtil.class })
public class DDLLocationSearchGoogleAPIKeyHelperTest extends PowerMockito {

	private static final long COMPANY_ID = 20l;
	private static final String GOOGLE_MAPS_API_KEY = "googleMapsAPIKey";
	private static final String GOOGLE_MAPS_API_KEY_VALUE = "abcde1234";

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Group mockGroup;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@InjectMocks
	private DDLLocationSearchGoogleAPIKeyHelper dDLLocationSearchGoogleAPIKeyHelper;

	@Before
	public void activateSetup() throws Exception {
		mockStatic(PrefsPropsUtil.class, GetterUtil.class);
		initMocks(this);

		when(PrefsPropsUtil.getPreferences(COMPANY_ID)).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue(GOOGLE_MAPS_API_KEY, null)).thenReturn(GOOGLE_MAPS_API_KEY_VALUE);
	}

	@Test
	public void render_WhenNoErrorAndGroupTypeSettingIsNull_ThenGetCompanyGoogleMapsAPIKey() throws Exception {
		String expected = GOOGLE_MAPS_API_KEY_VALUE;
		when(mockGroup.getTypeSettingsProperty(GOOGLE_MAPS_API_KEY)).thenReturn(null);
		when(GetterUtil.getString(mockGroup.getTypeSettingsProperty(GOOGLE_MAPS_API_KEY), null)).thenReturn(null);
		when(mockPortletPreferences.getValue(GOOGLE_MAPS_API_KEY, null)).thenReturn(expected);

		String result = dDLLocationSearchGoogleAPIKeyHelper.getGoogleMapsAPIKey(COMPANY_ID, mockGroup);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void render_WhenNoError_WhenNoErrorAndGroupTypeSettingIsNotNull_ThenGetGroupGoogleMapsAPIKey() throws Exception {
		String expected = GOOGLE_MAPS_API_KEY_VALUE;
		when(mockGroup.getTypeSettingsProperty(GOOGLE_MAPS_API_KEY)).thenReturn(GOOGLE_MAPS_API_KEY_VALUE);
		when(GetterUtil.getString(mockGroup.getTypeSettingsProperty(GOOGLE_MAPS_API_KEY), null)).thenReturn(expected);

		String result = dDLLocationSearchGoogleAPIKeyHelper.getGoogleMapsAPIKey(COMPANY_ID, mockGroup);

		assertThat(result, equalTo(expected));

	}



}
