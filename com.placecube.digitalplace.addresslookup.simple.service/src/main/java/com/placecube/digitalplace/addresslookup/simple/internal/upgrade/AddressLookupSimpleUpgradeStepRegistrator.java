package com.placecube.digitalplace.addresslookup.simple.internal.upgrade;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.addresslookup.simple.internal.upgrade.steps.Upgrade_1_0_1_SimpleAddressLookupEntryTableIndexesUpdate;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class AddressLookupSimpleUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("1.0.0", "1.0.1", new Upgrade_1_0_1_SimpleAddressLookupEntryTableIndexesUpdate());
	}

}