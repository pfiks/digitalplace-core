package com.placecube.digitalplace.user.account.web.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountUserFieldsService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, service = AccountUserFieldsService.class)
public class AccountUserFieldsServiceImpl implements AccountUserFieldsService {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private UserAccountFieldRegistry userAccountFieldRegistry;

	@Override
	public void populateContextWithExistingAccountFields(AccountContext accountContext, User user) {
		ExpandoBridge expandoBridge = user.getExpandoBridge();
		userAccountFieldRegistry.getUserAccountFields(accountContext.getCompanyId()).forEach(af -> accountContext.addExpandoField(af.getExpandoFieldName(), (String) expandoBridge.getAttribute(af.getExpandoFieldName())));
	}

	@Override
	public void populateContextWithExistingConfiguredExpandoFields(AccountContext accountContext, User user, UserProfileFieldsConfiguration userFieldsConfiguration) {
		ExpandoBridge expandoBridge = user.getExpandoBridge();
		accountConfigurationService.getConfiguredExpandoFields(accountContext, userFieldsConfiguration)
		.forEach(ef -> accountContext.addExpandoField(ef.getExpandoFieldKey(), (String) expandoBridge.getAttribute(ef.getExpandoFieldKey())));
	}
}
