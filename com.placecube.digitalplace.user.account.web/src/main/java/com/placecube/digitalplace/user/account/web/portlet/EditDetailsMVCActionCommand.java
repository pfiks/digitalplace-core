package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.user.account.web.util.AccountPermissionUtils;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.EDIT_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.EDIT_DETAILS }, service = MVCActionCommand.class)
public class EditDetailsMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private AccountService accountService;

	@Reference
	private AccountValidationService accountValidationService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String renderCommand = MVCCommandKeys.EDIT_DETAILS;

		if (AccountPermissionUtils.hasEditPermission(actionRequest)) {

			AccountContext accountContext = accountContextService.getOrCreateContext(actionRequest);
			accountContextService.configurePersonalDetails(accountContext, actionRequest);
			accountContextService.configureAdditionalDetails(accountContext, actionRequest);
			accountContextService.configureAddressDetails(accountContext, actionRequest);
			accountContextService.configureAccountFields(accountContext, actionRequest);
			accountContextService.configureExpandoFields(accountContext, actionRequest);

			UserProfileFieldsConfiguration userFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());
			CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, actionRequest);

			accountContext.clearErrors();
			accountValidationService.validatePersonalDetails(accountContext, actionRequest, accountConfiguration, userFieldsConfiguration);
			accountValidationService.validateAdditionalDetails(accountContext, actionRequest, accountConfiguration, userFieldsConfiguration);

			if (!accountContext.hasErrors()) {
				accountService.updateUserAccount(actionRequest, accountContext, accountConfiguration.addressOutOfLocalArea());
				actionResponse.getRenderParameters().setValue(AccountConnectorConstant.MODEL_ID, accountContext.getAccountConnectorModelId());
				actionResponse.getRenderParameters().setValue(RequestKeys.REDIRECT_URL, ParamUtil.getString(actionRequest, RequestKeys.REDIRECT_URL));
				renderCommand = MVCCommandKeys.CLOSE_POPUP;
			}
			accountContextService.updateContextInSession(accountContext, actionRequest);
		} else {
			SessionErrors.add(actionRequest, "no-permissions");
			hideDefaultErrorMessage(actionRequest);
		}

		actionResponse.getRenderParameters().setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, renderCommand);
	}

}
