package com.placecube.digitalplace.dataconsumer.utils.executor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.service.DataConsumerLocalService;
import com.placecube.digitalplace.dataconsumer.utils.DataConsumerUtils;
import com.placecube.digitalplace.dataconsumer.utils.DataConsumerWebClient;
import com.placecube.digitalplace.dataconsumer.utils.DataConsumerWebClientUtils;

@RunWith(MockitoJUnitRunner.class)
public class DataConsumerExecutorServiceTest {

	private static final String BODY = "{\"parameter\" : \"$placeholder$\"}";

	private static final long FORM_INSTANCE_ID = 232l;

	private static final long FORM_INSTANCE_RECORD_ID = 323;

	private static final Locale LOCALE = Locale.UK;

	private static final String METHOD = "GET";

	private static final String REPLACED_DATA_CONSUMER_ONE_BODY = "{\"parameter\" : \"valueOne\"}";

	private static final String REPLACED_DATA_CONSUMER_TWO_BODY = "{\"parameter\" : \"valueTwo\"}";

	private static final String URL_STRING = "http://localhost:8080/user/get-user";

	@InjectMocks
	private DataConsumerExecutorService dataConsumerExecutorService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private Map<String, String> mockConfirmedMapping;

	@Mock
	private DataConsumerLocalService mockDataConsumerLocalService;

	@Mock
	private DataConsumer mockDataConsumerOne;

	@Mock
	private DataConsumer mockDataConsumerTwo;

	@Mock
	private DataConsumerUtils mockDataConsumerUtils;

	@Mock
	private DataConsumerWebClient mockDataconsumerWebClient;

	@Mock
	private DataConsumerWebClientUtils mockDataconsumerWebClientUtils;

	@Mock
	private Map<String, String> mockDataProviderFieldsAndValuesOne;

	@Mock
	private Map<String, String> mockDataProviderFieldsAndValuesTwo;

	@Mock
	private DDMFormValues mockDataProviderOneDDMFormValues;

	@Mock
	private DDMFormValues mockDataProviderTwoDDMFormValues;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue;

	@Mock
	private DDMFormInstance mockDDMFormInstance;

	@Mock
	private DDMFormInstanceLocalService mockDDMFormInstanceLocalService;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private Map<String, String> mockHeaders;

	@Mock
	private JSONFactory mockJson;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private Portal mockPortal;

	@Mock
	private Value mockValue;

	@Test
	public void executeDataConsumersFromActionRequest_WhenDataConsumerChainIsNotZero_ThenExecutesFormDataConsumersWithMappingsFromDataProvidersToPreviousJsonResponseValues() throws Exception {
		final String dataConsumerOneJSONResponse = "{json}";
		when(mockActionRequest.getLocale()).thenReturn(LOCALE);

		List<DataConsumer> dataConsumers = Arrays.asList(new DataConsumer[] { mockDataConsumerOne, mockDataConsumerTwo });
		when(mockDDMFormInstance.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(dataConsumers);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);

		mockDataConsumer(mockDataConsumerOne, mockDataProviderOneDDMFormValues, mockDataProviderFieldsAndValuesOne, 0, dataConsumerOneJSONResponse, REPLACED_DATA_CONSUMER_ONE_BODY);
		mockDataConsumer(mockDataConsumerTwo, mockDataProviderTwoDDMFormValues, mockDataProviderFieldsAndValuesTwo, 1, StringPool.BLANK, REPLACED_DATA_CONSUMER_TWO_BODY);

		when(mockDataConsumerUtils.mapDataProviderFieldsToFormValues(mockConfirmedMapping, mockActionRequest, mockDDMStructure)).thenReturn(mockDataProviderFieldsAndValuesOne);
		when(mockDataConsumerUtils.mapDataProviderFieldsToResponseValues(mockConfirmedMapping, dataConsumerOneJSONResponse, mockActionRequest)).thenReturn(mockDataProviderFieldsAndValuesTwo);

		dataConsumerExecutorService.executeDataConsumersFromActionRequest(mockDDMFormInstance, FORM_INSTANCE_RECORD_ID, mockActionRequest);

		verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesOne, REPLACED_DATA_CONSUMER_ONE_BODY);
		verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesTwo, REPLACED_DATA_CONSUMER_TWO_BODY);

	}

	@Test
	public void executeDataConsumersFromActionRequest_WhenDataConsumerChainIsZero_ThenExecutesFormDataConsumersWithMappingsFromDataProvidersToFormValues() throws Exception {
		List<DataConsumer> dataConsumers = Arrays.asList(new DataConsumer[] { mockDataConsumerOne });
		when(mockDDMFormInstance.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(dataConsumers);
		when(mockActionRequest.getLocale()).thenReturn(LOCALE);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);

		mockDataConsumer(mockDataConsumerOne, mockDataProviderOneDDMFormValues, mockDataProviderFieldsAndValuesOne, 0, StringPool.BLANK, REPLACED_DATA_CONSUMER_ONE_BODY);

		when(mockDataConsumerUtils.mapDataProviderFieldsToFormValues(mockConfirmedMapping, mockActionRequest, mockDDMStructure)).thenReturn(mockDataProviderFieldsAndValuesOne);

		dataConsumerExecutorService.executeDataConsumersFromActionRequest(mockDDMFormInstance, FORM_INSTANCE_RECORD_ID, mockActionRequest);

		verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesOne, REPLACED_DATA_CONSUMER_ONE_BODY);

	}

	@Test
	public void executeDataConsumersFromActionRequest_WhenNoErrorAndMethodNotRequiresBody_ThenPutClassPKAndClassNameId() throws Exception {
		List<DataConsumer> dataConsumers = Arrays.asList(new DataConsumer[] { mockDataConsumerOne });
		when(mockDDMFormInstance.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(dataConsumers);
		when(mockActionRequest.getLocale()).thenReturn(LOCALE);
		when(mockDataconsumerWebClientUtils.isHttpMethodRequiresBody(ArgumentMatchers.anyString())).thenReturn(false);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);

		mockDataConsumer(mockDataConsumerOne, mockDataProviderOneDDMFormValues, mockDataProviderFieldsAndValuesOne, 0, StringPool.BLANK, REPLACED_DATA_CONSUMER_ONE_BODY);

		when(mockDataConsumerUtils.mapDataProviderFieldsToFormValues(mockConfirmedMapping, mockActionRequest, mockDDMStructure)).thenReturn(mockDataProviderFieldsAndValuesOne);

		long classNameId = 123;
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(classNameId);

		dataConsumerExecutorService.executeDataConsumersFromActionRequest(mockDDMFormInstance, FORM_INSTANCE_RECORD_ID, mockActionRequest);

		InOrder inOrder = inOrder(mockDataconsumerWebClient, mockDataProviderFieldsAndValuesOne);
		inOrder.verify(mockDataProviderFieldsAndValuesOne).put("classPK", String.valueOf(FORM_INSTANCE_RECORD_ID));
		inOrder.verify(mockDataProviderFieldsAndValuesOne).put("classNameId", String.valueOf(classNameId));
		inOrder.verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesOne, REPLACED_DATA_CONSUMER_ONE_BODY);

	}

	@Test
	public void executeDataConsumersFromActionRequest_WhenNoErrorAndMethodRequiresBody_ThenPutClassPKAndClassNameIdInBody() throws Exception {
		List<DataConsumer> dataConsumers = Arrays.asList(new DataConsumer[] { mockDataConsumerOne });
		when(mockDDMFormInstance.getFormInstanceId()).thenReturn(FORM_INSTANCE_ID);
		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(dataConsumers);
		when(mockActionRequest.getLocale()).thenReturn(LOCALE);
		when(mockDataconsumerWebClientUtils.isHttpMethodRequiresBody(ArgumentMatchers.anyString())).thenReturn(true);
		when(mockDDMFormInstance.getStructure()).thenReturn(mockDDMStructure);

		mockDataConsumer(mockDataConsumerOne, mockDataProviderOneDDMFormValues, mockDataProviderFieldsAndValuesOne, 0, StringPool.BLANK, REPLACED_DATA_CONSUMER_ONE_BODY);

		when(mockDataConsumerUtils.mapDataProviderFieldsToFormValues(mockConfirmedMapping, mockActionRequest, mockDDMStructure)).thenReturn(mockDataProviderFieldsAndValuesOne);
		when(mockJson.createJSONObject(ArgumentMatchers.anyString())).thenReturn(mockJsonObject);
		when(mockJsonObject.toJSONString()).thenReturn(REPLACED_DATA_CONSUMER_ONE_BODY + "classPK:classNameId");

		long classNameId = 123;
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(classNameId);

		dataConsumerExecutorService.executeDataConsumersFromActionRequest(mockDDMFormInstance, FORM_INSTANCE_RECORD_ID, mockActionRequest);

		InOrder inOrder = inOrder(mockDataconsumerWebClient, mockJsonObject);
		inOrder.verify(mockJsonObject, times(1)).put("classPK", FORM_INSTANCE_RECORD_ID);
		inOrder.verify(mockJsonObject, times(1)).put("classNameId", String.valueOf(classNameId));
		inOrder.verify(mockJsonObject, times(1)).toJSONString();
		inOrder.verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesOne, REPLACED_DATA_CONSUMER_ONE_BODY + "classPK:classNameId");
	}

	@Test
	public void executeDataConsumersFromFormValues_WhenDataConsumerChainIsNotZero_ThenExecutesFormDataConsumersWithMappingsFromDataProvidersToPreviousJsonResponseValues() throws Exception {
		final String dataConsumerOneJSONResponse = "{json}";
		List<DataConsumer> dataConsumers = Arrays.asList(new DataConsumer[] { mockDataConsumerOne, mockDataConsumerTwo });
		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(dataConsumers);

		mockDataConsumer(mockDataConsumerOne, mockDataProviderOneDDMFormValues, mockDataProviderFieldsAndValuesOne, 0, dataConsumerOneJSONResponse, REPLACED_DATA_CONSUMER_ONE_BODY);
		mockDataConsumer(mockDataConsumerTwo, mockDataProviderTwoDDMFormValues, mockDataProviderFieldsAndValuesTwo, 1, StringPool.BLANK, REPLACED_DATA_CONSUMER_TWO_BODY);

		when(mockDataConsumerUtils.mapDataProviderFieldsToFormValues(mockConfirmedMapping, LOCALE, mockDDMFormValues)).thenReturn(mockDataProviderFieldsAndValuesOne);
		when(mockDataConsumerUtils.mapDataProviderFieldsToResponseValues(mockConfirmedMapping, dataConsumerOneJSONResponse, LOCALE, mockDDMFormValues)).thenReturn(mockDataProviderFieldsAndValuesTwo);

		dataConsumerExecutorService.executeDataConsumersFromFormValues(FORM_INSTANCE_ID, FORM_INSTANCE_RECORD_ID, LOCALE, mockDDMFormValues);

		verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesOne, REPLACED_DATA_CONSUMER_ONE_BODY);
		verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesTwo, REPLACED_DATA_CONSUMER_TWO_BODY);

	}

	@Test
	public void executeDataConsumersFromFormValues_WhenDataConsumerChainIsZero_ThenExecutesFormDataConsumersWithMappingsFromDataProvidersToFormValues() throws Exception {
		List<DataConsumer> dataConsumers = Arrays.asList(new DataConsumer[] { mockDataConsumerOne });
		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(dataConsumers);
		when(mockDataconsumerWebClientUtils.isHttpMethodRequiresBody(ArgumentMatchers.anyString())).thenReturn(false);

		mockDataConsumer(mockDataConsumerOne, mockDataProviderOneDDMFormValues, mockDataProviderFieldsAndValuesOne, 0, StringPool.BLANK, REPLACED_DATA_CONSUMER_ONE_BODY);

		when(mockDataConsumerUtils.mapDataProviderFieldsToFormValues(mockConfirmedMapping, LOCALE, mockDDMFormValues)).thenReturn(mockDataProviderFieldsAndValuesOne);

		dataConsumerExecutorService.executeDataConsumersFromFormValues(FORM_INSTANCE_ID, FORM_INSTANCE_RECORD_ID, LOCALE, mockDDMFormValues);

		verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesOne, REPLACED_DATA_CONSUMER_ONE_BODY);

	}

	@Test
	public void executeDataConsumersFromFormValues_WhenNoErrorAndMethodNotRequiresBody_ThenPutClassPKAndClassNameId() throws Exception {
		List<DataConsumer> dataConsumers = Arrays.asList(new DataConsumer[] { mockDataConsumerOne });
		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(dataConsumers);
		when(mockDataconsumerWebClientUtils.isHttpMethodRequiresBody(ArgumentMatchers.anyString())).thenReturn(false);

		mockDataConsumer(mockDataConsumerOne, mockDataProviderOneDDMFormValues, mockDataProviderFieldsAndValuesOne, 0, StringPool.BLANK, REPLACED_DATA_CONSUMER_ONE_BODY);

		when(mockDataConsumerUtils.mapDataProviderFieldsToFormValues(mockConfirmedMapping, LOCALE, mockDDMFormValues)).thenReturn(mockDataProviderFieldsAndValuesOne);

		long classNameId = 123;
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(classNameId);

		dataConsumerExecutorService.executeDataConsumersFromFormValues(FORM_INSTANCE_ID, FORM_INSTANCE_RECORD_ID, LOCALE, mockDDMFormValues);

		InOrder inOrder = inOrder(mockDataconsumerWebClient, mockDataProviderFieldsAndValuesOne);
		inOrder.verify(mockDataProviderFieldsAndValuesOne).put("classPK", String.valueOf(FORM_INSTANCE_RECORD_ID));
		inOrder.verify(mockDataProviderFieldsAndValuesOne).put("classNameId", String.valueOf(classNameId));
		inOrder.verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesOne, REPLACED_DATA_CONSUMER_ONE_BODY);

	}

	@Test
	public void executeDataConsumersFromFormValues_WhenNoErrorAndMethodRequiresBody_ThenPutClassPKAndClassNameIdInBody() throws Exception {
		List<DataConsumer> dataConsumers = Arrays.asList(new DataConsumer[] { mockDataConsumerOne });
		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(dataConsumers);
		when(mockDataconsumerWebClientUtils.isHttpMethodRequiresBody(ArgumentMatchers.anyString())).thenReturn(true);

		mockDataConsumer(mockDataConsumerOne, mockDataProviderOneDDMFormValues, mockDataProviderFieldsAndValuesOne, 0, StringPool.BLANK, REPLACED_DATA_CONSUMER_ONE_BODY);

		when(mockDataConsumerUtils.mapDataProviderFieldsToFormValues(mockConfirmedMapping, LOCALE, mockDDMFormValues)).thenReturn(mockDataProviderFieldsAndValuesOne);
		when(mockJson.createJSONObject(ArgumentMatchers.anyString())).thenReturn(mockJsonObject);
		when(mockJsonObject.toJSONString()).thenReturn(REPLACED_DATA_CONSUMER_ONE_BODY + "classPK:classNameId");

		long classNameId = 123;
		when(mockPortal.getClassNameId(DDMFormInstanceRecord.class)).thenReturn(classNameId);

		dataConsumerExecutorService.executeDataConsumersFromFormValues(FORM_INSTANCE_ID, FORM_INSTANCE_RECORD_ID, LOCALE, mockDDMFormValues);

		InOrder inOrder = inOrder(mockDataconsumerWebClient, mockJsonObject);
		inOrder.verify(mockJsonObject, times(1)).put("classPK", FORM_INSTANCE_RECORD_ID);
		inOrder.verify(mockJsonObject, times(1)).put("classNameId", String.valueOf(classNameId));
		inOrder.verify(mockJsonObject, times(1)).toJSONString();
		inOrder.verify(mockDataconsumerWebClient, times(1)).execute(URL_STRING, METHOD, mockHeaders, mockDataProviderFieldsAndValuesOne, REPLACED_DATA_CONSUMER_ONE_BODY + "classPK:classNameId");

	}

	@Test
	public void hasDataConsumers_WhenFormHasDataConsumers_ThenReturnTrue() {

		List<DataConsumer> dataConsumers = Arrays.asList(new DataConsumer[] { mockDataConsumerOne, mockDataConsumerTwo });
		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(dataConsumers);

		assertTrue(dataConsumerExecutorService.hasDataConsumers(FORM_INSTANCE_ID));

	}

	@Test
	public void hasDataConsumers_WhenFormHasNoDataConsumers_ThenReturnFalse() {

		when(mockDataConsumerLocalService.getDataConsumersByDDMFormInstanceId(FORM_INSTANCE_ID)).thenReturn(Collections.emptyList());

		assertFalse(dataConsumerExecutorService.hasDataConsumers(FORM_INSTANCE_ID));

	}

	private void mockDataConsumer(DataConsumer dataConsumer, DDMFormValues dataProviderDDMFormValues, Map<String, String> dataProviderFieldsAndValues, int chain, String jsonResponse,
			String replacedBody) throws Exception {
		List<DDMFormFieldValue> formFieldValues = Arrays.asList(new DDMFormFieldValue[] { mockDDMFormFieldValue });

		Map<String, List<DDMFormFieldValue>> dataProviderFormFieldValuesMap = new HashMap<>();
		dataProviderFormFieldValuesMap.put("url", formFieldValues);

		when(mockDataConsumerUtils.getDDMFormValues(dataConsumer)).thenReturn(dataProviderDDMFormValues);
		when(mockDataConsumerUtils.getInputParametersMapping(dataConsumer, LOCALE, dataProviderDDMFormValues)).thenReturn(mockConfirmedMapping);
		when(mockDataConsumerUtils.replaceBodyPlaceholdersWithDataProviderFieldValues(dataProviderFieldsAndValues, BODY)).thenReturn(replacedBody);

		when(dataConsumer.getBody()).thenReturn(BODY);
		when(dataConsumer.getChain()).thenReturn(chain);
		when(dataProviderDDMFormValues.getDDMFormFieldValuesMap()).thenReturn(dataProviderFormFieldValuesMap);
		when(mockDDMFormFieldValue.getValue()).thenReturn(mockValue);
		when(mockValue.getString(LOCALE)).thenReturn(URL_STRING);
		when(dataConsumer.getMethod()).thenReturn(METHOD);

		when(mockDataConsumerUtils.getHeaders(dataConsumer, LOCALE, dataProviderDDMFormValues)).thenReturn(mockHeaders);

		when(mockDataconsumerWebClient.execute(URL_STRING, METHOD, mockHeaders, dataProviderFieldsAndValues, REPLACED_DATA_CONSUMER_ONE_BODY)).thenReturn(jsonResponse);
	}

}
