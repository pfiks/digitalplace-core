package com.placecube.digitalplace.form.extender.internal;

import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import org.osgi.framework.Bundle;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = JournalArticleURLService.class)
public class JournalArticleURLService {

	public List<URL> getJournalArticleURLs(Bundle bundle) {

		Enumeration<URL> enuUrls = bundle.findEntries("webcontent", "*.xml", true);

		if (Validator.isNotNull(enuUrls)) {
			return Collections.list(enuUrls);
		} else {
			return Collections.emptyList();
		}
	}

}
