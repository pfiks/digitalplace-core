/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;

import com.placecube.digitalplace.group.rest.exception.NoSuchGroupWebhookException;
import com.placecube.digitalplace.group.rest.model.GroupWebhook;
import com.placecube.digitalplace.group.rest.model.GroupWebhookTable;
import com.placecube.digitalplace.group.rest.model.impl.GroupWebhookImpl;
import com.placecube.digitalplace.group.rest.model.impl.GroupWebhookModelImpl;
import com.placecube.digitalplace.group.rest.service.persistence.GroupWebhookPersistence;
import com.placecube.digitalplace.group.rest.service.persistence.GroupWebhookUtil;
import com.placecube.digitalplace.group.rest.service.persistence.impl.constants.Placecube_GroupRestPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the group webhook service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = GroupWebhookPersistence.class)
public class GroupWebhookPersistenceImpl
	extends BasePersistenceImpl<GroupWebhook>
	implements GroupWebhookPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>GroupWebhookUtil</code> to access the group webhook persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		GroupWebhookImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByGroupId;
	private FinderPath _finderPathWithoutPaginationFindByGroupId;
	private FinderPath _finderPathCountByGroupId;

	/**
	 * Returns all the group webhooks where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId(long groupId) {
		return findByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the group webhooks where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<GroupWebhook> orderByComparator) {

		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<GroupWebhook> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId;
				finderArgs = new Object[] {groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId;
			finderArgs = new Object[] {groupId, start, end, orderByComparator};
		}

		List<GroupWebhook> list = null;

		if (useFinderCache) {
			list = (List<GroupWebhook>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (GroupWebhook groupWebhook : list) {
					if (groupId != groupWebhook.getGroupId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_GROUPWEBHOOK_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(GroupWebhookModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				list = (List<GroupWebhook>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook findByGroupId_First(
			long groupId, OrderByComparator<GroupWebhook> orderByComparator)
		throws NoSuchGroupWebhookException {

		GroupWebhook groupWebhook = fetchByGroupId_First(
			groupId, orderByComparator);

		if (groupWebhook != null) {
			return groupWebhook;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchGroupWebhookException(sb.toString());
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook fetchByGroupId_First(
		long groupId, OrderByComparator<GroupWebhook> orderByComparator) {

		List<GroupWebhook> list = findByGroupId(
			groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook findByGroupId_Last(
			long groupId, OrderByComparator<GroupWebhook> orderByComparator)
		throws NoSuchGroupWebhookException {

		GroupWebhook groupWebhook = fetchByGroupId_Last(
			groupId, orderByComparator);

		if (groupWebhook != null) {
			return groupWebhook;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchGroupWebhookException(sb.toString());
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook fetchByGroupId_Last(
		long groupId, OrderByComparator<GroupWebhook> orderByComparator) {

		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<GroupWebhook> list = findByGroupId(
			groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the group webhooks before and after the current group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param webhookId the primary key of the current group webhook
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	@Override
	public GroupWebhook[] findByGroupId_PrevAndNext(
			long webhookId, long groupId,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws NoSuchGroupWebhookException {

		GroupWebhook groupWebhook = findByPrimaryKey(webhookId);

		Session session = null;

		try {
			session = openSession();

			GroupWebhook[] array = new GroupWebhookImpl[3];

			array[0] = getByGroupId_PrevAndNext(
				session, groupWebhook, groupId, orderByComparator, true);

			array[1] = groupWebhook;

			array[2] = getByGroupId_PrevAndNext(
				session, groupWebhook, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected GroupWebhook getByGroupId_PrevAndNext(
		Session session, GroupWebhook groupWebhook, long groupId,
		OrderByComparator<GroupWebhook> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_GROUPWEBHOOK_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(GroupWebhookModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(groupWebhook)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<GroupWebhook> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the group webhooks where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (GroupWebhook groupWebhook :
				findByGroupId(
					groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(groupWebhook);
		}
	}

	/**
	 * Returns the number of group webhooks where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching group webhooks
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = _finderPathCountByGroupId;

		Object[] finderArgs = new Object[] {groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_GROUPWEBHOOK_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 =
		"groupWebhook.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByGroupId_Enabled;
	private FinderPath _finderPathWithoutPaginationFindByGroupId_Enabled;
	private FinderPath _finderPathCountByGroupId_Enabled;

	/**
	 * Returns all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @return the matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled) {

		return findByGroupId_Enabled(
			groupId, enabled, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled, int start, int end) {

		return findByGroupId_Enabled(groupId, enabled, start, end, null);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled, int start, int end,
		OrderByComparator<GroupWebhook> orderByComparator) {

		return findByGroupId_Enabled(
			groupId, enabled, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled, int start, int end,
		OrderByComparator<GroupWebhook> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId_Enabled;
				finderArgs = new Object[] {groupId, enabled};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId_Enabled;
			finderArgs = new Object[] {
				groupId, enabled, start, end, orderByComparator
			};
		}

		List<GroupWebhook> list = null;

		if (useFinderCache) {
			list = (List<GroupWebhook>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (GroupWebhook groupWebhook : list) {
					if ((groupId != groupWebhook.getGroupId()) ||
						(enabled != groupWebhook.isEnabled())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_GROUPWEBHOOK_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_ENABLED_GROUPID_2);

			sb.append(_FINDER_COLUMN_GROUPID_ENABLED_ENABLED_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(GroupWebhookModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(enabled);

				list = (List<GroupWebhook>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook findByGroupId_Enabled_First(
			long groupId, boolean enabled,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws NoSuchGroupWebhookException {

		GroupWebhook groupWebhook = fetchByGroupId_Enabled_First(
			groupId, enabled, orderByComparator);

		if (groupWebhook != null) {
			return groupWebhook;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", enabled=");
		sb.append(enabled);

		sb.append("}");

		throw new NoSuchGroupWebhookException(sb.toString());
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook fetchByGroupId_Enabled_First(
		long groupId, boolean enabled,
		OrderByComparator<GroupWebhook> orderByComparator) {

		List<GroupWebhook> list = findByGroupId_Enabled(
			groupId, enabled, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook findByGroupId_Enabled_Last(
			long groupId, boolean enabled,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws NoSuchGroupWebhookException {

		GroupWebhook groupWebhook = fetchByGroupId_Enabled_Last(
			groupId, enabled, orderByComparator);

		if (groupWebhook != null) {
			return groupWebhook;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", enabled=");
		sb.append(enabled);

		sb.append("}");

		throw new NoSuchGroupWebhookException(sb.toString());
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook fetchByGroupId_Enabled_Last(
		long groupId, boolean enabled,
		OrderByComparator<GroupWebhook> orderByComparator) {

		int count = countByGroupId_Enabled(groupId, enabled);

		if (count == 0) {
			return null;
		}

		List<GroupWebhook> list = findByGroupId_Enabled(
			groupId, enabled, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the group webhooks before and after the current group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param webhookId the primary key of the current group webhook
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	@Override
	public GroupWebhook[] findByGroupId_Enabled_PrevAndNext(
			long webhookId, long groupId, boolean enabled,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws NoSuchGroupWebhookException {

		GroupWebhook groupWebhook = findByPrimaryKey(webhookId);

		Session session = null;

		try {
			session = openSession();

			GroupWebhook[] array = new GroupWebhookImpl[3];

			array[0] = getByGroupId_Enabled_PrevAndNext(
				session, groupWebhook, groupId, enabled, orderByComparator,
				true);

			array[1] = groupWebhook;

			array[2] = getByGroupId_Enabled_PrevAndNext(
				session, groupWebhook, groupId, enabled, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected GroupWebhook getByGroupId_Enabled_PrevAndNext(
		Session session, GroupWebhook groupWebhook, long groupId,
		boolean enabled, OrderByComparator<GroupWebhook> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_GROUPWEBHOOK_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_ENABLED_GROUPID_2);

		sb.append(_FINDER_COLUMN_GROUPID_ENABLED_ENABLED_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(GroupWebhookModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		queryPos.add(enabled);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(groupWebhook)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<GroupWebhook> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the group webhooks where groupId = &#63; and enabled = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 */
	@Override
	public void removeByGroupId_Enabled(long groupId, boolean enabled) {
		for (GroupWebhook groupWebhook :
				findByGroupId_Enabled(
					groupId, enabled, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(groupWebhook);
		}
	}

	/**
	 * Returns the number of group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @return the number of matching group webhooks
	 */
	@Override
	public int countByGroupId_Enabled(long groupId, boolean enabled) {
		FinderPath finderPath = _finderPathCountByGroupId_Enabled;

		Object[] finderArgs = new Object[] {groupId, enabled};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_GROUPWEBHOOK_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_ENABLED_GROUPID_2);

			sb.append(_FINDER_COLUMN_GROUPID_ENABLED_ENABLED_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(enabled);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_ENABLED_GROUPID_2 =
		"groupWebhook.groupId = ? AND ";

	private static final String _FINDER_COLUMN_GROUPID_ENABLED_ENABLED_2 =
		"groupWebhook.enabled = ?";

	private FinderPath
		_finderPathWithPaginationFindByGroupId_Enabled_ClassName_ClassPK;
	private FinderPath
		_finderPathWithoutPaginationFindByGroupId_Enabled_ClassName_ClassPK;
	private FinderPath _finderPathCountByGroupId_Enabled_ClassName_ClassPK;

	/**
	 * Returns all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @return the matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK) {

		return findByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK,
		int start, int end) {

		return findByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK, start, end, null);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK,
		int start, int end, OrderByComparator<GroupWebhook> orderByComparator) {

		return findByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group webhooks
	 */
	@Override
	public List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK,
		int start, int end, OrderByComparator<GroupWebhook> orderByComparator,
		boolean useFinderCache) {

		className = Objects.toString(className, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByGroupId_Enabled_ClassName_ClassPK;
				finderArgs = new Object[] {
					groupId, enabled, className, classPK
				};
			}
		}
		else if (useFinderCache) {
			finderPath =
				_finderPathWithPaginationFindByGroupId_Enabled_ClassName_ClassPK;
			finderArgs = new Object[] {
				groupId, enabled, className, classPK, start, end,
				orderByComparator
			};
		}

		List<GroupWebhook> list = null;

		if (useFinderCache) {
			list = (List<GroupWebhook>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (GroupWebhook groupWebhook : list) {
					if ((groupId != groupWebhook.getGroupId()) ||
						(enabled != groupWebhook.isEnabled()) ||
						!className.equals(groupWebhook.getClassName()) ||
						(classPK != groupWebhook.getClassPK())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					6 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(6);
			}

			sb.append(_SQL_SELECT_GROUPWEBHOOK_WHERE);

			sb.append(
				_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_GROUPID_2);

			sb.append(
				_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_ENABLED_2);

			boolean bindClassName = false;

			if (className.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSNAME_3);
			}
			else {
				bindClassName = true;

				sb.append(
					_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSNAME_2);
			}

			sb.append(
				_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSPK_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(GroupWebhookModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(enabled);

				if (bindClassName) {
					queryPos.add(className);
				}

				queryPos.add(classPK);

				list = (List<GroupWebhook>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook findByGroupId_Enabled_ClassName_ClassPK_First(
			long groupId, boolean enabled, String className, long classPK,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws NoSuchGroupWebhookException {

		GroupWebhook groupWebhook =
			fetchByGroupId_Enabled_ClassName_ClassPK_First(
				groupId, enabled, className, classPK, orderByComparator);

		if (groupWebhook != null) {
			return groupWebhook;
		}

		StringBundler sb = new StringBundler(10);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", enabled=");
		sb.append(enabled);

		sb.append(", className=");
		sb.append(className);

		sb.append(", classPK=");
		sb.append(classPK);

		sb.append("}");

		throw new NoSuchGroupWebhookException(sb.toString());
	}

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook fetchByGroupId_Enabled_ClassName_ClassPK_First(
		long groupId, boolean enabled, String className, long classPK,
		OrderByComparator<GroupWebhook> orderByComparator) {

		List<GroupWebhook> list = findByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook findByGroupId_Enabled_ClassName_ClassPK_Last(
			long groupId, boolean enabled, String className, long classPK,
			OrderByComparator<GroupWebhook> orderByComparator)
		throws NoSuchGroupWebhookException {

		GroupWebhook groupWebhook =
			fetchByGroupId_Enabled_ClassName_ClassPK_Last(
				groupId, enabled, className, classPK, orderByComparator);

		if (groupWebhook != null) {
			return groupWebhook;
		}

		StringBundler sb = new StringBundler(10);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append(", enabled=");
		sb.append(enabled);

		sb.append(", className=");
		sb.append(className);

		sb.append(", classPK=");
		sb.append(classPK);

		sb.append("}");

		throw new NoSuchGroupWebhookException(sb.toString());
	}

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	@Override
	public GroupWebhook fetchByGroupId_Enabled_ClassName_ClassPK_Last(
		long groupId, boolean enabled, String className, long classPK,
		OrderByComparator<GroupWebhook> orderByComparator) {

		int count = countByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK);

		if (count == 0) {
			return null;
		}

		List<GroupWebhook> list = findByGroupId_Enabled_ClassName_ClassPK(
			groupId, enabled, className, classPK, count - 1, count,
			orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the group webhooks before and after the current group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param webhookId the primary key of the current group webhook
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	@Override
	public GroupWebhook[] findByGroupId_Enabled_ClassName_ClassPK_PrevAndNext(
			long webhookId, long groupId, boolean enabled, String className,
			long classPK, OrderByComparator<GroupWebhook> orderByComparator)
		throws NoSuchGroupWebhookException {

		className = Objects.toString(className, "");

		GroupWebhook groupWebhook = findByPrimaryKey(webhookId);

		Session session = null;

		try {
			session = openSession();

			GroupWebhook[] array = new GroupWebhookImpl[3];

			array[0] = getByGroupId_Enabled_ClassName_ClassPK_PrevAndNext(
				session, groupWebhook, groupId, enabled, className, classPK,
				orderByComparator, true);

			array[1] = groupWebhook;

			array[2] = getByGroupId_Enabled_ClassName_ClassPK_PrevAndNext(
				session, groupWebhook, groupId, enabled, className, classPK,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected GroupWebhook getByGroupId_Enabled_ClassName_ClassPK_PrevAndNext(
		Session session, GroupWebhook groupWebhook, long groupId,
		boolean enabled, String className, long classPK,
		OrderByComparator<GroupWebhook> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				7 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(6);
		}

		sb.append(_SQL_SELECT_GROUPWEBHOOK_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_GROUPID_2);

		sb.append(_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_ENABLED_2);

		boolean bindClassName = false;

		if (className.isEmpty()) {
			sb.append(
				_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSNAME_3);
		}
		else {
			bindClassName = true;

			sb.append(
				_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSNAME_2);
		}

		sb.append(_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSPK_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(GroupWebhookModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		queryPos.add(enabled);

		if (bindClassName) {
			queryPos.add(className);
		}

		queryPos.add(classPK);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(groupWebhook)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<GroupWebhook> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 */
	@Override
	public void removeByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK) {

		for (GroupWebhook groupWebhook :
				findByGroupId_Enabled_ClassName_ClassPK(
					groupId, enabled, className, classPK, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(groupWebhook);
		}
	}

	/**
	 * Returns the number of group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @return the number of matching group webhooks
	 */
	@Override
	public int countByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK) {

		className = Objects.toString(className, "");

		FinderPath finderPath =
			_finderPathCountByGroupId_Enabled_ClassName_ClassPK;

		Object[] finderArgs = new Object[] {
			groupId, enabled, className, classPK
		};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(5);

			sb.append(_SQL_COUNT_GROUPWEBHOOK_WHERE);

			sb.append(
				_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_GROUPID_2);

			sb.append(
				_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_ENABLED_2);

			boolean bindClassName = false;

			if (className.isEmpty()) {
				sb.append(
					_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSNAME_3);
			}
			else {
				bindClassName = true;

				sb.append(
					_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSNAME_2);
			}

			sb.append(
				_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSPK_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				queryPos.add(enabled);

				if (bindClassName) {
					queryPos.add(className);
				}

				queryPos.add(classPK);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_GROUPID_2 =
			"groupWebhook.groupId = ? AND ";

	private static final String
		_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_ENABLED_2 =
			"groupWebhook.enabled = ? AND ";

	private static final String
		_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSNAME_2 =
			"groupWebhook.className = ? AND ";

	private static final String
		_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSNAME_3 =
			"(groupWebhook.className IS NULL OR groupWebhook.className = '') AND ";

	private static final String
		_FINDER_COLUMN_GROUPID_ENABLED_CLASSNAME_CLASSPK_CLASSPK_2 =
			"groupWebhook.classPK = ?";

	public GroupWebhookPersistenceImpl() {
		setModelClass(GroupWebhook.class);

		setModelImplClass(GroupWebhookImpl.class);
		setModelPKClass(long.class);

		setTable(GroupWebhookTable.INSTANCE);
	}

	/**
	 * Caches the group webhook in the entity cache if it is enabled.
	 *
	 * @param groupWebhook the group webhook
	 */
	@Override
	public void cacheResult(GroupWebhook groupWebhook) {
		entityCache.putResult(
			GroupWebhookImpl.class, groupWebhook.getPrimaryKey(), groupWebhook);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the group webhooks in the entity cache if it is enabled.
	 *
	 * @param groupWebhooks the group webhooks
	 */
	@Override
	public void cacheResult(List<GroupWebhook> groupWebhooks) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (groupWebhooks.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (GroupWebhook groupWebhook : groupWebhooks) {
			if (entityCache.getResult(
					GroupWebhookImpl.class, groupWebhook.getPrimaryKey()) ==
						null) {

				cacheResult(groupWebhook);
			}
		}
	}

	/**
	 * Clears the cache for all group webhooks.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(GroupWebhookImpl.class);

		finderCache.clearCache(GroupWebhookImpl.class);
	}

	/**
	 * Clears the cache for the group webhook.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(GroupWebhook groupWebhook) {
		entityCache.removeResult(GroupWebhookImpl.class, groupWebhook);
	}

	@Override
	public void clearCache(List<GroupWebhook> groupWebhooks) {
		for (GroupWebhook groupWebhook : groupWebhooks) {
			entityCache.removeResult(GroupWebhookImpl.class, groupWebhook);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(GroupWebhookImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(GroupWebhookImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new group webhook with the primary key. Does not add the group webhook to the database.
	 *
	 * @param webhookId the primary key for the new group webhook
	 * @return the new group webhook
	 */
	@Override
	public GroupWebhook create(long webhookId) {
		GroupWebhook groupWebhook = new GroupWebhookImpl();

		groupWebhook.setNew(true);
		groupWebhook.setPrimaryKey(webhookId);

		groupWebhook.setCompanyId(CompanyThreadLocal.getCompanyId());

		return groupWebhook;
	}

	/**
	 * Removes the group webhook with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook that was removed
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	@Override
	public GroupWebhook remove(long webhookId)
		throws NoSuchGroupWebhookException {

		return remove((Serializable)webhookId);
	}

	/**
	 * Removes the group webhook with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the group webhook
	 * @return the group webhook that was removed
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	@Override
	public GroupWebhook remove(Serializable primaryKey)
		throws NoSuchGroupWebhookException {

		Session session = null;

		try {
			session = openSession();

			GroupWebhook groupWebhook = (GroupWebhook)session.get(
				GroupWebhookImpl.class, primaryKey);

			if (groupWebhook == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchGroupWebhookException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(groupWebhook);
		}
		catch (NoSuchGroupWebhookException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected GroupWebhook removeImpl(GroupWebhook groupWebhook) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(groupWebhook)) {
				groupWebhook = (GroupWebhook)session.get(
					GroupWebhookImpl.class, groupWebhook.getPrimaryKeyObj());
			}

			if (groupWebhook != null) {
				session.delete(groupWebhook);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (groupWebhook != null) {
			clearCache(groupWebhook);
		}

		return groupWebhook;
	}

	@Override
	public GroupWebhook updateImpl(GroupWebhook groupWebhook) {
		boolean isNew = groupWebhook.isNew();

		if (!(groupWebhook instanceof GroupWebhookModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(groupWebhook.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					groupWebhook);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in groupWebhook proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom GroupWebhook implementation " +
					groupWebhook.getClass());
		}

		GroupWebhookModelImpl groupWebhookModelImpl =
			(GroupWebhookModelImpl)groupWebhook;

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(groupWebhook);
			}
			else {
				groupWebhook = (GroupWebhook)session.merge(groupWebhook);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			GroupWebhookImpl.class, groupWebhookModelImpl, false, true);

		if (isNew) {
			groupWebhook.setNew(false);
		}

		groupWebhook.resetOriginalValues();

		return groupWebhook;
	}

	/**
	 * Returns the group webhook with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the group webhook
	 * @return the group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	@Override
	public GroupWebhook findByPrimaryKey(Serializable primaryKey)
		throws NoSuchGroupWebhookException {

		GroupWebhook groupWebhook = fetchByPrimaryKey(primaryKey);

		if (groupWebhook == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchGroupWebhookException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return groupWebhook;
	}

	/**
	 * Returns the group webhook with the primary key or throws a <code>NoSuchGroupWebhookException</code> if it could not be found.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	@Override
	public GroupWebhook findByPrimaryKey(long webhookId)
		throws NoSuchGroupWebhookException {

		return findByPrimaryKey((Serializable)webhookId);
	}

	/**
	 * Returns the group webhook with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook, or <code>null</code> if a group webhook with the primary key could not be found
	 */
	@Override
	public GroupWebhook fetchByPrimaryKey(long webhookId) {
		return fetchByPrimaryKey((Serializable)webhookId);
	}

	/**
	 * Returns all the group webhooks.
	 *
	 * @return the group webhooks
	 */
	@Override
	public List<GroupWebhook> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of group webhooks
	 */
	@Override
	public List<GroupWebhook> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of group webhooks
	 */
	@Override
	public List<GroupWebhook> findAll(
		int start, int end, OrderByComparator<GroupWebhook> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of group webhooks
	 */
	@Override
	public List<GroupWebhook> findAll(
		int start, int end, OrderByComparator<GroupWebhook> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<GroupWebhook> list = null;

		if (useFinderCache) {
			list = (List<GroupWebhook>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_GROUPWEBHOOK);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_GROUPWEBHOOK;

				sql = sql.concat(GroupWebhookModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<GroupWebhook>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the group webhooks from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (GroupWebhook groupWebhook : findAll()) {
			remove(groupWebhook);
		}
	}

	/**
	 * Returns the number of group webhooks.
	 *
	 * @return the number of group webhooks
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_GROUPWEBHOOK);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "webhookId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_GROUPWEBHOOK;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return GroupWebhookModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the group webhook persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"groupId"}, true);

		_finderPathWithoutPaginationFindByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			true);

		_finderPathCountByGroupId = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			false);

		_finderPathWithPaginationFindByGroupId_Enabled = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId_Enabled",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"groupId", "enabled"}, true);

		_finderPathWithoutPaginationFindByGroupId_Enabled = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId_Enabled",
			new String[] {Long.class.getName(), Boolean.class.getName()},
			new String[] {"groupId", "enabled"}, true);

		_finderPathCountByGroupId_Enabled = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId_Enabled",
			new String[] {Long.class.getName(), Boolean.class.getName()},
			new String[] {"groupId", "enabled"}, false);

		_finderPathWithPaginationFindByGroupId_Enabled_ClassName_ClassPK =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
				"findByGroupId_Enabled_ClassName_ClassPK",
				new String[] {
					Long.class.getName(), Boolean.class.getName(),
					String.class.getName(), Long.class.getName(),
					Integer.class.getName(), Integer.class.getName(),
					OrderByComparator.class.getName()
				},
				new String[] {"groupId", "enabled", "className", "classPK"},
				true);

		_finderPathWithoutPaginationFindByGroupId_Enabled_ClassName_ClassPK =
			new FinderPath(
				FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
				"findByGroupId_Enabled_ClassName_ClassPK",
				new String[] {
					Long.class.getName(), Boolean.class.getName(),
					String.class.getName(), Long.class.getName()
				},
				new String[] {"groupId", "enabled", "className", "classPK"},
				true);

		_finderPathCountByGroupId_Enabled_ClassName_ClassPK = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByGroupId_Enabled_ClassName_ClassPK",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				String.class.getName(), Long.class.getName()
			},
			new String[] {"groupId", "enabled", "className", "classPK"}, false);

		GroupWebhookUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		GroupWebhookUtil.setPersistence(null);

		entityCache.removeCache(GroupWebhookImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_GroupRestPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_GroupRestPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_GroupRestPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_GROUPWEBHOOK =
		"SELECT groupWebhook FROM GroupWebhook groupWebhook";

	private static final String _SQL_SELECT_GROUPWEBHOOK_WHERE =
		"SELECT groupWebhook FROM GroupWebhook groupWebhook WHERE ";

	private static final String _SQL_COUNT_GROUPWEBHOOK =
		"SELECT COUNT(groupWebhook) FROM GroupWebhook groupWebhook";

	private static final String _SQL_COUNT_GROUPWEBHOOK_WHERE =
		"SELECT COUNT(groupWebhook) FROM GroupWebhook groupWebhook WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "groupWebhook.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No GroupWebhook exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No GroupWebhook exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		GroupWebhookPersistenceImpl.class);

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}