
import {openConfirmModal, navigate} from 'frontend-js-web';

export default function propsTransformer({
	additionalProps: {
		deleteEntriesURL,
		searchContainerName,
		entriesIdsParameterName,
		deleteEntriesActionName
	},
	portletNamespace,
	...otherProps
}) {
	const deleteEntries = () => {
		
		openConfirmModal({
			message: Liferay.Language.get(
				'are-you-sure-you-want-to-delete-this'
			),
			onConfirm: (isConfirmed) => {
				if (isConfirmed) {
					const url = new URL(deleteEntriesURL);

					const searchContainer = Liferay.SearchContainer.get(
						`${portletNamespace}${searchContainerName}`
					);

					const keys = searchContainer.select
						.getAllSelectedElements()
						.get('value');

					url.searchParams.set(
							`${portletNamespace}${entriesIdsParameterName}`,
							keys
					);
					
					navigate(url);
				}
			},
		});
	};

	return {
		...otherProps,
		onActionButtonClick(event, {item}) {
			const action = item?.data?.action;

			if (action === deleteEntriesActionName) {
				deleteEntries();
			}
		}
	};
}
