package com.placecube.digitalplace.userimport.admin.web.application.list;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.model.Portlet;
import com.placecube.digitalplace.userimport.admin.web.constants.PortletKeys;

@Component(immediate = true, property = { "panel.app.order:Integer=600", "panel.category.key=" + PanelCategoryKeys.CONTROL_PANEL_USERS }, service = PanelApp.class)
public class UserImportPanelApp extends BasePanelApp {

	@Reference(target = "(javax.portlet.name=" + PortletKeys.USER_IMPORT + ")", unbind = "-")
	private Portlet portlet;

	@Override
	public Portlet getPortlet() {
		return portlet;
	}

	@Override
	public String getPortletId() {
		return PortletKeys.USER_IMPORT;
	}

}