package com.placecube.digitalplace.addresslookup.mock.national.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.addresslookup.mock.national.configuration.MockAddressNationalLookupCompanyConfiguration", localization = "content/Language", name = "address-national-lookup-mock")
public interface MockAddressNationalLookupCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "true", name = "national", description = "national-description")
	boolean national();

	@Meta.AD(required = false, deflt = "1", name = "weight", description = "weight-description")
	Integer weight();

}
