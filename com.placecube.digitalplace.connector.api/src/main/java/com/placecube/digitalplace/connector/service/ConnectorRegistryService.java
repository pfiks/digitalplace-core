package com.placecube.digitalplace.connector.service;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.placecube.digitalplace.connector.model.Connector;
import com.placecube.digitalplace.connector.model.ConnectorRegistryContext;

public interface ConnectorRegistryService {

	ConnectorRegistryContext createConnectorRegistryContext(String connectorType, String connectorClassName);

	Optional<ServiceWrapper<Connector>> getConnector(ConnectorRegistryContext connectorRegistryContext);

	Map<String, String> getConnectorNames(String interfaceClassName, Locale locale);

	List<ServiceWrapper<Connector>> getConnectors(String interfaceClassName);
}
