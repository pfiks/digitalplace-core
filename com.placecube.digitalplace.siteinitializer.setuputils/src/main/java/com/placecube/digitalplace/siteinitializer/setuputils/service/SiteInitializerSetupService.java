package com.placecube.digitalplace.siteinitializer.setuputils.service;

import java.net.URL;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.site.initializer.SiteInitializer;
import com.liferay.site.initializer.SiteInitializerRegistry;
import com.placecube.digitalplace.siteinitializer.setuputils.internal.utils.SiteInitializerBackupService;
import com.placecube.digitalplace.siteinitializer.setuputils.internal.utils.SiteInitializerUtils;

@Component(immediate = true, service = SiteInitializerSetupService.class)
public class SiteInitializerSetupService {

	private static final Log LOG = LogFactoryUtil.getLog(SiteInitializerSetupService.class);

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private SiteInitializerBackupService siteInitializerBackupService;

	@Reference
	private SiteInitializerRegistry siteInitializerRegistry;

	@Reference
	private SiteInitializerUtils siteInitializerUtils;

	@Reference
	private UserLocalService userLocalService;

	/**
	 * Updates the site by configuring additional details - It supports updating
	 * content layouts by updating the TargetCollections in fragments.
	 *
	 * Accepts the following json format
	 *
	 * {"assetListEntryUsagesReplacements":[{"layoutUrl":"/yourLayoutUrl","privateLayout":true,"targetCollectionPlaceholderValue":"thePlaceholderToReplace","assetListEntryUsageKey":"theAssetListEntryUsageKeyToUseToPopulateThePlaceholder"}]}
	 *
	 * @param userId the userId making the changes
	 * @param groupId the groupId
	 * @param jsonObject the jsonObject with the additionalData to configure
	 * @throws PortalException any exception configuring the additional data
	 */
	public void configureAdditionalSiteData(long userId, long groupId, JSONObject jsonObject) throws PortalException {
		if (Validator.isNotNull(jsonObject)) {
			siteInitializerUtils.configureAdditionalData(userId, groupId, jsonObject);
		}
	}

	/**
	 * It will create a new DispatchTrigger if none is found for the specified
	 * externalReferenceCode.
	 *
	 * The supported JSONArray format is:
	 *
	 * [{ "externalReferenceCode":"", "taskExecutorType":"", "name":"",
	 * "settings": { "setting1":"value1" } }]
	 *
	 * @param schedulersToCreateJSONArray the JSONArray containing the
	 *            DispatchTriggers to create
	 * @param serviceContext the service context
	 * @throws PortalException any exception creating the triggers
	 */
	public void createJobSchedulerTriggers(JSONArray schedulersToCreateJSONArray, ServiceContext serviceContext) throws PortalException {
		for (int i = 0; i < schedulersToCreateJSONArray.length(); i++) {
			JSONObject schedulerToCreateJSONObject = schedulersToCreateJSONArray.getJSONObject(i);

			siteInitializerUtils.createMissingDispatchTrigger(schedulerToCreateJSONObject.getString("externalReferenceCode"), schedulerToCreateJSONObject.getString("taskExecutorType"),
					schedulerToCreateJSONObject.getString("name"), schedulerToCreateJSONObject.getJSONObject("settings"), serviceContext);
		}
	}

	/**
	 * Returns a clone of the current service context, with the groupId and
	 * userId details updated. It also sets group and guest permissions to true.
	 * It will use the user from the PrincipalThreadLocal
	 *
	 * @param groupId the groupId
	 * @return the ServiceContext
	 * @throws PortalException exception creating the service context copy
	 */
	public ServiceContext getServiceContext(long groupId) throws PortalException {
		User user = userLocalService.getUser(PrincipalThreadLocal.getUserId());

		return siteInitializerUtils.getServiceContext(user, groupId);
	}

	/**
	 * Returns a clone of the current service context, with the groupId and
	 * userId details updated. It also sets group and guest permissions to true.
	 *
	 * @param user the user
	 * @param groupId the groupId
	 * @return the ServiceContext
	 * @throws PortalException exception creating the service context copy
	 */
	public ServiceContext getServiceContext(User user, long groupId) throws PortalException {
		return siteInitializerUtils.getServiceContext(user, groupId);
	}

	/**
	 * Runs the Site initialiser for the specified siteInitialiserKey in the
	 * groupId. It wraps the liferay calls in order to backup existing data that
	 * would otherwise be deleted by the liferay's site initialiser run. e.g.
	 * existing navigation menus. This is to allow administrators to run
	 * multiple site initialisers in the same group.
	 *
	 * @param userId the userId making the changes
	 * @param groupId the group where to run the initialiser
	 * @param siteInitializerkey the initialiser to run
	 * @throws PortalException any exception running the initialiser
	 */
	public void runSiteInitialiser(long userId, long groupId, String siteInitializerkey) throws PortalException {
		runSiteInitialiser(userId, groupId, siteInitializerkey, Optional.empty());
	}

	/**
	 * Runs the Site initialiser for the specified siteInitialiserKey in the
	 * groupId. It wraps the liferay calls in order to backup existing data that
	 * would otherwise be deleted by the liferay's site initialiser run. e.g.
	 * existing navigation menus. This is to allow administrators to run
	 * multiple site initialisers in the same group.
	 *
	 * Once the initialiser has run, it will setup the additional data as
	 * detailed in the setupDataFilePostSetup
	 *
	 * @param userId the userId making the changes
	 * @param groupId the group where to run the initialiser
	 * @param siteInitializerkey the initialiser to run
	 * @param additionalSetupDataFilePostRun the file containing the additional
	 *            setup to be done after the initialiser has run
	 * @throws PortalException any exception running the initialiser
	 */
	public void runSiteInitialiser(long userId, long groupId, String siteInitializerkey, Optional<URL> additionalSetupDataFilePostRun) throws PortalException {
		long backupGroupId = 0l;
		long currentGroupId = 0l;

		try {
			SiteInitializer siteInitializer = siteInitializerRegistry.getSiteInitializer(siteInitializerkey);

			if (null == siteInitializer) {
				throw new PortalException("No site initializer found for key: " + siteInitializerkey);
			}

			currentGroupId = groupId;
			backupGroupId = siteInitializerBackupService.backupContentInTemporaryGroup(currentGroupId);

			siteInitializer.initialize(currentGroupId);

			siteInitializerUtils.configureAdditionalData(userId, groupId, additionalSetupDataFilePostRun);

		} catch (Exception e) {
			LOG.error(e);
			throw new PortalException(e);

		} finally {
			siteInitializerBackupService.importBackedUpContentAndRemoveTemporaryGroup(currentGroupId, backupGroupId);
		}
	}

	/**
	 * It processes each entry and creates the expando value for the layout by
	 * replacing the placeholders if no fieldValue is specified
	 *
	 * The supported JSONArray format is:
	 *
	 * [{"private":false,"friendlyURL":"/test","expandoFields":[{"fieldName":"expandoFieldName","fieldValue":"","fieldType":
	 * 15, "placeholderValue":"[$YourPlaceholder$]"}]}]
	 *
	 * @param pagesToUpdateJSONArray the pages to update
	 * @param placeholdersValues the placeholder values
	 * @param serviceContext the service context
	 * @throws PortalException any exception updating the expando fields
	 */
	public void updateExpandoFieldsForPages(JSONArray pagesToUpdateJSONArray, Map<String, String> placeholdersValues, ServiceContext serviceContext) throws PortalException {
		for (int i = 0; i < pagesToUpdateJSONArray.length(); i++) {
			JSONObject pageToUpdateJSONObject = pagesToUpdateJSONArray.getJSONObject(i);
			JSONArray expandoFieldsJSONArray = pageToUpdateJSONObject.getJSONArray("expandoFields");

			Layout layout = siteInitializerUtils.getLayout(serviceContext, pageToUpdateJSONObject);
			siteInitializerUtils.addExpandoValues(layout, expandoFieldsJSONArray, placeholdersValues);

			Layout draftLayout = layoutLocalService.fetchDraftLayout(layout.getPlid());
			siteInitializerUtils.addExpandoValues(draftLayout, expandoFieldsJSONArray, placeholdersValues);
		}
	}

	/**
	 * It processes each entry and update the portletPreferenceValue by
	 * replacing the placeholders
	 *
	 * The supported JSONArray format is:
	 *
	 * [{"private":false,"friendlyURL":"/test","layoutName":"","placeholdersToReplace":[{"portletId":"com_liferay_dynamic_data_mapping_form_web_portlet_DDMFormPortlet","portletInstanceId":
	 * "commerceHeaderMainNavigation","preferenceName":"formInstanceId","placeholderValue":"[$YourPlaceholder$]"}]}]
	 *
	 * @param pagesToUpdateJSONArray the pages to update
	 * @param placeholdersValues the placeholder values
	 * @param serviceContext the service context
	 * @throws PortalException any exception updating the portlet preferences
	 */
	public void updatePortletPreferencesForPages(JSONArray pagesToUpdateJSONArray, Map<String, String> placeholdersValues, ServiceContext serviceContext) throws PortalException {
		for (int i = 0; i < pagesToUpdateJSONArray.length(); i++) {
			JSONObject pageToUpdateJSONObject = pagesToUpdateJSONArray.getJSONObject(i);
			JSONArray placeholdersToReplaceJSONArray = pageToUpdateJSONObject.getJSONArray("placeholdersToReplace");

			Layout layout = siteInitializerUtils.getLayout(serviceContext, pageToUpdateJSONObject);
			siteInitializerUtils.updatePortletPreferencesWithPlaceholders(layout, placeholdersToReplaceJSONArray, placeholdersValues);
			siteInitializerUtils.publishLayout(layout);

			Layout draftLayout = layoutLocalService.fetchDraftLayout(layout.getPlid());
			siteInitializerUtils.updatePortletPreferencesWithPlaceholders(draftLayout, placeholdersToReplaceJSONArray, placeholdersValues);
			siteInitializerUtils.publishLayout(draftLayout);
		}
	}

}
