package com.placecube.digitalplace.search.shared.service;

import javax.portlet.PortletRequest;

import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;

public interface SharedSearchDisplayService {

	String getDisplayStyle(PortletRequest portletRequest, String defaultStyle);

	String getDisplayStyleGridURL(SharedSearchResponse searchResponse);

	String getDisplayStyleListURL(SharedSearchResponse searchResponse);

	boolean isDisplayStyleList(PortletRequest portletRequest);

	boolean isDisplayStyleList(PortletRequest portletRequest, String defaultValue);

}
