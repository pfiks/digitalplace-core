package com.placecube.digitalplace.appointment.web.model;

import java.util.Date;

public class AppointmentDashboardModel {

	private final long id;

	private final Date startDate;

	private final String summary;

	private final String viewUrl;

	public AppointmentDashboardModel(long id, Date startDate, String summary, String viewUrl) {
		this.id = id;
		this.startDate = startDate;
		this.summary = summary;
		this.viewUrl = viewUrl;
	}

	public long getId() {
		return id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public String getSummary() {
		return summary;
	}

	public String getViewUrl() {
		return viewUrl;
	}

}
