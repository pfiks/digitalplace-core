package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration;

public final class DDLFieldFacetPortletConfigurationConstants {

	public static final String ALPHA_SORT = "alphaSort";

	public static final String CUSTOM_HEADING = "customHeading";

	public static final String DDL_RECORD_LINK_FIELD_NAME = "ddlRecordLinkFieldName";

	public static final String DDM_LINK_STRUCTURE_ID = "ddmLinkStructureId";

	public static final String DISPLAY_STYLE = "displayStyle";

	public static final String FIELD_NAME = "ddmFieldName";

	public static final String FILTER_COLOUR = "filterColour";

	public static final String INVISIBLE = "invisible";

	public static final String IS_FIELD_NAME_DDL_RECORD_LINK = "isFieldNameDDLRecordLink";

	public static final String PRESELECTED_VALUE = "preselectedValue";

	public static final String STRUCTURE_ID = "ddmStructureId";

	public static final String SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION = "showHeadingAsFirstDropdownOption";

	private DDLFieldFacetPortletConfigurationConstants() {
	}

}
