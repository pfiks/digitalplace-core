package com.placecube.digitalplace.common.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, PortalUtil.class })
public class PortletURLServiceTest {

	@InjectMocks
	private PortletURLService portletURLService;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletRequest mockOriginalHttpServletRequest;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class);
	}

	@Test
	public void isEditLayoutMode_WhenUrlModeParameterValueIsEdit_ThenReturnsTrue() {
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockOriginalHttpServletRequest);
		when(ParamUtil.getString(mockOriginalHttpServletRequest, "p_l_mode", Constants.VIEW)).thenReturn(Constants.EDIT);

		boolean result = portletURLService.isEditLayoutMode(mockPortletRequest);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isEditLayoutMode_WhenUrlModeParameterValueIsNotEdit_ThenReturnsFalse() {
		when(mockPortal.getHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockOriginalHttpServletRequest);
		when(ParamUtil.getString(mockOriginalHttpServletRequest, "p_l_mode", Constants.VIEW)).thenReturn(Constants.VIEW);

		boolean result = portletURLService.isEditLayoutMode(mockPortletRequest);

		assertThat(result, equalTo(false));
	}

	@Test
	public void getURLWithRedirectParameter_WhenCurrentURLIsPresentAndViewURLHasQueryParams_ThenReturnsRedirectParamWithAmpersandCharacterAppendedToViewURL() {
		String viewURL = "https://site.com/subpage?param1=param";
		String currentURL = "https://site.com/home";

		when(PortalUtil.escapeRedirect(currentURL)).thenReturn(currentURL);
		String result = portletURLService.getURLWithRedirectParameter(viewURL, currentURL);

		assertThat(result, equalTo(viewURL + "&redirect=" + currentURL));
	}

	@Test
	public void getURLWithRedirectParameter_WhenCurrentURLIsPresentAndViewURLHasNoQueryParams_ThenReturnsRedirectParamWithQuestionMarkCharacterAppendedToViewURL() {
		String viewURL = "https://site.com/subpage";
		String currentURL = "https://site.com/home";

		when(PortalUtil.escapeRedirect(currentURL)).thenReturn(currentURL);
		String result = portletURLService.getURLWithRedirectParameter(viewURL, currentURL);

		assertThat(result, equalTo(viewURL + "?redirect=" + currentURL));
	}

	@Test
	public void getURLWithRedirectParameter_WhenViewURLIsNotURI_ThenReturnsViewURL() {
		String viewURL = "[viewURL]";
		String currentURL = "https://site.com/home";

		String result = portletURLService.getURLWithRedirectParameter(viewURL, currentURL);

		assertThat(result, equalTo(viewURL));
	}

	@Test
	public void getURLWithRedirectParameter_WhenCurrentURLIsNull_ThenReturnsViewURL() {
		String viewURL = "https://site.com/subpage";

		String result = portletURLService.getURLWithRedirectParameter(viewURL, null);

		assertThat(result, equalTo(viewURL));
	}
}
