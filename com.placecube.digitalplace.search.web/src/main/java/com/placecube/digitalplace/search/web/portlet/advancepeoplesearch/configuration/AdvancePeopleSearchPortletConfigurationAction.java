package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration;

import static com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.UserProfileFacetPortletConfigurationConstants.APPLY_PRIVACY;
import static com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.UserProfileFacetPortletConfigurationConstants.FILTER_COLOUR;
import static com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.UserProfileFacetPortletConfigurationConstants.SEARCH_FIELDS;
import static com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.UserProfileFacetPortletConfigurationConstants.TITLE;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.ADVANCE_PEOPLE_SEARCH, service = ConfigurationAction.class)
public class AdvancePeopleSearchPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		AdvancePeopleSearchPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(AdvancePeopleSearchPortletInstanceConfiguration.class, themeDisplay);

		httpServletRequest.setAttribute(APPLY_PRIVACY, configuration.applyPrivacy());

		httpServletRequest.setAttribute(FILTER_COLOUR, configuration.filterColour());

		httpServletRequest.setAttribute(SEARCH_FIELDS, configuration.searchFields());

		String titleXML = LocalizationUtil.updateLocalization(configuration.title().getValues(), StringPool.BLANK, TITLE, LocaleUtil.toLanguageId(LocaleUtil.getSiteDefault()));
		httpServletRequest.setAttribute(TITLE, titleXML);

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		PortletPreferences preferences = actionRequest.getPreferences();

		boolean applyPrivacy = ParamUtil.getBoolean(actionRequest, APPLY_PRIVACY);
		setPreference(actionRequest, APPLY_PRIVACY, String.valueOf(applyPrivacy));

		String filterColour = ParamUtil.getString(actionRequest, FILTER_COLOUR, StringPool.BLANK);
		setPreference(actionRequest, FILTER_COLOUR, filterColour);

		String searchFields = ParamUtil.getString(actionRequest, SEARCH_FIELDS);
		setPreference(actionRequest, SEARCH_FIELDS, searchFields);

		LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "title");

		super.processAction(portletConfig, actionRequest, actionResponse);
	}
}
