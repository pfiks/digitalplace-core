package com.placecube.digitalplace.dataconsumer.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.data.provider.DDMDataProvider;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderRegistry;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializer;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializerDeserializeRequest;
import com.liferay.dynamic.data.mapping.io.DDMFormValuesDeserializerDeserializeResponse;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.util.DDMFormFactory;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.dataconsumer.model.DataProviderInputParameter;
import com.placecube.digitalplace.dataconsumer.model.DataProviderOutputParameter;
import com.placecube.digitalplace.dataconsumer.model.impl.DataProviderInputParameterImpl;
import com.placecube.digitalplace.dataconsumer.model.impl.DataProviderOutputParameterImpl;
import com.placecube.digitalplace.dataconsumer.service.DataTransformationService;

@Component(immediate = true, service = { DataTransformationService.class })
public class DataTransformationServiceImpl implements DataTransformationService {

	@Reference
	private DDMDataProviderRegistry ddmDataProviderRegistry;

	@Reference(target = "(ddm.form.values.deserializer.type=json)")
	private DDMFormValuesDeserializer jsonDDMFormValuesDeserializer;

	@Reference
	private JSONFactory jsonFactory;

	@Override
	public Map<String, String> deserializeMapping(String mappingJson) throws JSONException {
		JSONObject json = jsonFactory.createJSONObject(mappingJson);

		Map<String, String> result = new HashMap<>();
		json.keySet().forEach(k -> {
			result.put(k, json.getString(k));
		});

		return result;
	}

	@Override
	public DDMFormValues getDataProviderFormValues(DDMDataProviderInstance dataProviderInstance) {
		DDMForm dataProviderForm = getDataProviderInstanceSettingsDDMForm(dataProviderInstance);

		return deserialize(dataProviderInstance.getDefinition(), dataProviderForm);
	}

	@Override
	public List<DataProviderInputParameter> getInputParameters(DDMFormValues values, Locale locale) {
		List<DataProviderInputParameter> inputFields = new ArrayList<>();
		values.getDDMFormFieldValuesMap(false).get("inputParameters").forEach(ip -> {
			inputFields.add(getDataProviderInputParameterFromNestedValues(ip, locale));
		});

		return inputFields;
	}

	@Override
	public List<DataProviderOutputParameter> getOutputParameters(DDMFormValues values, Locale locale) {
		List<DataProviderOutputParameter> inputFields = new ArrayList<>();
		values.getDDMFormFieldValuesMap(false).get("outputParameters").forEach(ip -> {
			inputFields.add(getDataProviderOutputParameterFromNestedValues(ip, locale));
		});

		return inputFields;
	}

	@Override
	public String serializeMapping(Map<String, String> paramMap) {
		JSONObject mapping = jsonFactory.createJSONObject();
		paramMap.entrySet().forEach(p -> {
			mapping.put(p.getKey(), p.getValue());
		});

		return mapping.toJSONString();
	}

	@Override
	public String getDefaultInputParametersJSONMapping(List<DataProviderInputParameter> inputFields) {
		Map<String, String> inputParametersPlaceholdersMap = inputFields.stream().filter(p -> Validator.isNotNull(p.getName()))
				.collect(Collectors.toMap(DataProviderInputParameter::getName, p -> getInputParameterPlaceholder(p.getName()), (existing, replacement) -> existing));

		return inputParametersPlaceholdersMap.isEmpty() ? StringPool.BLANK : serializeMapping(inputParametersPlaceholdersMap);
	}

	private String getInputParameterPlaceholder(String inputParameter) {
		return "$" + inputParameter + "$";
	}

	// copied from LR codebase
	private DDMFormValues deserialize(String content, DDMForm ddmForm) {
		DDMFormValuesDeserializerDeserializeRequest.Builder builder = DDMFormValuesDeserializerDeserializeRequest.Builder.newBuilder(content, ddmForm);

		DDMFormValuesDeserializerDeserializeResponse ddmFormValuesDeserializerDeserializeResponse = jsonDDMFormValuesDeserializer.deserialize(builder.build());

		return ddmFormValuesDeserializerDeserializeResponse.getDDMFormValues();
	}

	private DataProviderInputParameter getDataProviderInputParameterFromNestedValues(DDMFormFieldValue nestedValue, Locale locale) {
		DataProviderInputParameterImpl dataProviderInputParameter = new DataProviderInputParameterImpl();
		nestedValue.getNestedDDMFormFieldValues().forEach(value -> {

			if (value.getName().equals("inputParameterLabel")) {
				dataProviderInputParameter.setLabel(value.getValue().getString(locale));
			}

			if (value.getName().equals("inputParameterName")) {
				dataProviderInputParameter.setName(value.getValue().getString(locale));
			}

			if (value.getName().equals("inputParameterRequired")) {
				dataProviderInputParameter.setRequired(Boolean.parseBoolean(value.getValue().getString(locale)));
			}

			if (value.getName().equals("inputParameterType")) {
				dataProviderInputParameter.setType(value.getValue().getString(locale));
			}
		});

		return dataProviderInputParameter;
	}

	// copied from LR codebase
	private DDMForm getDataProviderInstanceSettingsDDMForm(DDMDataProviderInstance dataProviderInstance) {

		DDMDataProvider ddmDataProvider = ddmDataProviderRegistry.getDDMDataProvider(dataProviderInstance.getType());

		Class<?> clazz = ddmDataProvider.getSettings();

		return DDMFormFactory.create(clazz);
	}

	private DataProviderOutputParameter getDataProviderOutputParameterFromNestedValues(DDMFormFieldValue nestedValue, Locale locale) {
		DataProviderOutputParameterImpl dataProviderOutputParameter = new DataProviderOutputParameterImpl();
		nestedValue.getNestedDDMFormFieldValues().forEach(value -> {

			if (value.getName().equals("outputParameterPath")) {
				dataProviderOutputParameter.setPath(value.getValue().getString(locale));
			}

			if (value.getName().equals("outputParameterName")) {
				dataProviderOutputParameter.setName(value.getValue().getString(locale));
			}

			if (value.getName().equals("outputParameterType")) {
				dataProviderOutputParameter.setType(value.getValue().getString(locale));
			}
		});

		return dataProviderOutputParameter;
	}

}
