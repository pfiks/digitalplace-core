package com.placecube.digitalplace.search.web.portlet.searchbar;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.model.impl.SortByScore;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class KeywordSearchMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private KeywordSearchMVCActionCommand keywordSearchMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SortByScore mockSortByScore;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenKeywordsAreNotSpecified_ThenRemovesTheKeywordsAsSingleValuedSharedSearchSessionAttributesAndExecutingTheRedirectWithoutClearingTheSharedSession() throws Exception {
		String expected = StringPool.THREE_SPACES;
		when(ParamUtil.getString(mockActionRequest, SearchParameters.SEARCH_KEYWORD)).thenReturn(expected);

		keywordSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, SearchParameters.SEARCH_KEYWORD);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
		verify(mockSharedSearchRequestService, never()).clearSharedSearchSession(any(ActionRequest.class));
	}

	@Test
	public void doProcessAction_WhenKeywordsAreSpecified_ThenClearsTheSharedSessionBeforeSettingTheSortByScoreIdAndTheKeywordsAsSingleValuedSharedSearchSessionAttributesAndExecutingTheRedirect()
			throws Exception {
		String expected = "expectedVal";
		String mockSortByScoreId = "mockSortByScoreId";

		when(ParamUtil.getString(mockActionRequest, SearchParameters.SEARCH_KEYWORD)).thenReturn(expected);
		when(mockSortByScore.getId()).thenReturn(mockSortByScoreId);

		keywordSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).clearSharedSearchSession(mockActionRequest);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, SearchParameters.SEARCH_KEYWORD, expected);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, SearchParameters.SELECTED_SORT_BY, mockSortByScoreId);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

}
