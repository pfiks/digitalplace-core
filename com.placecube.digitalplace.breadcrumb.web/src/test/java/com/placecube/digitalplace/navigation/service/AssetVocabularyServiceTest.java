package com.placecube.digitalplace.navigation.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.placecube.digitalplace.navigation.constants.ConfigurationConstants;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AssetVocabularyServiceTest {

	private static long GROUP_ID = 1;

	@InjectMocks
	private AssetVocabularyService assetVocabularyService;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private AssetVocabulary mockAssetVocabulary;

	@Test
	@Parameters({ "null", "-1" })
	public void getVocabularyId_WhenNoError_ThenReturnsDefaultVocabularyId(String vocabularyId) {

		long result = assetVocabularyService.getVocabularyId(GROUP_ID, vocabularyId);

		assertThat(result, equalTo(Long.valueOf(ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID)));

	}

	@Test
	public void getVocabularyId_WhenVocabularyIdIsNotEqualToDefaultOne_ThenReturnsVocabularyId() {

		String vocabularyId = "1";

		long result = assetVocabularyService.getVocabularyId(GROUP_ID, vocabularyId);

		assertThat(result, equalTo(Long.valueOf(vocabularyId)));

	}

	@Test
	public void getVocabularyId_WhenDefaultVocabularyNameDoesNotExistAndVocabularyIdIsTheDefaultOne_ThenReturnsDefaultVocabularyId() {

		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(GROUP_ID, ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_NAME)).thenReturn(null);

		long result = assetVocabularyService.getVocabularyId(GROUP_ID, ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID);

		assertThat(result, equalTo(Long.valueOf(ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID)));

	}

	@Test
	public void getVocabularyId_WhenDefaultVocabularyNameExistsAndVocabularyIdIsTheDefaultOne_ThenReturnsVocabularyId() {

		when(mockAssetVocabularyLocalService.fetchGroupVocabulary(GROUP_ID, ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_NAME)).thenReturn(mockAssetVocabulary);
		long vocabularyId = 2;
		when(mockAssetVocabulary.getVocabularyId()).thenReturn(vocabularyId);

		long result = assetVocabularyService.getVocabularyId(GROUP_ID, ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID);

		assertThat(result, equalTo(Long.valueOf(vocabularyId)));

	}

}
