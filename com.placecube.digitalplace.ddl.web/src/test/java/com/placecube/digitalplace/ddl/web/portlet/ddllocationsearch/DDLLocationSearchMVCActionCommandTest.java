package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.ddl.web.constants.LocationSearchParameters;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class DDLLocationSearchMVCActionCommandTest extends PowerMockito {

	private static final String LATITUDE = "10.000";
	private static final String LONGITUDE = "-2.000";
	private static final String ADDRESS = "Address";
	private static final String SEARCH_FIELD = "SearchField";

	@InjectMocks
	private DDLLocationSearchMVCActionCommand dDLLocationSearchMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenKeywordsAreNotSpecified_ThenRemovesTheKeywordsAsSingleValuedSharedSearchSessionAttributesAndExecutingTheRedirectWithoutClearingTheSharedSession() throws Exception {
		String latitude = null;
		String longitude = null;
		String address = null;
		String searchField = null;
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.SEARCH_LATITUDE)).thenReturn(latitude);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.SEARCH_LONGITUDE)).thenReturn(longitude);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.FORMATTED_ADDRESS)).thenReturn(address);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.GEO_LOCATION_FIELD)).thenReturn(searchField);

		dDLLocationSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, LocationSearchParameters.SEARCH_LATITUDE);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, LocationSearchParameters.SEARCH_LONGITUDE);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, LocationSearchParameters.FORMATTED_ADDRESS);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, LocationSearchParameters.GEO_LOCATION_FIELD);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
		verify(mockSharedSearchRequestService, never()).clearSharedSearchSession(any(ActionRequest.class));
	}

	@Test
	public void doProcessAction_WhenLatitudeIsSpecified_ThenClearsTheSharedSessionBeforeSettingTheLatitudeAsSingleValuedSharedSearchSessionAttributesAndExecutingTheRedirect() throws Exception {

		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.SEARCH_LATITUDE)).thenReturn(LATITUDE);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.SEARCH_LONGITUDE)).thenReturn(LONGITUDE);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.FORMATTED_ADDRESS)).thenReturn(ADDRESS);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.GEO_LOCATION_FIELD)).thenReturn(SEARCH_FIELD);

		dDLLocationSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).clearSharedSearchSession(mockActionRequest);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, LocationSearchParameters.SEARCH_LATITUDE, LATITUDE);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

	@Test
	public void doProcessAction_WhenLongitudeIsSpecified_ThenClearsTheSharedSessionBeforeSettingTheLongitudeAsSingleValuedSharedSearchSessionAttributesAndExecutingTheRedirect() throws Exception {

		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.SEARCH_LATITUDE)).thenReturn(LATITUDE);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.SEARCH_LONGITUDE)).thenReturn(LONGITUDE);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.FORMATTED_ADDRESS)).thenReturn(ADDRESS);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.GEO_LOCATION_FIELD)).thenReturn(SEARCH_FIELD);

		dDLLocationSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).clearSharedSearchSession(mockActionRequest);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, LocationSearchParameters.SEARCH_LONGITUDE, LONGITUDE);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

	@Test
	public void doProcessAction_WhenFormattedAddressIsSpecified_ThenClearsTheSharedSessionBeforeSettingTheFormattedAddressAsSingleValuedSharedSearchSessionAttributesAndExecutingTheRedirect()
			throws Exception {

		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.SEARCH_LATITUDE)).thenReturn(LATITUDE);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.SEARCH_LONGITUDE)).thenReturn(LONGITUDE);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.FORMATTED_ADDRESS)).thenReturn(ADDRESS);
		when(ParamUtil.getString(mockActionRequest, LocationSearchParameters.GEO_LOCATION_FIELD)).thenReturn(SEARCH_FIELD);

		dDLLocationSearchMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).clearSharedSearchSession(mockActionRequest);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, LocationSearchParameters.FORMATTED_ADDRESS, ADDRESS);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

}
