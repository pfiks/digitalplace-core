package com.placecube.digitalplace.notifications.govuknotificationsservice.service;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendMailContext;
import com.placecube.digitalplace.notifications.service.NotificationMailConnector;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import uk.gov.service.notify.NotificationClient;
import uk.gov.service.notify.NotificationClientException;

@Component(immediate = true, service = NotificationMailConnector.class)
public class GovUKNotificationMailConnector implements NotificationMailConnector {

	private static final Log LOG = LogFactoryUtil.getLog(GovUKNotificationMailConnector.class);

	@Reference
	private GovUKNotificationsService govukNotificationsService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return govukNotificationsService.isEmailEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public void sendMail(SendMailContext sendMailContext, ServiceContext serviceContext) throws NotificationsException, ConfigurationException {
		try {
			NotificationClient client = govukNotificationsService.getClient(govukNotificationsService.getApplicationKey(serviceContext.getCompanyId(), sendMailContext.getApiKeyName()));
			client.sendEmail(sendMailContext.getTemplateId(), sendMailContext.getEmailAddress(), sendMailContext.getPersonalisation(), sendMailContext.getReference(),
					sendMailContext.getFromEmailAddress());
		} catch (NotificationClientException e) {
			LOG.error(e.getMessage());
			throw new NotificationsException(e);
		}
	}

}
