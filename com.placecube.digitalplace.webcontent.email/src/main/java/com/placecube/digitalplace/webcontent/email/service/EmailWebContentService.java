package com.placecube.digitalplace.webcontent.email.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.webcontent.email.constants.EmailConstants;
import com.placecube.digitalplace.webcontent.email.constants.EmailDDMTemplate;
import com.placecube.initializer.service.DDMInitializer;

@Component(immediate = true, service = EmailWebContentService.class)
public class EmailWebContentService {

	@Reference
	private DDMInitializer ddmInitializer;

	public DDMStructure getOrCreateDDMStructure(ServiceContext serviceContext) throws PortalException {
		DDMStructure ddmStructure = getOrCreateEmailStructure(serviceContext);
		getOrCreateEmailTemplate(serviceContext, ddmStructure);
		return ddmStructure;
	}

	public DDMTemplate getOrCreateDDMTemplate(ServiceContext serviceContext) throws PortalException {
		DDMStructure ddmStructure = getOrCreateEmailStructure(serviceContext);
		return getOrCreateEmailTemplate(serviceContext, ddmStructure);
	}

	private DDMStructure getOrCreateEmailStructure(ServiceContext serviceContext) throws PortalException {
		return ddmInitializer.getOrCreateDDMStructure(EmailConstants.STRUCTURE_KEY, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/structure.xml", null, getClass().getClassLoader(), JournalArticle.class, serviceContext, true);
	}

	private DDMTemplate getOrCreateEmailTemplate(ServiceContext serviceContext, DDMStructure ddmStructure) throws PortalException {
		return ddmInitializer.getOrCreateDDMTemplate(ddmInitializer.getDDMTemplateContextForWebContent(EmailDDMTemplate.EMAIL.getName(), EmailDDMTemplate.EMAIL.getKey(), ddmStructure.getStructureId(), "com/placecube/digitalplace/webcontent/email/dependencies/ddm/" + EmailConstants.TEMPLATE_FILE_NAME, getClass().getClassLoader(), serviceContext), true);
	}

}
