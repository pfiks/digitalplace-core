package com.placecube.digitalplace.user.account.configuration;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.user.account.exception.UserAccountException;

public interface CreateAccountConfigurationFactory {

	CreateAccountConfiguration getCreateAccountConfiguration(Company company) throws UserAccountException, ConfigurationException;

	CreateAccountConfiguration getCreateAccountConfiguration(Group group) throws ConfigurationException;

	CreateAccountConfiguration getCreateAccountConfiguration(PortletRequest portletRequest) throws ConfigurationException;
}
