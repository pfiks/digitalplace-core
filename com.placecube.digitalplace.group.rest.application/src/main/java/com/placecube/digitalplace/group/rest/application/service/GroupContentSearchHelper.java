package com.placecube.digitalplace.group.rest.application.service;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcher;
import com.liferay.portal.kernel.search.facet.faceted.searcher.FacetedSearcherManager;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.sortby.mostliked.constants.MostLikedConstants;

@Component(immediate = true, service = GroupContentSearchHelper.class)
public class GroupContentSearchHelper {

	private static final Log LOG = LogFactoryUtil.getLog(GroupContentSearchHelper.class);

	@Reference
	private FacetedSearcherManager facetedSearcherManager;

	public void addBooleanQuery(SearchContext searchContext, String field, String value, BooleanClauseOccur booleanClauseOccur) {
		if (Validator.isNotNull(field) && Validator.isNotNull(value)) {
			BooleanClause<Query> queryToAdd = BooleanClauseFactoryUtil.create(field, value, booleanClauseOccur.getName());
			setBooleanClauses(searchContext, queryToAdd);
		}
	}

	public void addBooleanQueryForMultipleValues(SearchContext searchContext, String field, List<String> values, BooleanClauseOccur booleanClauseOccurForValues) {
		if (Validator.isNotNull(field) && Validator.isNotNull(values) && !values.isEmpty()) {
			BooleanQuery mainQuery = new BooleanQueryImpl();
			boolean areValuesAdded = false;
			for (String value : values) {
				if (Validator.isNotNull(value)) {
					BooleanQuery valueQuery = new BooleanQueryImpl();
					valueQuery.addExactTerm(field, value);
					try {
						mainQuery.add(valueQuery, booleanClauseOccurForValues);
						areValuesAdded = true;
					} catch (ParseException e) {
						LOG.error("Cannot add boolean subquery for value: " + value + " (" + e.getMessage() + ")");
						LOG.debug(e);
					}
				}
			}

			if (areValuesAdded) {
				BooleanClause<Query> queryToAdd = BooleanClauseFactoryUtil.create(mainQuery, BooleanClauseOccur.MUST.getName());

				setBooleanClauses(searchContext, queryToAdd);
			}

		}
	}

	public int evaluateStartResultBasedOnCurrentPageAndDelta(Integer startPage, Integer resultsPerPage) {
		int currentPage = Validator.isNotNull(startPage) ? startPage : 0;
		if (currentPage <= 0) {
			currentPage = 1;
		}

		if (Validator.isNull(resultsPerPage)) {
			resultsPerPage = 1;
		}

		return (currentPage - 1) * resultsPerPage;
	}

	public Hits executeSearch(SearchContext searchContext, Integer startValue, Integer resultsPerPage) throws SearchException {
		searchContext.setStart(startValue);
		searchContext.setEnd(startValue + resultsPerPage);

		FacetedSearcher facetedSearcher = facetedSearcherManager.createFacetedSearcher();
		return facetedSearcher.search(searchContext);

	}

	public SearchContext getBaseSearchContext(Group group, long userId) {
		SearchContext searchContext = new SearchContext();

		searchContext.setCompanyId(group.getCompanyId());
		searchContext.setGroupIds(new long[] { group.getGroupId() });
		searchContext.setUserId(userId);
		searchContext.setEntryClassNames(new String[] { DDLRecord.class.getName() });

		return searchContext;
	}

	public Sort[] getSortByOptions(boolean sortByRating) {
		Sort sortByMostLiked = SortFactoryUtil.create(MostLikedConstants.COUNTER_FIELD_NAME, true);
		Sort sortByScore = SortFactoryUtil.create(null, Sort.SCORE_TYPE, false);
		Sort sortByModifiedDate = SortFactoryUtil.create(Field.MODIFIED_DATE + "_sortable", true);

		if (sortByRating) {
			return new Sort[] { sortByMostLiked, sortByScore, sortByModifiedDate };
		}
		return new Sort[] { sortByScore, sortByModifiedDate, sortByMostLiked };
	}

	private void setBooleanClauses(SearchContext searchContext, BooleanClause<Query> queryToAdd) {
		BooleanClause<Query>[] booleanClauses = searchContext.getBooleanClauses();
		if (Validator.isNull(booleanClauses)) {
			booleanClauses = new BooleanClause[0];
		}

		searchContext.setBooleanClauses(ArrayUtil.append(booleanClauses, queryToAdd));
	}

}
