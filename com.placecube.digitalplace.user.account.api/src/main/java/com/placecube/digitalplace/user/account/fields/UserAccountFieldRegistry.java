package com.placecube.digitalplace.user.account.fields;

import java.util.List;

import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;

public interface UserAccountFieldRegistry {

	List<UserAccountField> getUserAccountFields(long companyId);

	List<UserAccountField> getUserAccountFieldsByStep(AccountCreationStep accountCreationStep, long companyId);
}
