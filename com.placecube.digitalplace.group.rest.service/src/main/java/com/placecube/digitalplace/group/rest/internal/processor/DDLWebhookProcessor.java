package com.placecube.digitalplace.group.rest.internal.processor;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.placecube.digitalplace.group.rest.constants.WebhookAction;
import com.placecube.digitalplace.group.rest.constants.WebhookEvent;
import com.placecube.digitalplace.group.rest.model.GroupWebhook;

@Component(immediate = true, service = DDLWebhookProcessor.class)
public class DDLWebhookProcessor {

	private static final Log LOG = LogFactoryUtil.getLog(DDLWebhookProcessor.class);

	@Reference
	private WebhookTriggerService webhookTriggerService;

	@Reference
	private WebhookDataRetrievalUtil webhookDataRetrievalUtil;

	@Reference
	private GroupLocalService groupLocalService;

	public void processDDLRecordAction(DDLRecord ddlRecord, WebhookAction action) {
		try {
			long webhookClassPK = ddlRecord.getRecordSetId();

			List<GroupWebhook> configuredWebhooks = webhookTriggerService.getConfiguredWebhooks(ddlRecord.getGroupId(), WebhookEvent.DDL_RECORD, webhookClassPK, action);

			if (!configuredWebhooks.isEmpty()) {

				Group group = groupLocalService.getGroup(ddlRecord.getGroupId());
				String entryTitle = webhookDataRetrievalUtil.getDDLRecordTitle(ddlRecord, group);
				long entryId = ddlRecord.getRecordId();
				Date entryDate = ddlRecord.getModifiedDate();
				long userId = ddlRecord.getUserId();

				configuredWebhooks.forEach(webhook -> webhookTriggerService.processWebhook(group, userId, webhook, action, entryId, entryTitle, entryDate));
			}
		} catch (Exception e) {
			LOG.debug(e);
			LOG.warn("Unable to process ddl record action " + e.getMessage());
		}
	}

}
