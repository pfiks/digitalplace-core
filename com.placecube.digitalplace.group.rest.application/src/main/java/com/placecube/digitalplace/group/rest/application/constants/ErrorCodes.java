package com.placecube.digitalplace.group.rest.application.constants;

public enum ErrorCodes {

	INTERNAL_ERROR("INTERNAL-ERROR"),

	INVALID_AUTHENTICATION("INVALID-AUTHENTICATION"),

	INVALID_PARAMETERS("INVALID-PARAMETERS"),

	INVALID_SIGNATURE("INVALID-SIGNATURE"),

	INVALID_TIMESTAMP("INVALID-TIMESTAMP"),

	MISSING_HEADERS("MISSING-HEADERS");

	public final String code;

	private ErrorCodes(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

}
