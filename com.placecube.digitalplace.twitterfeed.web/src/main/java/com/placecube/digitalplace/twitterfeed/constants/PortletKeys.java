package com.placecube.digitalplace.twitterfeed.constants;

public final class PortletKeys {

	public static final String TWITTER = "com_placecube_digitalplace_twitterfeed_TwitterFeedPortlet";

	private PortletKeys() {
	}
}