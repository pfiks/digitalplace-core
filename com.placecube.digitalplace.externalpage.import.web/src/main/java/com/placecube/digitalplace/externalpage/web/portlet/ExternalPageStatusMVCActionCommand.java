package com.placecube.digitalplace.externalpage.web.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;

import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET,
		"mvc.command.name=" + MVCCommandKeys.EXTERNAL_PAGE_STATUS }, service = MVCActionCommand.class)
public class ExternalPageStatusMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException {
		boolean status = ParamUtil.getBoolean(actionRequest, PortletRequestKeys.STATUS);
		long externalPageId = ParamUtil.getLong(actionRequest, PortletRequestKeys.EXTERNAL_PAGE_ID);

		Optional<ExternalPage> externalPageOptional = externalPageLocalService.getExternalPageImport(externalPageId);

		if (externalPageOptional.isPresent()) {
			ExternalPage externalPage = externalPageOptional.get();
			if (status) {
				externalPageLocalService.publishExternalPage(externalPage, status);
			} else {
				externalPageLocalService.unPublishExternalPage(externalPage, status);
			}
		}

		actionResponse.getRenderParameters().setValue(PortletRequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.EXTERNAL_PAGE_VIEW);
	}

}
