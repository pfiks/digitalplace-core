package com.placecube.digitalplace.navigation.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.placecube.digitalplace.navigation.configuration.NavigationPortletInstanceConfiguration;
import com.placecube.digitalplace.navigation.service.NavigationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class NavigationPortletTest extends PowerMockito {

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private NavigationPortletInstanceConfiguration mockNavigationPortletInstanceConfiguration;

	@Mock
	private NavigationService mockNavigationService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private NavigationPortlet navigationPortlet;

	@Test
	public void render_WhenConfigurationIsFoundAndMenuIsFoundAndTemplateIsFound_ThenSetsSiteNavigationMenuAndWidgetTemplateAsRequestAttributes() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockNavigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay)).thenReturn(Optional.of(mockNavigationPortletInstanceConfiguration));
		when(mockNavigationService.getSiteNavigationMenu(mockNavigationPortletInstanceConfiguration)).thenReturn(Optional.of(mockSiteNavigationMenu));
		when(mockNavigationService.getWidgetTemplate(mockNavigationPortletInstanceConfiguration)).thenReturn(Optional.of(mockDDMTemplate));

		navigationPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("siteNavigationMenu", mockSiteNavigationMenu);
		verify(mockRenderRequest, times(1)).setAttribute("widgetTemplate", mockDDMTemplate);
	}

	@Test
	public void render_WhenConfigurationIsFoundAndMenuIsFoundAndTemplateIsNotFound_ThenDoesNotSetAnyRequestAttributes() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockNavigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay)).thenReturn(Optional.of(mockNavigationPortletInstanceConfiguration));
		when(mockNavigationService.getSiteNavigationMenu(mockNavigationPortletInstanceConfiguration)).thenReturn(Optional.of(mockSiteNavigationMenu));
		when(mockNavigationService.getWidgetTemplate(mockNavigationPortletInstanceConfiguration)).thenReturn(Optional.empty());

		navigationPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(anyString(), any());
	}

	@Test
	public void render_WhenConfigurationIsFoundAndMenuIsNotFound_ThenDoesNotSetAnyRequestAttributes() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockNavigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay)).thenReturn(Optional.of(mockNavigationPortletInstanceConfiguration));
		when(mockNavigationService.getSiteNavigationMenu(mockNavigationPortletInstanceConfiguration)).thenReturn(Optional.empty());

		navigationPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(anyString(), any());
	}

	@Test
	public void render_WhenConfigurationIsNotFound_ThenDoesNotSetAnyRequestAttributes() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockNavigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay)).thenReturn(Optional.empty());

		navigationPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(anyString(), any());
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionRetrievingTheConfiguration_ThenThrowsPortletException() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockNavigationService.getValidPortletConfiguration(mockRenderRequest, mockThemeDisplay)).thenThrow(new ConfigurationException());

		navigationPortlet.render(mockRenderRequest, mockRenderResponse);
	}

}
