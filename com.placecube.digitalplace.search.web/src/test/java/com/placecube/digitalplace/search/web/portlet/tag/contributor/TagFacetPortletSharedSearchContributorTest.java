package com.placecube.digitalplace.search.web.portlet.tag.contributor;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.tag.configuration.TagFacetPortletConfigurationConstants;

public class TagFacetPortletSharedSearchContributorTest extends PowerMockito {

	@Mock
	private FacetConfiguration mockFacetConfiguration;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Set<String> mockValues;

	@InjectMocks
	private TagFacetPortletSharedSearchContributor tagFacetPortletSharedSearchContributor;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenNoErrorAndTagsQueryAndedIsFalse_ThenConfiguresTheSelectedAssetTagNamesInTheContributorSettings() throws ConfigurationException {
		mockFacetInit();

		List<SharedActiveFilter> activeCategoryFilters = new ArrayList<>();

		activeCategoryFilters.add(new SharedActiveFilter("n", "11", "l", "c"));
		activeCategoryFilters.add(new SharedActiveFilter("n", "12", "l", "c"));
		activeCategoryFilters.add(new SharedActiveFilter("n", "13", "l", "c"));

		when(mockSharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_TAGS)).thenReturn(activeCategoryFilters);
		when(mockPortletPreferences.getValue(TagFacetPortletConfigurationConstants.TAGS_QUERY_ANDED, "false")).thenReturn("false");

		tagFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		Set<String> opt = new HashSet<>();
		opt.add("11");
		opt.add("12");
		opt.add("13");

		verify(mockSharedSearchContributorSettings, times(1)).setAssetTagNames(opt);
	}

	@Test
	public void contribute_WhenNoError_ThenConfiguresTheTagFacetInTheContributorSettings() throws ConfigurationException {
		int maxTagsToShow = 2;
		int maxShowMoreTagsToShow = 4;
		mockFacetInit();
		when(mockPortletPreferences.getValue(TagFacetPortletConfigurationConstants.MAX_TAGS_TO_SHOW, "10")).thenReturn(String.valueOf(maxTagsToShow));
		when(mockPortletPreferences.getValue(TagFacetPortletConfigurationConstants.MAX_MORE_TAGS_TO_SHOW, "10")).thenReturn(String.valueOf(maxShowMoreTagsToShow));

		tagFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockFacetConfiguration, mockJSONObject, mockSharedSearchContributorSettings);
		inOrder.verify(mockFacetConfiguration, times(1)).setOrder("OrderHitsDesc");
		inOrder.verify(mockFacetConfiguration, times(1)).setStatic(false);
		inOrder.verify(mockFacetConfiguration, times(1)).setWeight(1.4d);
		inOrder.verify(mockJSONObject, times(1)).put("maxTerms", maxTagsToShow + maxShowMoreTagsToShow);
		inOrder.verify(mockJSONObject, times(1)).put("frequencyThreshold", 1);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).addFacet(SearchFacetConstants.ASSET_TAG_NAMES, Optional.of(mockFacetConfiguration));
	}

	@Test(expected = SystemException.class)
	public void contribute_WhenNoPortletPrefsFound_ThenThrowsSystemException() throws Exception {
		when(mockSharedSearchContributorSettings.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.empty());

		tagFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);
	}

	@Test
	public void contribute_WhenNoErrorAndTagQueryAndedSettingIsTrueAndActiveCategoriesPresent_ThenAddsBooleanQueryForEachTagToContributorSetting() throws ConfigurationException {
		mockFacetInit();
		String tagQueryAndedPreferenceValue = "true";

		List<SharedActiveFilter> activeTagFilters = new ArrayList<>();

		activeTagFilters.add(new SharedActiveFilter("n", "tag1", "l", "c"));
		activeTagFilters.add(new SharedActiveFilter("n", "tag2", "l", "c"));
		activeTagFilters.add(new SharedActiveFilter("n", "tag3", "l", "c"));

		when(mockSharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_TAGS)).thenReturn(activeTagFilters);
		when(mockPortletPreferences.getValue(TagFacetPortletConfigurationConstants.TAGS_QUERY_ANDED, "false")).thenReturn(tagQueryAndedPreferenceValue);

		tagFacetPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQuery(SearchFacetConstants.ASSET_TAG_NAMES, "tag1", BooleanClauseOccur.MUST);
		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQuery(SearchFacetConstants.ASSET_TAG_NAMES, "tag2", BooleanClauseOccur.MUST);
		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQuery(SearchFacetConstants.ASSET_TAG_NAMES, "tag3", BooleanClauseOccur.MUST);
	}

	private void mockFacetInit() throws ConfigurationException {
		when(mockSharedSearchContributorSettings.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.createFacetConfiguration(SearchFacetConstants.ASSET_TAG_NAMES)).thenReturn(mockFacetConfiguration);
		when(mockFacetConfiguration.getData()).thenReturn(mockJSONObject);
	}

}
