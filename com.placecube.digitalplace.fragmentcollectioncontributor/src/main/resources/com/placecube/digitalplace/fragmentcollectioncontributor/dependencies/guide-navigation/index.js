function showSection(id) {
	var elementLink = $("a[data-instance-id='" + id + "']");
		
	$('.multi_part_section').addClass('d-none');
	$('#multi_part_section_'+ id).removeClass('d-none');

	$('.multi-part-guide-navigation .list__item').removeClass('list__item--active-child');
	elementLink.parent('.list__item').addClass('list__item--active-child');
	
	$("html, body").animate({ scrollTop: 0 }, "slow");
}

function enableHashSection() {
	if( location.hash && location.hash.length ) {
		var hash = location.hash.substr(1);
		showSection(hash);
	}
}

$( document ).ready(function() {
	enableHashSection();
});

$(window).on('hashchange', function(e){
	enableHashSection();
});