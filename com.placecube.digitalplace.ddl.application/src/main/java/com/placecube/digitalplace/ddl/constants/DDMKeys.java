package com.placecube.digitalplace.ddl.constants;

public final class DDMKeys {

	public static final String OCCUPANT_EMAIL = "occupantEmail";

	public static final String TENANT_EMAIL = "tenantEmail";

	public DDMKeys() {
	}
}
