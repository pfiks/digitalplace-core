package com.placecube.digitalplace.appointment.web.constants;

public final class CriterionConstants {

	public static final String DATE_ALL = "ALL";

	public static final String DATE_FUTURE = "FUTURE";

	public static final String DATE_RANGE = "RANGE";

	public static final String DATE_TODAY = "TODAY";

	public static final String START_DATE_FIELD = "appointmentStartDate";

	private CriterionConstants() {
	}
}