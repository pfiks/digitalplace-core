/**
 * AcceptedCardType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class AcceptedCardType  implements java.io.Serializable {
    private com.capita_software_services.www.portal_api.CardDescription cardDescription;

    private com.capita_software_services.www.scp.base.CardType cardType;

    public AcceptedCardType() {
    }

    public AcceptedCardType(
           com.capita_software_services.www.portal_api.CardDescription cardDescription,
           com.capita_software_services.www.scp.base.CardType cardType) {
           this.cardDescription = cardDescription;
           this.cardType = cardType;
    }


    /**
     * Gets the cardDescription value for this AcceptedCardType.
     *
     * @return cardDescription
     */
    public com.capita_software_services.www.portal_api.CardDescription getCardDescription() {
        return cardDescription;
    }


    /**
     * Sets the cardDescription value for this AcceptedCardType.
     *
     * @param cardDescription
     */
    public void setCardDescription(com.capita_software_services.www.portal_api.CardDescription cardDescription) {
        this.cardDescription = cardDescription;
    }


    /**
     * Gets the cardType value for this AcceptedCardType.
     *
     * @return cardType
     */
    public com.capita_software_services.www.scp.base.CardType getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this AcceptedCardType.
     *
     * @param cardType
     */
    public void setCardType(com.capita_software_services.www.scp.base.CardType cardType) {
        this.cardType = cardType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AcceptedCardType)) return false;
        AcceptedCardType other = (AcceptedCardType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.cardDescription==null && other.getCardDescription()==null) ||
             (this.cardDescription!=null &&
              this.cardDescription.equals(other.getCardDescription()))) &&
            ((this.cardType==null && other.getCardType()==null) ||
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardDescription() != null) {
            _hashCode += getCardDescription().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AcceptedCardType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "acceptedCardType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/portal-api", "cardDescription"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
