package com.placecube.digitalplace.group.restadmin.web.constants;

public final class PortletRequestKeys {

	public static final String APPLICATION_KEY = "applicationKey";

	public static final String APPLICATION_SECRET = "applicationSecret";

	private PortletRequestKeys() {
	}

}
