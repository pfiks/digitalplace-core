package com.placecube.digitalplace.formadmin.web.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.digitalplace.form.service.FormDefinitionRegistry;
import com.placecube.digitalplace.form.service.FormDefinitionService;
import com.placecube.digitalplace.formadmin.web.model.FormDefinitionResult;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ FormDefinitionResult.class })
public class FormDefinitionListServiceTest {

	private static final long CLASS_NAME_ID = 1244;

	private static final String FORM_DEFINITION_KEY = "form-definition";

	private static final long GROUP_ID = 2341;

	@InjectMocks
	private FormDefinitionListService formDefinitionListService;

	@Mock
	private DDMStructure mockDdmStructure;

	@Mock
	private DDMStructureLocalService mockDdmStructureLocalService;

	@Mock
	private FormDefinition mockFormDefinition;

	@Mock
	private FormDefinitionRegistry mockFormDefinitionRegistry;

	@Mock
	private FormDefinitionResult mockFormDefinitionResult;

	@Mock
	private FormDefinitionService mockFormDefinitionService;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test
	public void getFormDefinitions_WhenStructureExistsForFormDefinition_ThenAddsFormDefinition() throws PortalException {

		Map<String, FormDefinition> formDefinitionsMap = new HashMap<>();
		formDefinitionsMap.put(FORM_DEFINITION_KEY, mockFormDefinition);
		when(mockFormDefinition.getKey()).thenReturn(FORM_DEFINITION_KEY);
		when(mockFormDefinitionRegistry.getFormNamesAndFormDefinitions()).thenReturn(formDefinitionsMap);

		List<FormDefinition> formDefinitions = new ArrayList<>();
		formDefinitions.add(mockFormDefinition);

		List<FormDefinition> formDefinitionsSortedList = new ArrayList<>();
		formDefinitionsSortedList.add(mockFormDefinition);

		when(mockFormDefinitionService.sort(formDefinitions)).thenReturn(formDefinitionsSortedList);

		when(mockPortal.getClassNameId(DDMFormInstance.class)).thenReturn(CLASS_NAME_ID);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockDdmStructureLocalService.fetchStructure(GROUP_ID, CLASS_NAME_ID, FORM_DEFINITION_KEY)).thenReturn(mockDdmStructure);
		when(FormDefinitionResult.create(mockFormDefinition, true)).thenReturn(mockFormDefinitionResult);

		List<FormDefinitionResult> result = formDefinitionListService.getFormDefinitions(mockRenderRequest);

		assertThat(result.get(0), sameInstance(mockFormDefinitionResult));

	}

	@Test
	public void getFormDefinitions_WhenThereIsNoFormDefinitionRegistered_ThenReturnsEmptyList() throws PortalException {

		when(mockFormDefinitionRegistry.getFormNamesAndFormDefinitions()).thenReturn(new HashMap<>());

		List<FormDefinitionResult> result = formDefinitionListService.getFormDefinitions(mockRenderRequest);

		assertTrue(result.isEmpty());
	}

	@Before
	public void setUp() {
		mockStatic(FormDefinitionResult.class);
	}

}
