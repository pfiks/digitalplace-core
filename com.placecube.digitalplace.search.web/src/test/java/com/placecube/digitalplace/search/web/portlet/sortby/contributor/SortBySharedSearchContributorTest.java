package com.placecube.digitalplace.search.web.portlet.sortby.contributor;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Sort;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.shared.model.SortByOption;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.service.SortByService;
import com.placecube.digitalplace.search.web.service.ObjectFactoryUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ObjectFactoryUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SortBySharedSearchContributorTest extends PowerMockito {

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Mock
	private Sort mockSort1;

	@Mock
	private Sort mockSort2;

	@Mock
	private SortByService mockSortByService;

	@InjectMocks
	private SortBySharedSearchContributor sortBySharedSearchContributor;

	@Before
	public void activateSetup() {
		mockStatic(ObjectFactoryUtil.class);
	}

	@Test(expected = SystemException.class)
	public void contribute_WhenNoPreferencesFound_ThenThrowsSystemException() {
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.empty());

		sortBySharedSearchContributor.contribute(mockSharedSearchContributorSettings);

	}

	@Test
	@Parameters({ "true", "false" })
	public void contribute_WhenValidPrefsAndChainedEnabled_ThenConfiguresChainedSortsInTheContributorSettings(boolean useOppositeSortDirection) {
		Sort[] contributorSettingsSorts = new Sort[] { mockSort1, mockSort2 };
		String searchKeyword = "";
		String selectedSortBy = "selectedSortBy";
		String[] enabledSortByOptions = new String[] { "opt1", "opt2" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		String chainSortOptions = "true";

		when(ObjectFactoryUtil.getSortByLinkedHashMap()).thenReturn(currentSorts);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(Optional.of(selectedSortBy));
		when(mockPortletPreferences.getValue("chainSortByOptions", "false")).thenReturn(chainSortOptions);
		when(mockPortletPreferences.getValues("enabledSortByOptions", new String[0])).thenReturn(enabledSortByOptions);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSortByService.useOppositeSortByDirection(Optional.of(selectedSortBy), mockSharedSearchContributorSettings)).thenReturn(useOppositeSortDirection);
		when(mockSortByService.getSortsForSortByOptions(ArgumentMatchers.any(SharedSearchContributorSettings.class), ArgumentMatchers.anyCollection(), eq(useOppositeSortDirection)))
				.thenReturn(contributorSettingsSorts);

		sortBySharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockSortByService, mockSharedSearchContributorSettings);
		inOrder.verify(mockSortByService, times(1)).addChainedSortByOptions(sortByOptions, currentSorts);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).setSortBy(contributorSettingsSorts);
	}

	@Test
	@Parameters({ "true", "false" })
	public void contribute_WhenValidPrefsAndChainedEnabledAndKeywordNotPresent_ThenConfiguresChainedSortsInTheContributorSettings(boolean useOppositeSortDirection) {
		Sort[] contributorSettingsSorts = new Sort[] { mockSort1, mockSort2 };
		String searchKeyword = "";
		String[] enabledSortByOptions = new String[] { "opt1", "opt2" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		String chainSortOptions = "true";
		String selectedSortBy = "selectedSortBy";

		when(ObjectFactoryUtil.getSortByLinkedHashMap()).thenReturn(currentSorts);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SEARCH_KEYWORD)).thenReturn(Optional.empty());
		when(mockPortletPreferences.getValue("chainSortByOptions", "false")).thenReturn(chainSortOptions);
		when(mockPortletPreferences.getValues("enabledSortByOptions", new String[0])).thenReturn(enabledSortByOptions);
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(Optional.of(selectedSortBy));
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSortByService.useOppositeSortByDirection(Optional.of(selectedSortBy), mockSharedSearchContributorSettings)).thenReturn(useOppositeSortDirection);
		when(mockSortByService.getSortsForSortByOptions(ArgumentMatchers.any(SharedSearchContributorSettings.class), ArgumentMatchers.anyCollection(), eq(useOppositeSortDirection)))
				.thenReturn(contributorSettingsSorts);

		sortBySharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockSortByService, mockSharedSearchContributorSettings);
		inOrder.verify(mockSortByService, never()).addScoreSortByOption(currentSorts);
		inOrder.verify(mockSortByService, times(1)).addChainedSortByOptions(sortByOptions, currentSorts);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).setSortBy(contributorSettingsSorts);
	}

	@Test
	@Parameters({ "true", "false" })
	public void contribute_WhenValidPrefsAndChainedEnabledAndKeywordPresent_ThenConfiguresChainedAndScoreSortsInTheContributorSettings(boolean useOppositeSortDirection) {
		Sort[] contributorSettingsSorts = new Sort[] { mockSort1, mockSort1 };
		String searchKeyword = "keyword";
		String[] enabledSortByOptions = new String[] { "opt1", "opt2" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		String chainSortOptions = "true";
		String selectedSortBy = "selectedSortBy";

		when(ObjectFactoryUtil.getSortByLinkedHashMap()).thenReturn(currentSorts);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(Optional.of(selectedSortBy));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SEARCH_KEYWORD)).thenReturn(Optional.of(searchKeyword));
		when(mockPortletPreferences.getValue("chainSortByOptions", "false")).thenReturn(chainSortOptions);
		when(mockPortletPreferences.getValues("enabledSortByOptions", new String[0])).thenReturn(enabledSortByOptions);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSortByService.useOppositeSortByDirection(Optional.of(selectedSortBy), mockSharedSearchContributorSettings)).thenReturn(useOppositeSortDirection);
		when(mockSortByService.getSortsForSortByOptions(ArgumentMatchers.any(SharedSearchContributorSettings.class), ArgumentMatchers.anyCollection(), eq(useOppositeSortDirection)))
				.thenReturn(contributorSettingsSorts);

		sortBySharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockSortByService, mockSharedSearchContributorSettings);
		inOrder.verify(mockSortByService, times(1)).addScoreSortByOption(currentSorts);
		inOrder.verify(mockSortByService, times(1)).addChainedSortByOptions(sortByOptions, currentSorts);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).setSortBy(contributorSettingsSorts);
	}

	@Test
	@Parameters({ "true", "false" })
	public void contribute_WhenValidPrefsAndChainedEnabledAndNotSortSelected_ThenConfiguresChainedInTheContributorSettings(boolean useOppositeSortDirection) {
		Sort[] contributorSettingsSorts = new Sort[] { mockSort1, mockSort2 };
		String searchKeyword = "";
		String[] enabledSortByOptions = new String[] { "opt1", "opt2" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		String chainSortOptions = "true";

		when(ObjectFactoryUtil.getSortByLinkedHashMap()).thenReturn(currentSorts);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(Optional.empty());
		when(mockPortletPreferences.getValue("chainSortByOptions", "false")).thenReturn(chainSortOptions);
		when(mockPortletPreferences.getValues("enabledSortByOptions", new String[0])).thenReturn(enabledSortByOptions);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSortByService.useOppositeSortByDirection(Optional.empty(), mockSharedSearchContributorSettings)).thenReturn(useOppositeSortDirection);
		when(mockSortByService.getSortsForSortByOptions(ArgumentMatchers.any(SharedSearchContributorSettings.class), ArgumentMatchers.anyCollection(), eq(useOppositeSortDirection)))
				.thenReturn(contributorSettingsSorts);

		sortBySharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockSortByService, mockSharedSearchContributorSettings);
		inOrder.verify(mockSortByService, never()).addSelectedSortByOption(sortByOptions, currentSorts, Optional.empty());
		inOrder.verify(mockSortByService, times(1)).addChainedSortByOptions(sortByOptions, currentSorts);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).setSortBy(contributorSettingsSorts);
	}

	@Test
	@Parameters({ "true", "false" })
	public void contribute_WhenValidPrefsAndChainedEnabledAndSortSelected_ThenConfiguresChainedAndSelectedSortsInTheContributorSettings(boolean useOppositeSortDirection) {
		Sort[] contributorSettingsSorts = new Sort[] { mockSort1, mockSort2 };
		String searchKeyword = "";
		String selectedSortBy = "selectedSortBy";
		String[] enabledSortByOptions = new String[] { "opt1", "opt2" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		String chainSortOptions = "true";

		when(ObjectFactoryUtil.getSortByLinkedHashMap()).thenReturn(currentSorts);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(Optional.of(selectedSortBy));
		when(mockPortletPreferences.getValue("chainSortByOptions", "false")).thenReturn(chainSortOptions);
		when(mockPortletPreferences.getValues("enabledSortByOptions", new String[0])).thenReturn(enabledSortByOptions);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSortByService.useOppositeSortByDirection(Optional.of(selectedSortBy), mockSharedSearchContributorSettings)).thenReturn(useOppositeSortDirection);
		when(mockSortByService.getSortsForSortByOptions(ArgumentMatchers.any(SharedSearchContributorSettings.class), ArgumentMatchers.anyCollection(), eq(useOppositeSortDirection)))
				.thenReturn(contributorSettingsSorts);

		sortBySharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockSortByService, mockSharedSearchContributorSettings);
		inOrder.verify(mockSortByService, times(1)).addSelectedSortByOption(sortByOptions, currentSorts, Optional.of(selectedSortBy));
		inOrder.verify(mockSortByService, times(1)).addChainedSortByOptions(sortByOptions, currentSorts);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).setSortBy(contributorSettingsSorts);
	}

	@Test
	@Parameters({ "true", "false" })
	public void contribute_WhenValidPrefsAndChainedEnabledAndSortSelectedAndKeywordPresent_ThenConfiguresChainedAndSelectedAndScoreSortsInTheContributorSettings(boolean useOppositeSortDirection) {
		Sort[] contributorSettingsSorts = new Sort[] { mockSort1, mockSort2 };
		String searchKeyword = "keyword";
		String selectedSortBy = "selectedSortBy";
		String[] enabledSortByOptions = new String[] { "opt1", "opt2" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		String chainSortOptions = "true";

		when(ObjectFactoryUtil.getSortByLinkedHashMap()).thenReturn(currentSorts);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(Optional.of(selectedSortBy));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SEARCH_KEYWORD)).thenReturn(Optional.of(searchKeyword));
		when(mockPortletPreferences.getValue("chainSortByOptions", "false")).thenReturn(chainSortOptions);
		when(mockPortletPreferences.getValues("enabledSortByOptions", new String[0])).thenReturn(enabledSortByOptions);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSortByService.useOppositeSortByDirection(Optional.of(selectedSortBy), mockSharedSearchContributorSettings)).thenReturn(useOppositeSortDirection);
		when(mockSortByService.getSortsForSortByOptions(ArgumentMatchers.any(SharedSearchContributorSettings.class), ArgumentMatchers.anyCollection(), eq(useOppositeSortDirection)))
				.thenReturn(contributorSettingsSorts);

		sortBySharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockSortByService, mockSharedSearchContributorSettings);
		inOrder.verify(mockSortByService, times(1)).addSelectedSortByOption(sortByOptions, currentSorts, Optional.of(selectedSortBy));
		inOrder.verify(mockSortByService, times(1)).addScoreSortByOption(currentSorts);
		inOrder.verify(mockSortByService, times(1)).addChainedSortByOptions(sortByOptions, currentSorts);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).setSortBy(contributorSettingsSorts);
	}

	@Test
	@Parameters({ "true", "false" })
	public void contribute_WhenValidPrefsAndChainedNotEnabled_ThenNotConfiguresChainedSortsInTheContributorSettings(boolean useOppositeSortDirection) {
		Sort[] contributorSettingsSorts = new Sort[] { mockSort1, mockSort2 };
		String searchKeyword = "";
		String selectedSortBy = "selectedSortBy";
		String[] enabledSortByOptions = new String[] { "opt1", "opt2" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		String chainSortOptions = "false";

		when(ObjectFactoryUtil.getSortByLinkedHashMap()).thenReturn(currentSorts);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(Optional.of(selectedSortBy));
		when(mockPortletPreferences.getValue("chainSortByOptions", "false")).thenReturn(chainSortOptions);
		when(mockPortletPreferences.getValues("enabledSortByOptions", new String[0])).thenReturn(enabledSortByOptions);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSortByService.useOppositeSortByDirection(Optional.of(selectedSortBy), mockSharedSearchContributorSettings)).thenReturn(useOppositeSortDirection);
		when(mockSortByService.getSortsForSortByOptions(ArgumentMatchers.any(SharedSearchContributorSettings.class), ArgumentMatchers.anyCollection(), eq(useOppositeSortDirection)))
				.thenReturn(contributorSettingsSorts);

		sortBySharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockSortByService, mockSharedSearchContributorSettings);
		inOrder.verify(mockSortByService, never()).addChainedSortByOptions(sortByOptions, currentSorts);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).setSortBy(contributorSettingsSorts);
	}

	@Test
	@Parameters({ "true", "false" })
	public void contribute_WhenValidPrefsAndNotChainedEnabled_ThenConfiguresChainedAndSelectorAndScoreSortsInTheContributorSettings(boolean useOppositeSortDirection) {
		Sort[] contributorSettingsSorts = new Sort[] { mockSort1, mockSort2 };
		String searchKeyword = "keyword";
		String selectedSortBy = "selectedSortBy";
		String[] enabledSortByOptions = new String[] { "opt1", "opt2" };
		List<SortByOption> sortByOptions = new LinkedList<>();
		Map<String, SortByOption> currentSorts = new LinkedHashMap<>();
		String chainSortOptions = "false";

		when(ObjectFactoryUtil.getSortByLinkedHashMap()).thenReturn(currentSorts);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY)).thenReturn(Optional.of(selectedSortBy));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SEARCH_KEYWORD)).thenReturn(Optional.of(searchKeyword));
		when(mockPortletPreferences.getValue("chainSortByOptions", "false")).thenReturn(chainSortOptions);
		when(mockPortletPreferences.getValues("enabledSortByOptions", new String[0])).thenReturn(enabledSortByOptions);
		when(mockSortByService.getAvailableSortByOptions(enabledSortByOptions, searchKeyword)).thenReturn(sortByOptions);
		when(mockSortByService.useOppositeSortByDirection(Optional.of(selectedSortBy), mockSharedSearchContributorSettings)).thenReturn(useOppositeSortDirection);
		when(mockSortByService.getSortsForSortByOptions(ArgumentMatchers.any(SharedSearchContributorSettings.class), ArgumentMatchers.anyCollection(), eq(useOppositeSortDirection)))
				.thenReturn(contributorSettingsSorts);

		sortBySharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		InOrder inOrder = inOrder(mockSortByService, mockSharedSearchContributorSettings);
		inOrder.verify(mockSortByService, times(1)).addSelectedSortByOption(sortByOptions, currentSorts, Optional.of(selectedSortBy));
		inOrder.verify(mockSortByService, times(1)).addDefaultSortByOption(sortByOptions, currentSorts);
		inOrder.verify(mockSortByService, times(1)).addScoreSortByOption(currentSorts);
		inOrder.verify(mockSharedSearchContributorSettings, times(1)).setSortBy(contributorSettingsSorts);

	}

}
