package com.placecube.digitalplace.group.rest.application.service;

import java.io.InputStream;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFileEntryService;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.permission.ModelPermissions;
import com.liferay.portal.kernel.service.permission.ModelPermissionsFactory;
import com.placecube.digitalplace.group.rest.application.model.FileAttachment;

@Component(immediate = true, service = GroupFileService.class)
public class GroupFileService {

	private static final Log LOG = LogFactoryUtil.getLog(GroupFileService.class);

	@Reference
	private DLAppService dlAppService;

	@Reference
	private DLFileEntryLocalService dlFileEntryLocalService;

	@Reference
	private DLFileEntryService dlFileEntryService;

	@Reference
	private RandomService randomService;

	@Reference
	private ResourcePermissionLocalService resourcePermissionLocalService;

	@Reference
	private RoleLocalService roleLocalService;

	public DLFileEntry createDLFileEntry(JSONObject fileJsonObject, ServiceContext serviceContext) throws PortalException {
		String fileUrl = fileJsonObject.getString("url");
		long groupId = serviceContext.getScopeGroupId();
		long folderId = 0;

		String fileName = randomService.getUniqueFileName(groupId, folderId, fileJsonObject.getString("fileName"));

		URI create = URI.create(fileUrl);

		try (InputStream in = create.toURL().openStream();) {

			String externalRefCode = StringPool.BLANK;
			String title = fileName;
			String mimeType = null;
			String description = StringPool.BLANK;
			String changeLog = StringPool.BLANK;
			long size = in.available();
			String urlTitle = StringPool.BLANK;
			Date displayDate = null;
			Date expirationDate = null;
			Date reviewDate = null;

			FileEntry fileEntry = dlAppService.addFileEntry(externalRefCode, groupId, folderId, fileName, mimeType, title, urlTitle, description, changeLog, in, size, displayDate, expirationDate,
					reviewDate, serviceContext);

			return dlFileEntryService.getFileEntry(fileEntry.getFileEntryId());
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public ModelPermissions getModelPermissions(DDLRecordSet ddlRecordSet) {
		ModelPermissions modelPermissions = ModelPermissionsFactory.create(new String[0], new String[0], DLFileEntry.class.getName());

		List<ResourcePermission> resourcePermissions = resourcePermissionLocalService.getResourcePermissions(ddlRecordSet.getCompanyId(), DDLRecordSet.class.getName(),
				ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(ddlRecordSet.getRecordSetId()));

		for (ResourcePermission resourcePermission : resourcePermissions) {
			addPermission(modelPermissions, resourcePermission);
		}

		return modelPermissions;
	}

	public void rollbackAttachmentsCreation(Set<FileAttachment> uploadedAttachments) {
		for (FileAttachment fileAttachment : uploadedAttachments) {
			try {
				dlFileEntryLocalService.deleteFileEntry(fileAttachment.getDlFileEntry());
			} catch (Exception e) {
				LOG.error("Unable to delete attachment", e);
			}
		}
	}

	private void addPermission(ModelPermissions modelPermissions, ResourcePermission resourcePermission) {
		try {
			if (resourcePermission.isViewActionId()) {
				String roleName = roleLocalService.getRole(resourcePermission.getRoleId()).getName();
				modelPermissions.addRolePermissions(roleName, ActionKeys.VIEW);
			}
		} catch (Exception e) {
			LOG.warn("Unable to retrieve role from resource permission", e);
		}
	}

}
