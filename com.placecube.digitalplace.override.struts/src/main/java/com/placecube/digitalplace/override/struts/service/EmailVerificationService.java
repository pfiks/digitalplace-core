package com.placecube.digitalplace.override.struts.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.TicketConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.AuthTokenUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.TicketLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.override.struts.portal.action.VerifyEmailOverrideAction;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = EmailVerificationService.class)
public class EmailVerificationService {

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private TicketLocalService ticketLocalService;

	public void sendEmailAddressVerification(HttpServletRequest httpServletRequest, ThemeDisplay themeDisplay) throws PortalException {

		User user = themeDisplay.getUser();

		ServiceContext serviceContext = ServiceContextFactory.getInstance(httpServletRequest);

		List<Ticket> tickets = ticketLocalService.getTickets(themeDisplay.getCompanyId(), User.class.getName(), user.getUserId(), TicketConstants.TYPE_EMAIL_ADDRESS);

		if (ListUtil.isEmpty(tickets)) {
			userLocalService.sendEmailAddressVerification(user, user.getEmailAddress(), serviceContext);
		} else {
			Ticket ticket = tickets.get(0);

			userLocalService.sendEmailAddressVerification(user, ticket.getExtraInfo(), serviceContext);
		}
	}

	public void verifyEmailAddress(HttpServletRequest httpServletRequest) throws PortalException {

		AuthTokenUtil.checkCSRFToken(httpServletRequest, VerifyEmailOverrideAction.class.getName());

		String ticketKey = ParamUtil.getString(httpServletRequest, "ticketKey");

		userLocalService.verifyEmailAddress(ticketKey);
	}
}
