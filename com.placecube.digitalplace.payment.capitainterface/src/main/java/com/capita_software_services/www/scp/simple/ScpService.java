/**
 * ScpService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.simple;

public interface ScpService extends javax.xml.rpc.Service {
    public java.lang.String getscpSoap11Address();

    public com.capita_software_services.www.scp.simple.Scp getscpSoap11() throws javax.xml.rpc.ServiceException;

    public com.capita_software_services.www.scp.simple.Scp getscpSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
