package com.placecube.digitalplace.search.web.constants;

public final class MVCCommandKeys {

	public static final String SEARCH = "/search";

	private MVCCommandKeys() {
	}
}
