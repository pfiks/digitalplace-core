package com.placecube.digitalplace.ddl.internal.configuration.tracker;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.ddl.internal.configuration.DynamicDataListCompanyConfiguration;
import com.placecube.digitalplace.ddl.internal.configuration.DynamicDataListCompanyConfigurationConstants;
import com.placecube.digitalplace.ddl.internal.util.ConfigurationParsingUtil;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;

@Component(configurationPid = DynamicDataListCompanyConfigurationConstants.PID, immediate = true, property = Constants.SERVICE_PID + "=" + DynamicDataListCompanyConfigurationConstants.PID
		+ ".scoped", service = { ManagedServiceFactory.class, DynamicDataListCompanyConfigurationTracker.class })
public class DynamicDataListCompanyConfigurationTrackerImpl implements ManagedServiceFactory, DynamicDataListCompanyConfigurationTracker {

	private final Map<Long, DynamicDataListCompanyConfiguration> companyConfigurationBeans = new ConcurrentHashMap<>();

	private final Map<Long, List<DynamicDataListMapping>> companyDynamicDataListMappings = new ConcurrentHashMap<>();

	@Reference
	private ConfigurationParsingUtil configurationParsingUtil;

	private final Map<String, Long> pidCompanyIdMapping = new ConcurrentHashMap<>();

	private volatile DynamicDataListCompanyConfiguration systemPersonalMenuConfiguration;

	@Activate
	@Modified
	public void activate(Map<String, Object> properties) {
		systemPersonalMenuConfiguration = ConfigurableUtil.createConfigurable(DynamicDataListCompanyConfiguration.class, properties);
	}

	@Override
	public void deleted(String pid) {
		unmapPid(pid);
	}

	@Override
	public List<DynamicDataListMapping> getConfiguredDynamicDataListMappingsForCompany(long companyId) {
		if (companyDynamicDataListMappings.containsKey(companyId)) {
			return companyDynamicDataListMappings.get(companyId);
		}
		return new ArrayList<>();
	}

	@Override
	public DynamicDataListCompanyConfiguration getDynamicDataListCompanyConfiguration(long companyId) {
		if (companyConfigurationBeans.containsKey(companyId)) {
			return companyConfigurationBeans.get(companyId);
		} else {
			return systemPersonalMenuConfiguration;
		}
	}

	@Override
	public String getName() {
		return DynamicDataListCompanyConfigurationConstants.PID + ".scoped";
	}

	@Override
	public void updated(String pid, Dictionary dictionary) throws ConfigurationException {
		unmapPid(pid);

		long companyId = GetterUtil.getLong(dictionary.get("companyId"), CompanyConstants.SYSTEM);

		if (companyId != CompanyConstants.SYSTEM) {
			pidCompanyIdMapping.put(pid, companyId);

			DynamicDataListCompanyConfiguration companyConfig = ConfigurableUtil.createConfigurable(DynamicDataListCompanyConfiguration.class, dictionary);
			companyConfigurationBeans.put(companyId, companyConfig);
			companyDynamicDataListMappings.put(companyId, configurationParsingUtil.getDynamicDataListMappings(companyConfig, companyId));
		}
	}

	private void unmapPid(String pid) {
		if (pidCompanyIdMapping.containsKey(pid)) {
			long companyId = pidCompanyIdMapping.remove(pid);
			companyConfigurationBeans.remove(companyId);
			companyDynamicDataListMappings.remove(companyId);
		}
	}

}
