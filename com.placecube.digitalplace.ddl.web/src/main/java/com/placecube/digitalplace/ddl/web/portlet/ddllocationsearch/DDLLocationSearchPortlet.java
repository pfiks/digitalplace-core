package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.NoSuchLayoutException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.LocationSearchParameters;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch.configuration.DDLLocationSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-ddl-location-search", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=portlet-location-ddl-search portlet-location-ddl-search-listings", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.restore-current-view=false", //
		"com.liferay.portlet.use-default-template=true", //
		"com.liferay.portlet.header-portlet-javascript=/js/google-places.js", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/ddl-location-search/view.jsp", //
		"javax.portlet.init-param.config-template=/ddl-location-search/configuration.jsp", //
		"javax.portlet.name=" + PortletKeys.DDL_LOCATION_SEARCH_PORTLET, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user" }, service = Portlet.class)
public class DDLLocationSearchPortlet extends MVCPortlet {

	private static final String GOOGLE_MAPS_API_KEY = "googleApiKey";

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Reference
	private DDLLocationSearchGoogleAPIKeyHelper ddLLocationSearchGoogleAPIKeyHelper;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {

			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			DDLLocationSearchPortletInstanceConfiguration configuration = ddlConfigurationService.getDDLLocationSearchPortletInstanceConfiguration(themeDisplay);

			renderRequest.setAttribute(GOOGLE_MAPS_API_KEY, ddLLocationSearchGoogleAPIKeyHelper.getGoogleMapsAPIKey(themeDisplay.getCompanyId(), themeDisplay.getScopeGroup()));
			renderRequest.setAttribute(LocationSearchParameters.GEO_LOCATION_FIELD,
					"ddm__keyword__" + configuration.ddmStructureId() + "__" + configuration.geoLocationField() + "_geolocation_geoLocationsortable");
			renderRequest.setAttribute("placeHolderText", LanguageUtil.get(themeDisplay.getLocale(), "enter-a-location"));

			if (!Validator.isBlank(configuration.destinationPage())) {

				setDestinationPage(renderRequest, themeDisplay, configuration);

			} else {

				SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

				renderRequest.setAttribute("redirectURL", themeDisplay.getLayout().getFriendlyURL());
				renderRequest.setAttribute(LocationSearchParameters.FORMATTED_ADDRESS, searchResponse.getSharedSearchSessionSingleValuedAttribute(LocationSearchParameters.FORMATTED_ADDRESS));

			}

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

	private void setDestinationPage(RenderRequest renderRequest, ThemeDisplay themeDisplay, DDLLocationSearchPortletInstanceConfiguration configuration) throws PortalException {
		try {
			Layout destinationLayout = layoutLocalService.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), themeDisplay.getLayout().isPrivateLayout(), configuration.destinationPage());
			renderRequest.setAttribute("destinationPlid", destinationLayout.getPlid());

		} catch (NoSuchLayoutException e) {
			renderRequest.setAttribute("invalidConfiguration", true);
			renderRequest.setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
		}
	}

}