package com.placecube.digitalplace.addresslookup.statmap.service.utils;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.json.JSONObject;

public class AddressRetrivalUtilTest extends PowerMockito {

	@InjectMocks
	private AddressRetrivalUtil addressRetrivalUtil;

	@Mock
	private CloseableHttpClient mockHttpClient;

	@Mock
	private CloseableHttpResponse mockHttpResponse;

	@Mock
	private JSONObject mockJSONObject;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getHttpGetMethod_WhenNoError_ThenReturnsTheConfiguredHttpGet() {
		String url = "http://localhost";

		HttpGet result = addressRetrivalUtil.getHttpGetMethod(url);

		assertThat(result.getFirstHeader("Accept").getValue(), equalTo("application/json"));
	}

	@Test
	public void getJoinedValuesFromFieldNamesWithNoSpace_WhenNoError_ThenReturnsTheValuesJoinedWithBlank() {
		when(mockJSONObject.getString("field1")).thenReturn("value1");
		when(mockJSONObject.getString("field2")).thenReturn("");
		when(mockJSONObject.getString("field3")).thenReturn("value3");

		String result = addressRetrivalUtil.getJoinedValuesFromFieldNamesWithNoSpace(mockJSONObject, "field1", "field2", "field3");

		assertThat(result, equalTo("value1value3"));
	}

	@Test
	public void getJoinedValuesWithSpace_WhenNoError_ThenReturnsTheValuesJoinedWithSpace() {
		String result = addressRetrivalUtil.getJoinedValuesWithSpace("value1", "", "value2", "value3");

		assertThat(result, equalTo("value1 value2 value3"));
	}

	@Test
	public void getURL_WhenNoError_ThenReturnsTheURLWithTheGazetteerName() {
		List<NameValuePair> nvps = new LinkedList<>();
		nvps.add(new BasicNameValuePair("param1", "value1"));

		String result = addressRetrivalUtil.getURL("baseEndpointUrl", nvps, "tokenValue");

		assertThat(result, equalTo("baseEndpointUrl?param1=value1&token=tokenValue"));
	}

	@Test
	public void quietlyCloseResources_WhenExceptionClosingResponse_ThenClosesClient() throws IOException {
		doThrow(new IOException()).when(mockHttpResponse).close();

		addressRetrivalUtil.quietlyCloseResources(mockHttpClient, mockHttpResponse);

		verify(mockHttpClient, times(1)).close();
	}

	@Test
	public void quietlyCloseResources_WhenExceptionClosingResponseAndClient_ThenNoErrorIsThrown() throws IOException {
		doThrow(new IOException()).when(mockHttpResponse).close();
		doThrow(new IOException()).when(mockHttpClient).close();

		try {
			addressRetrivalUtil.quietlyCloseResources(mockHttpClient, mockHttpResponse);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void quietlyCloseResources_WhenNoError_ThenClosesTheResponseAndTheClient() throws IOException {
		addressRetrivalUtil.quietlyCloseResources(mockHttpClient, mockHttpResponse);

		verify(mockHttpResponse, times(1)).close();
		verify(mockHttpClient, times(1)).close();
	}

}
