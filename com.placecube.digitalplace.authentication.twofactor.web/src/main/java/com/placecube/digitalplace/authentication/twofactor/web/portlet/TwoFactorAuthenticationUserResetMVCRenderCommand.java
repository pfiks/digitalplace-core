package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorPortletKeys;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;

@Component(property = { "javax.portlet.name=" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION, "mvc.command.name=" + TwoFactorMVCCommandKeys.USER_RESET }, service = MVCRenderCommand.class)
public class TwoFactorAuthenticationUserResetMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private TwoFactorRequestUtils twoFactorRequestUtils;

	@Reference
	private TwoFactorService twoFactorService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		HttpServletRequest originalServletRequest = twoFactorRequestUtils.getOriginalServletRequest(renderRequest);
		Optional<User> twoFactorUser = twoFactorService.getTwoFactorUser(renderRequest, originalServletRequest);

		if (!twoFactorUser.isPresent()) {
			SessionErrors.add(renderRequest, "sessionExpired");

			return "/two-factor-authentication/error.jsp";
		} else {
			String secretKey = twoFactorService.getSecretKey(renderRequest);

			renderRequest.setAttribute("twoFactorUrlQR", twoFactorService.getQRBarCodeURL(twoFactorUser.get(), secretKey));
			renderRequest.setAttribute("twoFactorSecretKey", secretKey);
			renderRequest.setAttribute("redirect", ParamUtil.getString(renderRequest, "redirect"));

			return "/two-factor-authentication/reset.jsp";
		}
	}
}
