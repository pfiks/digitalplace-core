package com.placecube.digitalplace.user.account.web.connector;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;

public class DigitalPlaceCreateAccountConnectorTest {

	@InjectMocks
	private DigitalPlaceCreateAccountConnector digitalPlaceCreateAccountConnector;

	@Mock
	private JSONFactory jsonFactory;

	@Mock
	private JSONObject jsonObject;

	@Test
	public void getCreateAccountMVCRenderCommandName_WhenNoError_ThenReturnMVCRenderCommandName() {
		String mvcRenderCommandName = digitalPlaceCreateAccountConnector.getCreateAccountMVCRenderCommandName();
		assertEquals(MVCCommandKeys.PERSONAL_DETAILS, mvcRenderCommandName);
	}

	@Test
	public void getModelId_WhenNoError_ThenReturnModelId() {
		String modelId = digitalPlaceCreateAccountConnector.getModelId();
		assertEquals("digitalPlaceUserCreateAccountModel", modelId);
	}

	@Test
	public void getPortletId_WhenNoError_ThenReturnPortletId() {
		String portletId = digitalPlaceCreateAccountConnector.getPortletId();
		assertEquals(AccountPortletKeys.CREATE_ACCOUNT, portletId);
	}

	@Test
	public void getRequestParameters_WhenNoError_ThenReturnRequestParameterJson() {
		when(jsonFactory.createJSONObject()).thenReturn(jsonObject);
		JSONObject requestParameters = digitalPlaceCreateAccountConnector.getRequestParameters();
		assertEquals(jsonObject, requestParameters);
	}

	@Before
	public void initSetup() {
		initMocks(this);
	}
}
