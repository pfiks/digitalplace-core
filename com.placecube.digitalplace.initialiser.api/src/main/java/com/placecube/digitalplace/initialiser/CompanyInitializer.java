package com.placecube.digitalplace.initialiser;

import com.liferay.portal.kernel.model.Company;

public interface CompanyInitializer {

	String getKey();

	String getName();

	void initialize(Company companyId) throws Exception;
}
