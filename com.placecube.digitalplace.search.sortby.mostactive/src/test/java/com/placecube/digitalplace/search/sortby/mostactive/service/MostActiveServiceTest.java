package com.placecube.digitalplace.search.sortby.mostactive.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.search.index.UpdateDocumentIndexWriter;
import com.liferay.portal.search.indexer.BaseModelDocumentFactory;
import com.liferay.social.kernel.model.SocialActivityConstants;
import com.liferay.social.kernel.service.SocialActivityLocalService;
import com.placecube.digitalplace.search.sortby.mostactive.constants.MostActiveConstants;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestrictionsFactoryUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class MostActiveServiceTest extends PowerMockito {

	@Mock
	private BaseModelDocumentFactory mockBaseModelDocumentFactory;

	@Mock
	private Criterion mockCriterionCompanyId;

	@Mock
	private Criterion mockCriterionCreateDate;

	@Mock
	private Criterion mockCriterionGroupId;

	@Mock
	private Criterion mockCriterionMirrorActivityId;

	@Mock
	private Criterion mockCriterionTypeNotRestoreFromTrash;

	@Mock
	private Criterion mockCriterionTypeNotTrash;

	@Mock
	private Criterion mockCriterionUserId;

	@Mock
	private Document mockDocument;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private Group mockGroup;

	@Mock
	private SocialActivityLocalService mockSocialActivityLocalService;

	@Mock
	private UpdateDocumentIndexWriter mockUpdateDocumentIndexWriter;

	@Mock
	private User mockUser;

	@InjectMocks
	private MostActiveService mostActiveService;

	@Before
	public void activateSetup() {
		mockStatic(RestrictionsFactoryUtil.class);
	}

	@Test
	public void getActivitiesCount_WithGroupParameter_ThenReturnsTheActivityCountForTheGroup() {
		long groupId = 123;
		long companyId = 3432;
		long count = 56;
		long createDate = Instant.now().minus(MostActiveConstants.DAYS_DELTA, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS).toEpochMilli();
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockGroup.getCompanyId()).thenReturn(companyId);
		when(mockSocialActivityLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(RestrictionsFactoryUtil.eq("companyId", companyId)).thenReturn(mockCriterionCompanyId);
		when(RestrictionsFactoryUtil.eq("groupId", groupId)).thenReturn(mockCriterionGroupId);
		when(RestrictionsFactoryUtil.eq("mirrorActivityId", 0l)).thenReturn(mockCriterionMirrorActivityId);
		when(RestrictionsFactoryUtil.gt("createDate", createDate)).thenReturn(mockCriterionCreateDate);
		when(RestrictionsFactoryUtil.ne("type", SocialActivityConstants.TYPE_RESTORE_FROM_TRASH)).thenReturn(mockCriterionTypeNotRestoreFromTrash);
		when(RestrictionsFactoryUtil.ne("type", SocialActivityConstants.TYPE_MOVE_TO_TRASH)).thenReturn(mockCriterionTypeNotTrash);
		when(mockSocialActivityLocalService.dynamicQueryCount(mockDynamicQuery)).thenReturn(count);

		long result = mostActiveService.getActivitiesCount(mockGroup);

		assertThat(result, equalTo(count));

		InOrder inOrder = inOrder(mockDynamicQuery, mockSocialActivityLocalService);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionCompanyId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionMirrorActivityId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionCreateDate);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionTypeNotRestoreFromTrash);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionTypeNotTrash);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionGroupId);
		inOrder.verify(mockSocialActivityLocalService, times(1)).dynamicQueryCount(mockDynamicQuery);
	}

	@Test
	public void getActivitiesCount_WithUserParameter_ThenReturnsTheActivityCountForTheUser() {
		long userId = 123;
		long companyId = 3432;
		long count = 56;
		long createDate = Instant.now().minus(MostActiveConstants.DAYS_DELTA, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS).toEpochMilli();
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockSocialActivityLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(RestrictionsFactoryUtil.eq("companyId", companyId)).thenReturn(mockCriterionCompanyId);
		when(RestrictionsFactoryUtil.eq("userId", userId)).thenReturn(mockCriterionUserId);
		when(RestrictionsFactoryUtil.eq("mirrorActivityId", 0l)).thenReturn(mockCriterionMirrorActivityId);
		when(RestrictionsFactoryUtil.gt("createDate", createDate)).thenReturn(mockCriterionCreateDate);
		when(RestrictionsFactoryUtil.ne("type", SocialActivityConstants.TYPE_RESTORE_FROM_TRASH)).thenReturn(mockCriterionTypeNotRestoreFromTrash);
		when(RestrictionsFactoryUtil.ne("type", SocialActivityConstants.TYPE_MOVE_TO_TRASH)).thenReturn(mockCriterionTypeNotTrash);
		when(mockSocialActivityLocalService.dynamicQueryCount(mockDynamicQuery)).thenReturn(count);

		long result = mostActiveService.getActivitiesCount(mockUser);

		assertThat(result, equalTo(count));

		InOrder inOrder = inOrder(mockDynamicQuery, mockSocialActivityLocalService);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionCompanyId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionMirrorActivityId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionCreateDate);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionTypeNotRestoreFromTrash);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionTypeNotTrash);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionUserId);
		inOrder.verify(mockSocialActivityLocalService, times(1)).dynamicQueryCount(mockDynamicQuery);
	}

	@Test
	@Parameters({ "true", "false" })
	public void reindexSocialActivityCount_WithGroupParameter_ThenExecutesThePartialDocumentUpdateForTheActivityCount(boolean commitImmediately) {
		long count = 56;
		long companyId = 788;
		when(mockBaseModelDocumentFactory.createDocument(mockGroup)).thenReturn(mockDocument);
		when(mockGroup.getCompanyId()).thenReturn(companyId);

		mostActiveService.reindexSocialActivityCount(mockGroup, count, commitImmediately);

		InOrder inOrder = inOrder(mockDocument, mockUpdateDocumentIndexWriter);
		inOrder.verify(mockDocument, times(1)).addKeyword(MostActiveConstants.COUNTER_FIELD_NAME, count);
		inOrder.verify(mockUpdateDocumentIndexWriter, times(1)).updateDocumentPartially(companyId, mockDocument, commitImmediately);
	}

	@Test
	@Parameters({ "true", "false" })
	public void reindexSocialActivityCount_WithUserParameter_ThenExecutesThePartialDocumentUpdateForTheActivityCount(boolean commitImmediately) {
		long userId = 123;
		long count = 56;
		long companyId = 788;
		when(mockBaseModelDocumentFactory.createDocument(mockUser)).thenReturn(mockDocument);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getCompanyId()).thenReturn(companyId);

		mostActiveService.reindexSocialActivityCount(mockUser, count, commitImmediately);

		InOrder inOrder = inOrder(mockDocument, mockUpdateDocumentIndexWriter);
		inOrder.verify(mockDocument, times(1)).addKeyword(MostActiveConstants.COUNTER_FIELD_NAME, count);
		inOrder.verify(mockUpdateDocumentIndexWriter, times(1)).updateDocumentPartially(companyId, mockDocument, commitImmediately);
	}
}
