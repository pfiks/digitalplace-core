package com.placecube.digitalplace.oauth.dataprovider.service;

import com.liferay.oauth2.provider.model.OAuth2Application;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.json.web.service.client.JSONWebServiceClient;
import com.liferay.portal.json.web.service.client.JSONWebServiceClientFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HashMapBuilder;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.oauth.dataprovider.util.URLUtil;
import com.placecube.digitalplace.oauth.service.DigitalPlaceOAuth2Service;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHeaders;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = DataProviderWebServiceClientService.class)
public class DataProviderWebServiceClientService {

	private static final Pattern PATH_PARAMETERS_PATTERN = Pattern.compile("\\{(.+?)\\}");

	@Reference
	private DigitalPlaceOAuth2Service digitalPlaceOAuth2Service;

	@Reference
	private DDMRESTOAuth2DataProviderService ddmRestOAuth2DataProviderUtil;

	@Reference
	private JSONWebServiceClientFactory jsonWebServiceClientFactory;

	@Reference
	private URLUtil urlUtil;

	public String callJSONWebService(long companyId, OAuth2Application oAuth2Application, URI uri, String absoluteURL, boolean trustSelfSignedCertificates, Map<String, String> allParametersMap)
			throws Exception {

		String token = digitalPlaceOAuth2Service.getOAuth2Token(companyId, oAuth2Application.getClientId(), oAuth2Application.getClientSecret());

		String headers = HttpHeaders.CONTENT_TYPE + "=application/json;" + HttpHeaders.AUTHORIZATION + "=Bearer " + token;

		JSONWebServiceClient jsonWebServiceClient = jsonWebServiceClientFactory.getInstance(HashMapBuilder.<String, Object>put("hostName", urlUtil.getHostName(uri.getHost()))
				.put("hostPort", urlUtil.getHostPort(uri.getPort(), uri.getScheme())).put("keyStore", ddmRestOAuth2DataProviderUtil.getKeyStore()).put("headers", headers)
				.put("protocol", uri.getScheme()).put("trustSelfSignedCertificates", trustSelfSignedCertificates).putAll(urlUtil.getProxySettingsMap()).build(), false);

		String response = null;

		try {
			response = jsonWebServiceClient.doGet(absoluteURL, getParametersArray(allParametersMap));
		} finally {
			jsonWebServiceClient.destroy();
		}

		return response;
	}

	public Map<String, String> getAllParametersMap(Map<String, String> pathInputParametersMap, String query, Map<String, Object> requestInputParametersMap) {

		Map<String, String> allParametersMap = new TreeMap<>();

		requestInputParametersMap.entrySet().stream().filter(x -> !pathInputParametersMap.containsKey(x.getKey()))
				.forEach(entry -> allParametersMap.put(entry.getKey(), String.valueOf(entry.getValue())));

		for (String queryParameter : StringUtil.split(query, StringPool.AMPERSAND)) {

			String[] queryParameterPartsMap = StringUtil.split(queryParameter, StringPool.EQUAL);

			if (queryParameterPartsMap.length > 1) {
				allParametersMap.put(queryParameterPartsMap[0], queryParameterPartsMap[1]);
			} else {
				allParametersMap.put(queryParameterPartsMap[0], StringPool.BLANK);
			}
		}

		return allParametersMap;
	}

	public Map<String, String> getPathInputParametersMap(Map<String, Object> requestInputParametersMap, String url) {

		Map<String, String> pathInputParametersMap = new HashMap<>();

		Matcher matcher = PATH_PARAMETERS_PATTERN.matcher(url);

		while (matcher.find()) {
			String pathParameterName = matcher.group(1);

			if (requestInputParametersMap.containsKey(pathParameterName)) {
				pathInputParametersMap.put(pathParameterName, GetterUtil.getString(requestInputParametersMap.get(pathParameterName)));
			}
		}

		return pathInputParametersMap;
	}

	private String[] getParametersArray(Map<String, String> allParametersMap) {
		List<String> parameters = new ArrayList<>();

		for (Map.Entry<String, String> entry : allParametersMap.entrySet()) {
			parameters.add(entry.getKey());
			parameters.add(entry.getValue());
		}

		return ArrayUtil.toStringArray(parameters);
	}

}
