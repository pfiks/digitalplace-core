package com.placecube.digitalplace.accessibility.web.internal.dynamicinclude.ariallabel;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.servlet.taglib.BaseDynamicInclude;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.accessibility.web.internal.configuration.AccessibilityCompanyConfiguration;

@Component(immediate = true, service = DynamicInclude.class)
public class BottomDynamicIncludePost extends BaseDynamicInclude {

	private static final Log LOG = LogFactoryUtil.getLog(BottomDynamicIncludePost.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.accessibility.web)")
	private ServletContext servletContext;

	@Override
	public void include(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String key) throws IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		if (Validator.isNotNull(themeDisplay)) {

			try {
				AccessibilityCompanyConfiguration accessibilityCompanyConfiguration = configurationProvider.getCompanyConfiguration(AccessibilityCompanyConfiguration.class, themeDisplay.getCompanyId());

				if (accessibilityCompanyConfiguration.enabled()) {
					PrintWriter printWriter = httpServletResponse.getWriter();
					printWriter.println(getIncludeString("/js/aria-label-addition-to-external-links.js"));
				}

			} catch (ConfigurationException e) {
				LOG.error(e);
			}
		}

	}

	@Override
	public void register(DynamicIncludeRegistry dynamicIncludeRegistry) {
		dynamicIncludeRegistry.register("/html/common/themes/bottom.jsp#post");
	}

	private String getIncludeString(String srcPath) {
		String scriptURL = servletContext.getContextPath() + srcPath;
		return "<script data-senna-track=\"temporary\" src=\"" + scriptURL + "\" type=\"text/javascript\"></script>";
	}

}
