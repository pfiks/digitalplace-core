package com.placecube.digitalplace.companyinitializeradmin.initializer.internal.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMapFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.initialiser.CompanyInitializer;

@Component(immediate = true, service = CompanyInitializerRegistry.class)
public class CompanyInitializerRegistry {

	private static final Log _log = LogFactoryUtil.getLog(CompanyInitializerRegistry.class);

	private ServiceTrackerMap<String, ServiceWrapper<CompanyInitializer>> companyInitilizersMap;

	public CompanyInitializer getCompanyInitializer(String key) {
		if (Validator.isNull(key)) {
			return null;
		}

		ServiceWrapper<CompanyInitializer> serviceWrapper = companyInitilizersMap.getService(key);

		if (serviceWrapper == null) {
			if (_log.isDebugEnabled()) {
				_log.debug("No company initializer registered with key " + key);
			}

			return null;
		}

		return serviceWrapper.getService();
	}

	public List<CompanyInitializer> getCompanyInitializers() {

		List<ServiceWrapper<CompanyInitializer>> allCheckers = ListUtil.fromCollection(companyInitilizersMap.values());

		List<CompanyInitializer> results = allCheckers.stream().map(ServiceWrapper::getService).collect(Collectors.toList());
		return Collections.unmodifiableList(results);
	}

	@Activate
	protected void activate(BundleContext bundleContext) {
		companyInitilizersMap = ServiceTrackerMapFactory.openSingleValueMap(bundleContext, CompanyInitializer.class, "company.initializer.key",
				ServiceTrackerCustomizerFactory.<CompanyInitializer>serviceWrapper(bundleContext));
	}

	@Deactivate
	protected void deactivate() {
		companyInitilizersMap.close();
	}
}
