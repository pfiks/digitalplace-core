<%@ include file="../init.jsp"%>

<c:choose>

    <c:when test="${invalidConfiguration}">
        <%@ include file="/common/invalid-configuration.jspf" %>
    </c:when>

    <c:otherwise>

        <c:set var="colourCssClass" value="${facetFieldName}-${ (filterColour.length() > 0) ? filterColour.substring(1) : filterColour }" />
        <style>
            .dp-search .search-filter-facet.${colourCssClass} .panel-header {
                border-bottom: 4px solid ${filterColour};
            }
        </style>

        <liferay-ui:panel collapsible="true" markupView="lexicon" cssClass="search-filter-facet ${colourCssClass}" title="${title}">

            <portlet:actionURL name="<%= MVCCommandKeys.SEARCH %>" var="updateSelectionURL">
                <portlet:param name="redirectURL" value="${redirectURL}" />
                <portlet:param name="filterColour" value="${filterColour}" />
            </portlet:actionURL>

            <aui:form action="${updateSelectionURL}" method="post" name="fm">

                <c:if test="${not empty searchFields}">
                    <c:forEach items="${searchFields}" var="searchField">
                        <aui:input
                                label="${searchField.getLabel()}"
                                name="${searchField.getField()}"
                                type="text"
                                value="${advancePeopleSearchDisplayContext.getFilterValue(searchField.getField())}"
                        />
                    </c:forEach>
                </c:if>

                <aui:button-row>

                    <portlet:actionURL name="<%= MVCCommandKeys.SEARCH %>" var="clearAllSearchFields">
                        <portlet:param name="cmd" value="clearAll"/>
                        <portlet:param name="redirectURL" value="${redirectURL}" />
                        <portlet:param name="filterName" value="${facetFieldName}" />
                    </portlet:actionURL>

                    <a href="${clearAllSearchFields}" class="btn btn-link btn-clear-selection">
                        <liferay-ui:message key="clear-all"/>
                    </a>

                    <aui:button type="submit" value="search" cssClass="btn-primary" />

                </aui:button-row>

            </aui:form>

        </liferay-ui:panel>

    </c:otherwise>
</c:choose>