package com.placecube.digitalplace.webcontent.email.constants;

public final class EmailConstants {

	public static final String STRUCTURE_KEY = "EMAIL";

	public static final String TEMPLATE_KEY = "EMAIL";

	public static final String TEMPLATE_FILE_NAME = "EMAIL.ftl";

	private EmailConstants() {

	}

}
