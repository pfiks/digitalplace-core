package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.net.URISyntaxException;

import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration.DDLFieldFacetPortletConfigurationConstants;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, HtmlUtil.class })
public class DDLFieldFacetRequestServiceTest {

	private static final String DDM_FIELD_NAME = "FieldOne";

	private static final String PREDEFINED_FIELD_VALUE = "predefinedValue";

	private static final String URL_FIELD_VALUE = "value";

	@InjectMocks
	private DDLFieldFacetRequestService ddlFieldFacetRequestService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, HtmlUtil.class);
	}

	@Test
	public void getConfiguredFieldNameValueFromUrl_WhenNoErrors_ThenReturnsFieldNameValueInRequest() {
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, DDM_FIELD_NAME, PREDEFINED_FIELD_VALUE)).thenReturn(URL_FIELD_VALUE);

		String result = ddlFieldFacetRequestService.getConfiguredFieldNameValueFromUrl(mockThemeDisplay, DDM_FIELD_NAME, PREDEFINED_FIELD_VALUE);

		assertThat(result, equalTo(URL_FIELD_VALUE));
	}

	@Test(expected = URISyntaxException.class)
	public void getEscapedRedirectUrlWithoutFieldUrlParameter_WhenIteratorUrlIsInvalid_ThenThrowsURISyntaxException() throws Exception {
		final String url = "invalid url";

		when(mockSharedSearchResponse.getIteratorURL()).thenReturn(url);
		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.FIELD_NAME, StringPool.BLANK)).thenReturn(DDM_FIELD_NAME);

		ddlFieldFacetRequestService.getEscapedRedirectUrlWithoutFieldUrlParameter(mockSharedSearchResponse, mockPortletPreferences);
	}

	@Test
	public void getEscapedRedirectUrlWithoutFieldUrlParameter_WhenSearchResponseIteratorUrlContainsFieldNameParameter_ThenReturnsScapedUrlWithoutFieldNameParameter() throws Exception {
		final String escapedUrlWithoutParameters = "https://escaped-url";
		final String urlWithoutParameters = "https://www.server.com/search-page?";
		final String url = urlWithoutParameters + DDM_FIELD_NAME + "=value";

		when(mockSharedSearchResponse.getIteratorURL()).thenReturn(url);
		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.FIELD_NAME, StringPool.BLANK)).thenReturn(DDM_FIELD_NAME);
		when(HtmlUtil.escape(urlWithoutParameters)).thenReturn(escapedUrlWithoutParameters);

		String result = ddlFieldFacetRequestService.getEscapedRedirectUrlWithoutFieldUrlParameter(mockSharedSearchResponse, mockPortletPreferences);

		assertThat(result, equalTo(escapedUrlWithoutParameters));

	}

	@Test
	public void isConfiguredFieldNameValueInUrl_WhenFieldParameterIsInUrl_ThenReturnsTrue() {
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.FIELD_NAME, StringPool.BLANK)).thenReturn(DDM_FIELD_NAME);
		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, StringPool.BLANK)).thenReturn(PREDEFINED_FIELD_VALUE);
		when(ParamUtil.getString(mockHttpServletRequest, DDM_FIELD_NAME, PREDEFINED_FIELD_VALUE)).thenReturn(URL_FIELD_VALUE);

		boolean result = ddlFieldFacetRequestService.isConfiguredFieldNameValueInUrl(mockThemeDisplay, mockPortletPreferences);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isConfiguredFieldNameValueInUrl_WhenFieldParameterIsNotInUrl_ThenReturnsFalse() {
		when(mockThemeDisplay.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.FIELD_NAME, StringPool.BLANK)).thenReturn(DDM_FIELD_NAME);
		when(mockPortletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, StringPool.BLANK)).thenReturn(PREDEFINED_FIELD_VALUE);
		when(ParamUtil.getString(mockHttpServletRequest, DDM_FIELD_NAME, PREDEFINED_FIELD_VALUE)).thenReturn(StringPool.BLANK);

		boolean result = ddlFieldFacetRequestService.isConfiguredFieldNameValueInUrl(mockThemeDisplay, mockPortletPreferences);

		assertThat(result, equalTo(false));
	}

}
