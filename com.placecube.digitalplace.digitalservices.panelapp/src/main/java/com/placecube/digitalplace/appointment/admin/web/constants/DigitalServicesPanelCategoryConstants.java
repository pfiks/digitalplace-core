package com.placecube.digitalplace.appointment.admin.web.constants;

public final class DigitalServicesPanelCategoryConstants {

	public static final String CONTROL_PANEL_CATEGORY_DIGITAL_SERVICES = "control_panel.digital_services";

	public static final String FRONT_END_PANEL_CATEGORY_DIGITAL_SERVICES = "category.digitalservices";

	private DigitalServicesPanelCategoryConstants() {
	}
}