package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.captcha.util.CaptchaUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.DISPLAY_CAPTCHA }, service = MVCResourceCommand.class)
public class CaptchaMVCResourceCommand implements MVCResourceCommand {

	@Reference
	private Portal portal;

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException {
		try {
			CaptchaUtil.serveImage(portal.getHttpServletRequest(resourceRequest), portal.getHttpServletResponse(resourceResponse));
			return true;
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}
