package com.placecube.digitalplace.notifications.service.internal.emaildefault;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleDisplay;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendMailContext;
import com.placecube.journal.service.JournalArticleRetrievalService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DefaultEmailNotificationServiceTest extends PowerMockito {

	private static final String BODY_TEXT = "bodyTextValue";
	private static final String EMAIL_FROM = "email.from@test.com";
	private static final String EMAIL_TO = "email.to@test.com";
	private static final long GROUP_ID = 123;
	private static final String NAME_FROM = "Name From";
	private static final String SUBJECT_TEXT = "subjectTextValue";
	private static final String TEMPLATE_ID = "templateIdValue";

	@InjectMocks
	private DefaultEmailNotificationService defaultEmailNotificationService;

	@Mock
	private EmailNotificationUtils mockEmailNotificationUtils;

	@Mock
	private File mockFile1;

	@Mock
	private File mockFile2;

	@Mock
	private InternetAddress mockInternetAddressBCC;

	@Mock
	private InternetAddress mockInternetAddressFrom;

	@Mock
	private InternetAddress mockInternetAddressTo;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleDisplay mockJournalArticleDisplay;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private JournalArticleRetrievalService mockJournalArticleRetrievalService;

	@Mock
	private MailMessage mockMailMessage;

	@Mock
	private MailService mockMailService;

	@Mock
	private Map<String, Object> mockPersonlisation;

	@Mock
	private SendMailContext mockSendMailContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test(expected = NotificationsException.class)
	public void sendMail_WhenArticleDisplayIsNull_ThenThrowsNotificationsException() throws PortalException {
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockSendMailContext.getTemplateId()).thenReturn(TEMPLATE_ID);
		when(mockJournalArticleLocalService.fetchLatestArticle(GROUP_ID, TEMPLATE_ID, WorkflowConstants.STATUS_APPROVED)).thenReturn(mockJournalArticle);
		when(mockJournalArticleLocalService.getArticleDisplay(GROUP_ID, TEMPLATE_ID, null, null, mockThemeDisplay)).thenReturn(null);

		defaultEmailNotificationService.sendMail(mockSendMailContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	@Parameters({ "true,false", "false,true", "false,false" })
	public void sendMail_WhenArticleFoundButSubjectOrBodyNotValid_ThenThrowsNotificationsException(boolean subjectFound, boolean bodyFound) throws NotificationsException {
		Locale locale = Locale.CANADA_FRENCH;
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockSendMailContext.getTemplateId()).thenReturn(TEMPLATE_ID);
		when(mockJournalArticleLocalService.fetchLatestArticle(GROUP_ID, TEMPLATE_ID, WorkflowConstants.STATUS_APPROVED)).thenReturn(mockJournalArticle);
		when(mockServiceContext.getLocale()).thenReturn(locale);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, "Subject", locale)).thenReturn(Optional.ofNullable(subjectFound ? "subject" : null));
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, "Body", locale)).thenReturn(Optional.ofNullable(bodyFound ? "body" : null));

		defaultEmailNotificationService.sendMail(mockSendMailContext, mockServiceContext);
	}

	@Test
	public void sendMail_WhenArticleFoundWithValidSubjectAndBody_ThenSendsTheEmail() throws NotificationsException, PortalException, AddressException {
		mockValidContent(GROUP_ID);
		String[] bccEmailAddresses = { "additionalEmailRecipients@xx.com" };
		InternetAddress[] internetAddressAdditionalEmailRecipients = { mockInternetAddressBCC, mockInternetAddressFrom, mockInternetAddressTo };
		when(mockSendMailContext.getEmailAddress()).thenReturn(EMAIL_TO);
		when(mockSendMailContext.getBccEmailAddresses()).thenReturn(bccEmailAddresses);
		when(mockSendMailContext.getFromEmailAddress()).thenReturn(EMAIL_FROM);
		when(mockSendMailContext.getFromName()).thenReturn(NAME_FROM);
		when(mockEmailNotificationUtils.getInternetAddress(EMAIL_TO, null)).thenReturn(mockInternetAddressTo);
		when(mockEmailNotificationUtils.getInternetAddress(bccEmailAddresses)).thenReturn(internetAddressAdditionalEmailRecipients);
		when(mockEmailNotificationUtils.getInternetAddress(EMAIL_FROM, NAME_FROM)).thenReturn(mockInternetAddressFrom);
		when(mockEmailNotificationUtils.createMailMessage()).thenReturn(mockMailMessage);
		List<File> attachments = new LinkedList<>();
		attachments.add(mockFile1);
		attachments.add(mockFile2);
		when(mockSendMailContext.getAttachments()).thenReturn(attachments);

		defaultEmailNotificationService.sendMail(mockSendMailContext, mockServiceContext);

		InOrder inOrder = inOrder(mockMailMessage, mockMailService);
		inOrder.verify(mockMailMessage, times(1)).setTo(mockInternetAddressTo);
		inOrder.verify(mockMailMessage, times(1)).setBCC(internetAddressAdditionalEmailRecipients);
		inOrder.verify(mockMailMessage, times(1)).setFrom(mockInternetAddressFrom);
		inOrder.verify(mockMailMessage, times(1)).setSubject(SUBJECT_TEXT);
		inOrder.verify(mockMailMessage, times(1)).setBody(BODY_TEXT);
		inOrder.verify(mockMailMessage, times(1)).setHTMLFormat(true);
		inOrder.verify(mockMailMessage, times(1)).addFileAttachment(mockFile1);
		inOrder.verify(mockMailMessage, times(1)).addFileAttachment(mockFile2);
		inOrder.verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test(expected = NotificationsException.class)
	public void sendMail_WhenArticleNotFound_ThenThrowsNotificationsException() throws NotificationsException {
		String templateId = "templateIdValue";
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockSendMailContext.getTemplateId()).thenReturn(templateId);
		when(mockJournalArticleLocalService.fetchLatestArticle(GROUP_ID, templateId, WorkflowConstants.STATUS_APPROVED)).thenReturn(null);

		defaultEmailNotificationService.sendMail(mockSendMailContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	public void sendMail_WhenErrorCreatingInternetAddressFrom_ThenThrowsNotificationsException() throws NotificationsException, PortalException {
		mockValidContent(GROUP_ID);
		when(mockSendMailContext.getFromEmailAddress()).thenReturn(EMAIL_FROM);
		when(mockSendMailContext.getFromName()).thenReturn(NAME_FROM);
		when(mockEmailNotificationUtils.getInternetAddress(EMAIL_FROM, NAME_FROM)).thenThrow(new NotificationsException());

		defaultEmailNotificationService.sendMail(mockSendMailContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	public void sendMail_WhenErrorCreatingInternetAddressTo_ThenThrowsNotificationsException() throws NotificationsException, PortalException {

		mockValidContent(GROUP_ID);
		when(mockSendMailContext.getEmailAddress()).thenReturn(EMAIL_TO);
		when(mockEmailNotificationUtils.getInternetAddress(EMAIL_TO, null)).thenThrow(new NotificationsException());

		defaultEmailNotificationService.sendMail(mockSendMailContext, mockServiceContext);
	}

	@Test
	public void sendMail_WhenErrorGettingArticleDisplay_ThenBodyFieldIsUsedForPersonalisation() throws PortalException {
		Locale locale = Locale.ENGLISH;
		String subject = "subjectValue";
		String body = "bodyValue";
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockSendMailContext.getTemplateId()).thenReturn(TEMPLATE_ID);
		when(mockJournalArticleLocalService.fetchLatestArticle(GROUP_ID, TEMPLATE_ID, WorkflowConstants.STATUS_APPROVED)).thenReturn(mockJournalArticle);
		when(mockServiceContext.getLocale()).thenReturn(locale);

		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, "Subject", locale)).thenReturn(Optional.ofNullable(subject));
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, "Body", locale)).thenReturn(Optional.ofNullable(body));
		when(mockJournalArticleLocalService.getArticleDisplay(GROUP_ID, TEMPLATE_ID, null, null, mockThemeDisplay)).thenThrow(new PortalException());

		when(mockSendMailContext.getPersonalisation()).thenReturn(mockPersonlisation);
		when(mockEmailNotificationUtils.createMailMessage()).thenReturn(mockMailMessage);
		when(mockEmailNotificationUtils.getBodyTextField(body, mockPersonlisation)).thenReturn(BODY_TEXT);

		defaultEmailNotificationService.sendMail(mockSendMailContext, mockServiceContext);

		verify(mockMailMessage, times(1)).setBody(BODY_TEXT);

	}

	@Test
	public void sendMail_WhenGroupIdIsGreaterThanZero_ThenGroupIdIsUsed() throws NotificationsException, PortalException {
		long groupId = 1;

		mockValidContent(groupId);
		when(mockSendMailContext.getGroupId()).thenReturn(groupId);
		when(mockJournalArticleLocalService.fetchLatestArticle(groupId, TEMPLATE_ID, WorkflowConstants.STATUS_APPROVED)).thenReturn(mockJournalArticle);
		String[] bccEmailAddresses = { "additionalEmailRecipients@xx.com" };
		InternetAddress[] internetAddressAdditionalEmailRecipients = { mockInternetAddressBCC, mockInternetAddressFrom, mockInternetAddressTo };
		when(mockSendMailContext.getEmailAddress()).thenReturn(EMAIL_TO);
		when(mockSendMailContext.getBccEmailAddresses()).thenReturn(bccEmailAddresses);
		when(mockSendMailContext.getFromEmailAddress()).thenReturn(EMAIL_FROM);
		when(mockSendMailContext.getFromName()).thenReturn(NAME_FROM);
		when(mockEmailNotificationUtils.getInternetAddress(EMAIL_TO, null)).thenReturn(mockInternetAddressTo);
		when(mockEmailNotificationUtils.getInternetAddress(bccEmailAddresses)).thenReturn(internetAddressAdditionalEmailRecipients);
		when(mockEmailNotificationUtils.getInternetAddress(EMAIL_FROM, NAME_FROM)).thenReturn(mockInternetAddressFrom);
		when(mockEmailNotificationUtils.createMailMessage()).thenReturn(mockMailMessage);
		List<File> attachments = new LinkedList<>();
		attachments.add(mockFile1);
		attachments.add(mockFile2);
		when(mockSendMailContext.getAttachments()).thenReturn(attachments);

		defaultEmailNotificationService.sendMail(mockSendMailContext, mockServiceContext);

		InOrder inOrder = inOrder(mockMailMessage, mockMailService);
		inOrder.verify(mockMailMessage, times(1)).setTo(mockInternetAddressTo);
		inOrder.verify(mockMailMessage, times(1)).setBCC(internetAddressAdditionalEmailRecipients);
		inOrder.verify(mockMailMessage, times(1)).setFrom(mockInternetAddressFrom);
		inOrder.verify(mockMailMessage, times(1)).setSubject(SUBJECT_TEXT);
		inOrder.verify(mockMailMessage, times(1)).setBody(BODY_TEXT);
		inOrder.verify(mockMailMessage, times(1)).setHTMLFormat(true);
		inOrder.verify(mockMailMessage, times(1)).addFileAttachment(mockFile1);
		inOrder.verify(mockMailMessage, times(1)).addFileAttachment(mockFile2);
		inOrder.verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	private void mockValidContent(long groupId) throws PortalException {
		Locale locale = Locale.ENGLISH;
		String subject = "subjectValue";
		String body = "bodyValue";
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockSendMailContext.getTemplateId()).thenReturn(TEMPLATE_ID);
		when(mockJournalArticleLocalService.fetchLatestArticle(groupId, TEMPLATE_ID, WorkflowConstants.STATUS_APPROVED)).thenReturn(mockJournalArticle);
		when(mockJournalArticleLocalService.getArticleDisplay(groupId, TEMPLATE_ID, null, null, mockThemeDisplay)).thenReturn(mockJournalArticleDisplay);
		when(mockServiceContext.getLocale()).thenReturn(locale);
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, "Subject", locale)).thenReturn(Optional.ofNullable(subject));
		when(mockJournalArticleRetrievalService.getFieldValue(mockJournalArticle, "Body", locale)).thenReturn(Optional.ofNullable(body));
		when(mockSendMailContext.getPersonalisation()).thenReturn(mockPersonlisation);
		when(mockEmailNotificationUtils.getSubjectTextField(subject, mockPersonlisation)).thenReturn(SUBJECT_TEXT);
		when(mockJournalArticleDisplay.getContent()).thenReturn(body);
		when(mockEmailNotificationUtils.getBodyTextField(body, mockPersonlisation)).thenReturn(BODY_TEXT);
	}
}
