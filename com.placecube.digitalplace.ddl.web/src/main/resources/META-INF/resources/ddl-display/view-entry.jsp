<%@ include file="init.jsp"%>

<c:if test="${hasUpdatePermission or hasDeletePermission}">
	<div class="row">
		<div class="col-md-12">
			<div class="ddl-manage-entry-actions">
				<c:if test="${hasUpdatePermission}">
					<portlet:renderURL var="editDDLEntryURL" windowState="${windowStateMaximized}">
						<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.UPDATE_ENTRY %>" />
						<portlet:param name="ddmStructureId" value="${ddmStructure.getStructureId()}"/>
						<portlet:param name="ddlRecordId" value="${ddlRecordId}"/>
						<portlet:param name="ddlTemplateId" value="${ddlTemplateId}"/>
						<portlet:param name="redirect" value="${themeDisplay.getURLCurrent()}"/>
					</portlet:renderURL>
					
					<aui:button href="${editDDLEntryURL}" icon="pencil" iconAlign="right" primary="true" value="edit"/>
				</c:if>
				
				<c:if test="${hasDeletePermission}">
					<portlet:actionURL name="<%= MVCCommandKeys.DELETE_ENTRY %>" var="deleteDDLEntryURL" windowState="${windowStateNormal}">
						<portlet:param name="ddlRecordId" value="${ddlRecordId}"/>
						<portlet:param name="redirect" value="${redirect}"/>
					</portlet:actionURL>
					
					<aui:button icon="trash" iconAlign="right" value="delete" onClick="${portletNamespace}deleteDDL();" />
					<aui:script>
						function <portlet:namespace />deleteDDL() {
							if (confirm('<liferay-ui:message key="are-you-sure-you-want-to-delete-this" />')) {
								window.location='${deleteDDLEntryURL}';
							}
						};
					</aui:script>
				</c:if>
			</div>
		</div>
	</div>
</c:if>

<div class="row">
	<div class="col-md-12">
		<div class="ddl-display-entry-content">
			${ddlContentForTemplate}
		</div>
	</div>
</div>
