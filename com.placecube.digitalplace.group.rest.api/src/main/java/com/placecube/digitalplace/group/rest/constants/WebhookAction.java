package com.placecube.digitalplace.group.rest.constants;

public enum WebhookAction {

	ADD,

	UPDATE,

	DELETE;

}
