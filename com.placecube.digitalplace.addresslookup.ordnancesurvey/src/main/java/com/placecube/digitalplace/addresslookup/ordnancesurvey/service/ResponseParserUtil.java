package com.placecube.digitalplace.addresslookup.ordnancesurvey.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.ordnancesurvey.constants.OrdnanceSurveyConstants;

@Component(immediate = true, service = ResponseParserUtil.class)
public class ResponseParserUtil {

	@Reference
	private JSONFactory jsonFactory;

	public AddressContext getAddressContextResultForDPA(JSONObject entryJsonObject) {
		String subBuildingName = entryJsonObject.getString(OrdnanceSurveyConstants.SUB_BUILDING_NAME, StringPool.BLANK);
		String buildingName = entryJsonObject.getString(OrdnanceSurveyConstants.BUILDING_NAME, StringPool.BLANK);
		String buildingNumber = entryJsonObject.getString(OrdnanceSurveyConstants.BUILDING_NUMBER, StringPool.BLANK);
		String organisationName = entryJsonObject.getString(OrdnanceSurveyConstants.ORGANISATION_NAME, StringPool.BLANK);
		String dependentLocality = entryJsonObject.getString(OrdnanceSurveyConstants.DEPENDENT_LOCALITY, StringPool.BLANK);
		String doubleDependentLocality = entryJsonObject.getString(OrdnanceSurveyConstants.DOUBLE_DEPENDENT_LOCALITY, StringPool.BLANK);
		String thoroughfareName = entryJsonObject.getString(OrdnanceSurveyConstants.THOROUGHFARE_NAME, StringPool.BLANK);
		String dependentThoroughfareName = entryJsonObject.getString(OrdnanceSurveyConstants.DEPENDENT_THOROUGHFARE_NAME, StringPool.BLANK);
		String city = entryJsonObject.getString(OrdnanceSurveyConstants.POST_TOWN, StringPool.BLANK);
		String postcode = entryJsonObject.getString(OrdnanceSurveyConstants.POSTCODE, StringPool.BLANK);
		String uprn = entryJsonObject.getString(OrdnanceSurveyConstants.UPRN, StringPool.BLANK);

		String buildingDetails = getJoinedValuesWithSpace(subBuildingName, buildingName);

		String addressLine1 = getJoinedValuesWithSpace(organisationName, Validator.isNotNull(buildingDetails) ? buildingDetails : buildingNumber);

		String streetName = getJoinedValuesWithSpace(dependentThoroughfareName, thoroughfareName);
		String addressLine2 = Validator.isNotNull(buildingDetails) ? buildingNumber + StringPool.SPACE + streetName : streetName;

		String addressLine3 = getJoinedValuesWithSpace(dependentLocality, doubleDependentLocality);

		String addressLine4 = StringPool.BLANK;

		return new AddressContext.AddressContextBuilder().setAddressLine1(addressLine1).setAddressLine2(addressLine2).setAddressLine3(addressLine3).setAddressLine4(addressLine4).setCity(city)
				.setPostcode(postcode).setSource(OrdnanceSurveyConstants.DPA).setUprn(uprn).build();
	}

	public AddressContext getAddressContextResultForLPI(JSONObject entryJsonObject) {
		String organisation = entryJsonObject.getString(OrdnanceSurveyConstants.ORGANISATION, StringPool.BLANK);
		String saoText = entryJsonObject.getString(OrdnanceSurveyConstants.SAO_TEXT, StringPool.BLANK);
		String saoStart = getJoinedValuesFromFieldNamesWithNoSpace(entryJsonObject, OrdnanceSurveyConstants.SAO_START_NUMBER, OrdnanceSurveyConstants.SAO_START_SUFFIX);
		String saoEnd = getJoinedValuesFromFieldNamesWithNoSpace(entryJsonObject, OrdnanceSurveyConstants.SAO_END_NUMBER, OrdnanceSurveyConstants.SAO_END_SUFFIX);
		String paoText = entryJsonObject.getString(OrdnanceSurveyConstants.PAO_TEXT, StringPool.BLANK);
		String paoStart = getJoinedValuesFromFieldNamesWithNoSpace(entryJsonObject, OrdnanceSurveyConstants.PAO_START_NUMBER, OrdnanceSurveyConstants.PAO_START_SUFFIX);
		String paoEnd = getJoinedValuesFromFieldNamesWithNoSpace(entryJsonObject, OrdnanceSurveyConstants.PAO_END_NUMBER, OrdnanceSurveyConstants.PAO_END_SUFFIX);

		String streetDescription = entryJsonObject.getString(OrdnanceSurveyConstants.STREET_DESCRIPTION, StringPool.BLANK);
		String localityName = entryJsonObject.getString(OrdnanceSurveyConstants.LOCALITY_NAME, StringPool.BLANK);
		String uprn = entryJsonObject.getString(OrdnanceSurveyConstants.UPRN, StringPool.BLANK);
		String city = entryJsonObject.getString(OrdnanceSurveyConstants.TOWN_NAME, StringPool.BLANK);
		String postcode = entryJsonObject.getString(OrdnanceSurveyConstants.POSTCODE_LOCATOR, StringPool.BLANK);

		String addressLine1 = getJoinedValuesWithSpace(organisation, saoText, saoStart, saoEnd, paoText, paoStart, paoEnd);
		String addressLine2 = streetDescription;
		String addressLine3 = localityName;
		String addressLine4 = StringPool.BLANK;

		return new AddressContext.AddressContextBuilder().setAddressLine1(addressLine1).setAddressLine2(addressLine2).setAddressLine3(addressLine3).setAddressLine4(addressLine4).setCity(city)
				.setPostcode(postcode).setSource(OrdnanceSurveyConstants.LPI).setUprn(uprn).build();
	}

	public JSONArray getResults(String responseValue) throws JSONException {
		JSONObject responseJsonObject = jsonFactory.createJSONObject(responseValue);
		return responseJsonObject.getJSONArray("results");
	}

	private String getJoinedValuesFromFieldNamesWithNoSpace(JSONObject recordJSONObject, String... fieldNames) {
		List<String> values = new ArrayList<>();
		for (String fieldName : fieldNames) {
			values.add(recordJSONObject.getString(fieldName, StringPool.BLANK));
		}
		return values.stream().filter(Validator::isNotNull).collect(Collectors.joining(StringPool.BLANK));
	}

	private String getJoinedValuesWithSpace(String... values) {
		return Stream.of(values).filter(Validator::isNotNull).collect(Collectors.joining(StringPool.SPACE));
	}

}
