/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.dataconsumer.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.dataconsumer.exception.NoSuchDataConsumerException;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the data consumer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DataConsumerUtil
 * @generated
 */
@ProviderType
public interface DataConsumerPersistence extends BasePersistence<DataConsumer> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DataConsumerUtil} to access the data consumer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the data consumers where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching data consumers
	 */
	public java.util.List<DataConsumer> findByUuid(String uuid);

	/**
	 * Returns a range of all the data consumers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where uuid = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public DataConsumer[] findByUuid_PrevAndNext(
			long dataConsumerId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Removes all the data consumers where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of data consumers where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching data consumers
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the data consumer where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDataConsumerException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByUUID_G(String uuid, long groupId)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the data consumer where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the data consumer where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the data consumer where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the data consumer that was removed
	 */
	public DataConsumer removeByUUID_G(String uuid, long groupId)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the number of data consumers where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching data consumers
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching data consumers
	 */
	public java.util.List<DataConsumer> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns an ordered range of all the data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the first data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the last data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public DataConsumer[] findByUuid_C_PrevAndNext(
			long dataConsumerId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Removes all the data consumers where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of data consumers where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching data consumers
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @return the matching data consumers
	 */
	public java.util.List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId);

	/**
	 * Returns a range of all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId, int start, int end);

	/**
	 * Returns an ordered range of all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns an ordered range of all the data consumers where ddmFormInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByDDMFormInstanceId(
		long ddmFormInstanceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByDDMFormInstanceId_First(
			long ddmFormInstanceId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the first data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByDDMFormInstanceId_First(
		long ddmFormInstanceId,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the last data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByDDMFormInstanceId_Last(
			long ddmFormInstanceId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the last data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByDDMFormInstanceId_Last(
		long ddmFormInstanceId,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where ddmFormInstanceId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public DataConsumer[] findByDDMFormInstanceId_PrevAndNext(
			long dataConsumerId, long ddmFormInstanceId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Removes all the data consumers where ddmFormInstanceId = &#63; from the database.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 */
	public void removeByDDMFormInstanceId(long ddmFormInstanceId);

	/**
	 * Returns the number of data consumers where ddmFormInstanceId = &#63;.
	 *
	 * @param ddmFormInstanceId the ddm form instance ID
	 * @return the number of matching data consumers
	 */
	public int countByDDMFormInstanceId(long ddmFormInstanceId);

	/**
	 * Returns all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @return the matching data consumers
	 */
	public java.util.List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId);

	/**
	 * Returns a range of all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId, int start, int end);

	/**
	 * Returns an ordered range of all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns an ordered range of all the data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByDDMDataProviderInstanceId_First(
			long ddmDataProviderInstanceId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the first data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByDDMDataProviderInstanceId_First(
		long ddmDataProviderInstanceId,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the last data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByDDMDataProviderInstanceId_Last(
			long ddmDataProviderInstanceId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the last data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByDDMDataProviderInstanceId_Last(
		long ddmDataProviderInstanceId,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public DataConsumer[] findByDDMDataProviderInstanceId_PrevAndNext(
			long dataConsumerId, long ddmDataProviderInstanceId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Removes all the data consumers where ddmDataProviderInstanceId = &#63; from the database.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 */
	public void removeByDDMDataProviderInstanceId(
		long ddmDataProviderInstanceId);

	/**
	 * Returns the number of data consumers where ddmDataProviderInstanceId = &#63;.
	 *
	 * @param ddmDataProviderInstanceId the ddm data provider instance ID
	 * @return the number of matching data consumers
	 */
	public int countByDDMDataProviderInstanceId(long ddmDataProviderInstanceId);

	/**
	 * Returns all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching data consumers
	 */
	public java.util.List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId);

	/**
	 * Returns a range of all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns an ordered range of all the data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching data consumers
	 */
	public java.util.List<DataConsumer> findByCompanyIdAndGroupId(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByCompanyIdAndGroupId_First(
			long companyId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the first data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByCompanyIdAndGroupId_First(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the last data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer
	 * @throws NoSuchDataConsumerException if a matching data consumer could not be found
	 */
	public DataConsumer findByCompanyIdAndGroupId_Last(
			long companyId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the last data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching data consumer, or <code>null</code> if a matching data consumer could not be found
	 */
	public DataConsumer fetchByCompanyIdAndGroupId_Last(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns the data consumers before and after the current data consumer in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param dataConsumerId the primary key of the current data consumer
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public DataConsumer[] findByCompanyIdAndGroupId_PrevAndNext(
			long dataConsumerId, long companyId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
				orderByComparator)
		throws NoSuchDataConsumerException;

	/**
	 * Removes all the data consumers where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 */
	public void removeByCompanyIdAndGroupId(long companyId, long groupId);

	/**
	 * Returns the number of data consumers where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching data consumers
	 */
	public int countByCompanyIdAndGroupId(long companyId, long groupId);

	/**
	 * Caches the data consumer in the entity cache if it is enabled.
	 *
	 * @param dataConsumer the data consumer
	 */
	public void cacheResult(DataConsumer dataConsumer);

	/**
	 * Caches the data consumers in the entity cache if it is enabled.
	 *
	 * @param dataConsumers the data consumers
	 */
	public void cacheResult(java.util.List<DataConsumer> dataConsumers);

	/**
	 * Creates a new data consumer with the primary key. Does not add the data consumer to the database.
	 *
	 * @param dataConsumerId the primary key for the new data consumer
	 * @return the new data consumer
	 */
	public DataConsumer create(long dataConsumerId);

	/**
	 * Removes the data consumer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer that was removed
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public DataConsumer remove(long dataConsumerId)
		throws NoSuchDataConsumerException;

	public DataConsumer updateImpl(DataConsumer dataConsumer);

	/**
	 * Returns the data consumer with the primary key or throws a <code>NoSuchDataConsumerException</code> if it could not be found.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer
	 * @throws NoSuchDataConsumerException if a data consumer with the primary key could not be found
	 */
	public DataConsumer findByPrimaryKey(long dataConsumerId)
		throws NoSuchDataConsumerException;

	/**
	 * Returns the data consumer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dataConsumerId the primary key of the data consumer
	 * @return the data consumer, or <code>null</code> if a data consumer with the primary key could not be found
	 */
	public DataConsumer fetchByPrimaryKey(long dataConsumerId);

	/**
	 * Returns all the data consumers.
	 *
	 * @return the data consumers
	 */
	public java.util.List<DataConsumer> findAll();

	/**
	 * Returns a range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @return the range of data consumers
	 */
	public java.util.List<DataConsumer> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of data consumers
	 */
	public java.util.List<DataConsumer> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator);

	/**
	 * Returns an ordered range of all the data consumers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DataConsumerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of data consumers
	 * @param end the upper bound of the range of data consumers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of data consumers
	 */
	public java.util.List<DataConsumer> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DataConsumer>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the data consumers from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of data consumers.
	 *
	 * @return the number of data consumers
	 */
	public int countAll();

}