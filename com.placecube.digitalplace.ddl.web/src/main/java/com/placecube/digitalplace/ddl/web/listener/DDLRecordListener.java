package com.placecube.digitalplace.ddl.web.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.message.boards.service.MBMessageLocalService;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;

@Component(immediate = true, service = ModelListener.class)
public class DDLRecordListener extends BaseModelListener<DDLRecord> {

	private static final Log LOG = LogFactoryUtil.getLog(DDLRecordListener.class);

	@Reference
	private MBMessageLocalService mbMessageLocalService;

	@Override
	public void onAfterRemove(DDLRecord model) throws ModelListenerException {
		try {
			mbMessageLocalService.deleteDiscussionMessages(DDLRecord.class.getName(), model.getRecordId());
		} catch (PortalException e) {
			LOG.warn("Unable to clear DiscussionMessages for deleted ddl record - " + e.getMessage());
		}
	}

}
