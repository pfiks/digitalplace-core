package com.placecube.digitalplace.payment.capitapaymentinterface.models;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder.CardRequestType;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleConfiguration;

@RunWith(MockitoJUnitRunner.class)
public class OneOffPaymentOrderTest {

	private static final String AUTH_CODE = "auth";
	private static final String BACK_URL = "/back";
	private static final String CARD_TOKEN = "token";
	private static final int CONTINUOUS_AUDIT_NUMBER = 32423;
	private static final CardRequestType CARD_REQUEST_TYPE = CardRequestType.PAY_ONLY;
	private static final String EMAIL_ADDRESS = "email";
	private static final String FULL_NAME = "name";
	private static final String GENERATED_REF_NUMBER = "ref";
	private static final String INTERNAL_REFERENCE = "internalRef";
	private static final String LAST_4_DIGITS = "1234";
	private static final BigDecimal PAYMENT_VALUE_PROCESSED = BigDecimal.valueOf(454d);
	private static final String REDIRECT_URL = "/redirect";
	private static final String RETURN_URL = "/return";
	private static final int SALE_AMOUNT = 3245;
	private static final String SALE_DESCRIPTION = "desc";
	private static final String SALE_REF_CODE = "code";
	private static final String STATUS = "status";
	private static final String TRANSACTION_REF_NUMBER = "transactionRef";
	private static final String UNIQUE_TRANS_ID = "976654";

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private SimpleSaleConfiguration mockSimpleSaleConfiguration;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void equals_WhenObjectsAreEquals_ThenReturnsTrue() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		assertThat(one.equals(two), equalTo(true));
	}

	@Test
	public void equals_WhenObjectIsNull_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		assertThat(one.equals(null), equalTo(false));
	}

	@Test
	public void equals_WhenObjectIsOfDifferentClass_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		assertThat(one.equals(new Object()), equalTo(false));
	}

	@Test
	public void equals_WhenAddressIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setAddress(null);

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenAuthCodeIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setAuthCode("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenBackURLIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setBackURL("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenCardtokenIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setCardToken("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenContinuousAuditNumberIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setContinuousAuditNumber(555);

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenEmailAddressIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setEmailAddress("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenFullNameIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setFullName("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenGeneratedReferenceNumberIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setGeneratedReferenceNumber("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenInternalReferenceIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setInternalReference("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenLast4DigitsIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setLast4Digits("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenPaymentValueProcessedIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setPaymentValueProcessed(BigDecimal.TEN);

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenRedirectUrlIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setRedirectUrl("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenRequestTypeIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setRequestType(CardRequestType.PAY_AND_STORE_CARD);

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenReturnURLIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setReturnURL("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenSaleAmountIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setSaleAmount(8776);

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenSaleDescriptionIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setSaleDescription("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenSaleReferenceCodeIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setSaleReferenceCode("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenSimpleSaleConfigurationIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setSimpleSaleConfiguration(null);

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenStatusIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setStatus("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenTransactionReferenceNumberIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setTransactionReferenceNumber("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void equals_WhenUniqueTranIdIsDifferent_ThenReturnsFalse() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setUniqueTranId("test");

		assertThat(one.equals(two), equalTo(false));
	}

	@Test
	public void hashCode_WhenObjectsAreSimilar_ThenHashCodesAreSimilar() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		assertThat(one.hashCode(), equalTo(two.hashCode()));
	}

	@Test
	public void hashCode_WhenObjectsAreDifferent_ThenHashCodesAreDifferent() {
		OneOffPaymentOrder one = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(one);

		OneOffPaymentOrder two = new OneOffPaymentOrder();
		buildOneOffPaymentOrder(two);

		two.setBackURL("test");

		assertThat(one.hashCode() == two.hashCode(), equalTo(false));
	}

	public void buildOneOffPaymentOrder(OneOffPaymentOrder object) {
		object.setAddress(mockAddressContext);
		object.setAuthCode(AUTH_CODE);
		object.setBackURL(BACK_URL);
		object.setCardToken(CARD_TOKEN);
		object.setContinuousAuditNumber(CONTINUOUS_AUDIT_NUMBER);
		object.setEmailAddress(EMAIL_ADDRESS);
		object.setFullName(FULL_NAME);
		object.setGeneratedReferenceNumber(GENERATED_REF_NUMBER);
		object.setInternalReference(INTERNAL_REFERENCE);
		object.setLast4Digits(LAST_4_DIGITS);
		object.setPaymentValueProcessed(PAYMENT_VALUE_PROCESSED);
		object.setRedirectUrl(REDIRECT_URL);
		object.setRequestType(CARD_REQUEST_TYPE);
		object.setReturnURL(RETURN_URL);
		object.setSaleAmount(SALE_AMOUNT);
		object.setSaleDescription(SALE_DESCRIPTION);
		object.setSaleReferenceCode(SALE_REF_CODE);
		object.setSimpleSaleConfiguration(mockSimpleSaleConfiguration);
		object.setStatus(STATUS);
		object.setTransactionReferenceNumber(TRANSACTION_REF_NUMBER);
		object.setUniqueTranId(UNIQUE_TRANS_ID);
	}
}
