package com.placecube.digitalplace.payment.capitapaymentinterface.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Random;

import org.apache.axis.types.PositiveInteger;
import org.apache.axis.types.Token;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import com.capita_software_services.www.portal_api.CardDescription;
import com.capita_software_services.www.scp.base.AuthDetails;
import com.capita_software_services.www.scp.base.CardType;
import com.capita_software_services.www.scp.base.PaymentHeader;
import com.capita_software_services.www.scp.base.Status;
import com.capita_software_services.www.scp.base.StoreCardResult;
import com.capita_software_services.www.scp.base.TransactionState;
import com.capita_software_services.www.scp.simple.ScpSimpleQueryResponse;
import com.capita_software_services.www.scp.simple.SimpleItemSummary;
import com.capita_software_services.www.scp.simple.SimplePayment;
import com.capita_software_services.www.scp.simple.SimplePaymentResult;
import com.capita_software_services.www.scp.simple.SimpleSaleSummary;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;
import com.placecube.digitalplace.payment.capitainterface.service.CapitaPaymentService;
import com.placecube.digitalplace.payment.capitainterface.service.PaymentService;

@Ignore("Disabled due to tests not being compatible with builds on JDK11")
public class PaymentServiceUnitTest {

	PaymentService instance;

	@Before
	public void setUp() throws Exception {

		PaymentProperties properties = new PaymentProperties();
		properties.setEndpoint("https://sbsctest.e-paycapita.com:443/scp/scpws");
		properties.setHmacKey("0kvRVpMjRq+anPskP8cGmDtPR6PjpIZfLtHo8KR6BIHpsGmrRH64PswYxUGNI+KyBMNpWNUi+EZ2CE4T7OT2Ag==");
		properties.setHmacKeyId(456);
		properties.setScpId(8389789);
		properties.setSiteId(84);

		instance = new PaymentService(properties);

	}

	@Test
	public void testInvokeQueryServiceOneOffPaymentOrder() {

		// Empty order, should fail
		OneOffPaymentOrder order = new OneOffPaymentOrder();
		// Use mocking to return a payment successful object
		CapitaPaymentService capitaService = Mockito.mock(CapitaPaymentService.class);
		try {
			instance.setCapitaPaymentService(capitaService);

			// Build a fake Capita response object
			ScpSimpleQueryResponse response = new ScpSimpleQueryResponse();

			// fill in response with the required data
			response.setRequestId(new Token("BCC_LR_3115"));
			response.setScpReference("ovyflye6e6qt2zbxsstzddmjfs7cs7s");
			response.setTransactionState(TransactionState.COMPLETE);

			SimplePaymentResult paymentResult = new SimplePaymentResult();
			paymentResult.setStatus(Status.SUCCESS);
			SimplePayment simplePayment = new SimplePayment();

			PaymentHeader paymentHeader = new PaymentHeader();
			paymentHeader.setMachineCode("00036");
			paymentHeader.setTransactionDate(Calendar.getInstance());
			paymentHeader.setUniqueTranId("VBFZY64FLH2E");
			simplePayment.setPaymentHeader(paymentHeader);

			AuthDetails authDetails = new AuthDetails();
			authDetails.setAmountInMinorUnits(5300);
			authDetails.setAuthCode("100808");
			authDetails.setMaskedCardNumber("471505******0437");
			authDetails.setCardDescription(CardDescription.fromString("VISA"));
			authDetails.setCardType(CardType.CREDIT);
			authDetails.setMerchantNumber("12345678");
			authDetails.setExpiryDate("1217");
			authDetails.setContinuousAuditNumber(new PositiveInteger(130, new Random()));
			simplePayment.setAuthDetails(authDetails);

			SimpleSaleSummary saleSummary = new SimpleSaleSummary();

			SimpleItemSummary simpleItemSummary = new SimpleItemSummary();
			simpleItemSummary.setLineId(new Token("1"));
			simpleItemSummary.setContinuousAuditNumber(new PositiveInteger(130, new Random()));

			SimpleItemSummary[] items = new SimpleItemSummary[1];
			items[0] = simpleItemSummary;

			saleSummary.setItems(items);
			simplePayment.setSaleSummary(saleSummary);

			paymentResult.setPaymentDetails(simplePayment);
			response.setPaymentResult(paymentResult);

			StoreCardResult storeCardResult = new StoreCardResult();
			storeCardResult.setStatus(Status.NOT_ATTEMPTED);
			response.setStoreCardResult(storeCardResult);

			when(capitaService.capitaQuery(anyString(), anyString())).thenReturn(response);

			boolean success = instance.invokeQueryServiceOneOffPaymentOrder(order);
			assertTrue(success);
			assertEquals(order.getAuthCode(), "100808");
			assertTrue(Validator.isNull(order.getCardToken()));
			assertEquals(order.getUniqueTranId(), "VBFZY64FLH2E");

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Test
	public void testPaymentService() throws Exception {

		PaymentProperties properties = new PaymentProperties();
		properties.setEndpoint("https://sbsctest.e-paycapita.com:443/scp/scpws");
		properties.setHmacKey("11112222");
		properties.setHmacKeyId(1111);
		properties.setScpId(11);
		properties.setSiteId(111);

		instance = new PaymentService(properties);
		assertTrue(properties.getScpId() == 11);
		assertEquals(properties.getHmacKey(), "11112222");
		assertTrue(properties.getHmacKeyId() == 1111);
		assertTrue(properties.getSiteId() == 111);

	}

}
