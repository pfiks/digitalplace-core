package com.placecube.digitalplace.navigation.portlet;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.fragment.processor.PortletRegistry;
import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.taglib.ui.BreadcrumbEntry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.site.navigation.taglib.servlet.taglib.util.BreadcrumbUtil;
import com.placecube.digitalplace.navigation.configuration.BreadcrumbPortletInstanceConfiguration;
import com.placecube.digitalplace.navigation.constants.PortletPreferenceKeys;
import com.placecube.digitalplace.navigation.model.BreadcrumbServiceContext;
import com.placecube.digitalplace.navigation.service.BreadcrumbService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BreadcrumbUtil.class, ConfigurableUtil.class, MVCPortlet.class, PropsUtil.class, ReflectionUtil.class })
public class BreadcrumbPortletTest extends PowerMockito {

	private static final String CATEGORY_NAV_ROOT_ENTRY_NAME_CONFIGURATION = "categoryNavRootEntryNameConfiguration";

	private static final String CATEGORY_NAV_ROOT_ENTRY_NAME_PREFERENCE = "categoryNavRootEntryNamePreference";

	private static final String DISPLAY_STYLE = "displayStyle";

	private static final String DISPLAY_STYLE_GROUP_ID = "21";

	@InjectMocks
	private BreadcrumbPortlet breadcrumbPortlet;

	private List<List<BreadcrumbEntry>> categoryBreadcrumbs;

	@Mock
	private Map<Object, Object> mockActivationProperties;

	@Mock
	private BreadcrumbEntry mockBreadcrumbEntry1;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private Optional<BreadcrumbEntry> mockOptionalBreadcrumbEntry;

	@Mock
	private BreadcrumbServiceContext mockBreadcrumbServiceContext;

	@Mock
	private BreadcrumbPortletInstanceConfiguration mockBreadcrumbConfiguration;

	@Mock
	private BreadcrumbService mockBreadcrumbService;

	@Mock
	private Layout mockLayout;

	@Mock
	private List<BreadcrumbEntry> mockLayoutBreadcrumbs;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private PortletRegistry mockPortletRegistry;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void setUp() {

		mockStatic(BreadcrumbUtil.class, ConfigurableUtil.class, PropsUtil.class, ReflectionUtil.class);

		categoryBreadcrumbs = new ArrayList<>();

	}

	@Test
	public void activate_WhenBreadcrumbConfigurationFound_ThenDoesNotThrowException() {

		when(ConfigurableUtil.createConfigurable(BreadcrumbPortletInstanceConfiguration.class, mockActivationProperties)).thenReturn(mockBreadcrumbConfiguration);

		breadcrumbPortlet.activate(mockActivationProperties);

		assertTrue(true);

	}

	@Test(expected = IllegalStateException.class)
	public void activate_WhenBreadcrumbConfigurationNotFound_ThenThrowsException() {

		when(ConfigurableUtil.createConfigurable(BreadcrumbPortletInstanceConfiguration.class, mockActivationProperties)).thenReturn(null);

		breadcrumbPortlet.activate(mockActivationProperties);

	}

	@Test
	public void render_WhenIsAssetDisplayPage_ThenAppliesCurrentLayoutPreferenceFilterAndAddsCategoryBreadcrumbsToRequestInOrder() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		mockPortletPreferences();
		mockBreadcrumbConfiguration();
		mockBreadcrumbServiceContext();
		mockBreadcrumbServiceMethodResponses();
		suppressRenderMethodCall();
		when(BreadcrumbUtil.getLayoutBreadcrumbEntries(mockHttpServletRequest, mockThemeDisplay)).thenReturn(mockLayoutBreadcrumbs);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);

		breadcrumbPortlet.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = inOrder(mockBreadcrumbService, mockRenderRequest);

		inOrder.verify(mockBreadcrumbService, times(1)).applyCurrentLayoutPreferenceFilterToBreadcrumbTree(categoryBreadcrumbs, mockRenderRequest);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("categoryBreadcrumbEntries", categoryBreadcrumbs);
	}

	@Test
	public void render_WhenIsAssetDisplayPage_ThenDoesNotAddLayoutBreadcrumbsToRequest() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		mockPortletPreferences();
		mockBreadcrumbConfiguration();
		mockBreadcrumbServiceContext();
		mockBreadcrumbServiceMethodResponses();
		suppressRenderMethodCall();
		when(BreadcrumbUtil.getLayoutBreadcrumbEntries(mockHttpServletRequest, mockThemeDisplay)).thenReturn(mockLayoutBreadcrumbs);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);

		breadcrumbPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(same("layoutBreadcrumbEntries"), any());

	}

	@Test(expected = PortletException.class)
	public void render_WhenIsAssetDisplayPageAndErrorRetrievingSiteLayoutBreadcrumb_ThenThrowsException() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		mockPortletPreferences();
		mockBreadcrumbConfiguration();
		mockBreadcrumbServiceContext();
		mockBreadcrumbServiceMethodResponses();
		suppressRenderMethodCall();
		when(BreadcrumbUtil.getLayoutBreadcrumbEntries(mockHttpServletRequest, mockThemeDisplay)).thenReturn(mockLayoutBreadcrumbs);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);
		when(mockBreadcrumbService.getSiteLayoutBreadcrumbEntry(any(ThemeDisplay.class), anyString())).thenThrow(new Exception());

		breadcrumbPortlet.render(mockRenderRequest, mockRenderResponse);

	}

	@Test
	public void render_WhenIsNotAssetDisplayPage_ThenAppliesCurrentLayoutPreferenceFilterAndAddsRootElementAndAddsLayoutBreadcrumbsToRequest() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		mockPortletPreferences();
		mockBreadcrumbConfiguration();
		mockBreadcrumbServiceContext();
		mockBreadcrumbServiceMethodResponses();
		suppressRenderMethodCall();
		when(BreadcrumbUtil.getLayoutBreadcrumbEntries(mockHttpServletRequest, mockThemeDisplay)).thenReturn(mockLayoutBreadcrumbs);
		when(mockLayout.getType()).thenReturn(StringPool.BLANK);

		breadcrumbPortlet.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = inOrder(mockBreadcrumbService, mockRenderRequest);

		inOrder.verify(mockBreadcrumbService, times(1)).addRootEntryIfNecessary(mockLayoutBreadcrumbs, mockRenderRequest, mockOptionalBreadcrumbEntry);
		inOrder.verify(mockBreadcrumbService, times(1)).applyCurrentLayoutPreferenceFilter(mockLayoutBreadcrumbs, mockRenderRequest);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute("layoutBreadcrumbEntries", mockLayoutBreadcrumbs);
	}

	@Test
	public void render_WhenIsNotAssetDisplayPage_ThenDoesNotAddCategoryBreadcrumbsToRequest() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		mockPortletPreferences();
		mockBreadcrumbConfiguration();
		mockBreadcrumbServiceContext();
		mockBreadcrumbServiceMethodResponses();
		suppressRenderMethodCall();
		when(BreadcrumbUtil.getLayoutBreadcrumbEntries(mockHttpServletRequest, mockThemeDisplay)).thenReturn(mockLayoutBreadcrumbs);
		when(mockLayout.getType()).thenReturn(StringPool.BLANK);

		breadcrumbPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute("categoryBreadcrumbEntries", categoryBreadcrumbs);

	}

	@Test
	public void render_WhenIsNotAssetDisplayPage_ThenAddsDisplayPropertiesToRequest() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		mockPortletPreferences();
		mockBreadcrumbConfiguration();
		mockBreadcrumbServiceContext();
		mockBreadcrumbServiceMethodResponses();
		suppressRenderMethodCall();
		when(BreadcrumbUtil.getLayoutBreadcrumbEntries(mockHttpServletRequest, mockThemeDisplay)).thenReturn(mockLayoutBreadcrumbs);
		when(mockLayout.getType()).thenReturn(StringPool.BLANK);

		breadcrumbPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletPreferenceKeys.DISPLAY_STYLE, DISPLAY_STYLE);
		verify(mockRenderRequest, times(1)).setAttribute(PortletPreferenceKeys.DISPLAY_STYLE_GROUP_ID, Long.valueOf(DISPLAY_STYLE_GROUP_ID));

	}

	@Test
	public void render_WhenIsAssetDisplayPage_ThenAddsDisplayPropertiesToRequest() throws Exception {

		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		mockPortletPreferences();
		mockBreadcrumbConfiguration();
		mockBreadcrumbServiceContext();
		mockBreadcrumbServiceMethodResponses();
		suppressRenderMethodCall();
		when(BreadcrumbUtil.getLayoutBreadcrumbEntries(mockHttpServletRequest, mockThemeDisplay)).thenReturn(mockLayoutBreadcrumbs);
		when(mockLayout.getType()).thenReturn(LayoutConstants.TYPE_ASSET_DISPLAY);

		breadcrumbPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletPreferenceKeys.DISPLAY_STYLE, DISPLAY_STYLE);
		verify(mockRenderRequest, times(1)).setAttribute(PortletPreferenceKeys.DISPLAY_STYLE_GROUP_ID, Long.valueOf(DISPLAY_STYLE_GROUP_ID));

	}

	private void mockBreadcrumbConfiguration() {

		when(ConfigurableUtil.createConfigurable(BreadcrumbPortletInstanceConfiguration.class, mockActivationProperties)).thenReturn(mockBreadcrumbConfiguration);
		when(mockBreadcrumbConfiguration.categoryNavRootEntryName()).thenReturn(CATEGORY_NAV_ROOT_ENTRY_NAME_CONFIGURATION);

		breadcrumbPortlet.activate(mockActivationProperties);

	}

	private void mockBreadcrumbServiceContext() {

		when(mockBreadcrumbServiceContext.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockBreadcrumbServiceContext.getLayout()).thenReturn(mockLayout);

	}

	private void mockBreadcrumbServiceMethodResponses() throws Exception {

		when(mockBreadcrumbService.getBreadcrumbServiceContext(mockRenderRequest)).thenReturn(mockBreadcrumbServiceContext);
		when(mockBreadcrumbService.getDisplayPageBreadcrumbEntry(mockBreadcrumbServiceContext)).thenReturn(mockBreadcrumbEntry1);
		when(mockBreadcrumbService.getSiteLayoutBreadcrumbEntry(mockThemeDisplay, CATEGORY_NAV_ROOT_ENTRY_NAME_PREFERENCE)).thenReturn(mockOptionalBreadcrumbEntry);
		when(mockBreadcrumbService.getCategoryBreadcrumbEntries(mockBreadcrumbServiceContext, mockOptionalBreadcrumbEntry, mockBreadcrumbEntry1)).thenReturn(categoryBreadcrumbs);

	}

	private void mockPortletPreferences() {

		when(mockRenderRequest.getPreferences()).thenReturn(mockPortletPreferences);
		when(mockPortletPreferences.getValue(PortletPreferenceKeys.CATEGORY_NAV_ROOT_ENTRY_NAME, CATEGORY_NAV_ROOT_ENTRY_NAME_CONFIGURATION)).thenReturn(CATEGORY_NAV_ROOT_ENTRY_NAME_PREFERENCE);
		when(mockPortletPreferences.getValue(PortletPreferenceKeys.DISPLAY_STYLE, StringPool.BLANK)).thenReturn(DISPLAY_STYLE);
		when(mockPortletPreferences.getValue(PortletPreferenceKeys.DISPLAY_STYLE_GROUP_ID, "0")).thenReturn(DISPLAY_STYLE_GROUP_ID);
	}

	@SuppressWarnings("rawtypes")
	private void suppressRenderMethodCall() throws Exception {

		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);

	}

}
