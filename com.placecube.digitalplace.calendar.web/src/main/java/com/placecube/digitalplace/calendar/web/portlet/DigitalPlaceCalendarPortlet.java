package com.placecube.digitalplace.calendar.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.calendar.web.constants.PortletKeys;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=digitalplace-calendar", // //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=portlet-digitalplace-calendar", //
		"com.liferay.portlet.display-category=category.collaboration", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.config-template=/configuration.jsp", //
		"javax.portlet.init-param.view-template=/view.jsp", //
		"javax.portlet.name=" + PortletKeys.CALENDAR, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.supports.mime-type=text/html", //
		"com.liferay.portlet.header-portlet-css=/fullcalendar/main.min.css", //
		"com.liferay.portlet.header-portlet-javascript=/fullcalendar/moment.min.js", //
		"com.liferay.portlet.header-portlet-javascript=/fullcalendar/moment-timezone-with-data.min.js", //
		"com.liferay.portlet.header-portlet-javascript=/fullcalendar/tooltip.min.js", //
		"com.liferay.portlet.header-portlet-javascript=/fullcalendar/main.min.js", //
		"com.liferay.portlet.footer-portlet-javascript=/fullcalendar/main.global.min.js", //
		"com.liferay.portlet.footer-portlet-javascript=/fullcalendar/locales/cy.js", //
		"com.liferay.portlet.footer-portlet-javascript=/fullcalendar/locales/en.js", //
		"com.liferay.portlet.footer-portlet-javascript=/fullcalendar/locales/es.js", //
		"com.liferay.portlet.footer-portlet-javascript=/fullcalendar/locales/fr.js", //
		"com.liferay.portlet.footer-portlet-javascript=/fullcalendar/locales/it.js", //
		"com.liferay.portlet.footer-portlet-javascript=/fullcalendar/locales/pl.js", //
		"com.liferay.portlet.footer-portlet-javascript=/fullcalendar/locales/pt.js", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class DigitalPlaceCalendarPortlet extends MVCPortlet {

}
