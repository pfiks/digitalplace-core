package com.placecube.digitalplace.ddl.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactory;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.internal.util.DynamicDataListEntryRetrievalUtil;
import com.placecube.digitalplace.ddl.model.DynamicDataListEntry;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.digitalplace.layout.service.LayoutRetrievalService;

@Component(immediate = true, service = DynamicDataListService.class)
public class DynamicDataListServiceImpl implements DynamicDataListService {

	public static final String DDM_STRUCTURE_ID = "ddmStructureId";

	private static final String DDL_DISPLAY_PORTLET_PORTLET_KEY = "com_placecube_digitalplace_ddl_portlet_DDLDisplayPortlet";

	private static final Log LOG = LogFactoryUtil.getLog(DynamicDataListServiceImpl.class);

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private DDLRecordSetLocalService ddlRecordSetLocalService;

	@Reference
	private DynamicDataListConfigurationService dynamicDataListConfigurationService;

	@Reference
	private DynamicDataListEntryRetrievalUtil dynamicDataListEntryRetrievalUtil;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private LayoutRetrievalService layoutRetrievalService;

	@Reference
	private Portal portal;

	@Reference
	private SortFactory sortFactory;

	@Override
	public List<DynamicDataListEntry> getDDLRecordSetEntries(long ddlRecordSetId) {
		List<DDLRecord> records = ddlRecordLocalService.getRecords(ddlRecordSetId);

		return records.stream().map(record -> dynamicDataListEntryRetrievalUtil.getDynamicDataListEntry(record).orElse(null)).filter(Validator::isNotNull).collect(Collectors.toList());
	}

	@Override
	public List<DDLRecord> getSortedDDLRecordsByFieldName(HttpServletRequest httpServletRequest, String fieldName, String structureId) {
		Sort sort = sortFactory.create(fieldName, false);

		SearchContext searchContext = SearchContextFactory.getInstance(httpServletRequest);

		searchContext.setSorts(sort);

		addBooleanClause(searchContext, DDM_STRUCTURE_ID, structureId, BooleanClauseOccur.MUST.getName());

		Hits hits = ddlRecordLocalService.search(searchContext);

		Document documents[] = hits.getDocs();

		if (Objects.nonNull(documents)) {
			return Arrays.stream(documents).map(d -> ddlRecordLocalService.fetchDDLRecord(getDocumentEntryClassPK(d))).filter(Objects::nonNull).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	@Override
	public Optional<String> getViewDDLRecordURL(String portalURL, DDLRecord ddlRecord, boolean privateLayoutPreference, long viewDDLTemplateId) {
		return getViewDDLRecordURL(portalURL, ddlRecord, viewDDLTemplateId, privateLayoutPreference, WindowState.MAXIMIZED);
	}

	@Override
	public Optional<String> getViewDDLRecordURL(String portalURL, DDLRecord ddlRecord, long viewDDLTemplateId, boolean privateLayoutPreference, WindowState windowState) {
		try {
			long ddlRecordGroupId = ddlRecord.getGroupId();
			if (ddlRecordGroupId == 0) {
				return Optional.empty();
			}
			DDMStructure ddmStructure = ddlRecord.getRecordSet().getDDMStructure();
			Optional<Layout> layoutOptional = layoutRetrievalService.getPreferredLayoutWithPortletId(ddlRecordGroupId, DDL_DISPLAY_PORTLET_PORTLET_KEY, privateLayoutPreference,
					Collections.singletonMap("ddmStructureId", String.valueOf(ddmStructure.getStructureId())));

			if (layoutOptional.isPresent()) {
				Layout layout = layoutOptional.get();
				Group group = groupLocalService.getGroup(ddlRecordGroupId);

				String groupURL = layout.isPrivateLayout() ? getGroupURL(portalURL, group, true) : getGroupURL(portalURL, group, false);
				if (viewDDLTemplateId <= 0) {
					viewDDLTemplateId = getDefaultFullViewTemplateId(ddlRecord);
				}

				String viewUrl = groupURL + layout.getFriendlyURL() + "/-/ddl_display/ddl/" + ddlRecord.getRecordId() + "/" + viewDDLTemplateId + "/" + windowState;

				return Optional.of(viewUrl);

			}

		} catch (PortalException exception) {
			LOG.error("Portal exception getting resources for DDLRecord " + exception.getMessage());
			LOG.debug(exception);
		}
		LOG.warn("Layout or structure mapping not found for ddlRecordId: " + ddlRecord.getRecordId() + " - Unable to create view url");
		return Optional.empty();
	}

	@Override
	public String getViewDDLRecordURL(ThemeDisplay themeDisplay, DDLRecord ddlRecord, long viewDDLTemplateId) {
		return getViewDDLRecordURL(themeDisplay, ddlRecord, viewDDLTemplateId, WindowState.MAXIMIZED);
	}

	@Override
	public String getViewDDLRecordURL(ThemeDisplay themeDisplay, DDLRecord ddlRecord, long viewDDLTemplateId, String windowState) {

		WindowState windowStateClass = WindowState.NORMAL.toString().equals(windowState) ? WindowState.NORMAL : WindowState.MAXIMIZED;

		return getViewDDLRecordURL(themeDisplay, ddlRecord, viewDDLTemplateId, windowStateClass);

	}

	@Override
	public String getViewDDLRecordURL(ThemeDisplay themeDisplay, DDLRecord ddlRecord, long viewDDLTemplateId, WindowState windowState) {
		final String ddlDisplayUrlPart = "/-/ddl_display/ddl/";

		String urlCurrent = themeDisplay.getURLCurrent();

		String baseUrl = StringPool.BLANK;

		if (urlCurrent.contains(ddlDisplayUrlPart)) {
			baseUrl = StringUtil.extractFirst(urlCurrent, ddlDisplayUrlPart);
		} else {
			baseUrl = urlCurrent.contains("?") ? StringUtil.extractFirst(urlCurrent, "?") : urlCurrent;
		}

		Layout layout = themeDisplay.getLayout();
		if (baseUrl.equals("/")) {
			baseUrl = getGroupURL(themeDisplay.getPortalURL(), layout.getGroup(), layout.isPrivateLayout());
		}

		if (viewDDLTemplateId <= 0) {
			viewDDLTemplateId = getDefaultFullViewTemplateId(ddlRecord);
		}

		String layoutFriendlyURL = layout.getFriendlyURL(themeDisplay.getLocale());
		if (baseUrl.contains(layoutFriendlyURL)) {
			layoutFriendlyURL = StringPool.BLANK;
		}

		return baseUrl + layoutFriendlyURL + ddlDisplayUrlPart + ddlRecord.getRecordId() + "/" + viewDDLTemplateId + "/" + windowState;
	}

	private void addBooleanClause(SearchContext searchContext, String field, String value, String occur) {
		BooleanClause<Query> queryToAdd = BooleanClauseFactoryUtil.create(field, value, occur);

		BooleanClause<Query>[] booleanClauses = searchContext.getBooleanClauses();
		if (Validator.isNull(booleanClauses)) {
			booleanClauses = new BooleanClause[0];
		}

		searchContext.setBooleanClauses(ArrayUtil.append(booleanClauses, queryToAdd));
	}

	private long getDefaultFullViewTemplateId(DDLRecord ddlRecord) {
		try {
			Optional<DynamicDataListMapping> dynamicDataListMapping = dynamicDataListConfigurationService.getDynamicDataListMapping(ddlRecord.getRecordSet().getDDMStructure());
			return dynamicDataListMapping.map(DynamicDataListMapping::getFullViewDefaultTemplateId).orElse(0L);
		} catch (PortalException e) {
			LOG.warn("Unable to retrieve default full view template id " + e.getMessage());
			LOG.debug(e);
			return 0L;
		}
	}

	private long getDocumentEntryClassPK(Document document) {
		return GetterUtil.getLong(document.get(Field.ENTRY_CLASS_PK));
	}

	private String getGroupURL(String portalURL, Group group, boolean privateLayout) {
		String pathFriendlyURL = privateLayout ? portal.getPathFriendlyURLPrivateGroup() : portal.getPathFriendlyURLPublic();

		return portalURL + pathFriendlyURL + group.getFriendlyURL();
	}
}
