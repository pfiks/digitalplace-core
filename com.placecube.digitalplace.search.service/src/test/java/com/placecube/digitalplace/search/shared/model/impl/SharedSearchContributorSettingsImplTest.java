package com.placecube.digitalplace.search.shared.model.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.GeoDistanceSort;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.facet.Facet;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.liferay.portal.kernel.search.filter.GeoDistanceFilter;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.search.constants.SearchContextAttributes;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;
import com.placecube.digitalplace.search.internal.util.SearchUtil;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ BooleanClauseFactoryUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SharedSearchContributorSettingsImplTest extends PowerMockito {

	private static final long COMPANY_ID = 2322l;

	private static final String FACET_FIELD_NAME = "facetFieldNameValue";

	@Mock
	private BooleanClause<Query> mockBooleanClause1;

	@Mock
	private BooleanClause<Query> mockBooleanClause2;

	@Mock
	private BooleanQuery mockMainQuery;

	@Mock
	private BooleanQuery mockSubQuery1;

	@Mock
	private BooleanQuery mockSubQuery2;

	@Mock
	private Facet mockFacet;

	@Mock
	private FacetConfiguration mockFacetConfiguration;

	@Mock
	private FacetConfiguration mockCurrentFacetConfiguration;

	@Mock
	private SearchContext mockSearchContext;

	@Mock
	private SearchUtil mockSearchUtil;

	@Mock
	private SharedActiveFilter mockSharedActiveFilter;

	@Mock
	private SharedActiveFilter mockSharedActiveFilter2;

	@Mock
	private SharedSearchSession mockSharedSearchSession;

	@Mock
	private Sort mockSort;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Set<String> mockValues;

	@Mock
	private GeoDistanceFilter mockGeoDistanceFilter;

	@Mock
	private GeoDistanceSort mockGeoDistanceSort;

	@Mock
	private JSONObject mockJSONObjectOne;

	@Mock
	private JSONObject mockJSONObjectTwo;

	private SharedSearchContributorSettings sharedSearchContributorSettings;

	@Before
	public void activateSetup() {
		mockStatic(BooleanClauseFactoryUtil.class);

		sharedSearchContributorSettings = SharedSearchContributorSettingsImpl.init(mockSearchUtil, mockThemeDisplay, mockSearchContext, mockSharedSearchSession);
	}

	@Test
	@Parameters({ ",", "null,", ",null", "value,", "value,null", "null,value", ",value" })
	public void addBooleanQuery_WhenFieldNameOrFieldValueAreInvalid_ThenNoChangesAreMade(String fieldName, String fieldValue) {
		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;

		sharedSearchContributorSettings.addBooleanQuery(fieldName, fieldValue, booleanClauseOccur);

		verifyZeroInteractions(mockSearchContext);
	}

	@Test
	public void addBooleanQuery_WhenNoErrorAndClausesAlreadyConfigured_ThenConfiguresTheSearchContextAddingTheNewlyCreatedClauseToTheExistingOnes() {
		String value = "fieldValue123";
		String fieldName = "fieldNameVal";
		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;
		when(BooleanClauseFactoryUtil.create(fieldName, String.valueOf(value), booleanClauseOccur.getName())).thenReturn(mockBooleanClause2);

		BooleanClause<Query>[] existingQueries = new BooleanClause[] { mockBooleanClause1 };
		when(mockSearchContext.getBooleanClauses()).thenReturn(existingQueries);

		sharedSearchContributorSettings.addBooleanQuery(fieldName, value, booleanClauseOccur);

		verify(mockSearchContext, times(1)).setBooleanClauses(ArrayUtil.append(existingQueries, mockBooleanClause2));
	}

	@Test
	public void addBooleanQuery_WhenNoErrorAndNoClausesYetConfigured_ThenConfiguresTheSearchContextWithTheNewlyCreatedClause() {
		String value = "fieldValue123";
		String fieldName = "fieldNameVal";
		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;
		when(BooleanClauseFactoryUtil.create(fieldName, String.valueOf(value), booleanClauseOccur.getName())).thenReturn(mockBooleanClause2);

		when(mockSearchContext.getBooleanClauses()).thenReturn(null);

		sharedSearchContributorSettings.addBooleanQuery(fieldName, value, booleanClauseOccur);

		verify(mockSearchContext, times(1)).setBooleanClauses(ArrayUtil.append(new BooleanClause[0], mockBooleanClause2));
	}

	@Test
	public void addBooleanQuery_WhenNoError_ThenConfiguresTheSearchContextWithTheNewlyCreatedClause() {
		sharedSearchContributorSettings.addBooleanQuery(mockBooleanClause1);
		verify(mockSearchContext, times(1)).setBooleanClauses(ArrayUtil.append(new BooleanClause[0], mockBooleanClause1));
	}

	@Test
	@Parameters({ ",", "null,", ",null", "value,", "value,null", "null,value", ",value" })
	public void addBooleanQueryForMultipleValues_WhenFieldNameOrFieldValuesAreInvalid_ThenNoChangesAreMade(String fieldName, String fieldValue) {
		List<String> fieldValues = fieldValue == null ? null : Collections.singletonList(fieldValue);
		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;

		sharedSearchContributorSettings.addBooleanQueryForMultipleValues(fieldName, fieldValues, booleanClauseOccur);

		verifyZeroInteractions(mockSearchContext);
	}

	@Test
	public void addBooleanQueryForMultipleValues_WhenFieldNameAndFieldValuesListAreValidButNoValuesAreValid_ThenNoChangesAreMade() {
		String fieldName = "";
		String fieldValue1 = null;
		String fieldValue2 = "";
		List<String> fieldValues = Arrays.asList(fieldValue1, fieldValue2);

		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;

		sharedSearchContributorSettings.addBooleanQueryForMultipleValues(fieldName, fieldValues, booleanClauseOccur);

		verifyZeroInteractions(mockSearchContext);
	}

	@Test
	public void addBooleanQueryForMultipleValues_WhenNoError_ThenConstructsAMainQueryWithValuesSubQueries() throws Exception {
		String fieldValue1 = "fieldValue123";
		String fieldValue2 = "fieldValue456";
		String fieldName = "fieldNameVal";
		List<String> fieldValues = Arrays.asList(fieldValue1, fieldValue2);
		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;

		AtomicInteger constructorCallsCount = new AtomicInteger();
		when(mockSearchUtil.createBooleanQuery()).thenAnswer((Answer<BooleanQuery>) invocation -> {
			int callIndex = constructorCallsCount.get();
			BooleanQuery resultingQuery;
			switch (callIndex) {
			case 0:
				resultingQuery = mockMainQuery;
				break;
			case 1:
				resultingQuery = mockSubQuery1;
				break;
			case 2:
				resultingQuery = mockSubQuery2;
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + callIndex);
			}
			constructorCallsCount.set(++callIndex);
			return resultingQuery;
		});

		sharedSearchContributorSettings.addBooleanQueryForMultipleValues(fieldName, fieldValues, booleanClauseOccur);

		verify(mockSubQuery1, times(1)).addExactTerm(fieldName, fieldValue1);
		verify(mockSubQuery2, times(1)).addExactTerm(fieldName, fieldValue2);
		verify(mockMainQuery, times(1)).add(mockSubQuery1, BooleanClauseOccur.SHOULD);
		verify(mockMainQuery, times(1)).add(mockSubQuery2, BooleanClauseOccur.SHOULD);
	}

	@Test
	public void addBooleanQueryForMultipleValues_WhenFieldNameAndFieldValuesAreValidButASingleValueIsInvalid_ThenSkipsThisValue() throws ParseException {
		String fieldValue1 = "fieldValue123";
		String fieldValue2 = "";
		String fieldName = "fieldNameVal";
		List<String> fieldValues = Arrays.asList(fieldValue1, fieldValue2);
		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;

		AtomicInteger constructorCallsCount = new AtomicInteger();
		when(mockSearchUtil.createBooleanQuery()).thenAnswer((Answer<BooleanQuery>) invocation -> {
			int callIndex = constructorCallsCount.get();
			BooleanQuery resultingQuery;
			switch (callIndex) {
			case 0:
				resultingQuery = mockMainQuery;
				break;
			case 1:
				resultingQuery = mockSubQuery1;
				break;
			case 2:
				resultingQuery = mockSubQuery2;
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + callIndex);
			}
			constructorCallsCount.set(++callIndex);
			return resultingQuery;
		});

		sharedSearchContributorSettings.addBooleanQueryForMultipleValues(fieldName, fieldValues, booleanClauseOccur);

		verify(mockSubQuery1, times(1)).addExactTerm(fieldName, fieldValue1);
		verify(mockSubQuery2, never()).addExactTerm(fieldName, fieldValue2);
		verify(mockMainQuery, times(1)).add(mockSubQuery1, BooleanClauseOccur.SHOULD);
		verify(mockMainQuery, never()).add(mockSubQuery2, BooleanClauseOccur.SHOULD);
	}

	@Test
	@Parameters({ "true", "false" })
	public void addBooleanQueryForMultipleValues_WhenFieldNameAndFieldValuesAreValidButErrorOnAddingSubQueries_ThenCreateClauseOnlyIfAtLeastOneSubQueryWasAdded(boolean areAllQueriesInvalid)
			throws ParseException {
		String fieldValue1 = "fieldValue123";
		String fieldValue2 = "fieldValue456";
		String fieldName = "fieldNameVal";
		List<String> fieldValues = Arrays.asList(fieldValue1, fieldValue2);
		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;

		AtomicInteger constructorCallsCount = new AtomicInteger();
		when(mockSearchUtil.createBooleanQuery()).thenAnswer((Answer<BooleanQuery>) invocation -> {
			int callIndex = constructorCallsCount.get();
			BooleanQuery resultingQuery;
			switch (callIndex) {
			case 0:
				resultingQuery = mockMainQuery;
				break;
			case 1:
				resultingQuery = mockSubQuery1;
				break;
			case 2:
				resultingQuery = mockSubQuery2;
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + callIndex);
			}
			constructorCallsCount.set(++callIndex);
			return resultingQuery;
		});
		when(mockMainQuery.add(mockSubQuery1, BooleanClauseOccur.SHOULD)).thenThrow(new ParseException());
		if (areAllQueriesInvalid) {
			when(mockMainQuery.add(mockSubQuery2, BooleanClauseOccur.SHOULD)).thenThrow(new ParseException());
		}

		sharedSearchContributorSettings.addBooleanQueryForMultipleValues(fieldName, fieldValues, booleanClauseOccur);

		if (areAllQueriesInvalid) {
			verifyZeroInteractions(BooleanClauseFactoryUtil.class);
		} else {
			verifyStatic(BooleanClauseFactoryUtil.class, times(1));
			BooleanClauseFactoryUtil.create(mockMainQuery, booleanClauseOccur.getName());
		}
	}

	@Test
	public void addBooleanQueryForMultipleValues_WhenNoErrorAndClausesAlreadyConfigured_ThenConfiguresTheSearchContextAddingTheNewlyCreatedClauseToTheExistingOnes() {
		String fieldValue = "fieldValue123";
		String fieldName = "fieldNameVal";
		List<String> fieldValues = Collections.singletonList(fieldValue);
		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;

		AtomicInteger constructorCallsCount = new AtomicInteger();
		when(mockSearchUtil.createBooleanQuery()).thenAnswer((Answer<BooleanQuery>) invocation -> {
			if (constructorCallsCount.getAndIncrement() == 0) {
				return mockMainQuery;
			} else {
				return mockSubQuery1;
			}
		});
		when(BooleanClauseFactoryUtil.create(mockMainQuery, booleanClauseOccur.getName())).thenReturn(mockBooleanClause2);

		BooleanClause<Query>[] existingQueries = new BooleanClause[] { mockBooleanClause1 };
		when(mockSearchContext.getBooleanClauses()).thenReturn(existingQueries);

		sharedSearchContributorSettings.addBooleanQueryForMultipleValues(fieldName, fieldValues, booleanClauseOccur);

		verify(mockSearchContext, times(1)).setBooleanClauses(ArrayUtil.append(existingQueries, mockBooleanClause2));
	}

	@Test
	public void addBooleanQueryForMultipleValues_WhenNoErrorAndNoClausesYetConfigured_ThenConfiguresTheSearchContextWithTheNewlyCreatedClause() {
		String fieldValue = "fieldValue123";
		String fieldName = "fieldNameVal";
		List<String> fieldValues = Collections.singletonList(fieldValue);
		BooleanClauseOccur booleanClauseOccur = BooleanClauseOccur.MUST;

		AtomicInteger constructorCallsCount = new AtomicInteger();
		when(mockSearchUtil.createBooleanQuery()).thenAnswer((Answer<BooleanQuery>) invocation -> {
			if (constructorCallsCount.getAndIncrement() == 0) {
				return mockMainQuery;
			} else {
				return mockSubQuery1;
			}
		});
		when(BooleanClauseFactoryUtil.create(mockMainQuery, booleanClauseOccur.getName())).thenReturn(mockBooleanClause2);

		when(mockSearchContext.getBooleanClauses()).thenReturn(null);

		sharedSearchContributorSettings.addBooleanQueryForMultipleValues(fieldName, fieldValues, booleanClauseOccur);

		verify(mockSearchContext, times(1)).setBooleanClauses(ArrayUtil.append(new BooleanClause[0], mockBooleanClause2));
	}

	@Test
	public void addFacet_WhenFacetDoesNotExistAndConfigurationNotSpecified_ThenAddsTheFacetToTheSearchContext() {
		when(mockSearchContext.getFacet(FACET_FIELD_NAME)).thenReturn(null);
		when(mockSearchUtil.createFacet(FACET_FIELD_NAME, mockSearchContext)).thenReturn(mockFacet);

		sharedSearchContributorSettings.addFacet(FACET_FIELD_NAME, Optional.empty());

		verify(mockSearchContext, times(1)).addFacet(mockFacet);
	}

	@Test
	public void addFacet_WhenFacetDoesNotExistAndConfigurationSpecified_ThenAddsTheFacetWithItsConfigurationToTheSearchContext() {
		when(mockSearchContext.getFacet(FACET_FIELD_NAME)).thenReturn(null);
		when(mockSearchUtil.createFacet(FACET_FIELD_NAME, mockSearchContext)).thenReturn(mockFacet);

		sharedSearchContributorSettings.addFacet(FACET_FIELD_NAME, Optional.of(mockFacetConfiguration));

		InOrder inOrder = inOrder(mockFacet, mockSearchContext);
		inOrder.verify(mockFacet, times(1)).setFacetConfiguration(mockFacetConfiguration);
		inOrder.verify(mockSearchContext, times(1)).addFacet(mockFacet);
	}

	@Test
	public void addFacet_WhenFacetsExistsAndCurrentConfigurationExistsAndNewConfigurationIsSpecified_ThenAddsMaxTermsToExistingMaxTermsWithFacetMultiplier() {
		final int currentMaxTerms = 10;
		final int newMaxTerms = 8;
		final int facetMultiplier = 2;

		when(mockSearchContext.getFacet(FACET_FIELD_NAME)).thenReturn(mockFacet);
		when(mockFacet.getFacetConfiguration()).thenReturn(mockCurrentFacetConfiguration);
		when(mockCurrentFacetConfiguration.getData()).thenReturn(mockJSONObjectOne);
		when(mockJSONObjectOne.getInt("maxTerms")).thenReturn(currentMaxTerms);
		when(mockFacetConfiguration.getData()).thenReturn(mockJSONObjectTwo);
		when(mockJSONObjectTwo.getInt("maxTerms")).thenReturn(newMaxTerms);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSearchUtil.getFacetMultiplier(COMPANY_ID)).thenReturn(facetMultiplier);

		sharedSearchContributorSettings.addFacet(FACET_FIELD_NAME, Optional.of(mockFacetConfiguration));

		verify(mockJSONObjectOne, times(1)).put("maxTerms", (currentMaxTerms + newMaxTerms) * facetMultiplier);
	}

	@Test
	public void addFacet_WhenFacetsExistsAndCurrentConfigurationDoesNotExistAndNewConfigurationIsSpecified_ThenSetsNewConfigurationInFacet() {
		when(mockSearchContext.getFacet(FACET_FIELD_NAME)).thenReturn(mockFacet);
		when(mockFacet.getFacetConfiguration()).thenReturn(null);

		sharedSearchContributorSettings.addFacet(FACET_FIELD_NAME, Optional.of(mockFacetConfiguration));

		verify(mockFacet, times(1)).setFacetConfiguration(mockFacetConfiguration);
	}

	@Test
	public void addFacet_WhenFacetsExistsAndAndNewConfigurationIsNotSpecified_ThenDoesNotUpdateFacetConfiguration() {
		when(mockSearchContext.getFacet(FACET_FIELD_NAME)).thenReturn(mockFacet);
		when(mockFacet.getFacetConfiguration()).thenReturn(mockCurrentFacetConfiguration);
		when(mockCurrentFacetConfiguration.getData()).thenReturn(mockJSONObjectOne);

		sharedSearchContributorSettings.addFacet(FACET_FIELD_NAME, Optional.empty());

		verify(mockFacet, never()).setFacetConfiguration(any(FacetConfiguration.class));
		verify(mockJSONObjectOne, never()).put(anyString(), anyInt());
	}

	@Test
	public void addFacet_WhenFacetsExists_ThenDoesNotCreateFacet() {
		when(mockSearchContext.getFacet(FACET_FIELD_NAME)).thenReturn(mockFacet);

		sharedSearchContributorSettings.addFacet(FACET_FIELD_NAME, Optional.empty());

		verify(mockSearchUtil, never()).createFacet(anyString(), any(SearchContext.class));
	}

	@Test
	@Parameters({ "true", "false" })
	public void addSearchAttribute_WhenNoError_ThenAddsAnewSearchAttributeInTheSearchContext(boolean attributeValue) {
		String paramName = "paramNameValue";

		sharedSearchContributorSettings.addSearchAttribute(paramName, attributeValue);

		verify(mockSearchContext, times(1)).setAttribute(paramName, attributeValue);
	}

	@Test
	public void createFacetConfiguration_WhenNoError_ThenReturnsAnewFacetConfigurationForTheSpecifiedField() {
		FacetConfiguration result = sharedSearchContributorSettings.createFacetConfiguration(FACET_FIELD_NAME);

		assertThat(result.getFieldName(), equalTo(FACET_FIELD_NAME));
	}

	@Test
	public void getActiveFiltersForField_WhenMutipleFiltersExist_ThenReturnActiveFiltersForFieldName() {
		String fieldName = "attributeNameVal";

		List<SharedActiveFilter> mockFilterList = new ArrayList<>();
		mockFilterList.add(mockSharedActiveFilter);
		mockFilterList.add(mockSharedActiveFilter2);

		when(mockSharedSearchSession.getActiveFilters()).thenReturn(mockFilterList);
		when(mockSharedActiveFilter.getName()).thenReturn(fieldName);
		when(mockSharedActiveFilter2.getName()).thenReturn("different");

		List<SharedActiveFilter> results = sharedSearchContributorSettings.getActiveFiltersForField(fieldName);

		assertThat(results.size(), equalTo(1));
		assertThat(results.get(0), equalTo(mockSharedActiveFilter));
	}

	@Test
	public void getActiveFiltersForField_WhenNoFiltersExist_ThenReturnEmptyList() {
		String fieldName = "attributeNameVal";
		List<SharedActiveFilter> results = sharedSearchContributorSettings.getActiveFiltersForField(fieldName);
		assertThat(results.size(), equalTo(0));
	}

	@Test
	public void getAssetCategoryIds_WhenNoError_ThenReturnsCategoryIdsInTheSearchContext() {
		long[] values = new long[] { 1l, 2l, 3l };

		when(mockSearchContext.getAssetCategoryIds()).thenReturn(values);

		long[] results = sharedSearchContributorSettings.getAssetCategoryIds();

		assertThat(results, equalTo(values));
	}

	@Test
	public void getSharedSearchSessionSingleValuedAttribute_WhenNoError_ThenReturnTheAttributeValueFromTheSharedSearchSession() {
		String attributeName = "attributeNameVal";
		String attributeValue = "attributeValue";
		when(mockSharedSearchSession.getSingleValuedAttribute(attributeName)).thenReturn(attributeValue);

		Optional<String> result = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(attributeName);

		assertThat(result.get(), equalTo(attributeValue));
	}

	@Test
	public void getSharedSearchSessionSingleValuedAttribute_WhenNoErrorAndSuppliedValueIsBlank_ThenReturnEmptyOptional() {
		String attributeName = "attributeNameVal";
		String attributeValue = "";
		when(mockSharedSearchSession.getSingleValuedAttribute(attributeName)).thenReturn(attributeValue);

		Optional<String> result = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(attributeName);

		assertFalse(result.isPresent());
	}

	@Test
	public void getThemeDisplay_WhenNoError_ThenReturnsTheThemeDisplay() {
		ThemeDisplay result = sharedSearchContributorSettings.getThemeDisplay();

		assertThat(result, sameInstance(mockThemeDisplay));
	}

	@Test
	@Parameters({ "true", "false" })
	public void setAllowEmptySearch_WhenNoError_ThenSetsTheAttributeKeyEmptySearchAsSearchContextAttribute(boolean allowEmptySearch) {
		sharedSearchContributorSettings.setAllowEmptySearch(allowEmptySearch);

		verify(mockSearchContext, times(1)).setAttribute(SearchContextAttributes.ATTRIBUTE_KEY_EMPTY_SEARCH, allowEmptySearch);
	}

	@Test
	public void setAssetTagNames_WhenNoError_ThenConfiguresTheTagNamesInTheSearchContext() {
		sharedSearchContributorSettings.setAssetTagNames(mockValues);

		verify(mockSearchContext, times(1)).setAssetTagNames(mockValues.toArray(new String[mockValues.size()]));
	}

	@Test
	public void setEntryClassNames_WhenNoError_ThenConfiguresTheEntryClassNamesInTheSearchContext() {
		String[] values = new String[4];

		sharedSearchContributorSettings.setEntryClassNames(values);

		verify(mockSearchContext, times(1)).setEntryClassNames(values);
	}

	@Test
	public void setSearchGroupIds_WhenNoError_ThenConfiguresTheGroupIdsInTheSearchContext() {
		long[] values = new long[4];

		sharedSearchContributorSettings.setSearchGroupIds(values);

		verify(mockSearchContext, times(1)).setGroupIds(values);
	}

	@Test
	public void setSearchKeywords_WhenNoError_ThenConfiguresTheSearchKeywordInTheSearchContext() {
		String value = "val";

		sharedSearchContributorSettings.setSearchKeywords(value);

		verify(mockSearchContext, times(1)).setKeywords(value);
	}

	@Test
	public void setSortBy_WhenSortByToAddAreNull_ThenConfiguresTheSortsInTheSearchContext() {
		Sort[] values = new Sort[] { mockSort };

		sharedSearchContributorSettings.setSortBy(values);

		verify(mockSearchContext, times(1)).setSorts(values);
	}

	@Test
	public void setSortBy_WhenSortByToAddAreNull_ThenNoChangesAreMadeToTheSearchContext() {
		sharedSearchContributorSettings.setSortBy(null);

		verifyZeroInteractions(mockSearchContext);
	}
}
