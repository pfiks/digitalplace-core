package com.placecube.digitalplace.externalpage.web.portlet;

import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SessionErrors.class, PropsUtil.class, ParamUtil.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.util.ParamUtil")
public class ExternalPageDeleteMVCActionCommandTest extends PowerMockito {

	@InjectMocks
	private ExternalPageDeleteMVCActionCommand externalPageDeleteMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	@Before
	public void activeSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, SessionErrors.class);
	}

	@Test
	public void doProcessAction_WhenExternalPageIdIsGreaterThanZero_ThenDeleteExternalPageByExternalPageId() throws PortalException {
		long externalPageId = 123;
		String[] deleteExternalPageIds = new String[] { String.valueOf(externalPageId) };
		when(ParamUtil.getLong(mockActionRequest, "externalPageId")).thenReturn(externalPageId);
		when(ParamUtil.getStringValues(mockActionRequest, "rowIds", new String[] { String.valueOf(externalPageId) })).thenReturn(deleteExternalPageIds);

		externalPageDeleteMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockExternalPageLocalService, times(1)).deleteExternalPage(externalPageId);

	}

	@Test
	public void doProcessAction_WhenExternalPageIdIsLessThanZero_ThenDeleteExternalPageByRowId() throws PortalException {
		long externalPageId = 0;
		String[] deleteExternalPageIds = { "123", "456" };
		when(ParamUtil.getLong(mockActionRequest, "externalPageId")).thenReturn(externalPageId);
		when(ParamUtil.getStringValues(mockActionRequest, "rowIds", new String[] {})).thenReturn(deleteExternalPageIds);

		externalPageDeleteMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockExternalPageLocalService, times(1)).deleteExternalPage(123);
		verify(mockExternalPageLocalService, times(1)).deleteExternalPage(456);

	}

	@Test(expected = PortalException.class)
	public void doProcessAction_WhenExternalPageIdsIsEmpty_ThenPortalExceptionIsThrown() throws Exception {
		long externalPageId = 0;
		String[] deleteExternalPageIds = new String[] {};
		when(ParamUtil.getLong(mockActionRequest, "externalPageId")).thenReturn(externalPageId);
		when(ParamUtil.getStringValues(mockActionRequest, "rowIds", new String[] {})).thenReturn(deleteExternalPageIds);

		externalPageDeleteMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

	}
}