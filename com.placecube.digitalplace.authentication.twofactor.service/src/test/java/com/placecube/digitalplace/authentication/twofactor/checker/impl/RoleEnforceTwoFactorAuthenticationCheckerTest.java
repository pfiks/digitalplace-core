package com.placecube.digitalplace.authentication.twofactor.checker.impl;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupGroupRole;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.UserGroupGroupRoleLocalService;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.internal.TwoFactorUtils;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

public class RoleEnforceTwoFactorAuthenticationCheckerTest extends PowerMockito {

	private static final long COMPANY_ID = 433L;

	@Mock
	private AuthenticationTwoFactorCompanyConfiguration mockAuthenticationTwoFactorCompanyConfiguration;

	@Mock
	private Role mockRole1;

	@Mock
	private Role mockRole2;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private TwoFactorUtils mockTwoFactorUtils;

	@Mock
	private User mockUser;

	@Mock
	private UserGroupGroupRole mockUserGroupGroupRole1;

	@Mock
	private UserGroupGroupRole mockUserGroupGroupRole2;

	@Mock
	private UserGroupGroupRoleLocalService mockUserGroupGroupRoleLocalService;

	@Mock
	private UserGroupRole mockUserGroupRole1;

	@Mock
	private UserGroupRole mockUserGroupRole2;

	@Mock
	private UserGroupRoleLocalService mockUserGroupRoleLocalService;

	@InjectMocks
	private RoleEnforceTwoFactorAuthenticationChecker roleEnforceTwoFactorAuthenticationChecker;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void checkIfTwoFactorAuthenticationShouldBeEnforced_WhenNoErrorAndUserHasARoleForWhich2FAIsEnforced_ThenReturnsTrue() throws PortalException {
		List<Role> standardRoles = Arrays.asList(mockRole1, mockRole2);
		List<UserGroupRole> userGroupRoles = Arrays.asList(mockUserGroupRole1, mockUserGroupRole2);
		List<UserGroupGroupRole> userGroupGroupRoles = Arrays.asList(mockUserGroupGroupRole1, mockUserGroupGroupRole2);
		long userId = 1;
		long roleId1 = 21;
		long roleId2 = 22;
		long roleId3 = 23;
		long roleId4 = 24;
		long roleId5 = 25;
		long roleId6 = 26;
		String[] enforcedRoleIds = { "12", "25", "111" };

		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getUserRoles(userId)).thenReturn(standardRoles);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(userId)).thenReturn(userGroupRoles);
		when(mockUserGroupGroupRoleLocalService.getUserGroupGroupRolesByUser(userId)).thenReturn(userGroupGroupRoles);
		when(mockUserGroupRole1.getRoleId()).thenReturn(roleId3);
		when(mockUserGroupRole2.getRoleId()).thenReturn(roleId4);
		when(mockUserGroupGroupRole1.getRoleId()).thenReturn(roleId5);
		when(mockUserGroupGroupRole2.getRoleId()).thenReturn(roleId6);
		when(mockRole1.getRoleId()).thenReturn(roleId1);
		when(mockRole2.getRoleId()).thenReturn(roleId2);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockAuthenticationTwoFactorCompanyConfiguration.twoFactorAuthenticationEnforcedRoleIds()).thenReturn(enforcedRoleIds);

		boolean result = roleEnforceTwoFactorAuthenticationChecker.checkIfTwoFactorAuthenticationShouldBeEnforced(mockUser);

		assertTrue(result);
	}

	@Test
	public void checkIfTwoFactorAuthenticationShouldBeEnforced_WhenNoErrorAndUserDoesNotHaveARoleForWhich2FAIsEnforced_ThenReturnsFalse() throws PortalException {
		List<Role> standardRoles = Arrays.asList(mockRole1, mockRole2);
		List<UserGroupRole> userGroupRoles = Arrays.asList(mockUserGroupRole1, mockUserGroupRole2);
		List<UserGroupGroupRole> userGroupGroupRoles = Arrays.asList(mockUserGroupGroupRole1, mockUserGroupGroupRole2);
		long userId = 1;
		long roleId1 = 21;
		long roleId2 = 22;
		long roleId3 = 23;
		long roleId4 = 24;
		long roleId5 = 25;
		long roleId6 = 26;
		String[] enforcedRoleIds = { "12" };

		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getUserRoles(userId)).thenReturn(standardRoles);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(userId)).thenReturn(userGroupRoles);
		when(mockUserGroupGroupRoleLocalService.getUserGroupGroupRolesByUser(userId)).thenReturn(userGroupGroupRoles);
		when(mockUserGroupRole1.getRoleId()).thenReturn(roleId3);
		when(mockUserGroupRole2.getRoleId()).thenReturn(roleId4);
		when(mockUserGroupGroupRole1.getRoleId()).thenReturn(roleId5);
		when(mockUserGroupGroupRole2.getRoleId()).thenReturn(roleId6);
		when(mockRole1.getRoleId()).thenReturn(roleId1);
		when(mockRole2.getRoleId()).thenReturn(roleId2);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockAuthenticationTwoFactorCompanyConfiguration.twoFactorAuthenticationEnforcedRoleIds()).thenReturn(enforcedRoleIds);

		boolean result = roleEnforceTwoFactorAuthenticationChecker.checkIfTwoFactorAuthenticationShouldBeEnforced(mockUser);

		assertFalse(result);
	}

	@Test
	public void checkIfTwoFactorAuthenticationShouldBeEnforced_WhenNoErrorAndConfigEnforces2FAForAllUsers_ThenReturnsTrue() throws PortalException {
		List<Role> standardRoles = Arrays.asList(mockRole1, mockRole2);
		List<UserGroupRole> userGroupRoles = Arrays.asList(mockUserGroupRole1, mockUserGroupRole2);
		List<UserGroupGroupRole> userGroupGroupRoles = Arrays.asList(mockUserGroupGroupRole1, mockUserGroupGroupRole2);
		long userId = 1;
		long roleId1 = 21;
		long roleId2 = 22;
		long roleId3 = 23;
		long roleId4 = 24;
		long roleId5 = 25;
		long roleId6 = 26;
		String[] enforcedRoleIds = { "-1" };

		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getUserRoles(userId)).thenReturn(standardRoles);
		when(mockUserGroupRoleLocalService.getUserGroupRoles(userId)).thenReturn(userGroupRoles);
		when(mockUserGroupGroupRoleLocalService.getUserGroupGroupRolesByUser(userId)).thenReturn(userGroupGroupRoles);
		when(mockUserGroupRole1.getRoleId()).thenReturn(roleId3);
		when(mockUserGroupRole2.getRoleId()).thenReturn(roleId4);
		when(mockUserGroupGroupRole1.getRoleId()).thenReturn(roleId5);
		when(mockUserGroupGroupRole2.getRoleId()).thenReturn(roleId6);
		when(mockRole1.getRoleId()).thenReturn(roleId1);
		when(mockRole2.getRoleId()).thenReturn(roleId2);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockAuthenticationTwoFactorCompanyConfiguration.twoFactorAuthenticationEnforcedRoleIds()).thenReturn(enforcedRoleIds);

		boolean result = roleEnforceTwoFactorAuthenticationChecker.checkIfTwoFactorAuthenticationShouldBeEnforced(mockUser);

		assertTrue(result);
	}

	@Test
	public void checkIfTwoFactorAuthenticationShouldBeEnforced_WhenExceptionGettingCompanyConfiguration_ThenReturnsFalse() throws PortalException {
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenThrow(PortalException.class);

		boolean result = roleEnforceTwoFactorAuthenticationChecker.checkIfTwoFactorAuthenticationShouldBeEnforced(mockUser);

		assertFalse(result);
	}

	@Test
	public void checkIfTwoFactorAuthenticationShouldBeEnforced_WhenNoErrorAndEnforcedRoleIdsSettingIsEmpty_ThenReturnsFalse() throws PortalException {
		String[] enforcedRoleIds = { "" };

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockAuthenticationTwoFactorCompanyConfiguration.twoFactorAuthenticationEnforcedRoleIds()).thenReturn(enforcedRoleIds);

		boolean result = roleEnforceTwoFactorAuthenticationChecker.checkIfTwoFactorAuthenticationShouldBeEnforced(mockUser);

		assertThat(result, equalTo(false));
	}
}
