package com.placecube.digitalplace.externalpage.web.constants;

public class ValidationKeys {

	public static final String INVALID_XPATH = "invalid-xpath";
	public static final String REQUIRED_URL = "required-url";

	private ValidationKeys() {

	}
}
