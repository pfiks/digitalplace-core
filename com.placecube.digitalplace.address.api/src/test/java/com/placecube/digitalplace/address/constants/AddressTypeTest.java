package com.placecube.digitalplace.address.constants;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.junit.Test;

public class AddressTypeTest {

	@Test
	public void getAddressTypeByName_WhenAddressTypeExists_ThenReturnsOptionalOfAddressType() {
		Optional<AddressType> result = AddressType.getAddressTypeByName("personal");
		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), equalTo(AddressType.PERSONAL));
	}

	@Test
	public void getAddressTypeByName_WhenAddressTypeDoesNotExist_ThenReturnsEmtpyOptional() {
		assertThat(AddressType.getAddressTypeByName("unknown").isEmpty(), equalTo(true));

	}

}
