package com.placecube.digitalplace.dataconsumer.service.update.steps;

import java.io.IOException;
import java.sql.SQLException;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class Update_2_2_0_DataConsumerTableUpdate extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {
		if (hasTable("Placecube_DataConsumer_DataConsumer")) {

			createNewIndex();

			dropLegacyIndexImmediately();
		}
	}

	private void createNewIndex() throws Exception, IOException, SQLException {
		if (!hasIndex("Placecube_DataConsumer_DataConsumer", "IX_A8F131B2")) {
			runSQL("create index IX_A8F131B2 on Placecube_DataConsumer_DataConsumer (uuid_[$COLUMN_LENGTH:75$]);");
		}
	}

	private void dropLegacyIndexImmediately() throws Exception {
		if (hasIndex("Placecube_DataConsumer_DataConsumer", "IX_3E152F36")) {
			runSQL(connection, "drop index IX_3E152F36 on Placecube_DataConsumer_DataConsumer;");
		}

		if (hasIndex("Placecube_DataConsumer_DataConsumer", "IX_626C0B38")) {
			runSQL(connection, "drop index IX_626C0B38 on Placecube_DataConsumer_DataConsumer;");
		}
	}
}
