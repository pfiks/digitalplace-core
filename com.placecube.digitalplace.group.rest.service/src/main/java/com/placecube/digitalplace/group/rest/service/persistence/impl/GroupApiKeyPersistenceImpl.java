/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;

import com.placecube.digitalplace.group.rest.exception.NoSuchGroupApiKeyException;
import com.placecube.digitalplace.group.rest.model.GroupApiKey;
import com.placecube.digitalplace.group.rest.model.GroupApiKeyTable;
import com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyImpl;
import com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyModelImpl;
import com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK;
import com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPersistence;
import com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyUtil;
import com.placecube.digitalplace.group.rest.service.persistence.impl.constants.Placecube_GroupRestPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the group api key service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = GroupApiKeyPersistence.class)
public class GroupApiKeyPersistenceImpl
	extends BasePersistenceImpl<GroupApiKey> implements GroupApiKeyPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>GroupApiKeyUtil</code> to access the group api key persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		GroupApiKeyImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByApplicationKey;
	private FinderPath _finderPathWithoutPaginationFindByApplicationKey;
	private FinderPath _finderPathCountByApplicationKey;

	/**
	 * Returns all the group api keys where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @return the matching group api keys
	 */
	@Override
	public List<GroupApiKey> findByApplicationKey(String applicationKey) {
		return findByApplicationKey(
			applicationKey, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the group api keys where applicationKey = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationKey the application key
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of matching group api keys
	 */
	@Override
	public List<GroupApiKey> findByApplicationKey(
		String applicationKey, int start, int end) {

		return findByApplicationKey(applicationKey, start, end, null);
	}

	/**
	 * Returns an ordered range of all the group api keys where applicationKey = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationKey the application key
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group api keys
	 */
	@Override
	public List<GroupApiKey> findByApplicationKey(
		String applicationKey, int start, int end,
		OrderByComparator<GroupApiKey> orderByComparator) {

		return findByApplicationKey(
			applicationKey, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the group api keys where applicationKey = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationKey the application key
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group api keys
	 */
	@Override
	public List<GroupApiKey> findByApplicationKey(
		String applicationKey, int start, int end,
		OrderByComparator<GroupApiKey> orderByComparator,
		boolean useFinderCache) {

		applicationKey = Objects.toString(applicationKey, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByApplicationKey;
				finderArgs = new Object[] {applicationKey};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByApplicationKey;
			finderArgs = new Object[] {
				applicationKey, start, end, orderByComparator
			};
		}

		List<GroupApiKey> list = null;

		if (useFinderCache) {
			list = (List<GroupApiKey>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (GroupApiKey groupApiKey : list) {
					if (!applicationKey.equals(
							groupApiKey.getApplicationKey())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_GROUPAPIKEY_WHERE);

			boolean bindApplicationKey = false;

			if (applicationKey.isEmpty()) {
				sb.append(_FINDER_COLUMN_APPLICATIONKEY_APPLICATIONKEY_3);
			}
			else {
				bindApplicationKey = true;

				sb.append(_FINDER_COLUMN_APPLICATIONKEY_APPLICATIONKEY_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(GroupApiKeyModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApplicationKey) {
					queryPos.add(applicationKey);
				}

				list = (List<GroupApiKey>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	@Override
	public GroupApiKey findByApplicationKey_First(
			String applicationKey,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws NoSuchGroupApiKeyException {

		GroupApiKey groupApiKey = fetchByApplicationKey_First(
			applicationKey, orderByComparator);

		if (groupApiKey != null) {
			return groupApiKey;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("applicationKey=");
		sb.append(applicationKey);

		sb.append("}");

		throw new NoSuchGroupApiKeyException(sb.toString());
	}

	/**
	 * Returns the first group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	@Override
	public GroupApiKey fetchByApplicationKey_First(
		String applicationKey,
		OrderByComparator<GroupApiKey> orderByComparator) {

		List<GroupApiKey> list = findByApplicationKey(
			applicationKey, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	@Override
	public GroupApiKey findByApplicationKey_Last(
			String applicationKey,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws NoSuchGroupApiKeyException {

		GroupApiKey groupApiKey = fetchByApplicationKey_Last(
			applicationKey, orderByComparator);

		if (groupApiKey != null) {
			return groupApiKey;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("applicationKey=");
		sb.append(applicationKey);

		sb.append("}");

		throw new NoSuchGroupApiKeyException(sb.toString());
	}

	/**
	 * Returns the last group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	@Override
	public GroupApiKey fetchByApplicationKey_Last(
		String applicationKey,
		OrderByComparator<GroupApiKey> orderByComparator) {

		int count = countByApplicationKey(applicationKey);

		if (count == 0) {
			return null;
		}

		List<GroupApiKey> list = findByApplicationKey(
			applicationKey, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the group api keys before and after the current group api key in the ordered set where applicationKey = &#63;.
	 *
	 * @param groupApiKeyPK the primary key of the current group api key
	 * @param applicationKey the application key
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	@Override
	public GroupApiKey[] findByApplicationKey_PrevAndNext(
			GroupApiKeyPK groupApiKeyPK, String applicationKey,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws NoSuchGroupApiKeyException {

		applicationKey = Objects.toString(applicationKey, "");

		GroupApiKey groupApiKey = findByPrimaryKey(groupApiKeyPK);

		Session session = null;

		try {
			session = openSession();

			GroupApiKey[] array = new GroupApiKeyImpl[3];

			array[0] = getByApplicationKey_PrevAndNext(
				session, groupApiKey, applicationKey, orderByComparator, true);

			array[1] = groupApiKey;

			array[2] = getByApplicationKey_PrevAndNext(
				session, groupApiKey, applicationKey, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected GroupApiKey getByApplicationKey_PrevAndNext(
		Session session, GroupApiKey groupApiKey, String applicationKey,
		OrderByComparator<GroupApiKey> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_GROUPAPIKEY_WHERE);

		boolean bindApplicationKey = false;

		if (applicationKey.isEmpty()) {
			sb.append(_FINDER_COLUMN_APPLICATIONKEY_APPLICATIONKEY_3);
		}
		else {
			bindApplicationKey = true;

			sb.append(_FINDER_COLUMN_APPLICATIONKEY_APPLICATIONKEY_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(GroupApiKeyModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindApplicationKey) {
			queryPos.add(applicationKey);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(groupApiKey)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<GroupApiKey> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the group api keys where applicationKey = &#63; from the database.
	 *
	 * @param applicationKey the application key
	 */
	@Override
	public void removeByApplicationKey(String applicationKey) {
		for (GroupApiKey groupApiKey :
				findByApplicationKey(
					applicationKey, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(groupApiKey);
		}
	}

	/**
	 * Returns the number of group api keys where applicationKey = &#63;.
	 *
	 * @param applicationKey the application key
	 * @return the number of matching group api keys
	 */
	@Override
	public int countByApplicationKey(String applicationKey) {
		applicationKey = Objects.toString(applicationKey, "");

		FinderPath finderPath = _finderPathCountByApplicationKey;

		Object[] finderArgs = new Object[] {applicationKey};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_GROUPAPIKEY_WHERE);

			boolean bindApplicationKey = false;

			if (applicationKey.isEmpty()) {
				sb.append(_FINDER_COLUMN_APPLICATIONKEY_APPLICATIONKEY_3);
			}
			else {
				bindApplicationKey = true;

				sb.append(_FINDER_COLUMN_APPLICATIONKEY_APPLICATIONKEY_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApplicationKey) {
					queryPos.add(applicationKey);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPLICATIONKEY_APPLICATIONKEY_2 =
		"CAST_CLOB_TEXT(groupApiKey.applicationKey) = ?";

	private static final String _FINDER_COLUMN_APPLICATIONKEY_APPLICATIONKEY_3 =
		"(groupApiKey.applicationKey IS NULL OR CAST_CLOB_TEXT(groupApiKey.applicationKey) = '')";

	private FinderPath _finderPathWithPaginationFindByApplicationSecret;
	private FinderPath _finderPathWithoutPaginationFindByApplicationSecret;
	private FinderPath _finderPathCountByApplicationSecret;

	/**
	 * Returns all the group api keys where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @return the matching group api keys
	 */
	@Override
	public List<GroupApiKey> findByApplicationSecret(String applicationSecret) {
		return findByApplicationSecret(
			applicationSecret, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the group api keys where applicationSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationSecret the application secret
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of matching group api keys
	 */
	@Override
	public List<GroupApiKey> findByApplicationSecret(
		String applicationSecret, int start, int end) {

		return findByApplicationSecret(applicationSecret, start, end, null);
	}

	/**
	 * Returns an ordered range of all the group api keys where applicationSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationSecret the application secret
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group api keys
	 */
	@Override
	public List<GroupApiKey> findByApplicationSecret(
		String applicationSecret, int start, int end,
		OrderByComparator<GroupApiKey> orderByComparator) {

		return findByApplicationSecret(
			applicationSecret, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the group api keys where applicationSecret = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param applicationSecret the application secret
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group api keys
	 */
	@Override
	public List<GroupApiKey> findByApplicationSecret(
		String applicationSecret, int start, int end,
		OrderByComparator<GroupApiKey> orderByComparator,
		boolean useFinderCache) {

		applicationSecret = Objects.toString(applicationSecret, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByApplicationSecret;
				finderArgs = new Object[] {applicationSecret};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByApplicationSecret;
			finderArgs = new Object[] {
				applicationSecret, start, end, orderByComparator
			};
		}

		List<GroupApiKey> list = null;

		if (useFinderCache) {
			list = (List<GroupApiKey>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (GroupApiKey groupApiKey : list) {
					if (!applicationSecret.equals(
							groupApiKey.getApplicationSecret())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_GROUPAPIKEY_WHERE);

			boolean bindApplicationSecret = false;

			if (applicationSecret.isEmpty()) {
				sb.append(_FINDER_COLUMN_APPLICATIONSECRET_APPLICATIONSECRET_3);
			}
			else {
				bindApplicationSecret = true;

				sb.append(_FINDER_COLUMN_APPLICATIONSECRET_APPLICATIONSECRET_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(GroupApiKeyModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApplicationSecret) {
					queryPos.add(applicationSecret);
				}

				list = (List<GroupApiKey>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	@Override
	public GroupApiKey findByApplicationSecret_First(
			String applicationSecret,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws NoSuchGroupApiKeyException {

		GroupApiKey groupApiKey = fetchByApplicationSecret_First(
			applicationSecret, orderByComparator);

		if (groupApiKey != null) {
			return groupApiKey;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("applicationSecret=");
		sb.append(applicationSecret);

		sb.append("}");

		throw new NoSuchGroupApiKeyException(sb.toString());
	}

	/**
	 * Returns the first group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	@Override
	public GroupApiKey fetchByApplicationSecret_First(
		String applicationSecret,
		OrderByComparator<GroupApiKey> orderByComparator) {

		List<GroupApiKey> list = findByApplicationSecret(
			applicationSecret, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key
	 * @throws NoSuchGroupApiKeyException if a matching group api key could not be found
	 */
	@Override
	public GroupApiKey findByApplicationSecret_Last(
			String applicationSecret,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws NoSuchGroupApiKeyException {

		GroupApiKey groupApiKey = fetchByApplicationSecret_Last(
			applicationSecret, orderByComparator);

		if (groupApiKey != null) {
			return groupApiKey;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("applicationSecret=");
		sb.append(applicationSecret);

		sb.append("}");

		throw new NoSuchGroupApiKeyException(sb.toString());
	}

	/**
	 * Returns the last group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group api key, or <code>null</code> if a matching group api key could not be found
	 */
	@Override
	public GroupApiKey fetchByApplicationSecret_Last(
		String applicationSecret,
		OrderByComparator<GroupApiKey> orderByComparator) {

		int count = countByApplicationSecret(applicationSecret);

		if (count == 0) {
			return null;
		}

		List<GroupApiKey> list = findByApplicationSecret(
			applicationSecret, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the group api keys before and after the current group api key in the ordered set where applicationSecret = &#63;.
	 *
	 * @param groupApiKeyPK the primary key of the current group api key
	 * @param applicationSecret the application secret
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	@Override
	public GroupApiKey[] findByApplicationSecret_PrevAndNext(
			GroupApiKeyPK groupApiKeyPK, String applicationSecret,
			OrderByComparator<GroupApiKey> orderByComparator)
		throws NoSuchGroupApiKeyException {

		applicationSecret = Objects.toString(applicationSecret, "");

		GroupApiKey groupApiKey = findByPrimaryKey(groupApiKeyPK);

		Session session = null;

		try {
			session = openSession();

			GroupApiKey[] array = new GroupApiKeyImpl[3];

			array[0] = getByApplicationSecret_PrevAndNext(
				session, groupApiKey, applicationSecret, orderByComparator,
				true);

			array[1] = groupApiKey;

			array[2] = getByApplicationSecret_PrevAndNext(
				session, groupApiKey, applicationSecret, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected GroupApiKey getByApplicationSecret_PrevAndNext(
		Session session, GroupApiKey groupApiKey, String applicationSecret,
		OrderByComparator<GroupApiKey> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_GROUPAPIKEY_WHERE);

		boolean bindApplicationSecret = false;

		if (applicationSecret.isEmpty()) {
			sb.append(_FINDER_COLUMN_APPLICATIONSECRET_APPLICATIONSECRET_3);
		}
		else {
			bindApplicationSecret = true;

			sb.append(_FINDER_COLUMN_APPLICATIONSECRET_APPLICATIONSECRET_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(GroupApiKeyModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindApplicationSecret) {
			queryPos.add(applicationSecret);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(groupApiKey)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<GroupApiKey> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the group api keys where applicationSecret = &#63; from the database.
	 *
	 * @param applicationSecret the application secret
	 */
	@Override
	public void removeByApplicationSecret(String applicationSecret) {
		for (GroupApiKey groupApiKey :
				findByApplicationSecret(
					applicationSecret, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(groupApiKey);
		}
	}

	/**
	 * Returns the number of group api keys where applicationSecret = &#63;.
	 *
	 * @param applicationSecret the application secret
	 * @return the number of matching group api keys
	 */
	@Override
	public int countByApplicationSecret(String applicationSecret) {
		applicationSecret = Objects.toString(applicationSecret, "");

		FinderPath finderPath = _finderPathCountByApplicationSecret;

		Object[] finderArgs = new Object[] {applicationSecret};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_GROUPAPIKEY_WHERE);

			boolean bindApplicationSecret = false;

			if (applicationSecret.isEmpty()) {
				sb.append(_FINDER_COLUMN_APPLICATIONSECRET_APPLICATIONSECRET_3);
			}
			else {
				bindApplicationSecret = true;

				sb.append(_FINDER_COLUMN_APPLICATIONSECRET_APPLICATIONSECRET_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindApplicationSecret) {
					queryPos.add(applicationSecret);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String
		_FINDER_COLUMN_APPLICATIONSECRET_APPLICATIONSECRET_2 =
			"CAST_CLOB_TEXT(groupApiKey.applicationSecret) = ?";

	private static final String
		_FINDER_COLUMN_APPLICATIONSECRET_APPLICATIONSECRET_3 =
			"(groupApiKey.applicationSecret IS NULL OR CAST_CLOB_TEXT(groupApiKey.applicationSecret) = '')";

	public GroupApiKeyPersistenceImpl() {
		setModelClass(GroupApiKey.class);

		setModelImplClass(GroupApiKeyImpl.class);
		setModelPKClass(GroupApiKeyPK.class);

		setTable(GroupApiKeyTable.INSTANCE);
	}

	/**
	 * Caches the group api key in the entity cache if it is enabled.
	 *
	 * @param groupApiKey the group api key
	 */
	@Override
	public void cacheResult(GroupApiKey groupApiKey) {
		entityCache.putResult(
			GroupApiKeyImpl.class, groupApiKey.getPrimaryKey(), groupApiKey);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the group api keys in the entity cache if it is enabled.
	 *
	 * @param groupApiKeys the group api keys
	 */
	@Override
	public void cacheResult(List<GroupApiKey> groupApiKeys) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (groupApiKeys.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (GroupApiKey groupApiKey : groupApiKeys) {
			if (entityCache.getResult(
					GroupApiKeyImpl.class, groupApiKey.getPrimaryKey()) ==
						null) {

				cacheResult(groupApiKey);
			}
		}
	}

	/**
	 * Clears the cache for all group api keys.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(GroupApiKeyImpl.class);

		finderCache.clearCache(GroupApiKeyImpl.class);
	}

	/**
	 * Clears the cache for the group api key.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(GroupApiKey groupApiKey) {
		entityCache.removeResult(GroupApiKeyImpl.class, groupApiKey);
	}

	@Override
	public void clearCache(List<GroupApiKey> groupApiKeys) {
		for (GroupApiKey groupApiKey : groupApiKeys) {
			entityCache.removeResult(GroupApiKeyImpl.class, groupApiKey);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(GroupApiKeyImpl.class);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(GroupApiKeyImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new group api key with the primary key. Does not add the group api key to the database.
	 *
	 * @param groupApiKeyPK the primary key for the new group api key
	 * @return the new group api key
	 */
	@Override
	public GroupApiKey create(GroupApiKeyPK groupApiKeyPK) {
		GroupApiKey groupApiKey = new GroupApiKeyImpl();

		groupApiKey.setNew(true);
		groupApiKey.setPrimaryKey(groupApiKeyPK);

		groupApiKey.setCompanyId(CompanyThreadLocal.getCompanyId());

		return groupApiKey;
	}

	/**
	 * Removes the group api key with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key that was removed
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	@Override
	public GroupApiKey remove(GroupApiKeyPK groupApiKeyPK)
		throws NoSuchGroupApiKeyException {

		return remove((Serializable)groupApiKeyPK);
	}

	/**
	 * Removes the group api key with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the group api key
	 * @return the group api key that was removed
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	@Override
	public GroupApiKey remove(Serializable primaryKey)
		throws NoSuchGroupApiKeyException {

		Session session = null;

		try {
			session = openSession();

			GroupApiKey groupApiKey = (GroupApiKey)session.get(
				GroupApiKeyImpl.class, primaryKey);

			if (groupApiKey == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchGroupApiKeyException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(groupApiKey);
		}
		catch (NoSuchGroupApiKeyException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected GroupApiKey removeImpl(GroupApiKey groupApiKey) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(groupApiKey)) {
				groupApiKey = (GroupApiKey)session.get(
					GroupApiKeyImpl.class, groupApiKey.getPrimaryKeyObj());
			}

			if (groupApiKey != null) {
				session.delete(groupApiKey);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (groupApiKey != null) {
			clearCache(groupApiKey);
		}

		return groupApiKey;
	}

	@Override
	public GroupApiKey updateImpl(GroupApiKey groupApiKey) {
		boolean isNew = groupApiKey.isNew();

		if (!(groupApiKey instanceof GroupApiKeyModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(groupApiKey.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(groupApiKey);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in groupApiKey proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom GroupApiKey implementation " +
					groupApiKey.getClass());
		}

		GroupApiKeyModelImpl groupApiKeyModelImpl =
			(GroupApiKeyModelImpl)groupApiKey;

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(groupApiKey);
			}
			else {
				groupApiKey = (GroupApiKey)session.merge(groupApiKey);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			GroupApiKeyImpl.class, groupApiKeyModelImpl, false, true);

		if (isNew) {
			groupApiKey.setNew(false);
		}

		groupApiKey.resetOriginalValues();

		return groupApiKey;
	}

	/**
	 * Returns the group api key with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the group api key
	 * @return the group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	@Override
	public GroupApiKey findByPrimaryKey(Serializable primaryKey)
		throws NoSuchGroupApiKeyException {

		GroupApiKey groupApiKey = fetchByPrimaryKey(primaryKey);

		if (groupApiKey == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchGroupApiKeyException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return groupApiKey;
	}

	/**
	 * Returns the group api key with the primary key or throws a <code>NoSuchGroupApiKeyException</code> if it could not be found.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key
	 * @throws NoSuchGroupApiKeyException if a group api key with the primary key could not be found
	 */
	@Override
	public GroupApiKey findByPrimaryKey(GroupApiKeyPK groupApiKeyPK)
		throws NoSuchGroupApiKeyException {

		return findByPrimaryKey((Serializable)groupApiKeyPK);
	}

	/**
	 * Returns the group api key with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key, or <code>null</code> if a group api key with the primary key could not be found
	 */
	@Override
	public GroupApiKey fetchByPrimaryKey(GroupApiKeyPK groupApiKeyPK) {
		return fetchByPrimaryKey((Serializable)groupApiKeyPK);
	}

	/**
	 * Returns all the group api keys.
	 *
	 * @return the group api keys
	 */
	@Override
	public List<GroupApiKey> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of group api keys
	 */
	@Override
	public List<GroupApiKey> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of group api keys
	 */
	@Override
	public List<GroupApiKey> findAll(
		int start, int end, OrderByComparator<GroupApiKey> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of group api keys
	 */
	@Override
	public List<GroupApiKey> findAll(
		int start, int end, OrderByComparator<GroupApiKey> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<GroupApiKey> list = null;

		if (useFinderCache) {
			list = (List<GroupApiKey>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_GROUPAPIKEY);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_GROUPAPIKEY;

				sql = sql.concat(GroupApiKeyModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<GroupApiKey>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the group api keys from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (GroupApiKey groupApiKey : findAll()) {
			remove(groupApiKey);
		}
	}

	/**
	 * Returns the number of group api keys.
	 *
	 * @return the number of group api keys
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_GROUPAPIKEY);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getCompoundPKColumnNames() {
		return _compoundPKColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "groupApiKeyPK";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_GROUPAPIKEY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return GroupApiKeyModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the group api key persistence.
	 */
	@Activate
	public void activate() {
		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByApplicationKey = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByApplicationKey",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"applicationKey"}, true);

		_finderPathWithoutPaginationFindByApplicationKey = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByApplicationKey",
			new String[] {String.class.getName()},
			new String[] {"applicationKey"}, true);

		_finderPathCountByApplicationKey = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByApplicationKey",
			new String[] {String.class.getName()},
			new String[] {"applicationKey"}, false);

		_finderPathWithPaginationFindByApplicationSecret = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByApplicationSecret",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"applicationSecret"}, true);

		_finderPathWithoutPaginationFindByApplicationSecret = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByApplicationSecret", new String[] {String.class.getName()},
			new String[] {"applicationSecret"}, true);

		_finderPathCountByApplicationSecret = new FinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByApplicationSecret", new String[] {String.class.getName()},
			new String[] {"applicationSecret"}, false);

		GroupApiKeyUtil.setPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		GroupApiKeyUtil.setPersistence(null);

		entityCache.removeCache(GroupApiKeyImpl.class.getName());
	}

	@Override
	@Reference(
		target = Placecube_GroupRestPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = Placecube_GroupRestPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = Placecube_GroupRestPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_GROUPAPIKEY =
		"SELECT groupApiKey FROM GroupApiKey groupApiKey";

	private static final String _SQL_SELECT_GROUPAPIKEY_WHERE =
		"SELECT groupApiKey FROM GroupApiKey groupApiKey WHERE ";

	private static final String _SQL_COUNT_GROUPAPIKEY =
		"SELECT COUNT(groupApiKey) FROM GroupApiKey groupApiKey";

	private static final String _SQL_COUNT_GROUPAPIKEY_WHERE =
		"SELECT COUNT(groupApiKey) FROM GroupApiKey groupApiKey WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "groupApiKey.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No GroupApiKey exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No GroupApiKey exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		GroupApiKeyPersistenceImpl.class);

	private static final Set<String> _compoundPKColumnNames = SetUtil.fromArray(
		new String[] {"companyId", "groupId"});

	@Override
	protected FinderCache getFinderCache() {
		return finderCache;
	}

}