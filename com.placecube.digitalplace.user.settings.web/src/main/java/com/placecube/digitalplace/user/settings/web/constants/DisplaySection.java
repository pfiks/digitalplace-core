package com.placecube.digitalplace.user.settings.web.constants;

public enum DisplaySection {

	ACCOUNT_SETTINGS("account-settings", 2),

	NOTIFICATIONS("notifications", 1);

	private final String key;
	private final Integer order;

	private DisplaySection(String key, Integer order) {
		this.key = key;
		this.order = order;
	}

	public String getKey() {
		return key;
	}

	public Integer getOrder() {
		return order;
	}

}
