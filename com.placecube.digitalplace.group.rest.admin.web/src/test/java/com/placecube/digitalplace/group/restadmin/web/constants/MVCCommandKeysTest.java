package com.placecube.digitalplace.group.restadmin.web.constants;

import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isPrivate;
import static java.lang.reflect.Modifier.isStatic;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

public class MVCCommandKeysTest extends PowerMockito {

	private static final Class<?> CLASS_TO_TEST = MVCCommandKeys.class;

	@Test
	public void testClassIsConstantsClass() throws Exception {
		assertThat(isFinal(CLASS_TO_TEST.getModifiers()), equalTo(true));

		assertThat(CLASS_TO_TEST.getDeclaredConstructors().length, equalTo(1));
		Constructor<?> constructor = CLASS_TO_TEST.getDeclaredConstructor();
		assertThat(isPrivate(constructor.getModifiers()), equalTo(true));
		assertThat(constructor.isAccessible(), equalTo(false));

		constructor.setAccessible(true);
		constructor.newInstance();
		constructor.setAccessible(false);

		for (Method method : CLASS_TO_TEST.getMethods()) {
			if (method.isAccessible()) {
				assertThat(isStatic(method.getModifiers()), equalTo(true));
				assertThat(method.getDeclaringClass(), equalTo(CLASS_TO_TEST));
			}
		}
	}
}
