package com.placecube.digitalplace.fileupload.service.impl;

import java.io.InputStream;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.fileupload.exception.FileUploadException;
import com.placecube.digitalplace.fileupload.service.FileUploadConnector;
import com.placecube.digitalplace.fileupload.service.FileUploadService;

@Component(immediate = true, service = FileUploadService.class)
public class FileUploadServiceImpl implements FileUploadService {

	private Set<FileUploadConnector> fileUploadConnectors = new LinkedHashSet<>();

	@Override
	public void uploadFile(String name, String path, InputStream file, Optional<String> connectorId, ServiceContext serviceContext) throws FileUploadException {
		if (Validator.isNotNull(name) && Validator.isNotNull(path)) {
			FileUploadConnector fileUploadConnector = getFileUploadConnector(serviceContext.getCompanyId(), connectorId);
			fileUploadConnector.uploadFile(name, path, file, serviceContext);
		} else {
			throw new FileUploadException("Unable to upload file using name (" + name + ") and path (" + path + ")");
		}
	}

	protected FileUploadConnector getFileUploadConnector(long companyId, Optional<String> connectorId) throws FileUploadException {
		Optional<FileUploadConnector> fileUploadConnector;

		if (Validator.isNotNull(connectorId) && connectorId.isPresent()) {
			fileUploadConnector = fileUploadConnectors.stream().filter(entry -> entry.isEnabled(companyId) && entry.getId().equals(connectorId.get())).findFirst();
		} else {
			fileUploadConnector = fileUploadConnectors.stream().filter(entry -> entry.isEnabled(companyId) && entry.isDefault(companyId)).findFirst();
		}

		if (fileUploadConnector.isPresent()) {
			return fileUploadConnector.get();
		} else {
			throw new FileUploadException(
					"No file upload connector found. Total connecotrs: ( " + fileUploadConnectors.size() + " ), company id: ( " + companyId + " ), connector id: ( " + connectorId.get() + " ).");
		}
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setFileUploadConnector(FileUploadConnector fileUploadConnector) {
		fileUploadConnectors.add(fileUploadConnector);
	}

	protected void unsetFileUploadConnector(FileUploadConnector fileUploadConnector) {
		fileUploadConnectors.remove(fileUploadConnector);
	}

}
