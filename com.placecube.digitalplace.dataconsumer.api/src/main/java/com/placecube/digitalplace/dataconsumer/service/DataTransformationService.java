package com.placecube.digitalplace.dataconsumer.service;

import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.json.JSONException;
import com.placecube.digitalplace.dataconsumer.model.DataProviderInputParameter;
import com.placecube.digitalplace.dataconsumer.model.DataProviderOutputParameter;

import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface DataTransformationService {

	Map<String, String> deserializeMapping(String mappingJson) throws JSONException;

	DDMFormValues getDataProviderFormValues(DDMDataProviderInstance dataProviderInstance);

	List<DataProviderInputParameter> getInputParameters(DDMFormValues values, Locale locale);

	List<DataProviderOutputParameter> getOutputParameters(DDMFormValues values, Locale locale);

	String serializeMapping(Map<String, String> paramMap);

	String getDefaultInputParametersJSONMapping(List<DataProviderInputParameter> inputFields);

}
