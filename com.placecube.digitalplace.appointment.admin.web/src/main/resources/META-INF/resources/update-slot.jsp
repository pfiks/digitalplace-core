<%@ include file="init.jsp" %>

<div class="container-fluid container-fluid-max-xl">
	<h1>
		<c:choose>
			<c:when test="${appointmentSlotForm.isNew()}">
				<liferay-ui:message key="create-new-slot"/>
			</c:when>
			<c:otherwise>
				<liferay-ui:message key="update-x" arguments="${appointmentSlotForm.getName()}"/>
			</c:otherwise>
		</c:choose>
	</h1>

	<div class="row">
		<div class="col-12">
			<portlet:actionURL name="<%=MVCCommandKeys.UPDATE_SLOT%>" var="updateAppointmentSlotURL">
				<portlet:param name="appointmentSlotId" value="${appointmentSlotForm.getAppointmentSlotId()}"/>
				<portlet:param name="slotType" value="${appointmentSlotForm.getSlotType()}"/>
			</portlet:actionURL>

			<aui:form action="${updateAppointmentSlotURL}" name="updateAppointmentSlotForm" method="post">

				<aui:input name="slotTypeCurrent" type="text" value="${appointmentSlotForm.getSlotType()}" label="type" disabled="true" />
				
				<c:choose>
					<c:when test="${appointmentSlotForm.isNew()}">
						<aui:input name="serviceId" type="text" value="${appointmentSlotForm.getServiceId()}" required="true" label="service-id">
							<aui:validator name="maxLength">250</aui:validator>
						</aui:input>
					</c:when>
					<c:otherwise>
						<aui:input name="serviceIdCurrent" type="text" value="${appointmentSlotForm.getServiceId()}" label="service-id" disabled="true" />
					</c:otherwise>
				</c:choose>

				<aui:input name="name" type="text" value="${appointmentSlotForm.getName()}" required="true" label="name">
					<aui:validator name="maxLength">250</aui:validator>
				</aui:input>
				
				<aui:row>
					<aui:col span="6">
						<aui:field-wrapper name="startDateValue" label="slot-start-date">
							<liferay-ui:input-date name="startDate"
								dayValue="${appointmentSlotForm.startDateTime.day}" dayParam="startDay"
								monthValue="${appointmentSlotForm.startDateTime.month}" monthParam="startMonth"
								yearValue="${appointmentSlotForm.startDateTime.year}" yearParam="startYear" />
						</aui:field-wrapper>
					</aui:col>
					<aui:col span="6">
						<aui:field-wrapper name="endDateValue" label="slot-end-date">
							<liferay-ui:input-date name="endDate"
								dayValue="${appointmentSlotForm.endDateTime.day}" dayParam="endDay"
								monthValue="${appointmentSlotForm.endDateTime.month}" monthParam="endMonth"
								yearValue="${appointmentSlotForm.endDateTime.year}" yearParam="endYear" />
						</aui:field-wrapper>
					</aui:col>
				</aui:row>

				<aui:row>
					<aui:col span="12">
						<label class="control-label"><liferay-ui:message key="slot-days-of-week"/></label>
						<aui:input type="checkbox" name="daysOfWeek" id="daysOfWeekMonday" value="<%= DayOfWeek.MONDAY.getValue() %>" label="<%= DayOfWeek.MONDAY.name() %>" checked="${appointmentSlotForm.isDayOfWeekSelected(1)}" />
						<aui:input type="checkbox" name="daysOfWeek" id="daysOfWeekTuesday" value="<%= DayOfWeek.TUESDAY.getValue() %>" label="<%= DayOfWeek.TUESDAY.name() %>" checked="${appointmentSlotForm.isDayOfWeekSelected(2)}" />
						<aui:input type="checkbox" name="daysOfWeek" id="daysOfWeekWednesday" value="<%= DayOfWeek.WEDNESDAY.getValue() %>" label="<%= DayOfWeek.WEDNESDAY.name() %>" checked="${appointmentSlotForm.isDayOfWeekSelected(3)}" />
						<aui:input type="checkbox" name="daysOfWeek" id="daysOfWeekThursday" value="<%= DayOfWeek.THURSDAY.getValue() %>" label="<%= DayOfWeek.THURSDAY.name() %>" checked="${appointmentSlotForm.isDayOfWeekSelected(4)}" />
						<aui:input type="checkbox" name="daysOfWeek" id="daysOfWeekFriday" value="<%= DayOfWeek.FRIDAY.getValue() %>" label="<%= DayOfWeek.FRIDAY.name() %>" checked="${appointmentSlotForm.isDayOfWeekSelected(5)}" />
						<aui:input type="checkbox" name="daysOfWeek" id="daysOfWeekSaturday" value="<%= DayOfWeek.SATURDAY.getValue() %>" label="<%= DayOfWeek.SATURDAY.name() %>" checked="${appointmentSlotForm.isDayOfWeekSelected(6)}" />
						<aui:input type="checkbox" name="daysOfWeek" id="daysOfWeekSunday" value="<%= DayOfWeek.SUNDAY.getValue() %>" label="<%= DayOfWeek.SUNDAY.name() %>" checked="${appointmentSlotForm.isDayOfWeekSelected(7)}" />
					</aui:col>
				</aui:row>
				
				
				<c:if test="${appointmentSlotForm.isSlotTypeConfiguration()}">
					<aui:input name="appointmentDuration" type="number" value="${appointmentSlotForm.getAppointmentDuration()}" required="true" label="appointment-duration">
						<aui:validator name="min">5</aui:validator>
					</aui:input>
					
					<aui:input name="maxAppointments" type="number" value="${appointmentSlotForm.getMaxAppointments()}" required="true" label="max-appointments">
						<aui:validator name="min">1</aui:validator>
					</aui:input>
				</c:if>
				<aui:row>
					<aui:col span="6">
						<aui:field-wrapper name="startDateTime" label="slot-start-time">
							<liferay-ui:input-time name="startTime"
								amPmValue="${appointmentSlotForm.startDateTime.amORpm}" amPmParam="startAMorPM"
								hourValue="${appointmentSlotForm.startDateTime.hour}" hourParam="startHour"
								minuteValue="${appointmentSlotForm.startDateTime.minute}" minuteParam="startMinute" />
						</aui:field-wrapper>
					</aui:col>
					<aui:col span="6">
						<aui:field-wrapper name="endDateAndTime" label="slot-end-time">
							<liferay-ui:input-time name="endTime"
								amPmValue="${appointmentSlotForm.endDateTime.amORpm}" amPmParam="endAMorPM"
								hourValue="${appointmentSlotForm.endDateTime.hour}" hourParam="endHour"
								minuteValue="${appointmentSlotForm.endDateTime.minute}" minuteParam="endMinute" />
						</aui:field-wrapper>
					</aui:col>
				</aui:row>
				
				<aui:button-row>
					<aui:button type="submit" value="save"/>

					<portlet:renderURL var="cancelURL"/>
					<aui:button href="${cancelURL}" type="cancel" value="cancel" />
				</aui:button-row>
			</aui:form>
		</div>
	</div>
</div>