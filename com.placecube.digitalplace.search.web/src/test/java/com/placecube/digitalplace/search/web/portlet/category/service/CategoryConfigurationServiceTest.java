package com.placecube.digitalplace.search.web.portlet.category.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.web.portlet.category.configuration.CategoryFacetPortletInstanceConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class CategoryConfigurationServiceTest extends PowerMockito {

	@InjectMocks
	private CategoryConfigurationService categoryConfigurationService;

	@Mock
	private CategoryFacetPortletInstanceConfiguration mockCategoryFacetPortletInstanceConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Test(expected = ConfigurationException.class)
	public void getValidConfiguration_WhenExceptionRetrievingTheConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		categoryConfigurationService.getValidConfiguration(mockThemeDisplay);
	}

	@Test
	@Parameters({ "0", "-1" })
	public void getValidConfiguration_WhenNoErrorAndConfigurationIsInvalid_ThenReturnsEmptyOptional(int maxCategoriesToShow) throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.maxCategoriesToShow()).thenReturn(maxCategoriesToShow);

		Optional<CategoryFacetPortletInstanceConfiguration> result = categoryConfigurationService.getValidConfiguration(mockThemeDisplay);

		assertFalse(result.isPresent());
	}

	@Test
	public void getValidConfiguration_WhenNoErrorAndConfigurationIsValid_ThenReturnsOptionalWithTheConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.maxCategoriesToShow()).thenReturn(1);

		Optional<CategoryFacetPortletInstanceConfiguration> result = categoryConfigurationService.getValidConfiguration(mockThemeDisplay);

		assertThat(result.get(), sameInstance(mockCategoryFacetPortletInstanceConfiguration));
	}

}
