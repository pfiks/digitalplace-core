package com.placecube.digitalplace.payment.capita.service.internal;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.exception.NoSuchFileEntryException;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Repository;
import com.liferay.portal.kernel.portletfilerepository.PortletFileRepository;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.placecube.digitalplace.payment.capita.service.internal.constants.OneOffPaymentOrderConstants;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;

@Component(immediate = true, service = OneOffPaymentHelper.class)
public class OneOffPaymentHelper {

	@Reference
	private PortletFileRepository portletFileRepository;

	@Reference
	private UserLocalService userLocalService;

	@Reference
	private DLFolderLocalService dlFolderLocalService;

	@Reference
	private DLFileEntryLocalService dlFileEntryLocalService;

	private static final Log LOG = LogFactoryUtil.getLog(OneOffPaymentHelper.class);

	public FileEntry addOrReplaceFileEntry(String fileName, File tempFile, Folder folder, ServiceContext serviceContext) throws PortalException {
		String contentType = MimeTypesUtil.getContentType(tempFile, fileName);

		try {
			FileEntry fileEntry = portletFileRepository.getPortletFileEntry(serviceContext.getScopeGroupId(), folder.getFolderId(), fileName);
			portletFileRepository.deletePortletFileEntry(fileEntry.getFileEntryId());

			LOG.debug("Previous file entry with title: " + fileName + " deleted");
		} catch (NoSuchFileEntryException e) {
			LOG.debug("File entry with title: " + fileName + " does not exist");
		}

		return portletFileRepository.addPortletFileEntry(null, serviceContext.getScopeGroupId(), userLocalService.getGuestUserId(serviceContext.getCompanyId()), DDMFormInstance.class.getName(),
				DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, DDMFormInstance.class.getName(), folder.getFolderId(), tempFile, fileName, contentType, true);
	}

	public File createPaymentTempFile(OneOffPaymentOrder oneOffPaymentOrder) throws IOException {
		return File.createTempFile(oneOffPaymentOrder.getInternalReference(), "");
	}

	public Folder getOrAddPaymentTempFilesFolder(ServiceContext serviceContext) throws PortalException {

		Folder paymentTempFilesFolder = null;
		Repository repository = getOrAddPortletRepository(serviceContext);

		try {
			paymentTempFilesFolder = portletFileRepository.getPortletFolder(repository.getRepositoryId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
					OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME);
		} catch (Exception e) {
			serviceContext.setAddGuestPermissions(true);
			paymentTempFilesFolder = portletFileRepository.addPortletFolder(serviceContext.getUserId(), repository.getRepositoryId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,
					OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME, serviceContext);
			LOG.debug("Temporary files folder added " + paymentTempFilesFolder);
		}
		return paymentTempFilesFolder;
	}

	public FileEntry getPortletFileEntry(String paymentReference, Folder folder) throws PortalException {

		try {
			return portletFileRepository.getPortletFileEntry(folder.getGroupId(), folder.getFolderId(), paymentReference);
		} catch (PortalException e) {
			LOG.warn("Unable to find the file entry for paymentReference: " + paymentReference + " on folderId: " + folder.getFolderId() + ". Trying to find it in existing "
					+ OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME + " folders.");
			return fileEntryFallback(paymentReference, folder);
		}
	}

	private FileEntry fileEntryFallback(String paymentReference, Folder folder) throws PortalException {

		DynamicQuery dlFolderDQ = dlFolderLocalService.dynamicQuery();
		dlFolderDQ.add(RestrictionsFactoryUtil.eq(Field.NAME, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME));
		dlFolderDQ.setProjection(PropertyFactoryUtil.forName(Field.FOLDER_ID));
		List<Long> dlFoldersIds = dlFolderLocalService.dynamicQuery(dlFolderDQ);

		DynamicQuery dlFileEntryDQ = dlFileEntryLocalService.dynamicQuery();
		dlFileEntryDQ.add(RestrictionsFactoryUtil.eq(Field.COMPANY_ID, folder.getCompanyId()));
		dlFileEntryDQ.add(RestrictionsFactoryUtil.in(Field.FOLDER_ID, dlFoldersIds));
		dlFileEntryDQ.add(RestrictionsFactoryUtil.eq("fileName", paymentReference));

		List<DLFileEntry> dlFileEntries = dlFileEntryLocalService.dynamicQuery(dlFileEntryDQ);
		if (!dlFileEntries.isEmpty()) {
			return portletFileRepository.getPortletFileEntry(dlFileEntries.get(0).getFileEntryId());
		}
		throw new PortalException("Unable to find the file entry for paymentReference: " + paymentReference);
	}

	private Repository getOrAddPortletRepository(ServiceContext serviceContext) throws PortalException {

		Repository repository = null;
		try {
			repository = portletFileRepository.getPortletRepository(serviceContext.getScopeGroupId(), DDMFormInstance.class.getName());
		} catch (Exception e) {
			serviceContext.setAddGuestPermissions(true);
			repository = portletFileRepository.addPortletRepository(serviceContext.getScopeGroupId(), DDMFormInstance.class.getName(), serviceContext);
			LOG.debug("Repository added " + repository);
		}

		return repository;
	}
}
