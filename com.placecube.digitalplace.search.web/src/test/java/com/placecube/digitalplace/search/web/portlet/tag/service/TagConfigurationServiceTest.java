package com.placecube.digitalplace.search.web.portlet.tag.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.web.portlet.tag.configuration.TagFacetPortletInstanceConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class TagConfigurationServiceTest extends PowerMockito {

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private TagFacetPortletInstanceConfiguration mockTagFacetPortletInstanceConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private TagConfigurationService tagConfigurationService;

	@Test(expected = ConfigurationException.class)
	public void getValidConfiguration_WhenExceptionRetrievingTheConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TagFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		tagConfigurationService.getValidConfiguration(mockThemeDisplay);
	}

	@Test
	@Parameters({ "0,1", "1,-1", "-1,0", "-1,1" })
	public void getValidConfiguration_WhenNoErrorAndConfigurationIsInvalid_ThenReturnsEmptyOptional(int maxTagsToShow, int maxShowMoreTagsToShow) throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TagFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTagFacetPortletInstanceConfiguration);
		when(mockTagFacetPortletInstanceConfiguration.maxShowMoreTagsToShow()).thenReturn(maxShowMoreTagsToShow);
		when(mockTagFacetPortletInstanceConfiguration.maxTagsToShow()).thenReturn(maxTagsToShow);

		Optional<TagFacetPortletInstanceConfiguration> result = tagConfigurationService.getValidConfiguration(mockThemeDisplay);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters({ "1,1", "1,0" })
	public void getValidConfiguration_WhenNoErrorAndConfigurationIsValid_ThenReturnsOptionalWithTheConfiguration(int maxTagsToShow, int maxShowMoreTagsToShow) throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TagFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTagFacetPortletInstanceConfiguration);
		when(mockTagFacetPortletInstanceConfiguration.maxShowMoreTagsToShow()).thenReturn(maxShowMoreTagsToShow);
		when(mockTagFacetPortletInstanceConfiguration.maxTagsToShow()).thenReturn(maxTagsToShow);

		Optional<TagFacetPortletInstanceConfiguration> result = tagConfigurationService.getValidConfiguration(mockThemeDisplay);

		assertThat(result.get(), sameInstance(mockTagFacetPortletInstanceConfiguration));
	}

}
