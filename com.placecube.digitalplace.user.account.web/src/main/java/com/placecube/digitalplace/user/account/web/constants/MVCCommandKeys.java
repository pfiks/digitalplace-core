package com.placecube.digitalplace.user.account.web.constants;

public final class MVCCommandKeys {

	public static final String ACCOUNT_SECURITY = "/createAccount/account-security";

	public static final String ADDITIONAL_DETAILS = "/createAccount/additional-details";

	public static final String ADDRESS_DETAILS = "/createAccount/address-details";

	public static final String ADDRESS_LOOKUP = "/createAccount/address-lookup";

	public static final String CLOSE_POPUP = "/createAccount/close-popup";

	public static final String CONFIRMATION_PAGE = "/createAccount/confirmation-page";

	public static final String CONFIRMATION_PASSWORD_RESET_PAGE = "/createAccount/confirmation-password-reset-page";

	public static final String DISPLAY_CAPTCHA = "/createAccount/captcha";

	public static final String EDIT_DETAILS = "/createAccount/edit-details";

	public static final String PASSWORD_DETAILS = "/createAccount/password-details";

	public static final String PASSWORD_RESET = "/createAccount/password-reset";

	public static final String PERSONAL_DETAILS = "/createAccount/personal-details";

	private MVCCommandKeys() {

	}

}
