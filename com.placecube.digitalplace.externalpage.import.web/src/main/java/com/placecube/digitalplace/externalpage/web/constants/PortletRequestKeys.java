package com.placecube.digitalplace.externalpage.web.constants;

public class PortletRequestKeys {

	public static final String EXTERNAL_PAGE_ID = "externalPageId";

	public static final String INDEX_CONTENT = "indexContent";

	public static final String MAX_PAGES_TO_FETCH = "maxPagesToFetch";

	public static final String MVC_RENDER_COMMAND_NAME = "mvcRenderCommandName";

	public static final String REDIRECT = "redirect";

	public static final String ROW_IDS = "rowIds";

	public static final String STATUS = "status";

	public static final String URL = "url";

	public static final String XPATH = "xpath";

	private PortletRequestKeys() {

	}
}
