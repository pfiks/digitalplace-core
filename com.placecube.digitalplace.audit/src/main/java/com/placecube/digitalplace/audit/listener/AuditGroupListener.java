package com.placecube.digitalplace.audit.listener;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.security.audit.event.generators.constants.EventTypes;
import com.placecube.digitalplace.audit.service.AuditService;

@Component(immediate = true, service = ModelListener.class)
public class AuditGroupListener extends BaseModelListener<Group> {

	@Reference
	private AuditService auditService;

	@Override
	public void onAfterCreate(Group group) {
		addAuditEntry(group, EventTypes.ADD);
	}

	@Override
	public void onAfterRemove(Group group) {
		addAuditEntry(group, EventTypes.DELETE);
	}

	@Override
	public void onAfterUpdate(Group previousGroup, Group group) {
		addAuditEntry(group, EventTypes.UPDATE);
	}

	private void addAuditEntry(Group group, String eventType) {
		if (group.isSite() || group.isOrganization()) {
			auditService.addAuditLogEntry(Group.class.getName(), group.getGroupId(), eventType, auditService.getGroupAuditInfo(group));
		}
	}

}
