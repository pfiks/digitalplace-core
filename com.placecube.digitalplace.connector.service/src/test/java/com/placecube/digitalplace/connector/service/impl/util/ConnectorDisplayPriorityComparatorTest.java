package com.placecube.digitalplace.connector.service.impl.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.placecube.digitalplace.connector.constants.ConnectorConstants;
import com.placecube.digitalplace.connector.model.Connector;

@RunWith(MockitoJUnitRunner.class)
public class ConnectorDisplayPriorityComparatorTest {

	private ConnectorDisplayPriorityComparator connectorDisplayPriorityComparator;

	@Mock
	private ServiceWrapper<Connector> mockServiceWrapper1;

	@Mock
	private ServiceWrapper<Connector> mockServiceWrapper2;

	@Test
	public void compare_WhenFirstServiceDisplayPriorityIsEqualToSecondServicePriority_ThenReturnsZero() {
		Map<String, Object> connectorService1Properties = new HashMap<>();
		connectorService1Properties.put(ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY, 2);

		Map<String, Object> connectorService2Properties = new HashMap<>();
		connectorService2Properties.put(ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY, 2);

		when(mockServiceWrapper1.getProperties()).thenReturn(connectorService1Properties);
		when(mockServiceWrapper2.getProperties()).thenReturn(connectorService2Properties);

		int result = connectorDisplayPriorityComparator.compare(mockServiceWrapper1, mockServiceWrapper2);

		assertThat(result, equalTo(0));
	}

	@Test
	public void compare_WhenFirstServiceDisplayPriorityIsGreaterThanSecondServicePriority_ThenReturnsOne() {
		Map<String, Object> connectorService1Properties = new HashMap<>();
		connectorService1Properties.put(ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY, 2);

		Map<String, Object> connectorService2Properties = new HashMap<>();
		connectorService2Properties.put(ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY, 1);

		when(mockServiceWrapper1.getProperties()).thenReturn(connectorService1Properties);
		when(mockServiceWrapper2.getProperties()).thenReturn(connectorService2Properties);

		int result = connectorDisplayPriorityComparator.compare(mockServiceWrapper1, mockServiceWrapper2);

		assertThat(result, equalTo(1));
	}

	@Test
	public void compare_WhenFirstServiceDisplayPriorityIsSmallerThanSecondServicePriority_ThenReturnsMinusOne() {
		Map<String, Object> connectorService1Properties = new HashMap<>();
		connectorService1Properties.put(ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY, 1);

		Map<String, Object> connectorService2Properties = new HashMap<>();
		connectorService2Properties.put(ConnectorConstants.CONNECTOR_DISPLAY_PRIORITY, 2);

		when(mockServiceWrapper1.getProperties()).thenReturn(connectorService1Properties);
		when(mockServiceWrapper2.getProperties()).thenReturn(connectorService2Properties);

		int result = connectorDisplayPriorityComparator.compare(mockServiceWrapper1, mockServiceWrapper2);

		assertThat(result, equalTo(-1));
	}

	@Before
	public void setUp() {
		connectorDisplayPriorityComparator = new ConnectorDisplayPriorityComparator();
	}
}
