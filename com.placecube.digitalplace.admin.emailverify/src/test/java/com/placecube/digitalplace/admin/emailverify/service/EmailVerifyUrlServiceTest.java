package com.placecube.digitalplace.admin.emailverify.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import javax.portlet.ActionRequest;
import javax.portlet.ActionURL;
import javax.portlet.PortletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.admin.emailverify.constants.EmailVerifyMVCCommandKeys;
import com.placecube.digitalplace.admin.emailverify.constants.EmailVerifyPortletKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class })
public class EmailVerifyUrlServiceTest extends PowerMockito {

	@Mock
	private ActionURL mockActionURL;

	@Mock
	private LiferayPortletResponse mockLiferayPortletResponse;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletResponse mockPortletResponse;

	@Mock
	private User mockUser;

	@InjectMocks
	private EmailVerifyUrlService emailVerifyUrlService;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void getVerifyEmailURL_WhenNoError_ThenReturnsTheConfiguredVerifyEmailURL() {
		String urlValue = "thisIsMyUrl&with" + PortletKeys.USERS_ADMIN + "andSomeOther" + PortletKeys.USERS_ADMIN + "andOtherStuff";
		String expectedValue = "thisIsMyUrl&with" + EmailVerifyPortletKeys.ADMIN_EMAIL_VERIFY + "andSomeOther" + EmailVerifyPortletKeys.ADMIN_EMAIL_VERIFY + "andOtherStuff";
		long userId = 123;
		when(mockPortal.getLiferayPortletResponse(mockPortletResponse)).thenReturn(mockLiferayPortletResponse);
		when(mockLiferayPortletResponse.createActionURL()).thenReturn(mockActionURL);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockActionURL.toString()).thenReturn(urlValue);

		String result = emailVerifyUrlService.getVerifyEmailURL(mockPortletResponse, mockUser);

		assertThat(result, equalTo(expectedValue));
		InOrder inOrder = inOrder(mockActionURL);
		inOrder.verify(mockActionURL, times(1)).setParameter("p_p_id", EmailVerifyPortletKeys.ADMIN_EMAIL_VERIFY);
		inOrder.verify(mockActionURL, times(1)).setParameter(ActionRequest.ACTION_NAME, EmailVerifyMVCCommandKeys.VERIFY);
		inOrder.verify(mockActionURL, times(1)).setParameter("selectedUserId", String.valueOf(userId));
	}

}
