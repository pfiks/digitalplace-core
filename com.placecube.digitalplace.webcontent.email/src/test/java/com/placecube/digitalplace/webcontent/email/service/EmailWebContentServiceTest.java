package com.placecube.digitalplace.webcontent.email.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.webcontent.email.constants.EmailConstants;
import com.placecube.digitalplace.webcontent.email.constants.EmailDDMTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.service.DDMInitializer;

@RunWith(PowerMockRunner.class)
public class EmailWebContentServiceTest extends PowerMockito {

	private static final long STRUCTURE_ID = 1L;

	@Mock
	private DDMInitializer mockDDMInitializer;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateContext mockDDMTemplateContext;

	@InjectMocks
	private EmailWebContentService emailContentService;

	@Mock
	private ServiceContext mockServiceContext;

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenErrorGettingOrCreatingStructure_ThenThrowPortalException() throws PortalException {
		when(mockDDMInitializer.getOrCreateDDMStructure(EmailConstants.STRUCTURE_KEY, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/structure.xml", null, getClass().getClassLoader(), JournalArticle.class, mockServiceContext, true)).thenThrow(new PortalException());

		emailContentService.getOrCreateDDMStructure(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenErrorGettingOrCreatingTemplate_ThenThrowPortalException() throws PortalException {
		when(mockDDMInitializer.getOrCreateDDMStructure(EmailConstants.STRUCTURE_KEY, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/structure.xml", null, getClass().getClassLoader(), JournalArticle.class, mockServiceContext, true)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);

		when(mockDDMInitializer.getDDMTemplateContextForWebContent(EmailDDMTemplate.EMAIL.getName(), EmailDDMTemplate.EMAIL.getKey(), STRUCTURE_ID, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/" + EmailConstants.TEMPLATE_FILE_NAME, getClass().getClassLoader(), mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext, true)).thenThrow(new PortalException());
		emailContentService.getOrCreateDDMStructure(mockServiceContext);
	}

	@Test
	public void getOrCreateDDMStructure_WhenNoError_ThenGetOrCreateDDMTemplateAndReturnDDMStructure() throws PortalException {
		when(mockDDMInitializer.getOrCreateDDMStructure(EmailConstants.STRUCTURE_KEY, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/structure.xml", null, getClass().getClassLoader(), JournalArticle.class, mockServiceContext, true)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);

		when(mockDDMInitializer.getDDMTemplateContextForWebContent(EmailDDMTemplate.EMAIL.getName(), EmailDDMTemplate.EMAIL.getKey(), STRUCTURE_ID, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/" + EmailConstants.TEMPLATE_FILE_NAME, getClass().getClassLoader(), mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext, true)).thenReturn(mockDDMTemplate);
		DDMStructure result = emailContentService.getOrCreateDDMStructure(mockServiceContext);
		assertThat(result, sameInstance(mockDDMStructure));
		verify(mockDDMInitializer, times(1)).getOrCreateDDMTemplate(mockDDMTemplateContext, true);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMTemplate_WhenErrorGettingOrCreatingStructure_ThenThrowPortalException() throws PortalException {
		when(mockDDMInitializer.getOrCreateDDMStructure(EmailConstants.STRUCTURE_KEY, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/structure.xml", null, getClass().getClassLoader(), JournalArticle.class, mockServiceContext, true)).thenThrow(new PortalException());
		emailContentService.getOrCreateDDMTemplate(mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMTemplate_WhenErrorGettingOrCreatingTemplate_ThenThrowPortalException() throws PortalException {
		when(mockDDMInitializer.getOrCreateDDMStructure(EmailConstants.STRUCTURE_KEY, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/structure.xml", null, getClass().getClassLoader(), JournalArticle.class, mockServiceContext, true)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);

		when(mockDDMInitializer.getDDMTemplateContextForWebContent(EmailDDMTemplate.EMAIL.getName(), EmailDDMTemplate.EMAIL.getKey(), STRUCTURE_ID, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/" + EmailConstants.TEMPLATE_FILE_NAME, getClass().getClassLoader(), mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext, true)).thenThrow(new PortalException());
		emailContentService.getOrCreateDDMTemplate(mockServiceContext);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenNoError_ThenGetORCreateDDMStructureAndReturnDDMTemplate() throws Exception {
		when(mockDDMInitializer.getOrCreateDDMStructure(EmailConstants.STRUCTURE_KEY, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/structure.xml", null, getClass().getClassLoader(), JournalArticle.class, mockServiceContext, true)).thenReturn(mockDDMStructure);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);

		when(mockDDMInitializer.getDDMTemplateContextForWebContent(EmailDDMTemplate.EMAIL.getName(), EmailDDMTemplate.EMAIL.getKey(), STRUCTURE_ID, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/" + EmailConstants.TEMPLATE_FILE_NAME, getClass().getClassLoader(), mockServiceContext)).thenReturn(mockDDMTemplateContext);
		when(mockDDMInitializer.getOrCreateDDMTemplate(mockDDMTemplateContext, true)).thenReturn(mockDDMTemplate);
		DDMTemplate result = emailContentService.getOrCreateDDMTemplate(mockServiceContext);
		assertThat(result, sameInstance(mockDDMTemplate));
		verify(mockDDMInitializer, times(1)).getOrCreateDDMStructure(EmailConstants.STRUCTURE_KEY, "com/placecube/digitalplace/webcontent/email/dependencies/ddm/structure.xml", null, getClass().getClassLoader(), JournalArticle.class, mockServiceContext, true);
	}

}
