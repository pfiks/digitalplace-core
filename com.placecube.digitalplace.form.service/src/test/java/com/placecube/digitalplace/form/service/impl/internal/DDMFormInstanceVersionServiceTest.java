package com.placecube.digitalplace.form.service.impl.internal;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceVersion;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureVersion;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceVersionLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

public class DDMFormInstanceVersionServiceTest {

	private static final String VERSION = "1.0";

	private static final long DDM_STRUCTURE_VERSION_ID = 11;

	private static final long DDM_FORM_INSTANCE_VERSION_ID = 22;

	@InjectMocks
	private DDMFormInstanceVersionService ddmFormInstanceVersionService;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private DDMFormInstanceVersionLocalService mockDDMFormInstanceVersionLocalService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private User mockUser;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private DDMFormInstance mockDDMFormInstance;

	@Mock
	private DDMStructureVersion mockDDMStructureVersion;

	@Mock
	private DDMFormInstanceVersion mockDDMFormInstanceVersion;

	@Before
	public void setUp() {

		initMocks(this);

	}

	@Test(expected = PortalException.class)
	public void addFormInstanceVersion_WhenErrorGettingStructureVersion_ThenPortalExceptionIsThrown() throws Exception {

		when(mockDDMStructure.getStructureVersion()).thenThrow(new PortalException());

		ddmFormInstanceVersionService.addFormInstanceVersion(mockDDMStructure, mockUser, mockDDMFormInstance, VERSION, mockServiceContext);
	}

	@Test
	public void addFormInstanceVersion_WhenNoError_ThenAddDDMFormInstanceVersion() throws Exception {

		when(mockDDMStructure.getStructureVersion()).thenReturn(mockDDMStructureVersion);
		when(mockDDMStructureVersion.getStructureVersionId()).thenReturn(DDM_STRUCTURE_VERSION_ID);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_VERSION_ID);
		when(mockDDMFormInstanceVersionLocalService.createDDMFormInstanceVersion(DDM_FORM_INSTANCE_VERSION_ID)).thenReturn(mockDDMFormInstanceVersion);

		ddmFormInstanceVersionService.addFormInstanceVersion(mockDDMStructure, mockUser, mockDDMFormInstance, VERSION, mockServiceContext);

		verify(mockDDMFormInstanceVersionLocalService, times(1)).updateDDMFormInstanceVersion(mockDDMFormInstanceVersion);
	}

	@Test
	public void addFormInstanceVersion_WhenNoError_ThenSetsValues() throws Exception {

		long groupId = 1;
		long companyId = 2;
		long userId = 3;
		String userName = "userName";
		Date modifiedDate = new Date();
		long formInstanceId = 4;
		String name = "name";
		String description = "description";
		String fullName = "fullName";

		when(mockDDMStructure.getStructureVersion()).thenReturn(mockDDMStructureVersion);
		when(mockDDMStructureVersion.getStructureVersionId()).thenReturn(DDM_STRUCTURE_VERSION_ID);
		when(mockCounterLocalService.increment()).thenReturn(DDM_FORM_INSTANCE_VERSION_ID);
		when(mockDDMFormInstanceVersionLocalService.createDDMFormInstanceVersion(DDM_FORM_INSTANCE_VERSION_ID)).thenReturn(mockDDMFormInstanceVersion);
		when(mockDDMFormInstance.getGroupId()).thenReturn(groupId);
		when(mockDDMFormInstance.getCompanyId()).thenReturn(companyId);
		when(mockDDMFormInstance.getUserId()).thenReturn(userId);
		when(mockDDMFormInstance.getUserName()).thenReturn(userName);
		when(mockDDMFormInstance.getModifiedDate()).thenReturn(modifiedDate);
		when(mockDDMFormInstance.getFormInstanceId()).thenReturn(formInstanceId);
		when(mockDDMFormInstance.getName()).thenReturn(name);
		when(mockDDMFormInstance.getDescription()).thenReturn(description);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getFullName()).thenReturn(fullName);
		when(mockDDMFormInstance.getModifiedDate()).thenReturn(modifiedDate);
		when(mockServiceContext.getAttribute("status")).thenReturn(WorkflowConstants.STATUS_APPROVED);

		ddmFormInstanceVersionService.addFormInstanceVersion(mockDDMStructure, mockUser, mockDDMFormInstance, VERSION, mockServiceContext);

		InOrder inOrder = Mockito.inOrder(mockDDMFormInstanceVersion, mockDDMFormInstanceVersionLocalService);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setGroupId(groupId);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setCompanyId(companyId);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setUserId(userId);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setUserName(userName);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setCreateDate(modifiedDate);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setFormInstanceId(formInstanceId);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setStructureVersionId(DDM_STRUCTURE_VERSION_ID);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setName(name);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setDescription(description);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setVersion(VERSION);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setStatus(WorkflowConstants.STATUS_APPROVED);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setStatusByUserId(userId);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setStatusByUserName(fullName);
		inOrder.verify(mockDDMFormInstanceVersion, times(1)).setStatusDate(modifiedDate);
		inOrder.verify(mockDDMFormInstanceVersionLocalService, times(1)).updateDDMFormInstanceVersion(mockDDMFormInstanceVersion);

	}
}
