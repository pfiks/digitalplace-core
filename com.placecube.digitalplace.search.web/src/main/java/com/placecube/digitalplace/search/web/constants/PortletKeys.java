package com.placecube.digitalplace.search.web.constants;

public final class PortletKeys {

	public static final String ACTIVE_FILTERS = "com_placecube_digitalplace_search_web_ActiveFiltersPortlet";

	public static final String ADVANCE_PEOPLE_SEARCH = "com_placecube_digitalplace_search_web_AdvancePeopleSearchPortlet";

	public static final String CATEGORY_FILTER = "com_placecube_digitalplace_search_web_CategoryFacetPortlet";

	public static final String CONTENT_TYPE_FACET = "com_placecube_digitalplace_search_web_ContentTypeFacetPortlet";

	public static final String DISPLAY_STYLE = "com_placecube_digitalplace_search_web_SearchDisplayStylePortlet";

	public static final String EXPIRED_CONTENT_FILTER_SEARCH = "com_placecube_digitalplace_search_web_ExpiredContentFilterSearchPortlet";

	public static final String SEARCH_BAR = "com_placecube_digitalplace_search_web_SearchBarPortlet";

	public static final String SORT_BY = "com_placecube_digitalplace_search_web_SortByPortlet";

	public static final String TAG_FILTER = "com_placecube_digitalplace_search_web_TagFacetPortlet";

	private PortletKeys() {
	}
}
