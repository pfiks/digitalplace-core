package com.placecube.digitalplace.fileupload.liferaydl.service;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portletfilerepository.PortletFileRepositoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.placecube.digitalplace.fileupload.exception.FileUploadException;
import com.placecube.digitalplace.fileupload.service.FileUploadConnector;

@Component(immediate = true, service = FileUploadConnector.class)
public class LiferayDLConnector implements FileUploadConnector {

	private static final Log LOG = LogFactoryUtil.getLog(LiferayDLConnector.class);

	@Reference
	private DLAppLocalService dlAppLocalService;

	@Reference
	private LiferayDLService liferayDLService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public String getId() {
		return "LIFERAYDL";
	}

	@Override
	public boolean isDefault(long companyId) {
		try {
			return liferayDLService.isDefault(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public boolean isEnabled(long companyId) {
		try {
			return liferayDLService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public void uploadFile(String fileName, String path, InputStream inputStream, ServiceContext serviceContext) throws FileUploadException {
		try {
			setServiceContextPermissions(serviceContext);

			long userId = serviceContext.getUserId();
			long repositoryId = serviceContext.getScopeGroupId();
			long folderId = liferayDLService.getFolderId(path, serviceContext);
			String sourceFileName = PortletFileRepositoryUtil.getUniqueFileName(serviceContext.getScopeGroupId(), folderId, fileName);
			String mimeType = MimeTypesUtil.getContentType(fileName);
			String title = sourceFileName;
			String description = StringPool.BLANK;
			String changeLog = null;
			long size = 0;

			dlAppLocalService.addFileEntry(null, userId, repositoryId, folderId, sourceFileName, mimeType, title, null, description, changeLog, inputStream, size, null, null, null, serviceContext);

		} catch (PortalException e) {
			throw new FileUploadException(e);
		}
	}

	private void setServiceContextPermissions(ServiceContext serviceContext) {
		serviceContext.setAddGroupPermissions(false);
		serviceContext.setAddGuestPermissions(false);
	}

}
