<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="../init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post">

	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />


	<liferay-frontend:edit-form-body>
		<dp-frontend:fieldset-group>

            <liferay-frontend:fieldset>

				<aui:input type="number" label="maximum-items-to-display" name="maxTagsToShow" value="${maxTagsToShow}" required="true">
					<aui:validator name="min">1</aui:validator>
				</aui:input>

				<aui:input
					label="filter-colour-label"
					name="filterColour"
					type="text"
					value="${ filterColour }"
				/>

				<aui:input cssClass="mr-1" type="checkbox" label="all-selected-tags-present-in-search-results" name="tagsQueryAnded" value="${tagsQueryAnded}"/>

            </liferay-frontend:fieldset>

		</dp-frontend:fieldset-group>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>

