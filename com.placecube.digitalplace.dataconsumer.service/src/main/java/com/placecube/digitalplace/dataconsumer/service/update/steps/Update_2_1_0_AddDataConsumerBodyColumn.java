package com.placecube.digitalplace.dataconsumer.service.update.steps;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class Update_2_1_0_AddDataConsumerBodyColumn extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {
		runSQLTemplateString("alter table Placecube_DataConsumer_DataConsumer add column body STRING null;", false);
	}
}
