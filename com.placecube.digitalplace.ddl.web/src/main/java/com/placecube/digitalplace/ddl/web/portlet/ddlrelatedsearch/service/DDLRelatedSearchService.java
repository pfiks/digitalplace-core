package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.search.document.Document;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.SearchRequestBuilderFactory;
import com.placecube.digitalplace.ddl.model.DynamicDataListMapping;
import com.placecube.digitalplace.ddl.service.DynamicDataListConfigurationService;
import com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util.DDLRelatedSearchQueryUtil;
import com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util.DDLRelatedSearchUtil;

@Component(immediate = true, service = DDLRelatedSearchService.class)
public class DDLRelatedSearchService {

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private DDLRelatedSearchQueryUtil ddlRelatedSearchQueryUtil;

	@Reference
	private DDLRelatedSearchUtil ddlRelatedSearchUtil;

	@Reference
	private DynamicDataListConfigurationService dynamicDataListConfigurationService;

	@Reference
	private Portal portal;

	@Reference
	private Queries queries;

	@Reference
	private SearchRequestBuilderFactory searchRequestBuilderFactory;

	public List<Object> getDDLDisplayEntryResults(List<Document> documents, ThemeDisplay themeDisplay, HttpServletRequest httpServletRequest) {
		List<Object> results = new LinkedList<>();
		List<DynamicDataListMapping> dynamicDataListMappings = dynamicDataListConfigurationService.getDynamicDataListMappings(themeDisplay.getCompanyId());

		for (Document document : documents) {
			long ddlRecordId = document.getLong(Field.ENTRY_CLASS_PK);
			Optional<String> templateStringOptional = ddlRelatedSearchUtil.getDDLRecordTemplateString(ddlRecordId, dynamicDataListMappings, themeDisplay, httpServletRequest);

			templateStringOptional.ifPresent(templateString -> results.add(ddlRelatedSearchUtil.createDDLDisplayEntry(ddlRecordId, templateString)));
		}
		return results;
	}

	public List<Document> getSearchResultDocuments(HttpServletRequest httpServletRequest, DDLRecord ddlRecord, int maxItems) throws PortalException {

		long companyId = portal.getCompanyId(httpServletRequest);
		String recordTitle = ddlRelatedSearchUtil.getDDLRecordTitle(ddlRecord);
		AssetEntry assetEntry = assetEntryLocalService.fetchEntry(DDLRecord.class.getName(), ddlRecord.getPrimaryKey());

		SearchRequestBuilder searchRequestBuilder = searchRequestBuilderFactory.builder();
		searchRequestBuilder.emptySearchEnabled(true);
		ddlRelatedSearchQueryUtil.configureSearchContext(searchRequestBuilder, companyId);

		BooleanQuery booleanQuery = queries.booleanQuery();

		ddlRelatedSearchQueryUtil.configureRecordAndStructureId(booleanQuery, ddlRecord);
		ddlRelatedSearchQueryUtil.configureKeywordsFromTitle(booleanQuery, recordTitle);
		if (Validator.isNotNull(assetEntry)) {
			ddlRelatedSearchQueryUtil.configureAssetTagIds(booleanQuery, assetEntry);
			ddlRelatedSearchQueryUtil.configureCategoryIds(booleanQuery, assetEntry);
		}

		return ddlRelatedSearchQueryUtil.getResultsFromSearchBuilder(searchRequestBuilder, booleanQuery, maxItems);
	}

	public SearchContainer<Object> getSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse, List<Object> results) {
		return new SearchContainer<>(renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM, 0, results.size(), renderResponse.createRenderURL(), null, "no-entries-were-found");
	}

}
