package com.placecube.digitalplace.ddl.web.portlet.display;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLEditorService;

@Component(immediate = true, property = { //
		"javax.portlet.name=" + PortletKeys.DDL_DISPLAY_PORTLET, //
		"mvc.command.name=" + MVCCommandKeys.UPDATE_ENTRY //
}, service = MVCActionCommand.class)
public class UpdateDDLEntryMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private DDLEditorService ddlEditorService;

	@Reference
	private DynamicDataListService dynamicDataListService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException, IOException {
		long ddlRecordId = ParamUtil.getLong(actionRequest, "ddlRecordId");
		long ddmStructureId = ParamUtil.getLong(actionRequest, "ddmStructureId");
		long ddlRecordSetId = ParamUtil.getLong(actionRequest, "ddlRecordSetId");
		long ddlTemplateId = ParamUtil.getLong(actionRequest, "ddlTemplateId");

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();

		DDMFormValues ddmFormValues = getFormValues(actionRequest, ddmStructureId);

		ServiceContext serviceContext = ddlEditorService.getServiceContextForUpdate(actionRequest, themeDisplay);
		DDLRecord ddlRecord = ddlEditorService.getOrCreateDDLRecord(ddlRecordId, ddmFormValues, ddlRecordSetId, serviceContext, permissionChecker);

		String redirect = dynamicDataListService.getViewDDLRecordURL(themeDisplay, ddlRecord, ddlTemplateId);
		actionResponse.sendRedirect(redirect);
	}

	private DDMFormValues getFormValues(ActionRequest actionRequest, long ddmStructureId) throws PortalException {
		long ddmFormTemplateId = ParamUtil.getLong(actionRequest, "ddmFormTemplateId");
		String serializedDDMFormValues = ParamUtil.getString(actionRequest, "ddmFormValues");
		DDMForm ddmForm = ddlEditorService.getDDMFormForUpdate(ddmStructureId, ddmFormTemplateId);
		return ddlEditorService.getDDMFormValuesFromDeserialisedContent(serializedDDMFormValues, ddmForm);
	}

}
