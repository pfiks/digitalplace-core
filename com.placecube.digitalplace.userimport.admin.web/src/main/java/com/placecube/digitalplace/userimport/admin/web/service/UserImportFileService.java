package com.placecube.digitalplace.userimport.admin.web.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.antivirus.AntivirusScannerException;
import com.liferay.document.library.kernel.antivirus.AntivirusScannerUtil;
import com.liferay.document.library.kernel.exception.FileExtensionException;
import com.liferay.document.library.kernel.exception.FileSizeException;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.petra.string.CharPool;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = UserImportFileService.class)
public class UserImportFileService {

	private static final Log LOG = LogFactoryUtil.getLog(UserImportFileService.class);

	@Reference
	private DLAppLocalService dlAppLocalService;

	@Reference
	private JSONFactory jsonFactory;

	public void deleteFileEntry(long fileEntryId) {
		try {
			dlAppLocalService.deleteFileEntry(fileEntryId);
		} catch (Exception e) {
			LOG.error("Unable to delete user data import file", e);
		}
	}

	public String getFileExtension(File file) {
		return FileUtil.getExtension(file.getName());
	}

	public boolean isCSVFile(String extension) {
		return "csv".equalsIgnoreCase(extension);
	}

	public boolean isJSONFile(String extension) {
		return "json".equalsIgnoreCase(extension);
	}

	public InputStream loadFileEntryContent(long fileEntryId) throws PortalException {
		FileEntry fileEntry = dlAppLocalService.getFileEntry(fileEntryId);
		return fileEntry.getContentStream();
	}

	public JSONArray readCSVFile(InputStream inputStream) throws PortalException {
		try {
			JSONArray csvDataArray = new JSONArray();
			CSVFormat csvFormat = CSVFormat.newFormat(CharPool.COMMA).withQuote(CharPool.QUOTE).withIgnoreEmptyLines().withTrim(true);
			CSVParser csvParser = csvFormat.parse(new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)));
			if (Validator.isNotNull(csvParser)) {
				int headersRowIndex = 0;
				List<String> headers = new ArrayList<>();
				JSONObject userObject = null;
				for (CSVRecord csvRecord : csvParser) {
					userObject = new JSONObject();
					for (int index = 0; index < csvRecord.size(); index++) {
						if (headersRowIndex == 0) {
							headers.add(csvRecord.get(index));
						} else {
							userObject.put(headers.get(index), csvRecord.get(index));
						}
					}
					if (headersRowIndex > 0) {
						csvDataArray.add(userObject);
					}
					headersRowIndex++;
				}
			}
			return csvDataArray;

		} catch (Exception e) {
			throw new PortalException("Unable to parse CSV file", e);
		}
	}

	public JSONArray readJSONFile(InputStream inputStream) throws PortalException {

		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)));
			return (JSONArray) obj;

		} catch (Exception e) {
			throw new PortalException("Unable to parse JSON file", e);
		}
	}

	public long saveFileAsFileEntry(File file, ServiceContext serviceContext) throws PortalException {
		long folderId = 0;
		String mimeType = MimeTypesUtil.getContentType(file);
		String sourceFileName = file.getName();
		String title = "UserDataImport " + sourceFileName;
		return dlAppLocalService.addFileEntry(StringPool.BLANK, serviceContext.getUserId(), serviceContext.getScopeGroupId(), folderId, sourceFileName, mimeType, title, StringPool.BLANK,
				StringPool.BLANK, StringPool.BLANK, file, null, null, null, serviceContext).getFileEntryId();
	}

	public void validateFile(File file) throws AntivirusScannerException, FileSizeException, FileExtensionException, JSONException {
		if (Validator.isNull(file)) {
			throw new FileSizeException();
		} else {
			AntivirusScannerUtil.scan(file);
			String extension = getFileExtension(file);
			if (Validator.isNull(extension) || !isValidExtension(extension)) {
				throw new FileExtensionException();
			}

			if (isJSONFile(extension)) {
				// validates JSON file
				try {
					readJSONFile(new FileInputStream(file));
				} catch (Exception e) {
					throw new JSONException("Error parsing the JSON file", e);
				}
			}
		}
	}

	private boolean isValidExtension(String extension) {
		return isJSONFile(extension) || isCSVFile(extension);
	}

}
