package com.placecube.digitalplace.payment.govuk.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.payment.govuk.configuration.GovUKCompanyConfiguration", localization = "content/Language", name = "payment-govuk")
public interface GovUKCompanyConfiguration {

	@Meta.AD(required = false, deflt = "", name = "additional-api-auth-key", description = "additional-api-auth-key-description")
	String[] additionalApiAuthKeys();

	@Meta.AD(required = false, deflt = "", name = "additional-api-message-verification-secret-key", description = "additional-api-message-verification-secret-key-description")
	String[] additionalApiMessageVerificationSecretKeys();

	@Meta.AD(required = false, deflt = "AUTH_KEY", name = "api-auth-key")
	String apiAuthKey();

	@Meta.AD(required = false, deflt = "AUTH_KEY", name = "api-message-verification-secret-key", description = "api-message-verification-secret-key-description")
	String apiMessageVerificationSecretKey();

	@Meta.AD(required = false, deflt = "https://publicapi.payments.service.gov.uk", name = "base-url-name", description = "base-url-description")
	String baseURL();

	@Meta.AD(required = false, deflt = "/v1/agreements", name = "create-agreement-url-name", description = "create-agreement-url-description")
	String createAgreementURL();

	@Meta.AD(required = false, deflt = "/v1/payments", name = "create-payment-url-name", description = "create-payment-url-description")
	String createPaymentURL();

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "/v1/agreements/{AGREEMENT_ID}/cancel", name = "agreement-cancel-url-name", description = "agreement-cancel-url-description")
	String getAgreementCancelURL();

	@Meta.AD(required = false, deflt = "/v1/agreements/{AGREEMENT_ID}", name = "agreement-status-url-name", description = "agreement-status-url-description")
	String getAgreementStatusURL();

	@Meta.AD(required = false, deflt = "/v1/payments/{PAYMENT_ID}", name = "payment-status-url-name", description = "payment-status-url-description")
	String getPaymentStatusURL();
}
