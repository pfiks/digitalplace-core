AUI.add(

		'com-placecube-digitalplace-user-settings-web',

		function(A) {

			var UserSettingsWeb = {

					executeActionOnChange: function(fieldSelector, paramName, resourceId) {
						this.executeActionOnChangeWithDataAttribute(fieldSelector, '', paramName, resourceId);
					},
					
					executeActionOnChangeWithDataAttribute: function(fieldSelector, dataAttributeName, paramName, resourceId) {
						try{
							$(fieldSelector).on('change', function(e) {

								var resourceURL = Liferay.PortletURL.createResourceURL();
								resourceURL.setPortletId('com_placecube_digitalplace_user_settings_UserSettingsPortlet');
								resourceURL.setResourceId(resourceId);

								if (this.type === 'checkbox') {
									resourceURL.setParameter(paramName, this.checked);
								} else {
									resourceURL.setParameter(paramName, this.value);
								}

								if (dataAttributeName) {
									var selectedData = $(this).attr('data-' + dataAttributeName);
									resourceURL.setParameter(dataAttributeName, selectedData);
								}

								$.get(
									resourceURL.toString(),
									{},
									function (data) {
										var parsedData = JSON.parse(data);
										var redirectURL = parsedData.redirect;
										if (redirectURL) {
											window.location.href = redirectURL;
										}
									}
								);

							});
						} catch(e) {
							console.log(e);
						}
					},
					
					disableCopyAndPasteOnField: function(params) {
						var fieldId = params.fieldId;
						try{
							document.getElementById(fieldId).onpaste = function(){
								return false;
							};
						} catch(e){
							console.log(e);
						}
					}
			};

			A.UserSettingsWeb = UserSettingsWeb;
		},
		'',
		{
			requires: [
				'aui-base',
				'liferay-portlet-url'
				]
		}

);

