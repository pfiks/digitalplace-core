package com.placecube.digitalplace.formadmin.web.portlet.command;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.service.DataConsumerLocalService;
import com.placecube.digitalplace.formadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.FORMS_MANAGER, "mvc.command.name=" + MVCCommandKeys.DATA_CONSUMER_DELETE }, service = MVCActionCommand.class)
public class DataConsumerDeleteMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private DataConsumerLocalService dataConsumerLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long formInstanceId = ParamUtil.getLong(actionRequest, PortletRequestKeys.FORM_INSTANCE_ID);
		int chain = ParamUtil.getInteger(actionRequest, PortletRequestKeys.CHAIN);

		List<DataConsumer> consumersForForm = dataConsumerLocalService.getDataConsumersByDDMFormInstanceId(formInstanceId);
		for (DataConsumer consumer : consumersForForm) {
			if (consumer.getChain() >= chain) {
				dataConsumerLocalService.deleteDataConsumer(consumer.getDataConsumerId());
			}
		}

		actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.SELECT_DATA_PROVIDER);
	}

}
