package com.placecube.digitalplace.template.contributor.freemarker;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.document.library.util.DLURLHelper;
import com.liferay.document.library.video.external.shortcut.resolver.DLVideoExternalShortcutResolver;
import com.liferay.portal.kernel.language.Language;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.address.service.AddressLookupService;
import com.placecube.digitalplace.category.service.CategoryRetrievalService;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.journal.service.JournalArticleRetrievalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceContextThreadLocal.class })
public class DigitalPlaceFreemarkerTemplateContextContributorTest {

	@InjectMocks
	private DigitalPlaceFreemarkerTemplateContextContributor digitalPlaceFreemarkerTemplateContextContributor;

	@Mock
	private AddressLookupService mockAddressLookupService;

	@Mock
	private CategoryRetrievalService mockCategoryRetrievalService;

	@Mock
	private Map<String, Object> mockContextObjects;

	@Mock
	private DLURLHelper mockDLURLHelper;

	@Mock
	private DLVideoExternalShortcutResolver mockDLVideoExternalShortcutResolver;

	@Mock
	private DynamicDataListService mockDynamicDataListService;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private JournalArticleRetrievalService mockJournalArticleRetrievalService;

	@Mock
	private Language mockLanguage;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetup() {
		mockStatic(ServiceContextThreadLocal.class);
	}

	@Test
	public void prepare_WhenNoErrors_ThenPutsDigitalPlaceCoreServicesAndConstantsInContext() {
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);

		digitalPlaceFreemarkerTemplateContextContributor.prepare(mockContextObjects, mockHttpServletRequest);

		verify(mockContextObjects, times(1)).put("digitalplace_addressLookupService", mockAddressLookupService);
		verify(mockContextObjects, times(1)).put("digitalplace_dlUrlHelper", mockDLURLHelper);
		verify(mockContextObjects, times(1)).put("digitalplace_dlVideoExternalShortcutResolver", mockDLVideoExternalShortcutResolver);
		verify(mockContextObjects, times(1)).put("digitalplace_journalArticleRetrievalService", mockJournalArticleRetrievalService);
		verify(mockContextObjects, times(1)).put("digitalplace_categoryRetrievalService", mockCategoryRetrievalService);
		verify(mockContextObjects, times(1)).put("digitalplace_dynamicDataListService", mockDynamicDataListService);
		verify(mockContextObjects, times(1)).put("digitalplace_language", mockLanguage);
		verify(mockContextObjects, times(1)).put("digitalplace_serviceContext", mockServiceContext);
		verify(mockContextObjects, times(1)).put("digitalplace_constantsView", Constants.VIEW);
		verify(mockContextObjects, times(1)).put("digitalplace_workflowStatusAny", WorkflowConstants.STATUS_ANY);
		verify(mockContextObjects, times(1)).put("digitalplace_workflowStatusApproved", WorkflowConstants.STATUS_APPROVED);
	}

}
