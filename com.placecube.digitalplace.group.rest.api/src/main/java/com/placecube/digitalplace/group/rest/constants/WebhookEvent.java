package com.placecube.digitalplace.group.rest.constants;

import java.util.Arrays;
import java.util.Optional;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.StringUtil;

public enum WebhookEvent {

	DDL_RECORD(DDLRecordSet.class.getName(), new WebhookAction[] { WebhookAction.ADD, WebhookAction.UPDATE, WebhookAction.DELETE });

	private final String className;
	private final WebhookAction[] actions;

	private WebhookEvent(String className, WebhookAction[] actions) {
		this.className = className;
		this.actions = actions;
	}

	public String getClassName() {
		return className;
	}

	public WebhookAction[] getActions() {
		return actions;
	}

	public String getLabel() {
		return StringUtil.extractLast(className, ".");
	}

	public static WebhookAction[] getActionsForClassName(String className) throws PortalException {
		Optional<WebhookEvent> findFirst = Arrays.stream(WebhookEvent.values()).filter(entry -> entry.getClassName().equals(className)).findFirst();
		if (findFirst.isPresent()) {
			return findFirst.get().getActions();
		}
		throw new PortalException("Classname not managed");
	}

}
