package com.placecube.digitalplace.pageimportadmin.constants;

public final class PortletKeys {

	public static final String PAGE_IMPORT_ADMIN = "com_placecube_digitalplace_pageimportadmin_PageImportAdminPortlet";

	private PortletKeys() {
		return;
	}
}