package com.placecube.digitalplace.address.taglib.servlet.taglib.context;

import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true)
public class ServletContextUtil {

	private static ServletContextUtil instance;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.address.taglib)", unbind = "-")
	protected ServletContext servletContext;

	public static final ServletContext getServletContext() {
		return instance.servletContext;
	}

	@Activate
	protected void activate() {
		instance = this;
	}

	@Deactivate
	protected void deactivate() {
		instance = null;
	}

}