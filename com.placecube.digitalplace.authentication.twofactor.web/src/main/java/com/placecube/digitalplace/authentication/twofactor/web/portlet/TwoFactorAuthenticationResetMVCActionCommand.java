package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorPortletKeys;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property = { "javax.portlet.name=" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION, "mvc.command.name=" + TwoFactorMVCCommandKeys.RESET }, service = MVCActionCommand.class)
public class TwoFactorAuthenticationResetMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private Portal portal;

	@Reference
	private UserTwoFactorAuthenticationLocalService userTwoFactorAuthenticationLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long selectedUserId = ParamUtil.getLong(actionRequest, "selectedUserId");
		long companyId = portal.getCompanyId(actionRequest);

		userTwoFactorAuthenticationLocalService.resetUserTwoFactorAuthentication(companyId, selectedUserId);

		String successMessage = ParamUtil.getString(actionRequest, "successMessage");

		SessionMessages.add(portal.getHttpServletRequest(actionRequest), "requestProcessed", successMessage);

		LiferayPortletURL usersAdminURL = portal.getLiferayPortletResponse(actionResponse).createLiferayPortletURL(PortletKeys.USERS_ADMIN, PortletRequest.RENDER_PHASE);
		actionResponse.sendRedirect(usersAdminURL.toString());
	}

}
