package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch.sortby;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Locale;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.GeoDistanceSort;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.liferay.portal.kernel.search.filter.GeoDistanceFilter;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.ddl.web.constants.LocationSearchParameters;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AggregatedResourceBundleUtil.class, SortFactoryUtil.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.search.SortFactoryUtil")
public class SortByGeoSpatialTest extends PowerMockito {

	private static final String LATITUDE = "10.012";
	private static final String LONGITUDE = "-2.015";
	private static final String SEARCH_FIELD = "SearchField";

	@Mock
	private BooleanQuery mockBooleanQuery;

	@Mock
	private GeoDistanceFilter mockGeoDistanceFilter;

	@Mock
	private GeoDistanceSort mockGeoDistanceSort;

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@InjectMocks
	private SortByGeoSpatial sortByGeoSpatial;

	@Test
	public void getId_WhenNoError_ThenReturnsTheId() {
		String result = sortByGeoSpatial.getId();

		assertThat(result, equalTo("sortByGeoSpatial"));
	}

	@Test
	public void getLabel_WhenNoError_ThenReturnsLabel() {
		String expected = "exp";
		Locale locale = Locale.UK;
		when(AggregatedResourceBundleUtil.get("sort-by-geo-spatial", locale, "com.placecube.digitalplace.ddl.web")).thenReturn(expected);

		String result = sortByGeoSpatial.getLabel(locale);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSorts_WhenFieldNameNotFoundInSession_ThenReturnsEmptySortArray() {

		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("latitude")).thenReturn(Optional.of(LATITUDE));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("longitude")).thenReturn(Optional.of(LONGITUDE));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("geoLocationField")).thenReturn(Optional.empty());

		Sort[] sorts = sortByGeoSpatial.getSorts(mockSharedSearchContributorSettings);

		assertThat(sorts, Matchers.emptyArray());
	}

	@Test
	public void getSorts_WhenLatitudeAndLongitudeAndFieldNameFoundInSessionAndSharedSearchContributorSettingsIsPresent_ThenReturnsGeoSort() {

		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("latitude")).thenReturn(Optional.of(LATITUDE));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("longitude")).thenReturn(Optional.of(LONGITUDE));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("geoLocationField")).thenReturn(Optional.of(SEARCH_FIELD));

		Sort[] sorts = sortByGeoSpatial.getSorts(mockSharedSearchContributorSettings);

		GeoDistanceSort geoSort = (GeoDistanceSort) sorts[0];
		assertEquals(SEARCH_FIELD, geoSort.getFieldName().toString());
		assertEquals(LATITUDE, String.valueOf(geoSort.getGeoLocationPoints().get(0).getLatitude()));
		assertEquals(LONGITUDE, String.valueOf(geoSort.getGeoLocationPoints().get(0).getLongitude()));

	}

	@Test
	public void getSorts_WhenLatitudeAndLongitudeAndFieldNameNotFoundInSession_ThenReturnsEmptySortArray() {

		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(LocationSearchParameters.SEARCH_LATITUDE)).thenReturn(Optional.empty());
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(LocationSearchParameters.SEARCH_LONGITUDE)).thenReturn(Optional.empty());
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(LocationSearchParameters.GEO_LOCATION_FIELD)).thenReturn(Optional.empty());

		Sort[] sorts = sortByGeoSpatial.getSorts(mockSharedSearchContributorSettings);

		assertThat(sorts, Matchers.emptyArray());
	}

	@Test
	public void getSorts_WhenLatitudeNotFoundInSession_ThenReturnsEmptySortArray() {

		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("latitude")).thenReturn(Optional.empty());
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("longitude")).thenReturn(Optional.of(LONGITUDE));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("geoLocationField")).thenReturn(Optional.of(SEARCH_FIELD));

		Sort[] sorts = sortByGeoSpatial.getSorts(mockSharedSearchContributorSettings);

		assertThat(sorts, Matchers.emptyArray());
	}

	@Test
	public void getSorts_WhenLongitudeNotFoundInSession_ThenReturnsEmptySortArray() {

		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("latitude")).thenReturn(Optional.of(LATITUDE));
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("longitude")).thenReturn(Optional.empty());
		when(mockSharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute("geoLocationField")).thenReturn(Optional.of(SEARCH_FIELD));

		Sort[] sorts = sortByGeoSpatial.getSorts(mockSharedSearchContributorSettings);

		assertThat(sorts, Matchers.emptyArray());
	}

	@Before
	public void setUp() {
		mockStatic(SortFactoryUtil.class, AggregatedResourceBundleUtil.class);

	}

}
