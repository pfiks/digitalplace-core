/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.externalpage.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.externalpage.model.ExternalPage;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for ExternalPage. This utility wraps
 * <code>com.placecube.digitalplace.externalpage.service.impl.ExternalPageLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ExternalPageLocalService
 * @generated
 */
public class ExternalPageLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.externalpage.service.impl.ExternalPageLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the external page to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExternalPageLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param externalPage the external page
	 * @return the external page that was added
	 */
	public static ExternalPage addExternalPage(ExternalPage externalPage) {
		return getService().addExternalPage(externalPage);
	}

	public static ExternalPage addOrUpdateExternalPage(
			long companyId, long groupId, long userId, String url, String title,
			String content, boolean indexContent)
		throws PortalException {

		return getService().addOrUpdateExternalPage(
			companyId, groupId, userId, url, title, content, indexContent);
	}

	/**
	 * Creates a new external page with the primary key. Does not add the external page to the database.
	 *
	 * @param externalPageId the primary key for the new external page
	 * @return the new external page
	 */
	public static ExternalPage createExternalPage(long externalPageId) {
		return getService().createExternalPage(externalPageId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the external page from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExternalPageLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param externalPage the external page
	 * @return the external page that was removed
	 */
	public static ExternalPage deleteExternalPage(ExternalPage externalPage) {
		return getService().deleteExternalPage(externalPage);
	}

	/**
	 * Deletes the external page with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExternalPageLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page that was removed
	 * @throws PortalException if a external page with the primary key could not be found
	 */
	public static ExternalPage deleteExternalPage(long externalPageId)
		throws PortalException {

		return getService().deleteExternalPage(externalPageId);
	}

	public static void deleteExternalPageByPrimaryKey(long externalPageId)
		throws PortalException {

		getService().deleteExternalPageByPrimaryKey(externalPageId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.externalpage.model.impl.ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.externalpage.model.impl.ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ExternalPage fetchExternalPage(long externalPageId) {
		return getService().fetchExternalPage(externalPageId);
	}

	/**
	 * Returns the external page matching the UUID and group.
	 *
	 * @param uuid the external page's UUID
	 * @param groupId the primary key of the group
	 * @return the matching external page, or <code>null</code> if a matching external page could not be found
	 */
	public static ExternalPage fetchExternalPageByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchExternalPageByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	/**
	 * Returns the external page with the primary key.
	 *
	 * @param externalPageId the primary key of the external page
	 * @return the external page
	 * @throws PortalException if a external page with the primary key could not be found
	 */
	public static ExternalPage getExternalPage(long externalPageId)
		throws PortalException {

		return getService().getExternalPage(externalPageId);
	}

	/**
	 * Returns the external page matching the UUID and group.
	 *
	 * @param uuid the external page's UUID
	 * @param groupId the primary key of the group
	 * @return the matching external page
	 * @throws PortalException if a matching external page could not be found
	 */
	public static ExternalPage getExternalPageByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getExternalPageByUuidAndGroupId(uuid, groupId);
	}

	public static java.util.Optional<ExternalPage> getExternalPageImport(
		long externalPageImportId) {

		return getService().getExternalPageImport(externalPageImportId);
	}

	/**
	 * Returns a range of all the external pages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.externalpage.model.impl.ExternalPageModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @return the range of external pages
	 */
	public static List<ExternalPage> getExternalPages(int start, int end) {
		return getService().getExternalPages(start, end);
	}

	public static List<ExternalPage> getExternalPages(
		long groupId, int start, int end) {

		return getService().getExternalPages(groupId, start, end);
	}

	/**
	 * Returns all the external pages matching the UUID and company.
	 *
	 * @param uuid the UUID of the external pages
	 * @param companyId the primary key of the company
	 * @return the matching external pages, or an empty list if no matches were found
	 */
	public static List<ExternalPage> getExternalPagesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getExternalPagesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of external pages matching the UUID and company.
	 *
	 * @param uuid the UUID of the external pages
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of external pages
	 * @param end the upper bound of the range of external pages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching external pages, or an empty list if no matches were found
	 */
	public static List<ExternalPage> getExternalPagesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ExternalPage> orderByComparator) {

		return getService().getExternalPagesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of external pages.
	 *
	 * @return the number of external pages
	 */
	public static int getExternalPagesCount() {
		return getService().getExternalPagesCount();
	}

	public static int getExternalPagesCount(long groupId) {
		return getService().getExternalPagesCount(groupId);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static ExternalPage publishExternalPage(
		ExternalPage externalPage, boolean status) {

		return getService().publishExternalPage(externalPage, status);
	}

	public static void removeExternalPagesByGroupId(long groupId) {
		getService().removeExternalPagesByGroupId(groupId);
	}

	public static ExternalPage unPublishExternalPage(
		ExternalPage externalPage, boolean status) {

		return getService().unPublishExternalPage(externalPage, status);
	}

	/**
	 * Updates the external page in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExternalPageLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param externalPage the external page
	 * @return the external page that was updated
	 */
	public static ExternalPage updateExternalPage(ExternalPage externalPage) {
		return getService().updateExternalPage(externalPage);
	}

	public static ExternalPageLocalService getService() {
		return _service;
	}

	public static void setService(ExternalPageLocalService service) {
		_service = service;
	}

	private static volatile ExternalPageLocalService _service;

}