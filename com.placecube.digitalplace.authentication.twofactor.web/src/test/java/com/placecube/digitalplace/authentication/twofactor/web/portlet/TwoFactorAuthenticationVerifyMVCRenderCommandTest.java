package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorRequestKeys;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SessionErrors.class })
public class TwoFactorAuthenticationVerifyMVCRenderCommandTest extends PowerMockito {

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private TwoFactorRequestUtils mockTwoFactorRequestUtils;

	@Mock
	private TwoFactorService mockTwoFactorService;

	@Mock
	private User mockUser;

	@Mock
	private AuthenticationTwoFactorCompanyConfiguration mockAuthenticationTwoFactorCompanyConfiguration;

	@InjectMocks
	private TwoFactorAuthenticationVerifyMVCRenderCommand twoFactorAuthenticationVerifyMVCRenderCommand;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, SessionErrors.class, ParamUtil.class);
	}

	@Test
	public void render_WhenUserFound_ThenReturnsTheVerifyPage() throws PortletException {
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.of(mockUser));

		String result = twoFactorAuthenticationVerifyMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/two-factor-authentication/verify.jsp"));
	}

	@Test
	public void render_WhenUserFound_ThenSetsTheRequestAttributes() throws PortletException {
		long userId = 123;
		String redirect = "redirectValue";
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.of(mockUser));
		when(mockUser.getUserId()).thenReturn(userId);
		when(ParamUtil.getString(mockRenderRequest, "redirect")).thenReturn(redirect);

		twoFactorAuthenticationVerifyMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("redirect", redirect);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenUserFoundAndEnabledTwoFactorConfigurationExists_ThenSetsTheResetButtonEnabledAttributeInRequest(boolean verificationPageResetButtonEnabled) throws PortletException {
		long companyId = 322L;
		when(mockTwoFactorRequestUtils.getCompanyId(mockRenderRequest)).thenReturn(companyId);
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.of(mockUser));
		when(mockTwoFactorService.getEnabledConfiguration(companyId)).thenReturn(Optional.of(mockAuthenticationTwoFactorCompanyConfiguration));
		when(mockAuthenticationTwoFactorCompanyConfiguration.verificationPageResetButtonEnabled()).thenReturn(verificationPageResetButtonEnabled);

		twoFactorAuthenticationVerifyMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(TwoFactorRequestKeys.RESET_BUTTON_ENABLED, verificationPageResetButtonEnabled);
	}

	@Test
	public void render_WhenUserFoundAndEnabledTwoFactorConfigurationDoesNotExist_ThenDoesNotSetResetButtonEnabledAttributeInRequest() throws PortletException {
		long companyId = 322L;
		when(mockTwoFactorRequestUtils.getCompanyId(mockRenderRequest)).thenReturn(companyId);
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.of(mockUser));
		when(mockTwoFactorService.getEnabledConfiguration(companyId)).thenReturn(Optional.empty());

		twoFactorAuthenticationVerifyMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(TwoFactorRequestKeys.RESET_BUTTON_ENABLED), anyBoolean());
	}

	@Test
	public void render_WhenUserNotFound_ThenReturnsTheErrorPage() throws PortletException {
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.empty());

		String result = twoFactorAuthenticationVerifyMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/two-factor-authentication/error.jsp"));
	}

	@Test
	public void render_WhenUserNotFound_ThenSetsTheSessionError() throws PortletException {
		when(mockTwoFactorRequestUtils.getOriginalServletRequest(mockRenderRequest)).thenReturn(mockHttpServletRequest);
		when(mockTwoFactorService.getTwoFactorUser(mockRenderRequest, mockHttpServletRequest)).thenReturn(Optional.empty());

		twoFactorAuthenticationVerifyMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockRenderRequest, "sessionExpired");
	}
}
