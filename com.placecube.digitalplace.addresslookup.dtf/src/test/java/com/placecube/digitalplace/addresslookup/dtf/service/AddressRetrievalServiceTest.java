package com.placecube.digitalplace.addresslookup.dtf.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.dtf.configuration.DTFCompanyConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AddressRetrievalServiceTest extends PowerMockito {

	private static final Long COMPANY_ID = 1l;

	private static final String DATABASE_PASSWORD = "password";

	private static final String DATABASE_URL = "url";

	private static final String DATABASE_USER = "user";

	private static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";

	private static final String SQL = "sql";

	@InjectMocks
	private AddressRetrievalService addressRetrievalService;

	@Mock
	private AddressContext mockAddressContext1;

	@Mock
	private AddressContext mockAddressContext2;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private Connection mockConnection;

	@Mock
	private DTFCompanyConfiguration mockDTFCompanyConfiguration;

	@Mock
	private ResponseParserService mockResponseParserService;

	@Mock
	private ResultSet mockResultSet;

	@Mock
	private Statement mockStatement;

	@Test
	public void closeConnection_WhenNoError_ThenCallsCloseOnConnection() throws SQLException {

		addressRetrievalService.closeConnection(mockConnection);

		verify(mockConnection, times(1)).close();
	}

	@Test
	public void executeQuery_WhenError_ThenReturnsEmptyOptional() throws SQLException {

		when(mockConnection.createStatement()).thenThrow(new SQLException());

		Optional<ResultSet> result = addressRetrievalService.executeQuery(mockConnection, SQL);

		assertFalse(result.isPresent());
	}

	@Test
	public void executeQuery_WhenNoError_ThenReturnsResultSetOptional() throws SQLException {

		when(mockConnection.createStatement()).thenReturn(mockStatement);
		when(mockStatement.executeQuery(SQL)).thenReturn(mockResultSet);

		Optional<ResultSet> result = addressRetrievalService.executeQuery(mockConnection, SQL);

		assertThat(result.get(), sameInstance(mockResultSet));
	}

	@Test
	public void getAddressContext_WhenHasResult_ThenReturnsAddressContextOptional() throws SQLException {

		when(mockResultSet.next()).thenReturn(true);
		when(mockResponseParserService.getAddressContextFromResultSet(mockResultSet)).thenReturn(mockAddressContext1);

		Optional<AddressContext> result = addressRetrievalService.getAddressContext(mockResultSet);

		assertThat(result.get(), sameInstance(mockAddressContext1));
	}

	@Test
	public void getAddressContext_WhenNoResult_ThenReturnsEmptyOptional() throws SQLException {

		when(mockResultSet.next()).thenReturn(false);

		Optional<AddressContext> result = addressRetrievalService.getAddressContext(mockResultSet);

		assertFalse(result.isPresent());
	}

	@Test
	public void getAddressContext_WhenSQLException_ThenReturnsEmptyOptional() throws SQLException {

		when(mockResultSet.next()).thenThrow(new SQLException());

		Optional<AddressContext> result = addressRetrievalService.getAddressContext(mockResultSet);

		assertFalse(result.isPresent());
	}

	@Test
	public void getAddressContexts_WhenHasResults_ThenReturnsAddressContextSet() throws SQLException {

		when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
		when(mockResponseParserService.getAddressContextFromResultSet(mockResultSet)).thenReturn(mockAddressContext1).thenReturn(mockAddressContext2);

		Set<AddressContext> result = addressRetrievalService.getAddressContexts(mockResultSet);

		assertThat(result, containsInAnyOrder(mockAddressContext1, mockAddressContext2));
	}

	@Test
	public void getAddressContexts_WhenNoResults_ThenReturnsEmptySet() throws SQLException {

		when(mockResultSet.next()).thenReturn(false);

		Set<AddressContext> result = addressRetrievalService.getAddressContexts(mockResultSet);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getAddressContexts_WhenSQLException_ThenReturnsEmptySet() throws SQLException {

		when(mockResultSet.next()).thenThrow(new SQLException());

		Set<AddressContext> result = addressRetrievalService.getAddressContexts(mockResultSet);

		assertTrue(result.isEmpty());
	}

	@Test(expected = ConfigurationException.class)
	public void getDatabaseClass_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		addressRetrievalService.getDatabaseClass(COMPANY_ID);
	}

	@Test
	public void getDatabaseClass_WhenNoError_ThenReturnsTheDatabaseClass() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockDTFCompanyConfiguration);
		when(mockDTFCompanyConfiguration.databaseClass()).thenReturn(DRIVER_CLASS);

		String result = addressRetrievalService.getDatabaseClass(COMPANY_ID);

		assertThat(result, equalTo(DRIVER_CLASS));
	}

	@Test(expected = ConfigurationException.class)
	public void getDatabasePassword_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		addressRetrievalService.getDatabasePassword(COMPANY_ID);
	}

	@Test
	public void getDatabasePassword_WhenNoError_ThenReturnsTheDatabasePassword() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockDTFCompanyConfiguration);
		when(mockDTFCompanyConfiguration.databasePassword()).thenReturn(DATABASE_PASSWORD);

		String result = addressRetrievalService.getDatabasePassword(COMPANY_ID);

		assertThat(result, equalTo(DATABASE_PASSWORD));
	}

	@Test(expected = ConfigurationException.class)
	public void getDatabaseURL_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		addressRetrievalService.getDatabaseURL(COMPANY_ID);
	}

	@Test
	public void getDatabaseURL_WhenNoError_ThenReturnsTheDatabaseURL() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockDTFCompanyConfiguration);
		when(mockDTFCompanyConfiguration.databaseURL()).thenReturn(DATABASE_URL);

		String result = addressRetrievalService.getDatabaseURL(COMPANY_ID);

		assertThat(result, equalTo(DATABASE_URL));
	}

	@Test(expected = ConfigurationException.class)
	public void getDatabaseUser_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		addressRetrievalService.getDatabaseUser(COMPANY_ID);
	}

	@Test
	public void getDatabaseUser_WhenNoError_ThenReturnsTheDatabaseUser() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockDTFCompanyConfiguration);
		when(mockDTFCompanyConfiguration.databaseUser()).thenReturn(DATABASE_USER);

		String result = addressRetrievalService.getDatabaseUser(COMPANY_ID);

		assertThat(result, equalTo(DATABASE_USER));
	}

	@Test(expected = ConfigurationException.class)
	public void getWeitght_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		addressRetrievalService.getWeight(COMPANY_ID);
	}

	@Test
	public void getWeitght_WhenNoError_ThenReturnsTheWeight() throws ConfigurationException {
		Integer expected = 1;
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockDTFCompanyConfiguration);
		when(mockDTFCompanyConfiguration.weight()).thenReturn(expected);

		Integer result = addressRetrievalService.getWeight(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = ConfigurationException.class)
	public void isEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		addressRetrievalService.isEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockDTFCompanyConfiguration);
		when(mockDTFCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = addressRetrievalService.isEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = ConfigurationException.class)
	public void isNational_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		addressRetrievalService.isNational(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isNational_WhenNoError_ThenReturnsIfTheConfigurationIsNationalOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockDTFCompanyConfiguration);
		when(mockDTFCompanyConfiguration.national()).thenReturn(expected);

		boolean result = addressRetrievalService.isNational(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}
}