package com.placecube.digitalplace.externalpage.web.constants;

public class BackgroundTaskKeys {

	public static final String COMPANY_ID = "companyId";

	public static final String INDEX_ARRAY = "indexArray";

	public static final String MAX_PAGES_TO_FETCH = "maxPagesToFetch";

	public static final String SCOPE_GROUP_ID = "scopeGroupId";

	public static final String TASK_NAME = "externalPageImport";

	public static final String TOTAL_URL_COUNT = "totalURLCount";

	public static final String URL = "url";

	public static final String USER_EMAIL_ADDRESS = "userEmailAddress";

	public static final String USER_ID = "userId";

	public static final String USER_NAME = "userName";

	public static final String X_PATH = "xpath";

	private BackgroundTaskKeys() {
	}
}
