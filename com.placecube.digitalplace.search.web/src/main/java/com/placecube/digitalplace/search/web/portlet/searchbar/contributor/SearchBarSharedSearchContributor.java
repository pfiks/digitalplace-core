package com.placecube.digitalplace.search.web.portlet.searchbar.contributor;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchParameters;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.SEARCH_BAR, service = SharedSearchContributor.class)
public class SearchBarSharedSearchContributor implements SharedSearchContributor {

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<String> parameter = sharedSearchContributorSettings.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SEARCH_KEYWORD);

		sharedSearchContributorSettings.setSearchKeywords(parameter.orElse(StringPool.BLANK));
	}

}