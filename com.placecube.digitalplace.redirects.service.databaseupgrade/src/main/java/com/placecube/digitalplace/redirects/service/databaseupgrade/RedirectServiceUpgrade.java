package com.placecube.digitalplace.redirects.service.databaseupgrade;

import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.liferay.redirect.service.RedirectEntryLocalService;
import com.placecube.digitalplace.redirects.service.databaseupgrade.upgrade_1_0_0.RedirectionUpgradeProcess;
import com.placecube.digitalplace.redirects.service.databaseupgrade.upgrade_1_1_0.DropRedirectTableUpgradeProcess;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class RedirectServiceUpgrade implements UpgradeStepRegistrator {

	@Reference
	private RedirectEntryLocalService redirectEntryLocalService;

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new RedirectionUpgradeProcess(redirectEntryLocalService, companyLocalService, groupLocalService));

		registry.register("1.0.0", "1.1.0", new DropRedirectTableUpgradeProcess());
	}

}
