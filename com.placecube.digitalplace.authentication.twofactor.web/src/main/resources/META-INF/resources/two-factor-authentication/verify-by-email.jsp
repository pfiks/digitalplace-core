<%@ include file="init.jsp" %>

<div class="two-factor-authenticator two-factor-authenticator-verify">

	<portlet:actionURL name="<%=TwoFactorMVCCommandKeys.VERIFY_BY_EMAIL %>" var="verifyURL">
		<portlet:param name="mvcRenderCommandName" value="<%=TwoFactorMVCCommandKeys.VERIFY_BY_EMAIL %>" />
		<portlet:param name="saveLastPath" value="false" />
		<portlet:param name="redirect" value="${redirect}" />
	</portlet:actionURL>
	
	<portlet:actionURL name="<%=TwoFactorMVCCommandKeys.SEND_EMAIL_WITH_CODE %>" var="sendEmailWithCodeUrl">
		<portlet:param name="mvcRenderCommandName" value="<%=TwoFactorMVCCommandKeys.VERIFY_BY_EMAIL %>" />
		<portlet:param name="saveLastPath" value="false" />
		<portlet:param name="redirect" value="${redirect}" />
	</portlet:actionURL>

	<aui:form action="${verifyURL}" cssClass="container-fluid container-fluid-max-xl sign-in-form" data-senna-off="true" method="post" name="verifyTwoFactorAuthenticationForm">

		<div class="alert alert-info">
			<liferay-ui:message key="two-factor-authentication-email-information" arguments="${twoFactorAuthenticationEmailUserHidedEmailAddress}"/>
			<liferay-ui:message key="two-factor-authentication-expiry-information" arguments="${expiryInSeconds}"/>
		</div>
		<a href="${sendEmailWithCodeUrl}">
			<aui:button-row>
				<aui:button value="send" />
			</aui:button-row>
		</a>

		<liferay-ui:error key="twoFactorVerificationFailed" message="two-factor-authentication-email-has-failed" />

		<aui:input autocomplete="off" label="authenticator-email-code" name="twoFactorAuthCode" required="true" />

		<aui:button-row>
			<aui:button type="submit" value="submit" />
		</aui:button-row>
		
	</aui:form>

</div>