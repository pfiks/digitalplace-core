package com.placecube.digitalplace.search.web.portlet.tag;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.tag.configuration.TagFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.tag.service.TagConfigurationService;
import com.placecube.digitalplace.search.web.service.FacetParsingService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class, ListUtil.class })
public class TagFacetPortletTest extends PowerMockito {

	@Mock
	private FacetParsingService mockFacetParsingService;

	@Mock
	private List<FacetResult> mockFacetResults;

	@Mock
	private List<FacetResult> mockFacetResultsSublist1;

	@Mock
	private List<FacetResult> mockFacetResultsSublist2;

	@Mock
	private Map<String, Integer> mockFacetValues;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private Set<String> mockSelectedValues;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private TagConfigurationService mockTagConfigurationService;

	@Mock
	private TagFacetPortletInstanceConfiguration mockTagFacetPortletInstanceConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private TagFacetPortlet tagFacetPortlet;

	@Before
	public void activateSetup() throws Exception {
		mockStatic(ListUtil.class);
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test
	public void render_WhenConfigurationIsInvalid_ThenSetsInvalidConfigurationTrueAsRequestAttributeAndDoesNotExecuteTheSearch() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockTagConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.empty());

		tagFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verifyZeroInteractions(mockSharedSearchRequestService);

	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockTagConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockTagFacetPortletInstanceConfiguration));
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		tagFacetPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenConfiguresTheTagsDetailsAsRequestAttributes() throws Exception {
		int maxTagsToShow = 3;
		int maxShowMoreTagsToShow = 5;
		String redirectURL = "redirectURLVal";
		String colour = "orange";
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getFacetValues(SearchFacetConstants.ASSET_TAG_NAMES)).thenReturn(mockFacetValues);
		when(mockSharedSearchResponse.getActiveValuesForFilter(SearchParameters.SELECTED_TAGS)).thenReturn(mockSelectedValues);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockTagConfigurationService.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockTagFacetPortletInstanceConfiguration));
		when(mockFacetParsingService.parseFacets(mockFacetValues, mockSelectedValues)).thenReturn(mockFacetResults);
		when(mockTagFacetPortletInstanceConfiguration.maxTagsToShow()).thenReturn(maxTagsToShow);
		when(mockTagFacetPortletInstanceConfiguration.maxShowMoreTagsToShow()).thenReturn(maxShowMoreTagsToShow);
		when(mockTagFacetPortletInstanceConfiguration.filterColour()).thenReturn(colour);
		when(mockSharedSearchResponse.getEscapedSearchResultsIteratorURL()).thenReturn(redirectURL);
		when(ListUtil.subList(mockFacetResults, 0, maxTagsToShow)).thenReturn(mockFacetResultsSublist1);
		when(ListUtil.subList(mockFacetResults, maxTagsToShow, maxTagsToShow + maxShowMoreTagsToShow)).thenReturn(mockFacetResultsSublist2);

		tagFacetPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("availableTags", mockFacetResultsSublist1);
		verify(mockRenderRequest, times(1)).setAttribute("availableShowMoreTags", mockFacetResultsSublist2);
		verify(mockRenderRequest, times(1)).setAttribute("filterColour", colour);
		verify(mockRenderRequest, never()).setAttribute(eq("invalidConfiguration"), any());
		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", redirectURL);
	}

}
