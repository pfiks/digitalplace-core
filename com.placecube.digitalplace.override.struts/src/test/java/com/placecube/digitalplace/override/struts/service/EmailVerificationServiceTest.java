package com.placecube.digitalplace.override.struts.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.TicketConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.AuthToken;
import com.liferay.portal.kernel.security.auth.AuthTokenUtil;
import com.liferay.portal.kernel.security.auth.PrincipalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.TicketLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.override.struts.portal.action.VerifyEmailOverrideAction;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceContextFactory.class, PropsUtil.class, ParamUtil.class, AuthTokenUtil.class })
public class EmailVerificationServiceTest {

	private static final Long COMPANY_ID = 4333L;
	private static final String USER_EMAIL_ADDRESS = "user.email@address.com";
	private static final Long USER_ID = 2876L;

	@InjectMocks
	private EmailVerificationService emailVerificationService;

	@Mock
	private AuthToken mockAuthToken;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TicketLocalService mockTicketLocalService;

	@Mock
	private Ticket mockTicketOne;

	@Mock
	private Ticket mockTicketTwo;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Test(expected = PortalException.class)
	public void sendEmailAddressVerification_WhenEmailAddressVerificationDeliveryFails_ThenThrowsPortalException() throws Exception {
		when(ServiceContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockServiceContext);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL_ADDRESS);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTicketLocalService.getTickets(COMPANY_ID, User.class.getName(), USER_ID, TicketConstants.TYPE_EMAIL_ADDRESS)).thenReturn(new ArrayList<>());

		doThrow(new PortalException()).when(mockUserLocalService).sendEmailAddressVerification(mockUser, USER_EMAIL_ADDRESS, mockServiceContext);

		emailVerificationService.sendEmailAddressVerification(mockHttpServletRequest, mockThemeDisplay);

	}

	@Test(expected = PortalException.class)
	public void sendEmailAddressVerification_WhenServiceContextCreationFails_ThenThrowsPortalException() throws Exception {
		when(ServiceContextFactory.getInstance(mockHttpServletRequest)).thenThrow(new PortalException());

		emailVerificationService.sendEmailAddressVerification(mockHttpServletRequest, mockThemeDisplay);

	}

	@Test
	public void sendEmailAddressVerification_WhenUserDoesNotHaveAnyEmailTickets_ThenSendsEmailNotificationWithUserEmailAddress() throws Exception {
		when(ServiceContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockServiceContext);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getEmailAddress()).thenReturn(USER_EMAIL_ADDRESS);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTicketLocalService.getTickets(COMPANY_ID, User.class.getName(), USER_ID, TicketConstants.TYPE_EMAIL_ADDRESS)).thenReturn(new ArrayList<>());

		emailVerificationService.sendEmailAddressVerification(mockHttpServletRequest, mockThemeDisplay);

		verify(mockUserLocalService, times(1)).sendEmailAddressVerification(mockUser, USER_EMAIL_ADDRESS, mockServiceContext);
	}

	@Test
	public void sendEmailAddressVerification_WhenUserHasEmailTickets_ThenSendsEmailNotificationForFirstTicketWithExtraInfo() throws Exception {
		final String extraInfo = "extra info";

		List<Ticket> tickets = new ArrayList<>();
		tickets.add(mockTicketOne);
		tickets.add(mockTicketTwo);

		when(mockTicketOne.getExtraInfo()).thenReturn(extraInfo);

		when(ServiceContextFactory.getInstance(mockHttpServletRequest)).thenReturn(mockServiceContext);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTicketLocalService.getTickets(COMPANY_ID, User.class.getName(), USER_ID, TicketConstants.TYPE_EMAIL_ADDRESS)).thenReturn(tickets);

		emailVerificationService.sendEmailAddressVerification(mockHttpServletRequest, mockThemeDisplay);

		verify(mockUserLocalService, times(1)).sendEmailAddressVerification(mockUser, extraInfo, mockServiceContext);
	}

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() {
		mockStatic(ServiceContextFactory.class, PropsUtil.class, ParamUtil.class);
		mockStatic(AuthTokenUtil.class);
	}

	@Test(expected = PrincipalException.class)
	public void verifyEmailAddress_WhenCSRFTokenCheckFails_ThenThrowsPrincipalException() throws Exception {
		doThrow(new PrincipalException()).when(AuthTokenUtil.class, "checkCSRFToken", mockHttpServletRequest, VerifyEmailOverrideAction.class.getName());

		emailVerificationService.verifyEmailAddress(mockHttpServletRequest);
	}

	@Test(expected = PortalException.class)
	public void verifyEmailAddress_WhenEmailVerificationFails_ThenThrowsPortalException() throws Exception {
		final String ticketKey = "ticket-key";

		when(ParamUtil.getString(mockHttpServletRequest, "ticketKey")).thenReturn(ticketKey);
		doThrow(new PortalException()).when(mockUserLocalService).verifyEmailAddress(ticketKey);

		emailVerificationService.verifyEmailAddress(mockHttpServletRequest);
	}

	@Test
	public void verifyEmailAddress_WhenNoErrors_ThenChecksCSRFToken() throws Exception {
		emailVerificationService.verifyEmailAddress(mockHttpServletRequest);

		verifyStatic(AuthTokenUtil.class);

		AuthTokenUtil.checkCSRFToken(mockHttpServletRequest, VerifyEmailOverrideAction.class.getName());
	}

	@Test
	public void verifyEmailAddress_WhenNoErrors_ThenVerifiesTicketKeyInRequest() throws Exception {
		final String ticketKey = "ticket-key";

		when(ParamUtil.getString(mockHttpServletRequest, "ticketKey")).thenReturn(ticketKey);

		emailVerificationService.verifyEmailAddress(mockHttpServletRequest);

		verify(mockUserLocalService, times(1)).verifyEmailAddress(ticketKey);
	}

}
