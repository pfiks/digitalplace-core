package com.placecube.digitalplace.payment.constants;

public final class PaymentConstants {

	public static final String API_AUTH_KEY = "apiAuthKey";

	public static final String DYNAMIC_DATA_MAPPING_FORM_INSTANCE_ID = "DYNAMIC_DATA_MAPPING_FORM_INSTANCE_ID";

	public static final String FORM_INSTANCE_RECORD_VERSION_ID = "formInstanceRecordVersionId";

	public static final String FORM_REFERENCE_PLACEHOLDER = "${FORM_REFERENCE}";

	public static final String GROUP_ID = "GROUP_ID";

	public static final String PAYMENT_REDIRECT_URL = "paymentRedirectURL";

	public static final String PAYMENT_REFERENCE_ID = "paymentReferenceID";

	public static final String PAYMENT_RETURN_URL = "paymentReturnURL";

	public static final String USER_ID = "userId";


	private PaymentConstants() {

	}

}
