package com.placecube.digitalplace.search.web.portlet.category.contributor;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.facet.config.FacetConfiguration;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.category.configuration.CategoryFacetPortletConfigurationConstants;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryLocalService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.CATEGORY_FILTER, service = SharedSearchContributor.class)
public class CategoryFacetPortletSharedSearchContributor implements SharedSearchContributor {

	@Reference
	private CategoryLocalService categoryLocalService;

	@Override
	public void contribute(SharedSearchContributorSettings sharedSearchContributorSettings) {
		addCategoryFacet(sharedSearchContributorSettings);

		addPreselectedCategoryFilters(sharedSearchContributorSettings);
	}

	private void addCategoryFacet(SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<PortletPreferences> prefs = sharedSearchContributorSettings.getPortletPreferences();
		if (prefs.isPresent()) {
			PortletPreferences portletPreferences = prefs.get();
			int maxItemsToShow = GetterUtil.getInteger(portletPreferences.getValue(CategoryFacetPortletConfigurationConstants.MAX_CATEGORIES_TO_SHOW, "10"));

			String facetFieldName = SearchFacetConstants.ASSET_CATEGORY_IDS;

			FacetConfiguration facetConfiguration = sharedSearchContributorSettings.createFacetConfiguration(facetFieldName);
			facetConfiguration.setOrder("OrderHitsDesc");
			facetConfiguration.setStatic(false);
			facetConfiguration.setWeight(1.3d);
			JSONObject data = facetConfiguration.getData();
			data.put("maxTerms", maxItemsToShow);
			data.put("frequencyThreshold", 1);

			sharedSearchContributorSettings.addFacet(facetFieldName, Optional.of(facetConfiguration));
		} else {
			throw new SystemException("Preferences not found");
		}
	}

	private void addPreselectedCategoryFilters(SharedSearchContributorSettings sharedSearchContributorSettings) {
		Optional<PortletPreferences> prefs = sharedSearchContributorSettings.getPortletPreferences();
		if (prefs.isPresent()) {
			PortletPreferences portletPreferences = prefs.get();

			Set<Long> preselectedCategories = getAggregatedPreselectedCategories(portletPreferences, sharedSearchContributorSettings.getThemeDisplay());

			String internalPortletId = portletPreferences.getValue(CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK);

			List<SharedActiveFilter> activeCategoryFilters = sharedSearchContributorSettings.getActiveFiltersForField(SearchParameters.SELECTED_CATEGORIES + "_" + internalPortletId);
			activeCategoryFilters.stream().map(SharedActiveFilter::getValue).map(GetterUtil::getLong).forEach(v -> preselectedCategories.add(v));

			long[] selectedCategoriesInSession = sharedSearchContributorSettings.getAssetCategoryIds();
			if (selectedCategoriesInSession != null) {
				for (long categoryId : selectedCategoriesInSession) {
					preselectedCategories.add(categoryId);
				}
			}

			List<String> assetCategoryIds = preselectedCategories.stream().map(Object::toString).collect(Collectors.toList());
			boolean isCategoriesFilterAnded = GetterUtil.getBoolean(portletPreferences.getValue(CategoryFacetPortletConfigurationConstants.CATEGORIES_QUERY_ANDED, Boolean.FALSE.toString()));
			addFiltersToContributorSettings(assetCategoryIds, sharedSearchContributorSettings, isCategoriesFilterAnded);

		} else {
			throw new SystemException("Preferences not found");
		}
	}

	private void addFiltersToContributorSettings(List<String> assetCategoryIds, SharedSearchContributorSettings sharedSearchContributorSettings, boolean isCategoriesFilterAnded) {
		if (isCategoriesFilterAnded) {
			for (String assetCategoryId : assetCategoryIds) {
				sharedSearchContributorSettings.addBooleanQuery(SearchFacetConstants.ASSET_CATEGORY_IDS, assetCategoryId, BooleanClauseOccur.MUST);
			}
		} else {
			sharedSearchContributorSettings.addBooleanQueryForMultipleValues(SearchFacetConstants.ASSET_CATEGORY_IDS, assetCategoryIds, BooleanClauseOccur.MUST);
		}
	}

	private Set<Long> getAggregatedPreselectedCategories(PortletPreferences portletPreferences, ThemeDisplay themeDisplay) {
		String[] vocabularies = GetterUtil.getStringValues(portletPreferences.getValues(CategoryFacetPortletConfigurationConstants.PRESELECTED_VOCABOLARY_ID, new String[0]));
		String[] categories = GetterUtil.getStringValues(portletPreferences.getValues(CategoryFacetPortletConfigurationConstants.PRESELECTED_CATEGORY_IDS, new String[0]));

		return categoryLocalService.getAggregateCategoryLists(themeDisplay, vocabularies, categories);
	}

}