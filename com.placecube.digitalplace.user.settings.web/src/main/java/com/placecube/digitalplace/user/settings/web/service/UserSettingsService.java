package com.placecube.digitalplace.user.settings.web.service;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;
import com.placecube.digitalplace.user.settings.web.constants.PortletKeys;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.initializer.service.ServiceContextInitializer;
import com.placecube.journal.service.JournalArticleCreationService;

@Component(immediate = true, service = UserSettingsService.class)
public class UserSettingsService {

	@Reference
	private JournalArticleCreationService journalArticleCreationService;

	@Reference
	private PortletURLFactory portletURLFactory;

	@Reference
	private ServiceContextInitializer serviceContextInitializer;

	@SuppressWarnings("rawtypes")
	public void configureUserSettingsWebContentArticle(Company company, Class clazz, String articlePath, String articleId, String articleTitle) throws PortalException {
		try {
			ServiceContext serviceContext = serviceContextInitializer.getServiceContext(company.getGroup());

			String articleContent = StringUtil.read(clazz.getClassLoader(), articlePath);

			JournalFolder journalFolder = journalArticleCreationService.getOrCreateJournalFolder("User Settings Portlet", serviceContext);

			journalArticleCreationService.getOrCreateBasicWebContentArticle(articleId, articleTitle, articleContent, journalFolder, serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public LiferayPortletURL getUserSettingsURL(PortletRequest portletRequest, ThemeDisplay themeDisplay, DisplaySection currentDisplayArea) {
		LiferayPortletURL currentPageUrl = portletURLFactory.create(portletRequest, PortletKeys.USER_SETTINGS, themeDisplay.getPlid(), PortletRequest.RENDER_PHASE);
		currentPageUrl.getRenderParameters().setValue(PortletRequestKeys.CURRENT_SECTION_ID, currentDisplayArea.getKey());
		return currentPageUrl;
	}

}
