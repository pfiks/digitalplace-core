package com.placecube.digitalplace.form.service.impl;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.constants.DDMStructureConstants;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormLayout;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureLayout;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLayoutLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.dynamic.data.mapping.storage.StorageType;
import com.liferay.exportimport.kernel.lar.ExportImportThreadLocal;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.form.exception.FormDefinitionException;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.digitalplace.form.model.impl.FormDefinitionImpl;
import com.placecube.digitalplace.form.service.FormDefinitionService;
import com.placecube.digitalplace.form.service.impl.internal.DDMDataProviderService;
import com.placecube.digitalplace.form.service.impl.internal.DDMFormInstanceService;
import com.placecube.digitalplace.form.service.impl.internal.FormDefinitionDeserializerService;
import com.placecube.digitalplace.form.service.impl.internal.FormJournalArticleService;
import com.placecube.digitalplace.form.service.impl.internal.util.FormDefinitionComparator;

@Component(immediate = true, service = FormDefinitionService.class)
public class FormDefinitionServiceImpl implements FormDefinitionService {

	private static final Log LOG = LogFactoryUtil.getLog(FormDefinitionServiceImpl.class);

	@Reference
	private DDMDataProviderService ddmDataProviderService;

	@Reference
	private DDMFormInstanceLocalService ddmFormInstanceLocalService;

	@Reference
	private DDMFormInstanceService ddmFormInstanceService;

	@Reference
	private DDMStructureLayoutLocalService ddmStructureLayoutLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private FormDefinitionDeserializerService formDefinitionDeserializerService;

	@Reference
	private FormJournalArticleService formJournalArticleService;

	@Reference
	private Portal portal;

	@Override
	public void addFormDefinition(FormDefinition formDefinition, ServiceContext serviceContext) throws FormDefinitionException {

		try {
			long groupId = serviceContext.getScopeGroupId();

			Locale locale = portal.getSiteDefaultLocale(groupId);

			List<DDMDataProviderInstance> ddmDataProviderInstances = ddmDataProviderService.getOrCreateDataProviders(formDefinition, serviceContext);

			String jsonDDMFormDefinition = getDDMFormDefinitionJSON(formDefinition, ddmDataProviderInstances, locale);

			DDMForm ddmForm = formDefinitionDeserializerService.getDDMForm(jsonDDMFormDefinition, locale);

			DDMFormLayout ddmFormLayout = formDefinitionDeserializerService.getDDMFormLayout(formDefinition);

			DDMFormValues settingsDDMFormValues = formDefinitionDeserializerService.getDDMFormValues(formDefinition, ddmForm, serviceContext);

			addFormDefinition(formDefinition, ddmForm, ddmFormLayout, settingsDDMFormValues, serviceContext);

		} catch (Exception e) {

			throw new FormDefinitionException(e);

		}
	}

	@Override
	public void addFormDefinition(FormDefinition formDefinition, String jsonDDMFormDefinition, String jsonDDMFormLayout, String jsonSettingsDDMFormValues, ServiceContext serviceContext)
			throws FormDefinitionException {

		try {

			long groupId = serviceContext.getScopeGroupId();

			Locale locale = portal.getSiteDefaultLocale(groupId);

			DDMForm ddmForm = formDefinitionDeserializerService.getDDMForm(jsonDDMFormDefinition, locale);

			DDMFormLayout ddmFormLayout = formDefinitionDeserializerService.getDDMFormLayout(jsonDDMFormLayout);

			DDMFormValues settingsDDMFormValues = formDefinitionDeserializerService.getDDMFormValues(jsonSettingsDDMFormValues, ddmForm);

			addFormDefinition(formDefinition, ddmForm, ddmFormLayout, settingsDDMFormValues, serviceContext);

		} catch (Exception e) {

			throw new FormDefinitionException(e);

		}

	}

	@Override
	public FormDefinition create(String name, String description, String key) {
		return new FormDefinitionImpl(name, description, key);
	}

	@Override
	public FormDefinition create(String name, String description, String category, String key, String journalArticleFolderName, URL ddmForm, URL ddmFormLayout, URL settingsDDMFormValues,
			List<URL> journalArticleURLs, List<URL> dataProviderDDMFormValues) {
		return new FormDefinitionImpl(name, description, category, key, journalArticleFolderName, ddmForm, ddmFormLayout, settingsDDMFormValues, journalArticleURLs, dataProviderDDMFormValues);
	}

	@Override
	public List<FormDefinition> sort(List<FormDefinition> formDefinitions) {

		Collections.sort(formDefinitions, FormDefinitionComparator.getInstance());

		return formDefinitions;
	}

	private void addFormDefinition(FormDefinition formDefinition, DDMForm ddmForm, DDMFormLayout ddmFormLayout, DDMFormValues settingsDDMFormValues, ServiceContext serviceContext)
			throws FormDefinitionException {

		long userId = serviceContext.getUserId();
		long groupId = serviceContext.getScopeGroupId();

		DDMFormInstance ddmFormInstance = null;
		DDMStructure ddmStructure = null;

		try {
			long classNameId = portal.getClassNameId(DDMFormInstance.class);

			String ddmStructureKey = formDefinition.getKey();
			ddmStructure = ddmStructureLocalService.fetchStructure(groupId, classNameId, ddmStructureKey);

			if (ddmStructure != null) {
				LOG.warn("DDMStructure already exists with ddmStructureKey:" + ddmStructureKey + ". No FormDefinition will be added for");
				return;
			}

			String name = formDefinition.getName();
			String description = formDefinition.getDescription();

			Map<Locale, String> nameMap = new HashMap<>();
			Map<Locale, String> descriptionMap = new HashMap<>();

			for (Locale curLocale : LanguageUtil.getAvailableLocales(groupId)) {
				nameMap.put(curLocale, LanguageUtil.get(curLocale, name));
				descriptionMap.put(curLocale, LanguageUtil.get(curLocale, description));
			}

			serviceContext.setAttribute("status", WorkflowConstants.STATUS_APPROVED);

			ddmStructure = ddmStructureLocalService.addStructure(userId, groupId, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, classNameId, ddmStructureKey, nameMap, descriptionMap, ddmForm,
					ddmFormLayout, StorageType.DEFAULT.toString(), DDMStructureConstants.TYPE_AUTO, serviceContext);

			ddmFormInstance = ddmFormInstanceService.addFormInstance(userId, groupId, ddmStructure, nameMap, descriptionMap, settingsDDMFormValues, serviceContext);

			if (Validator.isNotNull(formDefinition.getJournalArticleFolderName()) && Validator.isNotNull(formDefinition.getUrlsToJournalArticles())
					&& !formDefinition.getUrlsToJournalArticles().isEmpty()) {
				JournalFolder folder = formJournalArticleService.addJournalFolder(formDefinition, serviceContext);
				ExportImportThreadLocal.setLayoutValidationInProcess(true);
				formJournalArticleService.addJournalArticles(formDefinition, folder, serviceContext);
				ExportImportThreadLocal.setLayoutValidationInProcess(false);
			}

		} catch (Exception e) {

			LOG.error("Unable to add form instance for " + formDefinition.toString() + ". Rollling back due to " + e.getMessage(), e);

			rollback(formDefinition, ddmFormInstance, ddmStructure, serviceContext);

			throw new FormDefinitionException(e);
		}
	}

	private String getDDMFormDefinitionJSON(FormDefinition formDefinition, List<DDMDataProviderInstance> ddmDataProviderInstances, Locale locale) throws IOException {
		Map<String, String> placeholderValues = ddmDataProviderService.getDataProviderPlaceholders(ddmDataProviderInstances, locale);
		String jsonDDMFormDefinition = formDefinitionDeserializerService.getDDMFormDefinition(formDefinition);
		return ddmDataProviderService.replacePlaceholders(jsonDDMFormDefinition, placeholderValues);
	}

	private void rollback(FormDefinition formDefinition, DDMFormInstance ddmFormInstance, DDMStructure ddmStructure, ServiceContext serviceContext) {

		if (ddmFormInstance != null) {

			try {
				ddmFormInstanceLocalService.deleteFormInstance(ddmFormInstance);
			} catch (PortalException ex) {
				LOG.error("Unable to delete form instance for definition " + formDefinition);
			}

		} else {

			if (ddmStructure != null) {
				ddmStructureLocalService.deleteDDMStructure(ddmStructure);
			}

			DDMStructureLayout ddmStructureLayout = ddmStructureLayoutLocalService.fetchStructureLayout(serviceContext.getScopeGroupId(), portal.getClassNameId(DDMFormInstance.class),
					formDefinition.getKey().toUpperCase());
			if (ddmStructureLayout != null) {
				ddmStructureLayoutLocalService.deleteDDMStructureLayout(ddmStructureLayout);
			}

		}

	}

}
