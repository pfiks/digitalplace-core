<%@ include file="../init.jsp" %>


<portlet:renderURL var="cancelURL">
	<portlet:param name="mvcRenderCommandName" value="<%= MVCCommandKeys.VIEW %>" />
</portlet:renderURL>

<portlet:actionURL name="<%= MVCCommandKeys.UPDATE %>" var="updateNetworkWebhookURL">
	<portlet:param name="webhookId" value="${webhookFormModel.getWebhookId()}" />
	<portlet:param name="webhookClassName" value="${webhookFormModel.getClassName()}" />
</portlet:actionURL>

<div class="container-fluid container-fluid-max-xl">
	<div class="row">
		<div class="col-md-12">
			<div class="inline-alert-container lfr-alert-container"></div>

			<h1>
				<liferay-ui:message key="${webhookId gt 0 ? 'update': 'add'}" />
			</h1>

			<aui:form action="${updateNetworkWebhookURL}" method="post" name="fm" enctype="multipart/form-data">

				<div class="inline-alert-container lfr-alert-container"></div>

				<aui:row>
					<aui:input name="webhookName" type="text" required="true" label="name" value="${webhookFormModel.getName()}">
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:row>

				<aui:row>
					<aui:input name="webhookPayloadURL" type="text" required="true" label="url" value="${webhookFormModel.getPayloadURL()}">
						<aui:validator name="maxLength">400</aui:validator>
						<aui:validator name="url" />
					</aui:input>
				</aui:row>

				<aui:row>
					<aui:input name="webhookEnabled" type="checkbox" label="enabled" value="${webhookFormModel.isEnabled()}" />
				</aui:row>

				<aui:row>
					<aui:input name="webhookClassNameReadOnly" type="text" required="true" label="className" value="${webhookFormModel.getClassName()}" disabled="true" />
					<aui:input name="webhookClassPK" type="text" required="true" label="classPK" value="${webhookFormModel.getClassPK()}">
						<aui:validator name="min">1</aui:validator>
					</aui:input>
				</aui:row>

				<aui:field-wrapper label="events">
					<c:forEach items="${webhookAvailableEventActions}" var="availableEventAction">
						<aui:row>
							<aui:input name="webhookEventActions" type="checkbox" label="${availableEventAction.name().toLowerCase()}"
								value="${availableEventAction.name()}" checked="${webhookFormModel.isActionSelected(availableEventAction.name())}" />
						</aui:row>
					</c:forEach>
				</aui:field-wrapper>

				<aui:button-row>
					<aui:button type="submit" value="save" cssClass="btn btn-primary" />
					<aui:button href="${cancelURL}" type="cancel" cssClass="btn btn-default" />
				</aui:button-row>
			</aui:form>
		</div>
	</div>
</div>