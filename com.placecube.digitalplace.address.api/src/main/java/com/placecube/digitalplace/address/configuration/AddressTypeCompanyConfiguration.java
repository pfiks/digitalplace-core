package com.placecube.digitalplace.address.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;
import aQute.bnd.annotation.metatype.Meta.Type;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.address.configuration.AddressTypeCompanyConfiguration", localization = "content/Language", name = "address-type")
public interface AddressTypeCompanyConfiguration {

	@Meta.AD(deflt = "personal", required = false, optionLabels = { "primary", "personal", "business", "other" }, optionValues = { "primary", "personal", "business",
			"other" }, type = Type.String, name = "default-address-type-name", description = "default-address-type-desc")
	String defaultAddressType();
}