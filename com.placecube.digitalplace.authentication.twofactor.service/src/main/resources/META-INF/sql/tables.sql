create table Placecube_Auth_TwoFactor_UserTwoFactorAuthentication (
	uuid_ VARCHAR(75) null,
	companyId LONG not null,
	userId LONG not null,
	createDate DATE null,
	modifiedDate DATE null,
	secretKey VARCHAR(200) null,
	resetKey BOOLEAN,
	enabled BOOLEAN,
	primary key (companyId, userId)
);