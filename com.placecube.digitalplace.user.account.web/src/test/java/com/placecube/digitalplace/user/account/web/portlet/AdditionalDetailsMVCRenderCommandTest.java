package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AdditionalDetailsMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private AdditionalDetailsMVCRenderCommand additionalDetailsMVCRenderCommand;

	private final long COMPANY_ID = 1L;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private Set<ExpandoField> mockExpandoFields;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private UserAccountFieldRegistry mockUserAccountFieldRegistry;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenEmailAddressPageLocationIsAdditionalDetailsPage_ThenSetShowCaptchaOnEmailStepAttributeInRequest(boolean showCaptchaOnEmailStep)
			throws UserAccountException, PortletException {
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);
		when(mockAccountContextService.isShowCaptchaOnEmailStep(mockAccountContext, mockRenderRequest)).thenReturn(showCaptchaOnEmailStep);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.CAPTCHA_ENABLED, showCaptchaOnEmailStep);
	}

	@Test
	public void render_WhenEmailAddressPageLocationIsAdditionalDetailsPage_ThenSetsShowEmailAddressInRequestToTrue() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.SHOW_EMAIL_ADDRESS_IN_ADDITIONAL_DETAILS, true);
	}

	@Test
	public void render_WhenEmailAddressPageLocationIsNotAdditionalDetailsPage_ThenSetsShowEmailAddressInRequestToFalse() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.SHOW_EMAIL_ADDRESS_IN_ADDITIONAL_DETAILS, false);
	}

	@Test
	public void render_WhenNoError_ThenReturnsAdditionalDetailsJSP() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		String result = additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/additional-details.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetsAccountFieldsAsRequestAttribute() throws PortletException, UserAccountException {
		List<UserAccountField> userAccountFields = new ArrayList<>();

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockUserAccountFieldRegistry.getUserAccountFields(COMPANY_ID)).thenReturn(userAccountFields);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_FIELDS, userAccountFields);
	}

	@Test
	public void render_WhenNoError_ThenSetsAdditionalDetailsExpandoFieldsAsRequestAttribute() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		when(mockAccountConfigurationService.getConfiguredExpandoFields(mockAccountContext, mockUserProfileFieldsConfiguration)).thenReturn(mockExpandoFields);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("additionalDetailsExpandoFields", mockExpandoFields);
	}

	@Test
	public void render_WhenNoError_ThenSetsAdditionalDetailsPreviousStepAsRequestAttribute() throws PortletException, UserAccountException {
		String previousStep = "previousStep";

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getAdditionalDetailsPreviousStep(mockUserProfileFieldsConfiguration)).thenReturn(previousStep);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("additionalDetailsPreviousStep", previousStep);
	}

	@Test
	public void render_WhenNoError_ThenSetsCreateAccountConfigurationAsRequestAttribute() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_CONFIG, mockCreateAccountConfiguration);
	}

	@Test
	public void render_WhenNoError_ThenSetsCreateAccountContextAsRequestAttribute() throws UserAccountException, PortletException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_CONTEXT, mockAccountContext);
	}

	@Test
	public void render_WhenNoError_ThenSetsUserProfileFieldsConfigurationAsRequestAttribute() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.USER_PROFILE_CONFIG, mockUserProfileFieldsConfiguration);
	}

	@Test
	public void render_WhenNoError_ThenUpdateContextInSession() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockRenderRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		additionalDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockRenderRequest);
	}

}
