package com.placecube.digitalplace.payment.model;

import java.io.Serializable;

import com.placecube.digitalplace.payment.constants.PaymentStatus;

public class PaymentResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private AgreementStatus agreementStatus;

	private String redirectURL;

	private PaymentStatus status;

	public PaymentResponse(String redirectURL, PaymentStatus status) {
		this.redirectURL = redirectURL;
		this.status = status;
		agreementStatus = new AgreementStatus();
	}

	public PaymentResponse(String redirectURL, PaymentStatus status, AgreementStatus agreementStatus) {
		this.redirectURL = redirectURL;
		this.status = status;
		this.agreementStatus = agreementStatus;
	}

	public AgreementStatus getAgreementStatus() {
		return agreementStatus;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "PaymentResponse [redirectURL=" + redirectURL + ", agreementStatus=" + agreementStatus.toString() + ", status=" + status.toString() + "]";
	}
}
