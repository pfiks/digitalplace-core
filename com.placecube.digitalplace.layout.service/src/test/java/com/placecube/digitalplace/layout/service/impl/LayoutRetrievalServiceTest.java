package com.placecube.digitalplace.layout.service.impl;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.LayoutService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.theme.NavItem;
import com.pfiks.role.exception.RolePermissionException;
import com.pfiks.role.service.RolePermissionService;
import com.placecube.digitalplace.layout.util.LayoutRetrievalUtil;
import com.placecube.digitalplace.layout.util.LayoutWithPortletsRetrievalHelperUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LayoutRetrievalServiceTest extends PowerMockito {

	@InjectMocks
	private LayoutRetrievalServiceImpl layoutRetrievalServiceImpl;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private Layout mockLayoutOne;

	@Mock
	private LayoutRetrievalUtil mockLayoutRetrievalUtil;

	@Mock
	private Layout mockLayoutSample;

	@Mock
	private LayoutService mockLayoutService;

	@Mock
	private Layout mockLayoutThree;

	@Mock
	private Layout mockLayoutTwo;

	@Mock
	private LayoutWithPortletsRetrievalHelperUtil mockLayoutWithPortletsRetrievalHelperUtil;

	@Mock
	private NavItem mockNavItemOne;

	@Mock
	private NavItem mockNavItemTwo;

	@Mock
	private List<Portlet> mockPortletList;

	@Mock
	private Role mockRole1;

	@Mock
	private Role mockRole2;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private RolePermissionService mockRolePermissionService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getAllPortletsOnLayout_WhenNoError_ThenReturnsAllPortletsOnTheLayout() {
		when(mockLayoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(mockLayoutOne)).thenReturn(mockPortletList);

		List<Portlet> results = layoutRetrievalServiceImpl.getAllPortletsOnLayout(mockLayoutOne);

		assertThat(results, sameInstance(mockPortletList));
	}

	@Test
	public void getLayoutsWithPortletId_WhenNoErrors_ThenReturnsListWithTheLayoutsFound() {
		long groupId = 123;
		String portletId = "portletIdToMatchValue";
		List<Layout> groupLayouts = Collections.singletonList(mockLayoutOne);
		when(mockLayoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, true)).thenReturn(groupLayouts);

		List<Layout> result = layoutRetrievalServiceImpl.getLayoutsWithPortletId(groupId, portletId, true);

		assertThat(result, containsInAnyOrder(mockLayoutOne));
	}

	@Test
	public void getLayoutWithPortletId_WhenNoErrors_ThenReturnsOptionalWithTheLayout() {
		long groupId = 123;
		String portletId = "portletIdToMatchValue";
		List<Layout> groupLayouts = Arrays.asList(mockLayoutOne, mockLayoutTwo);
		when(mockLayoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, true)).thenReturn(groupLayouts);

		Optional<Layout> result = layoutRetrievalServiceImpl.getLayoutWithPortletId(groupId, portletId, true);

		assertThat(result.get(), sameInstance(mockLayoutOne));
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getNavItemForLayout_WhenExceptionRetrievingTheLayout_ThenThrowsPortalException(boolean privateLayout) throws Exception {
		long groupId = 123;
		String friendlyURL = "friendlyURLValue";
		when(mockLayoutLocalService.getFriendlyURLLayout(groupId, privateLayout, friendlyURL)).thenThrow(new PortalException());

		layoutRetrievalServiceImpl.getNavItemForLayout(mockHttpServletRequest, groupId, friendlyURL, privateLayout);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getNavItemForLayout_WhenNoError_ThenReturnsTheNavItemForTheLayout(boolean privateLayout) throws Exception {
		long groupId = 123;
		String friendlyURL = "friendlyURLValue";
		when(mockLayoutLocalService.getFriendlyURLLayout(groupId, privateLayout, friendlyURL)).thenReturn(mockLayoutOne);

		NavItem result = layoutRetrievalServiceImpl.getNavItemForLayout(mockHttpServletRequest, groupId, friendlyURL, privateLayout);

		assertThat(result.getLayout(), sameInstance(mockLayoutOne));
	}

	@Test
	public void getNavItemsForLayouts_WhenNoError_ThenReturnsTheNavItemsListForLayouts() {
		List<Layout> layouts = Arrays.asList(mockLayoutOne, mockLayoutTwo, mockLayoutThree);

		LinkedList<NavItem> result = layoutRetrievalServiceImpl.getNavItemsForLayouts(mockHttpServletRequest, layouts);

		for (int i = 0; i < layouts.size(); i++) {
			assertThat(result.get(i).getLayout(), sameInstance(layouts.get(i)));
		}
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOptionalNavItemForLayout_WhenLayoutFound_ThenReturnsOptionalWithTheNavItemForTheLayout(boolean privateLayout) {
		long groupId = 123;
		String friendlyURL = "friendlyURLValue";
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, privateLayout, friendlyURL)).thenReturn(mockLayoutOne);

		Optional<NavItem> result = layoutRetrievalServiceImpl.getOptionalNavItemForLayout(mockHttpServletRequest, groupId, friendlyURL, privateLayout);

		assertThat(result.get().getLayout(), sameInstance(mockLayoutOne));
	}

	@Parameters({ "true", "false" })
	public void getOptionalNavItemForLayout_WhenNoLayoutFound_ThenReturnsEmptyOptional(boolean privateLayout) {
		long groupId = 123;
		String friendlyURL = "friendlyURLValue";
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(groupId, privateLayout, friendlyURL)).thenReturn(null);

		Optional<NavItem> result = layoutRetrievalServiceImpl.getOptionalNavItemForLayout(mockHttpServletRequest, groupId, friendlyURL, privateLayout);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getPreferredLayoutWithPortletId_WhenNoLayoutsAreFound_ThenReturnsEmptyOptional() {
		long groupId = 123;
		String portletId = "portletIdToMatchValue";
		List<Layout> privateLayouts = new ArrayList<>();
		List<Layout> publicLayouts = new ArrayList<>();
		when(mockLayoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, true)).thenReturn(privateLayouts);
		when(mockLayoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, false)).thenReturn(publicLayouts);
		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayout(privateLayouts)).thenReturn(Optional.empty());
		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayout(publicLayouts)).thenReturn(Optional.empty());

		Optional<Layout> result = layoutRetrievalServiceImpl.getPreferredLayoutWithPortletId(groupId, "id", true);

		assertFalse(result.isPresent());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getPreferredLayoutWithPortletId_WhenThereIsOneValidLayout_ThenReturnsTheValidLayout(boolean preferenceOnPrivateLayout) {
		long groupId = 123;
		String portletId = "id";
		List<Layout> privateLayouts = Collections.singletonList(mockLayoutOne);
		List<Layout> publicLayouts = Collections.singletonList(mockLayoutTwo);

		when(mockLayoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, true)).thenReturn(privateLayouts);
		when(mockLayoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, false)).thenReturn(publicLayouts);
		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayout(privateLayouts)).thenReturn(preferenceOnPrivateLayout ? Optional.empty() : Optional.of(mockLayoutOne));
		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayout(publicLayouts)).thenReturn(preferenceOnPrivateLayout ? Optional.of(mockLayoutTwo) : Optional.empty());

		Optional<Layout> result = layoutRetrievalServiceImpl.getPreferredLayoutWithPortletId(groupId, portletId, preferenceOnPrivateLayout);

		assertTrue(result.isPresent());
		assertThat(result.get(), equalTo(preferenceOnPrivateLayout ? mockLayoutTwo : mockLayoutOne));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getPreferredLayoutWithPortletId_WhenThereIsOneValidLayoutAndPrefsArePresent_ThenReturnsTheValidLayout(boolean preferenceOnPrivateLayout) {
		long groupId = 123;
		String portletId = "id";
		Map<String, String> prefs = Collections.singletonMap("key", "value");

		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, true, prefs, portletId)).thenReturn(preferenceOnPrivateLayout ? Optional.empty() : Optional.of(mockLayoutOne));
		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, false, prefs, portletId)).thenReturn(preferenceOnPrivateLayout ? Optional.of(mockLayoutTwo) : Optional.empty());

		Optional<Layout> result = layoutRetrievalServiceImpl.getPreferredLayoutWithPortletId(groupId, portletId, preferenceOnPrivateLayout, prefs);

		assertTrue(result.isPresent());
		assertThat(result.get(), equalTo(preferenceOnPrivateLayout ? mockLayoutTwo : mockLayoutOne));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getPreferredLayoutWithPortletId_WhenThereIsOneValidPrivateLayoutAndThereIsOneValidPublicLayout_ThenReturnsTheValidLayout(boolean preferenceOnPrivateLayout) {
		long groupId = 123;
		String portletId = "id";
		List<Layout> privateLayouts = Collections.singletonList(mockLayoutOne);
		List<Layout> publicLayouts = Collections.singletonList(mockLayoutTwo);

		when(mockLayoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, true)).thenReturn(privateLayouts);
		when(mockLayoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, false)).thenReturn(publicLayouts);
		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayout(privateLayouts)).thenReturn(Optional.of(mockLayoutOne));
		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayout(publicLayouts)).thenReturn(Optional.of(mockLayoutTwo));

		Optional<Layout> result = layoutRetrievalServiceImpl.getPreferredLayoutWithPortletId(groupId, portletId, preferenceOnPrivateLayout);

		assertTrue(result.isPresent());
		assertThat(result.get(), equalTo(preferenceOnPrivateLayout ? mockLayoutOne : mockLayoutTwo));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getPreferredLayoutWithPortletId_WhenThereIsOneValidPrivateLayoutAndThereIsOneValidPublicLayoutAndPrefsArePresent_ThenReturnsTheValidLayout(boolean preferenceOnPrivateLayout) {
		long groupId = 123;
		String portletId = "id";
		Map<String, String> prefs = Collections.singletonMap("key", "value");

		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, true, prefs, portletId)).thenReturn(Optional.of(mockLayoutOne));
		when(mockLayoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, false, prefs, portletId)).thenReturn(Optional.of(mockLayoutTwo));

		Optional<Layout> result = layoutRetrievalServiceImpl.getPreferredLayoutWithPortletId(groupId, portletId, preferenceOnPrivateLayout, prefs);

		assertTrue(result.isPresent());
		assertThat(result.get(), equalTo(preferenceOnPrivateLayout ? mockLayoutOne : mockLayoutTwo));
	}

	@Test
	public void getPublicAndPrivateNavItems_WhenCurrentNavItemsArePrivate_ThenReturnsTheListWithTheNavItemsForThePublicLayoutsPlusTheCurrentNavItemsThatAreNotHidden() {
		long groupId = 123;
		when(mockNavItemOne.getLayout()).thenReturn(mockLayoutSample);
		when(mockLayoutSample.isPrivateLayout()).thenReturn(true);
		when(mockLayoutSample.getGroupId()).thenReturn(groupId);
		List<Layout> layouts = new LinkedList<>();
		layouts.add(mockLayoutOne);
		layouts.add(mockLayoutTwo);
		layouts.add(mockLayoutThree);
		when(mockLayoutService.getLayouts(groupId, false)).thenReturn(layouts);
		when(mockLayoutOne.isHidden()).thenReturn(false);
		when(mockLayoutTwo.isHidden()).thenReturn(true);
		when(mockLayoutThree.isHidden()).thenReturn(false);

		NavItem navItemLayoutOne = new NavItem(mockHttpServletRequest, mockLayoutOne);
		NavItem navItemLayoutTwo = new NavItem(mockHttpServletRequest, mockLayoutThree);

		List<NavItem> currentNavItems = new LinkedList<>();
		currentNavItems.add(mockNavItemOne);
		currentNavItems.add(mockNavItemTwo);

		List<NavItem> results = layoutRetrievalServiceImpl.getPublicAndPrivateNavItems(mockHttpServletRequest, currentNavItems);

		assertThat(results, contains(navItemLayoutOne, navItemLayoutTwo, mockNavItemOne, mockNavItemTwo));
	}

	@Test
	public void getPublicAndPrivateNavItems_WhenCurrentNavItemsArePublic_ThenReturnsTheListWithTheCurrentNavItemsPlusNavItemsForThePrivateLayoutsThatAreNotHidden() {
		long groupId = 123;
		when(mockNavItemOne.getLayout()).thenReturn(mockLayoutSample);
		when(mockLayoutSample.isPrivateLayout()).thenReturn(false);
		when(mockLayoutSample.getGroupId()).thenReturn(groupId);
		List<Layout> layouts = new LinkedList<>();
		layouts.add(mockLayoutOne);
		layouts.add(mockLayoutTwo);
		layouts.add(mockLayoutThree);
		when(mockLayoutService.getLayouts(groupId, true)).thenReturn(layouts);
		when(mockLayoutOne.isHidden()).thenReturn(false);
		when(mockLayoutTwo.isHidden()).thenReturn(true);
		when(mockLayoutThree.isHidden()).thenReturn(false);

		NavItem navItemLayoutOne = new NavItem(mockHttpServletRequest, mockLayoutOne);
		NavItem navItemLayoutTwo = new NavItem(mockHttpServletRequest, mockLayoutThree);

		List<NavItem> currentNavItems = new LinkedList<>();
		currentNavItems.add(mockNavItemOne);
		currentNavItems.add(mockNavItemTwo);

		List<NavItem> results = layoutRetrievalServiceImpl.getPublicAndPrivateNavItems(mockHttpServletRequest, currentNavItems);

		assertThat(results, contains(mockNavItemOne, mockNavItemTwo, navItemLayoutOne, navItemLayoutTwo));
	}

	@Test(expected = PortalException.class)
	public void hidePagesFromRoles_WhenExceptionRemovingApermission_ThenThrowsPortalException() throws Exception {
		long companyId = 11;
		long roleIdOne = 55;
		long plidOne = 66;

		List<Layout> layouts = new ArrayList<>();
		layouts.add(mockLayoutOne);
		when(mockLayoutOne.getCompanyId()).thenReturn(companyId);
		when(mockLayoutOne.getPlid()).thenReturn(plidOne);

		Set<String> roleNames = new HashSet<>();
		roleNames.add("roleNameOne");

		when(mockRoleLocalService.getRole(companyId, "roleNameOne")).thenReturn(mockRole1);
		when(mockRole1.getRoleId()).thenReturn(roleIdOne);

		doThrow(new RolePermissionException("", null)).when(mockRolePermissionService).removeIndividualPermissionForRole(companyId, roleIdOne, Layout.class.getName(), String.valueOf(plidOne),
				ActionKeys.VIEW);

		layoutRetrievalServiceImpl.hidePagesFromRoles(companyId, layouts, roleNames);
	}

	@Test
	public void hidePagesFromRoles_WhenNoError_ThenUpdatesAllTheLayoutsAsHiddenAndRemovesTheViewPermissionFromAllTheRoles() throws Exception {
		long companyId = 11;
		long roleIdOne = 55;
		long roleIdTwo = 77;
		long plidOne = 66;
		long plidTwo = 88;

		List<Layout> layouts = new LinkedList<>();
		layouts.add(mockLayoutOne);
		when(mockLayoutOne.getCompanyId()).thenReturn(companyId);
		when(mockLayoutOne.getPlid()).thenReturn(plidOne);

		layouts.add(mockLayoutTwo);
		when(mockLayoutTwo.getCompanyId()).thenReturn(companyId);
		when(mockLayoutTwo.getPlid()).thenReturn(plidTwo);

		Set<String> roleNames = new LinkedHashSet<>();
		roleNames.add("roleNameOne");
		when(mockRoleLocalService.getRole(companyId, "roleNameOne")).thenReturn(mockRole1);
		when(mockRole1.getRoleId()).thenReturn(roleIdOne);

		roleNames.add("roleNameTwo");
		when(mockRoleLocalService.getRole(companyId, "roleNameTwo")).thenReturn(mockRole2);
		when(mockRole2.getRoleId()).thenReturn(roleIdTwo);

		roleNames.add("invalidRoleName");
		when(mockRoleLocalService.getRole(companyId, "invalidRoleName")).thenThrow(new PortalException());

		layoutRetrievalServiceImpl.hidePagesFromRoles(companyId, layouts, roleNames);

		InOrder inOrder = Mockito.inOrder(mockLayoutLocalService, mockRolePermissionService, mockLayoutOne, mockLayoutTwo);
		inOrder.verify(mockRolePermissionService, times(1)).removeIndividualPermissionForRole(companyId, roleIdOne, Layout.class.getName(), String.valueOf(plidOne), ActionKeys.VIEW);
		inOrder.verify(mockRolePermissionService, times(1)).removeIndividualPermissionForRole(companyId, roleIdTwo, Layout.class.getName(), String.valueOf(plidOne), ActionKeys.VIEW);
		inOrder.verify(mockLayoutOne, times(1)).setHidden(true);
		inOrder.verify(mockLayoutLocalService, times(1)).updateLayout(mockLayoutOne);
		inOrder.verify(mockRolePermissionService, times(1)).removeIndividualPermissionForRole(companyId, roleIdOne, Layout.class.getName(), String.valueOf(plidTwo), ActionKeys.VIEW);
		inOrder.verify(mockRolePermissionService, times(1)).removeIndividualPermissionForRole(companyId, roleIdTwo, Layout.class.getName(), String.valueOf(plidTwo), ActionKeys.VIEW);
		inOrder.verify(mockLayoutTwo, times(1)).setHidden(true);
		inOrder.verify(mockLayoutLocalService, times(1)).updateLayout(mockLayoutTwo);
	}

	@Test(expected = PortalException.class)
	public void showPagesToRoles_WhenExceptionAddingApermission_ThenThrowsPortalException() throws Exception {
		long companyId = 11;
		long roleIdOne = 55;
		long plidOne = 66;

		List<Layout> layouts = new ArrayList<>();
		layouts.add(mockLayoutOne);
		when(mockLayoutOne.getCompanyId()).thenReturn(companyId);
		when(mockLayoutOne.getPlid()).thenReturn(plidOne);

		Set<String> roleNames = new HashSet<>();
		roleNames.add("roleNameOne");

		when(mockRoleLocalService.getRole(companyId, "roleNameOne")).thenReturn(mockRole1);
		when(mockRole1.getRoleId()).thenReturn(roleIdOne);

		doThrow(new RolePermissionException("", null)).when(mockRolePermissionService).addIndividualPermissionForRole(companyId, roleIdOne, Layout.class.getName(), String.valueOf(plidOne),
				ActionKeys.VIEW);

		layoutRetrievalServiceImpl.showPagesToRoles(companyId, layouts, roleNames);
	}

	@Test
	public void showPagesToRoles_WhenNoError_ThenUpdatesAllTheLayoutsAsNonHiddenAndAddsTheViewPermissionFromAllTheRoles() throws Exception {
		long companyId = 11;
		long roleIdOne = 55;
		long roleIdTwo = 77;
		long plidOne = 66;
		long plidTwo = 88;

		List<Layout> layouts = new LinkedList<>();
		layouts.add(mockLayoutOne);
		when(mockLayoutOne.getCompanyId()).thenReturn(companyId);
		when(mockLayoutOne.getPlid()).thenReturn(plidOne);

		layouts.add(mockLayoutTwo);
		when(mockLayoutTwo.getCompanyId()).thenReturn(companyId);
		when(mockLayoutTwo.getPlid()).thenReturn(plidTwo);

		Set<String> roleNames = new LinkedHashSet<>();
		roleNames.add("roleNameOne");
		when(mockRoleLocalService.getRole(companyId, "roleNameOne")).thenReturn(mockRole1);
		when(mockRole1.getRoleId()).thenReturn(roleIdOne);

		roleNames.add("roleNameTwo");
		when(mockRoleLocalService.getRole(companyId, "roleNameTwo")).thenReturn(mockRole2);
		when(mockRole2.getRoleId()).thenReturn(roleIdTwo);

		roleNames.add("invalidRoleName");
		when(mockRoleLocalService.getRole(companyId, "invalidRoleName")).thenThrow(new PortalException());

		layoutRetrievalServiceImpl.showPagesToRoles(companyId, layouts, roleNames);

		InOrder inOrder = Mockito.inOrder(mockLayoutLocalService, mockRolePermissionService, mockLayoutOne, mockLayoutTwo);
		inOrder.verify(mockRolePermissionService, times(1)).addIndividualPermissionForRole(companyId, roleIdOne, Layout.class.getName(), String.valueOf(plidOne), ActionKeys.VIEW);
		inOrder.verify(mockRolePermissionService, times(1)).addIndividualPermissionForRole(companyId, roleIdTwo, Layout.class.getName(), String.valueOf(plidOne), ActionKeys.VIEW);
		inOrder.verify(mockLayoutOne, times(1)).setHidden(false);
		inOrder.verify(mockLayoutLocalService, times(1)).updateLayout(mockLayoutOne);
		inOrder.verify(mockRolePermissionService, times(1)).addIndividualPermissionForRole(companyId, roleIdOne, Layout.class.getName(), String.valueOf(plidTwo), ActionKeys.VIEW);
		inOrder.verify(mockRolePermissionService, times(1)).addIndividualPermissionForRole(companyId, roleIdTwo, Layout.class.getName(), String.valueOf(plidTwo), ActionKeys.VIEW);
		inOrder.verify(mockLayoutTwo, times(1)).setHidden(false);
		inOrder.verify(mockLayoutLocalService, times(1)).updateLayout(mockLayoutTwo);
	}

}
