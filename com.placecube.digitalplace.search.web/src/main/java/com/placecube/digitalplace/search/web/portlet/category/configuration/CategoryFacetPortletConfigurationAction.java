package com.placecube.digitalplace.search.web.portlet.category.configuration;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryLocalService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.CATEGORY_FILTER, service = ConfigurationAction.class)
public class CategoryFacetPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private CategoryLocalService categoryLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		CategoryFacetPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, themeDisplay);

		List<AssetVocabulary> availableVocabularies = categoryLocalService.getAvailableVocabularies(themeDisplay);

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.ALPHA_SORT, configuration.alphaSort());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.CHECKBOXES_PANEL_COLLAPSED_BY_DEFAULT, configuration.checkboxesPanelCollapsedByDefault());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.CUSTOM_CSS_CLASS, configuration.customCssClass());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.CUSTOM_HEADING, configuration.customHeading());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.DISPLAY_CATEGORY_IDS, categoryLocalService.getLongValueList(configuration.displayCategoryIds()));

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.DISPLAY_STYLE, configuration.displayStyle());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.DISPLAY_VOCABOLARY_ID, categoryLocalService.getLongValueList(configuration.displayVocabularyIds()));

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.FILTER_COLOUR, configuration.filterColour());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, configuration.internalPortletId());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.INVISIBLE, configuration.invisible());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.MAX_CATEGORIES_TO_SHOW, GetterUtil.getInteger(configuration.maxCategoriesToShow(), 10));

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.MAX_TREE_VIEW_DEPTH, GetterUtil.getInteger(configuration.maxTreeViewDepth(), 3));

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.PRESELECTED_CATEGORY_IDS, categoryLocalService.getLongValueList(configuration.preselectedCategoryIds()));

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.PRESELECTED_VOCABOLARY_ID, categoryLocalService.getLongValueList(configuration.preselectedVocabularyIds()));

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.SHOW_ALL_CATEGORIES_ALWAYS, configuration.showAllCategoriesAlways());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.SHOW_FREQUENCIES, configuration.showFrequencies());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION, configuration.showHeadingAsFirstDropdownOption());

		httpServletRequest.setAttribute(CategoryFacetPortletConfigurationConstants.CATEGORIES_QUERY_ANDED, configuration.categoriesQueryAnded());

		httpServletRequest.setAttribute("availableVocabularies", availableVocabularies);

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		boolean alphaSort = ParamUtil.getBoolean(actionRequest, CategoryFacetPortletConfigurationConstants.ALPHA_SORT, false);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.ALPHA_SORT, String.valueOf(alphaSort));

		boolean checkboxesPanelCollapsedByDefault = ParamUtil.getBoolean(actionRequest, CategoryFacetPortletConfigurationConstants.CHECKBOXES_PANEL_COLLAPSED_BY_DEFAULT);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.CHECKBOXES_PANEL_COLLAPSED_BY_DEFAULT, String.valueOf(checkboxesPanelCollapsedByDefault));

		String customCssClass = ParamUtil.getString(actionRequest, CategoryFacetPortletConfigurationConstants.CUSTOM_CSS_CLASS, StringPool.BLANK);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.CUSTOM_CSS_CLASS, customCssClass);

		String customHeading = ParamUtil.getString(actionRequest, CategoryFacetPortletConfigurationConstants.CUSTOM_HEADING, StringPool.BLANK);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.CUSTOM_HEADING, customHeading);

		String[] displayCategoryIdsSelected = ParamUtil.getStringValues(actionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_CATEGORY_IDS, new String[0]);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_CATEGORY_IDS, displayCategoryIdsSelected);

		String displayStyle = ParamUtil.getString(actionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_STYLE, StringPool.BLANK);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_STYLE, displayStyle);

		String[] displayVocabolaryIdsSelected = ParamUtil.getStringValues(actionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_VOCABOLARY_ID, new String[0]);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_VOCABOLARY_ID, displayVocabolaryIdsSelected);

		String filterColour = ParamUtil.getString(actionRequest, CategoryFacetPortletConfigurationConstants.FILTER_COLOUR, StringPool.BLANK);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.FILTER_COLOUR, filterColour);

		boolean invisible = ParamUtil.getBoolean(actionRequest, CategoryFacetPortletConfigurationConstants.INVISIBLE);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.INVISIBLE, String.valueOf(invisible));

		int maxCategoriesToShow = ParamUtil.getInteger(actionRequest, CategoryFacetPortletConfigurationConstants.MAX_CATEGORIES_TO_SHOW);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.MAX_CATEGORIES_TO_SHOW, String.valueOf(maxCategoriesToShow));

		int maxTreeViewDepth = ParamUtil.getInteger(actionRequest, CategoryFacetPortletConfigurationConstants.MAX_TREE_VIEW_DEPTH);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.MAX_TREE_VIEW_DEPTH, String.valueOf(maxTreeViewDepth));

		String[] preselectedCategoryIdsSelected = ParamUtil.getStringValues(actionRequest, CategoryFacetPortletConfigurationConstants.PRESELECTED_CATEGORY_IDS, new String[0]);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.PRESELECTED_CATEGORY_IDS, preselectedCategoryIdsSelected);

		String[] preselectedVocabolaryIdsSelected = ParamUtil.getStringValues(actionRequest, CategoryFacetPortletConfigurationConstants.PRESELECTED_VOCABOLARY_ID, new String[0]);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.PRESELECTED_VOCABOLARY_ID, preselectedVocabolaryIdsSelected);

		boolean showAllCategoriesAlways = ParamUtil.getBoolean(actionRequest, CategoryFacetPortletConfigurationConstants.SHOW_ALL_CATEGORIES_ALWAYS);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.SHOW_ALL_CATEGORIES_ALWAYS, String.valueOf(showAllCategoriesAlways));

		boolean showFrequencies = ParamUtil.getBoolean(actionRequest, CategoryFacetPortletConfigurationConstants.SHOW_FREQUENCIES);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.SHOW_FREQUENCIES, String.valueOf(showFrequencies));

		boolean showHeadingAsFirstDropdownOption = ParamUtil.getBoolean(actionRequest, CategoryFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION, String.valueOf(showHeadingAsFirstDropdownOption));

		boolean categoriesQueryAnded = ParamUtil.getBoolean(actionRequest, CategoryFacetPortletConfigurationConstants.CATEGORIES_QUERY_ANDED);
		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.CATEGORIES_QUERY_ANDED, String.valueOf(categoriesQueryAnded));

		String internalPortletId = ParamUtil.getString(actionRequest, CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK);

		if (Validator.isNull(internalPortletId)) {
			internalPortletId = StringUtil.randomString(6);
		}

		setPreference(actionRequest, CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, internalPortletId);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
