package com.placecube.digitalplace.userimport.admin.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.userimport.admin.web.constants.PortletKeys;

@Component(immediate = true, property = { //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=digitalplace-user-import", //		
		"com.liferay.portlet.display-category=category.hidden", //
		"com.liferay.portlet.instanceable=false", //
		"javax.portlet.version=3.0", //
		"javax.portlet.name=" + PortletKeys.USER_IMPORT, //
		"javax.portlet.init-param.template-path=/", //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.supports.mime-type=text/html", //
		"javax.portlet.security-role-ref=power-user,user" //
}, service = Portlet.class)
public class UserImportPortlet extends MVCPortlet {
	
}