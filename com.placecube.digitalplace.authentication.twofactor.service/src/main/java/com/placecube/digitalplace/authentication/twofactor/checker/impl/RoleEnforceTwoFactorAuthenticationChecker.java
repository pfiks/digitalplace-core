package com.placecube.digitalplace.authentication.twofactor.checker.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupGroupRoleModel;
import com.liferay.portal.kernel.model.UserGroupRoleModel;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.UserGroupGroupRoleLocalService;
import com.liferay.portal.kernel.service.UserGroupRoleLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.authentication.twofactor.checker.EnforceTwoFactorAuthenticationChecker;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.internal.TwoFactorUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property = { EnforceTwoFactorAuthenticationChecker.CHECKER_ID + "=default.role" }, service = { EnforceTwoFactorAuthenticationChecker.class })
public class RoleEnforceTwoFactorAuthenticationChecker implements EnforceTwoFactorAuthenticationChecker {

	private static final Log LOG = LogFactoryUtil.getLog(RoleEnforceTwoFactorAuthenticationChecker.class);
	private static final long TWO_FACTOR_ENFORCED_FOR_ALL_USERS = -1;

	@Reference
	private RoleLocalService roleLocalService;

	@Reference
	private TwoFactorUtils twoFactorUtils;

	@Reference
	private UserGroupRoleLocalService userGroupRoleLocalService;

	@Reference
	private UserGroupGroupRoleLocalService userGroupGroupRoleLocalService;

	@Override
	public boolean checkIfTwoFactorAuthenticationShouldBeEnforced(User user) {
		try {
			AuthenticationTwoFactorCompanyConfiguration authenticationTwoFactorCompanyConfiguration = twoFactorUtils.getConfiguration(user.getCompanyId());
			long[] enforcedRoleIds = Arrays.stream(authenticationTwoFactorCompanyConfiguration.twoFactorAuthenticationEnforcedRoleIds()).filter(Validator::isNotNull).mapToLong(Long::valueOf)
					.toArray();

			if (enforcedRoleIds.length == 1 && enforcedRoleIds[0] == TWO_FACTOR_ENFORCED_FOR_ALL_USERS) {
				return true;
			} else {
				long userId = user.getUserId();
				List<Long> allRoleIds = new ArrayList<>();
				allRoleIds.addAll(roleLocalService.getUserRoles(userId).stream().map(Role::getRoleId).collect(Collectors.toList()));
				allRoleIds.addAll(userGroupRoleLocalService.getUserGroupRoles(userId).stream().map(UserGroupRoleModel::getRoleId).collect(Collectors.toList()));
				allRoleIds.addAll(userGroupGroupRoleLocalService.getUserGroupGroupRolesByUser(userId).stream().map(UserGroupGroupRoleModel::getRoleId).collect(Collectors.toList()));
				Set<Long> userRoleIds = new HashSet<>(allRoleIds);

				for (long enforcedRoleId : enforcedRoleIds) {
					if (userRoleIds.contains(enforcedRoleId)) {
						return true;
					}
				}
				return false;
			}
		} catch (PortalException exception) {
			LOG.error("Exception getting company configuration - will not enforce 2FA " + exception.getMessage());
			LOG.debug(exception);
			return false;
		}
	}
}
