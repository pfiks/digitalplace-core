package com.placecube.digitalplace.group.rest.internal.triggers;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.placecube.digitalplace.group.rest.constants.WebhookAction;
import com.placecube.digitalplace.group.rest.internal.processor.DDLWebhookProcessor;

@Component(immediate = true, service = ModelListener.class)
public class DDLRecordListener extends BaseModelListener<DDLRecord> {

	@Reference
	private DDLWebhookProcessor ddlWebhookProcessor;

	@Override
	public void onAfterRemove(DDLRecord ddlRecord) {
		ddlWebhookProcessor.processDDLRecordAction(ddlRecord, WebhookAction.DELETE);
	}

}
