package com.placecube.digitalplace.user.settings.web.portlet;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.security.auth.PrincipalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.settings.web.constants.PortletKeys;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.displaycontext.UserSettingsDisplayContext;
import com.placecube.digitalplace.user.settings.web.registry.UserSettingsSectionRegistry;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=user-settings", "com.liferay.portlet.css-class-wrapper=portlet-user-settings",
		"com.liferay.portlet.display-category=category.tools", "com.liferay.portlet.instanceable=false", "javax.portlet.resource-bundle=content.Language", "mvc.command.name=/",
		"com.liferay.portlet.header-portlet-javascript=/js/com.placecube.digitalplace.user.settings.web.js", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.security-role-ref=power-user,user", "javax.portlet.version=3.0",
		"javax.portlet.name=" + PortletKeys.USER_SETTINGS }, service = Portlet.class)
public class UserSettingsPortlet extends MVCPortlet {

	@Reference
	private UserSettingsSectionRegistry userSettingsSectionRegistry;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		if (!themeDisplay.isSignedIn()) {
			throw new PortletException(new PrincipalException("User must be logged in"));
		}

		PortletURL portletURL = renderResponse.createRenderURL();
		portletURL.getRenderParameters().setValue(PortletRequestKeys.CURRENT_SECTION_ID, ParamUtil.getString(renderRequest, PortletRequestKeys.CURRENT_SECTION_ID));
		renderRequest.setAttribute("portletURL", portletURL);

		renderRequest.setAttribute("userSettingsDisplayContext", UserSettingsDisplayContext.initDisplayContext(userSettingsSectionRegistry.getUserSettingsSections(themeDisplay.getUser())));

		super.render(renderRequest, renderResponse);
	}

}