package com.placecube.digitalplace.formadmin.web.service;

import java.io.IOException;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstanceLink;
import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.model.DDMFormInstanceRecord;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureLayout;
import com.liferay.dynamic.data.mapping.model.DDMStructureVersion;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLayoutLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureVersionLocalService;
import com.liferay.exportimport.constants.ExportImportConstants;
import com.liferay.exportimport.kernel.lar.ExportImportPathUtil;
import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.exportimport.kernel.lar.PortletDataHandler;
import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.portlet.PortletIdCodec;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.ReleaseInfo;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.placecube.digitalplace.formadmin.web.constants.FormExportKeys;

@Component(immediate = true, service = FormExportServiceUtil.class)
public class FormExportServiceUtil {

	@Reference
	private DDMDataProviderInstanceLocalService ddmDataProviderInstanceLocalService;

	@Reference
	private DDMStructureLayoutLocalService ddmStructureLayoutLocalService;

	@Reference
	private DDMStructureVersionLocalService ddmStructureVersionLocalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private Portal portal;

	protected void addDDMDataProviderInstanceElement(Element ddmFormAdminPortletDataHandlerElement, List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList) throws PortalException {
		Element ddmDataProviderInstanceElement = ddmFormAdminPortletDataHandlerElement.addElement(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE);
		for (DDMDataProviderInstanceLink ddmDataProviderInstanceLink : ddmDataProviderInstanceLinkList) {
			DDMDataProviderInstance ddmDataProviderInstance = ddmDataProviderInstanceLocalService.getDataProviderInstance(ddmDataProviderInstanceLink.getDataProviderInstanceId());

			Element ddmFormInstanceStagedModelElement = ddmDataProviderInstanceElement.addElement(FormExportKeys.STAGED_MODEL);
			ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.GROUP_ID, String.valueOf(ddmDataProviderInstance.getGroupId()));
			ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.UUID, String.valueOf(ddmDataProviderInstance.getUuid()));
			ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.PATH, getDDMDataProviderInstancePath(ddmDataProviderInstance));
			ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.USER_UUID, String.valueOf(ddmDataProviderInstance.getUserUuid()));
			ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.ASSET_ENTRY_PRIORITY, "0.0");
		}
	}

	protected void addDDMFormInstanceReferencesElement(Element ddmFormInstanceStagedModelElement, DDMStructure ddmStructure, DDMFormInstance ddmFormInstance, long groupId) throws PortalException {
		Element ddmFormInstanceReferencesElement = ddmFormInstanceStagedModelElement.addElement(FormExportKeys.REFERENCES);

		Element ddmFormInstanceReferenceElement = ddmFormInstanceReferencesElement.addElement(FormExportKeys.REFERENCE);
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.CLASS_NAME, ddmStructure.getModelClassName());
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.CLASS_PK, String.valueOf(ddmStructure.getPrimaryKey()));
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.ATTACHED_CLASS_NAME, ddmFormInstance.getModelClassName());
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.GROUP_ID, String.valueOf(ddmStructure.getGroupId()));
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.GROUP_KEY, groupLocalService.getGroup(ddmStructure.getGroupId()).getGroupKey());
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.LIVE_GROUP_ID, String.valueOf(groupId));
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.TYPE, PortletDataContext.REFERENCE_TYPE_STRONG);
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.UUID, ddmStructure.getUuid());
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.COMPANY_ID, String.valueOf(ddmStructure.getCompanyId()));
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.PRELOADED, String.valueOf(Boolean.FALSE));
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.REFERENCED_CLASS_NAME, ddmFormInstance.getModelClassName());
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.STRUCTURE_KEY, ddmStructure.getStructureKey());
		ddmFormInstanceReferenceElement.addAttribute(FormExportKeys.MISSING, String.valueOf(Boolean.FALSE));
	}

	protected Element addDDMFormInstanceStagedModelElement(Element ddmFormInstanceElement, DDMFormInstance ddmFormInstance) {
		Element ddmFormInstanceStagedModelElement = ddmFormInstanceElement.addElement(FormExportKeys.STAGED_MODEL);
		ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.GROUP_ID, String.valueOf(ddmFormInstance.getGroupId()));
		ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.UUID, String.valueOf(ddmFormInstance.getUuid()));
		ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.SETTINGS_DDM_FORM_VALUES_PATH, getDDMFormInstanceSettingsPath(ddmFormInstance));
		ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.PATH, getDDMFormInstancePath(ddmFormInstance));
		ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.USER_UUID, String.valueOf(ddmFormInstance.getUserUuid()));
		ddmFormInstanceStagedModelElement.addAttribute(FormExportKeys.ASSET_ENTRY_PRIORITY, "0.0");
		return ddmFormInstanceStagedModelElement;
	}

	protected void addDDMStructureReferencesElement(Element ddmStructureStagedModelElement, List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList, long groupId) throws PortalException {
		Element ddmStructureReferencesElement = ddmStructureStagedModelElement.addElement(FormExportKeys.REFERENCES);
		for (DDMDataProviderInstanceLink ddmDataProviderInstanceLink : ddmDataProviderInstanceLinkList) {
			DDMDataProviderInstance ddmDataProviderInstance = ddmDataProviderInstanceLocalService.getDataProviderInstance(ddmDataProviderInstanceLink.getDataProviderInstanceId());
			Element ddmStructureReferenceElement = ddmStructureReferencesElement.addElement(FormExportKeys.REFERENCE);
			ddmStructureReferenceElement.addAttribute(FormExportKeys.CLASS_NAME, ddmDataProviderInstance.getModelClassName());
			ddmStructureReferenceElement.addAttribute(FormExportKeys.CLASS_PK, String.valueOf(ddmDataProviderInstance.getPrimaryKey()));
			ddmStructureReferenceElement.addAttribute(FormExportKeys.GROUP_ID, String.valueOf(ddmDataProviderInstance.getGroupId()));
			ddmStructureReferenceElement.addAttribute(FormExportKeys.GROUP_KEY, groupLocalService.getGroup(ddmDataProviderInstance.getGroupId()).getGroupKey());
			ddmStructureReferenceElement.addAttribute(FormExportKeys.LIVE_GROUP_ID, String.valueOf(groupId));
			ddmStructureReferenceElement.addAttribute(FormExportKeys.TYPE, PortletDataContext.REFERENCE_TYPE_STRONG);
			ddmStructureReferenceElement.addAttribute(FormExportKeys.UUID, ddmDataProviderInstance.getUuid());
			ddmStructureReferenceElement.addAttribute(FormExportKeys.COMPANY_ID, String.valueOf(ddmDataProviderInstance.getCompanyId()));
			ddmStructureReferenceElement.addAttribute(FormExportKeys.MISSING, String.valueOf(Boolean.FALSE));
			if (Validator.isNull(ddmStructureStagedModelElement.attributeValue(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS))) {
				ddmStructureStagedModelElement.addAttribute(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS, String.valueOf(ddmDataProviderInstance.getPrimaryKey()));
			} else {
				ddmStructureStagedModelElement.addAttribute(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS,
						ddmStructureStagedModelElement.attributeValue(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS).concat(StringPool.COMMA)
								.concat(String.valueOf(ddmDataProviderInstance.getPrimaryKey())));
			}
		}
	}

	protected Element addDDMStructureStagedModelElement(Element ddmStructureElement, DDMStructure ddmStructure, DDMFormInstance ddmFormInstance) {
		Element ddmStructureStagedModelElement = ddmStructureElement.addElement(FormExportKeys.STAGED_MODEL);
		ddmStructureStagedModelElement.addAttribute(FormExportKeys.GROUP_ID, String.valueOf(ddmStructure.getGroupId()));
		ddmStructureStagedModelElement.addAttribute(FormExportKeys.UUID, ddmStructure.getUuid());
		ddmStructureStagedModelElement.addAttribute(FormExportKeys.DDM_FORM_PATH, getDDMStructureDDMFormPath(ddmStructure));
		ddmStructureStagedModelElement.addAttribute(FormExportKeys.DDM_DATA_PROVIDER_INSTANCE_IDS, StringPool.BLANK);
		ddmStructureStagedModelElement.addAttribute(FormExportKeys.DDM_FORM_LAYOUT_PATH, getDDMStructureDDMFormLayoutPath(ddmStructure));
		ddmStructureStagedModelElement.addAttribute(FormExportKeys.PATH, getDDMStructurePath(ddmStructure));
		ddmStructureStagedModelElement.addAttribute(FormExportKeys.ATTACHED_CLASS_NAME, ddmFormInstance.getModelClassName());
		ddmStructureStagedModelElement.addAttribute(FormExportKeys.USER_UUID, ddmFormInstance.getUserUuid());
		ddmStructureStagedModelElement.addAttribute(FormExportKeys.ASSET_ENTRY_PRIORITY, "0.0");
		return ddmStructureStagedModelElement;
	}

	protected void addHeaderElement(Element rootElement, Group userPersonalSiteGroup, Group companyGroup, long companyId, long groupId, String portletId) {
		Element headerElement = rootElement.addElement(FormExportKeys.HEADER);
		headerElement.addAttribute(FormExportKeys.AVAILABLE_LOCALES, StringUtil.merge(LanguageUtil.getAvailableLocales(portal.getSiteGroupId(groupId))));
		headerElement.addAttribute(FormExportKeys.BUILD_NUMBER, String.valueOf(ReleaseInfo.getBuildNumber()));
		headerElement.addAttribute(FormExportKeys.EXPORT_DATE, Time.getRFC822());
		headerElement.addAttribute(FormExportKeys.TYPE, FormExportKeys.PORTLET);
		headerElement.addAttribute(FormExportKeys.COMPANY_ID, String.valueOf(companyId));
		headerElement.addAttribute(FormExportKeys.COMPANY_GROUP_ID, String.valueOf(companyGroup.getGroupId()));
		headerElement.addAttribute(FormExportKeys.GROUP_ID, String.valueOf(groupId));
		headerElement.addAttribute(FormExportKeys.USER_PERSONAL_SITE_GROUP_ID, String.valueOf(userPersonalSiteGroup.getGroupId()));
		headerElement.addAttribute(FormExportKeys.PRIVATE_LAYOUT, String.valueOf(Boolean.TRUE));
		headerElement.addAttribute(FormExportKeys.ROOT_PORTLET_ID, PortletIdCodec.decodePortletName(portletId));
		headerElement.addAttribute(FormExportKeys.SCHEMA_VERSION, ExportImportConstants.EXPORT_IMPORT_SCHEMA_VERSION);
	}

	protected void addManifestSummaryElement(Element rootElement, int ddmDataProviderInstanceLinksLength) {
		Element manifestSummaryElement = rootElement.addElement(FormExportKeys.MANIFEST_SUMMARY);

		Element stagedModelDDMDataProviderInstanceElement = manifestSummaryElement.addElement(FormExportKeys.STAGED_MODEL);
		stagedModelDDMDataProviderInstanceElement.addAttribute(FormExportKeys.MANIFEST_SUMMARY_KEY, DDMDataProviderInstance.class.getName());
		if (ddmDataProviderInstanceLinksLength > 0) {
			stagedModelDDMDataProviderInstanceElement.addAttribute(FormExportKeys.ADDITION_COUNT, String.valueOf(ddmDataProviderInstanceLinksLength));
		}

		Element stagedModelDDMStructureElement = manifestSummaryElement.addElement(FormExportKeys.STAGED_MODEL);
		stagedModelDDMStructureElement.addAttribute(FormExportKeys.MANIFEST_SUMMARY_KEY, DDMStructure.class.getName() + StringPool.POUND + DDMFormInstance.class.getName());
		stagedModelDDMStructureElement.addAttribute(FormExportKeys.ADDITION_COUNT, "1");

		Element stagedModelDDMFormInstanceRecordElement = manifestSummaryElement.addElement(FormExportKeys.STAGED_MODEL);
		stagedModelDDMFormInstanceRecordElement.addAttribute(FormExportKeys.MANIFEST_SUMMARY_KEY, DDMFormInstanceRecord.class.getName());

		Element stagedModelDDMFormInstanceElement = manifestSummaryElement.addElement(FormExportKeys.STAGED_MODEL);
		stagedModelDDMFormInstanceElement.addAttribute(FormExportKeys.MANIFEST_SUMMARY_KEY, DDMFormInstance.class.getName());
		stagedModelDDMFormInstanceElement.addAttribute(FormExportKeys.ADDITION_COUNT, "1");

	}

	protected void addPortletElement(Element rootElement, Portlet portlet, PortletDataContext portletDataContext) {
		Element portletElement = rootElement.addElement(FormExportKeys.PORTLET);
		portletElement.addAttribute(FormExportKeys.PORTLET_ID, portlet.getPortletId());
		portletElement.addAttribute(FormExportKeys.LAYOUT_ID, "1");
		String portletXMLPath = StringBundler.concat(ExportImportPathUtil.getPortletPath(portletDataContext), StringPool.SLASH, "1", "/portlet.xml");
		portletElement.addAttribute(FormExportKeys.PATH, portletXMLPath);
		portletElement.addAttribute(FormExportKeys.PORTLET_DATA, String.valueOf(Boolean.TRUE));
		PortletDataHandler portletDataHandler = portlet.getPortletDataHandlerInstance();
		portletElement.addAttribute(FormExportKeys.SCHEMA_VERSION, portletDataHandler.getSchemaVersion());
		portletElement.addAttribute(FormExportKeys.PORTLET_CONFIGURATION, "setup");
	}

	protected void createDDMDataProviderInstanceXML(List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList, PortletDataContext portletDataContext) throws PortalException {
		for (DDMDataProviderInstanceLink ddmDataProviderInstanceLink : ddmDataProviderInstanceLinkList) {
			Document ddmDataProviderInstanceXML = SAXReaderUtil.createDocument();

			Element root = ddmDataProviderInstanceXML.addElement(FormExportKeys.ROOT);
			DDMDataProviderInstance ddmDataProviderInstance = ddmDataProviderInstanceLocalService.getDataProviderInstance(ddmDataProviderInstanceLink.getDataProviderInstanceId());
			portletDataContext.addClassedModel(root, ExportImportPathUtil.getModelPath(ddmDataProviderInstance), ddmDataProviderInstance);
		}
	}

	protected void createDDMFormInstanceXML(PortletDataContext portletDataContext, DDMFormInstance ddmFormInstance) throws PortalException {
		Document ddmFormInstanceXML = SAXReaderUtil.createDocument();

		Element root = ddmFormInstanceXML.addElement(FormExportKeys.ROOT);

		portletDataContext.addClassedModel(root, ExportImportPathUtil.getModelPath(ddmFormInstance), ddmFormInstance);
	}

	protected void createDDMFormJSON(PortletDataContext portletDataContext, DDMStructure ddmStructure) {
		portletDataContext.addZipEntry(getDDMStructureDDMFormPath(ddmStructure), ddmStructure.getDefinition());
	}

	protected void createDDMFormLayoutJSON(PortletDataContext portletDataContext, DDMStructure ddmStructure) throws PortalException {
		DDMStructureVersion ddmStructureVersion = ddmStructureVersionLocalService.getLatestStructureVersion(ddmStructure.getStructureId());
		DDMStructureLayout ddmStructureLayout = ddmStructureLayoutLocalService.getStructureLayoutByStructureVersionId(ddmStructureVersion.getStructureVersionId());

		portletDataContext.addZipEntry(getDDMStructureDDMFormLayoutPath(ddmStructure), ddmStructureLayout.getDefinition());
	}

	protected void createDDMStructureXML(PortletDataContext portletDataContext, DDMStructure ddmStructure) throws PortalException {
		Document ddmStructureXML = SAXReaderUtil.createDocument();

		Element root = ddmStructureXML.addElement(FormExportKeys.ROOT);

		portletDataContext.addClassedModel(root, ExportImportPathUtil.getModelPath(ddmStructure), ddmStructure);
	}

	protected void createManifestXML(Portlet portlet, PortletDataContext portletDataContext, long groupId, long companyId, String portletId, int ddmDataProviderInstanceLinksLength)
			throws IOException, PortalException {
		Group companyGroup = groupLocalService.fetchCompanyGroup(companyId);
		Group userPersonalSiteGroup = groupLocalService.fetchUserPersonalSiteGroup(companyId);

		Document document = SAXReaderUtil.createDocument();

		Element rootElement = document.addElement(FormExportKeys.ROOT);

		addHeaderElement(rootElement, userPersonalSiteGroup, companyGroup, companyId, groupId, portletId);
		rootElement.addElement(FormExportKeys.MISSING_REFERENCES);
		addPortletElement(rootElement, portlet, portletDataContext);
		addManifestSummaryElement(rootElement, ddmDataProviderInstanceLinksLength);

		portletDataContext.addZipEntry("manifest.xml", document.formattedString());
	}

	protected void createPortletDataXml(Portlet portlet, PortletDataContext portletDataContext, long groupId, DDMFormInstance ddmFormInstance, DDMStructure ddmStructure,
			List<DDMDataProviderInstanceLink> ddmDataProviderInstanceLinkList) throws PortalException, IOException {

		Document document = SAXReaderUtil.createDocument();

		Element ddmFormAdminPortletDataHandlerElement = document.addElement(FormExportKeys.DDM_FORM_ADMIN_PORTLET_DATA_HANDLER);
		ddmFormAdminPortletDataHandlerElement.addAttribute(FormExportKeys.SELF_PATH, getPortletDataPath(portlet, groupId));

		if (!ddmDataProviderInstanceLinkList.isEmpty()) {
			addDDMDataProviderInstanceElement(ddmFormAdminPortletDataHandlerElement, ddmDataProviderInstanceLinkList);
		}

		Element ddmFormInstanceElement = ddmFormAdminPortletDataHandlerElement.addElement(FormExportKeys.DDM_FORM_INSTANCE);
		Element ddmFormInstanceStagedModelElement = addDDMFormInstanceStagedModelElement(ddmFormInstanceElement, ddmFormInstance);
		addDDMFormInstanceReferencesElement(ddmFormInstanceStagedModelElement, ddmStructure, ddmFormInstance, groupId);
		Element ddmStructureElement = ddmFormAdminPortletDataHandlerElement.addElement(FormExportKeys.DDM_STRUCTURE);
		Element ddmStructureStagedModelElement = addDDMStructureStagedModelElement(ddmStructureElement, ddmStructure, ddmFormInstance);

		if (!ddmDataProviderInstanceLinkList.isEmpty()) {
			addDDMStructureReferencesElement(ddmStructureStagedModelElement, ddmDataProviderInstanceLinkList, groupId);
		}

		portletDataContext.addZipEntry(getPortletDataPath(portlet, groupId), document.formattedString());
	}

	protected void createPortletXML(Portlet portlet, long groupId, PortletDataContext portletDataContext) throws IOException {
		Document document = SAXReaderUtil.createDocument();

		Element portletElement = document.addElement(FormExportKeys.PORTLET);
		portletElement.addAttribute(FormExportKeys.PORTLET_ID, portlet.getPortletId());
		portletElement.addAttribute(FormExportKeys.ROOT_PORTLET_ID, portlet.getRootPortletId());
		portletElement.addAttribute(FormExportKeys.SCOPE_GROUP_ID, String.valueOf(groupId));
		portletElement.addAttribute(FormExportKeys.SCOPE_LAYOUT_TYPE, StringPool.BLANK);
		portletElement.addAttribute(FormExportKeys.SCOPE_LAYOUT_UUID, StringPool.BLANK);
		portletElement.addAttribute(FormExportKeys.PRIVATE_LAYOUT, String.valueOf(Boolean.FALSE));
		String portletPath = getPortletPath(portlet, groupId);
		portletElement.addAttribute(FormExportKeys.SELF_PATH, portletPath);

		Element portletDataElement = portletElement.addElement(FormExportKeys.PORTLET_DATA);
		String portletDaraPath = getPortletDataPath(portlet, groupId);
		portletDataElement.addAttribute(FormExportKeys.PATH, portletDaraPath);

		portletDataContext.addZipEntry(portletPath, document.formattedString());
	}

	protected void createSettingsDDMFormValuesJSON(PortletDataContext portletDataContext, DDMFormInstance ddmFormInstance) {
		String settingsDDMFormValuesPath = ExportImportPathUtil.getModelPath(ddmFormInstance, "settings-ddm-form-values.json");
		portletDataContext.addZipEntry(settingsDDMFormValuesPath, ddmFormInstance.getSettings());
	}

	private String getDDMDataProviderInstancePath(DDMDataProviderInstance ddmDataProviderInstance) {
		return StringBundler.concat(StringPool.FORWARD_SLASH, FormExportKeys.GROUP, StringPool.FORWARD_SLASH, String.valueOf(ddmDataProviderInstance.getGroupId()), StringPool.FORWARD_SLASH,
				ddmDataProviderInstance.getModelClassName(), StringPool.FORWARD_SLASH, String.valueOf(ddmDataProviderInstance.getDataProviderInstanceId()), ".xml");
	}

	private String getDDMFormInstancePath(DDMFormInstance ddmFormInstance) {
		return StringBundler.concat(StringPool.FORWARD_SLASH, FormExportKeys.GROUP, StringPool.FORWARD_SLASH, String.valueOf(ddmFormInstance.getGroupId()), StringPool.FORWARD_SLASH,
				ddmFormInstance.getModelClassName(), StringPool.FORWARD_SLASH, String.valueOf(ddmFormInstance.getFormInstanceId()), ".xml");
	}

	private String getDDMFormInstanceSettingsPath(DDMFormInstance ddmFormInstance) {
		return StringBundler.concat(StringPool.FORWARD_SLASH, FormExportKeys.GROUP, StringPool.FORWARD_SLASH, String.valueOf(ddmFormInstance.getGroupId()), StringPool.FORWARD_SLASH,
				ddmFormInstance.getModelClassName(), StringPool.FORWARD_SLASH, String.valueOf(ddmFormInstance.getFormInstanceId()), StringPool.FORWARD_SLASH, "settings-ddm-form-values.json");
	}

	private String getDDMStructureDDMFormLayoutPath(DDMStructure ddmStructure) {
		return StringBundler.concat(StringPool.FORWARD_SLASH, FormExportKeys.GROUP, StringPool.FORWARD_SLASH, String.valueOf(ddmStructure.getGroupId()), StringPool.FORWARD_SLASH,
				ddmStructure.getModelClassName(), StringPool.FORWARD_SLASH, String.valueOf(ddmStructure.getStructureId()), StringPool.FORWARD_SLASH, "ddm-form-layout.json");
	}

	private String getDDMStructureDDMFormPath(DDMStructure ddmStructure) {
		return StringBundler.concat(StringPool.FORWARD_SLASH, FormExportKeys.GROUP, StringPool.FORWARD_SLASH, String.valueOf(ddmStructure.getGroupId()), StringPool.FORWARD_SLASH,
				ddmStructure.getModelClassName(), StringPool.FORWARD_SLASH, String.valueOf(ddmStructure.getStructureId()), StringPool.FORWARD_SLASH, "ddm-form.json");
	}

	private String getDDMStructurePath(DDMStructure ddmStructure) {
		return StringBundler.concat(StringPool.FORWARD_SLASH, FormExportKeys.GROUP, StringPool.FORWARD_SLASH, String.valueOf(ddmStructure.getGroupId()), StringPool.FORWARD_SLASH,
				ddmStructure.getModelClassName(), StringPool.FORWARD_SLASH, String.valueOf(ddmStructure.getStructureId()), ".xml");
	}

	private String getPortletDataPath(Portlet portlet, long groupId) {
		return StringBundler.concat(StringPool.FORWARD_SLASH, FormExportKeys.GROUP, StringPool.FORWARD_SLASH, String.valueOf(groupId), StringPool.FORWARD_SLASH,
				ExportImportPathUtil.PATH_PREFIX_PORTLET, StringPool.FORWARD_SLASH, portlet.getPortletId(), StringPool.FORWARD_SLASH, String.valueOf(groupId), StringPool.FORWARD_SLASH,
				"portlet-data.xml");
	}

	private String getPortletPath(Portlet portlet, long groupId) {
		return StringBundler.concat(StringPool.FORWARD_SLASH, FormExportKeys.GROUP, StringPool.FORWARD_SLASH, String.valueOf(groupId), StringPool.FORWARD_SLASH,
				ExportImportPathUtil.PATH_PREFIX_PORTLET, StringPool.FORWARD_SLASH, portlet.getPortletId(), StringPool.FORWARD_SLASH, "1", StringPool.FORWARD_SLASH, "portlet.xml");
	}
}
