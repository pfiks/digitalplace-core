package com.placecube.digitalplace.externalpage.search.result.contributor;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

public class ExternalPageModelVisibilityContributorTest extends PowerMockito {

	private ExternalPageModelVisibilityContributor externalPageModelVisibilityContributor = new ExternalPageModelVisibilityContributor();

	@Test
	public void isVisible_ThenReturnsTrue() {
		boolean result = externalPageModelVisibilityContributor.isVisible(12l, 2);

		assertTrue(result);
	}

}
