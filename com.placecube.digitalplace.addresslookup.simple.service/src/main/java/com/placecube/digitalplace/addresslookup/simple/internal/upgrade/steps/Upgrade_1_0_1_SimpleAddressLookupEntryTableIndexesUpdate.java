package com.placecube.digitalplace.addresslookup.simple.internal.upgrade.steps;

import java.io.IOException;
import java.sql.SQLException;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class Upgrade_1_0_1_SimpleAddressLookupEntryTableIndexesUpdate extends UpgradeProcess {

	public Upgrade_1_0_1_SimpleAddressLookupEntryTableIndexesUpdate() {
	}

	@Override
	protected void doUpgrade() throws Exception {
		if (hasTable("Placecube_AddressLookupSimple_SimpleAddressLookupEntry")) {

			createNewIndex();

			dropLegacyIndexImmediately();
		}
	}

	private void createNewIndex() throws Exception, IOException, SQLException {
		if (!hasIndex("Placecube_AddressLookupSimple_SimpleAddressLookupEntry", "IX_7DB546D6")) {
			runSQL("create index IX_7DB546D6 on Placecube_AddressLookupSimple_SimpleAddressLookupEntry (uuid_[$COLUMN_LENGTH:75$]);");
		}
	}

	private void dropLegacyIndexImmediately() throws Exception {
		if (hasIndex("Placecube_AddressLookupSimple_SimpleAddressLookupEntry", "IX_2E5D3B92")) {
			runSQL(connection, "drop index IX_2E5D3B92 on Placecube_AddressLookupSimple_SimpleAddressLookupEntry;");
		}
	}

}
