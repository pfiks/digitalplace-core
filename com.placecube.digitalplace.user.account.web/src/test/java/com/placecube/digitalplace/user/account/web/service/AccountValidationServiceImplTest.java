package com.placecube.digitalplace.user.account.web.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.captcha.util.CaptchaUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.UserPasswordException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.PasswordPolicy;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.service.Snapshot;
import com.liferay.portal.kernel.service.PasswordPolicyLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.security.pwd.PwdToolkitUtil;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.constants.AccountValidatedField;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.CaptchaValidationService;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LanguageUtil.class, PropsUtil.class, Snapshot.class, CaptchaUtil.class, PortalUtil.class, PwdToolkitUtil.class, AggregatedResourceBundleUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@SuppressStaticInitializationFor({ "com.liferay.portal.security.pwd.PwdToolkitUtil" })
public class AccountValidationServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;

	private static final String EMPTY = "";

	private static final String NOT_EMPTY = "not-empty-value";

	@InjectMocks
	private AccountValidationServiceImpl accountValidationServiceImpl;

	private final Locale locale = Locale.ENGLISH;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private CaptchaValidationService mockCaptchaValidationService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private UserAccountField mockCreateUserAccountField;

	@Mock
	private ExpandoField mockExpandoField;

	@Mock
	private PasswordPolicy mockPasswordPolicy;

	@Mock
	private PasswordPolicyLocalService mockPasswordPolicyLocalService;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserAccountFieldRegistry mockuserAccountFieldRegistry;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Before
	public void activateSetup() {
		initMocks(this);
		mockStatic(LanguageUtil.class, PropsUtil.class, Snapshot.class, CaptchaUtil.class, PortalUtil.class, PwdToolkitUtil.class, AggregatedResourceBundleUtil.class);
	}

	@Test
	public void validateAdditionalDetails_WhenAnExpandoFieldIsEmptyAndNotRequired_ThenNoValidationDone() throws Exception {
		Set<ExpandoField> expandoFields = new HashSet<>();
		expandoFields.add(mockExpandoField);
		mockBasicSetup();
		when(mockAccountConfigurationService.getConfiguredExpandoFields(mockAccountContext, mockUserProfileFieldsConfiguration)).thenReturn(expandoFields);
		when(mockExpandoField.isRequired()).thenReturn(false);
		when(mockExpandoField.getExpandoFieldKey()).thenReturn("expandoKey");
		when(mockAccountContext.getExpandoValue(mockExpandoField.getExpandoFieldKey())).thenReturn(EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).addFieldError(same("expando--" + mockExpandoField.getExpandoFieldKey()), anyString());
	}

	@Test
	public void validateAdditionalDetails_WhenAnExpandoFieldIsEmptyAndRequired_ThenAddRequiredErrorMessage() throws Exception {
		Set<ExpandoField> expandoFields = new HashSet<>();
		expandoFields.add(mockExpandoField);
		mockBasicSetup();
		when(mockAccountConfigurationService.getConfiguredExpandoFields(mockAccountContext, mockUserProfileFieldsConfiguration)).thenReturn(expandoFields);
		when(mockExpandoField.isRequired()).thenReturn(true);
		when(mockExpandoField.getExpandoFieldKey()).thenReturn("expandoKey");
		when(mockAccountContext.getExpandoValue(mockExpandoField.getExpandoFieldKey())).thenReturn(EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError("expando--" + mockExpandoField.getExpandoFieldKey(), "this-field-is-required");
	}

	@Test
	public void validateAdditionalDetails_WhenBusinessPhoneIsRequiredAndEmpty_ThenAddRequiredErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberRequired()).thenReturn(true);
		when(mockAccountContext.getBusinessPhoneNumber()).thenReturn(EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(same(AccountValidatedField.BUSINESS_PHONE.getFieldName()), contains("required"));
	}

	@Test
	public void validateAdditionalDetails_WhenBusinessPhoneIsRequiredAndNotEmptyOrTooLong_ThenNoErrorsAdded() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberRequired()).thenReturn(true);
		when(mockAccountContext.getBusinessPhoneNumber()).thenReturn(NOT_EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).addFieldError(same(AccountValidatedField.BUSINESS_PHONE.getFieldName()), anyString());
	}

	@Test
	public void validateAdditionalDetails_WhenBusinessPhoneNumberDisabled_ThenNoValidationDone() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(false);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).getBusinessPhoneNumber();
	}

	@Test
	public void validateAdditionalDetails_WhenBusinessPhoneTooLong_ThenAddMaxLengthErrorMessage() throws Exception {
		String expected = "expectedVal";
		mockBasicSetup();
		when(LanguageUtil.format(locale, "please-enter-no-more-than-x-characters", AccountValidatedField.BUSINESS_PHONE.getMaxLength())).thenReturn(expected);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.businessPhoneNumberRequired()).thenReturn(true);
		when(mockAccountContext.getBusinessPhoneNumber()).thenReturn(NOT_EMPTY + new String(new char[AccountValidatedField.BUSINESS_PHONE.getMaxLength()]));

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.BUSINESS_PHONE.getFieldName(), expected);
	}

	@Test
	public void validateAdditionalDetails_WhenEmailAddressIsOnPageAndEnabledAndCaptchaErrorIsInContext_ThenDoesNotValidateEmailAddress() throws Exception {
		mockBasicSetup();
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);
		when(mockAccountContext.hasFieldError(AccountValidatedField.CAPTCHA.getFieldName())).thenReturn(true);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).getEmailAddress();
		verify(mockAccountContext, never()).getConfirmEmailAddress();
	}

	@Test
	public void validateAdditionalDetails_WhenEmailAddressIsOnPageAndEnabledAndCaptchaErrorIsNotInContext_ThenValidateEmailAddress() throws Exception {
		mockBasicSetup();
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);
		when(mockAccountContext.hasFieldError(AccountValidatedField.CAPTCHA.getFieldName())).thenReturn(false);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).getEmailAddress();
		verify(mockAccountContext, times(1)).getConfirmEmailAddress();
	}

	@Test
	public void validateAdditionalDetails_WhenEmailAddressNotOnPage_ThenDoNotValidateEmailAddress() throws Exception {
		mockBasicSetup();
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn("not-on-this-page");

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).getEmailAddress();
		verify(mockAccountContext, never()).getConfirmEmailAddress();
	}

	@Test
	public void validateAdditionalDetails_WhenHomePhoneDisabled_ThenNoValidationDone() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(false);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).getHomePhoneNumber();
	}

	@Test
	public void validateAdditionalDetails_WhenHomePhoneIsRequiredAndEmpty_ThenAddRequiredErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberRequired()).thenReturn(true);
		when(mockAccountContext.getHomePhoneNumber()).thenReturn(EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(same(AccountValidatedField.HOME_PHONE.getFieldName()), contains("required"));
	}

	@Test
	public void validateAdditionalDetails_WhenHomePhoneIsRequiredAndNotEmptyOrTooLong_ThenNoErrorsAdded() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberRequired()).thenReturn(true);
		when(mockAccountContext.getHomePhoneNumber()).thenReturn("not-empty");

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).addFieldError(same(AccountValidatedField.HOME_PHONE.getFieldName()), anyString());
	}

	@Test
	public void validateAdditionalDetails_WhenHomePhoneTooLong_ThenAddMaxLengthErrorMessage() throws Exception {
		mockBasicSetup();
		String expected = "expectedVal";
		when(LanguageUtil.format(locale, "please-enter-no-more-than-x-characters", AccountValidatedField.HOME_PHONE.getMaxLength())).thenReturn(expected);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.homePhoneNumberRequired()).thenReturn(true);
		when(mockAccountContext.getHomePhoneNumber()).thenReturn(NOT_EMPTY + new String(new char[AccountValidatedField.HOME_PHONE.getMaxLength()]));

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.HOME_PHONE.getFieldName(), expected);
	}

	@Test
	public void validateAdditionalDetails_WhenJobTitleDisabled_ThenNoValidationDone() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(false);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).getJobTitle();
	}

	@Test
	public void validateAdditionalDetails_WhenJobTitleIsRequiredAndEmpty_ThenAddRequiredErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.jobTitleRequired()).thenReturn(true);
		when(mockAccountContext.getJobTitle()).thenReturn(EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(same(AccountValidatedField.JOB_TITLE.getFieldName()), contains("required"));
	}

	@Test
	public void validateAdditionalDetails_WhenJobTitleIsRequiredAndNotEmptyOrTooLong_ThenNoErrorsAdded() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.jobTitleRequired()).thenReturn(true);
		when(mockAccountContext.getJobTitle()).thenReturn(NOT_EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).addFieldError(same(AccountValidatedField.JOB_TITLE.getFieldName()), anyString());
	}

	@Test
	public void validateAdditionalDetails_WhenJobTitleTooLong_ThenAddMaxLengthErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.jobTitleEnabled()).thenReturn(true);
		when(mockAccountContext.getJobTitle()).thenReturn(NOT_EMPTY + new String(new char[AccountValidatedField.JOB_TITLE.getMaxLength()]));
		String expected = "expectedVal";
		when(LanguageUtil.format(locale, "please-enter-no-more-than-x-characters", AccountValidatedField.JOB_TITLE.getMaxLength())).thenReturn(expected);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.JOB_TITLE.getFieldName(), expected);
	}

	@Test
	public void validateAdditionalDetails_WhenMobilePhoneDisabled_ThenNoValidationDone() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(false);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).getMobilePhoneNumber();
	}

	@Test
	public void validateAdditionalDetails_WhenMobilePhoneIsRequiredAndEmpty_ThenAddRequiredErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberRequired()).thenReturn(true);
		when(mockAccountContext.getMobilePhoneNumber()).thenReturn(EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(same(AccountValidatedField.MOBILE_PHONE.getFieldName()), contains("required"));
	}

	@Test
	public void validateAdditionalDetails_WhenMobilePhoneIsRequiredAndNotEmptyOrTooLong_ThenNoErrorsAdded() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberRequired()).thenReturn(true);
		when(mockAccountContext.getMobilePhoneNumber()).thenReturn(NOT_EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).addFieldError(same(AccountValidatedField.MOBILE_PHONE.getFieldName()), anyString());
	}

	@Test
	public void validateAdditionalDetails_WhenMobilePhoneTooLong_ThenAddMaxLengthErrorMessage() throws Exception {
		String expected = "expectedValue";
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberEnabled()).thenReturn(true);
		when(mockUserProfileFieldsConfiguration.mobilePhoneNumberRequired()).thenReturn(true);
		when(mockAccountContext.getMobilePhoneNumber()).thenReturn(NOT_EMPTY + new String(new char[AccountValidatedField.MOBILE_PHONE.getMaxLength()]));
		when(LanguageUtil.format(locale, "please-enter-no-more-than-x-characters", AccountValidatedField.MOBILE_PHONE.getMaxLength())).thenReturn(expected);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.MOBILE_PHONE.getFieldName(), expected);
	}

	@Test
	public void validateAdditionalDetails_WhenNoErrors_ThenValidateConfiguredExpandoFields() throws Exception {
		mockBasicSetup();

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountConfigurationService, times(1)).getConfiguredExpandoFields(mockAccountContext, mockUserProfileFieldsConfiguration);
	}

	@Test
	public void validateAdditionalDetails_WhenNoErrors_ThenValidateUserAccountFields() throws Exception {
		mockBasicSetup();

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockuserAccountFieldRegistry, times(1)).getUserAccountFieldsByStep(AccountCreationStep.ADDITIONAL_DETAILS, COMPANY_ID);
	}

	@Test
	public void validateCaptcha_WhenCaptchaIsNotEnabled_ThenDoNothing() throws UserAccountException {
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
		when(mockAccountContextService.isShowCaptchaOnEmailStep(mockAccountContext, mockPortletRequest)).thenReturn(false);

		accountValidationServiceImpl.validateCaptcha(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		verify(mockCaptchaValidationService, times(0)).validateCaptcha(mockAccountContext, AccountValidatedField.CAPTCHA, mockPortletRequest);
	}

	@Test
	public void validateCaptcha_WhenEmailIsNotOnTheCurrentPage_ThenDoNothing() throws UserAccountException {
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		accountValidationServiceImpl.validateCaptcha(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		verify(mockCaptchaValidationService, times(0)).validateCaptcha(mockAccountContext, AccountValidatedField.CAPTCHA, mockPortletRequest);
	}

	@Test(expected = UserAccountException.class)
	public void validateCaptcha_WhenErrorOnIsShowCaptchaMethod_ThenThrowException() throws UserAccountException {
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
		when(mockAccountContextService.isShowCaptchaOnEmailStep(mockAccountContext, mockPortletRequest)).thenThrow(new UserAccountException());

		accountValidationServiceImpl.validateCaptcha(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
	}

	@Test
	public void validateCaptcha_WhenIsShowCaptchaEnabled_ThenValidateCaptcha() throws UserAccountException {
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		when(mockAccountContextService.isShowCaptchaOnEmailStep(mockAccountContext, mockPortletRequest)).thenReturn(true);

		accountValidationServiceImpl.validateCaptcha(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
		verify(mockCaptchaValidationService, times(1)).validateCaptcha(mockAccountContext, AccountValidatedField.CAPTCHA, mockPortletRequest);
	}

	@Test
	public void validateCreateUserAccountFields_WhenCreateUserAccountDependentFieldIsEmailAndEmailAdderessFieldIsNotOnPage_ThenNoValidationDone() throws UserAccountException {
		mockBasicSetup();
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);
		Map<String, String> dependentFields = new HashMap<>();
		dependentFields.put("emailAddress", "error-key");
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn("emailAddress");
		when(mockAccountContext.getExpandoValue("emailAddress")).thenReturn("email");
		when(mockCreateUserAccountField.getDependentRequiredFieldNamesAndMessage(anyString(), same(locale))).thenReturn(dependentFields);
		when(ParamUtil.getString(mockPortletRequest, "emailAddress")).thenReturn(NOT_EMPTY);

		accountValidationServiceImpl.validateCreateUserAccountFields(mockAccountContext, createUserAccountFields, mockPortletRequest, false);

		verify(mockAccountContext, never()).addFieldError(same("emailAddress"), anyString());
	}

	@Test
	public void validateCreateUserAccountFields_WhenCreateUserAccountDependentFieldIsEmailAndEmailAddressFieldIsOnThePageEmpty_ThenAddRequiredErrorMessageForDependentField()
			throws UserAccountException {
		mockBasicSetup();
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);
		Map<String, String> dependentFields = new HashMap<>();
		dependentFields.put("emailAddress", "error-key");
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn("emailAddress");
		when(mockAccountContext.getExpandoValue("emailAddress")).thenReturn("email");
		when(mockCreateUserAccountField.getDependentRequiredFieldNamesAndMessage(anyString(), same(locale))).thenReturn(dependentFields);
		when(ParamUtil.getString(mockPortletRequest, "emailAddress")).thenReturn(EMPTY);

		accountValidationServiceImpl.validateCreateUserAccountFields(mockAccountContext, createUserAccountFields, mockPortletRequest, true);

		verify(mockAccountContext, times(1)).addFieldError(same("emailAddress"), anyString());
	}

	@Test
	public void validateCreateUserAccountFields_WhenCreateUserAccountDependentFieldIsEmailAndItIsEmpty_ThenValidationDone() throws UserAccountException {
		mockBasicSetup();
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);
		Map<String, String> dependentFields = new HashMap<>();
		dependentFields.put("emailAddress", "error-key");
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn("emailAddress");
		when(mockAccountContext.getExpandoValue("emailAddress")).thenReturn("email");
		when(mockCreateUserAccountField.getDependentRequiredFieldNamesAndMessage(anyString(), same(locale))).thenReturn(dependentFields);
		when(ParamUtil.getString(mockPortletRequest, "emailAddress")).thenReturn(EMPTY);

		accountValidationServiceImpl.validateCreateUserAccountFields(mockAccountContext, createUserAccountFields, mockPortletRequest, false);

		verify(mockAccountContext, times(1)).addFieldError(same("emailAddress"), anyString());
	}

	@Test
	@Parameters({ "true", "false" })
	public void validateCreateUserAccountFields_WhenCreateUserAccountDependentFieldsAreEmpty_ThenNoValidationDone(boolean isEmailAddressFieldOnPage) throws UserAccountException {
		mockBasicSetup();
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);
		Map<String, String> dependentFields = new HashMap<>();
		dependentFields.put("homePhoneNumber", "error-key");
		when(mockCreateUserAccountField.getDependentRequiredFieldNamesAndMessage(anyString(), same(locale))).thenReturn(dependentFields);
		when(ParamUtil.getString(mockPortletRequest, "homePhoneNumber")).thenReturn(NOT_EMPTY);

		accountValidationServiceImpl.validateCreateUserAccountFields(mockAccountContext, createUserAccountFields, mockPortletRequest, isEmailAddressFieldOnPage);

		verify(mockAccountContext, never()).addFieldError(same("homePhoneNumber"), anyString());
	}

	@Test
	@Parameters({ "true", "false" })
	public void validateCreateUserAccountFields_WhenCreateUserAccountFieldIsRequiredAndEmpty_ThenAddRequiredErrorMessage(boolean isEmailAddressFieldOnPage) throws UserAccountException {
		String fieldName = "fieldName";
		String errorMessage = "error";
		mockBasicSetup();
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);

		when(mockCreateUserAccountField.getRequiredMessage(locale)).thenReturn(errorMessage);
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn(fieldName);
		when(mockCreateUserAccountField.isRequired(COMPANY_ID)).thenReturn(true);
		when(mockAccountContext.getExpandoValue(fieldName)).thenReturn(EMPTY);

		accountValidationServiceImpl.validateCreateUserAccountFields(mockAccountContext, createUserAccountFields, mockPortletRequest, isEmailAddressFieldOnPage);

		verify(mockAccountContext, times(1)).addFieldError(fieldName, errorMessage);
	}

	@Test
	@Parameters({ "true", "false" })
	public void validateCreateUserAccountFields_WhenCreateUserAccountFieldIsRequiredAndNotEmpty_ThenNoErrorsAdded(boolean isEmailAddressFieldOnPage) throws UserAccountException {
		String fieldName = "fieldName";
		String errorMessage = "error";
		mockBasicSetup();
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);

		when(mockCreateUserAccountField.getRequiredMessage(locale)).thenReturn(errorMessage);
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn(fieldName);
		when(mockCreateUserAccountField.isRequired(COMPANY_ID)).thenReturn(true);
		when(mockAccountContext.getExpandoValue(fieldName)).thenReturn(NOT_EMPTY);

		accountValidationServiceImpl.validateCreateUserAccountFields(mockAccountContext, createUserAccountFields, mockPortletRequest, isEmailAddressFieldOnPage);

		verify(mockAccountContext, never()).addFieldError(same(fieldName), anyString());
	}

	@Test
	@Parameters({ "true", "false" })
	public void validateCreateUserAccountFields_WhenCreateUserAccountFieldNotRequiredAndEmpty_ThenNoValidationDone(boolean isEmailAddressFieldOnPage) throws UserAccountException {
		String fieldName = "fieldName";
		String emptyValue = EMPTY;
		String errorMessage = "error";
		mockBasicSetup();
		List<UserAccountField> createUserAccountFields = new ArrayList<>();
		createUserAccountFields.add(mockCreateUserAccountField);

		when(mockCreateUserAccountField.getRequiredMessage(locale)).thenReturn(errorMessage);
		when(mockCreateUserAccountField.getExpandoFieldName()).thenReturn(fieldName);
		when(mockCreateUserAccountField.isRequired(COMPANY_ID)).thenReturn(false);
		when(mockAccountContext.getExpandoValue(fieldName)).thenReturn(emptyValue);

		accountValidationServiceImpl.validateCreateUserAccountFields(mockAccountContext, createUserAccountFields, mockPortletRequest, isEmailAddressFieldOnPage);

		verify(mockAccountContext, never()).addFieldError(same(fieldName), anyString());
	}

	@Test
	public void validateDate_WhenNotRequiredAndAllDateComponentZero_ThenNoErrorsAdded() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, 0, 0, 0, false);

		verify(mockAccountContext, never()).addFieldError(same(dobAccountValidatedField.getFieldName()), anyString());
	}

	@Test
	public void validateDate_WhenNotRequiredAndOnlyDayComponentZero_ThenNoErrorsAdded() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, 1, 1, 0, false);

		verify(mockAccountContext, times(1)).addFieldError(dobAccountValidatedField.getFieldName(), "please-enter-a-valid-birthday");
	}

	@Test
	public void validateDate_WhenNotRequiredAndOnlyMonthComponentZero_ThenNoErrorsAdded() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, 1, 0, 1, false);

		verify(mockAccountContext, times(1)).addFieldError(dobAccountValidatedField.getFieldName(), "please-enter-a-valid-birthday");
	}

	@Test
	public void validateDate_WhenNotRequiredAndOnlyYearComponentZero_ThenNoErrorsAdded() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, 0, 1, 1, false);

		verify(mockAccountContext, times(1)).addFieldError(dobAccountValidatedField.getFieldName(), "please-enter-a-valid-birthday");
	}

	@Test
	public void validateDate_WhenRequiredAndAllDateValuesPopulatedAndValid_ThenNoErrorsAdded() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, 2020, 1, 1, true);

		verify(mockAccountContext, never()).addFieldError(same(dobAccountValidatedField.getFieldName()), anyString());
	}

	@Test
	public void validateDate_WhenRequiredAndDateValuesInFuture_ThenAddErrorMessage() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;
		int futureYear = LocalDate.now().getYear() + 1;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, futureYear, 1, 1, true);

		verify(mockAccountContext, times(1)).addFieldError(dobAccountValidatedField.getFieldName(), "date-can-not-be-in-future");
	}

	@Test
	public void validateDate_WhenRequiredAndDateValuesInvalid_ThenAddErrorMessage() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;
		int invalidMonth = 13;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, 2020, invalidMonth, 1, true);

		verify(mockAccountContext, times(1)).addFieldError(dobAccountValidatedField.getFieldName(), "please-enter-a-valid-birthday");
	}

	@Test
	public void validateDate_WhenRequiredAndDayComponentIsZero_ThenAddErrorMessage() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, 2020, 1, 0, true);

		verify(mockAccountContext, times(1)).addFieldError(dobAccountValidatedField.getFieldName(), dobAccountValidatedField.getMandatoryErrorKey());
	}

	@Test
	public void validateDate_WhenRequiredAndMonthComponentIsZero_ThenAddErrorMessage() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, 2020, 0, 1, true);

		verify(mockAccountContext, times(1)).addFieldError(dobAccountValidatedField.getFieldName(), dobAccountValidatedField.getMandatoryErrorKey());
	}

	@Test
	public void validateDate_WhenRequiredAndYearComponentIsZero_ThenAddErrorMessage() {
		AccountValidatedField dobAccountValidatedField = AccountValidatedField.DATE_OF_BIRTH;

		accountValidationServiceImpl.validateDate(mockAccountContext, dobAccountValidatedField, 0, 1, 1, true);

		verify(mockAccountContext, times(1)).addFieldError(dobAccountValidatedField.getFieldName(), dobAccountValidatedField.getMandatoryErrorKey());
	}

	@Test
	public void validateEmailAddress_WhenAddressIsInvalid_ThenAddErrorMessage() throws UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = "emailemail.com";
		String email2 = "emailemail.com";

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, times(1)).addFieldError(emailValidatedField.getFieldName(), "please-enter-a-valid-email-address");
	}

	@Test
	public void validateEmailAddress_WhenAddressIsvalid_ThenAddErrorMessage() throws UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = "email@email.com";
		String email2 = "email@email.com";

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, never()).addFieldError(emailValidatedField.getFieldName(), "please-enter-a-valid-email-address");
	}

	@Test
	public void validateEmailAddress_WhenEmailExceedsMaxLength_ThenAddErrorMessage() throws UserAccountException {
		String expected = "expectedVal";
		when(LanguageUtil.format(locale, "please-enter-no-more-than-x-characters", AccountValidatedField.EMAIL.getMaxLength())).thenReturn(expected);
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = new String(new char[AccountValidatedField.EMAIL.getMaxLength() + 1]);
		String email2 = "emailemail.com";

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, times(1)).addFieldError(emailValidatedField.getFieldName(), expected);
	}

	@Test
	public void validateEmailAddress_WhenEmailIsDisabled_ThenNoValidationDone() throws Exception {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = EMPTY;
		String email2 = EMPTY;

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, never()).getEmailAddress();
		verify(mockAccountContext, never()).getConfirmEmailAddress();
	}

	@Test
	public void validateEmailAddress_WhenEmailsAreEmptyAndNotRequired_ThenNoErrorMessage() throws PortalException, UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		when(mockCreateAccountConfiguration.emailAddressRequired()).thenReturn(false);
		String email1 = EMPTY;
		String email2 = EMPTY;

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, never()).addFieldError(emailValidatedField.getFieldName(), emailValidatedField.getMandatoryErrorKey());
	}

	@Test
	public void validateEmailAddress_WhenEmailsAreEmptyAndNotRequiredAndLoggedIn_ThenNotAddErrorMessage() throws PortalException, UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		when(mockCreateAccountConfiguration.emailAddressRequired()).thenReturn(false);
		when(mockAccountContext.isCreatorSignedIn()).thenReturn(true);
		String email1 = EMPTY;
		String email2 = EMPTY;

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, never()).addFieldError(same(emailValidatedField.getFieldName()), anyString());
	}

	@Test
	public void validateEmailAddress_WhenEmailsAreEmptyAndRequired_ThenAddErrorMessage() throws PortalException, UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		when(mockCreateAccountConfiguration.emailAddressRequired()).thenReturn(true);
		String email1 = EMPTY;
		String email2 = EMPTY;

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, times(1)).addFieldError(emailValidatedField.getFieldName(), emailValidatedField.getMandatoryErrorKey());
	}

	@Test
	public void validateEmailAddress_WhenEmailsDoNotMatch_ThenAddErrorMessage() throws UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = "email@email.com";
		String email2 = "email@email2.com";
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, email1)).thenReturn(mockUser);

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, times(1)).addFieldError(emailValidatedField.getFieldName(), "email-addresses-did-not-match");
	}

	@Test
	public void validateEmailAddress_WhenEmailsMatch_ThenNoErrorsAdded() throws UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = "email@email.com";
		String email2 = "email@email.com";

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, never()).addFieldError(emailValidatedField.getFieldName(), "email-addresses-did-not-match");
	}

	@Test
	public void validateEmailAddress_WhenEmailsMatchAndEmailAvailable_ThenNoErrorsAdded() throws UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = "email@email.com";
		String email2 = "email@email.com";
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, email1)).thenReturn(null);

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, never()).addFieldError(emailValidatedField.getFieldName(), "the-email-address-you-requested-is-already-taken");
	}

	@Test
	public void validateEmailAddress_WhenEmailsMatchAndEmailNotAvailable_ThenAddErrorMessage() throws UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = "email@email.com";
		String email2 = "email@email.com";
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, email1)).thenReturn(mockUser);

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockAccountContext, times(1)).addFieldError(emailValidatedField.getFieldName(), "the-email-address-you-requested-is-already-taken");
	}

	@Test
	public void validateEmailAddress_WhenIsUserUpdateAndEmailDifferentToExistingEmail_ThenEmailAvailabilityCheckIsPerformed() throws PortalException, UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = "email@email.com";
		String email2 = "email@email.com";
		long userId = 1L;
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, email1)).thenReturn(mockUser);
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.isUpdate()).thenReturn(true);
		when(mockUser.getEmailAddress()).thenReturn(email1 + "different");
		when(mockUserLocalService.getUserById(userId)).thenReturn(mockUser);

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockUserLocalService, times(1)).fetchUserByEmailAddress(COMPANY_ID, email1);
	}

	@Test
	public void validateEmailAddress_WhenIsUserUpdateAndEmailMatchesExistingEmail_ThenEmailAvailabilityCheckNotPerformed() throws PortalException, UserAccountException {
		AccountValidatedField emailValidatedField = AccountValidatedField.EMAIL;
		String email1 = "email@email.com";
		String email2 = "email@email.com";
		long userId = 1L;
		when(mockUserLocalService.fetchUserByEmailAddress(COMPANY_ID, email1)).thenReturn(mockUser);
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.isUpdate()).thenReturn(true);
		when(mockUser.getEmailAddress()).thenReturn(email1);
		when(mockUserLocalService.getUserById(userId)).thenReturn(mockUser);

		accountValidationServiceImpl.validateEmailAddress(mockAccountContext, emailValidatedField, email1, email2, mockCreateAccountConfiguration, locale);

		verify(mockUserLocalService, never()).fetchUserByEmailAddress(COMPANY_ID, email1);
	}

	@Test
	public void validatePasswordForm_WhenCaptchaIsDisplayedOnPasswordStep_ThenValidatesCaptcha() throws Exception {
		mockBasicSetup();
		when(mockAccountContextService.isShowCaptchaOnPasswordStep(mockAccountContext, mockPortletRequest)).thenReturn(true);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, never()).addFieldError(same(AccountValidatedField.CAPTCHA.getFieldName()), anyString());
	}

	@Test
	public void validatePasswordForm_WhenCaptchaIsNotDisplayedOnPasswordStep_ThenDoesNotValidateCaptcha() throws Exception {
		mockBasicSetup();
		when(mockAccountContextService.isShowCaptchaOnPasswordStep(mockAccountContext, mockPortletRequest)).thenReturn(false);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);

		verifyStatic(CaptchaUtil.class, never());
		CaptchaUtil.check(mockPortletRequest);
	}

	@Test(expected = UserAccountException.class)
	public void validatePasswordForm_WhenErrorGettingDefaultPasswordPolicy_ThenThrowException() throws Exception {
		long userId = 1L;
		String password1 = "Password1";
		String password2 = "Password1";
		mockBasicSetup();
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.getPassword1()).thenReturn(password1);
		when(mockAccountContext.getPassword2()).thenReturn(password2);

		when(mockPasswordPolicyLocalService.getDefaultPasswordPolicy(COMPANY_ID)).thenThrow(new PortalException());

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);
	}

	@Test(expected = UserAccountException.class)
	public void validatePasswordForm_WhenErrorValidatingPassword_ThenThrowException() throws Exception {
		long userId = 1L;
		String password1 = "Password1";
		String password2 = "Password1";
		mockBasicSetup();
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.getPassword1()).thenReturn(password1);
		when(mockAccountContext.getPassword2()).thenReturn(password2);
		when(mockPasswordPolicyLocalService.getDefaultPasswordPolicy(COMPANY_ID)).thenReturn(mockPasswordPolicy);
		doThrow(new PortalException()).when(PwdToolkitUtil.class, "validate", COMPANY_ID, userId, password1, password2, mockPasswordPolicy);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);
	}

	@Test
	public void validatePasswordForm_WhenPassword1And2DoNotMatch_ThenAddDoNotMatchErrorMessage() throws Exception {
		long userId = 1L;
		String errorMessage = "the-passwords-you-entered-do-not-match";
		String password1 = "password1";
		String password2 = "password2";
		mockBasicSetup();
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.getPassword1()).thenReturn(password1);
		when(mockAccountContext.getPassword2()).thenReturn(password2);
		when(mockPasswordPolicyLocalService.getDefaultPasswordPolicy(COMPANY_ID)).thenReturn(mockPasswordPolicy);
		doThrow(new UserPasswordException.MustMatch(userId)).when(PwdToolkitUtil.class, "validate", COMPANY_ID, userId, password1, password2, mockPasswordPolicy);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);
		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.PASSWORD.getFieldName(), errorMessage);
	}

	@Test
	public void validatePasswordForm_WhenPassword1And2IsEmpty_ThenAddErrorMessage() throws Exception {
		long userId = 1L;
		String errorMessage = "password-is-required";
		String password1 = EMPTY;
		String password2 = EMPTY;
		mockBasicSetup();
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.getPassword1()).thenReturn(password1);
		when(mockAccountContext.getPassword2()).thenReturn(password2);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);
		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.PASSWORD.getFieldName(), errorMessage);
	}

	@Test
	public void validatePasswordForm_WhenPassword1Empty_ThenAddRequiredErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockAccountContext.getPassword1()).thenReturn(EMPTY);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, times(1)).addFieldError(same(AccountValidatedField.PASSWORD.getFieldName()), contains("required"));
	}

	@Test
	public void validatePasswordForm_WhenPassword1IsEmpty_ThenAddErrorMessage() throws Exception {
		long userId = 1L;
		String errorMessage = "password-is-required";
		String password1 = EMPTY;
		String password2 = "Password2";
		mockBasicSetup();
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.getPassword1()).thenReturn(password1);
		when(mockAccountContext.getPassword2()).thenReturn(password2);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);
		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.PASSWORD.getFieldName(), errorMessage);
	}

	@Test
	public void validatePasswordForm_WhenPassword2Empty_ThenAddRequiredErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockAccountContext.getPassword2()).thenReturn(EMPTY);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);

		verify(mockAccountContext, times(1)).addFieldError(same(AccountValidatedField.PASSWORD.getFieldName()), contains("required"));
	}

	@Test
	public void validatePasswordForm_WhenPassword2IsEmpty_ThenAddErrorMessage() throws Exception {
		long userId = 1L;
		String errorMessage = "password-is-required";
		String password1 = "Password1";
		String password2 = EMPTY;
		mockBasicSetup();
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.getPassword1()).thenReturn(password1);
		when(mockAccountContext.getPassword2()).thenReturn(password2);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);
		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.PASSWORD.getFieldName(), errorMessage);
	}

	@Test
	public void validatePasswordForm_WhenPasswordDoNotHaveEnoughSymbols_ThenAddMoreSymbolsErrorMessage() throws Exception {
		long userId = 1L;
		long minSymbols = 1L;
		String errorMessage = "Password must use at least {0} of these symbols: {1}";
		String password1 = "Pass1234";
		String password2 = "Pass1234";
		String minSymbolsString = "1";
		String passSymbols = "PASSWORDS_PASSWORDPOLICYTOOLKIT_VALIDATOR_CHARSET_SYMBOLS";
		String[] messageArgs = new String[] { minSymbolsString, passSymbols };
		mockBasicSetup();
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.getPassword1()).thenReturn(password1);
		when(mockAccountContext.getPassword2()).thenReturn(password2);

		when(mockPasswordPolicyLocalService.getDefaultPasswordPolicy(COMPANY_ID)).thenReturn(mockPasswordPolicy);
		when(PropsUtil.get(PropsKeys.PASSWORDS_PASSWORDPOLICYTOOLKIT_VALIDATOR_CHARSET_SYMBOLS)).thenReturn(passSymbols);
		doThrow(new UserPasswordException.MustHaveMoreSymbols(minSymbols)).when(PwdToolkitUtil.class, "validate", COMPANY_ID, userId, password1, password2, mockPasswordPolicy);
		when(mockPortletRequest.getLocale()).thenReturn(Locale.UK);
		when(AggregatedResourceBundleUtil.format("password-must-use-at-least-x-of-these-symbols-x", messageArgs, Locale.UK, AccountConstants.BUNDLE_SYMBOLIC_NAME)).thenReturn(errorMessage);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);
		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.PASSWORD.getFieldName(), errorMessage);
	}

	@Test
	public void validatePasswordForm_WhenPasswordIsNotLongerEnough_ThenAddLongerPasswordErrorMessage() throws Exception {
		long userId = 1L;
		String errorMessage = "the-password-must-be-at-least-x-characters";
		String minLongString = "8";
		String password1 = "pass";
		String password2 = "pass";
		mockBasicSetup();
		when(mockAccountContext.getUserId()).thenReturn(userId);
		when(mockAccountContext.getPassword1()).thenReturn(password1);
		when(mockAccountContext.getPassword2()).thenReturn(password2);

		when(mockPasswordPolicyLocalService.getDefaultPasswordPolicy(COMPANY_ID)).thenReturn(mockPasswordPolicy);
		doThrow(new UserPasswordException.MustBeLonger(userId, Integer.parseInt(minLongString))).when(PwdToolkitUtil.class, "validate", COMPANY_ID, userId, password1, password2, mockPasswordPolicy);
		when(mockPortletRequest.getLocale()).thenReturn(Locale.UK);
		when(AggregatedResourceBundleUtil.format("the-password-must-be-at-least-x-characters", minLongString, Locale.UK, AccountConstants.BUNDLE_SYMBOLIC_NAME)).thenReturn(errorMessage);

		accountValidationServiceImpl.validatePasswordForm(mockAccountContext, mockPortletRequest);
		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.PASSWORD.getFieldName(), errorMessage);
	}

	@Test
	public void validatePersonalDetails_WhenFirstNameEmpty_ThenAddRequiredErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockAccountContext.getFirstName()).thenReturn(EMPTY);

		accountValidationServiceImpl.validatePersonalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(same(AccountValidatedField.FIRSTNAME.getFieldName()), contains("required"));
	}

	@Test
	public void validatePersonalDetails_WhenFirstNameNotEmptyOrTooLong_ThenNoErrorsAdded() throws Exception {
		mockBasicSetup();
		when(mockAccountContext.getFirstName()).thenReturn(NOT_EMPTY);

		accountValidationServiceImpl.validateAdditionalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).addFieldError(same(AccountValidatedField.FIRSTNAME.getFieldName()), anyString());
	}

	@Test
	public void validatePersonalDetails_WhenFirstNameTooLong_ThenAddMaxLengthErrorMessage() throws Exception {
		mockBasicSetup();
		String expected = "expectedVal";
		when(LanguageUtil.format(locale, "please-enter-no-more-than-x-characters", AccountValidatedField.FIRSTNAME.getMaxLength())).thenReturn(expected);
		when(mockAccountContext.getFirstName()).thenReturn(NOT_EMPTY + new String(new char[AccountValidatedField.FIRSTNAME.getMaxLength()]));

		accountValidationServiceImpl.validatePersonalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.FIRSTNAME.getFieldName(), expected);
	}

	@Test
	public void validatePersonalDetails_WhenLastNameEmpty_ThenAddRequiredErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockAccountContext.getLastName()).thenReturn(EMPTY);

		accountValidationServiceImpl.validatePersonalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(same(AccountValidatedField.LASTNAME.getFieldName()), contains("required"));
	}

	@Test
	public void validatePersonalDetails_WhenLastNameNotEmptyOrTooLong_ThenNoErrorsAdded() throws Exception {
		mockBasicSetup();
		when(mockAccountContext.getLastName()).thenReturn(NOT_EMPTY);

		accountValidationServiceImpl.validatePersonalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).addFieldError(same(AccountValidatedField.LASTNAME.getFieldName()), anyString());
	}

	@Test
	public void validatePersonalDetails_WhenLastNameTooLong_ThenAddMaxLengthErrorMessage() throws Exception {
		mockBasicSetup();
		String expected = "expectedVal";
		when(LanguageUtil.format(locale, "please-enter-no-more-than-x-characters", AccountValidatedField.LASTNAME.getMaxLength())).thenReturn(expected);
		when(mockAccountContext.getLastName()).thenReturn(NOT_EMPTY + new String(new char[AccountValidatedField.LASTNAME.getMaxLength()]));

		accountValidationServiceImpl.validatePersonalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(AccountValidatedField.LASTNAME.getFieldName(), expected);
	}

	@Test
	public void validatePersonDetails_WhenDateOfBirthEnabled_ThenValidateDateOfBirth() throws Exception {
		mockBasicSetup();
		when(mockUserProfileFieldsConfiguration.dateOfBirthEnabled()).thenReturn(true);

		accountValidationServiceImpl.validatePersonalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		InOrder inOrder = inOrder(mockAccountContext);
		inOrder.verify(mockAccountContext, times(1)).getBirthYear();
		inOrder.verify(mockAccountContext, times(1)).getBirthMonth();
		inOrder.verify(mockAccountContext, times(1)).getBirthDay();
	}

	@Test
	public void validatePersonDetails_WhenEmailAddressIsEmptyAndRequired_ThenAddRequiredErrorMessage() throws Exception {
		mockBasicSetup();
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn(AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);
		when(mockCreateAccountConfiguration.emailAddressRequired()).thenReturn(true);

		accountValidationServiceImpl.validatePersonalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, times(1)).addFieldError(same(AccountValidatedField.EMAIL.getFieldName()), contains("required"));
	}

	@Test
	public void validatePersonDetails_WhenEmailAddressNotOnPage_ThenDoNotValidateEmailAddress() throws Exception {
		mockBasicSetup();
		when(mockCreateAccountConfiguration.emailAddressPageLocation()).thenReturn("not-on-this-page");

		accountValidationServiceImpl.validatePersonalDetails(mockAccountContext, mockPortletRequest, mockCreateAccountConfiguration, mockUserProfileFieldsConfiguration);

		verify(mockAccountContext, never()).getEmailAddress();
		verify(mockAccountContext, never()).getConfirmEmailAddress();
	}

	private void mockBasicSetup() throws UserAccountException {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getLocale()).thenReturn(locale);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		Map<String, String> errors = new HashMap<>();

		when(mockAccountContext.getErrors()).thenReturn(errors);
	}
}
