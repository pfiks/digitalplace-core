package com.placecube.digitalplace.user.settings.web.constants;

public final class PortletRequestKeys {

	public static final String CURRENT_SECTION_ID = "userSettingsCurrentSectionId";

	private PortletRequestKeys() {
	}
}
