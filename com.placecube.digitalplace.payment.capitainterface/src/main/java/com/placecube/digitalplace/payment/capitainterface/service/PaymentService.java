package com.placecube.digitalplace.payment.capitainterface.service;

import java.math.BigDecimal;

import javax.xml.rpc.ServiceException;

import com.capita_software_services.www.scp.base.Address;
import com.capita_software_services.www.scp.base.AuthDetails;
import com.capita_software_services.www.scp.base.BillingDetails;
import com.capita_software_services.www.scp.base.Card;
import com.capita_software_services.www.scp.base.CardHolderDetails;
import com.capita_software_services.www.scp.base.Contact;
import com.capita_software_services.www.scp.base.ScpInvokeResponse;
import com.capita_software_services.www.scp.base.Status;
import com.capita_software_services.www.scp.base.StoredCardKey;
import com.capita_software_services.www.scp.base.TransactionState;
import com.capita_software_services.www.scp.simple.ScpSimpleQueryResponse;
import com.capita_software_services.www.scp.simple.SimplePayment;
import com.capita_software_services.www.scp.simple.SimplePaymentResult;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.DataField;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.DiagnosticData;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.ErrorCode;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.LoggingBean;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.Service;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.SourceSystem;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.service.LoggingFormatService;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;

/**
 * Facade class to decouple the payment interface from the Capita implementation
 * classes
 */

public class PaymentService {

	private static final Log LOG = LogFactoryUtil.getLog(PaymentService.class);

	private CapitaPaymentService capitaPaymentService;

	/**
	 * Constructor
	 *
	 * @param properties the properties
	 *
	 * @throws ServiceException if the initialisation of the connection to the
	 *             payment portal fails
	 */
	public PaymentService(PaymentProperties properties) throws ServiceException {
		capitaPaymentService = new CapitaPaymentService(properties);
		capitaPaymentService.setCapitaEndpoint(properties.getEndpoint());
	}

	/**
	 * Method to populate a BillingDetails object with the data held in the
	 * order. uses the email address and the stored card details of the order to
	 * populate a BillingDetails object to send to Capita
	 *
	 * @param order the order
	 * @return the billing details
	 */
	public BillingDetails createBillingDetails(OneOffPaymentOrder order) {
		BillingDetails billing = new BillingDetails();

		// Build the card holders details object
		CardHolderDetails cardHolderDetails = new CardHolderDetails();

		if (order.getEmailAddress() != null) {
			Contact contact = new Contact();
			contact.setEmail(order.getEmailAddress());
			cardHolderDetails.setContact(contact);
		}

		if (order.getAddress() != null) {
			Address cardholderAddress = new Address();
			if (Validator.isNotNull(order.getAddress().getAddressLine1())) {
				cardholderAddress.setAddress1(order.getAddress().getAddressLine1());
			}
			if (Validator.isNotNull(order.getAddress().getAddressLine2())) {
				cardholderAddress.setAddress2(order.getAddress().getAddressLine2());
			}
			if (Validator.isNotNull(order.getAddress().getAddressLine3())) {
				cardholderAddress.setAddress3(order.getAddress().getAddressLine3());
			}
			if (Validator.isNotNull(order.getAddress().getCity())) {
				cardholderAddress.setAddress4(order.getAddress().getCity());
			}
			if (Validator.isNotNull(order.getAddress().getPostcode())) {
				cardholderAddress.setPostcode(order.getAddress().getPostcode());
			}
			cardHolderDetails.setAddress(cardholderAddress);

		}

		if (Validator.isNotNull(order.getFullName())) {
			cardHolderDetails.setCardHolderName(order.getFullName());
		}

		billing.setCardHolderDetails(cardHolderDetails);

		// use stored card
		String last4Digits = order.getLast4Digits();
		String cardToken = order.getCardToken();

		if (Validator.isNotNull(last4Digits) && Validator.isNotNull(cardToken)) {
			Card card = new Card();
			StoredCardKey storedCardKey = new StoredCardKey();

			storedCardKey.setLastFourDigits(last4Digits);
			storedCardKey.setToken(cardToken);
			card.setStoredCardKey(storedCardKey);
			billing.setCard(card);
		}

		return billing;
	}

	/**
	 * Invokes a one off payment, based on the data in order. Sets the redirect
	 * url, the transaction reference number and the bcc reference number in the
	 * order object
	 *
	 * @param order the order
	 * @param userId the userId for the payment
	 * @param isImpersonated if the user is being impersonated
	 *
	 * @return String indicating successful payment: PAYMENT_SETUP_SUCCESSFUL or
	 *         unsuccessful: PAYMENT_ERROR
	 */
	public String invokePaymentService(OneOffPaymentOrder order, long userId, boolean isImpersonated) {

		final String PAYMENT_ERROR = "PAYMENT_ERROR";

		if (order == null) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0056, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.MISSING_MANDATORY_FIELD, "ALL"))));
			return PAYMENT_ERROR;
		}

		ScpInvokeResponse response = capitaPaymentService.capitaInvoke(order, createBillingDetails(order), userId, isImpersonated);

		if (null != response) {
			Status status = response.getInvokeResult().getStatus();

			if (status.getValue().equals(Status._SUCCESS)) {

				// Will need to build up some more data from the response here -
				// put
				// it into the order object?
				// Or return an order object setting a success status??
				order.setRedirectUrl(response.getInvokeResult().getRedirectUrl().toString());
				order.setTransactionReferenceNumber(response.getScpReference());
				order.setGeneratedReferenceNumber(response.getRequestId().toString());

				return "PAYMENT_SETUP_SUCCESSFUL";
			}
		}

		LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.CAPITA, ErrorCode.E0052, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
				new DiagnosticData(DataField.CAPITA_REF_NO, order.getSaleReferenceCode()), new DiagnosticData(DataField.CAPITA_STATUS, Status.ERROR.getValue()))));
		return PAYMENT_ERROR;

	}

	/**
	 * Queries the status of the payment. This is intended to be used after the
	 * payment redirect has completed i.e. the user has completed the payment
	 * form and control has been passed back to the BCC system
	 *
	 * @param order - uses the transaction reference number to query the payment
	 *            system. Populates the unique trans id, the auth code, the
	 *            payment value processed and the stored card details held in
	 *            order from the response received from the payment gateway.
	 * @return boolean indicating success of the payment.
	 */
	public boolean invokeQueryServiceOneOffPaymentOrder(OneOffPaymentOrder order) {
		ScpSimpleQueryResponse response = capitaPaymentService.capitaQuery(order.getTransactionReferenceNumber(), order.getEmailAddress());

		String txState = response != null ? response.getTransactionState().getValue() : "ERROR";
		if (!txState.equals(TransactionState._COMPLETE)) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.CAPITA, ErrorCode.E0053, CapitaConstants.QUERY_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.CAPITA_REF_NO, order.getSaleReferenceCode()), new DiagnosticData(DataField.CAPITA_TX_REF_NO, order.getTransactionReferenceNumber()))));

			return false;
		}

		SimplePaymentResult paymentResult = response.getPaymentResult();
		Status status = paymentResult.getStatus();

		if (!status.getValue().equals(Status._SUCCESS)) {
			LOG.error("getErrorId: " + paymentResult.getErrorDetails().getErrorId());
			LOG.error("getErrorMessage(): " + paymentResult.getErrorDetails().getErrorMessage());

			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.CAPITA, ErrorCode.E0054, CapitaConstants.QUERY_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.CAPITA_REF_NO, order.getSaleReferenceCode()), new DiagnosticData(DataField.CAPITA_STATUS, status.getValue()),
					new DiagnosticData(DataField.CAPITA_TX_REF_NO, order.getTransactionReferenceNumber()))));
			order.setStatus(status.getValue());

			return false;
		}

		SimplePayment paymentDetails = paymentResult.getPaymentDetails();
		AuthDetails authDetails = paymentDetails.getAuthDetails();

		order.setContinuousAuditNumber(authDetails.getContinuousAuditNumber().intValue());
		order.setUniqueTranId(paymentDetails.getPaymentHeader().getUniqueTranId());
		order.setAuthCode(authDetails.getAuthCode());
		order.setStatus(status.getValue());
		order.setPaymentValueProcessed(new BigDecimal(authDetails.getAmountInMinorUnits()));

		if (response.getStoreCardResult().getStatus().getValue().equals(Status._SUCCESS)) {
			order.setLast4Digits(response.getStoreCardResult().getStoredCardDetails().getStoredCardKey().getLastFourDigits());
			order.setCardToken(response.getStoreCardResult().getStoredCardDetails().getStoredCardKey().getToken());
		}

		return true;
	}

	/**
	 * used to set the capitaPaymentService if required.
	 *
	 * @param capitaPaymentService the Capita payment service
	 */
	public void setCapitaPaymentService(CapitaPaymentService capitaPaymentService) {
		this.capitaPaymentService = capitaPaymentService;
	}

}
