package com.placecube.digitalplace.appointment.admin.web.portlet;

import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.appointment.admin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;

@RunWith(MockitoJUnitRunner.class)
public class CancelBookedAppointmentMVCActionCommandTest {

	@InjectMocks
	private CancelBookedAppointmentMVCActionCommand cancelBookedAppointmentMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private AppointmentBookingEntry mockAppointmentBookingEntry;

	@Mock
	private AppointmentBookingService mockAppointmentBookingService;

	@Mock
	private AppointmentsManagerService mockAppointmentsManagerService;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	private MockedStatic<ParamUtil> mockParamUtil;

	private MockedStatic<PropsUtil> mockPropsUtil;

	@Before
	public void activateSetUp() {
		mockPropsUtil = mockStatic(PropsUtil.class);
		mockParamUtil = mockStatic(ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenCancelsTheAppointment() throws Exception {
		String serviceId = "myServiceId";
		long companyId = 1;
		mockParamUtil.when(() -> ParamUtil.getLong(mockActionRequest, "companyId")).thenReturn(companyId);
		mockParamUtil.when(() -> ParamUtil.getString(mockActionRequest, "serviceId", "")).thenReturn(serviceId);
		when(mockAppointmentsManagerService.getAppointmentBookingEntryFromRequest(mockActionRequest)).thenReturn(mockAppointmentBookingEntry);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);

		cancelBookedAppointmentMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAppointmentBookingService, times(1)).cancelAppointment(companyId, serviceId, mockAppointmentBookingEntry);
		verify(mockMutableRenderParameters, times(1)).setValue("mvcRenderCommandName", MVCCommandKeys.VIEW_BOOKED_APPOINTMENTS);
	}

	@After
	public void teardown() {
		mockPropsUtil.close();
		mockParamUtil.close();
	}

}
