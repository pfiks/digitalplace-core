package com.placecube.digitalplace.notifications.govuknotificationsservice.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendSmsContext;
import com.placecube.digitalplace.notifications.service.NotificationSmsConnector;

import uk.gov.service.notify.NotificationClient;
import uk.gov.service.notify.NotificationClientException;

@Component(immediate = true, service = NotificationSmsConnector.class)
public class GovUKNotificationSmsConnector implements NotificationSmsConnector {

	private static final Log LOG = LogFactoryUtil.getLog(GovUKNotificationMailConnector.class);

	@Reference
	private GovUKNotificationsService govukNotificationsService;

	@Override
	public boolean enabled(long companyId) {
		try {
			return govukNotificationsService.isSmsEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public void sendSms(SendSmsContext sendSmsContext, ServiceContext serviceContext) throws NotificationsException, ConfigurationException {
		try {
			NotificationClient client = govukNotificationsService.getClient(govukNotificationsService.getApplicationKey(serviceContext.getCompanyId(), sendSmsContext.getApiKeyName()));
			String senderId = sendSmsContext.getSmsSenderId();
			if (Validator.isNull(senderId)) {
				client.sendSms(sendSmsContext.getTemplateId(), sendSmsContext.getPhoneNumber(), sendSmsContext.getPersonalisation(), sendSmsContext.getReference());
			} else {
				client.sendSms(sendSmsContext.getTemplateId(), sendSmsContext.getPhoneNumber(), sendSmsContext.getPersonalisation(), sendSmsContext.getReference(), senderId);
			}
		} catch (NotificationClientException e) {
			LOG.error(e.getMessage());
			throw new NotificationsException(e);
		}
	}

}
