package com.placecube.digitalplace.ddl.internal.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "dynamic-data-mapping", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = DynamicDataListCompanyConfigurationConstants.PID, localization = "content/Language", name = "dynamic-data-list-mappings")
public interface DynamicDataListCompanyConfiguration {

	@Meta.AD(required = false, deflt = "", name = "dynamic-data-list-mapping", description = "dynamic-data-list-mapping-help")
	String[] dynamicDataListMappings();

}
