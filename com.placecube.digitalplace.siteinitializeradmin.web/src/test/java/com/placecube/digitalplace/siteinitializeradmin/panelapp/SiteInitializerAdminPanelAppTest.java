package com.placecube.digitalplace.siteinitializeradmin.panelapp;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.siteinitializeradmin.constants.PortletKeys;

@RunWith(PowerMockRunner.class)
public class SiteInitializerAdminPanelAppTest extends PowerMockito {

	@Mock
	private Group mockGroup;

	@Mock
	private PermissionChecker mockPermissionChecker;

	private SiteInitializerAdminPanelApp siteInitializerAdminPanelApp;

	@Before
	public void activeSetUp() {
		siteInitializerAdminPanelApp = new SiteInitializerAdminPanelApp();
	}

	@Test
	public void getPortletId_ThenReturnsThePortletId() {
		String result = siteInitializerAdminPanelApp.getPortletId();
		assertThat(result, equalTo(PortletKeys.SITE_INITIALIZER_ADMIN));
	}

	@Test
	public void isShow_WhenIsCompanyGroup_ThenReturnsFalse() throws PortalException {

		Long siteGroupId = 1234l;
		when(mockGroup.getGroupId()).thenReturn(siteGroupId);
		when(mockGroup.isCompany()).thenReturn(true);

		boolean result = siteInitializerAdminPanelApp.isShow(mockPermissionChecker, mockGroup);

		assertThat(result, equalTo(false));

	}

}
