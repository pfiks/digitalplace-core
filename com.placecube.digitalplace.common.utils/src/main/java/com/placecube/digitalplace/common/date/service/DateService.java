package com.placecube.digitalplace.common.date.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = DateService.class)
public class DateService {

	public Date localDateToDate(LocalDate localDate, ZoneId zoneId) {
		return Date.from(localDate.atStartOfDay(zoneId).toInstant());
	}

	public XMLGregorianCalendar toXMLGregorianCalendar(Date date) throws DatatypeConfigurationException {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
	}

	public XMLGregorianCalendar toXMLGregorianCalendar(LocalDate localDate) throws DatatypeConfigurationException {
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(localDate.toString());
	}
}