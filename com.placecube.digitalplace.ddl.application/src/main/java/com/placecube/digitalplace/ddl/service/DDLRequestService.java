package com.placecube.digitalplace.ddl.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.constants.DDLRecordConstants;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.constants.DDMKeys;
import com.placecube.digitalplace.ddl.constants.UserCustomFields;
import com.placecube.digitalplace.ddl.model.DDLRecordDetails;
import com.placecube.digitalplace.ddl.model.UserDetails;
import com.placecube.digitalplace.ddl.util.UserUtil;
import com.placecube.initializer.service.ServiceContextInitializer;

@Component(immediate = true, service = DDLRequestService.class)
public class DDLRequestService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLRequestService.class);

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private JsonParserService jsonParserService;

	@Reference
	private ServiceContextInitializer serviceContextInitializer;

	@Reference
	private UserLocalService userLocalService;

	public String addLinkedUser(long companyId, Long ddlRecordId, String fieldName, String emailAddress) throws PortalException {

		if (Validator.isNotNull(ddlRecordId) && ddlRecordId > 0) {
			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(ddlRecordId);
			if (Validator.isNotNull(ddlRecord)) {

				DDMFormValues ddmFormValues = getDDMFormValues(ddlRecord.getRecordSet());
				Locale defaultLocale = getDefaultLocaleFromDDLRecord(ddlRecord);
				List<DDMFormFieldValue> editedDdmFormFieldValues = new ArrayList<>();
				AtomicBoolean fieldNameExist = new AtomicBoolean(false);

				ddlRecord.getDDMFormValues().getDDMFormFieldValues().forEach(ddmFormFieldValue -> {
					if (ddmFormFieldValue.getName().equalsIgnoreCase(fieldName) && Validator.isNull(ddmFormFieldValue.getValue().getString(defaultLocale))) {

						Value value = new LocalizedValue(defaultLocale);
						value.addString(defaultLocale, emailAddress);
						ddmFormFieldValue.setValue(value);
						fieldNameExist.set(true);

					}
					editedDdmFormFieldValues.add(ddmFormFieldValue);

				});

				if (!fieldNameExist.get()) {

					DDMFormFieldValue newFieldValue = new DDMFormFieldValue();
					newFieldValue.setName(fieldName);
					Value value = new LocalizedValue(defaultLocale);
					value.addString(defaultLocale, emailAddress);
					newFieldValue.setValue(value);

					editedDdmFormFieldValues.add(newFieldValue);
				}

				ddmFormValues.setDDMFormFieldValues(editedDdmFormFieldValues);

				if (Validator.isNotNull(getCompanyGroupFromId(companyId))) {

					DDLRecord updatedRec = ddlRecordLocalService.updateRecord(ddlRecord.getRecordSet().getDDMStructure().getUserId(), ddlRecord.getRecordId(), true,
							DDLRecordConstants.DISPLAY_INDEX_DEFAULT, ddmFormValues, serviceContextInitializer.getServiceContext(getCompanyGroupFromId(companyId)));

					return getDDLRecordValues(updatedRec.getRecordId(), defaultLocale);

				}

			}

		}

		return StringPool.BLANK;
	}

	public String getDDLRecordValues(Long ddlRecordId, Locale locale) throws PortalException {

		DDLRecordDetails ddlRecordDetails = new DDLRecordDetails();

		if (Validator.isNotNull(ddlRecordId) && ddlRecordId > 0) {
			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(ddlRecordId);
			if (Validator.isNotNull(ddlRecord)) {
				ddlRecordDetails.setDdlRecord(ddlRecord);
				Map<String, String> ddmFormFieldValues = new HashMap<>();
				List<String> tenantEmails = new ArrayList<>();
				List<String> occupantEmails = new ArrayList<>();

				ddlRecord.getDDMFormValues().getDDMFormFieldValues().forEach(fValue -> {

					if (fValue.getName().equalsIgnoreCase(DDMKeys.TENANT_EMAIL)) {
						tenantEmails.add(fValue.getValue().getString(locale));
					} else if (fValue.getName().equalsIgnoreCase(DDMKeys.OCCUPANT_EMAIL)) {
						occupantEmails.add(fValue.getValue().getString(locale));
					} else {
						ddmFormFieldValues.put(fValue.getName(), fValue.getValue().getString(locale));
					}

				});

				ddmFormFieldValues.put(DDMKeys.TENANT_EMAIL, tenantEmails.stream().collect(Collectors.joining(",")));
				ddmFormFieldValues.put(DDMKeys.OCCUPANT_EMAIL, occupantEmails.stream().collect(Collectors.joining(",")));

				ddlRecordDetails.setDdmFormFieldValues(ddmFormFieldValues);
			}

		}
		return sendResponseWithCorrectJsonFormat(jsonParserService.toJson(ddlRecordDetails));
	}

	public String getLinkedUsers(long companyId, Long ddlRecordId, String fieldName, Locale locale) throws PortalException {
		String userListJson = "";
		if (Validator.isNotNull(ddlRecordId) && ddlRecordId > 0) {
			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(ddlRecordId);
			if (Validator.isNotNull(ddlRecord)) {

				List<DDMFormFieldValue> values = ddlRecord.getDDMFormFieldValues(fieldName);
				List<User> userList = getUserList(companyId, values, locale);

				userListJson = getAllUserDetailsJson(userList);
			}
		}

		return userListJson;
	}

	public String removeLinkedUser(long companyId, Long ddlRecordId, String fieldName, String emailAddress) throws PortalException {

		if (Validator.isNotNull(ddlRecordId) && ddlRecordId > 0) {
			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(ddlRecordId);
			if (Validator.isNotNull(ddlRecord)) {
				DDMFormValues ddmFormValues = getDDMFormValues(ddlRecord.getRecordSet());

				Locale defaultLocale = getDefaultLocaleFromDDLRecord(ddlRecord);

				List<DDMFormFieldValue> editedDdmFormFieldValues = new ArrayList<>();

				ddlRecord.getDDMFormValues().getDDMFormFieldValues().forEach(ddmFormFieldValue -> {
					if (!(ddmFormFieldValue.getName().equalsIgnoreCase(fieldName) && ddmFormFieldValue.getValue().getString(defaultLocale).equalsIgnoreCase(emailAddress))) {

						editedDdmFormFieldValues.add(ddmFormFieldValue);

					}
				});

				ddmFormValues.setDDMFormFieldValues(editedDdmFormFieldValues);

				if (Validator.isNotNull(getCompanyGroupFromId(companyId))) {

					DDLRecord updatedRec = ddlRecordLocalService.updateRecord(ddlRecord.getRecordSet().getDDMStructure().getUserId(), ddlRecord.getRecordId(), true,
							DDLRecordConstants.DISPLAY_INDEX_DEFAULT, ddmFormValues, serviceContextInitializer.getServiceContext(getCompanyGroupFromId(companyId)));

					return getDDLRecordValues(updatedRec.getRecordId(), defaultLocale);

				}
			}

		}
		return StringPool.BLANK;
	}

	private String getAllUserDetailsJson(List<User> userList) {

		List<UserDetails> userDetailsList = new ArrayList<>();

		userList.forEach(user -> {
			UserDetails userDetails = new UserDetails(user.getFullName(), user.getEmailAddress(), user.getUserId());
			userDetails.setUser(user);

			Map<String, Serializable> userCustomFields = new HashMap<>();

			try {
				userCustomFields.put(UserCustomFields.AGE, UserUtil.getAge(user));
			} catch (PortalException e) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Error while calculating age from user's birthdate:" + user.getFullName());
				}
			}
			userCustomFields.put(UserCustomFields.PIN, UserUtil.getPin(user));
			userCustomFields.put(UserCustomFields.PRIMARY_PHONE, UserUtil.getPrimeryPhone(user));

			Collections.list(user.getExpandoBridge().getAttributeNames()).forEach(fieldName -> {

				userCustomFields.put(fieldName, UserUtil.getExpandoUserValues(user, fieldName));
			});

			userDetails.setUserCustomFields(userCustomFields);

			userDetailsList.add(userDetails);

		});

		return jsonParserService.toJson(userDetailsList);
	}

	private Group getCompanyGroupFromId(long companyId) {
		try {
			return companyLocalService.getCompany(companyId).getGroup();
		} catch (PortalException e) {
			e.printStackTrace();
		}

		return null;
	}

	private DDMFormValues getDDMFormValues(DDLRecordSet ddlRecordSet) throws PortalException {
		DDMStructure propertyStructure = ddlRecordSet.getDDMStructure();
		DDMForm ddmForm = propertyStructure.getDDMForm();
		DDMFormValues ddmFormValues = new DDMFormValues(ddmForm);
		ddmFormValues.setAvailableLocales(ddmForm.getAvailableLocales());
		ddmFormValues.setDefaultLocale(ddmForm.getDefaultLocale());
		return ddmFormValues;
	}

	private Locale getDefaultLocaleFromDDLRecord(DDLRecord ddlRecord) throws PortalException {

		return ddlRecord.getRecordSet().getDDMStructure().getDDMForm().getDefaultLocale();
	}

	private List<User> getUserList(long companyId, List<DDMFormFieldValue> values, Locale locale) {
		List<User> userList = new ArrayList<>();
		if (Validator.isNotNull(values)) {
			values.forEach(email -> {
				try {
					User user = userLocalService.getUserByEmailAddress(companyId, email.getValue().getString(locale));
					userList.add(user);

				} catch (Exception e) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Error while getting user with email :" + email);
					}
				}
			});
		}
		return userList;
	}

	private String sendResponseWithCorrectJsonFormat(String responseJson) {
		responseJson = responseJson.startsWith("[") ? responseJson.substring(1) : responseJson;
		return responseJson.endsWith("]") ? responseJson.substring(0, responseJson.length() - 1) : responseJson;
	}

}
