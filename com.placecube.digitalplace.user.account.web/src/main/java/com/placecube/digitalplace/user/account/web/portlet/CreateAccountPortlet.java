package com.placecube.digitalplace.user.account.web.portlet;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;

@Component(configurationPid = "com.placecube.digitalplace.user.account.configuration.AccountPortletInstanceConfiguration", immediate = true, property = {
		"com.liferay.fragment.entry.processor.portlet.alias=create-account", "com.liferay.portlet.css-class-wrapper=portlet-user-account",
		"com.liferay.portlet.display-category=category.tools", "com.liferay.portlet.instanceable=true", "javax.portlet.resource-bundle=content.Language",
		"com.liferay.portlet.header-portlet-javascript=/js/com.placecube.digitalplace.user.account.web.js", "javax.portlet.security-role-ref=power-user,user",
		"com.liferay.portlet.add-default-resource=true", "javax.portlet.version=3.0", "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT,
		"javax.portlet.init-param.add-process-action-success-action=false", "com.liferay.portlet.header-portlet-css=/css/main.css",
		"portlet.add.default.resource.check.whitelist=" + AccountPortletKeys.CREATE_ACCOUNT }, service = Portlet.class)
public class CreateAccountPortlet extends MVCPortlet {

	@Reference
	private AccountService accountService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			accountService.checkAccountCreationEnabled(renderRequest);
		} catch (PortletException e) {
			renderRequest.setAttribute("accountCreationDisabled", true);
		}

		super.render(renderRequest, renderResponse);
	}
}