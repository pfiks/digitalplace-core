package com.placecube.digitalplace.address.constants.expando;

public final class UserExpandoUPRN {

	public static final String FIELD_NAME = "uprn";

	private UserExpandoUPRN() {
		return;
	}

}
