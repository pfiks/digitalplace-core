<%@ include file="init.jsp"%>

<portlet:actionURL name="<%=MVCCommandKeys.EXTERNAL_PAGE_IMPORT%>" var="externalPageImportURL" />
<portlet:renderURL var="viewExternalPageURL">
	<portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.EXTERNAL_PAGE_VIEW%>" />
</portlet:renderURL>

<div class="container-fluid container-fluid-max-xl edit-export">
	<aui:form action="${externalPageImportURL}" name="fm">
		<liferay-ui:error key="<%=ValidationKeys.INVALID_XPATH%>" message="invalid-xpath" />
		<liferay-ui:error key="<%=ValidationKeys.REQUIRED_URL%>" message="required-url" />
		<liferay-ui:error key="externalPageImportError" message="sorry-something-went-wrong" />

		<dp-frontend:fieldset-group>
				<aui:fieldset>
					<aui:input name="url" id="url" required="true" type="text"
						helpMessage="import-url-help">
						<aui:validator name="url" />
					</aui:input>
					<aui:input label="max-pages-to-fetch-label" name="maxPagesToFetch"
						id="maxPagesToFetch" required="true" type="number"
						helpMessage="max-pages-to-fetch-help-msg">
						<aui:validator name="digits" />
						<aui:validator name="min">1</aui:validator>
					</aui:input>
					<aui:input name="xpath" id="xpath" type="text" />
					<aui:select name="indexContent" showEmptyOption="false" label="post-crawl-action" helpMessage="index-content-help-message">
						<aui:option value="logResults" selected="true">Log results</aui:option>
						<aui:option value="entryOnly">Create entry only</aui:option>
						<aui:option value="entryPublished">Create and publish entry</aui:option>
					</aui:select>
				</aui:fieldset>
		</dp-frontend:fieldset-group>
		<aui:button-row>
			<aui:button type="submit" />
			<aui:button onClick="${viewExternalPageURL}" type="cancel" />
		</aui:button-row>
	</aui:form>
</div>