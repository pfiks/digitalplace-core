package com.placecube.digitalplace.user.account.web.internal.upgrade.registry;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.user.account.web.internal.upgrade.Upgrade_1_0_0_AccountConfigurationSettingsUpgradeProcess;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = { UpgradeStepRegistrator.class })
public class UserAccountWebUpgrade implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private GroupLocalService groupLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new Upgrade_1_0_0_AccountConfigurationSettingsUpgradeProcess(companyLocalService, configurationProvider, groupLocalService));
	}

}