package com.placecube.digitalplace.calendar.web.portlet.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.calendar.web.constants.DigitalPlaceCalendarConfigurationConstants;
import com.placecube.digitalplace.calendar.web.constants.PortletKeys;
import com.placecube.digitalplace.calendar.web.service.DigitalPlaceCalendarConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.CALENDAR, service = ConfigurationAction.class)
public class DigitalPlaceCalendarConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private DigitalPlaceCalendarConfigurationService digitalPlaceCalendarConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		DigitalPlaceCalendarInstanceConfiguration configuration = digitalPlaceCalendarConfigurationService.getDigitalPlaceCalendarConfiguration(themeDisplay);

		httpServletRequest.setAttribute("configuration", configuration);

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String scope = ParamUtil.getString(actionRequest, "scope", DigitalPlaceCalendarConfigurationConstants.SCOPE_GROUP);
		setPreference(actionRequest, "scope", scope);

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
