package com.placecube.digitalplace.initialiser.service.constants;

public enum ConfigurationKeys {

	CONTENT_OR_DISPLAY_PAGE_TEMPLATE("id", "preferences", "multivaluedPreferences"),

	DEFAULT("portletId", "portletPreferences","portletMultivaluedPreferences");

	private final String portletIdKey;
	private final String preferencesKey;
	private final String multivaluedPreferencesKey;

	ConfigurationKeys(String portletIdKey, String preferencesKey, String multivaluedPreferencesKey) {
		this.portletIdKey = portletIdKey;
		this.preferencesKey = preferencesKey;
		this.multivaluedPreferencesKey = multivaluedPreferencesKey;
	}

	public String getPortletIdKey() {
		return portletIdKey;
	}

	public String getPreferencesKey() {
		return preferencesKey;
	}

	public String getMultivaluedPreferencesKey() {
		return multivaluedPreferencesKey;
	}
}