package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.configuration.ExpiredContentFilterSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.constants.ExpiredContentFilterSearchConstants;
import com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.service.ExpiredContentFilterSearchService;

@Component(immediate = true, property = { //
		"com.liferay.fragment.entry.processor.portlet.alias=dp-search-expiredcontentfilter", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=dp-search-expired-content-filter-search dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.header-portlet-javascript=/js/com.placecube.digitalplace.search.web.js", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.always-display-default-configuration-icons=true", //
		"javax.portlet.init-param.config-template=/expired-content-search/configuration.jsp", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/expired-content-search/view.jsp", //
		"javax.portlet.name=" + PortletKeys.EXPIRED_CONTENT_FILTER_SEARCH, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class ExpiredContentFilterSearchPortlet extends MVCPortlet {

	@Reference
	private ExpiredContentFilterSearchService expiredContentFilterSearchService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			Optional<ExpiredContentFilterSearchPortletInstanceConfiguration> configurationOptional = expiredContentFilterSearchService.getValidConfiguration(themeDisplay);

			if (configurationOptional.isPresent()) {
				SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

				String includeExpiredContentValue = searchResponse.getSharedSearchSessionSingleValuedAttribute(ExpiredContentFilterSearchConstants.SHARED_SEARCH_ATTRIBUTE_NAME);
				boolean includeExpiredContent = GetterUtil.getBoolean(includeExpiredContentValue, false);

				renderRequest.setAttribute("includeExpiredContent", includeExpiredContent);
				renderRequest.setAttribute("redirectURL", searchResponse.getEscapedSearchResultsIteratorURL());
				renderRequest.setAttribute("title", expiredContentFilterSearchService.getTitle(themeDisplay, configurationOptional.get().title()));
			} else {
				renderRequest.setAttribute("invalidConfiguration", true);
			}

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}
}
