package com.placecube.digitalplace.user.account.web.override;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.constants.LiferayLoginKeys;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@Component(property = { "javax.portlet.name=" + LiferayLoginKeys.FAST_LOGIN_PORTLET_KEY, //
		"javax.portlet.name=" + LiferayLoginKeys.LOGIN_PORTLET_KEY, //
		"mvc.command.name=" + LiferayLoginKeys.MVC_COMMAND_NAME, //
		"mvc.command.name=" + LiferayLoginKeys.MVC_COMMAND_NAME_CREATE_ANONYMOUS_ACCOUNT, //
		"service.ranking:Integer=100" }, service = MVCRenderCommand.class)
public class CreateAccountMVCRenderCommandOverride implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(CreateAccountMVCRenderCommandOverride.class);

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	private MVCRenderCommand mvcRenderCommand;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(renderRequest);
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

			String accountPageURL = accountConfigurationService.getAccountConfiguration(accountContext, renderRequest).createAccountURL();

			Group group = themeDisplay.getScopeGroup();
			Layout layout = layoutLocalService.fetchLayoutByFriendlyURL(group.getGroupId(), false, accountPageURL);

			if (Validator.isNull(layout)) {
				group = groupLocalService.getGroup(accountContext.getCompanyId(), GroupConstants.GUEST);
				layout = layoutLocalService.getFriendlyURLLayout(group.getGroupId(), false, accountPageURL);
			}

			String redirect = group.getDisplayURL(themeDisplay).concat(layout.getFriendlyURL());
			if (!themeDisplay.getScopeGroup().isGuest()) {
				redirect += "?" + RequestKeys.REFERER + "=" + themeDisplay.getScopeGroupId();
			}

			accountContextService.updateContextInSession(accountContext, renderRequest);
			portal.getHttpServletResponse(renderResponse).sendRedirect(redirect);
			return StringPool.BLANK;
		} catch (Exception e) {
			LOG.error("Unable to override create account page", e);
			return mvcRenderCommand.render(renderRequest, renderResponse);
		}
	}

	@Reference(target = "(&(mvc.command.name=" + LiferayLoginKeys.MVC_COMMAND_NAME + ")(javax.portlet.name=" + LiferayLoginKeys.LOGIN_PORTLET_KEY + ")(component.name="
			+ LiferayLoginKeys.CREATE_ACCOUNT_CLASS_NAME + "))")
	protected void setMvcActionCommand(MVCRenderCommand mvcActionCommand) {
		mvcRenderCommand = mvcActionCommand;
	}

}