package com.placecube.digitalplace.accessibility.web.internal.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "accessibility", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.accessibility.web.internal.configuration.AccessibilityCompanyConfiguration", localization = "content/Language", name = "aria-label-name")
public interface AccessibilityCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled", description = "aria-label-description")
	boolean enabled();

}
