package com.placecube.digitalplace.address.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookupService;

public class AddressContextServiceImplTest {

	@InjectMocks
	private AddressContextServiceImpl addressContextServiceImpl;

	@Mock
	private Set<AddressContext> mockAddressContexts;

	@Mock
	private AddressLookupService mockAddressLookupService;

	@Before
	public void activeSetUp() {

		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void searchAddressByPostcode_WhenError_ThenPortalExceptionIsThrown() throws PortalException {
		long companyId = 1;
		String postcode = "BS1 5NP";
		when(mockAddressLookupService.getByPostcode(companyId, postcode, new String[0], false)).thenThrow(new PortalException());

		addressContextServiceImpl.searchAddressByPostcode(companyId, postcode, false);

	}

	@Test
	public void searchAddressByPostcode_WhenNoError_ThenReturnsAddressContexts() throws PortalException {
		long companyId = 1;
		String postcode = "BS1 5NP";
		when(mockAddressLookupService.getByPostcode(companyId, postcode, new String[0], false)).thenReturn(mockAddressContexts);

		Set<AddressContext> result = addressContextServiceImpl.searchAddressByPostcode(companyId, postcode, false);

		assertThat(result, sameInstance(mockAddressContexts));

	}
}
