<%@ include file="init.jsp" %>

<%
	GovUKPayGroupServiceConfiguration govUKPayGroupServiceConfiguration = (GovUKPayGroupServiceConfiguration)request.getAttribute(GovUKPayGroupServiceConfiguration.class.getName());
%>

<portlet:actionURL name="/commerce_payment_methods/edit_govuk_pay_commerce_payment_method_configuration" var="editCommercePaymentMethodActionURL" />

<aui:form action="${editCommercePaymentMethodActionURL}" method="post" name="fm">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="commerceChannelId" type="hidden" value='<%= ParamUtil.getLong(request, "commerceChannelId") %>' />
	<aui:input name="redirect" type="hidden" value="<%= currentURL %>" />

	<aui:input id="govuk-pay-api-key-name" label="govuk-pay-api-key-name" name="settings--apiKeyName--" value="<%= govUKPayGroupServiceConfiguration.keyName() %>" />

	<aui:button-row>
		<aui:button cssClass="btn-lg" type="submit" />

		<aui:button cssClass="btn-lg" href="<%= redirect %>" type="cancel" />
	</aui:button-row>
</aui:form>