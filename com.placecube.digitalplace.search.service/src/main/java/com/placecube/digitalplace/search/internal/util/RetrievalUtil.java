package com.placecube.digitalplace.search.internal.util;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.model.PortletPreferences;
import com.liferay.portal.kernel.service.PortletLocalService;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = RetrievalUtil.class)
public class RetrievalUtil {

	@Reference
	private PortletLocalService portletLocalService;

	@Reference
	private PortletPreferencesLocalService portletPreferencesLocalService;

	public Stream<Portlet> getAllPortletsOnLayout(Layout layout) {
		LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
		return layoutTypePortlet.getAllPortlets(false).stream();
	}

	public Optional<javax.portlet.PortletPreferences> getPortletPreferences(Portlet portlet, ThemeDisplay themeDisplay) {
		if (portlet.isStatic()) {
			return Optional.ofNullable(portletPreferencesLocalService
					.fetchPreferences(themeDisplay.getCompanyId(), themeDisplay.getSiteGroupId(), PortletKeys.PREFS_OWNER_TYPE_LAYOUT, PortletKeys.PREFS_PLID_SHARED, portlet.getPortletId()));
		}

		return Optional.ofNullable(portletPreferencesLocalService
				.fetchPreferences(themeDisplay.getCompanyId(), PortletKeys.PREFS_OWNER_ID_DEFAULT, PortletKeys.PREFS_OWNER_TYPE_LAYOUT, themeDisplay.getPlid(), portlet.getPortletId()));
	}

	public Stream<Portlet> getValidPortletsOnLayout(Layout layout, long companyId) {
		List<PortletPreferences> portletPreferencesList = portletPreferencesLocalService
				.getPortletPreferences(PortletKeys.PREFS_OWNER_ID_DEFAULT, PortletKeys.PREFS_OWNER_TYPE_LAYOUT, layout.getPlid());

		return portletPreferencesList.stream().map(portletPreferences -> portletLocalService.getPortletById(companyId, portletPreferences.getPortletId())).filter(this::isValidPortlet);
	}

	private boolean isValidPortlet(Portlet portlet) {

		boolean valid;

		if (portlet.isInstanceable()) {
			valid = Validator.isNotNull(portlet.getInstanceId());
		} else {
			valid = Validator.isNull(portlet.getInstanceId());
		}

		return valid;

	}

}
