package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.placecube.digitalplace.user.account.connector.constants.AccountConnectorConstant;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.CONFIRMATION_PAGE }, service = MVCRenderCommand.class)
public class ConfirmationPageMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(renderRequest);

			boolean isGeneratePasswordWhenLoggedInUserCreateAccount = accountContextService.isGeneratePasswordWhenLoggedInUserCreateAccount(accountContext);

			if (isGeneratePasswordWhenLoggedInUserCreateAccount) {
				renderRequest.setAttribute(RequestKeys.USER_EMAIL_ADDRESS_TO_SEND_RESET_PASSWORD, accountContext.getEmailAddress());
			}

			String redirectUrl = accountContextService.generateProfileRedirectUrl(accountContext);

			accountContextService.clearContext(renderRequest);

			renderRequest.setAttribute(AccountConnectorConstant.MODEL_ID, accountContext.getAccountConnectorModelId());
			renderRequest.setAttribute(RequestKeys.REDIRECT_URL, redirectUrl);
			renderRequest.setAttribute(RequestKeys.ACCOUNT_CREATED_WITH_ERRORS, SessionErrors.contains(renderRequest, "accountCreatedWithErrors"));
			renderRequest.setAttribute(RequestKeys.EMAIL_VERIFICATION_ENABLED, accountConfigurationService.getEmailVerificationEnabled(accountContext));
		} catch (UserAccountException e) {
			throw new PortletException(e);
		}

		return "/confirmation-page.jsp";
	}

}
