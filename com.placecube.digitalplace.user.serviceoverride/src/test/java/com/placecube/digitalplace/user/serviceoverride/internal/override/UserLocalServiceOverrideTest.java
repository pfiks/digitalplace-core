package com.placecube.digitalplace.user.serviceoverride.internal.override;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.security.auth.Authenticator;
import com.placecube.digitalplace.user.serviceoverride.constants.AuthenticationMode;
import com.placecube.digitalplace.user.serviceoverride.model.AuthenticationActionContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Ticket;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.TicketLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.digitalplace.user.serviceoverride.internal.util.ReindexUserUtil;
import com.placecube.digitalplace.user.serviceoverride.internal.util.ServiceContextDetailsUtil;
import com.placecube.digitalplace.user.serviceoverride.internal.util.UserServiceOverrideActionUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class UserLocalServiceOverrideTest extends PowerMockito {

	@Mock
	private List<Long> mockOriginalGroupIds;

	@Mock
	private ReindexUserUtil mockReindexUserUtil;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServiceContextDetailsUtil mockServiceContextDetailsUtil;

	@Mock
	private Ticket mockTicket;

	@Mock
	private TicketLocalService mockTicketLocalService;

	@Mock
	private List<Long> mockUpdatedGroupIds;

	@Mock
	private User mockUser1;

	@Mock
	private User mockUser2;

	@Mock
	private User mockUser3;

	@Mock
	private List<UserGroupRole> mockUserGroupRoles;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private UserServiceOverrideActionUtil mockUserServiceOverrideActionUtil;

	@InjectMocks
	private UserLocalServiceOverride userLocalServiceOverride;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void addGroupUser_WithGroupIdAndUserIdParameters_WhenNoError_ThenAddsTheGroupUserAndExecutesTheActionsForUserAddedToGroup() {
		long userId = 1;
		long groupId = 2;

		userLocalServiceOverride.addGroupUser(groupId, userId);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).addGroupUser(groupId, userId);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId, groupId);
	}

	@Test
	public void addGroupUser_WithGroupIdAndUserParameters_WhenNoError_ThenAddsTheGroupUserAndExecutesTheActionsForUserAddedToGroup() {
		long userId = 1;
		long groupId = 2;
		when(mockUser1.getUserId()).thenReturn(userId);

		userLocalServiceOverride.addGroupUser(groupId, mockUser1);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).addGroupUser(groupId, mockUser1);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId, groupId);
	}

	@Test
	public void addGroupUsers_WithGroupIdAndUserIdsParameters_WhenNoError_ThenAddsTheGroupUsersAndExecutesTheActionsForUsersAddedToGroup() throws PortalException {
		long userId1 = 1;
		long userId2 = 22;
		long groupId = 2;
		long[] userIds = new long[] { userId1, userId2 };

		userLocalServiceOverride.addGroupUsers(groupId, userIds);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).addGroupUsers(groupId, userIds);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId1, groupId);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId2, groupId);
	}

	@Test
	public void addGroupUsers_WithGroupIdAndUsersParameters_WhenNoError_ThenAddsTheGroupUsersAndExecutesTheActionsForUsersAddedToGroup() throws PortalException {
		long userId1 = 1;
		long userId2 = 22;
		long groupId = 2;
		when(mockUser1.getUserId()).thenReturn(userId1);
		when(mockUser2.getUserId()).thenReturn(userId2);

		List<User> users = new LinkedList<>();
		users.add(mockUser1);
		users.add(mockUser2);

		userLocalServiceOverride.addGroupUsers(groupId, users);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).addGroupUsers(groupId, users);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId1, groupId);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId2, groupId);
	}

	@Test
	public void addUser_WithParams_WhenNoError_ThenReturnsTheUserForTheExecuteActionsForUserCreation() throws PortalException {
		long creatorUserId = 1;
		long companyId = 2;
		boolean autoPassword = true;
		String password1 = "password1Val";
		String password2 = "password2Val";
		boolean autoScreenName = true;
		String screenName = "screenNameVal";
		String emailAddress = "emailAddressVal";
		Locale locale = Locale.CANADA_FRENCH;
		String firstName = "firstNameVal";
		String middleName = "middleNameVal";
		String lastName = "lastNameVal";
		long prefixId = 4;
		long suffixId = 5;
		boolean male = true;
		int birthdayMonth = 6;
		int birthdayDay = 7;
		int birthdayYear = 8;
		String jobTitle = "jobTitleVal";
		int type = 99999;
		long[] groupIds = new long[] { 11, 22 };
		long[] organizationIds = new long[] { 333, 444 };
		long[] roleIds = new long[] { 55, 66 };
		long[] userGroupIds = new long[] { 777, 888 };
		boolean sendEmail = true;
		when(mockUserLocalService.addUser(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, locale, firstName, middleName, lastName, prefixId,
				suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, type, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, mockServiceContext)).thenReturn(mockUser1);
		when(mockUserServiceOverrideActionUtil.executeActionsForUserCreation(mockUser1, Optional.of(mockServiceContext))).thenReturn(mockUser2);

		User result = userLocalServiceOverride.addUser(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, locale, firstName, middleName, lastName,
				prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, type, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, mockServiceContext);

		assertThat(result, sameInstance(mockUser2));

		InOrder inOrder = inOrder(mockUserLocalService, mockServiceContextDetailsUtil);
		inOrder.verify(mockServiceContextDetailsUtil, times(1)).populateMissingServiceContextDetails(companyId, mockServiceContext);
		inOrder.verify(mockUserLocalService, times(1)).addUser(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, locale, firstName, middleName,
				lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, type, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, mockServiceContext);
	}

	@Test
	public void addUser_WithUserParameter_WhenNoError_ThenReturnsTheUserForTheExecuteActionsForUserCreation() {
		when(mockUserLocalService.addUser(mockUser1)).thenReturn(mockUser2);
		when(mockUserServiceOverrideActionUtil.executeActionsForUserCreation(mockUser2, Optional.empty())).thenReturn(mockUser3);

		User result = userLocalServiceOverride.addUser(mockUser1);

		assertThat(result, sameInstance(mockUser3));
	}

	@Test
	public void addUserWithWorkflow_WhenNoError_ThenReturnsTheUserForTheExecuteActionsForUserCreation() throws PortalException {
		long creatorUserId = 1;
		long companyId = 2;
		boolean autoPassword = true;
		String password1 = "password1Val";
		String password2 = "password2Val";
		boolean autoScreenName = true;
		String screenName = "screenNameVal";
		String emailAddress = "emailAddressVal";
		Locale locale = Locale.CANADA_FRENCH;
		String firstName = "firstNameVal";
		String middleName = "middleNameVal";
		String lastName = "lastNameVal";
		long prefixId = 4;
		long suffixId = 5;
		boolean male = true;
		int birthdayMonth = 6;
		int birthdayDay = 7;
		int birthdayYear = 8;
		String jobTitle = "jobTitleVal";
		int type = 99999;
		long[] groupIds = new long[] { 11, 22 };
		long[] organizationIds = new long[] { 333, 444 };
		long[] roleIds = new long[] { 55, 66 };
		long[] userGroupIds = new long[] { 777, 888 };
		boolean sendEmail = true;
		when(mockUserLocalService.addUserWithWorkflow(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, locale, firstName, middleName, lastName,
				prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, type, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, mockServiceContext)).thenReturn(mockUser1);
		when(mockUserServiceOverrideActionUtil.executeActionsForUserCreation(mockUser1, Optional.of(mockServiceContext))).thenReturn(mockUser2);

		User result = userLocalServiceOverride.addUserWithWorkflow(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, locale, firstName,
				middleName, lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, type, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, mockServiceContext);

		assertThat(result, sameInstance(mockUser2));
		InOrder inOrder = inOrder(mockUserLocalService, mockServiceContextDetailsUtil);
		inOrder.verify(mockServiceContextDetailsUtil, times(1)).populateMissingServiceContextDetails(companyId, mockServiceContext);
		inOrder.verify(mockUserLocalService, times(1)).addUserWithWorkflow(creatorUserId, companyId, autoPassword, password1, password2, autoScreenName, screenName, emailAddress, locale, firstName,
				middleName, lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, type, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, mockServiceContext);
	}

	@Test
	public void authenticateByEmailAddress_WhenNoErrors_ThenExecutesActionsAfterUserAuthentication() throws PortalException {

		long companyId = 1L;
		String emailAddress = "email@email.com";
		String password = "thisisasecret";
		Map<String, String[]> headerMap = Collections.emptyMap();
		Map<String, String[]> parameterMap = Collections.emptyMap();
		Map<String, Object> resultsMap = Collections.emptyMap();
		String screenNameNotConfigured = StringPool.BLANK;
		long userIdNotConfigured = 0L;

		ArgumentCaptor<AuthenticationActionContext> captor = ArgumentCaptor.forClass(AuthenticationActionContext.class);
		when(mockUserLocalService.authenticateByEmailAddress(companyId, emailAddress, password, headerMap, parameterMap, resultsMap)).thenReturn(Authenticator.SUCCESS);

		int result = userLocalServiceOverride.authenticateByEmailAddress(companyId, emailAddress, password, headerMap, parameterMap, resultsMap);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).authenticateByEmailAddress(companyId, emailAddress, password, headerMap, parameterMap, resultsMap);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsAfterUserAuthentication(captor.capture());

		AuthenticationActionContext authenticationActionContext = captor.getValue();
		assertThat(authenticationActionContext.getAuthenticationMode(), equalTo(AuthenticationMode.EMAIL_ADDRESS));
		assertThat(authenticationActionContext.getEmailAddress(), equalTo(emailAddress));
		assertThat(authenticationActionContext.getScreenName(), equalTo(screenNameNotConfigured));
		assertThat(authenticationActionContext.getUserId(), equalTo(userIdNotConfigured));
		assertThat(result, equalTo(Authenticator.SUCCESS));

	}

	@Test
	public void authenticateByScreenName_WhenNoErrors_ThenExecutesActionsAfterUserAuthentication() throws PortalException {

		long companyId = 1L;
		String screenName = "a.user";
		String password = "thisisasecret";
		Map<String, String[]> headerMap = Collections.emptyMap();
		Map<String, String[]> parameterMap = Collections.emptyMap();
		Map<String, Object> resultsMap = Collections.emptyMap();
		String emailAddressNotConfigured = StringPool.BLANK;
		long userIdNotConfigured = 0L;

		ArgumentCaptor<AuthenticationActionContext> captor = ArgumentCaptor.forClass(AuthenticationActionContext.class);
		when(mockUserLocalService.authenticateByScreenName(companyId, screenName, password, headerMap, parameterMap, resultsMap)).thenReturn(Authenticator.SUCCESS);

		int result = userLocalServiceOverride.authenticateByScreenName(companyId, screenName, password, headerMap, parameterMap, resultsMap);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).authenticateByScreenName(companyId, screenName, password, headerMap, parameterMap, resultsMap);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsAfterUserAuthentication(captor.capture());

		AuthenticationActionContext authenticationActionContext = captor.getValue();
		assertThat(authenticationActionContext.getAuthenticationMode(), equalTo(AuthenticationMode.SCREEN_NAME));
		assertThat(authenticationActionContext.getEmailAddress(), equalTo(emailAddressNotConfigured));
		assertThat(authenticationActionContext.getScreenName(), equalTo(screenName));
		assertThat(authenticationActionContext.getUserId(), equalTo(userIdNotConfigured));
		assertThat(result, equalTo(Authenticator.SUCCESS));

	}

	@Test
	public void authenticateByUserId_WhenNoErrors_ThenExecutesActionsAfterUserAuthentication() throws PortalException {

		long companyId = 1L;
		long userId = 123L;
		String password = "thisisasecret";
		Map<String, String[]> headerMap = Collections.emptyMap();
		Map<String, String[]> parameterMap = Collections.emptyMap();
		Map<String, Object> resultsMap = Collections.emptyMap();
		String emailAddressNotConfigured = StringPool.BLANK;
		String screenNameNotConfigured = StringPool.BLANK;

		ArgumentCaptor<AuthenticationActionContext> captor = ArgumentCaptor.forClass(AuthenticationActionContext.class);
		when(mockUserLocalService.authenticateByUserId(companyId, userId, password, headerMap, parameterMap, resultsMap)).thenReturn(Authenticator.SUCCESS);

		int result = userLocalServiceOverride.authenticateByUserId(companyId, userId, password, headerMap, parameterMap, resultsMap);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).authenticateByUserId(companyId, userId, password, headerMap, parameterMap, resultsMap);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsAfterUserAuthentication(captor.capture());

		AuthenticationActionContext authenticationActionContext = captor.getValue();
		assertThat(authenticationActionContext.getAuthenticationMode(), equalTo(AuthenticationMode.USER_ID));
		assertThat(authenticationActionContext.getEmailAddress(), equalTo(emailAddressNotConfigured));
		assertThat(authenticationActionContext.getScreenName(), equalTo(screenNameNotConfigured));
		assertThat(authenticationActionContext.getUserId(), equalTo(userId));
		assertThat(result, equalTo(Authenticator.SUCCESS));

	}

	@Test
	public void deleteGroupUser_WithGroupIdAndUserIdParameters_WhenNoError_ThenRemovesTheGroupUserAndExecutesTheActionsForUserRemovedToGroup() {
		long userId = 1;
		long groupId = 2;

		userLocalServiceOverride.deleteGroupUser(groupId, userId);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).deleteGroupUser(groupId, userId);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId, groupId, Optional.empty());
	}

	@Test
	public void deleteGroupUser_WithGroupIdAndUserParameters_WhenNoError_ThenRemovesTheGroupUserAndExecutesTheActionsForUserRemovedToGroup() {
		long userId = 1;
		long groupId = 2;
		when(mockUser1.getUserId()).thenReturn(userId);

		userLocalServiceOverride.deleteGroupUser(groupId, mockUser1);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).deleteGroupUser(groupId, mockUser1);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId, groupId, Optional.empty());
	}

	@Test
	public void deleteGroupUsers_WithGroupIdAndUserIdsParameters_WhenNoError_ThenRemovesTheGroupUsersAndExecutesTheActionsForUsersRemovedToGroup() {
		long userId1 = 1;
		long userId2 = 22;
		long groupId = 2;
		long[] userIds = new long[] { userId1, userId2 };

		userLocalServiceOverride.deleteGroupUsers(groupId, userIds);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).deleteGroupUsers(groupId, userIds);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId1, groupId, Optional.empty());
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId2, groupId, Optional.empty());
	}

	@Test
	public void deleteGroupUsers_WithGroupIdAndUsersParameters_WhenNoError_ThenRemovesTheGroupUsersAndExecutesTheActionsForUsersRemovedToGroup() {
		long userId1 = 1;
		long userId2 = 22;
		long groupId = 2;
		when(mockUser1.getUserId()).thenReturn(userId1);
		when(mockUser2.getUserId()).thenReturn(userId2);

		List<User> users = new LinkedList<>();
		users.add(mockUser1);
		users.add(mockUser2);

		userLocalServiceOverride.deleteGroupUsers(groupId, users);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).deleteGroupUsers(groupId, users);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId1, groupId, Optional.empty());
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId2, groupId, Optional.empty());
	}

	@Test
	public void deleteUser_WithUserIdParameter_WhenNoError_ThenReturnsTheDeletedUserAndExecutesActionsForUserRemoval() throws PortalException {
		long userId = 123;
		when(mockUserLocalService.deleteUser(userId)).thenReturn(mockUser1);

		User result = userLocalServiceOverride.deleteUser(userId);

		assertThat(result, sameInstance(mockUser1));
		verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserDeletion(mockUser1);
	}

	@Test
	public void deleteUser_WithUserParameter_WhenNoError_ThenReturnsTheDeletedUserAndExecutesActionsForUserRemoval() throws PortalException {
		when(mockUserLocalService.deleteUser(mockUser1)).thenReturn(mockUser2);

		User result = userLocalServiceOverride.deleteUser(mockUser1);

		assertThat(result, sameInstance(mockUser2));
		verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserDeletion(mockUser2);
	}

	@Test
	public void setGroupUsers_WithGroupIdAndUserIdsParameters_WhenNoError_ThenSetsTheGroupUsersAndExecutesTheActionsForUsersAddedToGroup() {
		long userId1 = 1;
		long userId2 = 22;
		long groupId = 2;
		long[] userIds = new long[] { userId1, userId2 };

		userLocalServiceOverride.setGroupUsers(groupId, userIds);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).setGroupUsers(groupId, userIds);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId1, groupId);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId2, groupId);
	}

	@Test
	public void unsetGroupUsers_WithGroupIdAndUserIdsParameters_WhenNoErrorAndServiceContextNotSpecified_ThenUnsetsTheGroupUsersAndExecutesTheActionsForUsersRemovedToGroup() throws PortalException {
		long userId1 = 1;
		long userId2 = 22;
		long groupId = 2;
		long[] userIds = new long[] { userId1, userId2 };

		userLocalServiceOverride.unsetGroupUsers(groupId, userIds, null);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).unsetGroupUsers(groupId, userIds, null);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId1, groupId, Optional.empty());
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId2, groupId, Optional.empty());
	}

	@Test
	public void unsetGroupUsers_WithGroupIdAndUserIdsParameters_WhenNoErrorAndServiceContextSpecified_ThenUnsetsTheGroupUsersAndExecutesTheActionsForUsersRemovedToGroup() throws PortalException {
		long userId1 = 1;
		long userId2 = 22;
		long groupId = 2;
		long[] userIds = new long[] { userId1, userId2 };

		userLocalServiceOverride.unsetGroupUsers(groupId, userIds, mockServiceContext);

		InOrder inOrder = inOrder(mockUserLocalService, mockUserServiceOverrideActionUtil);
		inOrder.verify(mockUserLocalService, times(1)).unsetGroupUsers(groupId, userIds, mockServiceContext);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId1, groupId, Optional.of(mockServiceContext));
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId2, groupId, Optional.of(mockServiceContext));
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateAgreedToTermsOfUse_WhenNoError_ThenReindexesTheUser(boolean agreedToTermsOfUse) throws PortalException {
		long userId = 1;
		when(mockUserLocalService.updateAgreedToTermsOfUse(userId, agreedToTermsOfUse)).thenReturn(mockUser1);

		User result = userLocalServiceOverride.updateAgreedToTermsOfUse(userId, agreedToTermsOfUse);

		assertThat(result, sameInstance(mockUser1));
		verify(mockReindexUserUtil, times(1)).reindexUser(mockUser1);
	}

	@Test
	@Parameters({ "true", "false" })
	public void updateEmailAddressVerified_WhenNoError_ThenReindexesTheUser(boolean emailAddressVerified) throws PortalException {
		long userId = 1;
		when(mockUserLocalService.updateEmailAddressVerified(userId, emailAddressVerified)).thenReturn(mockUser1);

		User result = userLocalServiceOverride.updateEmailAddressVerified(userId, emailAddressVerified);

		assertThat(result, sameInstance(mockUser1));
		verify(mockReindexUserUtil, times(1)).reindexUser(mockUser1);
	}

	@Test
	public void updateUser_WhenNoError_ThenReturnsTheUpdatedUserAndExecuteActionsForUserAddedAndRemovedFromGroups() throws PortalException {
		long userId = 1;
		boolean passwordReset = true;
		String oldPassword = "oldPasswordVal";
		String newPassword1 = "password2Val";
		String newPassword2 = "password3Val";
		String screenName = "screenNameVal";
		String emailAddress = "emailAddressVal";
		String reminderQueryQuestion = "reminderQueryQuestionVal";
		String reminderQueryAnswer = "reminderQueryAnswerVal";
		String firstName = "firstNameVal";
		String middleName = "middleNameVal";
		String lastName = "lastNameVal";
		long prefixId = 4;
		long suffixId = 5;
		boolean male = true;
		int birthdayMonth = 6;
		int birthdayDay = 7;
		int birthdayYear = 8;
		String jobTitle = "jobTitleVal";
		long[] groupIds = new long[] { 11, 22 };
		long[] organizationIds = new long[] { 333, 444 };
		long[] roleIds = new long[] { 55, 66 };
		long[] userGroupIds = new long[] { 777, 888 };
		boolean portrait = true;
		byte[] portraitBytes = new byte[9];
		String languageId = "languageIdVal";
		String timeZoneId = "timeZoneIdVal";
		String greeting = "greetingVal";
		String comments = "commentsVal";
		String smsSn = "smsSnVal";
		String jabberSn = "jabberSnVal";
		String facebookSn = "facebookSnVal";
		String skypeSn = "skypeSnVal";
		String twitterSn = "twitterSnVal";
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser1);
		when(mockUserLocalService.updateUser(userId, oldPassword, newPassword1, newPassword2, passwordReset, reminderQueryQuestion, reminderQueryAnswer, screenName, emailAddress, portrait,
				portraitBytes, languageId, timeZoneId, greeting, comments, firstName, middleName, lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, smsSn, facebookSn,
				jabberSn, skypeSn, twitterSn, jobTitle, groupIds, organizationIds, roleIds, mockUserGroupRoles, userGroupIds, mockServiceContext)).thenReturn(mockUser2);

		when(mockUserServiceOverrideActionUtil.getGroupIds(mockUser1)).thenReturn(mockOriginalGroupIds);
		when(mockUserServiceOverrideActionUtil.getGroupIds(mockUser2)).thenReturn(mockUpdatedGroupIds);

		long groupIdAdded1 = 1234L;
		long groupIdAdded2 = 43423L;

		List<Long> groupIdsAdded = new ArrayList<>();
		groupIdsAdded.add(groupIdAdded1);
		groupIdsAdded.add(groupIdAdded2);

		long groupIdRemoved1 = 8989L;
		long groupIdRemoved2 = 7878L;

		List<Long> groupIdsRemoved = new ArrayList<>();
		groupIdsRemoved.add(groupIdRemoved1);
		groupIdsRemoved.add(groupIdRemoved2);

		when(mockUserServiceOverrideActionUtil.getGroupIdsAdded(mockOriginalGroupIds, mockUpdatedGroupIds)).thenReturn(groupIdsAdded);
		when(mockUserServiceOverrideActionUtil.getGroupIdsRemoved(mockOriginalGroupIds, mockUpdatedGroupIds)).thenReturn(groupIdsRemoved);

		User result = userLocalServiceOverride.updateUser(userId, oldPassword, newPassword1, newPassword2, passwordReset, reminderQueryQuestion, reminderQueryAnswer, screenName, emailAddress,
				portrait, portraitBytes, languageId, timeZoneId, greeting, comments, firstName, middleName, lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, smsSn,
				facebookSn, jabberSn, skypeSn, twitterSn, jobTitle, groupIds, organizationIds, roleIds, mockUserGroupRoles, userGroupIds, mockServiceContext);

		assertThat(result, sameInstance(mockUser2));

		verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUpdateUser(mockUser2, Optional.of(mockServiceContext));
		verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId, groupIdAdded1);
		verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserAddedToGroup(userId, groupIdAdded2);
		verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId, groupIdRemoved1, Optional.of(mockServiceContext));
		verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsForUserRemovedFromGroup(userId, groupIdRemoved2, Optional.of(mockServiceContext));
	}

	@Test
	public void verifyEmailAddress_WhenExceptionRetrievingTheTicket_ThenExecuteActionsAfterVerifyEmailAddressAndReindexesTheUserIdZero() throws PortalException {
		String ticketKey = "key";
		when(mockTicketLocalService.getTicket(ticketKey)).thenThrow(new PortalException());

		userLocalServiceOverride.verifyEmailAddress(ticketKey);

		InOrder inOrder = inOrder(mockUserLocalService, mockReindexUserUtil, mockUserServiceOverrideActionUtil, mockTicketLocalService);
		inOrder.verify(mockTicketLocalService, times(1)).getTicket(ticketKey);
		inOrder.verify(mockUserLocalService, times(1)).verifyEmailAddress(ticketKey);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsAfterVerifyEmailAddress(0L);
		inOrder.verify(mockReindexUserUtil, times(1)).reindexUser(0L);
	}

	@Test
	public void verifyEmailAddress_WhenNoError_ThenExecuteActionsAfterVerifyEmailAddressAndReindexesTheUser() throws PortalException {
		long userId = 1;
		String ticketKey = "key";
		when(mockTicketLocalService.getTicket(ticketKey)).thenReturn(mockTicket);
		when(mockTicket.getClassPK()).thenReturn(userId);

		userLocalServiceOverride.verifyEmailAddress(ticketKey);

		InOrder inOrder = inOrder(mockUserLocalService, mockReindexUserUtil, mockUserServiceOverrideActionUtil, mockTicketLocalService);
		inOrder.verify(mockTicketLocalService, times(1)).getTicket(ticketKey);
		inOrder.verify(mockUserLocalService, times(1)).verifyEmailAddress(ticketKey);
		inOrder.verify(mockUserServiceOverrideActionUtil, times(1)).executeActionsAfterVerifyEmailAddress(userId);
		inOrder.verify(mockReindexUserUtil, times(1)).reindexUser(userId);
	}

}
