package com.placecube.digitalplace.ddl.constants;

public final class UserCustomFields {

	public static final String AGE = "age";

	public static final String PIN = "pin";

	public static final String PRIMARY_PHONE = "PrimaryPhone";

	private UserCustomFields() {
	}
}
