package com.placecube.digitalplace.addresslookup.simple.internal.lookup;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.address.service.AddressLookup;
import com.placecube.digitalplace.addresslookup.simple.internal.lookup.configuration.SimpleAddressLookupCompanyConfiguration;
import com.placecube.digitalplace.addresslookup.simple.internal.lookup.service.SimpleAddressLookupParsingService;
import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;
import com.placecube.digitalplace.addresslookup.simple.service.SimpleAddressLookupEntryLocalService;

@Component(immediate = true, service = AddressLookup.class)
public class SimpleAddressLookup implements AddressLookup {

	private static final Log LOG = LogFactoryUtil.getLog(SimpleAddressLookup.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private SimpleAddressLookupEntryLocalService simpleAddressLookupEntryLocalService;

	@Reference
	private SimpleAddressLookupParsingService simpleAddressLookupParsingService;

	@Override
	public boolean enabled(long companyId) {
		try {
			SimpleAddressLookupCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(SimpleAddressLookupCompanyConfiguration.class, companyId);
			return configuration.enabled();
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public Set<AddressContext> findByKeyword(long companyId, String keyword, String[] paramsAndValues) {
		throw new UnsupportedOperationException("Unsupported method");
	}

	@Override
	public Set<AddressContext> findByPostcode(long companyId, String postcode, String[] paramsAndValues) {
		List<SimpleAddressLookupEntry> simpleAddressLookupEntries = simpleAddressLookupEntryLocalService.getByCompanyIdAndPostcode(companyId, postcode);
		return simpleAddressLookupParsingService.getAddressContexts(simpleAddressLookupEntries);
	}

	@Override
	public Optional<AddressContext> findByUprn(long companyId, String uprn, String[] paramsAndValues) {
		Optional<SimpleAddressLookupEntry> simpleAddressLookupEntry = simpleAddressLookupEntryLocalService.getByCompanyIdAndUprn(companyId, uprn);
		if (simpleAddressLookupEntry.isPresent()) {
			return Optional.of(simpleAddressLookupParsingService.getAddressContext(simpleAddressLookupEntry.get()));
		}
		return Optional.empty();
	}

	@Override
	public int getWeight(long companyId) {
		try {
			SimpleAddressLookupCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(SimpleAddressLookupCompanyConfiguration.class, companyId);
			return configuration.weight();
		} catch (Exception e) {
			LOG.error(e);
			return 0;
		}
	}

	@Override
	public boolean national(long companyId) {
		try {
			SimpleAddressLookupCompanyConfiguration configuration = configurationProvider.getCompanyConfiguration(SimpleAddressLookupCompanyConfiguration.class, companyId);
			return configuration.national();
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

}
