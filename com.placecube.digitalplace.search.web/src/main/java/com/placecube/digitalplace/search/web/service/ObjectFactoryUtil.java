package com.placecube.digitalplace.search.web.service;

import java.util.LinkedHashMap;
import java.util.Map;

import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.TermQuery;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.search.generic.TermQueryImpl;
import com.placecube.digitalplace.search.shared.model.SortByOption;

public final class ObjectFactoryUtil {

	private ObjectFactoryUtil() {

	}

	public static BooleanQuery createBooleanQuery() {
		return new BooleanQueryImpl();
	}

	public static TermQuery createTermQuery(String fieldName, String fieldValue) {
		return new TermQueryImpl(fieldName, fieldValue);
	}

	public static Map<String, SortByOption> getSortByLinkedHashMap() {
		return new LinkedHashMap<>();
	}

}
