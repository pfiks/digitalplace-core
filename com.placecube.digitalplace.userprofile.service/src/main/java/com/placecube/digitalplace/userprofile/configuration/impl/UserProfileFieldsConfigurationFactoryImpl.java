package com.placecube.digitalplace.userprofile.configuration.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsCompanyConfiguration;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfigurationFactory;

@Component(service = UserProfileFieldsConfigurationFactory.class)
public class UserProfileFieldsConfigurationFactoryImpl implements UserProfileFieldsConfigurationFactory {

	private static final Log LOG = LogFactoryUtil.getLog(UserProfileFieldsConfigurationFactoryImpl.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private GroupLocalService groupLocalService;

	@Override
	public UserProfileFieldsConfiguration getCompanyUserProfileFieldsConfiguration(long companyId) throws ConfigurationException {
		try {
			return new CompanyUserProfileFieldsConfiguration(companyId);
		} catch (ConfigurationException e) {
			LOG.error(e);
			throw new ConfigurationException(e);
		}
	}

	private class CompanyUserProfileFieldsConfiguration implements UserProfileFieldsConfiguration {

		private final UserProfileFieldsCompanyConfiguration userProfileFieldsCompanyConfiguration;

		private CompanyUserProfileFieldsConfiguration(long companyId) throws ConfigurationException {
			userProfileFieldsCompanyConfiguration = configurationProvider.getCompanyConfiguration(UserProfileFieldsCompanyConfiguration.class, companyId);
		}

		@Override
		public boolean addressEnabled() {
			return userProfileFieldsCompanyConfiguration.addressEnabled();
		}

		@Override
		public boolean addressRequired() {
			return userProfileFieldsCompanyConfiguration.addressRequired();
		}

		@Override
		public boolean businessPhoneNumberEnabled() {
			return userProfileFieldsCompanyConfiguration.businessPhoneNumberEnabled();
		}

		@Override
		public boolean businessPhoneNumberRequired() {
			return userProfileFieldsCompanyConfiguration.businessPhoneNumberRequired();
		}

		@Override
		public boolean dateOfBirthEnabled() {
			return userProfileFieldsCompanyConfiguration.dateOfBirthEnabled();
		}

		@Override
		public boolean dateOfBirthRequired() {
			return userProfileFieldsCompanyConfiguration.dateOfBirthRequired();
		}

		@Override
		public String[] expandoFieldsSettings() {
			return userProfileFieldsCompanyConfiguration.expandoFieldsSettings();
		}

		@Override
		public String generatedEmailSuffix() {
			return userProfileFieldsCompanyConfiguration.generatedEmailSuffix();
		}

		@Override
		public boolean homePhoneNumberEnabled() {
			return userProfileFieldsCompanyConfiguration.homePhoneNumberEnabled();
		}

		@Override
		public boolean homePhoneNumberRequired() {
			return userProfileFieldsCompanyConfiguration.homePhoneNumberRequired();
		}

		@Override
		public boolean jobTitleEnabled() {
			return userProfileFieldsCompanyConfiguration.jobTitleEnabled();
		}

		@Override
		public boolean jobTitleRequired() {
			return userProfileFieldsCompanyConfiguration.jobTitleRequired();
		}

		@Override
		public boolean mobilePhoneNumberEnabled() {
			return userProfileFieldsCompanyConfiguration.mobilePhoneNumberEnabled();
		}

		@Override
		public boolean mobilePhoneNumberRequired() {
			return userProfileFieldsCompanyConfiguration.mobilePhoneNumberRequired();
		}

		@Override
		public boolean twoFactorAuthenticationPromptEnabled() {
			return userProfileFieldsCompanyConfiguration.twoFactorAuthenticationPromptEnabled();
		}
	}

}
