package com.placecube.digitalplace.appointment.booking.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AppointmentBookingSlot implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<AppointmentBookingEntry> appointmentsAlreadyBooked;
	private final LocalDateTime endDateTime;
	private final int maxAppointments;
	private final LocalDateTime startDateTime;

	public AppointmentBookingSlot(LocalDateTime startDateTime, LocalDateTime endDateTime, int maxAppointments) {
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		appointmentsAlreadyBooked = new ArrayList<>();
		this.maxAppointments = maxAppointments;
	}

	public List<AppointmentBookingEntry> getAppointmentsAlreadyBooked() {
		return appointmentsAlreadyBooked;
	}

	public Date getEndDate() {
		return Date.from(endDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	public LocalDateTime getEndDateTime() {
		return endDateTime;
	}

	public int getMaxAppointments() {
		return maxAppointments;
	}

	public Date getStartDate() {
		return Date.from(startDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	public LocalDateTime getStartDateTime() {
		return startDateTime;
	}

	public void setAppointmentsAlreadyBooked(List<AppointmentBookingEntry> appointmentsAlreadyBooked) {
		this.appointmentsAlreadyBooked = appointmentsAlreadyBooked;
	}

	@Override
	public String toString() {
		return "[startDateTime: " + startDateTime.toString() + ", endDateTime: " + endDateTime.toString() + "]";
	}

}
