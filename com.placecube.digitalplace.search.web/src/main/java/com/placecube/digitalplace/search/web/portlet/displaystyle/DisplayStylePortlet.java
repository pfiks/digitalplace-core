package com.placecube.digitalplace.search.web.portlet.displaystyle;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchDisplayService;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-search-displaystyle", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=dp-search-display-style dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.always-display-default-configuration-icons=true", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/display-style/view.jsp", //
		"javax.portlet.name=" + PortletKeys.DISPLAY_STYLE, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class DisplayStylePortlet extends MVCPortlet {

	@Reference
	private SharedSearchDisplayService sharedSearchDisplayService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {

			SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

			String displayStyleGridURL = sharedSearchDisplayService.getDisplayStyleGridURL(searchResponse);
			String displayStyleListURL = sharedSearchDisplayService.getDisplayStyleListURL(searchResponse);

			renderRequest.setAttribute("isDisplayListSelected", sharedSearchDisplayService.isDisplayStyleList(renderRequest));
			renderRequest.setAttribute("displayStyleGridURL", displayStyleGridURL);
			renderRequest.setAttribute("displayStyleListURL", displayStyleListURL);

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}