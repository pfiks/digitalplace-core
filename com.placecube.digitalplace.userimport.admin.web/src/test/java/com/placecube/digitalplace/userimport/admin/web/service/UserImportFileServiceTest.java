package com.placecube.digitalplace.userimport.admin.web.service;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.document.library.kernel.antivirus.AntivirusScannerUtil;
import com.liferay.document.library.kernel.exception.FileExtensionException;
import com.liferay.document.library.kernel.exception.FileSizeException;
import com.liferay.petra.string.CharPool;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.FileUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CSVFormat.class, CSVParser.class, AntivirusScannerUtil.class, FileUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.document.library.kernel.antivirus.AntivirusScannerUtil" })
public class UserImportFileServiceTest extends PowerMockito {

	private final String CSV = "csv";

	@Mock
	private BufferedReader mockBufferedReader;

	@Mock
	private JSONArray mockCSVDataArray;

	@Mock
	private CSVFormat mockCSVFormat;

	@Mock
	private CSVParser mockCSVParser;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private InputStreamReader mockInputStreamReader;

	@InjectMocks
	private UserImportFileService mockUserImportFileService;

	@Mock
	private JSONObject mockUserJsonObject;

	@Test(expected = RuntimeException.class)
	public void readCSVFile_WhenCSVParserFailed_ThenThrowRuntimeException() throws Exception {

		whenNew(JSONArray.class).withNoArguments().thenReturn(mockCSVDataArray);
		when(CSVFormat.newFormat(CharPool.COMMA).withIgnoreEmptyLines().withTrim(true)).thenReturn(mockCSVFormat);
		whenNew(InputStreamReader.class).withArguments(mockInputStream, StandardCharsets.UTF_8).thenReturn(mockInputStreamReader);
		whenNew(BufferedReader.class).withArguments(mockInputStreamReader).thenReturn(mockBufferedReader);
		when(mockCSVFormat.parse(mockBufferedReader)).thenThrow(IOException.class);

		mockUserImportFileService.readCSVFile(mockInputStream);

		Mockito.doThrow(RuntimeException.class);
	}

	@Test(expected = PortalException.class)
	public void readCSVFile_WhenInputStreamIsEmpty_ThenThrowParseException() throws Exception {

		CSVFormat csvFormat = mock(CSVFormat.class);
		whenNew(InputStreamReader.class).withArguments(mockInputStream, StandardCharsets.UTF_8).thenReturn(mockInputStreamReader);
		whenNew(BufferedReader.class).withArguments(mockInputStreamReader).thenReturn(mockBufferedReader);
		when(csvFormat.parse(mockBufferedReader)).thenReturn(mockCSVParser);

		mockUserImportFileService.readCSVFile(mockInputStream);

		Mockito.doThrow(PortalException.class);
	}

	@Test
	public void readCSVFile_WhenNoError_ThenReturnUsersJSONArray() throws Exception {

		CSVFormat csvFormat = mock(CSVFormat.class);
		InputStream inputStream = getInputStreamWithData();
		JSONArray users = new JSONArray();
		JSONObject userJsonObject = buildUserJson();
		users.add(userJsonObject);
		whenNew(InputStreamReader.class).withArguments(inputStream, StandardCharsets.UTF_8).thenReturn(mockInputStreamReader);
		whenNew(BufferedReader.class).withArguments(mockInputStreamReader).thenReturn(mockBufferedReader);
		when(csvFormat.parse(mockBufferedReader)).thenReturn(mockCSVParser);

		JSONArray userJsonArray = mockUserImportFileService.readCSVFile(inputStream);

		assertEquals(users, userJsonArray);
	}

	@Before
	public void setUp() {
		mockStatic(AntivirusScannerUtil.class, FileUtil.class);
	}

	@Test(expected = FileExtensionException.class)
	public void validateFile_WhenFileExtentionIsMissing_ThenThrowFileExtensionException() throws Exception {
		File file = mock(File.class);
		when(file.getName()).thenReturn(StringPool.BLANK);
		mockUserImportFileService.validateFile(file);
		Mockito.doThrow(FileExtensionException.class);
	}

	@Test(expected = FileSizeException.class)
	public void validateFile_WhenFileIsMissing_ThenThrowFileSizeException() throws Exception {
		mockUserImportFileService.validateFile(null);
		Mockito.doThrow(FileSizeException.class);
	}

	@Test
	public void validateFile_WhenValidCSVFileUploaded_ThenDoNothing() throws Exception {
		File file = mock(File.class);
		when(FileUtil.getExtension(file.getName())).thenReturn(CSV);
		mockUserImportFileService.validateFile(file);
		Mockito.atLeastOnce();
	}

	private JSONObject buildUserJson() {
		Map<String, String> userFieldmap = new HashMap<String, String>();
		userFieldmap.put("Address Info Status", "");
		userFieldmap.put("Email Address Verified", "");
		userFieldmap.put("Create Date", "1640975400000");
		userFieldmap.put("First Name", "Test 1");
		userFieldmap.put("Birthday", "");
		userFieldmap.put("City", "");
		userFieldmap.put("Job Title", "");
		userFieldmap.put("Postcode", "");
		userFieldmap.put("Business phone", "");
		userFieldmap.put("Personal phone", "");
		userFieldmap.put("Preferred Method Of Contact", "");
		userFieldmap.put("Last Name", "1");
		userFieldmap.put("Address Line 1", "");
		userFieldmap.put("Agent Id", "");
		userFieldmap.put("Address Line 2", "");
		userFieldmap.put("Mobile Phone", "");
		userFieldmap.put("Email Address", "test1@liferay.com");
		userFieldmap.put("Uprn", "");
		userFieldmap.put("Password", "");
		userFieldmap.put("Address Line 3", "");

		return new JSONObject(userFieldmap);
	}

	private InputStream getInputStreamWithData() {
		return new ByteArrayInputStream(
				"Email Address,First Name,Last Name,Job Title,Email Address Verified,Create Date,Birthday,Address Line 1,City,Postcode,Personal phone,Address Info Status,Agent Id,Preferred Method Of Contact,Uprn,Password,Address Line 2,Address Line 3,Business phone,Mobile Phone \ntest1@liferay.com,Test 1,1,,,1640975400000,,,,,,,,,,,,,,"
						.getBytes());
	}

}
