<%@page import="com.placecube.digitalplace.search.web.portlet.category.constants.CategoryFacetDisplayConstants"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="../init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />
	<aui:input name="internalPortletId" type="hidden" value="${internalPortletId}" />

	<c:set var="dropDownDisplayStyle" value="<%=CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN %>" />
	<c:set var="checkboxDisplayStyle" value="<%=CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_CHECKBOX %>" />
	<c:set var="treeDisplayStyle" value="<%=CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_TREE %>" />

	<liferay-frontend:edit-form-body>
		<dp-frontend:fieldset-group>

			<liferay-frontend:fieldset>

				<aui:input type="number" label="maximum-items-to-display" name="maxCategoriesToShow" value="${maxCategoriesToShow}">
					<aui:validator name="min">1</aui:validator>
				</aui:input>

				<aui:input
					label="filter-colour-label"
					name="filterColour"
					type="text"
					value="${ filterColour }"
				/>

				<aui:input
					label="custom-heading"
					name="customHeading"
					type="text"
					value="${ customHeading }"
				/>

				<aui:input
					label="custom-css-class"
					name="customCssClass"
					type="text"
					value="${ customCssClass }"
				/>

				<div class="row">
					<div class="col-md-4 col-xs-12">
						<aui:input name="invisible" label="invisible" type="toggle-switch" value="${ invisible }" />
					</div>
					<div class="col-md-4 col-xs-12">
						<aui:input name="showAllCategoriesAlways" label="show-all-categories-always" type="toggle-switch" value="${ showAllCategoriesAlways }" />
					</div>
					<div class="col-md-4 col-xs-12">
						<aui:input name="alphaSort" label="alpha-sort" type="toggle-switch" value="${ alphaSort }" />
					</div>
				</div>


				<aui:select name="displayStyle" label="display-style" value="${displayStyle }" >
					<aui:option label="${checkboxDisplayStyle }" selected="${displayStyle eq checkboxDisplayStyle }" value="${checkboxDisplayStyle }"/>
					<aui:option label="${dropDownDisplayStyle }" selected="${displayStyle eq dropDownDisplayStyle }" value="${dropDownDisplayStyle }"/>
					<aui:option label="${treeDisplayStyle }" selected="${displayStyle eq treeDisplayStyle }" value="${treeDisplayStyle }"/>
				</aui:select>

				<aui:input name="showHeadingAsFirstDropdownOption" label="show-heading-as-first-dropdown-option" type="toggle-switch" value="${ showHeadingAsFirstDropdownOption }" />

				<aui:input name="checkboxesPanelCollapsedByDefault" label="checkboxes-panel-collapsed-by-default" type="toggle-switch" value="${ checkboxesPanelCollapsedByDefault }" />

				<aui:input name="showFrequencies" label="show-frequencies" type="toggle-switch" helpMessage="show-frequencies-next-to-checkboxes" value="${ showFrequencies }" />

				<aui:input name="categoriesQueryAnded" label="all-selected-categories-present-in-search-results" type="toggle-switch" value="${categoriesQueryAnded}"/>

				<div class="treeViewDepth">
					<aui:input type="number" label="maximum-tree-view-depth" name="maxTreeViewDepth" value="${maxTreeViewDepth}">
						<aui:validator name="min">1</aui:validator>
					</aui:input>
				</div>

			</liferay-frontend:fieldset>

			<c:if test="${not empty availableVocabularies}">
				<liferay-frontend:fieldset label="select-display-category-ids">
					<ul>
						<c:forEach items="${availableVocabularies}" var="availableVocabulary">
						<li class="list-unstyled">
							<aui:input type="checkbox" name="displayVocabularyIds" label="${availableVocabulary.getTitle(locale)}" value="${availableVocabulary.getVocabularyId()}"
								checked="${displayVocabularyIds.contains(availableVocabulary.getVocabularyId())}"/>
							<c:set var="availableCategories" value="${availableVocabulary.getCategories()}" />
							<c:if test="${not empty availableCategories}">
								<ul >
									<c:forEach items="${availableCategories}" var="availableCategory">
										<li class="list-unstyled">
											<aui:input type="checkbox" name="displayCategoryIds" label="${availableCategory.getTitle(locale)}" value="${availableCategory.getCategoryId()}"
												checked="${displayCategoryIds.contains(availableCategory.getCategoryId())}"/>
										</li>
									</c:forEach>
								</ul>
							</c:if>
						</li>
						</c:forEach>
					</ul>
				</liferay-frontend:fieldset>
				<liferay-frontend:fieldset label="select-category-ids-to-be-preselected">
					<ul>
						<c:forEach items="${availableVocabularies}" var="availableVocabulary">
						<li class="list-unstyled">
							<aui:input type="checkbox" name="preselectedVocabularyIds" label="${availableVocabulary.getTitle(locale)}" value="${availableVocabulary.getVocabularyId()}"
								checked="${preselectedVocabularyIds.contains(availableVocabulary.getVocabularyId())}"/>
							<c:set var="availableCategories" value="${availableVocabulary.getCategories()}" />
							<c:if test="${not empty availableCategories}">
								<ul >
									<c:forEach items="${availableCategories}" var="availableCategory">
										<li class="list-unstyled">
											<aui:input type="checkbox" name="preselectedCategoryIds" label="${availableCategory.getTitle(locale)}" value="${availableCategory.getCategoryId()}"
												checked="${preselectedCategoryIds.contains(availableCategory.getCategoryId())}"/>
										</li>
									</c:forEach>
								</ul>
							</c:if>
						</li>
						</c:forEach>
					</ul>
				</liferay-frontend:fieldset>
			</c:if>

		</dp-frontend:fieldset-group>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>
