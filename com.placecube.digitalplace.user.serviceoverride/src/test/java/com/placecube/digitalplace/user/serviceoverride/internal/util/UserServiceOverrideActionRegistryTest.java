package com.placecube.digitalplace.user.serviceoverride.internal.util;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.reflect.internal.WhiteboxImpl;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerCustomizerFactory.ServiceWrapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.placecube.digitalplace.user.serviceoverride.service.UserServiceOverrideAction;

public class UserServiceOverrideActionRegistryTest extends PowerMockito {

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private ServiceWrapper<UserServiceOverrideAction> mockServiceWrapper1;

	@Mock
	private ServiceWrapper<UserServiceOverrideAction> mockServiceWrapper2;

	@Mock
	private ServiceWrapper<UserServiceOverrideAction> mockServiceWrapper3;

	@Mock
	private UserServiceOverrideAction mockUserServiceOverrideAction1;

	@Mock
	private UserServiceOverrideAction mockUserServiceOverrideAction2;

	@Mock
	private UserServiceOverrideAction mockUserServiceOverrideAction3;

	@InjectMocks
	private UserServiceOverrideActionRegistry userServiceOverrideActionRegistry;

	@Before
	public void activateSetup() {
		initMocks(this);
		userServiceOverrideActionRegistry = spy(new UserServiceOverrideActionRegistry());
	}

	@Test
	public void getUserServiceOverrideActions_WhenNoError_ThenReturnsTheActionsOrderedByPriority() {
		configureInternalMap();

		when(mockServiceWrapper1.getService()).thenReturn(mockUserServiceOverrideAction1);
		when(mockServiceWrapper2.getService()).thenReturn(mockUserServiceOverrideAction2);
		when(mockServiceWrapper3.getService()).thenReturn(mockUserServiceOverrideAction3);

		when(mockServiceWrapper1.getProperties()).thenReturn(Collections.singletonMap(UserServiceOverrideAction.SERVICE_EXECUTION_PRIORITY, 2));
		when(mockServiceWrapper2.getProperties()).thenReturn(Collections.singletonMap(UserServiceOverrideAction.SERVICE_EXECUTION_PRIORITY, 1));
		when(mockServiceWrapper3.getProperties()).thenReturn(Collections.singletonMap(UserServiceOverrideAction.SERVICE_EXECUTION_PRIORITY, 3));

		List<UserServiceOverrideAction> results = userServiceOverrideActionRegistry.getUserServiceOverrideActions();

		assertThat(results, contains(mockUserServiceOverrideAction2, mockUserServiceOverrideAction1, mockUserServiceOverrideAction3));
	}

	private void configureInternalMap() {
		ServiceTrackerMap<String, ServiceWrapper<UserServiceOverrideAction>> userListenerActionMap = new ServiceTrackerMap<>() {

			@Override
			public void close() {
			}

			@Override
			public boolean containsKey(String key) {
				return false;
			}

			@Override
			public ServiceWrapper<UserServiceOverrideAction> getService(String key) {
				return null;
			}

			@Override
			public Set<String> keySet() {
				return null;
			}

			@Override
			public Collection<ServiceWrapper<UserServiceOverrideAction>> values() {
				Collection<ServiceWrapper<UserServiceOverrideAction>> actions = new ArrayList<>();
				actions.add(mockServiceWrapper1);
				actions.add(mockServiceWrapper2);
				actions.add(mockServiceWrapper3);
				return actions;
			}
		};
		WhiteboxImpl.setInternalState(userServiceOverrideActionRegistry, "userListenerActionMap", userListenerActionMap);
	}

}
