package com.placecube.digitalplace.addresslookup.ordnancesurvey.constants;

public final class OrdnanceSurveyConstants {

	public static final String BUILDING_NAME = "BUILDING_NAME";

	public static final String BUILDING_NUMBER = "BUILDING_NUMBER";

	public static final String DEPENDENT_LOCALITY = "DEPENDENT_LOCALITY";

	public static final String DEPENDENT_THOROUGHFARE_NAME = "DEPENDENT_THOROUGHFARE_NAME";

	public static final String DOUBLE_DEPENDENT_LOCALITY = "DOUBLE_DEPENDENT_LOCALITY";

	public static final String DPA = "DPA";

	public static final String LOCALITY_NAME = "LOCALITY_NAME";

	public static final String LPI = "LPI";

	public static final String ORGANISATION = "ORGANISATION";

	public static final String ORGANISATION_NAME = "ORGANISATION_NAME";

	public static final String PAO_END_NUMBER = "PAO_END_NUMBER";

	public static final String PAO_END_SUFFIX = "PAO_END_SUFFIX";

	public static final String PAO_START_NUMBER = "PAO_START_NUMBER";

	public static final String PAO_START_SUFFIX = "PAO_START_SUFFIX";

	public static final String PAO_TEXT = "PAO_TEXT";

	public static final String POST_TOWN = "POST_TOWN";

	public static final String POSTCODE = "POSTCODE";

	public static final String POSTCODE_LOCATOR = "POSTCODE_LOCATOR";

	public static final String SAO_END_NUMBER = "SAO_END_NUMBER";

	public static final String SAO_END_SUFFIX = "SAO_END_SUFFIX";

	public static final String SAO_START_NUMBER = "SAO_START_NUMBER";

	public static final String SAO_START_SUFFIX = "SAO_START_SUFFIX";

	public static final String SAO_TEXT = "SAO_TEXT";

	public static final String STREET_DESCRIPTION = "STREET_DESCRIPTION";

	public static final String SUB_BUILDING_NAME = "SUB_BUILDING_NAME";

	public static final String THOROUGHFARE_NAME = "THOROUGHFARE_NAME";

	public static final String TOWN_NAME = "TOWN_NAME";

	public static final String UPRN = "UPRN";

	private OrdnanceSurveyConstants() {
	}
}
