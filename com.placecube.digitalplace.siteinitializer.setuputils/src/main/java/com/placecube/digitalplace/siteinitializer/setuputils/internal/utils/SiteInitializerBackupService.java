package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@Component(immediate = true, service = SiteInitializerBackupService.class)
public class SiteInitializerBackupService {

	private static final Log LOG = LogFactoryUtil.getLog(SiteInitializerBackupService.class);

	@Reference
	private SiteInitializerBackupUtils siteInitializerBackupUtils;

	public long backupContentInTemporaryGroup(long currentGroupId) throws PortalException {
		if (siteInitializerBackupUtils.hasContentToBackUp(currentGroupId)) {
			LOG.info("Backing up content in current groupId: " + currentGroupId);

			long backupGroupId = siteInitializerBackupUtils.createBackupGroup(currentGroupId);

			siteInitializerBackupUtils.moveSiteNavigationMenus(currentGroupId, backupGroupId);

			LOG.info("Content backed up in temporary groupId: " + backupGroupId);
			return backupGroupId;
		}

		LOG.info("No content to backup for groupId: " + currentGroupId);
		return 0L;

	}

	public void importBackedUpContentAndRemoveTemporaryGroup(long currentGroupId, long backupGroupId) {
		if (backupGroupId > 0) {
			LOG.info("Importing backed up content from backupGroupId: " + backupGroupId + " in " + currentGroupId);
			try {
				siteInitializerBackupUtils.moveSiteNavigationMenus(backupGroupId, currentGroupId);
			} finally {
				siteInitializerBackupUtils.removeBackupGroup(backupGroupId);
				LOG.info("Removed back-up group with groupId: " + backupGroupId);
			}
		}
	}

}
