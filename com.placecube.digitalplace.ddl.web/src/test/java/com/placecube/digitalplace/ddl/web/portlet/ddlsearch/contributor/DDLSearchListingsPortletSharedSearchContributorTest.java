package com.placecube.digitalplace.ddl.web.portlet.ddlsearch.contributor;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.ddl.constants.DDLIndexerField;
import com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration.DDLSearchListingsConfigurationConstants;
import com.placecube.digitalplace.search.shared.constants.SharedSearchAttributeKeys;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;

public class DDLSearchListingsPortletSharedSearchContributorTest extends PowerMockito {

	@InjectMocks
	private DDLSearchListingsPortletSharedSearchContributor ddlSearchListingsPortletSharedSearchContributor;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenNoError_ThenAddsAbooleanQueryInTheContributorSettingsToMatchTheconfiguredStructureId() throws ConfigurationException {
		mockConfigurationDetails();
		String structureId = "123";
		when(mockPortletPreferences.getValue(DDLSearchListingsConfigurationConstants.STRUCTURE_ID, StringPool.BLANK)).thenReturn(structureId);

		ddlSearchListingsPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).addBooleanQuery(DDLIndexerField.DDM_STRUCTURE_ID, structureId, BooleanClauseOccur.MUST);
	}

	@Test
	public void contribute_WhenNoError_ThenAddsSharedSearchSessionAttributeWithConfiguredDelta() throws ConfigurationException {
		mockConfigurationDetails();
		String delta = "21";
		when(mockPortletPreferences.getValue(DDLSearchListingsConfigurationConstants.NUMBER_RESULTS_PER_PAGE, StringPool.BLANK)).thenReturn(delta);

		ddlSearchListingsPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).addSharedSearchSessionSingleValuedAttribute(SharedSearchAttributeKeys.SEARCH_CONTAINER_DELTA, delta);
	}

	@Test
	public void contribute_WhenNoError_ThenSetsAllowEmptySearchTrueInTheContributorSettings() throws ConfigurationException {
		mockConfigurationDetails();

		ddlSearchListingsPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).setAllowEmptySearch(true);
	}

	@Test
	public void contribute_WhenNoError_ThenSetsDDLRecordAsEntryClassNamesInTheContributorSettings() throws ConfigurationException {
		mockConfigurationDetails();

		ddlSearchListingsPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).setEntryClassNames(DDLRecord.class.getName());
	}

	@Test
	public void contribute_WhenNoError_ThenSetsScopeGroupIdAsASearchGroupIds() throws ConfigurationException {
		mockConfigurationDetails();
		long groupId = 12;
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);

		ddlSearchListingsPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		verify(mockSharedSearchContributorSettings, times(1)).setSearchGroupIds(new long[] { groupId });
	}

	@Test(expected = SystemException.class)
	public void contribute_WhenNoPreferencesFound_ThenThrowsSystemException() {
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.empty());

		ddlSearchListingsPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);
	}

	private void mockConfigurationDetails() throws ConfigurationException {
		when(mockSharedSearchContributorSettings.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
	}

}
