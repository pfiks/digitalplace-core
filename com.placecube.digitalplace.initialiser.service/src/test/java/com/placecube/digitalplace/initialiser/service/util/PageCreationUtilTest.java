package com.placecube.digitalplace.initialiser.service.util;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.UserActiveException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.initialiser.service.constants.ConfigurationKeys;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.LayoutPageTemplateCollectionInitializer;
import com.placecube.initializer.service.ModelCreationFactory;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@SuppressWarnings({ "unused", "rawtypes" })
public class PageCreationUtilTest extends PowerMockito {

	private static final Class MOCK_CLASS = UserActiveException.class;

	@Mock
	private Map<Integer, Set<PortletContext>> mockColumnConfigs;

	@Mock
	private Map<String, String> mockContentSets;

	@Mock
	private FragmentContext mockFragmentContext;

	@Mock
	private FragmentContext mockFragmentContext1;

	@Mock
	private FragmentContext mockFragmentContext2;

	@Mock
	private Set<FragmentContext> mockFragments;

	@Mock
	private JSONArray mockJSONArrayColumns;

	@Mock
	private JSONArray mockJSONArrayContentSets;

	@Mock
	private JSONArray mockJSONArrayPortlets1;

	@Mock
	private JSONArray mockJSONArrayPortlets2;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONObject1;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private JSONObject mockJSONObject3;

	@Mock
	private JSONObject mockJSONObjectColumn1;

	@Mock
	private JSONObject mockJSONObjectColumn2;

	@Mock
	private JSONObject mockJSONObjectPortlet1;

	@Mock
	private JSONObject mockJSONObjectPortlet2;

	@Mock
	private JSONObject mockJSONObjectPortlet3;

	@Mock
	private LayoutContext mockLayoutContext;

	@Mock
	private LayoutContextUtil mockLayoutContextUtil;

	@Mock
	private LayoutPageTemplateCollection mockLayoutPageTemplateCollection;

	@Mock
	private LayoutPageTemplateCollectionInitializer mockLayoutPageTemplateCollectionInitializer;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Mock
	private ModelCreationFactory mockModelCreationFactory;

	@Mock
	private Map<String, String> mockPageTypeSettings;

	@Mock
	private ParserUtil mockParserUtil;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletContext mockPortletContext1;

	@Mock
	private PortletContext mockPortletContext2;

	@Mock
	private PortletContext mockPortletContext3;

	@Mock
	private Map<String, String> mockPortletPreferences1;

	@Mock
	private Map<String, String> mockPortletPreferences2;

	@Mock
	private Map<String, String> mockPortletPreferences3;

	@Mock
	private Set<PortletContext> mockPortlets;

	@Mock
	private Map<String, String> mockPrefsPlaceholders;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private PageCreationUtil pageCreationUtil;

	@Test
	public void getColumns_WhenAColumnHasNoPortlets_ThenReturnsMapMapWithColumnWithEmptyPortletsList() {
		when(mockJSONArrayColumns.length()).thenReturn(1);
		when(mockJSONArrayColumns.getJSONObject(0)).thenReturn(mockJSONObjectColumn1);
		when(mockJSONObjectColumn1.getInt("columnId")).thenReturn(11);
		when(mockJSONObjectColumn1.getJSONArray("portlets")).thenReturn(null);

		Map<Integer, Set<PortletContext>> results = pageCreationUtil.getColumns(mockJSONArrayColumns, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT);

		assertThat(results.size(), equalTo(1));
		assertTrue(results.get(11).isEmpty());
	}

	@Test
	public void getColumns_WhenNoError_ThenReturnsMapWithColumnIdAndListOfPortletsForEachColumn() {
		when(mockJSONArrayColumns.length()).thenReturn(2);
		when(mockJSONArrayColumns.getJSONObject(0)).thenReturn(mockJSONObjectColumn1);
		when(mockJSONArrayColumns.getJSONObject(1)).thenReturn(mockJSONObjectColumn2);

		when(mockJSONObjectColumn1.getJSONArray("portlets")).thenReturn(mockJSONArrayPortlets1);
		when(mockJSONObjectColumn1.getInt("columnId")).thenReturn(11);
		when(mockJSONArrayPortlets1.length()).thenReturn(2);
		when(mockJSONArrayPortlets1.getJSONObject(0)).thenReturn(mockJSONObjectPortlet1);
		when(mockJSONArrayPortlets1.getJSONObject(1)).thenReturn(mockJSONObjectPortlet2);

		when(mockJSONObjectColumn2.getJSONArray("portlets")).thenReturn(mockJSONArrayPortlets2);
		when(mockJSONObjectColumn2.getInt("columnId")).thenReturn(22);
		when(mockJSONArrayPortlets2.length()).thenReturn(1);
		when(mockJSONArrayPortlets2.getJSONObject(0)).thenReturn(mockJSONObjectPortlet3);

		mockPortletDetails(mockJSONObjectPortlet1, mockPortletPreferences1, mockPortletContext1);
		mockPortletDetails(mockJSONObjectPortlet2, mockPortletPreferences2, mockPortletContext2);
		mockPortletDetails(mockJSONObjectPortlet3, mockPortletPreferences3, mockPortletContext3);

		Map<Integer, Set<PortletContext>> results = pageCreationUtil.getColumns(mockJSONArrayColumns, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT);

		assertThat(results.size(), equalTo(2));
		assertThat(results.get(11), containsInAnyOrder(mockPortletContext1, mockPortletContext2));
		assertThat(results.get(22), containsInAnyOrder(mockPortletContext3));
		verify(mockParserUtil, times(1)).configureMultivaluedPreferences(mockPortletContext1, mockJSONObjectPortlet1, ConfigurationKeys.DEFAULT);
		verify(mockParserUtil, times(1)).configureMultivaluedPreferences(mockPortletContext3, mockJSONObjectPortlet3, ConfigurationKeys.DEFAULT);
	}

	@Test
	public void getColumns_WhenNullJSONArray_ThenEmptyMap() {
		Map<Integer, Set<PortletContext>> results = pageCreationUtil.getColumns(null, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT);

		assertTrue(results.isEmpty());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContentPageToCreate_WhenNoError_ThenConfiguresLayoutContextWithCommonPageDetails(boolean privateLayout) {
		mockContentPageLayoutCreation(privateLayout);

		pageCreationUtil.getContentPageToCreate(MOCK_CLASS, mockJSONObject1, mockFragments, mockPortlets, mockPageTypeSettings);

		verify(mockLayoutContextUtil, times(1)).addCommonPageDetails(MOCK_CLASS, mockJSONObject1, mockLayoutContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContentPageToCreate_WhenNoError_ThenConfiguresLayoutContextWithFragments(boolean privateLayout) {
		mockContentPageLayoutCreation(privateLayout);

		pageCreationUtil.getContentPageToCreate(MOCK_CLASS, mockJSONObject1, mockFragments, mockPortlets, mockPageTypeSettings);

		verify(mockLayoutContext, times(1)).setFragmentContexts(mockFragments);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContentPageToCreate_WhenNoError_ThenConfiguresLayoutContextWithPortlets(boolean privateLayout) {
		mockContentPageLayoutCreation(privateLayout);

		pageCreationUtil.getContentPageToCreate(MOCK_CLASS, mockJSONObject1, mockFragments, mockPortlets, mockPageTypeSettings);

		verify(mockLayoutContext, times(1)).setPortletContexts(mockPortlets);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContentPageToCreate_WhenNoError_ThenConfiguresLayoutContextWithStructureContent(boolean privateLayout) {
		when(mockJSONObject1.getString("contentStructureDefinition")).thenReturn("structureContentValue");
		mockContentPageLayoutCreation(privateLayout);

		pageCreationUtil.getContentPageToCreate(MOCK_CLASS, mockJSONObject1, mockFragments, mockPortlets, mockPageTypeSettings);

		verify(mockLayoutContext, times(1)).setStructureContent("structureContentValue");
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContentPageToCreate_WhenNoError_ThenConfiguresLayoutContextWithTypeSettings(boolean privateLayout) {
		mockContentPageLayoutCreation(privateLayout);

		pageCreationUtil.getContentPageToCreate(MOCK_CLASS, mockJSONObject1, mockFragments, mockPortlets, mockPageTypeSettings);

		verify(mockLayoutContext, times(1)).setTypeSettings(mockPageTypeSettings);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getContentPageToCreate_WhenNoError_ThenReturnsLayoutContext(boolean privateLayout) {
		mockContentPageLayoutCreation(privateLayout);

		LayoutContext result = pageCreationUtil.getContentPageToCreate(MOCK_CLASS, mockJSONObject1, mockFragments, mockPortlets, mockPageTypeSettings);

		assertThat(result, sameInstance(mockLayoutContext));
	}

	@Test
	public void getFragments_WhenNoError_ThenReturnsSetOfFragments() {
		when(mockJSONArrayColumns.length()).thenReturn(2);
		when(mockJSONArrayColumns.getJSONObject(0)).thenReturn(mockJSONObjectColumn1);
		when(mockJSONArrayColumns.getJSONObject(1)).thenReturn(mockJSONObjectColumn2);
		when(mockParserUtil.getFragment(mockJSONObjectColumn1)).thenReturn(mockFragmentContext1);
		when(mockParserUtil.getFragment(mockJSONObjectColumn2)).thenReturn(mockFragmentContext2);

		Set<FragmentContext> results = pageCreationUtil.getFragments(mockJSONArrayColumns);

		assertThat(results, containsInAnyOrder(mockFragmentContext1, mockFragmentContext2));
	}

	@Test
	public void getFragments_WhenNullJsonArray_ThenReturnsEmtpySet() {
		Set<FragmentContext> results = pageCreationUtil.getFragments(null);

		assertTrue(results.isEmpty());
	}

	@Test(expected = PortalException.class)
	public void getJSONObject_WhenExceptionCreatingJsonObject_ThenThrowsPortalException() throws Exception {
		String path = "pathValue";
		Class clazz = UserActiveException.class;
		String jsonString = "jsonStringValue";
		when(StringUtil.read(clazz.getClassLoader(), path)).thenReturn(jsonString);
		when(mockJSONFactory.createJSONObject(jsonString)).thenThrow(new JSONException());

		pageCreationUtil.getJSONObject(clazz, path);
	}

	@Test(expected = PortalException.class)
	public void getJSONObject_WhenExceptionReadingFile_ThenThrowsPortalException() throws Exception {
		String path = "pathValue";
		Class clazz = UserActiveException.class;
		String jsonString = "jsonStringValue";
		when(StringUtil.read(clazz.getClassLoader(), path)).thenThrow(new IOException());

		pageCreationUtil.getJSONObject(clazz, path);
	}

	@Test
	public void getJSONObject_WhenNoError_ThenReturnsJSONObject() throws Exception {
		String path = "pathValue";
		Class clazz = UserActiveException.class;
		String jsonString = "jsonStringValue";
		when(StringUtil.read(clazz.getClassLoader(), path)).thenReturn(jsonString);
		when(mockJSONFactory.createJSONObject(jsonString)).thenReturn(mockJSONObject1);

		JSONObject result = pageCreationUtil.getJSONObject(clazz, path);

		assertThat(result, sameInstance(mockJSONObject1));
	}

	@Test
	public void getLayoutPageTemplateCollection_WhenNameNotSpecified_ThenReturnsNull() throws PortalException {
		when(mockJSONObject1.getString("collectionName")).thenReturn("");
		LayoutPageTemplateCollection result = pageCreationUtil.getLayoutPageTemplateCollection(mockJSONObject1, mockServiceContext);

		assertNull(result);
	}

	@Test
	public void getLayoutPageTemplateCollection_WhenNameSpecified_ThenReturnsTheCreatedCollection() throws PortalException {
		when(mockJSONObject1.getString("collectionName")).thenReturn("myCollectionName");
		when(mockLayoutPageTemplateCollectionInitializer.getOrCreateLayoutPageTemplateCollection("myCollectionName", mockServiceContext)).thenReturn(mockLayoutPageTemplateCollection);

		LayoutPageTemplateCollection result = pageCreationUtil.getLayoutPageTemplateCollection(mockJSONObject1, mockServiceContext);

		assertThat(result, sameInstance(mockLayoutPageTemplateCollection));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLinkToLayoutPageToCreate_WhenNoError_ThenConfiguresLayoutContextWithCommonPageDetails(boolean privateLayout) {
		mockLinkToLayoutPageLayoutCreation(privateLayout);

		pageCreationUtil.getLinkToLayoutPageToCreate(MOCK_CLASS, mockJSONObject1);

		verify(mockLayoutContextUtil, times(1)).addCommonPageDetails(MOCK_CLASS, mockJSONObject1, mockLayoutContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLinkToLayoutPageToCreate_WhenNoError_ThenConfiguresLayoutContextWithLinkToLayoutFriendlyURL(boolean privateLayout) {
		mockLinkToLayoutPageLayoutCreation(privateLayout);
		when(mockJSONObject1.getString("linkToPageURL")).thenReturn("linkToPageURLValue");

		pageCreationUtil.getLinkToLayoutPageToCreate(MOCK_CLASS, mockJSONObject1);

		verify(mockLayoutContext, times(1)).setLinkToLayoutFriendlyURL("linkToPageURLValue");
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLinkToLayoutPageToCreateWhenNoError_ThenReturnsLayoutContext(boolean privateLayout) {
		mockLinkToLayoutPageLayoutCreation(privateLayout);

		LayoutContext result = pageCreationUtil.getLinkToLayoutPageToCreate(MOCK_CLASS, mockJSONObject1);

		assertThat(result, sameInstance(mockLayoutContext));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getPageFromPageTemplateToCreate_WhenNoError_ThenReturnsLayoutContextWithTemplateClassNameAndPKSet(boolean isPrivate) {
		String pageTitle = "title";
		String friendlyURL = "/title";
		long classNameId = 123;
		long entryId = 456;

		when(mockModelCreationFactory.createContentPageLayoutContext(pageTitle, friendlyURL, isPrivate)).thenReturn(mockLayoutContext);
		when(mockLayoutPageTemplateEntry.getLayoutPageTemplateEntryId()).thenReturn(entryId);
		when(mockPortal.getClassNameId(LayoutPageTemplateEntry.class)).thenReturn(classNameId);
		LayoutContext result = pageCreationUtil.getPageFromPageTemplateToCreate(pageTitle, friendlyURL, isPrivate, mockLayoutPageTemplateEntry);

		assertThat(result, sameInstance(mockLayoutContext));
		verify(mockLayoutContext, times(1)).setClassPK(entryId);
		verify(mockLayoutContext, times(1)).setClassNameId(classNameId);
	}

	@Test
	public void getPageTemplateToCreate_WhenNoError_ThenReturnsLayoutContext() {
		mockPageTemplateCreation();

		LayoutContext result = pageCreationUtil.getPageTemplateToCreate(mockJSONObject1);

		assertThat(result, sameInstance(mockLayoutContext));
	}

	@Test
	public void getPortlets_WhenNoError_ThenConfiguresPreferencesInEachPortlet() {
		long groupId = 1;
		when(mockParserUtil.getContentSets(groupId, mockJSONArrayContentSets)).thenReturn(mockContentSets);
		when(mockJSONArrayColumns.length()).thenReturn(2);
		when(mockJSONArrayColumns.getJSONObject(0)).thenReturn(mockJSONObjectColumn1);
		when(mockJSONArrayColumns.getJSONObject(1)).thenReturn(mockJSONObjectColumn2);
		when(mockParserUtil.getPortletWithPlaceholder(mockJSONObjectColumn1, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletContext1);
		when(mockParserUtil.getPortletWithPlaceholder(mockJSONObjectColumn2, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletContext2);
		when(mockParserUtil.getPortletPreferencesWithPlaceholders(mockJSONObjectColumn1, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletPreferences1);
		when(mockParserUtil.getPortletPreferencesWithPlaceholders(mockJSONObjectColumn2, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletPreferences2);

		pageCreationUtil.getPortlets(groupId, mockJSONArrayColumns, mockJSONArrayContentSets, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT);

		verify(mockParserUtil, times(1)).configurePortletPreferences(mockPortletContext1, mockPortletPreferences1, mockContentSets);
		verify(mockParserUtil, times(1)).configureMultivaluedPreferences(mockPortletContext1, mockJSONObjectColumn1, ConfigurationKeys.DEFAULT);
		verify(mockParserUtil, times(1)).configurePortletPreferences(mockPortletContext2, mockPortletPreferences2, mockContentSets);
		verify(mockParserUtil, times(1)).configureMultivaluedPreferences(mockPortletContext2, mockJSONObjectColumn2, ConfigurationKeys.DEFAULT);
	}

	@Test
	public void getPortlets_WhenNoError_ThenReturnsThePortlets() {
		long groupId = 1;
		when(mockParserUtil.getContentSets(groupId, mockJSONArrayContentSets)).thenReturn(mockContentSets);
		when(mockJSONArrayColumns.length()).thenReturn(2);
		when(mockJSONArrayColumns.getJSONObject(0)).thenReturn(mockJSONObjectColumn1);
		when(mockJSONArrayColumns.getJSONObject(1)).thenReturn(mockJSONObjectColumn2);
		when(mockParserUtil.getPortletWithPlaceholder(mockJSONObjectColumn1, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletContext1);
		when(mockParserUtil.getPortletWithPlaceholder(mockJSONObjectColumn2, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletContext2);
		when(mockParserUtil.getPortletPreferencesWithPlaceholders(mockJSONObjectColumn1, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletPreferences1);
		when(mockParserUtil.getPortletPreferencesWithPlaceholders(mockJSONObjectColumn2, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(mockPortletPreferences2);

		Set<PortletContext> results = pageCreationUtil.getPortlets(groupId, mockJSONArrayColumns, mockJSONArrayContentSets, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT);

		assertThat(results, containsInAnyOrder(mockPortletContext1, mockPortletContext2));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getWidgetPageToCreate_WhenNoError_ThenConfiguresLayoutContextWithColumnPortlets(boolean privateLayout) {
		mockWidgetPageLayoutCreation(privateLayout);

		pageCreationUtil.getWidgetPageToCreate(MOCK_CLASS, mockJSONObject1, mockColumnConfigs);

		verify(mockLayoutContext, times(1)).setColumnPortletContexts(mockColumnConfigs);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getWidgetPageToCreate_WhenNoError_ThenConfiguresLayoutContextWithCommonPageDetails(boolean privateLayout) {
		mockWidgetPageLayoutCreation(privateLayout);

		pageCreationUtil.getWidgetPageToCreate(MOCK_CLASS, mockJSONObject1, mockColumnConfigs);

		verify(mockLayoutContextUtil, times(1)).addCommonPageDetails(MOCK_CLASS, mockJSONObject1, mockLayoutContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getWidgetPageToCreate_WhenNoError_ThenReturnsLayoutContext(boolean privateLayout) {
		mockWidgetPageLayoutCreation(privateLayout);

		LayoutContext result = pageCreationUtil.getWidgetPageToCreate(MOCK_CLASS, mockJSONObject1, mockColumnConfigs);

		assertThat(result, sameInstance(mockLayoutContext));
	}

	@Before
	public void setUp() {
		mockStatic(StringUtil.class);
	}

	private void mockContentPageLayoutCreation(boolean privateLayout) {
		when(mockJSONObject1.getString("pageName")).thenReturn("pageNameValue");
		when(mockJSONObject1.getString("pageFriendlyURL")).thenReturn("pageFriendlyURLValue");
		when(mockJSONObject1.getBoolean("private")).thenReturn(privateLayout);
		when(mockModelCreationFactory.createContentPageLayoutContext("pageNameValue", "pageFriendlyURLValue", privateLayout)).thenReturn(mockLayoutContext);
	}

	private void mockLinkToLayoutPageLayoutCreation(boolean privateLayout) {
		when(mockJSONObject1.getString("pageName")).thenReturn("pageNameValue");
		when(mockJSONObject1.getString("pageFriendlyURL")).thenReturn("pageFriendlyURLValue");
		when(mockJSONObject1.getBoolean("private")).thenReturn(privateLayout);
		when(mockModelCreationFactory.createLayoutContext("pageNameValue", "pageFriendlyURLValue", privateLayout)).thenReturn(mockLayoutContext);
	}

	private void mockPageTemplateCreation() {
		String pageTemplateName = "template";
		String layoutTemplateId = "30_60";

		when(mockJSONObject1.getString("pageTemplateName")).thenReturn(pageTemplateName);
		when(mockJSONObject1.getString("layoutTemplateId")).thenReturn(layoutTemplateId);
		when(mockModelCreationFactory.createPageTemplateLayoutContext(pageTemplateName, layoutTemplateId)).thenReturn(mockLayoutContext);
	}

	private void mockPortletDetails(JSONObject jsonObject, Map<String, String> portletPreferences, PortletContext digitalPlacePortletCreation) {
		when(mockParserUtil.getPortletPreferencesWithPlaceholders(jsonObject, mockPrefsPlaceholders, ConfigurationKeys.DEFAULT)).thenReturn(portletPreferences);
		when(mockParserUtil.getPortletWithPreferences(jsonObject, portletPreferences, ConfigurationKeys.DEFAULT)).thenReturn(digitalPlacePortletCreation);
	}

	private void mockWidgetPageLayoutCreation(boolean privateLayout) {
		when(mockJSONObject1.getString("pageName")).thenReturn("pageNameValue");
		when(mockJSONObject1.getString("pageFriendlyURL")).thenReturn("pageFriendlyURLValue");
		when(mockJSONObject1.getString("layoutTemplateId")).thenReturn("layoutTemplateIdValue");
		when(mockJSONObject1.getBoolean("private")).thenReturn(privateLayout);
		when(mockModelCreationFactory.createWidgetPageLayoutContext("pageNameValue", "pageFriendlyURLValue", privateLayout, "layoutTemplateIdValue")).thenReturn(mockLayoutContext);
	}
}
