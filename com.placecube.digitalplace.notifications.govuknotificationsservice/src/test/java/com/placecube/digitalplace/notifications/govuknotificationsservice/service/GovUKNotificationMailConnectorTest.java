package com.placecube.digitalplace.notifications.govuknotificationsservice.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendMailContext;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import uk.gov.service.notify.NotificationClient;
import uk.gov.service.notify.NotificationClientException;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class GovUKNotificationMailConnectorTest extends PowerMockito {

	private static final long COMPANY_ID = 1l;

	private static final String API_KEY_NAME = "API KEY NAME";

	@InjectMocks
	private GovUKNotificationMailConnector govUKNotificationMailConnector;

	@Mock
	private GovUKNotificationsService mockGovUKNotificationsService;

	@Mock
	private NotificationClient mockNotificationclient;

	@Mock
	private Map<String, Object> mockPersonalisation;

	@Mock
	private SendMailContext mockSendMailContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockGovUKNotificationsService.isEmailEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = govUKNotificationMailConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockGovUKNotificationsService.isEmailEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = govUKNotificationMailConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = ConfigurationException.class)
	public void sendMail_WhenExceptionRetrievingApplicationKey_ThenThrowsConfigurationException() throws Exception {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSendMailContext.getApiKeyName()).thenReturn(API_KEY_NAME);
		when(mockGovUKNotificationsService.getApplicationKey(COMPANY_ID, API_KEY_NAME)).thenThrow(new ConfigurationException());

		govUKNotificationMailConnector.sendMail(mockSendMailContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	public void sendMail_WhenExceptionSendingEmail_ThenThrowsNotificationsException() throws Exception {
		String appKey = "applicationKeyValue";
		String templateId = "templateIdValue";
		String email = "emailValue";
		String reference = "referenceValue";
		String emailReply = "emailReplyValue";
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSendMailContext.getApiKeyName()).thenReturn(API_KEY_NAME);
		when(mockGovUKNotificationsService.getApplicationKey(COMPANY_ID, API_KEY_NAME)).thenReturn(appKey);
		when(mockGovUKNotificationsService.getClient(appKey)).thenReturn(mockNotificationclient);
		when(mockSendMailContext.getTemplateId()).thenReturn(templateId);
		when(mockSendMailContext.getEmailAddress()).thenReturn(email);
		when(mockSendMailContext.getPersonalisation()).thenReturn(mockPersonalisation);
		when(mockSendMailContext.getReference()).thenReturn(reference);
		when(mockSendMailContext.getFromEmailAddress()).thenReturn(emailReply);
		when(mockNotificationclient.sendEmail(templateId, email, mockPersonalisation, reference, emailReply)).thenThrow(new NotificationClientException("msg"));

		govUKNotificationMailConnector.sendMail(mockSendMailContext, mockServiceContext);

	}

	@Test
	public void sendMail_WhenNoError_ThenSendsTheMail() throws Exception {
		String appKey = "applicationKeyValue";
		String templateId = "templateIdValue";
		String email = "emailValue";
		String reference = "referenceValue";
		String emailReply = "emailReplyValue";
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSendMailContext.getApiKeyName()).thenReturn(API_KEY_NAME);
		when(mockGovUKNotificationsService.getApplicationKey(COMPANY_ID, API_KEY_NAME)).thenReturn(appKey);
		when(mockGovUKNotificationsService.getClient(appKey)).thenReturn(mockNotificationclient);
		when(mockSendMailContext.getTemplateId()).thenReturn(templateId);
		when(mockSendMailContext.getEmailAddress()).thenReturn(email);
		when(mockSendMailContext.getPersonalisation()).thenReturn(mockPersonalisation);
		when(mockSendMailContext.getReference()).thenReturn(reference);
		when(mockSendMailContext.getFromEmailAddress()).thenReturn(emailReply);

		govUKNotificationMailConnector.sendMail(mockSendMailContext, mockServiceContext);

		verify(mockNotificationclient, times(1)).sendEmail(templateId, email, mockPersonalisation, reference, emailReply);
	}

}
