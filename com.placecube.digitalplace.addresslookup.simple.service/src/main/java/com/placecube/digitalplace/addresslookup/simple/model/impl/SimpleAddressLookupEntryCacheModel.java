/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;
import com.placecube.digitalplace.addresslookup.simple.service.persistence.SimpleAddressLookupEntryPK;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing SimpleAddressLookupEntry in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class SimpleAddressLookupEntryCacheModel
	implements CacheModel<SimpleAddressLookupEntry>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof SimpleAddressLookupEntryCacheModel)) {
			return false;
		}

		SimpleAddressLookupEntryCacheModel simpleAddressLookupEntryCacheModel =
			(SimpleAddressLookupEntryCacheModel)object;

		if (simpleAddressLookupEntryPK.equals(
				simpleAddressLookupEntryCacheModel.
					simpleAddressLookupEntryPK)) {

			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, simpleAddressLookupEntryPK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", uprn=");
		sb.append(uprn);
		sb.append(", addressLine1=");
		sb.append(addressLine1);
		sb.append(", addressLine2=");
		sb.append(addressLine2);
		sb.append(", addressLine3=");
		sb.append(addressLine3);
		sb.append(", city=");
		sb.append(city);
		sb.append(", postcode=");
		sb.append(postcode);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public SimpleAddressLookupEntry toEntityModel() {
		SimpleAddressLookupEntryImpl simpleAddressLookupEntryImpl =
			new SimpleAddressLookupEntryImpl();

		if (uuid == null) {
			simpleAddressLookupEntryImpl.setUuid("");
		}
		else {
			simpleAddressLookupEntryImpl.setUuid(uuid);
		}

		simpleAddressLookupEntryImpl.setCompanyId(companyId);

		if (uprn == null) {
			simpleAddressLookupEntryImpl.setUprn("");
		}
		else {
			simpleAddressLookupEntryImpl.setUprn(uprn);
		}

		if (addressLine1 == null) {
			simpleAddressLookupEntryImpl.setAddressLine1("");
		}
		else {
			simpleAddressLookupEntryImpl.setAddressLine1(addressLine1);
		}

		if (addressLine2 == null) {
			simpleAddressLookupEntryImpl.setAddressLine2("");
		}
		else {
			simpleAddressLookupEntryImpl.setAddressLine2(addressLine2);
		}

		if (addressLine3 == null) {
			simpleAddressLookupEntryImpl.setAddressLine3("");
		}
		else {
			simpleAddressLookupEntryImpl.setAddressLine3(addressLine3);
		}

		if (city == null) {
			simpleAddressLookupEntryImpl.setCity("");
		}
		else {
			simpleAddressLookupEntryImpl.setCity(city);
		}

		if (postcode == null) {
			simpleAddressLookupEntryImpl.setPostcode("");
		}
		else {
			simpleAddressLookupEntryImpl.setPostcode(postcode);
		}

		simpleAddressLookupEntryImpl.resetOriginalValues();

		return simpleAddressLookupEntryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		companyId = objectInput.readLong();
		uprn = objectInput.readUTF();
		addressLine1 = objectInput.readUTF();
		addressLine2 = objectInput.readUTF();
		addressLine3 = objectInput.readUTF();
		city = objectInput.readUTF();
		postcode = objectInput.readUTF();

		simpleAddressLookupEntryPK = new SimpleAddressLookupEntryPK(
			companyId, uprn);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(companyId);

		if (uprn == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uprn);
		}

		if (addressLine1 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(addressLine1);
		}

		if (addressLine2 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(addressLine2);
		}

		if (addressLine3 == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(addressLine3);
		}

		if (city == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(city);
		}

		if (postcode == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(postcode);
		}
	}

	public String uuid;
	public long companyId;
	public String uprn;
	public String addressLine1;
	public String addressLine2;
	public String addressLine3;
	public String city;
	public String postcode;
	public transient SimpleAddressLookupEntryPK simpleAddressLookupEntryPK;

}