package com.placecube.digitalplace.initialiser.service.util;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.UserActiveException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.placecube.initializer.model.LayoutContext;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest(LocaleUtil.class)
public class LayoutContextUtilTest extends PowerMockito {

	private static final Locale LOCALE = Locale.FRENCH;

	private static final Class MOCK_CLASS = UserActiveException.class;

	@InjectMocks
	private LayoutContextUtil layoutContextUtil;

	@Mock
	private Map<String, Serializable> mockExpandoValues;

	@Mock
	private JSONArray mockJsonArray;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private JSONObject mockJSONObjectLocalisedValue;

	@Mock
	private LayoutContext mockLayoutContext;

	@Mock
	private ParserUtil mockParserUtil;

	@Mock
	private Map<String, String[]> mockPermissionsToAdd;

	@Mock
	private Map<String, String[]> mockPermissionsToRemove;

	@Before
	public void activeSetup() {

		mockStatic(LocaleUtil.class);

	}

	@Test
	public void addCommonPageDetails_WhenInvalidLocalisedPageName_ThenDoesNotConfigureLocalisedNames() {
		when(mockJSONObject.getJSONArray("localisedPageName")).thenReturn(mockJsonArray);
		when(mockJsonArray.length()).thenReturn(1);
		when(mockJsonArray.getJSONObject(0)).thenReturn(mockJSONObjectLocalisedValue);
		when(mockJSONObjectLocalisedValue.getString("languageId")).thenReturn("es_ES");
		when(LocaleUtil.fromLanguageId("es_ES", true, false)).thenReturn(null);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, never()).addLocalisedName(any(Locale.class), anyString());
	}

	@Test
	public void addCommonPageDetails_WhenInvalidLocalisedPageTitle_ThenDoesNotConfigureLocalisedTitles() {
		when(mockJSONObject.getJSONArray("localisedPageTitle")).thenReturn(mockJsonArray);
		when(mockJsonArray.length()).thenReturn(1);
		when(mockJsonArray.getJSONObject(0)).thenReturn(mockJSONObjectLocalisedValue);
		when(mockJSONObjectLocalisedValue.getString("languageId")).thenReturn("es_ES");
		when(LocaleUtil.fromLanguageId("es_ES", true, false)).thenReturn(null);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, never()).addLocalisedTitle(any(Locale.class), anyString());
	}

	@Test
	public void addCommonPageDetails_WhenLocalisedPageNameSpecified_ThenConfiguresLocalisedNames() {
		when(mockJSONObject.getJSONArray("localisedPageName")).thenReturn(mockJsonArray);
		when(mockJsonArray.length()).thenReturn(1);
		when(mockJsonArray.getJSONObject(0)).thenReturn(mockJSONObjectLocalisedValue);
		when(mockJSONObjectLocalisedValue.getString("languageId")).thenReturn("es_ES");
		when(mockJSONObjectLocalisedValue.getString("value")).thenReturn("localisedValue");
		when(LocaleUtil.fromLanguageId("es_ES", true, false)).thenReturn(LOCALE);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, times(1)).addLocalisedName(Locale.FRENCH, "localisedValue");
	}

	@Test
	public void addCommonPageDetails_WhenLocalisedPageTitleSpecified_ThenConfiguresLocalisedTitles() {
		when(mockJSONObject.getJSONArray("localisedPageTitle")).thenReturn(mockJsonArray);
		when(mockJsonArray.length()).thenReturn(1);
		when(mockJsonArray.getJSONObject(0)).thenReturn(mockJSONObjectLocalisedValue);
		when(mockJSONObjectLocalisedValue.getString("languageId")).thenReturn("es_ES");
		when(mockJSONObjectLocalisedValue.getString("value")).thenReturn("localisedTitleValue");
		when(LocaleUtil.fromLanguageId("es_ES", true, false)).thenReturn(LOCALE);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, times(1)).addLocalisedTitle(LOCALE, "localisedTitleValue");
	}

	@Test
	public void addCommonPageDetails_WhenNoError_ThenConfiguresLayoutContextWithExpandoValues() {
		when(mockParserUtil.getExpandoValues(mockJSONObject)).thenReturn(mockExpandoValues);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, times(1)).setExpandoValues(mockExpandoValues);
	}

	@Test
	@Parameters({ "true", "false" })
	public void addCommonPageDetails_WhenNoError_ThenConfiguresLayoutContextWithHiddenValue(boolean hiddenLayout) {
		when(mockJSONObject.getBoolean("hidden", false)).thenReturn(hiddenLayout);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, times(1)).setHidden(hiddenLayout);
	}

	@Test
	public void addCommonPageDetails_WhenNoError_ThenConfiguresLayoutContextWithPageTitle() {
		String title = "titleValue";
		when(mockJSONObject.getString("pageTitle")).thenReturn(title);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, times(1)).setTitle(title);

	}

	@Test
	public void addCommonPageDetails_WhenNoError_ThenConfiguresLayoutContextWithParentFriendlyURL() {
		String parentFriendlyURL = "parentFriendlyURLValue";
		when(mockJSONObject.getString("parentFriendlyURL")).thenReturn(parentFriendlyURL);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, times(1)).setParentLayoutFriendlyURL(parentFriendlyURL);
	}

	@Test
	public void addCommonPageDetails_WhenNoLocalisedPageName_ThenDoesNotConfigureLocalisedNames() {
		when(mockJSONObject.getJSONArray("localisedPageName")).thenReturn(null);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, never()).addLocalisedName(any(Locale.class), anyString());
	}

	@Test
	public void addCommonPageDetails_WhenNoLocalisedPageTitle_ThenDoesNotConfigureLocalisedTitles() {
		when(mockJSONObject.getJSONArray("localisedPageTitle")).thenReturn(null);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, never()).addLocalisedTitle(any(Locale.class), anyString());
	}

	@Test
	public void addCommonPageDetails_WhenPageIconPathNotSpecified_ThenDoesNotConfigureTheLayoutContextWithPageIcon() {
		when(mockJSONObject.getString("pageIconPath")).thenReturn(StringPool.BLANK);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, never()).setPageIcon(any(InputStream.class));
	}

	@Test
	public void addCommonPageDetails_WhenPageIconPathSpecified_ThenConfiguresLayoutContextWithPageIcon() {
		String pageIconPath = "pageIconPathVal";
		when(mockJSONObject.getString("pageIconPath")).thenReturn(pageIconPath);

		layoutContextUtil.addCommonPageDetails(MOCK_CLASS, mockJSONObject, mockLayoutContext);

		verify(mockLayoutContext, times(1)).setPageIcon(MOCK_CLASS.getClassLoader().getResourceAsStream(pageIconPath));
	}

}
