/**
 * ScpSimpleQueryResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.simple;

public class ScpSimpleQueryResponse  extends com.capita_software_services.www.scp.base.ScpQueryResponse  implements java.io.Serializable {
    private com.capita_software_services.www.scp.simple.SimplePaymentResult paymentResult;

    public ScpSimpleQueryResponse() {
    }

    public ScpSimpleQueryResponse(
           org.apache.axis.types.Token requestId,
           java.lang.String scpReference,
           com.capita_software_services.www.scp.base.TransactionState transactionState,
           com.capita_software_services.www.scp.base.StoreCardResult storeCardResult,
           com.capita_software_services.www.scp.base.EmailResult emailResult,
           com.capita_software_services.www.scp.simple.SimplePaymentResult paymentResult) {
        super(
            requestId,
            scpReference,
            transactionState,
            storeCardResult,
            emailResult);
        this.paymentResult = paymentResult;
    }


    /**
     * Gets the paymentResult value for this ScpSimpleQueryResponse.
     *
     * @return paymentResult
     */
    public com.capita_software_services.www.scp.simple.SimplePaymentResult getPaymentResult() {
        return paymentResult;
    }


    /**
     * Sets the paymentResult value for this ScpSimpleQueryResponse.
     *
     * @param paymentResult
     */
    public void setPaymentResult(com.capita_software_services.www.scp.simple.SimplePaymentResult paymentResult) {
        this.paymentResult = paymentResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScpSimpleQueryResponse)) return false;
        ScpSimpleQueryResponse other = (ScpSimpleQueryResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.paymentResult==null && other.getPaymentResult()==null) ||
             (this.paymentResult!=null &&
              this.paymentResult.equals(other.getPaymentResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPaymentResult() != null) {
            _hashCode += getPaymentResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScpSimpleQueryResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "scpSimpleQueryResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "paymentResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "simplePaymentResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
