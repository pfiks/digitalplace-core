package com.placecube.digitalplace.calendar.web.portlet.configuration;

import com.placecube.digitalplace.calendar.web.constants.DigitalPlaceCalendarConfigurationConstants;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.calendar.web.portlet.configuration.DigitalPlaceCalendarInstanceConfiguration")
public interface DigitalPlaceCalendarInstanceConfiguration {

	@Meta.AD(required = false, deflt = DigitalPlaceCalendarConfigurationConstants.SCOPE_GROUP, optionLabels = { "group", "all" }, optionValues = {
			DigitalPlaceCalendarConfigurationConstants.SCOPE_GROUP, DigitalPlaceCalendarConfigurationConstants.SCOPE_ALL })
	String scope();

}
