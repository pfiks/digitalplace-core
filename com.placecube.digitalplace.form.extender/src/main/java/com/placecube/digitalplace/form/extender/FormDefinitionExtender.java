package com.placecube.digitalplace.form.extender;

import java.util.Dictionary;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.util.tracker.BundleTrackerCustomizer;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.form.extender.internal.FormDefinitionExtenderService;
import com.placecube.digitalplace.form.model.FormDefinition;

@Component(immediate = true)
public class FormDefinitionExtender implements BundleTrackerCustomizer<FormDefinition> {

	private static final Log LOG = LogFactoryUtil.getLog(FormDefinitionExtender.class);

	private BundleContext bundleContext;

	@Reference
	private FormDefinitionExtenderService formDefinitionExtenderService;

	@Override
	public FormDefinition addingBundle(Bundle bundle, BundleEvent bundleEvent) {

		Dictionary<String, String> headers = bundle.getHeaders();

		if (!formDefinitionExtenderService.isValidFormDefinition(headers) || !formDefinitionExtenderService.hasValidHeaders(headers)) {
			return null;
		}

		FormDefinition formDefinition = formDefinitionExtenderService.createFormDefinition(headers, bundle);

		formDefinitionExtenderService.register(formDefinition, bundleContext);

		LOG.debug("Form Definition registered for " + bundle.getSymbolicName());

		return formDefinition;

	}

	@Override
	public void modifiedBundle(Bundle bundle, BundleEvent bundleEvent, FormDefinition formDefinition) {
		return;
	}

	@Override
	public void removedBundle(Bundle bundle, BundleEvent bundleEvent, FormDefinition formDefinition) {

		formDefinitionExtenderService.unregister(formDefinition);

	}

	@Activate
	protected void activate(BundleContext bundleContext) {
		this.bundleContext = bundleContext;

		formDefinitionExtenderService.openBundleTracker(bundleContext, this);
	}

	@Deactivate
	protected void deactivate() {

		formDefinitionExtenderService.closeBundleTracker();

	}

}