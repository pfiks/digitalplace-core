package com.placecube.digitalplace.search.internal.contants;

public final class SharedSearchContextAttributeKeys {

	public static final String SEARCH_BOOLEAN_CLAUSE = "dpSearchBooleanClauses";

	private SharedSearchContextAttributeKeys() {

	}
}
