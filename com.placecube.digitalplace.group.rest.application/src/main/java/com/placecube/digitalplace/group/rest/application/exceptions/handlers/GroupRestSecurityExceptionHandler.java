package com.placecube.digitalplace.group.rest.application.exceptions.handlers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;

@Provider
public class GroupRestSecurityExceptionHandler implements ExceptionMapper<GroupRestSecurityException> {

	public GroupRestSecurityExceptionHandler() {
	}

	@Override
	public Response toResponse(GroupRestSecurityException exception) {
		return Response.status(exception.getResponse().getStatus()).entity(exception.getMessage()).build();
	}
}
