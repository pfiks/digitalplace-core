/**
 * Scp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.simple;

public interface Scp extends java.rmi.Remote {
    public com.capita_software_services.www.scp.simple.ScpSimpleQueryResponse scpSimpleQuery(com.capita_software_services.www.scp.base.ScpQueryRequest scpSimpleQueryRequest) throws java.rmi.RemoteException;
    public com.capita_software_services.www.scp.base.ScpInvokeResponse scpSimpleInvoke(com.capita_software_services.www.scp.simple.ScpSimpleInvokeRequest scpSimpleInvokeRequest) throws java.rmi.RemoteException;
    public com.capita_software_services.www.scp.base.ScpVersionResponse scpVersion(com.capita_software_services.www.scp.base.ScpVersionRequest scpVersionRequest) throws java.rmi.RemoteException;
}
