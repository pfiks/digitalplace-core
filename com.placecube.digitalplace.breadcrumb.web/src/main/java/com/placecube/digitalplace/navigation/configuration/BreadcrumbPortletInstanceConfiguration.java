package com.placecube.digitalplace.navigation.configuration;

import com.placecube.digitalplace.navigation.constants.ConfigurationConstants;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = ConfigurationConstants.BREADCRUMB_PID)
public interface BreadcrumbPortletInstanceConfiguration {

	@Meta.AD(required = false, deflt = ConfigurationConstants.DEFAULT_CATEGORY_NAV_FILTER_VOCABULARY_ID)
	long categoryNavFilterVocabularyId();

	@Meta.AD(required = false, deflt = ConfigurationConstants.DEFAULT_CATEGORY_NAV_ROOT_ENTRY_NAME)
	String categoryNavRootEntryName();

	@Meta.AD(required = false, deflt = ConfigurationConstants.DEFAULT_CATEGORY_NAV_URL_CATEGORY_PROPERTY_NAME)
	String categoryNavUrlCategoryPropertyName();

	@Meta.AD(required = false, deflt = ConfigurationConstants.DEFAULT_CATEGORY_NAV_BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME)
	String categoryNavBuildCategoriesHierarchyUrlPropertyName();

	@Meta.AD(required = false, deflt = "true")
	boolean categoryNavShowCurrentLayout();

	@Meta.AD(required = false, deflt = "false")
	boolean useLayoutUrl();

	@Meta.AD(required = false, deflt = "false")
	boolean forceRootForLayoutBreadcrumbs();

	@Meta.AD(required = false, deflt = "false")
	boolean displayOnlyOneAssetCategoryBreadcrumb();
}
