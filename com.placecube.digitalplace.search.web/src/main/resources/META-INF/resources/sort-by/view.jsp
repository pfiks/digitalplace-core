<%@ include file="../init.jsp"%>


<c:choose>

	<c:when test="${invalidConfiguration or empty sortByOptions}">
		<%@ include file="/common/invalid-configuration.jspf" %>
	</c:when>
	
	<c:when test="${isEditLayoutMode && !displayPortlet}">
		<%@ include file="/common/invisible-portlet-edit-mode.jspf" %>
	</c:when>

	<c:when test="${displayPortlet}">
		<c:set var="sortByLabelPrefix"><liferay-ui:message key="sort-by"/></c:set>
			
		<div class="row">
			<div class="col-md-12">

				<c:forEach items="${sortByOptions}" var="sortByOption">

					<portlet:actionURL name="<%=MVCCommandKeys.SEARCH %>" var="updateSortByURL">
						<portlet:param name="<%= SearchParameters.SELECTED_SORT_BY %>" value="${sortByOption.getId()}"/>
						<portlet:param name="redirectURL" value="${redirectURL}" />
					</portlet:actionURL>
					
					<c:set var="sortByLabel" value="${sortByOption.getLabel(locale, renderRequest.preferences)}" />
					<a href="${updateSortByURL}" class="btn sort-by ${sortByOption.getId().equals(selectedSortBy) ? 'sort-by-active' : ''}" title="${sortByLabelPrefix} ${sortByLabel}">
						${sortByLabel} 
					</a>

				</c:forEach>

				<c:if test="${sortDirectionChangeable}">
					<portlet:actionURL name="<%=MVCCommandKeys.SEARCH %>" var="updateSortByDirectionURL">
						<portlet:param name="redirectURL" value="${redirectURL}" />
						<portlet:param name="currentSelectedSortByDirection" value="${currentSelectedSortByDirection}"/>
						<portlet:param name="<%= SearchParameters.SELECTED_SORT_BY %>" value="${selectedSortBy}" />
						<portlet:param name="cmd" value="updateSortByDirection"/>
					</portlet:actionURL>
					
					<a href="${updateSortByDirectionURL}" class="pl-4 btn sort-by ${sortByOption.getId().equals(selectedSortBy) ? 'sort-by-active' : ''}" title="reverse-sort-direction">
						<liferay-ui:icon markupView="lexicon" icon="order-arrow" 
								cssClass='${"invertedDirection".equalsIgnoreCase(currentSelectedSortByDirection) ? "order-arrow-up-active" : "order-arrow-down-active"}' 
								message="reverse-sort-direction"/> 
					</a>
				</c:if>
			</div>
			
		</div>
	</c:when>
</c:choose>