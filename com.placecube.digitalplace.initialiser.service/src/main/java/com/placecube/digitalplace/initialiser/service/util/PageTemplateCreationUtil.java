package com.placecube.digitalplace.initialiser.service.util;

import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.initialiser.service.constants.ConfigurationKeys;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.LayoutPageTemplateEntryInitializer;

@Component(immediate = true, service = PageTemplateCreationUtil.class)
public class PageTemplateCreationUtil {

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private LayoutPageTemplateEntryInitializer layoutPageTemplateEntryInitializer;

	@Reference
	private PageCreationUtil pageCreationUtil;

	@Reference
	private Portal portal;

	public LayoutPageTemplateEntry createContentPageTemplate(LayoutContext layoutContext, JSONObject pageTemplateJSONObject, Map<String, String> preferencesPlaceholderValues,
			ServiceContext serviceContext) throws PortalException {

		configureLayoutContextWithContentDefinition(layoutContext, pageTemplateJSONObject, preferencesPlaceholderValues, serviceContext);

		LayoutPageTemplateCollection layoutPageTemplateCollection = pageCreationUtil.getLayoutPageTemplateCollection(pageTemplateJSONObject, serviceContext);

		return layoutPageTemplateEntryInitializer.createContentPageTemplate(layoutPageTemplateCollection, layoutContext, serviceContext);
	}

	public LayoutPageTemplateEntry createDisplayPageTemplate(LayoutContext layoutContext, JSONObject pageTemplateJSONObject, Map<String, String> preferencesPlaceholderValues,
			ServiceContext serviceContext) throws PortalException {

		long classNameId = portal.getClassNameId(JournalArticle.class);
		long classTypeId = ddmStructureLocalService
				.getStructure(GetterUtil.getBoolean(pageTemplateJSONObject.getBoolean("globalStructure"), false) ? groupLocalService.getCompanyGroup(CompanyThreadLocal.getCompanyId()).getGroupId()
						: serviceContext.getScopeGroupId(), classNameId, pageTemplateJSONObject.getString("mappingWebContentStructure"))
				.getStructureId();

		configureLayoutContextWithContentDefinition(layoutContext, pageTemplateJSONObject, preferencesPlaceholderValues, serviceContext);

		return layoutPageTemplateEntryInitializer.createDisplayPageTemplate(layoutContext, classNameId, classTypeId, serviceContext);
	}

	public LayoutPageTemplateEntry createWidgetPageTemplate(LayoutContext layoutContext, JSONObject pageTemplateJSONObject, Map<String, String> preferencesPlaceholderValues,
			ServiceContext serviceContext) throws PortalException {

		Map<Integer, Set<PortletContext>> columnsConfigs = pageCreationUtil.getColumns(pageTemplateJSONObject.getJSONArray("columns"), preferencesPlaceholderValues, ConfigurationKeys.DEFAULT);
		layoutContext.setColumnPortletContexts(columnsConfigs);

		LayoutPageTemplateCollection layoutPageTemplateCollection = pageCreationUtil.getLayoutPageTemplateCollection(pageTemplateJSONObject, serviceContext);

		return layoutPageTemplateEntryInitializer.createWidgetPageTemplate(layoutPageTemplateCollection, layoutContext, serviceContext);
	}

	private void configureLayoutContextWithContentDefinition(LayoutContext layoutContext, JSONObject pageTemplateJSONObject, Map<String, String> preferencesPlaceholderValues,
			ServiceContext serviceContext) {
		Set<FragmentContext> fragments = pageCreationUtil.getFragments(pageTemplateJSONObject.getJSONArray("fragments"));
		Set<PortletContext> portlets = pageCreationUtil.getPortlets(serviceContext.getScopeGroupId(), pageTemplateJSONObject.getJSONArray("portlets"), null, preferencesPlaceholderValues,
				ConfigurationKeys.CONTENT_OR_DISPLAY_PAGE_TEMPLATE);
		String structureDefinition = pageTemplateJSONObject.getJSONObject("contentStructureDefinition").toJSONString();
		layoutContext.setPortletContexts(portlets);
		layoutContext.setFragmentContexts(fragments);
		layoutContext.setStructureContent(structureDefinition);
	}

}
