/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.group.rest.exception.NoSuchGroupWebhookException;
import com.placecube.digitalplace.group.rest.model.GroupWebhook;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the group webhook service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GroupWebhookUtil
 * @generated
 */
@ProviderType
public interface GroupWebhookPersistence extends BasePersistence<GroupWebhook> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link GroupWebhookUtil} to access the group webhook persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the group webhooks where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId(long groupId);

	/**
	 * Returns a range of all the group webhooks where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public GroupWebhook findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
				orderByComparator)
		throws NoSuchGroupWebhookException;

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public GroupWebhook fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public GroupWebhook findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
				orderByComparator)
		throws NoSuchGroupWebhookException;

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public GroupWebhook fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns the group webhooks before and after the current group webhook in the ordered set where groupId = &#63;.
	 *
	 * @param webhookId the primary key of the current group webhook
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public GroupWebhook[] findByGroupId_PrevAndNext(
			long webhookId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
				orderByComparator)
		throws NoSuchGroupWebhookException;

	/**
	 * Removes all the group webhooks where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of group webhooks where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching group webhooks
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @return the matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled);

	/**
	 * Returns a range of all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled, int start, int end);

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId_Enabled(
		long groupId, boolean enabled, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public GroupWebhook findByGroupId_Enabled_First(
			long groupId, boolean enabled,
			com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
				orderByComparator)
		throws NoSuchGroupWebhookException;

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public GroupWebhook fetchByGroupId_Enabled_First(
		long groupId, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public GroupWebhook findByGroupId_Enabled_Last(
			long groupId, boolean enabled,
			com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
				orderByComparator)
		throws NoSuchGroupWebhookException;

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public GroupWebhook fetchByGroupId_Enabled_Last(
		long groupId, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns the group webhooks before and after the current group webhook in the ordered set where groupId = &#63; and enabled = &#63;.
	 *
	 * @param webhookId the primary key of the current group webhook
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public GroupWebhook[] findByGroupId_Enabled_PrevAndNext(
			long webhookId, long groupId, boolean enabled,
			com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
				orderByComparator)
		throws NoSuchGroupWebhookException;

	/**
	 * Removes all the group webhooks where groupId = &#63; and enabled = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 */
	public void removeByGroupId_Enabled(long groupId, boolean enabled);

	/**
	 * Returns the number of group webhooks where groupId = &#63; and enabled = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @return the number of matching group webhooks
	 */
	public int countByGroupId_Enabled(long groupId, boolean enabled);

	/**
	 * Returns all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @return the matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK);

	/**
	 * Returns a range of all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK,
		int start, int end);

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns an ordered range of all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching group webhooks
	 */
	public java.util.List<GroupWebhook> findByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public GroupWebhook findByGroupId_Enabled_ClassName_ClassPK_First(
			long groupId, boolean enabled, String className, long classPK,
			com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
				orderByComparator)
		throws NoSuchGroupWebhookException;

	/**
	 * Returns the first group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public GroupWebhook fetchByGroupId_Enabled_ClassName_ClassPK_First(
		long groupId, boolean enabled, String className, long classPK,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook
	 * @throws NoSuchGroupWebhookException if a matching group webhook could not be found
	 */
	public GroupWebhook findByGroupId_Enabled_ClassName_ClassPK_Last(
			long groupId, boolean enabled, String className, long classPK,
			com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
				orderByComparator)
		throws NoSuchGroupWebhookException;

	/**
	 * Returns the last group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching group webhook, or <code>null</code> if a matching group webhook could not be found
	 */
	public GroupWebhook fetchByGroupId_Enabled_ClassName_ClassPK_Last(
		long groupId, boolean enabled, String className, long classPK,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns the group webhooks before and after the current group webhook in the ordered set where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param webhookId the primary key of the current group webhook
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public GroupWebhook[] findByGroupId_Enabled_ClassName_ClassPK_PrevAndNext(
			long webhookId, long groupId, boolean enabled, String className,
			long classPK,
			com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
				orderByComparator)
		throws NoSuchGroupWebhookException;

	/**
	 * Removes all the group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 */
	public void removeByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK);

	/**
	 * Returns the number of group webhooks where groupId = &#63; and enabled = &#63; and className = &#63; and classPK = &#63;.
	 *
	 * @param groupId the group ID
	 * @param enabled the enabled
	 * @param className the class name
	 * @param classPK the class pk
	 * @return the number of matching group webhooks
	 */
	public int countByGroupId_Enabled_ClassName_ClassPK(
		long groupId, boolean enabled, String className, long classPK);

	/**
	 * Caches the group webhook in the entity cache if it is enabled.
	 *
	 * @param groupWebhook the group webhook
	 */
	public void cacheResult(GroupWebhook groupWebhook);

	/**
	 * Caches the group webhooks in the entity cache if it is enabled.
	 *
	 * @param groupWebhooks the group webhooks
	 */
	public void cacheResult(java.util.List<GroupWebhook> groupWebhooks);

	/**
	 * Creates a new group webhook with the primary key. Does not add the group webhook to the database.
	 *
	 * @param webhookId the primary key for the new group webhook
	 * @return the new group webhook
	 */
	public GroupWebhook create(long webhookId);

	/**
	 * Removes the group webhook with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook that was removed
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public GroupWebhook remove(long webhookId)
		throws NoSuchGroupWebhookException;

	public GroupWebhook updateImpl(GroupWebhook groupWebhook);

	/**
	 * Returns the group webhook with the primary key or throws a <code>NoSuchGroupWebhookException</code> if it could not be found.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook
	 * @throws NoSuchGroupWebhookException if a group webhook with the primary key could not be found
	 */
	public GroupWebhook findByPrimaryKey(long webhookId)
		throws NoSuchGroupWebhookException;

	/**
	 * Returns the group webhook with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param webhookId the primary key of the group webhook
	 * @return the group webhook, or <code>null</code> if a group webhook with the primary key could not be found
	 */
	public GroupWebhook fetchByPrimaryKey(long webhookId);

	/**
	 * Returns all the group webhooks.
	 *
	 * @return the group webhooks
	 */
	public java.util.List<GroupWebhook> findAll();

	/**
	 * Returns a range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @return the range of group webhooks
	 */
	public java.util.List<GroupWebhook> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of group webhooks
	 */
	public java.util.List<GroupWebhook> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator);

	/**
	 * Returns an ordered range of all the group webhooks.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GroupWebhookModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group webhooks
	 * @param end the upper bound of the range of group webhooks (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of group webhooks
	 */
	public java.util.List<GroupWebhook> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GroupWebhook>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the group webhooks from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of group webhooks.
	 *
	 * @return the number of group webhooks
	 */
	public int countAll();

}