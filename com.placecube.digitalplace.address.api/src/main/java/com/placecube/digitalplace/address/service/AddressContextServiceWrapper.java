/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.address.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AddressContextService}.
 *
 * @author Brian Wing Shun Chan
 * @see AddressContextService
 * @generated
 */
public class AddressContextServiceWrapper
	implements AddressContextService, ServiceWrapper<AddressContextService> {

	public AddressContextServiceWrapper() {
		this(null);
	}

	public AddressContextServiceWrapper(
		AddressContextService addressContextService) {

		_addressContextService = addressContextService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _addressContextService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.Set
		<com.placecube.digitalplace.address.model.AddressContext>
				searchAddressByPostcode(
					long companyId, String postcode,
					boolean fallbackToNationalLookup)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _addressContextService.searchAddressByPostcode(
			companyId, postcode, fallbackToNationalLookup);
	}

	@Override
	public AddressContextService getWrappedService() {
		return _addressContextService;
	}

	@Override
	public void setWrappedService(AddressContextService addressContextService) {
		_addressContextService = addressContextService;
	}

	private AddressContextService _addressContextService;

}