package com.placecube.digitalplace.oauth.dataprovider.settings;

import com.liferay.dynamic.data.mapping.data.provider.settings.DDMDataProviderSettingsProvider;

import org.osgi.service.component.annotations.Component;

@Component(property = "ddm.data.provider.type=restOAuth", service = DDMDataProviderSettingsProvider.class)
public class DDMRESTOAuth2DataProviderSettingsProvider implements DDMDataProviderSettingsProvider {

	@Override
	public Class<?> getSettings() {
		return DDMRESTOAuth2DataProviderSettings.class;
	}

}