package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorPortletKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorRequestKeys;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;

import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(property = { "javax.portlet.name=" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION, "mvc.command.name=" + TwoFactorMVCCommandKeys.VERIFY }, service = MVCRenderCommand.class)
public class TwoFactorAuthenticationVerifyMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private TwoFactorRequestUtils twoFactorRequestUtils;

	@Reference
	private TwoFactorService twoFactorService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {
		Optional<User> twoFactorUser = twoFactorService.getTwoFactorUser(renderRequest, twoFactorRequestUtils.getOriginalServletRequest(renderRequest));

		if (!twoFactorUser.isPresent()) {
			SessionErrors.add(renderRequest, "sessionExpired");

			return "/two-factor-authentication/error.jsp";

		} else {

			long companyId = twoFactorRequestUtils.getCompanyId(renderRequest);
			Optional<AuthenticationTwoFactorCompanyConfiguration> configuration = twoFactorService.getEnabledConfiguration(companyId);

			if (configuration.isPresent()) {
				renderRequest.setAttribute(TwoFactorRequestKeys.RESET_BUTTON_ENABLED, configuration.get().verificationPageResetButtonEnabled());
			}

			renderRequest.setAttribute("redirect", ParamUtil.getString(renderRequest, "redirect"));

			return "/two-factor-authentication/verify.jsp";
		}
	}

}
