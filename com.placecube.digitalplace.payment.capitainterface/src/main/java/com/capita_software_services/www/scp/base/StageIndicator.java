/**
 * StageIndicator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class StageIndicator  implements java.io.Serializable {
    private byte firstPortalStage;

    private byte totalStages;

    public StageIndicator() {
    }

    public StageIndicator(
           byte firstPortalStage,
           byte totalStages) {
           this.firstPortalStage = firstPortalStage;
           this.totalStages = totalStages;
    }


    /**
     * Gets the firstPortalStage value for this StageIndicator.
     *
     * @return firstPortalStage
     */
    public byte getFirstPortalStage() {
        return firstPortalStage;
    }


    /**
     * Sets the firstPortalStage value for this StageIndicator.
     *
     * @param firstPortalStage
     */
    public void setFirstPortalStage(byte firstPortalStage) {
        this.firstPortalStage = firstPortalStage;
    }


    /**
     * Gets the totalStages value for this StageIndicator.
     *
     * @return totalStages
     */
    public byte getTotalStages() {
        return totalStages;
    }


    /**
     * Sets the totalStages value for this StageIndicator.
     *
     * @param totalStages
     */
    public void setTotalStages(byte totalStages) {
        this.totalStages = totalStages;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StageIndicator)) return false;
        StageIndicator other = (StageIndicator) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            this.firstPortalStage == other.getFirstPortalStage() &&
            this.totalStages == other.getTotalStages();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getFirstPortalStage();
        _hashCode += getTotalStages();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StageIndicator.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "stageIndicator"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstPortalStage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "firstPortalStage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalStages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "totalStages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "byte"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
