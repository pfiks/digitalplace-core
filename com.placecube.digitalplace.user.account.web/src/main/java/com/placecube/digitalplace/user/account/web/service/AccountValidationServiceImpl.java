package com.placecube.digitalplace.user.account.web.service;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.UserPasswordException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.PasswordPolicy;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.PasswordPolicyLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.security.pwd.PwdToolkitUtil;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;
import com.placecube.digitalplace.user.account.constants.AccountValidatedField;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.UserAccountFieldRegistry;
import com.placecube.digitalplace.user.account.fields.model.AccountCreationStep;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountValidationService;
import com.placecube.digitalplace.user.account.service.CaptchaValidationService;
import com.placecube.digitalplace.user.account.web.constants.AccountConstants;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, service = AccountValidationService.class)
public class AccountValidationServiceImpl implements AccountValidationService {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private CaptchaValidationService captchaValidationService;

	@Reference
	private PasswordPolicyLocalService passwordPolicyLocalService;

	@Reference
	private UserAccountFieldRegistry userAccountFieldRegistry;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void validateAdditionalDetails(AccountContext accountContext, PortletRequest portletRequest, CreateAccountConfiguration addAccountConfiguration,
			UserProfileFieldsConfiguration userProfileConfiguration) throws UserAccountException {

		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		Locale locale = themeDisplay.getLocale();

		if (userProfileConfiguration.jobTitleEnabled()) {
			if (userProfileConfiguration.jobTitleRequired()) {
				validateMandatoryField(accountContext, accountContext.getJobTitle(), AccountValidatedField.JOB_TITLE);
			}
			validateMaxLength(accountContext, accountContext.getJobTitle(), AccountValidatedField.JOB_TITLE, locale);
		}

		if (userProfileConfiguration.homePhoneNumberEnabled()) {
			if (userProfileConfiguration.homePhoneNumberRequired()) {
				validateMandatoryField(accountContext, accountContext.getHomePhoneNumber(), AccountValidatedField.HOME_PHONE);
			}
			validateMaxLength(accountContext, accountContext.getHomePhoneNumber(), AccountValidatedField.HOME_PHONE, locale);
		}

		if (userProfileConfiguration.mobilePhoneNumberEnabled()) {
			if (userProfileConfiguration.mobilePhoneNumberRequired()) {
				validateMandatoryField(accountContext, accountContext.getMobilePhoneNumber(), AccountValidatedField.MOBILE_PHONE);
			}
			validateMaxLength(accountContext, accountContext.getMobilePhoneNumber(), AccountValidatedField.MOBILE_PHONE, locale);
		}

		if (userProfileConfiguration.businessPhoneNumberEnabled()) {
			if (userProfileConfiguration.businessPhoneNumberRequired()) {
				validateMandatoryField(accountContext, accountContext.getBusinessPhoneNumber(), AccountValidatedField.BUSINESS_PHONE);
			}
			validateMaxLength(accountContext, accountContext.getBusinessPhoneNumber(), AccountValidatedField.BUSINESS_PHONE, locale);
		}

		boolean isEmailAddressFieldOnPage = validateEmailIsOnCurrentPage(addAccountConfiguration, AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS);

		validateEmail(accountContext, isEmailAddressFieldOnPage, addAccountConfiguration, locale);

		validateCreateUserAccountFields(accountContext, userAccountFieldRegistry.getUserAccountFieldsByStep(AccountCreationStep.ADDITIONAL_DETAILS, accountContext.getCompanyId()), portletRequest,
				isEmailAddressFieldOnPage);

		accountConfigurationService.getConfiguredExpandoFields(accountContext, userProfileConfiguration).forEach(expandoField -> {
			if (expandoField.isRequired()) {
				validateExpandoMandatoryField(accountContext, accountContext.getExpandoValue(expandoField.getExpandoFieldKey()), "expando--" + expandoField.getExpandoFieldKey(),
						"this-field-is-required");
			}
		});

	}

	@Override
	public void validateCaptcha(AccountContext accountContext, PortletRequest portletRequest, CreateAccountConfiguration addAccountConfiguration, String currentPage) throws UserAccountException {
		if (validateEmailIsOnCurrentPage(addAccountConfiguration, currentPage) && accountContextService.isShowCaptchaOnEmailStep(accountContext, portletRequest)) {
			captchaValidationService.validateCaptcha(accountContext, AccountValidatedField.CAPTCHA, portletRequest);
		}
	}

	@Override
	public void validateCreateUserAccountFields(AccountContext accountContext, List<UserAccountField> createUserAccountFields, PortletRequest portletRequest, boolean isEmailAddressFieldOnPage) {
		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		Locale locale = themeDisplay.getLocale();

		createUserAccountFields.forEach(createUserAccountField -> {
			String fieldName = createUserAccountField.getExpandoFieldName();
			String selectedValue = GetterUtil.getString(accountContext.getExpandoValue(fieldName));

			if (createUserAccountField.isRequired(accountContext.getCompanyId())) {
				validateExpandoMandatoryField(accountContext, selectedValue, fieldName, createUserAccountField.getRequiredMessage(locale));
			}

			if (!"email".equalsIgnoreCase(selectedValue) || "email".equalsIgnoreCase(selectedValue) && isEmailAddressFieldOnPage || Validator.isNull(accountContext.getEmailAddress())) {
				createUserAccountField.getDependentRequiredFieldNamesAndMessage(selectedValue, locale).forEach((k, v) -> {
					AccountValidatedField validatedField = AccountValidatedField.getFromFieldName(k);
					validateExpandoMandatoryField(accountContext, ParamUtil.getString(portletRequest, validatedField.getFieldName()), validatedField.getFieldName(), v);
				});
			}
		});

	}

	@Override
	public void validateDate(AccountContext accountContext, AccountValidatedField validatedField, int year, int month, int day, boolean required) {
		if (required && (year == 0 || month == 0 || day == 0)) {
			accountContext.addFieldError(validatedField.getFieldName(), validatedField.getMandatoryErrorKey());
			return;
		}
		if (!required && year == 0 && month == 0 && day == 0) {
			return;
		} else if (!required && year == 0) {
			accountContext.addFieldError(validatedField.getFieldName(), "please-enter-a-valid-birthday");
		}

		try {
			LocalDate dateValue = LocalDate.of(year, month, day);
			if (dateValue.isAfter(LocalDate.now())) {
				accountContext.addFieldError(validatedField.getFieldName(), "date-can-not-be-in-future");
			}
		} catch (DateTimeException e) {
			accountContext.addFieldError(validatedField.getFieldName(), "please-enter-a-valid-birthday");
		}
	}

	@Override
	public void validateEmailAddress(AccountContext accountContext, AccountValidatedField emailField, String emailAddress, String confirmEmailAddress,
			CreateAccountConfiguration addAccountConfiguration, Locale locale) throws UserAccountException {
		if (!addAccountConfiguration.emailAddressRequired() && Validator.isNull(emailAddress)) {
			return;
		}

		if (Validator.isNull(emailAddress) && (addAccountConfiguration.emailAddressRequired() || !accountContext.isCreatorSignedIn())) {
			accountContext.addFieldError(emailField.getFieldName(), emailField.getMandatoryErrorKey());
			return;
		}
		if (Validator.isNull(emailAddress)) {
			return;
		}

		if (Validator.isNotNull(emailAddress) && emailAddress.length() > emailField.getMaxLength()) {
			validateMaxLength(accountContext, emailAddress, emailField, locale);
			return;
		}

		if (!Validator.isEmailAddress(emailAddress)) {
			accountContext.addFieldError(emailField.getFieldName(), "please-enter-a-valid-email-address");
			return;
		}

		if (!emailAddress.equals(confirmEmailAddress)) {
			accountContext.addFieldError(emailField.getFieldName(), "email-addresses-did-not-match");
			return;
		}

		if (accountContext.isUpdate()) {
			User user;
			try {
				user = userLocalService.getUserById(accountContext.getUserId());
			} catch (PortalException e) {
				throw new UserAccountException(e);
			}
			if (user.getEmailAddress().equals(emailAddress)) {
				return;
			}
		}

		if (Validator.isNotNull(userLocalService.fetchUserByEmailAddress(accountContext.getCompanyId(), emailAddress))) {
			accountContext.addFieldError(emailField.getFieldName(), "the-email-address-you-requested-is-already-taken");
		}
	}

	@Override
	public void validateExpandoMandatoryField(AccountContext accountContext, Serializable fieldValue, String fieldName, String mandatoryMessageKey) {
		if (Validator.isNull(fieldValue)) {
			accountContext.addFieldError(fieldName, mandatoryMessageKey);
		}
	}

	@Override
	public void validateMandatoryField(AccountContext accountContext, String fieldValue, AccountValidatedField validatedField) {
		if (Validator.isNull(fieldValue)) {
			accountContext.addFieldError(validatedField.getFieldName(), validatedField.getMandatoryErrorKey());
		}
	}

	@Override
	public void validateMaxLength(AccountContext accountContext, String fieldValue, AccountValidatedField validatedField, Locale locale) {
		if (Validator.isNotNull(fieldValue) && fieldValue.length() > validatedField.getMaxLength()) {
			accountContext.addFieldError(validatedField.getFieldName(), LanguageUtil.format(locale, "please-enter-no-more-than-x-characters", validatedField.getMaxLength()));
		}
	}

	@Override
	public void validatePasswordForm(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException {

		validatePassword(accountContext, portletRequest, AccountValidatedField.PASSWORD, accountContext.getPassword1(), accountContext.getPassword2());

		if (accountContextService.isShowCaptchaOnPasswordStep(accountContext, portletRequest)) {
			captchaValidationService.validateCaptcha(accountContext, AccountValidatedField.CAPTCHA, portletRequest);
		}

	}

	@Override
	public void validatePersonalDetails(AccountContext accountContext, PortletRequest portletRequest, CreateAccountConfiguration addAccountConfiguration,
			UserProfileFieldsConfiguration userProfileConfiguration) throws UserAccountException {

		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		Locale locale = themeDisplay.getLocale();

		validateMandatoryField(accountContext, accountContext.getFirstName(), AccountValidatedField.FIRSTNAME);
		validateMaxLength(accountContext, accountContext.getFirstName(), AccountValidatedField.FIRSTNAME, locale);

		validateMandatoryField(accountContext, accountContext.getLastName(), AccountValidatedField.LASTNAME);
		validateMaxLength(accountContext, accountContext.getLastName(), AccountValidatedField.LASTNAME, locale);

		if (userProfileConfiguration.dateOfBirthEnabled()) {
			validateDate(accountContext, AccountValidatedField.DATE_OF_BIRTH, accountContext.getBirthYear(), accountContext.getBirthMonth(), accountContext.getBirthDay(),
					userProfileConfiguration.dateOfBirthRequired());
		}

		boolean isEmailAddressFieldOnPage = validateEmailIsOnCurrentPage(addAccountConfiguration, AccountConfigurationConstants.PAGE_PERSONAL_DETAILS);

		validateEmail(accountContext, isEmailAddressFieldOnPage, addAccountConfiguration, locale);
	}

	private ThemeDisplay getThemeDisplay(PortletRequest portletRequest) {
		return (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
	}

	private void validateEmail(AccountContext accountContext, boolean isEmailAddressFieldOnPage, CreateAccountConfiguration addAccountConfiguration, Locale locale) throws UserAccountException {

		if (isEmailAddressFieldOnPage) {
			if (!accountContext.hasFieldError(AccountValidatedField.CAPTCHA.getFieldName())) {
				validateEmailAddress(accountContext, AccountValidatedField.EMAIL, accountContext.getEmailAddress(), accountContext.getConfirmEmailAddress(), addAccountConfiguration, locale);
			}
		}
	}

	private boolean validateEmailIsOnCurrentPage(CreateAccountConfiguration addAccountConfiguration, String currentPage) {
		return currentPage.equals(addAccountConfiguration.emailAddressPageLocation());
	}

	private void validatePassword(AccountContext accountContext, PortletRequest portletRequest, AccountValidatedField passwordField, String password, String confirmPassword)
			throws UserAccountException {

		if (Validator.isNull(password) || Validator.isNull(confirmPassword)) {
			accountContext.addFieldError(passwordField.getFieldName(), passwordField.getMandatoryErrorKey());
		} else {
			try {
				PasswordPolicy passwordPolicy = passwordPolicyLocalService.getDefaultPasswordPolicy(accountContext.getCompanyId());
				PwdToolkitUtil.validate(accountContext.getCompanyId(), accountContext.getUserId(), password, confirmPassword, passwordPolicy);
			} catch (PortalException e) {
				if (e instanceof UserPasswordException.MustMatch) {
					accountContext.addFieldError(passwordField.getFieldName(), "the-passwords-you-entered-do-not-match");
				} else if (e instanceof UserPasswordException.MustBeLonger) {
					String errorMessage = AggregatedResourceBundleUtil.format("the-password-must-be-at-least-x-characters", String.valueOf(((UserPasswordException.MustBeLonger) e).minLength),
							portletRequest.getLocale(), AccountConstants.BUNDLE_SYMBOLIC_NAME);
					accountContext.addFieldError(passwordField.getFieldName(), errorMessage);
				} else if (e instanceof UserPasswordException.MustHaveMoreSymbols) {
					String[] messageArgs = new String[] { String.valueOf(((UserPasswordException.MustHaveMoreSymbols) e).minSymbols),
							PropsUtil.get(PropsKeys.PASSWORDS_PASSWORDPOLICYTOOLKIT_VALIDATOR_CHARSET_SYMBOLS) };
					String errorMessage = AggregatedResourceBundleUtil.format("password-must-use-at-least-x-of-these-symbols-x", messageArgs, portletRequest.getLocale(),
							AccountConstants.BUNDLE_SYMBOLIC_NAME);
					accountContext.addFieldError(passwordField.getFieldName(), errorMessage);
				} else if (!e.getClass().getSuperclass().equals(UserPasswordException.class)) {
					throw new UserAccountException();
				} else {
					accountContext.addFieldError(passwordField.getFieldName(), e.getMessage());
				}
			}
		}
	}

}
