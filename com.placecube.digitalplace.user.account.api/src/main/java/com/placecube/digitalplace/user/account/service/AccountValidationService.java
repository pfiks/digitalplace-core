package com.placecube.digitalplace.user.account.service;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;

import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountValidatedField;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.fields.model.UserAccountField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

public interface AccountValidationService {

	void validateAdditionalDetails(AccountContext accountContext, PortletRequest portletRequest, CreateAccountConfiguration accountConfiguration,
			UserProfileFieldsConfiguration userProfileConfiguration) throws UserAccountException;

	void validateCaptcha(AccountContext accountContext, PortletRequest portletRequest, CreateAccountConfiguration accountConfiguration, String currentPage) throws UserAccountException;

	void validateCreateUserAccountFields(AccountContext accountContext, List<UserAccountField> createUserAccountFields, PortletRequest portletRequest, boolean isEmailAddressFieldOnPage);

	void validateDate(AccountContext accountContext, AccountValidatedField validatedField, int year, int month, int day, boolean required);

	void validateEmailAddress(AccountContext accountContext, AccountValidatedField emailField, String emailAddress, String confirmEmailAddress,
			CreateAccountConfiguration addAccountConfiguration, Locale locale) throws UserAccountException;

	void validateExpandoMandatoryField(AccountContext accountContext, Serializable fieldValue, String fieldName, String mandatoryMessageKey);

	void validateMandatoryField(AccountContext accountContext, String fieldValue, AccountValidatedField validatedField);

	void validateMaxLength(AccountContext accountContext, String fieldValue, AccountValidatedField validatedField, Locale locale);

	void validatePasswordForm(AccountContext accountContext, PortletRequest portletRequest) throws UserAccountException;

	void validatePersonalDetails(AccountContext accountContext, PortletRequest portletRequest, CreateAccountConfiguration accountConfiguration,
			UserProfileFieldsConfiguration userProfileConfiguration) throws UserAccountException;
}
