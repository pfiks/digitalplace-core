package com.placecube.digitalplace.userimport.admin.web.model;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;

public class ImportResultWrapper {

	private List<UserImportResult> userImportResults;
	private final List<Long> notValidGroupIds;
	private final List<Group> validGroups;

	public ImportResultWrapper() {
		notValidGroupIds = new ArrayList<>();
		validGroups = new ArrayList<>();
	}

	public void addValidGroup(Group group){
		this.validGroups.add(group);
	}

	public void addNotValidGroupId(long groupId){
		this.notValidGroupIds.add(groupId);
	}

	public void setUserImportResults(List<UserImportResult> userImportResults) {
		this.userImportResults = userImportResults;
	}

	public String getFriendlyGroupAssignmentMessage(ResourceBundle resourceBundle) {
		StringBuilder stringBuilder = new StringBuilder();

		if (!validGroups.isEmpty()) {
			String data = validGroups.stream().map(Group::getName).collect(Collectors.joining(", "));
			stringBuilder.append(getMessageLine("users-added-as-members-to-following-groups", resourceBundle, data));
		}

		if (!notValidGroupIds.isEmpty()) {
			String data = notValidGroupIds.stream().map(String::valueOf).collect(Collectors.joining(", "));
			stringBuilder.append(getMessageLine("following-group-ids-were-not-valid", resourceBundle, data));
		}

		return stringBuilder.toString();
	}

	private String getMessageLine(String key, ResourceBundle resourceBundle, String data) {
		return "<p>" + LanguageUtil.get(resourceBundle, key) + "<br>" + data + "</p>";
	}

	public List<UserImportResult> getUserImportResults() {
		return userImportResults;
	}
}
