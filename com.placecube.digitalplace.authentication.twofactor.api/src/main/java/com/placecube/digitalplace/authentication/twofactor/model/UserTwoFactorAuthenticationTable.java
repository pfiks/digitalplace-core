/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.authentication.twofactor.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;Placecube_Auth_TwoFactor_UserTwoFactorAuthentication&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see UserTwoFactorAuthentication
 * @generated
 */
public class UserTwoFactorAuthenticationTable
	extends BaseTable<UserTwoFactorAuthenticationTable> {

	public static final UserTwoFactorAuthenticationTable INSTANCE =
		new UserTwoFactorAuthenticationTable();

	public final Column<UserTwoFactorAuthenticationTable, String> uuid =
		createColumn("uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<UserTwoFactorAuthenticationTable, Long> companyId =
		createColumn(
			"companyId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<UserTwoFactorAuthenticationTable, Long> userId =
		createColumn("userId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<UserTwoFactorAuthenticationTable, Date> createDate =
		createColumn(
			"createDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<UserTwoFactorAuthenticationTable, Date> modifiedDate =
		createColumn(
			"modifiedDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<UserTwoFactorAuthenticationTable, String> secretKey =
		createColumn(
			"secretKey", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<UserTwoFactorAuthenticationTable, Boolean> resetKey =
		createColumn(
			"resetKey", Boolean.class, Types.BOOLEAN, Column.FLAG_DEFAULT);
	public final Column<UserTwoFactorAuthenticationTable, Boolean> enabled =
		createColumn(
			"enabled", Boolean.class, Types.BOOLEAN, Column.FLAG_DEFAULT);

	private UserTwoFactorAuthenticationTable() {
		super(
			"Placecube_Auth_TwoFactor_UserTwoFactorAuthentication",
			UserTwoFactorAuthenticationTable::new);
	}

}