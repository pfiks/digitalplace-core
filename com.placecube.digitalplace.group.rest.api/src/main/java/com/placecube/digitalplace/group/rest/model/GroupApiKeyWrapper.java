/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link GroupApiKey}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GroupApiKey
 * @generated
 */
public class GroupApiKeyWrapper
	extends BaseModelWrapper<GroupApiKey>
	implements GroupApiKey, ModelWrapper<GroupApiKey> {

	public GroupApiKeyWrapper(GroupApiKey groupApiKey) {
		super(groupApiKey);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("applicationKey", getApplicationKey());
		attributes.put("applicationSecret", getApplicationSecret());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String applicationKey = (String)attributes.get("applicationKey");

		if (applicationKey != null) {
			setApplicationKey(applicationKey);
		}

		String applicationSecret = (String)attributes.get("applicationSecret");

		if (applicationSecret != null) {
			setApplicationSecret(applicationSecret);
		}
	}

	@Override
	public GroupApiKey cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the application key of this group api key.
	 *
	 * @return the application key of this group api key
	 */
	@Override
	public String getApplicationKey() {
		return model.getApplicationKey();
	}

	/**
	 * Returns the application secret of this group api key.
	 *
	 * @return the application secret of this group api key
	 */
	@Override
	public String getApplicationSecret() {
		return model.getApplicationSecret();
	}

	/**
	 * Returns the company ID of this group api key.
	 *
	 * @return the company ID of this group api key
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the group ID of this group api key.
	 *
	 * @return the group ID of this group api key
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the primary key of this group api key.
	 *
	 * @return the primary key of this group api key
	 */
	@Override
	public
		com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK
			getPrimaryKey() {

		return model.getPrimaryKey();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the application key of this group api key.
	 *
	 * @param applicationKey the application key of this group api key
	 */
	@Override
	public void setApplicationKey(String applicationKey) {
		model.setApplicationKey(applicationKey);
	}

	/**
	 * Sets the application secret of this group api key.
	 *
	 * @param applicationSecret the application secret of this group api key
	 */
	@Override
	public void setApplicationSecret(String applicationSecret) {
		model.setApplicationSecret(applicationSecret);
	}

	/**
	 * Sets the company ID of this group api key.
	 *
	 * @param companyId the company ID of this group api key
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the group ID of this group api key.
	 *
	 * @param groupId the group ID of this group api key
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the primary key of this group api key.
	 *
	 * @param primaryKey the primary key of this group api key
	 */
	@Override
	public void setPrimaryKey(
		com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK
			primaryKey) {

		model.setPrimaryKey(primaryKey);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	protected GroupApiKeyWrapper wrap(GroupApiKey groupApiKey) {
		return new GroupApiKeyWrapper(groupApiKey);
	}

}