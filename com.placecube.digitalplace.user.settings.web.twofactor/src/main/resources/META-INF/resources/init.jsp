<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/journal" prefix="liferay-journal" %>

<%@ page import="com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys" %>
<%@ page import="com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<c:set var="ENABLE_TWO_FACTOR_AUTHENTICATION" value="<%= TwoFactorAuthenticationKeys.ENABLE_TWO_FACTOR_AUTHENTICATION %>" />
<c:set var="GENERATE_TWO_FACTOR_AUTHENTICATION" value="<%= TwoFactorAuthenticationKeys.GENERATE_TWO_FACTOR_AUTHENTICATION %>" />
<c:set var="QR_URL" value="<%= TwoFactorAuthenticationRequestKeys.QR_URL %>" />
<c:set var="SECRET_KEY" value="<%= TwoFactorAuthenticationRequestKeys.SECRET_KEY %>" />
<c:set var="SECRET_KEY_GENERATED" value="<%= TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED %>" />
<c:set var="TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM" value="<%= TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM %>" />
<c:set var="TWO_FACTOR_AUTHENTICATION_ENFORCED" value="<%= TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_ENFORCED %>" />
<c:set var="TWO_FACTOR_VERIFICATION_CODE" value="<%= TwoFactorAuthenticationRequestKeys.TWO_FACTOR_VERIFICATION_CODE %>" />
<c:set var="SAVE_TWO_FACTOR_AUTHENTICATION" value="<%= TwoFactorAuthenticationKeys.SAVE_TWO_FACTOR_AUTHENTICATION %>" />

<c:set var="qrUrl" value="${ requestScope[QR_URL] }" />
<c:set var="secretKey" value="${ requestScope[SECRET_KEY] }" />
<c:set var="secretKeyGenerated" value="${ requestScope[SECRET_KEY_GENERATED] }" />
<c:set var="twoFactorAuthenticationDisplayForm" value="${ requestScope[TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM] }" />
