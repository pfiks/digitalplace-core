/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.authentication.twofactor.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link UserTwoFactorAuthenticationLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see UserTwoFactorAuthenticationLocalService
 * @generated
 */
public class UserTwoFactorAuthenticationLocalServiceWrapper
	implements ServiceWrapper<UserTwoFactorAuthenticationLocalService>,
			   UserTwoFactorAuthenticationLocalService {

	public UserTwoFactorAuthenticationLocalServiceWrapper() {
		this(null);
	}

	public UserTwoFactorAuthenticationLocalServiceWrapper(
		UserTwoFactorAuthenticationLocalService
			userTwoFactorAuthenticationLocalService) {

		_userTwoFactorAuthenticationLocalService =
			userTwoFactorAuthenticationLocalService;
	}

	/**
	 * Adds the user two factor authentication to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 * @return the user two factor authentication that was added
	 */
	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication addUserTwoFactorAuthentication(
			com.placecube.digitalplace.authentication.twofactor.model.
				UserTwoFactorAuthentication userTwoFactorAuthentication) {

		return _userTwoFactorAuthenticationLocalService.
			addUserTwoFactorAuthentication(userTwoFactorAuthentication);
	}

	@Override
	public void configureTwoFactorAuthenticationForUser(
		com.liferay.portal.kernel.model.User user, String secretKey) {

		_userTwoFactorAuthenticationLocalService.
			configureTwoFactorAuthenticationForUser(user, secretKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userTwoFactorAuthenticationLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Creates a new user two factor authentication with the primary key. Does not add the user two factor authentication to the database.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key for the new user two factor authentication
	 * @return the new user two factor authentication
	 */
	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication createUserTwoFactorAuthentication(
			com.placecube.digitalplace.authentication.twofactor.service.
				persistence.UserTwoFactorAuthenticationPK
					userTwoFactorAuthenticationPK) {

		return _userTwoFactorAuthenticationLocalService.
			createUserTwoFactorAuthentication(userTwoFactorAuthenticationPK);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userTwoFactorAuthenticationLocalService.deletePersistedModel(
			persistedModel);
	}

	/**
	 * Deletes the user two factor authentication from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 * @return the user two factor authentication that was removed
	 */
	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication deleteUserTwoFactorAuthentication(
			com.placecube.digitalplace.authentication.twofactor.model.
				UserTwoFactorAuthentication userTwoFactorAuthentication) {

		return _userTwoFactorAuthenticationLocalService.
			deleteUserTwoFactorAuthentication(userTwoFactorAuthentication);
	}

	/**
	 * Deletes the user two factor authentication with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication that was removed
	 * @throws PortalException if a user two factor authentication with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication deleteUserTwoFactorAuthentication(
				com.placecube.digitalplace.authentication.twofactor.service.
					persistence.UserTwoFactorAuthenticationPK
						userTwoFactorAuthenticationPK)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _userTwoFactorAuthenticationLocalService.
			deleteUserTwoFactorAuthentication(userTwoFactorAuthenticationPK);
	}

	@Override
	public void disableTwoFactorAuthenticationForUser(
		com.liferay.portal.kernel.model.User user) {

		_userTwoFactorAuthenticationLocalService.
			disableTwoFactorAuthenticationForUser(user);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _userTwoFactorAuthenticationLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _userTwoFactorAuthenticationLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userTwoFactorAuthenticationLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _userTwoFactorAuthenticationLocalService.dynamicQuery(
			dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _userTwoFactorAuthenticationLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _userTwoFactorAuthenticationLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _userTwoFactorAuthenticationLocalService.dynamicQueryCount(
			dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _userTwoFactorAuthenticationLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication
			enableAndGetOrCreateTwoFactorAuthenticationForUser(
				com.liferay.portal.kernel.model.User user, boolean enabled) {

		return _userTwoFactorAuthenticationLocalService.
			enableAndGetOrCreateTwoFactorAuthenticationForUser(user, enabled);
	}

	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication fetchUserTwoFactorAuthentication(
			com.placecube.digitalplace.authentication.twofactor.service.
				persistence.UserTwoFactorAuthenticationPK
					userTwoFactorAuthenticationPK) {

		return _userTwoFactorAuthenticationLocalService.
			fetchUserTwoFactorAuthentication(userTwoFactorAuthenticationPK);
	}

	/**
	 * Returns the user two factor authentication with the matching UUID and company.
	 *
	 * @param uuid the user two factor authentication's UUID
	 * @param companyId the primary key of the company
	 * @return the matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication
			fetchUserTwoFactorAuthenticationByUuidAndCompanyId(
				String uuid, long companyId) {

		return _userTwoFactorAuthenticationLocalService.
			fetchUserTwoFactorAuthenticationByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Generates a new random security key
	 *
	 * @return the generated security key
	 */
	@Override
	public String generateSecretKey() {
		return _userTwoFactorAuthenticationLocalService.generateSecretKey();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _userTwoFactorAuthenticationLocalService.
			getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _userTwoFactorAuthenticationLocalService.
			getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _userTwoFactorAuthenticationLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _userTwoFactorAuthenticationLocalService.
			getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userTwoFactorAuthenticationLocalService.getPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Gets the QR bar code URL.
	 *
	 * @param companyId the company id
	 * @param emailAddress the email address of the user
	 * @param secretKey the secret key associated to the user
	 * @return the QR bar code URL
	 * @throws PortalException any exception retrieving the qr bar code url
	 */
	@Override
	public String getQRBarCodeURL(
			long companyId, String emailAddress, String secretKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userTwoFactorAuthenticationLocalService.getQRBarCodeURL(
			companyId, emailAddress, secretKey);
	}

	@Override
	public java.util.Optional
		<com.placecube.digitalplace.authentication.twofactor.model.
			UserTwoFactorAuthentication> getUserTwoFactorAuthentication(
				com.liferay.portal.kernel.model.User user) {

		return _userTwoFactorAuthenticationLocalService.
			getUserTwoFactorAuthentication(user);
	}

	/**
	 * Returns the user two factor authentication with the primary key.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication
	 * @throws PortalException if a user two factor authentication with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication getUserTwoFactorAuthentication(
				com.placecube.digitalplace.authentication.twofactor.service.
					persistence.UserTwoFactorAuthenticationPK
						userTwoFactorAuthenticationPK)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _userTwoFactorAuthenticationLocalService.
			getUserTwoFactorAuthentication(userTwoFactorAuthenticationPK);
	}

	/**
	 * Returns the user two factor authentication with the matching UUID and company.
	 *
	 * @param uuid the user two factor authentication's UUID
	 * @param companyId the primary key of the company
	 * @return the matching user two factor authentication
	 * @throws PortalException if a matching user two factor authentication could not be found
	 */
	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication
				getUserTwoFactorAuthenticationByUuidAndCompanyId(
					String uuid, long companyId)
			throws com.liferay.portal.kernel.exception.PortalException {

		return _userTwoFactorAuthenticationLocalService.
			getUserTwoFactorAuthenticationByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of all the user two factor authentications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @return the range of user two factor authentications
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.authentication.twofactor.model.
			UserTwoFactorAuthentication> getUserTwoFactorAuthentications(
				int start, int end) {

		return _userTwoFactorAuthenticationLocalService.
			getUserTwoFactorAuthentications(start, end);
	}

	/**
	 * Returns the number of user two factor authentications.
	 *
	 * @return the number of user two factor authentications
	 */
	@Override
	public int getUserTwoFactorAuthenticationsCount() {
		return _userTwoFactorAuthenticationLocalService.
			getUserTwoFactorAuthenticationsCount();
	}

	@Override
	public boolean isTwoFactorAuthenticationEnforcedForUser(
		com.liferay.portal.kernel.model.User user) {

		return _userTwoFactorAuthenticationLocalService.
			isTwoFactorAuthenticationEnforcedForUser(user);
	}

	/**
	 * Checks if the Two-Factor authentication code is valid for the user.
	 *
	 * @param user the user
	 * @param authenticationCode the authenticationCode to check
	 * @return true if the code is valid or if the feature is disabled for the
	 user. Returns false if the feature is enabled and the code is not
	 valid
	 * @throws PortalException any exception while checking the two factor
	 authentication code
	 */
	@Override
	public boolean isValidTwoFactorAuthentication(
			com.liferay.portal.kernel.model.User user,
			String authenticationCode)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userTwoFactorAuthenticationLocalService.
			isValidTwoFactorAuthentication(user, authenticationCode);
	}

	@Override
	public void resetUserTwoFactorAuthentication(long companyId, long userId) {
		_userTwoFactorAuthenticationLocalService.
			resetUserTwoFactorAuthentication(companyId, userId);
	}

	/**
	 * Updates the user two factor authentication in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 * @return the user two factor authentication that was updated
	 */
	@Override
	public com.placecube.digitalplace.authentication.twofactor.model.
		UserTwoFactorAuthentication updateUserTwoFactorAuthentication(
			com.placecube.digitalplace.authentication.twofactor.model.
				UserTwoFactorAuthentication userTwoFactorAuthentication) {

		return _userTwoFactorAuthenticationLocalService.
			updateUserTwoFactorAuthentication(userTwoFactorAuthentication);
	}

	@Override
	public boolean verifyAndEnableUserTwoFactorAuthentication(
			com.liferay.portal.kernel.model.User user,
			String authenticationCode, String secretKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _userTwoFactorAuthenticationLocalService.
			verifyAndEnableUserTwoFactorAuthentication(
				user, authenticationCode, secretKey);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _userTwoFactorAuthenticationLocalService.getBasePersistence();
	}

	@Override
	public UserTwoFactorAuthenticationLocalService getWrappedService() {
		return _userTwoFactorAuthenticationLocalService;
	}

	@Override
	public void setWrappedService(
		UserTwoFactorAuthenticationLocalService
			userTwoFactorAuthenticationLocalService) {

		_userTwoFactorAuthenticationLocalService =
			userTwoFactorAuthenticationLocalService;
	}

	private UserTwoFactorAuthenticationLocalService
		_userTwoFactorAuthenticationLocalService;

}