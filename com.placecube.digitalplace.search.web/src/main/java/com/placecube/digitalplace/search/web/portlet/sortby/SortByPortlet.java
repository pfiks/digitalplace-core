package com.placecube.digitalplace.search.web.portlet.sortby;

import java.io.IOException;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.common.portlet.PortletURLService;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.model.SortByOption;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.configuration.SortByPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.sortby.service.SortByService;

@Component(immediate = true, property = { "com.liferay.fragment.entry.processor.portlet.alias=dp-search-sortby", //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.css-class-wrapper=dp-search-sort-by dp-search", //
		"com.liferay.portlet.display-category=category.digitalplace.search", //
		"com.liferay.portlet.instanceable=false", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.preferences-owned-by-group=true", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.restore-current-view=false", //
		"com.liferay.portlet.use-default-template=true", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.add-process-action-success-action=false", //
		"javax.portlet.init-param.config-template=/sort-by/configuration.jsp", //
		"javax.portlet.init-param.template-path=/META-INF/resources/", //
		"javax.portlet.init-param.view-template=/sort-by/view.jsp", //
		"javax.portlet.name=" + PortletKeys.SORT_BY, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=guest,power-user,user", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class SortByPortlet extends MVCPortlet {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private PortletURLService portletURLService;

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Reference
	private SortByService sortByService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			SortByPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(SortByPortletInstanceConfiguration.class, themeDisplay);
			String[] enabledSortByOptions = configuration.enabledSortByOptions();

			if (ArrayUtil.isEmpty(enabledSortByOptions) || Validator.isNull(enabledSortByOptions[0]) || Boolean.FALSE.toString().equals(enabledSortByOptions[0])) {
				renderRequest.setAttribute("invalidConfiguration", true);
			} else {
				SharedSearchResponse searchResponse = sharedSearchRequestService.search(renderRequest);

				List<SortByOption> sortByOptions = sortByService.getAvailableSortByOptions(enabledSortByOptions, searchResponse.getKeywords());
				if (sortByOptions.isEmpty()) {
					renderRequest.setAttribute("invalidConfiguration", true);
				} else {
					String selectedSortBy = searchResponse.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY);

					if (Validator.isNull(selectedSortBy)) {
						selectedSortBy = sortByOptions.get(0).getId();
					}

					if (configuration.sortDirectionChangeable()) {
						renderRequest.setAttribute("sortDirectionChangeable", true);
						renderRequest.setAttribute("currentSelectedSortByDirection", searchResponse.getSharedSearchSessionSingleValuedAttribute(SearchParameters.SELECTED_SORT_BY_DIRECTION));
					}

					renderRequest.setAttribute("selectedSortBy", selectedSortBy);

					renderRequest.setAttribute("sortByOptions", sortByOptions);

					renderRequest.setAttribute("redirectURL", searchResponse.getEscapedSearchResultsIteratorURL());

					renderRequest.setAttribute("displayPortlet", configuration.displayPortlet());

					renderRequest.setAttribute("isEditLayoutMode", portletURLService.isEditLayoutMode(renderRequest));
				}
			}

			super.render(renderRequest, renderResponse);
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

}