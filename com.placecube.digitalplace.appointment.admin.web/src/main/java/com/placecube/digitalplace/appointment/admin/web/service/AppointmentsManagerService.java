package com.placecube.digitalplace.appointment.admin.web.service;

import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.petra.sql.dsl.DSLQueryFactoryUtil;
import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.ParamUtil;
import com.pfiks.common.time.model.DateTimeSelection;
import com.placecube.digitalplace.appointment.admin.web.model.AppointmentSlotForm;
import com.placecube.digitalplace.appointment.admin.web.model.AppointmentSlotModelBuilder;
import com.placecube.digitalplace.appointment.booking.constants.AppointmentSlotType;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;
import com.placecube.digitalplace.appointment.model.AppointmentSlot;
import com.placecube.digitalplace.appointment.model.AppointmentSlotTable;
import com.placecube.digitalplace.appointment.service.AppointmentSlotLocalService;

@Component(immediate = true, service = AppointmentsManagerService.class)
public class AppointmentsManagerService {

	@Reference
	private AppointmentBookingService appointmentBookingService;

	@Reference
	private AppointmentSlotLocalService appointmentSlotLocalService;

	@Reference
	private AppointmentSlotModelBuilder appointmentSlotModelBuilder;

	@Reference
	private AppointmentsManagerUtils appointmentsManagerUtils;

	@Reference
	private CounterLocalService counterLocalService;

	public List<AppointmentSlotForm> convertToAppointmentSlotForm(List<AppointmentSlot> appointmentSlots) {
		return appointmentSlots.stream().map(entry -> appointmentSlotModelBuilder.getAppointmentSlotForm(entry)).collect(Collectors.toList());
	}

	public void createNewAppointmentSlot(AppointmentSlotForm form, User user) {
		long nextId = counterLocalService.increment(AppointmentSlot.class.getName());

		AppointmentSlot appointmentSlot = appointmentSlotLocalService.createAppointmentSlot(nextId);

		appointmentSlot.setCompanyId(user.getCompanyId());
		appointmentSlot.setUserId(user.getUserId());
		appointmentSlot.setUserName(user.getFullName());

		appointmentSlot.setSlotType(form.getSlotType());
		appointmentSlot.setServiceId(form.getServiceId());

		setUpdatableFieldsInSlot(appointmentSlot, form, user.getTimeZone());

		appointmentSlotLocalService.addAppointmentSlot(appointmentSlot);
	}

	public AppointmentBookingEntry getAppointmentBookingEntryFromRequest(ActionRequest actionRequest) {
		AppointmentBookingEntry appointmentEntry = new AppointmentBookingEntry();
		appointmentEntry.setAppointmentEntryId(ParamUtil.getLong(actionRequest, "appointmentEntryId"));
		appointmentEntry.setAppointmentId(ParamUtil.getString(actionRequest, "appointmentId"));
		appointmentEntry.setClassNameId(ParamUtil.getLong(actionRequest, "classNameId"));
		appointmentEntry.setClassPK(ParamUtil.getLong(actionRequest, "classPK"));
		return appointmentEntry;
	}

	public AppointmentSlotForm getAppointmentSlotForm(long appointmentSlotId) throws PortalException {
		return appointmentSlotModelBuilder.getAppointmentSlotForm(appointmentSlotLocalService.getAppointmentSlot(appointmentSlotId));
	}

	public AppointmentSlotForm getAppointmentSlotFormFromRequest(ActionRequest actionRequest) throws PortalException {
		AppointmentSlotForm appointmentSlotForm;

		DateTimeSelection startDateTimeFromRequest = appointmentsManagerUtils.getStartDateTimeFromRequest(actionRequest);
		DateTimeSelection endDateTimeFromRequest = appointmentsManagerUtils.getEndDateTimeFromRequest(actionRequest);

		long appointmentSlotId = ParamUtil.getLong(actionRequest, "appointmentSlotId", 0l);
		if (appointmentSlotId > 0) {
			appointmentSlotForm = new AppointmentSlotForm(appointmentSlotLocalService.getAppointmentSlot(appointmentSlotId), startDateTimeFromRequest, endDateTimeFromRequest);
		} else {
			appointmentSlotForm = new AppointmentSlotForm(ParamUtil.getString(actionRequest, "slotType"), ParamUtil.getString(actionRequest, "serviceId"), startDateTimeFromRequest,
					endDateTimeFromRequest);
		}

		appointmentSlotForm.setAppointmentDuration(ParamUtil.getInteger(actionRequest, "appointmentDuration"));
		appointmentSlotForm.setName(ParamUtil.getString(actionRequest, "name"));
		appointmentSlotForm.setMaxAppointments(ParamUtil.getInteger(actionRequest, "maxAppointments", 1));
		appointmentSlotForm.setDaysOfWeekBitwiseValue(appointmentsManagerUtils.getDaysOfWeekBitwiseValue(ParamUtil.getIntegerValues(actionRequest, "daysOfWeek", new int[0])));

		return appointmentSlotForm;
	}

	public List<String> getAvailableServiceIds(long companyId) {
		DSLQuery dslQuery = DSLQueryFactoryUtil.selectDistinct(AppointmentSlotTable.INSTANCE.serviceId)//
				.from(AppointmentSlotTable.INSTANCE)//
				.where(AppointmentSlotTable.INSTANCE.companyId.eq(companyId));
		return appointmentSlotLocalService.dslQuery(dslQuery);
	}

	public AppointmentSlotForm getBlankAppointmentSlotForm(TimeZone timeZone, String slotType, String serviceId) {
		return new AppointmentSlotForm(slotType, serviceId, appointmentsManagerUtils.getStartDateTimeSelection(timeZone), appointmentsManagerUtils.getEndDateTimeSelection(timeZone));
	}

	public List<AppointmentBookingEntry> getBookedAppointments(long companyId, String serviceId, DateTimeSelection startDate, DateTimeSelection endDate) {
		AppointmentContext appointmentContext = appointmentsManagerUtils.getAppointmentContext(startDate, endDate);

		return appointmentBookingService.getAppointments(companyId, serviceId, appointmentContext);
	}

	public DateTimeSelection getFilterEndDate(boolean searchExecuted, RenderRequest renderRequest, TimeZone timeZone) {
		DateTimeSelection valueFromRequest = appointmentsManagerUtils.getEndDateTimeFromRequest(renderRequest);
		return searchExecuted && valueFromRequest.getYear() != 0 ? valueFromRequest : appointmentsManagerUtils.getEndDateTimeSelection(timeZone);
	}

	public DateTimeSelection getFilterStartDate(boolean searchExecuted, RenderRequest renderRequest, TimeZone timeZone) {
		DateTimeSelection valueFromRequest = appointmentsManagerUtils.getStartDateTimeFromRequest(renderRequest);
		return searchExecuted && valueFromRequest.getYear() != 0 ? valueFromRequest : appointmentsManagerUtils.getStartDateTimeSelection(timeZone);
	}

	public SearchContainer<AppointmentSlotForm> getSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse, String slotType) {
		SearchContainer<AppointmentSlotForm> searchContainer = new SearchContainer<>(renderRequest, null, null, "cur" + slotType, 20, renderResponse.createRenderURL(), null, "no-results-were-found");
		searchContainer.setDeltaConfigurable(true);
		searchContainer.setId("appointmentSlotsSearchContainer" + slotType);
		return searchContainer;
	}

	public void updateAppointmentSlot(AppointmentSlotForm form, TimeZone timeZone) throws PortalException {
		AppointmentSlot appointmentSlot = appointmentSlotLocalService.getAppointmentSlot(form.getAppointmentSlotId());
		setUpdatableFieldsInSlot(appointmentSlot, form, timeZone);
		appointmentSlotLocalService.updateAppointmentSlot(appointmentSlot);
	}

	private void setUpdatableFieldsInSlot(AppointmentSlot appointmentSlot, AppointmentSlotForm form, TimeZone timeZone) {
		appointmentSlot.setName(form.getName());
		appointmentSlot.setDaysOfWeekBitwise(form.getDaysOfWeekBitwiseValue());
		if (AppointmentSlotType.CONFIGURATION.name().equals(form.getSlotType())) {
			appointmentSlot.setMaxAppointments(form.getMaxAppointments());
			appointmentSlot.setAppointmentDuration(form.getAppointmentDuration());
		}

		appointmentSlot.setStartDate(form.getSlotStartDate(timeZone));
		appointmentSlot.setSlotStartTimeHour(form.getSlotStartTimeHour());
		appointmentSlot.setSlotStartTimeMinute(form.getSlotStartTimeMinute());

		appointmentSlot.setEndDate(form.getSlotEndDate(timeZone));
		appointmentSlot.setSlotEndTimeHour(form.getSlotEndTimeHour());
		appointmentSlot.setSlotEndTimeMinute(form.getSlotEndTimeMinute());
	}

}
