<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ include file="init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post" name="calendarconfigfm">

	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />

	<liferay-frontend:edit-form-body>
		<dp-frontend:fieldset-group>
			<liferay-frontend:fieldset>
				<aui:select label="display-events-for" name="scope" showEmptyOption="false" required="true">

					<aui:option value="GROUP" selected="${configuration.scope().equals('GROUP')}">
						<liferay-ui:message key="group" />
					</aui:option>

					<aui:option value="ALL" selected="${configuration.scope().equals('ALL')}">
						<liferay-ui:message key="all" />
					</aui:option>

				</aui:select>
			</liferay-frontend:fieldset>
		</dp-frontend:fieldset-group>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>