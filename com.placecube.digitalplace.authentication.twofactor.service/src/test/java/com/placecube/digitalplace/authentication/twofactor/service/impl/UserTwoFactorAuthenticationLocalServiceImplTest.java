package com.placecube.digitalplace.authentication.twofactor.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.joda.time.DateTimeUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.authentication.twofactor.checker.EnforceTwoFactorAuthenticationChecker;
import com.placecube.digitalplace.authentication.twofactor.checker.registry.EnforceTwoFactorAuthenticationCheckerRegistry;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.internal.TwoFactorUtils;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.authentication.twofactor.service.persistence.UserTwoFactorAuthenticationPK;
import com.placecube.digitalplace.authentication.twofactor.service.persistence.UserTwoFactorAuthenticationPersistence;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class UserTwoFactorAuthenticationLocalServiceImplTest extends PowerMockito {

	private static final String CODE = "codeValue";
	private static final long COMPANY_ID = 10;
	private static final String SECRET_KEY = "secretKeyValue";
	private static final long USER_ID = 20;

	private static final String UUID = "uuidVal";

	@Mock
	private AuthenticationTwoFactorCompanyConfiguration mockAuthenticationTwoFactorCompanyConfiguration;

	@Mock
	private EnforceTwoFactorAuthenticationChecker mockEnforceTwoFactorAuthenticationChecker1;

	@Mock
	private EnforceTwoFactorAuthenticationChecker mockEnforceTwoFactorAuthenticationChecker2;

	@Mock
	private EnforceTwoFactorAuthenticationCheckerRegistry mockEnforceTwoFactorAuthenticationCheckerRegistry;

	@Mock
	private TwoFactorUtils mockTwoFactorUtils;

	@Mock
	private UserTwoFactorAuthentication mockUpdatedUserTwoFactorAuthentication;

	@Mock
	private User mockUser;

	@Mock
	private UserTwoFactorAuthentication mockUserTwoFactorAuthentication;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	@Mock
	private UserTwoFactorAuthenticationPersistence mockUserTwoFactorAuthenticationPersistence;

	@Mock
	private UserTwoFactorAuthenticationPK mockUserTwoFactorAuthenticationPK;

	private Date testDate = new Date();

	@InjectMocks
	private UserTwoFactorAuthenticationLocalServiceImpl userTwoFactorAuthenticationLocalServiceImpl;

	@Before
	public void activateSetup() {
		initMocks(this);

		DateTimeUtils.setCurrentMillisFixed(testDate.getTime());
	}

	@Test
	public void configureTwoFactorAuthenticationForUser_WhenEncryptedValueFoundAndUserAlreadyHasTwoFactorConfig_ThenUpdatesTheCurrentConfig() {
		String encrypted = "encryptedVal";
		when(mockUser.getUserUuid()).thenReturn(UUID);
		when(mockTwoFactorUtils.encrypt(SECRET_KEY, UUID)).thenReturn(Optional.of(encrypted));
		when(mockTwoFactorUtils.getPrimaryKey(mockUser)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(mockUserTwoFactorAuthentication);

		userTwoFactorAuthenticationLocalServiceImpl.configureTwoFactorAuthenticationForUser(mockUser, SECRET_KEY);

		InOrder inOrder = inOrder(mockUserTwoFactorAuthentication, mockUserTwoFactorAuthenticationLocalService);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setSecretKey(encrypted);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setEnabled(true);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setResetKey(false);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setModifiedDate(testDate);
		inOrder.verify(mockUserTwoFactorAuthenticationLocalService, times(1)).updateUserTwoFactorAuthentication(mockUserTwoFactorAuthentication);
	}

	@Test
	public void configureTwoFactorAuthenticationForUser_WhenEncryptedValueFoundAndUserDoesNotYetHaveTwoFactorConfig_ThenCreatesAnewOneAndConfiguresIt() {
		String encrypted = "encryptedVal";
		when(mockUser.getUserUuid()).thenReturn(UUID);
		when(mockTwoFactorUtils.encrypt(SECRET_KEY, UUID)).thenReturn(Optional.of(encrypted));
		when(mockTwoFactorUtils.getPrimaryKey(mockUser)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(null);
		when(mockUserTwoFactorAuthenticationLocalService.createUserTwoFactorAuthentication(mockUserTwoFactorAuthenticationPK)).thenReturn(mockUserTwoFactorAuthentication);

		userTwoFactorAuthenticationLocalServiceImpl.configureTwoFactorAuthenticationForUser(mockUser, SECRET_KEY);

		InOrder inOrder = inOrder(mockUserTwoFactorAuthentication, mockUserTwoFactorAuthenticationLocalService);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setSecretKey(encrypted);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setEnabled(true);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setResetKey(false);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setModifiedDate(testDate);
		inOrder.verify(mockUserTwoFactorAuthenticationLocalService, times(1)).updateUserTwoFactorAuthentication(mockUserTwoFactorAuthentication);
	}

	@Test
	public void configureTwoFactorAuthenticationForUser_WhenEncryptedValueNotFound_ThenNoChangesAreMade() {
		String uuid = "uuidVal";
		when(mockUser.getUuid()).thenReturn(uuid);
		when(mockTwoFactorUtils.encrypt(SECRET_KEY, uuid)).thenReturn(Optional.empty());

		userTwoFactorAuthenticationLocalServiceImpl.configureTwoFactorAuthenticationForUser(mockUser, SECRET_KEY);

		verifyZeroInteractions(mockUserTwoFactorAuthenticationPersistence, mockUserTwoFactorAuthenticationLocalService);
	}

	@Test
	public void disableTwoFactorAuthenticationForUser_WhenNoError_ThenDisable2FAForUser() {
		when(mockTwoFactorUtils.getPrimaryKey(mockUser)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(mockUserTwoFactorAuthentication);

		userTwoFactorAuthenticationLocalServiceImpl.disableTwoFactorAuthenticationForUser(mockUser);

		InOrder inOrder = inOrder(mockUserTwoFactorAuthentication, mockUserTwoFactorAuthenticationLocalService);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setEnabled(false);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setSecretKey(StringPool.BLANK);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setResetKey(true);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setModifiedDate(testDate);
		inOrder.verify(mockUserTwoFactorAuthenticationLocalService, times(1)).updateUserTwoFactorAuthentication(mockUserTwoFactorAuthentication);
	}

	@Test
	public void disableTwoFactorAuthenticationForUser_WhenUserHasNoTwoFactorAuthentication_ThenDoNothing() {
		when(mockTwoFactorUtils.getPrimaryKey(mockUser)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(null);

		userTwoFactorAuthenticationLocalServiceImpl.disableTwoFactorAuthenticationForUser(mockUser);

		verifyZeroInteractions(mockUserTwoFactorAuthenticationLocalService);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enableAndGetOrCreateTwoFactorAuthenticationForUser_WhenNoErrors_ThenResetsUserTwoFactorAuthenticationFieldsAndUpdatesItInOrder(boolean enabled) {
		when(mockTwoFactorUtils.getPrimaryKey(mockUser)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(mockUserTwoFactorAuthentication);

		userTwoFactorAuthenticationLocalServiceImpl.enableAndGetOrCreateTwoFactorAuthenticationForUser(mockUser, enabled);

		InOrder inOrder = inOrder(mockUserTwoFactorAuthentication, mockUserTwoFactorAuthenticationPersistence);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setEnabled(enabled);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setSecretKey(StringPool.BLANK);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setResetKey(true);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setModifiedDate(testDate);
		inOrder.verify(mockUserTwoFactorAuthenticationPersistence, times(1)).update(mockUserTwoFactorAuthentication);
	}

	@Test
	public void enableAndGetOrCreateTwoFactorAuthenticationForUser_WhenUserTwoFactorAuthenticationDoesNotExist_ThenCreatesUserTwoFactorAuthenticationAndUpdatesAndReturnsIt() {
		when(mockTwoFactorUtils.getPrimaryKey(mockUser)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(null);
		when(mockUserTwoFactorAuthenticationPersistence.create(mockUserTwoFactorAuthenticationPK)).thenReturn(mockUserTwoFactorAuthentication);
		when(mockUserTwoFactorAuthenticationPersistence.update(mockUserTwoFactorAuthentication)).thenReturn(mockUpdatedUserTwoFactorAuthentication);

		UserTwoFactorAuthentication result = userTwoFactorAuthenticationLocalServiceImpl.enableAndGetOrCreateTwoFactorAuthenticationForUser(mockUser, true);

		assertThat(result, sameInstance(mockUpdatedUserTwoFactorAuthentication));
	}

	@Test
	public void enableAndGetOrCreateTwoFactorAuthenticationForUser_WhenUserTwoFactorAuthenticationExists_ThenUpdatesAndReturnsUserTwoFactorAuthenticationForUser() {
		when(mockTwoFactorUtils.getPrimaryKey(mockUser)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(mockUserTwoFactorAuthentication);
		when(mockUserTwoFactorAuthenticationPersistence.update(mockUserTwoFactorAuthentication)).thenReturn(mockUpdatedUserTwoFactorAuthentication);

		UserTwoFactorAuthentication result = userTwoFactorAuthenticationLocalServiceImpl.enableAndGetOrCreateTwoFactorAuthenticationForUser(mockUser, true);

		assertThat(result, sameInstance(mockUpdatedUserTwoFactorAuthentication));
	}

	@Test
	public void generateSecretKey_WhenNoError_ThenReturnsArandomSecretKey() {
		String expected = "expectedValue";
		when(mockTwoFactorUtils.generateRandomSecretKey()).thenReturn(expected);

		String result = userTwoFactorAuthenticationLocalServiceImpl.generateSecretKey();

		assertThat(result, equalTo(expected));
	}

	@Test(expected = PortalException.class)
	public void getQRBarCodeURL_WhenExcetpionRetrievingTheConfiguration_ThenThrowsPortalException() throws PortalException {
		String emailAddress = "emailAddressValue";
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenThrow(new PortalException());

		userTwoFactorAuthenticationLocalServiceImpl.getQRBarCodeURL(COMPANY_ID, emailAddress, SECRET_KEY);
	}

	@Test
	public void getQRBarCodeURL_WhenNoError_ThenReturnsTheGeneratedQRCodeURL() throws PortalException {
		String emailAddress = "emailAddressValue";
		String expected = "expectedValue";
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockTwoFactorUtils.getOTPUrl(mockAuthenticationTwoFactorCompanyConfiguration, emailAddress, SECRET_KEY)).thenReturn(expected);

		String result = userTwoFactorAuthenticationLocalServiceImpl.getQRBarCodeURL(COMPANY_ID, emailAddress, SECRET_KEY);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getUserTwoFactorAuthentication_WhenNoUserTwoFactorFound_ThenReturnsEmptyOptional() {
		when(mockTwoFactorUtils.getPrimaryKey(mockUser)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(null);

		Optional<UserTwoFactorAuthentication> result = userTwoFactorAuthenticationLocalServiceImpl.getUserTwoFactorAuthentication(mockUser);

		assertFalse(result.isPresent());
	}

	@Test
	public void getUserTwoFactorAuthentication_WhenUserTwoFactorFound_ThenReturnsOptionalWithTheValue() {
		when(mockTwoFactorUtils.getPrimaryKey(mockUser)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(mockUserTwoFactorAuthentication);

		Optional<UserTwoFactorAuthentication> result = userTwoFactorAuthenticationLocalServiceImpl.getUserTwoFactorAuthentication(mockUser);

		assertThat(result.get(), sameInstance(mockUserTwoFactorAuthentication));
	}

	@Test
	public void isTwoFactorAuthenticationEnforcedForUser_WhenNoErrorAndAnyCheckerReturnsTrue_ThenReturnsTrue() {
		List<EnforceTwoFactorAuthenticationChecker> checkers = Arrays.asList(mockEnforceTwoFactorAuthenticationChecker1, mockEnforceTwoFactorAuthenticationChecker2);

		when(mockEnforceTwoFactorAuthenticationCheckerRegistry.getEnforceTwoFactorAuthenticationCheckers()).thenReturn(checkers);
		when(mockEnforceTwoFactorAuthenticationChecker1.checkIfTwoFactorAuthenticationShouldBeEnforced(mockUser)).thenReturn(false);
		when(mockEnforceTwoFactorAuthenticationChecker2.checkIfTwoFactorAuthenticationShouldBeEnforced(mockUser)).thenReturn(true);

		boolean result = userTwoFactorAuthenticationLocalServiceImpl.isTwoFactorAuthenticationEnforcedForUser(mockUser);

		assertTrue(result);
	}

	@Test
	public void isTwoFactorAuthenticationEnforcedForUser_WhenNoErrorAndNoneOfTheCheckersReturnsTrue_ThenReturnsFalse() {
		List<EnforceTwoFactorAuthenticationChecker> checkers = Arrays.asList(mockEnforceTwoFactorAuthenticationChecker1, mockEnforceTwoFactorAuthenticationChecker2);

		when(mockEnforceTwoFactorAuthenticationCheckerRegistry.getEnforceTwoFactorAuthenticationCheckers()).thenReturn(checkers);
		when(mockEnforceTwoFactorAuthenticationChecker1.checkIfTwoFactorAuthenticationShouldBeEnforced(mockUser)).thenReturn(false);
		when(mockEnforceTwoFactorAuthenticationChecker2.checkIfTwoFactorAuthenticationShouldBeEnforced(mockUser)).thenReturn(false);

		boolean result = userTwoFactorAuthenticationLocalServiceImpl.isTwoFactorAuthenticationEnforcedForUser(mockUser);

		assertFalse(result);
	}

	@Test
	public void isValidTwoFactorAuthentication_WhenNoUserTwoFactorFound_ThenReturnsFalse() throws PortalException {
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.empty());

		boolean result = userTwoFactorAuthenticationLocalServiceImpl.isValidTwoFactorAuthentication(mockUser, CODE);

		assertFalse(result);
	}

	@Test
	public void isValidTwoFactorAuthentication_WhenUserTwoFactorFoundAndCodeDoesMatch_ThenReturnsTrue() throws PortalException {
		String decryptedValue = "myValue";
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockUserTwoFactorAuthentication.getSecretKey()).thenReturn(SECRET_KEY);
		when(mockUser.getUserUuid()).thenReturn(UUID);
		when(mockTwoFactorUtils.decrypt(SECRET_KEY, UUID)).thenReturn(Optional.of(decryptedValue));
		when(mockTwoFactorUtils.verifyUserOneTimeCode(decryptedValue, CODE)).thenReturn(true);

		boolean result = userTwoFactorAuthenticationLocalServiceImpl.isValidTwoFactorAuthentication(mockUser, CODE);

		assertTrue(result);
	}

	@Test
	public void isValidTwoFactorAuthentication_WhenUserTwoFactorFoundAndCodeDoesNotMatch_ThenReturnsFalse() throws PortalException {
		String decryptedValue = "myValue";
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockUserTwoFactorAuthentication.getSecretKey()).thenReturn(SECRET_KEY);
		when(mockUser.getUserUuid()).thenReturn(UUID);
		when(mockTwoFactorUtils.decrypt(SECRET_KEY, UUID)).thenReturn(Optional.of(decryptedValue));
		when(mockTwoFactorUtils.verifyUserOneTimeCode(decryptedValue, CODE)).thenReturn(false);

		boolean result = userTwoFactorAuthenticationLocalServiceImpl.isValidTwoFactorAuthentication(mockUser, CODE);

		assertFalse(result);
	}

	@Test
	public void isValidTwoFactorAuthentication_WhenUserTwoFactorFoundAndDecriptedValueNotFound_ThenReturnsFalse() throws PortalException {
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockUserTwoFactorAuthentication.getSecretKey()).thenReturn(SECRET_KEY);
		when(mockUser.getUserUuid()).thenReturn(UUID);
		when(mockTwoFactorUtils.decrypt(SECRET_KEY, UUID)).thenReturn(Optional.empty());

		boolean result = userTwoFactorAuthenticationLocalServiceImpl.isValidTwoFactorAuthentication(mockUser, CODE);

		assertFalse(result);
	}

	@Test
	public void resetUserTwoFactorAuthentication_WhenNoUserTwoFactorFound_ThenNoActionIsPerformed() {
		when(mockTwoFactorUtils.getPrimaryKey(COMPANY_ID, USER_ID)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(null);

		userTwoFactorAuthenticationLocalServiceImpl.resetUserTwoFactorAuthentication(COMPANY_ID, USER_ID);

		verifyZeroInteractions(mockUserTwoFactorAuthenticationLocalService);
	}

	@Test
	@Parameters({ "true", "false" })
	public void resetUserTwoFactorAuthentication_WhenUserTwoFactorFound_ThenUpdatesTheValueWithResetTrueAndAblankSecret(boolean enabled) {
		when(mockTwoFactorUtils.getPrimaryKey(COMPANY_ID, USER_ID)).thenReturn(mockUserTwoFactorAuthenticationPK);
		when(mockUserTwoFactorAuthenticationPersistence.fetchByPrimaryKey(mockUserTwoFactorAuthenticationPK)).thenReturn(mockUserTwoFactorAuthentication);
		when(mockUserTwoFactorAuthentication.getEnabled()).thenReturn(enabled);

		userTwoFactorAuthenticationLocalServiceImpl.resetUserTwoFactorAuthentication(COMPANY_ID, USER_ID);

		InOrder inOrder = inOrder(mockUserTwoFactorAuthentication, mockUserTwoFactorAuthenticationLocalService);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setEnabled(enabled);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setSecretKey(StringPool.BLANK);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setResetKey(true);
		inOrder.verify(mockUserTwoFactorAuthentication, times(1)).setModifiedDate(testDate);
		inOrder.verify(mockUserTwoFactorAuthenticationLocalService, times(1)).updateUserTwoFactorAuthentication(mockUserTwoFactorAuthentication);
	}

	@Test
	public void verifyAndEnableUserTwoFactorAuthentication_WhenAuthenticationCodeIsCorrect_ThenReturnsTrue() throws PortalException {
		String secretKey = "key";
		String authenticationCode = "code";

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockTwoFactorUtils.verifyUserOneTimeCode(secretKey, authenticationCode)).thenReturn(true);

		boolean result = userTwoFactorAuthenticationLocalServiceImpl.verifyAndEnableUserTwoFactorAuthentication(mockUser, authenticationCode, secretKey);

		verify(mockUserTwoFactorAuthenticationLocalService, times(1)).configureTwoFactorAuthenticationForUser(mockUser, secretKey);
		assertTrue(result);
	}

	@Test
	public void verifyAndEnableUserTwoFactorAuthentication_WhenAuthenticationCodeIsNotCorrect_ThenReturnsFalse() throws PortalException {
		String secretKey = "key";
		String authenticationCode = "code";

		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockTwoFactorUtils.getConfiguration(COMPANY_ID)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockTwoFactorUtils.verifyUserOneTimeCode(secretKey, authenticationCode)).thenReturn(false);

		boolean result = userTwoFactorAuthenticationLocalServiceImpl.verifyAndEnableUserTwoFactorAuthentication(mockUser, authenticationCode, secretKey);

		verifyZeroInteractions(mockUserTwoFactorAuthenticationLocalService);
		assertFalse(result);
	}
}
