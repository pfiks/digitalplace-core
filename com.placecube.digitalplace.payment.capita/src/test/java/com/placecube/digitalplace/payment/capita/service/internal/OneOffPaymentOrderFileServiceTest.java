package com.placecube.digitalplace.payment.capita.service.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Repository;
import com.liferay.portal.kernel.portletfilerepository.PortletFileRepository;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.FileUtil;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ FileUtil.class })
public class OneOffPaymentOrderFileServiceTest extends PowerMockito {

	private static final long FILE_ENTRY_ID = 344L;
	private static final String PAYMENT_REFERENCE = "1111";

	@Mock
	private FileEntry mockFileEntry;

	@Mock
	private Folder mockFolder;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private OneOffPaymentOrder mockOneOffPaymentOrder;

	@Mock
	private OneOffPaymentHelper mockOneOffPaymentHelper;

	@Mock
	private PortletFileRepository mockPortletFileRepository;

	@Mock
	private Repository mockRepository;

	@Mock
	private ServiceContext mockServiceContext;

	@InjectMocks
	private OneOffPaymentOrderFileService oneOffPaymentOrderFileService;

	@Before
	public void activeSetUp() {
		mockStatic(FileUtil.class);
		initMocks(this);
	}

	@Test
	public void loadPaymentTempFile_WhenPaymentReferenceIsNotNull_ThenReturnOptionalWithTheFile() throws Exception {
		OneOffPaymentOrder payment = new OneOffPaymentOrder();
		payment.setInternalReference(PAYMENT_REFERENCE);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
			oos.writeObject(payment);
			oos.flush();
			oos.close();
		}

		InputStream is = new ByteArrayInputStream(baos.toByteArray());

		when(mockOneOffPaymentHelper.getOrAddPaymentTempFilesFolder(mockServiceContext)).thenReturn(mockFolder);
		when(mockOneOffPaymentHelper.getPortletFileEntry(PAYMENT_REFERENCE, mockFolder)).thenReturn(mockFileEntry);
		when(mockFileEntry.getContentStream()).thenReturn(is);

		Optional<OneOffPaymentOrder> result = oneOffPaymentOrderFileService.loadPaymentTempFile(PAYMENT_REFERENCE, mockServiceContext);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), equalTo(payment));
	}

	@Test
	public void loadPaymentTempFile_WhenPaymentReferenceIsNull_ThenReturnEmptyOptional() {

		Optional<OneOffPaymentOrder> result = oneOffPaymentOrderFileService.loadPaymentTempFile(StringPool.BLANK, mockServiceContext);
		assertTrue(result.isEmpty());
	}

	@Test
	public void loadPaymentTempFile_WhenPortletFolderFailsToBeRetrievedOrCreated_ThenReturnsEmtpyOrder() throws PortalException {

		when(mockOneOffPaymentHelper.getOrAddPaymentTempFilesFolder(mockServiceContext)).thenThrow(new PortalException());

		Optional<OneOffPaymentOrder> result = oneOffPaymentOrderFileService.loadPaymentTempFile(PAYMENT_REFERENCE, mockServiceContext);
		assertTrue(result.isEmpty());
	}

	@Test
	public void uploadPaymentTempFile_WhenNoErrors_ThenAddsOrReplacesFileEntryForPaymentOrder() throws Exception {
		File file = File.createTempFile(PAYMENT_REFERENCE, "");

		OneOffPaymentOrder payment = new OneOffPaymentOrder();
		payment.setInternalReference(PAYMENT_REFERENCE);

		when(mockOneOffPaymentHelper.createPaymentTempFile(payment)).thenReturn(file);
		when(mockOneOffPaymentHelper.getOrAddPaymentTempFilesFolder(mockServiceContext)).thenReturn(mockFolder);
		when(mockOneOffPaymentHelper.addOrReplaceFileEntry(PAYMENT_REFERENCE, file, mockFolder, mockServiceContext)).thenReturn(mockFileEntry);
		when(mockFileEntry.getFileEntryId()).thenReturn(FILE_ENTRY_ID);

		oneOffPaymentOrderFileService.uploadPaymentTempFile(payment, mockServiceContext);

		verify(mockOneOffPaymentHelper, times(1)).addOrReplaceFileEntry(PAYMENT_REFERENCE, file, mockFolder, mockServiceContext);

		Files.delete(file.toPath());
	}

	@Test
	public void uploadPaymentTempFile_WhenNoErrors_ThenAddsOrReplacesFileEntryForPaymentOrderWithPaymentOrderContent() throws Exception {
		File file = File.createTempFile(PAYMENT_REFERENCE, "");

		OneOffPaymentOrder payment = new OneOffPaymentOrder();
		payment.setInternalReference(PAYMENT_REFERENCE);

		when(mockOneOffPaymentHelper.createPaymentTempFile(payment)).thenReturn(file);
		when(mockOneOffPaymentHelper.getOrAddPaymentTempFilesFolder(mockServiceContext)).thenReturn(mockFolder);
		when(mockOneOffPaymentHelper.addOrReplaceFileEntry(PAYMENT_REFERENCE, file, mockFolder, mockServiceContext)).thenReturn(mockFileEntry);
		when(mockFileEntry.getFileEntryId()).thenReturn(FILE_ENTRY_ID);

		oneOffPaymentOrderFileService.uploadPaymentTempFile(payment, mockServiceContext);

		byte[] data = Files.readAllBytes(file.toPath());

		ByteArrayOutputStream boas = new ByteArrayOutputStream();

		try (ObjectOutputStream ois = new ObjectOutputStream(boas)) {
			ois.writeObject(payment);
			assertThat(data, equalTo(boas.toByteArray()));
		}

		Files.delete(file.toPath());
	}

	@Test(expected = PortalException.class)
	public void uploadPaymentTempFile_WhenFileEntryCreationFails_ThenThrowsExceptionAndDeletesLocalFile() throws Exception {
		File file = File.createTempFile(PAYMENT_REFERENCE, "");

		OneOffPaymentOrder payment = new OneOffPaymentOrder();
		payment.setInternalReference(PAYMENT_REFERENCE);

		when(mockOneOffPaymentHelper.createPaymentTempFile(payment)).thenReturn(file);
		when(mockOneOffPaymentHelper.getOrAddPaymentTempFilesFolder(mockServiceContext)).thenReturn(mockFolder);
		when(mockOneOffPaymentHelper.addOrReplaceFileEntry(PAYMENT_REFERENCE, file, mockFolder, mockServiceContext)).thenThrow(new PortalException());

		oneOffPaymentOrderFileService.uploadPaymentTempFile(payment, mockServiceContext);

		verifyStatic(FileUtil.class);
		FileUtil.delete(file);
	}

}
