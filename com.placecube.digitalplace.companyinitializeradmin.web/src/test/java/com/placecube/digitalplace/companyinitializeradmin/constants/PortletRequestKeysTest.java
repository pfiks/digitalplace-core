package com.placecube.digitalplace.companyinitializeradmin.constants;

import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isPrivate;
import static java.lang.reflect.Modifier.isStatic;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.junit.Test;

public class PortletRequestKeysTest {

	@Test
	public void testClassIsAConstantsClass() throws Exception {
		Class<?> classToTest = PortletRequestKeys.class;

		assertThat(isFinal(classToTest.getModifiers()), equalTo(true));

		assertThat(classToTest.getDeclaredConstructors().length, equalTo(1));
		Constructor<?> constructor = classToTest.getDeclaredConstructor();
		assertThat(isPrivate(constructor.getModifiers()), equalTo(true));
		assertThat(constructor.isAccessible(), equalTo(false));

		constructor.setAccessible(true);
		constructor.newInstance();
		constructor.setAccessible(false);

		for (Method method : classToTest.getMethods()) {
			if (method.isAccessible()) {
				assertThat(isStatic(method.getModifiers()), equalTo(true));
				assertThat(method.getDeclaringClass(), equalTo(classToTest));
			}
		}
	}

	@Test
	public void testConstantValueForSelectedSiteInitializer() {
		assertThat(PortletRequestKeys.SELECTED_COMPANY_INITIALIZER, equalTo("selectedCompanyInitializer"));
	}

	@Test
	public void testConstantValueForSiteInitializers() {
		assertThat(PortletRequestKeys.COMPANY_INITIALIZERS, equalTo("companyInitializers"));
	}

}
