package com.placecube.digitalplace.form.service.impl.internal;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.form.model.FormDefinition;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ JournalArticleContext.class })
public class FormJournalArticleServiceTest extends PowerMockito {

	private static final String FOLDER_NAME = "test";

	private static final String CONTENT = "content";

	private static final Long CLASS_NAME_ID = 1l;

	private static final Long COMPANY_ID = 3l;

	private static final Long GROUP_ID = 2l;

	@InjectMocks
	private FormJournalArticleService formJournalArticleService;

	@Mock
	private FormDefinition mockFormDefinition;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private JournalArticleCreationService mockJournalArticleCreationService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private HttpURLConnection mockConnection;

	@Mock
	private JournalArticleContext mockJournalArticleContext;

	@Mock
	private ClassNameLocalService mockClassNameLocalService;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Group mockGroup;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(JournalArticleContext.class);
	}

	@Test
	public void addJournalFolder_WhenNoErrors_ThenReturnsJournalFolder() throws PortalException {

		when(mockFormDefinition.getJournalArticleFolderName()).thenReturn(FOLDER_NAME);
		when(mockJournalArticleCreationService.getOrCreateJournalFolder(FOLDER_NAME, mockServiceContext)).thenReturn(mockJournalFolder);

		JournalFolder result = formJournalArticleService.addJournalFolder(mockFormDefinition, mockServiceContext);

		assertThat(result, sameInstance(mockJournalFolder));
	}

	@Test
	public void addJournalArticles_WhenNoJournalArticleURLS_ThenDoesNotCreateJournalArticles() throws Exception {

		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(Collections.emptyList());

		formJournalArticleService.addJournalArticles(mockFormDefinition, mockJournalFolder, mockServiceContext);

		verifyZeroInteractions(mockJournalArticleCreationService);
	}

	@Test
	public void addJournalArticles_WhenHasJournalArticleURLs_ThenCreateJournalArticles() throws Exception {

		List<URL> journalArticleURLs = new ArrayList<URL>();
		URL journalArticle = new URL(null, "file://resources/webcontent/journal article.xml", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		ByteArrayInputStream is = new ByteArrayInputStream(CONTENT.getBytes("UTF-8"));
		doReturn(is).when(mockConnection).getInputStream();

		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(journalArticleURLs);
		when(JournalArticleContext.init("JOURNAL-ARTICLE", "journal article", CONTENT)).thenReturn(mockJournalArticleContext);

		formJournalArticleService.addJournalArticles(mockFormDefinition, mockJournalFolder, mockServiceContext);

		verify(mockJournalArticleCreationService).getOrCreateArticle(mockJournalArticleContext, mockServiceContext);
	}

	@Test
	public void addJournalArticles_WhenJournalArticleURLHasSubFolderAndIsNotValidStructure_ThenJournalArticleIsNotCreated() throws Exception {

		List<URL> journalArticleURLs = new ArrayList<URL>();
		URL journalArticle = new URL(null, "file://resources/webcontent/EMAIL/journal article.xml", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		ByteArrayInputStream is = new ByteArrayInputStream(CONTENT.getBytes("UTF-8"));
		doReturn(is).when(mockConnection).getInputStream();

		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(journalArticleURLs);
		when(JournalArticleContext.init("JOURNAL-ARTICLE", "journal article", CONTENT)).thenReturn(mockJournalArticleContext);

		when(mockClassNameLocalService.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, CLASS_NAME_ID, "EMAIL")).thenReturn(null);

		formJournalArticleService.addJournalArticles(mockFormDefinition, mockJournalFolder, mockServiceContext);

		verifyZeroInteractions(mockJournalArticleCreationService);
	}

	@Test
	public void addJournalArticles_WhenJournalArticleURLHasSubFolderAndIsValidStructure_ThenJournalArticleCreatedWithStructure() throws Exception {

		List<URL> journalArticleURLs = new ArrayList<URL>();
		URL journalArticle = new URL(null, "file://resources/webcontent/EMAIL/journal article.xml", createURLStreamHandler());
		journalArticleURLs.add(journalArticle);

		ByteArrayInputStream is = new ByteArrayInputStream(CONTENT.getBytes("UTF-8"));
		doReturn(is).when(mockConnection).getInputStream();

		when(mockFormDefinition.getUrlsToJournalArticles()).thenReturn(journalArticleURLs);
		when(JournalArticleContext.init("JOURNAL-ARTICLE", "journal article", CONTENT)).thenReturn(mockJournalArticleContext);

		when(mockClassNameLocalService.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockDDMStructureLocalService.fetchStructure(GROUP_ID, CLASS_NAME_ID, "EMAIL")).thenReturn(mockDDMStructure);

		formJournalArticleService.addJournalArticles(mockFormDefinition, mockJournalFolder, mockServiceContext);

		InOrder inOrder = Mockito.inOrder(mockJournalArticleContext, mockJournalArticleCreationService);
		inOrder.verify(mockJournalArticleContext).setDDMStructure(mockDDMStructure);
		inOrder.verify(mockJournalArticleCreationService).getOrCreateArticle(mockJournalArticleContext, mockServiceContext);
	}

	private URLStreamHandler createURLStreamHandler() throws IOException {

		URLStreamHandler stubURLStreamHandler = new URLStreamHandler() {

			@Override
			protected URLConnection openConnection(URL u) throws IOException {
				return mockConnection;
			}
		};

		return stubURLStreamHandler;
	}

}