package com.placecube.digitalplace.appointment.booking.digitalplaceconnector;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.appointment.booking.connector.AppointmentBookingConnector;
import com.placecube.digitalplace.appointment.booking.constants.AppointmentSlotType;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService;

@Component(immediate = true, service = AppointmentBookingConnector.class)
public class DigitalPlaceAppointmentBookingConnector implements AppointmentBookingConnector {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceAppointmentBookingConnector.class);

	@Reference
	private AppointmentEntryLocalService appointmentEntryLocalService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DigitalPlaceAppointmentBookingConnectorService digitalPlaceAppointmentBookingConnectorService;

	@Override
	public List<String> bookAppointment(long companyId, String serviceId, AppointmentBookingEntry appointmentBookingEntry) {
		String generatedId = appointmentEntryLocalService.generateExternalAppointmentId(companyId, serviceId, DigitalPlaceAppointmentBookingConnector.class.getName());
		return Collections.singletonList(generatedId);
	}

	@Override
	public List<String> cancelAppointment(long companyId, String serviceId, String appointmentId) {
		return Collections.singletonList(appointmentId);
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, AppointmentContext appointmentContext) {
		List<AppointmentEntry> appointmentEntries = digitalPlaceAppointmentBookingConnectorService.searchAppointmentEntries(companyId, serviceId, appointmentContext);
		return digitalPlaceAppointmentBookingConnectorService.convertEntries(appointmentEntries);
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, Date startDate, Date endDate, TimeZone timeZone) {
		List<AppointmentEntry> appointmentEntries = digitalPlaceAppointmentBookingConnectorService.getAppointmentEntriesWithMatchingDates(companyId, serviceId, startDate, endDate);
		return digitalPlaceAppointmentBookingConnectorService.convertEntries(appointmentEntries);
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, String appointmentId) {
		List<AppointmentEntry> appointmentEntries = digitalPlaceAppointmentBookingConnectorService.getAppointmentEntriesById(companyId, serviceId, appointmentId);
		return digitalPlaceAppointmentBookingConnectorService.convertEntries(appointmentEntries);
	}

	@Override
	public List<AppointmentBookingSlot> getAppointmentSlots(long companyId, String serviceId, AppointmentContext appointmentContext) {
		List<AppointmentBookingSlot> exclusions = digitalPlaceAppointmentBookingConnectorService
				.convertSlots(digitalPlaceAppointmentBookingConnectorService.getAppointmentSlots(companyId, AppointmentSlotType.EXCLUSION.name(), serviceId, appointmentContext), appointmentContext);

		List<AppointmentBookingSlot> configurations = digitalPlaceAppointmentBookingConnectorService.convertSlots(
				digitalPlaceAppointmentBookingConnectorService.getAppointmentSlots(companyId, AppointmentSlotType.CONFIGURATION.name(), serviceId, appointmentContext), appointmentContext);

		List<AppointmentBookingSlot> results = digitalPlaceAppointmentBookingConnectorService.getSlotsAvailable(exclusions, configurations);

		for (AppointmentBookingSlot availableAppointmentSlot : results) {
			availableAppointmentSlot.setAppointmentsAlreadyBooked(getAppointments(companyId, serviceId, availableAppointmentSlot.getStartDate(), availableAppointmentSlot.getEndDate(), null));
		}

		Collections.sort(results, (o1, o2) -> o1.getStartDate().compareTo(o2.getStartDate()));
		return results;
	}

	@Override
	public List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int numberOfDatesToReturn, int dayOfWeek, TimeZone timeZone,
			int onlyDatesWithLessThanXappoinments) {
		throw new UnsupportedOperationException("Unsupported method");
	}

	@Override
	public List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int dayOfWeek, TimeZone timeZone) {
		throw new UnsupportedOperationException("Unsupported method");
	}

	@Override
	public boolean isEnabled(long companyId) {
		try {
			return configurationProvider.getCompanyConfiguration(DigitalPlaceAppointmentBookingConnectorCompanyConfiguration.class, companyId).enabled();
		} catch (ConfigurationException e) {
			LOG.warn("Unable to check if connector is enabled ", e);
			return false;
		}
	}

}