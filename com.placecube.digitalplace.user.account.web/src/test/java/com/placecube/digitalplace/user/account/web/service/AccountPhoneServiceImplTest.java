package com.placecube.digitalplace.user.account.web.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.ListTypeConstants;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ListTypeLocalService;
import com.liferay.portal.kernel.service.PhoneLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.user.account.constants.PhoneType;
import com.placecube.digitalplace.user.account.model.AccountContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest(PropsUtil.class)
public class AccountPhoneServiceImplTest extends PowerMockito {

	@InjectMocks
	private AccountPhoneServiceImpl accountPhoneServiceImpl;

	private final String BUSINESS_PHONE_VALUE = "12345";

	private final String HOME_PHONE_VALUE = "11121314";

	private final String MOBILE_PHONE_VALUE = "678910";

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private ListType mockListType1;

	@Mock
	private ListType mockListType2;

	@Mock
	private ListTypeLocalService mockListTypeLocalService;

	@Mock
	private PhoneLocalService mockPhoneLocalService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class);
	}

	@Test(expected = PortalException.class)
	public void addPhone_WhenExceptionAddingPhone_ThenThrowsPortalException() throws PortalException {
		long userId = 1;
		long contactId = 2;
		long listTypeId = 3;
		List<ListType> listTypes = new ArrayList<>();
		listTypes.add(mockListType1);
		listTypes.add(mockListType2);
		when(mockListType1.getName()).thenReturn("someOtherName");
		when(mockListType2.getName()).thenReturn(PhoneType.MOBILE.getType());
		long companyId = 123;
		when(mockServiceContext.getCompanyId()).thenReturn(companyId );
		when(mockListTypeLocalService.getListTypes(companyId, ListTypeConstants.CONTACT_PHONE)).thenReturn(listTypes);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getContactId()).thenReturn(contactId);
		when(mockListType2.getListTypeId()).thenReturn(listTypeId);
		when(mockPhoneLocalService.addPhone(userId, Contact.class.getName(), contactId, "numberValue", StringPool.BLANK, listTypeId, false, mockServiceContext)).thenThrow(new PortalException());

		accountPhoneServiceImpl.addPhone(mockUser, "numberValue", PhoneType.MOBILE, mockServiceContext);

	}

	@Test
	public void addPhone_WhenNoError_ThenAddsAPhoneForTheSpecifiedType() throws PortalException {
		long userId = 1;
		long contactId = 2;
		long listTypeId = 3;
		List<ListType> listTypes = new ArrayList<>();
		listTypes.add(mockListType1);
		listTypes.add(mockListType2);
		when(mockListType1.getName()).thenReturn("someOtherName");
		when(mockListType2.getName()).thenReturn(PhoneType.MOBILE.getType());
		long companyId = 123;
		when(mockServiceContext.getCompanyId()).thenReturn(companyId );
		when(mockListTypeLocalService.getListTypes(companyId, ListTypeConstants.CONTACT_PHONE)).thenReturn(listTypes);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getContactId()).thenReturn(contactId);
		when(mockListType2.getListTypeId()).thenReturn(listTypeId);

		accountPhoneServiceImpl.addPhone(mockUser, "numberValue", PhoneType.MOBILE, mockServiceContext);

		verify(mockPhoneLocalService, times(1)).addPhone(userId, Contact.class.getName(), contactId, "numberValue", StringPool.BLANK, listTypeId, false, mockServiceContext);
	}

	@Test
	public void addPhone_WhenNoErrorAndPhoneTypeExists_ThenUpdateFirstOneOfSpecifiedType() throws PortalException {
		long userId = 1;
		long contactId = 2;
		long listTypeId = 3;
		String phoneNumber = "numberValue";
		List<ListType> listTypes = new ArrayList<>();
		listTypes.add(mockListType1);
		listTypes.add(mockListType2);
		when(mockListType1.getName()).thenReturn("someOtherName");
		when(mockListType2.getName()).thenReturn(PhoneType.MOBILE.getType());
		long companyId = 123;
		when(mockServiceContext.getCompanyId()).thenReturn(companyId );
		when(mockListTypeLocalService.getListTypes(companyId, ListTypeConstants.CONTACT_PHONE)).thenReturn(listTypes);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getContactId()).thenReturn(contactId);
		when(mockListType2.getListTypeId()).thenReturn(listTypeId);

		List<Phone> phones = new LinkedList<>();
		Phone mockExistsingMobilePhone = mock(Phone.class);
		ListType mockMobileListType = mock(ListType.class);
		when(mockMobileListType.getName()).thenReturn(PhoneType.MOBILE.getType());
		when(mockExistsingMobilePhone.getListType()).thenReturn(mockMobileListType);
		phones.add(mockExistsingMobilePhone);
		when(mockUser.getPhones()).thenReturn(phones);

		accountPhoneServiceImpl.addPhone(mockUser, phoneNumber, PhoneType.MOBILE, mockServiceContext);

		InOrder inOrder = inOrder(mockExistsingMobilePhone, mockPhoneLocalService);
		inOrder.verify(mockExistsingMobilePhone, times(1)).setNumber(phoneNumber);
		inOrder.verify(mockPhoneLocalService, times(1)).updatePhone(mockExistsingMobilePhone);
	}

	@Test(expected = PortalException.class)
	public void addPhone_WhenNoListTypeFound_ThenThrowsPortalException() throws PortalException {
		List<ListType> listTypes = new ArrayList<>();
		listTypes.add(mockListType1);
		when(mockListType1.getName()).thenReturn("someOtherName");
		long companyId = 123;
		when(mockServiceContext.getCompanyId()).thenReturn(companyId );
		when(mockListTypeLocalService.getListTypes(companyId, ListTypeConstants.CONTACT_PHONE)).thenReturn(listTypes);


		accountPhoneServiceImpl.addPhone(mockUser, "numberValue", PhoneType.MOBILE, mockServiceContext);
	}

	@Test
	public void addPhone_WhenNoNumberGiven_ThenDoesNotAddPhone() throws PortalException {
		long userId = 1;
		long contactId = 2;
		long listTypeId = 3;
		List<ListType> listTypes = new ArrayList<>();
		listTypes.add(mockListType1);
		listTypes.add(mockListType2);
		when(mockListType1.getName()).thenReturn("someOtherName");
		when(mockListType2.getName()).thenReturn(PhoneType.MOBILE.getType());
		long companyId = 123;
		when(mockServiceContext.getCompanyId()).thenReturn(companyId );
		when(mockListTypeLocalService.getListTypes(companyId, ListTypeConstants.CONTACT_PHONE)).thenReturn(listTypes);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockUser.getContactId()).thenReturn(contactId);
		when(mockListType2.getListTypeId()).thenReturn(listTypeId);

		accountPhoneServiceImpl.addPhone(mockUser, null, PhoneType.MOBILE, mockServiceContext);

		verifyZeroInteractions(mockPhoneLocalService);
	}

	@Test
	public void getFirstPhoneOfType_WhenNoPhoneNumberOfTheSameTypeExist_ThenReturnEmptyOptional() throws PortalException {
		List<Phone> phones = new LinkedList<>();
		ListType mockBusinessListType = mock(ListType.class);
		Phone mockBusinessPhone1 = mock(Phone.class);
		when(mockBusinessListType.getName()).thenReturn(PhoneType.BUSINESS.getType());
		when(mockBusinessPhone1.getListType()).thenReturn(mockBusinessListType);
		phones.add(mockBusinessPhone1);

		Optional<Phone> result = accountPhoneServiceImpl.getFirstPhoneOfType(phones, PhoneType.MOBILE);

		assertEquals(false, result.isPresent());
	}

	@Test
	public void getFirstPhoneOfType_WhenNoPhonesPresentInList_ThenEmptyOptionalReturned() throws PortalException {
		Optional<Phone> result = accountPhoneServiceImpl.getFirstPhoneOfType(null, PhoneType.BUSINESS);

		assertEquals(false, result.isPresent());
	}

	@Test
	public void getFirstPhoneOfType_WhenPhonesOfMulipleTypesExist_ThenReturnCorrectType() throws PortalException {
		List<Phone> phones = new LinkedList<>();
		mockDifferentPhoneTypes(phones);

		Optional<Phone> result = accountPhoneServiceImpl.getFirstPhoneOfType(phones, PhoneType.MOBILE);

		assertThat(result.get().getListType().getName(), equalTo(PhoneType.MOBILE.getType()));
	}

	@Test
	public void getFirstPhoneOfType_WhenTwoPhoneNumbersOfTheSameTypeExist_ThenReturnTheFirstOne() throws PortalException {
		List<Phone> phones = new LinkedList<>();
		ListType mockBusinessListType = mock(ListType.class);
		when(mockBusinessListType.getName()).thenReturn(PhoneType.BUSINESS.getType());

		Phone mockBusinessPhone1 = mock(Phone.class);
		when(mockBusinessPhone1.getListType()).thenReturn(mockBusinessListType);
		phones.add(mockBusinessPhone1);

		Phone mockBusinessPhone2 = mock(Phone.class);
		when(mockBusinessPhone2.getListType()).thenReturn(mockBusinessListType);
		phones.add(mockBusinessPhone2);

		Optional<Phone> result = accountPhoneServiceImpl.getFirstPhoneOfType(phones, PhoneType.BUSINESS);

		assertThat(result.get(), equalTo(mockBusinessPhone1));
	}

	@Test
	public void populateContextWithExistingPhones_WhenNoErrors_ThenSetPhoneNumbersInContext() throws PortalException {
		List<Phone> phones = new LinkedList<>();
		mockDifferentPhoneTypes(phones);
		when(mockUser.getPhones()).thenReturn(phones);

		accountPhoneServiceImpl.populateContextWithExistingPhones(mockAccountContext, mockUser);

		verify(mockAccountContext, times(1)).setBusinessPhoneNumber(BUSINESS_PHONE_VALUE);
		verify(mockAccountContext, times(1)).setHomePhoneNumber(HOME_PHONE_VALUE);
		verify(mockAccountContext, times(1)).setMobilePhoneNumber(MOBILE_PHONE_VALUE);
	}

	private void mockDifferentPhoneTypes(List<Phone> phones) throws PortalException {

		Phone mockBusinessPhone = mock(Phone.class);
		ListType mockBusinessListType = mock(ListType.class);
		when(mockBusinessListType.getName()).thenReturn(PhoneType.BUSINESS.getType());
		when(mockBusinessPhone.getListType()).thenReturn(mockBusinessListType);
		when(mockBusinessPhone.getNumber()).thenReturn(BUSINESS_PHONE_VALUE);
		phones.add(mockBusinessPhone);

		Phone mockMobilePhone = mock(Phone.class);
		ListType mockMobileListType = mock(ListType.class);
		when(mockMobileListType.getName()).thenReturn(PhoneType.MOBILE.getType());
		when(mockMobilePhone.getListType()).thenReturn(mockMobileListType);
		when(mockMobilePhone.getNumber()).thenReturn(MOBILE_PHONE_VALUE);
		phones.add(mockMobilePhone);

		Phone mockHomePhone = mock(Phone.class);
		ListType mockHomeListType = mock(ListType.class);
		when(mockHomeListType.getName()).thenReturn(PhoneType.PERSONAL.getType());
		when(mockHomePhone.getListType()).thenReturn(mockHomeListType);
		when(mockHomePhone.getNumber()).thenReturn(HOME_PHONE_VALUE);
		phones.add(mockHomePhone);
	}
}
