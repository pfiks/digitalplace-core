package com.placecube.digitalplace.user.account.web.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.user.account.constants.WebContentArticles;
import com.placecube.journal.service.JournalArticleCreationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
public class AccountSetupServiceImplTest extends PowerMockito {

	@InjectMocks
	private AccountSetupServiceImpl accountSetupServiceImpl;

	@Mock
	private Group mockGroup;

	@Mock
	private JournalArticleCreationService mockJournalArticleCreationService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private ServiceContext mockServiceContext;

	@Test(expected = PortalException.class)
	public void addArticle_WhenExceptionCreatingArticle_ThenThrowsPortalException() throws Exception {
		String articleContent = "articleContent";
		WebContentArticles webContent = WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITH_EMAIL_VERIFICATION;
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/user/account/web/dependencies/webcontent/" + webContent.getArticleId() + ".xml")).thenReturn(articleContent);
		when(mockJournalArticleCreationService.getOrCreateBasicWebContentArticle(webContent.getArticleId(), webContent.getArticleTitle(), articleContent, mockJournalFolder, mockServiceContext))
		.thenThrow(new PortalException());

		accountSetupServiceImpl.addArticle(webContent, mockJournalFolder, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void addArticle_WhenExceptionReadingArticleContent_ThenThrowsPortalException() throws Exception {
		WebContentArticles webContent = WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITH_EMAIL_VERIFICATION;
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/user/account/web/dependencies/webcontent/" + webContent.getArticleId() + ".xml")).thenThrow(new IOException());

		accountSetupServiceImpl.addArticle(webContent, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void addArticle_WhenNoError_ThenCreatesTheArticle() throws Exception {
		String articleContent = "articleContent";
		WebContentArticles webContent = WebContentArticles.USER_ACCOUNT_CONFIRMATION_WITH_EMAIL_VERIFICATION;
		when(StringUtil.read(getClass().getClassLoader(), "com/placecube/digitalplace/user/account/web/dependencies/webcontent/" + webContent.getArticleId() + ".xml")).thenReturn(articleContent);

		accountSetupServiceImpl.addArticle(webContent, mockJournalFolder, mockServiceContext);

		verify(mockJournalArticleCreationService, times(1)).getOrCreateBasicWebContentArticle(webContent.getArticleId(), webContent.getArticleTitle(), articleContent, mockJournalFolder,
				mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void addFolder_WhenExceptionCreatingTheFolder_ThenThrowsPortalException() throws PortalException {
		when(mockJournalArticleCreationService.getOrCreateJournalFolder("Create Account Portlet", mockServiceContext)).thenThrow(new PortalException());

		accountSetupServiceImpl.addFolder(mockServiceContext);
	}

	@Test
	public void addFolder_WhenNoError_ThenReturnsTheFolder() throws PortalException {
		when(mockJournalArticleCreationService.getOrCreateJournalFolder("Create Account Portlet", mockServiceContext)).thenReturn(mockJournalFolder);

		JournalFolder result = accountSetupServiceImpl.addFolder(mockServiceContext);

		assertThat(result, sameInstance(mockJournalFolder));
	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnsServiceContextWithCompanyIdConfigured() {
		Long expected = 11l;
		when(mockGroup.getCompanyId()).thenReturn(expected);

		ServiceContext result = accountSetupServiceImpl.getServiceContext(mockGroup);

		assertThat(result.getCompanyId(), equalTo(expected));
	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnsServiceContextWithGroupIdConfigured() {
		Long expected = 11l;
		when(mockGroup.getGroupId()).thenReturn(expected);

		ServiceContext result = accountSetupServiceImpl.getServiceContext(mockGroup);

		assertThat(result.getScopeGroupId(), equalTo(expected));
	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnsServiceContextWithLanguageIdConfigured() {
		String expected = "expectedValue";
		when(mockGroup.getDefaultLanguageId()).thenReturn(expected);

		ServiceContext result = accountSetupServiceImpl.getServiceContext(mockGroup);

		assertThat(result.getLanguageId(), equalTo(expected));
	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnsServiceContextWithUserIdConfigured() {
		Long expected = 11l;
		when(mockGroup.getCreatorUserId()).thenReturn(expected);

		ServiceContext result = accountSetupServiceImpl.getServiceContext(mockGroup);

		assertThat(result.getUserId(), equalTo(expected));
	}

	@Before
	public void setUp() {
		mockStatic(StringUtil.class);
	}
}
