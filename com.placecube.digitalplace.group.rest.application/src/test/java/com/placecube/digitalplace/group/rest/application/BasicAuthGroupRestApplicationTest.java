package com.placecube.digitalplace.group.rest.application;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.placecube.digitalplace.group.rest.application.endpoints.BasicAuthGroupContentRestEndpoint;
import com.placecube.digitalplace.group.rest.application.exceptions.handlers.GeneralExceptionHandler;
import com.placecube.digitalplace.group.rest.application.exceptions.handlers.GroupRestSecurityExceptionHandler;
import com.placecube.digitalplace.group.rest.application.filters.BasicAuthCheckerFilter;

public class BasicAuthGroupRestApplicationTest extends PowerMockito {

	@InjectMocks
	private BasicAuthGroupRestApplication basicAuthGroupRestApplication;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void getSingletons_WhenNoError_ThenReturnsASetWithAllTheRequiredSingletons() {
		Set<Object> singletons = basicAuthGroupRestApplication.getSingletons();

		Set<String> singletonClassNames = new LinkedHashSet<>();
		for (Object classToChange : singletons) {
			singletonClassNames.add(classToChange.getClass().getName());
		}

		assertThat(singletonClassNames,
				contains(BasicAuthCheckerFilter.class.getName(), JacksonJsonProvider.class.getName(), GroupRestSecurityExceptionHandler.class.getName(), GeneralExceptionHandler.class.getName(),
						BasicAuthGroupContentRestEndpoint.class.getName()));
	}

}
