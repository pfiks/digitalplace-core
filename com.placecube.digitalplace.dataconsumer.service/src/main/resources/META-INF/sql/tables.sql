create table Placecube_DataConsumer_DataConsumer (
	uuid_ VARCHAR(75) null,
	dataConsumerId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	ddmFormInstanceId LONG,
	ddmDataProviderInstanceId LONG,
	sourceDataProviderId LONG,
	method VARCHAR(75) null,
	mapping STRING null,
	headers STRING null,
	chain INTEGER,
	body STRING null
);