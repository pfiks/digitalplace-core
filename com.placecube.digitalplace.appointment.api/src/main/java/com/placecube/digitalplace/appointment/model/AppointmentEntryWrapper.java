/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AppointmentEntry}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentEntry
 * @generated
 */
public class AppointmentEntryWrapper
	extends BaseModelWrapper<AppointmentEntry>
	implements AppointmentEntry, ModelWrapper<AppointmentEntry> {

	public AppointmentEntryWrapper(AppointmentEntry appointmentEntry) {
		super(appointmentEntry);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("appointmentEntryId", getAppointmentEntryId());
		attributes.put("companyId", getCompanyId());
		attributes.put("entryClassPK", getEntryClassPK());
		attributes.put("entryClassNameId", getEntryClassNameId());
		attributes.put("appointmentId", getAppointmentId());
		attributes.put("serviceId", getServiceId());
		attributes.put("connectorClassName", getConnectorClassName());
		attributes.put("createDate", getCreateDate());
		attributes.put("appointmentStartDate", getAppointmentStartDate());
		attributes.put("appointmentEndDate", getAppointmentEndDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long appointmentEntryId = (Long)attributes.get("appointmentEntryId");

		if (appointmentEntryId != null) {
			setAppointmentEntryId(appointmentEntryId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long entryClassPK = (Long)attributes.get("entryClassPK");

		if (entryClassPK != null) {
			setEntryClassPK(entryClassPK);
		}

		Long entryClassNameId = (Long)attributes.get("entryClassNameId");

		if (entryClassNameId != null) {
			setEntryClassNameId(entryClassNameId);
		}

		String appointmentId = (String)attributes.get("appointmentId");

		if (appointmentId != null) {
			setAppointmentId(appointmentId);
		}

		String serviceId = (String)attributes.get("serviceId");

		if (serviceId != null) {
			setServiceId(serviceId);
		}

		String connectorClassName = (String)attributes.get(
			"connectorClassName");

		if (connectorClassName != null) {
			setConnectorClassName(connectorClassName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date appointmentStartDate = (Date)attributes.get(
			"appointmentStartDate");

		if (appointmentStartDate != null) {
			setAppointmentStartDate(appointmentStartDate);
		}

		Date appointmentEndDate = (Date)attributes.get("appointmentEndDate");

		if (appointmentEndDate != null) {
			setAppointmentEndDate(appointmentEndDate);
		}
	}

	@Override
	public AppointmentEntry cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the appointment end date of this appointment entry.
	 *
	 * @return the appointment end date of this appointment entry
	 */
	@Override
	public Date getAppointmentEndDate() {
		return model.getAppointmentEndDate();
	}

	/**
	 * Returns the appointment entry ID of this appointment entry.
	 *
	 * @return the appointment entry ID of this appointment entry
	 */
	@Override
	public long getAppointmentEntryId() {
		return model.getAppointmentEntryId();
	}

	/**
	 * Returns the appointment ID of this appointment entry.
	 *
	 * @return the appointment ID of this appointment entry
	 */
	@Override
	public String getAppointmentId() {
		return model.getAppointmentId();
	}

	/**
	 * Returns the appointment start date of this appointment entry.
	 *
	 * @return the appointment start date of this appointment entry
	 */
	@Override
	public Date getAppointmentStartDate() {
		return model.getAppointmentStartDate();
	}

	/**
	 * Returns the company ID of this appointment entry.
	 *
	 * @return the company ID of this appointment entry
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the connector class name of this appointment entry.
	 *
	 * @return the connector class name of this appointment entry
	 */
	@Override
	public String getConnectorClassName() {
		return model.getConnectorClassName();
	}

	/**
	 * Returns the create date of this appointment entry.
	 *
	 * @return the create date of this appointment entry
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the entry class name ID of this appointment entry.
	 *
	 * @return the entry class name ID of this appointment entry
	 */
	@Override
	public long getEntryClassNameId() {
		return model.getEntryClassNameId();
	}

	/**
	 * Returns the entry class pk of this appointment entry.
	 *
	 * @return the entry class pk of this appointment entry
	 */
	@Override
	public long getEntryClassPK() {
		return model.getEntryClassPK();
	}

	/**
	 * Returns the primary key of this appointment entry.
	 *
	 * @return the primary key of this appointment entry
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the service ID of this appointment entry.
	 *
	 * @return the service ID of this appointment entry
	 */
	@Override
	public String getServiceId() {
		return model.getServiceId();
	}

	/**
	 * Returns the uuid of this appointment entry.
	 *
	 * @return the uuid of this appointment entry
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the appointment end date of this appointment entry.
	 *
	 * @param appointmentEndDate the appointment end date of this appointment entry
	 */
	@Override
	public void setAppointmentEndDate(Date appointmentEndDate) {
		model.setAppointmentEndDate(appointmentEndDate);
	}

	/**
	 * Sets the appointment entry ID of this appointment entry.
	 *
	 * @param appointmentEntryId the appointment entry ID of this appointment entry
	 */
	@Override
	public void setAppointmentEntryId(long appointmentEntryId) {
		model.setAppointmentEntryId(appointmentEntryId);
	}

	/**
	 * Sets the appointment ID of this appointment entry.
	 *
	 * @param appointmentId the appointment ID of this appointment entry
	 */
	@Override
	public void setAppointmentId(String appointmentId) {
		model.setAppointmentId(appointmentId);
	}

	/**
	 * Sets the appointment start date of this appointment entry.
	 *
	 * @param appointmentStartDate the appointment start date of this appointment entry
	 */
	@Override
	public void setAppointmentStartDate(Date appointmentStartDate) {
		model.setAppointmentStartDate(appointmentStartDate);
	}

	/**
	 * Sets the company ID of this appointment entry.
	 *
	 * @param companyId the company ID of this appointment entry
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the connector class name of this appointment entry.
	 *
	 * @param connectorClassName the connector class name of this appointment entry
	 */
	@Override
	public void setConnectorClassName(String connectorClassName) {
		model.setConnectorClassName(connectorClassName);
	}

	/**
	 * Sets the create date of this appointment entry.
	 *
	 * @param createDate the create date of this appointment entry
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the entry class name ID of this appointment entry.
	 *
	 * @param entryClassNameId the entry class name ID of this appointment entry
	 */
	@Override
	public void setEntryClassNameId(long entryClassNameId) {
		model.setEntryClassNameId(entryClassNameId);
	}

	/**
	 * Sets the entry class pk of this appointment entry.
	 *
	 * @param entryClassPK the entry class pk of this appointment entry
	 */
	@Override
	public void setEntryClassPK(long entryClassPK) {
		model.setEntryClassPK(entryClassPK);
	}

	/**
	 * Sets the primary key of this appointment entry.
	 *
	 * @param primaryKey the primary key of this appointment entry
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the service ID of this appointment entry.
	 *
	 * @param serviceId the service ID of this appointment entry
	 */
	@Override
	public void setServiceId(String serviceId) {
		model.setServiceId(serviceId);
	}

	/**
	 * Sets the uuid of this appointment entry.
	 *
	 * @param uuid the uuid of this appointment entry
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	protected AppointmentEntryWrapper wrap(AppointmentEntry appointmentEntry) {
		return new AppointmentEntryWrapper(appointmentEntry);
	}

}