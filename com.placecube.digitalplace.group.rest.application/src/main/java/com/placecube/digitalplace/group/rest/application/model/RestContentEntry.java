package com.placecube.digitalplace.group.rest.application.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class RestContentEntry {

	private long id;
	private String title;
	private String description;
	private String viewURL;
	private List<RestContentEntryField> values;
	private List<RestContentEntry> tags;
	private List<RestContentEntry> categories;

	public void setValues(List<RestContentEntryField> values) {
		this.values = values;
	}

	public void setTags(List<RestContentEntry> tags) {
		this.tags = tags;
	}

	public void setCategories(List<RestContentEntry> categories) {
		this.categories = categories;
	}

	public static RestContentEntry init(long id, String title) {
		return new RestContentEntry(id, title);
	}

	public static RestContentEntry init(long id, String title, String description, String viewURL) {
		RestContentEntry result = new RestContentEntry(id, title);
		result.description = description;
		result.viewURL = viewURL;
		return result;
	}

	public RestContentEntry(long id) {
		this.id = id;
	}

	public RestContentEntry(long id, String title) {
		this.id = id;
		this.title = title;
	}

	public RestContentEntry(long id, String title, List<RestContentEntry> categories) {
		this.id = id;
		this.title = title;
		this.categories = categories;
	}

	public RestContentEntry(String title, List<RestContentEntryField> values) {
		this.title = title;
		this.values = values;
	}

	public List<RestContentEntry> getCategories() {
		return categories;
	}

	public String getDescription() {
		return description;
	}

	public long getId() {
		return id;
	}

	public List<RestContentEntry> getTags() {
		return tags;
	}

	public String getTitle() {
		return title;
	}

	public List<RestContentEntryField> getValues() {
		return values;
	}

	public String getViewURL() {
		return viewURL;
	}

}
