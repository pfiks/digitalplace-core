package com.placecube.digitalplace.appointment.admin.web.service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.ParamUtil;
import com.pfiks.common.time.DateTimeSelectionService;
import com.pfiks.common.time.model.DateTimeSelection;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;

@Component(immediate = true, service = AppointmentsManagerUtils.class)
public class AppointmentsManagerUtils {

	private static final String END_DATE_AM_OR_PM = "endAMorPM";
	private static final String END_DATE_DAY = "endDay";
	private static final String END_DATE_HOUR = "endHour";
	private static final String END_DATE_MINUTE = "endMinute";
	private static final String END_DATE_MONTH = "endMonth";
	private static final String END_DATE_YEAR = "endYear";
	private static final String START_DATE_AM_OR_PM = "startAMorPM";
	private static final String START_DATE_DAY = "startDay";
	private static final String START_DATE_HOUR = "startHour";
	private static final String START_DATE_MINUTE = "startMinute";
	private static final String START_DATE_MONTH = "startMonth";
	private static final String START_DATE_YEAR = "startYear";

	@Reference
	private DateTimeSelectionService dateTimeSelectionService;

	public AppointmentContext getAppointmentContext(DateTimeSelection startDate, DateTimeSelection endDate) {
		TimeZone timeZone = TimeZone.getDefault();
		ZoneId zoneId = timeZone.toZoneId();

		AppointmentContext appointmentContext = new AppointmentContext();

		appointmentContext.setExactDateMatch(false);
		appointmentContext.setStartDate(Optional.of(Date.from(ZonedDateTime.ofInstant(startDate.getDate(timeZone).toInstant(), zoneId).withMinute(0).withHour(0).withSecond(0).toInstant())));
		appointmentContext.setEndDate(Optional.of(Date.from(ZonedDateTime.ofInstant(endDate.getDate(timeZone).toInstant(), zoneId).withMinute(59).withHour(23).withSecond(0).toInstant())));

		return appointmentContext;
	}

	public DateTimeSelection getDateTimeSelection(Date date, int hour, int minute) {
		Calendar instance = Calendar.getInstance();
		instance.setTime(date);
		instance.set(Calendar.HOUR, hour);
		instance.set(Calendar.MINUTE, minute);
		return new DateTimeSelection(instance);
	}

	public long getDaysOfWeekBitwiseValue(int... days) {
		int encoded = 0;
		for (int day : days) {
			encoded |= 1 << day - 1;
		}
		return encoded;
	}

	public DateTimeSelection getEndDateTimeFromRequest(PortletRequest portletRequest) {
		DateTimeSelection endDateTime = new DateTimeSelection();
		int endDateHour = ParamUtil.getInteger(portletRequest, END_DATE_HOUR);
		int endTimeAmOrPm = ParamUtil.getInteger(portletRequest, END_DATE_AM_OR_PM);
		if (endTimeAmOrPm == Calendar.PM) {
			endDateHour += 12;
		}

		endDateTime.setYear(ParamUtil.getInteger(portletRequest, END_DATE_YEAR));
		endDateTime.setMonth(ParamUtil.getInteger(portletRequest, END_DATE_MONTH));
		endDateTime.setDay(ParamUtil.getInteger(portletRequest, END_DATE_DAY));
		endDateTime.setAmORpm(endTimeAmOrPm);
		endDateTime.setHour(endDateHour);
		endDateTime.setMinute(ParamUtil.getInteger(portletRequest, END_DATE_MINUTE));
		return endDateTime;
	}

	public DateTimeSelection getEndDateTimeSelection(TimeZone timeZone) {
		return dateTimeSelectionService.getDateTimeSelectionWithHours(timeZone, 2);
	}

	public DateTimeSelection getStartDateTimeFromRequest(PortletRequest portletRequest) {
		DateTimeSelection startDateTime = new DateTimeSelection();
		int startDateHour = ParamUtil.getInteger(portletRequest, START_DATE_HOUR);
		int startTimeAmOrPm = ParamUtil.getInteger(portletRequest, START_DATE_AM_OR_PM);
		if (startTimeAmOrPm == Calendar.PM) {
			startDateHour += 12;
		}

		startDateTime.setYear(ParamUtil.getInteger(portletRequest, START_DATE_YEAR));
		startDateTime.setMonth(ParamUtil.getInteger(portletRequest, START_DATE_MONTH));
		startDateTime.setDay(ParamUtil.getInteger(portletRequest, START_DATE_DAY));
		startDateTime.setAmORpm(startTimeAmOrPm);
		startDateTime.setHour(startDateHour);
		startDateTime.setMinute(ParamUtil.getInteger(portletRequest, START_DATE_MINUTE));
		return startDateTime;
	}

	public DateTimeSelection getStartDateTimeSelection(TimeZone timeZone) {
		return dateTimeSelectionService.getDateTimeSelectionWithHours(timeZone, 1);
	}
}
