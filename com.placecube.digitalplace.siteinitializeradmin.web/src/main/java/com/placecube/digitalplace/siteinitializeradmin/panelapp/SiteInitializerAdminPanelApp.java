package com.placecube.digitalplace.siteinitializeradmin.panelapp;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.siteinitializeradmin.constants.PortletKeys;

@Component(immediate = true, property = { "panel.app.order:Integer=100", "panel.category.key=" + PanelCategoryKeys.SITE_ADMINISTRATION_CONFIGURATION }, service = PanelApp.class)
public class SiteInitializerAdminPanelApp extends BasePanelApp {

	@Reference(target = "(javax.portlet.name=" + PortletKeys.SITE_INITIALIZER_ADMIN + ")", unbind = "-")
	private Portlet portlet;

	@Override
	public Portlet getPortlet() {
		return portlet;
	}

	@Override
	public String getPortletId() {
		return PortletKeys.SITE_INITIALIZER_ADMIN;
	}

	@Override
	public boolean isShow(PermissionChecker permissionChecker, Group group) throws PortalException {
		if(group.isCompany()){
			return false;
		}
		return super.isShow(permissionChecker, group);
	}

}