/**
 * ScpInvokeResponseInvokeResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class ScpInvokeResponseInvokeResult  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.Status status;

    private org.apache.axis.types.Token redirectUrl;

    private com.capita_software_services.www.scp.base.ErrorDetails errorDetails;

    public ScpInvokeResponseInvokeResult() {
    }

    public ScpInvokeResponseInvokeResult(
           com.capita_software_services.www.scp.base.Status status,
           org.apache.axis.types.Token redirectUrl,
           com.capita_software_services.www.scp.base.ErrorDetails errorDetails) {
           this.status = status;
           this.redirectUrl = redirectUrl;
           this.errorDetails = errorDetails;
    }


    /**
     * Gets the status value for this ScpInvokeResponseInvokeResult.
     *
     * @return status
     */
    public com.capita_software_services.www.scp.base.Status getStatus() {
        return status;
    }


    /**
     * Sets the status value for this ScpInvokeResponseInvokeResult.
     *
     * @param status
     */
    public void setStatus(com.capita_software_services.www.scp.base.Status status) {
        this.status = status;
    }


    /**
     * Gets the redirectUrl value for this ScpInvokeResponseInvokeResult.
     *
     * @return redirectUrl
     */
    public org.apache.axis.types.Token getRedirectUrl() {
        return redirectUrl;
    }


    /**
     * Sets the redirectUrl value for this ScpInvokeResponseInvokeResult.
     *
     * @param redirectUrl
     */
    public void setRedirectUrl(org.apache.axis.types.Token redirectUrl) {
        this.redirectUrl = redirectUrl;
    }


    /**
     * Gets the errorDetails value for this ScpInvokeResponseInvokeResult.
     *
     * @return errorDetails
     */
    public com.capita_software_services.www.scp.base.ErrorDetails getErrorDetails() {
        return errorDetails;
    }


    /**
     * Sets the errorDetails value for this ScpInvokeResponseInvokeResult.
     *
     * @param errorDetails
     */
    public void setErrorDetails(com.capita_software_services.www.scp.base.ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScpInvokeResponseInvokeResult)) return false;
        ScpInvokeResponseInvokeResult other = (ScpInvokeResponseInvokeResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.status==null && other.getStatus()==null) ||
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.redirectUrl==null && other.getRedirectUrl()==null) ||
             (this.redirectUrl!=null &&
              this.redirectUrl.equals(other.getRedirectUrl()))) &&
            ((this.errorDetails==null && other.getErrorDetails()==null) ||
             (this.errorDetails!=null &&
              this.errorDetails.equals(other.getErrorDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getRedirectUrl() != null) {
            _hashCode += getRedirectUrl().hashCode();
        }
        if (getErrorDetails() != null) {
            _hashCode += getErrorDetails().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScpInvokeResponseInvokeResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", ">scpInvokeResponse>invokeResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "status"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("redirectUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "redirectUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "token"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "errorDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "errorDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
