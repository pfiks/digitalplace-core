package com.placecube.digitalplace.externalpage.web.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.instanceable=false", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/external-page-import/view.jsp", "com.liferay.portlet.header-portlet-css=/css/main.css",
		"javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET, "javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supports.mime-type=text/html" }, service = Portlet.class)
public class ExternalPagePortlet extends MVCPortlet {

}
