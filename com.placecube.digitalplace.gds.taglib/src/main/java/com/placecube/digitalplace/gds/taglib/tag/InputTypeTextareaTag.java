package com.placecube.digitalplace.gds.taglib.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.liferay.taglib.util.IncludeTag;
import com.placecube.digitalplace.common.utils.HtmlUtil;
import com.placecube.digitalplace.gds.taglib.context.ServletContextUtil;

public class InputTypeTextareaTag extends IncludeTag {

	private String portletNamespace;
	private String fieldName;
	private String fieldLabel;
	private String fieldValue;
	private String errorMessage;
	private int fieldRows;

	@Override
	public int doStartTag() {
		setAttributeNamespace("gds-forms-ui:input-textarea");
		return EVAL_BODY_INCLUDE;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setFieldRows(int fieldRows) {
		this.fieldRows = fieldRows;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	@Override
	public void setPageContext(PageContext pageContext) {
		super.setPageContext(pageContext);
		setServletContext(ServletContextUtil.getServletContext());
	}

	public void setPortletNamespace(String portletNamespace) {
		this.portletNamespace = portletNamespace;
	}

	@Override
	protected void cleanUp() {
		super.cleanUp();
		portletNamespace = null;
		fieldName = null;
		fieldLabel = null;
		fieldValue = null;
		errorMessage = null;
		fieldRows = 5;
	}

	@Override
	protected String getPage() {
		return "/META-INF/taglibs/gds-forms-ui/textarea/view.jsp";
	}

	@Override
	protected void setAttributes(HttpServletRequest request) {
		request.setAttribute("portletNamespace", portletNamespace);
		request.setAttribute("fieldName", fieldName);
		request.setAttribute("fieldLabel", fieldLabel);
		request.setAttribute("fieldValue", HtmlUtil.escapeName(fieldValue));
		request.setAttribute("errorMessage", errorMessage);
		request.setAttribute("fieldRows", fieldRows <= 0 ? 5 : fieldRows);
	}

}
