package com.placecube.digitalplace.userimport.admin.web.lifecyclelistener;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.service.ServiceContext;

@Component(immediate = true, service = ServiceContextFactory.class)
public class ServiceContextFactory {

	public ServiceContext createServiceContext(long scopeGroupId, long userId) {
		ServiceContext serviceContext = new ServiceContext();

		serviceContext.setScopeGroupId(scopeGroupId);
		serviceContext.setUserId(userId);
		return serviceContext;
	}
}
