package com.placecube.digitalplace.oauth.dataprovider.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jayway.jsonpath.DocumentContext;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderInputParametersSettings;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderOutputParametersSettings;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderRequest;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderResponse;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderResponseStatus;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceLocalService;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.placecube.digitalplace.oauth.dataprovider.settings.DDMRESTOAuth2DataProviderSettings;

import java.io.IOException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DDMRESTOAuth2DataProviderServiceTest {

	private static final long REQUEST_COMPANY_ID = 43332l;
	private static final long COMPANY_ID_ONE = 9875l;
	private static final long COMPANY_ID_TWO = 3271l;
	private static final String DATA_PROVIDER_ID = "234322";

	@InjectMocks
	private DDMRESTOAuth2DataProviderService ddmRESTOAuth2DataProviderService;

	@Mock
	private CompanyLocalService mockCompanyLocalService;

	@Mock
	private DDMDataProviderInstanceLocalService mockDDMDataProviderInstanceLocalService;

	@Mock
	private DDMDataProviderRequest mockDDMDataProviderRequest;

	@Mock
	private DDMDataProviderResponse mockDDMDataProviderResponse;

	@Mock
	private DDMDataProviderInstance mockDDMDataProviderInstance;

	@Mock
	private Company mockCompanyOne;

	@Mock
	private Company mockCompanyTwo;

	@Mock
	private KeyStore mockNonStaticKeyStore;

	@Mock
	private DocumentContext mockDocumentContext;

	@Mock
	private DDMRESTOAuth2DataProviderSettings mockDDMRESTOAuth2DataProviderSettings;

	@Mock
	private DDMDataProviderResponse.Builder mockNonStaticDDMDataProviderResponseBuilder;

	@Mock
	private DDMDataProviderOutputParametersSettings mockDDMDataProviderOutputParametersSettingsText;

	@Mock
	private DDMDataProviderOutputParametersSettings mockDDMDataProviderOutputParametersSettingsNumber;

	@Mock
	private DDMDataProviderOutputParametersSettings mockDDMDataProviderOutputParametersSettingsList;

	@Mock
	private DDMDataProviderOutputParametersSettings mockDDMDataProviderOutputParametersSettingsUnknown;

	@Mock
	private DDMDataProviderInputParametersSettings mockDDMDataProviderInputParametersSettings;

	private MockedStatic<CompanyThreadLocal> mockCompanyThreadLocal;

	private MockedStatic<KeyStore> mockKeyStore;

	private MockedStatic<DDMDataProviderResponse.Builder> mockDDMDataProviderResponseBuilder;

	@Before
	public void activateSetup() {
		mockCompanyThreadLocal = mockStatic(CompanyThreadLocal.class);
		mockKeyStore = mockStatic(KeyStore.class);
		mockDDMDataProviderResponseBuilder = mockStatic(DDMDataProviderResponse.Builder.class);
	}

	@After
	public void tearDown() {
		mockCompanyThreadLocal.close();
		mockKeyStore.close();
		mockDDMDataProviderResponseBuilder.close();
	}

	@Test
	public void findDDMDataProviderInstance_WhenCompanyIsInRequestAndDataProviderIdIsUuid_ThenSetsRequestCompanyIdInCompanyThreadLocalAndRetrievesDataProviderInstance() {
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_ID);
		when(mockDDMDataProviderRequest.getCompanyId()).thenReturn(REQUEST_COMPANY_ID);

		when(mockDDMDataProviderInstanceLocalService.fetchDataProviderInstanceByUuid(DATA_PROVIDER_ID)).thenReturn(mockDDMDataProviderInstance);

		Optional<DDMDataProviderInstance> result = ddmRESTOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockDDMDataProviderInstance));

		InOrder inOrder = inOrder(mockDDMDataProviderInstanceLocalService, CompanyThreadLocal.class);

		inOrder.verify(mockCompanyThreadLocal, () -> CompanyThreadLocal.setCompanyId(REQUEST_COMPANY_ID));
		inOrder.verify(mockDDMDataProviderInstanceLocalService, times(1)).fetchDataProviderInstanceByUuid(DATA_PROVIDER_ID);
	}

	@Test
	public void findDDMDataProviderInstance_WhenCompanyIsNotInRequest_ThenSearchesDataProviderInstanceThroughCompanies() {
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_ID);
		when(mockDDMDataProviderRequest.getCompanyId()).thenReturn(0L);
		when(mockCompanyLocalService.getCompanies()).thenReturn(Arrays.asList(mockCompanyOne, mockCompanyTwo));
		when(mockCompanyOne.getCompanyId()).thenReturn(COMPANY_ID_ONE);
		when(mockCompanyTwo.getCompanyId()).thenReturn(COMPANY_ID_TWO);

		when(mockDDMDataProviderInstanceLocalService.fetchDataProviderInstanceByUuid(DATA_PROVIDER_ID)).thenReturn(null, mockDDMDataProviderInstance);

		Optional<DDMDataProviderInstance> result = ddmRESTOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockDDMDataProviderInstance));

		InOrder inOrder = inOrder(mockDDMDataProviderInstanceLocalService, CompanyThreadLocal.class);

		inOrder.verify(mockCompanyThreadLocal, () -> CompanyThreadLocal.setCompanyId(COMPANY_ID_ONE));
		inOrder.verify(mockDDMDataProviderInstanceLocalService, times(1)).fetchDataProviderInstanceByUuid(DATA_PROVIDER_ID);
		inOrder.verify(mockCompanyThreadLocal, () -> CompanyThreadLocal.setCompanyId(COMPANY_ID_TWO));
		inOrder.verify(mockDDMDataProviderInstanceLocalService, times(1)).fetchDataProviderInstanceByUuid(DATA_PROVIDER_ID);
	}

	@Test
	public void findDDMDataProviderInstance_WhenDataProviderIdIsId_ThenFindsDataProviderInstanceById() {
		when(mockDDMDataProviderRequest.getDDMDataProviderId()).thenReturn(DATA_PROVIDER_ID);
		when(mockDDMDataProviderRequest.getCompanyId()).thenReturn(REQUEST_COMPANY_ID);

		when(mockDDMDataProviderInstanceLocalService.fetchDataProviderInstance(GetterUtil.getLong(DATA_PROVIDER_ID))).thenReturn(mockDDMDataProviderInstance);

		Optional<DDMDataProviderInstance> result = ddmRESTOAuth2DataProviderService.findDDMDataProviderInstance(mockDDMDataProviderRequest);

		assertThat(result.isPresent(), equalTo(true));
		assertThat(result.get(), sameInstance(mockDDMDataProviderInstance));

		InOrder inOrder = inOrder(mockDDMDataProviderInstanceLocalService, CompanyThreadLocal.class);

		inOrder.verify(mockCompanyThreadLocal, () -> CompanyThreadLocal.setCompanyId(REQUEST_COMPANY_ID));
		inOrder.verify(mockDDMDataProviderInstanceLocalService, times(1)).fetchDataProviderInstanceByUuid(DATA_PROVIDER_ID);
	}

	@Test
	public void createDDMDataProviderResponse_WhenOutputParametersAreEmpty_ThenReturnsResponseWithStatusWithoutOutput() {
		when(mockDDMRESTOAuth2DataProviderSettings.outputParameters()).thenReturn(new DDMDataProviderOutputParametersSettings[] {});
		mockDDMDataProviderResponseBuilder.when(() -> DDMDataProviderResponse.Builder.newBuilder()).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);
		when(mockNonStaticDDMDataProviderResponseBuilder.build()).thenReturn(mockDDMDataProviderResponse);

		DDMDataProviderResponse result = ddmRESTOAuth2DataProviderService.createDDMDataProviderResponse(mockDocumentContext, DDMDataProviderResponseStatus.OK, mockDDMRESTOAuth2DataProviderSettings);

		assertThat(result, sameInstance(mockDDMDataProviderResponse));

		verify(mockNonStaticDDMDataProviderResponseBuilder, never()).withOutput(anyString(), any(Object.class));

		InOrder inOrder = inOrder(mockNonStaticDDMDataProviderResponseBuilder);

		inOrder.verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).withStatus(DDMDataProviderResponseStatus.OK);
		inOrder.verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).build();
	}

	@Test
	public void createDDMDataProviderResponse_WhenOutputParametersAreNotEmpty_ThenReturnsResponseWithStatusAndOutput() {
		List<KeyValuePair> keyValuePairs = new ArrayList<>();
		keyValuePairs.add(new KeyValuePair("key1", String.valueOf(20)));
		keyValuePairs.add(new KeyValuePair("key2", String.valueOf(30)));
		keyValuePairs.add(new KeyValuePair("key3", String.valueOf(40)));

		mockDDMDataProviderOutputParametersSettings();
		when(mockDDMRESTOAuth2DataProviderSettings.outputParameters()).thenReturn(new DDMDataProviderOutputParametersSettings[] { mockDDMDataProviderOutputParametersSettingsText,
				mockDDMDataProviderOutputParametersSettingsNumber, mockDDMDataProviderOutputParametersSettingsList, mockDDMDataProviderOutputParametersSettingsUnknown });

		mockDDMDataProviderResponseBuilder.when(() -> DDMDataProviderResponse.Builder.newBuilder()).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);
		when(mockNonStaticDDMDataProviderResponseBuilder.withOutput("1", "textOutput")).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);
		when(mockNonStaticDDMDataProviderResponseBuilder.withOutput("2", 666)).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);
		when(mockNonStaticDDMDataProviderResponseBuilder.withOutput("3", keyValuePairs)).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);
		when(mockNonStaticDDMDataProviderResponseBuilder.build()).thenReturn(mockDDMDataProviderResponse);

		DDMDataProviderResponse result = ddmRESTOAuth2DataProviderService.createDDMDataProviderResponse(mockDocumentContext, DDMDataProviderResponseStatus.OK, mockDDMRESTOAuth2DataProviderSettings);

		assertThat(result, sameInstance(mockDDMDataProviderResponse));

		InOrder inOrder = inOrder(mockNonStaticDDMDataProviderResponseBuilder);

		inOrder.verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).withStatus(DDMDataProviderResponseStatus.OK);
		inOrder.verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).withOutput("1", "textOutput");
		inOrder.verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).withOutput("2", 666);
		inOrder.verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).withOutput("3", keyValuePairs);
		inOrder.verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).build();
	}

	@Test
	public void createDDMDataProviderResponse_WhenListOutputParameterHasNoValues_ThenDoesNotAddListOutput() {

		when(mockDDMRESTOAuth2DataProviderSettings.outputParameters()).thenReturn(new DDMDataProviderOutputParametersSettings[] { mockDDMDataProviderOutputParametersSettingsList });
		when(mockDDMDataProviderOutputParametersSettingsList.outputParameterType()).thenReturn("list");
		when(mockDDMDataProviderOutputParametersSettingsList.outputParameterPath()).thenReturn(".path3_1;path3_2;path3_3");

		mockDDMDataProviderResponseBuilder.when(() -> DDMDataProviderResponse.Builder.newBuilder()).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);
		when(mockNonStaticDDMDataProviderResponseBuilder.build()).thenReturn(mockDDMDataProviderResponse);

		ddmRESTOAuth2DataProviderService.createDDMDataProviderResponse(mockDocumentContext, DDMDataProviderResponseStatus.OK, mockDDMRESTOAuth2DataProviderSettings);

		verify(mockNonStaticDDMDataProviderResponseBuilder, never()).withOutput(anyString(), any());
	}

	@Test
	public void createDDMDataProviderResponse_WhenThereIsOnlyOnePathIntheList_ThenAddsValuesAsOutputKeys() {
		List<KeyValuePair> keyValuePairs = new ArrayList<>();
		keyValuePairs.add(new KeyValuePair("20", String.valueOf(20)));
		keyValuePairs.add(new KeyValuePair("30", String.valueOf(30)));
		keyValuePairs.add(new KeyValuePair("40", String.valueOf(40)));

		when(mockDDMRESTOAuth2DataProviderSettings.outputParameters()).thenReturn(new DDMDataProviderOutputParametersSettings[] { mockDDMDataProviderOutputParametersSettingsList });
		when(mockDDMDataProviderOutputParametersSettingsList.outputParameterId()).thenReturn("3");
		when(mockDDMDataProviderOutputParametersSettingsList.outputParameterType()).thenReturn("list");
		when(mockDDMDataProviderOutputParametersSettingsList.outputParameterPath()).thenReturn(".path3_1");
		when(mockDocumentContext.read(".path3_1", List.class)).thenReturn(Arrays.asList(20, 30, 40));
		when(mockNonStaticDDMDataProviderResponseBuilder.withOutput("3", keyValuePairs)).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);

		mockDDMDataProviderResponseBuilder.when(() -> DDMDataProviderResponse.Builder.newBuilder()).thenReturn(mockNonStaticDDMDataProviderResponseBuilder);
		when(mockNonStaticDDMDataProviderResponseBuilder.build()).thenReturn(mockDDMDataProviderResponse);

		ddmRESTOAuth2DataProviderService.createDDMDataProviderResponse(mockDocumentContext, DDMDataProviderResponseStatus.OK, mockDDMRESTOAuth2DataProviderSettings);

		verify(mockNonStaticDDMDataProviderResponseBuilder, times(1)).withOutput("3", keyValuePairs);
	}

	@Test
	public void getKeyStore_WhenNoErrors_ThenReturnsKeyStore() throws Exception {
		String keyStoreType = "type";

		mockKeyStore.when(KeyStore::getDefaultType).thenReturn(keyStoreType);
		mockKeyStore.when(() -> KeyStore.getInstance(keyStoreType)).thenReturn(mockNonStaticKeyStore);

		KeyStore result = ddmRESTOAuth2DataProviderService.getKeyStore();

		assertThat(result, sameInstance(mockNonStaticKeyStore));

		verify(mockNonStaticKeyStore, times(1)).load(null);
	}

	@Test(expected = IOException.class)
	public void getKeyStore_WhenKeyStoreRetrievalFails_ThenThrowsException() throws Exception {
		String keyStoreType = "type";

		mockKeyStore.when(KeyStore::getDefaultType).thenReturn(keyStoreType);
		mockKeyStore.when(() -> KeyStore.getInstance(keyStoreType)).thenReturn(mockNonStaticKeyStore);

		doThrow(new IOException()).when(mockNonStaticKeyStore).load(null);

		ddmRESTOAuth2DataProviderService.getKeyStore();

	}

	@Test
	public void getRequestInputParametersMap_WhenRequestContainsParameters_ThenReturnsMapOfInputParameterNameToRequestParameterValue() {
		final String parameterName = "param";
		final String parameterValue = "value";

		Map<String, Object> requestParameters = new HashMap<>();
		requestParameters.put(parameterName, parameterValue);

		when(mockDDMRESTOAuth2DataProviderSettings.inputParameters()).thenReturn(new DDMDataProviderInputParametersSettings[] { mockDDMDataProviderInputParametersSettings });
		when(mockDDMDataProviderInputParametersSettings.inputParameterName()).thenReturn(parameterName);
		when(mockDDMDataProviderRequest.getParameters()).thenReturn(requestParameters);

		Map<String, Object> response = ddmRESTOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings);

		assertThat(response.size(), equalTo(1));
		assertThat(response.get(parameterName), equalTo(parameterValue));
	}

	@Test
	public void getRequestInputParametersMap_WhenRequestDoesNotContainInputParameters_ThenDoesNotReturnParameterInMap() {
		final String parameterName = "param";

		when(mockDDMRESTOAuth2DataProviderSettings.inputParameters()).thenReturn(new DDMDataProviderInputParametersSettings[] { mockDDMDataProviderInputParametersSettings });
		when(mockDDMDataProviderInputParametersSettings.inputParameterName()).thenReturn(parameterName);
		when(mockDDMDataProviderRequest.getParameters()).thenReturn(new HashMap<>());

		Map<String, Object> response = ddmRESTOAuth2DataProviderService.getRequestInputParametersMap(mockDDMDataProviderRequest, mockDDMRESTOAuth2DataProviderSettings);

		assertThat(response.isEmpty(), equalTo(true));
	}

	private void mockDDMDataProviderOutputParametersSettings() {
		when(mockDDMDataProviderOutputParametersSettingsText.outputParameterId()).thenReturn("1");
		when(mockDDMDataProviderOutputParametersSettingsText.outputParameterType()).thenReturn("text");
		when(mockDDMDataProviderOutputParametersSettingsText.outputParameterPath()).thenReturn("$path1");

		when(mockDDMDataProviderOutputParametersSettingsNumber.outputParameterId()).thenReturn("2");
		when(mockDDMDataProviderOutputParametersSettingsNumber.outputParameterType()).thenReturn("number");
		when(mockDDMDataProviderOutputParametersSettingsNumber.outputParameterPath()).thenReturn("*path2");

		when(mockDDMDataProviderOutputParametersSettingsList.outputParameterId()).thenReturn("3");
		when(mockDDMDataProviderOutputParametersSettingsList.outputParameterType()).thenReturn("list");
		when(mockDDMDataProviderOutputParametersSettingsList.outputParameterPath()).thenReturn(".path3_1;path3_2;path3_3");

		when(mockDDMDataProviderOutputParametersSettingsUnknown.outputParameterType()).thenReturn("other");

		when(mockDocumentContext.read("$path1", String.class)).thenReturn("textOutput");
		when(mockDocumentContext.read("*path2", Number.class)).thenReturn(666);

		when(mockDocumentContext.read(".path3_1", List.class)).thenReturn(Arrays.asList(20, 30, 40));
		when(mockDocumentContext.read(".path3_2")).thenReturn(Arrays.asList("key1", "key2", "key3"));
	}
}
