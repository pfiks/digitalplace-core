package com.placecube.digitalplace.group.restadmin.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.group.rest.model.GroupWebhook;
import com.placecube.digitalplace.group.rest.service.GroupWebhookLocalService;
import com.placecube.digitalplace.group.restadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.GROUP_WEBHOOKS, "mvc.command.name=" + MVCCommandKeys.UPDATE }, service = MVCActionCommand.class)
public class UpdateWebhookMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private GroupWebhookLocalService groupWebhookLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long webhookId = ParamUtil.getLong(actionRequest, "webhookId", 0L);
		String webhookClassName = ParamUtil.getString(actionRequest, "webhookClassName");
		String webhookName = ParamUtil.getString(actionRequest, "webhookName");
		String webhookPayloadURL = ParamUtil.getString(actionRequest, "webhookPayloadURL");
		boolean webhookEnabled = ParamUtil.getBoolean(actionRequest, "webhookEnabled");
		String[] webhookEventActions = ParamUtil.getStringValues(actionRequest, "webhookEventActions");
		long classPK = ParamUtil.getLong(actionRequest, "webhookClassPK", 0L);
		GroupWebhook webhook = groupWebhookLocalService.getOrCreateGroupWebhook(webhookId);
		webhook.setCompanyId(themeDisplay.getCompanyId());
		webhook.setEnabled(webhookEnabled);
		webhook.setClassName(webhookClassName);
		webhook.setClassPK(classPK);
		webhook.setEvents(String.join(StringPool.COMMA, webhookEventActions));
		webhook.setGroupId(themeDisplay.getScopeGroupId());
		webhook.setName(webhookName);
		webhook.setPayloadURL(webhookPayloadURL);

		groupWebhookLocalService.updateGroupWebhook(webhook);

		actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandKeys.VIEW);
	}

}