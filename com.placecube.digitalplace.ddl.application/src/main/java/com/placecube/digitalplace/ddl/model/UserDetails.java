package com.placecube.digitalplace.ddl.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.liferay.portal.kernel.model.User;

public class UserDetails {

	public User user;

	public String fullName;
	public String emailAddress;
	public long userId;

	private Map<String, Serializable> userCustomFields = new HashMap<>();

	public UserDetails(String fullName, String emailAddress, long userId) {
		this.fullName = fullName;
		this.emailAddress = emailAddress;
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public Map<String, Serializable> getUserCustomFields() {
		return userCustomFields;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserCustomFields(Map<String, Serializable> userCustomFields) {
		this.userCustomFields = userCustomFields;
	}

}
