package com.placecube.digitalplace.user.account.service;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

public interface UserAuthenticationService {

	/**
	 * Retrieves the content of the configured password policy article or the
	 * default password policy description if the former does not exist
	 *
	 * @param portletRequest the portlet request
	 * @return the password policy html content
	 * @throws PortletException if a system error occurs when retrieving the
	 *             content
	 */
	String getUserAccountPasswordPolicyHtml(PortletRequest portletRequest) throws PortletException;

	/**
	 * Retrieves the content of the configured password policy article or the
	 * default password policy description if the former does not exist
	 *
	 * @param httpServletRequest the servlet request
	 * @return the password policy html content
	 * @throws PortletException if a system error occurs when retrieving the
	 *             content
	 */
	String getUserAccountPasswordPolicyHtml(HttpServletRequest httpServletRequest) throws PortletException;
}
