package com.placecube.digitalplace.layout.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.service.PortletLocalService;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = LayoutWithPortletsRetrievalHelperUtil.class)
public class LayoutWithPortletsRetrievalHelperUtil {

	@Reference
	private PortletLocalService portletLocalService;

	@Reference
	private PortletPreferencesLocalService portletPreferencesLocalService;

	public List<Portlet> getAllPortletsOnLayout(Layout layout) {
		LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
		List<Portlet> allPortlets = layoutTypePortlet.getAllPortlets();
		if (!Objects.equals(layout.getType(), LayoutConstants.TYPE_PORTLET)) {
			List<com.liferay.portal.kernel.model.PortletPreferences> portletPreferencesList = portletPreferencesLocalService.getPortletPreferences(PortletKeys.PREFS_OWNER_ID_DEFAULT,
					PortletKeys.PREFS_OWNER_TYPE_LAYOUT, layout.getPlid());
			Stream<Portlet> portletsFromPrefs = portletPreferencesList.stream().map(portletPreferences -> portletLocalService.getPortletById(layout.getCompanyId(), portletPreferences.getPortletId()))
					.filter(this::isValidPortlet);
			allPortlets = Stream.concat(allPortlets.stream(), portletsFromPrefs).distinct().collect(Collectors.toList());
		}

		return allPortlets;
	}

	public boolean hasPortletId(List<Portlet> portlets, String portletIdToMatch) {
		for (Portlet portlet : portlets) {
			if (portlet.getPortletId().contains(portletIdToMatch)) {
				return true;
			}
		}
		return false;
	}

	public List<String> getPortletIdListFromListOfPortlets(List<Portlet> portlets, String portletIdToMatch) {
		List<String> portletIds = new ArrayList<>();

		for (Portlet portlet : portlets) {
			if (portlet.getPortletId().contains(portletIdToMatch)) {
				portletIds.add(portlet.getPortletId());
			}
		}

		return portletIds;
	}

	private boolean isValidPortlet(Portlet portlet) {
		if (portlet.isInstanceable()) {
			return Validator.isNotNull(portlet.getInstanceId());
		} else {
			return true;
		}
	}
}
