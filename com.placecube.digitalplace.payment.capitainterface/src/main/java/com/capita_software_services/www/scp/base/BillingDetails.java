/**
 * BillingDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class BillingDetails  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.Card card;

    private com.capita_software_services.www.scp.base.CardHolderDetails cardHolderDetails;

    private java.lang.Boolean editable;  // attribute

    public BillingDetails() {
    }

    public BillingDetails(
           com.capita_software_services.www.scp.base.Card card,
           com.capita_software_services.www.scp.base.CardHolderDetails cardHolderDetails,
           java.lang.Boolean editable) {
           this.card = card;
           this.cardHolderDetails = cardHolderDetails;
           this.editable = editable;
    }


    /**
     * Gets the card value for this BillingDetails.
     *
     * @return card
     */
    public com.capita_software_services.www.scp.base.Card getCard() {
        return card;
    }


    /**
     * Sets the card value for this BillingDetails.
     *
     * @param card
     */
    public void setCard(com.capita_software_services.www.scp.base.Card card) {
        this.card = card;
    }


    /**
     * Gets the cardHolderDetails value for this BillingDetails.
     *
     * @return cardHolderDetails
     */
    public com.capita_software_services.www.scp.base.CardHolderDetails getCardHolderDetails() {
        return cardHolderDetails;
    }


    /**
     * Sets the cardHolderDetails value for this BillingDetails.
     *
     * @param cardHolderDetails
     */
    public void setCardHolderDetails(com.capita_software_services.www.scp.base.CardHolderDetails cardHolderDetails) {
        this.cardHolderDetails = cardHolderDetails;
    }


    /**
     * Gets the editable value for this BillingDetails.
     *
     * @return editable
     */
    public java.lang.Boolean getEditable() {
        return editable;
    }


    /**
     * Sets the editable value for this BillingDetails.
     *
     * @param editable
     */
    public void setEditable(java.lang.Boolean editable) {
        this.editable = editable;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BillingDetails)) return false;
        BillingDetails other = (BillingDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.card==null && other.getCard()==null) ||
             (this.card!=null &&
              this.card.equals(other.getCard()))) &&
            ((this.cardHolderDetails==null && other.getCardHolderDetails()==null) ||
             (this.cardHolderDetails!=null &&
              this.cardHolderDetails.equals(other.getCardHolderDetails()))) &&
            ((this.editable==null && other.getEditable()==null) ||
             (this.editable!=null &&
              this.editable.equals(other.getEditable())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCard() != null) {
            _hashCode += getCard().hashCode();
        }
        if (getCardHolderDetails() != null) {
            _hashCode += getCardHolderDetails().hashCode();
        }
        if (getEditable() != null) {
            _hashCode += getEditable().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BillingDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "billingDetails"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("editable");
        attrField.setXmlName(new javax.xml.namespace.QName("", "editable"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("card");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "card"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "card"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardHolderDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "cardHolderDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
