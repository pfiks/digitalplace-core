package com.placecube.digitalplace.payment.govuk.service.commerce.configuration.frontend;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.ValidatorException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.commerce.product.constants.CPPortletKeys;
import com.liferay.commerce.product.model.CommerceChannel;
import com.liferay.commerce.product.service.CommerceChannelService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.settings.FallbackKeysSettingsUtil;
import com.liferay.portal.kernel.settings.GroupServiceSettingsLocator;
import com.liferay.portal.kernel.settings.ModifiableSettings;
import com.liferay.portal.kernel.settings.Settings;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.payment.govuk.service.commerce.GovUKCommerceConstants;

@Component(property = { "javax.portlet.name=" + CPPortletKeys.COMMERCE_PAYMENT_METHODS,
		"mvc.command.name=/commerce_payment_methods/edit_govuk_pay_commerce_payment_method_configuration" }, service = MVCActionCommand.class)
public class EditGovUKPayCommercePaymentMethodConfigurationMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private CommerceChannelService commerceChannelService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String cmd = ParamUtil.getString(actionRequest, Constants.CMD);

		if (cmd.equals(Constants.UPDATE)) {
			updateCommercePaymentMethod(actionRequest);
		}
	}

	private void updateCommercePaymentMethod(ActionRequest actionRequest) throws PortalException, ValidatorException, IOException {
		long commerceChannelId = ParamUtil.getLong(actionRequest, "commerceChannelId");

		CommerceChannel commerceChannel = commerceChannelService.getCommerceChannel(commerceChannelId);

		Settings settings = FallbackKeysSettingsUtil.getSettings(new GroupServiceSettingsLocator(commerceChannel.getGroupId(), GovUKCommerceConstants.SERVICE_NAME));

		ModifiableSettings modifiableSettings = settings.getModifiableSettings();

		String apiKeyName = ParamUtil.getString(actionRequest, "settings--apiKeyName--");

		modifiableSettings.setValue("keyName", apiKeyName);

		modifiableSettings.store();
	}

}