/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.appointment.model.AppointmentSlot;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing AppointmentSlot in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AppointmentSlotCacheModel
	implements CacheModel<AppointmentSlot>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AppointmentSlotCacheModel)) {
			return false;
		}

		AppointmentSlotCacheModel appointmentSlotCacheModel =
			(AppointmentSlotCacheModel)object;

		if (appointmentSlotId == appointmentSlotCacheModel.appointmentSlotId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, appointmentSlotId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(39);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", appointmentSlotId=");
		sb.append(appointmentSlotId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", slotType=");
		sb.append(slotType);
		sb.append(", name=");
		sb.append(name);
		sb.append(", serviceId=");
		sb.append(serviceId);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", slotStartTimeHour=");
		sb.append(slotStartTimeHour);
		sb.append(", slotStartTimeMinute=");
		sb.append(slotStartTimeMinute);
		sb.append(", slotEndTimeHour=");
		sb.append(slotEndTimeHour);
		sb.append(", slotEndTimeMinute=");
		sb.append(slotEndTimeMinute);
		sb.append(", appointmentDuration=");
		sb.append(appointmentDuration);
		sb.append(", daysOfWeekBitwise=");
		sb.append(daysOfWeekBitwise);
		sb.append(", maxAppointments=");
		sb.append(maxAppointments);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AppointmentSlot toEntityModel() {
		AppointmentSlotImpl appointmentSlotImpl = new AppointmentSlotImpl();

		if (uuid == null) {
			appointmentSlotImpl.setUuid("");
		}
		else {
			appointmentSlotImpl.setUuid(uuid);
		}

		appointmentSlotImpl.setAppointmentSlotId(appointmentSlotId);
		appointmentSlotImpl.setCompanyId(companyId);
		appointmentSlotImpl.setUserId(userId);

		if (userName == null) {
			appointmentSlotImpl.setUserName("");
		}
		else {
			appointmentSlotImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			appointmentSlotImpl.setCreateDate(null);
		}
		else {
			appointmentSlotImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			appointmentSlotImpl.setModifiedDate(null);
		}
		else {
			appointmentSlotImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (slotType == null) {
			appointmentSlotImpl.setSlotType("");
		}
		else {
			appointmentSlotImpl.setSlotType(slotType);
		}

		if (name == null) {
			appointmentSlotImpl.setName("");
		}
		else {
			appointmentSlotImpl.setName(name);
		}

		if (serviceId == null) {
			appointmentSlotImpl.setServiceId("");
		}
		else {
			appointmentSlotImpl.setServiceId(serviceId);
		}

		if (startDate == Long.MIN_VALUE) {
			appointmentSlotImpl.setStartDate(null);
		}
		else {
			appointmentSlotImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			appointmentSlotImpl.setEndDate(null);
		}
		else {
			appointmentSlotImpl.setEndDate(new Date(endDate));
		}

		appointmentSlotImpl.setSlotStartTimeHour(slotStartTimeHour);
		appointmentSlotImpl.setSlotStartTimeMinute(slotStartTimeMinute);
		appointmentSlotImpl.setSlotEndTimeHour(slotEndTimeHour);
		appointmentSlotImpl.setSlotEndTimeMinute(slotEndTimeMinute);
		appointmentSlotImpl.setAppointmentDuration(appointmentDuration);
		appointmentSlotImpl.setDaysOfWeekBitwise(daysOfWeekBitwise);
		appointmentSlotImpl.setMaxAppointments(maxAppointments);

		appointmentSlotImpl.resetOriginalValues();

		return appointmentSlotImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		appointmentSlotId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		slotType = objectInput.readUTF();
		name = objectInput.readUTF();
		serviceId = objectInput.readUTF();
		startDate = objectInput.readLong();
		endDate = objectInput.readLong();

		slotStartTimeHour = objectInput.readInt();

		slotStartTimeMinute = objectInput.readInt();

		slotEndTimeHour = objectInput.readInt();

		slotEndTimeMinute = objectInput.readInt();

		appointmentDuration = objectInput.readInt();

		daysOfWeekBitwise = objectInput.readLong();

		maxAppointments = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(appointmentSlotId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (slotType == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(slotType);
		}

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (serviceId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(serviceId);
		}

		objectOutput.writeLong(startDate);
		objectOutput.writeLong(endDate);

		objectOutput.writeInt(slotStartTimeHour);

		objectOutput.writeInt(slotStartTimeMinute);

		objectOutput.writeInt(slotEndTimeHour);

		objectOutput.writeInt(slotEndTimeMinute);

		objectOutput.writeInt(appointmentDuration);

		objectOutput.writeLong(daysOfWeekBitwise);

		objectOutput.writeInt(maxAppointments);
	}

	public String uuid;
	public long appointmentSlotId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String slotType;
	public String name;
	public String serviceId;
	public long startDate;
	public long endDate;
	public int slotStartTimeHour;
	public int slotStartTimeMinute;
	public int slotEndTimeHour;
	public int slotEndTimeMinute;
	public int appointmentDuration;
	public long daysOfWeekBitwise;
	public int maxAppointments;

}