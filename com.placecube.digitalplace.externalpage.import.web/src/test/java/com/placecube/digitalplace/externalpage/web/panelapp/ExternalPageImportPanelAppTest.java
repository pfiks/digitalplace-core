package com.placecube.digitalplace.externalpage.web.panelapp;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;

public class ExternalPageImportPanelAppTest extends PowerMockito {

	private ExternalPageImportPanelApp externalPageImportPanelApp = new ExternalPageImportPanelApp();

	@Test
	public void getPortletId_ThenReturnsTheExternalPageImportPortletId() {
		String result = externalPageImportPanelApp.getPortletId();

		assertThat(result, equalTo(PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET));
	}

}
