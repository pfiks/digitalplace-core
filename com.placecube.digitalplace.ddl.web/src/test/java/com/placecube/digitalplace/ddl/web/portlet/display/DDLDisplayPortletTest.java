package com.placecube.digitalplace.ddl.web.portlet.display;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.portlet.display.configuration.DDLDisplayPortletInstanceConfiguration;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLDisplayPortletRequestConfigurationService;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLEditorService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class DDLDisplayPortletTest extends PowerMockito {

	@InjectMocks
	private DDLDisplayPortlet ddlDisplayPortlet;

	@Mock
	private DDLConfigurationService mockDDLConfigurationService;

	@Mock
	private DDLDisplayPortletInstanceConfiguration mockConfiguration;

	@Mock
	private DDLDisplayPortletRequestConfigurationService mockDDLDisplayPortletRequestConfigurationService;

	@Mock
	private DDLEditorService mockDDLEditorService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
		suppress(methods(MVCPortlet.class, "render"));
	}

	@Test
	public void render_WhenNoExceptionAndOnlyDDMStructureIsPresent_ThenConfigureRequestParametersFromConfiguration() throws PortalException, PortletException, IOException {
		long structureId = 1;
		long recordId = 0;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockDDLEditorService.getDDMStructure(structureId)).thenReturn(Optional.of(mockDDMStructure));
		when(ParamUtil.get(mockRenderRequest, "ddlRecordId", 0L)).thenReturn(recordId);
		when(mockConfiguration.ddmStructureId()).thenReturn(structureId);

		ddlDisplayPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockDDLDisplayPortletRequestConfigurationService, times(1)).setRenderRequestAttributesUsingConfiguration(mockRenderRequest, mockThemeDisplay, mockConfiguration, mockDDMStructure);

	}

	@Test
	public void render_WhenNoExceptionAndDDMStructureIsNotPresentAndDDLRecordIdInRequest_ThenConfigureRequestParametersFromDDLRecord() throws PortalException, PortletException, IOException {
		long structureId = 0;
		long recordId = 2;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockDDLEditorService.getDDMStructure(structureId)).thenReturn(Optional.empty());
		when(ParamUtil.get(mockRenderRequest, "ddlRecordId", 0L)).thenReturn(recordId);
		when(mockConfiguration.ddmStructureId()).thenReturn(structureId);

		ddlDisplayPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockDDLDisplayPortletRequestConfigurationService, times(1)).setRenderRequestAttributesUsingDDLRecord(mockRenderRequest, mockThemeDisplay, recordId);


	}

	@Test
	public void render_WhenNoExceptionAndDDMStructureIsPresentAndDDLRecordIdInRequest_ThenConfigureRequestParametersFromDDLRecord() throws PortalException, PortletException, IOException {
		long structureId = 1;
		long recordId = 2;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockDDLEditorService.getDDMStructure(structureId)).thenReturn(Optional.empty());
		when(ParamUtil.get(mockRenderRequest, "ddlRecordId", 0L)).thenReturn(recordId);
		when(mockConfiguration.ddmStructureId()).thenReturn(structureId);

		ddlDisplayPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockDDLDisplayPortletRequestConfigurationService, times(1)).setRenderRequestAttributesUsingDDLRecord(mockRenderRequest, mockThemeDisplay, recordId);
		verify(mockDDLDisplayPortletRequestConfigurationService, never()).setRenderRequestAttributesUsingConfiguration(mockRenderRequest, mockThemeDisplay, mockConfiguration, mockDDMStructure);

	}

	@Test(expected = PortletException.class)
	public void render_WhenConfigurationException_ThenThrowsPortletException() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay)).thenThrow(new ConfigurationException());

		ddlDisplayPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoErrorAndNoStructureIdOrDDLRecordIdPresent_ThenSetsInvalidConfigurationAttribute() throws Exception {
		long structureId = 0;
		long ddlRecordId = 0;

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLDisplayPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.ddmStructureId()).thenReturn(structureId);
		when(mockDDLEditorService.getDDMStructure(structureId)).thenReturn(Optional.empty());
		when(ParamUtil.get(mockRenderRequest, "ddlRecordId", 0L)).thenReturn(ddlRecordId);

		ddlDisplayPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, Boolean.TRUE);
	}


}
