/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model.impl;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

/**
 * @author Brian Wing Shun Chan
 */
public class AppointmentSlotImpl extends AppointmentSlotBaseImpl {

	@Override
	public Date getDateWithEndTime(LocalDate localDate) {
		return Date.from(localDate.atTime(getSlotEndTimeHour(), getSlotEndTimeMinute()).atZone(ZoneId.systemDefault()).toInstant());
	}

	@Override
	public Date getDateWithStartTime(LocalDate localDate) {
		return Date.from(localDate.atTime(getSlotStartTimeHour(), getSlotStartTimeMinute()).atZone(ZoneId.systemDefault()).toInstant());
	}

	@Override
	public Set<DayOfWeek> getDaysOfWeek() {
		Set<DayOfWeek> days = EnumSet.noneOf(DayOfWeek.class);
		for (DayOfWeek day : DayOfWeek.values()) {
			if ((getDaysOfWeekBitwise() & 1 << day.getValue() - 1) != 0) {
				days.add(day);
			}
		}
		return days;
	}

}