<%@ include file="/init.jsp"%>

<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><liferay-ui:message key="credit-card-validation-demo"/></title>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/main.css">
	</head>

	<body>
		<div class="container-fluid">
			<header>
				<div class="limiter">
				</div>
			</header>
			<div class="creditCardForm">
				<div class="heading">
					<h1><liferay-ui:message key="confirm-payment-of-x" arguments="${amount}"/></h1>
				</div>

				<portlet:actionURL name="<%= MVCCommandKeys.REDIRECT_ACTION_COMMAND %>" var="confirmationUrl" />

				<div class="payment">
					<aui:form action="${ confirmationUrl }" method="POST">
						<aui:input type="hidden" name="returnUrl" value="${returnUrl}" />

						<div class="form-group owner">
							<aui:input type="text" class="form-control" name="nameOnTheCard" />
						</div>
						<div class="form-group CVV">
							<aui:input type="text" class="form-control" name="cvv" />
						</div>
						<div class="form-group" id="card-number-field">
							<aui:input type="text" class="form-control" name="cardNumber" />
						</div>
						<div class="form-group" id="expiration-date">
							<label><liferay-ui:message key="expiration-date"/></label>
							<aui:select name="expiryMonth" label="">
								<aui:option value="01"><liferay-ui:message key="january"/></aui:option>
								<aui:option value="02"><liferay-ui:message key="february"/></aui:option>
								<aui:option value="03"><liferay-ui:message key="march"/></aui:option>
								<aui:option value="04"><liferay-ui:message key="april"/></aui:option>
								<aui:option value="05"><liferay-ui:message key="may"/></aui:option>
								<aui:option value="06"><liferay-ui:message key="june"/></aui:option>
								<aui:option value="07"><liferay-ui:message key="july"/></aui:option>
								<aui:option value="08"><liferay-ui:message key="august"/></aui:option>
								<aui:option value="09"><liferay-ui:message key="september"/></aui:option>
								<aui:option value="10"><liferay-ui:message key="october"/></aui:option>
								<aui:option value="11"><liferay-ui:message key="november"/></aui:option>
								<aui:option value="12"><liferay-ui:message key="december"/></aui:option>
							</aui:select>
							<aui:select name="expiryYear" label="">
								<c:forEach begin="${startYear}" end="${startYear + 5}" varStatus="loop">
									<aui:option value="${loop.index}">${loop.index}</aui:option>
								</c:forEach>
							</aui:select>
						</div>
						<div class="form-group" id="credit_cards">
							<img src="<%= request.getContextPath() %>/images/visa.jpg" id="visa">
							<img src="<%= request.getContextPath() %>/images/mastercard.jpg" id="mastercard">
							<img src="<%= request.getContextPath() %>/images/amex.jpg" id="amex">
						</div>
						<div class="form-group" id="pay-now">
							<button type="submit" class="btn btn-default" id="confirm-purchase"><liferay-ui:message key="confirm"/></button>
						</div>
					</aui:form>
				</div>
			</div>

		</div>

	</body>

</html>
