package com.placecube.digitalplace.navigation.constants;

public final class PortletKeys {

	public static final String NAVIGATION = "com_placecube_digitalplace_navigation_NavigationPortlet";

	private PortletKeys() {
		return;
	}
}
