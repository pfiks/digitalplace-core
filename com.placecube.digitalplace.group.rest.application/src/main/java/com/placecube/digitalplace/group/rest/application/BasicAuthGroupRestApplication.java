package com.placecube.digitalplace.group.rest.application;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.placecube.digitalplace.ddl.service.DynamicDataListService;
import com.placecube.digitalplace.group.rest.application.model.ModelBuilder;
import com.placecube.digitalplace.group.rest.application.endpoints.BasicAuthGroupContentRestEndpoint;
import com.placecube.digitalplace.group.rest.application.exceptions.handlers.GeneralExceptionHandler;
import com.placecube.digitalplace.group.rest.application.exceptions.handlers.GroupRestSecurityExceptionHandler;
import com.placecube.digitalplace.group.rest.application.filters.BasicAuthCheckerFilter;
import com.placecube.digitalplace.group.rest.application.filters.DynamicDataListEntryPropertyFilter;
import com.placecube.digitalplace.group.rest.application.service.GroupRestRetrievalService;

@ApplicationPath("/ba-groups")
@Component(immediate = true, property = { "jaxrs.application=true" }, service = Application.class)
public class BasicAuthGroupRestApplication extends Application {

	@Reference
	private DynamicDataListService dynamicDataListService;

	@Reference
	private GroupRestRetrievalService groupRestRetrievalService;

	@Reference
	private ModelBuilder modelBuilder;

	public DynamicDataListService getDynamicDataListService() {
		return dynamicDataListService;
	}

	public GroupRestRetrievalService getGroupRestRetrievalService() {
		return groupRestRetrievalService;
	}

	public ModelBuilder getModelBuilder() {
		return modelBuilder;
	}

	@Override
	public Set<Object> getSingletons() {
		// These need to be in this order

		Set<Object> singletons = new LinkedHashSet<>();

		// Filters
		singletons.add(new BasicAuthCheckerFilter(this));

		// Jackson marshaller for JSON
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setFilterProvider(new SimpleFilterProvider().addFilter("FilterDynamicDataListEntry", new DynamicDataListEntryPropertyFilter()));
		singletons.add(new JacksonJsonProvider(objectMapper));

		// Exception handlers
		singletons.add(new GroupRestSecurityExceptionHandler());
		singletons.add(new GeneralExceptionHandler(this));

		// REST endpoints
		singletons.add(new BasicAuthGroupContentRestEndpoint(this));

		return singletons;
	}



}