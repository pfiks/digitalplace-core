package com.placecube.digitalplace.fileupload.exception;

import com.liferay.portal.kernel.exception.PortalException;

public class FileUploadException extends PortalException {

	private static final long serialVersionUID = 1L;

	public FileUploadException() {
		super();
	}

	public FileUploadException(String msg) {
		super(msg);
	}

	public FileUploadException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public FileUploadException(Throwable cause) {
		super(cause);
	}
}
