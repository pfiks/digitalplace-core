package com.placecube.digitalplace.payment.heycentric.service.internal.arp;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentLine;

@Component(immediate = true, service = HeycentricARPLineService.class)
public class HeycentricARPLineService {

	@Reference
	private JSONFactory jsonFactory;

	public JSONArray agentReferredPaymentLinesToJSON(AgentReferredPaymentContext agentReferredPaymentContext) {
		JSONArray lineItems = jsonFactory.createJSONArray();
		agentReferredPaymentContext.getPaymentLines().stream().forEach(l -> lineItems.put(agentReferredPaymentLineToJSONLineItem(l)));
		return lineItems;
	}

	private JSONObject agentReferredPaymentLineToJSONLineItem(AgentReferredPaymentLine agentReferredPaymentLine) {
		JSONObject lineItem = jsonFactory.createJSONObject();

		lineItem.put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEM_PMTTYP, agentReferredPaymentLine.getRevenueCode());
		lineItem.put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEM_VAL1, agentReferredPaymentLine.getReferenceNumber());
		lineItem.put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEM_VAL2, agentReferredPaymentLine.getExtraPlaceholder());
		lineItem.put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEM_AM, agentReferredPaymentLine.getAmount());

		return lineItem;
	}
}
