package com.placecube.digitalplace.form.exception;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FormDefinitionExceptionTest {

	private static final String TEST_MESSAGE = "Test message";

	@Test
	public void testFormDefinitionException_WithMessage() {
		FormDefinitionException formDefinitionException = new FormDefinitionException(TEST_MESSAGE);

		assertEquals(TEST_MESSAGE, formDefinitionException.getMessage());
	}

	@Test
	public void testFormDefinitionException_WithCause() {
		NullPointerException nullPointerException = new NullPointerException();
		FormDefinitionException formDefinitionException = new FormDefinitionException(nullPointerException);

		assertEquals("java.lang.NullPointerException", formDefinitionException.getMessage());
		assertEquals(nullPointerException, formDefinitionException.getCause());
	}

	@Test
	public void testFormDefinitionException_WithMessageAndCause() {
		NullPointerException nullPointerException = new NullPointerException();
		FormDefinitionException formDefinitionException = new FormDefinitionException(TEST_MESSAGE, nullPointerException);

		assertEquals(TEST_MESSAGE, formDefinitionException.getMessage());
		assertEquals(nullPointerException, formDefinitionException.getCause());
	}

}
