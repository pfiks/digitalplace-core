package com.placecube.digitalplace.search.shared.service.impl;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.internal.util.RequestUtil;
import com.placecube.digitalplace.search.shared.constants.SharedSearchDisplayCostants;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchDisplayService;

@Component(immediate = true, service = SharedSearchDisplayService.class)
public class SharedSearchDisplayServiceImpl implements SharedSearchDisplayService {

	private static final String DISPLAY_STYLE = "displayStyle";

	@Reference
	private RequestUtil requestUtil;

	@Override
	public String getDisplayStyle(PortletRequest portletRequest, String defaultStyle) {
		HttpServletRequest httpServletRequest = requestUtil.getOriginalHttpServletRequest(portletRequest);
		return ParamUtil.getString(httpServletRequest, DISPLAY_STYLE, Validator.isNull(defaultStyle) ? SharedSearchDisplayCostants.DISPLAY_STYLE_LIST : defaultStyle);
	}

	@Override
	public String getDisplayStyleGridURL(SharedSearchResponse searchResponse) {
		String iteratorURL = searchResponse.getIteratorURL();
		return HttpComponentsUtil.setParameter(iteratorURL, DISPLAY_STYLE, SharedSearchDisplayCostants.DISPLAY_STYLE_GRID);
	}

	@Override
	public String getDisplayStyleListURL(SharedSearchResponse searchResponse) {
		String iteratorURL = searchResponse.getIteratorURL();
		return HttpComponentsUtil.setParameter(iteratorURL, DISPLAY_STYLE, SharedSearchDisplayCostants.DISPLAY_STYLE_LIST);
	}

	@Override
	public boolean isDisplayStyleList(PortletRequest portletRequest) {
		HttpServletRequest httpServletRequest = requestUtil.getOriginalHttpServletRequest(portletRequest);
		String displayStyle = ParamUtil.getString(httpServletRequest, DISPLAY_STYLE, SharedSearchDisplayCostants.DISPLAY_STYLE_LIST);
		return SharedSearchDisplayCostants.DISPLAY_STYLE_LIST.equalsIgnoreCase(displayStyle);
	}

	@Override
	public boolean isDisplayStyleList(PortletRequest portletRequest, String defaultValue) {
		String defaultDisplayStyle = Validator.isNotNull(defaultValue) ? defaultValue : SharedSearchDisplayCostants.DISPLAY_STYLE_LIST;
		HttpServletRequest httpServletRequest = requestUtil.getOriginalHttpServletRequest(portletRequest);
		String displayStyle = ParamUtil.getString(httpServletRequest, DISPLAY_STYLE, defaultDisplayStyle);
		return SharedSearchDisplayCostants.DISPLAY_STYLE_LIST.equalsIgnoreCase(displayStyle);
	}
}
