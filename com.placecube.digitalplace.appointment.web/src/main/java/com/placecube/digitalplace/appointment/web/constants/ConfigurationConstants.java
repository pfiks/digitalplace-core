package com.placecube.digitalplace.appointment.web.constants;

public final class ConfigurationConstants {

	public static final String CONFIG_PID = "com.placecube.digitalplace.appointment.web.configuration.AppointmentDashboardPortletInstanceConfiguration";

	private ConfigurationConstants() {
	}
}