package com.placecube.digitalplace.calendar.web.service;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarEvent;
import com.placecube.digitalplace.common.portlet.PortletURLService;

@Component(immediate = true, service = DigitalPlaceCalendarEventParserService.class)
public class DigitalPlaceCalendarEventParserService {

	@Reference
	private PortletURLService portletURLService;

	public JSONArray parseCalendarEvents(List<DigitalPlaceCalendarEvent> calendarEvents, String currentURL) {
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		for (DigitalPlaceCalendarEvent calendarEvent : calendarEvents) {
			jsonArray.put(parseCalendarEvent(calendarEvent, currentURL));
		}
		return jsonArray;
	}

	private JSONObject parseCalendarEvent(DigitalPlaceCalendarEvent calendarEvent, String currentURL) {
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("title", calendarEvent.getTitle());
		jsonObject.put("start", calendarEvent.getStart());
		jsonObject.put("backgroundColor", calendarEvent.getBackgroundColour());
		jsonObject.put("borderColor", calendarEvent.getBackgroundColour());
		jsonObject.put("textColor", calendarEvent.getTextColour());

		if (Validator.isNotNull(calendarEvent.getEnd())) {
			jsonObject.put("end", calendarEvent.getEnd());
		}
		if (Validator.isNotNull(calendarEvent.getViewURL())) {
			String viewURLWithRedirect = portletURLService.getURLWithRedirectParameter(calendarEvent.getViewURL(), currentURL);
			jsonObject.put("url", viewURLWithRedirect);
		}
		return jsonObject;
	}
}
