package com.placecube.digitalplace.search.internal.task;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.util.Props;
import com.liferay.portal.kernel.util.PropsKeys;
import com.placecube.digitalplace.search.internal.util.RequestUtil;

/*
 * This class is a copy of the Liferay portal-search-web com.liferay.portal.search.web.internal.portlet.shared.task.PortletSharedTaskExecutorImpl
 */
@Component(immediate = true, service = SharedSearchTaskExecutor.class)
public class SharedSearchTaskExecutor {

	@Reference
	private Props props;

	private String requestSharedAttribute;

	@Reference
	private RequestUtil requestUtil;

	public <T> T executeOnlyOnce(SharedSearchTask<T> sharedSearchTask, String attributeSuffix, RenderRequest renderRequest) {

		String attributeName = requestSharedAttribute.concat(attributeSuffix);

		Optional<FutureTask<T>> oldFutureTaskOptional;
		FutureTask<T> futureTask;

		synchronized (renderRequest) {
			HttpServletRequest originalHttpServletRequest = requestUtil.getOriginalHttpServletRequest(renderRequest);

			oldFutureTaskOptional = Optional.ofNullable((FutureTask<T>) originalHttpServletRequest.getAttribute(attributeName));

			futureTask = oldFutureTaskOptional.orElseGet(() -> {
				FutureTask<T> newFutureTask = new FutureTask<>(sharedSearchTask::execute);

				originalHttpServletRequest.setAttribute(attributeName, newFutureTask);

				return newFutureTask;
			});
		}

		if (!oldFutureTaskOptional.isPresent()) {
			futureTask.run();
		}

		try {
			return futureTask.get();
		} catch (ExecutionException | InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	@Activate
	protected void activate() {
		String[] requestSharedAttributes = props.getArray(PropsKeys.REQUEST_SHARED_ATTRIBUTES);

		Arrays.sort(requestSharedAttributes);

		requestSharedAttribute = requestSharedAttributes[0];
	}

}