package com.placecube.digitalplace.payment.heycentric.service.internal.arp;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentContext;
import com.placecube.digitalplace.payment.model.AgentReferredPaymentLine;

@RunWith(MockitoJUnitRunner.class)
public class HeycentricARPLineServiceTest {

	@InjectMocks
	private HeycentricARPLineService heycentricARPLineService;

	@Mock
	private AgentReferredPaymentContext mockAgentReferredPaymentContext;

	@Mock
	private AgentReferredPaymentLine mockAgentReferredPaymentLine;

	@Mock
	private JSONArray mockJsonArray;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObject;

	private static final String REVENUE_CODE = "revenueCode";
	private static final String REFERENCE_NUMBER = "referenceNumber";
	private static final String EXTRA_PLACEHOLDER = "extraPlaceholder";
	private static final String AMOUNT = "100";

	@Test
	public void agentReferredPaymentLinesToJSON_WhenNoErrors_ThenReturnsJSONArrayContainingLineItems() {
		when(mockJsonFactory.createJSONArray()).thenReturn(mockJsonArray);
		when(mockJsonFactory.createJSONObject()).thenReturn(mockJsonObject);

		when(mockAgentReferredPaymentContext.getPaymentLines()).thenReturn(Arrays.asList(mockAgentReferredPaymentLine));

		when(mockAgentReferredPaymentLine.getRevenueCode()).thenReturn(REVENUE_CODE);
		when(mockAgentReferredPaymentLine.getReferenceNumber()).thenReturn(REFERENCE_NUMBER);
		when(mockAgentReferredPaymentLine.getExtraPlaceholder()).thenReturn(EXTRA_PLACEHOLDER);
		when(mockAgentReferredPaymentLine.getAmount()).thenReturn(AMOUNT);

		JSONArray result = heycentricARPLineService.agentReferredPaymentLinesToJSON(mockAgentReferredPaymentContext);
		assertThat(result, sameInstance(mockJsonArray));

		verify(mockJsonArray, times(1)).put(mockJsonObject);

		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEM_PMTTYP, REVENUE_CODE);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEM_VAL1, REFERENCE_NUMBER);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEM_VAL2, EXTRA_PLACEHOLDER);
		verify(mockJsonObject, times(1)).put(HeycentricPaymentConstants.JSON_PAYLOAD_LINE_ITEM_AM, AMOUNT);
	}
}