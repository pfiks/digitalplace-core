package com.placecube.digitalplace.twitterfeed.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.support.membermodification.MemberModifier.suppress;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.portlet.BaseJSPSettingsConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.portlet.PortletConfigFactoryUtil;
import com.liferay.portal.kernel.service.PortletLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.twitterfeed.configuration.TwitterFeedConfigurationUtil;
import com.placecube.digitalplace.twitterfeed.configuration.TwitterFeedPortletInstanceConfiguration;
import com.placecube.digitalplace.twitterfeed.constants.PortletRequestKeys;

@PrepareForTest({ PropsUtil.class, ParamUtil.class, DefaultConfigurationAction.class, PortletLocalServiceUtil.class, PortletConfigFactoryUtil.class, ParamUtil.class })
@RunWith(PowerMockRunner.class)
public class TwitterFeedPortletConfigurationActionTest {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Portlet mockPortlet;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private RequestDispatcher mockRequestDispatcher;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TwitterFeedConfigurationUtil mockTwitterFeedConfigurationUtil;

	@Mock
	private TwitterFeedPortletInstanceConfiguration mockTwitterFeedPortletInstanceConfiguration;

	@InjectMocks
	private TwitterFeedPortletConfigurationAction twitterFeedPortletConfigurationAction;

	@Test
	public void include_WhenNoError_ThenAddsConfigurationAsRequestAttribute() throws Exception {
		mockCallToSuper();

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockTwitterFeedConfigurationUtil.getConfiguration(mockThemeDisplay)).thenReturn(mockTwitterFeedPortletInstanceConfiguration);

		twitterFeedPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(PortletRequestKeys.CONFIGURATION, mockTwitterFeedPortletInstanceConfiguration);
	}

	@Test
	public void processAction_WhenNoErrors_ThenSavesTheSelectedNumberOfFeedsToShowInPreferences() throws Exception {
		twitterFeedPortletConfigurationAction = Mockito.spy(TwitterFeedPortletConfigurationAction.class);

		Long expected = 123l;
		when(ParamUtil.getLong(mockActionRequest, PortletRequestKeys.FEEDS_TO_SHOW, 0)).thenReturn(expected);

		twitterFeedPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(twitterFeedPortletConfigurationAction, times(1)).setPreference(mockActionRequest, PortletRequestKeys.FEEDS_TO_SHOW, String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoErrors_ThenSavesTheSelectedTwitterEmbeddedInPreferences() throws Exception {
		twitterFeedPortletConfigurationAction = Mockito.spy(TwitterFeedPortletConfigurationAction.class);
		String expected = "myExpectedValue";
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TWITTER_EMBEDDED, StringPool.BLANK)).thenReturn(expected);

		twitterFeedPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(twitterFeedPortletConfigurationAction, times(1)).setPreference(mockActionRequest, PortletRequestKeys.TWITTER_EMBEDDED, expected);
	}

	@Test
	public void processAction_WhenNoErrors_ThenSavesTheSelectedTwitterUsernameInPreferences() throws Exception {
		twitterFeedPortletConfigurationAction = Mockito.spy(TwitterFeedPortletConfigurationAction.class);

		String expected = "myExpectedValue";
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.TWITTER_USERNAME, StringPool.BLANK)).thenReturn(expected);

		twitterFeedPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(twitterFeedPortletConfigurationAction, times(1)).setPreference(mockActionRequest, PortletRequestKeys.TWITTER_USERNAME, expected);
	}

	@Before
	public void setUp() throws ServletException, IOException {
		initMocks(this);

		mockStatic(PropsUtil.class, ParamUtil.class, DefaultConfigurationAction.class, PortletLocalServiceUtil.class, PortletConfigFactoryUtil.class, ParamUtil.class);
		when(ParamUtil.getString(any(ActionRequest.class), eq(Constants.CMD))).thenReturn("");

		when(mockPortletConfig.getInitParameter(anyString())).thenReturn("test");
		when(PortletLocalServiceUtil.getPortletById(anyLong(), anyString())).thenReturn(mockPortlet);
		when(PortletConfigFactoryUtil.create(any(Portlet.class), any(ServletContext.class))).thenReturn(mockPortletConfig);

		Mockito.doNothing().when(mockRequestDispatcher).include(any(ServletRequest.class), any(ServletResponse.class));
		when(mockServletContext.getRequestDispatcher(anyString())).thenReturn(mockRequestDispatcher);

		when(mockHttpServletRequest.getAttribute(WebKeys.CTX)).thenReturn(mockServletContext);
	}

	@SuppressWarnings("rawtypes")
	private void mockCallToSuper() throws NoSuchMethodException {
		Class[] cArg = new Class[3];
		cArg[0] = PortletConfig.class;
		cArg[1] = HttpServletRequest.class;
		cArg[2] = HttpServletResponse.class;
		Method superIncludeMethod = BaseJSPSettingsConfigurationAction.class.getMethod("include", cArg);
		suppress(superIncludeMethod);
	}

}
