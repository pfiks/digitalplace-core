package com.placecube.digitalplace.externalpage.web.portlet;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SessionErrors.class, PropsUtil.class, ParamUtil.class })
public class ExternalPageClearAllMVCActionCommandTest extends PowerMockito {

	private static final long SCOPE_GROUP_ID = 123;

	@InjectMocks
	private ExternalPageClearAllMVCActionCommand externalPageClearAllMVCActionCommand;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(SessionErrors.class, PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenGroupIdIsValid_ThenRemoveExternalPagesbyGroupId() throws Exception {
		mockThemeDisplayDetails();

		externalPageClearAllMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockExternalPageLocalService, times(1)).removeExternalPagesByGroupId(SCOPE_GROUP_ID);
	}

	@Test
	public void doProcessAction_WhenExcptionRemovingExternalPagesbyGroupId_ThenSetsSessionError() throws Exception {
		mockThemeDisplayDetails();
		doThrow(new RuntimeException()).when(mockExternalPageLocalService).removeExternalPagesByGroupId(SCOPE_GROUP_ID);

		externalPageClearAllMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "externalPageImportError");
		verifyNoMoreInteractions(SessionErrors.class);
	}

	private void mockThemeDisplayDetails() {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
	}
}