package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;

import javax.portlet.PortletPreferences;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration.DDLFieldFacetPortletConfigurationConstants;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;

@Component(immediate = true, service = DDLFieldFacetRequestService.class)
public class DDLFieldFacetRequestService {

	public String getConfiguredFieldNameValueFromUrl(ThemeDisplay themeDisplay, String fieldName, String preselectedValue) {
		return ParamUtil.getString(themeDisplay.getRequest(), fieldName, preselectedValue);
	}

	public String getEscapedRedirectUrlWithoutFieldUrlParameter(SharedSearchResponse searchResponse, PortletPreferences portletPreferences) throws URISyntaxException {
		String iteratorUrl = searchResponse.getIteratorURL();

		String fieldName = portletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.FIELD_NAME, StringPool.BLANK);

		URIBuilder uriBuilder = new URIBuilder(iteratorUrl);

		List<NameValuePair> queryParameters = uriBuilder.getQueryParams();

		removeParameterFromQueryParameters(queryParameters, fieldName);

		uriBuilder.setParameters(queryParameters);

		return HtmlUtil.escape(uriBuilder.build().toString());
	}

	public boolean isConfiguredFieldNameValueInUrl(ThemeDisplay themeDisplay, PortletPreferences portletPreferences) {
		String fieldName = portletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.FIELD_NAME, StringPool.BLANK);
		final String preselectedValue = portletPreferences.getValue(DDLFieldFacetPortletConfigurationConstants.PRESELECTED_VALUE, StringPool.BLANK);

		String value = getConfiguredFieldNameValueFromUrl(themeDisplay, fieldName, preselectedValue);

		return Validator.isNotNull(value);
	}

	private void removeParameterFromQueryParameters(List<NameValuePair> queryParameters, String parameterName) {
		Iterator<NameValuePair> queryParameterIterator = queryParameters.iterator();

		while (queryParameterIterator.hasNext()) {
			NameValuePair queryParameter = queryParameterIterator.next();
			if (queryParameter.getName().equals(parameterName)) {
				queryParameterIterator.remove();
			}
		}

	}

}
