package com.placecube.digitalplace.payment.capitainterface.service;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.capita_software_services.www.portal_api.PanEntryMethod;
import com.capita_software_services.www.scp.base.AdditionalInstructions;
import com.capita_software_services.www.scp.base.BillingDetails;
import com.capita_software_services.www.scp.base.LgSaleDetails;
import com.capita_software_services.www.scp.base.RequestType;
import com.capita_software_services.www.scp.base.ScpInvokeResponse;
import com.capita_software_services.www.scp.base.ScpQueryRequest;
import com.capita_software_services.www.scp.simple.Scp;
import com.capita_software_services.www.scp.simple.ScpServiceLocator;
import com.capita_software_services.www.scp.simple.ScpSimpleInvokeRequest;
import com.capita_software_services.www.scp.simple.ScpSimpleQueryResponse;
import com.capita_software_services.www.scp.simple.SimpleSale;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.DataField;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.DiagnosticData;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.ErrorCode;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.LoggingBean;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.Service;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.SourceSystem;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.service.LoggingFormatService;
import com.placecube.digitalplace.payment.capitainterface.models.OneOffPaymentOrder;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;
import com.placecube.digitalplace.payment.model.PaymentContext;

import uk.co.capita_software.support.selfservice.Credentials;

/**
 * Service/facade class onto the Capita payment gateway
 *
 */
public class CapitaPaymentService {

	private static final Log LOG = LogFactoryUtil.getLog(CapitaPaymentService.class);
	private BuildCapitaElementsService capitaElementsService;
	private Scp capitaPort;

	private BuildCredentialsService credentialsService;

	private final PaymentProperties properties;

	/**
	 * Constructor
	 *
	 * @throws ServiceException if the initialisation of the connection to the
	 *             payment portal fails
	 */
	public CapitaPaymentService(PaymentProperties properties) throws ServiceException {

		ScpServiceLocator capitaService = new ScpServiceLocator();
		capitaService.setEndpointAddress(new javax.xml.namespace.QName(CapitaConstants.CAPITA_NAMESPACE, CapitaConstants.CAPITA_NAMESPACE_LOCAL_PART), properties.getEndpoint());
		capitaPort = capitaService.getscpSoap11();
		credentialsService = new BuildCredentialsService(properties);
		capitaElementsService = new BuildCapitaElementsService(properties);
		this.properties = properties;
	}

	/**
	 * Invokes a one off payment, based on the parameters passed in
	 *
	 * @param order a one off payment order
	 * @param billingDetails includes stored card details and email address
	 * @param userId the userId
	 * @param isImpersonated if the user is being impersonated
	 *
	 * @return the SCP invoke response
	 */
	public ScpInvokeResponse capitaInvoke(OneOffPaymentOrder order, BillingDetails billingDetails, long userId, boolean isImpersonated) {

		ScpInvokeResponse response = null;
		if (order == null) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0056, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.MISSING_MANDATORY_FIELD, "ALL"))));

		} else {

			if (Validator.isNull(order.getReturnURL())) {
				LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0056, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
						new DiagnosticData(DataField.MISSING_MANDATORY_FIELD, "returnURL"))));

			} else {

				ScpSimpleInvokeRequest scpSimpleInvokeRequest = new ScpSimpleInvokeRequest();

				Credentials creds = credentialsService.buildCredentials(billingDetails.getCardHolderDetails().getContact().getEmail());
				LOG.debug("Credentials :" + creds);

				// Only proceed if the credentials are valid
				if (creds.getRequestIdentification() == null || Validator.isNull(creds.getRequestIdentification().getTimeStamp())
						|| Validator.isNull(creds.getRequestIdentification().getUniqueReference())) {
					LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0059, "")));

				} else {

					response = buildInvokeRequestAndSend(order, billingDetails, scpSimpleInvokeRequest, creds, userId, isImpersonated);
				}
			}
		}

		return response;
	}

	/**
	 * Queries the Capita system for an update on the transaction with scp
	 * reference scpReference
	 *
	 * @param scpReference returned by the previous invoke call and is the
	 *            unique reference to track the request with Capita
	 * @param customerReference unique reference used to generate a unique
	 *            transaction id
	 * @return ScpSimpleQueryResponse the SCP simple query response
	 */
	public ScpSimpleQueryResponse capitaQuery(String scpReference, String customerReference) {

		Credentials creds = credentialsService.buildCredentials(customerReference);
		ScpQueryRequest request = new ScpQueryRequest();
		request.setCredentials(creds);
		request.setScpReference(scpReference);
		request.setSiteId(properties.getSiteId());

		try {
			return capitaPort.scpSimpleQuery(request);
		} catch (RemoteException e) {
			LOG.trace(e.getStackTrace());
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0058, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.CAPITA_TX_REF_NO, scpReference))), e);

		}

		return null;
	}

	/**
	 * Override the endpoint of the Capita web service
	 *
	 * @param capitaEndpoint the capita endpoint override
	 * @throws ServiceException a ServiceException
	 */
	public void setCapitaEndpoint(String capitaEndpoint) throws ServiceException {
		ScpServiceLocator capitaService = new ScpServiceLocator();
		capitaService.setEndpointAddress(new javax.xml.namespace.QName(CapitaConstants.CAPITA_NAMESPACE, CapitaConstants.CAPITA_NAMESPACE_LOCAL_PART), capitaEndpoint);
		capitaPort = capitaService.getscpSoap11();
	}

	private ScpInvokeResponse buildInvokeRequestAndSend(OneOffPaymentOrder order, BillingDetails billingDetails, ScpSimpleInvokeRequest scpSimpleInvokeRequest, Credentials creds, long userId, boolean isImpersonated) {

		ScpInvokeResponse response = null;

		if (order.getRequestType() == null) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0056, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.MISSING_MANDATORY_FIELD, "RequestType"))));

		} else {
			if (order.getRequestType().equals(OneOffPaymentOrder.CardRequestType.PAY_AND_STORE_CARD)) {

				scpSimpleInvokeRequest.setRequestType(RequestType.payAndStore);
			} else {
				scpSimpleInvokeRequest.setRequestType(RequestType.payOnly);
			}

			SimpleSale simpleSale;

			scpSimpleInvokeRequest.setCredentials(creds);

			setAdditionalInstructions(scpSimpleInvokeRequest, order.getSimpleSaleConfiguration().getPaymentContext());

			simpleSale = capitaElementsService.buildSale(order.getSaleDescription(), order.getSaleAmount(), order.getSaleReferenceCode(), order.getSimpleSaleConfiguration());

			if (simpleSale != null) {

				setPanEntryMethod(scpSimpleInvokeRequest, simpleSale, order.getSimpleSaleConfiguration().getPaymentContext(), userId, isImpersonated);

				scpSimpleInvokeRequest.setRequestId(capitaElementsService.generateToken(order.getSaleReferenceCode()));
				scpSimpleInvokeRequest.setRouting(capitaElementsService.buildRouting(order.getReturnURL(), order.getBackURL()));

				setDigitalPlaceSourceCode(simpleSale);
				scpSimpleInvokeRequest.setSale(simpleSale);

				if (!(scpSimpleInvokeRequest.getRequestId() == null || scpSimpleInvokeRequest.getRouting() == null || scpSimpleInvokeRequest.getSale() == null)) {
					// Error that we will have already logged, return null

					scpSimpleInvokeRequest.setBilling(billingDetails);

					try {

						response = capitaPort.scpSimpleInvoke(scpSimpleInvokeRequest);

						LOG.debug("ScpInvokeResponse response " + response);

					} catch (RemoteException e) {
						LOG.trace(e.getStackTrace());
						LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0057, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
								new DiagnosticData(DataField.CAPITA_REF_NO, order.getSaleReferenceCode()))), e);

					}
				}
			}
		}

		return response;
	}

	private void setAdditionalInstructions(ScpSimpleInvokeRequest request, PaymentContext paymentContext) {
		String merchantCode = GetterUtil.getString(paymentContext.getAdvancedConfigurationValue(CapitaConstants.ADDITIONAL_INSTRUCTIONS_MERCHANT_CODE));

		if (Validator.isNotNull(merchantCode)) {

			AdditionalInstructions additionalInstructions = new AdditionalInstructions();
			additionalInstructions.setMerchantCode(merchantCode);

			request.setAdditionalInstructions(additionalInstructions);
		}
	}

	private void setDigitalPlaceSourceCode(SimpleSale simpleSale) {
		LgSaleDetails lgSaleDetails = simpleSale.getLgSaleDetails();
		if (Validator.isNull(lgSaleDetails)) {
			lgSaleDetails = new LgSaleDetails();
		}
		lgSaleDetails.setSourceCode(CapitaConstants.SOURCE_CODE_DP);
		simpleSale.setLgSaleDetails(lgSaleDetails);
	}

	private void setPanEntryMethod(ScpSimpleInvokeRequest scpSimpleInvokeRequest, SimpleSale simpleSale, PaymentContext paymentContext, long userId, boolean isImpersonated) {
		String panEntryMethodOverride = GetterUtil.getString(paymentContext.getAdvancedConfigurationValue(CapitaConstants.PAN_ENTRY_METHOD));

		if (Validator.isNotNull(panEntryMethodOverride)) {

			if (PanEntryMethod.CNP.getValue().equalsIgnoreCase(panEntryMethodOverride)) {
				scpSimpleInvokeRequest.setPanEntryMethod(capitaElementsService.createCNPPanEntryMethod());
				simpleSale.setLgSaleDetails(capitaElementsService.buildLgSaleDetailsForCNP(userId));
				return;
			}
			if (PanEntryMethod.ECOM.getValue().equalsIgnoreCase(panEntryMethodOverride)) {
				scpSimpleInvokeRequest.setPanEntryMethod(capitaElementsService.createECOMPanEntryMethod());
				return;
			}

			LOG.debug("panEntryMethod overridden with incorrect value. Falling back on user impersonation logic");
		}

		if (isImpersonated) {
			scpSimpleInvokeRequest.setPanEntryMethod(capitaElementsService.createCNPPanEntryMethod());
			simpleSale.setLgSaleDetails(capitaElementsService.buildLgSaleDetailsForCNP(userId));
		} else {
			scpSimpleInvokeRequest.setPanEntryMethod(capitaElementsService.createECOMPanEntryMethod());
		}


	}
}
