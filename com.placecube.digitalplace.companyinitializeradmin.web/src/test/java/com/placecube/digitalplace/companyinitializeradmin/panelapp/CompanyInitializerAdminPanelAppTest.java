package com.placecube.digitalplace.companyinitializeradmin.panelapp;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.companyinitializeradmin.constants.PortletKeys;

@RunWith(PowerMockRunner.class)
public class CompanyInitializerAdminPanelAppTest extends PowerMockito {

	private CompanyInitializerAdminPanelApp companyInitializerAdminPanelApp;

	@Mock
	private Group mockGroup;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Before
	public void activeSetUp() {
		companyInitializerAdminPanelApp = new CompanyInitializerAdminPanelApp();
	}

	@Test
	public void getPortletId_ThenReturnsThePortletId() {
		String result = companyInitializerAdminPanelApp.getPortletId();
		assertThat(result, equalTo(PortletKeys.COMPANY_INITIALIZER_ADMIN));
	}
}
