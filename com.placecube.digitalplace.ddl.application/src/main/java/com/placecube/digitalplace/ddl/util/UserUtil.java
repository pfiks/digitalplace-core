package com.placecube.digitalplace.ddl.util;

import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;

public class UserUtil {

	public static String getAge(User user) throws PortalException {

		Date now = new Date();
		long timeBetween = now.getTime() - user.getBirthday().getTime();
		double yearsBetween = timeBetween / 3.15576e+10;
		int age = (int) Math.floor(yearsBetween);

		return String.valueOf(age);
	}

	public static String getExpandoUserValues(User user, String key) {
		String value = "";
		try {
			return user.getExpandoBridge().getAttribute(key).toString();
		} catch (Exception e) {
			//
		}

		return value;
	}

	public static String getPin(User user) {
		AtomicReference<String> value = new AtomicReference<>("");

		user.getAddresses().forEach(address -> {
			if (address.isPrimary()) {
				value.set(address.getZip());
			}
		});

		return value.get();
	}

	public static String getPrimeryPhone(User user) {
		AtomicReference<String> value = new AtomicReference<>("");
		user.getPhones().forEach(phone -> {
			if (phone.isPrimary()) {
				value.set(phone.getNumber());
			}
		});
		return value.get();
	}

}
