package com.placecube.digitalplace.addresslookup.simple.internal.lookup.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "connectors", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.addresslookup.simple.internal.lookup.configuration.SimpleAddressLookupCompanyConfiguration", localization = "content/Language", name = "address-lookup-simple", description = "address-lookup-simple-description")
public interface SimpleAddressLookupCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "enabled")
	boolean enabled();

	@Meta.AD(required = false, deflt = "false", name = "national", description = "national-description")
	boolean national();

	@Meta.AD(required = false, deflt = "1", name = "weight", description = "weight-description")
	Integer weight();

}
