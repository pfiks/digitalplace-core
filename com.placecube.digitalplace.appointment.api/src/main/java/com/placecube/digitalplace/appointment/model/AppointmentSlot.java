/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the AppointmentSlot service. Represents a row in the &quot;Placecube_Appointment_AppointmentSlot&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentSlotModel
 * @generated
 */
@ImplementationClassName(
	"com.placecube.digitalplace.appointment.model.impl.AppointmentSlotImpl"
)
@ProviderType
public interface AppointmentSlot extends AppointmentSlotModel, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>com.placecube.digitalplace.appointment.model.impl.AppointmentSlotImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<AppointmentSlot, Long>
		APPOINTMENT_SLOT_ID_ACCESSOR = new Accessor<AppointmentSlot, Long>() {

			@Override
			public Long get(AppointmentSlot appointmentSlot) {
				return appointmentSlot.getAppointmentSlotId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<AppointmentSlot> getTypeClass() {
				return AppointmentSlot.class;
			}

		};

	public java.util.Date getDateWithEndTime(java.time.LocalDate localDate);

	public java.util.Date getDateWithStartTime(java.time.LocalDate localDate);

	public java.util.Set<java.time.DayOfWeek> getDaysOfWeek();

}