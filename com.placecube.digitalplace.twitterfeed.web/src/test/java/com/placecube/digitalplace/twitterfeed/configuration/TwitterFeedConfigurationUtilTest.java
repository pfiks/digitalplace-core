package com.placecube.digitalplace.twitterfeed.configuration;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public class TwitterFeedConfigurationUtilTest {

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TwitterFeedPortletInstanceConfiguration mockTwitterFeedPortletInstanceConfiguration;

	@InjectMocks
	private TwitterFeedConfigurationUtil twitterFeedConfigurationUtil;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test(expected = ConfigurationException.class)
	public void getConfiguration_WhenException_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TwitterFeedPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		twitterFeedConfigurationUtil.getConfiguration(mockThemeDisplay);
	}

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsTwitterFeedPortletInstanceConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TwitterFeedPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTwitterFeedPortletInstanceConfiguration);

		TwitterFeedPortletInstanceConfiguration result = twitterFeedConfigurationUtil.getConfiguration(mockThemeDisplay);

		assertThat(result, sameInstance(mockTwitterFeedPortletInstanceConfiguration));
	}

	@Test
	public void getValidConfiguration_WhenExceptionRetrievingConfiguration_ThenReturnsEmptyOptional() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TwitterFeedPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		Optional<TwitterFeedPortletInstanceConfiguration> result = twitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getValidConfiguration_WhenNumberOfFeedsToShowIsGreaterThanZeroAndUsernameHasValue_ThenReturnsOptionalWithTheConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TwitterFeedPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTwitterFeedPortletInstanceConfiguration);
		when(mockTwitterFeedPortletInstanceConfiguration.feedsToShow()).thenReturn(1);
		when(mockTwitterFeedPortletInstanceConfiguration.twitterUsername()).thenReturn("a");

		Optional<TwitterFeedPortletInstanceConfiguration> result = twitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay);

		assertThat(result.get(), sameInstance(mockTwitterFeedPortletInstanceConfiguration));
	}

	@Test
	public void getValidConfiguration_WhenNumberOfFeedsToShowIsGreaterThanZeroAndUsernameIsEmptyAndHasTwitterEmbeddedValueConfigured_ThenReturnsOptionalWithConfiguration()
			throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TwitterFeedPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTwitterFeedPortletInstanceConfiguration);
		when(mockTwitterFeedPortletInstanceConfiguration.feedsToShow()).thenReturn(1);
		when(mockTwitterFeedPortletInstanceConfiguration.twitterUsername()).thenReturn(StringPool.THREE_SPACES);
		when(mockTwitterFeedPortletInstanceConfiguration.twitterEmbedded()).thenReturn("embedValue");

		Optional<TwitterFeedPortletInstanceConfiguration> result = twitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay);

		assertThat(result.get(), sameInstance(mockTwitterFeedPortletInstanceConfiguration));
	}

	@Test
	public void getValidConfiguration_WhenNumberOfFeedsToShowIsGreaterThanZeroAndUsernameIsEmptyAndNoTwitterEmbeddedConfigured_ThenReturnsEmptyOptional() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TwitterFeedPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTwitterFeedPortletInstanceConfiguration);
		when(mockTwitterFeedPortletInstanceConfiguration.feedsToShow()).thenReturn(1);
		when(mockTwitterFeedPortletInstanceConfiguration.twitterUsername()).thenReturn(StringPool.THREE_SPACES);

		Optional<TwitterFeedPortletInstanceConfiguration> result = twitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getValidConfiguration_WhenNumberOfFeedsToShowIsNotGreaterThanZeroAndHasTwitterEmbeddedValueConfigured_ThenReturnsOptionalWithConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TwitterFeedPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTwitterFeedPortletInstanceConfiguration);
		when(mockTwitterFeedPortletInstanceConfiguration.feedsToShow()).thenReturn(0);
		when(mockTwitterFeedPortletInstanceConfiguration.twitterEmbedded()).thenReturn("embedValue");

		Optional<TwitterFeedPortletInstanceConfiguration> result = twitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay);

		assertThat(result.get(), sameInstance(mockTwitterFeedPortletInstanceConfiguration));
	}

	@Test
	public void getValidConfiguration_WhenNumberOfFeedsToShowIsNotGreaterThanZeroAndNoTwitterEmbeddedConfigured_ThenReturnsEmptyOptional() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(TwitterFeedPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockTwitterFeedPortletInstanceConfiguration);
		when(mockTwitterFeedPortletInstanceConfiguration.feedsToShow()).thenReturn(0);

		Optional<TwitterFeedPortletInstanceConfiguration> result = twitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay);

		assertThat(result.isPresent(), equalTo(false));
	}
}
