package com.placecube.digitalplace.search.web.portlet.sortby;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.constants.SortByDirectionConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class SortByMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@InjectMocks
	private SortByMVCActionCommand sortByMVCActionCommand;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenCommandIsNotUpdateSortByDirection_ThenSetsTheSelectedSortByAsSingleValuedSharedSearchSessionAttributesAndExecutesTheRedirect() throws Exception {
		String selectedSortBy = "selectedSortByExpectedVal";
		when(ParamUtil.getString(mockActionRequest, SearchParameters.SELECTED_SORT_BY)).thenReturn(selectedSortBy);

		sortByMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, SearchParameters.SELECTED_SORT_BY, selectedSortBy);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, SearchParameters.SELECTED_SORT_BY_DIRECTION);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");

		verify(mockSharedSearchRequestService, never()).setSharedSearchSessionSingleValuedAttribute(eq(mockActionRequest), eq(SearchParameters.SELECTED_SORT_BY_DIRECTION), any());
	}

	@Test
	public void doProcessAction_WhenCommandIsUpdateSortByDirectionAndDirectionIsDefault_ThenSetsTheSelectedSortByAndSortByDirectionInvertedAsSingleValuedSharedSearchSessionAttributesAndExecutesTheRedirect()
			throws Exception {
		String selectedSortBy = "selectedSortByExpectedVal";
		when(ParamUtil.getString(mockActionRequest, SearchParameters.SELECTED_SORT_BY)).thenReturn(selectedSortBy);
		when(ParamUtil.getString(mockActionRequest, "currentSelectedSortByDirection", SortByDirectionConstants.DIRECTION_DEFAULT)).thenReturn(SortByDirectionConstants.DIRECTION_DEFAULT);
		when(ParamUtil.getString(mockActionRequest, "cmd")).thenReturn("updateSortByDirection");

		sortByMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, SearchParameters.SELECTED_SORT_BY, selectedSortBy);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, SearchParameters.SELECTED_SORT_BY_DIRECTION);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, SearchParameters.SELECTED_SORT_BY_DIRECTION,
				SortByDirectionConstants.DIRECTION_INVERTED);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

	@Test
	public void doProcessAction_WhenCommandIsUpdateSortByDirectionAndDirectionIsNotDefault_ThenSetsTheSelectedSortByAndSortByDirectionDefaultAsSingleValuedSharedSearchSessionAttributesAndExecutesTheRedirect()
			throws Exception {
		String selectedSortBy = "selectedSortByExpectedVal";
		when(ParamUtil.getString(mockActionRequest, SearchParameters.SELECTED_SORT_BY)).thenReturn(selectedSortBy);
		when(ParamUtil.getString(mockActionRequest, "currentSelectedSortByDirection", SortByDirectionConstants.DIRECTION_DEFAULT)).thenReturn("selectedSortByDirectionExpectedVal");
		when(ParamUtil.getString(mockActionRequest, "cmd")).thenReturn("updateSortByDirection");

		sortByMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, SearchParameters.SELECTED_SORT_BY, selectedSortBy);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, SearchParameters.SELECTED_SORT_BY_DIRECTION);
		inOrder.verify(mockSharedSearchRequestService, times(1)).setSharedSearchSessionSingleValuedAttribute(mockActionRequest, SearchParameters.SELECTED_SORT_BY_DIRECTION,
				SortByDirectionConstants.DIRECTION_DEFAULT);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

}
