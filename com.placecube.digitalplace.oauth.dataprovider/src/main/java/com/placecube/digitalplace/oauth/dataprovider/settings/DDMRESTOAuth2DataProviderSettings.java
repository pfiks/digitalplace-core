package com.placecube.digitalplace.oauth.dataprovider.settings;

import com.liferay.dynamic.data.mapping.annotations.DDMForm;
import com.liferay.dynamic.data.mapping.annotations.DDMFormField;
import com.liferay.dynamic.data.mapping.annotations.DDMFormLayout;
import com.liferay.dynamic.data.mapping.annotations.DDMFormLayoutColumn;
import com.liferay.dynamic.data.mapping.annotations.DDMFormLayoutPage;
import com.liferay.dynamic.data.mapping.annotations.DDMFormLayoutRow;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderParameterSettings;

@DDMForm
@DDMFormLayout({ @DDMFormLayoutPage({
		@DDMFormLayoutRow({ @DDMFormLayoutColumn(size = 12, value = { "url", "oauthApplicationExternalReferenceCode", "cacheable", "inputParameters", "outputParameters" }) }) }) })
public interface DDMRESTOAuth2DataProviderSettings extends DDMDataProviderParameterSettings {

	@DDMFormField(label = "%cache-data-on-the-first-request", properties = "showAsSwitcher=true")
	boolean cacheable();

	@DDMFormField(label = "%url", properties = "placeholder=%enter-the-rest-service-url", required = true, validationErrorMessage = "%please-enter-a-valid-url", validationExpression = "isURL(url)")
	String url();

	@DDMFormField(label = "%oauth-application-external-reference-code", properties = { "placeholder=%enter-the-oauth-application-external-reference-code",
			"tooltip=%provide-the-oauth-application-external-reference-code-to-authenticate-to-the-rest-provider" })
	String oauthApplicationExternalReferenceCode();

}