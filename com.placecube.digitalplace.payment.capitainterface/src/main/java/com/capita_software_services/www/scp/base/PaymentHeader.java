/**
 * PaymentHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class PaymentHeader  implements java.io.Serializable {
    private java.util.Calendar transactionDate;

    private java.lang.String machineCode;

    private java.lang.String uniqueTranId;

    public PaymentHeader() {
    }

    public PaymentHeader(
           java.util.Calendar transactionDate,
           java.lang.String machineCode,
           java.lang.String uniqueTranId) {
           this.transactionDate = transactionDate;
           this.machineCode = machineCode;
           this.uniqueTranId = uniqueTranId;
    }


    /**
     * Gets the transactionDate value for this PaymentHeader.
     *
     * @return transactionDate
     */
    public java.util.Calendar getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this PaymentHeader.
     *
     * @param transactionDate
     */
    public void setTransactionDate(java.util.Calendar transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the machineCode value for this PaymentHeader.
     *
     * @return machineCode
     */
    public java.lang.String getMachineCode() {
        return machineCode;
    }


    /**
     * Sets the machineCode value for this PaymentHeader.
     *
     * @param machineCode
     */
    public void setMachineCode(java.lang.String machineCode) {
        this.machineCode = machineCode;
    }


    /**
     * Gets the uniqueTranId value for this PaymentHeader.
     *
     * @return uniqueTranId
     */
    public java.lang.String getUniqueTranId() {
        return uniqueTranId;
    }


    /**
     * Sets the uniqueTranId value for this PaymentHeader.
     *
     * @param uniqueTranId
     */
    public void setUniqueTranId(java.lang.String uniqueTranId) {
        this.uniqueTranId = uniqueTranId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentHeader)) return false;
        PaymentHeader other = (PaymentHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.transactionDate==null && other.getTransactionDate()==null) ||
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.machineCode==null && other.getMachineCode()==null) ||
             (this.machineCode!=null &&
              this.machineCode.equals(other.getMachineCode()))) &&
            ((this.uniqueTranId==null && other.getUniqueTranId()==null) ||
             (this.uniqueTranId!=null &&
              this.uniqueTranId.equals(other.getUniqueTranId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getMachineCode() != null) {
            _hashCode += getMachineCode().hashCode();
        }
        if (getUniqueTranId() != null) {
            _hashCode += getUniqueTranId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "paymentHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "transactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("machineCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "machineCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uniqueTranId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "uniqueTranId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
