package com.placecube.digitalplace.addresslookup.statmap.constants;

public final class StatMapConstants {

	public static final String JSON_FIELD_IDS = "ids";

	public static final String JSON_FIELD_ORGANISATION = "ORGANISATION";

	public static final String JSON_FIELD_PAO_END_NUMBER = "PAO_END_NUMBER";

	public static final String JSON_FIELD_PAO_END_SUFFIX = "PAO_END_SUFFIX";

	public static final String JSON_FIELD_PAO_START_NUMBER = "PAO_START_NUMBER";

	public static final String JSON_FIELD_PAO_START_SUFFIX = "PAO_START_SUFFIX";

	public static final String JSON_FIELD_PAO_TEXT = "PAO_TEXT";

	public static final String JSON_FIELD_POSTCODE = "POSTCODE";

	public static final String JSON_FIELD_RECORD = "record";

	public static final String JSON_FIELD_SAO_END_NUMBER = "SAO_END_NUMBER";

	public static final String JSON_FIELD_SAO_END_SUFFIX = "SAO_END_SUFFIX";

	public static final String JSON_FIELD_SAO_START_NUMBER = "SAO_START_NUMBER";

	public static final String JSON_FIELD_SAO_START_SUFFIX = "SAO_START_SUFFIX";

	public static final String JSON_FIELD_SAO_TEXT = "SAO_TEXT";

	public static final String JSON_FIELD_STREET_DESCRIPTOR = "STREET_DESCRIPTOR";

	public static final String JSON_FIELD_TOWN_NAME = "TOWN_NAME";

	public static final String JSON_FIELD_UPRN = "UPRN";

	public static final String PARAM_SEARCH_TERM = "searchedTerm";

	public static final String PARAM_SEARCH_UPRN = "searchedUprn";

	public static final String PARAM_TOKEN = "token";

	private StatMapConstants() {
	}
}