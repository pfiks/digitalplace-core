package com.placecube.digitalplace.group.restadmin.web.panel.app;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletKeys;

@RunWith(PowerMockRunner.class)
public class GroupRestAdminPanelAppTest extends PowerMockito {

	@InjectMocks
	private GroupRestAdminPanelApp groupRestAdminPanelApp;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private Group mockGroup;

	@Test
	public void getPortletId_WhenNoError_ThenReturnsThePortletId() {
		String result = groupRestAdminPanelApp.getPortletId();

		assertThat(result, equalTo(PortletKeys.GROUP_REST_ADMIN));
	}

	@Test
	public void isShow_WhenGroupIsCompany_ThenReturnsFalse() {
		when(mockGroup.isCompany()).thenReturn(true);

		boolean result = groupRestAdminPanelApp.isShow(mockPermissionChecker, mockGroup);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShow_WhenGroupIsNotCompanyAndIsGuest_ThenReturnsFalse() {
		when(mockGroup.isCompany()).thenReturn(false);
		when(mockGroup.isGuest()).thenReturn(true);

		boolean result = groupRestAdminPanelApp.isShow(mockPermissionChecker, mockGroup);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShow_WhenGroupIsNotCompanyAndIsNotGuestAndIsNotAsite_ThenReturnsFalse() {
		when(mockGroup.isCompany()).thenReturn(false);
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockGroup.isSite()).thenReturn(false);

		boolean result = groupRestAdminPanelApp.isShow(mockPermissionChecker, mockGroup);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShow_WhenGroupIsNotCompanyAndIsNotGuestAndIsAsiteAndUserIsNotCompanyAdmin_ThenReturnsFalse() {
		when(mockGroup.isCompany()).thenReturn(false);
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockGroup.isSite()).thenReturn(true);
		long companyId = 123L;
		when(mockGroup.getCompanyId()).thenReturn(companyId);
		when(mockPermissionChecker.isCompanyAdmin(companyId)).thenReturn(false);

		boolean result = groupRestAdminPanelApp.isShow(mockPermissionChecker, mockGroup);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isShow_WhenGroupIsNotCompanyAndIsNotGuestAndIsAsiteAndUserIsCompanyAdmin_ThenReturnsTrue() {
		when(mockGroup.isCompany()).thenReturn(false);
		when(mockGroup.isGuest()).thenReturn(false);
		when(mockGroup.isSite()).thenReturn(true);
		long companyId = 123L;
		when(mockGroup.getCompanyId()).thenReturn(companyId);
		when(mockPermissionChecker.isCompanyAdmin(companyId)).thenReturn(true);

		boolean result = groupRestAdminPanelApp.isShow(mockPermissionChecker, mockGroup);

		assertThat(result, equalTo(true));
	}
}
