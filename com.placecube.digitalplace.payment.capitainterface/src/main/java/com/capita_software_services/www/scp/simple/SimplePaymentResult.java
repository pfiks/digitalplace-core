/**
 * SimplePaymentResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.simple;

public class SimplePaymentResult  extends com.capita_software_services.www.scp.base.PaymentResultBase  implements java.io.Serializable {
    private com.capita_software_services.www.scp.simple.SimplePayment paymentDetails;

    private com.capita_software_services.www.scp.base.ErrorDetails errorDetails;

    public SimplePaymentResult() {
    }

    public SimplePaymentResult(
           com.capita_software_services.www.scp.base.Status status,
           com.capita_software_services.www.scp.simple.SimplePayment paymentDetails,
           com.capita_software_services.www.scp.base.ErrorDetails errorDetails) {
        super(
            status);
        this.paymentDetails = paymentDetails;
        this.errorDetails = errorDetails;
    }


    /**
     * Gets the paymentDetails value for this SimplePaymentResult.
     *
     * @return paymentDetails
     */
    public com.capita_software_services.www.scp.simple.SimplePayment getPaymentDetails() {
        return paymentDetails;
    }


    /**
     * Sets the paymentDetails value for this SimplePaymentResult.
     *
     * @param paymentDetails
     */
    public void setPaymentDetails(com.capita_software_services.www.scp.simple.SimplePayment paymentDetails) {
        this.paymentDetails = paymentDetails;
    }


    /**
     * Gets the errorDetails value for this SimplePaymentResult.
     *
     * @return errorDetails
     */
    public com.capita_software_services.www.scp.base.ErrorDetails getErrorDetails() {
        return errorDetails;
    }


    /**
     * Sets the errorDetails value for this SimplePaymentResult.
     *
     * @param errorDetails
     */
    public void setErrorDetails(com.capita_software_services.www.scp.base.ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SimplePaymentResult)) return false;
        SimplePaymentResult other = (SimplePaymentResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.paymentDetails==null && other.getPaymentDetails()==null) ||
             (this.paymentDetails!=null &&
              this.paymentDetails.equals(other.getPaymentDetails()))) &&
            ((this.errorDetails==null && other.getErrorDetails()==null) ||
             (this.errorDetails!=null &&
              this.errorDetails.equals(other.getErrorDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPaymentDetails() != null) {
            _hashCode += getPaymentDetails().hashCode();
        }
        if (getErrorDetails() != null) {
            _hashCode += getErrorDetails().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SimplePaymentResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "simplePaymentResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "paymentDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "simplePayment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "errorDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "errorDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
