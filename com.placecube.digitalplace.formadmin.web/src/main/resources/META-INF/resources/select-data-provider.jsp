<%@ include file="init.jsp" %>

<portlet:renderURL var="backUrl">
	<portlet:param name="mvcRenderCommandName" value="/data-consumers" />
	<portlet:param name="currentTab" value="data-consumers" />
</portlet:renderURL>

<%
portletDisplay.setShowBackIcon(true);
portletDisplay.setURLBack(backUrl);

String formInstanceName = (String)request.getAttribute(PortletRequestKeys.FORM_INSTANCE_NAME);
formInstanceName = HtmlUtil.stripHtml(formInstanceName);
renderResponse.setTitle( formInstanceName );
%>

<div class="container container-fluid container-fluid-max-xl pt-3">

	<c:forEach items="${ dataConsumers }" var="consumer">

		<portlet:renderURL var="mappingUrl">
			<portlet:param name="mvcRenderCommandName" value="/data-consumer-mapping" />
			<portlet:param name="currentTab" value="data-consumers" />
			<portlet:param name="formInstanceId" value="${ formInstanceId }" />
			<portlet:param name="chain" value="${ consumer.getChain() }" />
			<portlet:param name="redirect" value="<%= currentURL %>" />
		</portlet:renderURL>

		<aui:form name="fm" action="<%= mappingUrl %>">
			<c:set var="lastChain" value="${ consumer.getChain() }" />
			<dp-frontend:fieldset-group>

				<aui:fieldset>
					<c:set var="selectBoxLabel" value="select-data-provider" />
					<c:if test="${ consumer.getChain() > 0 }">
						<c:set var="selectBoxLabel" value="chained-data-provider" />
					</c:if>
					<aui:select label="${ selectBoxLabel }" name="dataProviderInstance" required="true"
						showEmptyOption="true" disabled="true">
						<c:forEach items="${dataProviders}" var="provider">
							<c:if
								test="${ provider.getDataProviderInstanceId() == consumer.getDdmDataProviderInstanceId() }">
								<option value="${provider.getDataProviderInstanceId()}" selected label="${provider.getName(locale)}" />
							</c:if>
						</c:forEach>
					</aui:select>
				<aui:input type="hidden" name="dataProviderInstanceId" value="${ consumer.getDdmDataProviderInstanceId() }" />
				</aui:fieldset>
				
			<aui:button-row>
				<aui:button name="submitButton" type="submit" value="edit" />

				<portlet:actionURL name="/data-consumer-delete" var="deleteUrl">
					<portlet:param name="currentTab" value="data-consumers" />
					<portlet:param name="formInstanceId" value="${ formInstanceId }" />
					<portlet:param name="chain" value="${ consumer.getChain() }" />
				</portlet:actionURL>
				
				<c:choose>
					<c:when test="${dataConsumers.size() == 1}">
						<a href="${ deleteUrl }" class="btn btn-secondary">
							<liferay-ui:message key="remove" />
						</a>
					</c:when>
					<c:when test="${consumer.getChain() > 0}">
						<a href="${ deleteUrl }" class="btn btn-secondary">
							<liferay-ui:message key="delete-chain" />
						</a>
					</c:when>
				</c:choose>
				
				<c:if test="${ consumer.getChain() == dataConsumers.get(dataConsumers.size()-1).getChain() }">
					<aui:button type="button" value="add-chain" cssClass="pull-right" onclick="$('#chainAdding').show()" />
				</c:if>
			</aui:button-row>
			
			</dp-frontend:fieldset-group>
		</aui:form>
		<hr>
	</c:forEach>

	<div id="chainAdding" style="display: none;">

		<portlet:renderURL var="mappingUrl">
			<portlet:param name="mvcRenderCommandName" value="/data-consumer-mapping" />
			<portlet:param name="currentTab" value="data-consumers" />
			<portlet:param name="formInstanceId" value="${ formInstanceId }" />
			<portlet:param name="chain" value="${ lastChain + 1 }" />
			<portlet:param name="redirect" value="<%= currentURL %>" />
		</portlet:renderURL>

		<aui:form name="fm" action="<%= mappingUrl %>">
			<dp-frontend:fieldset-group>
				<aui:fieldset>
					<aui:select label="chain-data-provider" name="dataProviderInstanceId" showEmptyOption="true">
						<c:forEach items="${dataProviders}" var="provider">
							<option value="${provider.getDataProviderInstanceId()}"
								label="${provider.getName(locale)}" />
						</c:forEach>
					</aui:select>
				</aui:fieldset>
			</dp-frontend:fieldset-group>
			<aui:button-row>
				<aui:button name="submitButton" type="submit" value="add" />
			</aui:button-row>
		</aui:form>
	</div>

	<c:if test="${ empty dataConsumers }">

		<portlet:renderURL var="mappingUrl">
			<portlet:param name="mvcRenderCommandName" value="/data-consumer-mapping" />
			<portlet:param name="currentTab" value="data-consumers" />
			<portlet:param name="formInstanceId" value="${ formInstanceId }" />
			<portlet:param name="chain" value="0" />
			<portlet:param name="redirect" value="<%= currentURL %>" />
		</portlet:renderURL>

		<aui:form name="fm" action="<%= mappingUrl %>">
			<dp-frontend:fieldset-group>
				<aui:fieldset>
					<aui:select label="select-data-provider" name="dataProviderInstanceId" required="true"
						showEmptyOption="true">
						<c:forEach items="${dataProviders}" var="provider">
							<option value="${provider.getDataProviderInstanceId()}"
								label="${provider.getName(locale)}" />
						</c:forEach>
					</aui:select>
				</aui:fieldset>
			
			<aui:button-row>
				<aui:button name="submitButton" type="submit" value="add" />
			</aui:button-row>
			
			</dp-frontend:fieldset-group>
		</aui:form>

	</c:if>
</div>

<aui:script>

	$('#<portlet:namespace/>fm button[type=submit]').click(function() {
		var form = $(this).closest('form');
		var dataProviderInstanceId = $(form).find('#<portlet:namespace/>dataProviderInstanceId').val();
		if(dataProviderInstanceId !== ''){
			var mappingUrl = new URL(form.attr('action'));
			mappingUrl.searchParams.set('#<portlet:namespace/>dataProviderInstanceId', dataProviderInstanceId);	
			form.attr('action', mappingUrl.toString());
		}
	});
	
</aui:script>