/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.authentication.twofactor.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for UserTwoFactorAuthentication. This utility wraps
 * <code>com.placecube.digitalplace.authentication.twofactor.service.impl.UserTwoFactorAuthenticationLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see UserTwoFactorAuthenticationLocalService
 * @generated
 */
public class UserTwoFactorAuthenticationLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.authentication.twofactor.service.impl.UserTwoFactorAuthenticationLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the user two factor authentication to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 * @return the user two factor authentication that was added
	 */
	public static UserTwoFactorAuthentication addUserTwoFactorAuthentication(
		UserTwoFactorAuthentication userTwoFactorAuthentication) {

		return getService().addUserTwoFactorAuthentication(
			userTwoFactorAuthentication);
	}

	public static void configureTwoFactorAuthenticationForUser(
		com.liferay.portal.kernel.model.User user, String secretKey) {

		getService().configureTwoFactorAuthenticationForUser(user, secretKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new user two factor authentication with the primary key. Does not add the user two factor authentication to the database.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key for the new user two factor authentication
	 * @return the new user two factor authentication
	 */
	public static UserTwoFactorAuthentication createUserTwoFactorAuthentication(
		com.placecube.digitalplace.authentication.twofactor.service.persistence.
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK) {

		return getService().createUserTwoFactorAuthentication(
			userTwoFactorAuthenticationPK);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the user two factor authentication from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 * @return the user two factor authentication that was removed
	 */
	public static UserTwoFactorAuthentication deleteUserTwoFactorAuthentication(
		UserTwoFactorAuthentication userTwoFactorAuthentication) {

		return getService().deleteUserTwoFactorAuthentication(
			userTwoFactorAuthentication);
	}

	/**
	 * Deletes the user two factor authentication with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication that was removed
	 * @throws PortalException if a user two factor authentication with the primary key could not be found
	 */
	public static UserTwoFactorAuthentication deleteUserTwoFactorAuthentication(
			com.placecube.digitalplace.authentication.twofactor.service.
				persistence.UserTwoFactorAuthenticationPK
					userTwoFactorAuthenticationPK)
		throws PortalException {

		return getService().deleteUserTwoFactorAuthentication(
			userTwoFactorAuthenticationPK);
	}

	public static void disableTwoFactorAuthenticationForUser(
		com.liferay.portal.kernel.model.User user) {

		getService().disableTwoFactorAuthenticationForUser(user);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static UserTwoFactorAuthentication
		enableAndGetOrCreateTwoFactorAuthenticationForUser(
			com.liferay.portal.kernel.model.User user, boolean enabled) {

		return getService().enableAndGetOrCreateTwoFactorAuthenticationForUser(
			user, enabled);
	}

	public static UserTwoFactorAuthentication fetchUserTwoFactorAuthentication(
		com.placecube.digitalplace.authentication.twofactor.service.persistence.
			UserTwoFactorAuthenticationPK userTwoFactorAuthenticationPK) {

		return getService().fetchUserTwoFactorAuthentication(
			userTwoFactorAuthenticationPK);
	}

	/**
	 * Returns the user two factor authentication with the matching UUID and company.
	 *
	 * @param uuid the user two factor authentication's UUID
	 * @param companyId the primary key of the company
	 * @return the matching user two factor authentication, or <code>null</code> if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication
		fetchUserTwoFactorAuthenticationByUuidAndCompanyId(
			String uuid, long companyId) {

		return getService().fetchUserTwoFactorAuthenticationByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Generates a new random security key
	 *
	 * @return the generated security key
	 */
	public static String generateSecretKey() {
		return getService().generateSecretKey();
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Gets the QR bar code URL.
	 *
	 * @param companyId the company id
	 * @param emailAddress the email address of the user
	 * @param secretKey the secret key associated to the user
	 * @return the QR bar code URL
	 * @throws PortalException any exception retrieving the qr bar code url
	 */
	public static String getQRBarCodeURL(
			long companyId, String emailAddress, String secretKey)
		throws PortalException {

		return getService().getQRBarCodeURL(companyId, emailAddress, secretKey);
	}

	public static java.util.Optional<UserTwoFactorAuthentication>
		getUserTwoFactorAuthentication(
			com.liferay.portal.kernel.model.User user) {

		return getService().getUserTwoFactorAuthentication(user);
	}

	/**
	 * Returns the user two factor authentication with the primary key.
	 *
	 * @param userTwoFactorAuthenticationPK the primary key of the user two factor authentication
	 * @return the user two factor authentication
	 * @throws PortalException if a user two factor authentication with the primary key could not be found
	 */
	public static UserTwoFactorAuthentication getUserTwoFactorAuthentication(
			com.placecube.digitalplace.authentication.twofactor.service.
				persistence.UserTwoFactorAuthenticationPK
					userTwoFactorAuthenticationPK)
		throws PortalException {

		return getService().getUserTwoFactorAuthentication(
			userTwoFactorAuthenticationPK);
	}

	/**
	 * Returns the user two factor authentication with the matching UUID and company.
	 *
	 * @param uuid the user two factor authentication's UUID
	 * @param companyId the primary key of the company
	 * @return the matching user two factor authentication
	 * @throws PortalException if a matching user two factor authentication could not be found
	 */
	public static UserTwoFactorAuthentication
			getUserTwoFactorAuthenticationByUuidAndCompanyId(
				String uuid, long companyId)
		throws PortalException {

		return getService().getUserTwoFactorAuthenticationByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of all the user two factor authentications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.authentication.twofactor.model.impl.UserTwoFactorAuthenticationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of user two factor authentications
	 * @param end the upper bound of the range of user two factor authentications (not inclusive)
	 * @return the range of user two factor authentications
	 */
	public static List<UserTwoFactorAuthentication>
		getUserTwoFactorAuthentications(int start, int end) {

		return getService().getUserTwoFactorAuthentications(start, end);
	}

	/**
	 * Returns the number of user two factor authentications.
	 *
	 * @return the number of user two factor authentications
	 */
	public static int getUserTwoFactorAuthenticationsCount() {
		return getService().getUserTwoFactorAuthenticationsCount();
	}

	public static boolean isTwoFactorAuthenticationEnforcedForUser(
		com.liferay.portal.kernel.model.User user) {

		return getService().isTwoFactorAuthenticationEnforcedForUser(user);
	}

	/**
	 * Checks if the Two-Factor authentication code is valid for the user.
	 *
	 * @param user the user
	 * @param authenticationCode the authenticationCode to check
	 * @return true if the code is valid or if the feature is disabled for the
	 user. Returns false if the feature is enabled and the code is not
	 valid
	 * @throws PortalException any exception while checking the two factor
	 authentication code
	 */
	public static boolean isValidTwoFactorAuthentication(
			com.liferay.portal.kernel.model.User user,
			String authenticationCode)
		throws PortalException {

		return getService().isValidTwoFactorAuthentication(
			user, authenticationCode);
	}

	public static void resetUserTwoFactorAuthentication(
		long companyId, long userId) {

		getService().resetUserTwoFactorAuthentication(companyId, userId);
	}

	/**
	 * Updates the user two factor authentication in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect UserTwoFactorAuthenticationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param userTwoFactorAuthentication the user two factor authentication
	 * @return the user two factor authentication that was updated
	 */
	public static UserTwoFactorAuthentication updateUserTwoFactorAuthentication(
		UserTwoFactorAuthentication userTwoFactorAuthentication) {

		return getService().updateUserTwoFactorAuthentication(
			userTwoFactorAuthentication);
	}

	public static boolean verifyAndEnableUserTwoFactorAuthentication(
			com.liferay.portal.kernel.model.User user,
			String authenticationCode, String secretKey)
		throws PortalException {

		return getService().verifyAndEnableUserTwoFactorAuthentication(
			user, authenticationCode, secretKey);
	}

	public static UserTwoFactorAuthenticationLocalService getService() {
		return _service;
	}

	public static void setService(
		UserTwoFactorAuthenticationLocalService service) {

		_service = service;
	}

	private static volatile UserTwoFactorAuthenticationLocalService _service;

}