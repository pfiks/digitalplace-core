package com.placecube.digitalplace.audit.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.sql.Date;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.audit.AuditException;
import com.liferay.portal.kernel.audit.AuditMessage;
import com.liferay.portal.kernel.audit.AuditRouter;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.PortalPreferenceValue;
import com.liferay.portal.kernel.model.PortletPreferences;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.HashMapDictionary;
import com.liferay.portal.security.audit.event.generators.util.AuditMessageBuilder;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DateUtil.class, AuditMessageBuilder.class, PrincipalThreadLocal.class })
public class AuditServiceTest extends PowerMockito {

	@InjectMocks
	private AuditService auditService;

	@Mock
	private AuditMessage mockAuditMessage;

	@Mock
	private AuditRouter mockAuditRouter;

	@Mock
	private Date mockDate;

	@Mock
	private Group mockGroup;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private PortalPreferenceValue mockPortalPreferenceValue;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private javax.portlet.PortletPreferences mockPortletPreferences1;

	@Mock
	private PortletPreferencesLocalService mockPortletPreferencesLocalService;

	@Before
	public void activateSetup() {
		mockStatic(PrincipalThreadLocal.class, AuditMessageBuilder.class, DateUtil.class);
	}

	@Test
	public void addAuditLogEntry_WhenException_ThenDoesNotThrowAnyError() throws AuditException {
		long classPK = 100;
		long userId = 200;
		String eventType = "myEventType";
		String className = "myClassName";
		when(PrincipalThreadLocal.getUserId()).thenReturn(userId);
		when(AuditMessageBuilder.buildAuditMessage(eventType, className, classPK, null)).thenReturn(mockAuditMessage);
		when(mockAuditMessage.getAdditionalInfo()).thenReturn(mockJSONObject);
		when(DateUtil.newDate()).thenReturn(mockDate);
		doThrow(new AuditException()).when(mockAuditRouter).route(mockAuditMessage);

		try {
			auditService.addAuditLogEntry(className, classPK, eventType, Collections.emptyMap());
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void addAuditLogEntry_WhenNoError_ThenRouteAuditRouter() throws AuditException {
		long classPK = 100;
		long userId = 200;
		String eventType = "myEventType";
		String className = "myClassName";
		when(PrincipalThreadLocal.getUserId()).thenReturn(userId);
		when(AuditMessageBuilder.buildAuditMessage(eventType, className, classPK, null)).thenReturn(mockAuditMessage);
		when(mockAuditMessage.getAdditionalInfo()).thenReturn(mockJSONObject);
		when(DateUtil.newDate()).thenReturn(mockDate);

		Map<String, String> additionalInformation = new LinkedHashMap<>();
		additionalInformation.put("key_1", "value_1");
		additionalInformation.put("key_2", "value_2");

		auditService.addAuditLogEntry(className, classPK, eventType, additionalInformation);

		InOrder inOrder = inOrder(mockJSONObject, mockAuditRouter, mockAuditMessage);
		inOrder.verify(mockJSONObject, times(1)).put("userId", userId);
		inOrder.verify(mockJSONObject, times(1)).put("date", mockDate);
		inOrder.verify(mockJSONObject, times(1)).put("key_1", "value_1");
		inOrder.verify(mockJSONObject, times(1)).put("key_2", "value_2");
		inOrder.verify(mockAuditMessage, times(1)).setAdditionalInfo(mockJSONObject);
		inOrder.verify(mockAuditRouter, times(1)).route(mockAuditMessage);
	}

	@Test
	public void getAdditionalInformationForGroup_WhenNoError_ThenReturnMapOfAdditionalInformation() {
		long groupId = 1;
		String defaultLanguageId = "myDefaultLanguageId";
		String groupName = "myGroupName";
		String url = "myGroupUrl";

		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockGroup.getDefaultLanguageId()).thenReturn(defaultLanguageId);
		when(mockGroup.getName(defaultLanguageId)).thenReturn(groupName);
		when(mockGroup.getFriendlyURL()).thenReturn(url);

		Map<String, String> results = auditService.getGroupAuditInfo(mockGroup);

		assertThat(results.size(), equalTo(3));
		assertThat(results.get("groupId"), equalTo(String.valueOf(groupId)));
		assertThat(results.get("groupName"), equalTo(groupName));
		assertThat(results.get("friendlyUrl"), equalTo(url));
	}

	@Test
	public void getConfigurationAuditInfo_WithPidAndDictionaryParam_WhenNoError_ThenReturnsMapOfAdditionalInfo() {
		String pid = "myPid";
		Map<String, Object> propValues = new LinkedHashMap<>();
		propValues.put("key_1", "value_1");
		propValues.put("key_2", "value_2");
		Dictionary<String, Object> properties = new HashMapDictionary<>(propValues);

		Map<String, String> results = auditService.getConfigurationAuditInfo(pid, properties);

		assertThat(results.size(), equalTo(3));
		assertThat(results.get("pid"), equalTo(pid));
		assertThat(results.get("key_1"), equalTo("value_1"));
		assertThat(results.get("key_2"), equalTo("value_2"));
	}

	@Test
	public void getConfigurationAuditInfo_WithPidParam_WhenNoError_ThenReturnsMapOfAdditionalInfo() {
		String pid = "myPid";

		Map<String, String> results = auditService.getConfigurationAuditInfo(pid);

		assertThat(results.size(), equalTo(1));
		assertThat(results.get("pid"), equalTo(pid));
	}

	@Test
	public void getPortalPreferenceValueAuditInfo_WhenNoError_ThenReturnsMapOfAdditionalInfo() {
		long portalPreferencesId = 11;
		long portalPreferenceValueId = 22;
		String key = "myKey";
		String smallValue = "mySmallValue";
		String largeValue = "myLargeValue";
		when(mockPortalPreferenceValue.getPortalPreferencesId()).thenReturn(portalPreferencesId);
		when(mockPortalPreferenceValue.getPortalPreferenceValueId()).thenReturn(portalPreferenceValueId);
		when(mockPortalPreferenceValue.getKey()).thenReturn(key);
		when(mockPortalPreferenceValue.getSmallValue()).thenReturn(smallValue);
		when(mockPortalPreferenceValue.getLargeValue()).thenReturn(largeValue);

		Map<String, String> results = auditService.getPortalPreferenceValueAuditInfo(mockPortalPreferenceValue);

		assertThat(results.size(), equalTo(5));
		assertThat(results.get("portalPreferencesId"), equalTo(String.valueOf(portalPreferencesId)));
		assertThat(results.get("portalPreferenceValueId"), equalTo(String.valueOf(portalPreferenceValueId)));
		assertThat(results.get("key"), equalTo(key));
		assertThat(results.get("smallValue"), equalTo(smallValue));
		assertThat(results.get("largeValue"), equalTo(largeValue));
	}

	@Test
	public void getPortletPreferencesAuditInfo_WhenNoError_ThenReturnsMapOfAdditionalInfo() {
		long portletPreferencesId = 1;
		long plid = 2;
		long companyId = 3;
		long ownerId = 4;
		int ownerType = 5;
		String portletId = "myPortletId";
		when(mockPortletPreferences.getPortletPreferencesId()).thenReturn(portletPreferencesId);
		when(mockPortletPreferences.getPlid()).thenReturn(plid);
		when(mockPortletPreferences.getPortletId()).thenReturn(portletId);
		when(mockPortletPreferences.getCompanyId()).thenReturn(companyId);
		when(mockPortletPreferences.getOwnerId()).thenReturn(ownerId);
		when(mockPortletPreferences.getOwnerType()).thenReturn(ownerType);
		when(mockPortletPreferencesLocalService.getStrictPreferences(companyId, ownerId, ownerType, plid, portletId)).thenReturn(mockPortletPreferences1);
		Map<String, String[]> prefsMap1 = new HashMap<>();
		prefsMap1.put("key1", new String[] { "value1" });
		prefsMap1.put("key11", new String[] { "value11", "value22" });
		when(mockPortletPreferences1.getMap()).thenReturn(prefsMap1);

		Map<String, String> results = auditService.getPortletPreferencesAuditInfo(mockPortletPreferences);

		assertThat(results.size(), equalTo(5));
		assertThat(results.get("portletPreferencesId"), equalTo(String.valueOf(portletPreferencesId)));
		assertThat(results.get("plid"), equalTo(String.valueOf(plid)));
		assertThat(results.get("portletId"), equalTo(portletId));
		assertThat(results.get("key1"), equalTo("[value1]"));
		assertThat(results.get("key11"), equalTo("[value11, value22]"));
	}

}
