package com.placecube.digitalplace.companyinitializeradmin.constants;

public final class PortletKeys {

	public static final String COMPANY_INITIALIZER_ADMIN = "com_placecube_digitalplace_companyinitializeradmin_CompanyInitializerAdminPortlet";

	private PortletKeys() {
	}
}