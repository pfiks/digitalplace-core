package com.placecube.digitalplace.authentication.twofactor.internal;

import java.util.Optional;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Base64;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.security.SecureRandom;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.internal.encryption.CipherService;
import com.placecube.digitalplace.authentication.twofactor.internal.encryption.ModelFactoryBuilder;
import com.placecube.digitalplace.authentication.twofactor.internal.encryption.TOTPService;
import com.placecube.digitalplace.authentication.twofactor.service.persistence.UserTwoFactorAuthenticationPK;

@Component(immediate = true, service = TwoFactorUtils.class)
public class TwoFactorUtils {

	private static final Log LOG = LogFactoryUtil.getLog(TwoFactorUtils.class);

	@Reference
	private CipherService cipherService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private ModelFactoryBuilder modelFactoryBuilder;

	@Reference
	private TOTPService totpService;

	public Optional<String> decrypt(String encrypted, String uuid) {
		if (Validator.isNull(encrypted) || Validator.isNull(uuid)) {
			throw new IllegalArgumentException();
		}
		return cipherService.getCipher(Cipher.DECRYPT_MODE, uuid).map(c -> {
			try {
				return modelFactoryBuilder.bytesToString(c.doFinal(Base64.decodeBase64(encrypted)));
			} catch (IllegalBlockSizeException | BadPaddingException e) {
				LOG.error(e.getMessage(), e);
				return null;
			}
		});
	}

	public Optional<String> encrypt(String value, String uuid) {
		if (Validator.isNull(value) || Validator.isNull(uuid)) {
			throw new IllegalArgumentException();
		}
		return cipherService.getCipher(Cipher.ENCRYPT_MODE, uuid).map(c -> {
			try {
				return Base64.encodeBase64String(c.doFinal(value.getBytes()));
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			return null;
		});
	}

	public String generateRandomSecretKey() {
		SecureRandom random = modelFactoryBuilder.getSecureRandomInstance();
		byte[] bytes = modelFactoryBuilder.instantiateBytes();
		random.nextBytes(bytes);
		Base32 base32 = modelFactoryBuilder.getBase32Instance();
		return base32.encodeToString(bytes);
	}

	public AuthenticationTwoFactorCompanyConfiguration getConfiguration(long companyId) throws PortalException {
		try {
			return configurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, companyId);
		} catch (ConfigurationException e) {
			LOG.error(e);
			throw new PortalException(e);
		}
	}

	public String getOTPUrl(AuthenticationTwoFactorCompanyConfiguration authConfiguration, String emailAddress, String secretKey) {
		String qrBarCodeUrl = authConfiguration.qrCodeUrl();
		String issuer = authConfiguration.qrCodeIssuer();
		String algorithm = authConfiguration.algorithmHmac();
		int authenticatorCodeLength = authConfiguration.authenticatorCodeLength();
		int authenticatorCodeDurationInSecs = authConfiguration.authenticatorCodeDuration();
		return totpService.getOTPUrl(qrBarCodeUrl, issuer, emailAddress, secretKey, algorithm, authenticatorCodeLength, authenticatorCodeDurationInSecs).orElse(StringPool.BLANK);
	}

	public UserTwoFactorAuthenticationPK getPrimaryKey(long companyId, long userId) {
		return new UserTwoFactorAuthenticationPK(companyId, userId);
	}

	public UserTwoFactorAuthenticationPK getPrimaryKey(User user) {
		return getPrimaryKey(user.getCompanyId(), user.getUserId());
	}

	public boolean verifyUserOneTimeCode(String secretKey, String oneTimeCode) {
		return totpService.verifyUserOneTimeCode(secretKey, oneTimeCode);
	}

}
