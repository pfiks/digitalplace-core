package com.placecube.digitalplace.search.web.portlet.category.util;

import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.web.portlet.category.constants.JSONKeys;

@Component(immediate = true, service = CategoryFacetTreeUtil.class)
public class CategoryFacetTreeUtil {

	private static final Log LOG = LogFactoryUtil.getLog(CategoryFacetTreeUtil.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private JSONFactory jsonFactory;

	public Optional<AssetCategory> fetchCategoryById(long categoryId) {
		try {
			return Optional.of( assetCategoryLocalService.getCategory(categoryId));
		} catch (PortalException e) {
			LOG.debug("Could not get category [" + categoryId + "]");
		}
		return Optional.empty();
	}

	public void pruneCategoryTree(JSONArray prunedParentArray, JSONArray parentArray, Set<Long> categoriesToPrune) {
		for (int i = 0; i < parentArray.length(); i++) {
			JSONObject leaf = parentArray.getJSONObject(i);
			if (!categoriesToPrune.contains(leaf.getLong(JSONKeys.ID))) {
				prunedParentArray.put(leaf);
				if (leaf.has(JSONKeys.CHILDREN)) {
					JSONArray originalChildrenArray = leaf.getJSONArray(JSONKeys.CHILDREN);
					leaf.remove(JSONKeys.CHILDREN);
					JSONArray prunedChildrenArray = jsonFactory.createJSONArray();
					pruneCategoryTree(prunedChildrenArray, originalChildrenArray, categoriesToPrune);
					leaf.put(JSONKeys.CHILDREN, prunedChildrenArray);
				}
			}
		}
	}

	public void buildCategoryTreeBranch(Optional<FacetResult> facetResultOpt, AssetCategory assetCategory, JSONArray categoryTree, Map<Long, JSONObject> insertedCategories,
			Set<String> activeValuesForFilter, int maxDepth, Set<Long> categoriesToPrune, Locale locale) {
		long categoryId = assetCategory.getCategoryId();
		long parentCategoryId = assetCategory.getParentCategoryId();
		boolean isCategorySelected = isCategorySelected(activeValuesForFilter, categoryId);
		if (parentCategoryId == 0) {
			// put category as root category in the tree
			JSONObject rootCategoryObject = createCategoryJSONObject(facetResultOpt, assetCategory, isCategorySelected, 0, locale);
			categoryTree.put(rootCategoryObject);
			insertedCategories.put(categoryId, rootCategoryObject);
		} else {
			// check if parent category was already added to the tree
			if (insertedCategories.containsKey(parentCategoryId)) {
				JSONObject parentCategoryObject = insertedCategories.get(parentCategoryId);
				// check if parent category already has children
				JSONArray childrenArray;
				boolean hasChildren = parentCategoryObject.has(JSONKeys.CHILDREN);
				if (!hasChildren) {
					childrenArray = jsonFactory.createJSONArray();
				} else {
					childrenArray = parentCategoryObject.getJSONArray(JSONKeys.CHILDREN);
				}
				int childDepth = parentCategoryObject.getInt(JSONKeys.DEPTH) + 1;
				if (childDepth < maxDepth) {
					JSONObject childObject = createCategoryJSONObject(facetResultOpt, assetCategory, isCategorySelected, childDepth, locale);
					insertedCategories.put(categoryId, childObject);
					childrenArray.put(childObject);
					// add new child category to parent category
					parentCategoryObject.put(JSONKeys.CHILDREN, childrenArray);
					// parent category is not a leaf, since it has children
					parentCategoryObject.put(JSONKeys.LEAF, Boolean.FALSE);
					// expand all parent categories if child is selected
					if (isCategorySelected) {
						expandParents(assetCategory.getParentCategory(), insertedCategories);
					}
				} else if (parentCategoryObject.getInt(JSONKeys.FREQUENCY) == 0) {
					// if parent has 0 frequency then prune it
					categoriesToPrune.add(parentCategoryObject.getLong(JSONKeys.ID));
				} else {
					// if parent has larger frequency then set is as a leaf
					parentCategoryObject.put(JSONKeys.LEAF, Boolean.TRUE);
				}
			} else {
				// insert the parent to the tree and then insert the category itself to the tree
				buildCategoryTreeBranch(Optional.empty(), assetCategory.getParentCategory(), categoryTree, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, locale);
				// check if parent was inserted
				if(insertedCategories.containsKey(parentCategoryId)) {
					buildCategoryTreeBranch(facetResultOpt, assetCategory, categoryTree, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, locale);
				}
			}
		}
	}


	private Boolean isCategorySelected(Set<String> activeValuesForFilter, long categoryId) {
		return activeValuesForFilter.contains(String.valueOf(categoryId));
	}


	private void expandParents(AssetCategory assetCategory, Map<Long, JSONObject> putCategories) {
		JSONObject parentCategoryObject = putCategories.get(assetCategory.getCategoryId());
		if (parentCategoryObject.get(JSONKeys.EXPANDED) == Boolean.TRUE) {
			return;
		}
		parentCategoryObject.put(JSONKeys.EXPANDED, Boolean.TRUE);
		if (assetCategory.getParentCategoryId() > 0) {
			expandParents(assetCategory.getParentCategory(), putCategories);
		}
	}

	private JSONObject createCategoryJSONObject(Optional<FacetResult> facetResult, AssetCategory assetCategory, Boolean isCategorySelected, int depth, Locale locale) {
		long categoryId = assetCategory.getCategoryId();
		int frequency = facetResult.map(FacetResult::getFrequency).orElse(0);
		String label = assetCategory.getTitle(locale) + " (" + frequency + ")";
		JSONObject node = jsonFactory.createJSONObject();
		node.put(JSONKeys.LABEL, label);
		node.put(JSONKeys.ID, categoryId);
		node.put(JSONKeys.DEPTH, depth);
		node.put(JSONKeys.TYPE, JSONKeys.CHECK);
		node.put(JSONKeys.LEAF, Boolean.TRUE);
		node.put(JSONKeys.CHECKED, isCategorySelected);
		node.put(JSONKeys.FREQUENCY, frequency);
		if (isCategorySelected) {
			node.put(JSONKeys.EXPANDED, Boolean.TRUE);
		}
		return node;
	}
}
