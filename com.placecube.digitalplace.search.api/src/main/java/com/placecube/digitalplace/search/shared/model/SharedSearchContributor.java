package com.placecube.digitalplace.search.shared.model;

/**
 * Used to contribute logic to the search executed as a shared search request
 *
 */
public interface SharedSearchContributor {

	void contribute(SharedSearchContributorSettings sharedSearchContributorSettings);

}