package com.placecube.digitalplace.appointment.booking.service.impl;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.appointment.booking.connector.AppointmentBookingConnector;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.booking.registry.AppointmentsRegistry;
import com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService;

@RunWith(MockitoJUnitRunner.class)
public class AppointmentBookingServiceImplTest {

	private static final long COMPANY_ID = 123;
	private static final String SERVICE_ID = "serviceIdVal";

	@InjectMocks
	private AppointmentBookingServiceImpl appointmentBookingServiceImpl;

	@Mock
	private AppointmentBookingSlot mockAppointmentBookingSlot1;

	@Mock
	private AppointmentBookingSlot mockAppointmentBookingSlot2;

	@Mock
	private AppointmentBookingSlot mockAppointmentBookingSlot3;

	@Mock
	private AppointmentBookingConnector mockAppointmentConnector1;

	@Mock
	private AppointmentBookingConnector mockAppointmentConnector2;

	@Mock
	private AppointmentContext mockAppointmentContext;

	@Mock
	private AppointmentBookingEntry mockAppointmentEntry1;

	@Mock
	private AppointmentBookingEntry mockAppointmentEntry2;

	@Mock
	private AppointmentBookingEntry mockAppointmentEntry3;

	@Mock
	private AppointmentEntryLocalService mockAppointmentEntryLocalService;

	@Mock
	private AppointmentsRegistry mockAppointmentsRegistry;

	@Mock
	private Date mockDate1;

	@Mock
	private Date mockDate2;

	@Mock
	private Date mockDate3;

	@Mock
	private Date mockEndDate;

	@Mock
	private Date mockStartDate;

	@Mock
	private TimeZone mockTimeZone;

	@Test(expected = PortalException.class)
	public void bookAppointment_WhenClassNameIdInvalid_ThenThrowsPortalException() throws PortalException {
		when(mockAppointmentEntry1.getClassPK()).thenReturn(1l);

		appointmentBookingServiceImpl.bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1);

		verifyNoInteractions(mockAppointmentsRegistry);
	}

	@Test(expected = PortalException.class)
	public void bookAppointment_WhenClassPKInvalid_ThenThrowsPortalException() throws PortalException {
		appointmentBookingServiceImpl.bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1);

		verifyNoInteractions(mockAppointmentsRegistry);
	}

	@Test(expected = PortalException.class)
	public void bookAppointment_WhenEndDateInvalid_ThenThrowsPortalException() throws PortalException {
		when(mockAppointmentEntry1.getClassPK()).thenReturn(1l);
		when(mockAppointmentEntry1.getClassNameId()).thenReturn(2l);
		when(mockAppointmentEntry1.getStartDate()).thenReturn(mockDate1);

		appointmentBookingServiceImpl.bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1);

		verifyNoInteractions(mockAppointmentsRegistry);
	}

	@Test(expected = PortalException.class)
	public void bookAppointment_WhenStartDateInvalid_ThenThrowsPortalException() throws PortalException {
		when(mockAppointmentEntry1.getClassPK()).thenReturn(1l);
		when(mockAppointmentEntry1.getClassNameId()).thenReturn(2l);

		appointmentBookingServiceImpl.bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1);

		verifyNoInteractions(mockAppointmentsRegistry);
	}

	@Test(expected = PortalException.class)
	public void bookAppointment_WhenTimeZoneInvalid_ThenThrowsPortalException() throws PortalException {
		when(mockAppointmentEntry1.getClassPK()).thenReturn(1l);
		when(mockAppointmentEntry1.getClassNameId()).thenReturn(2l);
		when(mockAppointmentEntry1.getStartDate()).thenReturn(mockDate1);
		when(mockAppointmentEntry1.getEndDate()).thenReturn(mockDate2);

		appointmentBookingServiceImpl.bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1);

		verifyNoInteractions(mockAppointmentsRegistry);
	}

	@Test
	public void bookAppointment_WhenValidDetails_ThenBooksTheAppointmentForEachConnectorFoundAndAddsEntriesIfIdsAreValid() throws PortalException {
		when(mockAppointmentEntry1.getClassPK()).thenReturn(1l);
		when(mockAppointmentEntry1.getClassNameId()).thenReturn(2l);
		when(mockAppointmentEntry1.getTimeZone()).thenReturn(mockTimeZone);
		when(mockAppointmentEntry1.getStartDate()).thenReturn(mockDate1);
		when(mockAppointmentEntry1.getEndDate()).thenReturn(mockDate2);
		when(mockAppointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID)).thenReturn(Set.of(mockAppointmentConnector1, mockAppointmentConnector2));
		when(mockAppointmentConnector1.bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1)).thenReturn(List.of("id1", "id2"));
		when(mockAppointmentConnector2.bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1)).thenReturn(Collections.emptyList());

		appointmentBookingServiceImpl.bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1);

		verify(mockAppointmentConnector1, times(1)).bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1);
		verify(mockAppointmentConnector2, times(1)).bookAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1);
		verify(mockAppointmentEntryLocalService, times(1)).addAppointmentEntry(COMPANY_ID, SERVICE_ID, mockAppointmentConnector1.getClass().getName(), mockAppointmentEntry1, "id1");
		verify(mockAppointmentEntryLocalService, times(1)).addAppointmentEntry(COMPANY_ID, SERVICE_ID, mockAppointmentConnector1.getClass().getName(), mockAppointmentEntry1, "id2");
	}

	@Test
	public void cancelAppointment_WhenNoError_ThenCancelsTheAppointmentForEachConnectorFoundAndRemovesTheEntries() {
		String appointmentId = "appointmentIdValue";
		long classPK = 12;
		long classNameId = 34;
		when(mockAppointmentEntry1.getAppointmentId()).thenReturn(appointmentId);
		when(mockAppointmentEntry1.getClassPK()).thenReturn(classPK);
		when(mockAppointmentEntry1.getClassNameId()).thenReturn(classNameId);
		when(mockAppointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID)).thenReturn(Set.of(mockAppointmentConnector1, mockAppointmentConnector2));
		when(mockAppointmentConnector1.cancelAppointment(COMPANY_ID, SERVICE_ID, appointmentId)).thenReturn(List.of("id1", "id2"));
		when(mockAppointmentConnector2.cancelAppointment(COMPANY_ID, SERVICE_ID, appointmentId)).thenReturn(Collections.emptyList());

		appointmentBookingServiceImpl.cancelAppointment(COMPANY_ID, SERVICE_ID, mockAppointmentEntry1);

		verify(mockAppointmentConnector1, times(1)).cancelAppointment(COMPANY_ID, SERVICE_ID, appointmentId);
		verify(mockAppointmentConnector2, times(1)).cancelAppointment(COMPANY_ID, SERVICE_ID, appointmentId);
		verify(mockAppointmentEntryLocalService, times(1)).removeAppointmentEntry(COMPANY_ID, SERVICE_ID, mockAppointmentConnector1.getClass().getName(), classPK, classNameId, "id1");
		verify(mockAppointmentEntryLocalService, times(1)).removeAppointmentEntry(COMPANY_ID, SERVICE_ID, mockAppointmentConnector1.getClass().getName(), classPK, classNameId, "id2");
	}

	@Test
	public void getAppointments_WithCompanyIdAndServiceIdAndAppointmentContextParameters_WhenNoError_ThenReturnsAllTheAppointmentsForEachConnectorFound() {
		when(mockAppointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID)).thenReturn(Set.of(mockAppointmentConnector1, mockAppointmentConnector2));

		List<AppointmentBookingEntry> appointments1 = new LinkedList<>();
		appointments1.add(mockAppointmentEntry1);
		appointments1.add(mockAppointmentEntry2);
		when(mockAppointmentConnector1.getAppointments(COMPANY_ID, SERVICE_ID, mockAppointmentContext)).thenReturn(appointments1);

		List<AppointmentBookingEntry> appointments2 = new LinkedList<>();
		appointments1.add(mockAppointmentEntry3);
		when(mockAppointmentConnector2.getAppointments(COMPANY_ID, SERVICE_ID, mockAppointmentContext)).thenReturn(appointments2);

		List<AppointmentBookingEntry> results = appointmentBookingServiceImpl.getAppointments(COMPANY_ID, SERVICE_ID, mockAppointmentContext);

		assertThat(results, contains(mockAppointmentEntry1, mockAppointmentEntry2, mockAppointmentEntry3));
	}

	@Test
	public void getAppointments_WithCompanyIdAndServiceIdAndAppointmentIdParameters_WhenNoError_ThenReturnsAllTheAppointmentsForEachConnectorFound() {
		when(mockAppointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID)).thenReturn(Set.of(mockAppointmentConnector1, mockAppointmentConnector2));
		String apointmentId = "myAppointmentId";

		List<AppointmentBookingEntry> appointments1 = new LinkedList<>();
		appointments1.add(mockAppointmentEntry1);
		appointments1.add(mockAppointmentEntry2);
		when(mockAppointmentConnector1.getAppointments(COMPANY_ID, SERVICE_ID, apointmentId)).thenReturn(appointments1);

		List<AppointmentBookingEntry> appointments2 = new LinkedList<>();
		appointments1.add(mockAppointmentEntry3);
		when(mockAppointmentConnector2.getAppointments(COMPANY_ID, SERVICE_ID, apointmentId)).thenReturn(appointments2);

		List<AppointmentBookingEntry> results = appointmentBookingServiceImpl.getAppointments(COMPANY_ID, SERVICE_ID, apointmentId);

		assertThat(results, contains(mockAppointmentEntry1, mockAppointmentEntry2, mockAppointmentEntry3));
	}

	@Test
	public void getAppointments_WithCompanyIdAndServiceIdAndStartDateAndEndDateAndTimeZoneParameters_WhenNoError_ThenReturnsAllTheAppointmentsForEachConnectorFound() {
		when(mockAppointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID)).thenReturn(Set.of(mockAppointmentConnector1, mockAppointmentConnector2));

		List<AppointmentBookingEntry> appointments1 = new LinkedList<>();
		appointments1.add(mockAppointmentEntry1);
		appointments1.add(mockAppointmentEntry2);
		when(mockAppointmentConnector1.getAppointments(COMPANY_ID, SERVICE_ID, mockStartDate, mockEndDate, mockTimeZone)).thenReturn(appointments1);

		List<AppointmentBookingEntry> appointments2 = new LinkedList<>();
		appointments1.add(mockAppointmentEntry3);
		when(mockAppointmentConnector2.getAppointments(COMPANY_ID, SERVICE_ID, mockStartDate, mockEndDate, mockTimeZone)).thenReturn(appointments2);

		List<AppointmentBookingEntry> results = appointmentBookingServiceImpl.getAppointments(COMPANY_ID, SERVICE_ID, mockStartDate, mockEndDate, mockTimeZone);

		assertThat(results, contains(mockAppointmentEntry1, mockAppointmentEntry2, mockAppointmentEntry3));
	}

	@Test
	public void getAppointmentSlots_WhenNoError_ThenReturnsAllTheAppointmentSlotsForEachConnectorFound() throws ParseException {
		when(mockAppointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID)).thenReturn(Set.of(mockAppointmentConnector1, mockAppointmentConnector2));

		List<AppointmentBookingSlot> appointments1 = new LinkedList<>();
		appointments1.add(mockAppointmentBookingSlot1);
		appointments1.add(mockAppointmentBookingSlot2);
		when(mockAppointmentConnector1.getAppointmentSlots(COMPANY_ID, SERVICE_ID, mockAppointmentContext)).thenReturn(appointments1);

		List<AppointmentBookingSlot> appointments2 = new LinkedList<>();
		appointments1.add(mockAppointmentBookingSlot3);
		when(mockAppointmentConnector2.getAppointmentSlots(COMPANY_ID, SERVICE_ID, mockAppointmentContext)).thenReturn(appointments2);

		List<AppointmentBookingSlot> results = appointmentBookingServiceImpl.getAppointmentSlots(COMPANY_ID, SERVICE_ID, mockAppointmentContext);

		assertThat(results, contains(mockAppointmentBookingSlot1, mockAppointmentBookingSlot2, mockAppointmentBookingSlot3));
	}

	@Test
	public void getDatesWithAvailableAppoinmentsForDayOfWeek_WithCompanyIdAndServiceIdAndDayOfWeekAndTimeZoneParameters_WhenNoError_ThenReturnsAllTheDatesAvailableForEachConnectorFound() {
		int dayOfWeek = 56;

		Set<AppointmentBookingConnector> connectors = new LinkedHashSet<>();
		connectors.add(mockAppointmentConnector1);
		connectors.add(mockAppointmentConnector2);
		when(mockAppointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID)).thenReturn(connectors);

		List<Date> appointments1 = new LinkedList<>();
		appointments1.add(mockDate1);
		appointments1.add(mockDate2);
		when(mockAppointmentConnector1.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, dayOfWeek, mockTimeZone)).thenReturn(appointments1);

		List<Date> appointments2 = new LinkedList<>();
		appointments1.add(mockDate3);
		when(mockAppointmentConnector2.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, dayOfWeek, mockTimeZone)).thenReturn(appointments2);

		List<Date> results = appointmentBookingServiceImpl.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, dayOfWeek, mockTimeZone);

		assertThat(results, contains(mockDate1, mockDate2, mockDate3));
	}

	@Test
	public void getDatesWithAvailableAppoinmentsForDayOfWeek_WithCompanyIdAndServiceIdAndNumberOfDatesAndDayOfWeekAndTimeZoneAndAppointmentNumberParameters_WhenNoError_ThenReturnsAllTheDatesAvailableForEachConnectorFound() {
		int onlyDatesWithLessThanXappoinments = 12;
		int numberOfDatesToReturn = 34;
		int dayOfWeek = 56;

		Set<AppointmentBookingConnector> connectors = new LinkedHashSet<>();
		connectors.add(mockAppointmentConnector1);
		connectors.add(mockAppointmentConnector2);
		when(mockAppointmentsRegistry.getEnabledAppointmentConnectors(COMPANY_ID)).thenReturn(connectors);

		List<Date> appointments1 = new LinkedList<>();
		appointments1.add(mockDate1);
		appointments1.add(mockDate2);
		when(mockAppointmentConnector1.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, numberOfDatesToReturn, dayOfWeek, mockTimeZone, onlyDatesWithLessThanXappoinments))
				.thenReturn(appointments1);

		List<Date> appointments2 = new LinkedList<>();
		appointments1.add(mockDate3);
		when(mockAppointmentConnector2.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, numberOfDatesToReturn, dayOfWeek, mockTimeZone, onlyDatesWithLessThanXappoinments))
				.thenReturn(appointments2);

		List<Date> results = appointmentBookingServiceImpl.getDatesWithAvailableAppoinmentsForDayOfWeek(COMPANY_ID, SERVICE_ID, numberOfDatesToReturn, dayOfWeek, mockTimeZone,
				onlyDatesWithLessThanXappoinments);

		assertThat(results, contains(mockDate1, mockDate2, mockDate3));
	}
}
