/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.appointment.service;

import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * Provides a wrapper for {@link AppointmentSlotLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentSlotLocalService
 * @generated
 */
public class AppointmentSlotLocalServiceWrapper
	implements AppointmentSlotLocalService,
			   ServiceWrapper<AppointmentSlotLocalService> {

	public AppointmentSlotLocalServiceWrapper() {
		this(null);
	}

	public AppointmentSlotLocalServiceWrapper(
		AppointmentSlotLocalService appointmentSlotLocalService) {

		_appointmentSlotLocalService = appointmentSlotLocalService;
	}

	/**
	 * Adds the appointment slot to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentSlotLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentSlot the appointment slot
	 * @return the appointment slot that was added
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentSlot
		addAppointmentSlot(
			com.placecube.digitalplace.appointment.model.AppointmentSlot
				appointmentSlot) {

		return _appointmentSlotLocalService.addAppointmentSlot(appointmentSlot);
	}

	@Override
	public int countAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(
		long companyId, String slotType, String serviceId) {

		return _appointmentSlotLocalService.
			countAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(
				companyId, slotType, serviceId);
	}

	/**
	 * Creates a new appointment slot with the primary key. Does not add the appointment slot to the database.
	 *
	 * @param appointmentSlotId the primary key for the new appointment slot
	 * @return the new appointment slot
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentSlot
		createAppointmentSlot(long appointmentSlotId) {

		return _appointmentSlotLocalService.createAppointmentSlot(
			appointmentSlotId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentSlotLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the appointment slot from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentSlotLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentSlot the appointment slot
	 * @return the appointment slot that was removed
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentSlot
		deleteAppointmentSlot(
			com.placecube.digitalplace.appointment.model.AppointmentSlot
				appointmentSlot) {

		return _appointmentSlotLocalService.deleteAppointmentSlot(
			appointmentSlot);
	}

	/**
	 * Deletes the appointment slot with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentSlotLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot that was removed
	 * @throws PortalException if a appointment slot with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentSlot
			deleteAppointmentSlot(long appointmentSlotId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentSlotLocalService.deleteAppointmentSlot(
			appointmentSlotId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentSlotLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _appointmentSlotLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _appointmentSlotLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _appointmentSlotLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _appointmentSlotLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _appointmentSlotLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _appointmentSlotLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _appointmentSlotLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _appointmentSlotLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentSlot
		fetchAppointmentSlot(long appointmentSlotId) {

		return _appointmentSlotLocalService.fetchAppointmentSlot(
			appointmentSlotId);
	}

	/**
	 * Returns the appointment slot with the matching UUID and company.
	 *
	 * @param uuid the appointment slot's UUID
	 * @param companyId the primary key of the company
	 * @return the matching appointment slot, or <code>null</code> if a matching appointment slot could not be found
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentSlot
		fetchAppointmentSlotByUuidAndCompanyId(String uuid, long companyId) {

		return _appointmentSlotLocalService.
			fetchAppointmentSlotByUuidAndCompanyId(uuid, companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _appointmentSlotLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.appointment.booking.model.
			AppointmentBookingSlot> getAppointmentBookingSlots(
				com.placecube.digitalplace.appointment.model.AppointmentSlot
					appointmentSlot,
				java.util.Date fromDate, java.util.Date toDate) {

		return _appointmentSlotLocalService.getAppointmentBookingSlots(
			appointmentSlot, fromDate, toDate);
	}

	/**
	 * Returns the appointment slot with the primary key.
	 *
	 * @param appointmentSlotId the primary key of the appointment slot
	 * @return the appointment slot
	 * @throws PortalException if a appointment slot with the primary key could not be found
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentSlot
			getAppointmentSlot(long appointmentSlotId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentSlotLocalService.getAppointmentSlot(
			appointmentSlotId);
	}

	/**
	 * Returns the appointment slot with the matching UUID and company.
	 *
	 * @param uuid the appointment slot's UUID
	 * @param companyId the primary key of the company
	 * @return the matching appointment slot
	 * @throws PortalException if a matching appointment slot could not be found
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentSlot
			getAppointmentSlotByUuidAndCompanyId(String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentSlotLocalService.
			getAppointmentSlotByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of all the appointment slots.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.appointment.model.impl.AppointmentSlotModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of appointment slots
	 * @param end the upper bound of the range of appointment slots (not inclusive)
	 * @return the range of appointment slots
	 */
	@Override
	public java.util.List
		<com.placecube.digitalplace.appointment.model.AppointmentSlot>
			getAppointmentSlots(int start, int end) {

		return _appointmentSlotLocalService.getAppointmentSlots(start, end);
	}

	@Override
	public java.util.List
		<com.placecube.digitalplace.appointment.model.AppointmentSlot>
			getAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(
				long companyId, String slotType, String serviceId, int start,
				int end) {

		return _appointmentSlotLocalService.
			getAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(
				companyId, slotType, serviceId, start, end);
	}

	/**
	 * Returns the number of appointment slots.
	 *
	 * @return the number of appointment slots
	 */
	@Override
	public int getAppointmentSlotsCount() {
		return _appointmentSlotLocalService.getAppointmentSlotsCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _appointmentSlotLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _appointmentSlotLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _appointmentSlotLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appointmentSlotLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the appointment slot in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppointmentSlotLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appointmentSlot the appointment slot
	 * @return the appointment slot that was updated
	 */
	@Override
	public com.placecube.digitalplace.appointment.model.AppointmentSlot
		updateAppointmentSlot(
			com.placecube.digitalplace.appointment.model.AppointmentSlot
				appointmentSlot) {

		return _appointmentSlotLocalService.updateAppointmentSlot(
			appointmentSlot);
	}

	@Override
	public BasePersistence<?> getBasePersistence() {
		return _appointmentSlotLocalService.getBasePersistence();
	}

	@Override
	public AppointmentSlotLocalService getWrappedService() {
		return _appointmentSlotLocalService;
	}

	@Override
	public void setWrappedService(
		AppointmentSlotLocalService appointmentSlotLocalService) {

		_appointmentSlotLocalService = appointmentSlotLocalService;
	}

	private AppointmentSlotLocalService _appointmentSlotLocalService;

}