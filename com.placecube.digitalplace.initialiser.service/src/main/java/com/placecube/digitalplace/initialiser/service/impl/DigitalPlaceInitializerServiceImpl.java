package com.placecube.digitalplace.initialiser.service.impl;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.petra.string.CharPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.initialiser.service.DigitalPlaceInitializerService;
import com.placecube.digitalplace.initialiser.service.constants.ConfigurationKeys;
import com.placecube.digitalplace.initialiser.service.util.PageCreationUtil;
import com.placecube.digitalplace.initialiser.service.util.PageTemplateCreationUtil;
import com.placecube.digitalplace.initialiser.service.util.ParserUtil;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.LayoutInitializer;

@SuppressWarnings("rawtypes")
@Component(immediate = true, service = DigitalPlaceInitializerService.class)
public class DigitalPlaceInitializerServiceImpl implements DigitalPlaceInitializerService {

	@Reference
	private LayoutInitializer layoutInitializer;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private LayoutPageTemplateEntryLocalService layoutPageTemplateEntryLocalService;

	@Reference
	private PageCreationUtil pageCreationUtil;

	@Reference
	private PageTemplateCreationUtil pageTemplateCreationUtil;

	@Reference
	private ParserUtil parserUtil;

	@Override
	public Layout configurePageAsFirstLayout(Layout layout) throws PortalException {
		return layoutLocalService.updatePriority(layout, 0);
	}

	@Override
	public Layout initializeContentPage(Class clazz, JSONObject pageJSONObject, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException {
		try {
			Set<FragmentContext> fragments = pageCreationUtil.getFragments(pageJSONObject.getJSONArray("fragments"));
			Set<PortletContext> portlets = pageCreationUtil.getPortlets(serviceContext.getScopeGroupId(), pageJSONObject.getJSONArray("portlets"),
					pageJSONObject.getJSONArray("contentSetPlaceholders"), preferencesPlaceholderValues, ConfigurationKeys.DEFAULT);
			Map<String, String> pageTypeSettings = parserUtil.getNameValueMap(pageJSONObject.getJSONArray("pageTypeSettings"), preferencesPlaceholderValues);
			return layoutInitializer.getOrCreateContentLayout(pageCreationUtil.getContentPageToCreate(clazz, pageJSONObject, fragments, portlets, pageTypeSettings), serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	@Override
	public Layout initializeContentPage(Class clazz, String jsonFilePath, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException {
		try {
			JSONObject pageJSONObject = pageCreationUtil.getJSONObject(clazz, jsonFilePath);
			return initializeContentPage(clazz, pageJSONObject, preferencesPlaceholderValues, serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	@Override
	public Layout initializeLinkToLayoutPage(Class clazz, String jsonFilePath, ServiceContext serviceContext) throws PortalException {
		try {
			JSONObject pageJSONObject = pageCreationUtil.getJSONObject(clazz, jsonFilePath);

			return layoutInitializer.getOrCreateLinkToPageLayout(pageCreationUtil.getLinkToLayoutPageToCreate(clazz, pageJSONObject), serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	@Override
	public Layout initializePageFromPageTemplate(Map<Locale, String> pageTitleMap, String friendlyURL, boolean isPrivate, LayoutPageTemplateEntry layoutPageTemplateEntry,
			ServiceContext serviceContext) throws PortalException {
		String pageTitle = pageTitleMap.values().iterator().next();
		LayoutContext layoutContext = pageCreationUtil.getPageFromPageTemplateToCreate(pageTitle, friendlyURL, isPrivate, layoutPageTemplateEntry);
		for (Entry<Locale, String> entry : pageTitleMap.entrySet()) {
			layoutContext.addLocalisedName(entry.getKey(), entry.getValue());
			layoutContext.addLocalisedTitle(entry.getKey(), entry.getValue());
		}
		return layoutInitializer.getOrCreatePageTemplateLayout(layoutContext, layoutPageTemplateEntry, serviceContext);
	}

	@Override
	public Layout initializePageFromPageTemplate(String pageTitle, String friendlyURL, boolean isPrivate, LayoutPageTemplateEntry layoutPageTemplateEntry, Map<String, String[]> rolePermissionsToAdd,
			ServiceContext serviceContext) throws PortalException {

		LayoutContext layoutContext = pageCreationUtil.getPageFromPageTemplateToCreate(pageTitle, friendlyURL, isPrivate, layoutPageTemplateEntry);
		layoutContext.setRolePermissionsToAdd(rolePermissionsToAdd);
		return layoutInitializer.getOrCreatePageTemplateLayout(layoutContext, layoutPageTemplateEntry, serviceContext);
	}

	@Override
	public Layout initializePageFromPageTemplate(String pageTitle, String friendlyURL, boolean isPrivate, LayoutPageTemplateEntry layoutPageTemplateEntry, ServiceContext serviceContext)
			throws PortalException {
		return initializePageFromPageTemplate(pageTitle, friendlyURL, isPrivate, layoutPageTemplateEntry, Collections.emptyMap(), serviceContext);
	}

	@Override
	public LayoutPageTemplateEntry initializePageTemplate(Class clazz, String jsonFilePath, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException {
		try {
			JSONObject pageTemplateJSONObject = pageCreationUtil.getJSONObject(clazz, jsonFilePath);

			String pageTemplateName = pageTemplateJSONObject.getString("pageTemplateName");
			String layoutPageTemplateEntryKey = StringUtil.toLowerCase(pageTemplateName.trim());
			layoutPageTemplateEntryKey = StringUtil.replace(layoutPageTemplateEntryKey, CharPool.SPACE, CharPool.DASH);

			LayoutPageTemplateEntry layoutPageTemplateEntry = layoutPageTemplateEntryLocalService.fetchLayoutPageTemplateEntry(serviceContext.getScopeGroupId(), layoutPageTemplateEntryKey);
			if (Validator.isNotNull(layoutPageTemplateEntry)) {
				return layoutPageTemplateEntry;
			}
			LayoutContext layoutContext = pageCreationUtil.getPageTemplateToCreate(pageTemplateJSONObject);
			boolean isDisplayPageTemplate = GetterUtil.getBoolean(pageTemplateJSONObject.getBoolean("displayPageTemplate"), false);
			boolean isWidgetTemplatePage = GetterUtil.getBoolean(pageTemplateJSONObject.getBoolean("widgetPageTemplate"), false);
			if (isDisplayPageTemplate) {
				return pageTemplateCreationUtil.createDisplayPageTemplate(layoutContext, pageTemplateJSONObject, preferencesPlaceholderValues, serviceContext);
			}
			if (isWidgetTemplatePage) {
				return pageTemplateCreationUtil.createWidgetPageTemplate(layoutContext, pageTemplateJSONObject, preferencesPlaceholderValues, serviceContext);
			}
			return pageTemplateCreationUtil.createContentPageTemplate(layoutContext, pageTemplateJSONObject, preferencesPlaceholderValues, serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	@Override
	public Layout initializeWidgetPage(Class clazz, JSONObject pageJSONObject, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException {
		try {
			Map<Integer, Set<PortletContext>> columnsConfigs = pageCreationUtil.getColumns(pageJSONObject.getJSONArray("columns"), preferencesPlaceholderValues, ConfigurationKeys.DEFAULT);

			return layoutInitializer.getOrCreatePortletLayout(pageCreationUtil.getWidgetPageToCreate(clazz, pageJSONObject, columnsConfigs), serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	@Override
	public Layout initializeWidgetPage(Class clazz, String jsonFilePath, Map<String, String> preferencesPlaceholderValues, ServiceContext serviceContext) throws PortalException {
		try {
			JSONObject pageJSONObject = pageCreationUtil.getJSONObject(clazz, jsonFilePath);

			return initializeWidgetPage(clazz, pageJSONObject, preferencesPlaceholderValues, serviceContext);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	@Override
	public Layout initializeWidgetPage(Class clazz, String jsonFilePath, ServiceContext serviceContext) throws PortalException {
		return initializeWidgetPage(clazz, jsonFilePath, Collections.emptyMap(), serviceContext);
	}
}
