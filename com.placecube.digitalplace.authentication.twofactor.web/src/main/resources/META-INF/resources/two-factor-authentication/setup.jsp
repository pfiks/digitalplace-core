<%@ include file="init.jsp" %>

<portlet:actionURL name="<%=TwoFactorMVCCommandKeys.SETUP %>" var="saveTwoFactorAuthentication">
	<portlet:param name="redirect" value="${redirect}" />
	<portlet:param name="twoFactorSecretKey" value="${twoFactorSecretKey}"/>
</portlet:actionURL>

<div class="two-factor-authenticator two-factor-authenticator-setup">

	<aui:form action="${ saveTwoFactorAuthentication }" cssClass="container-fluid container-fluid-max-xl sign-in-form" data-senna-off="true" method="post" name="saveTwoFactorAuthenticationForm" >
	
		<liferay-journal:journal-article 
			articleId="<%=WebContentArticles.TWO_FACTOR_SETUP.getArticleId() %>" 
			groupId="${themeDisplay.getCompanyGroupId()}" 
			showTitle="false" />		
		
		<liferay-ui:error key="twoFactorVerificationFailed" message="two-factor-authentication-has-failed" />
		
		<div class="form-group">		
			<p>
				<liferay-ui:message key="secret-key"/>
			</p>
			<p>${twoFactorSecretKey}</p>
			
			<img alt="qrcode" src="${twoFactorUrlQR}" class="" width="166" height="166">							
		</div>
		
		<aui:input autocomplete="off" label="authenticator-app-code" name="twoFactorAuthCode" required="true" />
		
		<aui:button-row>
			<aui:button type="submit" value="verify-code" />
		</aui:button-row>
	</aui:form>
</div>