package com.placecube.digitalplace.address.expando;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.pfiks.expando.creator.ExpandoColumnCreator;
import com.pfiks.expando.creator.ExpandoColumnCreatorInputStreamService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.placecube.digitalplace.address.constants.expando.UserExpandoUPRN;

@Component(immediate = true, property = { "expandocolumn.creator=user.uprn" }, service = ExpandoColumnCreator.class)
public class UserUPRNExpandoColumnCreator implements ExpandoColumnCreator {

	@Reference
	private ExpandoColumnCreatorInputStreamService expandoColumnCreatorInputStreamService;

	@Override
	public ExpandoColumn create(Company company) throws ExpandoColumnCreationException {
		InputStream inputStream = UserUPRNExpandoColumnCreator.class.getClassLoader()
				.getResourceAsStream("com/placecube/digitalplace/address/dependencies/expando/" + UserExpandoUPRN.FIELD_NAME + ".xml");
		return expandoColumnCreatorInputStreamService.createExpandoColumn(company, User.class.getName(), inputStream);
	}

}
