package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.UserAuthenticationService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class PasswordDetailsMVCRenderCommandTest extends PowerMockito {

	private static final long COMPANY_ID = 123;

	private static final long GROUP_ID = 456;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private UserAuthenticationService mockUserAuthenticationService;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@InjectMocks
	private PasswordDetailsMVCRenderCommand passwordDetailsMVCRenderCommand;

	@Test
	public void render_WhenNoError_ThenReturnsPasswordDetailsJSP() throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);

		String result = passwordDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/password-details.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetsPasswordPolicyDescriptionAsRequestAttribute() throws PortletException, UserAccountException {
		String passwordPolicyDescription = "passwordPolicyDescriptionValue";

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserAuthenticationService.getUserAccountPasswordPolicyHtml(mockRenderRequest)).thenReturn(passwordPolicyDescription);

		passwordDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("passwordPolicyDescription", passwordPolicyDescription);
	}

	@Test
	public void render_WhenNoError_ThenSetsPreviousStepAsRequestAttribute() throws PortletException, UserAccountException {
		String previousStep = "previousStep";

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getPasswordPreviousStep(mockAccountContext, mockUserProfileFieldsConfiguration)).thenReturn(previousStep);

		passwordDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("passwordPreviousStep", previousStep);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoErrors_ThenSetsAccountSecurityStepEnabledAttribute(boolean accountSecurityStepEnabled) throws PortletException, UserAccountException {

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.isAccountSecurityStepEnabled(mockAccountContext)).thenReturn(accountSecurityStepEnabled);

		passwordDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_SECURITY_STEP_ENABLED, accountSecurityStepEnabled);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoErrors_ThenSetShowCaptchaOnPasswordStepAttributeInRequest(boolean showCaptchaOnPasswordStep) throws PortletException, UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(GROUP_ID);
		when(mockAccountContextService.isShowCaptchaOnPasswordStep(mockAccountContext, mockRenderRequest)).thenReturn(showCaptchaOnPasswordStep);

		passwordDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.CAPTCHA_ENABLED, showCaptchaOnPasswordStep);
	}

	@Test
	public void render_WhenNoError_ThenUpdateContextInSession() throws PortletException, UserAccountException {
		String previousStep = "previousStep";

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountConfigurationService.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);
		when(mockAccountConfigurationService.getPasswordPreviousStep(mockAccountContext, mockUserProfileFieldsConfiguration)).thenReturn(previousStep);

		passwordDetailsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockRenderRequest);
	}

}
