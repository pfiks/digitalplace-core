<%@ include file="../init.jsp"%>

<c:choose>

	<c:when test="${invalidConfiguration}">
		<%@ include file="/common/invalid-configuration.jspf" %>
	</c:when>
	
	<c:otherwise>
	
		<portlet:actionURL name="<%= MVCCommandKeys.SEARCH %>" var="updateExpiredContentFilterURL">
			<portlet:param name="redirectURL" value="${redirectURL}" />
		</portlet:actionURL>
		
		<aui:form action="${updateExpiredContentFilterURL}" method="post" name="updateExpiredContentFilterForm">
			<aui:input
				label="${title}"
				title="${title}"
				id="includeExpiredContent"
				name="includeExpiredContent"
				type="toggle-switch"
				checked="${includeExpiredContent}"
				inlineField="true"
				cssClass="search-expired-content-filter-selector"
			/>
		</aui:form>
		
		
		<aui:script use="com-placecube-digitalplace-search-web">
			A.DigitalPlaceSearchWeb.submitFormOnChange(
				'.search-expired-content-filter-selector',
				'<portlet:namespace/>updateExpiredContentFilterForm'
			);
		</aui:script>

	</c:otherwise>
</c:choose>