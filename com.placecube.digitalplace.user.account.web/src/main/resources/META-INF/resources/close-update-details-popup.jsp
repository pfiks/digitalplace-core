<%@ include file="init.jsp" %>
<aui:script>
	
	Liferay.fire(
		'closeWindow',
		{
			id: '${ modelId }',
			redirect: '${ redirectUrl }' ? '${ redirectUrl }' : window.parent.location.href
		}
	);
	
</aui:script>