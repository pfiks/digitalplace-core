package com.placecube.digitalplace.oauth.dataprovider.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.Http;
import com.liferay.portal.kernel.util.SystemProperties;

@RunWith(MockitoJUnitRunner.class)
public class URLUtilTest {

	@Mock
	private HttpURLConnection mockHttpURLConnection;

	@Mock
	private OutputStream mockOutputStream;

	private MockedStatic<SystemProperties> mockSystemProperties;

	@Mock
	private URL mockURL;

	@InjectMocks
	private URLUtil urlUtil;

	@Before
	public void activateSetup() {
		mockSystemProperties = mockStatic(SystemProperties.class);
	}

	@Test
	public void buildURI_WhenNoErrors_ThenReturnsURIForUrl() throws Exception {
		URI result = urlUtil.buildURI("http://localhost:8080");
		assertThat(result, notNullValue());
	}

	@Test(expected = URISyntaxException.class)
	public void buildURI_WhenURIFailsToBuild_ThenThrowsException() throws Exception {
		urlUtil.buildURI("htp:\\test::123");
	}

	@Test
	public void buildURL_WhenNoErrors_ThenReplacesInputParametersInUrlWithEscapedValues() {
		final String url = "http://server:8080/{parameter1}/{parameter2}";

		Map<String, String> pathInputParametersMap = new HashMap<>();
		pathInputParametersMap.put("parameter1", "value1");
		pathInputParametersMap.put("parameter2", "value2");
		Mockito.mockStatic(HtmlUtil.class);
		when(HtmlUtil.escapeURL("value1")).thenReturn("value1escaped");
		when(HtmlUtil.escapeURL("value2")).thenReturn("value2escaped");

		String result = urlUtil.buildURL(pathInputParametersMap, url);

		assertThat(result, equalTo("http://server:8080/value1escaped/value2escaped"));
	}

	@Test
	public void getAbsoluteURL_WhenQueryIsEmpty_ThenReturnsUrl() {
		final String url = "http://server:8080/{parameter1}/{parameter2}";
		String result = urlUtil.getAbsoluteURL(StringPool.BLANK, url);

		assertThat(result, equalTo(url));
	}

	@Test
	public void getAbsoluteURL_WhenQueryIsNotEmpty_ThenRemovesQueryFromUrl() {
		final String query = "param1=value1";
		final String url = "http://server:8080/{parameter1}/{parameter2}";

		String result = urlUtil.getAbsoluteURL(query, url + "?" + query);

		assertThat(result, equalTo(url));
	}

	@Test
	public void getHostName_WhenHostDoesNotStartsWithwww_ThenReturnsFullHostName() {
		assertThat(urlUtil.getHostName("govuk.com"), equalTo("govuk.com"));
	}

	@Test
	public void getHostName_WhenHostStartsWithwww_ThenReturnsPartAfterwww() {
		assertThat(urlUtil.getHostName("www.govuk.com"), equalTo("govuk.com"));
	}

	@Test
	public void getHostPort_WhenPortIsMinusOneAndSchemeIsHttp_ThenReturnsHttpPort() {
		assertThat(urlUtil.getHostPort(-1, "http"), equalTo(Http.HTTP_PORT));
	}

	@Test
	public void getHostPort_WhenPortIsMinusOneAndSchemeIsHttps_ThenReturnsHttpsPort() {
		assertThat(urlUtil.getHostPort(-1, "https"), equalTo(Http.HTTPS_PORT));
	}

	@Test
	public void getHostPort_WhenPortIsNotMinusOne_ThenReturnsPort() {
		assertThat(urlUtil.getHostPort(20, "ftp"), equalTo(20));
	}

	@Test
	public void getProxySettingsMap_WhenProxyHostAndPortAreNotEmpty_ThenSetsThemInProxySettings() {
		final String httpProxyHost = "http-proxy";
		final String httpProxyPort = "20";

		mockSystemProperties.when(() -> SystemProperties.get("http.proxyHost")).thenReturn(httpProxyHost);
		mockSystemProperties.when(() -> SystemProperties.get("http.proxyPort")).thenReturn(httpProxyPort);

		Map<String, Object> result = urlUtil.getProxySettingsMap();

		assertThat(result.get("proxyHostName"), equalTo(httpProxyHost));
		assertThat(result.get("proxyHostPort"), equalTo(20));
	}

	@Test
	public void getProxySettingsMap_WhenProxyHostIsEmpty_ThenReturnsEmptyMap() {
		final String httpProxyPort = "20";

		mockSystemProperties.when(() -> SystemProperties.get("http.proxyPort")).thenReturn(httpProxyPort);

		Map<String, Object> result = urlUtil.getProxySettingsMap();

		assertThat(result.get("proxyHostName"), nullValue());
		assertThat(result.get("proxyHostPort"), nullValue());
	}

	@Test
	public void getProxySettingsMap_WhenProxyPortIsEmpty_ThenReturnsEmptyMap() {
		final String httpProxyHost = "http-proxy";

		mockSystemProperties.when(() -> SystemProperties.get("http.proxyHost")).thenReturn(httpProxyHost);

		Map<String, Object> result = urlUtil.getProxySettingsMap();

		assertThat(result.get("proxyHostName"), nullValue());
		assertThat(result.get("proxyHostPort"), nullValue());
	}

	@After
	public void teardown() {
		mockSystemProperties.close();
	}
}
