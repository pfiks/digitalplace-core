/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.placecube.digitalplace.appointment.service.impl;

import java.time.Instant;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.internal.utils.AppointmentUtils;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.service.base.AppointmentEntryLocalServiceBaseImpl;

/**
 * The implementation of the appointment entry local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.placecube.digitalplace.appointment.service.AppointmentEntryLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppointmentEntryLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.placecube.digitalplace.appointment.model.AppointmentEntry", service = AopService.class)
public class AppointmentEntryLocalServiceImpl extends AppointmentEntryLocalServiceBaseImpl {

	private static final Log LOG = LogFactoryUtil.getLog(AppointmentEntryLocalServiceImpl.class);

	@Reference
	private AppointmentUtils appointmentUtils;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>com.placecube.digitalplace.appointment.service.
	 * AppointmentEntryLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>com.placecube.digitalplace.appointment.service.
	 * AppointmentEntryLocalServiceUtil</code>.
	 */

	/**
	 * Creates a new appointment entry
	 *
	 * @param companyId the companyId
	 * @param serviceId the serviceId
	 * @param connectorClassName the connector classname
	 * @param appointmentBookingEntry the appointmentBookingEntry
	 * @param externalAppointmentId the externalAppointmentId
	 * @return the created appointment entry
	 */
	@Override
	public AppointmentEntry addAppointmentEntry(long companyId, String serviceId, String connectorClassName, AppointmentBookingEntry appointmentBookingEntry, String externalAppointmentId) {
		long appointmentEntryId = counterLocalService.increment(AppointmentEntry.class.getName(), 1);
		AppointmentEntry appointmentEntry = appointmentEntryPersistence.create(appointmentEntryId);
		appointmentEntry.setCompanyId(companyId);

		appointmentEntry.setServiceId(serviceId);
		appointmentEntry.setConnectorClassName(connectorClassName);
		appointmentEntry.setAppointmentId(externalAppointmentId);

		appointmentEntry.setCreateDate(DateUtil.newDate());
		appointmentEntry.setAppointmentEndDate(appointmentBookingEntry.getEndDate());
		appointmentEntry.setAppointmentStartDate(appointmentBookingEntry.getStartDate());
		appointmentEntry.setEntryClassPK(appointmentBookingEntry.getClassPK());
		appointmentEntry.setEntryClassNameId(appointmentBookingEntry.getClassNameId());

		appointmentEntry = appointmentEntryLocalService.addAppointmentEntry(appointmentEntry);

		LOG.info("AppointmentEntry added - appointmentEntryId: " + appointmentEntryId + ", serviceId: " + serviceId + ", classPK: " + appointmentBookingEntry.getClassPK() + ", classNameId: "
				+ appointmentBookingEntry.getClassNameId() + ", externalAppointmentId: " + externalAppointmentId);

		return appointmentEntry;
	}

	/**
	 * Returns a random generated appointmentId, unique for the
	 * companyId-serviceId-connectorClassName values
	 *
	 * @param companyId the companyId
	 * @param serviceId the serviceId
	 * @param connectorClassName the connector classname
	 * @return the auto-generated id
	 */
	@Override
	public String generateExternalAppointmentId(long companyId, String serviceId, String connectorClassName) {
		String result = StringUtil.randomString(1).concat(String.valueOf(Instant.now().getEpochSecond()));
		boolean isUnique = appointmentEntryPersistence.countByCompanyId_ServiceId_ConnectorClassName_AppointmentId(companyId, serviceId, connectorClassName, result) <= 0;
		while (!isUnique) {
			result = StringUtil.randomString(1).concat(String.valueOf(Instant.now().getEpochSecond()));
			if (appointmentEntryPersistence.countByCompanyId_ServiceId_ConnectorClassName_AppointmentId(companyId, serviceId, connectorClassName, result) <= 0) {
				isUnique = true;
			}
		}
		return result;
	}

	public List<AppointmentEntry> getAppointmentEntries(long companyId, long classPK, long classNameId) {
		return appointmentEntryPersistence.findByCompanyId_EntryClassPK_EntryClassNameId(companyId, classPK, classNameId);
	}

	@Override
	public void removeAppointmentEntry(long companyId, String serviceId, String connectorClassName, long classPK, long classNameId, String appointmentId) {
		List<AppointmentEntry> appointmentEntries = appointmentEntryPersistence.findByCompanyId_EntryClassPK_EntryClassNameId(companyId, classPK, classNameId);
		appointmentEntries.stream().forEach(appointmentEntry -> {
			if (appointmentUtils.doesAppointmentMatch(serviceId, connectorClassName, appointmentId, appointmentEntry)) {
				appointmentEntryLocalService.deleteAppointmentEntry(appointmentEntry);
			}
		});
	}

}