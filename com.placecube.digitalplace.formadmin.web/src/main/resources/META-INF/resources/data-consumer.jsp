<%@ include file="init.jsp" %>

<div class="container-fluid container-fluid-max-xl">

	<liferay-ui:search-container deltaConfigurable="${ formSearchContainer.isDeltaConfigurable() }"
		searchContainer="${ formSearchContainer }" total="${ formSearchContainer.getTotal() }"
		emptyResultsMessage="${ formSearchContainer.getEmptyResultsMessage() }"
		emptyResultsMessageCssClass="${ formSearchContainer.getEmptyResultsMessageCssClass() }">

		<liferay-ui:search-container-row className="com.liferay.dynamic.data.mapping.model.DDMFormInstance"
			modelVar="formDefinitionResult">

			<liferay-ui:search-container-column-text name="name" value="${ formDefinitionResult.getName(locale) }" />
			<liferay-ui:search-container-column-text name="description" value="${ formDefinitionResult.getDescription(locale) }" />

			<c:set var="isConfigured" value="" />
			<c:forEach items="${ dataConsumers }" var="consumer">

				<c:if
					test="${ formDefinitionResult.getFormInstanceId() == consumer.getDdmFormInstanceId() && consumer.getChain() == 0 }">
					
					<portlet:renderURL var="editUrl">
						<portlet:param name="mvcRenderCommandName" value="/select-data-provider" />
						<portlet:param name="currentTab" value="data-consumers" />
						<portlet:param name="formInstanceId" value="${ formDefinitionResult.getFormInstanceId() }" />
						<portlet:param name="dataProviderInstanceId"
							value="${ consumer.getDdmDataProviderInstanceId() }" />
					</portlet:renderURL>

					<c:set var="isConfigured" value="true" />

				</c:if>

			</c:forEach>

			<c:if test="${ empty isConfigured }">
				<portlet:renderURL var="createUrl">
					<portlet:param name="mvcRenderCommandName" value="/select-data-provider" />
					<portlet:param name="currentTab" value="data-consumers" />
					<portlet:param name="formInstanceId" value="${ formDefinitionResult.getFormInstanceId() }" />
				</portlet:renderURL>
			</c:if>

			<liferay-ui:search-container-column-text colspan="3" >
				<portlet:resourceURL id="<%=MVCCommandKeys.EXPORT_FORM%>" var="exportFormUrl">
					<portlet:param name="formInstanceId" value="${ formDefinitionResult.getFormInstanceId() }" />
				</portlet:resourceURL>

				<liferay-ui:icon-menu
					direction="left-side"
					markupView="lexicon"
					showWhenSingleIcon="true"
				>

					<liferay-ui:icon
						message="export-form-definition"
						url="${exportFormUrl}"
					/>

					<liferay-ui:icon
						message="configure-data-consumer"
						url="${isConfigured eq 'true' ? editUrl : createUrl }"
					/>

				</liferay-ui:icon-menu>

			</liferay-ui:search-container-column-text>

		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator paginate="true" displayStyle="list" markupView="lexicon"
			searchContainer="${ formSearchContainer }" />

	</liferay-ui:search-container>
</div>