package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = DDLLocationSearchGoogleAPIKeyHelper.class)
public class DDLLocationSearchGoogleAPIKeyHelper {

	private static final String GOOGLE_MAPS_API_KEY = "googleMapsAPIKey";

	public String getGoogleMapsAPIKey(long companyId, Group group) {
		String googleMapsAPIKey = GetterUtil.getString(group.getTypeSettingsProperty(GOOGLE_MAPS_API_KEY), null);

		if (Validator.isNull(googleMapsAPIKey)) {
			googleMapsAPIKey = getGoogleMapsAPIKey(companyId);
		}

		return googleMapsAPIKey;
	}

	private String getGoogleMapsAPIKey(long companyId) {

		PortletPreferences companyPortletPreferences = PrefsPropsUtil.getPreferences(companyId);
		return companyPortletPreferences.getValue(GOOGLE_MAPS_API_KEY, null);
	}
}
