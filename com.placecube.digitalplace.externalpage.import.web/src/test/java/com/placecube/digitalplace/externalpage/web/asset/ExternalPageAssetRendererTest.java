package com.placecube.digitalplace.externalpage.web.asset;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.Locale;

import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HtmlUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.asset.kernel.model.BaseAssetRenderer" })
public class ExternalPageAssetRendererTest extends PowerMockito {

	private ExternalPageAssetRenderer externalPageAssetRenderer;

	@Mock
	private ExternalPage mockExternalPage;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private LiferayPortletResponse mockLiferayPortletResponse;

	@Mock
	private LiferayPortletRequest mockLiferayPortletRequest;

	@Mock
	private AssetRendererFactory<ExternalPage> mockAssetRendererFactory;

	@Before
	public void setUp() {
		mockStatic(HtmlUtil.class);

		externalPageAssetRenderer = new ExternalPageAssetRenderer(mockExternalPage);
	}

	@Test
	public void getAssetObject_WhenNoError_ThenReturnsTheExternalPage() {
		ExternalPage result = externalPageAssetRenderer.getAssetObject();

		assertThat(result, sameInstance(mockExternalPage));
	}

	@Test
	public void getClassName_WhenNoError_ThenReturnsTheClassName() {
		String result = externalPageAssetRenderer.getClassName();

		assertThat(result, equalTo(ExternalPage.class.getName()));
	}

	@Test
	public void getClassPK_WhenNoError_ThenReturnsTheExternalPageId() {
		long expected = 123l;
		when(mockExternalPage.getExternalPageId()).thenReturn(expected);

		long result = externalPageAssetRenderer.getClassPK();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getGroupId_WhenNoError_ThenReturnsTheExternalPageGroupId() {
		long expected = 123l;
		when(mockExternalPage.getGroupId()).thenReturn(expected);

		long result = externalPageAssetRenderer.getGroupId();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getJspPath_ThenReturnsNull() {
		String result = externalPageAssetRenderer.getJspPath(mockHttpServletRequest, "template");

		assertNull(result);
	}

	@Test
	public void getPortletId_WhenNoError_ThenReturnsThePortletId() {
		String expected = "expectedValue";
		externalPageAssetRenderer = spy(new ExternalPageAssetRenderer(mockExternalPage));
		doReturn(mockAssetRendererFactory).when(externalPageAssetRenderer).getAssetRendererFactory();
		when(mockAssetRendererFactory.getPortletId()).thenReturn(expected);

		String result = externalPageAssetRenderer.getPortletId();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSummary_WhenNoError_ThenReturnsTheExternalPageTruncatedContent() {
		String htmlContent = "htmlContent";
		String content = "myContentValue".concat(StringUtil.randomString(300));
		when(mockExternalPage.getContent()).thenReturn(htmlContent);
		when(HtmlUtil.escape(htmlContent)).thenReturn(content);

		String result = externalPageAssetRenderer.getSummary();

		assertThat(result, equalTo(StringUtil.shorten(content, 200)));
	}

	@Test
	public void getTitle_WhenNoError_ThenReturnsTheExternalPageTitle() {
		String expected = "expectedValue";
		when(mockExternalPage.getTitle()).thenReturn(expected);

		String result = externalPageAssetRenderer.getTitle(Locale.CANADA_FRENCH);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getType_WhenNoError_ThenReturnsTheType() {
		String expected = "expectedValue";
		externalPageAssetRenderer = spy(new ExternalPageAssetRenderer(mockExternalPage));
		doReturn(mockAssetRendererFactory).when(externalPageAssetRenderer).getAssetRendererFactory();
		when(mockAssetRendererFactory.getType()).thenReturn(expected);

		String result = externalPageAssetRenderer.getType();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getUrlTitle_WhenNoError_ThenReturnsTheExternalPageURL() {
		String expected = "expectedValue";
		when(mockExternalPage.getUrl()).thenReturn(expected);

		String result = externalPageAssetRenderer.getUrlTitle();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getURLView_WhenNoError_ThenReturnsTheExternalPageURL() {
		String expected = "expectedValue";
		when(mockExternalPage.getUrl()).thenReturn(expected);

		String result = externalPageAssetRenderer.getURLView(mockLiferayPortletResponse, WindowState.MAXIMIZED);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getURLViewInContext_WhenNoError_ThenReturnsTheExternalPageURL() {
		String expected = "expectedValue";
		when(mockExternalPage.getUrl()).thenReturn(expected);

		String result = externalPageAssetRenderer.getURLViewInContext(mockLiferayPortletRequest, mockLiferayPortletResponse, "redirect");

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getUserId_WhenNoError_ThenReturnsTheExternalPageUserId() {
		long expected = 123l;
		when(mockExternalPage.getUserId()).thenReturn(expected);

		long result = externalPageAssetRenderer.getUserId();

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getUserName_WhenNoError_ThenReturnsABlankString() {
		String result = externalPageAssetRenderer.getUserName();

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getUuid_WhenNoError_ThenReturnsTheExternalPageUuid() {
		String expected = "expectedValue";
		when(mockExternalPage.getUuid()).thenReturn(expected);

		String result = externalPageAssetRenderer.getUuid();

		assertThat(result, equalTo(expected));
	}

}
