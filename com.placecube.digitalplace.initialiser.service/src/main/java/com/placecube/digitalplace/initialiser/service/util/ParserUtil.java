package com.placecube.digitalplace.initialiser.service.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.initialiser.service.constants.ConfigurationKeys;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.ModelCreationFactory;

@Component(immediate = true, service = ParserUtil.class)
public class ParserUtil {

	@Reference
	private AssetListEntryInitializer assetListEntryInitializer;

	@Reference
	private ModelCreationFactory modelCreationFactory;

	public void configureMultivaluedPreferences(PortletContext portletContext, JSONObject portletJSONObject, ConfigurationKeys configurationKeys) {
		JSONArray jsonArray = portletJSONObject.getJSONArray(configurationKeys.getMultivaluedPreferencesKey());
		if (Validator.isNotNull(jsonArray)) {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject portletPref = jsonArray.getJSONObject(i);
				addMultivaluedPref(portletContext, portletPref);
			}
		}
	}

	public void configurePortletPreferences(PortletContext portletContext, Map<String, String> portletPreferences, Map<String, String> contentSetPlaceholders) {
		if (portletPreferences.containsKey("assetListEntryId")) {
			portletPreferences.put("assetListEntryId", contentSetPlaceholders.get(portletPreferences.get("assetListEntryId")));
		}

		configurePortletPreferences(portletContext, portletPreferences);
	}

	public void configurePortletPreferences(PortletContext portletContext, Map<String, String> portletPreferences) {
		portletPreferences.entrySet().stream().forEach(entry -> portletContext.addPreference(entry.getKey(), entry.getValue()));
	}

	public Map<String, String> getContentSets(long groupId, JSONArray contentSetPlaceholdersJSONArray) {
		Map<String, String> results = new HashMap<>();
		if (Validator.isNotNull(contentSetPlaceholdersJSONArray)) {
			for (int i = 0; i < contentSetPlaceholdersJSONArray.length(); i++) {
				JSONObject contentSetJSONObject = contentSetPlaceholdersJSONArray.getJSONObject(i);
				Optional<AssetListEntry> contentSet = assetListEntryInitializer.getContentSet(groupId, contentSetJSONObject.getString("value"));
				results.put(contentSetJSONObject.getString("name"), String.valueOf(contentSet.get().getAssetListEntryId()));
			}
		}
		return results;
	}

	public Map<String, Serializable> getExpandoValues(JSONObject pageJSONObject) {
		Map<String, Serializable> results = new HashMap<>();
		JSONArray expandoFields = pageJSONObject.getJSONArray("expandoFields");
		if (Validator.isNotNull(expandoFields)) {
			for (int i = 0; i < expandoFields.length(); i++) {
				JSONObject expandoField = expandoFields.getJSONObject(i);
				String name = expandoField.getString("name");
				String value = expandoField.getString("value");
				int type = expandoField.getInt("type");

				switch (type) {

				case ExpandoColumnConstants.STRING_ARRAY:
					results.put(name, StringUtil.split(value, StringPool.SEMICOLON));
					break;

				default:
					results.put(name, value);
					break;
				}

			}
		}

		return results;
	}

	public FragmentContext getFragment(JSONObject entryJsonObject) {
		return modelCreationFactory.createFragmentContext(entryJsonObject.getString("id"), entryJsonObject.getString("placeholder"), entryJsonObject.getString("preferences"));
	}

	public Map<String, String> getNameValueMap(JSONArray jsonArray, Map<String, String> prefsPlaceholderValues) {
		Map<String, String> results = new HashMap<>();
		if (Validator.isNotNull(jsonArray)) {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject portletPref = jsonArray.getJSONObject(i);
				String name = portletPref.getString("name");
				String value = portletPref.getString("value");
				String prefValue = prefsPlaceholderValues.containsKey(value) ? prefsPlaceholderValues.get(value) : value;
				results.put(name, prefValue);
			}
		}
		return results;
	}

	public Map<String, String[]> getPagePermissions(JSONObject pageJSONObject, String permissionsKey) {
		Map<String, String[]> rolePermissions = new HashMap<>();
		JSONArray pagePermissions = pageJSONObject.getJSONArray(permissionsKey);
		if (Validator.isNotNull(pagePermissions)) {
			for (int i = 0; i < pagePermissions.length(); i++) {
				JSONObject permissionForRole = pagePermissions.getJSONObject(i);
				String roleName = permissionForRole.getString("roleName");
				String actionIds = permissionForRole.getString("actionIds");
				rolePermissions.put(roleName, StringUtil.split(actionIds, StringPool.COMMA));
			}
		}

		return rolePermissions;
	}

	public Map<String, String> getPortletPreferencesWithPlaceholders(JSONObject portletObject, Map<String, String> prefsPlaceholderValues, ConfigurationKeys configurationKeys) {
		JSONArray jsonArray = portletObject.getJSONArray(configurationKeys.getPreferencesKey());

		return getNameValueMap(jsonArray, prefsPlaceholderValues);
	}

	public PortletContext getPortletWithPlaceholder(JSONObject portletJSONObject, ConfigurationKeys configurationKeys) {
		return modelCreationFactory.createPortletContext(portletJSONObject.getString(configurationKeys.getPortletIdKey()), portletJSONObject.getString("placeholder"),
				portletJSONObject.getString("customInstanceId", StringPool.BLANK));
	}

	public PortletContext getPortletWithPreferences(JSONObject portletJSONObject, Map<String, String> prefs, ConfigurationKeys configurationKeys) {
		return modelCreationFactory.createPortletContext(portletJSONObject.getString(configurationKeys.getPortletIdKey()), prefs, portletJSONObject.getString("customInstanceId", StringPool.BLANK));
	}

	private void addMultivaluedPref(PortletContext portletContext, JSONObject portletPref) {
		String name = portletPref.getString("name");
		JSONArray prefValuesJSONArray = portletPref.getJSONArray("values");
		if (Validator.isNotNull(prefValuesJSONArray)) {
			String[] values = new String[0];
			for (int i = 0; i < prefValuesJSONArray.length(); i++) {
				values = ArrayUtil.append(values, prefValuesJSONArray.getJSONObject(i).getString("value"));
			}
			portletContext.addMultivaluedPreference(name, values);
		}
	}
}
