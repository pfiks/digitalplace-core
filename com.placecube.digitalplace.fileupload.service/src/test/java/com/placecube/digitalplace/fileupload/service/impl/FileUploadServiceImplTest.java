package com.placecube.digitalplace.fileupload.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.fileupload.exception.FileUploadException;
import com.placecube.digitalplace.fileupload.service.FileUploadConnector;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class FileUploadServiceImplTest extends PowerMockito {

	private static final Long COMPANY_ID = 11l;
	private static final Optional<String> CONNECTOR_ID = Optional.ofNullable("CONNECTOR_ID");

	private static final String NAME = "NAME";
	private static final String PATH = "PATH";

	@InjectMocks
	private FileUploadServiceImpl fileUploadServiceImpl;

	@Mock
	private FileUploadConnector mockFileUploadConnector1;

	@Mock
	private FileUploadConnector mockFileUploadConnector2;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		initMocks(this);
		fileUploadServiceImpl = new FileUploadServiceImpl();
	}

	@Test
	public void uploadFile_WhenNameAndOrPathIsNotNull() throws FileUploadException {
		addFileUploadConnector(COMPANY_ID, CONNECTOR_ID, mockFileUploadConnector1, true, true);
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		doNothing().when(mockFileUploadConnector1).uploadFile(NAME, PATH, mockInputStream, mockServiceContext);

		fileUploadServiceImpl.uploadFile(NAME, PATH, mockInputStream, CONNECTOR_ID, mockServiceContext);

		Mockito.verify(mockFileUploadConnector1, Mockito.times(1)).uploadFile(NAME, PATH, mockInputStream, mockServiceContext);
	}

	@Test(expected = FileUploadException.class)
	@Parameters(method = "parametersToTestUploadFile")
	public void uploadFile_WhenNameAndOrPathIsNull_ThenThrowFileUploadException(String name, String path) throws FileUploadException {
		fileUploadServiceImpl.uploadFile(name, path, null, null, null);
	}

	@SuppressWarnings("unused")
	private static final Object[] parametersToTestUploadFile() {
		return new Object[] { new Object[] { null, null }, new Object[] { NAME, null }, new Object[] { null, PATH } };
	}

	@Test
	public void getFileUploadConnector_twoConnectorsAvailable_returnEnabledOne() throws Exception {
		addFileUploadConnector(COMPANY_ID, CONNECTOR_ID, mockFileUploadConnector1, false, true);
		addFileUploadConnector(COMPANY_ID, CONNECTOR_ID, mockFileUploadConnector2, true, true);

		FileUploadConnector result = fileUploadServiceImpl.getFileUploadConnector(COMPANY_ID, CONNECTOR_ID);

		assertEquals(mockFileUploadConnector2, result);
	}

	@Test
	public void getFileUploadConnector_twoEnabledConnectors_returnFirstOne() throws Exception {
		addFileUploadConnector(COMPANY_ID, CONNECTOR_ID, mockFileUploadConnector1, true, true);
		addFileUploadConnector(COMPANY_ID, CONNECTOR_ID, mockFileUploadConnector2, true, true);

		FileUploadConnector result = fileUploadServiceImpl.getFileUploadConnector(COMPANY_ID, CONNECTOR_ID);

		assertEquals(mockFileUploadConnector1, result);
	}

	@Test(expected = FileUploadException.class)
	public void getFileUploadConnectorConnector_noEnabledConnectorsForCompany_Exception() throws Exception {
		addFileUploadConnector(COMPANY_ID, CONNECTOR_ID, mockFileUploadConnector1, false, true);
		addFileUploadConnector(1l, CONNECTOR_ID, mockFileUploadConnector2, true, true);

		fileUploadServiceImpl.getFileUploadConnector(COMPANY_ID, CONNECTOR_ID);
	}

	private void addFileUploadConnector(long companyId, Optional<String> connectorId, FileUploadConnector fileUploadConnector, boolean enabled, boolean isDefault) {
		when(fileUploadConnector.isEnabled(companyId)).thenReturn(enabled);
		when(fileUploadConnector.isDefault(companyId)).thenReturn(enabled);
		if (Validator.isNotNull(connectorId) && connectorId.isPresent()) {
			when(fileUploadConnector.getId()).thenReturn(connectorId.get());
		}
		fileUploadServiceImpl.setFileUploadConnector(fileUploadConnector);
	}

}
