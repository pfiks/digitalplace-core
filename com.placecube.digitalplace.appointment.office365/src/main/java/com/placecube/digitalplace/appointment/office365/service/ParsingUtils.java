package com.placecube.digitalplace.appointment.office365.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.TimeZoneUtil;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.office365.model.AppointmentConfiguredService;

@Component(immediate = true, service = ParsingUtils.class)
public class ParsingUtils {

	private static final Log LOG = LogFactoryUtil.getLog(ParsingUtils.class);

	@Reference
	private JSONFactory jsonFactory;

	public String getAccessTokenFromResponse(String responseString) throws JSONException {
		JSONObject responseJson = jsonFactory.createJSONObject(responseString);

		return responseJson.getString("access_token");
	}

	public AppointmentConfiguredService getAppointmentConfiguredServiceFromJsonString(String configuredService) {
		try {
			JSONObject json = jsonFactory.createJSONObject(configuredService);
			return new AppointmentConfiguredService(json);
		} catch (JSONException e) {
			LOG.warn("Invalid configuration for: " + configuredService + " - " + e.getMessage());
			return null;
		}
	}

	public AppointmentBookingEntry getAppointmentEntry(JSONObject eventJson) {
		AppointmentBookingEntry appointmentEntry = new AppointmentBookingEntry();
		appointmentEntry.setAppointmentId(eventJson.getString("id"));
		appointmentEntry.setTitle(eventJson.getString("subject"));
		appointmentEntry.setDescription(eventJson.getString("bodyPreview"));

		String timeZone = eventJson.getJSONObject("start").getString("timeZone");
		appointmentEntry.setTimeZone(TimeZoneUtil.getTimeZone(timeZone));

		appointmentEntry.setStartDate(getDate(eventJson, timeZone, "start"));
		appointmentEntry.setEndDate(getDate(eventJson, timeZone, "end"));

		return appointmentEntry;
	}

	public AppointmentBookingEntry getAppointmentEntryFromResponse(String event) {
		try {
			JSONObject eventJson = jsonFactory.createJSONObject(event);
			return getAppointmentEntry(eventJson);
		} catch (JSONException e) {
			LOG.warn("Unable to retrieve event from: " + event + " - " + e.getMessage());
			return null;
		}
	}

	public String getAppointmentIdFromResponse(String responseString) throws JSONException {
		JSONObject responseJson = jsonFactory.createJSONObject(responseString);
		return responseJson.getString("id");
	}

	public List<Date> getDatesFromLocalDateTimes(List<LocalDateTime> dates) {
		List<Date> results = new ArrayList<>();
		ZoneId systemDefault = ZoneId.systemDefault();
		for (LocalDateTime availableDateToMatch : dates) {
			results.add(Date.from(availableDateToMatch.atZone(systemDefault).toInstant()));
		}
		Collections.sort(results);
		return results;
	}

	public List<LocalDateTime> getDatesWithDayOfWeekTruncatedToDays(LocalDate localDate, int numberOfDatesToReturn) {
		List<LocalDateTime> results = new ArrayList<>();
		LocalDateTime startDate = localDate.atStartOfDay().truncatedTo(ChronoUnit.DAYS);
		results.add(startDate);
		while (results.size() < numberOfDatesToReturn) {
			startDate = startDate.plusWeeks(1);
			results.add(startDate);
		}
		return results;
	}

	public JSONArray getEmptyJSONArray() {
		return jsonFactory.createJSONArray();
	}

	public String getFormattedDateTime(Date date, TimeZone timeZone) {
		ZonedDateTime ofInstant = ZonedDateTime.ofInstant(date.toInstant(), ZoneId.of(timeZone.getID()));
		return ofInstant.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
	}

	public String getFormattedDateTimeAtEndOfDay(LocalDate localDate) {
		return localDate.atTime(LocalTime.MAX).format(DateTimeFormatter.ISO_DATE_TIME);
	}

	public String getFormattedDateTimeAtStartOfDay(LocalDate localDate) {
		return localDate.atTime(LocalTime.MIN).format(DateTimeFormatter.ISO_DATE_TIME);
	}

	public String getJsonStringFromAppointmentEntry(AppointmentBookingEntry appointmentEntry) {
		TimeZone timeZone = appointmentEntry.getTimeZone();
		String id = timeZone.getID();

		JSONObject eventJson = jsonFactory.createJSONObject();
		eventJson.put("transactionId", appointmentEntry.getAppointmentId());
		eventJson.put("allowNewTimeProposals", false);

		eventJson.put("subject", appointmentEntry.getTitle());

		JSONObject bodyJson = jsonFactory.createJSONObject();
		bodyJson.put("contentType", "HTML");
		bodyJson.put("content", appointmentEntry.getDescription());
		eventJson.put("body", bodyJson);

		JSONObject startJson = jsonFactory.createJSONObject();
		startJson.put("dateTime", getFormattedDateTime(appointmentEntry.getStartDate(), timeZone));
		startJson.put("timeZone", id);
		eventJson.put("start", startJson);

		JSONObject endJson = jsonFactory.createJSONObject();
		endJson.put("dateTime", getFormattedDateTime(appointmentEntry.getEndDate(), timeZone));
		endJson.put("timeZone", id);
		eventJson.put("end", endJson);

		return eventJson.toJSONString();
	}

	public LocalDateTime getStartDateFromAppointmentEntryTruncatedToDays(JSONObject eventJson) {
		JSONObject dateObj = eventJson.getJSONObject("start");
		String dateTime = dateObj.getString("dateTime");
		LocalDateTime localDateTime = LocalDateTime.parse(dateTime);
		return localDateTime.truncatedTo(ChronoUnit.DAYS);
	}

	public JSONArray getValuesJsonArrayFromResponse(String responseString) throws JSONException {
		JSONObject responseJson = jsonFactory.createJSONObject(responseString);
		return responseJson.getJSONArray("value");
	}

	private Date getDate(JSONObject eventJson, String timeZone, String jsonObjectName) {
		JSONObject dateObj = eventJson.getJSONObject(jsonObjectName);
		String dateTime = dateObj.getString("dateTime");
		LocalDateTime localDateTime = LocalDateTime.parse(dateTime);
		return Date.from(localDateTime.atZone(ZoneId.of(timeZone)).toInstant());
	}
}
