package com.placecube.digitalplace.search.internal.util;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;

@Component(immediate = true, service = RequestUtil.class)
public class RequestUtil {

	@Reference
	private Portal portal;

	public SharedSearchSession getOrCreateSharedSearchSession(HttpServletRequest httpServletRequest, ThemeDisplay themeDisplay) {
		long plid = themeDisplay.getPlid();
		String name = getSharedSearchSessionAttributeName(plid);

		HttpSession session = httpServletRequest.getSession();

		SharedSearchSession sharedSearchSession = (SharedSearchSession) session.getAttribute(name);

		if (Validator.isNull(sharedSearchSession)) {
			sharedSearchSession = SharedSearchSession.init(plid);
			session.setAttribute(name, sharedSearchSession);
		}

		return sharedSearchSession;
	}

	public HttpServletRequest getOriginalHttpServletRequest(PortletRequest portletRequest) {
		HttpServletRequest httpServletRequest = portal.getHttpServletRequest(portletRequest);
		return portal.getOriginalServletRequest(httpServletRequest);
	}

	public ThemeDisplay getThemeDisplay(PortletRequest portletRequest) {
		return (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
	}

	public void removeSharedSearchSession(HttpServletRequest httpServletRequest, long plid) {
		HttpSession httpSession = httpServletRequest.getSession();
		String name = getSharedSearchSessionAttributeName(plid);
		httpSession.removeAttribute(name);
	}

	public void updateSharedSearchSession(HttpServletRequest httpServletRequest, SharedSearchSession sharedSearchSession) {
		HttpSession httpSession = httpServletRequest.getSession();
		String name = getSharedSearchSessionAttributeName(sharedSearchSession.getPlid());
		httpSession.removeAttribute(name);
		httpSession.setAttribute(name, sharedSearchSession);
	}

	private String getSharedSearchSessionAttributeName(long plid) {
		return "LIFERAY_SHARED_DIGITALPLACE_SHARED_SEARCH_REQUEST_" + plid;
	}

}
