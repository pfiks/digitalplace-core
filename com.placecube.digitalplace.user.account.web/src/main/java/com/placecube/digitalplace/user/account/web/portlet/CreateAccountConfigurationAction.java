package com.placecube.digitalplace.user.account.web.portlet;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.JavaConstants;
import com.placecube.digitalplace.user.account.configuration.AccountPortletInstanceConfiguration;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@Component(configurationPid = "com.placecube.digitalplace.user.account.configuration.AccountPortletInstanceConfiguration", immediate = true, property = "javax.portlet.name="
		+ AccountPortletKeys.CREATE_ACCOUNT, service = ConfigurationAction.class)
public class CreateAccountConfigurationAction extends DefaultConfigurationAction {

	private AccountPortletInstanceConfiguration accountPortletInstanceConfiguration;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		PortletRequest portletRequest = (PortletRequest) httpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST);
		PortletPreferences preferences = portletRequest.getPreferences();

		httpServletRequest.setAttribute(RequestKeys.EMAIL_ADDRESS_REQUIRED,
				preferences.getValue(RequestKeys.EMAIL_ADDRESS_REQUIRED, String.valueOf(accountPortletInstanceConfiguration.emailAddressRequired())));
		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String emailAddressRequired = getParameter(actionRequest, RequestKeys.EMAIL_ADDRESS_REQUIRED);
		setPreference(actionRequest, RequestKeys.EMAIL_ADDRESS_REQUIRED, emailAddressRequired);
		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		accountPortletInstanceConfiguration = ConfigurableUtil.createConfigurable(AccountPortletInstanceConfiguration.class, properties);

		if (accountPortletInstanceConfiguration == null) {
			throw new IllegalStateException("Account portlet instance configuration missing");
		}

	}
}