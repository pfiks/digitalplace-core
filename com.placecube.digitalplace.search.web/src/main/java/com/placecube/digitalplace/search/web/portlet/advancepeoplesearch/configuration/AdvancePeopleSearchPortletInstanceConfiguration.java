package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration;

import com.liferay.portal.kernel.settings.LocalizedValuesMap;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.AdvancePeopleSearchPortletInstanceConfiguration")
public interface AdvancePeopleSearchPortletInstanceConfiguration {

	@Meta.AD(required = false)
	boolean applyPrivacy();

	@Meta.AD(required = false)
	String filterColour();

	@Meta.AD(required = false)
	String searchFields();

	@Meta.AD(required = false)
	LocalizedValuesMap title();

}
