package com.placecube.digitalplace.group.rest.application.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;

import org.apache.commons.io.IOUtils;
import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.group.rest.application.constants.RequestHeaders;

@Component(immediate = true, service = RequestFiltersService.class)
public class RequestFiltersService {

	public String getAcceptHeader(ContainerRequestContext requestContext) {
		return GetterUtil.getString(requestContext.getHeaderString(RequestHeaders.ACCEPT));
	}

	public String getApiKeyHeader(ContainerRequestContext requestContext) {
		return GetterUtil.getString(requestContext.getHeaderString(RequestHeaders.X_API_KEY));
	}

	public String getBody(ContainerRequestContext requestContext) throws IOException {
		InputStream entityStream = requestContext.getEntityStream();
		String body = IOUtils.toString(entityStream, StandardCharsets.UTF_8);
		// Replace input stream for Jersey as we've already read it
		InputStream inputStream = IOUtils.toInputStream(body, StandardCharsets.UTF_8);
		requestContext.setEntityStream(inputStream);
		return GetterUtil.getString(body);
	}

	public String getContextPath(HttpServletRequest httpServletRequest) {
		return GetterUtil.getString(httpServletRequest.getContextPath());
	}

	public String getDateHeader(ContainerRequestContext requestContext) {
		return GetterUtil.getString(requestContext.getHeaderString(RequestHeaders.X_DATE));
	}

	public String getMethod(ContainerRequestContext requestContext) {
		return GetterUtil.getString(requestContext.getMethod());
	}

	public String getPath(ContainerRequestContext requestContext) {
		String path = GetterUtil.getString(requestContext.getUriInfo().getPath());
		if (Validator.isNotNull(path) && !path.startsWith(StringPool.FORWARD_SLASH)) {
			return StringPool.FORWARD_SLASH + path;
		}
		return path;
	}

	public String getSignatureHeader(ContainerRequestContext requestContext) {
		return GetterUtil.getString(requestContext.getHeaderString(RequestHeaders.X_SIGNATURE));
	}

}
