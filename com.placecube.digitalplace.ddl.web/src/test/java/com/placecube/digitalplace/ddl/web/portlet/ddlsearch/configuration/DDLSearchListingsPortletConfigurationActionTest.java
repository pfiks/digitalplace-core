package com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class DDLSearchListingsPortletConfigurationActionTest extends PowerMockito {

	@InjectMocks
	private DDLSearchListingsPortletConfigurationAction ddlSearchListingsPortletConfigurationAction;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private DDLConfigurationService mockDDLConfigurationService;

	@Mock
	private DDLSearchListingsPortletInstanceConfiguration mockDDLSearchListingsPortletInstanceConfiguration;

	@Mock
	private List<DDMStructure> mockDDMStructures;

	@Mock
	private Group mockGroup;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheAvailableStructuresAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockDDLConfigurationService.getAvailableDDMStructures(mockGroup)).thenReturn(mockDDMStructures);

		ddlSearchListingsPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("availableStructures", mockDDMStructures);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheConfigurationAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockDDLConfigurationService.getDDLSearchListingsPortletInstanceConfiguration(mockThemeDisplay)).thenReturn(mockDDLSearchListingsPortletInstanceConfiguration);

		ddlSearchListingsPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute("configuration", mockDDLSearchListingsPortletInstanceConfiguration);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesDDMStructureIdAsPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		long ddmStructureId = 34;
		when(ParamUtil.getLong(mockActionRequest, "ddmStructureId", 0)).thenReturn(ddmStructureId);
		ddlSearchListingsPortletConfigurationAction = spy(new DDLSearchListingsPortletConfigurationAction());

		ddlSearchListingsPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(ddlSearchListingsPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "ddmStructureId", String.valueOf(ddmStructureId));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesDefaultDisplayStyleAsPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String defaultDisplayStyle = "list";
		when(ParamUtil.getString(mockActionRequest, "defaultDisplayStyle", StringPool.BLANK)).thenReturn(defaultDisplayStyle);
		ddlSearchListingsPortletConfigurationAction = spy(new DDLSearchListingsPortletConfigurationAction());

		ddlSearchListingsPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(ddlSearchListingsPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "defaultDisplayStyle", defaultDisplayStyle);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesGridDisplayStyleCssAsPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String gridDisplayStyleCss = "list";
		when(ParamUtil.getString(mockActionRequest, "gridDisplayStyleCss", "col-md-4")).thenReturn(gridDisplayStyleCss);
		ddlSearchListingsPortletConfigurationAction = spy(new DDLSearchListingsPortletConfigurationAction());

		ddlSearchListingsPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(ddlSearchListingsPortletConfigurationAction, times(1)).setPreference(mockActionRequest, "gridDisplayStyleCss", gridDisplayStyleCss);
	}

	@Test
	@Parameters({ "true", "false" })
	public void processAction_WhenNoError_ThenSavesHidePaginationAsPreference(boolean expected) throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		when(ParamUtil.getBoolean(mockActionRequest, DDLSearchListingsConfigurationConstants.HIDE_PAGINATION, false)).thenReturn(expected);
		ddlSearchListingsPortletConfigurationAction = spy(new DDLSearchListingsPortletConfigurationAction());

		ddlSearchListingsPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(ddlSearchListingsPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLSearchListingsConfigurationConstants.HIDE_PAGINATION, String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesNumberOfResultsPerPageAsPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		int numResultsPerPage = 21;
		when(ParamUtil.getInteger(mockActionRequest, DDLSearchListingsConfigurationConstants.NUMBER_RESULTS_PER_PAGE, 20)).thenReturn(numResultsPerPage);
		ddlSearchListingsPortletConfigurationAction = spy(new DDLSearchListingsPortletConfigurationAction());

		ddlSearchListingsPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(ddlSearchListingsPortletConfigurationAction, times(1)).setPreference(mockActionRequest, DDLSearchListingsConfigurationConstants.NUMBER_RESULTS_PER_PAGE,
				String.valueOf(numResultsPerPage));
	}

}
