package com.placecube.digitalplace.search.web.portlet.category.constants;

public class JSONKeys {

	public static final String CHECK = "check";

	public static final String CHECKED = "checked";

	public static final String CHILDREN = "children";

	public static final String DEPTH = "depth";

	public static final String EXPANDED = "expanded";

	public static final String FREQUENCY = "frequency";

	public static final String ID = "id";

	public static final String LABEL = "label";

	public static final String LEAF = "leaf";

	public static final String TYPE = "type";

	private JSONKeys() {
	}
}
