package com.placecube.digitalplace.appointment.booking.rest;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.appointment.booking.helper.AppointmentModelBuilder;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.booking.service.AppointmentBookingService;

@Component(immediate = true, property = { //
		JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/appointments", //
		JaxrsWhiteboardConstants.JAX_RS_NAME + "=DPAppointments.Rest", //
		"oauth2.scopechecker.type=none", //
		"auth.verifier.guest.allowed=true", //
		"liferay.access.control.disable=true" //
}, service = Application.class)
public class AppointmentRESTApplication extends Application {

	private static final Log LOG = LogFactoryUtil.getLog(AppointmentRESTApplication.class);

	@Reference
	private AppointmentBookingService appointmentBookingService;

	@Reference
	private AppointmentModelBuilder appointmentModelBuilder;

	@POST
	@Path("/book-appointment/{serviceId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response bookAppointmentSlot(String bodyRequest, @PathParam("serviceId") String serviceId, @Context HttpServletRequest request) {
		try {

			appointmentBookingService.bookAppointment(getCompanyId(request), serviceId, appointmentModelBuilder.getAppointmentBookingEntryFromPayload(request, bodyRequest));

			return Response.status(Status.OK).build();

		} catch (Exception e) {
			LOG.error("Appointment booking failed", e);
			return getErrorResponse(e);
		}
	}

	@DELETE
	@Path("/cancel-appointment/{serviceId}/{appointmentId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancelAppointment(String bodyRequest, @PathParam("serviceId") String serviceId, @PathParam("appointmentId") String appointmentId, @Context HttpServletRequest request) {
		try {
			long companyId = getCompanyId(request);

			List<AppointmentBookingEntry> appointmentBookingEntries = appointmentBookingService.getAppointments(companyId, serviceId, appointmentId);
			for (AppointmentBookingEntry appointmentBookingEntry : appointmentBookingEntries) {
				appointmentBookingService.cancelAppointment(companyId, serviceId, appointmentBookingEntry);
			}

			return Response.status(Status.OK).build();

		} catch (Exception e) {
			LOG.error("Cancel appointment failed", e);
			return getErrorResponse(e);
		}
	}

	@POST
	@Path("/get-appointments/{serviceId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAppointments(String bodyRequest, @PathParam("serviceId") String serviceId, @Context HttpServletRequest request) {
		try {
			List<AppointmentBookingEntry> bookedAppointments = appointmentBookingService.getAppointments(getCompanyId(request), serviceId,
					appointmentModelBuilder.getAppointmentContextFromPayload(request, bodyRequest));

			Response.ResponseBuilder responseBuilder = Response.status(Status.OK);
			String responseJSONString = JSONFactoryUtil.looseSerialize(bookedAppointments);
			responseBuilder.entity(responseJSONString);
			return responseBuilder.build();

		} catch (Exception e) {
			LOG.error("Get booked appointments failed", e);
			return getErrorResponse(e);
		}
	}

	@POST
	@Path("/get-appointment-slots/{serviceId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAppointmentSlots(String bodyRequest, @PathParam("serviceId") String serviceId, @Context HttpServletRequest request) {
		try {
			List<AppointmentBookingSlot> availableAppointmentSlots = appointmentBookingService.getAppointmentSlots(getCompanyId(request), serviceId,
					appointmentModelBuilder.getAppointmentContextFromPayload(request, bodyRequest));

			Response.ResponseBuilder responseBuilder = Response.status(Status.OK);

			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			String jsonString = mapper.writeValueAsString(availableAppointmentSlots);

			LOG.debug("getAppointmentSlots - serviceId: " + serviceId + ", results: " + jsonString);

			responseBuilder.entity(jsonString);
			return responseBuilder.build();
		} catch (Exception e) {
			LOG.error("Get appointment slots failed", e);
			return getErrorResponse(e);
		}
	}

	@Override
	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}

	private long getCompanyId(HttpServletRequest request) {
		return GetterUtil.getLong(request.getAttribute("COMPANY_ID"), 0l);
	}

	private Response getErrorResponse(Exception exception) {
		Response.ResponseBuilder responseBuilder = Response.status(Status.INTERNAL_SERVER_ERROR);
		responseBuilder.entity(exception.getMessage());
		return responseBuilder.build();
	}
}
