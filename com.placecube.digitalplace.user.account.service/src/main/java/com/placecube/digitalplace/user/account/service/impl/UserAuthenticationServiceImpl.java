package com.placecube.digitalplace.user.account.service.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletRequestModel;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.service.UserAuthenticationService;
import com.placecube.digitalplace.user.account.service.util.UserAuthenticationServiceUtil;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UserAuthenticationService.class)
public class UserAuthenticationServiceImpl implements UserAuthenticationService {

	private static final Log LOG = LogFactoryUtil.getLog(UserAuthenticationService.class);

	@Reference
	private UserAuthenticationServiceUtil userAuthenticationServiceUtil;

	@Override
	public String getUserAccountPasswordPolicyHtml(PortletRequest portletRequest) throws PortletException {

		PortletRequestModel portletRequestModel = userAuthenticationServiceUtil.getPortletRequestModel(portletRequest);

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		return getPasswordPolicyHtml(themeDisplay, portletRequestModel);
	}

	@Override
	public String getUserAccountPasswordPolicyHtml(HttpServletRequest httpServletRequest) throws PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		return getPasswordPolicyHtml(themeDisplay, null);

	}

	private String getPasswordPolicyHtml(ThemeDisplay themeDisplay, PortletRequestModel portletRequestModel) throws PortletException {
		String passwordPolicyHtml = StringPool.BLANK;

		try {
			passwordPolicyHtml = userAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(portletRequestModel, themeDisplay, themeDisplay.getLanguageId());

		} catch (PortalException e) {
			LOG.error(e.toString(), e);
		}

		if (Validator.isNull(passwordPolicyHtml)) {
			passwordPolicyHtml = userAuthenticationServiceUtil.getDefaultPasswordPolicyDescription(themeDisplay.getCompanyId());
		}

		return passwordPolicyHtml;

	}

}
