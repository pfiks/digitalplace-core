package com.placecube.digitalplace.search.web.portlet.category.service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.web.portlet.category.util.CategoryFacetTreeUtil;

@Component(immediate = true, service = CategoryFacetTreeService.class)
public class CategoryFacetTreeService {

	@Reference
	private CategoryFacetTreeUtil categoryFacetTreeUtil;

	@Reference
	private JSONFactory jsonFactory;

	public JSONArray toJSONTree(List<FacetResult> availableCategories, Set<String> activeValuesForFilter, int maxDepth, Locale locale) {
		JSONArray categoryTree = jsonFactory.createJSONArray();
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		Set<Long> categoriesToPrune = new HashSet<>();
		availableCategories = availableCategories.stream().filter(facet -> Validator.isNotNull(facet.getValue())).sorted(Comparator.comparingLong(facet->Long.parseLong(facet.getValue()))).collect(
				Collectors.toList());
		for (FacetResult facetResult : availableCategories) {
			Optional<AssetCategory> assetCategory = categoryFacetTreeUtil.fetchCategoryById(Long.parseLong(facetResult.getValue()));
			if (assetCategory.isPresent() && !insertedCategories.containsKey(assetCategory.get().getCategoryId())) {
				categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(facetResult), assetCategory.get(), categoryTree, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, locale);
			}
		}

		JSONArray prunedTree = jsonFactory.createJSONArray();
		categoryFacetTreeUtil.pruneCategoryTree(prunedTree, categoryTree, categoriesToPrune);

		return prunedTree;
	}
}
