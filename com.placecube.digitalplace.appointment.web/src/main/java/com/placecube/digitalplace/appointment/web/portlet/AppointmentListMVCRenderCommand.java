package com.placecube.digitalplace.appointment.web.portlet;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.object.model.ObjectDefinition;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.common.time.model.DateTimeSelection;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;
import com.placecube.digitalplace.appointment.web.configuration.AppointmentDashboardPortletInstanceConfiguration;
import com.placecube.digitalplace.appointment.web.constants.CriterionConstants;
import com.placecube.digitalplace.appointment.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.web.constants.PortletKeys;
import com.placecube.digitalplace.appointment.web.model.AppointmentDashboardModel;
import com.placecube.digitalplace.appointmentweb.service.AppointmentsDashboardUtil;
import com.placecube.digitalplace.appointmentweb.service.AppointmentsRetrievalService;
import com.placecube.digitalplace.appointmentweb.service.ObjectEntryRetrievalService;

@Component(immediate = true, property = { //
		"javax.portlet.name=" + PortletKeys.APPOINTMENTS_DASHBOARD, //
		"mvc.command.name=/", 
		"mvc.command.name=" + MVCCommandKeys.VIEW_APPOINTMENTS //
}, service = MVCRenderCommand.class)
public class AppointmentListMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AppointmentsRetrievalService appointmentsRetrievalService;

	@Reference
	private AppointmentsDashboardUtil appointmentsDashboardUtil;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private LayoutPageTemplateEntryLocalService layoutPageTemplateEntryLocalService;

	@Reference
	private ObjectEntryRetrievalService objectEntryRetrievalService;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {

			AppointmentDashboardPortletInstanceConfiguration configuration = configurationProvider.getPortletInstanceConfiguration(AppointmentDashboardPortletInstanceConfiguration.class,
					themeDisplay);

			String serviceId = configuration.serviceId();
			long configuredDPT = configuration.displayPageTemplateEntryId();
			long objectDefinitionId = configuration.objectDefinitionId();

			if (configuredDPT > 1 && objectDefinitionId > 1 && Validator.isNotNull(serviceId)) {
				long companyId = themeDisplay.getCompanyId();
				long classNameId = portal.getClassNameId(ObjectDefinition.class.getName() + "#" + objectDefinitionId);
				Optional<Criterion> dateFilterCriterionOpt = getDateFilterCriterion(renderRequest, renderResponse.getNamespace());
				Set<Long> includeOnlyTheseIds = objectEntryRetrievalService.getObjectEntryIdsFromFilterExpressions(objectDefinitionId, configuration.odataFilter());

				SearchContainer<AppointmentDashboardModel> searchContainer = appointmentsDashboardUtil.getSearchContainer(renderRequest, renderResponse);
				List<AppointmentEntry> appointmentEntries = appointmentsRetrievalService.getAppointmentEntries(serviceId, companyId, classNameId, dateFilterCriterionOpt, includeOnlyTheseIds,
						searchContainer.getStart(), searchContainer.getEnd());

				int totalResults = appointmentsRetrievalService.getAppointmentEntriesCount(serviceId, companyId, classNameId, dateFilterCriterionOpt, includeOnlyTheseIds);

				String baseUrl = getDTPBaseUrl(configuredDPT, themeDisplay.getLocale());
				List<AppointmentDashboardModel> appointmentDashboardModels = appointmentsDashboardUtil.convertAppointmentEntriresToAppointmentDashboardModels(appointmentEntries, baseUrl,
						configuration.summaryFieldExpression());

				searchContainer.setResultsAndTotal(() -> appointmentDashboardModels, totalResults);
				renderRequest.setAttribute("searchContainerAppointments", searchContainer);
				renderRequest.setAttribute("configuredIdColumnLabel", configuration.idColumnLabel());
				renderRequest.setAttribute("configuredFilterColumnLabel", configuration.filterColumnLabel());
				renderRequest.setAttribute("configuredStartDateColumnLabel", configuration.startDateColumnLabel());
				renderRequest.setAttribute("configuredSummaryColumnLabel", configuration.summaryColumnLabel());
			} else {
				renderRequest.setAttribute("invalidConfigurationError", true);
			}

		} catch (Exception e) {
			throw new PortletException(e);
		}

		return "/view-appointments.jsp";
	}

	private Optional<Criterion> getDateFilterCriterion(RenderRequest renderRequest, String namespace) throws ParseException {
		Optional<Criterion> dateFilterCriterionOpt;
		HttpServletRequest originalRequest = portal.getOriginalServletRequest(portal.getHttpServletRequest(renderRequest));

		String dateFilterType = ParamUtil.getString(originalRequest, namespace + "dateFilter", CriterionConstants.DATE_ALL);
		String fromDate = ParamUtil.getString(originalRequest, namespace + "fromDate", StringPool.BLANK);
		String toDate = ParamUtil.getString(originalRequest, namespace + "toDate", StringPool.BLANK);

		DateTimeSelection selectedDateFrom = appointmentsDashboardUtil.getEmptyDateTimeSelection();
		DateTimeSelection selectedDateTo = appointmentsDashboardUtil.getEmptyDateTimeSelection();
		if (CriterionConstants.DATE_RANGE.equals(dateFilterType) && Validator.isNotNull(fromDate)) {
			boolean useAutoToDate = Validator.isNull(toDate);
			if (useAutoToDate) {
				toDate = "01/01/3000";
			}
			Calendar fromDateCalendar = appointmentsDashboardUtil.getStartOfDayCalendar(fromDate);
			Calendar toDateCalendar = appointmentsDashboardUtil.getEndOfDayCalendar(toDate);
			dateFilterCriterionOpt = appointmentsRetrievalService.getDateRangeCriterion(fromDateCalendar, toDateCalendar);
			selectedDateFrom = new DateTimeSelection(fromDateCalendar);
			selectedDateTo = !useAutoToDate ? new DateTimeSelection(toDateCalendar) : selectedDateTo;
		} else {
			dateFilterCriterionOpt = appointmentsRetrievalService.getDateFilterCriterion(dateFilterType);
		}

		renderRequest.setAttribute("selectedDateFrom", selectedDateFrom);
		renderRequest.setAttribute("selectedDateTo", selectedDateTo);
		renderRequest.setAttribute("dateFilterType", dateFilterType);

		return dateFilterCriterionOpt;
	}

	private String getDTPBaseUrl(long layoutPageTemplateEntryId, Locale locale) throws PortalException {
		LayoutPageTemplateEntry layoutPageTemplateEntry = layoutPageTemplateEntryLocalService.getLayoutPageTemplateEntry(layoutPageTemplateEntryId);
		Layout layout = layoutLocalService.getLayout(layoutPageTemplateEntry.getPlid());
		return "/e" + layout.getFriendlyURL(locale);
	}

}
