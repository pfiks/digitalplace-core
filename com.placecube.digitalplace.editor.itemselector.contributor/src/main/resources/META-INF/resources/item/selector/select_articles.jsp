<%@ include file="init.jsp" %>

<c:set var="isDescriptiveView" value="${journalArticleItemSelectorViewHelper.isDescriptiveView(journalArticleItemSelectorViewDisplayContext.getDisplayStyle())}" />
<c:set var="isIconView" value="${journalArticleItemSelectorViewHelper.isIconView(journalArticleItemSelectorViewDisplayContext.getDisplayStyle())}" />

<clay:management-toolbar managementToolbarDisplayContext="${journalArticleItemSelectorViewManagementToolbarDisplayContext }" />

<clay:container-fluid
	cssClass="item-selector lfr-item-viewer"
	id='<%= liferayPortletResponse.getNamespace() + "articlesContainer" %>'
>
	<liferay-site-navigation:breadcrumb
		breadcrumbEntries="${journalArticleItemSelectorViewDisplayContext.getPortletBreadcrumbEntries()}"
	/>

	<liferay-ui:search-container
		emptyResultsMessage="no-web-content-was-found"
		id="articles"
		searchContainer="${journalArticleItemSelectorViewDisplayContext.getSearchContainer()}"
	>
		<liferay-ui:search-container-row
			className="Object"
			modelVar="object"
		>
		
			<c:choose>
				<c:when test="${journalArticleItemSelectorViewHelper.isRowObjectInstanceOfJournalFolder(row) }">
					<c:set var="curFolder" value="${object}" />
				</c:when>
				<c:otherwise>
					<c:set var="curArticle" value="${journalArticleItemSelectorViewDisplayContext.getLatestArticle(object)}" />
				</c:otherwise>
			</c:choose>

			<c:choose>
				<c:when test="${not empty curArticle}">
					${journalArticleItemSelectorViewHelper.setJournalArticleRowData(row, curArticle, themeDisplay)}
					${journalArticleItemSelectorViewHelper.addResultRowClass(row, "articles selector-button")}
					
					<c:choose>
						<c:when test="${isDescriptiveView }">

							${journalArticleItemSelectorViewHelper.addResultRowClass(row, "item-preview ")}
							
							<liferay-ui:search-container-column-text>
								<liferay-ui:user-portrait
									userId="${curArticle.getUserId()}"
								/>
							</liferay-ui:search-container-column-text>

							<liferay-ui:search-container-column-text
								colspan="<%= 2 %>"
							>
								<c:set var="modifiedDateDescription" value="${journalArticleItemSelectorViewHelper.getTimeDescription(themeDisplay.getRequest(), curArticle.getModifiedDate())}" />
								
								<span class="text-default">
									<liferay-ui:message arguments="${journalArticleItemSelectorViewHelper.toArray(html.escape(curArticle.getUserName()), modifiedDateDescription)}" key="x-modified-x-ago" />
								</span>

								<p class="font-weight-bold h5">
									${html.escape(curArticle.getTitle(locale, true))}
								</p>

								<c:if test="${journalArticleItemSelectorViewDisplayContext.isSearchEverywhere()}">
									<h6 class="text-default">
										<liferay-ui:message key="location" />:
										<span class="text-secondary">
											<clay:icon
												symbol="${journalArticleItemSelectorViewDisplayContext.getGroupCssIcon(curArticle.getGroupId())}"
											/>

											<small>${journalArticleItemSelectorViewDisplayContext.getGroupLabel(curArticle.getGroupId(), locale)}</small>
										</span>
									</h6>
								</c:if>
							</liferay-ui:search-container-column-text>
						</c:when>
						<c:when test="${isIconView }">
						
							${journalArticleItemSelectorViewHelper.addResultRowClass(row, "card-page-item card-page-item-directory entry ")}							
						 
							<liferay-ui:search-container-column-text>
								<clay:vertical-card
									verticalCard="${journalArticleItemSelectorViewHelper.getJournalArticleItemSelectorVerticalCard(curArticle, renderRequest)}"
								/>
							</liferay-ui:search-container-column-text>
						</c:when>
						<c:otherwise>
							${journalArticleItemSelectorViewHelper.addResultRowClass(row, "item-preview ")}
						
							<c:if test="${journalArticleItemSelectorViewDisplayContext.showArticleId()}">
								<liferay-ui:search-container-column-text
									name="id"
									value="${html.escape(curArticle.getArticleId())}"
								/>
							</c:if>

							<liferay-ui:search-container-column-text
								cssClass="table-cell-expand table-cell-minw-200 table-title"
								name="title"
								value="${curArticle.getTitle(locale, true)}"
							/>

							<liferay-ui:search-container-column-text
								cssClass="table-cell-expand table-cell-minw-200 text-truncate"
								name="description"
								value="${journalArticleItemSelectorViewHelper.shortenText(html.stripHtml(curArticle.getDescription(locale)), 200)}"
							/>

							<c:if test="${journalArticleItemSelectorViewDisplayContext.isSearchEverywhere()}">
								<liferay-ui:search-container-column-text
									name="location"
								>
									<span class="text-secondary">
										<clay:icon
											symbol="${journalArticleItemSelectorViewDisplayContext.getGroupCssIcon(curArticle.getGroupId())}"
										/>

										<small>${journalArticleItemSelectorViewDisplayContext.getGroupLabel(curArticle.getGroupId(), locale)}</small>
									</span>
								</liferay-ui:search-container-column-text>
							</c:if>

							<liferay-ui:search-container-column-text
								cssClass="table-cell-expand-smallest table-cell-minw-100"
								name="author"
								value="${html.escape(journalArticleItemSelectorViewHelper.getArticleAuthorName(curArticle)) }"
							/>

							<liferay-ui:search-container-column-date
								cssClass="table-cell-expand-smallest table-cell-ws-nowrap"
								name="modified-date"
								value="${curArticle.getModifiedDate()}"
							/>

							<liferay-ui:search-container-column-date
								cssClass="table-cell-expand-smallest table-cell-ws-nowrap"
								name="display-date"
								value="${curArticle.getDisplayDate()}"
							/>

							<liferay-ui:search-container-column-text
								cssClass="table-cell-expand-smallest table-cell-minw-100"
								name="type"
								value="${html.escape(curArticle.getDDMStructure().getName(locale))}"
							/>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:when test="${ not empty curFolder}">
					
					<c:set var="rowURL" value="${journalArticleItemSelectorViewHelper.buildFolderRowUrl(journalArticleItemSelectorViewDisplayContext.getPortletURL(), curFolder)}" />

					<c:choose>
						<c:when test="${isDescriptiveView }">
							<liferay-ui:search-container-column-icon
								icon="folder"
								toggleRowChecker="<%= true %>"
							/>

							<liferay-ui:search-container-column-text
								colspan="<%= 2 %>"
							>
								<c:set var="createDateDescription" value="${journalArticleItemSelectorViewHelper.getTimeDescription(themeDisplay.getRequest(), curFolder.getCreateDate())}" />								

								<span class="text-default">
									<liferay-ui:message arguments="${journalArticleItemSelectorViewHelper.toArray(html.escape(curFolder.getUserName()), createDateDescription)}" key="x-modified-x-ago" />
								</span>

								<p class="font-weight-bold h5">
									<a href="${rowURL }">
										${html.escape(curFolder.getName()) }
									</a>
								</p>

								<c:if test="${journalArticleItemSelectorViewDisplayContext.isSearchEverywhere()}">
									<h6 class="text-default">
										<liferay-ui:message key="location" />:
										<span class="text-secondary">
											<clay:icon
												symbol="${journalArticleItemSelectorViewDisplayContext.getGroupCssIcon(curFolder.getGroupId())}"
											/>

											<small>${journalArticleItemSelectorViewDisplayContext.getGroupLabel(curFolder.getGroupId(), locale)}</small>
										</span>
									</h6>
								</c:if>
							</liferay-ui:search-container-column-text>
						</c:when>
						<c:when test="${isIconView }">
							${journalArticleItemSelectorViewHelper.addResultRowClass(row, "card-page-item card-page-item-directory ")}
					
							<liferay-ui:search-container-column-text
								colspan="<%= 2 %>"
							>
								<div class="card card-horizontal card-interactive card-interactive-secondary card-type-directory">
									<div class="card-body">
										<div class="card-row">
											<clay:content-col>
												<clay:sticker
													displayType="secondary"
													icon="folder"
													inline="<%= true %>"
												/>
											</clay:content-col>

											<div class="autofit-col autofit-col-expand autofit-col-gutters">
												<a class="card-title text-truncate" href="${rowURL}" title="${html.escapeAttribute(curFolder.getName())}">
													${html.escape(curFolder.getName())}
												</a>

												<c:if test="${journalArticleItemSelectorViewDisplayContext.isSearchEverywhere()}">
													<span class="text-secondary">
														<clay:icon
															symbol="${journalArticleItemSelectorViewDisplayContext.getGroupCssIcon(curFolder.getGroupId())}"
														/>

														<small>${journalArticleItemSelectorViewDisplayContext.getGroupLabel(curFolder.getGroupId(), locale)}</small>
													</span>
												</c:if>
											</div>
										</div>
									</div>
								</div>
							</liferay-ui:search-container-column-text>
						</c:when>
						<c:otherwise>
							<liferay-ui:search-container-column-text
								cssClass="table-cell-expand table-cell-minw-200 table-list-title"
								href="${rowURL}"
								name="title"
								value="${html.escape(curFolder.getName())}"
							/>

							<liferay-ui:search-container-column-text
								cssClass="table-cell-expand table-cell-minw-200 text-truncate"
								name="description"
								value="${html.escape(curFolder.getDescription())}"
							/>

							<c:if test="${journalArticleItemSelectorViewDisplayContext.isSearchEverywhere()}">
								<liferay-ui:search-container-column-text
									name="location"
								>
									<span class="text-secondary">
										<clay:icon
											symbol="${journalArticleItemSelectorViewDisplayContext.getGroupCssIcon(curFolder.getGroupId())}"
										/>

										<small>${journalArticleItemSelectorViewDisplayContext.getGroupLabel(curFolder.getGroupId(), locale)}</small>
									</span>
								</liferay-ui:search-container-column-text>
							</c:if>

							<liferay-ui:search-container-column-text
								cssClass="table-cell-expand-smallest table-cell-minw-150"
								name="author"
								value="${html.escape(journalArticleItemSelectorViewHelper.getFolderAuthorName(curFolder))}"
							/>

							<liferay-ui:search-container-column-date
								cssClass="table-cell-expand-smallest table-cell-ws-nowrap"
								name="modified-date"
								value="${curFolder.getModifiedDate()}"
							/>

							<liferay-ui:search-container-column-text
								cssClass="table-cell-expand-smallest table-cell-ws-nowrap"
								name="display-date"
								value="--"
							/>

							<liferay-ui:search-container-column-text
								cssClass="table-cell-expand-smallest table-cell-minw-150"
								name="type"
								value='<%= LanguageUtil.get(request, "folder") %>'
							/>
						</c:otherwise>
					</c:choose>
				</c:when>
			</c:choose>
		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator
			displayStyle="${journalArticleItemSelectorViewDisplayContext.getDisplayStyle()}"
			markupView="lexicon"
			resultRowSplitter="<%= new JournalResultRowSplitter() %>"
			searchContainer="<%= searchContainer %>"
		/>
	</liferay-ui:search-container>
</clay:container-fluid>