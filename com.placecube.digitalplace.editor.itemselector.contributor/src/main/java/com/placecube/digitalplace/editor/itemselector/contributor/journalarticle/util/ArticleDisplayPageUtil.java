package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.util;

import com.liferay.asset.display.page.model.AssetDisplayPageEntry;
import com.liferay.asset.display.page.service.AssetDisplayPageEntryLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.service.ClassNameServiceUtil;

public class ArticleDisplayPageUtil {

    public static boolean articleHasDisplayPageTemplate(JournalArticle curArticle) {
        if (curArticle != null) {
            AssetDisplayPageEntry articleDisplayPageEntry = AssetDisplayPageEntryLocalServiceUtil.fetchAssetDisplayPageEntry(curArticle.getGroupId(), ClassNameServiceUtil.fetchClassName(JournalArticle.class.getName()).getClassNameId(), curArticle.getResourcePrimKey());
            if (articleDisplayPageEntry != null && articleDisplayPageEntry.getLayoutPageTemplateEntryId() != 0) {
                return true;
            }
        }
        return false;
    }
}
