package com.placecube.digitalplace.fileupload.liferaydl.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.mail.internet.InternetAddress;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.fileupload.liferaydl.configuration.LiferayDLConnectorConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LiferayDLServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 1l;
	private static final long GROUP_ID = 1l;
	private static final long PARENT_FOLDER_ID = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
	private static final String FOLDER_NAME = "FOLDER_NAME";
	private static final String SECOND_FOLDER_NAME = "SECOND_FOLDER_NAME";

	private static final long USER_ID = 1l;
	private static final long FOLDER_ID = 1l;
	private static final long SECOND_FOLDER_ID = 1l;

	@InjectMocks
	private LiferayDLService liferayDLService;

	@Mock
	private LiferayDLConnector defaultLiferayDLConnector;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private LiferayDLConnectorConfiguration mockLiferayDLConnectorConfiguration;

	@Mock
	private MailService mockMailService;

	@Mock
	private InternetAddress mockFromAddress;

	@Mock
	private InternetAddress mockToAddress;

	@Mock
	private MailMessage mockMailMessage;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private DLAppLocalService mockDlAppLocalService;

	@Mock
	private Folder mockFolder;

	@Mock
	private Folder mockFolder2;

	@Test(expected = ConfigurationException.class)
	public void isDefault_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(LiferayDLConnectorConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		liferayDLService.isDefault(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isDefault_WhenNoError_ThenReturnsIfTheConfigurationIsDefaultOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(LiferayDLConnectorConfiguration.class, COMPANY_ID)).thenReturn(mockLiferayDLConnectorConfiguration);
		when(mockLiferayDLConnectorConfiguration.isDefault()).thenReturn(expected);

		boolean result = liferayDLService.isDefault(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = ConfigurationException.class)
	public void isEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(LiferayDLConnectorConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		liferayDLService.isEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(LiferayDLConnectorConfiguration.class, COMPANY_ID)).thenReturn(mockLiferayDLConnectorConfiguration);
		when(mockLiferayDLConnectorConfiguration.isEnabled()).thenReturn(expected);

		boolean result = liferayDLService.isEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = PortalException.class)
	public void getFolderId_WhenExceptionAddingFolder_thenThrowsPortalException() throws PortalException {

		mockServiceContext();

		when(mockDlAppLocalService.getFolder(GROUP_ID, PARENT_FOLDER_ID, FOLDER_NAME)).thenThrow(new PortalException());
		when(mockDlAppLocalService.addFolder(null, USER_ID, GROUP_ID, PARENT_FOLDER_ID, FOLDER_NAME, FOLDER_NAME, mockServiceContext)).thenThrow(new PortalException());

		liferayDLService.getFolderId(FOLDER_NAME, mockServiceContext);
	}

	@Test
	public void getFolderId_WhenSingleFolderPath_thenReturnsFolderId() throws PortalException {

		mockServiceContext();

		when(mockDlAppLocalService.getFolder(GROUP_ID, PARENT_FOLDER_ID, FOLDER_NAME)).thenReturn(mockFolder);
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);

		long result = liferayDLService.getFolderId(FOLDER_NAME, mockServiceContext);

		assertThat(result, equalTo(FOLDER_ID));
	}

	@Test
	public void getFolderId_WhenMultipleFolderPath_thenReturnsFolderId() throws PortalException {

		mockServiceContext();

		String path = FOLDER_NAME + StringPool.FORWARD_SLASH + SECOND_FOLDER_NAME;

		when(mockDlAppLocalService.getFolder(GROUP_ID, PARENT_FOLDER_ID, FOLDER_NAME)).thenReturn(mockFolder);
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);

		when(mockDlAppLocalService.getFolder(GROUP_ID, FOLDER_ID, SECOND_FOLDER_NAME)).thenReturn(mockFolder2);
		when(mockFolder2.getFolderId()).thenReturn(SECOND_FOLDER_ID);

		long result = liferayDLService.getFolderId(path, mockServiceContext);

		assertThat(result, equalTo(SECOND_FOLDER_ID));
	}

	@Test
	public void getFolderId_WhenPathHasLeadingSlash_thenReturnsFolderId() throws PortalException {

		mockServiceContext();

		String path = StringPool.FORWARD_SLASH + FOLDER_NAME;

		when(mockDlAppLocalService.getFolder(GROUP_ID, PARENT_FOLDER_ID, FOLDER_NAME)).thenReturn(mockFolder);
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);

		long result = liferayDLService.getFolderId(path, mockServiceContext);

		assertThat(result, equalTo(FOLDER_ID));
	}

	@Test
	public void getFolderId_WhenPathHasTrailingSlash_thenReturnsFolderId() throws PortalException {

		mockServiceContext();

		String path = FOLDER_NAME + StringPool.FORWARD_SLASH;

		when(mockDlAppLocalService.getFolder(GROUP_ID, PARENT_FOLDER_ID, FOLDER_NAME)).thenReturn(mockFolder);
		when(mockFolder.getFolderId()).thenReturn(FOLDER_ID);

		long result = liferayDLService.getFolderId(path, mockServiceContext);

		assertThat(result, equalTo(FOLDER_ID));
	}

	private void mockServiceContext() {
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
	}
}
