package com.placecube.digitalplace.externalpage.search;

import com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.util.SystemBundleUtil;
import com.liferay.portal.kernel.search.*;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AssetRendererFactoryRegistryUtil.class, SystemBundleUtil.class, IndexerRegistryUtil.class, IndexWriterHelperUtil.class, PropsUtil.class, SearchEngineHelperUtil.class,
		FastDateFormatFactoryUtil.class, GroupLocalServiceUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.module.util.SystemBundleUtil",
		"com.liferay.portal.kernel.search.SearchEngineHelperUtil", "com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil" })
public class ExternalPageIndexerTest extends PowerMockito {

	private static final String DATE_PATTERN = "dd.MM.yyyy";
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_PATTERN);

	private ExternalPageIndexer externalPageIndexer;

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private Document mockDocument;

	@Mock
	private ExternalPage mockExternalPage;

	@Mock
	private ExternalPageLocalService mockExternalPageLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private IndexWriterHelper mockIndexWriterHelper;

	@Mock
	private HtmlParser mockHtmlParser;

	@Captor
	private ArgumentCaptor<ActionableDynamicQuery.PerformActionMethod<ExternalPage>> performActionMethodArgumentCaptor;

	@Before
	public void activateSetup() throws PortalException {
		mockStatic(AssetRendererFactoryRegistryUtil.class, SystemBundleUtil.class, IndexerRegistryUtil.class, IndexWriterHelperUtil.class, PropsUtil.class, SearchEngineHelperUtil.class,
				FastDateFormatFactoryUtil.class, GroupLocalServiceUtil.class);
		when(SystemBundleUtil.getBundleContext()).thenReturn(mockBundleContext);
		when(PropsUtil.get(PropsKeys.INDEX_DATE_FORMAT_PATTERN)).thenReturn(DATE_PATTERN);
		when(FastDateFormatFactoryUtil.getSimpleDateFormat(DATE_PATTERN)).thenReturn(DATE_FORMAT);
		when(GroupLocalServiceUtil.getGroup(anyLong())).thenReturn(mockGroup);

		externalPageIndexer = new ExternalPageIndexer();
		Whitebox.setInternalState(externalPageIndexer, "indexWriterHelper", mockIndexWriterHelper);
		Whitebox.setInternalState(externalPageIndexer, "externalPageLocalService", mockExternalPageLocalService);
		Whitebox.setInternalState(externalPageIndexer, "htmlParser", mockHtmlParser);
	}

	@Test
	public void getClassName_WhenNoError_ThenReturnsExternalPageClassName() {
		String result = externalPageIndexer.getClassName();

		assertEquals(ExternalPage.class.getName(), result);
	}

	@Test
	public void doDelete_WhenNoError_ThenDeletesDocument() throws Exception {
		long companyId = 123;
		String externalPageUID = "externalPageUID";

		when(mockExternalPage.getCompanyId()).thenReturn(companyId);
		when(mockExternalPage.getUuid()).thenReturn(externalPageUID);

		externalPageIndexer.setCommitImmediately(true);
		externalPageIndexer.doDelete(mockExternalPage);

		verifyStatic(IndexWriterHelperUtil.class, times(1));
		IndexWriterHelperUtil.deleteDocument(companyId, externalPageUID, true);

	}

	@Test
	public void doGetDocument_WhenStatusIsFalse_ThenReturnsNull() throws Exception {

		when(mockExternalPage.getStatus()).thenReturn(false);

		Document result = externalPageIndexer.doGetDocument(mockExternalPage);

		assertNull(result);
	}

	@Test
	public void doGetDocument_WhenStatusIsTrue_ThenReturnsDocumentWithFieldsSet() throws Exception {
		String uuid = "uuid";
		String url = "url";
		String title = "title";
		String titleExtracted = "titleExtracted";
		String content = "content";
		String contentExtracted = "contentExtracted";
		long classPK = 10;
		long companyId = 11;
		long groupId = 12;
		long userId = 13;
		Date createDate = new Date(1000);
		Date modifiedDate = new Date(2000);

		when(mockExternalPage.getStatus()).thenReturn(true);
		when(mockExternalPage.getUuid()).thenReturn(uuid);
		when(mockExternalPage.getPrimaryKeyObj()).thenReturn(classPK);
		when(mockExternalPage.getExternalPageId()).thenReturn(classPK);
		when(mockExternalPage.getCompanyId()).thenReturn(companyId);
		when(mockExternalPage.getGroupId()).thenReturn(groupId);
		when(mockExternalPage.getUserId()).thenReturn(userId);
		when(mockExternalPage.getUrl()).thenReturn(url);
		when(mockExternalPage.getTitle()).thenReturn(title);
		when(mockExternalPage.getContent()).thenReturn(content);
		when(mockExternalPage.getCreateDate()).thenReturn(createDate);
		when(mockExternalPage.getModifiedDate()).thenReturn(modifiedDate);

		when(mockHtmlParser.extractText(title)).thenReturn(titleExtracted);
		when(mockHtmlParser.extractText(content)).thenReturn(contentExtracted);

		Document result = externalPageIndexer.doGetDocument(mockExternalPage);

		assertNotNull(result);
		assertEquals(uuid, result.get(Field.UID));
		assertEquals(ExternalPage.class.getName(), result.get(Field.ENTRY_CLASS_NAME));
		assertEquals(String.valueOf(classPK), result.get(Field.CLASS_PK));
		assertEquals(String.valueOf(companyId), result.get(Field.COMPANY_ID));
		assertEquals(String.valueOf(groupId), result.get(Field.GROUP_ID));
		assertEquals(String.valueOf(groupId), result.get(Field.SCOPE_GROUP_ID));
		assertEquals(String.valueOf(userId), result.get(Field.USER_ID));
		assertEquals(String.valueOf(WorkflowConstants.STATUS_APPROVED), result.get(Field.STATUS));
		assertEquals(url, result.get(Field.URL));
		assertEquals(titleExtracted, result.get(Field.TITLE));
		assertEquals(contentExtracted, result.get(Field.CONTENT));
		assertEquals(DATE_FORMAT.format(createDate), result.get(Field.CREATE_DATE));
		assertEquals(DATE_FORMAT.format(createDate), result.get(Field.DISPLAY_DATE));
		assertEquals(DATE_FORMAT.format(createDate), result.get(Field.PUBLISH_DATE));
		assertEquals(DATE_FORMAT.format(modifiedDate), result.get(Field.MODIFIED_DATE));
	}

	@Test
	public void doGetSummary_WhenNoError_ThenReturnsSummary() throws Exception {
		String title = "title";
		String content = "content";

		when(mockDocument.get(Field.TITLE)).thenReturn(title);
		when(mockDocument.get(Field.CONTENT)).thenReturn(content);

		Summary result = externalPageIndexer.doGetSummary(mockDocument, Locale.UK, null, null, null);

		assertEquals(title, result.getTitle());
		assertEquals(content, result.getContent());
		assertEquals(200, result.getMaxContentLength());
	}

	@Test
	public void doReindex_CalledWithExternalPage_WhenExternalPageIsNull_ThenDoesNothing() throws Exception {
		externalPageIndexer.doReindex((ExternalPage) null);

		verify(mockIndexWriterHelper, never()).updateDocument(anyLong(), any(Document.class));
	}

	@Test
	public void doReindex_CalledWithExternalPage_WhenExternalPageIsNotNullAndStatusIsTrue_ThenReindexesTheExternalPage() throws Exception {
		long companyId = 123;
		long classPK = 741;

		when(mockExternalPage.getStatus()).thenReturn(true);
		when(mockExternalPage.getPrimaryKeyObj()).thenReturn(classPK);
		when(mockExternalPage.getCompanyId()).thenReturn(companyId);

		externalPageIndexer.setCommitImmediately(true);
		externalPageIndexer.doReindex(mockExternalPage);

		verify(mockIndexWriterHelper, times(1)).updateDocument(eq(companyId), any(Document.class));

	}

	@Test
	public void doReindex_CalledWithExternalPage_WhenExternalPageIsFalse_ThenDoesNotReindexButDeletesTheExternalPage() throws Exception {
		long companyId = 123;
		String externalPageUID = "externalPageUID";

		when(mockExternalPage.getCompanyId()).thenReturn(companyId);
		when(mockExternalPage.getUuid()).thenReturn(externalPageUID);
		when(mockExternalPage.getStatus()).thenReturn(false);

		externalPageIndexer.setCommitImmediately(true);
		externalPageIndexer.doReindex(mockExternalPage);

		verify(mockIndexWriterHelper, never()).updateDocument(anyLong(), any(Document.class));
		verifyStatic(IndexWriterHelperUtil.class, times(1));
		IndexWriterHelperUtil.deleteDocument(companyId, externalPageUID, true);

	}

	@Test
	public void doReindex_CalledWithClassNameAndClassPK_WhenClassNameIsNotExternalPage_ThenDoesNotReindexOrDeleteDocument() throws Exception {
		String className = "className";
		long companyId = 123;
		long classPK = 741;

		when(mockExternalPageLocalService.getExternalPage(classPK)).thenReturn(mockExternalPage);
		when(mockExternalPage.getStatus()).thenReturn(true);
		when(mockExternalPage.getPrimaryKeyObj()).thenReturn(classPK);
		when(mockExternalPage.getCompanyId()).thenReturn(companyId);

		externalPageIndexer.setCommitImmediately(true);
		externalPageIndexer.doReindex(className, classPK);

		verify(mockIndexWriterHelper, never()).updateDocument(anyLong(), any(Document.class));
		verifyStatic(IndexWriterHelperUtil.class, never());
		IndexWriterHelperUtil.deleteDocument(anyLong(), anyString(), anyBoolean());

	}

	@Test
	public void doReindex_CalledWithClassNameAndClassPK_WhenClassNameIsExternalPageAndStatusIsTrue_ThenReindexesTheExternalPage() throws Exception {
		String className = ExternalPage.class.getName();
		long companyId = 123;
		long classPK = 741;

		when(mockExternalPageLocalService.getExternalPage(classPK)).thenReturn(mockExternalPage);
		when(mockExternalPage.getStatus()).thenReturn(true);
		when(mockExternalPage.getPrimaryKeyObj()).thenReturn(classPK);
		when(mockExternalPage.getCompanyId()).thenReturn(companyId);

		externalPageIndexer.setCommitImmediately(true);
		externalPageIndexer.doReindex(className, classPK);

		verify(mockIndexWriterHelper, times(1)).updateDocument(eq(companyId), any(Document.class));

	}

	@Test
	public void doReindex_CalledWithClassNameAndClassPK_WhenExternalPageIsFalse_ThenDoesNotReindexButDeletesTheExternalPage() throws Exception {
		String className = ExternalPage.class.getName();
		String externalPageUID = "externalPageUID";
		long companyId = 123;
		long classPK = 741;

		when(mockExternalPageLocalService.getExternalPage(classPK)).thenReturn(mockExternalPage);
		when(mockExternalPage.getCompanyId()).thenReturn(companyId);
		when(mockExternalPage.getUuid()).thenReturn(externalPageUID);
		when(mockExternalPage.getStatus()).thenReturn(false);

		externalPageIndexer.setCommitImmediately(true);
		externalPageIndexer.doReindex(className, classPK);

		verify(mockIndexWriterHelper, never()).updateDocument(anyLong(), any(Document.class));
		verifyStatic(IndexWriterHelperUtil.class, times(1));
		IndexWriterHelperUtil.deleteDocument(companyId, externalPageUID, true);

	}

	@Test
	public void doReindex_CalledWithIds_WhenIdsLengthIs0_ThenDoesCallAnyAction() throws Exception {
		externalPageIndexer.doReindex(new String[] {});

		verify(mockExternalPageLocalService, never()).getIndexableActionableDynamicQuery();

	}

	@Test
	public void doReindex_CalledWithIds_WhenIdsLengthIsGreaterThan0_ThenPerformsAddDocumentsActionOnAGivenCompany() throws Exception {
		String uuid = "uuid";
		long classPK = 10;
		long companyId = 11;

		IndexableActionableDynamicQuery mockIndexableDynamicQuery = mock(IndexableActionableDynamicQuery.class);
		when(mockExternalPageLocalService.getIndexableActionableDynamicQuery()).thenReturn(mockIndexableDynamicQuery);
		when(mockExternalPage.getStatus()).thenReturn(true);
		when(mockExternalPage.getUuid()).thenReturn(uuid);
		when(mockExternalPage.getPrimaryKeyObj()).thenReturn(classPK);

		externalPageIndexer.doReindex(new String[] { String.valueOf(companyId) });

		InOrder inOrder = inOrder(mockIndexableDynamicQuery);
		inOrder.verify(mockIndexableDynamicQuery, times(1)).setCompanyId(companyId);
		inOrder.verify(mockIndexableDynamicQuery, times(1)).setPerformActionMethod(performActionMethodArgumentCaptor.capture());
		inOrder.verify(mockIndexableDynamicQuery, times(1)).performActions();

		ActionableDynamicQuery.PerformActionMethod<ExternalPage> actionMethod = performActionMethodArgumentCaptor.getValue();
		actionMethod.performAction(mockExternalPage);
		verify(mockIndexableDynamicQuery, times(1)).addDocuments(any(Document.class));
	}
}
