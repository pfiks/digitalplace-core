package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.TermQuery;
import com.placecube.digitalplace.search.web.service.ObjectFactoryUtil;

@Component(immediate = true, service = AdvancePeopleSearchQueryHelper.class)
public class AdvancePeopleSearchQueryHelper {

	public BooleanQuery getValueAsTermsQuery(String field, String value) throws ParseException {

		String[] splitValues = value.split(StringPool.SPACE);
		BooleanQuery multiTermQuery = ObjectFactoryUtil.createBooleanQuery();

		for (String splitValue : splitValues) {
			TermQuery termQuery = ObjectFactoryUtil.createTermQuery(field, splitValue);
			multiTermQuery.add(termQuery, BooleanClauseOccur.MUST);
		}

		return multiTermQuery;

	}

	public BooleanQuery getValueAsExactMatchQuery(String field, String value) {

		BooleanQuery fullValueQuery = ObjectFactoryUtil.createBooleanQuery();
		fullValueQuery.addExactTerm(field, value);

		return fullValueQuery;
	}

	public BooleanClause<Query> createBooleanClause(BooleanQuery booleanQuery, BooleanClauseOccur booleanClauseOccur) {
		return BooleanClauseFactoryUtil.create(booleanQuery, booleanClauseOccur.getName());
	}

}
