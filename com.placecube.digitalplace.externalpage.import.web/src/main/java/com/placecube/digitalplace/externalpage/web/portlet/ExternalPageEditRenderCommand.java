package com.placecube.digitalplace.externalpage.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.externalpage.model.ExternalPage;
import com.placecube.digitalplace.externalpage.service.ExternalPageLocalService;
import com.placecube.digitalplace.externalpage.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletKeys;
import com.placecube.digitalplace.externalpage.web.constants.ViewKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.EXTERNAL_PAGE_IMPORT_PORTLET, "mvc.command.name=" + MVCCommandKeys.EXTERNAL_PAGE_EDIT }, service = MVCRenderCommand.class)
public class ExternalPageEditRenderCommand implements MVCRenderCommand {

	@Reference
	private ExternalPageLocalService externalPageLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		long externalPageId = ParamUtil.getLong(renderRequest, "externalPageId");

		if (externalPageId > 0) {
			ExternalPage externalPage = externalPageLocalService.fetchExternalPage(externalPageId);
			renderRequest.setAttribute("externalPage", externalPage);
		}

		return ViewKeys.EDIT;
	}

}
