var namespace;

function loadScript(GOOGLE_API_KEY, portletNamespace) {
	namespace = portletNamespace;
	if (!(window.google && window.google.maps)) {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://maps.googleapis.com/maps/api/js?key=' + GOOGLE_API_KEY + '&callback=initAutocomplete&libraries=places&v=weekly'; //& needed
		document.body.appendChild(script);
	} else {
		initAutocomplete();
	}

}

function initAutocomplete() {
	var autocomplete = new google.maps.places.Autocomplete(document.querySelector('#' + namespace + 'autocomplete'), {
		componentRestrictions: { country: ['uk'] },
		fields: ['formatted_address', 'geometry', 'name'],
		types: ['geocode'],
	});
	autocomplete.addListener("place_changed", () => {
		var place = autocomplete.getPlace();
		if (!place.geometry || !place.geometry.location) {
			return;
		}
		if (place.geometry.location) {
			document.getElementById(namespace + "latitude").value = place.geometry.location.lat();
			document.getElementById(namespace + "longitude").value = place.geometry.location.lng();
			document.getElementById(namespace + "formattedAddress").value = place.formatted_address;
			document.getElementById(namespace + "location-search-form").submit();
		}

	});
}

function clearCurrentLocation() {
	document.getElementById(namespace + "formattedAddress").value = '';
	document.getElementById(namespace + "location-search-form").submit();
}

function getCurrentLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition);
	} else {
		window.alert("Geolocation is not supported by this browser.");
	}
}

function showPosition(position) {
	document.getElementById(namespace + "latitude").value = position.coords.latitude;
	document.getElementById(namespace + "longitude").value = position.coords.longitude;
	document.getElementById(namespace + "formattedAddress").value = "Current location";
	document.getElementById(namespace + "location-search-form").submit();
}