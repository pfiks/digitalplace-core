/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service;

import com.liferay.petra.sql.dsl.query.DSLQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.group.rest.model.GroupApiKey;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for GroupApiKey. This utility wraps
 * <code>com.placecube.digitalplace.group.rest.service.impl.GroupApiKeyLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see GroupApiKeyLocalService
 * @generated
 */
public class GroupApiKeyLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.placecube.digitalplace.group.rest.service.impl.GroupApiKeyLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the group api key to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupApiKeyLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupApiKey the group api key
	 * @return the group api key that was added
	 */
	public static GroupApiKey addGroupApiKey(GroupApiKey groupApiKey) {
		return getService().addGroupApiKey(groupApiKey);
	}

	/**
	 * Creates a new group api key with the primary key. Does not add the group api key to the database.
	 *
	 * @param groupApiKeyPK the primary key for the new group api key
	 * @return the new group api key
	 */
	public static GroupApiKey createGroupApiKey(
		com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK
			groupApiKeyPK) {

		return getService().createGroupApiKey(groupApiKeyPK);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the group api key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupApiKeyLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupApiKey the group api key
	 * @return the group api key that was removed
	 */
	public static GroupApiKey deleteGroupApiKey(GroupApiKey groupApiKey) {
		return getService().deleteGroupApiKey(groupApiKey);
	}

	/**
	 * Deletes the group api key with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupApiKeyLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key that was removed
	 * @throws PortalException if a group api key with the primary key could not be found
	 */
	public static GroupApiKey deleteGroupApiKey(
			com.placecube.digitalplace.group.rest.service.persistence.
				GroupApiKeyPK groupApiKeyPK)
		throws PortalException {

		return getService().deleteGroupApiKey(groupApiKeyPK);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static <T> T dslQuery(DSLQuery dslQuery) {
		return getService().dslQuery(dslQuery);
	}

	public static int dslQueryCount(DSLQuery dslQuery) {
		return getService().dslQueryCount(dslQuery);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static GroupApiKey fetchGroupApiKey(
		com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK
			groupApiKeyPK) {

		return getService().fetchGroupApiKey(groupApiKeyPK);
	}

	public static String generateKey(
			com.placecube.digitalplace.group.rest.constants.GroupRestKeyType
				keyType)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		return getService().generateKey(keyType);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static String getEncodedTextWithGroupKey(
			com.liferay.portal.kernel.model.Group group, String textToEncode)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		return getService().getEncodedTextWithGroupKey(group, textToEncode);
	}

	public static String getEncodedTextWithKey(
			String applicationKey, String textToEncode)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		return getService().getEncodedTextWithKey(applicationKey, textToEncode);
	}

	/**
	 * Returns the group api key with the primary key.
	 *
	 * @param groupApiKeyPK the primary key of the group api key
	 * @return the group api key
	 * @throws PortalException if a group api key with the primary key could not be found
	 */
	public static GroupApiKey getGroupApiKey(
			com.placecube.digitalplace.group.rest.service.persistence.
				GroupApiKeyPK groupApiKeyPK)
		throws PortalException {

		return getService().getGroupApiKey(groupApiKeyPK);
	}

	/**
	 * Returns a range of all the group api keys.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of group api keys
	 * @param end the upper bound of the range of group api keys (not inclusive)
	 * @return the range of group api keys
	 */
	public static List<GroupApiKey> getGroupApiKeys(int start, int end) {
		return getService().getGroupApiKeys(start, end);
	}

	/**
	 * Returns the number of group api keys.
	 *
	 * @return the number of group api keys
	 */
	public static int getGroupApiKeysCount() {
		return getService().getGroupApiKeysCount();
	}

	public static String getGroupApplicationKey(long companyId, long groupId) {
		return getService().getGroupApplicationKey(companyId, groupId);
	}

	public static String getGroupApplicationSecret(
		long companyId, long groupId) {

		return getService().getGroupApplicationSecret(companyId, groupId);
	}

	public static com.liferay.portal.kernel.model.Group getGroupByKey(
			com.placecube.digitalplace.group.rest.constants.GroupRestKeyType
				keyType,
			String keyValue)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		return getService().getGroupByKey(keyType, keyValue);
	}

	public static String getGroupRestKeyValue(
		com.liferay.portal.kernel.model.Group group,
		com.placecube.digitalplace.group.rest.constants.GroupRestKeyType
			keyType) {

		return getService().getGroupRestKeyValue(group, keyType);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static void regenerateGroupApiKey(
			com.liferay.portal.kernel.model.Group group)
		throws com.placecube.digitalplace.group.rest.exception.
			GroupRestKeyException {

		getService().regenerateGroupApiKey(group);
	}

	/**
	 * Updates the group api key in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GroupApiKeyLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param groupApiKey the group api key
	 * @return the group api key that was updated
	 */
	public static GroupApiKey updateGroupApiKey(GroupApiKey groupApiKey) {
		return getService().updateGroupApiKey(groupApiKey);
	}

	public static GroupApiKeyLocalService getService() {
		return _service;
	}

	public static void setService(GroupApiKeyLocalService service) {
		_service = service;
	}

	private static volatile GroupApiKeyLocalService _service;

}