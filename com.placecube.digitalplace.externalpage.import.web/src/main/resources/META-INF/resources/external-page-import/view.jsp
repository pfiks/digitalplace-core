<%@include file="init.jsp"%>

<portlet:actionURL name="<%=MVCCommandKeys.EXTERNAL_PAGE_DELETE %>" var="deleteExternalPageURL">
	<portlet:param name="redirect" value="${currentURL}" />
</portlet:actionURL>
						
						
<div class="container-fluid container-fluid-max-xl container-table">
	<clay:management-toolbar 
		actionDropdownItems="${externalManagementToolbarDisplayContext.getActionItemsDropdownItems()}" 
		additionalProps="${externalManagementToolbarDisplayContext.getAdditionalProps()}" 
		componentId="externalManagementToolbar" 
		searchContainerId="${externalManagementToolbarDisplayContext.getSearchContainerId()}" 
		showCreationMenu="true" 
		creationMenu="${externalManagementToolbarDisplayContext.getCreationMenu()}" 
		showSearch="false" 
		selectable="true" 
		itemsTotal="${searchContainers.getTotal()}" 
		propsTransformer="js/ManagementToolbarPropsTransformer"/>

<aui:form action="${deleteExternalPageURL}" method="post" name="searchContainerForm">
	<aui:input name="deleteExternalPageIds" type="hidden" />
	
		<liferay-ui:search-container rowChecker="<%= new RowChecker(renderResponse) %>" emptyResultsMessage="no-results-were-found" id="${externalManagementToolbarDisplayContext.getSearchContainerId()}" iteratorURL="${portletURL}" total="${searchContainers.getTotal()}">
			
			<liferay-ui:search-container-results results="${searchContainers.getResults()}" />
			
			<liferay-ui:search-container-row keyProperty="externalPageId" className="com.placecube.digitalplace.externalpage.model.ExternalPage" modelVar="entry">
				
				<portlet:renderURL var="viewExternalPageURL">
					<portlet:param name="mvcRenderCommandName" value="<%=MVCCommandKeys.EXTERNAL_PAGE_VIEW %>" />
					<portlet:param name="redirect" value="${currentURL}" />
					<portlet:param name="externalPageId" value="${entry.externalPageId}" />
				</portlet:renderURL>
				
				<liferay-ui:search-container-column-text name="title" value="${entry.title}" />
				<liferay-ui:search-container-column-text name="url" value="${entry.url}" />
				<liferay-ui:search-container-column-date name="create-date" property="createDate" />
				<c:choose>
					<c:when  test='${entry.status eq false}'>
						<liferay-ui:search-container-column-text name="status" value="Unpublished" />
					</c:when>
					<c:otherwise>
						<liferay-ui:search-container-column-text name="status" value="Published" />
					</c:otherwise>
				</c:choose>					
				<liferay-ui:search-container-column-jsp align="right" path="/external-page-import/external_action.jsp" />
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator displayStyle="list" markupView="lexicon" />
		</liferay-ui:search-container>
		
	</aui:form>
</div>