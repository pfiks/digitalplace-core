/**
 * SaleBase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class SaleBase  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.SummaryData saleSummary;

    private com.capita_software_services.www.scp.base.ContactDetails deliveryDetails;

    private com.capita_software_services.www.scp.base.LgSaleDetails lgSaleDetails;

    public SaleBase() {
    }

    public SaleBase(
           com.capita_software_services.www.scp.base.SummaryData saleSummary,
           com.capita_software_services.www.scp.base.ContactDetails deliveryDetails,
           com.capita_software_services.www.scp.base.LgSaleDetails lgSaleDetails) {
           this.saleSummary = saleSummary;
           this.deliveryDetails = deliveryDetails;
           this.lgSaleDetails = lgSaleDetails;
    }


    /**
     * Gets the saleSummary value for this SaleBase.
     *
     * @return saleSummary
     */
    public com.capita_software_services.www.scp.base.SummaryData getSaleSummary() {
        return saleSummary;
    }


    /**
     * Sets the saleSummary value for this SaleBase.
     *
     * @param saleSummary
     */
    public void setSaleSummary(com.capita_software_services.www.scp.base.SummaryData saleSummary) {
        this.saleSummary = saleSummary;
    }


    /**
     * Gets the deliveryDetails value for this SaleBase.
     *
     * @return deliveryDetails
     */
    public com.capita_software_services.www.scp.base.ContactDetails getDeliveryDetails() {
        return deliveryDetails;
    }


    /**
     * Sets the deliveryDetails value for this SaleBase.
     *
     * @param deliveryDetails
     */
    public void setDeliveryDetails(com.capita_software_services.www.scp.base.ContactDetails deliveryDetails) {
        this.deliveryDetails = deliveryDetails;
    }


    /**
     * Gets the lgSaleDetails value for this SaleBase.
     *
     * @return lgSaleDetails
     */
    public com.capita_software_services.www.scp.base.LgSaleDetails getLgSaleDetails() {
        return lgSaleDetails;
    }


    /**
     * Sets the lgSaleDetails value for this SaleBase.
     *
     * @param lgSaleDetails
     */
    public void setLgSaleDetails(com.capita_software_services.www.scp.base.LgSaleDetails lgSaleDetails) {
        this.lgSaleDetails = lgSaleDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaleBase)) return false;
        SaleBase other = (SaleBase) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.saleSummary==null && other.getSaleSummary()==null) ||
             (this.saleSummary!=null &&
              this.saleSummary.equals(other.getSaleSummary()))) &&
            ((this.deliveryDetails==null && other.getDeliveryDetails()==null) ||
             (this.deliveryDetails!=null &&
              this.deliveryDetails.equals(other.getDeliveryDetails()))) &&
            ((this.lgSaleDetails==null && other.getLgSaleDetails()==null) ||
             (this.lgSaleDetails!=null &&
              this.lgSaleDetails.equals(other.getLgSaleDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSaleSummary() != null) {
            _hashCode += getSaleSummary().hashCode();
        }
        if (getDeliveryDetails() != null) {
            _hashCode += getDeliveryDetails().hashCode();
        }
        if (getLgSaleDetails() != null) {
            _hashCode += getLgSaleDetails().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SaleBase.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "saleBase"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saleSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "saleSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "summaryData"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deliveryDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "deliveryDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "contactDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lgSaleDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lgSaleDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lgSaleDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
