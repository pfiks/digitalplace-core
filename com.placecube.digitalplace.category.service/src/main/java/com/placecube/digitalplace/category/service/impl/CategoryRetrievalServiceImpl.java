package com.placecube.digitalplace.category.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.category.property.model.AssetCategoryProperty;
import com.liferay.asset.category.property.service.AssetCategoryPropertyLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.category.service.CategoryRetrievalService;

@Component(immediate = true, service = CategoryRetrievalService.class)
public class CategoryRetrievalServiceImpl implements CategoryRetrievalService {

	private static final Log LOG = LogFactoryUtil.getLog(CategoryRetrievalServiceImpl.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private AssetCategoryPropertyLocalService assetCategoryPropertyLocalService;

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Override
	public String getCategoriesPropertyValues(AssetEntry assetEntry, String propertyKey, long vocabularyId) {
		List<AssetCategory> categories = filterAssetCategoriesByVocabularyId(assetEntry.getCategories(), vocabularyId);

		return getAssetCategoriesJoinedPropertyValues(categories, propertyKey);
	}

	@Override
	public String getCategoriesPropertyValues(String className, long classPK, String propertyKey, long vocabularyId) throws PortalException {
		AssetEntry assetEntry = assetEntryLocalService.getEntry(className, classPK);

		return getCategoriesPropertyValues(assetEntry, propertyKey, vocabularyId);
	}

	@Override
	public List<AssetCategory> getCategoriesByParentCategory(String className, long classPK, long parentCategoryId) {
		List<AssetCategory> assetCategories = assetCategoryLocalService.getCategories(className, classPK);
		return filterAssetCategoriesByParentCategory(assetCategories, parentCategoryId);
	}

	@Override
	public String getPropertyValue(AssetCategory assetCategory, String propertyKey) {
		try {
			AssetCategoryProperty categoryProperty = assetCategoryPropertyLocalService.getCategoryProperty(assetCategory.getCategoryId(), propertyKey);
			return categoryProperty.getValue();
		} catch (Exception e) {
			LOG.debug(e);
			return StringPool.BLANK;
		}
	}

	private List<AssetCategory> filterAssetCategoriesByVocabularyId(List<AssetCategory> assetCategories, long vocabularyId) {
		return assetCategories.stream().filter(a -> a.getVocabularyId() == vocabularyId).collect(Collectors.toList());
	}

	private String getAssetCategoriesJoinedPropertyValues(List<AssetCategory> categories, String propertyKey) {
		return categories.stream().map(c -> getPropertyValue(c, propertyKey)).filter(Validator::isNotNull).collect(Collectors.joining(","));
	}

	private List<AssetCategory> filterAssetCategoriesByParentCategory(List<AssetCategory> assetCategories, long parentCategoryId) {
		return assetCategories.stream().filter(a -> a.getParentCategoryId() == parentCategoryId).collect(Collectors.toList());
	}
}
