package com.placecube.digitalplace.payment.govuk.service.commerce;

public final class GovUKCommerceConstants {

	public static final String CANCEL_PAYMENT_RESULT_CODE = "P0030";

	public static final String ENTRY_KEY_GOVUK_PAY_COMMERCE_PAYMENT_METHOD_CONFIGURATION = "govuk-pay-configuration";

	public static final String PAYMENT_ENGINE_KEY = "payment-govuk";

	public static final String SERVICE_NAME = "com.liferay.commerce.payment.engine.method." + PAYMENT_ENGINE_KEY;

	public static final String SERVLET_PATH = "payment-govuk";

	public static final String STATUS_SERVLET_PATH = "payment-govuk-status";

	private GovUKCommerceConstants() {
	}
}
