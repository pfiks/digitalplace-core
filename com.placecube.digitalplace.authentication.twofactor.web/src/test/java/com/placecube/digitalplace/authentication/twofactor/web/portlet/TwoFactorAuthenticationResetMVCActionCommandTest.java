package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, SessionMessages.class })
public class TwoFactorAuthenticationResetMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private LiferayPortletResponse mockLiferayPortletResponse;

	@Mock
	private LiferayPortletURL mockLiferayPortletURL;

	@Mock
	private Portal mockPortal;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	@InjectMocks
	private TwoFactorAuthenticationResetMVCActionCommand twoFactorAuthenticationResetMVCActionCommand;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class, SessionMessages.class);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenResetsTheTwoFactorAuthenticationForTheUser() throws Exception {
		long selectedUserId = 123;
		long companyId = 456;
		when(ParamUtil.getLong(mockActionRequest, "selectedUserId")).thenReturn(selectedUserId);
		when(mockPortal.getCompanyId(mockActionRequest)).thenReturn(companyId);
		when(mockPortal.getLiferayPortletResponse(mockActionResponse)).thenReturn(mockLiferayPortletResponse);
		when(mockLiferayPortletResponse.createLiferayPortletURL(PortletKeys.USERS_ADMIN, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);

		twoFactorAuthenticationResetMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockUserTwoFactorAuthenticationLocalService, times(1)).resetUserTwoFactorAuthentication(companyId, selectedUserId);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenSendsAredirectToTheUsersAdminPortlet() throws Exception {
		long selectedUserId = 123;
		long companyId = 456;
		String redirect = "redirectValue";
		when(ParamUtil.getLong(mockActionRequest, "selectedUserId")).thenReturn(selectedUserId);
		when(mockPortal.getCompanyId(mockActionRequest)).thenReturn(companyId);
		when(mockPortal.getLiferayPortletResponse(mockActionResponse)).thenReturn(mockLiferayPortletResponse);
		when(mockLiferayPortletResponse.createLiferayPortletURL(PortletKeys.USERS_ADMIN, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);
		when(mockLiferayPortletURL.toString()).thenReturn(redirect);

		twoFactorAuthenticationResetMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockActionResponse, times(1)).sendRedirect(redirect);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenAddsSuccessMessageToHttpServletRequest() throws Exception {
		final String successMessage = "request-processed-sucessfully";

		when(ParamUtil.getString(mockActionRequest, "successMessage")).thenReturn(successMessage);
		when(mockPortal.getHttpServletRequest(mockActionRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getLiferayPortletResponse(mockActionResponse)).thenReturn(mockLiferayPortletResponse);
		when(mockLiferayPortletResponse.createLiferayPortletURL(PortletKeys.USERS_ADMIN, PortletRequest.RENDER_PHASE)).thenReturn(mockLiferayPortletURL);

		twoFactorAuthenticationResetMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionMessages.class);

		SessionMessages.add(mockHttpServletRequest, "requestProcessed", successMessage);
	}
}
