package com.placecube.digitalplace.user.account.web.service;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.ListType;
import com.liferay.portal.kernel.model.ListTypeConstants;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ListTypeLocalService;
import com.liferay.portal.kernel.service.PhoneLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.user.account.constants.PhoneType;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountPhoneService;

@Component(immediate = true, service = AccountPhoneService.class)
public class AccountPhoneServiceImpl implements AccountPhoneService {

	@Reference
	private ListTypeLocalService listTypeLocalService;

	@Reference
	private PhoneLocalService phoneLocalService;

	@Override
	public void addPhone(User user, String number, PhoneType type, ServiceContext serviceContext) throws PortalException {
		List<ListType> listTypes = listTypeLocalService.getListTypes(serviceContext.getCompanyId(), ListTypeConstants.CONTACT_PHONE);
		Optional<ListType> listType = listTypes.stream().filter(w -> w.getName().equals(type.getType())).findFirst();

		if (listType.isPresent()) {
			Optional<Phone> existingPhoneOpt = getFirstPhoneOfType(user.getPhones(), type);
			if (existingPhoneOpt.isPresent()) {
				Phone phone = existingPhoneOpt.get();
				phone.setNumber(number);
				phoneLocalService.updatePhone(phone);
			} else if (Validator.isNotNull(number)) {
				phoneLocalService.addPhone(user.getUserId(), Contact.class.getName(), user.getContactId(), number, StringPool.BLANK, listType.get().getListTypeId(), false, serviceContext);
			}

		} else {
			throw new PortalException("Unable to save phone. Unknown type " + type.getType());
		}

	}

	@Override
	public Optional<Phone> getFirstPhoneOfType(List<Phone> phones, PhoneType type) throws PortalException {

		if (!ListUtil.isEmpty(phones)) {
			for (Phone phone : phones) {
				if (phone.getListType().getName().equals(type.getType())) {
					return Optional.of(phone);
				}
			}
		}

		return Optional.empty();
	}

	@Override
	public void populateContextWithExistingPhones(AccountContext accountContext, User user) throws PortalException {
		List<Phone> phones = user.getPhones();
		getFirstPhoneOfType(phones, PhoneType.BUSINESS).ifPresent(p -> accountContext.setBusinessPhoneNumber(p.getNumber()));
		getFirstPhoneOfType(phones, PhoneType.PERSONAL).ifPresent(p -> accountContext.setHomePhoneNumber(p.getNumber()));
		getFirstPhoneOfType(phones, PhoneType.MOBILE).ifPresent(p -> accountContext.setMobilePhoneNumber(p.getNumber()));
	}
}
