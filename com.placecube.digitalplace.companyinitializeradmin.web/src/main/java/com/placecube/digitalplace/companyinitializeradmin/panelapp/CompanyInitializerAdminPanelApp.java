package com.placecube.digitalplace.companyinitializeradmin.panelapp;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.application.list.constants.PanelCategoryKeys;
import com.liferay.portal.kernel.model.Portlet;
import com.placecube.digitalplace.companyinitializeradmin.constants.PortletKeys;

@Component(immediate = true, property = { "panel.app.order:Integer=100", "panel.category.key=" + PanelCategoryKeys.CONTROL_PANEL_CONFIGURATION }, service = PanelApp.class)
public class CompanyInitializerAdminPanelApp extends BasePanelApp {

	@Reference(target = "(javax.portlet.name=" + PortletKeys.COMPANY_INITIALIZER_ADMIN + ")", unbind = "-")
	private Portlet portlet;

	@Override
	public Portlet getPortlet() {
		return portlet;
	}

	@Override
	public String getPortletId() {
		return PortletKeys.COMPANY_INITIALIZER_ADMIN;
	}

}