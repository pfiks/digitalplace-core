package com.placecube.digitalplace.user.account.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SessionErrors.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ConfirmationPageMVCRenderCommandTest extends PowerMockito {

	@InjectMocks
	private ConfirmationPageMVCRenderCommand confirmationPageMVCRenderCommand;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Before
	public void activateSetUp() {
		mockStatic(SessionErrors.class);
	}

	@Test
	public void render_WhenGeneratePasswordWhenLoggedInUserCreateAccountFalse_ThenNotAddToRequestAttribiuteAddressEmialUserCreatedToSendResetPassword() throws UserAccountException, PortletException {
		mockSessionErrorFound(false);
		long companyId = 322L;
		String emailAddress = "EmailAddress";
		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(companyId);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContextService.isGeneratePasswordWhenLoggedInUserCreateAccount(mockAccountContext)).thenReturn(false);
		when(mockAccountContext.getEmailAddress()).thenReturn(emailAddress);

		confirmationPageMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(RequestKeys.USER_EMAIL_ADDRESS_TO_SEND_RESET_PASSWORD), anyString());
	}

	@Test
	public void render_WhenGeneratePasswordWhenLoggedInUserCreateAccountIsTrue_ThenAddsToRequestAttribiuteAddressEmialUserCreatedToSendResetPassword() throws UserAccountException, PortletException {
		mockSessionErrorFound(false);
		long companyId = 322L;
		String emailAddress = "EmailAddress";
		when(mockPortal.getCompanyId(mockRenderRequest)).thenReturn(companyId);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContextService.isGeneratePasswordWhenLoggedInUserCreateAccount(mockAccountContext)).thenReturn(true);
		when(mockAccountContext.getEmailAddress()).thenReturn(emailAddress);

		confirmationPageMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.USER_EMAIL_ADDRESS_TO_SEND_RESET_PASSWORD, emailAddress);
	}

	@Test
	public void render_WhenNoError_ThenAddsTheRedirectUrlAttribute() throws UserAccountException, PortletException {
		String redirectURL = "SOME_URL";

		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);
		when(mockAccountContextService.generateProfileRedirectUrl(mockAccountContext)).thenReturn(redirectURL);

		confirmationPageMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.REDIRECT_URL, redirectURL);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoError_ThenClearsTheContext(boolean sessionErrorFound) throws UserAccountException, PortletException {
		mockSessionErrorFound(sessionErrorFound);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);

		confirmationPageMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAccountContextService, times(1)).clearContext(mockRenderRequest);
	}

	@Test
	@Parameters({ "true", "false" })
	public void render_WhenNoError_ThenReturnsConfirmationPageJSP(boolean sessionErrorFound) throws UserAccountException, PortletException {
		mockSessionErrorFound(sessionErrorFound);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);

		String result = confirmationPageMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/confirmation-page.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetsEmailVerificationAttributeInRequest() throws UserAccountException, PortletException {
		final boolean emailVerificationEnabled = true;

		when(mockAccountConfigurationService.getEmailVerificationEnabled(mockAccountContext)).thenReturn(emailVerificationEnabled);
		mockSessionErrorFound(false);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);

		confirmationPageMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.EMAIL_VERIFICATION_ENABLED, emailVerificationEnabled);
	}

	@Test
	public void render_WhenNoErrorAndSessionErrorIsFound_ThenAddsTheRequestAttributeAsTrue() throws UserAccountException, PortletException {
		mockSessionErrorFound(true);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);

		confirmationPageMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_CREATED_WITH_ERRORS, true);
	}

	@Test
	public void render_WhenNoErrorAndSessionErrorIsNotFound_ThenAddsTheRequestAttributeAsFalse() throws UserAccountException, PortletException {
		mockSessionErrorFound(false);
		when(mockAccountContextService.getOrCreateContext(mockRenderRequest)).thenReturn(mockAccountContext);

		confirmationPageMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(RequestKeys.ACCOUNT_CREATED_WITH_ERRORS, false);
	}

	private void mockSessionErrorFound(boolean sessionErrorFound) {
		when(SessionErrors.contains(mockRenderRequest, RequestKeys.ACCOUNT_CREATED_WITH_ERRORS)).thenReturn(sessionErrorFound);
	}
}
