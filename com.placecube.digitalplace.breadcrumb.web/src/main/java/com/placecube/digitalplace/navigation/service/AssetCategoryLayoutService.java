package com.placecube.digitalplace.navigation.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.entry.rel.model.AssetEntryAssetCategoryRel;
import com.liferay.asset.entry.rel.service.AssetEntryAssetCategoryRelLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = AssetCategoryLayoutService.class)
public class AssetCategoryLayoutService {

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private AssetEntryAssetCategoryRelLocalService assetEntryAssetCategoryRelLocalService;

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private ClassNameLocalService classNameLocalService;

	public Optional<Layout> getLayoutForCategory(AssetCategory assetCategory, long scopeGroupId) {
		long classNameId = classNameLocalService.getClassNameId(Layout.class);

		DynamicQuery categoryRelQuery = assetEntryAssetCategoryRelLocalService.dynamicQuery();
		categoryRelQuery.add(RestrictionsFactoryUtil.eq("assetCategoryId", assetCategory.getCategoryId()));
		List<AssetEntryAssetCategoryRel> assetEntryAssetCategoryRelItems = assetEntryAssetCategoryRelLocalService.dynamicQuery(categoryRelQuery);
		List<Long> assetEntryIds = assetEntryAssetCategoryRelItems.stream().map(AssetEntryAssetCategoryRel::getAssetEntryId).collect(Collectors.toList());

		if (!assetEntryIds.isEmpty()) {
			DynamicQuery layoutAssetEntryQuery = assetEntryLocalService.dynamicQuery();
			layoutAssetEntryQuery.add(RestrictionsFactoryUtil.eq("classNameId", classNameId));
			layoutAssetEntryQuery.add(RestrictionsFactoryUtil.eq("groupId", scopeGroupId));
			layoutAssetEntryQuery.add(RestrictionsFactoryUtil.in("entryId", assetEntryIds));
			List<AssetEntry> assetLayoutEntries = assetEntryLocalService.dynamicQuery(layoutAssetEntryQuery);

			for (AssetEntry assetLayoutEntry : assetLayoutEntries) {
				Layout layout = layoutLocalService.fetchLayout(assetLayoutEntry.getClassPK());
				if (Validator.isNotNull(layout) && !layout.isSystem()) {
					return Optional.ofNullable(layout);
				}
			}
		}

		return Optional.empty();

	}
}
