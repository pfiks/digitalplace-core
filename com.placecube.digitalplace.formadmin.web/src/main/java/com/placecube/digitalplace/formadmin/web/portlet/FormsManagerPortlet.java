package com.placecube.digitalplace.formadmin.web.portlet;

import java.io.IOException;
import java.util.Locale;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.formadmin.web.constants.FormsManagerTabs;
import com.placecube.digitalplace.formadmin.web.constants.PortletKeys;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.tabs.FormsManagerNavigationProvider;

@Component(immediate = true, property = { //
		"com.liferay.portlet.add-default-resource=true", //
		"com.liferay.portlet.display-category=category.hidden", //
		"com.liferay.portlet.layout-cacheable=true", //
		"com.liferay.portlet.private-request-attributes=false", //
		"com.liferay.portlet.private-session-attributes=false", //
		"com.liferay.portlet.render-weight=50", //
		"com.liferay.portlet.use-default-template=true", //
		"com.liferay.portlet.footer-portlet-javascript=/js/data-consumer.js", //
		"com.liferay.portlet.header-portlet-css=/css/main.css", //
		"javax.portlet.expiration-cache=0", //
		"javax.portlet.init-param.template-path=/", //
		"javax.portlet.name=" + PortletKeys.FORMS_MANAGER, //
		"javax.portlet.resource-bundle=content.Language", //
		"javax.portlet.security-role-ref=power-user,user", //
		"javax.portlet.supports.mime-type=text/html", //
		"javax.portlet.version=3.0"//
}, service = Portlet.class)
public class FormsManagerPortlet extends MVCPortlet {

	@Reference
	private FormsManagerNavigationProvider formsManagerNavigationProvider;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

		String currentTab = ParamUtil.getString(renderRequest, PortletRequestKeys.CURRENT_TAB, FormsManagerTabs.FORMS.getNavigationKey());

		PortletURL portletUrl = renderResponse.createRenderURL();

		Locale locale = renderRequest.getLocale();

		renderRequest.setAttribute(PortletRequestKeys.NAVIGATION_ITEMS, formsManagerNavigationProvider.getViewNavigationItems(portletUrl, currentTab, locale));

		super.render(renderRequest, renderResponse);
	}
}