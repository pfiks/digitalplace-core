package com.placecube.digitalplace.override.language;

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.language.UTF8Control;

@Component(property = { "language.id=en_GB" }, service = ResourceBundle.class)
public class LanguageOverrideEnGb extends ResourceBundle {

	private final ResourceBundle resourceBundle = ResourceBundle.getBundle("content.Language_en_GB", UTF8Control.INSTANCE);

	@Override
	public Enumeration<String> getKeys() {
		return resourceBundle.getKeys();
	}

	@Override
	protected Object handleGetObject(String key) {
		return resourceBundle.getObject(key);
	}

}