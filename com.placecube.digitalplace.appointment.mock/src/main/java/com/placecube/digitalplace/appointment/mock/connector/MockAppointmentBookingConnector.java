package com.placecube.digitalplace.appointment.mock.connector;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.security.RandomUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.appointment.booking.connector.AppointmentBookingConnector;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.mock.connector.configuration.MockConnectorCompanyConfiguration;

@Component(immediate = true, service = AppointmentBookingConnector.class)
public class MockAppointmentBookingConnector implements AppointmentBookingConnector {

	private static final Log LOG = LogFactoryUtil.getLog(MockAppointmentBookingConnector.class);

	private static AppointmentBookingEntry createAppointment(Instant now, String suffix, int minusStartDays, int plusEndDays) {
		AppointmentBookingEntry appointmentEntry = new AppointmentBookingEntry();
		appointmentEntry.setTitle("Appointment title " + suffix);
		appointmentEntry.setDescription("Appointment description " + suffix);
		appointmentEntry.setStartDate(Date.from(now.minus(minusStartDays, ChronoUnit.DAYS)));
		appointmentEntry.setEndDate(Date.from(now.plus(plusEndDays, ChronoUnit.DAYS)));
		appointmentEntry.setTimeZone(TimeZone.getDefault());
		return appointmentEntry;
	}

	private Map<String, List<AppointmentBookingEntry>> appointmentServiceIdMap = new HashMap<>();

	private Map<String, LocalDateTime> appointmentServiceIdMapAge = new HashMap<>();

	@Reference
	private ConfigurationProvider configurationProvider;

	@Override
	public List<String> bookAppointment(long companyId, String serviceId, AppointmentBookingEntry appointmentEntry) {
		LOG.debug("Booking mock appointment");
		List<String> results = new ArrayList<>();
		String newId = String.valueOf(RandomUtil.nextInt(1000));
		results.add(newId);
		appointmentEntry.setAppointmentId(newId);
		List<AppointmentBookingEntry> appointments = getAppointments(serviceId);
		appointments.removeIf(a -> a.getStartDate().equals(appointmentEntry.getStartDate()) && a.getEndDate().equals(appointmentEntry.getEndDate()));
		appointments.add(appointmentEntry);
		return results;
	}

	@Override
	public List<String> cancelAppointment(long companyId, String serviceId, String appointmentId) {
		LOG.debug("Cancel mock appointment");
		List<String> results = new ArrayList<>();
		List<AppointmentBookingEntry> appointments = getAppointments(serviceId);
		Optional<AppointmentBookingEntry> appointmentToCancelOpt = appointments.stream().filter(a -> Validator.isNotNull(a.getAppointmentId()) && a.getAppointmentId().equals(appointmentId)).findAny();

		appointmentToCancelOpt.ifPresent(appointmentToCancel -> {
			appointments.remove(appointmentToCancel);
			appointmentToCancel.setAppointmentId("");
			appointments.add(appointmentToCancel);
		});

		results.add("appointmentId");
		return results;
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, AppointmentContext appointmentContext) {
		return getAppointments(serviceId).stream().filter(a -> Validator.isNotNull(a.getAppointmentId())).collect(Collectors.toList());
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, Date startDate, Date endDate, TimeZone timeZone) {
		LOG.debug("Get mock appointments");
		return getAppointments(serviceId);
	}

	@Override
	public List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, String appointmentId) {
		List<AppointmentBookingEntry> appointments = getAppointments(serviceId);
		return appointments.stream().filter(a -> Validator.isNotNull(a.getAppointmentId()) && a.getAppointmentId().equals(appointmentId)).collect(Collectors.toList());
	}

	@Override
	public List<AppointmentBookingSlot> getAppointmentSlots(long companyId, String serviceId, AppointmentContext appointmentContext) {
		List<AppointmentBookingSlot> results = new LinkedList<>();

		long minStartMillis = appointmentContext.getStartDate().orElse(new Date()).getTime();
		long maxEndMillis = appointmentContext.getEndDate().orElse(new Date()).getTime();

		long startTimestamp = ThreadLocalRandom.current().nextLong(minStartMillis, maxEndMillis);
		long endTimestamp = ThreadLocalRandom.current().nextLong(startTimestamp, maxEndMillis);
		AppointmentBookingSlot slot = new AppointmentBookingSlot(LocalDateTime.ofInstant(new Date(startTimestamp).toInstant(), ZoneId.systemDefault()),
				LocalDateTime.ofInstant(new Date(endTimestamp).toInstant(), ZoneId.systemDefault()), 5);
		slot.setAppointmentsAlreadyBooked(getAppointments(serviceId));
		results.add(slot);

		startTimestamp = ThreadLocalRandom.current().nextLong(minStartMillis, maxEndMillis);
		endTimestamp = ThreadLocalRandom.current().nextLong(startTimestamp, maxEndMillis);
		results.add(new AppointmentBookingSlot(LocalDateTime.ofInstant(new Date(startTimestamp).toInstant(), ZoneId.systemDefault()),
				LocalDateTime.ofInstant(new Date(endTimestamp).toInstant(), ZoneId.systemDefault()), 2));

		return results;
	}

	@Override
	public List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int numberOfDatesToReturn, int dayOfWeek, TimeZone timeZone,
			int onlyDatesWithLessThanXappoinments) {

		LOG.debug("Get mock appointments for numberOfDatesToReturn: " + numberOfDatesToReturn);

		List<Date> results = new LinkedList<>();

		Instant instant = Instant.now();

		while (results.size() < numberOfDatesToReturn) {
			instant = instant.plus(7, ChronoUnit.DAYS);
			results.add(Date.from(instant));
		}
		return results;
	}

	@Override
	public List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int dayOfWeek, TimeZone timeZone) {
		LOG.debug("Get mock appointments for numberOfDatesToReturn fixed to default 10");

		List<Date> results = new LinkedList<>();

		Instant instant = Instant.now();

		while (results.size() < 10) {
			instant = instant.plus(7, ChronoUnit.DAYS);
			results.add(Date.from(instant));
		}
		return results;
	}

	@Override
	public boolean isEnabled(long companyId) {
		return isConfigurationEnabled(companyId);
	}

	private List<AppointmentBookingEntry> createAppointments() {

		List<AppointmentBookingEntry> appointments = new ArrayList<>();
		Instant now = Instant.now().truncatedTo(ChronoUnit.MINUTES);

		appointments.add(createAppointment(now, "1", 0, 2));
		appointments.add(createAppointment(now, "2", -1, 3));
		appointments.add(createAppointment(now, "3", -2, 4));
		appointments.add(createAppointment(now, "4", -3, 3));
		appointments.add(createAppointment(now, "5", -4, 5));
		appointments.add(createAppointment(now, "6", -5, 6));
		appointments.add(createAppointment(now, "7", -6, 8));
		appointments.add(createAppointment(now, "8", -7, 10));
		appointments.add(createAppointment(now, "9", -8, 10));
		appointments.add(createAppointment(now, "10", -9, 11));

		return appointments;
	}

	private List<AppointmentBookingEntry> getAppointments(String serviceId) {
		List<AppointmentBookingEntry> appointments = appointmentServiceIdMap.getOrDefault(serviceId, new ArrayList<>());
		LocalDateTime ageOfAppointments = appointmentServiceIdMapAge.get(serviceId);

		if (appointments == null || ageOfAppointments == null || ageOfAppointments.isBefore(LocalDateTime.now().minusDays(1))) {
			appointments = createAppointments();
			appointmentServiceIdMap.put(serviceId, appointments);
			appointmentServiceIdMapAge.put(serviceId, LocalDateTime.now());
		}

		return appointments;
	}

	private boolean isConfigurationEnabled(long companyId) {
		try {
			return configurationProvider.getCompanyConfiguration(MockConnectorCompanyConfiguration.class, companyId).enabled();
		} catch (ConfigurationException e) {
			LOG.warn("Unable to check if mock connector is enabled ", e);
			return false;
		}
	}
}