package com.placecube.digitalplace.group.restadmin.web.service;

import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.group.rest.model.GroupWebhook;
import com.placecube.digitalplace.group.rest.service.GroupWebhookLocalService;
import com.placecube.digitalplace.group.restadmin.web.model.WebhookFormModel;

@Component(immediate = true, service = GroupWebhookManagementService.class)
public class GroupWebhookManagementService {

	@Reference
	private GroupWebhookLocalService groupWebhookLocalService;

	public SearchContainer<GroupWebhook> getSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse) {
		PortletURL iteratorURL = renderResponse.createRenderURL();

		SearchContainer<GroupWebhook> searchContainer = new SearchContainer<>(renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM, SearchContainer.DEFAULT_DELTA, iteratorURL, null,
				"no-entries-were-found");
		searchContainer.setId("groupWebhooks");
		return searchContainer;
	}

	public WebhookFormModel getWebhookFormModel(long webhookId) throws PortletException {
		try {
			GroupWebhook webhook = groupWebhookLocalService.getGroupWebhook(webhookId);
			return new WebhookFormModel(webhook);
		} catch (PortalException e) {
			throw new PortletException(e);
		}
	}

	public WebhookFormModel getWebhookFormModel(String webhookClassName) {
		return new WebhookFormModel(webhookClassName);
	}

}
