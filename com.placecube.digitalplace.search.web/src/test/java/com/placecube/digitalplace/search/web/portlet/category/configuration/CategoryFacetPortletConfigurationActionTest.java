package com.placecube.digitalplace.search.web.portlet.category.configuration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.portlet.category.service.CategoryLocalService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ DefaultConfigurationAction.class, ParamUtil.class, StringUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class CategoryFacetPortletConfigurationActionTest extends PowerMockito {

	@InjectMocks
	private CategoryFacetPortletConfigurationAction categoryFacetPortletConfigurationAction;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CategoryFacetPortletInstanceConfiguration mockCategoryFacetPortletInstanceConfiguration;

	@Mock
	private CategoryLocalService mockCategoryLocalService;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private List<Long> mockDisplayCategoryIds;

	@Mock
	private List<Long> mockDisplayVocabularyIds;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private PortletConfig mockPortletConfig;

	@Mock
	private List<Long> mockPreselectedCategoryIds;

	@Mock
	private List<Long> mockPreselectedVocabularyIds;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class, StringUtil.class);
	}

	@Test
	public void include_WhenInternalPortletIdHasBeenSaved_ThenSetsTheInternalPortletIdAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));

		String internalPortletId = "dfhsrv";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.internalPortletId()).thenReturn(internalPortletId);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, internalPortletId);
	}

	@Test
	public void include_WhenNoError_ThenSetsAlphaSortAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		boolean alphaSort = true;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.alphaSort()).thenReturn(alphaSort);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.ALPHA_SORT, alphaSort);
	}

	@Test
	public void include_WhenNoError_ThenSetsCategoriesQueryIsAndedAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		boolean categoriesQueryIsAnded = true;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.categoriesQueryAnded()).thenReturn(categoriesQueryIsAnded);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.CATEGORIES_QUERY_ANDED, categoriesQueryIsAnded);
	}

	@Test
	public void include_WhenNoError_ThenSetsCheckboxesPanelCollapsedByDefaultAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		boolean checkboxesPanelCollapsedByDefault = true;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.checkboxesPanelCollapsedByDefault()).thenReturn(checkboxesPanelCollapsedByDefault);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.CHECKBOXES_PANEL_COLLAPSED_BY_DEFAULT, checkboxesPanelCollapsedByDefault);
	}

	@Test
	public void include_WhenNoError_ThenSetsInvisibleAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		boolean invisible = true;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.invisible()).thenReturn(invisible);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.INVISIBLE, invisible);
	}

	@Test
	public void include_WhenNoError_ThenSetsShowAllCategoriesAlwaysAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		boolean showAllCategoriesAlways = true;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.showAllCategoriesAlways()).thenReturn(showAllCategoriesAlways);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.SHOW_ALL_CATEGORIES_ALWAYS, showAllCategoriesAlways);
	}

	@Test
	public void include_WhenNoError_ThenSetsShowHeadingAsFirstDropdownOptionAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		boolean showHeadingAsFirstDropdownOption = true;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.showHeadingAsFirstDropdownOption()).thenReturn(showHeadingAsFirstDropdownOption);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION, showHeadingAsFirstDropdownOption);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheCustomCssClassAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String expected = "cssClass";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.customCssClass()).thenReturn(expected);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.CUSTOM_CSS_CLASS, expected);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheCustomHeadingAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String expected = "heading";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.customHeading()).thenReturn(expected);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.CUSTOM_HEADING, expected);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheDisplayCategoryIdsAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String[] expected = { "1" };
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.displayCategoryIds()).thenReturn(expected);
		when(mockCategoryLocalService.getLongValueList(expected)).thenReturn(mockDisplayCategoryIds);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.DISPLAY_CATEGORY_IDS, mockDisplayCategoryIds);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheDisplayStyleAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));

		String displayStyle = "displayStyle";

		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.displayStyle()).thenReturn(displayStyle);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.DISPLAY_STYLE, displayStyle);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheDisplayVocabularyIdsAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String[] expected = { "2" };
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.displayVocabularyIds()).thenReturn(expected);
		when(mockCategoryLocalService.getLongValueList(expected)).thenReturn(mockDisplayVocabularyIds);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.DISPLAY_VOCABOLARY_ID, mockDisplayVocabularyIds);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheFilterColourAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String expected = "filterColourValue";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.filterColour()).thenReturn(expected);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.FILTER_COLOUR, expected);
	}

	@Test
	public void include_WhenNoError_ThenSetsTheMaxCategoriesToShowAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		int expected = 34;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.maxCategoriesToShow()).thenReturn(expected);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.MAX_CATEGORIES_TO_SHOW, expected);
	}

	@Test
	public void include_WhenNoError_ThenSetsThePreselectedCategoryIdsAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String[] expected = { "3" };
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.preselectedCategoryIds()).thenReturn(expected);
		when(mockCategoryLocalService.getLongValueList(expected)).thenReturn(mockPreselectedCategoryIds);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.PRESELECTED_CATEGORY_IDS, mockPreselectedCategoryIds);
	}

	@Test
	public void include_WhenNoError_ThenSetsThePreselectedVocabularyIdsAsRequestAttribute() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "include"));
		String[] expected = { "4" };
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);

		when(mockConfigurationProvider.getPortletInstanceConfiguration(CategoryFacetPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockCategoryFacetPortletInstanceConfiguration);
		when(mockCategoryFacetPortletInstanceConfiguration.preselectedVocabularyIds()).thenReturn(expected);
		when(mockCategoryLocalService.getLongValueList(expected)).thenReturn(mockPreselectedVocabularyIds);

		categoryFacetPortletConfigurationAction.include(mockPortletConfig, mockHttpServletRequest, mockHttpServletResponse);

		verify(mockHttpServletRequest, times(1)).setAttribute(CategoryFacetPortletConfigurationConstants.PRESELECTED_VOCABOLARY_ID, mockPreselectedVocabularyIds);
	}

	@Test
	public void processAction_WhenInternalPortletIdIsEmpty_ThenGeneratesAndSavesTheInternalPortletIdOfLengthSix() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String internalPortletId = "8jkgsjdf";

		when(ParamUtil.getString(mockActionRequest, CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK)).thenReturn(StringPool.BLANK);
		when(StringUtil.randomString(6)).thenReturn(internalPortletId);

		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, internalPortletId);
	}

	@Test
	public void processAction_WhenInternalPortletIdIsNotEmpty_ThenSavesTheInternalPortletId() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String internalPortletId = "8jkgsjdf";
		when(ParamUtil.getString(mockActionRequest, CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, StringPool.BLANK)).thenReturn(internalPortletId);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.INTERNAL_PORTLET_ID, internalPortletId);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesAlphaSortPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		boolean expected = true;
		when(ParamUtil.getBoolean(mockActionRequest, CategoryFacetPortletConfigurationConstants.ALPHA_SORT, false)).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.ALPHA_SORT, String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesCategoryQueryAnded() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		boolean categoriesQueryAnded = true;
		when(ParamUtil.getBoolean(mockActionRequest, CategoryFacetPortletConfigurationConstants.CATEGORIES_QUERY_ANDED)).thenReturn(categoriesQueryAnded);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.CATEGORIES_QUERY_ANDED,
				String.valueOf(categoriesQueryAnded));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesCustomCssClassPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String expected = "css-class";
		when(ParamUtil.getString(mockActionRequest, CategoryFacetPortletConfigurationConstants.CUSTOM_CSS_CLASS, StringPool.BLANK)).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.CUSTOM_CSS_CLASS, expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesCustomHeadingPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String expected = "heading";
		when(ParamUtil.getString(mockActionRequest, CategoryFacetPortletConfigurationConstants.CUSTOM_HEADING, StringPool.BLANK)).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.CUSTOM_HEADING, expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesDisplayCategoryIdsPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String[] expected = { "ID", "ID2" };
		when(ParamUtil.getStringValues(mockActionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_CATEGORY_IDS, new String[0])).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_CATEGORY_IDS, expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesDisplayVocabularyIdsPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String[] expected = { "ID", "ID2" };
		when(ParamUtil.getStringValues(mockActionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_VOCABOLARY_ID, new String[0])).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_VOCABOLARY_ID, expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesFilterColourToShowPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String expected = "colorVal";
		when(ParamUtil.getString(mockActionRequest, CategoryFacetPortletConfigurationConstants.FILTER_COLOUR, StringPool.BLANK)).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.FILTER_COLOUR, expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesInvisiblePreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		boolean expected = true;
		when(ParamUtil.getBoolean(mockActionRequest, CategoryFacetPortletConfigurationConstants.INVISIBLE)).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.INVISIBLE, String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesMaxCategoriesToShowPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		int expected = 34;
		when(ParamUtil.getInteger(mockActionRequest, CategoryFacetPortletConfigurationConstants.MAX_CATEGORIES_TO_SHOW)).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.MAX_CATEGORIES_TO_SHOW, String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesPreselectedCategoryIdsPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String[] expected = { "ID", "ID2" };
		when(ParamUtil.getStringValues(mockActionRequest, CategoryFacetPortletConfigurationConstants.PRESELECTED_CATEGORY_IDS, new String[0])).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.PRESELECTED_CATEGORY_IDS, expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesPreselectedVocabularyIdsPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String[] expected = { "ID", "ID2" };
		when(ParamUtil.getStringValues(mockActionRequest, CategoryFacetPortletConfigurationConstants.PRESELECTED_VOCABOLARY_ID, new String[0])).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.PRESELECTED_VOCABOLARY_ID, expected);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesTheCheckboxesPanelCollapsedByDefaultPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		boolean expected = true;
		when(ParamUtil.getBoolean(mockActionRequest, CategoryFacetPortletConfigurationConstants.CHECKBOXES_PANEL_COLLAPSED_BY_DEFAULT)).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.CHECKBOXES_PANEL_COLLAPSED_BY_DEFAULT,
				String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesTheDisplayStyle() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		String displayStyle = "displayStyle";
		when(ParamUtil.getString(mockActionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_STYLE, StringPool.BLANK)).thenReturn(displayStyle);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.DISPLAY_STYLE, displayStyle);
	}

	@Test
	public void processAction_WhenNoError_ThenSavesTheShowAllCategoriesAlwaysPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		boolean expected = true;
		when(ParamUtil.getBoolean(mockActionRequest, CategoryFacetPortletConfigurationConstants.SHOW_ALL_CATEGORIES_ALWAYS)).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.SHOW_ALL_CATEGORIES_ALWAYS, String.valueOf(expected));
	}

	@Test
	public void processAction_WhenNoError_ThenSavesTheShowHeadingAsFirstDropdownOptionPreference() throws Exception {
		suppress(methods(DefaultConfigurationAction.class, "processAction"));
		boolean expected = true;
		when(ParamUtil.getBoolean(mockActionRequest, CategoryFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION)).thenReturn(expected);
		categoryFacetPortletConfigurationAction = spy(new CategoryFacetPortletConfigurationAction());

		categoryFacetPortletConfigurationAction.processAction(mockPortletConfig, mockActionRequest, mockActionResponse);

		verify(categoryFacetPortletConfigurationAction, times(1)).setPreference(mockActionRequest, CategoryFacetPortletConfigurationConstants.SHOW_HEADING_AS_FIRST_DROPDOWN_OPTION,
				String.valueOf(expected));
	}

}
