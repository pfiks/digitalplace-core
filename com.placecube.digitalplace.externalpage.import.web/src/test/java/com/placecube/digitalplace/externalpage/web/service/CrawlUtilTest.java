package com.placecube.digitalplace.externalpage.web.service;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.TagNodeVisitor;
import org.htmlcleaner.XPatherException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.externalpage.web.cleaner.HtmlCleanerFactory;

import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.url.WebURL;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ HtmlCleanerFactory.class, PropsUtil.class, ParamUtil.class })
public class CrawlUtilTest extends PowerMockito {

	private static final String TEST_CONTENT = "<p>content</p>";
	private static final String TEST_XPATH = "//node";

	@Mock
	private CleanerProperties mockCleanerProperties;

	@Mock
	private CrawlController mockCrawlController;

	@Mock
	private HtmlCleaner mockHtmlCleaner;

	@Mock
	private Page mockPage;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private TagNode mockSubNodeOne;

	@Mock
	private TagNode mockSubNodeTwo;

	@Mock
	private TagNode mockTagNode;

	@Mock
	private TagNodeVisitor mockTagNodeVisitor;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private WebURL mockWebURL;

	@Test
	public void extractContent_WhenNoErrors_ThenRemovesExtraSpaces() {
		final String text = " test  content ";
		final String trimmedText = "test content";

		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockTagNode.getText()).thenReturn(text);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);

		assertThat(CrawlUtil.extractContent(TEST_CONTENT, StringPool.BLANK), equalTo(trimmedText));
	}

	@Test
	public void extractContent_WhenNoErrors_ThenSetsCharsetAndUnicodeProperties() {
		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);

		CrawlUtil.extractContent(TEST_CONTENT, StringPool.BLANK);

		verify(mockCleanerProperties, times(1)).setCharset("utf-8");
	}

	@Test
	public void extractContent_WhenNoErrors_ThenSetsOmitCommentsProperty() {
		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);

		CrawlUtil.extractContent(TEST_CONTENT, StringPool.BLANK);

		verify(mockCleanerProperties, times(1)).setOmitComments(true);
	}

	@Test
	public void extractContent_WhenNoErrors_ThenSetsPruneScriptAndStyleTagsProperty() {
		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);

		CrawlUtil.extractContent(TEST_CONTENT, StringPool.BLANK);

		verify(mockCleanerProperties, times(1)).setPruneTags("script,style");
	}

	@Test
	public void extractContent_WhenXpathIsEmpty_ThenAddsLeadingSpaceToContentNodes_AndGetsNodeText_InOrder() throws Exception {
		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(HtmlCleanerFactory.createLeadingSpaceTagNodeVisitor()).thenReturn(mockTagNodeVisitor);

		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);

		InOrder inOrder = inOrder(mockTagNode);

		CrawlUtil.extractContent(TEST_CONTENT, StringPool.BLANK);

		inOrder.verify(mockTagNode, times(1)).traverse(mockTagNodeVisitor);
		inOrder.verify(mockTagNode, times(1)).getText();
	}

	@Test
	public void extractContent_WhenXpathIsEmpty_ThenReturnsTopMostNodeText() {
		final String text = "content";

		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockTagNode.getText()).thenReturn(text);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);

		assertThat(CrawlUtil.extractContent(TEST_CONTENT, StringPool.BLANK), equalTo(text));
	}

	@Test
	public void extractContent_WhenXpathIsNotEmpty_AndResultsAreTagNodes_ThenReturnsTagNodesTextConcatenated() throws Exception {
		final String subNodeOneText = "text 1";
		final String subNodeTwoText = "text 2";

		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockSubNodeOne.getText()).thenReturn(subNodeOneText);
		when(mockSubNodeTwo.getText()).thenReturn(subNodeTwoText);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);
		when(mockTagNode.evaluateXPath(TEST_XPATH)).thenReturn(new TagNode[] { mockSubNodeOne, mockSubNodeTwo });

		assertThat(CrawlUtil.extractContent(TEST_CONTENT, TEST_XPATH), equalTo(subNodeOneText + subNodeTwoText));
	}

	@Test
	public void extractContent_WhenXpathIsNotEmpty_AndResultsContainObjects_ThenReturnsTagNodesAndObjectsToStringConcatenated() throws Exception {
		final String subNodeOneText = "text 1";
		final String subNodeTwoText = "text 2";

		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockSubNodeOne.getText()).thenReturn(subNodeOneText);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);
		when(mockTagNode.evaluateXPath(TEST_XPATH)).thenReturn(new Object[] { mockSubNodeOne, subNodeTwoText });

		assertThat(CrawlUtil.extractContent(TEST_CONTENT, TEST_XPATH), equalTo(subNodeOneText + subNodeTwoText));
	}

	@Test
	public void extractContent_WhenXpathIsNotEmpty_AndXpathResolutionFails_ThenReturnsEmptyString() throws Exception {
		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);
		when(mockTagNode.evaluateXPath(TEST_XPATH)).thenThrow(new XPatherException());

		assertThat(CrawlUtil.extractContent(TEST_CONTENT, TEST_XPATH), equalTo(StringPool.BLANK));
	}

	@Test
	public void extractContent_WhenXpathIsNotEmpty_ThenAddsLeadingSpaceToContentNodes_AndEvaluatesXpathInOrder() throws Exception {
		when(HtmlCleanerFactory.createHtmlCleaner()).thenReturn(mockHtmlCleaner);
		when(HtmlCleanerFactory.createLeadingSpaceTagNodeVisitor()).thenReturn(mockTagNodeVisitor);

		when(mockHtmlCleaner.clean(TEST_CONTENT)).thenReturn(mockTagNode);
		when(mockHtmlCleaner.getProperties()).thenReturn(mockCleanerProperties);
		when(mockTagNode.evaluateXPath(TEST_XPATH)).thenReturn(new Object[] { mockSubNodeOne });

		InOrder inOrder = inOrder(mockTagNode);

		CrawlUtil.extractContent(TEST_CONTENT, TEST_XPATH);

		inOrder.verify(mockTagNode, times(1)).traverse(mockTagNodeVisitor);
		inOrder.verify(mockTagNode, times(1)).evaluateXPath(TEST_XPATH);
	}

	@Test
	public void generateCustomData_WhenNoError_ThenReturnsTheCustomDataContainingTheCompanyIdGroupIdUserIdXpathAndIndexContentFlag() {
		long companyId = 123;
		long groupId = 456;
		long userId = 789;
		String indexContent[] = new String[] { "1" };
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);
		when(mockThemeDisplay.getUserId()).thenReturn(userId);
		when(ParamUtil.getString(mockPortletRequest, "xpath")).thenReturn(TEST_XPATH);
		when(ParamUtil.getStringValues(mockPortletRequest, "indexContent")).thenReturn(indexContent);

		String result = CrawlUtil.generateCustomData(mockPortletRequest);

		assertThat(result, equalTo("companyId:" + companyId + ";groupId:" + groupId + ";userId:" + userId + ";xpath:" + TEST_XPATH + ";indexContent:" + indexContent[0] + ";"));
	}

	@Test
	public void generateCustomDataWithParams_WhenNoError_ThenReturnsTheCustomDataContainingTheCompanyIdGroupIdUserIdXpathAndIndexContentFlag() {
		long companyId = 123;
		long groupId = 456;
		long userId = 789;
		String xPath = "xPathValues";
		String indexContent[] = new String[] { "1" };

		String result = CrawlUtil.generateCustomData(xPath, indexContent[0], companyId, groupId, userId);

		assertThat(result, equalTo("companyId:" + companyId + ";groupId:" + groupId + ";userId:" + userId + ";xpath:" + xPath + ";indexContent:" + indexContent[0] + ";"));
	}

	@Test
	public void getBooleanFromCustomData_WhenValueIsFound_ThenReturnsValueForSpecifiedPrefix() throws PortalException {
		final String dataPrefix = "key:";
		final boolean dataValue = true;
		List<String> customData = new ArrayList<>();
		customData.add(dataPrefix + String.valueOf(dataValue));

		boolean result = CrawlUtil.getBooleanFromCustomData(customData, dataPrefix);

		assertThat(result, equalTo(dataValue));
	}

	@Test(expected = PortalException.class)
	public void getBooleanFromCustomData_WhenValueIsNotFound_ThenThrowsPortalException() throws PortalException {
		List<String> customData = new ArrayList<>();
		customData.add("differentKey:true");

		CrawlUtil.getBooleanFromCustomData(customData, "key");
	}

	@Test
	public void getCustomData_WhenNoError_ThenReturnsTheCustomDataItemsSplitBySemicolon() {
		String customData = "valueOne:1;valueTwo:2;value3:3";
		when(mockCrawlController.getCustomData()).thenReturn(customData);

		List<String> results = CrawlUtil.getCustomData(mockCrawlController);

		assertThat(results, containsInAnyOrder("valueOne:1", "valueTwo:2", "value3:3"));
	}

	@Test
	@Parameters({ "http://mydomain.com", "http://mydomain.com/", "https://mydomain.com", "https://mydomain.com/", "http://mydomain.com/anotherPath/123?and=test&two=another",
			"https://mydomain.com/anotherPath/123?and=test&two=another" })
	public void getDomain_WhenNoError_ThenReturnsTheDomain(String url) throws URISyntaxException {
		String result = CrawlUtil.getDomain(url);

		assertThat(result, equalTo("mydomain.com"));
	}

	@Test(expected = URISyntaxException.class)
	public void getDomain_WhenUrlIsInvalid_ThenThrowsURISyntaxException() throws URISyntaxException {
		CrawlUtil.getDomain("http://");
	}

	@Test(expected = PortalException.class)
	public void getIdFromCustomData_WhenNoValueFoundForTheGivenPrefix_ThenThrowsPortalException() throws PortalException {
		String valuePrefix = "value4";
		List<String> customData = new ArrayList<>();
		customData.add("valueOne:1");
		customData.add("valueTwo:2");
		customData.add("value3:3");

		CrawlUtil.getIdFromCustomData(customData, valuePrefix);
	}

	@Test
	public void getIdFromCustomData_WhenValueFound_ThenReturnsTheValueForThePrefix() throws PortalException {
		String valuePrefix = "valueTwo:";
		List<String> customData = new ArrayList<>();
		customData.add("valueOne:1");
		customData.add("valueTwo:2");
		customData.add("value3:3");

		long result = CrawlUtil.getIdFromCustomData(customData, valuePrefix);

		assertThat(result, equalTo(2l));
	}

	@Test
	public void getPageURL_WhenNoError_ThenReturnsThePageURL() {
		String expected = "expectedValue";
		when(mockPage.getWebURL()).thenReturn(mockWebURL);
		when(mockWebURL.getURL()).thenReturn(expected);

		String result = CrawlUtil.getPageURL(mockPage);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getStringFromCustomData_WhenValueIsFound_ThenReturnsValueForSpecifiedPrefix() throws PortalException {
		final String dataPrefix = "key:";
		final String dataValue = "value";
		List<String> customData = new ArrayList<>();
		customData.add(dataPrefix + dataValue);

		String result = CrawlUtil.getStringFromCustomData(customData, dataPrefix);

		assertThat(result, equalTo(dataValue));
	}

	@Test(expected = PortalException.class)
	public void getStringFromCustomData_WhenValueIsNotFound_ThenThrowsPortalException() throws PortalException {
		List<String> customData = new ArrayList<>();
		customData.add("differentKey:value");

		CrawlUtil.getStringFromCustomData(customData, "key");
	}

	@Test
	@Parameters({ "http://www.myurl.com?file.css", "http://www.myurl.com/file.js", "http://www.myurl.com/file.zip", "http://www.myurl.com/file.gz", "https://www.myurl.com/file.mp3",
			"https://www.myurl.com/file.mp4", "https://www.myurl.com/file.gif", "https://www.myurl.com/file.jpg", "https://www.myurl.com/file.pdf", "http://www.myurl.com?one=and&two=and2&file.png" })
	public void isValidURL_WhenURLContainsAnInvalidPattern_ThenReturnsFalse(String url) {
		boolean result = CrawlUtil.isValidURL(url);

		assertFalse(result);
	}

	@Test
	@Parameters({ "http://www.myurl.com", "http://www.myurl.com/", "https://www.myurl.com/", "https://www.myurl.com", "http://www.myurl.com?one=and&two=and2",
			"https://www.myurl.com?one=and&two=and2" })
	public void isValidURL_WhenURLIsValid_ThenReturnsTrue(String url) {
		boolean result = CrawlUtil.isValidURL(url);

		assertTrue(result);
	}

	@Before
	public void setUp() {
		initMocks(this);

		mockStatic(HtmlCleanerFactory.class, PropsUtil.class, ParamUtil.class);
	}

}
