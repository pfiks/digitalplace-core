package com.placecube.digitalplace.user.account.web.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.RenderRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.constants.AccountValidatedField;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.AccountService;
import com.placecube.digitalplace.user.account.service.CaptchaValidationService;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ SessionMessages.class, SessionErrors.class, PortalUtil.class })
public class AccountSecurityMVCActionCommandTest {

	private static final long COMPANY_ID = 123L;

	private static final long GROUP_ID = 456L;

	@InjectMocks
	private AccountSecurityMVCActionCommand accountSecurityMVCActionCommand;

	@Mock
	private AccountConfigurationService mockAccountConfigurationService;

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private AccountContextService mockAccountContextService;

	@Mock
	private AccountService mockAccountService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private CaptchaValidationService mockCaptchaValidationService;

	@Mock
	private CreateAccountConfiguration mockCreateAccountConfiguration;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(SessionMessages.class, SessionErrors.class, PortalUtil.class);
	}

	@Test
	public void doProcessAction_WhenCaptchaIsNotShownOnAccountSecurityStep_ThenDoesNotValidatesCaptcha() throws Exception {
		mockBasicDetails(true);

		when(mockAccountContextService.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockActionRequest)).thenReturn(false);

		accountSecurityMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockCaptchaValidationService, never()).validateCaptcha(mockAccountContext, AccountValidatedField.CAPTCHA, mockActionRequest);
	}

	@Test
	public void doProcessAction_WhenCaptchaIsShownOnAccountSecurityStep_ThenClearsPreexistingErrorsAndValidatesCaptchaInOrder() throws Exception {
		mockBasicDetails(true);

		when(mockAccountContextService.isShowCaptchaOnAccountSecurityStep(mockAccountContext, mockActionRequest)).thenReturn(true);

		accountSecurityMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountContext, mockCaptchaValidationService);
		inOrder.verify(mockAccountContext, times(1)).clearErrors();
		inOrder.verify(mockCaptchaValidationService, times(1)).validateCaptcha(mockAccountContext, AccountValidatedField.CAPTCHA, mockActionRequest);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenExceptionCreatingUser_ThenAddsExceptionToSessionErrors(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);
		UserAccountException exception = new UserAccountException();
		doThrow(exception).when(mockAccountService).createUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);

		accountSecurityMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, exception.getClass(), exception);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenExceptionCreatingUser_ThenHidesDefaultErrorMessage(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);
		String portletId = "portletIdValue";
		when(PortalUtil.getPortletId(mockActionRequest)).thenReturn(portletId);
		doThrow(new UserAccountException()).when(mockAccountService).createUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);

		accountSecurityMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verifyStatic(SessionMessages.class, times(1));
		SessionMessages.add(mockActionRequest, portletId + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenExceptionCreatingUser_ThenShowsAccountSecurityPage(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);
		doThrow(new UserAccountException()).when(mockAccountService).createUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);

		accountSecurityMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.ACCOUNT_SECURITY);
	}

	@Test
	public void doProcessAction_WhenNoValidationErrors_ThenSetsConfirmationPageAsRenderCommandName() throws Exception {
		mockBasicDetails(true);

		accountSecurityMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.CONFIRMATION_PAGE);
		verify(mockAccountContextService, times(1)).updateContextInSession(mockAccountContext, mockActionRequest);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoValidationErrors_ThenSetsTheAccountSecurityParametersInContextAndCreatesTheAccountInOrder(boolean addressOutOfLocalArea) throws Exception {
		mockBasicDetails(addressOutOfLocalArea);
		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(addressOutOfLocalArea);

		accountSecurityMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockAccountService, mockAccountContextService);
		inOrder.verify(mockAccountContextService, times(1)).configureAccountSecurity(mockAccountContext, mockActionRequest);
		inOrder.verify(mockAccountService, times(1)).createUserAccount(mockActionRequest, mockAccountContext, addressOutOfLocalArea);
	}

	@Test
	public void doProcessAction_WhenThereAreValidationErrors_ThenDoesNotCreateTheAccount() throws Exception {
		mockBasicDetails(true);
		when(mockAccountContext.hasErrors()).thenReturn(true);

		accountSecurityMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockAccountService, never()).createUserAccount(any(RenderRequest.class), any(AccountContext.class), anyBoolean());
	}

	@Test
	public void doProcessAction_WhenThereAreValidationErrors_ThenShowsAccountSecurityPage() throws Exception {
		mockBasicDetails(true);
		when(mockAccountContext.hasErrors()).thenReturn(true);

		accountSecurityMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.ACCOUNT_SECURITY);
	}

	private void mockBasicDetails(boolean addressOutOfLocalArea) throws UserAccountException {
		when(mockAccountContextService.getOrCreateContext(mockActionRequest)).thenReturn(mockAccountContext);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockAccountContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockAccountContext.getGroupId()).thenReturn(GROUP_ID);
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(mockAccountConfigurationService.getAccountConfiguration(mockAccountContext, mockActionRequest)).thenReturn(mockCreateAccountConfiguration);
		when(mockCreateAccountConfiguration.addressOutOfLocalArea()).thenReturn(addressOutOfLocalArea);
	}

}
