package com.placecube.digitalplace.externalpage.web.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.externalpage.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.externalpage.web.constants.ViewKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class, ExternalPageImportRenderCommandTest.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.util.ParamUtil")
public class ExternalPageImportRenderCommandTest extends PowerMockito {

	@InjectMocks
	private ExternalPageImportRenderCommand externalPageImportRenderCommand;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activeSetup() {
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test
	public void render_WhenNoError_ThenSetShowBackIcon() throws Exception {

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		externalPageImportRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockPortletDisplay, times(1)).setShowBackIcon(true);
	}

	@Test
	public void render_WhenNoError_ThenReturnsCreateExternalPageJSP() throws PortletException {
		String redirect = "/redirect";
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);
		when(ParamUtil.getString(mockRenderRequest, PortletRequestKeys.REDIRECT)).thenReturn(redirect);
		String result = externalPageImportRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(ViewKeys.ADD));
	}

}
