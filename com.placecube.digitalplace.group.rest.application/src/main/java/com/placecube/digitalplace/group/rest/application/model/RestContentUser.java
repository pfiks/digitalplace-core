package com.placecube.digitalplace.group.rest.application.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.ArrayUtil;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class RestContentUser {

	private final long userId;
	private final String emailAddress;
	private final boolean isGroupMember;

	public RestContentUser(String emailAddress) {
		userId = 0;
		this.emailAddress = emailAddress;
		isGroupMember = false;
	}

	public RestContentUser(User user, Group group) {
		userId = user.getUserId();
		emailAddress = user.getEmailAddress();
		isGroupMember = ArrayUtil.contains(user.getGroupIds(), group.getGroupId());
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public long getUserId() {
		return userId;
	}

	public boolean isGroupMember() {
		return isGroupMember;
	}

}
