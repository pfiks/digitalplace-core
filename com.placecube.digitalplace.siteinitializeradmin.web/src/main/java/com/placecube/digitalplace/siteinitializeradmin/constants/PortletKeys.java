package com.placecube.digitalplace.siteinitializeradmin.constants;

public final class PortletKeys {

	public static final String SITE_INITIALIZER_ADMIN = "com_placecube_digitalplace_siteinitializeradmin_SiteInitializerAdminPortlet";

	private PortletKeys() {
		return;
	}
}