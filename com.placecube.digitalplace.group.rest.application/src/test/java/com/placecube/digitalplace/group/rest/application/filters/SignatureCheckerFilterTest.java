package com.placecube.digitalplace.group.rest.application.filters;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import javax.ws.rs.container.ContainerRequestContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.group.rest.application.GroupRestApplication;
import com.placecube.digitalplace.group.rest.application.service.RequestFiltersService;
import com.placecube.digitalplace.group.rest.application.service.SignatureBuilderService;

public class SignatureCheckerFilterTest extends PowerMockito {

	private SignatureCheckerFilter signatureCheckerFilter;

	private static final String APPLICATION_KEY = "applicationKeyValue";
	private static final String DATE_HEADER = "dateHeaderValue";
	private static final String REQUEST_CONTEXT_PATH = "requestContextPathValue";
	private static final String PATH = "pathValue";
	private static final String METHOD = "methodValue";
	private static final String BODY = "bodyValue";
	private static final String SIGNATURE = "signatureValue";
	private static final String EVALUATED_SIGNATURE = "evaluatedSignatureValue";

	@Mock
	private GroupRestApplication mockGroupRestApplication;

	@Mock
	private SignatureBuilderService mockSignatureBuilderService;

	@Mock
	private RequestFiltersService mockRequestFiltersService;

	@Mock
	private ContainerRequestContext mockContainerRequestContext;

	@Before
	public void setUp() throws Exception {
		initMocks(this);

		when(mockGroupRestApplication.getRequestFiltersService()).thenReturn(mockRequestFiltersService);
		when(mockGroupRestApplication.getSignatureBuilderService()).thenReturn(mockSignatureBuilderService);

		signatureCheckerFilter = new SignatureCheckerFilter(mockGroupRestApplication);
	}

	@Test
	public void filter_WhenNoError_ThenValidatesTheSignature() throws Exception {
		when(mockRequestFiltersService.getSignatureHeader(mockContainerRequestContext)).thenReturn(SIGNATURE);
		when(mockRequestFiltersService.getApiKeyHeader(mockContainerRequestContext)).thenReturn(APPLICATION_KEY);
		when(mockRequestFiltersService.getDateHeader(mockContainerRequestContext)).thenReturn(DATE_HEADER);
		when(mockRequestFiltersService.getContextPath(any())).thenReturn(REQUEST_CONTEXT_PATH);
		when(mockRequestFiltersService.getPath(mockContainerRequestContext)).thenReturn(PATH);
		when(mockRequestFiltersService.getMethod(mockContainerRequestContext)).thenReturn(METHOD);
		when(mockRequestFiltersService.getBody(mockContainerRequestContext)).thenReturn(BODY);
		when(mockSignatureBuilderService.getEvaluatedSignature(APPLICATION_KEY, DATE_HEADER, REQUEST_CONTEXT_PATH, PATH, METHOD, BODY)).thenReturn(EVALUATED_SIGNATURE);

		signatureCheckerFilter.filter(mockContainerRequestContext);

		verify(mockSignatureBuilderService, times(1)).validateSignatureMatch(SIGNATURE, EVALUATED_SIGNATURE);
	}

}
