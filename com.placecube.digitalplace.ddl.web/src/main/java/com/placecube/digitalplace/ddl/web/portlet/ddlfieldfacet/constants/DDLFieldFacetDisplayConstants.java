package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.constants;

public class DDLFieldFacetDisplayConstants {

	public static final String FACET_DISPLAY_STYLE_CHECKBOX = "checkbox";

	public static final String FACET_DISPLAY_STYLE_DROPDOWN = "dropdown";

	private DDLFieldFacetDisplayConstants() {

	}
}
