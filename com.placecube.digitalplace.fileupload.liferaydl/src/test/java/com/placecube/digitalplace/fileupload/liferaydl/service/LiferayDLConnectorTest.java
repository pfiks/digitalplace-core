package com.placecube.digitalplace.fileupload.liferaydl.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.portletfilerepository.PortletFileRepositoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.placecube.digitalplace.fileupload.exception.FileUploadException;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ PortletFileRepositoryUtil.class, MimeTypesUtil.class })
public class LiferayDLConnectorTest extends PowerMockito {

	private static final String CHANGE_LOG = null;
	private static final long COMPANY_ID = 1l;
	private static final String DESCRIPTION = StringPool.BLANK;
	private static final String FILE_NAME = "FILE_NAME";
	private static final long FOLDER_ID = 1l;
	private static final String FOLDER_NAME = "FOLDER_NAME";

	private static final long GROUP_ID = 1l;
	private static final String MIME_TYPE = "MIME_TYPE";
	private static final long REPOSITORY_ID = 1l;
	private static final long SIZE = 0;
	private static final String SOURCE_FILE_NAME = "SOURCE_FILE_NAME";
	private static final String TITLE = SOURCE_FILE_NAME;
	private static final long USER_ID = 1l;

	@InjectMocks
	private LiferayDLConnector liferayDLConnector;

	@Mock
	private DLAppLocalService mockDlAppLocalService;

	@Mock
	private FileEntry mockFileEntry;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private LiferayDLService mockLiferayDLService;

	@Mock
	private ServiceContext mockServiceContext;

	@Test
	public void isDefault_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockLiferayDLService.isDefault(COMPANY_ID)).thenThrow(new ConfigurationException());
		boolean result = liferayDLConnector.isDefault(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isDefault_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockLiferayDLService.isDefault(COMPANY_ID)).thenReturn(expected);
		boolean result = liferayDLConnector.isDefault(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void isEnabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockLiferayDLService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());
		boolean result = liferayDLConnector.isEnabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockLiferayDLService.isEnabled(COMPANY_ID)).thenReturn(expected);
		boolean result = liferayDLConnector.isEnabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = FileUploadException.class)
	public void uploadFile_WhenException_ThenThrowFileUploadException() throws PortalException {
		mockServiceContext();
		when(mockLiferayDLService.getFolderId(FOLDER_NAME, mockServiceContext)).thenReturn(FOLDER_ID);
		mockStatic();

		when(mockDlAppLocalService.addFileEntry(null, USER_ID, REPOSITORY_ID, FOLDER_ID, SOURCE_FILE_NAME, MIME_TYPE, TITLE, null, DESCRIPTION, CHANGE_LOG, mockInputStream, SIZE, null, null, null,
				mockServiceContext)).thenThrow(new FileUploadException());

		liferayDLConnector.uploadFile(FILE_NAME, FOLDER_NAME, mockInputStream, mockServiceContext);
	}

	@Test
	public void uploadFile_WhenUploadFileRun_ThenUploadFile() throws PortalException {
		mockServiceContext();
		when(mockLiferayDLService.getFolderId(FOLDER_NAME, mockServiceContext)).thenReturn(FOLDER_ID);
		mockStatic();

		when(mockDlAppLocalService.addFileEntry(null, USER_ID, REPOSITORY_ID, FOLDER_ID, SOURCE_FILE_NAME, MIME_TYPE, TITLE, null, DESCRIPTION, CHANGE_LOG, mockInputStream, SIZE, null, null, null,
				mockServiceContext)).thenReturn(mockFileEntry);

		liferayDLConnector.uploadFile(FILE_NAME, FOLDER_NAME, mockInputStream, mockServiceContext);

		Mockito.verify(mockDlAppLocalService, Mockito.times(1)).addFileEntry(null, USER_ID, REPOSITORY_ID, FOLDER_ID, SOURCE_FILE_NAME, MIME_TYPE, TITLE, null, DESCRIPTION, CHANGE_LOG,
				mockInputStream, SIZE, null, null, null, mockServiceContext);
	}

	private void mockServiceContext() {
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
	}

	private void mockStatic() {

		mockStatic(PortletFileRepositoryUtil.class, MimeTypesUtil.class);
		when(PortletFileRepositoryUtil.getUniqueFileName(GROUP_ID, FOLDER_ID, FILE_NAME)).thenReturn(SOURCE_FILE_NAME);
		when(MimeTypesUtil.getContentType(FILE_NAME)).thenReturn(MIME_TYPE);
	}

}
