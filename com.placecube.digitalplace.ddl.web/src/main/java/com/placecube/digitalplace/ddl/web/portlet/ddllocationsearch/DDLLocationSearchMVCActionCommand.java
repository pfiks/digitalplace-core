package com.placecube.digitalplace.ddl.web.portlet.ddllocationsearch;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.ddl.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.ddl.web.constants.LocationSearchParameters;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@Component(immediate = true, property = { "javax.portlet.name=" + com.placecube.digitalplace.ddl.web.constants.PortletKeys.DDL_LOCATION_SEARCH_PORTLET,
		"mvc.command.name=" + MVCCommandKeys.LOCATION_SEARCH }, service = MVCActionCommand.class)
public class DDLLocationSearchMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String latitude = ParamUtil.getString(actionRequest, LocationSearchParameters.SEARCH_LATITUDE);
		String longitude = ParamUtil.getString(actionRequest, LocationSearchParameters.SEARCH_LONGITUDE);
		String formattedAddress = ParamUtil.getString(actionRequest, LocationSearchParameters.FORMATTED_ADDRESS);
		String searchField = ParamUtil.getString(actionRequest, LocationSearchParameters.GEO_LOCATION_FIELD);

		if (Validator.isNotNull(latitude) && Validator.isNotNull(longitude) && Validator.isNotNull(formattedAddress) && Validator.isNotNull(searchField)) {
			sharedSearchRequestService.clearSharedSearchSession(actionRequest);
			sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, LocationSearchParameters.SEARCH_LATITUDE, latitude);
			sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, LocationSearchParameters.SEARCH_LONGITUDE, longitude);
			sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, LocationSearchParameters.FORMATTED_ADDRESS, formattedAddress);
			sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, LocationSearchParameters.GEO_LOCATION_FIELD, searchField);
		} else {
			sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, LocationSearchParameters.SEARCH_LATITUDE);
			sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, LocationSearchParameters.SEARCH_LONGITUDE);
			sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, LocationSearchParameters.FORMATTED_ADDRESS);
			sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, LocationSearchParameters.GEO_LOCATION_FIELD);
		}

		sharedSearchRequestService.sendRedirectFromParameter(actionRequest, actionResponse, "redirectURL");
	}

}
