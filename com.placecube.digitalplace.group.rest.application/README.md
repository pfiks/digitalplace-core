#com.placecube.digitalplace.group.rest.application

## CXF Endpoints and REST Extender
Due to https://issues.liferay.com/browse/LPS-101642 the CXF Endpoints and REST Extender cannot currently be configured programatically.

## Manual configuration
Manually configure the Liferay CXF endpoints and REST Extender provided by the BasicAuthGroupRestApplication as follows: 
* Go to 'System settings' > 'Web API'
* Select CXF Endpoints and add a new one with the following values:
	* Context Path: /ba.group.rest.application
	* Authentication Verifier Properties: auth.verifier.BasicAuthHeaderAuthVerifier.urls.includes=*
* Select REST Extender and add a new one with the following values:
	* Context Paths: /ba.group.rest.application
	* JAX-RS Application Filters: (component.name=com.placecube.digitalplace.group.rest.application.BasicAuthGroupRestApplication)

Repeat the same procedure for the GroupRestApplication.

APIs that create or update group content (e.g. `GroupContentRestEndpoint.updateDynamicContentAssetEntries(...)`) require 
additional configuration due to them being secured using Liferay's Service Access Policy and role / permissions system.

* Go to Control Panel -> Configuration -> Service Access Policy -> SYSTEM_DEFAULT
* Switch to advanced view
* Add the following entries and save
```
com.liferay.dynamic.data.lists.service.DDLRecordService#addRecord
com.liferay.dynamic.data.lists.service.DDLRecordSetService#getRecordSet
com.liferay.dynamic.data.lists.service.DDLRecordSetService#getRecordSets
com.liferay.document.library.kernel.service.DLAppService#addFileEntry
com.liferay.document.library.kernel.service.DLFileEntryService#getFileEntry
```

### OSGi Server configuration
To configure them on the server, copy the 4 config files into the /osgi/config folder and restart the server.

