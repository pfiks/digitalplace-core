package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.servlet.taglib.clay;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import com.liferay.frontend.taglib.clay.servlet.taglib.VerticalCard;
import com.liferay.frontend.taglib.clay.servlet.taglib.util.LabelItem;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.ResourceBundleUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.util.ArticleDisplayPageUtil;

public class JournalArticleItemSelectorVerticalCard implements VerticalCard {

	private final JournalArticle article;
	private final HttpServletRequest httpServletRequest;
	private final ThemeDisplay themeDisplay;

	public JournalArticleItemSelectorVerticalCard(JournalArticle article1, RenderRequest renderRequest) {

		article = article1;
		httpServletRequest = PortalUtil.getHttpServletRequest(renderRequest);

		themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public String getElementClasses() {
		return "card-interactive card-interactive-secondary";
	}

	@Override
	public String getIcon() {
		return "web-content";
	}

	@Override
	public String getImageSrc() {
		return HtmlUtil.escape(article.getArticleImageURL(themeDisplay));
	}

	@Override
	public List<LabelItem> getLabels() {
		List<LabelItem> labelItems = null;
		LabelItem labelItem = new LabelItem();
		if (!ArticleDisplayPageUtil.articleHasDisplayPageTemplate(article)) {
			ResourceBundle resourceBundle = ResourceBundleUtil.getBundle("content.Language", themeDisplay.getLocale(), getClass());
			labelItem.setLabel(resourceBundle.getString("no-page-template-found"));
			labelItems = new ArrayList<>();
			labelItems.add(labelItem);
		}
		return labelItems;
	}

	@Override
	public String getSubtitle() {
		Date createDate = article.getModifiedDate();

		String modifiedDateDescription = LanguageUtil.getTimeDescription(httpServletRequest, System.currentTimeMillis() - createDate.getTime(), true);

		String subtitle = LanguageUtil.format(httpServletRequest, "modified-x-ago", modifiedDateDescription);

		return subtitle;
	}

	@Override
	public String getTitle() {
		String title = article.getTitle(themeDisplay.getLocale());

		if (Validator.isNotNull(title)) {
			return HtmlUtil.escape(title);
		}

		Locale defaultLanguage = LocaleUtil.fromLanguageId(article.getDefaultLanguageId());

		return HtmlUtil.escape(article.getTitle(defaultLanguage));
	}

	@Override
	public boolean isSelectable() {
		return false;
	}

}