/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the simple address lookup entry service. This utility wraps <code>com.placecube.digitalplace.addresslookup.simple.service.persistence.impl.SimpleAddressLookupEntryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SimpleAddressLookupEntryPersistence
 * @generated
 */
public class SimpleAddressLookupEntryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		getPersistence().clearCache(simpleAddressLookupEntry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, SimpleAddressLookupEntry>
		fetchByPrimaryKeys(Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<SimpleAddressLookupEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<SimpleAddressLookupEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<SimpleAddressLookupEntry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static SimpleAddressLookupEntry update(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		return getPersistence().update(simpleAddressLookupEntry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static SimpleAddressLookupEntry update(
		SimpleAddressLookupEntry simpleAddressLookupEntry,
		ServiceContext serviceContext) {

		return getPersistence().update(
			simpleAddressLookupEntry, serviceContext);
	}

	/**
	 * Returns all the simple address lookup entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the simple address lookup entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry findByUuid_First(
			String uuid,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry fetchByUuid_First(
		String uuid,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry findByUuid_Last(
			String uuid,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry fetchByUuid_Last(
		String uuid,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the simple address lookup entries before and after the current simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the current simple address lookup entry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public static SimpleAddressLookupEntry[] findByUuid_PrevAndNext(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK, String uuid,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByUuid_PrevAndNext(
			simpleAddressLookupEntryPK, uuid, orderByComparator);
	}

	/**
	 * Removes all the simple address lookup entries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of simple address lookup entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching simple address lookup entries
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the simple address lookup entries before and after the current simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the current simple address lookup entry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public static SimpleAddressLookupEntry[] findByUuid_C_PrevAndNext(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK, String uuid,
			long companyId,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByUuid_C_PrevAndNext(
			simpleAddressLookupEntryPK, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the simple address lookup entries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching simple address lookup entries
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @return the matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode) {

		return getPersistence().findByCompanyId_Postcode(companyId, postcode);
	}

	/**
	 * Returns a range of all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode, int start, int end) {

		return getPersistence().findByCompanyId_Postcode(
			companyId, postcode, start, end);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().findByCompanyId_Postcode(
			companyId, postcode, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode, int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByCompanyId_Postcode(
			companyId, postcode, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry findByCompanyId_Postcode_First(
			long companyId, String postcode,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByCompanyId_Postcode_First(
			companyId, postcode, orderByComparator);
	}

	/**
	 * Returns the first simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry fetchByCompanyId_Postcode_First(
		long companyId, String postcode,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().fetchByCompanyId_Postcode_First(
			companyId, postcode, orderByComparator);
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry findByCompanyId_Postcode_Last(
			long companyId, String postcode,
			OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByCompanyId_Postcode_Last(
			companyId, postcode, orderByComparator);
	}

	/**
	 * Returns the last simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public static SimpleAddressLookupEntry fetchByCompanyId_Postcode_Last(
		long companyId, String postcode,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().fetchByCompanyId_Postcode_Last(
			companyId, postcode, orderByComparator);
	}

	/**
	 * Returns the simple address lookup entries before and after the current simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the current simple address lookup entry
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public static SimpleAddressLookupEntry[]
			findByCompanyId_Postcode_PrevAndNext(
				SimpleAddressLookupEntryPK simpleAddressLookupEntryPK,
				long companyId, String postcode,
				OrderByComparator<SimpleAddressLookupEntry> orderByComparator)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByCompanyId_Postcode_PrevAndNext(
			simpleAddressLookupEntryPK, companyId, postcode, orderByComparator);
	}

	/**
	 * Removes all the simple address lookup entries where companyId = &#63; and postcode = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 */
	public static void removeByCompanyId_Postcode(
		long companyId, String postcode) {

		getPersistence().removeByCompanyId_Postcode(companyId, postcode);
	}

	/**
	 * Returns the number of simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @return the number of matching simple address lookup entries
	 */
	public static int countByCompanyId_Postcode(
		long companyId, String postcode) {

		return getPersistence().countByCompanyId_Postcode(companyId, postcode);
	}

	/**
	 * Caches the simple address lookup entry in the entity cache if it is enabled.
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 */
	public static void cacheResult(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		getPersistence().cacheResult(simpleAddressLookupEntry);
	}

	/**
	 * Caches the simple address lookup entries in the entity cache if it is enabled.
	 *
	 * @param simpleAddressLookupEntries the simple address lookup entries
	 */
	public static void cacheResult(
		List<SimpleAddressLookupEntry> simpleAddressLookupEntries) {

		getPersistence().cacheResult(simpleAddressLookupEntries);
	}

	/**
	 * Creates a new simple address lookup entry with the primary key. Does not add the simple address lookup entry to the database.
	 *
	 * @param simpleAddressLookupEntryPK the primary key for the new simple address lookup entry
	 * @return the new simple address lookup entry
	 */
	public static SimpleAddressLookupEntry create(
		SimpleAddressLookupEntryPK simpleAddressLookupEntryPK) {

		return getPersistence().create(simpleAddressLookupEntryPK);
	}

	/**
	 * Removes the simple address lookup entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public static SimpleAddressLookupEntry remove(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().remove(simpleAddressLookupEntryPK);
	}

	public static SimpleAddressLookupEntry updateImpl(
		SimpleAddressLookupEntry simpleAddressLookupEntry) {

		return getPersistence().updateImpl(simpleAddressLookupEntry);
	}

	/**
	 * Returns the simple address lookup entry with the primary key or throws a <code>NoSuchSimpleAddressLookupEntryException</code> if it could not be found.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public static SimpleAddressLookupEntry findByPrimaryKey(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws com.placecube.digitalplace.addresslookup.simple.exception.
			NoSuchSimpleAddressLookupEntryException {

		return getPersistence().findByPrimaryKey(simpleAddressLookupEntryPK);
	}

	/**
	 * Returns the simple address lookup entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry, or <code>null</code> if a simple address lookup entry with the primary key could not be found
	 */
	public static SimpleAddressLookupEntry fetchByPrimaryKey(
		SimpleAddressLookupEntryPK simpleAddressLookupEntryPK) {

		return getPersistence().fetchByPrimaryKey(simpleAddressLookupEntryPK);
	}

	/**
	 * Returns all the simple address lookup entries.
	 *
	 * @return the simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findAll(
		int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of simple address lookup entries
	 */
	public static List<SimpleAddressLookupEntry> findAll(
		int start, int end,
		OrderByComparator<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the simple address lookup entries from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of simple address lookup entries.
	 *
	 * @return the number of simple address lookup entries
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getCompoundPKColumnNames() {
		return getPersistence().getCompoundPKColumnNames();
	}

	public static SimpleAddressLookupEntryPersistence getPersistence() {
		return _persistence;
	}

	public static void setPersistence(
		SimpleAddressLookupEntryPersistence persistence) {

		_persistence = persistence;
	}

	private static volatile SimpleAddressLookupEntryPersistence _persistence;

}