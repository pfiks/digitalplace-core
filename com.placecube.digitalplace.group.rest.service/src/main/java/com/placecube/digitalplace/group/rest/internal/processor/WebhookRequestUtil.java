package com.placecube.digitalplace.group.rest.internal.processor;

import java.io.IOException;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.servlet.HttpMethods;
import com.pfiks.common.time.DateFormatter;
import com.placecube.digitalplace.group.rest.constants.WebhookAction;
import com.placecube.digitalplace.group.rest.exception.GroupRestKeyException;
import com.placecube.digitalplace.group.rest.internal.constants.RequestHeaders;
import com.placecube.digitalplace.group.rest.internal.constants.ResponseEntry;
import com.placecube.digitalplace.group.rest.service.GroupApiKeyLocalService;

@Component(immediate = true, service = WebhookRequestUtil.class)
public class WebhookRequestUtil {

	private static final Log LOG = LogFactoryUtil.getLog(WebhookRequestUtil.class);

	@Reference
	private DateFormatter dateFormatter;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private GroupApiKeyLocalService groupApiKeyLocalService;

	public String buildDateValue() {
		return dateFormatter.getFormattedInstant(dateFormatter.getNow());
	}

	public String buildRequestBody(long groupId, long userId, WebhookAction webhookAction, long entryId, String entryName, Date entryDate) {
		JSONObject jsonBody = jsonFactory.createJSONObject();

		jsonBody.put(ResponseEntry.GROUP_ID, groupId);
		jsonBody.put(ResponseEntry.USER_ID, userId);
		jsonBody.put(ResponseEntry.EVENT, webhookAction.name());

		jsonBody.put(ResponseEntry.ENTRY_ID, entryId);
		jsonBody.put(ResponseEntry.ENTRY_NAME, entryName);
		jsonBody.put(ResponseEntry.ENTRY_DATE, dateFormatter.getFormattedInstant(entryDate.toInstant()));

		return jsonBody.toString();
	}

	public String buildSignature(Group group, String applicationKey, String dateHeader, WebhookAction webhookAction, String body, String payloadURL) throws GroupRestKeyException {
		String textToEncode = String.join(StringPool.NEW_LINE, applicationKey, dateHeader, webhookAction.name(), payloadURL, HttpMethods.POST, body);
		return groupApiKeyLocalService.getEncodedTextWithGroupKey(group, textToEncode);
	}

	public HttpEntity buildStringEntity(String body) {
		return new StringEntity(body, ContentType.create(ContentType.APPLICATION_JSON.getMimeType()));
	}

	public void configureEntity(HttpPost httpMethod, HttpEntity entity) {
		httpMethod.setEntity(entity);
	}

	public void configureHeaders(HttpPost httpMethod, String applicationKey, String dateHeader, WebhookAction webhookAction, String signature) {
		httpMethod.setHeader(HTTP.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
		httpMethod.addHeader(RequestHeaders.ACCEPT, ContentType.APPLICATION_JSON.getMimeType());
		httpMethod.addHeader(RequestHeaders.X_API_KEY, applicationKey);
		httpMethod.addHeader(RequestHeaders.X_DATE, dateHeader);
		httpMethod.addHeader(RequestHeaders.X_EVENT, webhookAction.name());
		httpMethod.addHeader(RequestHeaders.X_SIGNATURE, signature);
	}

	public HttpPost getHttpPost(String payloadURL) {
		return new HttpPost(payloadURL);
	}

	public void executePostCall(HttpPost httpPost) throws IOException {
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		try {
			client = HttpClients.createDefault();
			response = client.execute(httpPost);
			LOG.debug("Response: " + response.getStatusLine().toString());
		} finally {
			HttpClientUtils.closeQuietly(client);
			HttpClientUtils.closeQuietly(response);
		}
	}

}
