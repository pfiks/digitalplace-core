package com.placecube.digitalplace.form.extender.internal;

import java.util.HashMap;
import java.util.Map;

import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.form.model.FormDefinition;

@Component(immediate = true, service = RegistrationService.class)
public class RegistrationService {

	private final Map<String, ServiceRegistration<FormDefinition>> serviceRegistrations = new HashMap<>();

	public void registerService(FormDefinition formDefinition, ServiceRegistration<FormDefinition> serviceRegistration) {

		serviceRegistrations.put(formDefinition.getKey(), serviceRegistration);
	}

	public void unregisterService(FormDefinition formDefinition) {
		serviceRegistrations.get(formDefinition.getKey()).unregister();
		serviceRegistrations.remove(formDefinition.getKey());

	}

}
