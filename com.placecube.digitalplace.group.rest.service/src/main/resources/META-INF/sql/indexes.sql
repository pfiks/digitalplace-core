create index IX_718CC87E on Placecube_GroupRest_GroupApiKey (applicationKey[$COLUMN_LENGTH:2000000$]);
create index IX_BAFCC15 on Placecube_GroupRest_GroupApiKey (applicationSecret[$COLUMN_LENGTH:2000000$]);

create index IX_CBFB14C4 on Placecube_GroupRest_GroupWebhook (groupId, enabled, className[$COLUMN_LENGTH:400$], classPK);