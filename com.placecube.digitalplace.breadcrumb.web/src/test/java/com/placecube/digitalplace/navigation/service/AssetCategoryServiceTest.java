package com.placecube.digitalplace.navigation.service;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.category.service.CategoryRetrievalService;

@PrepareForTest({ GetterUtil.class })
@RunWith(PowerMockRunner.class)
public class AssetCategoryServiceTest extends PowerMockito {

	private static final long CATEGORY_ID_1 = 1L;

	private static final long CATEGORY_ID_2 = 2L;

	private static final long CATEGORY_ID_3 = 3L;

	private static final long GROUP_ID = 9L;

	private static final long VOCABULARY_ID_1 = 4L;

	private static final long VOCABULARY_ID_2 = 5L;

	private static final long VOCABULARY_ID_3 = 6L;

	private static final String URL_CATEGORY_PROPERTY_NAME = "urlCategoryPropertyName";

	private static final String BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME = "buildCategoriesHierarchyUrlPropertyName";

	@InjectMocks
	private AssetCategoryService assetCategoryService;

	@Mock
	private AssetCategoryLayoutService mockAssetCategoryLayoutService;

	@Mock
	private CategoryRetrievalService mockCategoryRetrievalService;

	@Mock
	private AssetCategory mockAssetCategory1;

	@Mock
	private AssetCategory mockAssetCategory2;

	@Mock
	private AssetCategory mockAssetCategory3;

	@Mock
	private AssetCategory mockAssetCategory4;

	@Mock
	private AssetCategory mockAssetCategory5;

	@Mock
	private AssetCategory mockAssetCategory6;

	@Mock
	private AssetEntry mockAssetEntry;

	@Mock
	private Layout mockLayout;

	@Before
	public void setUp() {
		initMocks(this);

		mockStatic(GetterUtil.class);

		when(mockAssetCategory1.getCategoryId()).thenReturn(CATEGORY_ID_1);
		when(mockAssetCategory2.getCategoryId()).thenReturn(CATEGORY_ID_2);
		when(mockAssetCategory3.getCategoryId()).thenReturn(CATEGORY_ID_3);

		when(mockAssetCategory1.getVocabularyId()).thenReturn(VOCABULARY_ID_1);
		when(mockAssetCategory2.getVocabularyId()).thenReturn(VOCABULARY_ID_2);
		when(mockAssetCategory3.getVocabularyId()).thenReturn(VOCABULARY_ID_3);
		when(mockAssetCategory4.getVocabularyId()).thenReturn(VOCABULARY_ID_1);
		when(mockAssetCategory5.getVocabularyId()).thenReturn(VOCABULARY_ID_2);
		when(mockAssetCategory6.getVocabularyId()).thenReturn(VOCABULARY_ID_3);
	}

	@Test
	public void getAncestors_WhenExceptionGettingAncestors_ThenReturnsEmptyList() throws PortalException {

		when(mockAssetCategory1.getAncestors()).thenThrow(new PortalException());

		List<AssetCategory> ancestors = assetCategoryService.getAncestors(mockAssetCategory1);

		assertThat(ancestors, empty());

	}

	@Test
	public void getAncestors_WhenNoExceptionGettingAncestors_ThenReturnsAncestors() throws PortalException {

		List<AssetCategory> mockAncestors = getMockCategories(mockAssetCategory2);
		when(mockAssetCategory1.getAncestors()).thenReturn(mockAncestors);

		List<AssetCategory> ancestors = assetCategoryService.getAncestors(mockAssetCategory1);

		assertThat(ancestors, contains(mockAssetCategory2));

	}

	@Test
	public void getCategories_WhenHasCategoriesWithMatchingVocabularyId_ThenReturnsCategoryMatches() {

		List<AssetCategory> mockCategories = getMockCategories(mockAssetCategory1, mockAssetCategory2, mockAssetCategory3);
		when(mockAssetEntry.getCategories()).thenReturn(mockCategories);

		List<AssetCategory> categories = assetCategoryService.getCategories(mockAssetEntry, VOCABULARY_ID_1);

		assertThat(categories, contains(mockAssetCategory1));

	}

	@Test
	public void getCategories_WhenHasCategoriesButNoMatchingVocabularyId_ThenReturnsEmptyList() {

		int nonMatchingFilterVocabularyId = 12345;
		List<AssetCategory> mockCategories = getMockCategories(mockAssetCategory1, mockAssetCategory2, mockAssetCategory3);
		when(mockAssetEntry.getCategories()).thenReturn(mockCategories);

		List<AssetCategory> categories = assetCategoryService.getCategories(mockAssetEntry, nonMatchingFilterVocabularyId);

		assertThat(categories, empty());

	}

	@Test
	public void getCategories_WhenNoHasCategoriesFound_ThenReturnsEmptyList() {

		List<AssetCategory> mockCategories = Collections.emptyList();
		when(mockAssetEntry.getCategories()).thenReturn(mockCategories);

		List<AssetCategory> categories = assetCategoryService.getCategories(mockAssetEntry, VOCABULARY_ID_1);

		assertThat(categories, empty());

	}

	@Test
	public void getHierarchicalCategories_WhenCategoriesFromVocabularyFound_ThenReturnsHierarchicalCategories() throws PortalException {

		List<AssetCategory> mockCategories = new ArrayList<>();
		mockCategories.add(mockAssetCategory1);
		when(mockAssetCategory1.getVocabularyId()).thenReturn(VOCABULARY_ID_1);
		mockCategories.add(mockAssetCategory2);
		mockCategories.add(mockAssetCategory3);

		List<AssetCategory> mockCategoryAncestors = new ArrayList<>();
		mockCategoryAncestors.add(mockAssetCategory4);

		when(mockAssetCategory1.getAncestors()).thenReturn(mockCategoryAncestors);

		List<List<AssetCategory>> hierarchicalCategories = assetCategoryService.getHierarchicalCategories(mockCategories, VOCABULARY_ID_1);

		assertThat(hierarchicalCategories.size(), equalTo(1));
		assertThat(hierarchicalCategories.get(0), contains(mockAssetCategory4, mockAssetCategory1));
	}

	@Test
	public void getHierarchicalCategories_WhenCategoriesFromVocabularyFound_ThenReturnsHierarchicalCategoriesOrderedFromRootCategory() throws PortalException {

		List<AssetCategory> mockCategories = new ArrayList<>();
		mockCategories.add(mockAssetCategory1);
		when(mockAssetCategory1.getVocabularyId()).thenReturn(VOCABULARY_ID_1);

		List<AssetCategory> mockCategoryAncestors = new ArrayList<>();
		mockCategoryAncestors.add(mockAssetCategory4);
		mockCategoryAncestors.add(mockAssetCategory3);
		mockCategoryAncestors.add(mockAssetCategory2);

		when(mockAssetCategory1.getAncestors()).thenReturn(mockCategoryAncestors);

		List<List<AssetCategory>> hierarchicalCategories = assetCategoryService.getHierarchicalCategories(mockCategories, VOCABULARY_ID_1);

		assertThat(hierarchicalCategories.size(), equalTo(1));
		assertThat(hierarchicalCategories.get(0), contains(mockAssetCategory2, mockAssetCategory3, mockAssetCategory4, mockAssetCategory1));
	}

	@Test
	public void getHierarchicalCategories_WhenCategoryHasNotAncestors_ThenReturnsHierarchicalCategoriesWithCategory() throws PortalException {

		List<AssetCategory> mockCategories = new ArrayList<>();
		mockCategories.add(mockAssetCategory1);
		when(mockAssetCategory1.getVocabularyId()).thenReturn(VOCABULARY_ID_1);

		List<AssetCategory> mockCategoryAncestors = new ArrayList<>();

		when(mockAssetCategory1.getAncestors()).thenReturn(mockCategoryAncestors);

		List<List<AssetCategory>> hierarchicalCategories = assetCategoryService.getHierarchicalCategories(mockCategories, VOCABULARY_ID_1);

		assertThat(hierarchicalCategories.size(), equalTo(1));
		assertThat(hierarchicalCategories.get(0), contains(mockAssetCategory1));
	}

	@Test
	public void getHierarchicalCategories_WhenCategoriesFromVocabularyNotFound_ThenReturnsEmptyList() throws PortalException {

		List<AssetCategory> mockCategories = new ArrayList<>();
		mockCategories.add(mockAssetCategory1);
		mockCategories.add(mockAssetCategory2);

		List<List<AssetCategory>> hierarchicalCategories = assetCategoryService.getHierarchicalCategories(mockCategories, VOCABULARY_ID_3);

		assertThat(hierarchicalCategories, empty());

	}

	@Test(expected = PortalException.class)
	public void getHierarchicalCategories_WhenExceptionGettingCategoryAncestors_ThenThrowsException() throws PortalException {

		List<AssetCategory> mockCategories = new ArrayList<>();
		mockCategories.add(mockAssetCategory1);
		mockCategories.add(mockAssetCategory2);

		when(mockAssetCategory1.getAncestors()).thenThrow(new PortalException());

		assetCategoryService.getHierarchicalCategories(mockCategories, VOCABULARY_ID_1);

	}

	@Test
	public void getTheDeepestCategories_WhenAssetCategoriesIsNotEmpty_ThenReturnsTheDeepestCategory() throws PortalException {

		List<AssetCategory> mockCategories = new ArrayList<>();
		mockCategories.add(mockAssetCategory1);
		when(mockAssetCategory1.getAncestors()).thenReturn(Arrays.asList(mockAssetCategory2, mockAssetCategory3));
		mockCategories.add(mockAssetCategory2);
		when(mockAssetCategory2.getAncestors()).thenReturn(Collections.emptyList());

		List<AssetCategory> result = assetCategoryService.getTheDeepestCategories(mockCategories);
		assertThat(result.size(), equalTo(1));
		assertThat(result, contains(mockAssetCategory1));

	}

	@Test
	public void getTheDeepestCategories_WhenAssetCategoriesIsEmpty_ThenReturnsEmptyList() throws PortalException {

		List<AssetCategory> mockCategories = new ArrayList<>();

		List<AssetCategory> result = assetCategoryService.getTheDeepestCategories(mockCategories);
		assertTrue(result.isEmpty());

	}

	@Test
	public void getAssetCategoryUrl_WhenBuildCategoriesHierarchyUrlPropertyIsFalseAndUrlCategoryPropertyIsEmpty_ThenReturnsEmptyString() {
		final String buildCategoriesHiearchyUrlPropertyValue = "buildCategoriesHiearchyUrl";

		mockAssetCategoryProperties(mockAssetCategory1, StringPool.BLANK, false, buildCategoriesHiearchyUrlPropertyValue);

		String result = assetCategoryService.getAssetCategoryUrl(mockAssetCategory1, URL_CATEGORY_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, false, GROUP_ID);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getAssetCategoryUrl_WhenBuildCategoriesHierarchyUrlPropertyIsFalseAndUrlCategoryPropertyIsNull_ThenReturnsEmptyString() {
		final String buildCategoriesHiearchyUrlPropertyValue = "buildCategoriesHiearchyUrl";

		mockAssetCategoryProperties(mockAssetCategory1, null, false, buildCategoriesHiearchyUrlPropertyValue);

		String result = assetCategoryService.getAssetCategoryUrl(mockAssetCategory1, URL_CATEGORY_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, false, GROUP_ID);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getAssetCategoryUrl_WhenBuildCategoriesHierarchyUrlPropertyIsFalseAndUrlCategoryPropertyIsNotEmptyAndUseLayoutUrlIsFalse_ThenReturnsCategoryPropertyUrlWithForwardSlash() {
		final String buildCategoriesHiearchyUrlPropertyValue = "buildCategoriesHiearchyUrl";
		final String categoryUrl = "category-url";
		mockAssetCategoryProperties(mockAssetCategory1, categoryUrl, false, buildCategoriesHiearchyUrlPropertyValue);

		String result = assetCategoryService.getAssetCategoryUrl(mockAssetCategory1, URL_CATEGORY_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, false, GROUP_ID);

		assertThat(result, equalTo(StringPool.FORWARD_SLASH + categoryUrl));
	}

	@Test
	public void getAssetCategoryUrl_WhenBuildCategoriesHierarchyUrlPropertyIsFalseAndUrlCategoryPropertyIsNotEmptyAndUseLayoutUrlIsTrue_ThenReturnsLayoutFriendlyUrl() {
		final String buildCategoriesHiearchyUrlPropertyValue = "buildCategoriesHiearchyUrl";
		final String layoutUrl = "layout-url";
		mockAssetCategoryProperties(mockAssetCategory1, layoutUrl, false, buildCategoriesHiearchyUrlPropertyValue);
		when(mockAssetCategoryLayoutService.getLayoutForCategory(mockAssetCategory1, GROUP_ID)).thenReturn(Optional.of(mockLayout));
		when(mockLayout.getFriendlyURL()).thenReturn(layoutUrl);

		String result = assetCategoryService.getAssetCategoryUrl(mockAssetCategory1, URL_CATEGORY_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, true, GROUP_ID);

		assertThat(result, equalTo(layoutUrl));
	}

	@Test
	public void getAssetCategoryUrl_WhenBuildCategoriesHierarchyUrlPropertyIsFalseAndUrlCategoryPropertyIsNotEmptyAndUseLayoutUrlIsTrueAndFriendlyUrlIsEmpty_ThenReturnsEmpty() {
		final String buildCategoriesHiearchyUrlPropertyValue = "buildCategoriesHiearchyUrl";
		final String layoutUrl = "layout-url";
		mockAssetCategoryProperties(mockAssetCategory1, layoutUrl, false, buildCategoriesHiearchyUrlPropertyValue);
		when(mockAssetCategoryLayoutService.getLayoutForCategory(mockAssetCategory1, GROUP_ID)).thenReturn(Optional.empty());

		String result = assetCategoryService.getAssetCategoryUrl(mockAssetCategory1, URL_CATEGORY_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, true, GROUP_ID);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getAssetCategoryUrl_WhenBuildCategoriesHierarchyUrlPropertyIsTrueAndParentCategoryIsNull_ThenReturnsCategoryUrl() {
		final String buildCategoriesHiearchyUrlPropertyValue = "buildCategoriesHiearchyUrl";
		final String categoryUrl = "category-url";
		mockAssetCategoryProperties(mockAssetCategory1, categoryUrl, true, buildCategoriesHiearchyUrlPropertyValue);

		when(mockAssetCategory1.getParentCategory()).thenReturn(null);

		String result = assetCategoryService.getAssetCategoryUrl(mockAssetCategory1, URL_CATEGORY_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, false, GROUP_ID);

		assertThat(result, equalTo(StringPool.FORWARD_SLASH + categoryUrl));
	}

	@Test
	public void getAssetCategoryUrl_WhenBuildCategoriesHierarchyUrlPropertyIsTrueAndParentCategoryIsNotNull_ThenReturnsCategoryUrlPrecededByParentCategoryUrl() {
		final String buildCategoriesHiearchyUrlPropertyValue1 = "buildCategoriesHiearchyUrl1";
		final String buildCategoriesHiearchyUrlPropertyValue2 = "buildCategoriesHiearchyUrl2";
		final String categoryUrl = "category-url";
		final String parentCategoryUrl = "parent-url";

		mockAssetCategoryProperties(mockAssetCategory1, categoryUrl, true, buildCategoriesHiearchyUrlPropertyValue1);
		mockAssetCategoryProperties(mockAssetCategory2, parentCategoryUrl, false, buildCategoriesHiearchyUrlPropertyValue2);

		when(mockAssetCategory1.getParentCategory()).thenReturn(mockAssetCategory2);

		String result = assetCategoryService.getAssetCategoryUrl(mockAssetCategory1, URL_CATEGORY_PROPERTY_NAME, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME, false, GROUP_ID);

		assertThat(result, equalTo(StringPool.FORWARD_SLASH + parentCategoryUrl + StringPool.FORWARD_SLASH + categoryUrl));
	}

	private List<AssetCategory> getMockCategories(AssetCategory... categories) {

		List<AssetCategory> mockCategories = new ArrayList<>();
		Collections.addAll(mockCategories, categories);

		return mockCategories;

	}

	private void mockAssetCategoryProperties(AssetCategory assetCategory, String categoryUrl, boolean buildCategoriesHiearchyUrl, String buildCategoriesHiearchyUrlPropertyValue) {
		when(mockCategoryRetrievalService.getPropertyValue(assetCategory, URL_CATEGORY_PROPERTY_NAME)).thenReturn(categoryUrl);
		when(mockCategoryRetrievalService.getPropertyValue(assetCategory, BUILD_CATEGORIES_HIERARCHY_URL_PROPERTY_NAME)).thenReturn(buildCategoriesHiearchyUrlPropertyValue);
		when(GetterUtil.getBoolean(buildCategoriesHiearchyUrlPropertyValue)).thenReturn(buildCategoriesHiearchyUrl);
	}

}
