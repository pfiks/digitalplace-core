/**
 * ScpInvokeRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class ScpInvokeRequest  extends com.capita_software_services.www.scp.base.RequestWithCredentials  implements java.io.Serializable {
    private com.capita_software_services.www.scp.base.RequestType requestType;

    private org.apache.axis.types.Token requestId;

    private com.capita_software_services.www.scp.base.Routing routing;

    private com.capita_software_services.www.portal_api.PanEntryMethod panEntryMethod;

    private com.capita_software_services.www.scp.base.AdditionalInstructions additionalInstructions;

    private com.capita_software_services.www.scp.base.BillingDetails billing;

    public ScpInvokeRequest() {
    }

    public ScpInvokeRequest(
           uk.co.capita_software.support.selfservice.Credentials credentials,
           com.capita_software_services.www.scp.base.RequestType requestType,
           org.apache.axis.types.Token requestId,
           com.capita_software_services.www.scp.base.Routing routing,
           com.capita_software_services.www.portal_api.PanEntryMethod panEntryMethod,
           com.capita_software_services.www.scp.base.AdditionalInstructions additionalInstructions,
           com.capita_software_services.www.scp.base.BillingDetails billing) {
        super(
            credentials);
        this.requestType = requestType;
        this.requestId = requestId;
        this.routing = routing;
        this.panEntryMethod = panEntryMethod;
        this.additionalInstructions = additionalInstructions;
        this.billing = billing;
    }


    /**
     * Gets the requestType value for this ScpInvokeRequest.
     *
     * @return requestType
     */
    public com.capita_software_services.www.scp.base.RequestType getRequestType() {
        return requestType;
    }


    /**
     * Sets the requestType value for this ScpInvokeRequest.
     *
     * @param requestType
     */
    public void setRequestType(com.capita_software_services.www.scp.base.RequestType requestType) {
        this.requestType = requestType;
    }


    /**
     * Gets the requestId value for this ScpInvokeRequest.
     *
     * @return requestId
     */
    public org.apache.axis.types.Token getRequestId() {
        return requestId;
    }


    /**
     * Sets the requestId value for this ScpInvokeRequest.
     *
     * @param requestId
     */
    public void setRequestId(org.apache.axis.types.Token requestId) {
        this.requestId = requestId;
    }


    /**
     * Gets the routing value for this ScpInvokeRequest.
     *
     * @return routing
     */
    public com.capita_software_services.www.scp.base.Routing getRouting() {
        return routing;
    }


    /**
     * Sets the routing value for this ScpInvokeRequest.
     *
     * @param routing
     */
    public void setRouting(com.capita_software_services.www.scp.base.Routing routing) {
        this.routing = routing;
    }


    /**
     * Gets the panEntryMethod value for this ScpInvokeRequest.
     *
     * @return panEntryMethod
     */
    public com.capita_software_services.www.portal_api.PanEntryMethod getPanEntryMethod() {
        return panEntryMethod;
    }


    /**
     * Sets the panEntryMethod value for this ScpInvokeRequest.
     *
     * @param panEntryMethod
     */
    public void setPanEntryMethod(com.capita_software_services.www.portal_api.PanEntryMethod panEntryMethod) {
        this.panEntryMethod = panEntryMethod;
    }


    /**
     * Gets the additionalInstructions value for this ScpInvokeRequest.
     *
     * @return additionalInstructions
     */
    public com.capita_software_services.www.scp.base.AdditionalInstructions getAdditionalInstructions() {
        return additionalInstructions;
    }


    /**
     * Sets the additionalInstructions value for this ScpInvokeRequest.
     *
     * @param additionalInstructions
     */
    public void setAdditionalInstructions(com.capita_software_services.www.scp.base.AdditionalInstructions additionalInstructions) {
        this.additionalInstructions = additionalInstructions;
    }


    /**
     * Gets the billing value for this ScpInvokeRequest.
     *
     * @return billing
     */
    public com.capita_software_services.www.scp.base.BillingDetails getBilling() {
        return billing;
    }


    /**
     * Sets the billing value for this ScpInvokeRequest.
     *
     * @param billing
     */
    public void setBilling(com.capita_software_services.www.scp.base.BillingDetails billing) {
        this.billing = billing;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScpInvokeRequest)) return false;
        ScpInvokeRequest other = (ScpInvokeRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.requestType==null && other.getRequestType()==null) ||
             (this.requestType!=null &&
              this.requestType.equals(other.getRequestType()))) &&
            ((this.requestId==null && other.getRequestId()==null) ||
             (this.requestId!=null &&
              this.requestId.equals(other.getRequestId()))) &&
            ((this.routing==null && other.getRouting()==null) ||
             (this.routing!=null &&
              this.routing.equals(other.getRouting()))) &&
            ((this.panEntryMethod==null && other.getPanEntryMethod()==null) ||
             (this.panEntryMethod!=null &&
              this.panEntryMethod.equals(other.getPanEntryMethod()))) &&
            ((this.additionalInstructions==null && other.getAdditionalInstructions()==null) ||
             (this.additionalInstructions!=null &&
              this.additionalInstructions.equals(other.getAdditionalInstructions()))) &&
            ((this.billing==null && other.getBilling()==null) ||
             (this.billing!=null &&
              this.billing.equals(other.getBilling())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRequestType() != null) {
            _hashCode += getRequestType().hashCode();
        }
        if (getRequestId() != null) {
            _hashCode += getRequestId().hashCode();
        }
        if (getRouting() != null) {
            _hashCode += getRouting().hashCode();
        }
        if (getPanEntryMethod() != null) {
            _hashCode += getPanEntryMethod().hashCode();
        }
        if (getAdditionalInstructions() != null) {
            _hashCode += getAdditionalInstructions().hashCode();
        }
        if (getBilling() != null) {
            _hashCode += getBilling().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScpInvokeRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "scpInvokeRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "requestType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "requestType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "requestId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "token"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routing");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "routing"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "routing"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("panEntryMethod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "panEntryMethod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/portal-api", "panEntryMethod"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalInstructions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "additionalInstructions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "additionalInstructions"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billing");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "billing"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "billingDetails"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
