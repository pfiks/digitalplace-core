package com.placecube.digitalplace.fileupload.liferaydl.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.exception.NoSuchFolderException;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.fileupload.liferaydl.configuration.LiferayDLConnectorConfiguration;

@Component(immediate = true, service = LiferayDLService.class)
public class LiferayDLService {

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DLAppLocalService dlAppLocalService;

	public boolean isDefault(long companyId) throws ConfigurationException {
		return getConfiguration(companyId).isDefault();
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		return getConfiguration(companyId).isEnabled();
	}

	public long getFolderId(String path, ServiceContext serviceContext) throws PortalException {

		long folderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;

		if (path.startsWith("/")) {
			path = path.substring(1);
		}

		if (path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}

		String[] folders = path.split("/");

		for (String folderName : folders) {
			Folder folder = getOrCreateFolder(folderId, folderName, serviceContext);
			folderId = folder.getFolderId();
		}

		return folderId;
	}

	private Folder getOrCreateFolder(long parentFolderId, String folderName, ServiceContext serviceContext) throws PortalException {

		Folder folder = null;

		try {
			folder = dlAppLocalService.getFolder(serviceContext.getScopeGroupId(), parentFolderId, folderName);
		} catch (NoSuchFolderException e) {
			folder = dlAppLocalService.addFolder(null, serviceContext.getUserId(), serviceContext.getScopeGroupId(), parentFolderId, folderName, folderName, serviceContext);
		}

		return folder;
	}

	private LiferayDLConnectorConfiguration getConfiguration(long companyId) throws ConfigurationException {
		return configurationProvider.getCompanyConfiguration(LiferayDLConnectorConfiguration.class, companyId);
	}

}
