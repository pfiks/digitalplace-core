package com.placecube.digitalplace.authentication.twofactor.internal.encryption;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base32;
import org.apache.http.client.utils.URIBuilder;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.security.SecureRandom;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = ModelFactoryBuilder.class)
public class ModelFactoryBuilder {

	public String bytesToString(byte[] bytes) {
		if (Validator.isNull(bytes)) {
			throw new IllegalArgumentException();
		}
		return new String(bytes);
	}

	public Base32 getBase32Instance() {
		return new Base32();
	}

	public ByteArrayOutputStream getByteArrayOutputStreamInstance() {
		return new ByteArrayOutputStream();
	}

	public byte[] getBytesFromFile(String path) throws IOException {
		InputStream is = getClass().getClassLoader().getResourceAsStream(path);
		return toByteArray(is);
	}

	public String getFormattedIssuerLabel(String issuer, String accountName) {
		if (issuer.contains(":")) {
			throw new IllegalArgumentException("Issuer cannot contain the \':\' character.");
		}
		return issuer.concat(":").concat(accountName);
	}

	public GCMParameterSpec getGCMParameterSpecInstance(int length, byte[] iv) {
		if (Validator.isNull(iv) || length < 0) {
			throw new IllegalArgumentException();
		}
		return new GCMParameterSpec(length, iv);
	}

	public SecretKeySpec getSecretKeySpecInstance(byte[] optBytes, String algorithm) {
		if (Validator.isNull(optBytes) || Validator.isNull(algorithm)) {
			throw new IllegalArgumentException();
		}
		return new SecretKeySpec(optBytes, algorithm);
	}

	public SecureRandom getSecureRandomInstance() {
		return new SecureRandom();
	}

	public URIBuilder getURIBuilderInstance() {
		return new URIBuilder();
	}

	public byte[] instantiateBytes() {
		return new byte[20];
	}

	public Date now() {
		return new Date();
	}

	private byte[] getBufferInstance(int length) {
		if (length < 0) {
			throw new IllegalArgumentException();
		}
		return new byte[length];
	}

	private byte[] toByteArray(InputStream in) throws IOException {
		if (Validator.isNull(in)) {
			throw new IllegalArgumentException();
		}
		ByteArrayOutputStream os = getByteArrayOutputStreamInstance();
		byte[] buffer = getBufferInstance(1024);
		int len;
		while ((len = in.read(buffer)) != -1) {
			os.write(buffer, 0, len);
		}
		return os.toByteArray();
	}

}
