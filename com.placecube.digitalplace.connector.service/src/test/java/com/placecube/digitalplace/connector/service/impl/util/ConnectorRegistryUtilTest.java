package com.placecube.digitalplace.connector.service.impl.util;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.ServiceReference;

import com.liferay.osgi.service.tracker.collections.map.ServiceReferenceMapper;
import com.liferay.osgi.service.tracker.collections.map.ServiceReferenceMapper.Emitter;
import com.placecube.digitalplace.connector.constants.ConnectorConstants;
import com.placecube.digitalplace.connector.model.Connector;

@RunWith(MockitoJUnitRunner.class)
public class ConnectorRegistryUtilTest {

	private ConnectorRegistryUtil connectorRegistryUtil;

	@Mock
	private Connector mockConnector;

	@Mock
	private Emitter<String> mockEmitter;

	@Mock
	private ServiceReference<Connector> mockServiceReference;

	@Test
	public void getCouncilTaxConnectorServiceReferenceMapper_WhenNoErrors_ThenReturnsServiceReferenceToConnectorTypePropertyMapper() {
		final String connectorType = "ConnectorType";

		when(mockServiceReference.getProperty(ConnectorConstants.CONNECTOR_TYPE)).thenReturn(connectorType);

		ServiceReferenceMapper<String, Connector> serviceReferenceMapper = connectorRegistryUtil.getInterfaceServiceReferenceMapper();

		serviceReferenceMapper.map(mockServiceReference, mockEmitter);

		verify(mockEmitter, times(1)).emit(connectorType);
	}

	@Before
	public void setUp() {
		connectorRegistryUtil = new ConnectorRegistryUtil();
	}

}
