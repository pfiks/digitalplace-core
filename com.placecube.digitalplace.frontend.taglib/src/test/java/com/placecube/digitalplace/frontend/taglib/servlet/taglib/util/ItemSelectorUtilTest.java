package com.placecube.digitalplace.frontend.taglib.servlet.taglib.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import com.liferay.item.selector.ItemSelector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ FrameworkUtil.class })
public class ItemSelectorUtilTest {

	@Mock
	private Bundle mockBundle;

	@Mock
	private BundleContext mockBundleContext;

	@Mock
	private ItemSelector mockItemSelector;

	@Mock
	private ServiceReference<ItemSelector> mockServiceReference;

	@Before
	public void activateSetup() {
		mockStatic(FrameworkUtil.class);

		when(FrameworkUtil.getBundle(ItemSelectorUtil.class)).thenReturn(mockBundle);
		when(mockBundle.getBundleContext()).thenReturn(mockBundleContext);
		when(mockBundleContext.getServiceReference(ItemSelector.class)).thenReturn(mockServiceReference);
		when(mockBundleContext.getService(mockServiceReference)).thenReturn(mockItemSelector);
	}

	@Test
	public void getItemSelector_WhenNoErrors_ThenReturnsItemSelector() {
		ItemSelector result = ItemSelectorUtil.getItemSelector();

		assertThat(result, sameInstance(mockItemSelector));
	}
}
