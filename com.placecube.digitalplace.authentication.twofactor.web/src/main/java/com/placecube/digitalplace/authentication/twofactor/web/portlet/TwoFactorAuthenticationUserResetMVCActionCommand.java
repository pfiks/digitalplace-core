package com.placecube.digitalplace.authentication.twofactor.web.portlet;

import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorPortletKeys;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorRequestUtils;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;

@Component(property = { "javax.portlet.name=" + TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION, "mvc.command.name=" + TwoFactorMVCCommandKeys.USER_RESET }, service = MVCActionCommand.class)
public class TwoFactorAuthenticationUserResetMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private TwoFactorRequestUtils twoFactorRequestUtils;

	@Reference
	private TwoFactorService twoFactorService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		HttpServletRequest originalServletRequest = twoFactorRequestUtils.getOriginalServletRequest(actionRequest);
		Optional<User> twoFactorUser = twoFactorService.getTwoFactorUser(actionRequest, originalServletRequest);

		if (!twoFactorUser.isPresent()) {
			addError(actionRequest, "sessionExpired");

			MutableRenderParameters renderParameters = actionResponse.getRenderParameters();
			renderParameters.setValue("mvcPath", "/two-factor-authentication/error.jsp");
			renderParameters.setValue("mvcRenderCommandName", "/");
		} else {
			String secretKey = ParamUtil.getString(actionRequest, "twoFactorSecretKey");
			String twoFactorAuthCode = ParamUtil.getString(actionRequest, "twoFactorAuthCode");

			User user = twoFactorUser.get();

			boolean verified = twoFactorService.verifyAndEnableTwoFactorAuthentication(user, twoFactorAuthCode, secretKey);

			if (!verified) {
				actionRequest.setAttribute("twoFactorSecretKey", secretKey);

				addError(actionRequest, "twoFactorVerificationFailed");

				MutableRenderParameters renderParameters = actionResponse.getRenderParameters();
				renderParameters.setValue("mvcRenderCommandName", TwoFactorMVCCommandKeys.USER_RESET);
			} else {
				user = twoFactorService.setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail(user, actionRequest);
				twoFactorService.setTwoFactorValidatedUserIdInHttpSession(originalServletRequest, user);
			}
		}
	}

	private void addError(ActionRequest actionRequest, String errorKey) {
		SessionErrors.add(actionRequest, errorKey);
		hideDefaultErrorMessage(actionRequest);
	}

}
