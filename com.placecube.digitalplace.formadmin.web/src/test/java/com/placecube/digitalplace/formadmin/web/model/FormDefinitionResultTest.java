package com.placecube.digitalplace.formadmin.web.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.placecube.digitalplace.form.model.FormDefinition;

public class FormDefinitionResultTest {

	private static boolean INSTALLED = false;

	@Mock
	private FormDefinition mockFormDefinition;

	@Before
	public void setUp() {

		initMocks(this);

	}

	@Test
	public void create_WhenNoError_ThenReturnsFormDefinitionResultCreated() {

		assertThat(FormDefinitionResult.create(mockFormDefinition, INSTALLED), notNullValue());

	}

	@Test
	public void getName_WhenNoError_ThenReturnsName() {

		String name = "name";
		when(mockFormDefinition.getName()).thenReturn(name);

		FormDefinitionResult result = FormDefinitionResult.create(mockFormDefinition, INSTALLED);

		assertThat(result.getName(), equalTo(name));

	}

	@Test
	public void getCategory_WhenNoError_ThenReturnsCategory() {

		String category = "category";
		when(mockFormDefinition.getCategory()).thenReturn(category);

		FormDefinitionResult result = FormDefinitionResult.create(mockFormDefinition, INSTALLED);

		assertThat(result.getCategory(), equalTo(category));

	}

	@Test
	public void getDescription_WhenNoError_ThenReturnsDescription() {

		String description = "description";
		when(mockFormDefinition.getDescription()).thenReturn(description);

		FormDefinitionResult result = FormDefinitionResult.create(mockFormDefinition, INSTALLED);

		assertThat(result.getDescription(), equalTo(description));

	}

	@Test
	public void getKey_WhenNoError_ThenReturnsKey() {

		String key = "key";
		when(mockFormDefinition.getKey()).thenReturn(key);

		FormDefinitionResult result = FormDefinitionResult.create(mockFormDefinition, INSTALLED);

		assertThat(result.getKey(), equalTo(key));

	}

	@Test
	public void isInstalled_WhenNoError_ThenReturnsInstalledFlagValue() {

		FormDefinitionResult result = FormDefinitionResult.create(mockFormDefinition, INSTALLED);

		assertFalse(result.isInstalled());

	}
}
