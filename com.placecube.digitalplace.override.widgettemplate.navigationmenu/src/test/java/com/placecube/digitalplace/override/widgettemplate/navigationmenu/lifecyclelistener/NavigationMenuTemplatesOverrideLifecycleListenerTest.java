package com.placecube.digitalplace.override.widgettemplate.navigationmenu.lifecyclelistener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.placecube.digitalplace.override.widgettemplate.navigationmenu.service.WidgetTemplateService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

public class NavigationMenuTemplatesOverrideLifecycleListenerTest extends PowerMockito {

	@InjectMocks
	private NavigationMenuTemplatesOverrideLifecycleListener navigationMenuTemplatesOverrideLifecycleListener;

	@Mock
	private WidgetTemplateService mockWidgetTemplateService;

	@Mock
	private Company mockCompany;

	@Mock
	private DDMTemplate mockDDMTemplate1;

	@Mock
	private DDMTemplate mockDDMTemplate2;

	@Mock
	private DDMTemplate mockClonedDDMTemplate1;

	@Mock
	private DDMTemplate mockClonedDDMTemplate2;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenExceptionRetrievingTheCompanyGroupId_ThenThrowsPortalException() throws Exception {
		when(mockCompany.getGroupId()).thenThrow(new PortalException());

		navigationMenuTemplatesOverrideLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenRemovesTheItemsNotFoundAlertFromAllTheNavigationTemplatesFound() throws Exception {
		Long groupId = 123l;
		when(mockCompany.getGroupId()).thenReturn(groupId);
		List<DDMTemplate> templates = new ArrayList<>();
		templates.add(mockDDMTemplate1);
		templates.add(mockDDMTemplate2);
		when(mockWidgetTemplateService.getNavigationMenuTemplates(groupId)).thenReturn(templates);
		when(mockDDMTemplate1.clone()).thenReturn(mockClonedDDMTemplate1);
		when(mockDDMTemplate2.clone()).thenReturn(mockClonedDDMTemplate2);

		navigationMenuTemplatesOverrideLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockWidgetTemplateService, times(1)).removeNoItemsFoundAlert(mockClonedDDMTemplate1);
		verify(mockWidgetTemplateService, times(1)).removeNoItemsFoundAlert(mockClonedDDMTemplate2);
	}

}
