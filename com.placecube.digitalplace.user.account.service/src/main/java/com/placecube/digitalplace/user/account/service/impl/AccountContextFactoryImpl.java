package com.placecube.digitalplace.user.account.service.impl;

import org.osgi.service.component.annotations.Component;

import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.model.impl.AccountContextImpl;
import com.placecube.digitalplace.user.account.service.AccountContextFactory;

@Component(service = AccountContextFactory.class)
public class AccountContextFactoryImpl implements AccountContextFactory {

	@Override
	public AccountContext createAccountContext() {
		return new AccountContextImpl();
	}
}
