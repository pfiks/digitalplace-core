package com.placecube.digitalplace.ddl.web.portlet.display.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.dynamic.data.lists.constants.DDLRecordConstants;
import com.liferay.dynamic.data.lists.constants.DDLRecordSetConstants;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.lists.service.DDLRecordService;
import com.liferay.dynamic.data.lists.service.DDLRecordSetLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceContextThreadLocal.class, ServiceContextFactory.class, ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class DDLEditorServiceTest extends PowerMockito {

	private static final String REDIRECT = "redirect";

	@Captor
	private ArgumentCaptor<DDMFormFieldValue> addedDDMFormFieldValueCaptor;

	@InjectMocks
	private DDLEditorService ddlEditorService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private DDLPermissionService mockDDLPermissionService;

	@Mock
	private DDLRecord mockDDLRecord;

	@Mock
	private DDLRecordLocalService mockDDLRecordLocalService;

	@Mock
	private DDLRecordService mockDDLRecordService;

	@Mock
	private DDLRecordSet mockDDLRecordSet1;

	@Mock
	private DDLRecordSet mockDDLRecordSet2;

	@Mock
	private DDLRecordSetLocalService mockDDLRecordSetLocalService;

	@Mock
	private DDMFormFieldValue mockDDMFormFieldValue;

	@Mock
	private DDMFormValues mockDDMFormValues;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private Map<Locale, String> mockDescriptionMap;

	@Mock
	private HttpServletRequest mockHTTPServletRequest;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private JSONObject mockJSONobject;

	@Mock
	private Map<Locale, String> mockNameMap;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private Portal mockPortal;

	@Mock
	private PortletDisplay mockPortletDisplay;

	@Mock
	private LiferayPortletURL mockPortletURL;

	@Mock
	private PortletURLFactory mockPortletURLFactory;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(ServiceContextThreadLocal.class, ServiceContextFactory.class, ParamUtil.class);
	}

	@Test
	public void configureBackLinkURL_WhenNoError_ThenConfiguresBackURLAndAddsRedirectAsRenderRequestAttribute() {
		String url = "url";
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		ddlEditorService.configureBackLinkURL(mockThemeDisplay, url, mockRenderRequest);

		verify(mockPortletDisplay, times(1)).setShowBackIcon(true);
		verify(mockPortletDisplay, times(1)).setURLBack(url);
		verify(mockRenderRequest, times(1)).setAttribute(REDIRECT, url);
	}

	@Test
	public void getAutofillFieldsAsJSONArray_WhenEmptyValues_ThenReturnsEmptyString() {
		String result = ddlEditorService.getFieldsToAutofill(new String[0]);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getAutofillFieldsAsJSONArray_WhenNoValidValues_ThenReturnsEmptyString() {
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray);

		String result = ddlEditorService.getFieldsToAutofill(new String[] { "hello" });

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getAutofillFieldsAsJSONArray_WhenNoValues_ThenReturnsEmptyString() {
		String result = ddlEditorService.getFieldsToAutofill(null);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getAutofillFieldsAsJSONArray_WhenValidValues_ThenReturnsStringWithTheJsonArrayOfTheFieldsAndTheirValues() {
		when(mockJSONFactory.createJSONArray()).thenReturn(mockJSONArray);
		when(mockJSONArray.toJSONString()).thenReturn("expectedValue");
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJSONobject);
		when(mockJSONArray.length()).thenReturn(1);

		String result = ddlEditorService.getFieldsToAutofill(new String[] { "one=hello" });

		assertThat(result, equalTo("expectedValue"));
		InOrder inOrder = inOrder(mockJSONArray, mockJSONobject);
		inOrder.verify(mockJSONobject, times(1)).put("fieldName", "one");
		inOrder.verify(mockJSONobject, times(1)).put("fieldValue", "hello");
		inOrder.verify(mockJSONArray, times(1)).put(mockJSONobject);
		inOrder.verify(mockJSONArray, times(1)).toJSONString();
	}

	@Test
	public void getBackToDDLDisplayPortletURL_WhenNoError_ThenReturnsURLToDDLDisplayPortletRenderPhase() {
		long plid = 1234;

		when(mockThemeDisplay.getPlid()).thenReturn(plid);
		when(mockPortletURLFactory.create(mockRenderRequest, PortletKeys.DDL_DISPLAY_PORTLET, plid, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletURL);

		String result = ddlEditorService.getBackToDDLDisplayPortletURL(mockRenderRequest, mockThemeDisplay);

		assertThat(result, equalTo(mockPortletURL.toString()));
	}

	@Test
	public void getBackURL_WhenRedirectDoesNotExistAsRenderParameterButIsPresentInOriginalServletRequest_ThenReturnsOriginalRedirectParameterValue() {
		String redirect = "redirect";
		when(ParamUtil.getString(mockRenderRequest, REDIRECT)).thenReturn(null);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHTTPServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHTTPServletRequest)).thenReturn(mockHTTPServletRequest);
		when(mockHTTPServletRequest.getParameter(REDIRECT)).thenReturn(redirect);
		when(mockRenderRequest.getWindowState()).thenReturn(WindowState.MAXIMIZED);

		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		String result = ddlEditorService.getBackURL(mockRenderRequest, mockThemeDisplay);

		assertThat(result, equalTo(redirect));
	}

	@Test
	public void getBackURL_WhenRedirectDoesNotExistInAnyRequest_ThenReturnsURLToDDLDisplayPortletRenderPhase() {
		long plid = 1234;

		when(ParamUtil.getString(mockRenderRequest, REDIRECT)).thenReturn(null);
		when(mockPortal.getHttpServletRequest(mockRenderRequest)).thenReturn(mockHTTPServletRequest);
		when(mockPortal.getOriginalServletRequest(mockHTTPServletRequest)).thenReturn(mockHTTPServletRequest);
		when(mockHTTPServletRequest.getParameter(REDIRECT)).thenReturn(null);
		when(mockThemeDisplay.getPlid()).thenReturn(plid);
		when(mockPortletURLFactory.create(mockRenderRequest, PortletKeys.DDL_DISPLAY_PORTLET, plid, PortletRequest.RENDER_PHASE)).thenReturn(mockPortletURL);

		String result = ddlEditorService.getBackURL(mockRenderRequest, mockThemeDisplay);

		assertThat(result, equalTo(mockPortletURL.toString()));
	}

	@Test
	public void getBackURL_WhenRedirectExistsAsRenderParameter_ThenReturnsRequestRedirectParameterValue() {
		String redirect = "redirect";
		when(ParamUtil.getString(mockRenderRequest, REDIRECT)).thenReturn(redirect);
		when(mockThemeDisplay.getPortletDisplay()).thenReturn(mockPortletDisplay);

		String result = ddlEditorService.getBackURL(mockRenderRequest, mockThemeDisplay);

		assertThat(result, equalTo(redirect));

	}

	@Test
	public void getOrCreateDDLRecord_WhenDDLRecordIdGreaterThanZero_ThenReturnsTheUpdatedRecordAfterUpdatingTheAssetEntry() throws PortalException {
		long recordId = 1;
		long ddlRecordSetId = 2;
		long userId = 4;
		long userIdRecord = 5;
		long groupId = 6;
		String[] assetTagsNames = new String[] { "test2", "test3", "test1" };
		long[] assetCategoryIds = new long[] { 456, 126, 589 };

		when(mockServiceContext.getAssetTagNames()).thenReturn(assetTagsNames);
		when(mockServiceContext.getAssetCategoryIds()).thenReturn(assetCategoryIds);
		when(mockPermissionChecker.getUserId()).thenReturn(userId);
		when(mockDDLRecordLocalService.updateRecord(userId, recordId, true, DDLRecordConstants.DISPLAY_INDEX_DEFAULT, mockDDMFormValues, mockServiceContext)).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getUserId()).thenReturn(userIdRecord);
		when(mockDDLRecord.getGroupId()).thenReturn(groupId);
		when(mockDDLRecord.getRecordId()).thenReturn(recordId);

		DDLRecord result = ddlEditorService.getOrCreateDDLRecord(recordId, mockDDMFormValues, ddlRecordSetId, mockServiceContext, mockPermissionChecker);

		assertThat(result, sameInstance(mockDDLRecord));
		verify(mockAssetEntryLocalService, times(1)).updateEntry(userIdRecord, groupId, DDLRecord.class.getName(), recordId, assetCategoryIds, assetTagsNames);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDLRecord_WhenDDLRecordIdGreaterThanZeroAndExceptionUpdatingTheRecord_ThenThrowsPortalException() throws PortalException {
		long recordId = 1;
		long ddlRecordSetId = 2;
		long userId = 4;
		String[] assetTagsNames = new String[0];
		long[] assetCategoryIds = new long[0];

		when(mockServiceContext.getAssetTagNames()).thenReturn(assetTagsNames);
		when(mockServiceContext.getAssetCategoryIds()).thenReturn(assetCategoryIds);
		when(mockPermissionChecker.getUserId()).thenReturn(userId);
		when(mockDDLRecordLocalService.updateRecord(userId, recordId, true, DDLRecordConstants.DISPLAY_INDEX_DEFAULT, mockDDMFormValues, mockServiceContext)).thenThrow(new PortalException());

		ddlEditorService.getOrCreateDDLRecord(recordId, mockDDMFormValues, ddlRecordSetId, mockServiceContext, mockPermissionChecker);

		verifyNoInteractions(mockAssetEntryLocalService);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDLRecord_WhenDDLRecordIdGreaterThanZeroAndExceptionValidatingPermissions_ThenThrowsPortalException() throws PortalException {
		long recordId = 1;
		long ddlRecordSetId = 2;
		String[] assetTagsNames = new String[0];
		long[] assetCategoryIds = new long[0];

		when(mockServiceContext.getAssetTagNames()).thenReturn(assetTagsNames);
		when(mockServiceContext.getAssetCategoryIds()).thenReturn(assetCategoryIds);
		doThrow(new PortalException()).when(mockDDLPermissionService).validateUpdatePermission(mockPermissionChecker, recordId);

		ddlEditorService.getOrCreateDDLRecord(recordId, mockDDMFormValues, ddlRecordSetId, mockServiceContext, mockPermissionChecker);

		verifyNoInteractions(mockAssetEntryLocalService, mockDDLRecordLocalService);
	}

	@Test
	public void getOrCreateDDLRecord_WhenDDLRecordIdIsNotGreaterThanZero_ThenReturnsTheCreatedRecord() throws PortalException {
		long recordId = 0;
		long ddlRecordSetId = 2;
		long groupId = 3;
		long userIdRecord = 4;
		String[] assetTagsNames = new String[] { "test2", "test3", "test1" };
		long[] assetCategoryIds = new long[] { 456, 126, 589 };

		when(mockServiceContext.getAssetTagNames()).thenReturn(assetTagsNames);
		when(mockServiceContext.getAssetCategoryIds()).thenReturn(assetCategoryIds);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDLRecordService.addRecord(groupId, ddlRecordSetId, DDLRecordConstants.DISPLAY_INDEX_DEFAULT, mockDDMFormValues, mockServiceContext)).thenReturn(mockDDLRecord);
		when(mockDDLRecord.getUserId()).thenReturn(userIdRecord);
		when(mockDDLRecord.getGroupId()).thenReturn(groupId);
		when(mockDDLRecord.getRecordId()).thenReturn(recordId);

		DDLRecord result = ddlEditorService.getOrCreateDDLRecord(recordId, mockDDMFormValues, ddlRecordSetId, mockServiceContext, mockPermissionChecker);

		assertThat(result, sameInstance(mockDDLRecord));
		verify(mockAssetEntryLocalService, times(1)).updateEntry(userIdRecord, groupId, DDLRecord.class.getName(), recordId, assetCategoryIds, assetTagsNames);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDLRecord_WhenDDLRecordIdIsNotGreaterThanZeroAndExceptionCreatingTheRecord_ThenThrowsPortalException() throws PortalException {
		long recordId = 0;
		long ddlRecordSetId = 2;
		long groupId = 3;
		String[] assetTagsNames = new String[0];
		long[] assetCategoryIds = new long[0];

		when(mockServiceContext.getAssetTagNames()).thenReturn(assetTagsNames);
		when(mockServiceContext.getAssetCategoryIds()).thenReturn(assetCategoryIds);
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDLRecordService.addRecord(groupId, ddlRecordSetId, DDLRecordConstants.DISPLAY_INDEX_DEFAULT, mockDDMFormValues, mockServiceContext)).thenThrow(new PortalException());

		ddlEditorService.getOrCreateDDLRecord(recordId, mockDDMFormValues, ddlRecordSetId, mockServiceContext, mockPermissionChecker);

		verifyNoInteractions(mockAssetEntryLocalService);
	}

	@Test
	public void getOrCreateDDLRecordSet_WhenRecordSetFound_ThenReturnsTheRecordSet() throws PortalException {
		long groupId = 1;
		long structureId = 2;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		List<DDLRecordSet> recordSets = new ArrayList<>();
		recordSets.add(mockDDLRecordSet1);
		recordSets.add(mockDDLRecordSet2);
		when(mockDDLRecordSetLocalService.getRecordSets(groupId)).thenReturn(recordSets);

		when(mockDDLRecordSet1.getDDMStructureId()).thenReturn(789l);
		when(mockDDLRecordSet2.getDDMStructureId()).thenReturn(structureId);

		DDLRecordSet result = ddlEditorService.getOrCreateDDLRecordSet(mockDDMStructure, mockServiceContext);

		assertThat(result, sameInstance(mockDDLRecordSet2));
		verifyZeroInteractions(mockDDLPermissionService);
	}

	@Test
	public void getOrCreateDDLRecordSet_WhenRecordSetNotFound_ThenCreatesAndReturnsTheRecordSet() throws PortalException {
		long groupId = 1;
		long structureId = 2;
		long userId = 3;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockDDMStructure.getStructureId()).thenReturn(structureId);
		when(mockDDMStructure.getNameMap()).thenReturn(mockNameMap);
		when(mockDDMStructure.getDescriptionMap()).thenReturn(mockDescriptionMap);
		List<DDLRecordSet> recordSets = new ArrayList<>();
		recordSets.add(mockDDLRecordSet1);
		when(mockDDLRecordSetLocalService.getRecordSets(groupId)).thenReturn(recordSets);
		when(mockDDLRecordSet1.getDDMStructureId()).thenReturn(789l);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockDDLRecordSetLocalService.addRecordSet(userId, groupId, structureId, null, mockNameMap, mockDescriptionMap, DDLRecordSetConstants.MIN_DISPLAY_ROWS_DEFAULT,
				DDLRecordSetConstants.SCOPE_DYNAMIC_DATA_LISTS, mockServiceContext)).thenReturn(mockDDLRecordSet2);

		DDLRecordSet result = ddlEditorService.getOrCreateDDLRecordSet(mockDDMStructure, mockServiceContext);

		assertThat(result, sameInstance(mockDDLRecordSet2));
		InOrder inOrder = inOrder(mockDDLRecordSetLocalService, mockDDLPermissionService);
		inOrder.verify(mockDDLPermissionService, times(1)).configureDefaultDDLRecordSetPermissionsInServiceContext(mockServiceContext);
		inOrder.verify(mockDDLRecordSetLocalService, times(1)).addRecordSet(userId, groupId, structureId, null, mockNameMap, mockDescriptionMap, DDLRecordSetConstants.MIN_DISPLAY_ROWS_DEFAULT,
				DDLRecordSetConstants.SCOPE_DYNAMIC_DATA_LISTS, mockServiceContext);

	}

	@Test
	public void getServiceContextForScopeGroup_WhenNoError_ThenReturnsTheServiceContextWithTheScopeGroupId() {
		long groupId = 12;
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);

		ServiceContext result = ddlEditorService.getServiceContextForScopeGroup(mockThemeDisplay);

		assertThat(result, sameInstance(mockServiceContext));
		verify(mockServiceContext, times(1)).setScopeGroupId(groupId);
	}

	@Test(expected = PortalException.class)
	public void getServiceContextForUpdate_WhenExceptionRetrievingTheServiceContext_ThenThrowsPortalException() throws PortalException {
		when(ServiceContextFactory.getInstance(DDLRecord.class.getName(), mockActionRequest)).thenThrow(new PortalException());

		ddlEditorService.getServiceContextForUpdate(mockActionRequest, mockThemeDisplay);
	}

	@Test
	public void getServiceContextForUpdate_WhenNoError_ThenReturnsTheServiceContextForDDLRecordWithTheScopeGroupId() throws PortalException {
		long groupId = 12;
		when(ServiceContextFactory.getInstance(DDLRecord.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(mockThemeDisplay.getScopeGroupId()).thenReturn(groupId);

		ServiceContext result = ddlEditorService.getServiceContextForUpdate(mockActionRequest, mockThemeDisplay);

		assertThat(result, sameInstance(mockServiceContext));
		verify(mockServiceContext, times(1)).setScopeGroupId(groupId);
	}
}
