/**
 * SubjectType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uk.co.capita_software.support.selfservice;

public class SubjectType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SubjectType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _CapitaSite = "CapitaSite";
    public static final java.lang.String _Integrator = "Integrator";
    public static final java.lang.String _OtherSecurityEntity = "OtherSecurityEntity";
    public static final java.lang.String _SecureBureauServiceSite = "SecureBureauServiceSite";
    public static final java.lang.String _CapitaPortal = "CapitaPortal";
    public static final SubjectType CapitaSite = new SubjectType(_CapitaSite);
    public static final SubjectType Integrator = new SubjectType(_Integrator);
    public static final SubjectType OtherSecurityEntity = new SubjectType(_OtherSecurityEntity);
    public static final SubjectType SecureBureauServiceSite = new SubjectType(_SecureBureauServiceSite);
    public static final SubjectType CapitaPortal = new SubjectType(_CapitaPortal);
    public java.lang.String getValue() { return _value_;}
    public static SubjectType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SubjectType enumeration = (SubjectType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SubjectType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubjectType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "subjectType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
