/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link GroupWebhook}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GroupWebhook
 * @generated
 */
public class GroupWebhookWrapper
	extends BaseModelWrapper<GroupWebhook>
	implements GroupWebhook, ModelWrapper<GroupWebhook> {

	public GroupWebhookWrapper(GroupWebhook groupWebhook) {
		super(groupWebhook);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("webhookId", getWebhookId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("name", getName());
		attributes.put("payloadURL", getPayloadURL());
		attributes.put("enabled", isEnabled());
		attributes.put("events", getEvents());
		attributes.put("className", getClassName());
		attributes.put("classPK", getClassPK());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long webhookId = (Long)attributes.get("webhookId");

		if (webhookId != null) {
			setWebhookId(webhookId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String payloadURL = (String)attributes.get("payloadURL");

		if (payloadURL != null) {
			setPayloadURL(payloadURL);
		}

		Boolean enabled = (Boolean)attributes.get("enabled");

		if (enabled != null) {
			setEnabled(enabled);
		}

		String events = (String)attributes.get("events");

		if (events != null) {
			setEvents(events);
		}

		String className = (String)attributes.get("className");

		if (className != null) {
			setClassName(className);
		}

		Long classPK = (Long)attributes.get("classPK");

		if (classPK != null) {
			setClassPK(classPK);
		}
	}

	@Override
	public GroupWebhook cloneWithOriginalValues() {
		return wrap(model.cloneWithOriginalValues());
	}

	/**
	 * Returns the class name of this group webhook.
	 *
	 * @return the class name of this group webhook
	 */
	@Override
	public String getClassName() {
		return model.getClassName();
	}

	/**
	 * Returns the class pk of this group webhook.
	 *
	 * @return the class pk of this group webhook
	 */
	@Override
	public long getClassPK() {
		return model.getClassPK();
	}

	/**
	 * Returns the company ID of this group webhook.
	 *
	 * @return the company ID of this group webhook
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	@Override
	public java.util.List<String> getConfiguredEvents() {
		return model.getConfiguredEvents();
	}

	/**
	 * Returns the enabled of this group webhook.
	 *
	 * @return the enabled of this group webhook
	 */
	@Override
	public boolean getEnabled() {
		return model.getEnabled();
	}

	/**
	 * Returns the events of this group webhook.
	 *
	 * @return the events of this group webhook
	 */
	@Override
	public String getEvents() {
		return model.getEvents();
	}

	/**
	 * Returns the group ID of this group webhook.
	 *
	 * @return the group ID of this group webhook
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the name of this group webhook.
	 *
	 * @return the name of this group webhook
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the payload url of this group webhook.
	 *
	 * @return the payload url of this group webhook
	 */
	@Override
	public String getPayloadURL() {
		return model.getPayloadURL();
	}

	/**
	 * Returns the primary key of this group webhook.
	 *
	 * @return the primary key of this group webhook
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the webhook ID of this group webhook.
	 *
	 * @return the webhook ID of this group webhook
	 */
	@Override
	public long getWebhookId() {
		return model.getWebhookId();
	}

	/**
	 * Returns <code>true</code> if this group webhook is enabled.
	 *
	 * @return <code>true</code> if this group webhook is enabled; <code>false</code> otherwise
	 */
	@Override
	public boolean isEnabled() {
		return model.isEnabled();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the class name of this group webhook.
	 *
	 * @param className the class name of this group webhook
	 */
	@Override
	public void setClassName(String className) {
		model.setClassName(className);
	}

	/**
	 * Sets the class pk of this group webhook.
	 *
	 * @param classPK the class pk of this group webhook
	 */
	@Override
	public void setClassPK(long classPK) {
		model.setClassPK(classPK);
	}

	/**
	 * Sets the company ID of this group webhook.
	 *
	 * @param companyId the company ID of this group webhook
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets whether this group webhook is enabled.
	 *
	 * @param enabled the enabled of this group webhook
	 */
	@Override
	public void setEnabled(boolean enabled) {
		model.setEnabled(enabled);
	}

	/**
	 * Sets the events of this group webhook.
	 *
	 * @param events the events of this group webhook
	 */
	@Override
	public void setEvents(String events) {
		model.setEvents(events);
	}

	/**
	 * Sets the group ID of this group webhook.
	 *
	 * @param groupId the group ID of this group webhook
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the name of this group webhook.
	 *
	 * @param name the name of this group webhook
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the payload url of this group webhook.
	 *
	 * @param payloadURL the payload url of this group webhook
	 */
	@Override
	public void setPayloadURL(String payloadURL) {
		model.setPayloadURL(payloadURL);
	}

	/**
	 * Sets the primary key of this group webhook.
	 *
	 * @param primaryKey the primary key of this group webhook
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the webhook ID of this group webhook.
	 *
	 * @param webhookId the webhook ID of this group webhook
	 */
	@Override
	public void setWebhookId(long webhookId) {
		model.setWebhookId(webhookId);
	}

	@Override
	public String toXmlString() {
		return model.toXmlString();
	}

	@Override
	protected GroupWebhookWrapper wrap(GroupWebhook groupWebhook) {
		return new GroupWebhookWrapper(groupWebhook);
	}

}