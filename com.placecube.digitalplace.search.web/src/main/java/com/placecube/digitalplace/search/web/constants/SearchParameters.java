package com.placecube.digitalplace.search.web.constants;

public final class SearchParameters {

	public static final String SEARCH_KEYWORD = "keywords";

	public static final String SELECTED_CATEGORIES = "selectedCategories";

	public static final String SELECTED_CONTENT_TYPES = "selectedContentTypes";

	public static final String SELECTED_SORT_BY = "selectedSortByFieldName";

	public static final String SELECTED_SORT_BY_DIRECTION = "selectedSortByDirection";

	public static final String SELECTED_TAGS = "selectedTags";

	private SearchParameters() {
	}

}
