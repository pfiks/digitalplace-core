package com.placecube.digitalplace.payment.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.liferay.portal.kernel.model.User;

@RunWith(MockitoJUnitRunner.class)
public class AgentReferredPaymentContextBuilderTest {

	@Mock
	private User mockUser;

	@Mock
	private AgentReferredPaymentLine mockPaymentLine;

	@Test
	public void build_WhenNoErrors_ThenBuildsAgentReferredPaymentContextWithValues() {
		AgentReferredPaymentContextBuilder builder = new AgentReferredPaymentContextBuilder();
		builder.withCallerPhoneNumber("123456789");
		builder.withCallerEmail("caller@example.com");
		builder.withReceiptEmail("receipt@example.com");
		builder.withImmediateCallBack(true);
		builder.withAgentPhoneNumber("987654321");
		builder.withReturnUrl("http://return.url");
		builder.withSid("SID123");
		builder.withLogUrl("http://log.url");
		builder.withUser(mockUser);
		builder.withPaymentLines(Collections.singletonList(mockPaymentLine));
		AgentReferredPaymentContext agentReferredPaymentContext = builder.build();

		assertThat(agentReferredPaymentContext.getCallerEmail(), equalTo("caller@example.com"));
		assertThat(agentReferredPaymentContext.getReceiptEmail(), equalTo("receipt@example.com"));
		assertThat(agentReferredPaymentContext.isImmediateCallBack(), equalTo(true));
		assertThat(agentReferredPaymentContext.getAgentPhoneNumber(), equalTo("987654321"));
		assertThat(agentReferredPaymentContext.getReturnUrl(), equalTo("http://return.url"));
		assertThat(agentReferredPaymentContext.getSid(), equalTo("SID123"));
		assertThat(agentReferredPaymentContext.getLogUrl(), equalTo("http://log.url"));
		assertThat(agentReferredPaymentContext.getUser(), sameInstance(mockUser));
		assertThat(agentReferredPaymentContext.getPaymentLines().size(), equalTo(1));
		assertThat(agentReferredPaymentContext.getPaymentLines().get(0), sameInstance(mockPaymentLine));

	}

}