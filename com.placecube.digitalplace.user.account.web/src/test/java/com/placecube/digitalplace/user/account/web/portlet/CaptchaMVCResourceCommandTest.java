package com.placecube.digitalplace.user.account.web.portlet;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.captcha.util.CaptchaUtil;
import com.liferay.portal.kernel.module.service.Snapshot;
import com.liferay.portal.kernel.util.Portal;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Snapshot.class, CaptchaUtil.class})
public class CaptchaMVCResourceCommandTest extends PowerMockito {

	@InjectMocks
	private CaptchaMVCResourceCommand captchaMVCResourceCommand;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private Portal mockPortal;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Before
	public void setUp() {
		mockStatic(Snapshot.class, CaptchaUtil.class);

		when(mockPortal.getHttpServletRequest(mockResourceRequest)).thenReturn(mockHttpServletRequest);
		when(mockPortal.getHttpServletResponse(mockResourceResponse)).thenReturn(mockHttpServletResponse);
	}

	@Test
	public void serveResource_WhenNoError_ThenReturnsTrue() throws PortletException {
		boolean result = captchaMVCResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		assertTrue(result);
	}

	@Test
	public void serveResource_WhenNoError_ThenCallsCaptchaUtilServeResource() throws Exception {
		captchaMVCResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);

		verifyStatic(CaptchaUtil.class, times(1));
		CaptchaUtil.serveImage(mockHttpServletRequest, mockHttpServletResponse);
	}

	@Test(expected = PortletException.class)
	public void serveResource_WhenExceptionServingCaptchaImage_ThenThrowsPortletException() throws Exception {
		doThrow(new IOException()).when(CaptchaUtil.class);
		CaptchaUtil.serveImage(mockHttpServletRequest, mockHttpServletResponse);

		captchaMVCResourceCommand.serveResource(mockResourceRequest, mockResourceResponse);
	}

}
