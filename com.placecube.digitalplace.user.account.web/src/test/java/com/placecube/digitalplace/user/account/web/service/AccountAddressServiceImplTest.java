package com.placecube.digitalplace.user.account.web.service;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.web.util.AccountAddressUtils;

@RunWith(PowerMockRunner.class)
public class AccountAddressServiceImplTest extends PowerMockito {

	@InjectMocks
	private AccountAddressServiceImpl accountAddressServiceImpl;

	private final String ADDRESS_VALUE = "address-value<br>";

	private final List<Address> ADDRESSES = new ArrayList<>();

	@Mock
	private AccountContext mockAccountContext;

	@Mock
	private Address mockAddress;

	@Mock
	private Contact mockContact;

	@Mock
	private User mockUser;

	@Test
	public void populateContextWithExistingPrimaryAddress_WhenUserHasAddresses_ThenFullAddressIsNotSet() throws PortalException {
		when(AccountAddressUtils.getFullAddress(mockAddress, "<br>")).thenReturn(ADDRESS_VALUE);
		when(mockUser.getAddresses()).thenReturn(ADDRESSES);
		ADDRESSES.add(mockAddress);

		accountAddressServiceImpl.populateContextWithExistingPrimaryAddress(mockAccountContext, mockUser);

		verify(mockAccountContext, never()).setFullAddress(ADDRESS_VALUE);

	}

	@Test
	public void populateContextWithExistingPrimaryAddress_WhenUserHasNoPrimaryAddress_ThenFullAddressIsNotSet() throws PortalException {
		when(mockAddress.isPrimary()).thenReturn(false);
		when(AccountAddressUtils.getFullAddress(mockAddress, "<br>")).thenReturn(ADDRESS_VALUE);
		when(mockUser.getAddresses()).thenReturn(ADDRESSES);
		ADDRESSES.add(mockAddress);

		accountAddressServiceImpl.populateContextWithExistingPrimaryAddress(mockAccountContext, mockUser);

		verify(mockAccountContext, never()).setFullAddress(ADDRESS_VALUE);

	}

	@Test
	public void populateContextWithExistingPrimaryAddress_WhenUserHasPrimaryAddress_ThenFullAddressIsSet() throws PortalException {
		when(mockAddress.isPrimary()).thenReturn(true);
		when(AccountAddressUtils.getFullAddress(mockAddress, "<br>")).thenReturn(ADDRESS_VALUE);
		when(mockUser.getAddresses()).thenReturn(ADDRESSES);
		ADDRESSES.add(mockAddress);

		accountAddressServiceImpl.populateContextWithExistingPrimaryAddress(mockAccountContext, mockUser);

		verify(mockAccountContext, times(1)).setFullAddress(ADDRESS_VALUE);

	}
}
