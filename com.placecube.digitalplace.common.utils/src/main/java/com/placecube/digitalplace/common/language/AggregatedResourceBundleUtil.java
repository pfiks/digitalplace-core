package com.placecube.digitalplace.common.language;

import java.util.Locale;
import java.util.ResourceBundle;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.placecube.digitalplace.common.internal.utils.DigitalPlaceResourceBundleUtil;

public final class AggregatedResourceBundleUtil {

	public static String format(String messageKey, String argument, Locale locale, String... bundleSymbolicNames) {
		ResourceBundle resourceBundle = DigitalPlaceResourceBundleUtil.getAggregatedResourceBundle(locale, bundleSymbolicNames);

		return LanguageUtil.format(resourceBundle, messageKey, argument);
	}

	public static String format(String messageKey, String[] arguments, Locale locale, String... bundleSymbolicNames) {
		ResourceBundle resourceBundle = DigitalPlaceResourceBundleUtil.getAggregatedResourceBundle(locale, bundleSymbolicNames);

		return LanguageUtil.format(resourceBundle, messageKey, arguments);
	}

	public static String get(String messageKey, Locale locale, String... bundleSymbolicNames) {
		ResourceBundle resourceBundle = DigitalPlaceResourceBundleUtil.getAggregatedResourceBundle(locale, bundleSymbolicNames);

		return LanguageUtil.get(resourceBundle, messageKey);
	}

	public static ResourceBundle getAggregatedResourceBundle(Locale locale, String... bundleSymbolicNames) {
		return DigitalPlaceResourceBundleUtil.getAggregatedResourceBundle(locale, bundleSymbolicNames);
	}

}
