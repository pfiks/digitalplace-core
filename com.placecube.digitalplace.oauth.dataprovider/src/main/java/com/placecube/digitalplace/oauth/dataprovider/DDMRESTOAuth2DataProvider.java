package com.placecube.digitalplace.oauth.dataprovider;

import com.jayway.jsonpath.JsonPath;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProvider;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderException;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderInstanceSettings;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderRequest;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderResponse;
import com.liferay.dynamic.data.mapping.data.provider.DDMDataProviderResponseStatus;
import com.liferay.dynamic.data.mapping.data.provider.configuration.DDMDataProviderConfiguration;
import com.liferay.dynamic.data.mapping.data.provider.settings.DDMDataProviderSettingsProvider;
import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.oauth2.provider.model.OAuth2Application;
import com.liferay.oauth2.provider.service.OAuth2ApplicationLocalService;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.json.web.service.client.JSONWebServiceException;
import com.liferay.portal.kernel.cache.MultiVMPool;
import com.liferay.portal.kernel.cache.PortalCache;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.placecube.digitalplace.oauth.dataprovider.service.DDMRESTOAuth2DataProviderService;
import com.placecube.digitalplace.oauth.dataprovider.service.DataProviderPortalCacheService;
import com.placecube.digitalplace.oauth.dataprovider.service.DataProviderWebServiceClientService;
import com.placecube.digitalplace.oauth.dataprovider.settings.DDMRESTOAuth2DataProviderSettings;
import com.placecube.digitalplace.oauth.dataprovider.util.URLUtil;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

@Component(configurationPid = "com.liferay.dynamic.data.mapping.data.provider.configuration.DDMDataProviderConfiguration", property = "ddm.data.provider.type=restOAuth", service = DDMDataProvider.class)
public class DDMRESTOAuth2DataProvider implements DDMDataProvider {

	private static final Log LOG = LogFactoryUtil.getLog(DDMRESTOAuth2DataProvider.class);

	@Reference
	private DDMDataProviderInstanceSettings ddmDataProviderInstanceSettings;

	@Reference(target = "(ddm.data.provider.type=restOAuth)")
	private DDMDataProviderSettingsProvider ddmDataProviderSettingsProvider;

	@Reference
	private MultiVMPool multiVMPool;

	@Reference
	private DDMRESTOAuth2DataProviderService ddmRESTOAuth2DataProviderService;

	@Reference
	private DataProviderWebServiceClientService dataProviderWebServiceClientService;

	@Reference
	private DataProviderPortalCacheService dataProviderPortalCacheService;

	@Reference
	private OAuth2ApplicationLocalService oAuth2ApplicationLocalService;

	@Reference
	private URLUtil urlUtil;

	private DDMDataProviderConfiguration ddmDataProviderConfiguration;

	private PortalCache<String, DDMDataProviderResponse> portalCache;

	@Override
	public DDMDataProviderResponse getData(DDMDataProviderRequest ddmDataProviderRequest) throws DDMDataProviderException {
		long threadLocalCompanyId = CompanyThreadLocal.getCompanyId();

		try {
			Optional<DDMDataProviderInstance> ddmDataProviderInstanceOptional = ddmRESTOAuth2DataProviderService.findDDMDataProviderInstance(ddmDataProviderRequest);

			if (ddmDataProviderInstanceOptional.isEmpty()) {
				DDMDataProviderResponse.Builder builder = DDMDataProviderResponse.Builder.newBuilder();

				return builder.withStatus(DDMDataProviderResponseStatus.SERVICE_UNAVAILABLE).build();
			}

			DDMDataProviderInstance ddmDataProviderInstance = ddmDataProviderInstanceOptional.get();

			DDMRESTOAuth2DataProviderSettings ddmRESTDataProviderSettings = ddmDataProviderInstanceSettings.getSettings(ddmDataProviderInstance, DDMRESTOAuth2DataProviderSettings.class);

			return getDDMDataProviderResponse(ddmDataProviderRequest, ddmRESTDataProviderSettings, ddmDataProviderInstance);

		} catch (Exception exception) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(exception);
			}

			throw new DDMDataProviderException(exception);
		} finally {
			CompanyThreadLocal.setCompanyId(threadLocalCompanyId);
		}
	}

	@Override
	public Class<?> getSettings() {
		return ddmDataProviderSettingsProvider.getSettings();
	}

	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		ddmDataProviderConfiguration = ConfigurableUtil.createConfigurable(DDMDataProviderConfiguration.class, properties);
		portalCache = (PortalCache<String, DDMDataProviderResponse>) multiVMPool.getPortalCache(DDMRESTOAuth2DataProvider.class.getName());
	}

	@Deactivate
	protected void deactivate() {
		multiVMPool.removePortalCache(DDMRESTOAuth2DataProvider.class.getName());
	}

	private DDMDataProviderResponse getDDMDataProviderResponse(DDMDataProviderRequest ddmDataProviderRequest, DDMRESTOAuth2DataProviderSettings ddmRESTDataProviderSettings,
			DDMDataProviderInstance ddmDataProviderInstance) throws Exception {
		try {
			return getData(ddmDataProviderRequest, ddmRESTDataProviderSettings, ddmDataProviderInstance.getCompanyId());
		} catch (JSONWebServiceException jsonWebServiceException) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(jsonWebServiceException);
			} else if (LOG.isWarnEnabled()) {
				LOG.warn("The data provider was not able to connect to the " + "web service. " + jsonWebServiceException);
			}
		}

		return ddmRESTOAuth2DataProviderService.createDDMDataProviderResponse(JsonPath.parse("{}"), DDMDataProviderResponseStatus.SERVICE_UNAVAILABLE, ddmRESTDataProviderSettings);
	}

	private DDMDataProviderResponse getData(DDMDataProviderRequest ddmDataProviderRequest, DDMRESTOAuth2DataProviderSettings ddmRESTDataProviderSettings, long companyId) throws Exception {

		Map<String, Object> requestInputParametersMap = ddmRESTOAuth2DataProviderService.getRequestInputParametersMap(ddmDataProviderRequest, ddmRESTDataProviderSettings);

		String settingsUrl = ddmRESTDataProviderSettings.url();

		Map<String, String> pathInputParametersMap = dataProviderWebServiceClientService.getPathInputParametersMap(requestInputParametersMap, settingsUrl);

		String url = urlUtil.buildURL(pathInputParametersMap, settingsUrl);

		URI uri = urlUtil.buildURI(url);

		Map<String, String> allParametersMap = dataProviderWebServiceClientService.getAllParametersMap(pathInputParametersMap, uri.getQuery(), requestInputParametersMap);

		String absoluteURL = urlUtil.getAbsoluteURL(uri.getQuery(), url);

		String portalCacheKey = dataProviderPortalCacheService.getPortalCacheKey(ddmDataProviderRequest.getDDMDataProviderId(), allParametersMap, absoluteURL);

		DDMDataProviderResponse ddmDataProviderResponse = portalCache.get(portalCacheKey);

		if (ddmDataProviderResponse != null && ddmRESTDataProviderSettings.cacheable()) {
			return ddmDataProviderResponse;
		}

		OAuth2Application oAuth2Application = oAuth2ApplicationLocalService.getOAuth2ApplicationByExternalReferenceCode(ddmRESTDataProviderSettings.oauthApplicationExternalReferenceCode(), companyId);

		String response = dataProviderWebServiceClientService.callJSONWebService(companyId, oAuth2Application, uri, absoluteURL, ddmDataProviderConfiguration.trustSelfSignedCertificates(),
				allParametersMap);

		String sanitizedResponse = IOUtils.toString(new BOMInputStream(new ByteArrayInputStream(response.getBytes())), StandardCharsets.UTF_8);

		ddmDataProviderResponse = ddmRESTOAuth2DataProviderService.createDDMDataProviderResponse(JsonPath.parse(sanitizedResponse), DDMDataProviderResponseStatus.OK, ddmRESTDataProviderSettings);

		if (ddmRESTDataProviderSettings.cacheable()) {
			portalCache.put(portalCacheKey, ddmDataProviderResponse);
		}

		return ddmDataProviderResponse;
	}

}