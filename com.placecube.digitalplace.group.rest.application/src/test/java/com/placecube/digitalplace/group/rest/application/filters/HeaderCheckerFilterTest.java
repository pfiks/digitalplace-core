package com.placecube.digitalplace.group.rest.application.filters;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.petra.string.StringPool;
import com.placecube.digitalplace.group.rest.application.GroupRestApplication;
import com.placecube.digitalplace.group.rest.application.constants.ErrorCodes;
import com.placecube.digitalplace.group.rest.application.exceptions.GroupRestSecurityException;
import com.placecube.digitalplace.group.rest.application.service.RequestFiltersService;

public class HeaderCheckerFilterTest {

	private HeaderCheckerFilter headerCheckerFilter;

	@Mock
	private ContainerRequestContext mockContainerRequestContext;

	@Mock
	private GroupRestApplication mockGroupRestApplication;

	@Mock
	private RequestFiltersService mockRequestFiltersService;

	@Before
	public void activateSetUp() {
		initMocks(this);

		when(mockGroupRestApplication.getRequestFiltersService()).thenReturn(mockRequestFiltersService);
		headerCheckerFilter = new HeaderCheckerFilter(mockGroupRestApplication);
	}

	@Test
	public void filter_WhenAcceptHeaderHasNoValue_ThenThrowsGroupSecurityException() {
		mockApiKeyHeaderWithValue();
		mockDateHeaderWithValue();
		mockSignatureHeaderWithValue();
		when(mockRequestFiltersService.getAcceptHeader(mockContainerRequestContext)).thenReturn("    ");

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			verifyExceptionValues(e);
		}
	}

	@Test
	public void filter_WhenAcceptHeaderHasValueDifferentFromApplicationJson_ThenThrowsGroupSecurityException() throws IOException {
		mockApiKeyHeaderWithValue();
		mockDateHeaderWithValue();
		mockSignatureHeaderWithValue();
		when(mockRequestFiltersService.getAcceptHeader(mockContainerRequestContext)).thenReturn("adifferentValue");

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			verifyExceptionValues(e);
		}
	}

	@Test
	public void filter_WhenAcceptHeaderIsMissing_ThenThrowsGroupSecurityException() throws IOException {
		mockApiKeyHeaderWithValue();
		mockDateHeaderWithValue();
		mockSignatureHeaderWithValue();
		when(mockRequestFiltersService.getAcceptHeader(mockContainerRequestContext)).thenReturn(null);

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			verifyExceptionValues(e);
		}
	}

	@Test
	public void filter_WhenAllRequiredHeadersArePresent_ThenNoExceptionIsThrown() throws IOException {
		mockApiKeyHeaderWithValue();
		mockDateHeaderWithValue();
		mockSignatureHeaderWithValue();
		mockAcceptHeaderWithExpectedValue();

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			fail();
		}
	}

	@Test
	public void filter_WhenXAPIKeyHeaderHasNoValue_ThenThrowsGroupSecurityException() throws IOException {
		when(mockRequestFiltersService.getApiKeyHeader(mockContainerRequestContext)).thenReturn(" ");
		mockDateHeaderWithValue();
		mockSignatureHeaderWithValue();
		mockAcceptHeaderWithExpectedValue();

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			verifyExceptionValues(e);
		}
	}

	@Test
	public void filter_WhenXAPIKeyHeaderIsMissing_ThenThrowsGroupSecurityException() throws IOException {
		when(mockRequestFiltersService.getApiKeyHeader(mockContainerRequestContext)).thenReturn(null);
		mockDateHeaderWithValue();
		mockSignatureHeaderWithValue();
		mockAcceptHeaderWithExpectedValue();

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			verifyExceptionValues(e);
		}
	}

	@Test
	public void filter_WhenXDateHeaderHasNoValue_ThenThrowsGroupSecurityException() throws IOException {
		mockApiKeyHeaderWithValue();
		when(mockRequestFiltersService.getDateHeader(mockContainerRequestContext)).thenReturn(StringPool.THREE_SPACES);
		mockSignatureHeaderWithValue();
		mockAcceptHeaderWithExpectedValue();

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			verifyExceptionValues(e);
		}
	}

	@Test
	public void filter_WhenXDateHeaderIsMissing_ThenThrowsGroupSecurityException() throws IOException {
		mockApiKeyHeaderWithValue();
		when(mockRequestFiltersService.getDateHeader(mockContainerRequestContext)).thenReturn(null);
		mockSignatureHeaderWithValue();
		mockAcceptHeaderWithExpectedValue();

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			verifyExceptionValues(e);
		}
	}

	@Test
	public void filter_WhenXSignatureHeaderHasNoValue_ThenThrowsGroupSecurityException() throws IOException {
		mockApiKeyHeaderWithValue();
		mockDateHeaderWithValue();
		when(mockRequestFiltersService.getSignatureHeader(mockContainerRequestContext)).thenReturn("    ");
		mockAcceptHeaderWithExpectedValue();

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			verifyExceptionValues(e);
		}
	}

	@Test
	public void filter_WhenXSignatureHeaderIsMissing_ThenThrowsGroupSecurityException() throws IOException {
		mockApiKeyHeaderWithValue();
		mockDateHeaderWithValue();
		when(mockRequestFiltersService.getSignatureHeader(mockContainerRequestContext)).thenReturn(null);
		mockAcceptHeaderWithExpectedValue();

		try {
			headerCheckerFilter.filter(mockContainerRequestContext);
		} catch (GroupRestSecurityException e) {
			verifyExceptionValues(e);
		}
	}

	private void mockAcceptHeaderWithExpectedValue() {
		when(mockRequestFiltersService.getAcceptHeader(mockContainerRequestContext)).thenReturn("application/json");
	}

	private void mockApiKeyHeaderWithValue() {
		when(mockRequestFiltersService.getApiKeyHeader(mockContainerRequestContext)).thenReturn("myAPIKeyHeader");
	}

	private void mockDateHeaderWithValue() {
		when(mockRequestFiltersService.getDateHeader(mockContainerRequestContext)).thenReturn("myDateHeader");
	}

	private void mockSignatureHeaderWithValue() {
		when(mockRequestFiltersService.getSignatureHeader(mockContainerRequestContext)).thenReturn("mySignatureHeader");
	}

	private void verifyExceptionValues(GroupRestSecurityException e) {
		assertThat(e.getMessage(), equalTo(ErrorCodes.MISSING_HEADERS.getCode()));
		assertThat(e.getResponse().getStatusInfo(), equalTo(Response.Status.BAD_REQUEST));
	}

}
