package com.placecube.digitalplace.oauth.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpHeaders;
import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = URLUtil.class)
public class URLUtil {

	public URL buildURL(String url) throws MalformedURLException {
		return new URL(url);
	}

	public HttpURLConnection getConnection(URL url, int postDataLength) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
		connection.setRequestProperty(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());
		connection.setRequestProperty(HttpHeaders.CONTENT_LENGTH, Integer.toString(postDataLength));
		connection.setUseCaches(false);

		return connection;
	}

	public String getResponse(HttpURLConnection conn) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

		StringBuilder response = new StringBuilder();
		String responseLine = null;
		while ((responseLine = br.readLine()) != null) {
			response.append(responseLine.trim());
		}

		br.close();

		return response.toString();
	}

	public void writePostData(HttpURLConnection conn, byte[] postData) throws IOException {
		try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
			wr.write(postData);
		}
	}
}
