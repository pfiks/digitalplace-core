package com.placecube.digitalplace.calendar.web.registry;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarConnector;

@Component(immediate = true, service = DigitalPlaceCalendarConnectorRegistry.class)
public class DigitalPlaceCalendarConnectorRegistry {

	private static final Log LOG = LogFactoryUtil.getLog(DigitalPlaceCalendarConnectorRegistry.class);

	List<DigitalPlaceCalendarConnector> calendarConnectors = new ArrayList<>();

	public List<DigitalPlaceCalendarConnector> getCalendarConnectors() {
		return calendarConnectors;

	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setCalendarConnector(DigitalPlaceCalendarConnector calendarConnector) {
		LOG.debug("Registering DigitalPlaceCalendarConnector with bundleId: " + calendarConnector.getBundleId());
		calendarConnectors.add(calendarConnector);
	}

	protected void unsetCalendarConnector(DigitalPlaceCalendarConnector calendarConnector) {
		LOG.debug("Unregistering DigitalPlaceCalendarConnector with bundleId: " + calendarConnector.getBundleId());
		calendarConnectors.remove(calendarConnector);
	}

}
