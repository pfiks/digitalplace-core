package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.settings.LocalizedValuesMap;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.configuration.AdvancePeopleSearchPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.model.UserSearchField;

@RunWith(PowerMockRunner.class)
public class AdvancePeopleSearchConfigurationServiceTest extends PowerMockito {

	private static final String EXPANDO_FIELD_1 = "expando__field__1";

	private static final String EXPANDO_FIELD_2 = "expando__field__2";

	private static final String EXPANDO_LABEL_1 = "expando-label-1";

	private static final String EXPANDO_LABEL_2 = "expando-label-2";

	private static final String FIELD = "field";

	private static final String LABEL = "label";

	@InjectMocks
	private AdvancePeopleSearchConfigurationService advancePeopleSearchConfigurationService;

	@Mock
	private AdvancePeopleSearchPortletInstanceConfiguration mockConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Mock
	private ExpandoColumnLocalService mockExpandoColumnLocalService;

	@Mock
	private JSONArray mockJsonArray;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private JSONObject mockJsonObject1;

	@Mock
	private JSONObject mockJsonObject2;

	@Mock
	private JSONObject mockJsonObject3;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private LocalizedValuesMap mockTitleMap;

	@Test
	public void getSearchFields_WhenConfiguredFieldsIsEmpty_ThenReturnEmptyFieldList() {
		long companyId = 123;

		List<UserSearchField> result = advancePeopleSearchConfigurationService.getSearchFields(companyId, Locale.UK, StringPool.BLANK);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getSearchFields_WhenErrorIsThrownAndConfiguredFieldsIsNotEmpty_ThenReturnEmptyFieldList() {
		long companyId = 123;
		String configuredFields = "{invalid JSON";

		List<UserSearchField> result = advancePeopleSearchConfigurationService.getSearchFields(companyId, Locale.UK, configuredFields);

		assertTrue(result.isEmpty());
	}

	@Test
	public void getSearchFields_WhenNoErrorAndConfiguredFieldsIsNotEmpty_ThenReturnFieldList() throws JSONException {
		long companyId = 123;
		String configuredFields = "[]";
		List<UserSearchField> expected = new LinkedList<>();
		expected.add(new UserSearchField(FIELD, LABEL));
		expected.add(new UserSearchField(EXPANDO_FIELD_1, EXPANDO_LABEL_1));
		expected.add(new UserSearchField(EXPANDO_FIELD_2, EXPANDO_LABEL_2));

		when(mockJsonFactory.createJSONArray(configuredFields)).thenReturn(mockJsonArray);
		when(mockJsonArray.length()).thenReturn(3);

		when(mockJsonArray.getJSONObject(0)).thenReturn(mockJsonObject1);
		when(mockJsonObject1.getString(FIELD)).thenReturn(FIELD);
		when(mockJsonObject1.getString(LABEL)).thenReturn(LABEL);

		when(mockJsonArray.getJSONObject(1)).thenReturn(mockJsonObject2);
		when(mockJsonObject2.getString(FIELD)).thenReturn(EXPANDO_FIELD_1);
		when(mockJsonObject2.getString(LABEL)).thenReturn(EXPANDO_LABEL_1);

		when(mockJsonArray.getJSONObject(2)).thenReturn(mockJsonObject3);
		when(mockJsonObject3.getString(FIELD)).thenReturn(EXPANDO_FIELD_2);
		when(mockJsonObject3.getString(LABEL)).thenReturn(EXPANDO_LABEL_2);

		when(mockExpandoColumnLocalService.getColumn(companyId, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME, EXPANDO_LABEL_1)).thenReturn(mockExpandoColumn);
		when(mockExpandoColumn.getDisplayName(Locale.UK)).thenReturn(EXPANDO_LABEL_1);

		List<UserSearchField> result = advancePeopleSearchConfigurationService.getSearchFields(companyId, Locale.UK, configuredFields);

		assertThat(result.size(), equalTo(2));

		for (int x = 0; x < 2; x++) {
			assertThat(result.get(x).getField(), equalTo(expected.get(x).getField()));
			assertThat(result.get(x).getLabel(), equalTo(expected.get(x).getLabel()));
		}
	}

	@Test
	public void getTitle_WhenExistTitleForCurrentLocale_ThenReturnTitleForCurrentLocale() {
		String title = "title";
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockTitleMap.get(Locale.UK)).thenReturn(title);
		String result = advancePeopleSearchConfigurationService.getTitle(mockThemeDisplay, mockTitleMap);
		assertThat(result, equalTo(title));
	}

	@Test
	public void getTitle_WhenNotExistTitleForCurrentLocaleAndExistTitleForSiteDefaultLocale_ThenReturnTitleForSiteDefaultLocale() {
		String title = "title";
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockThemeDisplay.getSiteDefaultLocale()).thenReturn(Locale.FRENCH);
		when(mockTitleMap.get(Locale.FRENCH)).thenReturn(title);
		String result = advancePeopleSearchConfigurationService.getTitle(mockThemeDisplay, mockTitleMap);
		assertThat(result, equalTo(title));
	}

	@Test(expected = ConfigurationException.class)
	public void getValidConfiguration_WhenExceptionRetrievingTheConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(AdvancePeopleSearchPortletInstanceConfiguration.class, mockThemeDisplay)).thenThrow(new ConfigurationException());

		advancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay);
	}

	@Test
	public void getValidConfiguration_WhenNoErrorAndConfigurationIsInvalid_ThenReturnsEmptyOptional() throws ConfigurationException {
		when(mockConfigurationProvider.getPortletInstanceConfiguration(AdvancePeopleSearchPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockConfiguration.title()).thenReturn(mockTitleMap);

		Optional<AdvancePeopleSearchPortletInstanceConfiguration> result = advancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay);

		assertFalse(result.isPresent());
	}

	@Test
	public void getValidConfiguration_WhenNoErrorAndConfigurationIsValid_ThenReturnOptionalWithTheConfiguration() throws ConfigurationException, JSONException {
		long companyId = 123;
		String configuredFields = "[]";

		when(mockConfigurationProvider.getPortletInstanceConfiguration(AdvancePeopleSearchPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
		when(mockThemeDisplay.getSiteDefaultLocale()).thenReturn(Locale.UK);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockConfiguration.searchFields()).thenReturn(configuredFields);
		when(mockConfiguration.title()).thenReturn(mockTitleMap);
		when(mockConfiguration.title().get(Locale.UK)).thenReturn("mockTitle");
		when(mockJsonFactory.createJSONArray(configuredFields)).thenReturn(mockJsonArray);
		when(mockJsonArray.length()).thenReturn(1);
		when(mockJsonArray.getJSONObject(0)).thenReturn(mockJsonObject1);
		when(mockJsonObject1.getString(FIELD)).thenReturn(FIELD);
		when(mockJsonObject1.getString(LABEL)).thenReturn(LABEL);

		Optional<AdvancePeopleSearchPortletInstanceConfiguration> result = advancePeopleSearchConfigurationService.getValidConfiguration(mockThemeDisplay);

		assertThat(result.get(), sameInstance(mockConfiguration));
	}
}
