package com.placecube.digitalplace.user.account.configuration;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import com.placecube.digitalplace.user.account.constants.AccountConfigurationConstants;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "users", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.user.account.configuration.AccountCompanyConfiguration", localization = "content/Language", name = "create-account")
public interface AccountCompanyConfiguration {

	@Meta.AD(required = false, deflt = "false", name = "address-out-of-local-area", description = "address-out-of-local-area-description")
	boolean addressOutOfLocalArea();

	@Meta.AD(required = false, deflt = "/create-account", name = "create-account-url", description = "create-account-url-help")
	String createAccountURL();

	@Meta.AD(required = false, deflt = AccountConfigurationConstants.PAGE_PERSONAL_DETAILS, optionLabels = { "Personal Details", "Additional Details" }, optionValues = {
			AccountConfigurationConstants.PAGE_PERSONAL_DETAILS, AccountConfigurationConstants.PAGE_ADDITIONAL_DETAILS }, name = "location-of-email-address")
	String emailAddressPageLocation();

	@Meta.AD(required = false, deflt = "true", name = "email-address-required")
	boolean emailAddressRequired();

	@Meta.AD(required = false, deflt = "/email-address-verification", name = "email-address-verification-url", description = "email-address-verification-url-help")
	String emailAddressVerificationURL();

	@Meta.AD(required = false, deflt = "false", name = "generate-password-when-logged-in-user-create-account", description = "generate-password-when-logged-in-user-create-account-help")
	boolean generatePasswordWhenLoggedInUserCreateAccount();

	@Meta.AD(required = false, deflt = StringPool.BLANK, name = "registration-referer-user-expando-field-name", description = "registration-referer-user-expando-field-name-help")
	String registrationRefererGroupIdUserExpandoFieldName();

	@Meta.AD(required = false, deflt = "false", name = "require-captcha-before-email-address-entry", description = "require-captcha-before-email-address-entry-description")
	boolean requireCaptchaBeforeEmailAddressEntry();

	@Meta.AD(required = false, deflt = StringPool.BLANK, name = "reset-password-article-id", description = "reset-password-article-id-help")
	String resetPasswordArticleId();

	@Meta.AD(required = false, deflt = "false", name = "reset-password-button-in-confirmation-page-when-logged-in-user-create-account", description = "reset-password-button-in-confirmation-page-when-logged-in-user-create-account-help")
	boolean resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount();
}
