package com.placecube.digitalplace.externalpage.web.service;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.SystemProperties;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor(value = { "edu.uci.ics.crawler4j.crawler.CrawlController" })
public class CrawlModelBuilderTest extends PowerMockito {

	@InjectMocks
	private CrawlModelBuilder crawlModelBuilder;

	@Mock
	private CrawlConfig mockCrawlConfig;

	@Mock
	private PageFetcher mockPageFetcher;

	@Mock
	private RobotstxtServer mockRobotstxtServer;

	@Mock
	private RobotstxtConfig mockRobotstxtConfig;

	@Mock
	private CrawlController mockCrawlController;

	@Test
	public void getCrawlConfig_WhenNoError_ThenReturnsCrawlConfig() {
		CrawlConfig result = crawlModelBuilder.getCrawlConfig();

		assertNotNull(result);
		assertThat(result.getCrawlStorageFolder(), equalTo(SystemProperties.get(SystemProperties.TMP_DIR)));
	}

	@Test
	public void getCrawlController_WhenNoError_ThenReturnsCrawlController() throws Exception {
		crawlModelBuilder = spy(new CrawlModelBuilder());
		doReturn(mockCrawlController).when(crawlModelBuilder).getCrawlController(mockCrawlConfig, mockPageFetcher, mockRobotstxtServer);

		CrawlController result = crawlModelBuilder.getCrawlController(mockCrawlConfig, mockPageFetcher, mockRobotstxtServer);

		assertThat(result, sameInstance(mockCrawlController));
	}

	@Test
	public void getPageFetcher_WhenNoError_ThenReturnsPageFetcher() {
		when(mockCrawlConfig.getMaxConnectionsPerHost()).thenReturn(1);
		when(mockCrawlConfig.getMaxTotalConnections()).thenReturn(1);

		PageFetcher result = crawlModelBuilder.getPageFetcher(mockCrawlConfig);

		assertThat(result, instanceOf(PageFetcher.class));
	}

	@Test
	public void getRobotstxtConfig_WhenNoError_ThenReturnsRobotstxtConfig() {
		RobotstxtConfig result = crawlModelBuilder.getRobotstxtConfig();

		assertNotNull(result);
		assertFalse(result.isEnabled());
	}

	@Test
	public void getRobotstxtServer_WhenNoError_ThenReturnsRobotstxtServer1() {
		RobotstxtServer result = crawlModelBuilder.getRobotstxtServer(mockRobotstxtConfig, mockPageFetcher);

		assertNotNull(result);
	}

}
