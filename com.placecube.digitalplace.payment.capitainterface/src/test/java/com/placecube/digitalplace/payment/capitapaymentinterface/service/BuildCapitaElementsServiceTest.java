package com.placecube.digitalplace.payment.capitapaymentinterface.service;

import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_AMOUNT_IN_MINOR_UNITS;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_DESCRIPTION;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_FUND_CODE;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_FORENAME;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_SURNAME;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_TITLE;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_NARRATIVE;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_REFERENCE;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_TAX_VAT_VAT_AMOUNT_IN_MINOR_UNITS;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_TAX_VAT_VAT_CODE;
import static com.placecube.digitalplace.payment.capitainterface.CapitaConstants.SALE_ITEM_TAX_VAT_VAT_RATE;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.apache.axis.types.Token;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.capita_software_services.www.portal_api.PanEntryMethod;
import com.capita_software_services.www.scp.base.LgItemDetails;
import com.capita_software_services.www.scp.base.LgSaleDetails;
import com.capita_software_services.www.scp.base.Routing;
import com.capita_software_services.www.scp.base.SummaryData;
import com.capita_software_services.www.scp.base.TaxItem;
import com.capita_software_services.www.scp.base.ThreePartName;
import com.capita_software_services.www.scp.base.VatItem;
import com.capita_software_services.www.scp.simple.SimpleItem;
import com.capita_software_services.www.scp.simple.SimpleSale;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleConfiguration;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleItemConfiguration;
import com.placecube.digitalplace.payment.capitainterface.service.BuildCapitaElementsService;
import com.placecube.digitalplace.payment.model.PaymentContext;

public class BuildCapitaElementsServiceTest extends PowerMockito {

	private BuildCapitaElementsService buildCapitaElementsService;

	@Mock
	private PaymentContext mockPaymentContext;

	@Mock
	private PaymentProperties mockPaymentProperties;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SimpleSaleConfiguration mockSimpleSaleConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Before
	public void activateSetUp() throws Exception {
		initMocks(this);
		buildCapitaElementsService = new BuildCapitaElementsService(mockPaymentProperties);
	}

	@Test
	public void buildLgSaleDetailsForCNP_WhenNoError_ThenReturnsLgSaleDetails() {
		long realUserId = 1;

		LgSaleDetails result = buildCapitaElementsService.buildLgSaleDetailsForCNP(realUserId);

		assertThat(result.getUserName(), equalTo(String.valueOf(realUserId)));
	}

	@Test
	public void buildRouting_WhenReturnURLIsNotNull_ThenReturnsRouting() {
		String backURL = "http://www.abc.com";
		String returnURL = "http://www.abc2.com";

		Routing routing = buildCapitaElementsService.buildRouting(returnURL, backURL);
		assertEquals(routing.getBackUrl().toString(), backURL);
		assertEquals(routing.getReturnUrl().toString(), returnURL);
	}

	@Test
	public void buildRouting_WhenReturnURLIsNull_ThenReturnsNull() {
		String returnURL = null;

		Routing routing = buildCapitaElementsService.buildRouting(returnURL, "backURL");

		assertNull(routing);
	}

	@Test
	public void buildSale_WhenSalesDescriptionIsNotNullAndAMountInPenceIsInvalid_ThenReturnsNull() {
		String saleDescription = "still not going to pass any money";
		String refCode = "ABC123";
		int amountInPence = 0;

		SimpleSale sale = buildCapitaElementsService.buildSale(saleDescription, amountInPence, refCode, mockSimpleSaleConfiguration);

		assertNull(sale);
	}

	@Test
	public void buildSale_WhenSalesDescriptionIsNotNullAndAMountInPenceIsValidAndListOfItems_ThenReturnsSaleWithTheSummaryDataAndItems() {
		String saleDescription = "still not going to pass any money";
		String refCode = "ABC123";
		int amountInPence = 10;
		String formInstanceRecordId = "formInstanceRecordId123";

		List<SimpleSaleItemConfiguration> listOfItemDetails = new ArrayList<>();
		SimpleSaleItemConfiguration simpleSaleItemConfiguration = getSimpleSaleItemConfiguration();
		listOfItemDetails.add(simpleSaleItemConfiguration);

		when(mockSimpleSaleConfiguration.getSimpleSaleItemConfigurations()).thenReturn(listOfItemDetails);
		when(mockSimpleSaleConfiguration.getPaymentContext()).thenReturn(mockPaymentContext);
		when(mockPaymentContext.getInternalReference()).thenReturn(formInstanceRecordId);

		SimpleSale sale = buildCapitaElementsService.buildSale(saleDescription, amountInPence, refCode, mockSimpleSaleConfiguration);

		assertEquals(sale.getSaleSummary().getDescription(), saleDescription);
		assertEquals(sale.getSaleSummary().getAmountInMinorUnits(), amountInPence);
		assertEquals(sale.getSaleSummary().getReference(), refCode);
		assertEquals(sale.getSaleSummary().getDisplayableReference(), refCode);

		for (SimpleItem simpleItem : sale.getItems()) {
			SummaryData summary = simpleItem.getItemSummary();
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_AMOUNT_IN_MINOR_UNITS), String.valueOf(summary.getAmountInMinorUnits()));
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_DESCRIPTION), summary.getDescription());
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_REFERENCE), summary.getReference());
			assertEquals(refCode, summary.getDisplayableReference());

			LgItemDetails lgItemDetails = simpleItem.getLgItemDetails();
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_FUND_CODE), lgItemDetails.getFundCode());
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_LG_ITEM_DETAILS_NARRATIVE), lgItemDetails.getNarrative());
			assertEquals(formInstanceRecordId, lgItemDetails.getAdditionalReference());

			ThreePartName threePartName = lgItemDetails.getAccountName();
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_FORENAME), threePartName.getForename());
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_SURNAME), threePartName.getSurname());
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_TITLE), threePartName.getTitle());

			TaxItem texItem = simpleItem.getTax();
			VatItem vatItem = texItem.getVat();
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_TAX_VAT_VAT_AMOUNT_IN_MINOR_UNITS), String.valueOf(vatItem.getVatAmountInMinorUnits()));
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_TAX_VAT_VAT_CODE), vatItem.getVatCode());
			assertEquals(simpleSaleItemConfiguration.getSaleItemDetail(SALE_ITEM_TAX_VAT_VAT_RATE), vatItem.getVatRate().toString());
		}

		assertTrue(sale.getItems().length == 1);
	}

	@Test
	public void buildSale_WhenSalesDescriptionIsNotNullAndAMountInPenceIsValidAndNoListOfItems_ThenReturnsSaleWithTheSummaryData() {
		String saleDescription = "still not going to pass any money";
		String refCode = "ABC123";
		int amountInPence = 10;
		List<SimpleSaleItemConfiguration> listOfItemDetails = new ArrayList<>();
		when(mockSimpleSaleConfiguration.getSimpleSaleItemConfigurations()).thenReturn(listOfItemDetails);

		SimpleSale sale = buildCapitaElementsService.buildSale(saleDescription, amountInPence, refCode, mockSimpleSaleConfiguration);

		assertEquals(sale.getSaleSummary().getDescription(), saleDescription);
		assertEquals(sale.getSaleSummary().getAmountInMinorUnits(), amountInPence);
		assertEquals(sale.getSaleSummary().getReference(), refCode);
		assertEquals(sale.getSaleSummary().getDisplayableReference(), refCode);
		assertNull(sale.getItems());

	}

	@Test
	public void buildSale_WhenSalesDescriptionIsNull_ThenReturnsNull() {
		String saleDescription = "";
		int amountInPence = 10;
		String refCode = "ref";
		List<SimpleSaleItemConfiguration> listOfItemDetails = new ArrayList<>();
		when(mockSimpleSaleConfiguration.getSimpleSaleItemConfigurations()).thenReturn(listOfItemDetails);

		SimpleSale sale = buildCapitaElementsService.buildSale(saleDescription, amountInPence, refCode, mockSimpleSaleConfiguration);

		assertNull(sale);
	}

	@Test
	public void createECOMPanEntryMethod_WhenNoError_ThenReturnsECOM() {
		PanEntryMethod pem = buildCapitaElementsService.createECOMPanEntryMethod();
		assertEquals(pem, PanEntryMethod.ECOM);
	}

	@Test
	public void generateToken_WhenUniqueIdIsNotNull_ThenReturnsTheTokenWithTheIdAsValue() {
		String uniqueId = "1234";

		Token token = buildCapitaElementsService.generateToken(uniqueId);

		assertEquals(token, "1234");
	}

	@Test
	public void generateToken_WhenUniqueIdIsNull_ThenReturnsNull() {
		String uniqueId = null;

		Token token = buildCapitaElementsService.generateToken(uniqueId);

		assertNull(token);
	}

	private SimpleSaleItemConfiguration getSimpleSaleItemConfiguration() {
		SimpleSaleItemConfiguration simpleSaleItemConfiguration = SimpleSaleItemConfiguration.newSimpleSaleItemConfiguration();
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_AMOUNT_IN_MINOR_UNITS, "122");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_DESCRIPTION, "description 123");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_REFERENCE, "ref 124");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_FUND_CODE, "fundCode 125");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_FORENAME, "forename 126");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_SURNAME, "surname 127");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_TITLE, "title 128");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_NARRATIVE, "narrative 129");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_TAX_VAT_VAT_AMOUNT_IN_MINOR_UNITS, "130");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_TAX_VAT_VAT_CODE, "vat code 131");
		simpleSaleItemConfiguration.putSaleItemDetail(CapitaConstants.SALE_ITEM_TAX_VAT_VAT_RATE, "1.0");

		return simpleSaleItemConfiguration;
	}
}
