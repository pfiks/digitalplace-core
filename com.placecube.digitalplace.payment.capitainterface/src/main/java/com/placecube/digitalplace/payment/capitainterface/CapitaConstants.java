package com.placecube.digitalplace.payment.capitainterface;

import uk.co.capita_software.support.selfservice.Algorithm;
import uk.co.capita_software.support.selfservice.SubjectType;

/**
 * The shared constants for the Capita payment gateway interface
 *
 */
public final class CapitaConstants {

	/**
	 * String literals which are required in multiple places.
	 * AMOUNT_IN_MINOR_UNITS and SALE_DESCRIPTION for parsing lists of items and
	 * HASH_ALGORITHM and ENCODING_TYPE for building the message digest
	 */
	public static final String ADDITIONAL_INSTRUCTIONS_MERCHANT_CODE = "additionalInstructions.merchantCode";

	/**
	 * Algorithm type for the message digest algorithm
	 */
	public static final Algorithm ALGORITHM = Algorithm.Original;
	public static final String AMOUNT_IN_MINOR_UNITS = "sale.saleSummary.amountInMinorUnits";
	public static final String CAPITA_NAMESPACE = "http://www.capita-software-services.com/scp/simple";

	public static final String CAPITA_NAMESPACE_LOCAL_PART = "scpSoap11";
	public static final String CAPITA_WS_ENDPOINT = "";
	/**
	 * Separator when building the string that we turn into message digest
	 */
	public static final String DIGEST_SEPARATOR = "!";
	public static final String ENCODING_TYPE = "UTF-8";
	public static final String HASH_ALGORITHM = "HmacSHA256";
	public static final String INVOKE_WSDL_OPERATION_NAME = "scpSimpleInvoke";
	public static final String PAN_ENTRY_METHOD = "panEntryMethod";
	public static final String QUERY_WSDL_OPERATION_NAME = "scpSimpleQuery";
	public static final String SALE_DESCRIPTION = "sale.saleSummary.description";
	public static final String SALE_ITEM_AMOUNT_IN_MINOR_UNITS = "sale.item.saleSummary.amountInMinorUnits";
	public static final String SALE_ITEM_DESCRIPTION = "sale.item.saleSummary.description";
	public static final String SALE_ITEM_FUND_CODE = "sale.item.lgItemDetails.fundCode";
	public static final String SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_FORENAME = "sale.item.lgItemDetails.accountName.forename";
	public static final String SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_SURNAME = "sale.item.lgItemDetails.accountName.surname";
	public static final String SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_TITLE = "sale.item.lgItemDetails.accountName.title";
	public static final String SALE_ITEM_LG_ITEM_DETAILS_NARRATIVE = "sale.item.lgItemDetails.narrative";
	public static final String SALE_ITEM_REFERENCE = "sale.item.saleSummary.reference";
	public static final String SALE_ITEM_TAX_VAT_VAT_AMOUNT_IN_MINOR_UNITS = "sale.item.tax.vat.vatAmountInMinorUnits";
	public static final String SALE_ITEM_TAX_VAT_VAT_CODE = "sale.item.tax.vat.vatCode";
	public static final String SALE_ITEM_TAX_VAT_VAT_RATE = "sale.item.tax.vat.vatRate";
	public static final String SALE_REFERENCE = "sale.saleSummary.reference";
	public static final String SOURCE_CODE_DP = "DP";

	public static final SubjectType SUBJECT_TYPE = SubjectType.fromValue("CapitaPortal");

	// This may be set per LOB which uses payment. THerefore it may need to be
	// added into the methods as a parameter
	public static final String TOKEN_PREFIX = "DP_";

	private CapitaConstants() {

	}

}
