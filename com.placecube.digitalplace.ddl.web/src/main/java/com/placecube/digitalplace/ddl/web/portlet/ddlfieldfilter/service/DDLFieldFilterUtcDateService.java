package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.osgi.service.component.annotations.Component;

@Component(immediate = true, service = DDLFieldFilterUtcDateService.class)
public final class DDLFieldFilterUtcDateService {

	private static final ZoneId UTC_ZONE = ZoneId.of("UTC");

	public long getDateWithDaysOffsetInMillis(LocalDate localDate, long numDaysToOffset) {

		return Date.from(localDate.plusDays(numDaysToOffset).atStartOfDay(UTC_ZONE).toInstant()).getTime();

	}
}
