<%@page import="com.placecube.digitalplace.search.web.portlet.category.constants.CategoryFacetDisplayConstants"%>
<%@ include file="../init.jsp"%>

<c:choose>
	
	<c:when test="${invalidConfiguration}">
		<%@ include file="/common/invalid-configuration.jspf" %>
	</c:when>
	
	<c:when test="${isEditLayoutMode && configuration.invisible()}">
		<%@ include file="/common/invisible-portlet-edit-mode.jspf" %>
	</c:when>
	
	<c:otherwise>
		<c:set var="availableResults" value="${availableCategories}" />
		<c:set var="checkboxesPanelCollapsedByDefault" value="${configuration.checkboxesPanelCollapsedByDefault() }" />
		<c:set var="customCssClass" value="${configuration.customCssClass() }" />
		<c:set var="dropDownDisplayStyle" value="<%=CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_DROPDOWN %>" />
		<c:set var="facetFieldName" value="${facetFieldName }" />
		<c:set var="filterColour" value="${configuration.filterColour() }" />
		<c:set var="sectionTitleKey" value="${not empty configuration.customHeading() ? configuration.customHeading() : 'categories'}" />
		<c:set var="hideFrequencies" value="${not configuration.showFrequencies() }" />
		<c:set var="treeDisplayStyle" value="<%=CategoryFacetDisplayConstants.FACET_DISPLAY_STYLE_TREE %>" />
	
		<c:choose>			
			<c:when test="${displayStyle eq dropDownDisplayStyle }">
				<div class="form">
					<%@ include file="facet-dropdown-view.jspf" %>
				</div>
			</c:when>
            <c:when test="${displayStyle eq treeDisplayStyle}">
                <%@ include file="tree-view.jspf" %>
            </c:when>					
			<c:otherwise>
				<%@ include file="../common/facet-checkbox-view.jspf" %>
			</c:otherwise>
		</c:choose>		

	</c:otherwise>
</c:choose>