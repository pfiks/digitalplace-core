<%@ include file="../init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post">

    <aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
    <aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />

    <liferay-frontend:edit-form-body>
        <dp-frontend:fieldset-group>

            <liferay-frontend:fieldset>


				 <aui:input
                        label="date-parameter-name"
                        name="dateParameterName"
                        type="text"
                        value="${ dateParameterName }"
                />
                
                
                <aui:field-wrapper label="title">
                    <liferay-ui:input-localized
                            defaultLanguageId="<%= LocaleUtil.toLanguageId(themeDisplay.getSiteDefaultLocale()) %>"
                            name="title"
                            xml="${title}"
                    />
                </aui:field-wrapper>

            </liferay-frontend:fieldset>

        </dp-frontend:fieldset-group>
    </liferay-frontend:edit-form-body>

    <liferay-frontend:edit-form-footer>
        <aui:button type="submit" />

        <aui:button type="cancel" />
    </liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>