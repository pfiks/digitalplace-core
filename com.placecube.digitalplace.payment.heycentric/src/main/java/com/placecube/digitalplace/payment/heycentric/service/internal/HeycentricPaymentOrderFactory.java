package com.placecube.digitalplace.payment.heycentric.service.internal;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.heycentric.configuration.HeycentricPaymentCompanyConfiguration;
import com.placecube.digitalplace.payment.heycentric.service.constants.HeycentricPaymentConstants;
import com.placecube.digitalplace.payment.model.PaymentContext;

@Component(immediate = true, service = HeycentricPaymentOrderFactory.class)
public class HeycentricPaymentOrderFactory {

	private static final Log LOG = LogFactoryUtil.getLog(HeycentricPaymentOrderFactory.class);

	@Reference
	private HeycentricHashingService heycentricHashingService;

	public String getPaymentURL(HeycentricPaymentCompanyConfiguration heycentricPaymentCompanyConfiguration, PaymentContext paymentContext) throws ConfigurationException {

		String url = heycentricPaymentCompanyConfiguration.baseURL();
		String amount = paymentContext.getAmount().toString();
		url = addParameter(url, HeycentricPaymentConstants.HEADER_PARAMETER_CLIENT, heycentricPaymentCompanyConfiguration.client());
		url = addParameter(url, HeycentricPaymentConstants.HEADER_PARAMETER_ENTITY, heycentricPaymentCompanyConfiguration.entity());
		url = addParameter(url, HeycentricPaymentConstants.HEADER_PARAMETER_AREA, heycentricPaymentCompanyConfiguration.area());
		url = addParameter(url, HeycentricPaymentConstants.HEADER_PARAMETER_TILL, heycentricPaymentCompanyConfiguration.till());
		url = addParameter(url, HeycentricPaymentConstants.HEADER_PARAMETER_RURL, paymentContext.getReturnURL());
		url = addParameter(url, HeycentricPaymentConstants.HEADER_PARAMETER_BURL, paymentContext.getBackURL());
		url = addParameter(url, HeycentricPaymentConstants.LINE_PARAMETER_PMTTYP, heycentricPaymentCompanyConfiguration.pmtTyp());
		url = addParameter(url, HeycentricPaymentConstants.LINE_PARAMETER_VAL1, paymentContext.getSalesReference());
		url = addParameter(url, HeycentricPaymentConstants.LINE_PARAMETER_VAL1DESC, paymentContext.getSalesDescription());
		url = addParameter(url, HeycentricPaymentConstants.LINE_PARAMETER_AM, amount);
		url = addParameter(url, HeycentricPaymentConstants.HEADER_PARAMETER_EMAIL, paymentContext.getEmailAddress());
		url = addParameter(url, HeycentricPaymentConstants.HASH_PARAMETER, getHash(heycentricPaymentCompanyConfiguration, amount));

		LOG.debug("Heycentric payment url:" + url);

		return url;
	}

	private String addParameter(String url, String parameterName, String parameterValue) {
		if (Validator.isNotNull(parameterValue)) {
			String encodedValue = URLEncoder.encode(parameterValue, StandardCharsets.UTF_8);
			if (!url.contains(StringPool.QUESTION)) {
				url += StringPool.QUESTION + parameterName + StringPool.EQUAL + encodedValue;
			} else if (url.endsWith(StringPool.QUESTION)) {
				url += parameterName + StringPool.EQUAL + encodedValue;
			} else {
				url += StringPool.AMPERSAND + parameterName + StringPool.EQUAL + encodedValue;
			}
		}
		return url;
	}

	private String getHash(HeycentricPaymentCompanyConfiguration heycentricPaymentCompanyConfiguration, String amount) throws ConfigurationException {
		String hash = HeycentricPaymentConstants.HASH_PARAMETER_VALUE;
		try {
			if (heycentricPaymentCompanyConfiguration.isHash() && Validator.isNull(heycentricPaymentCompanyConfiguration.privateKey())) {
				ConfigurationException e = new ConfigurationException("Heycentric is configured to use a hash but no private key is configured.");
				LOG.error(e.getMessage());
				throw e;
			} else if (heycentricPaymentCompanyConfiguration.isHash()) {
				String rawHash = heycentricHashingService.getHeycentricRawHashString(heycentricPaymentCompanyConfiguration, amount);
				String md5Hex = heycentricHashingService.rawHashToMd5Hex(rawHash);
				hash = heycentricHashingService.base64EncodeString(md5Hex);
			}
		} catch (NoSuchAlgorithmException e) {
			throw new ConfigurationException(e.getMessage());
		}

		return hash;
	}
}
