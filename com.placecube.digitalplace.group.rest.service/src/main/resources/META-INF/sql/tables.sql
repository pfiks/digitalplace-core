create table Placecube_GroupRest_GroupApiKey (
	companyId LONG not null,
	groupId LONG not null,
	applicationKey TEXT null,
	applicationSecret TEXT null,
	primary key (companyId, groupId)
);

create table Placecube_GroupRest_GroupWebhook (
	webhookId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	name VARCHAR(75) null,
	payloadURL STRING null,
	enabled BOOLEAN,
	events VARCHAR(75) null,
	className VARCHAR(400) null,
	classPK LONG
);