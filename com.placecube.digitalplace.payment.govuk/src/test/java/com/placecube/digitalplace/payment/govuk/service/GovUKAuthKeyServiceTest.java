package com.placecube.digitalplace.payment.govuk.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.payment.govuk.configuration.GovUKCompanyConfiguration;

@RunWith(PowerMockRunner.class)
public class GovUKAuthKeyServiceTest extends PowerMockito {

	private GovUKAuthKeyService govUKAuthKeyService = new GovUKAuthKeyService();

	@Mock
	private GovUKCompanyConfiguration mockGovUKCompanyConfiguration;

	private static final String CONFIGURED_API_KEY = "configureApiKey";

	@Test
	public void getApiAuthKey_WhenConfigForAdditionalApiKeysIsNull_ThenReturnsConfiguredApiKey() throws ConfigurationException {
		when(mockGovUKCompanyConfiguration.apiAuthKey()).thenReturn(CONFIGURED_API_KEY);
		when(mockGovUKCompanyConfiguration.additionalApiAuthKeys()).thenReturn(null);

		String result = govUKAuthKeyService.getApiAuthKey(mockGovUKCompanyConfiguration, "key1");

		assertEquals(CONFIGURED_API_KEY, result);

	}

	@Test
	public void getApiAuthKey_WhenConfigForAdditionalApiKeysIsEmptyArray_ThenReturnsConfiguredApiKey() throws ConfigurationException {
		String[] additionalApiKeys = new String[] {};
		when(mockGovUKCompanyConfiguration.apiAuthKey()).thenReturn(CONFIGURED_API_KEY);
		when(mockGovUKCompanyConfiguration.additionalApiAuthKeys()).thenReturn(additionalApiKeys);

		String result = govUKAuthKeyService.getApiAuthKey(mockGovUKCompanyConfiguration, "key1");

		assertEquals(CONFIGURED_API_KEY, result);

	}
	
	@Test
	public void getApiAuthKey_WhenAdditionalApiKeysArePresentAndMalFormed_ThenReturnsConfiguredApiKey() throws ConfigurationException {
		String[] additionalApiKeys = new String[] { "key1=apiKeyValue1" };
		when(mockGovUKCompanyConfiguration.apiAuthKey()).thenReturn(CONFIGURED_API_KEY);
		when(mockGovUKCompanyConfiguration.additionalApiAuthKeys()).thenReturn(additionalApiKeys);

		String result = govUKAuthKeyService.getApiAuthKey(mockGovUKCompanyConfiguration, "key1");

		assertEquals(CONFIGURED_API_KEY, result);

	}

	@Test
	public void getApiAuthKey_WhenAdditionalApiKeysArePresentAndNotMatchRequestedKey_ThenReturnsConfiguredApiKey() throws ConfigurationException {
		String[] additionalApiKeys = new String[] { "key1:apiKeyValue1" };
		when(mockGovUKCompanyConfiguration.apiAuthKey()).thenReturn(CONFIGURED_API_KEY);
		when(mockGovUKCompanyConfiguration.additionalApiAuthKeys()).thenReturn(additionalApiKeys);

		String result = govUKAuthKeyService.getApiAuthKey(mockGovUKCompanyConfiguration, "key2");

		assertEquals(CONFIGURED_API_KEY, result);

	}
	
	@Test
	public void getApiAuthKey_WhenAdditionalApiKeysArePresentAndMatchRequestedKey_ThenReturnMatchingAdditionalApiKey() throws ConfigurationException {
		String[] additionalApiKeys = new String[] { "key1:apiKeyValue1" };
		when(mockGovUKCompanyConfiguration.apiAuthKey()).thenReturn(CONFIGURED_API_KEY);
		when(mockGovUKCompanyConfiguration.additionalApiAuthKeys()).thenReturn(additionalApiKeys);

		String result = govUKAuthKeyService.getApiAuthKey(mockGovUKCompanyConfiguration, "key1");

		assertEquals("apiKeyValue1", result);

	}
	
	@Test
	public void getApiAuthKey_WhenMultipleAdditionalApiKeysArePresent_ThenReturnsMatchingAdditionalApiKey() throws ConfigurationException {
		String[] additionalApiKeys = new String[] { "key2:apiKeyValue2", "key1:apiKeyValue1" };
		when(mockGovUKCompanyConfiguration.apiAuthKey()).thenReturn(CONFIGURED_API_KEY);
		when(mockGovUKCompanyConfiguration.additionalApiAuthKeys()).thenReturn(additionalApiKeys);

		String result = govUKAuthKeyService.getApiAuthKey(mockGovUKCompanyConfiguration, "key1");

		assertEquals("apiKeyValue1", result);

	}


}
