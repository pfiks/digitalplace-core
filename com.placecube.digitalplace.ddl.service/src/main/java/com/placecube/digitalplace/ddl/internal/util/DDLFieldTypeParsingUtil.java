package com.placecube.digitalplace.ddl.internal.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldOptions;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = DDLFieldTypeParsingUtil.class)
public class DDLFieldTypeParsingUtil {

	private static final Log LOG = LogFactoryUtil.getLog(DDLRecordParsingUtil.class);

	@Reference
	private DLAppLocalService dlAppLocalService;

	@Reference
	private JSONFactory jsonFactory;

	public Optional<FileEntry> getFileEntry(String baseValue) {
		try {
			JSONObject documentLibraryValues = jsonFactory.createJSONObject(baseValue);
			String entryIdFieldName = documentLibraryValues.has("fileEntryId") ? "fileEntryId" : "classPK";
			long fileEntryId = documentLibraryValues.getLong(entryIdFieldName, 0l);
			return Optional.of(dlAppLocalService.getFileEntry(fileEntryId));
		} catch (Exception e) {
			LOG.warn(e);
			return Optional.empty();
		}
	}

	public String getFileEntryAdditionalDetails(Locale locale, Optional<FileEntry> fileEntry) {
		return fileEntry.isPresent() ? LanguageUtil.get(locale, "downloads") + StringPool.COLON + fileEntry.get().getReadCount() : StringPool.BLANK;
	}

	public String getFileEntryTitle(String baseValue, Optional<FileEntry> fileEntry) {
		return fileEntry.isPresent() ? fileEntry.get().getTitle() : baseValue;
	}

	public String getOptionLabel(DDMFormField ddmFormField, Locale locale, String valueString) {
		DDMFormFieldOptions ddmFormFieldOptions = ddmFormField.getDDMFormFieldOptions();
		LocalizedValue optionLabels = ddmFormFieldOptions.getOptionLabels(valueString);
		return Validator.isNotNull(optionLabels) ? optionLabels.getString(locale) : valueString;
	}

	public String getSelectFieldValue(DDMFormField ddmFormField, Locale locale, String baseValue) {
		String valueForField = getValueWithoutBrackets(baseValue);
		if (ddmFormField.isMultiple()) {
		List<String> optionValues = new ArrayList<>();
		String[] values = StringUtil.split(valueForField, StringPool.COMMA);
		for (String val : values) {
			optionValues.add(getOptionLabel(ddmFormField, locale, getValueWithoutWrappingQuotes(val)));
		}
		return optionValues.stream().collect(Collectors.joining(StringPool.COMMA_AND_SPACE));
		} else {
			return getOptionLabel(ddmFormField, locale, valueForField);
		}
	}

	public boolean isValidValue(String valueString) {
		return Validator.isNotNull(valueString) && !"[]".equals(valueString);
	}

	private String getValueWithoutBrackets(String valueString) {
		valueString = StringUtil.replaceFirst(valueString, "[\"", StringPool.BLANK);
		return StringUtil.replaceLast(valueString, "\"]", StringPool.BLANK);
	}

	private String getValueWithoutWrappingQuotes(String valueString) {
		valueString = StringUtil.replaceFirst(valueString, "\"", StringPool.BLANK);
		return StringUtil.replaceLast(valueString, "\"", StringPool.BLANK);
	}

}
