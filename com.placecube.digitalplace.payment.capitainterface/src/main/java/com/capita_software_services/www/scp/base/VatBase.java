/**
 * VatBase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class VatBase  implements java.io.Serializable {
    private java.lang.String vatCode;

    private java.math.BigDecimal vatRate;

    public VatBase() {
    }

    public VatBase(
           java.lang.String vatCode,
           java.math.BigDecimal vatRate) {
           this.vatCode = vatCode;
           this.vatRate = vatRate;
    }


    /**
     * Gets the vatCode value for this VatBase.
     *
     * @return vatCode
     */
    public java.lang.String getVatCode() {
        return vatCode;
    }


    /**
     * Sets the vatCode value for this VatBase.
     *
     * @param vatCode
     */
    public void setVatCode(java.lang.String vatCode) {
        this.vatCode = vatCode;
    }


    /**
     * Gets the vatRate value for this VatBase.
     *
     * @return vatRate
     */
    public java.math.BigDecimal getVatRate() {
        return vatRate;
    }


    /**
     * Sets the vatRate value for this VatBase.
     *
     * @param vatRate
     */
    public void setVatRate(java.math.BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof VatBase)) return false;
        VatBase other = (VatBase) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.vatCode==null && other.getVatCode()==null) ||
             (this.vatCode!=null &&
              this.vatCode.equals(other.getVatCode()))) &&
            ((this.vatRate==null && other.getVatRate()==null) ||
             (this.vatRate!=null &&
              this.vatRate.equals(other.getVatRate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVatCode() != null) {
            _hashCode += getVatCode().hashCode();
        }
        if (getVatRate() != null) {
            _hashCode += getVatRate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(VatBase.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "vatBase"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vatCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "vatCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vatRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "vatRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
