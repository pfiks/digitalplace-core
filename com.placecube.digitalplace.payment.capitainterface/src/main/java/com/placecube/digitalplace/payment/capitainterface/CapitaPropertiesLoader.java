package com.placecube.digitalplace.payment.capitainterface;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.ErrorCode;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.LoggingBean;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.Service;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.SourceSystem;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.service.LoggingFormatService;

/**
 * Helper class to load the Capita properties from file
 *
 */
public class CapitaPropertiesLoader {

	private static final Log LOG = LogFactoryUtil.getLog(CapitaPropertiesLoader.class);

	/**
	 * reads the Capita properties from file and returns them
	 * 
	 * @return Capita properties
	 */
	public Properties getCapitaProperties() {

		// read properties file
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = getClass().getClassLoader().getResourceAsStream("capita-payment-interface.properties");

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			LOG.trace(ex.getStackTrace());
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0055, "")), ex);

		}

		return prop;

	}

}
