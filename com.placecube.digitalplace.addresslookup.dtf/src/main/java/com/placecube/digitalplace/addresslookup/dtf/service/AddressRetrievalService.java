package com.placecube.digitalplace.addresslookup.dtf.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.dtf.configuration.DTFCompanyConfiguration;

@Component(immediate = true, service = AddressRetrievalService.class)
public class AddressRetrievalService {

	private static final Log LOG = LogFactoryUtil.getLog(AddressRetrievalService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private ResponseParserService responseParserService;

	public Connection getConnection(long companyId, String driverClass, String url, String user, String password) throws SQLException, ClassNotFoundException {
		Class.forName(driverClass);
		return DriverManager.getConnection(url, user, password);
	}

	public Optional<ResultSet> executeQuery(Connection conn, String sql) {

		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			return Optional.of(rs);

		} catch (SQLException e) {
			LOG.error("Error executing SQL: " + e.getMessage());
			LOG.debug(e);
		}

		return Optional.empty();
	}

	public void closeConnection(Connection conn) {
		try {
			conn.close();
		} catch (SQLException e) {
			LOG.error("Error closing connection: " + e.getMessage());
			LOG.debug(e);
		}
	}

	public Set<AddressContext> getAddressContexts(ResultSet results) {

		Set<AddressContext> addressLookups = new LinkedHashSet<>();

		try {
			while (results.next()) {
				AddressContext addressContext = this.responseParserService.getAddressContextFromResultSet(results);
				addressLookups.add(addressContext);
			}
		} catch (SQLException se) {
			LOG.error("Error getting address contexts: " + se.getMessage());
			LOG.debug(se);
			return Collections.emptySet();
		}

		return addressLookups;
	}

	public Optional<AddressContext> getAddressContext(ResultSet results) {
		try {
			if (results.next()) {
				return Optional.of(this.responseParserService.getAddressContextFromResultSet(results));
			}
		} catch (SQLException se) {
			LOG.error(se, se);
		}

		return Optional.empty();
	}

	public String getDatabaseClass(long companyId) throws ConfigurationException {
		return this.getConfiguration(companyId).databaseClass();
	}

	public String getDatabaseURL(long companyId) throws ConfigurationException {
		return this.getConfiguration(companyId).databaseURL();
	}

	public String getDatabaseUser(long companyId) throws ConfigurationException {
		return this.getConfiguration(companyId).databaseUser();
	}

	public String getDatabasePassword(long companyId) throws ConfigurationException {
		return this.getConfiguration(companyId).databasePassword();
	}

	public boolean isEnabled(long companyId) throws ConfigurationException {
		DTFCompanyConfiguration configuration = this.configurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, companyId);
		return configuration.enabled();
	}

	public boolean isNational(long companyId) throws ConfigurationException {
		DTFCompanyConfiguration configuration = this.configurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, companyId);
		return configuration.national();
	}

	public Integer getWeight(long companyId) throws ConfigurationException {
		DTFCompanyConfiguration configuration = this.configurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, companyId);
		return configuration.weight();
	}

	private DTFCompanyConfiguration getConfiguration(Long companyId) throws ConfigurationException {

		return this.configurationProvider.getCompanyConfiguration(DTFCompanyConfiguration.class, companyId);
	}
}
