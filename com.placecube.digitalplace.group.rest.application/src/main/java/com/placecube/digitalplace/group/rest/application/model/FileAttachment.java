package com.placecube.digitalplace.group.rest.application.model;

import com.liferay.document.library.kernel.model.DLFileEntry;

public class FileAttachment {

	public static FileAttachment init(String placeholderKey, DLFileEntry dlFileEntry, String jsonObjectForAttachment) {
		return new FileAttachment(placeholderKey, dlFileEntry, jsonObjectForAttachment);
	}

	private final String placeholderKey;
	private final DLFileEntry dlFileEntry;

	private final String jsonValue;

	private FileAttachment(String placeholderKey, DLFileEntry dlFileEntry, String jsonValue) {
		this.placeholderKey = placeholderKey;
		this.dlFileEntry = dlFileEntry;
		this.jsonValue = jsonValue;
	}

	public DLFileEntry getDlFileEntry() {
		return dlFileEntry;
	}

	public String getJsonValue() {
		return jsonValue;
	}

	public String getPlaceholderKey() {
		return placeholderKey;
	}

}
