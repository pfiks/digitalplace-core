package com.placecube.digitalplace.payment.govuk.service;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.govuk.service.commerce.GovUKCommerceConstants;
import com.placecube.digitalplace.payment.govuk.service.internal.GovUKPaymentOrderFactory;
import com.placecube.digitalplace.payment.govuk.service.internal.models.GovUKPaymentProperties;
import com.placecube.digitalplace.payment.model.AgreementStatus;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;
import com.placecube.digitalplace.payment.service.PaymentConnector;

@Component(immediate = true, service = PaymentConnector.class, property = { PaymentConnector.PAYMENT_CONNECTOR_KEY + "=" + GovUKCommerceConstants.PAYMENT_ENGINE_KEY })
public class GovUKPaymentConnector implements PaymentConnector {

	private static final int AGREEMENT_CANCELED = 204;
	private static final Log LOG = LogFactoryUtil.getLog(GovUKPaymentConnector.class);
	private static final int PAYMENT_CREATED = 201;
	private static final int PAYMENT_SUCCESS = 200;

	@Reference
	private GovUKPaymentOrderFactory govUKPaymentOrderFactory;

	@Reference
	private GovUKPaymentService govUKPaymentService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private Portal portal;

	@Override
	public boolean cancelAgreement(String agreementId, ServiceContext serviceContext) {

		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;

		try {
			GovUKPaymentProperties govUKPaymentProperties = govUKPaymentService.getGovUKPaymentProperties(serviceContext);

			String postCancelAgreementURL = govUKPaymentProperties.getBaseURL() + govUKPaymentProperties.getAgreementCancelURL();
			postCancelAgreementURL = postCancelAgreementURL.replace("{AGREEMENT_ID}", agreementId);

			HttpPost httpPost = govUKPaymentOrderFactory.getHttpPostMethod(postCancelAgreementURL, govUKPaymentProperties.getApiAuthKey(), jsonFactory.createJSONObject());

			client = govUKPaymentOrderFactory.getHttpClient();
			response = govUKPaymentOrderFactory.executeRequest(httpPost, client);

			int statusCode = govUKPaymentOrderFactory.getResponseStatusCode(response);

			LOG.debug("Status code for cancel agreement: " + postCancelAgreementURL + " is " + statusCode);

			if (statusCode == AGREEMENT_CANCELED) {
				LOG.info("GovUK Agreement : " + agreementId + " has been cancelled");
				return true;
			}

		} catch (Exception e) {
			LOG.error("Unable to cancel agreement: " + agreementId, e);
		} finally {
			govUKPaymentOrderFactory.quietlyCloseResources(client, response);
		}

		return false;
	}

	@Override
	public AgreementStatus createAgreement(PaymentContext paymentContext, ServiceContext serviceContext) {

		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		AgreementStatus agreementStatus = new AgreementStatus();

		try {
			GovUKPaymentProperties govUKPaymentProperties = govUKPaymentService.getGovUKPaymentProperties(serviceContext);

			JSONObject requestObject = govUKPaymentOrderFactory.createGovUKAgreementRequest(paymentContext);

			String requestJsonString = requestObject.toJSONString();

			LOG.debug("Agreement request:" + requestJsonString);

			HttpPost httpPost = govUKPaymentOrderFactory.getHttpPostMethod(govUKPaymentProperties.getBaseURL() + govUKPaymentProperties.getCreateAgreementURL(), govUKPaymentProperties.getApiAuthKey(),
					requestObject);

			client = govUKPaymentOrderFactory.getHttpClient();
			response = govUKPaymentOrderFactory.executeRequest(httpPost, client);

			int statusCode = govUKPaymentOrderFactory.getResponseStatusCode(response);
			if (statusCode == PAYMENT_CREATED) {
				LOG.debug("Agreement payment created for request: " + requestJsonString);
				agreementStatus = govUKPaymentOrderFactory.getResponseAgreementStatus(response);
			}

		} catch (Exception e) {
			LOG.error(e);
		} finally {
			govUKPaymentOrderFactory.quietlyCloseResources(client, response);
		}

		return agreementStatus;
	}

	@Override
	public boolean enabled(long companyId) {
		try {
			return govUKPaymentService.isEnabled(companyId);
		} catch (Exception e) {
			LOG.error(e);
			return false;
		}
	}

	@Override
	public AgreementStatus getAgreementStatus(String agreementId, ServiceContext serviceContext) {

		AgreementStatus agreementStatus = new AgreementStatus();
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		try {
			GovUKPaymentProperties govUKPaymentProperties = govUKPaymentService.getGovUKPaymentProperties(serviceContext);

			LOG.debug("Getting agreement status. Api auth key:" + govUKPaymentProperties.getApiAuthKey() + " agreementId:" + agreementId);

			if (Validator.isNotNull(agreementId)) {
				String getAgreementStatusURL = govUKPaymentProperties.getBaseURL() + govUKPaymentProperties.getAgreementStatusURL();
				getAgreementStatusURL = getAgreementStatusURL.replace("{AGREEMENT_ID}", agreementId);
				HttpGet httpGet = govUKPaymentOrderFactory.getHttpGetMethod(getAgreementStatusURL, govUKPaymentProperties.getApiAuthKey());
				client = govUKPaymentOrderFactory.getHttpClient();
				response = govUKPaymentOrderFactory.executeRequest(httpGet, client);

				int statusCode = govUKPaymentOrderFactory.getResponseStatusCode(response);

				if (statusCode == 200) {
					agreementStatus = govUKPaymentOrderFactory.getResponseAgreementStatus(response);
				}
			}
		} catch (Exception e) {
			LOG.error(e);
		} finally {
			govUKPaymentOrderFactory.quietlyCloseResources(client, response);
		}

		return agreementStatus;
	}

	@Override
	public Optional<String> getBackURL(String paymentReference, ServiceContext serviceContext) {
		return Optional.ofNullable(GetterUtil.getString(serviceContext.getAttribute("redirect"), null));
	}

	@Override
	public PaymentStatus getPaymentStatus(String paymentReference, ServiceContext serviceContext) {

		PaymentStatus paymentStatus = PaymentStatus.paymentError();
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		try {
			GovUKPaymentProperties govUKPaymentProperties = govUKPaymentService.getGovUKPaymentProperties(serviceContext);

			String paymentId = govUKPaymentOrderFactory.getPaymentIdFromSession(serviceContext.getRequest());

			LOG.debug("Getting payment status. Api auth key:" + govUKPaymentProperties.getApiAuthKey() + " PaymentId:" + paymentId);

			if (Validator.isNotNull(paymentId)) {
				String getPaymentStatusURL = govUKPaymentProperties.getBaseURL() + govUKPaymentProperties.getPaymentStatusURL();
				getPaymentStatusURL = getPaymentStatusURL.replace("{PAYMENT_ID}", paymentId);
				HttpGet httpGet = govUKPaymentOrderFactory.getHttpGetMethod(getPaymentStatusURL, govUKPaymentProperties.getApiAuthKey());
				client = govUKPaymentOrderFactory.getHttpClient();
				response = govUKPaymentOrderFactory.executeRequest(httpGet, client);

				int statusCode = govUKPaymentOrderFactory.getResponseStatusCode(response);
				if (statusCode == PAYMENT_SUCCESS) {
					JSONObject paymentStatusJSON = govUKPaymentOrderFactory.getResponseStatusJSON(response);

					if (PaymentStatus.success().getValue().equalsIgnoreCase(paymentStatusJSON.getString("status"))) {
						paymentStatus = PaymentStatus.success();
					} else {
						paymentStatus = PaymentStatus.paymentError(GetterUtil.getString(paymentStatusJSON.getString("code")), GetterUtil.getString(paymentStatusJSON.getString("message")));
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Unable to get payment status for paymentReference: " + paymentReference, e);
			return PaymentStatus.appError();
		} finally {
			govUKPaymentOrderFactory.quietlyCloseResources(client, response);
		}
		return paymentStatus;
	}

	@Override
	public PaymentResponse preparePayment(PaymentContext paymentContext, ServiceContext serviceContext) {
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		String paymentURL = StringPool.BLANK;
		PaymentStatus paymentStatus = PaymentStatus.appError();
		try {
			GovUKPaymentProperties govUKPaymentProperties = govUKPaymentService.getGovUKPaymentProperties(serviceContext);

			JSONObject requestObject = govUKPaymentOrderFactory.createGovUKPaymentRequest(paymentContext);
			LOG.debug("Payment request:" + requestObject.toJSONString());
			HttpPost httpPost = govUKPaymentOrderFactory.getHttpPostMethod(govUKPaymentProperties.getBaseURL() + govUKPaymentProperties.getCreatePaymentURL(), govUKPaymentProperties.getApiAuthKey(),
					requestObject);

			client = govUKPaymentOrderFactory.getHttpClient();
			response = govUKPaymentOrderFactory.executeRequest(httpPost, client);

			int statusCode = govUKPaymentOrderFactory.getResponseStatusCode(response);
			if (statusCode == PAYMENT_CREATED) {
				JSONObject resultJson = govUKPaymentOrderFactory.getPaymentURLJSON(response);

				if (Validator.isNotNull(resultJson)) {
					paymentURL = resultJson.getString("href");

					HttpServletRequest httpServletRequest = portal.getOriginalServletRequest(serviceContext.getRequest());
					HttpSession httpSession = httpServletRequest.getSession();
					httpSession.setAttribute(GovUKPaymentOrderFactory.PAYMENT_ID, resultJson.getString(GovUKPaymentOrderFactory.PAYMENT_ID));
				}

				if (Validator.isNotNull(paymentURL) && Validator.isUrl(paymentURL)) {
					paymentStatus = PaymentStatus.success();
				}
			}

		} catch (Exception e) {
			LOG.error("Unable to repare payment", e);
			paymentStatus = PaymentStatus.appError();
		} finally {
			govUKPaymentOrderFactory.quietlyCloseResources(client, response);
		}
		return new PaymentResponse(paymentURL, paymentStatus);
	}

	@Override
	public PaymentResponse prepareRecurringPayment(PaymentContext paymentContext, ServiceContext serviceContext) {
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		String paymentURL = StringPool.BLANK;
		PaymentStatus paymentStatus = PaymentStatus.appError();
		AgreementStatus agreementStatus = new AgreementStatus();
		try {
			GovUKPaymentProperties govUKPaymentProperties = govUKPaymentService.getGovUKPaymentProperties(serviceContext);

			agreementStatus = createAgreement(paymentContext, serviceContext);

			JSONObject requestObject = govUKPaymentOrderFactory.createGovUKFirstRecurringPaymentRequest(paymentContext, agreementStatus.getAgreementId());

			LOG.debug("First recurring payment request:" + requestObject.toJSONString());

			HttpPost httpPost = govUKPaymentOrderFactory.getHttpPostMethod(govUKPaymentProperties.getBaseURL() + govUKPaymentProperties.getCreatePaymentURL(), govUKPaymentProperties.getApiAuthKey(),
					requestObject);

			client = govUKPaymentOrderFactory.getHttpClient();
			response = govUKPaymentOrderFactory.executeRequest(httpPost, client);

			int statusCode = govUKPaymentOrderFactory.getResponseStatusCode(response);
			if (statusCode == PAYMENT_CREATED) {
				JSONObject resultJson = govUKPaymentOrderFactory.getPaymentURLJSON(response);

				if (Validator.isNotNull(resultJson)) {
					paymentURL = resultJson.getString("href");

					HttpServletRequest httpServletRequest = portal.getOriginalServletRequest(serviceContext.getRequest());
					HttpSession httpSession = httpServletRequest.getSession();
					httpSession.setAttribute(GovUKPaymentOrderFactory.PAYMENT_ID, resultJson.getString(GovUKPaymentOrderFactory.PAYMENT_ID));
				}

				if (Validator.isNotNull(paymentURL) && Validator.isUrl(paymentURL)) {
					paymentStatus = PaymentStatus.success();
				}
			}

		} catch (Exception e) {
			LOG.error(e);
			paymentStatus = PaymentStatus.appError();
		} finally {
			govUKPaymentOrderFactory.quietlyCloseResources(client, response);
		}
		return new PaymentResponse(paymentURL, paymentStatus, agreementStatus);
	}

	@Override
	public PaymentResponse takeRecurringPayment(PaymentContext paymentContext, String agreementId, String idempotencyKey, ServiceContext serviceContext) {
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		String paymentURL = StringPool.BLANK;
		PaymentStatus paymentStatus = PaymentStatus.appError();
		try {
			GovUKPaymentProperties govUKPaymentProperties = govUKPaymentService.getGovUKPaymentProperties(serviceContext);

			JSONObject requestObject = govUKPaymentOrderFactory.createGovUKFollowingRecurringPaymentRequest(paymentContext, agreementId);
			LOG.debug("Following recurring payment request:" + requestObject.toJSONString());
			HttpPost httpPost = govUKPaymentOrderFactory.getHttpPostMethod(govUKPaymentProperties.getBaseURL() + govUKPaymentProperties.getCreatePaymentURL(), govUKPaymentProperties.getApiAuthKey(),
					requestObject);
			httpPost.addHeader("Idempotency-Key", idempotencyKey);

			client = govUKPaymentOrderFactory.getHttpClient();
			response = govUKPaymentOrderFactory.executeRequest(httpPost, client);

			int statusCode = govUKPaymentOrderFactory.getResponseStatusCode(response);
			if (statusCode == PAYMENT_CREATED) {
				JSONObject paymentJSON = govUKPaymentOrderFactory.getResponseRecurringPaymentStatusJSON(response);

				if (PaymentStatus.created().getValue().equalsIgnoreCase(paymentJSON.getString("status"))) {
					paymentStatus = PaymentStatus.created();
				} else {
					paymentStatus = PaymentStatus.paymentError(GetterUtil.getString(paymentJSON.getString("code")), GetterUtil.getString(paymentJSON.getString("message")));
				}
			} else if (statusCode == PAYMENT_SUCCESS) {
				paymentStatus = PaymentStatus.success();
			}

		} catch (Exception e) {
			LOG.error(e);
			paymentStatus = PaymentStatus.appError();
		} finally {
			govUKPaymentOrderFactory.quietlyCloseResources(client, response);
		}
		return new PaymentResponse(paymentURL, paymentStatus);
	}

}
