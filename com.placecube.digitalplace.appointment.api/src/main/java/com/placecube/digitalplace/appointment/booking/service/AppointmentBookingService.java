package com.placecube.digitalplace.appointment.booking.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.liferay.portal.kernel.exception.PortalException;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingEntry;
import com.placecube.digitalplace.appointment.booking.model.AppointmentContext;
import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;

public interface AppointmentBookingService {

	/**
	 * Create a new appointment for the supplied serviceId
	 *
	 * @param companyId the companyId
	 * @param serviceId the service id
	 * @param appointmentEntry the appointmentEntry to book
	 * @throws PortalException if the appointment entry is invalid and cannot be
	 *             created
	 */
	void bookAppointment(long companyId, String serviceId, AppointmentBookingEntry appointmentEntry) throws PortalException;

	/**
	 * Cancel the event specified by the given appointmentId
	 *
	 * @param companyId the companyId
	 * @param serviceId the service id
	 * @param appointmentEntry the appointment entry to cancel
	 */
	void cancelAppointment(long companyId, String serviceId, AppointmentBookingEntry appointmentEntry);

	List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, AppointmentContext appointmentContext);

	/**
	 * Gets a list of appointments on the given date interval for the specified
	 * serviceId
	 *
	 * @param companyId the companyId
	 * @param serviceId the service id
	 * @param startDate the startDate
	 * @param endDate the endDate
	 * @param timeZone the timeZone
	 * @return list of appointments
	 */
	List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, Date startDate, Date endDate, TimeZone timeZone);

	/**
	 * Gets a list of appointments for the specified appointmentId in any
	 * calendars configured for the service
	 *
	 * @param companyId the companyId
	 * @param serviceId the service id
	 * @param appointmentId the appointmentId
	 * @return list of appointments
	 */
	List<AppointmentBookingEntry> getAppointments(long companyId, String serviceId, String appointmentId);

	List<AppointmentBookingSlot> getAppointmentSlots(long companyId, String serviceId, AppointmentContext appointmentContext) throws ParseException;

	/**
	 * Gets a list of the next 'numberOfDatesToReturn' dates for the specific
	 * day of the week that do not have more than
	 * 'onlyDatesWithLessThanXappoinments' appointments already booked. Dates
	 * are evaluated starting from today's midnight.
	 *
	 * @param companyId the companyId
	 * @param serviceId the service id
	 * @param numberOfDatesToReturn the max number of results to return
	 * @param dayOfWeek the day of week
	 * @param timeZone the timeZone
	 * @param onlyDatesWithLessThanXappoinments the max number of appoinments
	 *            already in the day
	 * @return list of appointments
	 */
	List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int numberOfDatesToReturn, int dayOfWeek, TimeZone timeZone, int onlyDatesWithLessThanXappoinments);

	/**
	 * Gets a list of the next x dates for the specific day of the week that do
	 * not have more than y appointments already booked. Dates are evaluated
	 * starting from today's midnight. x and y numbers are based on the
	 * configured serviceId
	 *
	 * @param companyId the companyId
	 * @param serviceId the service id
	 * @param dayOfWeek the day of week
	 * @param timeZone the timeZone
	 * @return list of appointments
	 */
	List<Date> getDatesWithAvailableAppoinmentsForDayOfWeek(long companyId, String serviceId, int dayOfWeek, TimeZone timeZone);

}
