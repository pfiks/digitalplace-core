package com.placecube.digitalplace.ddl.web.portlet.display.configuration;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.portlet.display.service.DDLCategorisationService;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.DDL_DISPLAY_PORTLET, service = ConfigurationAction.class)
public class DDLDisplayPortletConfigurationAction extends DefaultConfigurationAction {

	private static final String DDM_STRUCTURE_ID = "ddmStructureId";

	private static final String ENABLE_TRANSLATIONS = "enableTranslations";

	private static final String MANDATORY_CATEGORY_IDS = "mandatoryCategoryIds";

	private static final String MANDATORY_VOCABULARY_IDS = "mandatoryVocabularyIds";

	private static final String SHOW_LOGIN_OPTION = "showLoginOption";

	@Reference
	private DDLCategorisationService ddlCategorisationService;

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		DDLDisplayPortletInstanceConfiguration configuration = ddlConfigurationService.getDDLDisplayPortletInstanceConfiguration(themeDisplay);

		List<AssetVocabulary> availableVocabularies = ddlCategorisationService.getAvailableVocabularies(themeDisplay);
		List<AssetCategory> availableCategories = ddlCategorisationService.getAvailableCategories(availableVocabularies);

		httpServletRequest.setAttribute("configuration", configuration);
		httpServletRequest.setAttribute("availableStructures", ddlConfigurationService.getAvailableDDMStructures(themeDisplay.getScopeGroup()));
		httpServletRequest.setAttribute(MANDATORY_CATEGORY_IDS, ddlConfigurationService.getLongValuesAsList(configuration.mandatoryCategoryIds()));
		httpServletRequest.setAttribute(MANDATORY_VOCABULARY_IDS, ddlConfigurationService.getLongValuesAsList(configuration.mandatoryVocabularyIds()));
		httpServletRequest.setAttribute("availableCategories", availableCategories);
		httpServletRequest.setAttribute("availableVocabularies", availableVocabularies);

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long ddmStructureId = ParamUtil.getLong(actionRequest, DDM_STRUCTURE_ID, 0);
		boolean enableTranslations = ParamUtil.getBoolean(actionRequest, ENABLE_TRANSLATIONS, false);
		boolean showLoginOption = ParamUtil.getBoolean(actionRequest, SHOW_LOGIN_OPTION, true);
		String[] mandatoryCategoryIdsSelected = ParamUtil.getStringValues(actionRequest, MANDATORY_CATEGORY_IDS, new String[0]);
		String[] mandatoryVocabularyIdsSelected = ParamUtil.getStringValues(actionRequest, MANDATORY_VOCABULARY_IDS, new String[0]);

		setPreference(actionRequest, ENABLE_TRANSLATIONS, String.valueOf(enableTranslations));
		setPreference(actionRequest, MANDATORY_CATEGORY_IDS, mandatoryCategoryIdsSelected);
		setPreference(actionRequest, MANDATORY_VOCABULARY_IDS, mandatoryVocabularyIdsSelected);
		setPreference(actionRequest, DDM_STRUCTURE_ID, String.valueOf(ddmStructureId));
		setPreference(actionRequest, SHOW_LOGIN_OPTION, String.valueOf(showLoginOption));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
