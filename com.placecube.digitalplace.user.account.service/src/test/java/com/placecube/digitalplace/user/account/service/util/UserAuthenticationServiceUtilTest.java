package com.placecube.digitalplace.user.account.service.util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleDisplay;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PasswordPolicy;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.portlet.PortletRequestModel;
import com.liferay.portal.kernel.service.PasswordPolicyLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.user.account.configuration.UserAuthenticationCompanyConfiguration;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserAuthenticationServiceUtilTest {

	private static final long COMPANY_ID = 32543L;
	private static final long PASSWORD_POLICY_GROUP_ID = 2342L;

	private static final String ARTICLE_ID = "3453";
	private static final String ARTICLE_TEMPLATE_KEY = "template";
	private static final String LANGUAGE_ID = "en_GB";
	private static final double ARTICLE_VERSION = 432d;

	@InjectMocks
	private UserAuthenticationServiceUtil userAuthenticationServiceUtil;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private PasswordPolicyLocalService mockPasswordPolicyLocalService;

	@Mock
	private UserAuthenticationCompanyConfiguration mockUserAuthenticationCompanyConfiguration;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletResponse mockPortletResponse;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleDisplay mockJournalArticleDisplay;

	@Mock
	private PortletRequestModel mockPortletRequestModel;

	@Mock
	private PasswordPolicy mockPasswordPolicy;

	@Mock
	private Portal mockPortal;

	@Test
	public void getDefaultPasswordPolicyDescription_WhenNoErrors_ThenReturnsDefaultPasswordPolicyDescription() throws Exception {
		final String description = "description";

		when(mockPasswordPolicyLocalService.getDefaultPasswordPolicy(COMPANY_ID)).thenReturn(mockPasswordPolicy);
		when(mockPasswordPolicy.getDescription()).thenReturn(description);

		String result = userAuthenticationServiceUtil.getDefaultPasswordPolicyDescription(COMPANY_ID);

		assertThat(result, equalTo(description));
	}

	@Test
	public void getDefaultPasswordPolicyDescription_WhenDefaultPasswordPolicyDoesNotExist_ThenReturnsEmptyString() throws Exception {
		when(mockPasswordPolicyLocalService.getDefaultPasswordPolicy(COMPANY_ID)).thenThrow(new PortalException());

		String result = userAuthenticationServiceUtil.getDefaultPasswordPolicyDescription(COMPANY_ID);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getConfiguredPasswordPolicyArticleContent_WhenConfiguredPassworPolicyArticleIdIsNotEmpty_ThenReturnsPasswordPolicyArticleContent() throws Exception {
		final String content = "content";

		mockConfiguration();
		mockJournalArticle();

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockJournalArticleDisplay.getContent()).thenReturn(content);

		String result = userAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(mockPortletRequestModel, mockThemeDisplay, LANGUAGE_ID);

		assertThat(result, equalTo(content));
	}

	@Test
	public void getConfiguredPasswordPolicyArticleContent_WhenConfiguredPassworPolicyArticleIdIsEmpty_ThenReturnsEmptyString() throws Exception {
		mockConfiguration();

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUserAuthenticationCompanyConfiguration.passwordPolicyArticleId()).thenReturn(StringPool.BLANK);

		String result = userAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(mockPortletRequestModel, mockThemeDisplay, LANGUAGE_ID);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test(expected = PortalException.class)
	public void getConfiguredPasswordPolicyArticleContent_WhenPasswordPolicyArticleDoesNotExist_ThenThrowsPortalException() throws Exception {
		mockConfiguration();
		mockJournalArticle();

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockJournalArticleLocalService.getLatestArticle(PASSWORD_POLICY_GROUP_ID, ARTICLE_ID, WorkflowConstants.STATUS_APPROVED)).thenThrow(new PortalException());

		userAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(mockPortletRequestModel, mockThemeDisplay, LANGUAGE_ID);
	}

	@Test(expected = PortalException.class)
	public void getConfiguredPasswordPolicyArticleContent_WhenPasswordPolicyArticleDisplayCannotBeRetrieved_ThenThrowsPortalException() throws Exception {
		mockConfiguration();
		mockJournalArticle();

		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockJournalArticleLocalService.getArticleDisplay(PASSWORD_POLICY_GROUP_ID, ARTICLE_ID, ARTICLE_VERSION, ARTICLE_TEMPLATE_KEY, Constants.VIEW, LANGUAGE_ID, 1, mockPortletRequestModel,
				mockThemeDisplay)).thenThrow(new PortalException());

		userAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(mockPortletRequestModel, mockThemeDisplay, LANGUAGE_ID);
	}

	@Test(expected = PortletException.class)
	public void getConfiguredPasswordPolicyArticleContent_WhenUserAuthenticationConfigurationRetrievalFails_ThenThrowsPortletException() throws Exception {
		when(mockConfigurationProvider.getCompanyConfiguration(UserAuthenticationCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());
		when(mockThemeDisplay.getCompanyId()).thenReturn(COMPANY_ID);

		userAuthenticationServiceUtil.getConfiguredPasswordPolicyArticleContent(mockPortletRequestModel, mockThemeDisplay, LANGUAGE_ID);
	}

	@Test
	public void getPortletRequestModel_WhenNoErrors_ThenReturnsPortletRequestModel() {
		when(mockPortletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_RESPONSE)).thenReturn(mockPortletResponse);

		PortletRequestModel result = userAuthenticationServiceUtil.getPortletRequestModel(mockPortletRequest);

		assertThat(result, notNullValue());
		assertThat(result.getPortletRequest(), sameInstance(mockPortletRequest));
		assertThat(result.getPortletResponse(), sameInstance(mockPortletResponse));
	}

	private void mockJournalArticle() throws PortalException {
		when(mockJournalArticleLocalService.getLatestArticle(PASSWORD_POLICY_GROUP_ID, ARTICLE_ID, WorkflowConstants.STATUS_APPROVED)).thenReturn(mockJournalArticle);

		when(mockJournalArticle.getGroupId()).thenReturn(PASSWORD_POLICY_GROUP_ID);
		when(mockJournalArticle.getArticleId()).thenReturn(ARTICLE_ID);
		when(mockJournalArticle.getVersion()).thenReturn(ARTICLE_VERSION);
		when(mockJournalArticle.getDDMTemplateKey()).thenReturn(ARTICLE_TEMPLATE_KEY);

		when(mockJournalArticleLocalService.getArticleDisplay(PASSWORD_POLICY_GROUP_ID, ARTICLE_ID, ARTICLE_VERSION, ARTICLE_TEMPLATE_KEY, Constants.VIEW, LANGUAGE_ID, 1, mockPortletRequestModel,
				mockThemeDisplay)).thenReturn(mockJournalArticleDisplay);

	}

	private void mockConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(UserAuthenticationCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockUserAuthenticationCompanyConfiguration);
		when(mockUserAuthenticationCompanyConfiguration.passwordPolicyArticleId()).thenReturn(ARTICLE_ID);
		when(mockUserAuthenticationCompanyConfiguration.passwordPolicyGroupId()).thenReturn(PASSWORD_POLICY_GROUP_ID);
	}
}
