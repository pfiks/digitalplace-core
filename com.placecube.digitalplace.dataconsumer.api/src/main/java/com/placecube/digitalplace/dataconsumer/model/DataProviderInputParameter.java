package com.placecube.digitalplace.dataconsumer.model;

public interface DataProviderInputParameter {

	String getLabel();

	String getName();

	String getType();

	boolean isRequired();

}
