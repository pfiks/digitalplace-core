package com.placecube.digitalplace.editor.configuration;

import java.util.Optional;

import org.junit.runner.RunWith;
import org.osgi.service.component.annotations.Reference;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;

@RunWith(PowerMockRunner.class)
public class CKEditorConfigurationServiceTest extends PowerMockito {

	private static final Log LOG = LogFactoryUtil.getLog(CKEditorConfigurationService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JSONFactory jsonFactory;

	public Optional<JSONObject> getPluginAndRolesJsonConfiguration(long companyId) {

		try {
			return Optional.of(jsonFactory.createJSONObject(configurationProvider.getCompanyConfiguration(CKEditorCompanyConfiguration.class, companyId).pluginAndRolesJsonConfiguration()));
		} catch (Exception e) {
			LOG.error("Unable to retrieve CKEditorCompanyConfiguration for companyId: " + companyId);
			return Optional.empty();
		}
	}
}
