package com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.configuration;

import com.liferay.portal.kernel.settings.LocalizedValuesMap;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.search.web.portlet.expiredcontentfilter.configuration.ExpiredContentFilterSearchPortletInstanceConfiguration")
public interface ExpiredContentFilterSearchPortletInstanceConfiguration {

	@Meta.AD(required = false)
	String dateParameterName();

	@Meta.AD(required = false)
	LocalizedValuesMap title();
}
