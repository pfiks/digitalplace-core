/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.externalpage.service.persistence.impl;

import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.model.BaseModel;

import com.placecube.digitalplace.externalpage.model.ExternalPageTable;
import com.placecube.digitalplace.externalpage.model.impl.ExternalPageImpl;
import com.placecube.digitalplace.externalpage.model.impl.ExternalPageModelImpl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.service.component.annotations.Component;

/**
 * The arguments resolver class for retrieving value from ExternalPage.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(
	property = {
		"class.name=com.placecube.digitalplace.externalpage.model.impl.ExternalPageImpl",
		"table.name=Placecube_ExternalPage_ExternalPage"
	},
	service = ArgumentsResolver.class
)
public class ExternalPageModelArgumentsResolver implements ArgumentsResolver {

	@Override
	public Object[] getArguments(
		FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
		boolean original) {

		String[] columnNames = finderPath.getColumnNames();

		if ((columnNames == null) || (columnNames.length == 0)) {
			if (baseModel.isNew()) {
				return new Object[0];
			}

			return null;
		}

		ExternalPageModelImpl externalPageModelImpl =
			(ExternalPageModelImpl)baseModel;

		long columnBitmask = externalPageModelImpl.getColumnBitmask();

		if (!checkColumn || (columnBitmask == 0)) {
			return _getValue(externalPageModelImpl, columnNames, original);
		}

		Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
			finderPath);

		if (finderPathColumnBitmask == null) {
			finderPathColumnBitmask = 0L;

			for (String columnName : columnNames) {
				finderPathColumnBitmask |=
					externalPageModelImpl.getColumnBitmask(columnName);
			}

			_finderPathColumnBitmasksCache.put(
				finderPath, finderPathColumnBitmask);
		}

		if ((columnBitmask & finderPathColumnBitmask) != 0) {
			return _getValue(externalPageModelImpl, columnNames, original);
		}

		return null;
	}

	@Override
	public String getClassName() {
		return ExternalPageImpl.class.getName();
	}

	@Override
	public String getTableName() {
		return ExternalPageTable.INSTANCE.getTableName();
	}

	private static Object[] _getValue(
		ExternalPageModelImpl externalPageModelImpl, String[] columnNames,
		boolean original) {

		Object[] arguments = new Object[columnNames.length];

		for (int i = 0; i < arguments.length; i++) {
			String columnName = columnNames[i];

			if (original) {
				arguments[i] = externalPageModelImpl.getColumnOriginalValue(
					columnName);
			}
			else {
				arguments[i] = externalPageModelImpl.getColumnValue(columnName);
			}
		}

		return arguments;
	}

	private static final Map<FinderPath, Long> _finderPathColumnBitmasksCache =
		new ConcurrentHashMap<>();

}