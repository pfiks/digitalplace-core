package com.placecube.digitalplace.form.service.impl.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMDataProviderInstance;
import com.liferay.dynamic.data.mapping.service.DDMDataProviderInstanceLocalService;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.form.model.FormDefinition;

@Component(immediate = true, service = DDMDataProviderService.class)
public class DDMDataProviderService {

	private static final Log LOG = LogFactoryUtil.getLog(DDMDataProviderService.class);

	@Reference
	private FormDefinitionDeserializerService formDefinitionDeserializerService;

	@Reference
	private DDMDataProviderInstanceLocalService ddmDataProviderInstanceLocalService;

	@Reference
	private ResourcePermissionLocalService resourcePermissionLocalService;

	@Reference
	private RoleLocalService roleLocalService;

	public List<DDMDataProviderInstance> getOrCreateDataProviders(FormDefinition formDefinition, ServiceContext serviceContext) throws IOException {

		List<DDMDataProviderInstance> ddmDataProviderInstances = new ArrayList<>();

		long groupId = serviceContext.getScopeGroupId();
		long companyId = serviceContext.getCompanyId();

		Map<String, DDMFormValues> ddmDataProviderFormValues = formDefinitionDeserializerService.getDDMDataProviderFormValues(formDefinition, serviceContext);

		ddmDataProviderFormValues.forEach((dataProviderName, ddmFormValues) -> {

			Optional<DDMDataProviderInstance> existingProvider = getDDMDataProviderInstance(groupId, dataProviderName);

			if (existingProvider.isPresent()) {
				LOG.debug("Data provider " + dataProviderName + " already exists");
				ddmDataProviderInstances.add(existingProvider.get());
			} else {
				try {
					Map<Locale, String> nameMap = getLocalizedMap(LanguageUtil.getAvailableLocales(), dataProviderName);
					DDMDataProviderInstance ddmDataProviderInstance = ddmDataProviderInstanceLocalService.addDataProviderInstance(serviceContext.getUserId(), serviceContext.getScopeGroupId(), nameMap,
							nameMap, ddmFormValues, "rest", serviceContext);

					addRolePermission(companyId, String.valueOf(ddmDataProviderInstance.getDataProviderInstanceId()), DDMDataProviderInstance.class.getName(), "Guest",
							new String[] { ActionKeys.VIEW });

					ddmDataProviderInstances.add(ddmDataProviderInstance);
				} catch (PortalException pe) {
					LOG.error("Error creating data provider: " + pe.getMessage(), pe);
				}
			}
		});

		return ddmDataProviderInstances;
	}

	public Map<String, String> getDataProviderPlaceholders(List<DDMDataProviderInstance> ddmDataProviderInstances, Locale locale) {
		Map<String, String> dataProviderPlaceholders = new HashMap<>();

		ddmDataProviderInstances.forEach(ddmDataProvider -> {
			String dataProviderName = ddmDataProvider.getName(locale).toUpperCase();
			dataProviderName = dataProviderName.replace(" ", "_");
			dataProviderPlaceholders.put(dataProviderName + "_DATA_PROVIDER_UUID", ddmDataProvider.getUuid());
		});

		return dataProviderPlaceholders;
	}

	public Optional<DDMDataProviderInstance> getDDMDataProviderInstance(long groupId, String name) {
		return ddmDataProviderInstanceLocalService.getDataProviderInstances(new long[] { groupId }).stream().filter(entry -> name.equalsIgnoreCase(entry.getName(entry.getDefaultLanguageId())))
				.findFirst();
	}

	public Map<Locale, String> getLocalizedMap(Set<Locale> availableLocales, String value) {
		Map<Locale, String> results = new HashMap<>();
		for (Locale curLocale : availableLocales) {
			results.put(curLocale, LanguageUtil.get(curLocale, value));
		}
		return results;
	}

	public String replacePlaceholders(String jsonFormDefinition, Map<String, String> placeholdersValues) {

		String formDefinition = jsonFormDefinition;

		for (Entry<String, String> placeHolderAndValue : placeholdersValues.entrySet()) {
			formDefinition = replacePlaceholder(formDefinition, placeHolderAndValue.getKey(), placeHolderAndValue.getValue());
		}
		return formDefinition;
	}

	private String replacePlaceholder(String content, String key, String value) {
		return content.replace("[$" + key + "$]", value);
	}

	private void addRolePermission(long companyId, String primKey, String name, String roleName, String[] permissions) throws PortalException {
		Role role = roleLocalService.getRole(companyId, roleName);

		resourcePermissionLocalService.setResourcePermissions(companyId, name, ResourceConstants.SCOPE_INDIVIDUAL, primKey, role.getRoleId(), permissions);
	}
}
