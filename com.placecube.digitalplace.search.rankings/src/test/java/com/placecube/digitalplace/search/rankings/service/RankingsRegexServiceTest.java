package com.placecube.digitalplace.search.rankings.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;

public class RankingsRegexServiceTest {

	private final String END_STRING = StringPool.DOLLAR;

	private final String START_STRING = StringPool.CARET;

	private final String WILD_END = "($|\\W.*)";

	private final String WILD_INNER = "(.*\\W)";

	private final String WILD_START = "(^|.*\\W)";

	private RankingsRegexService rankingsRegexService = new RankingsRegexService();

	@Test
	public void getAliasWildcardRegex_WhenAliasIsEmpty_ThenReturnsEmpty() throws ConfigurationException {
		String alias = "";

		String result = rankingsRegexService.getAliasWildcardRegex(alias);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getAliasWildcardRegex_WhenAliasHasNoAsterisk_ThenReturnsAliasWithStartAndEndStringRegex() throws ConfigurationException {
		String alias = "alias";

		String result = rankingsRegexService.getAliasWildcardRegex(alias);

		assertThat(result, equalTo(START_STRING + "alias" + END_STRING));
	}

	@Test
	public void getAliasWildcardRegex_WhenAliasIsNull_ThenReturnsNull() throws ConfigurationException {
		String alias = null;

		String result = rankingsRegexService.getAliasWildcardRegex(alias);

		assertThat(result, equalTo(null));
	}

	@Test
	public void getAliasWildcardRegex_WhenAliasStartsWithAsterisk_ThenReturnsAliasWithStartRegexExpAndEndStringRegex() throws ConfigurationException {
		String alias = "alias";
		String wildAlias = StringPool.STAR + alias;

		String result = rankingsRegexService.getAliasWildcardRegex(wildAlias);

		assertThat(result, equalTo(WILD_START + "alias" + END_STRING));
	}

	@Test
	public void getAliasWildcardRegex_WhenAliasEndsWithAsterisk_ThenReturnsAliasWithEndRegexExpAndStartStringRegex() throws ConfigurationException {
		String alias = "alias";
		String wildAlias = alias + StringPool.STAR;

		String result = rankingsRegexService.getAliasWildcardRegex(wildAlias);

		assertThat(result, equalTo(START_STRING + "alias" + WILD_END));
	}

	@Test
	public void getAliasWildcardRegex_WhenAliasStartsAndEndsWithAsterisk_ThenReturnsAliasWithStartAndEndRegexExp() throws ConfigurationException {
		String alias = "alias";
		String wildAlias = StringPool.STAR + alias + StringPool.STAR;

		String result = rankingsRegexService.getAliasWildcardRegex(wildAlias);

		assertThat(result, equalTo(WILD_START + "alias" + WILD_END));
	}

	@Test
	public void getAliasWildcardRegex_WhenAliasContainsInnerAsterisk_ThenReturnsAliasWithStartAndEndStringRegexAndInnerAsteriskRegexExp() throws ConfigurationException {
		String wildAlias = "alias*wildcard";

		String result = rankingsRegexService.getAliasWildcardRegex(wildAlias);

		assertThat(result, equalTo(START_STRING + "alias" + WILD_INNER + "wildcard" + END_STRING));
	}

	@Test
	public void getAliasWildcardRegex_WhenAliasStartsAndEndsWithAndContainsInnerAsterisk_ThenReturnsAliasWithStartAndEndAndInnerAsteriskRegexExp() throws ConfigurationException {
		String wildAlias = "*alias*wildcard*";

		String result = rankingsRegexService.getAliasWildcardRegex(wildAlias);

		assertThat(result, equalTo(WILD_START + "alias" + WILD_INNER + "wildcard" + WILD_END));
	}
}
