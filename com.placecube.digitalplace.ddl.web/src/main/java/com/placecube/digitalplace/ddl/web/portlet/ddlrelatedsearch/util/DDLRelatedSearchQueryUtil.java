package com.placecube.digitalplace.ddl.web.portlet.ddlrelatedsearch.util;

import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetTag;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.search.document.Document;
import com.liferay.portal.search.hits.SearchHit;
import com.liferay.portal.search.query.BooleanQuery;
import com.liferay.portal.search.query.Queries;
import com.liferay.portal.search.searcher.SearchRequest;
import com.liferay.portal.search.searcher.SearchRequestBuilder;
import com.liferay.portal.search.searcher.Searcher;

@Component(immediate = true, service = DDLRelatedSearchQueryUtil.class)
public class DDLRelatedSearchQueryUtil {

	@Reference
	private Queries queries;

	@Reference
	private Searcher searcher;

	public void configureAssetTagIds(BooleanQuery query, AssetEntry assetEntry) {
		List<AssetTag> assetTags = assetEntry.getTags();

		for (AssetTag assetTag : assetTags) {
			query.addShouldQueryClauses(queries.term("assetTagIds", String.valueOf(assetTag.getTagId())));
		}
	}

	public void configureCategoryIds(BooleanQuery query, AssetEntry assetEntry) {
		long[] categoryIds = assetEntry.getCategoryIds();

		for (long id : categoryIds) {
			query.addShouldQueryClauses(queries.term("assetCategoryIds", String.valueOf(id)));
		}
	}

	public void configureRecordAndStructureId(BooleanQuery booleanQuery, DDLRecord ddlRecord) throws PortalException {
		booleanQuery.addMustNotQueryClauses(queries.term("entryClassPK", String.valueOf(ddlRecord.getRecordId())));
		booleanQuery.addShouldQueryClauses(queries.term("ddmStructureId", String.valueOf(ddlRecord.getRecordSet().getDDMStructureId())));
	}

	public void configureKeywordsFromTitle(BooleanQuery query, String recordTitle) {
		String[] keywords = recordTitle.replaceAll("\\p{Punct}", StringPool.BLANK).toLowerCase().split(StringPool.SPACE);

		for (String keyword : KeywordsFilterUtil.removeStopWords(keywords)) {
			query.addShouldQueryClauses(queries.term("title", keyword));
		}
	}

	public void configureSearchContext(SearchRequestBuilder searchRequestBuilder, long companyId) {
		searchRequestBuilder.withSearchContext(searchContext -> {
			searchContext.setCompanyId(companyId);
			searchContext.setEntryClassNames(new String[] { DDLRecord.class.getName() });
		});
	}

	public List<Document> getResultsFromSearchBuilder(SearchRequestBuilder searchRequestBuilder, BooleanQuery booleanQuery, int maxItems) {
		SearchRequest searchRequest = searchRequestBuilder.query(booleanQuery).size(maxItems).build();
		return searcher.search(searchRequest).getSearchHits().getSearchHits().stream().map(SearchHit::getDocument).collect(Collectors.toList());
	}

}
