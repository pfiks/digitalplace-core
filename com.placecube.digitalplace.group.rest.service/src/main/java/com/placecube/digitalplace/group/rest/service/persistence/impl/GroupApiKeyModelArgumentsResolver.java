/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.service.persistence.impl;

import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.model.BaseModel;

import com.placecube.digitalplace.group.rest.model.GroupApiKeyTable;
import com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyImpl;
import com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyModelImpl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.service.component.annotations.Component;

/**
 * The arguments resolver class for retrieving value from GroupApiKey.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(
	property = {
		"class.name=com.placecube.digitalplace.group.rest.model.impl.GroupApiKeyImpl",
		"table.name=Placecube_GroupRest_GroupApiKey"
	},
	service = ArgumentsResolver.class
)
public class GroupApiKeyModelArgumentsResolver implements ArgumentsResolver {

	@Override
	public Object[] getArguments(
		FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
		boolean original) {

		String[] columnNames = finderPath.getColumnNames();

		if ((columnNames == null) || (columnNames.length == 0)) {
			if (baseModel.isNew()) {
				return new Object[0];
			}

			return null;
		}

		GroupApiKeyModelImpl groupApiKeyModelImpl =
			(GroupApiKeyModelImpl)baseModel;

		long columnBitmask = groupApiKeyModelImpl.getColumnBitmask();

		if (!checkColumn || (columnBitmask == 0)) {
			return _getValue(groupApiKeyModelImpl, columnNames, original);
		}

		Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
			finderPath);

		if (finderPathColumnBitmask == null) {
			finderPathColumnBitmask = 0L;

			for (String columnName : columnNames) {
				finderPathColumnBitmask |=
					groupApiKeyModelImpl.getColumnBitmask(columnName);
			}

			_finderPathColumnBitmasksCache.put(
				finderPath, finderPathColumnBitmask);
		}

		if ((columnBitmask & finderPathColumnBitmask) != 0) {
			return _getValue(groupApiKeyModelImpl, columnNames, original);
		}

		return null;
	}

	@Override
	public String getClassName() {
		return GroupApiKeyImpl.class.getName();
	}

	@Override
	public String getTableName() {
		return GroupApiKeyTable.INSTANCE.getTableName();
	}

	private static Object[] _getValue(
		GroupApiKeyModelImpl groupApiKeyModelImpl, String[] columnNames,
		boolean original) {

		Object[] arguments = new Object[columnNames.length];

		for (int i = 0; i < arguments.length; i++) {
			String columnName = columnNames[i];

			if (original) {
				arguments[i] = groupApiKeyModelImpl.getColumnOriginalValue(
					columnName);
			}
			else {
				arguments[i] = groupApiKeyModelImpl.getColumnValue(columnName);
			}
		}

		return arguments;
	}

	private static final Map<FinderPath, Long> _finderPathColumnBitmasksCache =
		new ConcurrentHashMap<>();

}