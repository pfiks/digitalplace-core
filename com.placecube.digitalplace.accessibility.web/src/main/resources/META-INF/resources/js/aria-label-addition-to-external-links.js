Liferay.on('allPortletsReady', function() {
	setInterval(() => {
		document.body.querySelectorAll('a[target="_blank"]:not([aria-label])').forEach(a => {
			const linkText = a.textContent
			.trim()
			.replace(/\s{2,}/g, ' ')
			.replace(/\n/g, ' ');

			a.setAttribute('aria-label', linkText + ' (Opens in a new tab)');
		});
	}, 5000);
});