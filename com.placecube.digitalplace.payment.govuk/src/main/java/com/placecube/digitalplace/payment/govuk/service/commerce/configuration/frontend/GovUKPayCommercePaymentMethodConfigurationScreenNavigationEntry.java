package com.placecube.digitalplace.payment.govuk.service.commerce.configuration.frontend;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.commerce.payment.constants.CommercePaymentScreenNavigationConstants;
import com.liferay.commerce.payment.model.CommercePaymentMethodGroupRel;
import com.liferay.commerce.product.model.CommerceChannel;
import com.liferay.commerce.product.service.CommerceChannelService;
import com.liferay.frontend.taglib.servlet.taglib.ScreenNavigationEntry;
import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.language.Language;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.settings.GroupServiceSettingsLocator;
import com.liferay.portal.kernel.settings.ParameterMapSettingsLocator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.payment.govuk.service.commerce.GovUKCommerceConstants;
import com.placecube.digitalplace.payment.govuk.service.commerce.configuration.GovUKPayGroupServiceConfiguration;

@Component(
	property = "screen.navigation.entry.order:Integer=20",
	service = ScreenNavigationEntry.class
)
public class GovUKPayCommercePaymentMethodConfigurationScreenNavigationEntry implements ScreenNavigationEntry<CommercePaymentMethodGroupRel> {

	@Reference
	private CommerceChannelService commerceChannelService;

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private JSPRenderer jspRenderer;

	@Reference
	private Language language;

	@Reference(target = "(osgi.web.symbolicname=com.placecube.digitalplace.payment.govuk)", unbind = "-")
	private ServletContext servletContext;

	@Override
	public String getCategoryKey() {
		return CommercePaymentScreenNavigationConstants.CATEGORY_KEY_COMMERCE_PAYMENT_METHOD_CONFIGURATION;
	}

	@Override
	public String getEntryKey() {
		return GovUKCommerceConstants.ENTRY_KEY_GOVUK_PAY_COMMERCE_PAYMENT_METHOD_CONFIGURATION;
	}

	@Override
	public String getLabel(Locale locale) {
		return language.get(locale, CommercePaymentScreenNavigationConstants.CATEGORY_KEY_COMMERCE_PAYMENT_METHOD_CONFIGURATION);
	}

	@Override
	public String getScreenNavigationKey() {
		return CommercePaymentScreenNavigationConstants.SCREEN_NAVIGATION_KEY_COMMERCE_PAYMENT_METHOD;
	}

	@Override
	public boolean isVisible(User user, CommercePaymentMethodGroupRel commercePaymentMethod) {
		if (commercePaymentMethod == null) {
			return false;
		}

		return GovUKCommerceConstants.PAYMENT_ENGINE_KEY.equals(commercePaymentMethod.getPaymentIntegrationKey());
	}

	@Override
	public void render(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
		try {
			long commerceChannelId = ParamUtil.getLong(httpServletRequest, "commerceChannelId");

			CommerceChannel commerceChannel = commerceChannelService.getCommerceChannel(commerceChannelId);

			GovUKPayGroupServiceConfiguration govukPayGroupServiceConfiguration =	configurationProvider.getConfiguration(
						GovUKPayGroupServiceConfiguration.class, new ParameterMapSettingsLocator(httpServletRequest.getParameterMap(),
						new GroupServiceSettingsLocator(commerceChannel.getGroupId(), GovUKCommerceConstants.SERVICE_NAME)));

			httpServletRequest.setAttribute(GovUKPayGroupServiceConfiguration.class.getName(), govukPayGroupServiceConfiguration);
		}
		catch (Exception exception) {
			throw new IOException(exception);
		}

		jspRenderer.renderJSP(servletContext, httpServletRequest, httpServletResponse, "/configuration.jsp");
	}

}