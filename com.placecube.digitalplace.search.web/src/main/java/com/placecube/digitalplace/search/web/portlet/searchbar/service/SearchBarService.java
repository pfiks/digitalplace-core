package com.placecube.digitalplace.search.web.portlet.searchbar.service;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.layout.service.LayoutRetrievalService;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.portlet.searchbar.configuration.SearchBarPortletInstanceConfiguration;

@Component(immediate = true, service = SearchBarService.class)
public class SearchBarService {

	private static final Log LOG = LogFactoryUtil.getLog(SearchBarService.class);

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private LayoutRetrievalService layoutRetrievalService;

	public Optional<Layout> getDestinationLayout(ThemeDisplay themeDisplay, SearchBarPortletInstanceConfiguration configuration) throws PortalException {
		long scopeGroupId = configuration.searchGuestGroup() ? getGuestGroupId(themeDisplay) : themeDisplay.getScopeGroupId();
		boolean isPrivateLayout = themeDisplay.getLayout().isPrivateLayout();
		String destinationPage = configuration.destinationPage();

		LOG.debug("Searching for destination layout in groupId: " + scopeGroupId + " and url: " + destinationPage);

		Layout destinationLayout = layoutLocalService.fetchLayoutByFriendlyURL(scopeGroupId, isPrivateLayout, destinationPage);
		if (Validator.isNull(destinationLayout)) {
			destinationLayout = layoutLocalService.fetchLayoutByFriendlyURL(scopeGroupId, !isPrivateLayout, destinationPage);
		}
		return Optional.ofNullable(destinationLayout);
	}

	public String getDestinationSearchBarPortletIdOnLayout(Layout layout) {
		String portletId = StringPool.BLANK;

		List<Portlet> allPortletsOnLayout = layoutRetrievalService.getAllPortletsOnLayout(layout);
		Optional<Portlet> searchPortletFound = allPortletsOnLayout.stream().filter(portlet -> portlet.getPortletId().contains(PortletKeys.SEARCH_BAR)).findFirst();
		if (searchPortletFound.isPresent()) {
			portletId = searchPortletFound.get().getPortletId();
			LOG.debug("Found search portlet on layout " + layout.getPlid() + ", portletId: " + portletId);
		}

		return portletId;
	}

	private long getGuestGroupId(ThemeDisplay themeDisplay) throws PortalException {
		return groupLocalService.getGroup(themeDisplay.getCompanyId(), GroupConstants.GUEST).getGroupId();
	}
}
