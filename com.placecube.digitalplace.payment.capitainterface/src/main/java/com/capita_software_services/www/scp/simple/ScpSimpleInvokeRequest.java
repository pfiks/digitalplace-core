/**
 * ScpSimpleInvokeRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.simple;

public class ScpSimpleInvokeRequest  extends com.capita_software_services.www.scp.base.ScpInvokeRequest  implements java.io.Serializable {
    private com.capita_software_services.www.scp.simple.SimpleSale sale;

    public ScpSimpleInvokeRequest() {
    }

    public ScpSimpleInvokeRequest(
           uk.co.capita_software.support.selfservice.Credentials credentials,
           com.capita_software_services.www.scp.base.RequestType requestType,
           org.apache.axis.types.Token requestId,
           com.capita_software_services.www.scp.base.Routing routing,
           com.capita_software_services.www.portal_api.PanEntryMethod panEntryMethod,
           com.capita_software_services.www.scp.base.AdditionalInstructions additionalInstructions,
           com.capita_software_services.www.scp.base.BillingDetails billing,
           com.capita_software_services.www.scp.simple.SimpleSale sale) {
        super(
            credentials,
            requestType,
            requestId,
            routing,
            panEntryMethod,
            additionalInstructions,
            billing);
        this.sale = sale;
    }


    /**
     * Gets the sale value for this ScpSimpleInvokeRequest.
     *
     * @return sale
     */
    public com.capita_software_services.www.scp.simple.SimpleSale getSale() {
        return sale;
    }


    /**
     * Sets the sale value for this ScpSimpleInvokeRequest.
     *
     * @param sale
     */
    public void setSale(com.capita_software_services.www.scp.simple.SimpleSale sale) {
        this.sale = sale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScpSimpleInvokeRequest)) return false;
        ScpSimpleInvokeRequest other = (ScpSimpleInvokeRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.sale==null && other.getSale()==null) ||
             (this.sale!=null &&
              this.sale.equals(other.getSale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSale() != null) {
            _hashCode += getSale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScpSimpleInvokeRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "scpSimpleInvokeRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "sale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/simple", "simpleSale"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
