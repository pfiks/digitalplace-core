package com.placecube.digitalplace.override.jsp.dynamicinclude;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.servlet.taglib.DynamicInclude.DynamicIncludeRegistry;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.override.jsp.configuration.CompanyStyleConfiguration;

public class TopHeadDynamicIncludePreTest extends PowerMockito {

	@Mock
	private CompanyStyleConfiguration mockCompanyStyleConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private DynamicIncludeRegistry mockDynamicIncludeRegistry;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private TopHeadDynamicIncludePre topHeadDynamicIncludePre;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void include_WhenThemeDisplayIsNull_ThenNothingIsAddedToTheRequest() throws IOException {
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(null);

		topHeadDynamicIncludePre.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, never()).setAttribute(anyString(), any());
	}

	@Test
	public void include_WhenThemeDisplayIsValidAndThemeSettingFaviconNotSpecified_ThenAddsTheFaviconAndTheCssFromTheConfigurationToTheRequest() throws Exception {
		String expectedFavicon = "expectedFavicon";
		String expectedCSS = "expectedCSS";
		long companyId = 123;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getThemeSetting("favicon-url")).thenReturn(StringPool.THREE_SPACES);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(CompanyStyleConfiguration.class, companyId)).thenReturn(mockCompanyStyleConfiguration);
		when(mockCompanyStyleConfiguration.companyCSS()).thenReturn(expectedCSS);
		when(mockCompanyStyleConfiguration.companyFaviconURL()).thenReturn(expectedFavicon);

		topHeadDynamicIncludePre.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, times(1)).setAttribute("customCompanyFaviconURL", expectedFavicon);
		verify(mockHttpServletRequest, times(1)).setAttribute("customCompanyCSS", expectedCSS);
	}

	@Test
	public void include_WhenThemeDisplayIsValidAndThemeSettingFaviconNotSpecifiedAndExceptionRetrievingTheConfiguration_ThenNothingIsAddedToTheRequest() throws Exception {
		long companyId = 123;
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getThemeSetting("favicon-url")).thenReturn(StringPool.THREE_SPACES);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(CompanyStyleConfiguration.class, companyId)).thenThrow(new ConfigurationException());

		topHeadDynamicIncludePre.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, never()).setAttribute(eq("customCompanyFaviconURL"), any());
		verify(mockHttpServletRequest, never()).setAttribute(eq("customCompanyCSS"), any());
	}

	@Test
	public void include_WhenThemeDisplayIsValidAndThemeSettingFaviconSpecified_ThenAddsTheFaviconFromTheThemeToTheRequest() throws IOException {
		String expectedFavicon = "expectedFavicon";
		when(mockHttpServletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getThemeSetting("favicon-url")).thenReturn(expectedFavicon);

		topHeadDynamicIncludePre.include(mockHttpServletRequest, mockHttpServletResponse, "key");

		verify(mockHttpServletRequest, times(1)).setAttribute("customCompanyFaviconURL", expectedFavicon);
		verify(mockHttpServletRequest, never()).setAttribute(eq("customCompanyCSS"), any());
	}

	@Test
	public void register_WhenNoError_ThenRegistersTopHeadPre() {
		topHeadDynamicIncludePre.register(mockDynamicIncludeRegistry);

		verify(mockDynamicIncludeRegistry, times(1)).register("/html/common/themes/top_head.jsp#pre");
	}

}
