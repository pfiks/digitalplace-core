package com.placecube.digitalplace.user.settings.web.twofactor.portlet;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.frontend.taglib.servlet.taglib.util.JSPRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.constants.DisplaySection;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.util.TwoFactorAuthenticationUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class TwoFactorAuthenticationUserSettingsSectionTest extends PowerMockito {

	private static final long COMPANY_ID = 123;
	private static final String EMAIL_ADDRESS = "abc";

	@Mock
	private AuthenticationTwoFactorCompanyConfiguration mockAuthenticationTwoFactorCompanyConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpServletResponse mockHttpServletResponse;

	@Mock
	private JSPRenderer mockJSPRenderer;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private ServletContext mockServletContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TwoFactorAuthenticationUtil mockTwoFactorAuthenticationUtil;

	@Mock
	private User mockUser;

	@Mock
	private UserTwoFactorAuthentication mockUserTwoFactorAuthentication;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	@InjectMocks
	private TwoFactorAuthenticationUserSettingsSection twoFactorAuthenticationUserSettingsSection;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getBundleId_WhenNoError_ThenReturnsTheBundleId() {
		String result = twoFactorAuthenticationUserSettingsSection.getBundleId();

		assertThat(result, equalTo(TwoFactorAuthenticationKeys.BUNDLE_ID));
	}

	@Test
	public void getDisplayOrder_WhenNoError_ThenReturnsTheDisplayOrder() {
		int result = twoFactorAuthenticationUserSettingsSection.getDisplayOrder();

		assertThat(result, equalTo(TwoFactorAuthenticationKeys.DISPLAY_ORDER));
	}

	@Test
	public void getDisplaySection_WhenNoError_ThenReturnsAccountSettings() {
		DisplaySection result = twoFactorAuthenticationUserSettingsSection.getDisplaySection();

		assertThat(result, equalTo(DisplaySection.ACCOUNT_SETTINGS));
	}

	@Test
	public void getTitleKey_WhenNoError_ThenReturnsTheTitleKey() {
		String result = twoFactorAuthenticationUserSettingsSection.getTitleKey();

		assertThat(result, equalTo("two-factor-authentication"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void displayForUser_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean isEnabled) throws ConfigurationException {
		long companyId = 1;
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, companyId)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockAuthenticationTwoFactorCompanyConfiguration.enabled()).thenReturn(isEnabled);

		boolean result = twoFactorAuthenticationUserSettingsSection.displayForUser(mockUser);

		assertThat(result, equalTo(isEnabled));
	}

	@Test
	public void displayForUser_WhenExceptionRetrievingConfiguration_ThenReturnsFalse() throws ConfigurationException {
		long companyId = 1;
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockConfigurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, companyId)).thenThrow(ConfigurationException.class);

		boolean result = twoFactorAuthenticationUserSettingsSection.displayForUser(mockUser);

		assertFalse(result);
	}

	@Test
	public void render_WhenTwoFactorIsNotEnabled_ThenOnlySetsDisplayFormToFalseAsAttribute() throws IOException {
		mockHttpRequestAndThemeDisplay();
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockTwoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication))).thenReturn(false);

		twoFactorAuthenticationUserSettingsSection.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_ENFORCED), anyString());
		verify(mockRenderRequest, never()).setAttribute(eq(TwoFactorAuthenticationRequestKeys.SECRET_KEY), anyString());
		verify(mockRenderRequest, never()).setAttribute(eq(TwoFactorAuthenticationRequestKeys.QR_URL), anyString());
		InOrder inOrder = Mockito.inOrder(mockRenderRequest, mockJSPRenderer);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, false);
		inOrder.verify(mockJSPRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	@Test
	public void render_WhenTwoFactorIsEnabledAndIsEnforcedAndSecretKeyIsPresent_ThenSets2FAEnforcedToTrueAndQRURLAndDisplayFormToTrueAsAttribute() throws IOException, PortalException {
		String secretKey = "key";
		String qrURL = "url";

		mockHttpRequestAndThemeDisplay();
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockTwoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication))).thenReturn(true);
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY)).thenReturn(secretKey);
		when(mockUserTwoFactorAuthenticationLocalService.getQRBarCodeURL(COMPANY_ID, EMAIL_ADDRESS, secretKey)).thenReturn(qrURL);
		when(mockUserTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(mockUser)).thenReturn(true);

		twoFactorAuthenticationUserSettingsSection.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(TwoFactorAuthenticationRequestKeys.SECRET_KEY), anyString());
		InOrder inOrder = Mockito.inOrder(mockRenderRequest, mockJSPRenderer);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_ENFORCED, true);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.QR_URL, qrURL);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, true);
		inOrder.verify(mockJSPRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	@Test
	public void render_WhenTwoFactorIsEnabledAndIsNotEnforcedAndSecretKeyIsPresent_ThenSets2FAEnforcedToFalseAndQRURLAndDisplayFormToTrueAsAttribute() throws IOException, PortalException {
		String secretKey = "key";
		String qrURL = "url";

		mockHttpRequestAndThemeDisplay();
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockTwoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication))).thenReturn(true);
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY)).thenReturn(secretKey);
		when(mockUserTwoFactorAuthenticationLocalService.getQRBarCodeURL(COMPANY_ID, EMAIL_ADDRESS, secretKey)).thenReturn(qrURL);
		when(mockUserTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(mockUser)).thenReturn(false);

		twoFactorAuthenticationUserSettingsSection.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(TwoFactorAuthenticationRequestKeys.SECRET_KEY), anyString());
		InOrder inOrder = Mockito.inOrder(mockRenderRequest, mockJSPRenderer);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_ENFORCED, false);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.QR_URL, qrURL);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, true);
		inOrder.verify(mockJSPRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	@Test
	public void render_WhenTwoFactorIsEnabledAndIsEnforcedAndSecretKeyIsNotPresent_ThenSets2FAEnforcedToTrueAndQRURLAndDisplayFormToTrueAndTheGeneratedSecretKeyAsAttribute()
			throws PortalException, IOException {
		String secretKey = "key";
		String qrURL = "url";

		mockHttpRequestAndThemeDisplay();
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockTwoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication))).thenReturn(true);
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY)).thenReturn(null);
		when(mockTwoFactorAuthenticationUtil.getSecretKey(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication))).thenReturn(secretKey);
		when(mockUserTwoFactorAuthenticationLocalService.getQRBarCodeURL(COMPANY_ID, EMAIL_ADDRESS, secretKey)).thenReturn(qrURL);
		when(mockUserTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(mockUser)).thenReturn(true);

		twoFactorAuthenticationUserSettingsSection.render(mockHttpServletRequest, mockHttpServletResponse);

		InOrder inOrder = Mockito.inOrder(mockRenderRequest, mockJSPRenderer);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_ENFORCED, true);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY, secretKey);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.QR_URL, qrURL);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, true);
		inOrder.verify(mockJSPRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	@Test
	public void render_WhenTwoFactorIsEnabledAndIsEnforcedAndSecretKeyIsNotPresentAndExceptionGettingQRCodeURL_ThenSets2FAEnforcedToTrueAndDisplayFormToTrueAndTheGeneratedSecretKeyAsAttribute()
			throws PortalException, IOException {
		String secretKey = "key";

		mockHttpRequestAndThemeDisplay();
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockTwoFactorAuthenticationUtil.isTwoFactorEnabled(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication))).thenReturn(true);
		when(mockRenderRequest.getAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY)).thenReturn(null);
		when(mockTwoFactorAuthenticationUtil.getSecretKey(mockRenderRequest, Optional.of(mockUserTwoFactorAuthentication))).thenReturn(secretKey);
		when(mockUserTwoFactorAuthenticationLocalService.getQRBarCodeURL(COMPANY_ID, EMAIL_ADDRESS, secretKey)).thenThrow(PortalException.class);
		when(mockUserTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(mockUser)).thenReturn(true);

		twoFactorAuthenticationUserSettingsSection.render(mockHttpServletRequest, mockHttpServletResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(TwoFactorAuthenticationRequestKeys.QR_URL), anyString());
		InOrder inOrder = Mockito.inOrder(mockRenderRequest, mockJSPRenderer);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_ENFORCED, true);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY, secretKey);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, true);
		inOrder.verify(mockJSPRenderer, times(1)).renderJSP(mockServletContext, mockHttpServletRequest, mockHttpServletResponse, "/view.jsp");
	}

	private void mockHttpRequestAndThemeDisplay() {
		when(mockHttpServletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_REQUEST)).thenReturn(mockRenderRequest);
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getEmailAddress()).thenReturn(EMAIL_ADDRESS);
	}
}
