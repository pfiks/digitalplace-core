package com.placecube.digitalplace.ddl.model.impl;

import com.placecube.digitalplace.ddl.model.DynamicDataListValue;

public class DynamicDataListValueImpl implements DynamicDataListValue {

	private final String fieldAdditionalDetails;
	private final String fieldName;
	private final String fieldValue;

	DynamicDataListValueImpl(String fieldName, String fieldValue, String fieldAdditionalDetails) {
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		this.fieldAdditionalDetails = fieldAdditionalDetails;
	}

	@Override
	public String getFieldAdditionalDetails() {
		return fieldAdditionalDetails;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public String getFieldValue() {
		return fieldValue;
	}

}
