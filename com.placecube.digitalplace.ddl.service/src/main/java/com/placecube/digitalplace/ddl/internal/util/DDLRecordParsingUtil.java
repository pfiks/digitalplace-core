package com.placecube.digitalplace.ddl.internal.util;

import java.util.Locale;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.LocalizedValue;
import com.liferay.dynamic.data.mapping.model.Value;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = DDLRecordParsingUtil.class)
public class DDLRecordParsingUtil {

	@Reference
	private DDLFieldTypeParsingUtil ddlFieldTypeParsingUtil;

	public String getFieldLabel(DDMFormField ddmFormField) {
		LocalizedValue label = ddmFormField.getLabel();
		return label.getString(label.getDefaultLocale());
	}

	public String[] getFieldValueAndAdditionalDetails(DDMFormField ddmFormField, DDMFormFieldValue ddmFormFieldValue) {
		String[] result = new String[] { StringPool.BLANK, StringPool.BLANK };

		Value value = ddmFormFieldValue.getValue();

		if (Validator.isNotNull(value)) {
			Locale locale = value.getDefaultLocale();
			String baseValue = value.getString(locale);

			if (ddlFieldTypeParsingUtil.isValidValue(baseValue)) {

				String fieldType = ddmFormField.getType();

				if ("ddm-text-html".equals(fieldType)) {
					result[0] = HtmlUtil.stripHtml(baseValue);

				} else if ("select".equals(fieldType)) {
					result[0] = ddlFieldTypeParsingUtil.getSelectFieldValue(ddmFormField, locale, baseValue);

				} else if ("ddm-documentlibrary".equals(fieldType)) {
					Optional<FileEntry> fileEntry = ddlFieldTypeParsingUtil.getFileEntry(baseValue);
					result[0] = ddlFieldTypeParsingUtil.getFileEntryTitle(baseValue, fileEntry);
					result[1] = ddlFieldTypeParsingUtil.getFileEntryAdditionalDetails(locale, fileEntry);

				} else if ("radio".equals(fieldType)) {
					result[0] = ddlFieldTypeParsingUtil.getOptionLabel(ddmFormField, locale, baseValue);

				} else {
					result[0] = baseValue;
				}
			}
		}
		return result;
	}

}
