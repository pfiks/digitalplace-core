package com.placecube.digitalplace.authentication.twofactor.web.service;

import javax.portlet.ActionRequest;
import javax.portlet.ActionURL;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderURL;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.portlet.PortletURLFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorMVCCommandKeys;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorPortletKeys;

@Component(immediate = true, service = TwoFactorUrlService.class)
public class TwoFactorUrlService {

	@Reference
	private Portal portal;

	@Reference
	private PortletURLFactory portletURLFactory;

	public ActionURL getLoginActionURL(LiferayPortletResponse liferayPortletResponse, String encryptedStateMapJSON) {
		ActionURL actionURL = liferayPortletResponse.createActionURL();
		actionURL.setParameter(ActionRequest.ACTION_NAME, "/login/login");
		actionURL.setParameter("state", encryptedStateMapJSON);
		return actionURL;
	}

	public String getRedirectURL(String mvcRenderCommandName, long plid, ActionRequest actionRequest, ActionURL actionURL, HttpServletRequest httpServletRequest, RenderURL returnToFullPageRenderURL)
			throws WindowStateException {

		LiferayPortletURL liferayPortletURL = portletURLFactory.create(httpServletRequest, TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION, plid, PortletRequest.RENDER_PHASE);

		String portletId = ParamUtil.getString(httpServletRequest, "p_p_id");
		if (PortletKeys.FAST_LOGIN.equals(portletId)) {
			liferayPortletURL.setWindowState(actionRequest.getWindowState());
		} else {
			liferayPortletURL.setWindowState(WindowState.MAXIMIZED);
		}

		liferayPortletURL.setParameter("saveLastPath", Boolean.FALSE.toString());
		liferayPortletURL.setParameter("mvcRenderCommandName", mvcRenderCommandName);
		liferayPortletURL.setParameter("redirect", actionURL.toString());
		liferayPortletURL.setParameter("returnToFullPageURL", returnToFullPageRenderURL.toString());

		return liferayPortletURL.toString();
	}

	public String getResetTwoFactorURL(PortletResponse portletResponse, User user) {
		LiferayPortletResponse liferayPortletResponse = portal.getLiferayPortletResponse(portletResponse);

		ActionURL actionURL = liferayPortletResponse.createActionURL();
		actionURL.setParameter("p_p_id", TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION);
		actionURL.setParameter(ActionRequest.ACTION_NAME, TwoFactorMVCCommandKeys.RESET);
		actionURL.setParameter("selectedUserId", String.valueOf(user.getUserId()));
		String urlValue = actionURL.toString();
		return urlValue.replace(PortletKeys.USERS_ADMIN, TwoFactorPortletKeys.TWO_FACTOR_AUTHENTICATION);
	}

	public RenderURL getReturnToFullPageURL(LiferayPortletResponse liferayPortletResponse, String redirect) {
		RenderURL returnToFullPageRenderURL = liferayPortletResponse.createRenderURL();

		if (Validator.isNotNull(redirect)) {
			returnToFullPageRenderURL.setParameter("redirect", redirect);
		}

		return returnToFullPageRenderURL;
	}

}
