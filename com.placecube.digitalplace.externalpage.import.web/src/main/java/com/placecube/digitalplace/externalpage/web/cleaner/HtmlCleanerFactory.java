package com.placecube.digitalplace.externalpage.web.cleaner;

import org.htmlcleaner.ContentNode;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNodeVisitor;

public class HtmlCleanerFactory {

	public static HtmlCleaner createHtmlCleaner() {
		return new HtmlCleaner();
	}

	public static TagNodeVisitor createLeadingSpaceTagNodeVisitor() {
		return new LeadingSpaceTagNodeVisitor();
	}

	public static ContentNode createContentNode(String content) {
		return new ContentNode(content);
	}
}
