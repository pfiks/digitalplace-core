package com.placecube.digitalplace.search.web.portlet.category.util;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.search.shared.model.FacetResult;
import com.placecube.digitalplace.search.web.portlet.category.constants.JSONKeys;

public class CategoryFacetTreeUtilTest extends PowerMockito {

	private static final long ROOT_CATEGORY_ID = 1;
	private static final long CHILD_CATEGORY_ID = 2;
	private static final long GRANDCHILD_CATEGORY_ID = 3;
	private static final long INVALID_CATEGORY_ID = 4;

	private static final String ROOT_CATEGORY_TITLE = "ROOT TITLE";
	private static final String CHILD_CATEGORY_TITLE = "CHILD TITLE";
	private static final String GRANDCHILD_CATEGORY_TITLE = "GRAND CHILD TITLE";

	private static final Locale LOCALE = Locale.CANADA;

	@InjectMocks
	private CategoryFacetTreeUtil categoryFacetTreeUtil;

	@Mock
	private AssetCategory mockRootCategory;

	@Mock
	private AssetCategory mockChildCategory;

	@Mock
	private AssetCategory mockGrandChildCategory;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private FacetResult mockRootFacetResult;

	@Mock
	private FacetResult mockChildFacetResult;

	@Mock
	private FacetResult mockGrandChildFacetResult;

	@Mock
	private JSONArray mockTreeJSON;

	@Mock
	private JSONArray mockPrunedTreeJSON;

	@Mock
	private JSONArray mockChildrenJSONArray;

	@Mock
	private JSONArray mockGrandChildrenJSONArray;

	@Mock
	private JSONArray mockInvalidChildrenJSONArray;

	@Mock
	private JSONObject mockRootJSONObject;

	@Mock
	private JSONObject mockChildJSONObject;

	@Mock
	private JSONObject mockGrandChildJSONObject;

	@Mock
	private JSONFactory mockJsonFactory;


	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void fetchCategoryById_WhenNoError_ThenReturnsAssetCategory() throws PortalException {
		when(mockAssetCategoryLocalService.getCategory(ROOT_CATEGORY_ID)).thenReturn(mockRootCategory);

		Optional<AssetCategory> result = categoryFacetTreeUtil.fetchCategoryById(ROOT_CATEGORY_ID);

		assertTrue(result.isPresent());
		assertThat(result.get(), equalTo(mockRootCategory));

	}

	@Test
	public void fetchCategoryById_WhenErrorOnGettingAssetCategory_ThenReturnsNull() throws PortalException {
		when(mockAssetCategoryLocalService.getCategory(INVALID_CATEGORY_ID)).thenThrow(new PortalException());

		Optional<AssetCategory> result = categoryFacetTreeUtil.fetchCategoryById(INVALID_CATEGORY_ID);

		assertFalse(result.isPresent());
	}

	@Test
	public void pruneCategoryTree_WhenNoErrorAndNoCategoriesToPrune_ThenReturnsJSONTreeWithoutAnyChanges() {
		Set<Long> categoriesToPrune = new HashSet<>();

		mockJSONArrayCreation(false);
		mockFullTreeStructure();

		when(mockTreeJSON.length()).thenReturn(1);
		when(mockTreeJSON.getJSONObject(0)).thenReturn(mockRootJSONObject);
		when(mockRootJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockRootJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockChildrenJSONArray);
		when(mockChildrenJSONArray.length()).thenReturn(1);
		when(mockChildrenJSONArray.getJSONObject(0)).thenReturn(mockChildJSONObject);
		when(mockChildJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockChildJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockGrandChildrenJSONArray);
		when(mockGrandChildrenJSONArray.length()).thenReturn(1);
		when(mockGrandChildrenJSONArray.getJSONObject(0)).thenReturn(mockGrandChildJSONObject);

		when(mockRootJSONObject.getLong(JSONKeys.ID)).thenReturn(ROOT_CATEGORY_ID);
		when(mockChildJSONObject.getLong(JSONKeys.ID)).thenReturn(CHILD_CATEGORY_ID);
		when(mockGrandChildJSONObject.getLong(JSONKeys.ID)).thenReturn(GRANDCHILD_CATEGORY_ID);


		categoryFacetTreeUtil.pruneCategoryTree(mockPrunedTreeJSON, mockTreeJSON, categoriesToPrune);

		verify(mockPrunedTreeJSON, times(1)).put(mockRootJSONObject);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.CHILDREN, mockChildrenJSONArray);
		verify(mockChildrenJSONArray, times(1)).put(mockChildJSONObject);
		verify(mockChildJSONObject, times(1)).put(JSONKeys.CHILDREN, mockGrandChildrenJSONArray);
		verify(mockGrandChildrenJSONArray, times(1)).put(mockGrandChildJSONObject);

	}

	@Test
	public void pruneCategoryTree_WhenNoErrorAndNoThereAreCategoriesToPrune_ThenReturnsJSONTreeWithoutThoseCategoriesNorItsChildren() {
		Set<Long> categoriesToPrune = new HashSet<>();
		categoriesToPrune.add(CHILD_CATEGORY_ID);

		mockJSONArrayCreation(false);
		mockFullTreeStructure();

		when(mockTreeJSON.length()).thenReturn(1);
		when(mockTreeJSON.getJSONObject(0)).thenReturn(mockRootJSONObject);
		when(mockRootJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockRootJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockChildrenJSONArray);
		when(mockChildrenJSONArray.length()).thenReturn(1);
		when(mockChildrenJSONArray.getJSONObject(0)).thenReturn(mockChildJSONObject);
		when(mockChildJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockChildJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockGrandChildrenJSONArray);
		when(mockGrandChildrenJSONArray.length()).thenReturn(1);
		when(mockGrandChildrenJSONArray.getJSONObject(0)).thenReturn(mockGrandChildJSONObject);

		when(mockRootJSONObject.getLong(JSONKeys.ID)).thenReturn(ROOT_CATEGORY_ID);
		when(mockChildJSONObject.getLong(JSONKeys.ID)).thenReturn(CHILD_CATEGORY_ID);
		when(mockGrandChildJSONObject.getLong(JSONKeys.ID)).thenReturn(GRANDCHILD_CATEGORY_ID);

		categoryFacetTreeUtil.pruneCategoryTree(mockPrunedTreeJSON, mockTreeJSON, categoriesToPrune);

		verify(mockPrunedTreeJSON, times(1)).put(mockRootJSONObject);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.CHILDREN, mockChildrenJSONArray);
		verify(mockChildrenJSONArray, never()).put(mockChildJSONObject);
		verify(mockChildJSONObject, never()).put(JSONKeys.CHILDREN, mockGrandChildrenJSONArray);
		verify(mockGrandChildrenJSONArray, never()).put(mockGrandChildJSONObject);

	}

	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndFacetResultExistParentCategoryIdIs0AndCategoryIsSelected_ThenPutsRootCategoryJSONObjectWithParamsSetToTheJSONTreeAndAddsItToInsertedCategories() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		Set<String> activeValuesForFilter = new HashSet<>();
		activeValuesForFilter.add(String.valueOf(ROOT_CATEGORY_ID));
		Set<Long> categoriesToPrune = new HashSet<>();
		int maxDepth = 25;
		int frequency = 5;

		when(mockJsonFactory.createJSONObject()).thenReturn(mockRootJSONObject);

		when(mockRootCategory.getCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockRootCategory.getParentCategoryId()).thenReturn(0L);
		when(mockRootFacetResult.getFrequency()).thenReturn(frequency);
		when(mockRootCategory.getTitle(LOCALE)).thenReturn(ROOT_CATEGORY_TITLE);

		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(mockRootFacetResult), mockRootCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockTreeJSON, times(1)).put(mockRootJSONObject);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.LABEL, ROOT_CATEGORY_TITLE + " (" + frequency + ")");
		verify(mockRootJSONObject, times(1)).put(JSONKeys.ID, ROOT_CATEGORY_ID);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.DEPTH, 0);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.TYPE, JSONKeys.CHECK);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.LEAF, Boolean.TRUE);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.CHECKED, Boolean.TRUE);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.FREQUENCY, frequency);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.EXPANDED, Boolean.TRUE);
		assertTrue(insertedCategories.containsKey(ROOT_CATEGORY_ID));
		assertThat(insertedCategories.get(ROOT_CATEGORY_ID), sameInstance(mockRootJSONObject));
	}

	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndFacetResultExistParentCategoryIdIs0AndCategoryIsNotSelected_ThenPutsRootCategoryJSONObjectWithParamsSetToTheJSONTreeAndAddsItToInsertedCategories() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		Set<String> activeValuesForFilter = new HashSet<>();
		Set<Long> categoriesToPrune = new HashSet<>();
		int maxDepth = 25;
		int frequency = 5;

		when(mockJsonFactory.createJSONObject()).thenReturn(mockRootJSONObject);

		when(mockRootCategory.getCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockRootCategory.getParentCategoryId()).thenReturn(0L);
		when(mockRootFacetResult.getFrequency()).thenReturn(frequency);
		when(mockRootCategory.getTitle(LOCALE)).thenReturn(ROOT_CATEGORY_TITLE);

		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(mockRootFacetResult), mockRootCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockTreeJSON, times(1)).put(mockRootJSONObject);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.LABEL, ROOT_CATEGORY_TITLE + " (" + frequency + ")");
		verify(mockRootJSONObject, times(1)).put(JSONKeys.ID, ROOT_CATEGORY_ID);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.DEPTH, 0);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.TYPE, JSONKeys.CHECK);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.LEAF, Boolean.TRUE);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.CHECKED, Boolean.FALSE);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.FREQUENCY, frequency);
		verify(mockRootJSONObject, never()).put(JSONKeys.EXPANDED, Boolean.TRUE);
		assertTrue(insertedCategories.containsKey(ROOT_CATEGORY_ID));
		assertThat(insertedCategories.get(ROOT_CATEGORY_ID), sameInstance(mockRootJSONObject));
	}


	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndFacetResultDoesNotExist_ThenPutsJSONObjectToTheTreeWithFrequency0() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		Set<String> activeValuesForFilter = new HashSet<>();
		Set<Long> categoriesToPrune = new HashSet<>();
		int maxDepth = 25;

		when(mockJsonFactory.createJSONObject()).thenReturn(mockRootJSONObject);

		when(mockRootCategory.getCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockRootCategory.getParentCategoryId()).thenReturn(0L);
		when(mockRootCategory.getTitle(LOCALE)).thenReturn(ROOT_CATEGORY_TITLE);

		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.empty(), mockRootCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockTreeJSON, times(1)).put(mockRootJSONObject);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.LABEL, ROOT_CATEGORY_TITLE + " (" + 0 + ")");
		verify(mockRootJSONObject, times(1)).put(JSONKeys.FREQUENCY, 0);

	}

	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndNotRootCategoryAndParentWasInsertedAndAlreadyHasChildrenAndDepthConditionIsSatisfied_ThenInsertsChildJSONObjectToTheAlreadyExistingChildrenArrayWithParentDepthPlus1SetAndSetsParentLeafAsFalse() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		insertedCategories.put(ROOT_CATEGORY_ID, mockRootJSONObject);

		Set<String> activeValuesForFilter = new HashSet<>();
		Set<Long> categoriesToPrune = new HashSet<>();
		int maxDepth = 25;
		int parentDepth = 3;
		int frequency = 5;

		when(mockJsonFactory.createJSONObject()).thenReturn(mockChildJSONObject);

		when(mockChildCategory.getCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockChildCategory.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockChildCategory.getTitle(LOCALE)).thenReturn(CHILD_CATEGORY_TITLE);
		when(mockRootJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockRootJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockChildrenJSONArray);
		when(mockRootJSONObject.getInt(JSONKeys.DEPTH)).thenReturn(parentDepth);
		when(mockChildFacetResult.getFrequency()).thenReturn(frequency);


		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(mockChildFacetResult), mockChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockChildrenJSONArray, times(1)).put(mockChildJSONObject);
		verify(mockChildJSONObject, times(1)).put(JSONKeys.LABEL, CHILD_CATEGORY_TITLE + " (" + frequency + ")");
		verify(mockChildJSONObject, times(1)).put(JSONKeys.ID, CHILD_CATEGORY_ID);
		verify(mockChildJSONObject, times(1)).put(JSONKeys.DEPTH, parentDepth + 1);
		verify(mockChildJSONObject, times(1)).put(JSONKeys.LEAF, Boolean.TRUE);
		verify(mockChildJSONObject, times(1)).put(JSONKeys.FREQUENCY, frequency);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.CHILDREN, mockChildrenJSONArray);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.LEAF, Boolean.FALSE);

		assertTrue(insertedCategories.containsKey(CHILD_CATEGORY_ID));
		assertThat(insertedCategories.get(CHILD_CATEGORY_ID), sameInstance(mockChildJSONObject));

	}

	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndNotRootCategoryAndParentWasInsertedAndHasNoChildrenAndDepthConditionIsSatisfied_ThenInsertsChildJSONObjectToTheNewChildrenArray() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		insertedCategories.put(ROOT_CATEGORY_ID, mockRootJSONObject);

		Set<String> activeValuesForFilter = new HashSet<>();
		Set<Long> categoriesToPrune = new HashSet<>();
		int maxDepth = 25;
		int parentDepth = 3;
		int frequency = 5;

		when(mockJsonFactory.createJSONObject()).thenReturn(mockChildJSONObject);
		when(mockJsonFactory.createJSONArray()).thenReturn(mockChildrenJSONArray);

		when(mockChildCategory.getCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockChildCategory.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockRootJSONObject.has(JSONKeys.CHILDREN)).thenReturn(false);
		when(mockRootJSONObject.getInt(JSONKeys.DEPTH)).thenReturn(parentDepth);
		when(mockChildFacetResult.getFrequency()).thenReturn(frequency);

		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(mockChildFacetResult), mockChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockChildrenJSONArray, times(1)).put(mockChildJSONObject);

		assertTrue(insertedCategories.containsKey(CHILD_CATEGORY_ID));
		assertThat(insertedCategories.get(CHILD_CATEGORY_ID), sameInstance(mockChildJSONObject));

	}

	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndNotRootCategoryAndParentWasInsertedAndCategoryIsSelected_ThenInsertsChildJSONObjectToChildrenArrayAndExpandsAllItsParentsUntilParentIsAlreadyExpanded() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		insertedCategories.put(ROOT_CATEGORY_ID, mockRootJSONObject);
		insertedCategories.put(CHILD_CATEGORY_ID, mockChildJSONObject);

		Set<String> activeValuesForFilter = new HashSet<>();
		activeValuesForFilter.add(String.valueOf(GRANDCHILD_CATEGORY_ID));
		Set<Long> categoriesToPrune = new HashSet<>();
		int maxDepth = 25;
		int parentDepth = 3;

		when(mockJsonFactory.createJSONObject()).thenReturn(mockGrandChildJSONObject);

		when(mockGrandChildCategory.getCategoryId()).thenReturn(GRANDCHILD_CATEGORY_ID);
		when(mockGrandChildCategory.getParentCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockGrandChildCategory.getParentCategory()).thenReturn(mockChildCategory);
		when(mockGrandChildCategory.getTitle(LOCALE)).thenReturn(GRANDCHILD_CATEGORY_TITLE);
		when(mockChildCategory.getCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockChildCategory.getParentCategory()).thenReturn(mockRootCategory);
		when(mockRootCategory.getCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockChildJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockChildJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockGrandChildrenJSONArray);
		when(mockChildJSONObject.getInt(JSONKeys.DEPTH)).thenReturn(parentDepth);
		when(mockChildJSONObject.get(JSONKeys.EXPANDED)).thenReturn(Boolean.FALSE);
		when(mockRootJSONObject.get(JSONKeys.EXPANDED)).thenReturn(Boolean.TRUE);

		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(mockGrandChildFacetResult), mockGrandChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockGrandChildrenJSONArray, times(1)).put(mockGrandChildJSONObject);
		verify(mockChildJSONObject, times(1)).put(JSONKeys.EXPANDED, Boolean.TRUE);
		verify(mockRootJSONObject, never()).put(JSONKeys.EXPANDED, Boolean.TRUE);

		assertTrue(insertedCategories.containsKey(GRANDCHILD_CATEGORY_ID));
		assertThat(insertedCategories.get(GRANDCHILD_CATEGORY_ID), sameInstance(mockGrandChildJSONObject));

	}

	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndNotRootCategoryAndParentWasInsertedWith0FrequencyAndDepthConditionIsNotSatisfied_ThenDoesNotInsertChildJSONObjectToTheChildrenArrayAndAddsParentToCategoriesToPruneSet() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		insertedCategories.put(ROOT_CATEGORY_ID, mockRootJSONObject);

		Set<String> activeValuesForFilter = new HashSet<>();
		Set<Long> categoriesToPrune = new HashSet<>();
		int maxDepth = 25;
		int parentDepth = maxDepth;
		int parentFrequency = 0;

		when(mockJsonFactory.createJSONObject()).thenReturn(mockChildJSONObject);
		when(mockJsonFactory.createJSONArray()).thenReturn(mockChildrenJSONArray);

		when(mockChildCategory.getCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockChildCategory.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockRootJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockRootJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockChildrenJSONArray);
		when(mockRootJSONObject.getInt(JSONKeys.DEPTH)).thenReturn(parentDepth);
		when(mockRootJSONObject.getLong(JSONKeys.ID)).thenReturn(ROOT_CATEGORY_ID);
		when(mockRootJSONObject.getInt(JSONKeys.FREQUENCY)).thenReturn(parentFrequency);

		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(mockChildFacetResult), mockChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockChildrenJSONArray, never()).put(mockChildJSONObject);

		assertFalse(insertedCategories.containsKey(CHILD_CATEGORY_ID));
		assertTrue(categoriesToPrune.contains(ROOT_CATEGORY_ID));

	}

	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndNotRootCategoryAndParentWasInsertedWithMoreThan0FrequencyAndDepthConditionIsNotSatisfied_ThenDoesNotInsertChildJSONObjectToTheChildrenArrayAndDoesNotAddParentToCategoriesToPruneSetAndSetsParentAsLeaf() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		insertedCategories.put(ROOT_CATEGORY_ID, mockRootJSONObject);

		Set<String> activeValuesForFilter = new HashSet<>();
		Set<Long> categoriesToPrune = new HashSet<>();
		int maxDepth = 25;
		int parentDepth = maxDepth;
		int parentFrequency = 5;

		when(mockJsonFactory.createJSONObject()).thenReturn(mockChildJSONObject);
		when(mockJsonFactory.createJSONArray()).thenReturn(mockChildrenJSONArray);

		when(mockChildCategory.getCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockChildCategory.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockRootJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockRootJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockChildrenJSONArray);
		when(mockRootJSONObject.getInt(JSONKeys.DEPTH)).thenReturn(parentDepth);
		when(mockRootJSONObject.getInt(JSONKeys.FREQUENCY)).thenReturn(parentFrequency);

		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(mockChildFacetResult), mockChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockChildrenJSONArray, never()).put(mockChildJSONObject);
		verify(mockRootJSONObject, times(1)).put(JSONKeys.LEAF, Boolean.TRUE);

		assertFalse(insertedCategories.containsKey(CHILD_CATEGORY_ID));
		assertFalse(categoriesToPrune.contains(ROOT_CATEGORY_ID));

	}

	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndNotRootCategoryAndParentWasNotInserted_ThenInsertsParentObjectFirstAndThenTheChildObject() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		insertedCategories.put(ROOT_CATEGORY_ID, mockRootJSONObject);

		Set<String> activeValuesForFilter = new HashSet<>();
		Set<Long> categoriesToPrune = new HashSet<>();
		int maxDepth = 25;

		AtomicBoolean wasObjectCreated = new AtomicBoolean(false);
		doAnswer((Answer<JSONObject>) invocation -> {
			if (wasObjectCreated.get()) {
				return mockGrandChildJSONObject;
			} else {
				wasObjectCreated.set(true);
				return mockChildJSONObject;
			}
		}).when(mockJsonFactory).createJSONObject();

		when(mockGrandChildCategory.getCategoryId()).thenReturn(GRANDCHILD_CATEGORY_ID);
		when(mockGrandChildCategory.getParentCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockGrandChildCategory.getParentCategory()).thenReturn(mockChildCategory);
		when(mockChildJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockChildJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockGrandChildrenJSONArray);

		when(mockChildCategory.getCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockChildCategory.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockChildCategory.getParentCategory()).thenReturn(mockRootCategory);
		when(mockRootJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockRootJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockChildrenJSONArray);

		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(mockGrandChildFacetResult), mockGrandChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockGrandChildrenJSONArray, times(1)).put(mockGrandChildJSONObject);
		verify(mockChildrenJSONArray, times(1)).put(mockChildJSONObject);

		assertTrue(insertedCategories.containsKey(GRANDCHILD_CATEGORY_ID));
		assertThat(insertedCategories.get(GRANDCHILD_CATEGORY_ID), sameInstance(mockGrandChildJSONObject));

		assertTrue(insertedCategories.containsKey(CHILD_CATEGORY_ID));
		assertThat(insertedCategories.get(CHILD_CATEGORY_ID), sameInstance(mockChildJSONObject));

	}

	@Test
	public void buildCategoryTreeBranch_WhenNoErrorAndNotRootCategoryAndParentWasNotInsertedAndParentDepthExceedsMaximalDepth_ThenDoesNotInsertParentObjectNorChildObject() {
		Map<Long, JSONObject> insertedCategories = new HashMap<>();
		insertedCategories.put(ROOT_CATEGORY_ID, mockRootJSONObject);

		Set<String> activeValuesForFilter = new HashSet<>();
		Set<Long> categoriesToPrune = new HashSet<>();
		int parentDepth = 3;
		int maxDepth = parentDepth;

		AtomicBoolean wasObjectCreated = new AtomicBoolean(false);
		doAnswer((Answer<JSONObject>) invocation -> {
			if (wasObjectCreated.get()) {
				return mockGrandChildJSONObject;
			} else {
				wasObjectCreated.set(true);
				return mockChildJSONObject;
			}
		}).when(mockJsonFactory).createJSONObject();

		when(mockGrandChildCategory.getCategoryId()).thenReturn(GRANDCHILD_CATEGORY_ID);
		when(mockGrandChildCategory.getParentCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockGrandChildCategory.getParentCategory()).thenReturn(mockChildCategory);
		when(mockChildJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockChildJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockGrandChildrenJSONArray);

		when(mockChildCategory.getCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockChildCategory.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockChildCategory.getParentCategory()).thenReturn(mockRootCategory);
		when(mockRootJSONObject.has(JSONKeys.CHILDREN)).thenReturn(true);
		when(mockRootJSONObject.getJSONArray(JSONKeys.CHILDREN)).thenReturn(mockChildrenJSONArray);
		when(mockRootJSONObject.getInt(JSONKeys.DEPTH)).thenReturn(parentDepth);

		categoryFacetTreeUtil.buildCategoryTreeBranch(Optional.of(mockGrandChildFacetResult), mockGrandChildCategory, mockTreeJSON, insertedCategories, activeValuesForFilter, maxDepth, categoriesToPrune, LOCALE);

		verify(mockGrandChildrenJSONArray, never()).put(mockGrandChildJSONObject);
		verify(mockChildrenJSONArray, never()).put(mockChildJSONObject);

		assertFalse(insertedCategories.containsKey(GRANDCHILD_CATEGORY_ID));

		assertFalse(insertedCategories.containsKey(CHILD_CATEGORY_ID));

	}

	private void mockFullTreeStructure() {
		when(mockRootCategory.getCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockChildCategory.getCategoryId()).thenReturn(CHILD_CATEGORY_ID);
		when(mockGrandChildCategory.getCategoryId()).thenReturn(GRANDCHILD_CATEGORY_ID);

		when(mockChildCategory.getParentCategory()).thenReturn(mockRootCategory);
		when(mockGrandChildCategory.getParentCategory()).thenReturn(mockChildCategory);

		when(mockRootCategory.getParentCategoryId()).thenReturn(0L);
		when(mockChildCategory.getParentCategoryId()).thenReturn(ROOT_CATEGORY_ID);
		when(mockGrandChildCategory.getParentCategoryId()).thenReturn(CHILD_CATEGORY_ID);
	}
	private void mockJSONArrayCreation(boolean includeTreeJSON) {
		int startIndex = includeTreeJSON ? 0 : 1;
		AtomicInteger arrayCounter = new AtomicInteger(startIndex);
		when(mockJsonFactory.createJSONArray()).thenAnswer((Answer<JSONArray>) invocation -> {
			int counterValue = arrayCounter.get();
			JSONArray result;
			switch (counterValue) {
			case 0:
				result = mockTreeJSON;
				break;
			case 1:
				result = mockChildrenJSONArray;
				break;
			case 2:
				result = mockGrandChildrenJSONArray;
				break;
			default:
				return mockInvalidChildrenJSONArray;
			}
			arrayCounter.incrementAndGet();
			return result;
		});
	}

}
