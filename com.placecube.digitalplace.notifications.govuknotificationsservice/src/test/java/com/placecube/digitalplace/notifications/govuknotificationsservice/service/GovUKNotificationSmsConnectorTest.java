package com.placecube.digitalplace.notifications.govuknotificationsservice.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.notifications.exception.NotificationsException;
import com.placecube.digitalplace.notifications.model.SendSmsContext;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import uk.gov.service.notify.NotificationClient;
import uk.gov.service.notify.NotificationClientException;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class GovUKNotificationSmsConnectorTest extends PowerMockito {

	private static final long COMPANY_ID = 1l;

	private static final String API_KEY_NAME = "API KEY NAME";

	@InjectMocks
	private GovUKNotificationSmsConnector govUKNotificationSmsConnector;

	@Mock
	private GovUKNotificationsService mockGovUKNotificationsService;

	@Mock
	private NotificationClient mockNotificationclient;

	@Mock
	private Map<String, Object> mockPersonalisation;

	@Mock
	private SendSmsContext mockSendSmsContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockGovUKNotificationsService.isSmsEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = govUKNotificationSmsConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockGovUKNotificationsService.isSmsEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = govUKNotificationSmsConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test(expected = ConfigurationException.class)
	public void sendSms_WhenExceptionRetrievingApplicationKey_ThenThrowsConfigurationException() throws Exception {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSendSmsContext.getApiKeyName()).thenReturn(API_KEY_NAME);
		when(mockGovUKNotificationsService.getApplicationKey(COMPANY_ID, API_KEY_NAME)).thenThrow(new ConfigurationException());

		govUKNotificationSmsConnector.sendSms(mockSendSmsContext, mockServiceContext);
	}

	@Test(expected = NotificationsException.class)
	public void sendSms_WhenExceptionSendingSmsWithoutSenderId_ThenThrowsNotificationsException() throws Exception {
		String appKey = "applicationKeyValue";
		String templateId = "templateIdValue";
		String phone = "phoneValue";
		String reference = "referenceValue";
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSendSmsContext.getApiKeyName()).thenReturn(API_KEY_NAME);
		when(mockGovUKNotificationsService.getApplicationKey(COMPANY_ID, API_KEY_NAME)).thenReturn(appKey);
		when(mockGovUKNotificationsService.getClient(appKey)).thenReturn(mockNotificationclient);
		when(mockSendSmsContext.getTemplateId()).thenReturn(templateId);
		when(mockSendSmsContext.getPhoneNumber()).thenReturn(phone);
		when(mockSendSmsContext.getPersonalisation()).thenReturn(mockPersonalisation);
		when(mockSendSmsContext.getReference()).thenReturn(reference);
		when(mockSendSmsContext.getSmsSenderId()).thenReturn(StringPool.BLANK);
		when(mockNotificationclient.sendSms(templateId, phone, mockPersonalisation, reference)).thenThrow(new NotificationClientException("msg"));

		govUKNotificationSmsConnector.sendSms(mockSendSmsContext, mockServiceContext);

	}

	@Test(expected = NotificationsException.class)
	public void sendSms_WhenExceptionSendingSmsWithSenderId_ThenThrowsNotificationsException() throws Exception {
		String appKey = "applicationKeyValue";
		String templateId = "templateIdValue";
		String phone = "phoneValue";
		String reference = "referenceValue";
		String senderId = "senderIdValue";
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSendSmsContext.getApiKeyName()).thenReturn(API_KEY_NAME);
		when(mockGovUKNotificationsService.getApplicationKey(COMPANY_ID, API_KEY_NAME)).thenReturn(appKey);
		when(mockGovUKNotificationsService.getClient(appKey)).thenReturn(mockNotificationclient);
		when(mockSendSmsContext.getTemplateId()).thenReturn(templateId);
		when(mockSendSmsContext.getPhoneNumber()).thenReturn(phone);
		when(mockSendSmsContext.getPersonalisation()).thenReturn(mockPersonalisation);
		when(mockSendSmsContext.getReference()).thenReturn(reference);
		when(mockSendSmsContext.getSmsSenderId()).thenReturn(senderId);
		when(mockNotificationclient.sendSms(templateId, phone, mockPersonalisation, reference, senderId)).thenThrow(new NotificationClientException("msg"));

		govUKNotificationSmsConnector.sendSms(mockSendSmsContext, mockServiceContext);

	}

	@Test
	public void sendSms_WhenNoErrorAndSenderIdNotSpecified_ThenSendsTheSmsWithoutTheSenderId() throws Exception {
		String appKey = "applicationKeyValue";
		String templateId = "templateIdValue";
		String phone = "phoneValue";
		String reference = "referenceValue";
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSendSmsContext.getApiKeyName()).thenReturn(API_KEY_NAME);
		when(mockGovUKNotificationsService.getApplicationKey(COMPANY_ID, API_KEY_NAME)).thenReturn(appKey);
		when(mockGovUKNotificationsService.getClient(appKey)).thenReturn(mockNotificationclient);
		when(mockSendSmsContext.getTemplateId()).thenReturn(templateId);
		when(mockSendSmsContext.getPhoneNumber()).thenReturn(phone);
		when(mockSendSmsContext.getPersonalisation()).thenReturn(mockPersonalisation);
		when(mockSendSmsContext.getReference()).thenReturn(reference);

		govUKNotificationSmsConnector.sendSms(mockSendSmsContext, mockServiceContext);

		verify(mockNotificationclient, times(1)).sendSms(templateId, phone, mockPersonalisation, reference);
	}

	@Test
	public void sendSms_WhenNoErrorAndSenderIdSpecified_ThenSendsTheSmsWithTheSenderId() throws Exception {
		String appKey = "applicationKeyValue";
		String templateId = "templateIdValue";
		String phone = "phoneValue";
		String reference = "referenceValue";
		String senderId = "senderIdValue";
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockSendSmsContext.getApiKeyName()).thenReturn(API_KEY_NAME);
		when(mockGovUKNotificationsService.getApplicationKey(COMPANY_ID, API_KEY_NAME)).thenReturn(appKey);
		when(mockGovUKNotificationsService.getClient(appKey)).thenReturn(mockNotificationclient);
		when(mockSendSmsContext.getTemplateId()).thenReturn(templateId);
		when(mockSendSmsContext.getPhoneNumber()).thenReturn(phone);
		when(mockSendSmsContext.getPersonalisation()).thenReturn(mockPersonalisation);
		when(mockSendSmsContext.getReference()).thenReturn(reference);
		when(mockSendSmsContext.getSmsSenderId()).thenReturn(senderId);

		govUKNotificationSmsConnector.sendSms(mockSendSmsContext, mockServiceContext);

		verify(mockNotificationclient, times(1)).sendSms(templateId, phone, mockPersonalisation, reference, senderId);
	}

}
