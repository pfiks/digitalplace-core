package com.placecube.digitalplace.userimport.admin.web.portlet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.placecube.digitalplace.userimport.admin.web.constants.UserProfileFields;
import com.placecube.digitalplace.userimport.admin.web.service.UserImportCategoryService;
import com.placecube.digitalplace.userimport.admin.web.service.UserImportService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceContextThreadLocal.class)
public class UserImportMVCRenderCommandTest extends PowerMockito {

	@Mock
	private AssetVocabulary mockAssetVocabulary;

	@Mock
	private ExpandoColumn mockExpandoColumn1;

	@Mock
	private ExpandoColumn mockExpandoColumn2;

	@Mock
	private ExpandoColumn mockExpandoColumn3;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private UserImportCategoryService mockUserImportCategoryService;

	@Mock
	private UserImportService mockUserImportService;

	@InjectMocks
	private UserImportMVCRenderCommand userImportMVCRenderCommand;

	@Before
	public void activeSetUp() {
		mockStatic(ServiceContextThreadLocal.class);
		when(ServiceContextThreadLocal.getServiceContext()).thenReturn(mockServiceContext);
	}

	@Test
	public void render_WhenCalled_ThenSetsUserProfileFieldsAsAttribute() throws PortletException, PortalException {
		long mockCompanyId = 1255L;
		when(mockServiceContext.getCompanyId()).thenReturn(mockCompanyId);
		when(mockUserImportService.getUserExpandoFields(mockServiceContext)).thenReturn(Collections.emptyList());
		when(mockUserImportCategoryService.getGlobalVocabulariesOfUserType(mockCompanyId)).thenReturn(Collections.emptyList());

		String result = userImportMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/user-import/view.jsp"));
		verify(mockRenderRequest, times(1)).setAttribute("userProfileFields", UserProfileFields.values());
	}

	@Test
	public void render_WhenExpandoFieldsAreVisible_ThenSetsVisibleExpandosAsAttribute() throws PortletException, PortalException {
		UnicodeProperties mockUnicodeProperties1 = mock(UnicodeProperties.class);
		when(mockUnicodeProperties1.get("hidden")).thenReturn("false");
		when(mockExpandoColumn1.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties1);

		UnicodeProperties mockUnicodeProperties2 = mock(UnicodeProperties.class);
		when(mockUnicodeProperties2.get("hidden")).thenReturn("true");
		when(mockExpandoColumn2.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties2);

		UnicodeProperties mockUnicodeProperties3 = mock(UnicodeProperties.class);
		when(mockUnicodeProperties3.get("hidden")).thenReturn("1");
		when(mockExpandoColumn3.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties3);

		long mockCompanyId = 1255L;
		when(mockServiceContext.getCompanyId()).thenReturn(mockCompanyId);
		when(mockUserImportService.getUserExpandoFields(mockServiceContext)).thenReturn(Arrays.asList(mockExpandoColumn1, mockExpandoColumn2, mockExpandoColumn3));
		when(mockUserImportCategoryService.getGlobalVocabulariesOfUserType(mockCompanyId)).thenReturn(Collections.emptyList());

		userImportMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("userExpandoFields", Arrays.asList(mockExpandoColumn1));
	}

	@Test
	public void render_WhenGlobalVocabulariesAreAvailable_ThenSetsVocabulariesAsAttribute() throws PortalException, PortletException {
		long mockCompanyId = 1255L;
		when(mockServiceContext.getCompanyId()).thenReturn(mockCompanyId);

		when(mockUserImportService.getUserExpandoFields(mockServiceContext)).thenReturn(Collections.emptyList());
		when(mockUserImportCategoryService.getGlobalVocabulariesOfUserType(mockCompanyId)).thenReturn(Collections.singletonList(mockAssetVocabulary));

		userImportMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("vocabularies", Arrays.asList(mockAssetVocabulary));
	}

	@Test
	public void render_WhenPortalExceptionOccurs_ThenDoesNotSetVocabulariesAttribute() throws PortalException, PortletException {
		long mockCompanyId = 1255L;
		when(mockServiceContext.getCompanyId()).thenReturn(mockCompanyId);

		when(mockUserImportService.getUserExpandoFields(mockServiceContext)).thenReturn(Collections.emptyList());
		when(mockUserImportCategoryService.getGlobalVocabulariesOfUserType(mockCompanyId)).thenThrow(new PortalException());

		String result = userImportMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/user-import/view.jsp"));
		verify(mockRenderRequest, times(1)).setAttribute("userProfileFields", UserProfileFields.values());
		verify(mockRenderRequest, times(1)).setAttribute("userExpandoFields", Collections.emptyList());

		verify(mockRenderRequest, never()).setAttribute(eq("vocabularies"), anyCollection());
	}
}
