package com.placecube.digitalplace.search.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "search", scope = ExtendedObjectClassDefinition.Scope.COMPANY)
@Meta.OCD(id = "com.placecube.digitalplace.search.configuration.SearchCompanyConfiguration", localization = "content/Language", name = "digitalplace-search")
public interface SearchCompanyConfiguration {

	@Meta.AD(required = false, deflt = "1")
	int facetMultiplier();
}
