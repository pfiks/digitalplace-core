<%@ include file="/init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="<%= true %>" var="configurationActionURL" />
<liferay-frontend:edit-form
	action="<%= configurationActionURL %>"
	cssClass="container-fluid container-fluid-max-xl"
	method="post"
>
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<liferay-frontend:edit-form-body>
		<liferay-frontend:fieldset>
			
			<aui:input checked="${ emailAddressRequired }" label="email-address-required" name="preferences--emailAddressRequired--" type="checkbox" value="${ emailAddressRequired }" />
		</liferay-frontend:fieldset>
		
	</liferay-frontend:edit-form-body>
	<liferay-frontend:edit-form-footer>
		<liferay-frontend:edit-form-buttons />
	</liferay-frontend:edit-form-footer>
</liferay-frontend:edit-form>