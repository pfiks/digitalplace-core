package com.placecube.digitalplace.twitterfeed.constants;

public final class ViewKeys {

	public static final String VIEW_JSP = "/view.jsp";

	private ViewKeys() {
	}
}
