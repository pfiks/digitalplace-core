package com.placecube.digitalplace.navigation.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.category.property.service.AssetCategoryPropertyLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.category.service.CategoryRetrievalService;

@Component(immediate = true, service = AssetCategoryService.class)
public class AssetCategoryService {

	private static final Log LOG = LogFactoryUtil.getLog(AssetCategoryService.class);

	@Reference
	private AssetCategoryLayoutService assetCategoryLayoutService;

	@Reference
	private AssetCategoryPropertyLocalService assetCategoryPropertyLocalService;

	@Reference
	private CategoryRetrievalService categoryRetrievalService;

	public List<AssetCategory> getAncestors(AssetCategory assetCategory) {

		List<AssetCategory> assetCategories;

		try {

			assetCategories = assetCategory.getAncestors();

		} catch (PortalException e) {

			LOG.debug(e);

			assetCategories = Collections.emptyList();

		}

		return assetCategories;

	}

	public List<AssetCategory> getCategories(AssetEntry assetEntry, long filterVocabularyId) {

		List<AssetCategory> categories = assetEntry.getCategories();

		return categories.stream().filter(category -> category.getVocabularyId() == filterVocabularyId).collect(Collectors.toList());

	}

	public List<List<AssetCategory>> getHierarchicalCategories(List<AssetCategory> categories, long filterVocabularyId) throws PortalException {

		List<List<AssetCategory>> hierarchicalCategories = new ArrayList<>();

		for (AssetCategory category : categories) {

			if (category.getVocabularyId() == filterVocabularyId) {

				List<AssetCategory> categoryHierarchy = new ArrayList<>();
				List<AssetCategory> ancestors = category.getAncestors();
				if (!ancestors.isEmpty()) {
					Collections.reverse(ancestors);
					categoryHierarchy.addAll(ancestors);
				}
				categoryHierarchy.add(category);
				hierarchicalCategories.add(categoryHierarchy);
			}

		}

		return hierarchicalCategories;

	}

	public List<AssetCategory> getTheDeepestCategories(List<AssetCategory> assetCategories) throws PortalException {

		List<AssetCategory> deepestCategories = assetCategories;
		List<AssetCategory> ancestors = new ArrayList<>();
		for (AssetCategory assetCategory : assetCategories) {

			ancestors.addAll(assetCategory.getAncestors());

		}
		deepestCategories.removeAll(ancestors);
		return deepestCategories;
	}

	public String getAssetCategoryUrl(AssetCategory assetCategory, String urlCategoryPropertyName, String buildCategoriesHierarchyUrlPropertyName, boolean useLayoutUrl, long scopeGroupId) {
		boolean buildCategoriesHiearchyUrlProperty = GetterUtil.getBoolean(categoryRetrievalService.getPropertyValue(assetCategory, buildCategoriesHierarchyUrlPropertyName));

		String url = getAssetCategoryUrl(assetCategory, urlCategoryPropertyName, useLayoutUrl, scopeGroupId);

		if (buildCategoriesHiearchyUrlProperty && assetCategory.getParentCategory() != null) {
			return getAssetCategoryUrl(assetCategory.getParentCategory(), urlCategoryPropertyName, buildCategoriesHierarchyUrlPropertyName, useLayoutUrl, scopeGroupId) + url;
		} else {
			return url;
		}
	}

	private String getAssetCategoryUrl(AssetCategory assetCategory, String urlCategoryPropertyName, boolean useLayoutUrl, long scopeGroupId) {
		if (useLayoutUrl) {
			return getAssetCategoryLayoutFriendlyUrlValue(assetCategory, scopeGroupId);
		} else {
			return getAssetCategoryUrlPropertyValue(assetCategory, urlCategoryPropertyName);
		}
	}

	private String getAssetCategoryUrlPropertyValue(AssetCategory assetCategory, String urlCategoryPropertyName) {
		String urlProperty = categoryRetrievalService.getPropertyValue(assetCategory, urlCategoryPropertyName);

		return Validator.isNull(urlProperty) ? StringPool.BLANK : StringPool.FORWARD_SLASH + urlProperty;
	}

	private String getAssetCategoryLayoutFriendlyUrlValue(AssetCategory assetCategory, long scopeGroupId) {
		Optional<Layout> layoutOpt = assetCategoryLayoutService.getLayoutForCategory(assetCategory, scopeGroupId);

		return layoutOpt.isPresent() ? layoutOpt.get().getFriendlyURL() : StringPool.BLANK;

	}
}
