package com.placecube.digitalplace.ddl.web.portlet.display.friendlyurl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;

public class DDLDisplayFriendlyURLMapperTest extends PowerMockito {

	@InjectMocks
	private DDLDisplayFriendlyURLMapper ddlDisplayFriendlyURLMapper;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getMapping_WhenNoError_ThenReturnsTheDDLDisplayMappingKey() {
		String result = ddlDisplayFriendlyURLMapper.getMapping();

		assertThat(result, equalTo("ddl_display"));
	}

}
