package com.placecube.digitalplace.user.account.connector.constants;

public final class AccountConnectorConstant {

	public static final String MODEL_ID = "modelId";

	private AccountConnectorConstant() {
	}
}
