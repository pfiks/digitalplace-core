package com.placecube.digitalplace.user.settings.web.twofactor.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionMessages.class, SessionErrors.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SaveTwoFactorAuthenticationMVCActionCommandTest {

	private static final String ONE_TIME_CODE = "oneTimeCodeValue";
	private static final String SECRET_KEY = "secretKeyValue";

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MutableRenderParameters mockRenderParameters;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private User mockUser;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	@InjectMocks
	private SaveTwoFactorAuthenticationMVCActionCommand saveTwoFactorAuthenticationMVCActionCommand;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(PropsUtil.class, ParamUtil.class, PortalUtil.class, SessionMessages.class, SessionErrors.class);
	}

	@Test
	@Parameters({ "true", "false" })
	public void doProcessAction_WhenNoError_ThenVerifiesAndEnablesThe2FAForTheUser(boolean isOneTimeCodeCorrect) throws PortalException {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(ParamUtil.getString(mockActionRequest, TwoFactorAuthenticationRequestKeys.TWO_FACTOR_VERIFICATION_CODE)).thenReturn(ONE_TIME_CODE);
		when(ParamUtil.getString(mockActionRequest, TwoFactorAuthenticationRequestKeys.SECRET_KEY)).thenReturn(SECRET_KEY);
		when(mockUserTwoFactorAuthenticationLocalService.verifyAndEnableUserTwoFactorAuthentication(mockUser, ONE_TIME_CODE, SECRET_KEY)).thenReturn(isOneTimeCodeCorrect);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		saveTwoFactorAuthenticationMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockUserTwoFactorAuthenticationLocalService, times(1)).verifyAndEnableUserTwoFactorAuthentication(mockUser, ONE_TIME_CODE, SECRET_KEY);
		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, !isOneTimeCodeCorrect);
		if (!isOneTimeCodeCorrect) {
			verifyStatic(SessionErrors.class, times(1));
			SessionErrors.add(mockActionRequest, "twoFactorVerificationFailed");
		}
		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, Boolean.TRUE);
		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY, SECRET_KEY);
		verify(mockRenderParameters, times(1)).setValue(PortletRequestKeys.CURRENT_SECTION_ID, TwoFactorAuthenticationKeys.BUNDLE_ID);
	}

	@Test
	public void doProcessAction_WhenExceptionVerifyingTheOneTimeCode_ThenAddsErrorToSessionErrors() throws PortalException {
		when(mockActionRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);
		when(ParamUtil.getString(mockActionRequest, TwoFactorAuthenticationRequestKeys.TWO_FACTOR_VERIFICATION_CODE)).thenReturn(ONE_TIME_CODE);
		when(ParamUtil.getString(mockActionRequest, TwoFactorAuthenticationRequestKeys.SECRET_KEY)).thenReturn(SECRET_KEY);
		when(mockUserTwoFactorAuthenticationLocalService.verifyAndEnableUserTwoFactorAuthentication(mockUser, ONE_TIME_CODE, SECRET_KEY)).thenThrow(PortalException.class);
		when(mockActionResponse.getRenderParameters()).thenReturn(mockRenderParameters);

		saveTwoFactorAuthenticationMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockUserTwoFactorAuthenticationLocalService, times(1)).verifyAndEnableUserTwoFactorAuthentication(mockUser, ONE_TIME_CODE, SECRET_KEY);
		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY_GENERATED, true);
		verifyStatic(SessionErrors.class, times(1));
		SessionErrors.add(mockActionRequest, "twoFactorVerificationFailed");
		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, Boolean.TRUE);
		verify(mockActionRequest, times(1)).setAttribute(TwoFactorAuthenticationRequestKeys.SECRET_KEY, SECRET_KEY);
		verify(mockRenderParameters, times(1)).setValue(PortletRequestKeys.CURRENT_SECTION_ID, TwoFactorAuthenticationKeys.BUNDLE_ID);
	}

}
