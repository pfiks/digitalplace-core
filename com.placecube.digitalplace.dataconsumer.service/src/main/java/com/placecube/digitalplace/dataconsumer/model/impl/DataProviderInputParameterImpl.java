package com.placecube.digitalplace.dataconsumer.model.impl;

import com.placecube.digitalplace.dataconsumer.model.DataProviderInputParameter;

public class DataProviderInputParameterImpl implements DataProviderInputParameter {

	private String label;
	private String name;
	private boolean required;
	private String type;

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public boolean isRequired() {
		return required;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public void setType(String type) {
		this.type = type;
	}

}
