package com.placecube.digitalplace.payment.govuk.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.osgi.framework.ServiceException;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.payment.constants.PaymentStatus;
import com.placecube.digitalplace.payment.govuk.service.internal.GovUKPaymentOrderFactory;
import com.placecube.digitalplace.payment.govuk.service.internal.models.GovUKPaymentProperties;
import com.placecube.digitalplace.payment.model.PaymentContext;
import com.placecube.digitalplace.payment.model.PaymentResponse;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class GovUKPaymentConnectorTest extends PowerMockito {

	private static final String API_AUTH_KEY = "API_AUTH_KEY";
	private static final String BASE_URL = "BASE_URL";
	private static final long COMPANY_ID = 10;
	private static final String CREATE_PAYMENT_URL = "CREATE-PAYMENT_URL";
	private static final String FAIL_CREATED_PAYMENT_URL = "PAYMENT_CREATED_FAILED";
	private static final int FAILED_STATUS_CODE = 500;
	private static final String GOVUK_BLANK_PAYMENT_ID = StringPool.BLANK;
	private static final String GOVUK_PAYMENT_ID = "GOVUK_PAYMENT_ID";
	private static final int PAYMENT_CREATED_FAIL_STATUS_CODE = 500;
	private static final int PAYMENT_CREATED_SUCCESS_STATUS_CODE = 201;
	private static final String PAYMENT_REFERENCE = "PAYMENT_REFERENCE";
	private static final String PAYMENT_STATUS_URL = "PAYMENT_STATUS_URL";
	private static final String PAYMENT_SUCCESS_STATE = "SUCCESS";
	private static final String SUCCESS_CREATED_PAYMENT_URL = "https://www.google.com";
	private static final int SUCCESS_STATUS_CODE = 200;

	@InjectMocks
	private GovUKPaymentConnector govUKPaymentConnector;

	@Mock
	private CloseableHttpClient mockClient;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private GovUKPaymentOrderFactory mockGovUKPaymentOrderFactory;

	@Mock
	private GovUKPaymentProperties mockGovUKPaymentProperties;

	@Mock
	private GovUKPaymentService mockGovUKPaymentService;

	@Mock
	private HttpEntity mockHttpEntity;

	@Mock
	private HttpGet mockHttpGet;

	@Mock
	private HttpPost mockHttpPost;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpSession mockhttpSession;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private PaymentContext mockPaymentContext;

	@Mock
	private Portal mockPortal;

	@Mock
	private CloseableHttpResponse mockResponse;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private StatusLine mockStatusLine;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockGovUKPaymentService.isEnabled(COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = govUKPaymentConnector.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockGovUKPaymentService.isEnabled(COMPANY_ID)).thenReturn(expected);

		boolean result = govUKPaymentConnector.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getBackURL_WhenRequestRedirectAttributeIsPresent_ThenReturnRedirectWithParamValue() {
		String redirectAttributeValue = "redirect-value";
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(GetterUtil.getString(mockServiceContext.getAttribute("redirect"))).thenReturn(redirectAttributeValue);

		Optional<String> result = govUKPaymentConnector.getBackURL(redirectAttributeValue, mockServiceContext);

		assertEquals(redirectAttributeValue, result.get());
	}

	@Test
	public void getBackURL_WhenRequestRedirectAttributeNotPresent_ThenReturnEmptyOptional() {
		when(GetterUtil.getString(mockServiceContext.getAttribute("redirect"))).thenReturn(null);

		Optional<String> result = govUKPaymentConnector.getBackURL(StringPool.BLANK, mockServiceContext);

		assertFalse(result.isPresent());
	}

	@Test
	public void getPaymentStatus_WhenErrorGettingConfiguration_ThenReturnStatusAppError() throws ServiceException, ConfigurationException {
		String expected = PaymentStatus.appError().getValue();
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenThrow(new ConfigurationException());

		PaymentStatus paymentStatus = govUKPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		String actual = paymentStatus.getValue();

		assertEquals(expected, actual);
	}

	@Test
	public void getPaymentStatus_WhenExceptionExecutingRequest_ThenReturnAppErrorPaymentStatus() throws ServiceException, ConfigurationException, ClientProtocolException, IOException, JSONException {
		String expected = PaymentStatus.appError().getValue();
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getPaymentStatusURL()).thenReturn(PAYMENT_STATUS_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(GOVUK_PAYMENT_ID);
		when(mockGovUKPaymentOrderFactory.getHttpGetMethod(BASE_URL + PAYMENT_STATUS_URL, API_AUTH_KEY)).thenReturn(mockHttpGet);
		when(mockGovUKPaymentOrderFactory.getHttpClient()).thenReturn(mockClient);
		when(mockGovUKPaymentOrderFactory.executeRequest(mockHttpGet, mockClient)).thenThrow(new ClientProtocolException());

		PaymentStatus paymentStatus = govUKPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		String actual = paymentStatus.getValue();

		assertEquals(expected, actual);
	}

	@Test
	public void getPaymentStatus_WhenPaymentIdIsNull_ThenReturnPaymentErrorStatus() throws ServiceException, ConfigurationException {
		String expected = PaymentStatus.paymentError().getValue();
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getPaymentStatusURL()).thenReturn(PAYMENT_STATUS_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(GOVUK_BLANK_PAYMENT_ID);

		PaymentStatus paymentStatus = govUKPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		String actual = paymentStatus.getValue();

		assertEquals(expected, actual);
	}

	@Test
	public void getPaymentStatus_WhenResponseStatusCodeIsEqualsToPaymentSuccess_ThenReturnSuccessPaymentStatus()
			throws ServiceException, ConfigurationException, ClientProtocolException, IOException, JSONException {
		String expected = PaymentStatus.success().getValue();
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getPaymentStatusURL()).thenReturn(PAYMENT_STATUS_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(GOVUK_PAYMENT_ID);
		when(mockGovUKPaymentOrderFactory.getHttpGetMethod(BASE_URL + PAYMENT_STATUS_URL, API_AUTH_KEY)).thenReturn(mockHttpGet);
		when(mockGovUKPaymentOrderFactory.getHttpClient()).thenReturn(mockClient);
		when(mockGovUKPaymentOrderFactory.executeRequest(mockHttpGet, mockClient)).thenReturn(mockResponse);
		when(mockGovUKPaymentOrderFactory.getResponseStatusCode(mockResponse)).thenReturn(SUCCESS_STATUS_CODE);
		when(mockGovUKPaymentOrderFactory.getResponseStatusJSON(mockResponse)).thenReturn(mockJsonObject);
		when(mockJsonObject.getString("status")).thenReturn(PAYMENT_SUCCESS_STATE);

		PaymentStatus paymentStatus = govUKPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		String actual = paymentStatus.getValue();

		assertEquals(expected, actual);
	}

	@Test
	public void getPaymentStatus_WhenResponseStatusCodeIsNotEqualsToPaymentSuccess_ThenReturnPaymentErrorStatus()
			throws ServiceException, ConfigurationException, ClientProtocolException, IOException, JSONException {
		String expected = PaymentStatus.paymentError().getValue();
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getPaymentStatusURL()).thenReturn(PAYMENT_STATUS_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(GOVUK_PAYMENT_ID);
		when(mockGovUKPaymentOrderFactory.getHttpGetMethod(BASE_URL + PAYMENT_STATUS_URL, API_AUTH_KEY)).thenReturn(mockHttpGet);
		when(mockGovUKPaymentOrderFactory.getHttpClient()).thenReturn(mockClient);
		when(mockGovUKPaymentOrderFactory.executeRequest(mockHttpGet, mockClient)).thenReturn(mockResponse);
		when(mockGovUKPaymentOrderFactory.getResponseStatusCode(mockResponse)).thenReturn(SUCCESS_STATUS_CODE);
		when(mockGovUKPaymentOrderFactory.getResponseStatusJSON(mockResponse)).thenReturn(mockJsonObject);
		when(mockJsonObject.getString("status")).thenReturn("else");

		PaymentStatus paymentStatus = govUKPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		String actual = paymentStatus.getValue();

		assertEquals(expected, actual);
	}

	@Test
	public void getPaymentStatus_WhenResponseStatusCodeIsNotEqualsToPaymentSuccess_ThenReturnStatusPaymentError()
			throws ServiceException, ConfigurationException, ClientProtocolException, IOException {
		String expected = PaymentStatus.paymentError().getValue();
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getPaymentStatusURL()).thenReturn(PAYMENT_STATUS_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.getPaymentIdFromSession(mockHttpServletRequest)).thenReturn(GOVUK_PAYMENT_ID);
		when(mockGovUKPaymentOrderFactory.getHttpGetMethod(BASE_URL + PAYMENT_STATUS_URL, API_AUTH_KEY)).thenReturn(mockHttpGet);
		when(mockGovUKPaymentOrderFactory.getHttpClient()).thenReturn(mockClient);
		when(mockGovUKPaymentOrderFactory.executeRequest(mockHttpGet, mockClient)).thenReturn(mockResponse);
		when(mockGovUKPaymentOrderFactory.getResponseStatusCode(mockResponse)).thenReturn(FAILED_STATUS_CODE);

		PaymentStatus paymentStatus = govUKPaymentConnector.getPaymentStatus(PAYMENT_REFERENCE, mockServiceContext);

		String actual = paymentStatus.getValue();

		assertEquals(expected, actual);
	}

	@Test
	public void preparePayment_WhenClientProtocolException_ThenReturnPaymentResponseWithStatusAppError() throws ServiceException, PortalException, ClientProtocolException, IOException {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getCreatePaymentURL()).thenReturn(CREATE_PAYMENT_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.createGovUKPaymentRequest(mockPaymentContext)).thenReturn(mockJsonObject);
		when(mockGovUKPaymentOrderFactory.getHttpPostMethod(BASE_URL + CREATE_PAYMENT_URL, API_AUTH_KEY, mockJsonObject)).thenReturn(mockHttpPost);
		when(mockGovUKPaymentOrderFactory.getHttpClient()).thenReturn(mockClient);
		when(mockGovUKPaymentOrderFactory.executeRequest(mockHttpPost, mockClient)).thenThrow(new ClientProtocolException());

		PaymentResponse paymentResponse = govUKPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = paymentResponse.getRedirectURL();
		PaymentStatus actualStatus = paymentResponse.getStatus();

		assertEquals(StringPool.BLANK, actualRedirectURL);
		assertEquals(PaymentStatus.appError(), actualStatus);
	}

	@Test
	public void preparePayment_WhenConfigurationException_ThenReturnStatusAppErrorPaymentResponse() throws ServiceException, PortalException {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenThrow(new ConfigurationException());

		PaymentResponse paymentResponse = govUKPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = paymentResponse.getRedirectURL();
		PaymentStatus actualStatus = paymentResponse.getStatus();

		assertEquals(StringPool.BLANK, actualRedirectURL);
		assertEquals(PaymentStatus.appError(), actualStatus);
	}

	@Test
	public void preparePayment_WhenPaymentCreatedSuccessfully_ThenReturnStatusSuccessPaymentResponse() throws ServiceException, PortalException, ClientProtocolException, IOException {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getCreatePaymentURL()).thenReturn(CREATE_PAYMENT_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.createGovUKPaymentRequest(mockPaymentContext)).thenReturn(mockJsonObject);
		when(mockGovUKPaymentOrderFactory.getHttpPostMethod(BASE_URL + CREATE_PAYMENT_URL, API_AUTH_KEY, mockJsonObject)).thenReturn(mockHttpPost);
		when(mockGovUKPaymentOrderFactory.getHttpClient()).thenReturn(mockClient);
		when(mockGovUKPaymentOrderFactory.executeRequest(mockHttpPost, mockClient)).thenReturn(mockResponse);
		when(mockGovUKPaymentOrderFactory.getResponseStatusCode(mockResponse)).thenReturn(PAYMENT_CREATED_SUCCESS_STATUS_CODE);
		when(mockGovUKPaymentOrderFactory.getPaymentURLJSON(mockResponse)).thenReturn(mockJsonObject);
		when(mockJsonObject.getString("href")).thenReturn(SUCCESS_CREATED_PAYMENT_URL);

		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockhttpSession);
		when(mockJsonObject.getString("payment_id")).thenReturn(GOVUK_PAYMENT_ID);

		PaymentResponse paymentResponse = govUKPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = paymentResponse.getRedirectURL();
		PaymentStatus actualStatus = paymentResponse.getStatus();

		assertEquals(SUCCESS_CREATED_PAYMENT_URL, actualRedirectURL);
		assertEquals(PaymentStatus.success(), actualStatus);
	}

	@Test
	public void preparePayment_WhenPaymentCreationFail_ThenReturnPaymentResponseWithStatusAppError() throws ServiceException, PortalException, ClientProtocolException, IOException {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getCreatePaymentURL()).thenReturn(CREATE_PAYMENT_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.createGovUKPaymentRequest(mockPaymentContext)).thenReturn(mockJsonObject);
		when(mockGovUKPaymentOrderFactory.getHttpPostMethod(BASE_URL + CREATE_PAYMENT_URL, API_AUTH_KEY, mockJsonObject)).thenReturn(mockHttpPost);
		when(mockGovUKPaymentOrderFactory.getHttpClient()).thenReturn(mockClient);
		when(mockGovUKPaymentOrderFactory.executeRequest(mockHttpPost, mockClient)).thenReturn(mockResponse);
		when(mockGovUKPaymentOrderFactory.getResponseStatusCode(mockResponse)).thenReturn(PAYMENT_CREATED_SUCCESS_STATUS_CODE);
		when(mockGovUKPaymentOrderFactory.getPaymentURLJSON(mockResponse)).thenReturn(mockJsonObject);
		when(mockJsonObject.getString("href")).thenReturn(FAIL_CREATED_PAYMENT_URL);

		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockhttpSession);
		when(mockJsonObject.getString("payment_id")).thenReturn(GOVUK_PAYMENT_ID);

		PaymentResponse paymentResponse = govUKPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = paymentResponse.getRedirectURL();
		PaymentStatus actualStatus = paymentResponse.getStatus();

		assertEquals(FAIL_CREATED_PAYMENT_URL, actualRedirectURL);
		assertEquals(PaymentStatus.appError(), actualStatus);
	}

	@Test
	public void preparePayment_WhenPaymentCreationFailAndReturnNotSuccessCode_ThenReturnPaymentResponseWithStatusAppError()
			throws ServiceException, PortalException, ClientProtocolException, IOException {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);
		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getCreatePaymentURL()).thenReturn(CREATE_PAYMENT_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.createGovUKPaymentRequest(mockPaymentContext)).thenReturn(mockJsonObject);
		when(mockGovUKPaymentOrderFactory.getHttpPostMethod(BASE_URL + CREATE_PAYMENT_URL, API_AUTH_KEY, mockJsonObject)).thenReturn(mockHttpPost);
		when(mockGovUKPaymentOrderFactory.getHttpClient()).thenReturn(mockClient);
		when(mockGovUKPaymentOrderFactory.executeRequest(mockHttpPost, mockClient)).thenReturn(mockResponse);
		when(mockGovUKPaymentOrderFactory.getResponseStatusCode(mockResponse)).thenReturn(PAYMENT_CREATED_FAIL_STATUS_CODE);

		when(mockPortal.getOriginalServletRequest(mockHttpServletRequest)).thenReturn(mockHttpServletRequest);
		when(mockHttpServletRequest.getSession()).thenReturn(mockhttpSession);
		when(mockJsonObject.getString("payment_id")).thenReturn(GOVUK_PAYMENT_ID);

		PaymentResponse paymentResponse = govUKPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = paymentResponse.getRedirectURL();
		PaymentStatus actualStatus = paymentResponse.getStatus();

		assertEquals(StringPool.BLANK, actualRedirectURL);
		assertEquals(PaymentStatus.appError(), actualStatus);
	}

	@Test
	public void preparePayment_WhenResponseIsNull_ThenReturnPaymentResponseWithStatusAppError() throws ServiceException, PortalException, ClientProtocolException, IOException {
		when(mockServiceContext.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockServiceContext.getRequest()).thenReturn(mockHttpServletRequest);

		when(mockGovUKPaymentService.getGovUKPaymentProperties(mockServiceContext)).thenReturn(mockGovUKPaymentProperties);
		when(mockGovUKPaymentProperties.getBaseURL()).thenReturn(BASE_URL);
		when(mockGovUKPaymentProperties.getCreatePaymentURL()).thenReturn(CREATE_PAYMENT_URL);
		when(mockGovUKPaymentProperties.getApiAuthKey()).thenReturn(API_AUTH_KEY);

		when(mockGovUKPaymentOrderFactory.createGovUKPaymentRequest(mockPaymentContext)).thenReturn(mockJsonObject);
		when(mockGovUKPaymentOrderFactory.getHttpPostMethod(BASE_URL + CREATE_PAYMENT_URL, API_AUTH_KEY, mockJsonObject)).thenReturn(mockHttpPost);
		when(mockGovUKPaymentOrderFactory.getHttpClient()).thenReturn(mockClient);
		when(mockGovUKPaymentOrderFactory.executeRequest(mockHttpPost, mockClient)).thenReturn(mockResponse);
		when(mockGovUKPaymentOrderFactory.getResponseStatusCode(mockResponse)).thenReturn(PAYMENT_CREATED_SUCCESS_STATUS_CODE);

		PaymentResponse paymentResponse = govUKPaymentConnector.preparePayment(mockPaymentContext, mockServiceContext);

		String actualRedirectURL = paymentResponse.getRedirectURL();
		PaymentStatus actualStatus = paymentResponse.getStatus();

		assertEquals(StringPool.BLANK, actualRedirectURL);
		assertEquals(PaymentStatus.appError(), actualStatus);
	}
}
