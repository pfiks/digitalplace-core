package com.placecube.digitalplace.pageimportadmin.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.document.library.kernel.antivirus.AntivirusScannerException;
import com.liferay.document.library.kernel.antivirus.AntivirusScannerUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.LayoutFriendlyURLsException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.pageimportadmin.model.PageImportModel;
import com.placecube.digitalplace.pageimportadmin.model.RowImportModel;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.service.LayoutInitializer;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ WorkbookFactory.class, AntivirusScannerUtil.class, LanguageUtil.class, ServiceContextFactory.class })
public class ContentImportServiceTest extends PowerMockito {

	private static final long GROUP_ID = 20;
	private static final Locale LOCALE = Locale.ENGLISH;
	private static final String MESSAGE = "message";

	@InjectMocks
	private ContentImportService contentImportService;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ContentValidationService mockContentValidationService;

	@Mock
	private File mockFile;

	@Mock
	private FileParserService mockFileParserService;

	@Mock
	private JournalArticleContext mockJournalArticleContext;

	@Mock
	private JournalArticleCreationService mockJournalArticleCreationService;

	@Mock
	private LayoutContext mockLayoutContext;

	@Mock
	private LayoutInitializer mockLayoutInitializer;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Mock
	private ModelFactoryBuilder mockModelFactoryBuilder;

	@Mock
	private PageImportModel mockPageImportModel;

	@Mock
	private Portal mockPortal;

	@Mock
	private Row mockRow;

	@Mock
	private RowImportModel mockRowImportModel;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private Sheet mockSheet;

	@Mock
	private UploadPortletRequest mockUploadPortletRequest;

	@Mock
	private WebContentImportModel mockWebContentImportModel;

	@Mock
	private Workbook mockWorkbook;

	@Test
	public void createPage_WhenExceptionRetrievingServiceContext_ThenAddsErrorMessage() throws PortalException {
		PortalException exception = new PortalException();
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenThrow(exception);
		when(LanguageUtil.format(LOCALE, "unable-to-import-page", exception.getMessage())).thenReturn(MESSAGE);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		verify(mockPageImportModel, times(1)).addErrorMessage(MESSAGE);
		verifyZeroInteractions(mockLayoutInitializer, mockContentValidationService, mockModelFactoryBuilder);
	}

	@Test
	public void createPage_WhenPageIsNotValid_ThenNoPageIsCreated() throws Exception {
		when(mockPageImportModel.isValid()).thenReturn(false);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		verifyZeroInteractions(mockLayoutInitializer);
		verify(mockContentValidationService, times(1)).validatePage(mockPageImportModel, mockServiceContext);
	}

	@Test
	public void createPage_WhenPageIsValidAndNoPageTemplateSpecifiedAndExceptionCreatingThePage_ThenAddsErrorMessage() throws PortalException {
		when(mockPageImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(mockPageImportModel.getCategoryIds()).thenReturn(Collections.emptyList());
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.empty());
		when(mockModelFactoryBuilder.getLayoutContext(mockPageImportModel)).thenReturn(mockLayoutContext);
		PortalException exception = new PortalException();
		when(mockLayoutInitializer.getOrCreateContentLayout(mockLayoutContext, mockServiceContext)).thenThrow(exception);
		when(LanguageUtil.format(LOCALE, "unable-to-import-page", exception.getMessage())).thenReturn(MESSAGE);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		verify(mockPageImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void createPage_WhenPageIsValidAndNoPageTemplateSpecifiedAndLayoutFriendlyURLsExceptionCreatingThePage_ThenAddsErrorMessage() throws PortalException {
		when(mockPageImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(mockPageImportModel.getCategoryIds()).thenReturn(Collections.emptyList());
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.empty());
		when(mockModelFactoryBuilder.getLayoutContext(mockPageImportModel)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreateContentLayout(mockLayoutContext, mockServiceContext)).thenThrow(new LayoutFriendlyURLsException());
		when(LanguageUtil.get(LOCALE, "invalid-url")).thenReturn(MESSAGE);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		verify(mockPageImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void createPage_WhenPageIsValidAndNoPageTemplateSpecifiedAndNoError_ThenCreatesThePageAsContentPage() throws PortalException {
		when(mockPageImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		List<Long> categoryIds = new ArrayList<>();
		categoryIds.add(123l);
		categoryIds.add(456l);
		when(mockPageImportModel.getCategoryIds()).thenReturn(categoryIds);
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.empty());
		when(mockModelFactoryBuilder.getLayoutContext(mockPageImportModel)).thenReturn(mockLayoutContext);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		InOrder inOrder = Mockito.inOrder(mockLayoutInitializer, mockContentValidationService, mockServiceContext);
		inOrder.verify(mockContentValidationService, times(1)).validatePage(mockPageImportModel, mockServiceContext);
		inOrder.verify(mockServiceContext, times(1)).setAssetCategoryIds(ArrayUtil.toArray(categoryIds.toArray(new Long[categoryIds.size()])));
		inOrder.verify(mockLayoutInitializer, times(1)).getOrCreateContentLayout(mockLayoutContext, mockServiceContext);
	}

	@Test
	public void createPage_WhenPageIsValidAndPageTemplateSpecifiedOfTypeContentAndExceptionCreatingThePage_ThenAddsErrorMessage() throws PortalException {
		when(mockPageImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(mockPageImportModel.getCategoryIds()).thenReturn(Collections.emptyList());
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.of(mockLayoutPageTemplateEntry));
		when(mockLayoutPageTemplateEntry.getType()).thenReturn(0);
		when(mockModelFactoryBuilder.getLayoutContext(mockPageImportModel)).thenReturn(mockLayoutContext);
		PortalException exception = new PortalException();
		when(mockLayoutInitializer.getOrCreateContentLayout(mockLayoutContext, mockServiceContext)).thenThrow(exception);
		when(LanguageUtil.format(LOCALE, "unable-to-import-page", exception.getMessage())).thenReturn(MESSAGE);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		verify(mockPageImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void createPage_WhenPageIsValidAndPageTemplateSpecifiedOfTypeContentAndLayoutFriendlyURLsExceptionCreatingThePage_ThenAddsErrorMessage() throws PortalException {
		when(mockPageImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(mockPageImportModel.getCategoryIds()).thenReturn(Collections.emptyList());
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.of(mockLayoutPageTemplateEntry));
		when(mockLayoutPageTemplateEntry.getType()).thenReturn(0);
		when(mockModelFactoryBuilder.getLayoutContext(mockPageImportModel)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreateContentLayout(mockLayoutContext, mockServiceContext)).thenThrow(new LayoutFriendlyURLsException());
		when(LanguageUtil.get(LOCALE, "invalid-url")).thenReturn(MESSAGE);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		verify(mockPageImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void createPage_WhenPageIsValidAndPageTemplateSpecifiedOfTypeContentAndNoError_ThenCreatesThePageAsContentPage() throws PortalException {
		when(mockPageImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		List<Long> categoryIds = new ArrayList<>();
		categoryIds.add(123l);
		categoryIds.add(456l);
		when(mockPageImportModel.getCategoryIds()).thenReturn(categoryIds);
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.of(mockLayoutPageTemplateEntry));
		when(mockLayoutPageTemplateEntry.getType()).thenReturn(0);
		when(mockModelFactoryBuilder.getLayoutContext(mockPageImportModel)).thenReturn(mockLayoutContext);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		InOrder inOrder = Mockito.inOrder(mockLayoutInitializer, mockModelFactoryBuilder, mockContentValidationService, mockServiceContext);
		inOrder.verify(mockContentValidationService, times(1)).validatePage(mockPageImportModel, mockServiceContext);
		inOrder.verify(mockModelFactoryBuilder, times(1)).configurePageTemplate(mockLayoutContext, mockLayoutPageTemplateEntry);
		inOrder.verify(mockServiceContext, times(1)).setAssetCategoryIds(ArrayUtil.toArray(categoryIds.toArray(new Long[categoryIds.size()])));
		inOrder.verify(mockLayoutInitializer, times(1)).getOrCreateContentLayout(mockLayoutContext, mockServiceContext);
	}

	@Test
	public void createPage_WhenPageIsValidAndPageTemplateSpecifiedOfTypeWidgetAndExceptionCreatingThePage_ThenAddsErrorMessage() throws PortalException {
		when(mockPageImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(mockPageImportModel.getCategoryIds()).thenReturn(Collections.emptyList());
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.of(mockLayoutPageTemplateEntry));
		when(mockLayoutPageTemplateEntry.getType()).thenReturn(10);
		when(mockModelFactoryBuilder.getLayoutContext(mockPageImportModel)).thenReturn(mockLayoutContext);
		PortalException exception = new PortalException();
		when(mockLayoutInitializer.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext)).thenThrow(exception);
		when(LanguageUtil.format(LOCALE, "unable-to-import-page", exception.getMessage())).thenReturn(MESSAGE);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		verify(mockPageImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void createPage_WhenPageIsValidAndPageTemplateSpecifiedOfTypeWidgetAndLayoutFriendlyURLsExceptionCreatingThePage_ThenAddsErrorMessage() throws PortalException {
		when(mockPageImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(mockPageImportModel.getCategoryIds()).thenReturn(Collections.emptyList());
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.of(mockLayoutPageTemplateEntry));
		when(mockLayoutPageTemplateEntry.getType()).thenReturn(10);
		when(mockModelFactoryBuilder.getLayoutContext(mockPageImportModel)).thenReturn(mockLayoutContext);
		when(mockLayoutInitializer.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext)).thenThrow(new LayoutFriendlyURLsException());
		when(LanguageUtil.get(LOCALE, "invalid-url")).thenReturn(MESSAGE);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		verify(mockPageImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void createPage_WhenPageIsValidAndPageTemplateSpecifiedOfTypeWidgetAndNoError_ThenCreatesThePageAsWidgetPage() throws PortalException {
		when(mockPageImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(Layout.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		List<Long> categoryIds = new ArrayList<>();
		categoryIds.add(123l);
		categoryIds.add(456l);
		when(mockPageImportModel.getCategoryIds()).thenReturn(categoryIds);
		when(mockPageImportModel.getPageTemplate()).thenReturn(Optional.of(mockLayoutPageTemplateEntry));
		when(mockLayoutPageTemplateEntry.getType()).thenReturn(10);
		when(mockModelFactoryBuilder.getLayoutContext(mockPageImportModel)).thenReturn(mockLayoutContext);

		contentImportService.createPage(mockPageImportModel, LOCALE, mockActionRequest);

		InOrder inOrder = Mockito.inOrder(mockLayoutInitializer, mockModelFactoryBuilder, mockContentValidationService, mockServiceContext);
		inOrder.verify(mockContentValidationService, times(1)).validatePage(mockPageImportModel, mockServiceContext);
		inOrder.verify(mockModelFactoryBuilder, times(1)).configurePageTemplate(mockLayoutContext, mockLayoutPageTemplateEntry);
		inOrder.verify(mockServiceContext, times(1)).setAssetCategoryIds(ArrayUtil.toArray(categoryIds.toArray(new Long[categoryIds.size()])));
		inOrder.verify(mockLayoutInitializer, times(1)).getOrCreatePortletLayout(mockLayoutContext, mockServiceContext);
	}

	@Test
	public void createWebContent_WhenContentIsNotValid_ThenNoArticleIsCreated() throws PortalException {
		when(mockWebContentImportModel.isValid()).thenReturn(false);
		when(ServiceContextFactory.getInstance(JournalArticle.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);

		contentImportService.createWebContent(mockWebContentImportModel, LOCALE, mockActionRequest);

		verifyZeroInteractions(mockJournalArticleCreationService);
		verify(mockContentValidationService, times(1)).validateWebContent(mockWebContentImportModel, mockServiceContext);
	}

	@Test
	public void createWebContent_WhenContentIsValidAndExceptionCreatingTheArticle_ThenAddsErrorMessage() throws PortalException {
		when(mockWebContentImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(JournalArticle.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		when(mockWebContentImportModel.getCategoryIds()).thenReturn(Collections.emptyList());
		when(mockModelFactoryBuilder.getJournalArticleContext(mockWebContentImportModel)).thenReturn(mockJournalArticleContext);
		PortalException exception = new PortalException();
		when(mockJournalArticleCreationService.getOrCreateArticle(mockJournalArticleContext, mockServiceContext)).thenThrow(exception);
		when(LanguageUtil.format(LOCALE, "unable-to-import-web-content", exception.getMessage())).thenReturn(MESSAGE);

		contentImportService.createWebContent(mockWebContentImportModel, LOCALE, mockActionRequest);

		verify(mockWebContentImportModel, times(1)).addErrorMessage(MESSAGE);
	}

	@Test
	public void createWebContent_WhenContentIsValidAndNoError_ThenCreatesTheArticle() throws PortalException {
		when(mockWebContentImportModel.isValid()).thenReturn(true);
		when(ServiceContextFactory.getInstance(JournalArticle.class.getName(), mockActionRequest)).thenReturn(mockServiceContext);
		List<Long> categoryIds = new ArrayList<>();
		categoryIds.add(123l);
		categoryIds.add(456l);
		when(mockWebContentImportModel.getCategoryIds()).thenReturn(categoryIds);
		when(mockModelFactoryBuilder.getJournalArticleContext(mockWebContentImportModel)).thenReturn(mockJournalArticleContext);

		contentImportService.createWebContent(mockWebContentImportModel, LOCALE, mockActionRequest);

		InOrder inOrder = Mockito.inOrder(mockJournalArticleCreationService, mockContentValidationService, mockServiceContext);
		inOrder.verify(mockContentValidationService, times(1)).validateWebContent(mockWebContentImportModel, mockServiceContext);
		inOrder.verify(mockServiceContext, times(1)).setAssetCategoryIds(ArrayUtil.toArray(categoryIds.toArray(new Long[categoryIds.size()])));
		inOrder.verify(mockJournalArticleCreationService, times(1)).getOrCreateArticle(mockJournalArticleContext, mockServiceContext);
	}

	@Test
	public void createWebContent_WhenExceptionRetrievingServiceContext_ThenAddsErrorMessage() throws PortalException {
		PortalException exception = new PortalException();
		when(ServiceContextFactory.getInstance(JournalArticle.class.getName(), mockActionRequest)).thenThrow(exception);
		when(LanguageUtil.format(LOCALE, "unable-to-import-web-content", exception.getMessage())).thenReturn(MESSAGE);

		contentImportService.createWebContent(mockWebContentImportModel, LOCALE, mockActionRequest);

		verify(mockWebContentImportModel, times(1)).addErrorMessage(MESSAGE);
		verifyZeroInteractions(mockContentValidationService, mockJournalArticleCreationService, mockModelFactoryBuilder);
	}

	@Test(expected = AntivirusScannerException.class)
	public void getFile_WhenAntivirusScannerException_ThenThrowsAntivirusScannerException() throws AntivirusScannerException {
		when(mockPortal.getUploadPortletRequest(mockActionRequest)).thenReturn(mockUploadPortletRequest);
		when(mockUploadPortletRequest.getFile("file")).thenReturn(mockFile);
		doThrow(new AntivirusScannerException(new Exception())).when(AntivirusScannerUtil.class);
		AntivirusScannerUtil.scan(mockFile);

		contentImportService.getFile(mockActionRequest);
	}

	@Test
	public void getFile_WhenNoError_ThenReturnsTheFile() throws AntivirusScannerException {
		when(mockPortal.getUploadPortletRequest(mockActionRequest)).thenReturn(mockUploadPortletRequest);
		when(mockUploadPortletRequest.getFile("file")).thenReturn(mockFile);

		File result = contentImportService.getFile(mockActionRequest);

		assertThat(result, sameInstance(mockFile));
	}

	@Test
	public void getFile_WhenNoError_ThenScansTheFile() throws AntivirusScannerException {
		when(mockPortal.getUploadPortletRequest(mockActionRequest)).thenReturn(mockUploadPortletRequest);
		when(mockUploadPortletRequest.getFile("file")).thenReturn(mockFile);

		contentImportService.getFile(mockActionRequest);

		verifyStatic(AntivirusScannerUtil.class);
		AntivirusScannerUtil.scan(mockFile);
	}

	@Test(expected = PortletException.class)
	public void getFirstSheet_WhenEncryptedDocumentException_ThenThrowsPortletException() throws Exception {
		when(WorkbookFactory.create(mockFile)).thenThrow(new EncryptedDocumentException(""));

		contentImportService.getFirstSheet(mockFile);
	}

	@Test
	public void getFirstSheet_WhenExceptionClosingTheWorkbook_ThenNoErrorIsThrown() throws Exception {
		when(WorkbookFactory.create(mockFile)).thenReturn(mockWorkbook);
		when(mockWorkbook.getSheetAt(0)).thenReturn(mockSheet);
		when(mockSheet.getLastRowNum()).thenReturn(1);
		when(mockSheet.getRow(mockSheet.getLastRowNum())).thenReturn(mockRow);
		doThrow(new IOException()).when(mockWorkbook).close();

		try {
			contentImportService.getFirstSheet(mockFile);
		} catch (Exception e) {
			fail();
		}
	}

	@Test(expected = PortletException.class)
	public void getFirstSheet_WhenInvalidFormatException_ThenThrowsPortletException() throws Exception {
		when(WorkbookFactory.create(mockFile)).thenThrow(new InvalidFormatException(""));

		contentImportService.getFirstSheet(mockFile);
	}

	@Test(expected = PortletException.class)
	public void getFirstSheet_WhenIOException_ThenThrowsPortletException() throws Exception {
		when(WorkbookFactory.create(mockFile)).thenThrow(new IOException(""));

		contentImportService.getFirstSheet(mockFile);
	}

	@Test
	public void getFirstSheet_WhenNoError_ThenReturnsTheFirstSheetOfTheWorkbookAndClosesTheWorkbook() throws Exception {
		when(WorkbookFactory.create(mockFile)).thenReturn(mockWorkbook);
		when(mockWorkbook.getSheetAt(0)).thenReturn(mockSheet);
		when(mockSheet.getLastRowNum()).thenReturn(1);
		when(mockSheet.getRow(mockSheet.getLastRowNum())).thenReturn(mockRow);

		Optional<Sheet> result = contentImportService.getFirstSheet(mockFile);

		assertThat(result.get(), sameInstance(mockSheet));
		verify(mockWorkbook, times(1)).close();
	}

	@Test
	public void getFirstSheet_WhenSheetHasNoRows_ThenReturnsEmptyOptional() throws Exception {
		when(WorkbookFactory.create(mockFile)).thenReturn(mockWorkbook);
		when(mockWorkbook.getSheetAt(0)).thenReturn(mockSheet);
		when(mockSheet.getLastRowNum()).thenReturn(0);
		when(mockSheet.getRow(mockSheet.getLastRowNum())).thenReturn(null);

		Optional<Sheet> result = contentImportService.getFirstSheet(mockFile);

		assertFalse(result.isPresent());
		verify(mockWorkbook, times(1)).close();
	}

	@Test
	public void getFirstSheet_WhenSheetHasOneRowsAndRowsisEmpty_ThenReturnsEmptyOptional() throws Exception {
		when(WorkbookFactory.create(mockFile)).thenReturn(mockWorkbook);
		when(mockWorkbook.getSheetAt(0)).thenReturn(mockSheet);
		when(mockSheet.getLastRowNum()).thenReturn(0);
		when(mockSheet.getRow(mockSheet.getLastRowNum())).thenReturn(null);

		Optional<Sheet> result = contentImportService.getFirstSheet(mockFile);

		assertFalse(result.isPresent());
		verify(mockWorkbook, times(1)).close();
	}

	@Test
	public void getFirstSheet_WhenSheetHasRowsAndRowsAreEmpty_ThenReturnsSheetWithEmptyRows() throws Exception {
		when(WorkbookFactory.create(mockFile)).thenReturn(mockWorkbook);
		when(mockWorkbook.getSheetAt(0)).thenReturn(mockSheet);
		when(mockSheet.getLastRowNum()).thenReturn(1);
		when(mockSheet.getRow(mockSheet.getLastRowNum())).thenReturn(null);

		Optional<Sheet> result = contentImportService.getFirstSheet(mockFile);

		assertThat(result.get(), sameInstance(mockSheet));
		verify(mockWorkbook, times(1)).close();
	}

	@Test
	@Parameters({ "true", "false" })
	public void getRowContent_WhenGetRowContentReturnsEmptyImportModelWithNoError_ThenDoesntConfiguresAndValidatesCategoriesInTheRowContent(boolean privateLayout) {
		int index = 123;
		long assetVocabularyId = 456;
		when(mockFileParserService.getRowContent(mockSheet, index, GROUP_ID, privateLayout)).thenReturn(mockRowImportModel);
		when(mockPageImportModel.isPageImport()).thenReturn(false);
		when(mockWebContentImportModel.isWebContent()).thenReturn(true);

		contentImportService.getRowContent(mockSheet, index, GROUP_ID, LOCALE, privateLayout, assetVocabularyId);

		verify(mockModelFactoryBuilder, never()).configureAndValidateCategories(mockRowImportModel, assetVocabularyId, LOCALE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getRowContent_WhenGetRowContentReturnsPageImportModelWithNoError_ThenConfiguresAndValidatesCategoriesInTheRowContent(boolean privateLayout) {
		int index = 123;
		long assetVocabularyId = 456;
		when(mockFileParserService.getRowContent(mockSheet, index, GROUP_ID, privateLayout)).thenReturn(mockPageImportModel);
		when(mockPageImportModel.isPageImport()).thenReturn(true);
		when(mockWebContentImportModel.isWebContent()).thenReturn(false);

		contentImportService.getRowContent(mockSheet, index, GROUP_ID, LOCALE, privateLayout, assetVocabularyId);

		verify(mockModelFactoryBuilder, times(1)).configureAndValidateCategories(mockPageImportModel, assetVocabularyId, LOCALE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getRowContent_WhenGetRowContentReturnsWebContentImportModelWithNoError_ThenConfiguresAndValidatesCategoriesInTheRowContent(boolean privateLayout) {
		int index = 123;
		long assetVocabularyId = 456;
		when(mockFileParserService.getRowContent(mockSheet, index, GROUP_ID, privateLayout)).thenReturn(mockWebContentImportModel);
		when(mockPageImportModel.isPageImport()).thenReturn(false);
		when(mockWebContentImportModel.isWebContent()).thenReturn(true);

		contentImportService.getRowContent(mockSheet, index, GROUP_ID, LOCALE, privateLayout, assetVocabularyId);

		verify(mockModelFactoryBuilder, times(1)).configureAndValidateCategories(mockWebContentImportModel, assetVocabularyId, LOCALE);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getRowContent_WhenNoError_ThenReturnsTheRowContent(boolean privateLayout) {
		int index = 123;
		long assetVocabularyId = 456;
		when(mockFileParserService.getRowContent(mockSheet, index, GROUP_ID, privateLayout)).thenReturn(mockRowImportModel);

		RowImportModel result = contentImportService.getRowContent(mockSheet, index, GROUP_ID, LOCALE, privateLayout, assetVocabularyId);

		assertThat(result, sameInstance(mockRowImportModel));
	}

	@Before
	public void setUp() {
		mockStatic(WorkbookFactory.class, AntivirusScannerUtil.class, LanguageUtil.class, ServiceContextFactory.class);
	}
}
