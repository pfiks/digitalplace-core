package com.placecube.digitalplace.user.serviceoverride.internal.util;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.placecube.digitalplace.user.serviceoverride.model.AuthenticationActionContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.user.serviceoverride.service.UserServiceOverrideAction;

public class UserServiceOverrideActionUtilTest extends PowerMockito {

	@Mock
	private AuthenticationActionContext mockAuthenticationActionContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private User mockUser1;

	@Mock
	private User mockUser2;

	@Mock
	private User mockUser3;

	@Mock
	private UserServiceOverrideAction mockUserServiceOverrideAction1;

	@Mock
	private UserServiceOverrideAction mockUserServiceOverrideAction2;

	@Mock
	private UserServiceOverrideActionRegistry mockUserServiceOverrideActionRegistry;

	@InjectMocks
	private UserServiceOverrideActionUtil userServiceOverrideActionUtil;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void executeActionsAfterUserAuthentication_WhenThereAreUserServiceOverrideActions_ThenCallsExecuteAfterUserAuthenticationForAllUserServiceOverrideActions() throws PortalException {

		List<UserServiceOverrideAction> actions = new LinkedList<>();
		actions.add(mockUserServiceOverrideAction1);
		actions.add(mockUserServiceOverrideAction2);
		when(mockUserServiceOverrideActionRegistry.getUserServiceOverrideActions()).thenReturn(actions);

		userServiceOverrideActionUtil.executeActionsAfterUserAuthentication(mockAuthenticationActionContext);

		verify(mockUserServiceOverrideAction1, times(1)).executeAfterUserAuthentication(mockAuthenticationActionContext);
		verify(mockUserServiceOverrideAction2, times(1)).executeAfterUserAuthentication(mockAuthenticationActionContext);

	}

	@Test
	public void executeActionsAfterVerifyEmailAddress_WhenUserIdIs0_ThenNotRetrieveUserServiceOverrideActionRegistry() {
		long userId = 0;

		userServiceOverrideActionUtil.executeActionsAfterVerifyEmailAddress(userId);

		verifyZeroInteractions(mockUserServiceOverrideActionRegistry);
	}

	@Test
	public void executeActionsAfterVerifyEmailAddress_WhenUserIdIsGreaterThen0_ThenExecuteOnAfterVerifyEmailAddress() {
		long userId = 1;

		List<UserServiceOverrideAction> actions = new LinkedList<>();
		actions.add(mockUserServiceOverrideAction1);
		actions.add(mockUserServiceOverrideAction2);
		when(mockUserServiceOverrideActionRegistry.getUserServiceOverrideActions()).thenReturn(actions);

		userServiceOverrideActionUtil.executeActionsAfterVerifyEmailAddress(userId);

		verify(mockUserServiceOverrideAction1, times(1)).executeOnAfterVerifyEmailAddress(userId);
		verify(mockUserServiceOverrideAction2, times(1)).executeOnAfterVerifyEmailAddress(userId);
	}

	@Test
	public void executeActionsForUserCreation_WhenNoError_ThenReturnsTheUpdatedUser() throws PortalException {
		List<UserServiceOverrideAction> actions = new LinkedList<>();
		actions.add(mockUserServiceOverrideAction1);
		actions.add(mockUserServiceOverrideAction2);
		when(mockUserServiceOverrideActionRegistry.getUserServiceOverrideActions()).thenReturn(actions);
		when(mockUserServiceOverrideAction1.executeOnAfterCreateActions(mockUser1, Optional.of(mockServiceContext))).thenReturn(mockUser2);
		when(mockUserServiceOverrideAction2.executeOnAfterCreateActions(mockUser2, Optional.of(mockServiceContext))).thenReturn(mockUser3);

		User result = userServiceOverrideActionUtil.executeActionsForUserCreation(mockUser1, Optional.of(mockServiceContext));

		assertThat(result, sameInstance(mockUser3));
	}

	@Test
	public void executeActionsForUserAddedToGroup_WhenNoError_ThenExecutesTheUserAddedToGroupActions() {
		long userId = 1;
		long groupId = 2;

		List<UserServiceOverrideAction> actions = new LinkedList<>();
		actions.add(mockUserServiceOverrideAction1);
		actions.add(mockUserServiceOverrideAction2);
		when(mockUserServiceOverrideActionRegistry.getUserServiceOverrideActions()).thenReturn(actions);

		userServiceOverrideActionUtil.executeActionsForUserAddedToGroup(userId, groupId);

		verify(mockUserServiceOverrideAction1, times(1)).executeOnAfterAddUserToGroupActions(userId, groupId);
		verify(mockUserServiceOverrideAction2, times(1)).executeOnAfterAddUserToGroupActions(userId, groupId);
	}

	@Test(expected = SystemException.class)
	public void executeActionsForUserCreation_WhenExceptionExecutingOneOfTheAfterCreateActions_ThenThrowsSystemException() throws PortalException {
		List<UserServiceOverrideAction> actions = new LinkedList<>();
		actions.add(mockUserServiceOverrideAction1);
		actions.add(mockUserServiceOverrideAction2);
		when(mockUserServiceOverrideActionRegistry.getUserServiceOverrideActions()).thenReturn(actions);
		when(mockUserServiceOverrideAction1.executeOnAfterCreateActions(mockUser1, Optional.of(mockServiceContext))).thenReturn(mockUser2);
		when(mockUserServiceOverrideAction2.executeOnAfterCreateActions(mockUser2, Optional.of(mockServiceContext))).thenThrow(new PortalException());

		User result = userServiceOverrideActionUtil.executeActionsForUserCreation(mockUser1, Optional.of(mockServiceContext));

		assertThat(result, sameInstance(mockUser3));
	}

	@Test
	public void executeActionsForUpdateUser_WhenNoError_ThenReturnsTheUpdatedUser() throws PortalException {
		List<UserServiceOverrideAction> actions = new LinkedList<>();
		actions.add(mockUserServiceOverrideAction1);
		actions.add(mockUserServiceOverrideAction2);
		when(mockUserServiceOverrideActionRegistry.getUserServiceOverrideActions()).thenReturn(actions);
		when(mockUserServiceOverrideAction1.executeOnAfterUpdateActions(mockUser1, Optional.of(mockServiceContext))).thenReturn(mockUser2);
		when(mockUserServiceOverrideAction2.executeOnAfterUpdateActions(mockUser2, Optional.of(mockServiceContext))).thenReturn(mockUser3);

		User result = userServiceOverrideActionUtil.executeActionsForUpdateUser(mockUser1, Optional.of(mockServiceContext));

		assertThat(result, sameInstance(mockUser3));
	}

	@Test(expected = PortalException.class)
	public void executeActionsForUpdateUser_WhenExceptionExecutingOneOfTheAfterCreateActions_ThenThrowsSystemException() throws PortalException {
		List<UserServiceOverrideAction> actions = new LinkedList<>();
		actions.add(mockUserServiceOverrideAction1);
		actions.add(mockUserServiceOverrideAction2);
		when(mockUserServiceOverrideActionRegistry.getUserServiceOverrideActions()).thenReturn(actions);
		when(mockUserServiceOverrideAction1.executeOnAfterUpdateActions(mockUser1, Optional.of(mockServiceContext))).thenReturn(mockUser2);
		when(mockUserServiceOverrideAction2.executeOnAfterUpdateActions(mockUser2, Optional.of(mockServiceContext))).thenThrow(new PortalException());

		User result = userServiceOverrideActionUtil.executeActionsForUpdateUser(mockUser1, Optional.of(mockServiceContext));

		assertThat(result, sameInstance(mockUser3));
	}

	@Test
	public void executeActionsForUserDeletion_WhenNoError_ThenExecutesTheDeleteActions() {
		List<UserServiceOverrideAction> actions = new LinkedList<>();
		actions.add(mockUserServiceOverrideAction1);
		actions.add(mockUserServiceOverrideAction2);
		when(mockUserServiceOverrideActionRegistry.getUserServiceOverrideActions()).thenReturn(actions);

		userServiceOverrideActionUtil.executeActionsForUserDeletion(mockUser1);

		verify(mockUserServiceOverrideAction1, times(1)).executeOnAfterDeleteActions(mockUser1);
		verify(mockUserServiceOverrideAction2, times(1)).executeOnAfterDeleteActions(mockUser1);
	}

	@Test
	public void executeActionsForUserRemovedFromGroup_WhenNoError_ThenExecutesTheUserRemovedFromGroupActions() {
		long userId = 1;
		long groupId = 2;

		List<UserServiceOverrideAction> actions = new LinkedList<>();
		actions.add(mockUserServiceOverrideAction1);
		actions.add(mockUserServiceOverrideAction2);
		when(mockUserServiceOverrideActionRegistry.getUserServiceOverrideActions()).thenReturn(actions);

		userServiceOverrideActionUtil.executeActionsForUserRemovedFromGroup(userId, groupId, Optional.of(mockServiceContext));

		verify(mockUserServiceOverrideAction1, times(1)).executeOnAfterRemoveUserFromGroupActions(userId, groupId, Optional.of(mockServiceContext));
		verify(mockUserServiceOverrideAction2, times(1)).executeOnAfterRemoveUserFromGroupActions(userId, groupId, Optional.of(mockServiceContext));
	}

	@Test
	public void getGroupIds_WhenUserIsNotNull_ThenReturnsListWithTheUserGroupIds() {
		long groupId1 = 12;
		long groupId2 = 34;
		long groupId3 = 56;
		when(mockUser1.getGroupIds()).thenReturn(new long[] { groupId1, groupId2, groupId3 });

		List<Long> results = userServiceOverrideActionUtil.getGroupIds(mockUser1);

		assertThat(results.size(), equalTo(3));
		assertThat(results, containsInAnyOrder(groupId1, groupId2, groupId3));
	}

	@Test
	public void getGroupIds_WhenUserIsNull_ThenReturnsEmptyList() {
		List<Long> results = userServiceOverrideActionUtil.getGroupIds(null);

		assertTrue(results.isEmpty());
	}

	@Test
	public void getGroupIdsAdded_WhenNoError_ThenReturnsACopyOfTheGroupIdsAdded() {
		long groupId1 = 12;
		long groupId2 = 34;
		long groupId3 = 56;
		long groupId4 = 78;

		List<Long> originalGroupIds = new ArrayList<>();
		originalGroupIds.add(groupId1);
		originalGroupIds.add(groupId4);
		originalGroupIds.add(9090L);

		List<Long> updatedGroupIds = new ArrayList<>();
		updatedGroupIds.add(groupId1);
		updatedGroupIds.add(groupId2);
		updatedGroupIds.add(groupId3);

		List<Long> results = userServiceOverrideActionUtil.getGroupIdsAdded(originalGroupIds, updatedGroupIds);

		assertThat(results, not(sameInstance(originalGroupIds)));
		assertThat(results, not(sameInstance(updatedGroupIds)));
		assertThat(results, containsInAnyOrder(groupId2, groupId3));
	}

	@Test
	public void getGroupIdsRemoved_WhenNoError_ThenReturnsACopyOfTheGroupIdsRemoved() {
		long groupId1 = 12;
		long groupId2 = 34;
		long groupId3 = 56;
		long groupId4 = 78;

		List<Long> originalGroupIds = new ArrayList<>();
		originalGroupIds.add(groupId1);
		originalGroupIds.add(groupId2);
		originalGroupIds.add(groupId4);

		List<Long> updatedGroupIds = new ArrayList<>();
		updatedGroupIds.add(groupId1);
		updatedGroupIds.add(groupId2);
		updatedGroupIds.add(groupId3);

		List<Long> results = userServiceOverrideActionUtil.getGroupIdsRemoved(originalGroupIds, updatedGroupIds);

		assertThat(results, not(sameInstance(originalGroupIds)));
		assertThat(results, not(sameInstance(updatedGroupIds)));
		assertThat(results, containsInAnyOrder(groupId4));
	}
}
