/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.dataconsumer.model.impl;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringUtil;

import com.placecube.digitalplace.dataconsumer.model.DataConsumer;
import com.placecube.digitalplace.dataconsumer.model.DataConsumerModel;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.sql.Blob;
import java.sql.Types;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * The base model implementation for the DataConsumer service. Represents a row in the &quot;Placecube_DataConsumer_DataConsumer&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface <code>DataConsumerModel</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link DataConsumerImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DataConsumerImpl
 * @generated
 */
public class DataConsumerModelImpl
	extends BaseModelImpl<DataConsumer> implements DataConsumerModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a data consumer model instance should use the <code>DataConsumer</code> interface instead.
	 */
	public static final String TABLE_NAME =
		"Placecube_DataConsumer_DataConsumer";

	public static final Object[][] TABLE_COLUMNS = {
		{"uuid_", Types.VARCHAR}, {"dataConsumerId", Types.BIGINT},
		{"groupId", Types.BIGINT}, {"companyId", Types.BIGINT},
		{"userId", Types.BIGINT}, {"createDate", Types.TIMESTAMP},
		{"modifiedDate", Types.TIMESTAMP}, {"ddmFormInstanceId", Types.BIGINT},
		{"ddmDataProviderInstanceId", Types.BIGINT},
		{"sourceDataProviderId", Types.BIGINT}, {"method", Types.VARCHAR},
		{"mapping", Types.VARCHAR}, {"headers", Types.VARCHAR},
		{"chain", Types.INTEGER}, {"body", Types.VARCHAR}
	};

	public static final Map<String, Integer> TABLE_COLUMNS_MAP =
		new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("uuid_", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("dataConsumerId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("groupId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("companyId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("createDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("modifiedDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("ddmFormInstanceId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("ddmDataProviderInstanceId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("sourceDataProviderId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("method", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("mapping", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("headers", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("chain", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("body", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE =
		"create table Placecube_DataConsumer_DataConsumer (uuid_ VARCHAR(75) null,dataConsumerId LONG not null primary key,groupId LONG,companyId LONG,userId LONG,createDate DATE null,modifiedDate DATE null,ddmFormInstanceId LONG,ddmDataProviderInstanceId LONG,sourceDataProviderId LONG,method VARCHAR(75) null,mapping STRING null,headers STRING null,chain INTEGER,body STRING null)";

	public static final String TABLE_SQL_DROP =
		"drop table Placecube_DataConsumer_DataConsumer";

	public static final String ORDER_BY_JPQL =
		" ORDER BY dataConsumer.dataConsumerId ASC";

	public static final String ORDER_BY_SQL =
		" ORDER BY Placecube_DataConsumer_DataConsumer.dataConsumerId ASC";

	public static final String DATA_SOURCE = "liferayDataSource";

	public static final String SESSION_FACTORY = "liferaySessionFactory";

	public static final String TX_MANAGER = "liferayTransactionManager";

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long COMPANYID_COLUMN_BITMASK = 1L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long DDMDATAPROVIDERINSTANCEID_COLUMN_BITMASK = 2L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long DDMFORMINSTANCEID_COLUMN_BITMASK = 4L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long GROUPID_COLUMN_BITMASK = 8L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long UUID_COLUMN_BITMASK = 16L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *		#getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long DATACONSUMERID_COLUMN_BITMASK = 32L;

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setEntityCacheEnabled(boolean entityCacheEnabled) {
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setFinderCacheEnabled(boolean finderCacheEnabled) {
	}

	public DataConsumerModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _dataConsumerId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setDataConsumerId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _dataConsumerId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return DataConsumer.class;
	}

	@Override
	public String getModelClassName() {
		return DataConsumer.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		Map<String, Function<DataConsumer, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		for (Map.Entry<String, Function<DataConsumer, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<DataConsumer, Object> attributeGetterFunction =
				entry.getValue();

			attributes.put(
				attributeName,
				attributeGetterFunction.apply((DataConsumer)this));
		}

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Map<String, BiConsumer<DataConsumer, Object>>
			attributeSetterBiConsumers = getAttributeSetterBiConsumers();

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			String attributeName = entry.getKey();

			BiConsumer<DataConsumer, Object> attributeSetterBiConsumer =
				attributeSetterBiConsumers.get(attributeName);

			if (attributeSetterBiConsumer != null) {
				attributeSetterBiConsumer.accept(
					(DataConsumer)this, entry.getValue());
			}
		}
	}

	public Map<String, Function<DataConsumer, Object>>
		getAttributeGetterFunctions() {

		return AttributeGetterFunctionsHolder._attributeGetterFunctions;
	}

	public Map<String, BiConsumer<DataConsumer, Object>>
		getAttributeSetterBiConsumers() {

		return AttributeSetterBiConsumersHolder._attributeSetterBiConsumers;
	}

	private static class AttributeGetterFunctionsHolder {

		private static final Map<String, Function<DataConsumer, Object>>
			_attributeGetterFunctions;

		static {
			Map<String, Function<DataConsumer, Object>>
				attributeGetterFunctions =
					new LinkedHashMap<String, Function<DataConsumer, Object>>();

			attributeGetterFunctions.put("uuid", DataConsumer::getUuid);
			attributeGetterFunctions.put(
				"dataConsumerId", DataConsumer::getDataConsumerId);
			attributeGetterFunctions.put("groupId", DataConsumer::getGroupId);
			attributeGetterFunctions.put(
				"companyId", DataConsumer::getCompanyId);
			attributeGetterFunctions.put("userId", DataConsumer::getUserId);
			attributeGetterFunctions.put(
				"createDate", DataConsumer::getCreateDate);
			attributeGetterFunctions.put(
				"modifiedDate", DataConsumer::getModifiedDate);
			attributeGetterFunctions.put(
				"ddmFormInstanceId", DataConsumer::getDdmFormInstanceId);
			attributeGetterFunctions.put(
				"ddmDataProviderInstanceId",
				DataConsumer::getDdmDataProviderInstanceId);
			attributeGetterFunctions.put(
				"sourceDataProviderId", DataConsumer::getSourceDataProviderId);
			attributeGetterFunctions.put("method", DataConsumer::getMethod);
			attributeGetterFunctions.put("mapping", DataConsumer::getMapping);
			attributeGetterFunctions.put("headers", DataConsumer::getHeaders);
			attributeGetterFunctions.put("chain", DataConsumer::getChain);
			attributeGetterFunctions.put("body", DataConsumer::getBody);

			_attributeGetterFunctions = Collections.unmodifiableMap(
				attributeGetterFunctions);
		}

	}

	private static class AttributeSetterBiConsumersHolder {

		private static final Map<String, BiConsumer<DataConsumer, Object>>
			_attributeSetterBiConsumers;

		static {
			Map<String, BiConsumer<DataConsumer, ?>>
				attributeSetterBiConsumers =
					new LinkedHashMap<String, BiConsumer<DataConsumer, ?>>();

			attributeSetterBiConsumers.put(
				"uuid",
				(BiConsumer<DataConsumer, String>)DataConsumer::setUuid);
			attributeSetterBiConsumers.put(
				"dataConsumerId",
				(BiConsumer<DataConsumer, Long>)
					DataConsumer::setDataConsumerId);
			attributeSetterBiConsumers.put(
				"groupId",
				(BiConsumer<DataConsumer, Long>)DataConsumer::setGroupId);
			attributeSetterBiConsumers.put(
				"companyId",
				(BiConsumer<DataConsumer, Long>)DataConsumer::setCompanyId);
			attributeSetterBiConsumers.put(
				"userId",
				(BiConsumer<DataConsumer, Long>)DataConsumer::setUserId);
			attributeSetterBiConsumers.put(
				"createDate",
				(BiConsumer<DataConsumer, Date>)DataConsumer::setCreateDate);
			attributeSetterBiConsumers.put(
				"modifiedDate",
				(BiConsumer<DataConsumer, Date>)DataConsumer::setModifiedDate);
			attributeSetterBiConsumers.put(
				"ddmFormInstanceId",
				(BiConsumer<DataConsumer, Long>)
					DataConsumer::setDdmFormInstanceId);
			attributeSetterBiConsumers.put(
				"ddmDataProviderInstanceId",
				(BiConsumer<DataConsumer, Long>)
					DataConsumer::setDdmDataProviderInstanceId);
			attributeSetterBiConsumers.put(
				"sourceDataProviderId",
				(BiConsumer<DataConsumer, Long>)
					DataConsumer::setSourceDataProviderId);
			attributeSetterBiConsumers.put(
				"method",
				(BiConsumer<DataConsumer, String>)DataConsumer::setMethod);
			attributeSetterBiConsumers.put(
				"mapping",
				(BiConsumer<DataConsumer, String>)DataConsumer::setMapping);
			attributeSetterBiConsumers.put(
				"headers",
				(BiConsumer<DataConsumer, String>)DataConsumer::setHeaders);
			attributeSetterBiConsumers.put(
				"chain",
				(BiConsumer<DataConsumer, Integer>)DataConsumer::setChain);
			attributeSetterBiConsumers.put(
				"body",
				(BiConsumer<DataConsumer, String>)DataConsumer::setBody);

			_attributeSetterBiConsumers = Collections.unmodifiableMap(
				(Map)attributeSetterBiConsumers);
		}

	}

	@Override
	public String getUuid() {
		if (_uuid == null) {
			return "";
		}
		else {
			return _uuid;
		}
	}

	@Override
	public void setUuid(String uuid) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_uuid = uuid;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalUuid() {
		return getColumnOriginalValue("uuid_");
	}

	@Override
	public long getDataConsumerId() {
		return _dataConsumerId;
	}

	@Override
	public void setDataConsumerId(long dataConsumerId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_dataConsumerId = dataConsumerId;
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_groupId = groupId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalGroupId() {
		return GetterUtil.getLong(this.<Long>getColumnOriginalValue("groupId"));
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_companyId = companyId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalCompanyId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("companyId"));
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() {
		try {
			User user = UserLocalServiceUtil.getUserById(getUserId());

			return user.getUuid();
		}
		catch (PortalException portalException) {
			return "";
		}
	}

	@Override
	public void setUserUuid(String userUuid) {
	}

	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_createDate = createDate;
	}

	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public boolean hasSetModifiedDate() {
		return _setModifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_setModifiedDate = true;

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_modifiedDate = modifiedDate;
	}

	@Override
	public long getDdmFormInstanceId() {
		return _ddmFormInstanceId;
	}

	@Override
	public void setDdmFormInstanceId(long ddmFormInstanceId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_ddmFormInstanceId = ddmFormInstanceId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalDdmFormInstanceId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("ddmFormInstanceId"));
	}

	@Override
	public long getDdmDataProviderInstanceId() {
		return _ddmDataProviderInstanceId;
	}

	@Override
	public void setDdmDataProviderInstanceId(long ddmDataProviderInstanceId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_ddmDataProviderInstanceId = ddmDataProviderInstanceId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalDdmDataProviderInstanceId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("ddmDataProviderInstanceId"));
	}

	@Override
	public long getSourceDataProviderId() {
		return _sourceDataProviderId;
	}

	@Override
	public void setSourceDataProviderId(long sourceDataProviderId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_sourceDataProviderId = sourceDataProviderId;
	}

	@Override
	public String getMethod() {
		if (_method == null) {
			return "";
		}
		else {
			return _method;
		}
	}

	@Override
	public void setMethod(String method) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_method = method;
	}

	@Override
	public String getMapping() {
		if (_mapping == null) {
			return "";
		}
		else {
			return _mapping;
		}
	}

	@Override
	public void setMapping(String mapping) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_mapping = mapping;
	}

	@Override
	public String getHeaders() {
		if (_headers == null) {
			return "";
		}
		else {
			return _headers;
		}
	}

	@Override
	public void setHeaders(String headers) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_headers = headers;
	}

	@Override
	public Integer getChain() {
		return _chain;
	}

	@Override
	public void setChain(Integer chain) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_chain = chain;
	}

	@Override
	public String getBody() {
		if (_body == null) {
			return "";
		}
		else {
			return _body;
		}
	}

	@Override
	public void setBody(String body) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_body = body;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return new StagedModelType(
			PortalUtil.getClassNameId(DataConsumer.class.getName()));
	}

	public long getColumnBitmask() {
		if (_columnBitmask > 0) {
			return _columnBitmask;
		}

		if ((_columnOriginalValues == null) ||
			(_columnOriginalValues == Collections.EMPTY_MAP)) {

			return 0;
		}

		for (Map.Entry<String, Object> entry :
				_columnOriginalValues.entrySet()) {

			if (!Objects.equals(
					entry.getValue(), getColumnValue(entry.getKey()))) {

				_columnBitmask |= _columnBitmasks.get(entry.getKey());
			}
		}

		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(
			getCompanyId(), DataConsumer.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public DataConsumer toEscapedModel() {
		if (_escapedModel == null) {
			Function<InvocationHandler, DataConsumer>
				escapedModelProxyProviderFunction =
					EscapedModelProxyProviderFunctionHolder.
						_escapedModelProxyProviderFunction;

			_escapedModel = escapedModelProxyProviderFunction.apply(
				new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		DataConsumerImpl dataConsumerImpl = new DataConsumerImpl();

		dataConsumerImpl.setUuid(getUuid());
		dataConsumerImpl.setDataConsumerId(getDataConsumerId());
		dataConsumerImpl.setGroupId(getGroupId());
		dataConsumerImpl.setCompanyId(getCompanyId());
		dataConsumerImpl.setUserId(getUserId());
		dataConsumerImpl.setCreateDate(getCreateDate());
		dataConsumerImpl.setModifiedDate(getModifiedDate());
		dataConsumerImpl.setDdmFormInstanceId(getDdmFormInstanceId());
		dataConsumerImpl.setDdmDataProviderInstanceId(
			getDdmDataProviderInstanceId());
		dataConsumerImpl.setSourceDataProviderId(getSourceDataProviderId());
		dataConsumerImpl.setMethod(getMethod());
		dataConsumerImpl.setMapping(getMapping());
		dataConsumerImpl.setHeaders(getHeaders());
		dataConsumerImpl.setChain(getChain());
		dataConsumerImpl.setBody(getBody());

		dataConsumerImpl.resetOriginalValues();

		return dataConsumerImpl;
	}

	@Override
	public DataConsumer cloneWithOriginalValues() {
		DataConsumerImpl dataConsumerImpl = new DataConsumerImpl();

		dataConsumerImpl.setUuid(this.<String>getColumnOriginalValue("uuid_"));
		dataConsumerImpl.setDataConsumerId(
			this.<Long>getColumnOriginalValue("dataConsumerId"));
		dataConsumerImpl.setGroupId(
			this.<Long>getColumnOriginalValue("groupId"));
		dataConsumerImpl.setCompanyId(
			this.<Long>getColumnOriginalValue("companyId"));
		dataConsumerImpl.setUserId(this.<Long>getColumnOriginalValue("userId"));
		dataConsumerImpl.setCreateDate(
			this.<Date>getColumnOriginalValue("createDate"));
		dataConsumerImpl.setModifiedDate(
			this.<Date>getColumnOriginalValue("modifiedDate"));
		dataConsumerImpl.setDdmFormInstanceId(
			this.<Long>getColumnOriginalValue("ddmFormInstanceId"));
		dataConsumerImpl.setDdmDataProviderInstanceId(
			this.<Long>getColumnOriginalValue("ddmDataProviderInstanceId"));
		dataConsumerImpl.setSourceDataProviderId(
			this.<Long>getColumnOriginalValue("sourceDataProviderId"));
		dataConsumerImpl.setMethod(
			this.<String>getColumnOriginalValue("method"));
		dataConsumerImpl.setMapping(
			this.<String>getColumnOriginalValue("mapping"));
		dataConsumerImpl.setHeaders(
			this.<String>getColumnOriginalValue("headers"));
		dataConsumerImpl.setChain(
			this.<Integer>getColumnOriginalValue("chain"));
		dataConsumerImpl.setBody(this.<String>getColumnOriginalValue("body"));

		return dataConsumerImpl;
	}

	@Override
	public int compareTo(DataConsumer dataConsumer) {
		long primaryKey = dataConsumer.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DataConsumer)) {
			return false;
		}

		DataConsumer dataConsumer = (DataConsumer)object;

		long primaryKey = dataConsumer.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isEntityCacheEnabled() {
		return true;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isFinderCacheEnabled() {
		return true;
	}

	@Override
	public void resetOriginalValues() {
		_columnOriginalValues = Collections.emptyMap();

		_setModifiedDate = false;

		_columnBitmask = 0;
	}

	@Override
	public CacheModel<DataConsumer> toCacheModel() {
		DataConsumerCacheModel dataConsumerCacheModel =
			new DataConsumerCacheModel();

		dataConsumerCacheModel.uuid = getUuid();

		String uuid = dataConsumerCacheModel.uuid;

		if ((uuid != null) && (uuid.length() == 0)) {
			dataConsumerCacheModel.uuid = null;
		}

		dataConsumerCacheModel.dataConsumerId = getDataConsumerId();

		dataConsumerCacheModel.groupId = getGroupId();

		dataConsumerCacheModel.companyId = getCompanyId();

		dataConsumerCacheModel.userId = getUserId();

		Date createDate = getCreateDate();

		if (createDate != null) {
			dataConsumerCacheModel.createDate = createDate.getTime();
		}
		else {
			dataConsumerCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			dataConsumerCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			dataConsumerCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		dataConsumerCacheModel.ddmFormInstanceId = getDdmFormInstanceId();

		dataConsumerCacheModel.ddmDataProviderInstanceId =
			getDdmDataProviderInstanceId();

		dataConsumerCacheModel.sourceDataProviderId = getSourceDataProviderId();

		dataConsumerCacheModel.method = getMethod();

		String method = dataConsumerCacheModel.method;

		if ((method != null) && (method.length() == 0)) {
			dataConsumerCacheModel.method = null;
		}

		dataConsumerCacheModel.mapping = getMapping();

		String mapping = dataConsumerCacheModel.mapping;

		if ((mapping != null) && (mapping.length() == 0)) {
			dataConsumerCacheModel.mapping = null;
		}

		dataConsumerCacheModel.headers = getHeaders();

		String headers = dataConsumerCacheModel.headers;

		if ((headers != null) && (headers.length() == 0)) {
			dataConsumerCacheModel.headers = null;
		}

		Integer chain = getChain();

		if (chain != null) {
			dataConsumerCacheModel.chain = chain;
		}

		dataConsumerCacheModel.body = getBody();

		String body = dataConsumerCacheModel.body;

		if ((body != null) && (body.length() == 0)) {
			dataConsumerCacheModel.body = null;
		}

		return dataConsumerCacheModel;
	}

	@Override
	public String toString() {
		Map<String, Function<DataConsumer, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			(5 * attributeGetterFunctions.size()) + 2);

		sb.append("{");

		for (Map.Entry<String, Function<DataConsumer, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<DataConsumer, Object> attributeGetterFunction =
				entry.getValue();

			sb.append("\"");
			sb.append(attributeName);
			sb.append("\": ");

			Object value = attributeGetterFunction.apply((DataConsumer)this);

			if (value == null) {
				sb.append("null");
			}
			else if (value instanceof Blob || value instanceof Date ||
					 value instanceof Map || value instanceof String) {

				sb.append(
					"\"" + StringUtil.replace(value.toString(), "\"", "'") +
						"\"");
			}
			else {
				sb.append(value);
			}

			sb.append(", ");
		}

		if (sb.index() > 1) {
			sb.setIndex(sb.index() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	private static class EscapedModelProxyProviderFunctionHolder {

		private static final Function<InvocationHandler, DataConsumer>
			_escapedModelProxyProviderFunction =
				ProxyUtil.getProxyProviderFunction(
					DataConsumer.class, ModelWrapper.class);

	}

	private String _uuid;
	private long _dataConsumerId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private Date _createDate;
	private Date _modifiedDate;
	private boolean _setModifiedDate;
	private long _ddmFormInstanceId;
	private long _ddmDataProviderInstanceId;
	private long _sourceDataProviderId;
	private String _method;
	private String _mapping;
	private String _headers;
	private Integer _chain;
	private String _body;

	public <T> T getColumnValue(String columnName) {
		columnName = _attributeNames.getOrDefault(columnName, columnName);

		Function<DataConsumer, Object> function =
			AttributeGetterFunctionsHolder._attributeGetterFunctions.get(
				columnName);

		if (function == null) {
			throw new IllegalArgumentException(
				"No attribute getter function found for " + columnName);
		}

		return (T)function.apply((DataConsumer)this);
	}

	public <T> T getColumnOriginalValue(String columnName) {
		if (_columnOriginalValues == null) {
			return null;
		}

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		return (T)_columnOriginalValues.get(columnName);
	}

	private void _setColumnOriginalValues() {
		_columnOriginalValues = new HashMap<String, Object>();

		_columnOriginalValues.put("uuid_", _uuid);
		_columnOriginalValues.put("dataConsumerId", _dataConsumerId);
		_columnOriginalValues.put("groupId", _groupId);
		_columnOriginalValues.put("companyId", _companyId);
		_columnOriginalValues.put("userId", _userId);
		_columnOriginalValues.put("createDate", _createDate);
		_columnOriginalValues.put("modifiedDate", _modifiedDate);
		_columnOriginalValues.put("ddmFormInstanceId", _ddmFormInstanceId);
		_columnOriginalValues.put(
			"ddmDataProviderInstanceId", _ddmDataProviderInstanceId);
		_columnOriginalValues.put(
			"sourceDataProviderId", _sourceDataProviderId);
		_columnOriginalValues.put("method", _method);
		_columnOriginalValues.put("mapping", _mapping);
		_columnOriginalValues.put("headers", _headers);
		_columnOriginalValues.put("chain", _chain);
		_columnOriginalValues.put("body", _body);
	}

	private static final Map<String, String> _attributeNames;

	static {
		Map<String, String> attributeNames = new HashMap<>();

		attributeNames.put("uuid_", "uuid");

		_attributeNames = Collections.unmodifiableMap(attributeNames);
	}

	private transient Map<String, Object> _columnOriginalValues;

	public static long getColumnBitmask(String columnName) {
		return _columnBitmasks.get(columnName);
	}

	private static final Map<String, Long> _columnBitmasks;

	static {
		Map<String, Long> columnBitmasks = new HashMap<>();

		columnBitmasks.put("uuid_", 1L);

		columnBitmasks.put("dataConsumerId", 2L);

		columnBitmasks.put("groupId", 4L);

		columnBitmasks.put("companyId", 8L);

		columnBitmasks.put("userId", 16L);

		columnBitmasks.put("createDate", 32L);

		columnBitmasks.put("modifiedDate", 64L);

		columnBitmasks.put("ddmFormInstanceId", 128L);

		columnBitmasks.put("ddmDataProviderInstanceId", 256L);

		columnBitmasks.put("sourceDataProviderId", 512L);

		columnBitmasks.put("method", 1024L);

		columnBitmasks.put("mapping", 2048L);

		columnBitmasks.put("headers", 4096L);

		columnBitmasks.put("chain", 8192L);

		columnBitmasks.put("body", 16384L);

		_columnBitmasks = Collections.unmodifiableMap(columnBitmasks);
	}

	private long _columnBitmask;
	private DataConsumer _escapedModel;

}