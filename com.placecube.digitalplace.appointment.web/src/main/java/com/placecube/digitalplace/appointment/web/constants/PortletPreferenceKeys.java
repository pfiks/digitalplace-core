package com.placecube.digitalplace.appointment.web.constants;

public final class PortletPreferenceKeys {

	public static final String DISPLAY_PAGE_TEMPLATE_ENTRY_ID = "displayPageTemplateEntryId";

	public static final String ID_COLUMN_LABEL = "idColumnLabel";

	public static final String FILTER_COLUMN_LABEL = "filterColumnLabel";

	public static final String OBJECT_DEFINTION_ID = "objectDefinitionId";

	public static final String ODATA_FILTER = "odataFilter";

	public static final String SERVICE_ID = "serviceId";

	public static final String STARTDATE_COLUMN_LABEL = "startDateColumnLabel";

	public static final String SUMMARY_COLUMN_LABEL = "summaryColumnLabel";

	public static final String SUMMARY_FIELD_EXPRESSION = "summaryFieldExpression";

	private PortletPreferenceKeys() {
	}
}