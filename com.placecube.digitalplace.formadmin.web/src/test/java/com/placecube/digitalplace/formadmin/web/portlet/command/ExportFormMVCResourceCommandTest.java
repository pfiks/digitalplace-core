package com.placecube.digitalplace.formadmin.web.portlet.command;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMFormInstance;
import com.liferay.dynamic.data.mapping.service.DDMFormInstanceLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.service.FormExportService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ FileUtil.class, ParamUtil.class, PortletResponseUtil.class, PropsUtil.class, ServiceContextFactory.class })
public class ExportFormMVCResourceCommandTest extends PowerMockito {

	@Mock
	private DDMFormInstance mockDDMFormInstance;

	@Mock
	private DDMFormInstanceLocalService mockDDMFormInstanceLocalService;

	@InjectMocks
	private ExportFormMVCResourceCommand exportFormMVCResourceCommand;

	@Mock
	private File mockFile;

	@Mock
	private FileInputStream mockFileInputStream;

	@Mock
	private FormExportService mockFormExportService;

	@Mock
	private ResourceRequest mockResourceRequest;

	@Mock
	private ResourceResponse mockResourceResponse;

	@Before
	public void activateSetup() {
		mockStatic(FileUtil.class, PropsUtil.class, ParamUtil.class, PortletResponseUtil.class, ServiceContextFactory.class);
	}

	@Test(expected = Exception.class)
	public void doServeResource_WhenErrorGettingFormInstanceId_ThenPortletExceptionIsThrown() throws Exception {
		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.FORM_INSTANCE_ID)).thenThrow(new PortletException());
		exportFormMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test(expected = PortalException.class)
	public void doServeResource_WhenErrorExportingForm_ThenPortalExceptionIsThrown() throws Exception {
		long ddmFormInstanceId = 123L;
		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.FORM_INSTANCE_ID)).thenReturn(ddmFormInstanceId);
		when(mockDDMFormInstanceLocalService.getDDMFormInstance(ddmFormInstanceId)).thenReturn(mockDDMFormInstance);

		when(mockFormExportService.exportForm(mockDDMFormInstance)).thenThrow(new PortalException());
		exportFormMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test(expected = IOException.class)
	public void doServeResource_WhenErrorExportingForm_ThenIOExceptionIsThrown() throws Exception {
		long ddmFormInstanceId = 123L;
		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.FORM_INSTANCE_ID)).thenReturn(ddmFormInstanceId);
		when(mockDDMFormInstanceLocalService.getDDMFormInstance(ddmFormInstanceId)).thenReturn(mockDDMFormInstance);

		when(mockFormExportService.exportForm(mockDDMFormInstance)).thenThrow(new IOException());
		exportFormMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test(expected = FileNotFoundException.class)
	public void doServeResource_WhenErrorGettingFileInputStream_ThenThrowException() throws Exception {
		long ddmFormInstanceId = 123L;

		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.FORM_INSTANCE_ID)).thenReturn(ddmFormInstanceId);
		when(mockDDMFormInstanceLocalService.getDDMFormInstance(ddmFormInstanceId)).thenReturn(mockDDMFormInstance);

		when(mockFile.length()).thenReturn(345L);
		when(mockFile.getPath()).thenReturn("test");
		when(mockFormExportService.exportForm(mockDDMFormInstance)).thenReturn(mockFile);
		when(mockResourceRequest.getLocale()).thenReturn(Locale.UK);

		when(mockFormExportService.getFileInputStream(mockFile)).thenThrow(new FileNotFoundException());

		exportFormMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test(expected = IOException.class)
	public void doServeResource_WhenErrorReading_ThenThrowException() throws Exception {
		long ddmFormInstanceId = 123L;

		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.FORM_INSTANCE_ID)).thenReturn(ddmFormInstanceId);
		when(mockDDMFormInstanceLocalService.getDDMFormInstance(ddmFormInstanceId)).thenReturn(mockDDMFormInstance);

		when(mockFile.length()).thenReturn(345L);
		when(mockFile.getPath()).thenReturn("test");
		when(mockFormExportService.exportForm(mockDDMFormInstance)).thenReturn(mockFile);
		when(mockResourceRequest.getLocale()).thenReturn(Locale.UK);

		when(mockFormExportService.getFileInputStream(mockFile)).thenReturn(mockFileInputStream);

		when(mockFile.length()).thenReturn(5L);

		when(mockFileInputStream.read(Mockito.any())).thenThrow(new IOException());

		exportFormMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);
	}

	@Test
	public void doServeResource_WhenNoError_ThenDownloadForm() throws Exception {
		long ddmFormInstanceId = 123L;
		String formName = "formName";
		byte[] bytes = formName.getBytes();
		when(ParamUtil.getLong(mockResourceRequest, PortletRequestKeys.FORM_INSTANCE_ID)).thenReturn(ddmFormInstanceId);
		when(mockDDMFormInstanceLocalService.getDDMFormInstance(ddmFormInstanceId)).thenReturn(mockDDMFormInstance);

		when(mockFile.length()).thenReturn(345L);
		when(mockFile.getPath()).thenReturn("test");
		when(mockFormExportService.exportForm(mockDDMFormInstance)).thenReturn(mockFile);
		when(mockResourceRequest.getLocale()).thenReturn(Locale.UK);
		when(mockDDMFormInstance.getName(Locale.UK)).thenReturn(formName);

		when(mockFormExportService.getFileInputStream(mockFile)).thenReturn(mockFileInputStream);

		when(mockFile.length()).thenReturn(5L);

		when(mockFileInputStream.read(Mockito.any())).thenReturn(5);

		when(FileUtil.getBytes(mockFile)).thenReturn(bytes);

		exportFormMVCResourceCommand.doServeResource(mockResourceRequest, mockResourceResponse);

		verify(mockResourceResponse, Mockito.times(1)).addProperty(Mockito.eq(HttpHeaders.CONTENT_DISPOSITION), Mockito.anyString());

		verifyStatic(FileUtil.class, times(1));
		FileUtil.getBytes(mockFile);

		verifyStatic(PortletResponseUtil.class, times(1));
		PortletResponseUtil.sendFile(mockResourceRequest, mockResourceResponse, formName + ".lar", bytes);
	}
}
