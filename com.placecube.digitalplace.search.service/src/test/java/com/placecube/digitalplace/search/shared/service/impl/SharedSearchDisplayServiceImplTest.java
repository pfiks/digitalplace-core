package com.placecube.digitalplace.search.shared.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.util.HttpComponentsUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.search.internal.util.RequestUtil;
import com.placecube.digitalplace.search.shared.constants.SharedSearchDisplayCostants;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ ParamUtil.class, HttpComponentsUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class SharedSearchDisplayServiceImplTest extends PowerMockito {

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private RequestUtil mockRequestUtil;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@InjectMocks
	private SharedSearchDisplayServiceImpl sharedSearchDisplayServiceImpl;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class, HttpComponentsUtil.class);
	}

	@Test
	public void getDisplayStyle_WhenDisplayStyleNotInRequest_ThenReturnsProvidedDefaultStyle() {
		String defaultStyle = "default-style";
		when(mockRequestUtil.getOriginalHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "displayStyle", defaultStyle)).thenReturn(defaultStyle);

		String result = sharedSearchDisplayServiceImpl.getDisplayStyle(mockPortletRequest, defaultStyle);

		assertEquals(defaultStyle, result);
	}

	@Test
	@Parameters({ "", "null" })
	public void getDisplayStyle_WhenDisplayStyleNotPresentInRequestAndDefaultStyleIsNullOrEmpty_ThenReturnsListStyle(String defaultStyle) {

		when(mockRequestUtil.getOriginalHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "displayStyle", SharedSearchDisplayCostants.DISPLAY_STYLE_LIST)).thenReturn(SharedSearchDisplayCostants.DISPLAY_STYLE_LIST);

		String result = sharedSearchDisplayServiceImpl.getDisplayStyle(mockPortletRequest, defaultStyle);

		assertEquals(SharedSearchDisplayCostants.DISPLAY_STYLE_LIST, result);
	}

	@Test
	public void getDisplayStyle_WhenDisplayStylePresentInRequest_ThenReturnsProvidedDefaultStyle() {
		String defaultStyle = "default-style";
		String expectedStyle = "expected-style";
		when(mockRequestUtil.getOriginalHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "displayStyle", defaultStyle)).thenReturn(expectedStyle);

		String result = sharedSearchDisplayServiceImpl.getDisplayStyle(mockPortletRequest, defaultStyle);

		assertEquals(expectedStyle, result);
	}

	@Test
	public void getDisplayStyleGridURL_WhenNoError_ThenReturnsTheIteratorURLWithDisplayStyleSetToGrid() {
		String iteratorURL = "iteratorURLVal";
		String expected = "expectedVal";
		when(mockSharedSearchResponse.getIteratorURL()).thenReturn(iteratorURL);
		when(HttpComponentsUtil.setParameter(iteratorURL, "displayStyle", "grid")).thenReturn(expected);

		String result = sharedSearchDisplayServiceImpl.getDisplayStyleGridURL(mockSharedSearchResponse);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getDisplayStyleListURL_WhenNoError_ThenReturnsTheIteratorURLWithDisplayStyleSetToList() {
		String iteratorURL = "iteratorURLVal";
		String expected = "expectedVal";
		when(mockSharedSearchResponse.getIteratorURL()).thenReturn(iteratorURL);
		when(HttpComponentsUtil.setParameter(iteratorURL, "displayStyle", "list")).thenReturn(expected);

		String result = sharedSearchDisplayServiceImpl.getDisplayStyleListURL(mockSharedSearchResponse);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void isDisplayStyleList_WhenDisplayStyleIsList_ThenReturnsTrue() {
		when(mockRequestUtil.getOriginalHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "displayStyle", "list")).thenReturn("list");

		boolean result = sharedSearchDisplayServiceImpl.isDisplayStyleList(mockPortletRequest);

		assertTrue(result);
	}

	@Test
	public void isDisplayStyleList_WhenDisplayStyleIsNotList_ThenReturnsFalse() {
		when(mockRequestUtil.getOriginalHttpServletRequest(mockPortletRequest)).thenReturn(mockHttpServletRequest);
		when(ParamUtil.getString(mockHttpServletRequest, "displayStyle", "list")).thenReturn("somethingElse");

		boolean result = sharedSearchDisplayServiceImpl.isDisplayStyleList(mockPortletRequest);

		assertFalse(result);
	}

}
