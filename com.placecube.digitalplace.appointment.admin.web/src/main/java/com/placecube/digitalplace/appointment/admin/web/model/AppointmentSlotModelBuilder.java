package com.placecube.digitalplace.appointment.admin.web.model;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerUtils;
import com.placecube.digitalplace.appointment.model.AppointmentSlot;

@Component(immediate = true, service = AppointmentSlotModelBuilder.class)
public class AppointmentSlotModelBuilder {

	@Reference
	private AppointmentsManagerUtils appointmentsManagerUtils;

	public AppointmentSlotForm getAppointmentSlotForm(AppointmentSlot appointmentSlot) {
		return new AppointmentSlotForm(appointmentSlot,
				appointmentsManagerUtils.getDateTimeSelection(appointmentSlot.getStartDate(), appointmentSlot.getSlotStartTimeHour(), appointmentSlot.getSlotStartTimeMinute()),
				appointmentsManagerUtils.getDateTimeSelection(appointmentSlot.getEndDate(), appointmentSlot.getSlotEndTimeHour(), appointmentSlot.getSlotEndTimeMinute()));
	}
}
