package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.service;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.osgi.service.component.annotations.Component;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.mapping.form.field.type.constants.DDMFormFieldTypeConstants;
import com.liferay.dynamic.data.mapping.model.DDMFormField;
import com.liferay.dynamic.data.mapping.model.DDMFormFieldType;
import com.liferay.dynamic.data.mapping.storage.DDMFormFieldValue;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@Component(immediate = true, service = DDLFieldFacetDDMFormFieldService.class)
public class DDLFieldFacetDDMFormFieldService {

	private static final Log LOG = LogFactoryUtil.getLog(DDLFieldFacetDDMFormFieldService.class);

	public boolean fieldTypeAllowsOptions(DDMFormField ddmFormField) {
		String type = ddmFormField.getType();
		return !ddmFormField.isRepeatable() && //
				(DDMFormFieldType.CHECKBOX.equals(type) //
						|| DDMFormFieldTypeConstants.CHECKBOX.equals(type)//
						|| DDMFormFieldType.RADIO.equals(type) //
						|| DDMFormFieldTypeConstants.RADIO.equals(type) //
						|| DDMFormFieldType.SELECT.equals(type) //
						|| DDMFormFieldTypeConstants.SELECT.equals(type));
	}

	public String getDDMFormFieldStringValue(DDLRecord ddlRecord, String fieldName, Locale locale) {

		try {
			List<DDMFormFieldValue> values = ddlRecord.getDDMFormFieldValues(fieldName);

			return Objects.nonNull(values) && !values.isEmpty() ? values.get(0).getValue().getString(locale) : StringPool.BLANK;
		} catch (PortalException e) {
			LOG.error(e);
			return StringPool.BLANK;
		}

	}
}
