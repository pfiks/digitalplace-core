package com.placecube.digitalplace.audit.service;

import java.util.Arrays;
import java.util.Dictionary;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.audit.AuditMessage;
import com.liferay.portal.kernel.audit.AuditRouter;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.PortalPreferenceValue;
import com.liferay.portal.kernel.model.PortletPreferences;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.security.audit.event.generators.util.AuditMessageBuilder;

@Component(immediate = true, service = AuditService.class)
public class AuditService {

	private static final Log LOG = LogFactoryUtil.getLog(AuditService.class);

	@Reference
	private AuditRouter auditRouter;

	@Reference
	private PortletPreferencesLocalService portletPreferencesLocalService;

	public void addAuditLogEntry(String className, long classPK, String eventType, Map<String, String> aditionalInformationMap) {
		try {
			long userId = PrincipalThreadLocal.getUserId();

			AuditMessage auditMessage = AuditMessageBuilder.buildAuditMessage(eventType, className, classPK, null);

			JSONObject additionalInfo = auditMessage.getAdditionalInfo();

			additionalInfo.put("userId", userId);
			additionalInfo.put("date", DateUtil.newDate());

			aditionalInformationMap.forEach(additionalInfo::put);

			auditMessage.setAdditionalInfo(additionalInfo);

			auditRouter.route(auditMessage);
		} catch (Exception e) {
			LOG.error("Unable to add audit log entry", e);
		}

	}

	public Map<String, String> getConfigurationAuditInfo(String pid) {
		Map<String, String> results = new LinkedHashMap<>();
		results.put("pid", pid);
		return results;
	}

	public Map<String, String> getConfigurationAuditInfo(String pid, Dictionary<String, Object> properties) {
		Map<String, String> results = new LinkedHashMap<>();
		results.put("pid", pid);
		properties.keys().asIterator().forEachRemaining(entry -> results.put(entry, GetterUtil.getString(properties.get(entry))));
		return results;
	}

	public Map<String, String> getGroupAuditInfo(Group group) {
		Map<String, String> results = new LinkedHashMap<>();
		results.put("groupId", String.valueOf(group.getGroupId()));
		results.put("groupName", group.getName(group.getDefaultLanguageId()));
		results.put("friendlyUrl", group.getFriendlyURL());
		return results;
	}

	public Map<String, String> getPortalPreferenceValueAuditInfo(PortalPreferenceValue portalPreferenceValue) {
		Map<String, String> results = new LinkedHashMap<>();
		results.put("portalPreferencesId", String.valueOf(portalPreferenceValue.getPortalPreferencesId()));
		results.put("portalPreferenceValueId", String.valueOf(portalPreferenceValue.getPortalPreferenceValueId()));
		results.put("key", portalPreferenceValue.getKey());
		results.put("smallValue", portalPreferenceValue.getSmallValue());
		results.put("largeValue", portalPreferenceValue.getLargeValue());
		return results;
	}

	public Map<String, String> getPortletPreferencesAuditInfo(PortletPreferences portletPreferences) {
		Map<String, String> results = new LinkedHashMap<>();
		results.put("portletPreferencesId", String.valueOf(portletPreferences.getPortletPreferencesId()));
		results.put("plid", String.valueOf(portletPreferences.getPlid()));
		results.put("portletId", portletPreferences.getPortletId());

		javax.portlet.PortletPreferences strictPreferences = portletPreferencesLocalService.getStrictPreferences(portletPreferences.getCompanyId(), portletPreferences.getOwnerId(),
				portletPreferences.getOwnerType(), portletPreferences.getPlid(), portletPreferences.getPortletId());
		for (Entry<String, String[]> prefValues : strictPreferences.getMap().entrySet()) {
			results.put(prefValues.getKey(), Arrays.toString(prefValues.getValue()));
		}

		return results;
	}

}
