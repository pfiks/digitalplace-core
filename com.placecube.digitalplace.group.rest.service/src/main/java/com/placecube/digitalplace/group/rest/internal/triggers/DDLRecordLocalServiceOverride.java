package com.placecube.digitalplace.group.rest.internal.triggers;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalServiceWrapper;
import com.liferay.dynamic.data.mapping.storage.DDMFormValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceWrapper;
import com.placecube.digitalplace.group.rest.constants.WebhookAction;
import com.placecube.digitalplace.group.rest.internal.processor.DDLWebhookProcessor;

@Component(immediate = true, property = { "service.ranking:Integer=100" }, service = ServiceWrapper.class)
public class DDLRecordLocalServiceOverride extends DDLRecordLocalServiceWrapper {

	@Reference
	private DDLWebhookProcessor ddlWebhookProcessor;

	public DDLRecordLocalServiceOverride() {
		super(null);
	}

	@Override
	public DDLRecord updateRecord(long userId, long recordId, boolean majorVersion, int displayIndex, DDMFormValues ddmFormValues, ServiceContext serviceContext) throws PortalException {
		DDLRecord record = super.updateRecord(userId, recordId, majorVersion, displayIndex, ddmFormValues, serviceContext);
		ddlWebhookProcessor.processDDLRecordAction(record, WebhookAction.UPDATE);
		return record;
	}

	@Override
	public DDLRecord addRecord(long userId, long groupId, long recordSetId, int displayIndex, DDMFormValues ddmFormValues, ServiceContext serviceContext) throws PortalException {
		DDLRecord record = super.addRecord(userId, groupId, recordSetId, displayIndex, ddmFormValues, serviceContext);
		ddlWebhookProcessor.processDDLRecordAction(record, WebhookAction.ADD);
		return record;
	}

}