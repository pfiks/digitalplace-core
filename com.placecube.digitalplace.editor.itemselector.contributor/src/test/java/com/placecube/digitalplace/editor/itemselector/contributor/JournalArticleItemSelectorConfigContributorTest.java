package com.placecube.digitalplace.editor.itemselector.contributor;

import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.editor.configuration.BaseEditorConfigContributor;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.language.constants.LanguageConstants;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.JournalArticleItemSelectorConfigContributor;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LanguageUtil.class, BaseEditorConfigContributor.class })
public class JournalArticleItemSelectorConfigContributorTest extends Mockito {

	@InjectMocks
	private JournalArticleItemSelectorConfigContributor journalArticleItemSelectorConfigContributor;

	@Mock
	private Map<String, Object> mockInputEditorTaglibAttributes;

	@Mock
	private JSONObject mockJsonObject;

	@Mock
	private RequestBackedPortletURLFactory mockRequestBackedPortletURLFactory;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void activateSetup() {
		mockStatic(LanguageUtil.class, BaseEditorConfigContributor.class);
	}

	@Test
	public void addJournalArticleTab_WhenConfigurationCalled() {
		when(mockInputEditorTaglibAttributes.get("liferay-ui:input-editor:contentsLanguageId")).thenReturn(Locale.US.toString());
		when(LanguageUtil.get(Locale.US, LanguageConstants.KEY_DIR)).thenReturn(String.valueOf(Locale.US));
		// when(HtmlUtil.escapeJS(anyString())).thenReturn("ltr");
		// could not invoked due to null pointer exception at
		// HtmlUtil.escapeJS(getContentsLanguageDir(inputEditorTaglibAttributes)
		// for populateConfigJSONObject
		// journalArticleItemSelectorConfigContributor.populateConfigJSONObject(mockJsonObject,
		// mockInputEditorTaglibAttributes, mockThemeDisplay,
		// mockRequestBackedPortletURLFactory);
		// verify(mockJsonObject, times(1));
	}

}
