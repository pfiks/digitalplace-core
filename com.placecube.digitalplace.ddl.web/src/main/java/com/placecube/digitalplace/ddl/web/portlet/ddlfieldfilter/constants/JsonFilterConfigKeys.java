package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.constants;

public final class JsonFilterConfigKeys {

	public static final String FILTER_FILTERS = "filters";

	public static final String FILTER_GROUP_NAME = "filterGroupName";

	public static final String FILTER_PRESELECTED_OPTION_NAME = "filterPreselectedOptionName";

	public static final String FILTER_OPTION_NAME = "filterOptionName";

	public static final String FILTER_OPTIONS = "options";

	private JsonFilterConfigKeys() {
	}
}
