
package com.placecube.digitalplace.externalpage.search.result.contributor;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.search.spi.model.result.contributor.ModelVisibilityContributor;

@Component(immediate = true, property = "indexer.class.name=com.placecube.digitalplace.externalpage.model.ExternalPage", service = ModelVisibilityContributor.class)
public class ExternalPageModelVisibilityContributor implements ModelVisibilityContributor {

	@Override
	public boolean isVisible(long classPK, int status) {
		return true;
	}

}