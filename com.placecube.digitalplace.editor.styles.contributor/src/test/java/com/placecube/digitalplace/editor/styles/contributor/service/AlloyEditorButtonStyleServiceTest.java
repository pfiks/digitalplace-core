package com.placecube.digitalplace.editor.styles.contributor.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.placecube.digitalplace.editor.styles.contributor.constants.ButtonStyle;

@RunWith(PowerMockRunner.class)
public class AlloyEditorButtonStyleServiceTest extends PowerMockito {

	@InjectMocks
	private AlloyEditorButtonStyleService alloyEditorButtonStyleService;

	@Mock
	private JSONObject mockJsonObjectStyleConfiguration;

	@Mock
	private JSONObject mockJsonObjectStyle;

	@Mock
	private JSONObject mockJsonObjectAttributes;

	@Mock
	private JSONObject mockJsonObjectAlloyEditor;

	@Mock
	private JSONObject mockJsonObjectToolbars;

	@Mock
	private JSONObject mockJsonObjectStyles;

	@Mock
	private JSONArray mockJsonArraySelections;

	@Mock
	private JSONObject mockJsonArraySelectionText;

	@Mock
	private JSONObject mockJsonArraySelectionImage;

	@Mock
	private JSONArray mockArrayButtons;

	@Mock
	private JSONObject mockJsonObjectButton;

	@Mock
	private JSONObject mockJsonObjectCfg;

	@Mock
	private JSONArray mockJsonArrayStyles;

	@Mock
	private JSONFactory mockJSONFactory;

	@Test
	public void createButtonStyle_WhenNoError_ThenReturnCreatedButtonStyleJsonObject() {
		when(mockJSONFactory.createJSONObject()).thenReturn(mockJsonObjectAttributes, mockJsonObjectStyle, mockJsonObjectStyleConfiguration);

		JSONObject result = alloyEditorButtonStyleService.createButtonStyle(ButtonStyle.BASE);

		verify(mockJsonObjectAttributes, times(1)).put("class", ButtonStyle.BASE.getValue());

		verify(mockJsonObjectStyle, times(1)).put("attributes", mockJsonObjectAttributes);
		verify(mockJsonObjectStyle, times(1)).put("element", ButtonStyle.BASE.getElement());

		verify(mockJsonObjectStyleConfiguration, times(1)).put("name", ButtonStyle.BASE.getName());
		verify(mockJsonObjectStyleConfiguration, times(1)).put("style", mockJsonObjectStyle);

		assertThat(result, equalTo(mockJsonObjectStyleConfiguration));

	}

	@Test
	public void getTextButtonStyles_WhenSelectionsHaveJsonArrayObjectWithNameText_ThenReturnTextButtonStyles() {
		mockAlloyEditorJsonObject(true);

		Optional<JSONArray> result = alloyEditorButtonStyleService.getTextButtonStyles(mockJsonObjectAlloyEditor);

		assertThat(result, Matchers.equalTo(Optional.of(mockJsonArrayStyles)));
	}

	@Test
	public void getTextButtonStyles_WhenSelectionsNotHaveJsonArrayObjectWithNameText_ThenReturnOptionalEmpty() {
		mockAlloyEditorJsonObject(false);

		Optional<JSONArray> result = alloyEditorButtonStyleService.getTextButtonStyles(mockJsonObjectAlloyEditor);

		assertThat(result, Matchers.equalTo(Optional.empty()));
	}

	@Test
	public void getTextButtonStyles_WhenError_ThenReturnOptionalEmpty() {
		when(mockJsonObjectAlloyEditor.getJSONObject("toolbars")).thenReturn(null);

		Optional<JSONArray> result = alloyEditorButtonStyleService.getTextButtonStyles(mockJsonObjectAlloyEditor);

		assertThat(result, Matchers.equalTo(Optional.empty()));
	}

	private void mockAlloyEditorJsonObject(boolean addTextJsonObjectSelections) {
		when(mockJsonObjectAlloyEditor.getJSONObject("toolbars")).thenReturn(mockJsonObjectToolbars);
		when(mockJsonObjectToolbars.getJSONObject("styles")).thenReturn(mockJsonObjectStyles);
		when(mockJsonObjectStyles.getJSONArray("selections")).thenReturn(mockJsonArraySelections);

		when(mockJsonArraySelections.getJSONObject(0)).thenReturn(mockJsonArraySelectionImage);
		when(mockJsonArraySelectionImage.get("name")).thenReturn("image");
		int mockJsonArraySelectionsElements = 1;
		if (addTextJsonObjectSelections) {
			when(mockJsonArraySelections.getJSONObject(1)).thenReturn(mockJsonArraySelectionText);
			when(mockJsonArraySelectionText.getJSONArray("buttons")).thenReturn(mockArrayButtons);
			when(mockJsonArraySelectionText.get("name")).thenReturn("text");
			when(mockArrayButtons.getJSONObject(0)).thenReturn(mockJsonObjectButton);
			when(mockJsonObjectButton.getJSONObject("cfg")).thenReturn(mockJsonObjectCfg);
			when(mockJsonObjectCfg.getJSONArray("styles")).thenReturn(mockJsonArrayStyles);

			mockJsonArraySelectionsElements++;
		}
		when(mockJsonArraySelections.length()).thenReturn(mockJsonArraySelectionsElements);

	}
}
