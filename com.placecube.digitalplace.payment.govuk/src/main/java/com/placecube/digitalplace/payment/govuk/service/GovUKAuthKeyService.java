package com.placecube.digitalplace.payment.govuk.service;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.govuk.configuration.GovUKCompanyConfiguration;

@Component(immediate = true, service = GovUKAuthKeyService.class)
public class GovUKAuthKeyService {

	private static final Log LOG = LogFactoryUtil.getLog(GovUKAuthKeyService.class);

	public String getApiAuthKey(GovUKCompanyConfiguration configuration, String apiKeyName) {
		String apiAuthKey = configuration.apiAuthKey().trim();
		if (Validator.isNotNull(configuration.additionalApiAuthKeys())) {
			for (String apiKey : configuration.additionalApiAuthKeys()) {
				String[] additionalKey = apiKey.split(StringPool.COLON);
				if (additionalKey.length == 2 && additionalKey[0].equals(apiKeyName)) {
					apiAuthKey = additionalKey[1].trim();
					break;
				}
			}
		}

		LOG.debug("Returning api auth key:" + apiAuthKey);
		return apiAuthKey;
	}

	public String getApiMessageVerificationSecretKey(GovUKCompanyConfiguration configuration, String apiMessageVerificationSecretKeyName) {
		String apiMessageVerificationSecretKey = configuration.apiMessageVerificationSecretKey().trim();
		if (Validator.isNotNull(configuration.additionalApiMessageVerificationSecretKeys())) {
			for (String apiKey : configuration.additionalApiMessageVerificationSecretKeys()) {
				String[] additionalKey = apiKey.split(StringPool.COLON);
				if (additionalKey.length == 2 && additionalKey[0].equals(apiMessageVerificationSecretKeyName)) {
					apiMessageVerificationSecretKey = additionalKey[1].trim();
					break;
				}
			}
		}

		LOG.debug("Returning api message verification secret key:" + apiMessageVerificationSecretKey);
		return apiMessageVerificationSecretKey;
	}
}
