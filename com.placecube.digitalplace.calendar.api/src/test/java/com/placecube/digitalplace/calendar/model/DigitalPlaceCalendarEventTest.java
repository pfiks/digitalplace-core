package com.placecube.digitalplace.calendar.model;

import static com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarEvent.FORMATTER;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.Test;

public class DigitalPlaceCalendarEventTest {

	@Test
	public void DigitalPlaceCalendarEvent_WhenNew_ThenReturnsCalendarEventObjectWithDataSet() {
		String title = "title";
		String viewURL = "viewURL";
		String backgroundColour = "backgroundColour";
		String textColour = "textColour";
		LocalDateTime startDateTime = LocalDateTime.now();
		LocalDateTime endDateTime = startDateTime.plus(2, ChronoUnit.DAYS);

		DigitalPlaceCalendarEvent digitalPlaceCalendarEvent = new DigitalPlaceCalendarEvent(title, viewURL, backgroundColour, textColour, startDateTime, endDateTime);

		assertThat(digitalPlaceCalendarEvent.getTitle(), equalTo(title));
		assertThat(digitalPlaceCalendarEvent.getViewURL(), equalTo(viewURL));
		assertThat(digitalPlaceCalendarEvent.getBackgroundColour(), equalTo(backgroundColour));
		assertThat(digitalPlaceCalendarEvent.getTextColour(), equalTo(textColour));
		assertThat(digitalPlaceCalendarEvent.getStart(), equalTo(startDateTime.format(FORMATTER)));
		assertThat(digitalPlaceCalendarEvent.getEnd(), equalTo(endDateTime.format(FORMATTER)));
	}

}
