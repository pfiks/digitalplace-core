package com.placecube.digitalplace.addresslookup.dtf.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.dtf.constants.DTFConstants;

@Component(immediate = true, service = ResponseParserService.class)
public class ResponseParserService {

	public AddressContext getAddressContextFromResultSet(ResultSet results) throws SQLException {
		String uprn = results.getString(DTFConstants.FIELD_NAME_UPRN);
		String buildingDetails = getBuildingDetails(results.getString(DTFConstants.FIELD_NAME_SAODESC), results.getInt(DTFConstants.FIELD_NAME_SAONUMBER),
				results.getString(DTFConstants.FIELD_NAME_PAODESC));
		String addressLine1 = getAddressLine1(buildingDetails, results.getString(DTFConstants.FIELD_NAME_PAONUMBER));
		String addressLine2 = getAddressLine2(buildingDetails, results.getString(DTFConstants.FIELD_NAME_PAONUMBER), results.getString(DTFConstants.FIELD_NAME_STREET));
		String addressLine3 = StringPool.BLANK;
		String addressLine4 = StringPool.BLANK;
		String city = results.getString(DTFConstants.FIELD_NAME_POSTTOWN);
		String postcode = results.getString(DTFConstants.FIELD_NAME_POSTCODE);

		return new AddressContext(uprn, addressLine1, addressLine2, addressLine3, addressLine4, city, postcode);
	}

	private String getAddressLine1(String buildingDetails, String buildingNumber) {

		return Validator.isNotNull(buildingDetails) ? buildingDetails : buildingNumber;
	}

	private String getAddressLine2(String buildingDetails, String buildingNumber, String streetName) {
		final String parsedStreetName = GetterUtil.getString(streetName);

		if (Validator.isNotNull(buildingDetails)) {
			return Validator.isNotNull(parsedStreetName) ? buildingNumber + StringPool.SPACE + parsedStreetName : buildingNumber;
		}
		return parsedStreetName;
	}

	private String getBuildingDetails(String saoDesc, int saoNumber, String paoDesc) {

		String buildingDetails = StringPool.BLANK;

		if (Validator.isNotNull(saoDesc)) {
			buildingDetails = buildingDetails + saoDesc;
		} else if (saoNumber > 0) {
			buildingDetails = buildingDetails + saoNumber;
		}

		if (Validator.isNotNull(paoDesc)) {

			if (buildingDetails.length() > 0) {
				buildingDetails = buildingDetails + StringPool.SPACE;
			}

			buildingDetails = buildingDetails + paoDesc;
		}

		return buildingDetails;
	}
}
