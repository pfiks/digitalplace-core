package com.placecube.digitalplace.userimport.admin.web.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;

import org.json.simple.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.entry.rel.service.AssetEntryAssetCategoryRelLocalService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.userimport.admin.web.model.UserImportResult;

@Component(immediate = true, service = UserImportCategoryService.class)
public class UserImportCategoryService {

	private static final Log LOG = LogFactoryUtil.getLog(UserImportCategoryService.class);

	@Reference
	private AssetCategoryLocalService assetCategoryLocalService;

	@Reference
	private AssetEntryAssetCategoryRelLocalService assetEntryAssetCategoryRelLocalService;

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private AssetVocabularyLocalService assetVocabularyLocalService;

	@Reference
	private ClassNameLocalService classNameLocalService;

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private Portal portal;

	public void addCategoriesToUser(JSONObject userToImport, Map<String, String> userVocabularyMappings, User user, UserImportResult userImportResult) throws PortalException {
		long globalGroupId = companyLocalService.getCompany(user.getCompanyId()).getGroupId();
		AssetEntry userEntry = assetEntryLocalService.getEntry(User.class.getName(), user.getUserId());

		userVocabularyMappings.forEach((originalVocabularyName, mappedVocabularyName) -> {
			String categoriesFromFile = GetterUtil.getString(userToImport.get(mappedVocabularyName));
			if (Validator.isBlank(categoriesFromFile)) {
				return;
			}
			Set<String> givenCategoryNames = Arrays.stream(categoriesFromFile.split(StringPool.COMMA)).map(String::trim).collect(Collectors.toSet());

			try {
				AssetVocabulary groupVocabulary = assetVocabularyLocalService.getGroupVocabulary(globalGroupId, originalVocabularyName);
				List<AssetCategory> vocabularyCategories = assetCategoryLocalService.getVocabularyCategories(groupVocabulary.getVocabularyId(), -1, -1, null);
				List<AssetCategory> matchedCategories = vocabularyCategories.stream().filter(c -> givenCategoryNames.contains(c.getName())).collect(Collectors.toList());

				handleMissingCategories(matchedCategories, givenCategoryNames, mappedVocabularyName, userImportResult);
				addMatchedCategoriesToUser(matchedCategories, userEntry);

			} catch (PortalException e) {
				LOG.error(e);
			}

		});
	}

	public List<AssetVocabulary> getGlobalVocabulariesOfUserType(long companyId) throws PortalException {
		long globalGroupId = companyLocalService.getCompany(companyId).getGroupId();

		long userClassNameId = classNameLocalService.getClassNameId(User.class);
		List<AssetVocabulary> groupVocabularies = assetVocabularyLocalService.getGroupVocabularies(globalGroupId);

		return groupVocabularies.stream().filter(v -> v.isAssociatedToClassNameId(userClassNameId)).collect(Collectors.toList());
	}

	public Map<String, String> getUserVocabularyMappings(ActionRequest actionRequest, ServiceContext serviceContext) throws PortalException {
		Map<String, String> vocabularyMappings = new HashMap<>();

		for (AssetVocabulary vocabulary : getGlobalVocabulariesOfUserType(serviceContext.getCompanyId())) {
			String vocabularyMapping = ParamUtil.getString(actionRequest, vocabulary.getName());
			if (Validator.isNotNull(vocabularyMapping)) {
				vocabularyMappings.put(vocabulary.getName(), vocabularyMapping);
			}
		}

		return vocabularyMappings;
	}

	private void addMatchedCategoriesToUser(List<AssetCategory> matchedCategories, AssetEntry userEntry) {
		Set<Long> userCategryIds = matchedCategories.stream().map(AssetCategory::getCategoryId).collect(Collectors.toSet());
		for (Long categoryToApply : userCategryIds) {
			assetEntryAssetCategoryRelLocalService.addAssetEntryAssetCategoryRel(userEntry.getEntryId(), categoryToApply);
		}
	}

	private void handleMissingCategories(List<AssetCategory> matchedCategories, Set<String> givenCategoryNames, String mappedVocabularyName, UserImportResult userImportResult) {
		Set<String> matchedCategoryNames = matchedCategories.stream().map(AssetCategory::getName).collect(Collectors.toSet());
		givenCategoryNames.removeAll(matchedCategoryNames);
		if (!givenCategoryNames.isEmpty()) {
			userImportResult.addExceptionMessage(new Exception("Following categories were not found in vocabulary '" + mappedVocabularyName + "': " + String.join(", ", givenCategoryNames)));
		}
	}

}
