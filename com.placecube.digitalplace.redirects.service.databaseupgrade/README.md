# com.placecube.digitalplace.redirects.service.databaseupgrade

This module is only required on environments where the com.placecube.digitalplace.redirects.service was previously used and has since been replaced by Liferay own implementation (RedirectEntryService API)

The upgrade process iterates through the existing database table, and for each row creates a native Liferay redirect using the RedirectEntryService API.