package com.placecube.digitalplace.group.rest.application.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

public class RestContentEntryTest extends PowerMockito {

	private static final String TITLE = "titleValue";
	private static final long ID = 10;
	private static final String VIEW_URL = "viewURLValue";
	private static final String DESCRIPTION = "descValue";

	@Test
	public void init_WithIdAndTitleParameters_WhenNoError_ThenReturnsEntryWithId() {
		RestContentEntry result = RestContentEntry.init(ID, TITLE);

		assertThat(result.getId(), equalTo(ID));
	}

	@Test
	public void init_WithIdAndTitleParameters_WhenNoError_ThenReturnsEntryWithTitle() {
		RestContentEntry result = RestContentEntry.init(ID, TITLE);

		assertThat(result.getTitle(), equalTo(TITLE));
	}

	@Test
	public void init_WithIdAndTitleParameters_WhenNoError_ThenReturnsEntryWithNoDescription() {
		RestContentEntry result = RestContentEntry.init(ID, TITLE);

		assertThat(result.getDescription(), nullValue());
	}

	@Test
	public void init_WithIdAndTitleParameters_WhenNoError_ThenReturnsEntryWithNoViewURL() {
		RestContentEntry result = RestContentEntry.init(ID, TITLE);

		assertThat(result.getViewURL(), nullValue());
	}

	@Test
	public void init_WithAllParameters_WhenNoError_ThenReturnsEntryWithId() {
		RestContentEntry result = RestContentEntry.init(ID, TITLE, DESCRIPTION, VIEW_URL);

		assertThat(result.getId(), equalTo(ID));
	}

	@Test
	public void init_WithAllParameters_WhenNoError_ThenReturnsEntryWithTitle() {
		RestContentEntry result = RestContentEntry.init(ID, TITLE, DESCRIPTION, VIEW_URL);

		assertThat(result.getTitle(), equalTo(TITLE));
	}

	@Test
	public void init_WithAllParameters_WhenNoError_ThenReturnsEntryWithDescription() {
		RestContentEntry result = RestContentEntry.init(ID, TITLE, DESCRIPTION, VIEW_URL);

		assertThat(result.getDescription(), equalTo(DESCRIPTION));
	}

	@Test
	public void init_WithAllParameters_WhenNoError_ThenReturnsEntryWithViewURL() {
		RestContentEntry result = RestContentEntry.init(ID, TITLE, DESCRIPTION, VIEW_URL);

		assertThat(result.getViewURL(), equalTo(VIEW_URL));
	}

}
