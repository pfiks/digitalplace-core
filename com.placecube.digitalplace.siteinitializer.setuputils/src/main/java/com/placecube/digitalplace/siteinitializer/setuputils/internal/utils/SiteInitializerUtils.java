package com.placecube.digitalplace.siteinitializer.setuputils.internal.utils;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dispatch.model.DispatchTrigger;
import com.liferay.dispatch.service.DispatchTriggerLocalService;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.service.ExpandoValueLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.PortletPreferences;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactory;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.PortletPreferencesLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.UnicodePropertiesBuilder;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

@Component(immediate = true, service = SiteInitializerUtils.class)
public class SiteInitializerUtils {

	private static final Log LOG = LogFactoryUtil.getLog(SiteInitializerUtils.class);

	@Reference
	private DispatchTriggerLocalService dispatchTriggerLocalService;

	@Reference
	private ExpandoValueLocalService expandoValueLocalService;

	@Reference
	private GroupLocalService groupLocalService;

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private PortletPreferencesFactory portletPreferencesFactory;

	@Reference
	private PortletPreferencesLocalService portletPreferencesLocalService;

	@Reference
	private SiteInitialiserSetupContentService siteInitialiserSetupContentService;

	public void addExpandoValues(Layout layout, JSONArray expandoFieldsJSONArray, Map<String, String> placeholdersValues) throws PortalException {
		if (Validator.isNotNull(layout)) {
			for (int i = 0; i < expandoFieldsJSONArray.length(); i++) {
				JSONObject expandoJSONObject = expandoFieldsJSONArray.getJSONObject(i);
				String fieldName = expandoJSONObject.getString("fieldName");
				Object fieldFixedValue = expandoJSONObject.getString("fieldValue");
				int fieldType = expandoJSONObject.getInt("fieldType", ExpandoColumnConstants.STRING);
				Object valueSaved = Validator.isNull(fieldFixedValue) ? placeholdersValues.get(expandoJSONObject.getString("placeholderValue")) : fieldFixedValue;
				Object value = valueSaved;
				if (ExpandoColumnConstants.LONG == fieldType) {
					value = GetterUtil.getLong(valueSaved);
				}
				expandoValueLocalService.addValue(layout.getCompanyId(), Layout.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME, fieldName, layout.getPlid(), value);
			}
		}
	}

	public void configureAdditionalData(long userId, long groupId, JSONObject jsonObject) throws PortalException {
		siteInitialiserSetupContentService.configureAssetListEntryUsage(userId, groupId, jsonObject.getJSONArray("assetListEntryUsagesReplacements"));
	}

	public void configureAdditionalData(long userId, long groupId, Optional<URL> urlToFile) throws IOException, PortalException {
		if (urlToFile.isPresent()) {
			String fileContentUpdated = siteInitialiserSetupContentService.getFileContentWithDefaultReplacements(urlToFile, groupLocalService.getGroup(groupId));
			JSONObject jsonObject = jsonFactory.createJSONObject(fileContentUpdated);
			siteInitialiserSetupContentService.configureAssetListEntryUsage(userId, groupId, jsonObject.getJSONArray("assetListEntryUsagesReplacements"));
		}
	}

	public void createMissingDispatchTrigger(String externalReferenceCode, String taskExecutorType, String name, JSONObject settings, ServiceContext serviceContext) throws PortalException {
		DispatchTrigger dispatchTrigger = dispatchTriggerLocalService.fetchDispatchTriggerByExternalReferenceCode(externalReferenceCode, serviceContext.getCompanyId());
		if (Validator.isNull(dispatchTrigger)) {
			UnicodeProperties settingsProperties = null;
			if (Validator.isNotNull(settings)) {
				settingsProperties = getUnicodePropertiesFromSettingsJSONObject(settings);
			}

			dispatchTriggerLocalService.addDispatchTrigger(externalReferenceCode, serviceContext.getUserId(), taskExecutorType, settingsProperties, name, false);
		}
	}

	public Layout getLayout(ServiceContext serviceContext, JSONObject jsonObject) throws PortalException {
		String friendlyURL = jsonObject.getString("friendlyURL");
		boolean privateLayout = jsonObject.getBoolean("private");

		if (Validator.isNotNull(friendlyURL)) {
			LOG.info("Retrieving layout by url " + friendlyURL);
			return layoutLocalService.getLayoutByFriendlyURL(serviceContext.getScopeGroupId(), privateLayout, friendlyURL);
		}

		String layoutName = jsonObject.getString("layoutName");

		if (Validator.isNotNull(layoutName)) {
			LOG.info("Retrieving layout by name " + layoutName);
			DynamicQuery dynamicQuery = layoutLocalService.dynamicQuery();
			dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", serviceContext.getCompanyId()));
			dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", serviceContext.getScopeGroupId()));
			dynamicQuery.add(RestrictionsFactoryUtil.eq("privateLayout", privateLayout));
			dynamicQuery.add(RestrictionsFactoryUtil.ilike("name", "%" + layoutName + "%"));

			List<Layout> layouts = layoutLocalService.dynamicQuery(dynamicQuery, QueryUtil.ALL_POS, QueryUtil.ALL_POS);

			Optional<Layout> layoutByName = layouts.stream().filter(entry -> entry.getName(entry.getDefaultLanguageId()).equals(layoutName) && !entry.isDraftLayout()).findFirst();
			if (layoutByName.isPresent()) {
				return layoutByName.get();
			}
		}

		throw new PortalException("Unable to find layout to update from jsonObject: " + jsonObject.toJSONString());
	}

	public ServiceContext getServiceContext(User user, long groupId) throws PortalException {
		Group group = groupLocalService.getGroup(groupId);

		ServiceContext serviceContext = (ServiceContext) ServiceContextThreadLocal.getServiceContext().clone();

		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setLanguageId(group.getDefaultLanguageId());
		serviceContext.setScopeGroupId(groupId);
		serviceContext.setTimeZone(user.getTimeZone());
		serviceContext.setUserId(user.getUserId());

		return serviceContext;
	}

	public void publishLayout(Layout layout) throws PortalException {
		if (Validator.isNotNull(layout)) {
			layout = layoutLocalService.getLayout(layout.getPlid());

			layout.setStatus(WorkflowConstants.STATUS_APPROVED);

			UnicodeProperties typeSettingsUnicodeProperties = layout.getTypeSettingsProperties();
			typeSettingsUnicodeProperties.put("published", Boolean.TRUE.toString());

			layout.setTypeSettingsProperties(typeSettingsUnicodeProperties);

			layoutLocalService.updateLayout(layout);
		}
	}

	public void updatePortletPreferencesWithPlaceholders(Layout layout, JSONArray placeholdersToReplaceJSONArray, Map<String, String> placeholdersValues) throws PortalException {
		try {
			if (Validator.isNotNull(layout)) {
				List<PortletPreferences> portletPreferencesByPlid = portletPreferencesLocalService.getPortletPreferencesByPlid(layout.getPlid());

				for (int i = 0; i < placeholdersToReplaceJSONArray.length(); i++) {
					JSONObject prefJSONObject = placeholdersToReplaceJSONArray.getJSONObject(i);
					Optional<PortletPreferences> portletPrefsToUpdate = getPortletPreferencesToUpdate(portletPreferencesByPlid, prefJSONObject);

					if (portletPrefsToUpdate.isPresent()) {
						javax.portlet.PortletPreferences portletSetup = portletPreferencesFactory.getLayoutPortletSetup(layout, portletPrefsToUpdate.get().getPortletId());

						String key = prefJSONObject.getString("preferenceName");
						String multivaluePreferenceValues = prefJSONObject.getString("multivaluePreferenceValues");

						if (Validator.isNotNull(multivaluePreferenceValues)) {
							String[] value = StringUtil.split(multivaluePreferenceValues, ",");
							portletSetup.reset(key);
							portletSetup.setValues(key, value);

						} else {
							String value = placeholdersValues.get(prefJSONObject.getString("placeholderValue"));
							portletSetup.reset(key);
							portletSetup.setValue(key, value);
						}

						portletSetup.store();
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Exception updating portlet prefs for layout", e);
			throw new PortalException(e);
		}
	}

	private Optional<PortletPreferences> getPortletPreferencesToUpdate(List<PortletPreferences> portletPreferencesByPlid, JSONObject prefJSONObject) {
		String portletId = prefJSONObject.getString("portletId");

		Stream<PortletPreferences> portletPrefs = portletPreferencesByPlid.stream().filter(entry -> entry.getPortletId().startsWith(portletId));

		String portletInstanceId = prefJSONObject.getString("portletInstanceId");

		if (Validator.isNull(portletInstanceId)) {
			return portletPrefs.findFirst();
		}

		return portletPrefs.filter(entry -> entry.getPortletId().endsWith(portletInstanceId)).findFirst();
	}

	private UnicodeProperties getUnicodePropertiesFromSettingsJSONObject(JSONObject jsonObject) {
		Map<String, Object> settingsMap = jsonObject.toMap();
		Map<String, String> settingsStringMap = settingsMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> String.valueOf(entry.getValue())));

		return UnicodePropertiesBuilder.create(settingsStringMap, true).build();
	}

}
