package com.placecube.digitalplace.appointment.web.constants;

public final class MVCCommandKeys {

	public static final String VIEW_APPOINTMENTS = "/appointment-dashboard/view-appointments";

	private MVCCommandKeys() {
	}
}