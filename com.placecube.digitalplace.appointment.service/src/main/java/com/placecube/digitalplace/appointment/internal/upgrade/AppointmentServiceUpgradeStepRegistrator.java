package com.placecube.digitalplace.appointment.internal.upgrade;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.appointment.internal.upgrade.steps.Upgrade_1_0_1_AppointmentEntryTableIndexesUpdate;
import com.placecube.digitalplace.appointment.internal.upgrade.steps.Upgrade_1_1_0_AppointmentEntryTableUpdates;
import com.placecube.digitalplace.appointment.internal.upgrade.steps.Upgrade_1_2_0_AppointmentSlotTableCreation;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class AppointmentServiceUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Override
	public void register(Registry registry) {
		registry.register("1.0.0", "1.0.1", new Upgrade_1_0_1_AppointmentEntryTableIndexesUpdate());
		registry.register("1.0.1", "1.1.0", new Upgrade_1_1_0_AppointmentEntryTableUpdates());
		registry.register("1.1.0", "1.2.0", new Upgrade_1_2_0_AppointmentSlotTableCreation());
	}

}