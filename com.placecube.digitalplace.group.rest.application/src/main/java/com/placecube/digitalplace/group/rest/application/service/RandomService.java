package com.placecube.digitalplace.group.rest.application.service;

import java.util.Random;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = RandomService.class)
public class RandomService {

	private static final int ASCII_0 = 48;

	private static final int ASCII_FORWARD_SLASH = 57;

	private static final int ASCII_LOWERCASE_A = 97;

	private static final int ASCII_LOWERCASE_Z = 122;

	private static final int ASCII_UPPERCASE_A = 65;

	private static final int ASCII_UPPERCASE_Z = 90;

	@Reference
	private DLFileEntryLocalService dlFileEntryLocalService;

	public String generateRandomAlphanumericString(int targetStringLength) {

		Random random = new Random();

		return random.ints(ASCII_0, ASCII_LOWERCASE_Z + 1).filter(i -> (i <= ASCII_FORWARD_SLASH || i >= ASCII_UPPERCASE_A) && (i <= ASCII_UPPERCASE_Z || i >= ASCII_LOWERCASE_A))
				.limit(targetStringLength).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

	}

	public String getUniqueFileName(long groupId, long folderId, String fileName) {
		boolean isUnique = Validator.isNull(dlFileEntryLocalService.fetchFileEntry(groupId, folderId, fileName));
		while (!isUnique) {
			String extension = com.liferay.portal.kernel.util.StringUtil.extractLast(fileName, ".");
			String fileNameValue = com.liferay.portal.kernel.util.StringUtil.extractFirst(fileName, "." + extension);
			fileName = fileNameValue.concat("_" + System.currentTimeMillis()).concat("." + extension);
			isUnique = Validator.isNull(dlFileEntryLocalService.fetchFileEntry(groupId, folderId, fileName));
		}
		return fileName;
	}

}
