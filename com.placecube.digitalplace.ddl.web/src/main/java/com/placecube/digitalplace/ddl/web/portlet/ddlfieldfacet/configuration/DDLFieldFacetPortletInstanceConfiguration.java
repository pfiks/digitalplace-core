package com.placecube.digitalplace.ddl.web.portlet.ddlfieldfacet.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.placecube.digitalplace.ddl.web.portlet.ddlfieldfilter.configuration.DDLFieldFacetPortletInstanceConfiguration")
public interface DDLFieldFacetPortletInstanceConfiguration {

	@Meta.AD(required = false)
	boolean alphaSort();

	@Meta.AD(required = false)
	String customHeading();

	@Meta.AD(required = false)
	String ddlRecordLinkFieldName();

	@Meta.AD(required = false)
	long ddmLinkStructureId();

	@Meta.AD(required = false)
	long ddmStructureId();

	@Meta.AD(required = false)
	String ddmFieldName();

	@Meta.AD(required = false, deflt = "CHECKBOX")
	String displayStyle();

	@Meta.AD(required = false)
	String filterColour();

	@Meta.AD(required = false)
	boolean invisible();

	@Meta.AD(required = false)
	boolean isFieldNameDDLRecordLink();

	@Meta.AD(required = false)
	String preselectedValue();

	@Meta.AD(required = false)
	boolean showHeadingAsFirstDropdownOption();

}
