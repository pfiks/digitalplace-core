package com.placecube.digitalplace.search.internal.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;

public class SharedSearchSession implements Serializable {

	private static final long serialVersionUID = 1L;

	private final List<SharedActiveFilter> activeFilters;
	private final Map<String, Set<String>> multiAttributeValues;
	private final long plid;
	private final Map<String, String> singleAttributeValues;

	public static SharedSearchSession init(long plid) {
		return new SharedSearchSession(plid);
	}

	private SharedSearchSession(long plid) {
		this.plid = plid;
		singleAttributeValues = new HashMap<>();
		multiAttributeValues = new HashMap<>();
		activeFilters = new ArrayList<>();
	}

	public void addActiveFilter(String filterName, String filterValue, String filterLabel, String filterColour) {
		if (Validator.isNotNull(filterName) && Validator.isNotNull(filterValue) && Validator.isNotNull(filterLabel) && filterForValueNotAlreadySet(filterName, filterValue)) {
			activeFilters.add(new SharedActiveFilter(filterName, filterValue, filterLabel, filterColour));
		}
	}

	public void addSingleValuedAttribute(String attributeName, String attributeValue) {
		if (Validator.isNotNull(attributeName) && Validator.isNotNull(attributeValue)) {
			singleAttributeValues.put(attributeName, attributeValue);
		}
	}

	public List<SharedActiveFilter> getActiveFilters() {
		return activeFilters;
	}

	public long getPlid() {
		return plid;
	}

	public String getSingleValuedAttribute(String attributeName) {
		return singleAttributeValues.getOrDefault(attributeName, StringPool.BLANK);
	}

	public void removeActiveFilter(String filterName) {
		activeFilters.removeIf(f -> f.getName().equals(filterName));
	}

	public void removeActiveFilterValue(String filterName, String filterValue) {
		activeFilters.removeIf(f -> f.getName().equals(filterName) && f.getValue().equals(filterValue));
	}

	public void removeAttribute(String attributeName) {
		singleAttributeValues.remove(attributeName);
		multiAttributeValues.remove(attributeName);
		removeActiveFilter(attributeName);
	}

	public void removeSingleValuedAttribute(String attributeName) {
		singleAttributeValues.remove(attributeName);
	}

	private boolean filterForValueNotAlreadySet(String filterName, String filterValue) {
		return !activeFilters.stream().filter(activeFilter -> activeFilter.getName().equals(filterName) && activeFilter.getValue().equals(filterValue)).findAny().isPresent();
	}

}
