package com.placecube.digitalplace.search.web.portlet.activefilters;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class ActiveFiltersUpdateMVCActionCommandTest extends PowerMockito {

	private static final String FILTER_NAME = "filterNameVal";
	private static final String FILTER_VALUE = "filterValueVal";
	private static final String FILTER_LABEL = "filterLabel";
	private static final String FILTER_COLOUR = "filterColour";

	@InjectMocks
	private ActiveFiltersUpdateMVCActionCommand activeFiltersUpdateMVCActionCommand;

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Before
	public void activateSetup() {
		mockStatic(ParamUtil.class);
	}

	@Test
	public void doProcessAction_WhenCommandIsAdd_ThenAddsActiveFilterToSessionAndSendsTheRedirect() throws Exception {
		when(ParamUtil.getString(mockActionRequest, "filterName")).thenReturn(FILTER_NAME);
		when(ParamUtil.getString(mockActionRequest, "filterValue")).thenReturn(FILTER_VALUE);
		when(ParamUtil.getString(mockActionRequest, "filterLabel")).thenReturn(FILTER_LABEL);
		when(ParamUtil.getString(mockActionRequest, "filterColour")).thenReturn(FILTER_COLOUR);
		when(ParamUtil.getString(mockActionRequest, "cmd")).thenReturn("add");

		activeFiltersUpdateMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).addSharedActiveFilter(mockActionRequest, FILTER_NAME, FILTER_VALUE, FILTER_LABEL, FILTER_COLOUR);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

	@Test
	public void doProcessAction_WhenCommandIsClearAll_ThenRemovesAllTheSharedSearchSessionAttributesAndSendsTheRedirect() throws Exception {
		when(ParamUtil.getString(mockActionRequest, "filterName")).thenReturn(FILTER_NAME);
		when(ParamUtil.getString(mockActionRequest, "filterValue")).thenReturn(FILTER_VALUE);
		when(ParamUtil.getString(mockActionRequest, "cmd")).thenReturn("clearAll");

		activeFiltersUpdateMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedSearchSessionAttribute(mockActionRequest, FILTER_NAME);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

	@Test
	public void doProcessAction_WhenCommandIsNotManaged_ThenOnlyPerformsTheRedirect() throws Exception {
		when(ParamUtil.getString(mockActionRequest, "filterName")).thenReturn(FILTER_NAME);
		when(ParamUtil.getString(mockActionRequest, "filterValue")).thenReturn(FILTER_VALUE);
		when(ParamUtil.getString(mockActionRequest, "cmd")).thenReturn("somethingElse");

		activeFiltersUpdateMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
		verify(mockSharedSearchRequestService, never()).removeSharedSearchSessionAttribute(any(ActionRequest.class), anyString());
		verify(mockSharedSearchRequestService, never()).addSharedActiveFilter(any(ActionRequest.class), anyString(), anyString(), anyString(), anyString());
		verify(mockSharedSearchRequestService, never()).removeSharedActiveFilterValue(any(ActionRequest.class), anyString(), anyString());
	}

	@Test
	public void doProcessAction_WhenCommandIsRemove_ThenRemoveTheActiveFilterFromSessionAndSendsTheRedirect() throws Exception {
		when(ParamUtil.getString(mockActionRequest, "filterName")).thenReturn(FILTER_NAME);
		when(ParamUtil.getString(mockActionRequest, "filterValue")).thenReturn(FILTER_VALUE);
		when(ParamUtil.getString(mockActionRequest, "cmd")).thenReturn("remove");

		activeFiltersUpdateMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		InOrder inOrder = inOrder(mockSharedSearchRequestService);
		inOrder.verify(mockSharedSearchRequestService, times(1)).removeSharedActiveFilterValue(mockActionRequest, FILTER_NAME, FILTER_VALUE);
		inOrder.verify(mockSharedSearchRequestService, times(1)).sendRedirectFromParameter(mockActionRequest, mockActionResponse, "redirectURL");
	}

}
