package com.placecube.digitalplace.search.web.portlet.searchbar;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.portlet.searchbar.configuration.SearchBarPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.searchbar.service.SearchBarService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class SearchBarPortletTest extends PowerMockito {

	@Mock
	private SearchBarPortletInstanceConfiguration mockConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private Layout mockLayout;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private SearchBarService mockSearchBarService;

	@Mock
	private SharedSearchRequestService mockSharedSearchRequestService;

	@Mock
	private SharedSearchResponse mockSharedSearchResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private SearchBarPortlet searchBarPortlet;

	@Before
	public void activateSetup() throws Exception {
		suppress(methods(MVCPortlet.class, "render"));
		initMocks(this);

		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockConfigurationProvider.getPortletInstanceConfiguration(SearchBarPortletInstanceConfiguration.class, mockThemeDisplay)).thenReturn(mockConfiguration);
	}

	@Test
	public void render_WhenDestinationPageSetAndLayoutFoundAndPortletIdFound_ThenSearchIsNotExecutedAndAttributesSetInRequest() throws Exception {
		long plid = 123;
		when(mockConfiguration.destinationPage()).thenReturn("fakeUrl");
		when(mockSearchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration)).thenReturn(Optional.of(mockLayout));
		when(mockSearchBarService.getDestinationSearchBarPortletIdOnLayout(mockLayout)).thenReturn("myPortletId");
		when(mockLayout.getPlid()).thenReturn(plid);

		searchBarPortlet.render(mockRenderRequest, mockRenderResponse);

		verifyNoInteractions(mockSharedSearchRequestService);
		verify(mockRenderRequest, times(1)).setAttribute("destinationPlid", plid);
		verify(mockRenderRequest, times(1)).setAttribute("destinationSearchBarPortletId", "myPortletId");
		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", false);
	}

	@Test
	public void render_WhenDestinationPageSetAndLayoutFoundAndPortletIdNotFound_ThenSearchIsNotExecutedAndSetsInvalidConfigurationToTrue() throws Exception {
		long plid = 123;
		when(mockConfiguration.destinationPage()).thenReturn("fakeUrl");
		when(mockSearchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration)).thenReturn(Optional.of(mockLayout));
		when(mockSearchBarService.getDestinationSearchBarPortletIdOnLayout(mockLayout)).thenReturn("");
		when(mockLayout.getPlid()).thenReturn(plid);

		searchBarPortlet.render(mockRenderRequest, mockRenderResponse);

		verifyNoInteractions(mockSharedSearchRequestService);
		verify(mockRenderRequest, never()).setAttribute(eq("destinationPlid"), anyLong());
		verify(mockRenderRequest, never()).setAttribute(eq("destinationSearchBarPortletId"), anyString());
		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
	}

	@Test
	public void render_WhenDestinationPageSetAndLayoutNotFound_ThenSearchIsNotExecutedAndSetsInvalidConfigurationToTrue() throws Exception {
		when(mockConfiguration.destinationPage()).thenReturn("fakeUrl");
		when(mockSearchBarService.getDestinationLayout(mockThemeDisplay, mockConfiguration)).thenReturn(Optional.empty());

		searchBarPortlet.render(mockRenderRequest, mockRenderResponse);

		verifyNoInteractions(mockSharedSearchRequestService);
		verify(mockRenderRequest, never()).setAttribute(eq("destinationPlid"), anyLong());
		verify(mockRenderRequest, never()).setAttribute(eq("destinationSearchBarPortletId"), anyString());
		verify(mockRenderRequest, times(1)).setAttribute("invalidConfiguration", true);
	}

	@Test(expected = PortletException.class)
	public void render_WhenNoDestinationPageSetAndExceptionExecutingSearch_ThenThrowsPortletException() throws Exception {
		when(mockConfiguration.destinationPage()).thenReturn(StringPool.BLANK);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenThrow(new SearchException());

		searchBarPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoDestinationPageSetAndNoError_ThenExecutesTheSearchAndSetsResultsAttributes() throws Exception {
		String iteratorUrl = "expectedVal";
		String keywords = "expectedVal";
		int totals = 123;
		when(mockConfiguration.destinationPage()).thenReturn(StringPool.BLANK);
		when(mockSharedSearchRequestService.search(mockRenderRequest)).thenReturn(mockSharedSearchResponse);
		when(mockSharedSearchResponse.getKeywords()).thenReturn(keywords);
		when(mockSharedSearchResponse.getEscapedSearchResultsIteratorURL()).thenReturn(iteratorUrl);
		when(mockSharedSearchResponse.getTotalResults()).thenReturn(totals);

		searchBarPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("redirectURL", iteratorUrl);
		verify(mockRenderRequest, times(1)).setAttribute("keywords", keywords);
		verify(mockRenderRequest, times(1)).setAttribute("totalResults", totals);
		verify(mockRenderRequest, never()).setAttribute(eq("invalidConfiguration"), anyBoolean());
	}

}
