package com.placecube.digitalplace.appointment.admin.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.appointment.admin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.appointment.admin.web.constants.PortletKeys;
import com.placecube.digitalplace.appointment.admin.web.model.AppointmentSlotForm;
import com.placecube.digitalplace.appointment.admin.web.service.AppointmentsManagerService;
import com.placecube.digitalplace.appointment.booking.constants.AppointmentSlotType;
import com.placecube.digitalplace.appointment.service.AppointmentSlotLocalService;

@Component(immediate = true, property = { //
		"javax.portlet.name=" + PortletKeys.APPOINTMENTS_MANAGER, //
		"mvc.command.name=/", "mvc.command.name=" + MVCCommandKeys.VIEW_ALL_SLOTS //
}, service = MVCRenderCommand.class)
public class AppointmentSlotListMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AppointmentSlotLocalService appointmentSlotLocalService;

	@Reference
	private AppointmentsManagerService appointmentsManagerService;

	@Reference
	private Portal portal;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		HttpServletRequest httpServletRequest = portal.getHttpServletRequest(renderRequest);

		long companyId = portal.getCompanyId(httpServletRequest);
		String serviceId = ParamUtil.getString(renderRequest, "serviceId", StringPool.BLANK);

		if (Validator.isNotNull(serviceId)) {
			renderRequest.setAttribute("searchContainerSlotConfiguration", getSearchContainer(renderRequest, renderResponse, companyId, AppointmentSlotType.CONFIGURATION, serviceId));
			renderRequest.setAttribute("searchContainerSlotExclusion", getSearchContainer(renderRequest, renderResponse, companyId, AppointmentSlotType.EXCLUSION, serviceId));
		}

		renderRequest.setAttribute("availableServiceIds", appointmentsManagerService.getAvailableServiceIds(companyId));
		renderRequest.setAttribute("serviceId", serviceId);

		return "/view-slots.jsp";
	}

	private SearchContainer<AppointmentSlotForm> getSearchContainer(RenderRequest renderRequest, RenderResponse renderResponse, long companyId, AppointmentSlotType slotType, String serviceId) {
		SearchContainer<AppointmentSlotForm> searchContainer = appointmentsManagerService.getSearchContainer(renderRequest, renderResponse, slotType.name());
		searchContainer.setResultsAndTotal(
				() -> appointmentsManagerService.convertToAppointmentSlotForm(
						appointmentSlotLocalService.getAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(companyId, slotType.name(), serviceId, searchContainer.getStart(), searchContainer.getEnd())),
				appointmentSlotLocalService.countAppointmentSlotsByCompanyIdAndSlotTypeAndServiceId(companyId, slotType.name(), serviceId));
		return searchContainer;
	}

}
