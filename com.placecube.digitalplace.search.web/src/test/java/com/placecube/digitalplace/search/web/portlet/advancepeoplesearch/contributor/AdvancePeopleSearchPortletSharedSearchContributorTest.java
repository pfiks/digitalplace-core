package com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.contributor;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.search.shared.model.SharedActiveFilter;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.model.UserSearchField;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service.AdvancePeopleSearchConfigurationService;
import com.placecube.digitalplace.search.web.portlet.advancepeoplesearch.service.AdvancePeopleSearchQueryHelper;
import com.placecube.digitalplace.search.web.service.ObjectFactoryUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ObjectFactoryUtil.class })
public class AdvancePeopleSearchPortletSharedSearchContributorTest extends PowerMockito {

	private static final Locale LOCALE = Locale.UK;

	@InjectMocks
	AdvancePeopleSearchPortletSharedSearchContributor advancePeopleSearchPortletSharedSearchContributor;

	@Mock
	private AdvancePeopleSearchConfigurationService mockAdvancePeopleSearchConfigurationService;

	@Mock
	private AdvancePeopleSearchQueryHelper advancePeopleSearchQueryHelper;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Mock
	private ThemeDisplay mockThemeDisplay;
	@Mock
	private BooleanQuery mockBooleanClause;
	@Mock
	private BooleanQuery mockBooleanQuery;

	@Before
	public void setup() {
		mockStatic(ObjectFactoryUtil.class);
		initMocks(this);
	}

	@Test
	public void contribute_WhenPortletPrefsFoundAndDoNotApplyPrivacy_ThenAddsSearchFiltersAndNotPrivacyFilter() throws ParseException {
		long companyId = 123;
		String configuredSearchFields = "[]";
		List<String> searchUserIds = Arrays.asList("0", "111");
		List<UserSearchField> searchFields = new ArrayList<>();
		searchFields.add(new UserSearchField("field1", "label1"));
		searchFields.add(new UserSearchField("field2", "label2"));
		List<SharedActiveFilter> sharedActiveFilters = new ArrayList<>();

		for (UserSearchField userSearchField : searchFields) {
			sharedActiveFilters.add(new SharedActiveFilter(userSearchField.getField(), "value", userSearchField.getLabel(), "filterColor"));
		}

		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockPortletPreferences.getValue("searchFields", StringPool.BLANK)).thenReturn(configuredSearchFields);
		when(mockAdvancePeopleSearchConfigurationService.getSearchFields(companyId, LOCALE, configuredSearchFields)).thenReturn(searchFields);
		when(mockSharedSearchContributorSettings.getThemeDisplay().getUserId()).thenReturn(111L);
		when(ObjectFactoryUtil.createBooleanQuery()).thenReturn(mockBooleanQuery);

		for (UserSearchField userSearchField : searchFields) {
			List<SharedActiveFilter> activeFilters = sharedActiveFilters.stream().filter(filter -> filter.getName().equals(userSearchField.getField())).collect(Collectors.toList());
			when(mockSharedSearchContributorSettings.getActiveFiltersForField(userSearchField.getField())).thenReturn(activeFilters);
		}

		advancePeopleSearchPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		for (UserSearchField userSearchField : searchFields) {
			for (SharedActiveFilter activeFilter : sharedActiveFilters) {
				if (userSearchField.getField().equals(activeFilter.getName())) {
					String field = userSearchField.getField();
					InOrder inOrder = inOrder(advancePeopleSearchQueryHelper, mockSharedSearchContributorSettings);
					inOrder.verify(advancePeopleSearchQueryHelper, times(1)).getValueAsTermsQuery(field, activeFilter.getValue());
					inOrder.verify(advancePeopleSearchQueryHelper, times(1)).getValueAsExactMatchQuery(field, activeFilter.getValue());
					inOrder.verify(advancePeopleSearchQueryHelper, times(1)).createBooleanClause(mockBooleanQuery, BooleanClauseOccur.MUST);

					inOrder.verify(mockSharedSearchContributorSettings, never()).addBooleanQueryForMultipleValues("privacy_" + field, searchUserIds, BooleanClauseOccur.MUST);

				}
			}
		}
	}

	@Test
	public void contribute_WhenPortletPrefsFoundAndApplyPrivacy_ThenAddsSearchFiltersAndPrivacyFilters() throws ParseException {
		long companyId = 123;
		List<String> searchUserIds = Arrays.asList("0", "111");
		String configuredSearchFields = "[]";
		List<UserSearchField> searchFields = new ArrayList<>();
		searchFields.add(new UserSearchField("field1", "label1"));
		searchFields.add(new UserSearchField("field2", "label2"));
		searchFields.add(new UserSearchField("biography", "biography"));
		List<SharedActiveFilter> sharedActiveFilters = new ArrayList<>();

		for (UserSearchField userSearchField : searchFields) {
			sharedActiveFilters.add(new SharedActiveFilter(userSearchField.getField(), "value", userSearchField.getLabel(), "filterColor"));
		}

		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockPortletPreferences.getValue("searchFields", StringPool.BLANK)).thenReturn(configuredSearchFields);
		when(mockPortletPreferences.getValue("applyPrivacy", StringPool.FALSE)).thenReturn(StringPool.TRUE);
		when(mockAdvancePeopleSearchConfigurationService.getSearchFields(companyId, LOCALE, configuredSearchFields)).thenReturn(searchFields);
		when(mockSharedSearchContributorSettings.getThemeDisplay().getUserId()).thenReturn(111L);
		when(ObjectFactoryUtil.createBooleanQuery()).thenReturn(mockBooleanQuery);

		for (UserSearchField userSearchField : searchFields) {
			List<SharedActiveFilter> activeFilters = sharedActiveFilters.stream().filter(filter -> filter.getName().equals(userSearchField.getField())).collect(Collectors.toList());
			when(mockSharedSearchContributorSettings.getActiveFiltersForField(userSearchField.getField())).thenReturn(activeFilters);
		}

		advancePeopleSearchPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		for (UserSearchField userSearchField : searchFields) {
			for (SharedActiveFilter activeFilter : sharedActiveFilters) {
				String field = userSearchField.getField();
				if (field.equals(activeFilter.getName())) {

					if ("biography".equals(field)) {
						InOrder inOrder = inOrder(advancePeopleSearchQueryHelper, mockSharedSearchContributorSettings);
						inOrder.verify(advancePeopleSearchQueryHelper, times(1)).getValueAsTermsQuery("description", activeFilter.getValue());
						inOrder.verify(advancePeopleSearchQueryHelper, times(1)).getValueAsExactMatchQuery("description", activeFilter.getValue());
						inOrder.verify(advancePeopleSearchQueryHelper, times(1)).createBooleanClause(mockBooleanQuery, BooleanClauseOccur.MUST);
						inOrder.verify(mockSharedSearchContributorSettings).addBooleanQueryForMultipleValues("privacy_description", searchUserIds, BooleanClauseOccur.MUST);

						inOrder.verify(advancePeopleSearchQueryHelper, times(1)).getValueAsTermsQuery("content", activeFilter.getValue());
						inOrder.verify(advancePeopleSearchQueryHelper, times(1)).getValueAsExactMatchQuery("content", activeFilter.getValue());
						inOrder.verify(advancePeopleSearchQueryHelper, times(1)).createBooleanClause(mockBooleanQuery, BooleanClauseOccur.MUST);
						inOrder.verify(mockSharedSearchContributorSettings).addBooleanQueryForMultipleValues("privacy_content", searchUserIds, BooleanClauseOccur.MUST);
					} else {
						InOrder inOrder = inOrder(advancePeopleSearchQueryHelper, mockSharedSearchContributorSettings);
						inOrder.verify(advancePeopleSearchQueryHelper, times(1)).getValueAsTermsQuery(field, activeFilter.getValue());
						inOrder.verify(advancePeopleSearchQueryHelper, times(1)).getValueAsExactMatchQuery(field, activeFilter.getValue());
						inOrder.verify(advancePeopleSearchQueryHelper, times(1)).createBooleanClause(mockBooleanQuery, BooleanClauseOccur.MUST);

						inOrder.verify(mockSharedSearchContributorSettings).addBooleanQueryForMultipleValues("privacy_" + field, searchUserIds, BooleanClauseOccur.MUST);
					}
				}
			}
		}
	}

	@Test
	public void contribute_WhenPortletPrefsFoundAndFieldNameIsBlank_ThenNoQueriesAddedToSearchContributorSettings() throws ParseException {
		long companyId = 123;
		String configuredSearchFields = "[]";
		List<UserSearchField> searchFields = new ArrayList<>();
		searchFields.add(new UserSearchField(StringPool.BLANK, "label1"));
		List<SharedActiveFilter> sharedActiveFilters = new ArrayList<>();

		for (UserSearchField userSearchField : searchFields) {
			sharedActiveFilters.add(new SharedActiveFilter(userSearchField.getField(), "value", userSearchField.getLabel(), "filterColor"));
		}

		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockPortletPreferences.getValue("searchFields", StringPool.BLANK)).thenReturn(configuredSearchFields);
		when(mockAdvancePeopleSearchConfigurationService.getSearchFields(companyId, LOCALE, configuredSearchFields)).thenReturn(searchFields);
		when(mockSharedSearchContributorSettings.getThemeDisplay().getUserId()).thenReturn(111L);
		when(ObjectFactoryUtil.createBooleanQuery()).thenReturn(mockBooleanQuery);

		for (UserSearchField userSearchField : searchFields) {
			List<SharedActiveFilter> activeFilters = sharedActiveFilters.stream().filter(filter -> filter.getName().equals(userSearchField.getField())).collect(Collectors.toList());
			when(mockSharedSearchContributorSettings.getActiveFiltersForField(userSearchField.getField())).thenReturn(activeFilters);
		}

		advancePeopleSearchPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		for (UserSearchField userSearchField : searchFields) {
			for (SharedActiveFilter activeFilter : sharedActiveFilters) {
				String field = userSearchField.getField();
				verify(advancePeopleSearchQueryHelper, never()).getValueAsTermsQuery(field, activeFilter.getValue());
				verify(advancePeopleSearchQueryHelper, never()).getValueAsExactMatchQuery(field, activeFilter.getValue());
				verify(advancePeopleSearchQueryHelper, never()).createBooleanClause(mockBooleanQuery, BooleanClauseOccur.MUST);
			}
		}

	}

	@Test
	public void contribute_WhenPortletPrefsFoundAndFieldValueIsBlank_ThenNoQueriesAddedToSearchContributorSettings() throws ParseException {
		long companyId = 123;
		String configuredSearchFields = "[]";
		List<UserSearchField> searchFields = new ArrayList<>();
		searchFields.add(new UserSearchField("fieldname1", StringPool.BLANK));
		List<SharedActiveFilter> sharedActiveFilters = new ArrayList<>();

		for (UserSearchField userSearchField : searchFields) {
			sharedActiveFilters.add(new SharedActiveFilter(userSearchField.getField(), StringPool.BLANK, userSearchField.getLabel(), "filterColor"));
		}

		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.of(mockPortletPreferences));
		when(mockSharedSearchContributorSettings.getThemeDisplay()).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getCompanyId()).thenReturn(companyId);
		when(mockThemeDisplay.getLocale()).thenReturn(Locale.UK);
		when(mockPortletPreferences.getValue("searchFields", "label")).thenReturn(configuredSearchFields);
		when(mockAdvancePeopleSearchConfigurationService.getSearchFields(companyId, LOCALE, configuredSearchFields)).thenReturn(searchFields);
		when(mockSharedSearchContributorSettings.getThemeDisplay().getUserId()).thenReturn(111L);
		when(ObjectFactoryUtil.createBooleanQuery()).thenReturn(mockBooleanQuery);

		for (UserSearchField userSearchField : searchFields) {
			List<SharedActiveFilter> activeFilters = sharedActiveFilters.stream().filter(filter -> filter.getName().equals(userSearchField.getField())).collect(Collectors.toList());
			when(mockSharedSearchContributorSettings.getActiveFiltersForField(userSearchField.getField())).thenReturn(activeFilters);
		}

		advancePeopleSearchPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);

		for (UserSearchField userSearchField : searchFields) {
			for (SharedActiveFilter activeFilter : sharedActiveFilters) {
				String field = userSearchField.getField();
				verify(advancePeopleSearchQueryHelper, never()).getValueAsTermsQuery(field, activeFilter.getValue());
				verify(advancePeopleSearchQueryHelper, never()).getValueAsExactMatchQuery(field, activeFilter.getValue());
				verify(advancePeopleSearchQueryHelper, never()).createBooleanClause(mockBooleanQuery, BooleanClauseOccur.MUST);
			}
		}

	}

	@Test(expected = SystemException.class)
	public void contribute_WhenNoPortletPrefsFound_ThenThrowsSystemException() {
		when(mockSharedSearchContributorSettings.getPortletPreferences()).thenReturn(Optional.empty());

		advancePeopleSearchPortletSharedSearchContributor.contribute(mockSharedSearchContributorSettings);
	}

}
