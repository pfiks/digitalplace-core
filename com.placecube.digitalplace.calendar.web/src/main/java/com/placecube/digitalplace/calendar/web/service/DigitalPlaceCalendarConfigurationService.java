package com.placecube.digitalplace.calendar.web.service;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.placecube.digitalplace.calendar.web.portlet.configuration.DigitalPlaceCalendarInstanceConfiguration;

@Component(immediate = true, service = DigitalPlaceCalendarConfigurationService.class)
public class DigitalPlaceCalendarConfigurationService {

	@Reference
	private ConfigurationProvider configurationProvider;

	public DigitalPlaceCalendarInstanceConfiguration getDigitalPlaceCalendarConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		return configurationProvider.getPortletInstanceConfiguration(DigitalPlaceCalendarInstanceConfiguration.class, themeDisplay);
	}

}
