package com.placecube.digitalplace.search.sortby.mostactive.indexer;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.search.spi.model.index.contributor.ModelDocumentContributor;
import com.placecube.digitalplace.search.sortby.mostactive.constants.MostActiveConstants;
import com.placecube.digitalplace.search.sortby.mostactive.service.MostActiveService;

@Component(immediate = true, property = "indexer.class.name=com.liferay.portal.kernel.model.Group", service = ModelDocumentContributor.class)
public class MostActiveGroupModelDocumentContributor implements ModelDocumentContributor<Group> {

	private static final Log LOG = LogFactoryUtil.getLog(MostActiveGroupModelDocumentContributor.class);

	@Reference
	private MostActiveService mostActiveService;

	@Override
	public void contribute(Document document, Group group) {
		if (!group.isOrganization()) {
			document.addKeyword(MostActiveConstants.COUNTER_FIELD_NAME, mostActiveService.getActivitiesCount(group));

			LOG.debug("Indexing groupId: " + group.getGroupId() + " with sort by most active field - " + document.toString());
		}
	}

}