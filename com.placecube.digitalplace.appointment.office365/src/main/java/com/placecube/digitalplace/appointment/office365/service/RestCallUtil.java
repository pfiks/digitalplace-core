package com.placecube.digitalplace.appointment.office365.service;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

@Component(immediate = true, service = RestCallUtil.class)
public class RestCallUtil {

	private static final Log LOG = LogFactoryUtil.getLog(RestCallUtil.class);

	public String executeGetCall(String url, String authenticationBearer) throws PortalException {
		try {
			LOG.debug("Executing call to: " + url);

			HttpGet method = new HttpGet(url);
			method.addHeader("Authorization", "Bearer " + authenticationBearer);
			method.addHeader("Content-Type", "application/json");

			return executeCall(method);
		} catch (Exception e) {
			throw new PortalException("Unable to execute get call to : " + url, e);
		}
	}

	public String executePostCallWithAuthenticationBearer(String url, String requestBody, String authenticationBearer) throws PortalException {
		try {
			LOG.debug("Executing call to: " + url + ", with requestBody: " + requestBody);

			HttpPost method = getBaseHttpPost(url, requestBody);
			method.addHeader("Authorization", "Bearer " + authenticationBearer);
			method.addHeader("Content-Type", "application/json");

			return executeCall(method);

		} catch (Exception e) {
			throw new PortalException("Unable to execute post call to : " + url, e);
		}
	}

	public String executePostCallWithFormEncodedBody(String url, String requestBody) throws PortalException {
		try {
			LOG.debug("Executing call to: " + url + ", with requestBody: " + requestBody);

			HttpPost method = getBaseHttpPost(url, requestBody);
			method.addHeader("Content-Type", "application/x-www-form-urlencoded");

			return executeCall(method);
		} catch (Exception e) {
			throw new PortalException("Unable to execute post call to : " + url, e);
		}
	}

	private String executeCall(HttpUriRequest method) throws ClientProtocolException, IOException {

		HttpClient client = HttpClients.createDefault();
		HttpResponse response = client.execute(method);

		int statusCode = response.getStatusLine().getStatusCode();
		String result = EntityUtils.toString(response.getEntity());
		LOG.debug("Response code: " + statusCode);
		LOG.debug("Response entity: " + result);

		boolean success = statusCode >= 200 && statusCode < 300;

		if (!success) {
			LOG.error("Error with code: " + statusCode + ". Response: " + response);
			throw new IOException();
		}
		return result;
	}

	private HttpPost getBaseHttpPost(String url, String requestBody) {
		HttpPost method = new HttpPost(url);
		method.addHeader("Accept", "application/json");
		method.setEntity(new StringEntity(requestBody, ContentType.create("application/json")));
		return method;
	}
}
