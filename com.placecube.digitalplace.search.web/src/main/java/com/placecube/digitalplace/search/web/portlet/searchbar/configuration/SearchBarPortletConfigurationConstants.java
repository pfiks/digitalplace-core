package com.placecube.digitalplace.search.web.portlet.searchbar.configuration;

public final class SearchBarPortletConfigurationConstants {

	public static final String DESTINATION_PAGE = "destinationPage";

	public static final String SEARCH_GUEST_GROUP = "searchGuestGroup";

	private SearchBarPortletConfigurationConstants() {
	}
}