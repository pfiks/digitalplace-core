package com.placecube.digitalplace.companyinitializeradmin.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

import java.lang.reflect.Method;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.placecube.digitalplace.companyinitializeradmin.constants.PortletRequestKeys;
import com.placecube.digitalplace.companyinitializeradmin.initializer.internal.service.CompanyInitializerRegistry;
import com.placecube.digitalplace.initialiser.CompanyInitializer;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class CompanyInitializerAdminPortletTest {

	private CompanyInitializerAdminPortlet companyInitializerAdminPortlet;

	@Mock
	private CompanyInitializerRegistry mockCompanyInitializerRegistry;

	@Mock
	private List<CompanyInitializer> mockCompanyInitializers;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Test
	public void render_WhenNoError_ThenSetsActiveCompanyInitializersAsRequestAttribute() throws Exception {
		mockCallToSuper();
		when(mockCompanyInitializerRegistry.getCompanyInitializers()).thenReturn(mockCompanyInitializers);

		companyInitializerAdminPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.COMPANY_INITIALIZERS, mockCompanyInitializers);
	}

	@Before
	public void setUp() {
		initMocks(this);

		companyInitializerAdminPortlet = new CompanyInitializerAdminPortlet();
		companyInitializerAdminPortlet.companyInitializerRegistry = mockCompanyInitializerRegistry;
	}

	@SuppressWarnings("rawtypes")
	private void mockCallToSuper() throws Exception {
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

}
