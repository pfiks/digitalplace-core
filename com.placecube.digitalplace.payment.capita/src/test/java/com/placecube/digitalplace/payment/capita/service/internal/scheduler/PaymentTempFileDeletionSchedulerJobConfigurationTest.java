package com.placecube.digitalplace.payment.capita.service.internal.scheduler;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Collections;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.placecube.digitalplace.payment.capita.service.internal.constants.OneOffPaymentOrderConstants;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ RestrictionsFactoryUtil.class, ProjectionFactoryUtil.class, PropertyFactoryUtil.class, CompanyThreadLocal.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.security.auth.CompanyThreadLocal")
public class PaymentTempFileDeletionSchedulerJobConfigurationTest extends PowerMockito {

	private static final long COMPANY_ID = 1L;

	@Mock
	private Criterion mockCriterion1;

	@Mock
	private Criterion mockCriterion2;

	@Mock
	private Criterion mockdateCriterion;

	@Mock
	private Criterion mockCompanyIdCriterion;

	@Mock
	private DLFileEntryLocalService mockDLFileEntryLocalService;

	@Mock
	private DLFolderLocalService mockDLFolderLocalService;

	@Mock
	private DynamicQuery mockDynamicQuery1;

	@Mock
	private DynamicQuery mockDynamicQuery2;

	@Mock
	private DynamicQuery mockDynamicQuery3;

	@Mock
	private DynamicQuery mockDynamicQuery4;

	@Mock
	private DLFileEntry mockFileEntry;

	@Mock
	private Criterion mockfolderIdCriterion;

	@Mock
	private Property mockProperty;

	@InjectMocks
	private PaymentTempFileDeletionSchedulerJobConfiguration paymentTempFileDeletionSchedulerJobConfiguration;

	@Before
	public void activateSetup() {
		initMocks(this);
		mockStatic(RestrictionsFactoryUtil.class, ProjectionFactoryUtil.class, PropertyFactoryUtil.class, CompanyThreadLocal.class);
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenNoTempFilesFolder_ThenDoNothing() throws Exception {
		when(mockDLFolderLocalService.dynamicQuery()).thenReturn(mockDynamicQuery1);
		when(RestrictionsFactoryUtil.eq(Field.NAME, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME)).thenReturn(mockCriterion1);
		when(RestrictionsFactoryUtil.eq(Field.COMPANY_ID, COMPANY_ID)).thenReturn(mockCompanyIdCriterion);
		when(PropertyFactoryUtil.forName("folderId")).thenReturn(mockProperty);

		when(mockDynamicQuery1.setProjection(mockProperty)).thenReturn(mockDynamicQuery2);
		when(mockDynamicQuery2.add(mockCriterion1)).thenReturn(mockDynamicQuery3);
		when(mockDynamicQuery3.add(mockCompanyIdCriterion)).thenReturn(mockDynamicQuery4);

		when(mockDLFolderLocalService.dynamicQuery(mockDynamicQuery4)).thenReturn(Collections.emptyList());

		paymentTempFileDeletionSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer().accept(COMPANY_ID);
		verifyZeroInteractions(mockDLFileEntryLocalService);
		verifyStatic(CompanyThreadLocal.class, times(1));
		CompanyThreadLocal.setCompanyId(COMPANY_ID);
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenOneFolderAndOneFile_ThenDeleteTheFile() throws Exception {
		long folderId = 1L;

		when(mockDLFolderLocalService.dynamicQuery()).thenReturn(mockDynamicQuery1);
		when(RestrictionsFactoryUtil.eq(Field.NAME, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME)).thenReturn(mockCriterion1);
		when(RestrictionsFactoryUtil.eq(Field.COMPANY_ID, COMPANY_ID)).thenReturn(mockCompanyIdCriterion);
		when(PropertyFactoryUtil.forName("folderId")).thenReturn(mockProperty);

		when(mockDynamicQuery1.setProjection(mockProperty)).thenReturn(mockDynamicQuery2);
		when(mockDynamicQuery2.add(mockCriterion1)).thenReturn(mockDynamicQuery3);
		when(mockDynamicQuery3.add(mockCompanyIdCriterion)).thenReturn(mockDynamicQuery4);

		when(mockDLFolderLocalService.dynamicQuery(mockDynamicQuery4)).thenReturn(Collections.singletonList(folderId));

		when(mockDLFileEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery1);
		when(RestrictionsFactoryUtil.in(Field.FOLDER_ID, Collections.singletonList(folderId))).thenReturn(mockfolderIdCriterion);
		when(RestrictionsFactoryUtil.lt(Mockito.eq(Field.CREATE_DATE), Mockito.any(Date.class))).thenReturn(mockdateCriterion);
		when(RestrictionsFactoryUtil.and(mockfolderIdCriterion, mockdateCriterion)).thenReturn(mockCriterion2);
		when(mockDynamicQuery1.add(mockCriterion2)).thenReturn(mockDynamicQuery2);

		when(mockDLFileEntryLocalService.dynamicQuery(mockDynamicQuery2)).thenReturn(Collections.singletonList(mockFileEntry));

		paymentTempFileDeletionSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer().accept(COMPANY_ID);
		verify(mockDLFileEntryLocalService, times(1)).deleteDLFileEntry(mockFileEntry);
	}

	@Test
	public void getCompanyJobExecutorUnsafeConsumer_WhenOneFolderButEmpty_ThenDoNothing() throws Exception {
		long folderId = 1L;

		when(mockDLFolderLocalService.dynamicQuery()).thenReturn(mockDynamicQuery1);
		when(RestrictionsFactoryUtil.eq(Field.NAME, OneOffPaymentOrderConstants.TEMP_FILES_FOLDER_NAME)).thenReturn(mockCriterion1);
		when(RestrictionsFactoryUtil.eq(Field.COMPANY_ID, COMPANY_ID)).thenReturn(mockCompanyIdCriterion);
		when(PropertyFactoryUtil.forName("folderId")).thenReturn(mockProperty);

		when(mockDynamicQuery1.setProjection(mockProperty)).thenReturn(mockDynamicQuery2);
		when(mockDynamicQuery2.add(mockCriterion1)).thenReturn(mockDynamicQuery3);
		when(mockDynamicQuery3.add(mockCompanyIdCriterion)).thenReturn(mockDynamicQuery4);

		when(mockDLFolderLocalService.dynamicQuery(mockDynamicQuery4)).thenReturn(Collections.singletonList(folderId));

		when(mockDLFileEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery1);
		when(RestrictionsFactoryUtil.in(Field.FOLDER_ID, Collections.singletonList(folderId))).thenReturn(mockfolderIdCriterion);
		when(RestrictionsFactoryUtil.lt(Mockito.eq(Field.CREATE_DATE), Mockito.any(Date.class))).thenReturn(mockdateCriterion);
		when(RestrictionsFactoryUtil.and(mockfolderIdCriterion, mockdateCriterion)).thenReturn(mockCriterion2);
		when(mockDynamicQuery1.add(mockCriterion2)).thenReturn(mockDynamicQuery2);

		when(mockDLFileEntryLocalService.dynamicQuery(mockDynamicQuery2)).thenReturn(Collections.emptyList());

		paymentTempFileDeletionSchedulerJobConfiguration.getCompanyJobExecutorUnsafeConsumer().accept(COMPANY_ID);
		verify(mockDLFileEntryLocalService, never()).deleteDLFileEntry(Mockito.any());
	}

}
