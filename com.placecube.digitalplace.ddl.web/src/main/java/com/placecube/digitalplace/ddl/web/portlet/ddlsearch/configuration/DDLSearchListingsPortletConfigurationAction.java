package com.placecube.digitalplace.ddl.web.portlet.ddlsearch.configuration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.ddl.web.constants.PortletKeys;
import com.placecube.digitalplace.ddl.web.service.DDLConfigurationService;

@Component(immediate = true, property = "javax.portlet.name=" + PortletKeys.DDL_SEARCH_LISTINGS_PORTLET, service = ConfigurationAction.class)
public class DDLSearchListingsPortletConfigurationAction extends DefaultConfigurationAction {

	@Reference
	private DDLConfigurationService ddlConfigurationService;

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		httpServletRequest.setAttribute("configuration", ddlConfigurationService.getDDLSearchListingsPortletInstanceConfiguration(themeDisplay));
		httpServletRequest.setAttribute("availableStructures", ddlConfigurationService.getAvailableDDMStructures(themeDisplay.getScopeGroup()));
		httpServletRequest.setAttribute("availablePageDeltas", new int[] { 5, 10, 20, 30, 40, 50, 75 });

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		long ddmStructureId = ParamUtil.getLong(actionRequest, DDLSearchListingsConfigurationConstants.STRUCTURE_ID, 0);
		setPreference(actionRequest, DDLSearchListingsConfigurationConstants.STRUCTURE_ID, String.valueOf(ddmStructureId));

		String defaultDisplayStyle = ParamUtil.getString(actionRequest, DDLSearchListingsConfigurationConstants.DEFAULT_DISPLAY_STYLE, StringPool.BLANK);
		setPreference(actionRequest, DDLSearchListingsConfigurationConstants.DEFAULT_DISPLAY_STYLE, defaultDisplayStyle);

		String gridDisplayStyleCss = ParamUtil.getString(actionRequest, DDLSearchListingsConfigurationConstants.GRID_DISPLAY_STYLE_CSS, "col-md-4");
		setPreference(actionRequest, DDLSearchListingsConfigurationConstants.GRID_DISPLAY_STYLE_CSS, gridDisplayStyleCss);

		int numberOfResultsPerPage = ParamUtil.getInteger(actionRequest, DDLSearchListingsConfigurationConstants.NUMBER_RESULTS_PER_PAGE, 20);
		setPreference(actionRequest, DDLSearchListingsConfigurationConstants.NUMBER_RESULTS_PER_PAGE, String.valueOf(numberOfResultsPerPage));

		boolean hidePagination = ParamUtil.getBoolean(actionRequest, DDLSearchListingsConfigurationConstants.HIDE_PAGINATION, false);
		setPreference(actionRequest, DDLSearchListingsConfigurationConstants.HIDE_PAGINATION, String.valueOf(hidePagination));

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

}
