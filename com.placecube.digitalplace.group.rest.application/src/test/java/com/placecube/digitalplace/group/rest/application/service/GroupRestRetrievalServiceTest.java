package com.placecube.digitalplace.group.rest.application.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;

public class GroupRestRetrievalServiceTest extends PowerMockito {

	@InjectMocks
	private GroupRestRetrievalService groupRestRetrievalService;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void isGuestUser_WhenExceptionRetrievingTheUser_ThenReturnsTrue() throws PortalException {
		long userId = 12;
		when(mockUserLocalService.getUser(userId)).thenThrow(new PortalException());

		boolean result = groupRestRetrievalService.isGuestUser(userId);

		assertTrue(result);
	}

	@Test
	public void isGuestUser_WhenUserIsTheGuestUser_ThenReturnsTrue() throws PortalException {
		long userId = 12;
		when(mockUserLocalService.getUser(userId)).thenReturn(mockUser);
		when(mockUser.isGuestUser()).thenReturn(true);

		boolean result = groupRestRetrievalService.isGuestUser(userId);

		assertTrue(result);
	}

	@Test
	public void isGuestUser_WhenUserIsTheNotGuestUser_ThenReturnsFalse() throws PortalException {
		long userId = 12;
		when(mockUserLocalService.getUser(userId)).thenReturn(mockUser);
		when(mockUser.isGuestUser()).thenReturn(false);

		boolean result = groupRestRetrievalService.isGuestUser(userId);

		assertFalse(result);
	}

}
