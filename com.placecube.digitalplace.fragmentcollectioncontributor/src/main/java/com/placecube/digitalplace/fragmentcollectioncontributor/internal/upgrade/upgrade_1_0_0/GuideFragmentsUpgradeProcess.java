package com.placecube.digitalplace.fragmentcollectioncontributor.internal.upgrade.upgrade_1_0_0;

import java.util.List;

import com.liferay.fragment.model.FragmentEntry;
import com.liferay.fragment.service.FragmentEntryLocalService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class GuideFragmentsUpgradeProcess extends UpgradeProcess {

	private final CompanyLocalService companyLocalService;

	private final FragmentEntryLocalService fragmentEntryLocalService;

	public GuideFragmentsUpgradeProcess(CompanyLocalService companyLocalService, FragmentEntryLocalService fragmentEntryLocalService) {
		this.companyLocalService = companyLocalService;
		this.fragmentEntryLocalService = fragmentEntryLocalService;
	}

	@Override
	protected void doUpgrade() throws Exception {

		if (CompanyThreadLocal.getCompanyId() == 0) {
			for (Company company : companyLocalService.getCompanies()) {
				upgradeFragments(company.getCompanyId());
			}
		} else {
			upgradeFragments(CompanyThreadLocal.getCompanyId());
		}

	}

	private void upgradeFragments(long companyId) throws Exception {

		DynamicQuery dynamicQuery = fragmentEntryLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		Criterion likeGuideContent = RestrictionsFactoryUtil.like("fragmentEntryKey", "%guide-content%");
		Criterion likeGuideNavigation = RestrictionsFactoryUtil.like("fragmentEntryKey", "%guide-navigation%");
		dynamicQuery.add(RestrictionsFactoryUtil.or(likeGuideContent, likeGuideNavigation));

		List<FragmentEntry> fragmentEntries = fragmentEntryLocalService.dynamicQuery(dynamicQuery);

		for (FragmentEntry fragmentEntry : fragmentEntries) {

			fragmentEntry.setHtml(upgradeScript(fragmentEntry.getHtml()));
			fragmentEntryLocalService.updateFragmentEntry(fragmentEntry);

		}

	}

	private String upgradeScript(String html) {

		String versionScript = "[#assign version = getterUtil.getDouble(request.getParameter('version')) ]\n" + "\n" + "\t\t\t[#if version > 0 ]\n"
				+ "\t\t\t\t[#assign journalArticleLatest = journalArticleLocalService.fetchLatestArticle(resourcePrimKey, WorkflowConstants.STATUS_ANY) ]\n"
				+ "\t\t\t\t[#assign journalArticle = journalArticleLocalService.fetchArticle(journalArticleLatest.getGroupId(), journalArticleLatest.getArticleId(), version) ]\n" + "\t\t\t[#else]\n"
				+ "\t\t\t\t[#assign journalArticle = journalArticleLocalService.fetchLatestArticle(resourcePrimKey, WorkflowConstants.STATUS_APPROVED) ]\n" + "\t\t\t[/#if]\n";

		String upgradedHtml = html.replace("[#assign journalArticle = journalArticleLocalService.fetchLatestArticle(resourcePrimKey, WorkflowConstants.STATUS_APPROVED) ]", versionScript);
		upgradedHtml = upgradedHtml.replace("articleId=journalArticle.getArticleId()", "article=journalArticle");

		return upgradedHtml.replace("groupId=themeDisplay.scopeGroupId", "");
	}
}
