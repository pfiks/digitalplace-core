package com.placecube.digitalplace.ddl.model;

import java.util.List;

import com.liferay.dynamic.data.lists.model.DDLRecord;

public interface DynamicDataListEntry {

	DDLRecord getDDLRecord();

	long getId();

	List<DynamicDataListValue> getValues();

}
