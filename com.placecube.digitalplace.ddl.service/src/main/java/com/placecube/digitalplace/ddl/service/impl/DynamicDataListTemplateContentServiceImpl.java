package com.placecube.digitalplace.ddl.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropsKeys;
import com.placecube.digitalplace.ddl.internal.util.DDMTemplateUtil;
import com.placecube.digitalplace.ddl.internal.util.TemplateService;
import com.placecube.digitalplace.ddl.internal.util.TemplateUtil;
import com.placecube.digitalplace.ddl.internal.util.TransformContextService;
import com.placecube.digitalplace.ddl.internal.util.Transformer;
import com.placecube.digitalplace.ddl.service.DynamicDataListTemplateContentService;

@Component(immediate = true, service = DynamicDataListTemplateContentService.class)
public class DynamicDataListTemplateContentServiceImpl implements DynamicDataListTemplateContentService {

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private TemplateService templateService;

	@Reference
	private TemplateUtil templateUtil;

	@Reference
	private TransformContextService transformContextService;

	private static class TransformerHolder {

		private static final Transformer transformer = new Transformer(PropsKeys.PORTLET_DISPLAY_TEMPLATES_ERROR, true);

		public static Transformer getTransformer() {
			return transformer;
		}
	}

	@Override
	public String getTranformedTemplate(List<AssetEntry> ddlRecords, DDMStructure ddmStructure, DDMTemplate ddmTemplate, ThemeDisplay themeDisplay, RenderRequest renderRequest)
			throws PortalException {
		Map<String, Object> contextObjects = transformContextService.getContextObjects(ddmTemplate, ddlRecords, ddmStructure.getStructureId(), renderRequest);
		return transform(themeDisplay, contextObjects, ddmTemplate);
	}

	@Override
	public String getTransformedTemplate(DDLRecord ddlRecord, DDMStructure ddmStructure, DDMTemplate ddmTemplate, ThemeDisplay themeDisplay, HttpServletRequest httpServletRequest)
			throws PortalException {
		try {
			Map<String, Object> contextObjects = transformContextService.getContextObjects(ddmTemplate, ddlRecord, ddmStructure.getStructureId(), themeDisplay, httpServletRequest);
			return transform(themeDisplay, contextObjects, ddmTemplate);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	@Override
	public String getTransformedTemplate(DDLRecord ddlRecord, long ddlTemplateId, ThemeDisplay themeDisplay, HttpServletRequest httpServletRequest) throws PortalException {
		try {
			long structureId = ddlRecord.getRecordSet().getDDMStructureId();
			DDMStructure ddmStructure = ddmStructureLocalService.getDDMStructure(structureId);
			Optional<DDMTemplate> template = DDMTemplateUtil.getTemplateById(ddmStructure, ddlTemplateId);
			if (template.isPresent()) {
				Map<String, Object> contextObjects = transformContextService.getContextObjects(template.get(), ddlRecord, ddmStructure.getStructureId(), themeDisplay,
						httpServletRequest);
				return transform(themeDisplay, contextObjects, template.get());
			}
			return StringPool.BLANK;
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	@Override
	public String getTransformedTemplate(long recordId, DDMStructure ddmStructure, DDMTemplate ddmTemplate, ThemeDisplay themeDisplay, HttpServletRequest httpServletRequest) throws PortalException {

			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(recordId);
			Map<String, Object> contextObjects = transformContextService.getContextObjects(ddmTemplate, ddlRecord, ddmStructure.getStructureId(), themeDisplay, httpServletRequest);

			Transformer transformer = TransformerHolder.getTransformer();
			return  transformer.transform(themeDisplay, contextObjects, ddmTemplate.getScript(), ddmTemplate.getLanguage(), new UnsyncStringWriter(), httpServletRequest, themeDisplay.getResponse());
	}

	private String transform(ThemeDisplay themeDisplay, Map<String, Object> contextObjects, DDMTemplate ddmTemplate) throws TemplateException {
		long companyId = themeDisplay.getCompanyId();
		long companyGroupId = themeDisplay.getCompanyGroupId();
		long scopeGroupId = themeDisplay.getScopeGroupId();
		UnsyncStringWriter unsyncStringWriter = templateUtil.getUnsyncStringWriterInstance();
		Template template = templateService.getTemplate(ddmTemplate.getTemplateId(), companyId, companyGroupId, scopeGroupId, ddmTemplate.getScript(), ddmTemplate.getLanguage());
		templateService.prepareTemplate(themeDisplay, template);
		templateService.initTemplate(template, contextObjects, themeDisplay, unsyncStringWriter);
		return unsyncStringWriter.toString();
	}
}
