package com.placecube.digitalplace.user.account.web.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.placecube.digitalplace.user.account.service.ResetPasswordService;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;

@RunWith(PowerMockRunner.class)
public class ResetPasswordMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	@Mock
	private ResetPasswordService mockResetPasswordService;

	@InjectMocks
	private ResetPasswordMVCActionCommand resetPasswordMVCActionCommand;

	@Before
	public void activateSetup() {
		when(mockActionResponse.getRenderParameters()).thenReturn(mockMutableRenderParameters);
	}

	@Test(expected = PortletException.class)
	public void doProcessAction_WhenErrorDuringResetPassword_ThenThrowsPortletException() throws Exception {
		doThrow(new PortletException()).when(mockResetPasswordService).resetPassword(mockActionRequest);
		resetPasswordMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenResetPassword() throws Exception {
		resetPasswordMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockResetPasswordService, times(1)).resetPassword(mockActionRequest);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenSetMVCRenderCommandName() throws Exception {
		resetPasswordMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockMutableRenderParameters, times(1)).setValue(RequestKeys.MVC_RENDER_COMMAND_NAME, MVCCommandKeys.CONFIRMATION_PAGE);
	}

}
