package com.placecube.digitalplace.user.settings.web.twofactor.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.user.settings.web.constants.PortletKeys;
import com.placecube.digitalplace.user.settings.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationRequestKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.USER_SETTINGS,
		"mvc.command.name=" + TwoFactorAuthenticationKeys.ENABLE_TWO_FACTOR_AUTHENTICATION }, service = MVCActionCommand.class)
public class EnableTwoFactorAuthenticationMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private UserTwoFactorAuthenticationLocalService userTwoFactorAuthenticationLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		boolean twoFactorDisplay = ParamUtil.getBoolean(actionRequest, TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM);

		actionRequest.setAttribute(TwoFactorAuthenticationRequestKeys.TWO_FACTOR_AUTHENTICATION_DISPLAY_FORM, twoFactorDisplay);
		if (!twoFactorDisplay) {
			userTwoFactorAuthenticationLocalService.disableTwoFactorAuthenticationForUser(themeDisplay.getUser());
		}

		hideDefaultSuccessMessage(actionRequest);
		actionResponse.getRenderParameters().setValue(PortletRequestKeys.CURRENT_SECTION_ID, TwoFactorAuthenticationKeys.BUNDLE_ID);
	}

}
