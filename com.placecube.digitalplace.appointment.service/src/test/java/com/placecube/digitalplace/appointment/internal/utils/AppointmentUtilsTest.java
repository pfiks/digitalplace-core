package com.placecube.digitalplace.appointment.internal.utils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.TimeZoneUtil;
import com.placecube.digitalplace.appointment.model.AppointmentEntry;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AppointmentUtilsTest {

	@InjectMocks
	private AppointmentUtils appointmentUtils;

	@Mock
	private AppointmentEntry mockAppointmentEntry;

	@Mock
	private Date mockDate;
	private MockedStatic<DateUtil> mockDateUtil;

	private MockedStatic<LocaleUtil> mockLocaleUtil;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TimeZone mockTimeZone;

	private MockedStatic<TimeZoneUtil> mockTimeZoneUtil;

	@Before
	public void activateSetUp() {
		mockLocaleUtil = mockStatic(LocaleUtil.class);
		mockTimeZoneUtil = mockStatic(TimeZoneUtil.class);
		mockDateUtil = mockStatic(DateUtil.class);
	}

	@Test
	public void doesAppointmentMatch_WhenAppointmentIdIsDifferent_ThenReturnsFalse() {
		when(mockAppointmentEntry.getAppointmentId()).thenReturn("somethingElse");

		boolean result = appointmentUtils.doesAppointmentMatch("serviceIdValue", "connectorClassNameValue", "appointmentIdValue", mockAppointmentEntry);

		assertFalse(result);
	}

	@Test
	@Parameters({ "null,null,null,null", "s1,s1,c1,c1", "null,null,c1,c1", "s1,s1,null,null" })
	public void doesAppointmentMatch_WhenAppointmentIdIsTheSameAndServiceIdAndConnectorClassNameAreAmatch_ThenReturnsTrue(String serviceId, String serviceIdFromEntry, String connector,
			String connectorFromEntry) {
		when(mockAppointmentEntry.getAppointmentId()).thenReturn("appointmentIdValue");
		when(mockAppointmentEntry.getServiceId()).thenReturn(serviceIdFromEntry);
		when(mockAppointmentEntry.getConnectorClassName()).thenReturn(connectorFromEntry);

		boolean result = appointmentUtils.doesAppointmentMatch(serviceId, connector, "appointmentIdValue", mockAppointmentEntry);

		assertTrue(result);
	}

	@Test
	@Parameters({ "s1,null,null,null", "null,s1,null,null", "null,null,c1,null", "null,null,null,c1", "s1,s1,c1,null", "s1,s1,null,c1", "s1,null,c1,c1", "null,s1,c1,c1", "s1,s2,c1,c1",
			"s1,s1,c1,c2" })
	public void doesAppointmentMatch_WhenAppointmentIdIsTheSameAndServiceIdAndConnectorClassNameAreNotAmatch_ThenReturnsFalse(String serviceId, String connector, String serviceIdFromEntry,
			String connectorFromEntry) {
		when(mockAppointmentEntry.getAppointmentId()).thenReturn("appointmentIdValue");
		when(mockAppointmentEntry.getServiceId()).thenReturn(serviceIdFromEntry);
		when(mockAppointmentEntry.getConnectorClassName()).thenReturn(connectorFromEntry);

		boolean result = appointmentUtils.doesAppointmentMatch(serviceId, connector, "appointmentIdValue", mockAppointmentEntry);

		assertFalse(result);
	}

	@Test
	public void getLocale_WhenThemeDisplayIsNotNull_ThenReturnsTheLocaleFromThemeDisplay() {
		Locale locale = Locale.ENGLISH;
		when(mockThemeDisplay.getLocale()).thenReturn(locale);

		Locale result = appointmentUtils.getLocale(mockThemeDisplay);

		assertThat(result, equalTo(locale));
	}

	@Test
	public void getLocale_WhenThemeDisplayIsNull_ThenReturnsTheDefaultLocale() {
		Locale locale = Locale.ENGLISH;
		mockLocaleUtil.when(() -> LocaleUtil.getDefault()).thenReturn(locale);

		Locale result = appointmentUtils.getLocale(null);

		assertThat(result, equalTo(locale));
	}

	@Test
	public void getParsedDate_WhenValueIsNotNullAndDatePatternIsNotNull_ThenReturnsOptionalWithDateCreatedFromParsedValue() throws ParseException {
		mockDateUtil.when(() -> DateUtil.parseDate("myPattern", "myValue", Locale.ENGLISH)).thenReturn(mockDate);

		Optional<Date> result = appointmentUtils.getParsedDate("myValue", Locale.ENGLISH, "myPattern");

		assertThat(result.get(), sameInstance(mockDate));
	}

	@Test
	public void getParsedDate_WhenValueIsNotNullAndDatePatternIsNull_ThenReturnsOptionalWithDateCreatedFromTimestamp() throws ParseException {
		long value = 123456l;

		Optional<Date> result = appointmentUtils.getParsedDate("" + value, Locale.ENGLISH, "");

		assertThat(result.get(), equalTo(new Date(value)));
	}

	@Test
	public void getParsedDate_WhenValueIsNull_ThenReturnsEmptyOptional() throws ParseException {
		Optional<Date> result = appointmentUtils.getParsedDate(null, Locale.ENGLISH, "myDatePattern");

		assertFalse(result.isPresent());
	}

	@Test
	public void getTimeZone_WhenThemeDisplayIsNotNull_ThenReturnsTheLocaleFromThemeDisplay() {
		when(mockThemeDisplay.getTimeZone()).thenReturn(mockTimeZone);

		TimeZone result = appointmentUtils.getTimeZone(mockThemeDisplay);

		assertThat(result, equalTo(mockTimeZone));
	}

	@Test
	public void getTimeZone_WhenThemeDisplayIsNull_ThenReturnsTheDefaultLocale() {
		mockTimeZoneUtil.when(() -> TimeZoneUtil.getDefault()).thenReturn(mockTimeZone);

		TimeZone result = appointmentUtils.getTimeZone(null);

		assertThat(result, equalTo(mockTimeZone));
	}

	@After
	public void teadDown() {
		mockLocaleUtil.close();
		mockTimeZoneUtil.close();
		mockDateUtil.close();
	}
}
