package com.placecube.digitalplace.search.web.portlet.sortby;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.search.shared.service.SharedSearchRequestService;
import com.placecube.digitalplace.search.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.search.web.constants.PortletKeys;
import com.placecube.digitalplace.search.web.constants.SearchParameters;
import com.placecube.digitalplace.search.web.portlet.sortby.constants.SortByDirectionConstants;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.SORT_BY, "mvc.command.name=" + MVCCommandKeys.SEARCH }, service = MVCActionCommand.class)
public class SortByMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private SharedSearchRequestService sharedSearchRequestService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String cmd = ParamUtil.getString(actionRequest, "cmd");
		String selectedSortBy = ParamUtil.getString(actionRequest, SearchParameters.SELECTED_SORT_BY);
		sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, SearchParameters.SELECTED_SORT_BY, selectedSortBy);
		sharedSearchRequestService.removeSharedSearchSessionAttribute(actionRequest, SearchParameters.SELECTED_SORT_BY_DIRECTION);

		if ("updateSortByDirection".equalsIgnoreCase(cmd)) {
			String selectedSortByDirection = SortByDirectionConstants.DIRECTION_DEFAULT;
			String currentSelectedSortByDirection = ParamUtil.getString(actionRequest, "currentSelectedSortByDirection", SortByDirectionConstants.DIRECTION_DEFAULT);
			if (SortByDirectionConstants.DIRECTION_DEFAULT.equalsIgnoreCase(currentSelectedSortByDirection)) {
				selectedSortByDirection = SortByDirectionConstants.DIRECTION_INVERTED;
			}
			sharedSearchRequestService.setSharedSearchSessionSingleValuedAttribute(actionRequest, SearchParameters.SELECTED_SORT_BY_DIRECTION, selectedSortByDirection);
		}

		sharedSearchRequestService.sendRedirectFromParameter(actionRequest, actionResponse, "redirectURL");
	}

}
