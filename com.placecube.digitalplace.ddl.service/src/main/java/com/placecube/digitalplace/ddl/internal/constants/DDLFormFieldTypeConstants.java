package com.placecube.digitalplace.ddl.internal.constants;

public class DDLFormFieldTypeConstants {

	public static final String DDM_DATE = "ddm-date";

	private DDLFormFieldTypeConstants() {

	}
}
