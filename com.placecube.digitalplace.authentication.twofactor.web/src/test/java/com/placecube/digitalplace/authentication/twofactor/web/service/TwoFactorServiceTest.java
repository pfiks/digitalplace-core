package com.placecube.digitalplace.authentication.twofactor.web.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.model.UserTwoFactorAuthentication;
import com.placecube.digitalplace.authentication.twofactor.service.UserTwoFactorAuthenticationLocalService;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorAuditMessage;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorSessionKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ServiceContextFactory.class })
public class TwoFactorServiceTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private AuthenticationTwoFactorCompanyConfiguration mockAuthenticationTwoFactorCompanyConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private HttpServletRequest mockHttpServletRequest;

	@Mock
	private HttpSession mockHttpSession;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TwoFactorAuditMessageService mockTwoFactorAuditMessageService;

	@Mock
	private User mockUser;

	@Mock
	private User mockUserUpdated;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private UserTwoFactorAuthentication mockUserTwoFactorAuthentication;

	@Mock
	private UserTwoFactorAuthenticationLocalService mockUserTwoFactorAuthenticationLocalService;

	@InjectMocks
	private TwoFactorService twoFactorService;

	@Before
	public void activateSetup() {
		mockStatic(ServiceContextFactory.class);
		initMocks(this);
	}

	@Test
	public void configureTwoFactorAuthenticationForUser_WhenSecretKeyEmpty_ThenNoActionIsPerformed() {
		twoFactorService.configureTwoFactorAuthenticationForUser(mockUser, "");

		verifyZeroInteractions(mockUserTwoFactorAuthenticationLocalService);
	}

	@Test
	public void configureTwoFactorAuthenticationForUser_WhenSecretKeySpecified_ThenConfiguredTheTwoFactorAuthenticationForTheUser() {
		String key = "keyValue";
		twoFactorService.configureTwoFactorAuthenticationForUser(mockUser, key);

		verify(mockUserTwoFactorAuthenticationLocalService, times(1)).configureTwoFactorAuthenticationForUser(mockUser, key);
	}

	@Test
	public void getEnabledConfiguration_WhenConfigurationFoundAndIsEnabled_ThenReturnsOptionalWithTheConfiguration() throws ConfigurationException {
		long companyId = 1223;
		when(mockConfigurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, companyId)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockAuthenticationTwoFactorCompanyConfiguration.enabled()).thenReturn(true);

		Optional<AuthenticationTwoFactorCompanyConfiguration> result = twoFactorService.getEnabledConfiguration(companyId);

		assertThat(result.get(), sameInstance(mockAuthenticationTwoFactorCompanyConfiguration));
	}

	@Test
	public void getEnabledConfiguration_WhenConfigurationFoundButIsNotEnabled_ThenReturnsEmptyOptional() throws ConfigurationException {
		long companyId = 1223;
		when(mockConfigurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, companyId)).thenReturn(mockAuthenticationTwoFactorCompanyConfiguration);
		when(mockAuthenticationTwoFactorCompanyConfiguration.enabled()).thenReturn(false);

		Optional<AuthenticationTwoFactorCompanyConfiguration> result = twoFactorService.getEnabledConfiguration(companyId);

		assertFalse(result.isPresent());
	}

	@Test
	public void getEnabledConfiguration_WhenExceptionRetrievingConfiguration_ThenReturnsEmptyOptional() throws ConfigurationException {
		long companyId = 1223;
		when(mockConfigurationProvider.getCompanyConfiguration(AuthenticationTwoFactorCompanyConfiguration.class, companyId)).thenThrow(new ConfigurationException());

		Optional<AuthenticationTwoFactorCompanyConfiguration> result = twoFactorService.getEnabledConfiguration(companyId);

		assertFalse(result.isPresent());
	}

	@Test(expected = PortletException.class)
	public void getQRBarCodeURL_WhenExceptionRetrievingTheBarCode_ThenthrowsPortletException() throws Exception {
		long companyId = 1223;
		String email = "emailValue";
		String key = "keyValue";
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockUser.getEmailAddress()).thenReturn(email);
		when(mockUserTwoFactorAuthenticationLocalService.getQRBarCodeURL(companyId, email, key)).thenThrow(new PortalException());

		twoFactorService.getQRBarCodeURL(mockUser, key);
	}

	@Test
	public void getQRBarCodeURL_WhenNoError_ThenReturnsTheBarCodeURL() throws Exception {
		long companyId = 1223;
		String email = "emailValue";
		String key = "keyValue";
		String expected = "expectedValue";
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockUser.getEmailAddress()).thenReturn(email);
		when(mockUserTwoFactorAuthenticationLocalService.getQRBarCodeURL(companyId, email, key)).thenReturn(expected);

		String result = twoFactorService.getQRBarCodeURL(mockUser, key);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSecretKey_WhenKeyAsFoundAsRequestAttribute_ThenReturnsTheValueFromTheRequest() {
		String expected = "expectedValue";
		when(mockPortletRequest.getAttribute("twoFactorSecretKey")).thenReturn(expected);

		String result = twoFactorService.getSecretKey(mockPortletRequest);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSecretKey_WhenKeyNotFoundAsRequestAttribute_ThenReturnsAnewGeneratedKey() {
		String expected = "expectedValue";
		when(mockPortletRequest.getAttribute("twoFactorSecretKey")).thenReturn("");
		when(mockUserTwoFactorAuthenticationLocalService.generateSecretKey()).thenReturn(expected);

		String result = twoFactorService.getSecretKey(mockPortletRequest);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getTwoFactorUser_WhenIsNotSignedInAndUserFoundFromValueInSession_ThenReturnsTheUserFromSession() {
		long userId = 123;
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(false);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_USER_ID)).thenReturn(userId);
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);

		Optional<User> result = twoFactorService.getTwoFactorUser(mockPortletRequest, mockHttpServletRequest);

		assertThat(result.get(), sameInstance(mockUser));
	}

	@Test
	public void getTwoFactorUser_WhenIsNotSignedInAndUserNotFoundFromValueInSession_ThenReturnsEmptyOptional() {
		long userId = 123;
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(false);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_USER_ID)).thenReturn(userId);
		when(mockUserLocalService.fetchUser(userId)).thenReturn(null);

		Optional<User> result = twoFactorService.getTwoFactorUser(mockPortletRequest, mockHttpServletRequest);

		assertFalse(result.isPresent());
	}

	@Test
	public void getTwoFactorUser_WhenIsSignedIn_ThenReturnsTheSignedInUser() {
		when(mockPortletRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.isSignedIn()).thenReturn(true);
		when(mockThemeDisplay.getUser()).thenReturn(mockUser);

		Optional<User> result = twoFactorService.getTwoFactorUser(mockPortletRequest, mockHttpServletRequest);

		assertThat(result.get(), sameInstance(mockUser));
	}

	@Test
	public void getUserTwoFactorAuthentication_WhenNoUserFound_ThenReturnsEmptyOptional() {
		long userId = 123;
		when(mockUserLocalService.fetchUser(userId)).thenReturn(null);

		Optional<UserTwoFactorAuthentication> result = twoFactorService.getUserTwoFactorAuthentication(userId);

		assertFalse(result.isPresent());
	}

	@Test
	public void getUserTwoFactorAuthentication_WhenUserFound_ThenReturnsOptionalWithTheUserTwoFactorAuthentication() {
		long userId = 123;
		when(mockUserLocalService.fetchUser(userId)).thenReturn(mockUser);
		Optional<UserTwoFactorAuthentication> expected = Optional.of(mockUserTwoFactorAuthentication);
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(expected);

		Optional<UserTwoFactorAuthentication> result = twoFactorService.getUserTwoFactorAuthentication(userId);

		assertThat(result, sameInstance(expected));
	}

	@Test
	public void isTwoFactorAlreadyVerified_WhenUserIsFoundAndSessionIsNull_ThenReturnsFalseAndAddsTheInvalidSessionAuditMessage() {
		long userId = 123;
		when(mockHttpServletRequest.getSession(false)).thenReturn(null);

		boolean result = twoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId);

		assertFalse(result);
		verify(mockTwoFactorAuditMessageService, times(1)).addAuditMessage(mockUser, TwoFactorAuditMessage.INVALID_SESSION);
	}

	@Test
	public void isTwoFactorAlreadyVerified_WhenUserIsFoundAndSessionIsValidAndNoSessionAttributeForValidatedUserId_ThenReturnsFalseAndAddsTheUserNotVerifiedAuditMessage() {
		long userId = 123;
		when(mockHttpServletRequest.getSession(false)).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_VALIDATED_USER_ID)).thenReturn(null);

		boolean result = twoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId);

		assertFalse(result);
		verify(mockTwoFactorAuditMessageService, times(1)).addAuditMessage(mockUser, TwoFactorAuditMessage.USER_NOT_YET_VERIFIED);
	}

	@Test
	public void isTwoFactorAlreadyVerified_WhenUserIsFoundAndSessionIsValidAndSessionAttributeForValidatedUserIdDifferentFromTheCurrentUserId_ThenReturnsFalseAndAddsTheUserNotTheSameAuditMessage() {
		long userId = 123;
		when(mockHttpServletRequest.getSession(false)).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_VALIDATED_USER_ID)).thenReturn(456l);

		boolean result = twoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId);

		assertFalse(result);
		verify(mockTwoFactorAuditMessageService, times(1)).addAuditMessage(mockUser, TwoFactorAuditMessage.USER_NOT_THE_SAME);
	}

	@Test
	public void isTwoFactorAlreadyVerified_WhenUserIsFoundAndSessionIsValidAndSessionAttributeForValidatedUserIdIsTheSameAsTheCurrentUserId_ThenReturnsTrueAndAddsNoAuditMessages() {
		long userId = 123;
		when(mockHttpServletRequest.getSession(false)).thenReturn(mockHttpSession);
		when(mockHttpSession.getAttribute(TwoFactorSessionKeys.TWO_FACTOR_VALIDATED_USER_ID)).thenReturn(userId);

		boolean result = twoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, mockUser, userId);

		assertTrue(result);
		verifyZeroInteractions(mockTwoFactorAuditMessageService);
	}

	@Test
	public void isTwoFactorAlreadyVerified_WhenUserIsNull_ThenReturnsFalseAndAddsTheUserNotFoundAuditMessage() {
		long userId = 123;

		boolean result = twoFactorService.isTwoFactorAlreadyVerified(mockHttpServletRequest, null, userId);

		assertFalse(result);
		verify(mockTwoFactorAuditMessageService, times(1)).addAuditMessage(userId, TwoFactorAuditMessage.USER_NOT_FOUND);
	}

	@Test
	public void isValidTwoFactorCode_WhenAuthCodeIsBlank_ThenReturnsFalse() throws PortalException {
		boolean result = twoFactorService.isValidTwoFactorCode(mockUser, "", mockHttpServletRequest);

		assertFalse(result);
		verifyZeroInteractions(mockUserTwoFactorAuthenticationLocalService, mockTwoFactorAuditMessageService);
	}

	@Test
	public void isValidTwoFactorCode_WhenAuthCodeIsInvalidForTheUser_ThenReturnsFalseAndAddsTheInvaliAuthCodeAuditMessage() throws PortalException {
		String authCode = "codeValue";
		when(mockUserTwoFactorAuthenticationLocalService.isValidTwoFactorAuthentication(mockUser, authCode)).thenReturn(false);

		boolean result = twoFactorService.isValidTwoFactorCode(mockUser, authCode, mockHttpServletRequest);

		assertFalse(result);
		verify(mockTwoFactorAuditMessageService, times(1)).addAuditMessage(mockUser, TwoFactorAuditMessage.INVALID_AUTH_CODE);
	}

	@Test
	public void isValidTwoFactorCode_WhenAuthCodeIsValidForTheUser_ThenReturnsTrueAndAddsTheUserIdAsValidatedUserIdInSession() throws PortalException {
		String authCode = "codeValue";
		long userId = 123;
		when(mockUserTwoFactorAuthenticationLocalService.isValidTwoFactorAuthentication(mockUser, authCode)).thenReturn(true);
		when(mockUser.getUserId()).thenReturn(userId);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);

		boolean result = twoFactorService.isValidTwoFactorCode(mockUser, authCode, mockHttpServletRequest);

		assertTrue(result);
		verify(mockHttpSession, times(1)).setAttribute(TwoFactorSessionKeys.TWO_FACTOR_VALIDATED_USER_ID, userId);
	}

	@Test
	public void isTwoFactorAlreadyVerified_WhenUserIsFoundAndTwoFactorIsEnforced_ThenReturnsTrue() {
		long userId = 123;
		when(mockUserTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(mockUser)).thenReturn(true);

		boolean result = twoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId);

		assertTrue(result);
	}

	@Test
	public void isTwoFactorAlreadyVerified_WhenUserIsFoundAndTwoFactorIsNotEnforcedAndTwoFactorIsFoundAndEnabled_ThenReturnsTrue() {
		long userId = 123;
		when(mockUserTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(mockUser)).thenReturn(false);
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockUserTwoFactorAuthentication.getEnabled()).thenReturn(true);

		boolean result = twoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId);

		assertTrue(result);
	}

	@Test
	public void isTwoFactorAlreadyVerified_WhenUserIsFoundAndTwoFactorIsNotEnforcedAndTwoFactorIsFoundAndNotEnabled_ThenReturnsFalse() {
		long userId = 123;
		when(mockUserTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(mockUser)).thenReturn(false);
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.of(mockUserTwoFactorAuthentication));
		when(mockUserTwoFactorAuthentication.getEnabled()).thenReturn(false);

		boolean result = twoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId);

		assertFalse(result);
	}

	@Test
	public void isTwoFactorAlreadyVerified_WhenUserIsFoundAndTwoFactorIsNotEnforcedAndTwoFactorIsNotFound_ThenReturnsFalse() {
		long userId = 123;
		when(mockUserTwoFactorAuthenticationLocalService.isTwoFactorAuthenticationEnforcedForUser(mockUser)).thenReturn(false);
		when(mockUserTwoFactorAuthenticationLocalService.getUserTwoFactorAuthentication(mockUser)).thenReturn(Optional.empty());

		boolean result = twoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(mockUser, userId);

		assertFalse(result);
	}

	@Test
	public void isTwoFactorAuthenticationEnforcedOrEnabled_WhenUserIsNull_ThenReturnsFalseAndAddsTheUserNotFoundAuditMessage() {
		long userId = 123;

		boolean result = twoFactorService.isTwoFactorAuthenticationEnforcedOrEnabled(null, userId);

		assertFalse(result);
	}

	@Test
	public void verifyAndEnableTwoFactorAuthentication_WhenVerificationCodeIsCorrect_ThenReturnsTrue() throws PortalException {
		String authCode = "code";
		String secretKey = "key";

		when(mockUserTwoFactorAuthenticationLocalService.verifyAndEnableUserTwoFactorAuthentication(mockUser, authCode, secretKey)).thenReturn(true);

		boolean result = twoFactorService.verifyAndEnableTwoFactorAuthentication(mockUser, authCode, secretKey);

		assertTrue(result);
	}

	@Test
	public void verifyAndEnableTwoFactorAuthentication_WhenVerificationCodeIsIncorrect_ThenReturnsFalse() throws PortalException {
		String authCode = "code";
		String secretKey = "key";

		when(mockUserTwoFactorAuthenticationLocalService.verifyAndEnableUserTwoFactorAuthentication(mockUser, authCode, secretKey)).thenReturn(false);

		boolean result = twoFactorService.verifyAndEnableTwoFactorAuthentication(mockUser, authCode, secretKey);

		assertFalse(result);
	}

	@Test(expected = PortalException.class)
	public void verifyAndEnableTwoFactorAuthentication_WhenVerificationThrowsPortalException_ThenThrowPortalException() throws PortalException {
		String authCode = "code";
		String secretKey = "key";

		when(mockUserTwoFactorAuthenticationLocalService.verifyAndEnableUserTwoFactorAuthentication(mockUser, authCode, secretKey)).thenThrow(PortalException.class);

		twoFactorService.verifyAndEnableTwoFactorAuthentication(mockUser, authCode, secretKey);
	}

	@Test
	public void setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail_WhenNoError_ThenUpdatesUserEmailAddressVerifiedToFalseAndSendsVerifyEmailAddressEmail() throws PortalException {
		String emailAddress = "a@b.c";

		when(mockUser.getEmailAddress()).thenReturn(emailAddress);
		when(mockUserUpdated.getEmailAddress()).thenReturn(emailAddress);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockUserLocalService.updateUser(mockUser)).thenReturn(mockUserUpdated);

		twoFactorService.setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail(mockUser, mockActionRequest);

		InOrder inOrder = inOrder(mockUser, mockUserLocalService);
		inOrder.verify(mockUser, times(1)).setEmailAddressVerified(false);
		inOrder.verify(mockUserLocalService, times(1)).updateUser(mockUser);
		inOrder.verify(mockUserLocalService, times(1)).sendEmailAddressVerification(mockUserUpdated, emailAddress, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail_WhenPortalExceptionSendingEmailVerificationEmail_ThenThrowsPortalException() throws PortalException {
		String emailAddress = "a@b.c";

		when(mockUser.getEmailAddress()).thenReturn(emailAddress);
		when(mockUserUpdated.getEmailAddress()).thenReturn(emailAddress);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockUserLocalService.updateUser(mockUser)).thenReturn(mockUserUpdated);
		doThrow(new PortalException()).when(mockUserLocalService).sendEmailAddressVerification(mockUserUpdated, emailAddress, mockServiceContext);

		twoFactorService.setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail(mockUser, mockActionRequest);
	}

	@Test
	public void setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail_WhenPortalExceptionSendingEmailVerificationEmail_ThenUpdatesUserEmailAddressVerifiedToFalse() throws PortalException {
		String emailAddress = "a@b.c";

		when(mockUser.getEmailAddress()).thenReturn(emailAddress);
		when(mockUserUpdated.getEmailAddress()).thenReturn(emailAddress);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenReturn(mockServiceContext);
		when(mockUserLocalService.updateUser(mockUser)).thenReturn(mockUserUpdated);
		doThrow(new PortalException()).when(mockUserLocalService).sendEmailAddressVerification(mockUserUpdated, emailAddress, mockServiceContext);

		try {
			twoFactorService.setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail(mockUser, mockActionRequest);
		} catch (PortalException e) {
			//
		}

		InOrder inOrder = inOrder(mockUser, mockUserLocalService);
		inOrder.verify(mockUser, times(1)).setEmailAddressVerified(false);
		inOrder.verify(mockUserLocalService, times(1)).updateUser(mockUser);
	}

	@Test(expected = PortalException.class)
	public void setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail_WhenPortalExceptionGettingServiceContext_ThenThrowsPortalException() throws PortalException {
		String emailAddress = "a@b.c";

		when(mockUser.getEmailAddress()).thenReturn(emailAddress);
		when(mockUserUpdated.getEmailAddress()).thenReturn(emailAddress);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenThrow(PortalException.class);
		when(mockUserLocalService.updateUser(mockUser)).thenReturn(mockUserUpdated);

		twoFactorService.setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail(mockUser, mockActionRequest);
	}

	@Test
	public void setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail_WhenPortalExceptionGettingServiceContext_ThenUpdatesUserEmailAddressVerifiedToFalse() throws PortalException {
		String emailAddress = "a@b.c";

		when(mockUser.getEmailAddress()).thenReturn(emailAddress);
		when(mockUserUpdated.getEmailAddress()).thenReturn(emailAddress);
		when(ServiceContextFactory.getInstance(mockActionRequest)).thenThrow(PortalException.class);
		when(mockUserLocalService.updateUser(mockUser)).thenReturn(mockUserUpdated);

		try {
			twoFactorService.setUserEmailAddressVerifiedToFalseAndSendVerifyEmailAddressEmail(mockUser, mockActionRequest);
		} catch (PortalException e) {
			//
		}

		InOrder inOrder = inOrder(mockUser, mockUserLocalService);
		inOrder.verify(mockUser, times(1)).setEmailAddressVerified(false);
		inOrder.verify(mockUserLocalService, times(1)).updateUser(mockUser);
	}

	@Test
	public void setTwoFactorValidatedUserIdInHttpSession_WhenNoError_ThenSetsTwoFactorValidatedUserIdInHttpSession() {
		long userId = 1;

		when(mockUser.getUserId()).thenReturn(userId);
		when(mockHttpServletRequest.getSession()).thenReturn(mockHttpSession);

		twoFactorService.setTwoFactorValidatedUserIdInHttpSession(mockHttpServletRequest, mockUser);

		verify(mockHttpSession, times(1)).setAttribute(TwoFactorSessionKeys.TWO_FACTOR_VALIDATED_USER_ID, userId);
	}
}
