package com.placecube.digitalplace.payment.model;

import java.io.Serializable;

import com.liferay.petra.string.StringPool;

public class AgreementStatus implements Serializable {

	private static final long serialVersionUID = 1L;
	private String agreementId;
	private String status;

	public AgreementStatus() {
		agreementId = StringPool.BLANK;
		status = StringPool.BLANK;
	}

	public AgreementStatus(String agreementId, String status) {
		this.agreementId = agreementId;
		this.status = status;
	}

	public String getAgreementId() {
		return agreementId;
	}

	public String getStatus() {
		return status;
	}

	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "AgreementStatus [agreementId=" + agreementId + ", status=" + status + "]";
	}
}
