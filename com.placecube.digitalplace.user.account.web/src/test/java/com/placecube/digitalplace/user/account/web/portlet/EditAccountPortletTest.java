package com.placecube.digitalplace.user.account.web.portlet;

import java.lang.reflect.Method;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.digitalplace.user.account.service.AccountService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class EditAccountPortletTest extends PowerMockito {

	@InjectMocks
	private EditAccountPortlet editAccountPortlet;

	@Mock
	private AccountService mockAccountService;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Test(expected = PortletException.class)
	public void render_WhenExceptionCheckingAccountCreation_ThenThrowsPortletException() throws Exception {
		mockCallToSuper();
		doThrow(new PortletException()).when(mockAccountService).checkAccountCreationEnabled(mockRenderRequest);

		editAccountPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@SuppressWarnings("rawtypes")
	private void mockCallToSuper() throws Exception {
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);
	}
}
