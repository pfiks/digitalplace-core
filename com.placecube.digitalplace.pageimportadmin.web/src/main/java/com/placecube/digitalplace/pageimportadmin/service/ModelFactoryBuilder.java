package com.placecube.digitalplace.pageimportadmin.service;

import java.util.List;
import java.util.Locale;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.pageimportadmin.model.PageImportModel;
import com.placecube.digitalplace.pageimportadmin.model.RowImportModel;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.service.ModelCreationFactory;
import com.placecube.journal.model.JournalArticleContext;

@Component(immediate = true, service = ModelFactoryBuilder.class)
public class ModelFactoryBuilder {

	static final DataFormatter DATA_FORMATTER = new DataFormatter();

	@Reference
	private ContentRetrievalService contentRetrievalService;

	@Reference
	private ModelCreationFactory modelCreationFactory;

	@Reference
	private Portal portal;

	public void configureAndValidateCategories(RowImportModel rowImportModel, long assetVocabularyId, Locale locale) {
		for (String categoryName : rowImportModel.getCategoriesNames()) {
			List<Long> categoriesIds = contentRetrievalService.getCategoriesByName(assetVocabularyId, categoryName);

			if (categoriesIds.isEmpty()) {
				rowImportModel.addWarningMessage(LanguageUtil.format(locale, "category-not-found", categoryName));
			} else if (categoriesIds.size() > 1) {
				rowImportModel.addWarningMessage(LanguageUtil.format(locale, "too-many-categories-found", categoryName));
			} else {
				rowImportModel.addCategoryId(categoriesIds.get(0));
			}
		}
	}

	public void configurePageTemplate(LayoutContext layoutContext, LayoutPageTemplateEntry layoutPageTemplateEntry) {
		layoutContext.setClassPK(layoutPageTemplateEntry.getLayoutPageTemplateEntryId());
		layoutContext.setClassNameId(portal.getClassNameId(LayoutPageTemplateEntry.class));
	}

	public JournalArticleContext getJournalArticleContext(WebContentImportModel webContentImportModel) {
		JournalArticleContext journalArticleContext = JournalArticleContext.init(StringPool.BLANK, webContentImportModel.getTitle(), "<?xml version=\"1.0\"?><root></root>");
		journalArticleContext.setDDMStructure(webContentImportModel.getWebContentStructure().get());
		journalArticleContext.setUrlTitle(webContentImportModel.getUrl());
		return journalArticleContext;
	}

	public LayoutContext getLayoutContext(PageImportModel pageImportModel) {
		LayoutContext layoutContext = modelCreationFactory.createContentPageLayoutContext(pageImportModel.getTitle(), pageImportModel.getUrl(), pageImportModel.isPrivateLayout());
		layoutContext.setParentLayoutFriendlyURL(pageImportModel.getParentPageURL());
		return layoutContext;
	}

	public RowImportModel getPageImportModel(int index, long groupId, String title, String url, List<String> categories, boolean privateLayout, String parentPageURL, String pageTemplateName) {
		PageImportModel pageImportModel = new PageImportModel(groupId, index, title, url, categories);
		pageImportModel.setPrivateLayout(privateLayout);
		pageImportModel.setParentPageURL(parentPageURL);
		pageImportModel.setParentLayout(contentRetrievalService.getLayout(groupId, parentPageURL, privateLayout));
		pageImportModel.setPageTemplateName(pageTemplateName);
		pageImportModel.setPageTemplate(contentRetrievalService.getPageTemplate(groupId, pageImportModel.getPageTemplateName()));
		return pageImportModel;
	}

	public RowImportModel getWebContentImportModel(int index, long groupId, String title, String url, List<String> categories, String webContentStructureName) {
		WebContentImportModel webContentImportModel = new WebContentImportModel(groupId, index, title, url, categories);
		webContentImportModel.setWebContentStructure(contentRetrievalService.getWebContentStructure(groupId, webContentStructureName));
		return webContentImportModel;
	}

	public RowImportModel getEmptyRowImportModel(int index, long groupId) {
		return new RowImportModel(groupId, index, null, null, null);
	}

	public boolean importAsWebContent(String pageTemplateName, String webContentStructureName) {
		return Validator.isNull(pageTemplateName) && Validator.isNotNull(webContentStructureName);
	}

	public String getStringField(Row row, int index) {
		Cell cell = row.getCell(index);
		return Validator.isNotNull(cell) ?  DATA_FORMATTER.formatCellValue(cell) : StringPool.BLANK;
	}

}
