create index IX_EFC3D581 on Placecube_Appointment_AppointmentEntry (companyId, entryClassPK, entryClassNameId);
create index IX_2DA5010C on Placecube_Appointment_AppointmentEntry (companyId, serviceId[$COLUMN_LENGTH:4000$], connectorClassName[$COLUMN_LENGTH:4000$], appointmentId[$COLUMN_LENGTH:2000000$]);
create index IX_F6746F38 on Placecube_Appointment_AppointmentEntry (uuid_[$COLUMN_LENGTH:75$]);

create index IX_17D6EAEC on Placecube_Appointment_AppointmentSlot (companyId, slotType[$COLUMN_LENGTH:75$], serviceId[$COLUMN_LENGTH:4000$]);
create index IX_9FA8DB34 on Placecube_Appointment_AppointmentSlot (uuid_[$COLUMN_LENGTH:75$]);