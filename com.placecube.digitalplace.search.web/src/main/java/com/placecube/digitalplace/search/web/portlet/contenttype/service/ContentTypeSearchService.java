package com.placecube.digitalplace.search.web.portlet.contenttype.service;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.dynamic.data.lists.service.DDLRecordLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.search.web.constants.SearchFacetConstants;
import com.placecube.digitalplace.search.web.portlet.contenttype.configuration.ContentTypeFacetPortletInstanceConfiguration;
import com.placecube.digitalplace.search.web.portlet.contenttype.constants.ContentTypeConstants;

@Component(immediate = true, service = ContentTypeSearchService.class)
public class ContentTypeSearchService {

	private static final Log LOG = LogFactoryUtil.getLog(ContentTypeSearchService.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Reference
	private DDLRecordLocalService ddlRecordLocalService;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	public ContentTypeFacetPortletInstanceConfiguration getConfiguration(ThemeDisplay themeDisplay) throws ConfigurationException {
		return configurationProvider.getPortletInstanceConfiguration(ContentTypeFacetPortletInstanceConfiguration.class, themeDisplay);
	}

	public Field getContentTypeSearchField(String className) {
		return new Field(SearchFacetConstants.CONTENT_TYPE, className);
	}

	public String getDDLRecordContentTypeValue(String structureId) {
		return getDDLClassNamePrefix().concat(structureId);
	}

	public String getDDLStructureIdFromDocument(Document document) throws PortalException {
		String ddmStructureId = document.get("ddmStructureId");
		if (Validator.isNull(ddmStructureId)) {
			long entryClassPK = GetterUtil.getLong(document.get("entryClassPK"));
			DDLRecord ddlRecord = ddlRecordLocalService.getDDLRecord(entryClassPK);
			ddmStructureId = String.valueOf(ddlRecord.getRecordSet().getDDMStructureId());
		}
		return ddmStructureId;
	}

	public Map<Locale, String> getLabelMapForContentType(Locale locale, String contentTypeValue) {
		String label;
		if (contentTypeValue.startsWith(getDDLClassNamePrefix())) {
			label = getDDLStructureLabel(locale, contentTypeValue);
		} else {
			label = AggregatedResourceBundleUtil.get(ContentTypeConstants.getMessageKeyForClassName(contentTypeValue), locale, "com.placecube.digitalplace.search.web");
		}

		return Collections.singletonMap(locale, label);
	}

	private String getDDLClassNamePrefix() {
		return ContentTypeConstants.DDL_RECORD_CLASS_NAME.concat(StringPool.UNDERLINE);
	}

	private String getDDLStructureLabel(Locale locale, String contentTypeValue) {
		String structureIdValue = StringUtil.removeSubstring(contentTypeValue, getDDLClassNamePrefix());
		try {
			long ddmStructureId = GetterUtil.getLong(structureIdValue);
			return ddmStructureLocalService.getDDMStructure(ddmStructureId).getName(locale);
		} catch (PortalException e) {
			LOG.warn("Unable to retrieve ddmStructureId: " + e.getMessage());
			return AggregatedResourceBundleUtil.get("content-type-ddl", locale, "com.placecube.digitalplace.search.web").concat(StringPool.BLANK).concat(structureIdValue);
		}
	}
}
