package com.placecube.digitalplace.user.account.web.service;

import java.util.Locale;
import java.util.Optional;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.ResetPasswordService;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = ResetPasswordService.class)
public class ResetPasswordServiceImpl implements ResetPasswordService {

	private static final Log LOG = LogFactoryUtil.getLog(ResetPasswordService.class);

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private JournalArticleRetrievalService journalArticleRetrievalService;

	@Reference
	private Portal portal;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void resetPassword(PortletRequest portletRequest) throws PortletException {
		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(portletRequest);
			Locale locale = portal.getLocale(portletRequest);
			String userEmailAddressCreated = ParamUtil.getString(portletRequest, RequestKeys.USER_EMAIL_ADDRESS_TO_SEND_RESET_PASSWORD);
			CreateAccountConfiguration accountConfiguration = accountConfigurationService.getAccountConfiguration(accountContext, portletRequest);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(portletRequest);
			serviceContext.setPlid(0);

			JournalArticle journalArticle = journalArticleLocalService.fetchLatestArticle(accountContext.getGroupId(), accountConfiguration.resetPasswordArticleId(),
					WorkflowConstants.STATUS_APPROVED);

			Optional<String> body = journalArticleRetrievalService.getFieldValue(journalArticle, "Body", locale);
			Optional<String> subject = journalArticleRetrievalService.getFieldValue(journalArticle, "Subject", locale);

			userLocalService.sendPassword(accountContext.getCompanyId(), userEmailAddressCreated, null, null, subject.orElse(null), body.orElse(null), serviceContext);
			accountContextService.updateContextInSession(accountContext, portletRequest);
		} catch (UserAccountException | PortalException e) {
			LOG.error("Error during resetting password", e);
			throw new PortletException(e);
		}

	}

}
