package com.placecube.digitalplace.userindexer.indexer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.userindexer.constants.UserIndexerFields;
import com.placecube.digitalplace.userindexer.internal.service.UserRoleService;

public class UserPostProcessorIndexerTest extends PowerMockito {

	@InjectMocks
	private UserPostProcessorIndexer userPostProcessorIndexer;

	@Mock
	private UserRoleService mockUserRoleService;

	@Mock
	private User mockUser;

	@Mock
	private Document mockDocument;

	@Mock
	private Field mockField;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void postProcessDocument_WhenUserHasNotGroupRoles_ThenGroupRoleIdsFieldIsNotIndexed() throws Exception {

		when(mockUserRoleService.getGroupRoleIdsFieldValues(mockUser)).thenReturn(Collections.emptyList());

		userPostProcessorIndexer.postProcessDocument(mockDocument, mockUser);

		verify(mockDocument, never()).addText(eq(UserIndexerFields.GROUP_ROLE_IDS), any(String[].class));

	}

	@Test
	public void postProcessDocument_WhenUserHasGroupRoles_ThenGroupRoleIdsFieldIsIndexed() throws Exception {

		List<String> groupRoleIds = new ArrayList<>();
		long groupId = 1;
		long userId = 2;
		String userGroupRoleSearchValue = UserIndexerFields.getUserGroupRoleSearchValue(groupId, userId);
		groupRoleIds.add(userGroupRoleSearchValue);

		when(mockUserRoleService.getGroupRoleIdsFieldValues(mockUser)).thenReturn(groupRoleIds);

		userPostProcessorIndexer.postProcessDocument(mockDocument, mockUser);

		verify(mockDocument, times(1)).addText(UserIndexerFields.GROUP_ROLE_IDS, groupRoleIds.toArray(new String[groupRoleIds.size()]));

	}

	@Test
	public void postProcessDocument_WhenUserHasNotRegularRoles_ThenRoleIdsFieldIsNotIndexed() throws Exception {

		when(mockDocument.hasField(UserIndexerFields.ROLE_IDS)).thenReturn(false);
		when(mockUserRoleService.getInheritedRegularRoleIds(mockUser)).thenReturn(Collections.emptySet());

		userPostProcessorIndexer.postProcessDocument(mockDocument, mockUser);

		verify(mockDocument, never()).addKeyword(eq(UserIndexerFields.ROLE_IDS), any(String[].class));

	}

	@Test
	public void postProcessDocument_WhenRoleIdsFieldIsIndexedAndUserHasNotRegularRoles_ThenRoleIdsFieldValuesAreIndexed() throws Exception {

		when(mockDocument.hasField(UserIndexerFields.ROLE_IDS)).thenReturn(true);
		when(mockDocument.getField(UserIndexerFields.ROLE_IDS)).thenReturn(mockField);
		String[] values = new String[] { "1", "2" };
		when(mockField.getValues()).thenReturn(values);

		when(mockUserRoleService.getInheritedRegularRoleIds(mockUser)).thenReturn(Collections.emptySet());

		userPostProcessorIndexer.postProcessDocument(mockDocument, mockUser);

		verify(mockDocument, times(1)).addKeyword(UserIndexerFields.ROLE_IDS, values);

	}

	@Test
	public void postProcessDocument_WhenRoleIdsFieldIsIndexedAndUserHasRegularRoles_ThenAllRoleIdsAreIndexed() throws Exception {

		when(mockDocument.hasField(UserIndexerFields.ROLE_IDS)).thenReturn(true);
		when(mockDocument.getField(UserIndexerFields.ROLE_IDS)).thenReturn(mockField);
		String[] values = new String[] { "1", "2" };
		when(mockField.getValues()).thenReturn(values);

		Set<Long> regularRoleIds = new HashSet<>();
		regularRoleIds.add(3L);
		regularRoleIds.add(4L);
		when(mockUserRoleService.getInheritedRegularRoleIds(mockUser)).thenReturn(regularRoleIds);

		String[] expected = new String[] { "1", "2", "3", "4" };

		userPostProcessorIndexer.postProcessDocument(mockDocument, mockUser);

		verify(mockDocument, times(1)).addKeyword(UserIndexerFields.ROLE_IDS, expected);

	}

	@Test
	public void postProcessDocument_WhenRoleIdsFieldIsNotIndexedAndUserHasRegularRoles_ThenRegularRoleIdsAreIndexed() throws Exception {

		when(mockDocument.hasField(UserIndexerFields.ROLE_IDS)).thenReturn(false);

		Set<Long> regularRoleIds = new HashSet<>();
		regularRoleIds.add(3L);
		regularRoleIds.add(4L);
		when(mockUserRoleService.getInheritedRegularRoleIds(mockUser)).thenReturn(regularRoleIds);

		String[] expected = new String[] { "3", "4" };

		userPostProcessorIndexer.postProcessDocument(mockDocument, mockUser);

		verify(mockDocument, times(1)).addKeyword(UserIndexerFields.ROLE_IDS, expected);

	}
}
