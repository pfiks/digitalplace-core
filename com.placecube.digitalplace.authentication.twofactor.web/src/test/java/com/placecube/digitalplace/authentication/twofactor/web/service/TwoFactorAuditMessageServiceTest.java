package com.placecube.digitalplace.authentication.twofactor.web.service;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.audit.AuditException;
import com.liferay.portal.kernel.audit.AuditMessage;
import com.liferay.portal.kernel.audit.AuditRouter;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.security.audit.event.generators.util.AuditMessageBuilder;
import com.placecube.digitalplace.authentication.twofactor.web.constants.TwoFactorAuditMessage;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CompanyThreadLocal.class, AuditMessageBuilder.class, DateUtil.class })
public class TwoFactorAuditMessageServiceTest extends PowerMockito {

	private static final long COMPANY_ID = 12;

	private static final long USER_ID = 345;

	@Mock
	private AuditMessage mockAuditMessage;

	@Mock
	private AuditRouter mockAuditRouter;

	@Mock
	private Date mockDate;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private User mockUser;

	@InjectMocks
	private TwoFactorAuditMessageService twoFactorAuditMessageService;

	@Before
	public void activateSetup() {
		mockStatic(CompanyThreadLocal.class, AuditMessageBuilder.class, DateUtil.class);
	}

	@Test
	public void addAuditMessage_WithUserIdParameter_WhenExceptionAddingTheAuditMessage_ThenNoExceptionIsThrown() throws AuditException {
		TwoFactorAuditMessage twoFactorAuditMessage = TwoFactorAuditMessage.INVALID_AUTH_CODE;
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(AuditMessageBuilder.buildAuditMessage(twoFactorAuditMessage.getActionType(), TwoFactorAuditMessageService.class.getName(), USER_ID, null)).thenReturn(mockAuditMessage);
		when(mockAuditMessage.getAdditionalInfo()).thenReturn(mockJSONObject);
		doThrow(new AuditException()).when(mockAuditRouter).route(mockAuditMessage);

		try {
			twoFactorAuditMessageService.addAuditMessage(USER_ID, twoFactorAuditMessage);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void addAuditMessage_WithUserIdParameter_WhenNoError_ThenAddsAnewAuditMessage() throws AuditException {
		TwoFactorAuditMessage twoFactorAuditMessage = TwoFactorAuditMessage.INVALID_SESSION;
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(AuditMessageBuilder.buildAuditMessage(twoFactorAuditMessage.getActionType(), TwoFactorAuditMessageService.class.getName(), USER_ID, null)).thenReturn(mockAuditMessage);
		when(mockAuditMessage.getAdditionalInfo()).thenReturn(mockJSONObject);
		when(DateUtil.newDate()).thenReturn(mockDate);

		twoFactorAuditMessageService.addAuditMessage(USER_ID, twoFactorAuditMessage);

		InOrder inOrder = inOrder(mockJSONObject, mockAuditMessage, mockAuditRouter);
		inOrder.verify(mockJSONObject, times(1)).put("userId", USER_ID);
		inOrder.verify(mockJSONObject, times(1)).put("date", mockDate);
		inOrder.verify(mockJSONObject, times(1)).put("reason", twoFactorAuditMessage.getMessage());
		inOrder.verify(mockAuditMessage, times(1)).setAdditionalInfo(mockJSONObject);
		inOrder.verify(mockAuditRouter, times(1)).route(mockAuditMessage);
	}

	@Test
	public void addAuditMessage_WithUserParameter_WhenExceptionAddingTheAuditMessage_ThenNoExceptionIsThrown() throws AuditException {
		String screenName = "screenNameValue";
		TwoFactorAuditMessage twoFactorAuditMessage = TwoFactorAuditMessage.INVALID_AUTH_CODE;
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getScreenName()).thenReturn(screenName);
		when(AuditMessageBuilder.buildAuditMessage(twoFactorAuditMessage.getActionType(), TwoFactorAuditMessageService.class.getName(), USER_ID, null)).thenReturn(mockAuditMessage);
		when(mockAuditMessage.getAdditionalInfo()).thenReturn(mockJSONObject);
		doThrow(new AuditException()).when(mockAuditRouter).route(mockAuditMessage);

		try {
			twoFactorAuditMessageService.addAuditMessage(mockUser, twoFactorAuditMessage);
		} catch (Exception e) {
			fail();
		}

	}

	@Test
	public void addAuditMessage_WithUserParameter_WhenNoError_ThenAddsAnewAuditMessage() throws AuditException {
		String screenName = "screenNameValue";
		TwoFactorAuditMessage twoFactorAuditMessage = TwoFactorAuditMessage.INVALID_AUTH_CODE;
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockUser.getScreenName()).thenReturn(screenName);
		when(AuditMessageBuilder.buildAuditMessage(twoFactorAuditMessage.getActionType(), TwoFactorAuditMessageService.class.getName(), USER_ID, null)).thenReturn(mockAuditMessage);
		when(mockAuditMessage.getAdditionalInfo()).thenReturn(mockJSONObject);
		when(DateUtil.newDate()).thenReturn(mockDate);

		twoFactorAuditMessageService.addAuditMessage(mockUser, twoFactorAuditMessage);

		InOrder inOrder = inOrder(mockJSONObject, mockAuditMessage, mockAuditRouter);
		inOrder.verify(mockJSONObject, times(1)).put("userId", USER_ID);
		inOrder.verify(mockJSONObject, times(1)).put("date", mockDate);
		inOrder.verify(mockJSONObject, times(1)).put("reason", twoFactorAuditMessage.getMessage());
		inOrder.verify(mockAuditMessage, times(1)).setAdditionalInfo(mockJSONObject);
		inOrder.verify(mockAuditRouter, times(1)).route(mockAuditMessage);
	}

}
