package com.placecube.digitalplace.group.rest.application.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestErrorMessage implements Serializable {

	private final String errorCode;
	private final int errorStatus;
	private final String stackTrace;

	RestErrorMessage(int errorStatus, String errorCode, String stackTrace) {
		this.errorStatus = errorStatus;
		this.errorCode = errorCode;
		this.stackTrace = stackTrace;
	}

	public static RestErrorMessage init(int errorStatus, String errorCode, String stackTrace) {
		return new RestErrorMessage(errorStatus, errorCode, stackTrace);
	}

	public String getErrorCode() {
		return errorCode;
	}

	public int getErrorStatus() {
		return errorStatus;
	}

	public String getStackTrace() {
		return stackTrace;
	}

}
