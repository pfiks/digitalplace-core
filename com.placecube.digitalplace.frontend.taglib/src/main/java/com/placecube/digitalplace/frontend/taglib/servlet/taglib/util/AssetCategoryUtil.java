package com.placecube.digitalplace.frontend.taglib.servlet.taglib.util;

import java.util.ArrayList;
import java.util.List;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;

public class AssetCategoryUtil {

	public static final String CATEGORY_SEPARATOR = "_CATEGORY_";

	public static long[] filterCategoryIds(long vocabularyId, long[] categoryIds) {

		List<Long> filteredCategoryIds = new ArrayList<>();

		for (long categoryId : categoryIds) {
			AssetCategory category = AssetCategoryLocalServiceUtil.fetchCategory(categoryId);

			if (category == null) {
				continue;
			}

			if (category.getVocabularyId() == vocabularyId) {
				filteredCategoryIds.add(category.getCategoryId());
			}
		}

		return ArrayUtil.toArray(filteredCategoryIds.toArray(new Long[0]));
	}

	public static String[] getCategoryIdsTitles(long[] categoryIdsArray, long vocabularyId, ThemeDisplay themeDisplay) {

		String categoryIds = StringPool.BLANK;
		String categoryNames = StringPool.BLANK;

		if (ArrayUtil.isNotEmpty(categoryIdsArray)) {

			if (vocabularyId > 0) {
				categoryIdsArray = filterCategoryIds(vocabularyId, categoryIdsArray);
			}

			if (categoryIdsArray.length > 0) {
				StringBundler categoryIdsSB = new StringBundler(categoryIdsArray.length * 2);
				StringBundler categoryNamesSB = new StringBundler(categoryIdsArray.length * 2);

				for (long categoryId : categoryIdsArray) {
					AssetCategory category = AssetCategoryLocalServiceUtil.fetchCategory(categoryId);

					if (category == null) {
						continue;
					}

					categoryIdsSB.append(categoryId);
					categoryIdsSB.append(StringPool.COMMA);

					categoryNamesSB.append(category.getTitle(themeDisplay.getLocale()));
					categoryNamesSB.append(CATEGORY_SEPARATOR);
				}

				if (categoryIdsSB.index() > 0) {
					categoryIdsSB.setIndex(categoryIdsSB.index() - 1);
					categoryNamesSB.setIndex(categoryNamesSB.index() - 1);

					categoryIds = categoryIdsSB.toString();
					categoryNames = categoryNamesSB.toString();
				}
			}
		}

		return new String[] { categoryIds, categoryNames };
	}

}