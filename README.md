# digitalplace-core

## Supported versions
* Liferay 7.4 update 60
* Java JDK 11

## Modules

### Address Modules
These modules provide support to lookup address by postcode. They are the following;

#### com.placecube.digitalplace.address.api, com.placecube.digitalplace.address.service
These modules provide the support for the `AddressLookup` connector and `AddressContext` remote service. The AddressLookup implementation needs to provide a bean configuration to function for the settings;
* Enabled. If checked, the connector will be available to use.
* National. If checked, the connector will be used as the fallback for address lookups, where an address lookup field allows a national address selection.
* Weight. Numeric value used to sort this connector relative to other address lookup connectors, national or local. If two local or two national connectors have the same weight value, there is no guarantee of which one will be used first. Lower weight connectors (e.g. 1) are used first.

Add the following to Control Panel > Service Access Policy > SYSTEM_DEFAULT

serviceClass: com.placecube.digitalplace.address.service.AddressContextService
Method name: searchAddressByPostcode

#### com.placecube.digitalplace.address.taglib
This module provides a standard taglib to be used by any portlet like `com.placecube.digitalplace.account.user.web`. It retrieves the addresses by invoking a JSON Web Service;

````
Liferay.Service(
	'/placecube_digitalplace.addresscontext/search-address-by-postcode',
	{
	companyId: Liferay.ThemeDisplay.getCompanyId(),
	postcode: postcodeValue,
	fallbackToNationalLookup: false
	},
	function(data) {

	}
);
````

#### com.placecube.digitalplace.addresslookup.mock
This module provides two mock implementations of `AddressLookup`, Local and National, and they can be enabled in System Settings > Platform > Connectors.

Both implementation generate the mock addresses on the fly depending on a valid postcode using the second digit to build them.

The local implementation generates unique UPRN for the value rage 1-5. For a valid local postcode as BS6 2SN, it will generates two addresses with unique UPRN: BS612SN, BS622SN.

For any postcode starts with `ZZ` or `NN` will return empty results to cover the scenario when there is no address for a valid postcode.

The national implementation generates unique UPRN for the value range 6-9. For a valid national postcode as NN6 7SN, it will generates two addresses with unique UPRN: NN667SN, NN677SN.

For any postcode starts with `ZZ` or not equal to `N` will return empty results to cover the scenario when there is no address for a valid postcode.

#### com.placecube.digitalplace.addresslookup.ordnancesurvey'
This module provides a real implementation of `AddressLookup` using _OS Places API_ and it can be enabled in System Settings > Platform > Connectors > Address lookup - Ordnance Survey

### com.placecube.digitalplace.breadcrumb.web
This module contains a breadcrumb portlet that provides a normal layout hierarchy based breadcrumb or category based one for use with web content display page templates.
On display page templates, a filter vocabulary can configured to control what categories should be used when building the breadcrumb. A category property can also be configured
and the value then used as the category breadcrumb entry's link destination.

### com.placecube.digitalplace.fragment.collection
Adds fragments for use in content pages e.g. banners.

### com.placecube.digitalplace.override.language.engb
Overrides the default language keys globally and in other applications (including your own).

**Note:** Global language key overrides for multiple locales require a separate module for each locale. See <https://help.liferay.com/hc/es/articles/360017886312-Overriding-Global-Language-Keys#create-a-resource-bundle-service-component>

### com.placecube.digitalplace.siteinitializeradmin.web
The module 'com.placecube.digitalplace.siteinitializeradmin.web' is a control panel portlet available in the Site Configuration section.
It allows the user to run a site initializer for the current group.

### com.placecube.digitalplace.webcontent.article
The module 'com.placecube.digitalplace.webcontent.article' contains the WebContent Strucuture 'Article' and exposes the 'ArticleWebContentCreator.configure' method that needs to
be invoked in order to create the structure in the group. It also includes services to retrieve the structure.

## License
Copyright (C) 2019-present Placecube Limited.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
