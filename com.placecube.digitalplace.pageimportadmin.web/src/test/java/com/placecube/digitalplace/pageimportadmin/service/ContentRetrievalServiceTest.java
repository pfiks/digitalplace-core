package com.placecube.digitalplace.pageimportadmin.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.digitalplace.pageimportadmin.model.WebContentImportModel;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest({ RestrictionsFactoryUtil.class, ProjectionFactoryUtil.class })
public class ContentRetrievalServiceTest extends PowerMockito {

	private static final Long ID = 11l;
	private static final String VALUE = "urlValue ";

	@InjectMocks
	private ContentRetrievalService contentRetrievalService;

	@Mock
	private AssetCategoryLocalService mockAssetCategoryLocalService;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private LayoutPageTemplateEntryLocalService mockLayoutPageTemplateEntryLocalService;

	@Mock
	private WebContentImportModel mockWebContentImportModel;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private Criterion mockCriterionId;

	@Mock
	private Criterion mockCriterionValue;

	@Mock
	private Projection mockProjection;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private Layout mockLayout;

	@Mock
	private DDMStructure mockDDMStructure1;

	@Mock
	private DDMStructure mockDDMStructure2;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(RestrictionsFactoryUtil.class, ProjectionFactoryUtil.class);
	}

	@Test
	public void doesArticleWithURLTitleExists_WhenArticleIsInvalid_ThenReturnsFalse() {
		when(mockWebContentImportModel.getUrl()).thenReturn(null);

		boolean result = contentRetrievalService.doesArticleWithURLTitleExists(mockWebContentImportModel);

		assertFalse(result);
	}

	@Test
	public void doesArticleWithURLTitleExists_WhenArticleIsFound_ThenReturnsTrue() {
		when(mockWebContentImportModel.getGroupId()).thenReturn(ID);
		when(mockWebContentImportModel.getUrl()).thenReturn(VALUE);
		when(mockJournalArticleLocalService.fetchArticleByUrlTitle(ID, VALUE)).thenReturn(mockJournalArticle);

		boolean result = contentRetrievalService.doesArticleWithURLTitleExists(mockWebContentImportModel);

		assertTrue(result);
	}

	@Test
	public void doesArticleWithURLTitleExists_WhenArticleNotFound_ThenReturnsFalse() {
		when(mockWebContentImportModel.getGroupId()).thenReturn(ID);
		when(mockWebContentImportModel.getUrl()).thenReturn(VALUE);
		when(mockJournalArticleLocalService.fetchArticleByUrlTitle(ID, VALUE)).thenReturn(null);

		boolean result = contentRetrievalService.doesArticleWithURLTitleExists(mockWebContentImportModel);

		assertFalse(result);
	}

	@Test
	public void getCategoriesByName_WhenNoError_ThenReturnsTheCategoryIds() {
		List<Object> expected = new ArrayList<>();
		expected.add(123l);
		expected.add(456l);
		when(mockAssetCategoryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockAssetCategoryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(expected);

		List<Long> results = contentRetrievalService.getCategoriesByName(ID, VALUE);

		assertThat(results, sameInstance(expected));
	}

	@Test
	public void getCategoriesByName_WhenNoError_ThenConfiguresTheDynamicQuery() {
		when(mockAssetCategoryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(RestrictionsFactoryUtil.eq("vocabularyId", ID)).thenReturn(mockCriterionId);
		when(RestrictionsFactoryUtil.eq("name", StringUtil.trim(VALUE))).thenReturn(mockCriterionValue);
		when(ProjectionFactoryUtil.property("categoryId")).thenReturn(mockProjection);

		contentRetrievalService.getCategoriesByName(ID, VALUE);

		InOrder inOrder = Mockito.inOrder(mockDynamicQuery, mockAssetCategoryLocalService);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionValue);
		inOrder.verify(mockDynamicQuery, times(1)).setProjection(mockProjection);
		inOrder.verify(mockAssetCategoryLocalService, times(1)).dynamicQuery(mockDynamicQuery);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLayout_WhenUrlIsInvalid_ThenReturnsEmptyOptional(boolean privateLayout) {
		Optional<Layout> result = contentRetrievalService.getLayout(ID, null, privateLayout);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLayout_WhenNoLayoutFound_ThenReturnsEmptyOptional(boolean privateLayout) {
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(ID, privateLayout, VALUE)).thenReturn(null);

		Optional<Layout> result = contentRetrievalService.getLayout(ID, VALUE, privateLayout);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLayout_WhenLayoutFound_ThenReturnsOptionalWithTheLayout(boolean privateLayout) {
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(ID, privateLayout, VALUE)).thenReturn(mockLayout);

		Optional<Layout> result = contentRetrievalService.getLayout(ID, VALUE, privateLayout);

		assertThat(result.get(), sameInstance(mockLayout));
	}

	@Test
	public void getPageTemplate_WhenNameIsInvalid_ThenReturnsEmptyOptional() {
		Optional<LayoutPageTemplateEntry> result = contentRetrievalService.getPageTemplate(ID, null);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getPageTemplate_WhenNoTemplateFound_ThenReturnsEmptyOptional() {
		when(mockLayoutPageTemplateEntryLocalService.fetchLayoutPageTemplateEntry(ID, VALUE)).thenReturn(null);

		Optional<LayoutPageTemplateEntry> result = contentRetrievalService.getPageTemplate(ID, VALUE);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getPageTemplate_WhenTemplateFound_ThenReturnsOptionalWithTheLayoutPageTemplateEntry() {
		when(mockLayoutPageTemplateEntryLocalService.fetchLayoutPageTemplateEntry(ID, VALUE)).thenReturn(mockLayoutPageTemplateEntry);

		Optional<LayoutPageTemplateEntry> result = contentRetrievalService.getPageTemplate(ID, VALUE);

		assertThat(result.get(), sameInstance(mockLayoutPageTemplateEntry));
	}

	@Test
	public void getWebContentStructure_WhenStructureIsFound_ThenReturnsOptionalWithTheStructure() {
		List<Object> expected = new ArrayList<>();
		expected.add(mockDDMStructure1);
		expected.add(mockDDMStructure2);
		when(mockDDMStructureLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockDDMStructureLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(expected);
		when(mockDDMStructure1.getDefaultLanguageId()).thenReturn("languageId1");
		when(mockDDMStructure1.getName("languageId1")).thenReturn(VALUE);

		Optional<DDMStructure> result = contentRetrievalService.getWebContentStructure(ID, VALUE);

		assertThat(result.get(), sameInstance(mockDDMStructure1));
	}

	@Test
	public void getWebContentStructure_WhenStructureIsNotFound_ThenReturnsEmptyOptional() {
		List<Object> expected = new ArrayList<>();
		expected.add(mockDDMStructure1);
		when(mockDDMStructureLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockDDMStructureLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(expected);
		when(mockDDMStructure1.getDefaultLanguageId()).thenReturn("languageId1");
		when(mockDDMStructure1.getName("languageId1")).thenReturn("differentValue");

		Optional<DDMStructure> result = contentRetrievalService.getWebContentStructure(ID, VALUE);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getWebContentStructure_WhenNoError_ThenConfiguresTheDynamicQuery() {
		when(mockDDMStructureLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(RestrictionsFactoryUtil.eq("groupId", ID)).thenReturn(mockCriterionId);
		when(RestrictionsFactoryUtil.like("name", "%" + VALUE + "%")).thenReturn(mockCriterionValue);

		contentRetrievalService.getWebContentStructure(ID, VALUE);

		InOrder inOrder = Mockito.inOrder(mockDynamicQuery, mockDDMStructureLocalService);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionValue);
		inOrder.verify(mockDDMStructureLocalService, times(1)).dynamicQuery(mockDynamicQuery);
	}

}
