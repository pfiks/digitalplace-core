package com.placecube.digitalplace.dataconsumer.model.impl;

import com.placecube.digitalplace.dataconsumer.model.DataProviderOutputParameter;

public class DataProviderOutputParameterImpl implements DataProviderOutputParameter {

	private String name;
	private String path;
	private String type;

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public String getType() {
		return type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setType(String type) {
		this.type = type;
	}

}
