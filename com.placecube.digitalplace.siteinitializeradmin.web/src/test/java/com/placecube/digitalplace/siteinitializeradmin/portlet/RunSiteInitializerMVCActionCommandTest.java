package com.placecube.digitalplace.siteinitializeradmin.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.site.exception.InitializationException;
import com.placecube.digitalplace.siteinitializer.setuputils.service.SiteInitializerSetupService;
import com.placecube.digitalplace.siteinitializeradmin.constants.PortletRequestKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class })
@SuppressStaticInitializationFor({ "com.liferay.portal.kernel.util.ParamUtil" })
public class RunSiteInitializerMVCActionCommandTest extends PowerMockito {

	@Mock
	private ActionRequest mockActionRequest;

	@Mock
	private ActionResponse mockActionResponse;

	@Mock
	private Portal mockPortal;

	@Mock
	private SiteInitializerSetupService mockSiteInitializerSetupService;

	@InjectMocks
	private RunSiteInitializerMVCActionCommand runSiteInitializerMVCActionCommand;

	@Before
	public void activateSetUp() {
		mockStatic(ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void doProcessAction_WhenExceptionExecutingTheSiteInitializer_ThenThrowsPortletException() throws Exception {
		String siteInitializerKey = "siteInitializerKeyValue";
		long groupId = 11l;
		long userId = 2;
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.SELECTED_SITE_INITIALIZER)).thenReturn(siteInitializerKey);
		when(mockPortal.getScopeGroupId(mockActionRequest)).thenReturn(groupId);
		when(mockPortal.getUserId(mockActionRequest)).thenReturn(userId);
		doThrow(new InitializationException()).when(mockSiteInitializerSetupService).runSiteInitialiser(userId, groupId, siteInitializerKey);

		runSiteInitializerMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test(expected = PortletException.class)
	public void doProcessAction_WhenExceptionRetrievingTheGroupId_ThenThrowsPortletException() throws Exception {
		when(mockPortal.getScopeGroupId(mockActionRequest)).thenThrow(new PortalException());

		runSiteInitializerMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);
	}

	@Test
	public void doProcessAction_WhenNoError_ThenRunsTheSiteInitialiser() throws Exception {
		String siteInitializerKey = "siteInitializerKeyValue";
		Long groupId = 11l;
		long userId = 2;
		when(ParamUtil.getString(mockActionRequest, PortletRequestKeys.SELECTED_SITE_INITIALIZER)).thenReturn(siteInitializerKey);
		when(mockPortal.getScopeGroupId(mockActionRequest)).thenReturn(groupId);
		when(mockPortal.getUserId(mockActionRequest)).thenReturn(userId);

		runSiteInitializerMVCActionCommand.doProcessAction(mockActionRequest, mockActionResponse);

		verify(mockSiteInitializerSetupService, times(1)).runSiteInitialiser(userId, groupId, siteInitializerKey);
	}

}
