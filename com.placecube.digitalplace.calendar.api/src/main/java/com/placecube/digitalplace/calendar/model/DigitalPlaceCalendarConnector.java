package com.placecube.digitalplace.calendar.model;

import java.time.LocalDateTime;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public interface DigitalPlaceCalendarConnector {

	String getBundleId();

	List<DigitalPlaceCalendarEvent> getCalendarEvents(long userId, long[] groupIds, LocalDateTime startDate, LocalDateTime endDate, ThemeDisplay themeDisplay) throws PortalException;
}