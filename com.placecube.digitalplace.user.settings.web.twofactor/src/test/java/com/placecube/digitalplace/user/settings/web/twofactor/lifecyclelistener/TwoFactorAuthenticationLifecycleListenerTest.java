package com.placecube.digitalplace.user.settings.web.twofactor.lifecyclelistener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.placecube.digitalplace.user.settings.web.service.UserSettingsService;
import com.placecube.digitalplace.user.settings.web.twofactor.constants.TwoFactorAuthenticationKeys;

public class TwoFactorAuthenticationLifecycleListenerTest extends PowerMockito {

	@Mock
	private Company mockCompany;

	@Mock
	private UserSettingsService mockUserSettingsService;

	@InjectMocks
	private TwoFactorAuthenticationLifecycleListener twoFactorAuthenticationLifecycleListener;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void portalInstanceRegistered_WhenExceptionCreatingTheArticle_ThenThrowsPortalException() throws Exception {
		doThrow(new PortalException()).when(mockUserSettingsService)
				.configureUserSettingsWebContentArticle(mockCompany, TwoFactorAuthenticationLifecycleListener.class, "dependencies/webcontent.xml", TwoFactorAuthenticationKeys.WEBCONTENT_ARTICLE_ID,
						"2FA");

		twoFactorAuthenticationLifecycleListener.portalInstanceRegistered(mockCompany);
	}

	@Test
	public void portalInstanceRegistered_WhenNoError_ThenCreatesTheArticle() throws Exception {
		twoFactorAuthenticationLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockUserSettingsService, times(1))
				.configureUserSettingsWebContentArticle(mockCompany, TwoFactorAuthenticationLifecycleListener.class, "dependencies/webcontent.xml", TwoFactorAuthenticationKeys.WEBCONTENT_ARTICLE_ID,
						"2FA");
	}

}
