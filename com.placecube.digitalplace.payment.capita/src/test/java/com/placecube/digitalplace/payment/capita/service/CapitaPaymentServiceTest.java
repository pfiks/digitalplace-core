package com.placecube.digitalplace.payment.capita.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Dictionary;
import java.util.Optional;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.CompanyConstants;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.placecube.digitalplace.payment.capita.configuration.CapitaCompanyConfiguration;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;
import com.placecube.digitalplace.payment.capitainterface.service.PaymentService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@Ignore("Disabled due to tests not being compatible with builds on JDK11")
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class CapitaPaymentServiceTest extends PowerMockito {

	private static final String CAPITA_ENDPOINT_URL = "https://sbsctest.e-paycapita.com:443/scp/scpws";
	private static final String CAPITA_HMAC_KEY = "0kvRVpMjRq+anPskP8cGmDtPR6PjpIZfLtHo8KR6BIHpsGmrRH64PswYxUGNI+KyBMNpWNUi+EZ2CE4T7OT2Ag==";
	private static final String CAPITA_HMAC_KEY_ID = "456";
	private static final String CAPITA_SCP_ID = "8389789";
	private static final String CAPITA_SITE_ID = "84";
	private static final long COMPANY_ID = 10;

	@InjectMocks
	private CapitaPaymentService capitaPaymentService;

	@Mock
	private CapitaCompanyConfiguration mockCapitaCompanyConfiguration;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private Dictionary<String, ?> mockDictionary;

	@Mock(name = "paymentService")
	private PaymentService mockPaymentService;

	@Mock
	private ServiceContext mockServiceContext;

	@Test(expected = ConfigurationException.class)
	public void getDefaultAdvancedConfiguration_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(CapitaCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		capitaPaymentService.getDefaultAdvancedConfiguration(COMPANY_ID);
	}

	@Test
	public void getDefaultAdvancedConfiguration_WhenNoErrorAndThereIsAConfigValue_ThenReturnsOptionalWithTheConfiguration() throws ConfigurationException {
		mockGetCompanyConfiguration();
		when(mockCapitaCompanyConfiguration.defaultAdvancedConfiguration()).thenReturn("config1");

		Optional<String> result = capitaPaymentService.getDefaultAdvancedConfiguration(COMPANY_ID);

		assertThat(result.get(), equalTo("config1"));
	}

	@Test
	public void getDefaultAdvancedConfiguration_WhenNoErrorAndThereIsNoConfigValue_ThenReturnsEmptyOptional() throws ConfigurationException {
		mockGetCompanyConfiguration();
		when(mockCapitaCompanyConfiguration.defaultAdvancedConfiguration()).thenReturn(null);

		Optional<String> result = capitaPaymentService.getDefaultAdvancedConfiguration(COMPANY_ID);

		assertFalse(result.isPresent());
	}

	@Test
	public void getPaymentService_WhenPaymentServiceExists_ThenReturnExistingPaymentService() throws Exception {
		final PaymentProperties paymentProperties = new PaymentProperties();
		mockGetCompanyConfiguration();
		mockSetPaymentProperties(paymentProperties);

		capitaPaymentService.updated(StringPool.BLANK, mockDictionary);
		PaymentService actual = capitaPaymentService.getPaymentService(mockServiceContext);

		assertNotNull(actual);
	}

	@Test(expected = ConfigurationException.class)
	public void isEnabled_WhenExceptionRetrievingConfiguration_ThenThrowsConfigurationException() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(CapitaCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		capitaPaymentService.isEnabled(COMPANY_ID);
	}

	@Test
	@Parameters({ "true", "false" })
	public void isEnabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabledOrNot(boolean expected) throws ConfigurationException {
		mockGetCompanyConfiguration();
		when(mockCapitaCompanyConfiguration.enabled()).thenReturn(expected);

		boolean actual = capitaPaymentService.isEnabled(COMPANY_ID);

		assertThat(actual, equalTo(expected));
	}

	@Test
	public void update_WhenInstanceCompanyIdIsPresent_ThenReturnsIfTheConfigurationIsEnabledOrNot() throws Exception {
		when(GetterUtil.getLong(mockDictionary.get("companyId"), CompanyConstants.SYSTEM)).thenReturn(COMPANY_ID);
		final PaymentProperties paymentProperties = new PaymentProperties();
		mockGetCompanyConfiguration();
		mockSetPaymentProperties(paymentProperties);

		capitaPaymentService.updated(StringPool.BLANK, mockDictionary);

		verify(mockConfigurationProvider).getCompanyConfiguration(CapitaCompanyConfiguration.class, COMPANY_ID);
		verify(mockCapitaCompanyConfiguration).capitaEndpointURL();
		verify(mockCapitaCompanyConfiguration).capitaHMACKey();
		verify(mockCapitaCompanyConfiguration).capitaHMACKeyId();
		verify(mockCapitaCompanyConfiguration).capitaSCPId();
		verify(mockCapitaCompanyConfiguration).capitaSiteId();
	}

	@Test
	public void update_WhenSystemCompanyIdIsPresent_ThenReturnsIfTheConfigurationIsEnabledOrNot() throws Exception {
		when(GetterUtil.getLong(mockDictionary.get("companyId"), CompanyConstants.SYSTEM)).thenReturn(CompanyConstants.SYSTEM);
		final PaymentProperties paymentProperties = new PaymentProperties();
		mockGetCompanyConfiguration();
		mockSetPaymentProperties(paymentProperties);

		capitaPaymentService.updated(StringPool.BLANK, mockDictionary);

		verify(mockConfigurationProvider, never()).getCompanyConfiguration(any(), anyLong());
		verify(mockCapitaCompanyConfiguration, never()).capitaEndpointURL();
		verify(mockCapitaCompanyConfiguration, never()).capitaHMACKey();
		verify(mockCapitaCompanyConfiguration, never()).capitaHMACKeyId();
		verify(mockCapitaCompanyConfiguration, never()).capitaSCPId();
		verify(mockCapitaCompanyConfiguration, never()).capitaSiteId();
	}

	private void mockGetCompanyConfiguration() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(CapitaCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockCapitaCompanyConfiguration);
	}

	private void mockSetPaymentProperties(final PaymentProperties paymentProperties) throws Exception {
		PowerMockito.whenNew(PaymentProperties.class).withNoArguments().thenReturn(paymentProperties);
		when(mockCapitaCompanyConfiguration.capitaEndpointURL()).thenReturn(CAPITA_ENDPOINT_URL);
		when(mockCapitaCompanyConfiguration.capitaHMACKey()).thenReturn(CAPITA_HMAC_KEY);
		when(mockCapitaCompanyConfiguration.capitaHMACKeyId()).thenReturn(CAPITA_HMAC_KEY_ID);
		when(mockCapitaCompanyConfiguration.capitaSCPId()).thenReturn(CAPITA_SCP_ID);
		when(mockCapitaCompanyConfiguration.capitaSiteId()).thenReturn(CAPITA_SITE_ID);
	}

}
