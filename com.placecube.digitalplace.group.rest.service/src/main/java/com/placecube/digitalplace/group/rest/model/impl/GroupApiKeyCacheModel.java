/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.group.rest.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.placecube.digitalplace.group.rest.model.GroupApiKey;
import com.placecube.digitalplace.group.rest.service.persistence.GroupApiKeyPK;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing GroupApiKey in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class GroupApiKeyCacheModel
	implements CacheModel<GroupApiKey>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof GroupApiKeyCacheModel)) {
			return false;
		}

		GroupApiKeyCacheModel groupApiKeyCacheModel =
			(GroupApiKeyCacheModel)object;

		if (groupApiKeyPK.equals(groupApiKeyCacheModel.groupApiKeyPK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, groupApiKeyPK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", applicationKey=");
		sb.append(applicationKey);
		sb.append(", applicationSecret=");
		sb.append(applicationSecret);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public GroupApiKey toEntityModel() {
		GroupApiKeyImpl groupApiKeyImpl = new GroupApiKeyImpl();

		groupApiKeyImpl.setCompanyId(companyId);
		groupApiKeyImpl.setGroupId(groupId);

		if (applicationKey == null) {
			groupApiKeyImpl.setApplicationKey("");
		}
		else {
			groupApiKeyImpl.setApplicationKey(applicationKey);
		}

		if (applicationSecret == null) {
			groupApiKeyImpl.setApplicationSecret("");
		}
		else {
			groupApiKeyImpl.setApplicationSecret(applicationSecret);
		}

		groupApiKeyImpl.resetOriginalValues();

		return groupApiKeyImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();
		applicationKey = (String)objectInput.readObject();
		applicationSecret = (String)objectInput.readObject();

		groupApiKeyPK = new GroupApiKeyPK(companyId, groupId);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		if (applicationKey == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(applicationKey);
		}

		if (applicationSecret == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(applicationSecret);
		}
	}

	public long companyId;
	public long groupId;
	public String applicationKey;
	public String applicationSecret;
	public transient GroupApiKeyPK groupApiKeyPK;

}