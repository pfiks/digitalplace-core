package com.placecube.digitalplace.dataconsumer.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = DataConsumerWebClientUtils.class)
public class DataConsumerWebClientUtils {

	public void appendBody(HttpURLConnection conn, String jsonBody) throws IOException {
		if (jsonBody != null) {
			conn.setDoOutput(true);
			OutputStream os = conn.getOutputStream();
			os.write(jsonBody.getBytes());
			os.flush();
		}
	}

	public String getEncodedParameter(String parameter) {
		String encodedParameter = URLEncoder.encode(parameter, StandardCharsets.UTF_8);
		return Validator.isNotNull(encodedParameter) ? encodedParameter.replace(StringPool.QUOTE, StringPool.BLANK) : StringPool.BLANK;
	}

	public String getParamString(Map<String, String> parameterMap) {
		StringBuilder stringBuilder = new StringBuilder(100);
		if (parameterMap.size() > 0) {
			stringBuilder.append("?");
			parameterMap.entrySet().forEach(f -> {
				stringBuilder.append(f.getKey() + "=" + getEncodedParameter(f.getValue()) + "&");
			});
		}
		String paramString = stringBuilder.toString();

		return paramString.length() > 0 ? paramString.substring(0, paramString.length() - 1) : paramString;
	}

	public boolean isHttpMethodRequiresBody(String httpMethod) {
		return httpMethod.equals("POST") || httpMethod.equals("PUT") || httpMethod.equals("PATCH");
	}

	public HttpURLConnection openConnection(String urlString) throws IOException {
		if (!urlString.contains("://")) {
			urlString = "http://" + urlString;
		}
		return (HttpURLConnection) new URL(urlString).openConnection();
	}

	public String readConnectionOutput(HttpURLConnection conn) throws IOException {
		int responseCode = conn.getResponseCode();

		if (HttpURLConnection.HTTP_OK == responseCode) {
			return getSuccessfulResponse(conn);
		} else {
			throw new IOException(getErrorResponse(conn));
		}
	}

	private String getErrorResponse(HttpURLConnection conn) throws IOException {
		BufferedReader errorBr = new BufferedReader(new InputStreamReader(conn.getErrorStream()));

		String errorOutput;
		String error = StringPool.BLANK;
		while ((errorOutput = errorBr.readLine()) != null) {
			error += errorOutput.trim() + StringPool.SPACE;
		}
		return error.trim();
	}

	private String getSuccessfulResponse(HttpURLConnection conn) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

		String output;
		String result = StringPool.BLANK;
		while ((output = br.readLine()) != null) {
			result += output.trim() + StringPool.SPACE;
		}
		return result.trim();
	}

}
