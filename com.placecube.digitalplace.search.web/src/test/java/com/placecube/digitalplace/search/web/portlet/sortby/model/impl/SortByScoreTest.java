package com.placecube.digitalplace.search.web.portlet.sortby.model.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.placecube.digitalplace.common.language.AggregatedResourceBundleUtil;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributorSettings;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AggregatedResourceBundleUtil.class, SortFactoryUtil.class })
@SuppressStaticInitializationFor("com.liferay.portal.kernel.search.SortFactoryUtil")
public class SortByScoreTest extends PowerMockito {

	@Mock
	private SharedSearchContributorSettings mockSharedSearchContributorSettings;

	@Mock
	private Sort mockSort;

	@InjectMocks
	private SortByScore sortByScore;

	@Before
	public void activateSetup() {
		mockStatic(SortFactoryUtil.class, AggregatedResourceBundleUtil.class);
	}

	@Test
	public void getId_WhenNoError_ThenReturnsTheId() {
		String result = sortByScore.getId();

		assertThat(result, equalTo("sortByScore"));
	}

	@Test
	public void getLabel_WhenNoError_ThenReturnsLabel() {
		String expected = "exp";
		Locale locale = Locale.FRENCH;
		when(AggregatedResourceBundleUtil.get("relevance", locale, "com.placecube.digitalplace.search.web")).thenReturn(expected);

		String result = sortByScore.getLabel(locale);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getSort_WhenUseOppositeSortDirectionIsFalse_ThenReturnsTheSortsWithSortOrderFalse() {
		when(SortFactoryUtil.create(null, Sort.SCORE_TYPE, false)).thenReturn(mockSort);

		Sort[] results = sortByScore.getSorts(mockSharedSearchContributorSettings, false);

		assertThat(results.length, equalTo(1));
		assertThat(results[0], equalTo(mockSort));
	}

	@Test
	public void getSort_WhenUseOppositeSortDirectionIsTrue_ThenReturnsTheSortsWithSortOrderTrue() {
		when(SortFactoryUtil.create(null, Sort.SCORE_TYPE, true)).thenReturn(mockSort);

		Sort[] results = sortByScore.getSorts(mockSharedSearchContributorSettings, true);

		assertThat(results.length, equalTo(1));
		assertThat(results[0], equalTo(mockSort));
	}

}
