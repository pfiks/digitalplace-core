package com.placecube.digitalplace.userindexer.indexer;

import java.util.List;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.BaseIndexerPostProcessor;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.IndexerPostProcessor;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.placecube.digitalplace.userindexer.constants.UserIndexerFields;
import com.placecube.digitalplace.userindexer.internal.service.UserRoleService;

@Component(immediate = true, property = { "indexer.class.name=com.liferay.portal.kernel.model.User" }, service = IndexerPostProcessor.class)
public class UserPostProcessorIndexer extends BaseIndexerPostProcessor {

	private static final Log LOG = LogFactoryUtil.getLog(UserPostProcessorIndexer.class);

	@Reference
	private UserRoleService userRoleService;

	@Override
	public void postProcessDocument(Document document, Object obj) throws Exception {
		User user = (User) obj;

		addGroupRoles(document, user);
		addInheritedRegularRoles(document, user);
	}

	private void addGroupRoles(Document document, User user) {

		List<String> groupRoleIds = userRoleService.getGroupRoleIdsFieldValues(user);

		if (ListUtil.isNotEmpty(groupRoleIds)) {

			document.addText(UserIndexerFields.GROUP_ROLE_IDS, groupRoleIds.toArray(new String[groupRoleIds.size()]));

			LOG.debug("Indexing userId: " + user.getUserId() + " with groupRoleIds: " + groupRoleIds.toString());
		}
	}

	private void addInheritedRegularRoles(Document document, User user) {
		String[] regularRoleIds = new String[0];
		if (document.hasField(UserIndexerFields.ROLE_IDS)) {
			Field field = document.getField(UserIndexerFields.ROLE_IDS);
			regularRoleIds = field.getValues();
		}

		Set<Long> inheritedUserGroupRoles = userRoleService.getInheritedRegularRoleIds(user);

		for (long userGroupRoleId : inheritedUserGroupRoles) {
			regularRoleIds = ArrayUtil.append(regularRoleIds, String.valueOf(userGroupRoleId));
		}

		if (ArrayUtil.isNotEmpty(regularRoleIds)) {
			document.addKeyword(UserIndexerFields.ROLE_IDS, regularRoleIds);
		}
	}

}
