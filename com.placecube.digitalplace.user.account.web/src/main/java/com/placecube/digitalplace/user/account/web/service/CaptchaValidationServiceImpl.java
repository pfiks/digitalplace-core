package com.placecube.digitalplace.user.account.web.service;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.captcha.util.CaptchaUtil;
import com.liferay.portal.kernel.captcha.CaptchaException;
import com.placecube.digitalplace.user.account.constants.AccountValidatedField;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.CaptchaValidationService;

@Component(immediate = true, service = CaptchaValidationService.class)
public class CaptchaValidationServiceImpl implements CaptchaValidationService {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Override
	public void validateCaptcha(AccountContext accountContext, AccountValidatedField captchaField, PortletRequest portletRequest) {

		if (accountConfigurationService.isCaptchaEnabled(portletRequest)) {
			try {
				if (!accountContext.isCreatorSignedIn()) {
					CaptchaUtil.check(portletRequest);
				}
			} catch (CaptchaException e) {
				accountContext.addFieldError(captchaField.getFieldName(), captchaField.getMandatoryErrorKey());
			}
		}
	}
}

