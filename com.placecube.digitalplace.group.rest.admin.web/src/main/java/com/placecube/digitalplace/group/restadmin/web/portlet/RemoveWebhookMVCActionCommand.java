package com.placecube.digitalplace.group.restadmin.web.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.placecube.digitalplace.group.rest.service.GroupWebhookLocalService;
import com.placecube.digitalplace.group.restadmin.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.group.restadmin.web.constants.PortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.GROUP_WEBHOOKS, "mvc.command.name=" + MVCCommandKeys.REMOVE }, service = MVCActionCommand.class)
public class RemoveWebhookMVCActionCommand extends BaseMVCActionCommand {

	@Reference
	private GroupWebhookLocalService groupWebhookLocalService;

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		long webhookId = ParamUtil.getLong(actionRequest, "webhookId");

		groupWebhookLocalService.deleteGroupWebhook(webhookId);
	}

}
