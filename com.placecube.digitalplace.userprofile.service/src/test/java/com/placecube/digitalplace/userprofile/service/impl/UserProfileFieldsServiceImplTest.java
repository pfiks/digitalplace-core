package com.placecube.digitalplace.userprofile.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfigurationFactory;
import com.placecube.digitalplace.userprofile.model.ExpandoField;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class UserProfileFieldsServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 10;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Mock
	private ExpandoColumnLocalService mockExpandoColumnLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private UserProfileFieldsConfiguration mockUserProfileFieldsConfiguration;

	@Mock
	private UserProfileFieldsConfigurationFactory userProfileFieldsConfigurationFactory;

	@InjectMocks
	private UserProfileFieldsServiceImpl userProfileFieldsServiceImpl;

	@Test
	public void getConfiguration_WhenNoError_ThenReturnsTheCompanyConfiguration() throws PortalException {

		when(userProfileFieldsConfigurationFactory.getCompanyUserProfileFieldsConfiguration(COMPANY_ID)).thenReturn(mockUserProfileFieldsConfiguration);

		UserProfileFieldsConfiguration result = userProfileFieldsServiceImpl.getCompanyUserProfileFieldsConfiguration(COMPANY_ID);

		assertThat(result, sameInstance(mockUserProfileFieldsConfiguration));
	}

	@Test
	public void getConfiguredExpandoFields_WhenConfiguredExpandoFieldsSettingIsEmpty_ThenReturnsEmptySet() {
		when(mockUserProfileFieldsConfiguration.expandoFieldsSettings()).thenReturn(new String[0]);

		Set<ExpandoField> results = userProfileFieldsServiceImpl.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration);

		assertTrue(results.isEmpty());
	}

	@Test
	public void getConfiguredExpandoFields_WhenConfiguredExpandoFieldsSettingIsEmptyStringArray_ThenReturnsEmptySet() {
		String expandoFieldsArray[] = new String[0];

		Set<ExpandoField> results = userProfileFieldsServiceImpl.getConfiguredExpandoFields(COMPANY_ID, expandoFieldsArray);

		assertTrue(results.isEmpty());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getConfiguredExpandoFields_WhenConfiguredExpandoFieldsSettingIsNotEmptyStringArray_ThenReturnsAsetWithAllTheAvailableExpandoFields(boolean expandoRequired) {
		String expandoFieldsArray[] = new String[] { "validExpandoName1=" + expandoRequired, "invalidExpando1=" + expandoRequired, StringPool.BLANK };

		when(mockExpandoColumnLocalService.getColumn(COMPANY_ID, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME, "validExpandoName1")).thenReturn(mockExpandoColumn);
		when(mockExpandoColumnLocalService.getColumn(COMPANY_ID, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME, "invalidExpando1")).thenReturn(null);
		when(mockExpandoColumn.getName()).thenReturn("expandoColumnName");
		when(mockExpandoColumn.getColumnId()).thenReturn(123l);

		Set<ExpandoField> results = userProfileFieldsServiceImpl.getConfiguredExpandoFields(COMPANY_ID, expandoFieldsArray);

		assertThat(results.size(), equalTo(1));
		ExpandoField firstResult = results.iterator().next();
		assertThat(firstResult.isRequired(), equalTo(expandoRequired));
		assertThat(firstResult.getExpandoFieldKey(), equalTo("expandoColumnName"));
		assertThat(firstResult.getExpandoColumnId(), equalTo(123l));
	}

	@Test
	public void getConfiguredExpandoFields_WhenConfiguredExpandoFieldsSettingIsNull_ThenReturnsEmptySet() {
		when(mockUserProfileFieldsConfiguration.expandoFieldsSettings()).thenReturn(null);

		Set<ExpandoField> results = userProfileFieldsServiceImpl.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration);

		assertTrue(results.isEmpty());
	}

	@Test
	public void getConfiguredExpandoFields_WhenConfiguredExpandoFieldsSettingStringArrayIsNull_ThenReturnsEmptySet() {
		String expandoFieldsArray[] = null;

		Set<ExpandoField> results = userProfileFieldsServiceImpl.getConfiguredExpandoFields(COMPANY_ID, expandoFieldsArray);

		assertTrue(results.isEmpty());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getConfiguredExpandoFields_WhenNoError_ThenReturnsAsetWithAllTheAvailableExpandoFields(boolean expandoRequired) {
		when(mockUserProfileFieldsConfiguration.expandoFieldsSettings()).thenReturn(new String[] { "validExpandoName1=" + expandoRequired, "invalidExpando1=" + expandoRequired, StringPool.BLANK });
		when(mockExpandoColumnLocalService.getColumn(COMPANY_ID, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME, "validExpandoName1")).thenReturn(mockExpandoColumn);
		when(mockExpandoColumnLocalService.getColumn(COMPANY_ID, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME, "invalidExpando1")).thenReturn(null);
		when(mockExpandoColumn.getName()).thenReturn("expandoColumnName");
		when(mockExpandoColumn.getColumnId()).thenReturn(123l);

		Set<ExpandoField> results = userProfileFieldsServiceImpl.getConfiguredExpandoFields(COMPANY_ID, mockUserProfileFieldsConfiguration);

		assertThat(results.size(), equalTo(1));
		ExpandoField firstResult = results.iterator().next();
		assertThat(firstResult.isRequired(), equalTo(expandoRequired));
		assertThat(firstResult.getExpandoFieldKey(), equalTo("expandoColumnName"));
		assertThat(firstResult.getExpandoColumnId(), equalTo(123l));
	}

}