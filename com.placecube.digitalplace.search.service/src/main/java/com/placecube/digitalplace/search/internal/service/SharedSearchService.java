package com.placecube.digitalplace.search.internal.service;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.osgi.service.tracker.collections.map.ServiceTrackerMap;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.search.internal.model.SharedSearchSession;
import com.placecube.digitalplace.search.internal.util.RequestUtil;
import com.placecube.digitalplace.search.internal.util.RetrievalUtil;
import com.placecube.digitalplace.search.internal.util.SearchUtil;
import com.placecube.digitalplace.search.shared.model.SharedSearchContributor;
import com.placecube.digitalplace.search.shared.model.SharedSearchResponse;
import com.placecube.digitalplace.search.shared.model.impl.SharedSearchContributorSettingsImpl;
import com.placecube.digitalplace.search.shared.model.impl.SharedSearchResponseImpl;

@Component(service = SharedSearchService.class)
public class SharedSearchService {

	@Reference
	private RequestUtil requestUtil;

	@Reference
	private RetrievalUtil retrievalUtil;

	@Reference
	private SearchUtil searchUtil;

	public SharedSearchResponse doSearch(RenderRequest renderRequest, ServiceTrackerMap<String, SharedSearchContributor> sharedSearchContributors) throws SearchException {
		ThemeDisplay themeDisplay = requestUtil.getThemeDisplay(renderRequest);

		SearchContext searchContext = searchUtil.getBaseSearchContext(themeDisplay);
		HttpServletRequest originalServletRequest = requestUtil.getOriginalHttpServletRequest(renderRequest);

		SharedSearchSession sharedSearchSession = requestUtil.getOrCreateSharedSearchSession(originalServletRequest, themeDisplay);
		SharedSearchContributorSettingsImpl contributorSetting = SharedSearchContributorSettingsImpl.init(searchUtil, themeDisplay, searchContext, sharedSearchSession);

		Stream<Portlet> portletsOnPage = getPortlets(themeDisplay.getLayout(), themeDisplay.getCompanyId());
		portletsOnPage.forEach(portletOnPage -> applyPortletContributor(themeDisplay, portletOnPage, sharedSearchContributors, contributorSetting));

		SearchContainer<Object> searchContainer = searchUtil.getBaseSearchContainer(renderRequest, originalServletRequest, contributorSetting);
		Hits hits = searchUtil.executeSearch(searchContext, searchContainer);
		return SharedSearchResponseImpl.init(hits, searchContext, searchContainer, sharedSearchSession);
	}

	private void applyPortletContributor(ThemeDisplay themeDisplay, Portlet portlet, ServiceTrackerMap<String, SharedSearchContributor> searchContributors,
			SharedSearchContributorSettingsImpl sharedSearchContributorSetting) {
		SharedSearchContributor contributorForPortlet = searchContributors.getService(portlet.getPortletName());

		if (Validator.isNotNull(contributorForPortlet)) {
			Optional<PortletPreferences> portletPreferencesOptional = retrievalUtil.getPortletPreferences(portlet, themeDisplay);
			sharedSearchContributorSetting.setPortletPreferences(portletPreferencesOptional);
			contributorForPortlet.contribute(sharedSearchContributorSetting);
		}
	}

	private Stream<Portlet> getPortlets(Layout layout, long companyId) {
		Stream<Portlet> portlets = retrievalUtil.getAllPortletsOnLayout(layout);

		if (Objects.equals(layout.getType(), LayoutConstants.TYPE_PORTLET)) {
			return portlets;
		}

		return Stream.concat(portlets, retrievalUtil.getValidPortletsOnLayout(layout, companyId)).distinct();
	}

}
