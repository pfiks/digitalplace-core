package com.placecube.digitalplace.override.struts.portal.action;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.struts.StrutsAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.override.struts.service.EmailVerificationService;

import java.io.IOException;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, property = { "path=/portal/verify_email_address" }, service = StrutsAction.class)
public class VerifyEmailOverrideAction implements StrutsAction {

	private static final String PORTAL_VERIFY_EMAIL_ADDRESS_FORWARD = "portal.verify_email_address";

	private static final String REFERER_JSP_PATH = "/common/referer_jsp.jsp";

	@Reference
	private EmailVerificationService emailVerificationService;

	@Reference
	private Portal portal;

	@Override
	public String execute(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String cmd = ParamUtil.getString(httpServletRequest, Constants.CMD);

		if (Validator.isNull(cmd)) {
			return PORTAL_VERIFY_EMAIL_ADDRESS_FORWARD;

		} else if (themeDisplay.isSignedIn() && cmd.equals(Constants.SEND)) {
			return sendEmailVerification(httpServletRequest, themeDisplay);

		} else {
			return verifyEmailAddress(httpServletRequest, httpServletResponse, themeDisplay);
		}
	}

	private String sendEmailVerification(HttpServletRequest httpServletRequest, ThemeDisplay themeDisplay) throws PortalException {
		emailVerificationService.sendEmailAddressVerification(httpServletRequest, themeDisplay);

		String successMessage = ParamUtil.getString(httpServletRequest, "successMessage");

		SessionMessages.add(httpServletRequest, "requestProcessed", successMessage);

		return PORTAL_VERIFY_EMAIL_ADDRESS_FORWARD;
	}

	private String verifyEmailAddress(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, ThemeDisplay themeDisplay) throws IOException, ServletException {
		try {
			emailVerificationService.verifyEmailAddress(httpServletRequest);

			if (!themeDisplay.isSignedIn()) {
				PortletURL portletURL = PortletURLFactoryUtil.create(httpServletRequest, PortletKeys.LOGIN, PortletRequest.RENDER_PHASE);

				httpServletResponse.sendRedirect(portletURL.toString());

				return null;
			}

			return REFERER_JSP_PATH;

		} catch (Exception e) {
			return hadleException(e, httpServletRequest, httpServletResponse);
		}
	}

	private String hadleException(Exception e, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
		if (e instanceof PortalException || e instanceof SystemException) {
			SessionErrors.add(httpServletRequest, e.getClass());

			return PORTAL_VERIFY_EMAIL_ADDRESS_FORWARD;
		}

		portal.sendError(e, httpServletRequest, httpServletResponse);

		return null;

	}
}