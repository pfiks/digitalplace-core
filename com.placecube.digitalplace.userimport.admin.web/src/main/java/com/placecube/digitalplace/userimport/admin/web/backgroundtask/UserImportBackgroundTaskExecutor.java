package com.placecube.digitalplace.userimport.admin.web.backgroundtask;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.backgroundtask.BackgroundTask;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskResult;
import com.liferay.portal.kernel.backgroundtask.BaseBackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.constants.BackgroundTaskConstants;
import com.liferay.portal.kernel.backgroundtask.display.BackgroundTaskDisplay;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.digitalplace.userimport.admin.web.model.ImportResultWrapper;
import com.placecube.digitalplace.userimport.admin.web.model.UserImportContext;
import com.placecube.digitalplace.userimport.admin.web.service.UserImportNotificationService;
import com.placecube.digitalplace.userimport.admin.web.service.UserImportService;

@Component(property = "background.task.executor.class.name=com.placecube.digitalplace.userimport.admin.web.backgroundtask.UserImportBackgroundTaskExecutor", service = BackgroundTaskExecutor.class)
public class UserImportBackgroundTaskExecutor extends BaseBackgroundTaskExecutor {

	private static final Log LOG = LogFactoryUtil.getLog(UserImportBackgroundTaskExecutor.class);

	@Reference
	private UserImportNotificationService userImportNotificationService;

	@Reference
	private UserImportService userImportService;

	@Override
	public BackgroundTaskExecutor clone() {
		return this;
	}

	@Override
	public BackgroundTaskResult execute(BackgroundTask backgroundTask) {

		ServiceContext serviceContext = userImportService.getServiceContext(backgroundTask);

		try {
			UserImportContext userImportContext = userImportService.getUserImportContext(backgroundTask);

			ImportResultWrapper userImportResults = userImportService.importUserData(userImportContext, serviceContext);

			userImportNotificationService.notifyUserOfImportCompleted(serviceContext, userImportResults);

			return BackgroundTaskResult.SUCCESS;

		} catch (Exception e) {
			LOG.error("Exception running background task to import users", e);

			userImportNotificationService.notifyUserOfImportFailed(serviceContext, e.getMessage());

			return new BackgroundTaskResult(BackgroundTaskConstants.STATUS_FAILED);
		} finally {
			userImportService.removeFileEntry(backgroundTask);
		}
	}

	@Override
	public BackgroundTaskDisplay getBackgroundTaskDisplay(BackgroundTask backgroundTask) {
		return null;
	}

	@Override
	public int getIsolationLevel() {
		return BackgroundTaskConstants.ISOLATION_LEVEL_TASK_NAME;
	}

}