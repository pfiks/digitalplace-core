package com.placecube.digitalplace.formadmin.web.portlet.command;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.PropsUtil;
import com.placecube.digitalplace.formadmin.web.constants.PortletRequestKeys;
import com.placecube.digitalplace.formadmin.web.model.FormDefinitionResult;
import com.placecube.digitalplace.formadmin.web.service.FormDefinitionListService;
import com.placecube.digitalplace.formadmin.web.service.SearchContainerService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PropsUtil.class })
public class FormsMVCRenderCommandTest extends PowerMockito {

	@Mock
	protected FormDefinitionListService mockFormDefinitionListService;

	@Mock
	protected SearchContainerService mockSearchContainerService;

	@InjectMocks
	private FormsMVCRenderCommand formsMVCRenderCommand;

	@Mock
	private List<FormDefinitionResult> mockFormDefinitionResults;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	private SearchContainer<FormDefinitionResult> mockSearchContainer;

	@Before
	public void activateSetup() {
		mockStatic(PropsUtil.class);

		mockSearchContainer = mock(SearchContainer.class);
	}

	@Test
	public void render_WhenNoError_ThenReturnsTheViewJSP() throws Exception {
		when(mockFormDefinitionListService.getFormDefinitions(mockRenderRequest)).thenReturn(mockFormDefinitionResults);
		when(mockSearchContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse, "no-form-definitions-were-found")).thenReturn(mockSearchContainer);

		String result = formsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo("/view.jsp"));
	}

	@Test
	public void render_WhenNoError_ThenSetsSearchContainerAsRequestAttribute() throws Exception {
		int total = 234;
		when(mockFormDefinitionListService.getFormDefinitions(mockRenderRequest)).thenReturn(mockFormDefinitionResults);
		when(mockFormDefinitionResults.size()).thenReturn(total);
		when(mockSearchContainerService.getSearchContainer(mockRenderRequest, mockRenderResponse, "no-form-definitions-were-found")).thenReturn(mockSearchContainer);

		formsMVCRenderCommand.render(mockRenderRequest, mockRenderResponse);

		InOrder inOrder = inOrder(mockRenderRequest, mockSearchContainer);
		inOrder.verify(mockSearchContainer, times(1)).setResultsAndTotal(mockFormDefinitionResults);
		inOrder.verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.SEARCH_CONTAINER, mockSearchContainer);
	}
}
