package com.placecube.digitalplace.authentication.twofactor.web.useradmins;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.User;
import com.placecube.digitalplace.authentication.twofactor.configuration.AuthenticationTwoFactorCompanyConfiguration;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorService;
import com.placecube.digitalplace.authentication.twofactor.web.service.TwoFactorUrlService;

public class TwoFactorAuthenticationUserActionContributorTest extends PowerMockito {

	@Mock
	private AuthenticationTwoFactorCompanyConfiguration mockAuthenticationTwoFactorCompanyConfiguration;

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private PortletResponse mockPortletResponse;

	@Mock
	private User mockSelectedUser;

	@Mock
	private TwoFactorService mockTwoFactorService;

	@Mock
	private TwoFactorUrlService mockTwoFactorUrlService;

	@Mock
	private User mockUser;

	@InjectMocks
	private TwoFactorAuthenticationUserActionContributor twoFactorAuthenticationUserActionContributor;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void getURL_WhenNoError_ThenReturnsTheResetTwoFactorURL() {
		String expected = "expectedValue";
		when(mockTwoFactorUrlService.getResetTwoFactorURL(mockPortletResponse, mockSelectedUser)).thenReturn(expected);

		String result = twoFactorAuthenticationUserActionContributor.getURL(mockPortletRequest, mockPortletResponse, mockUser, mockSelectedUser);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void isShow_WhenConfigurationFound_ThenReturnsTrue() {
		long companyId = 1;
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockTwoFactorService.getEnabledConfiguration(companyId)).thenReturn(Optional.of(mockAuthenticationTwoFactorCompanyConfiguration));

		boolean result = twoFactorAuthenticationUserActionContributor.isShow(mockPortletRequest, mockUser, mockSelectedUser);

		assertTrue(result);
	}

	@Test
	public void isShow_WhenConfigurationNotFound_ThenReturnsFalse() {
		long companyId = 1;
		when(mockUser.getCompanyId()).thenReturn(companyId);
		when(mockTwoFactorService.getEnabledConfiguration(companyId)).thenReturn(Optional.empty());

		boolean result = twoFactorAuthenticationUserActionContributor.isShow(mockPortletRequest, mockUser, mockSelectedUser);

		assertFalse(result);
	}

	@Test
	public void isShowConfirmationMessage_WhenNoError_ThenReturnsTrue() {
		boolean result = twoFactorAuthenticationUserActionContributor.isShowConfirmationMessage(mockSelectedUser);

		assertTrue(result);
	}

}
