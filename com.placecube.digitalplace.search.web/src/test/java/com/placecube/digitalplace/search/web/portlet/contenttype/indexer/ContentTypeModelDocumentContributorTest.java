package com.placecube.digitalplace.search.web.portlet.contenttype.indexer;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.placecube.digitalplace.search.web.portlet.contenttype.constants.ContentTypeConstants;
import com.placecube.digitalplace.search.web.portlet.contenttype.service.ContentTypeSearchService;

public class ContentTypeModelDocumentContributorTest extends PowerMockito {

	@InjectMocks
	private ContentTypeModelDocumentContributor contentTypeModelDocumentContributor;

	@Mock
	private BaseModel mockBaseModel;

	@Mock
	private ContentTypeSearchService mockContentTypeSearchService;

	@Mock
	private Document mockDocument;

	@Mock
	private Field mockField;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void contribute_WhenClassNameIsDDLRecordAndExceptionRetrievingTheDDMStructureIdIdValue_ThenAddsIndexerFieldNameWithTheClassName() throws PortalException {
		String className = ContentTypeConstants.DDL_RECORD_CLASS_NAME;
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(className);
		when(mockContentTypeSearchService.getDDLStructureIdFromDocument(mockDocument)).thenThrow(new PortalException());
		when(mockContentTypeSearchService.getContentTypeSearchField(className)).thenReturn(mockField);

		contentTypeModelDocumentContributor.contribute(mockDocument, mockBaseModel);

		verify(mockDocument, times(1)).add(mockField);
	}

	@Test
	public void contribute_WhenClassNameIsDDLRecordAndNoError_ThenAddsIndexerFieldNameWithTheClassNameValuePlusTheDDMStructureId() throws PortalException {
		String className = ContentTypeConstants.DDL_RECORD_CLASS_NAME;
		String recordSetId = "123";
		String expectedValue = "expectedValue";
		when(mockContentTypeSearchService.getDDLStructureIdFromDocument(mockDocument)).thenReturn(recordSetId);
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(className);
		when(mockContentTypeSearchService.getDDLRecordContentTypeValue(recordSetId)).thenReturn(expectedValue);
		when(mockContentTypeSearchService.getContentTypeSearchField(expectedValue)).thenReturn(mockField);

		contentTypeModelDocumentContributor.contribute(mockDocument, mockBaseModel);

		verify(mockDocument, times(1)).add(mockField);
	}

	@Test
	public void contribute_WhenClassNameIsNotDDLRecord_ThenAddsIndexerFieldNameWithTheClassNameValue() {
		String className = "myClassName";
		when(mockDocument.get(Field.ENTRY_CLASS_NAME)).thenReturn(className);
		when(mockContentTypeSearchService.getContentTypeSearchField(className)).thenReturn(mockField);

		contentTypeModelDocumentContributor.contribute(mockDocument, mockBaseModel);

		verify(mockDocument, times(1)).add(mockField);
	}

}
