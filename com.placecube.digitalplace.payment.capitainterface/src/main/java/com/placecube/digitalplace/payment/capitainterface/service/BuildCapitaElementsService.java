package com.placecube.digitalplace.payment.capitainterface.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis.types.Token;

import com.capita_software_services.www.portal_api.PanEntryMethod;
import com.capita_software_services.www.scp.base.LgItemDetails;
import com.capita_software_services.www.scp.base.LgSaleDetails;
import com.capita_software_services.www.scp.base.ReturnUrl;
import com.capita_software_services.www.scp.base.Routing;
import com.capita_software_services.www.scp.base.SummaryData;
import com.capita_software_services.www.scp.base.TaxItem;
import com.capita_software_services.www.scp.base.ThreePartName;
import com.capita_software_services.www.scp.base.VatItem;
import com.capita_software_services.www.scp.simple.SimpleItem;
import com.capita_software_services.www.scp.simple.SimpleSale;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.payment.capitainterface.CapitaConstants;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.DataField;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.DiagnosticData;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.ErrorCode;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.LoggingBean;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.Service;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.beans.SourceSystem;
import com.placecube.digitalplace.payment.capitainterface.common.utilities.service.LoggingFormatService;
import com.placecube.digitalplace.payment.capitainterface.models.PaymentProperties;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleConfiguration;
import com.placecube.digitalplace.payment.capitainterface.models.SimpleSaleItemConfiguration;
import com.placecube.digitalplace.payment.model.PaymentContext;

/**
 * utility class to build reusable parts of the Capita interface
 *
 */
public class BuildCapitaElementsService {

	private static final Log LOG = LogFactoryUtil.getLog(BuildCapitaElementsService.class);

	private final PaymentProperties properties;

	public BuildCapitaElementsService(PaymentProperties properties) {
		this.properties = properties;
	}

	/**
	 * Build a LgSaleDetails object filled with user's detail.
	 * Typically called when the payment is made on behalf of the card holder.
	 *
	 * @param userId the user id
	 * @return a LgSaleDetails filled with loggedIn user's details
	 */
	public LgSaleDetails buildLgSaleDetailsForCNP(long userId) {

		LgSaleDetails lgSaleDetails = new LgSaleDetails();
		lgSaleDetails.setUserName(String.valueOf(userId));

		return lgSaleDetails;
	}

	/**
	 * Build a routing object. This is the object that tells Capita about the
	 * URLs it should redirect to after successful payment or is user selects
	 * back button.
	 *
	 * @param returnURL the return URL
	 * @param backURL the back URL
	 * @return a routing object
	 */
	public Routing buildRouting(String returnURL, String backURL) {
		Routing routing = new Routing();

		if (Validator.isNotNull(backURL)) {
			Token backURLToken = new Token();
			backURLToken.setValue(backURL);
			routing.setBackUrl(backURLToken);
		}

		if (Validator.isNull(returnURL)) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0056, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.MISSING_MANDATORY_FIELD, "returnURL"))));

			return null;
		}
		ReturnUrl returnURLValue = new ReturnUrl(returnURL);
		routing.setReturnUrl(returnURLValue);

		routing.setErrorUrl(null);

		routing.setScpId(properties.getScpId());
		routing.setSiteId(properties.getSiteId());

		return routing;
	}

	/**
	 * Build a Sale object containing the description, amount and itemised
	 * details
	 *
	 * @param saleDescription the sale description
	 * @param amountInPence the amount pence
	 * @param saleReferenceCode the sale reference code
	 * @param simpleSaleConfiguration the simple sale configuration settings
	 * @return a simple sale object
	 */
	public SimpleSale buildSale(String saleDescription, int amountInPence, String saleReferenceCode, SimpleSaleConfiguration simpleSaleConfiguration) {

		SummaryData summaryData = buildSummary(saleDescription, amountInPence, saleReferenceCode);

		if (Validator.isNotNull(summaryData)) {
			SimpleSale sale = new SimpleSale();

			sale.setSaleSummary(summaryData);

			return buildSaleItems(simpleSaleConfiguration, sale);
		}

		return null;
	}

	/**
	 * Builds the itemised details of a sale
	 *
	 * @param simpleSaleConfiguration the simple sale configuration settings
	 * @param sale a simple sale object
	 * @return the sale items
	 */
	public SimpleSale buildSaleItems(SimpleSaleConfiguration simpleSaleConfiguration, SimpleSale sale) {
		List<SimpleSaleItemConfiguration> simpleSaleItemConfigurations = simpleSaleConfiguration.getSimpleSaleItemConfigurations();
		if (!simpleSaleItemConfigurations.isEmpty()) {
			List<SimpleItem> items = new ArrayList<>();

			int index = 0;
			for (SimpleSaleItemConfiguration simpleSaleItemConfiguration : simpleSaleItemConfigurations) {
				// Check the correct entries are present in the itemDetails
				int amountInMinorUnits;
				try {
					amountInMinorUnits = Integer.parseInt(simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_AMOUNT_IN_MINOR_UNITS));
				} catch (NumberFormatException e) {

					LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0051, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
							new DiagnosticData(DataField.AMOUNT, simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_AMOUNT_IN_MINOR_UNITS)))), e);
					amountInMinorUnits = -1;
				}
				String summarySaleDescription = simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_DESCRIPTION);
				SimpleItem simpleItem = buildSimpleItem(simpleSaleConfiguration, summarySaleDescription, amountInMinorUnits, simpleSaleItemConfiguration, index,
						sale.getSaleSummary().getDisplayableReference());
				if (simpleItem != null) {
					items.add(simpleItem);
					index++;
				}
			}

			if (!items.isEmpty()) {
				sale.setItems(items.toArray(new SimpleItem[0]));
			}
		}

		return sale;
	}

	/**
	 * Build the standard PanEntryMethod object required for Capita calls when
	 * the payment is on behalf of the card holder
	 *
	 * @return a PanEntryMethod
	 */
	public PanEntryMethod createCNPPanEntryMethod() {

		return PanEntryMethod.CNP;
	}

	/**
	 * Build the standard PanEntryMethod object required for Capita calls
	 *
	 * @return a PanEntryMethod
	 */
	public PanEntryMethod createECOMPanEntryMethod() {

		return PanEntryMethod.ECOM;
	}

	/**
	 * Build a Token object from a String. Token objects are required in several
	 * places in the Capita web services
	 *
	 * @param uniqueId a uniqueId
	 * @return a Token object for use with Capita web services
	 */
	public Token generateToken(String uniqueId) {
		if (Validator.isNull(uniqueId)) {
			return null;
		}
		Token token = new Token();
		token.setValue(uniqueId);

		return token;
	}

	private SimpleItem buildSimpleItem(SimpleSaleConfiguration simpleSaleConfiguration, String summarySaleDescription, int amountInMinorUnits, SimpleSaleItemConfiguration simpleSaleItemConfiguration,
			int index, String formReferenceCode) {

		SimpleItem simpleItem = null;

		if (Validator.isNull(summarySaleDescription)) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0056, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.MISSING_MANDATORY_FIELD, "summarySaleDescription"))));

			return simpleItem;
		}

		if (amountInMinorUnits <= 0) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0056, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.MISSING_MANDATORY_FIELD, "amountInMinorUnits"))));

			return simpleItem;
		}

		simpleItem = new SimpleItem();
		simpleItem.setSurchargeable(false);
		SummaryData summary = new SummaryData();
		summary.setAmountInMinorUnits(Integer.parseInt(simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_AMOUNT_IN_MINOR_UNITS)));
		summary.setDescription(simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_DESCRIPTION));
		summary.setReference(simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_REFERENCE));
		summary.setDisplayableReference(formReferenceCode);

		simpleItem.setItemSummary(summary);

		LgItemDetails lgItemDetails = getLgItemDetails(simpleSaleItemConfiguration, simpleSaleConfiguration.getPaymentContext());
		if (Validator.isNotNull(lgItemDetails)) {
			simpleItem.setLgItemDetails(lgItemDetails);
		}

		TaxItem taxItem = getTaxItem(simpleSaleItemConfiguration);
		if (Validator.isNotNull(taxItem)) {
			simpleItem.setTax(taxItem);
		}

		Token lineId = new Token();
		lineId.setValue(Integer.toString(index + 1));
		simpleItem.setLineId(lineId);

		return simpleItem;
	}

	private SummaryData buildSummary(String saleDescription, int amountInPence, String saleReferenceCode) {
		if (Validator.isNull(saleDescription)) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0056, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.MISSING_MANDATORY_FIELD, "saleDescription"))));

			return null;
		}

		if (amountInPence <= 0) {
			LOG.error(LoggingFormatService.formatLoggingBean(new LoggingBean(Service.COMMON_LIBRARY, SourceSystem.LIFERAY, ErrorCode.E0056, CapitaConstants.INVOKE_WSDL_OPERATION_NAME,
					new DiagnosticData(DataField.MISSING_MANDATORY_FIELD, "amountInPence"))));

			return null;
		}

		SummaryData summary = new SummaryData();
		summary.setAmountInMinorUnits(amountInPence);
		summary.setDescription(saleDescription);
		summary.setReference(saleReferenceCode);
		summary.setDisplayableReference(saleReferenceCode);

		return summary;
	}

	private ThreePartName getAccountName(SimpleSaleItemConfiguration simpleSaleItemConfiguration) {
		String surname = GetterUtil.getString(simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_SURNAME));
		String title = GetterUtil.getString(simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_TITLE));
		String forename = GetterUtil.getString(simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_ACCOUNT_NAME_FORENAME));

		if (Validator.isNotNull(surname)) {
			return new ThreePartName(surname, title, forename);
		}

		return null;
	}

	private LgItemDetails getLgItemDetails(SimpleSaleItemConfiguration simpleSaleItemConfiguration, PaymentContext paymentContext) {
		LgItemDetails lgItemDetails = new LgItemDetails();

		String fundCode = simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_FUND_CODE);
		if (Validator.isNotNull(fundCode)) {
			lgItemDetails.setFundCode(fundCode);
		}

		String narrative = simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_LG_ITEM_DETAILS_NARRATIVE);
		if (Validator.isNotNull(narrative)) {
			lgItemDetails.setNarrative(narrative);
		}

		ThreePartName accountName = getAccountName(simpleSaleItemConfiguration);
		if (Validator.isNotNull(accountName)) {
			lgItemDetails.setAccountName(accountName);
		}

		if (Validator.isNull(lgItemDetails.getFundCode()) && Validator.isNull(lgItemDetails.getNarrative()) && Validator.isNull(lgItemDetails.getAccountName())) {
			return null;
		}

		lgItemDetails.setAdditionalReference(paymentContext.getInternalReference());

		return lgItemDetails;

	}

	private TaxItem getTaxItem(SimpleSaleItemConfiguration simpleSaleItemConfiguration) {
		String vatRateString = simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_TAX_VAT_VAT_RATE);
		if (Validator.isNull(vatRateString)) {
			return null;
		}
		BigDecimal vatRate = new BigDecimal(vatRateString);
		String vatCode = GetterUtil.getString(simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_TAX_VAT_VAT_CODE));
		int vatAmount = Integer.parseInt(simpleSaleItemConfiguration.getSaleItemDetail(CapitaConstants.SALE_ITEM_TAX_VAT_VAT_AMOUNT_IN_MINOR_UNITS));

		if (Validator.isNotNull(vatCode) && Validator.isNotNull(vatRate)) {
			VatItem vatItem = new VatItem(vatCode, vatRate, vatAmount);
			return new TaxItem(vatItem);
		}

		return null;
	}
}
