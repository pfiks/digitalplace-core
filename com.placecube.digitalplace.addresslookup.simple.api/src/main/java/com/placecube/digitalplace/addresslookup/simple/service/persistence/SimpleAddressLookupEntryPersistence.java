/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package com.placecube.digitalplace.addresslookup.simple.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.placecube.digitalplace.addresslookup.simple.exception.NoSuchSimpleAddressLookupEntryException;
import com.placecube.digitalplace.addresslookup.simple.model.SimpleAddressLookupEntry;

import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the simple address lookup entry service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SimpleAddressLookupEntryUtil
 * @generated
 */
@ProviderType
public interface SimpleAddressLookupEntryPersistence
	extends BasePersistence<SimpleAddressLookupEntry> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SimpleAddressLookupEntryUtil} to access the simple address lookup entry persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the simple address lookup entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByUuid(String uuid);

	/**
	 * Returns a range of all the simple address lookup entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns the simple address lookup entries before and after the current simple address lookup entry in the ordered set where uuid = &#63;.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the current simple address lookup entry
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public SimpleAddressLookupEntry[] findByUuid_PrevAndNext(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Removes all the simple address lookup entries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of simple address lookup entries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching simple address lookup entries
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns an ordered range of all the simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Returns the first simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Returns the last simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns the simple address lookup entries before and after the current simple address lookup entry in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the current simple address lookup entry
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public SimpleAddressLookupEntry[] findByUuid_C_PrevAndNext(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK, String uuid,
			long companyId,
			com.liferay.portal.kernel.util.OrderByComparator
				<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Removes all the simple address lookup entries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of simple address lookup entries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching simple address lookup entries
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @return the matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode);

	/**
	 * Returns a range of all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode, int start, int end);

	/**
	 * Returns an ordered range of all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns an ordered range of all the simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findByCompanyId_Postcode(
		long companyId, String postcode, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry findByCompanyId_Postcode_First(
			long companyId, String postcode,
			com.liferay.portal.kernel.util.OrderByComparator
				<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Returns the first simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry fetchByCompanyId_Postcode_First(
		long companyId, String postcode,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns the last simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry findByCompanyId_Postcode_Last(
			long companyId, String postcode,
			com.liferay.portal.kernel.util.OrderByComparator
				<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Returns the last simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching simple address lookup entry, or <code>null</code> if a matching simple address lookup entry could not be found
	 */
	public SimpleAddressLookupEntry fetchByCompanyId_Postcode_Last(
		long companyId, String postcode,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns the simple address lookup entries before and after the current simple address lookup entry in the ordered set where companyId = &#63; and postcode = &#63;.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the current simple address lookup entry
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public SimpleAddressLookupEntry[] findByCompanyId_Postcode_PrevAndNext(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK,
			long companyId, String postcode,
			com.liferay.portal.kernel.util.OrderByComparator
				<SimpleAddressLookupEntry> orderByComparator)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Removes all the simple address lookup entries where companyId = &#63; and postcode = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 */
	public void removeByCompanyId_Postcode(long companyId, String postcode);

	/**
	 * Returns the number of simple address lookup entries where companyId = &#63; and postcode = &#63;.
	 *
	 * @param companyId the company ID
	 * @param postcode the postcode
	 * @return the number of matching simple address lookup entries
	 */
	public int countByCompanyId_Postcode(long companyId, String postcode);

	/**
	 * Caches the simple address lookup entry in the entity cache if it is enabled.
	 *
	 * @param simpleAddressLookupEntry the simple address lookup entry
	 */
	public void cacheResult(SimpleAddressLookupEntry simpleAddressLookupEntry);

	/**
	 * Caches the simple address lookup entries in the entity cache if it is enabled.
	 *
	 * @param simpleAddressLookupEntries the simple address lookup entries
	 */
	public void cacheResult(
		java.util.List<SimpleAddressLookupEntry> simpleAddressLookupEntries);

	/**
	 * Creates a new simple address lookup entry with the primary key. Does not add the simple address lookup entry to the database.
	 *
	 * @param simpleAddressLookupEntryPK the primary key for the new simple address lookup entry
	 * @return the new simple address lookup entry
	 */
	public SimpleAddressLookupEntry create(
		SimpleAddressLookupEntryPK simpleAddressLookupEntryPK);

	/**
	 * Removes the simple address lookup entry with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry that was removed
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public SimpleAddressLookupEntry remove(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws NoSuchSimpleAddressLookupEntryException;

	public SimpleAddressLookupEntry updateImpl(
		SimpleAddressLookupEntry simpleAddressLookupEntry);

	/**
	 * Returns the simple address lookup entry with the primary key or throws a <code>NoSuchSimpleAddressLookupEntryException</code> if it could not be found.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry
	 * @throws NoSuchSimpleAddressLookupEntryException if a simple address lookup entry with the primary key could not be found
	 */
	public SimpleAddressLookupEntry findByPrimaryKey(
			SimpleAddressLookupEntryPK simpleAddressLookupEntryPK)
		throws NoSuchSimpleAddressLookupEntryException;

	/**
	 * Returns the simple address lookup entry with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param simpleAddressLookupEntryPK the primary key of the simple address lookup entry
	 * @return the simple address lookup entry, or <code>null</code> if a simple address lookup entry with the primary key could not be found
	 */
	public SimpleAddressLookupEntry fetchByPrimaryKey(
		SimpleAddressLookupEntryPK simpleAddressLookupEntryPK);

	/**
	 * Returns all the simple address lookup entries.
	 *
	 * @return the simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findAll();

	/**
	 * Returns a range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @return the range of simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator);

	/**
	 * Returns an ordered range of all the simple address lookup entries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>SimpleAddressLookupEntryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of simple address lookup entries
	 * @param end the upper bound of the range of simple address lookup entries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of simple address lookup entries
	 */
	public java.util.List<SimpleAddressLookupEntry> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<SimpleAddressLookupEntry> orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the simple address lookup entries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of simple address lookup entries.
	 *
	 * @return the number of simple address lookup entries
	 */
	public int countAll();

	public Set<String> getCompoundPKColumnNames();

}