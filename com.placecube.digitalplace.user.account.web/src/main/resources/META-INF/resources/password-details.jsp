<%@ include file="init.jsp"%>

<c:set var="progressBarValue" value="75"/>
<%@ include file="/fragments/progress-bar.jspf" %>

<div class="row">
	<%@ include file="/fragments/side-bar.jspf" %>

	<div class="col-md-8">

		<portlet:actionURL name="<%=MVCCommandKeys.PASSWORD_DETAILS%>" var="passwordDetailsURL" />

		<gds-forms-ui:errorSummary portletNamespace="${portletNamespace}" errors="${accountContext.getErrors()}"/>

		<aui:form action="${passwordDetailsURL}" method="post" name="createAccountForm">
			<%@ include file="/fragments/creation-errors.jspf" %>

			<div class="row">
				<div class="col-md-6">
					<gds-forms-ui:input-text
						portletNamespace="${portletNamespace}"
						fieldName="<%=AccountValidatedField.PASSWORD.getFieldName()%>"
						fieldValue=""
						fieldLabel="password"
						errorMessage="${accountContext.getFieldError('password')}"
						type="password"
						required="true"
					/>

					<gds-forms-ui:input-text
						portletNamespace="${portletNamespace}"
						fieldName="<%=AccountValidatedField.PASSWORD_CONFIRM.getFieldName()%>"
						fieldValue=""
						fieldLabel="confirm-password"
						errorMessage="${accountContext.getFieldError('password')}"
						type="password"
						required="true"
					/>

					<c:if test="${captchaEnabled}">
						<%@ include file="/fragments/captcha.jspf" %>
					</c:if>
				</div>
				<div class="col-md-6">
					<div class="password-policy">
						${passwordPolicyDescription}
					</div>
				</div>
			</div>

			<aui:button-row>
				<div class="pull-right">
					<aui:button type="submit" value="${accountSecurityStepEnabled ? 'next' : 'register' }" cssClass="btn btn-primary" icon="glyphicon glyphicon-chevron-right" iconAlign="right"/>
				</div>
				<div class="pull-left">
					<portlet:renderURL var="backURL">
						<portlet:param name="mvcRenderCommandName" value="${passwordPreviousStep}" />
					</portlet:renderURL>
					<aui:button type="button" value="previous" cssClass="btn btn-default" href="${backURL}" icon="glyphicon glyphicon-chevron-left" iconAlign="left"/>
				</div>
			</aui:button-row>
		</aui:form>
	</div>
</div>