package com.placecube.digitalplace.layout.service.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.LayoutService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.theme.NavItem;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.role.service.RolePermissionService;
import com.placecube.digitalplace.layout.service.LayoutRetrievalService;
import com.placecube.digitalplace.layout.util.LayoutRetrievalUtil;
import com.placecube.digitalplace.layout.util.LayoutWithPortletsRetrievalHelperUtil;

@Component(immediate = true, service = LayoutRetrievalService.class)
public class LayoutRetrievalServiceImpl implements LayoutRetrievalService {

	private static final Log LOG = LogFactoryUtil.getLog(LayoutRetrievalServiceImpl.class);

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private LayoutRetrievalUtil layoutRetrievalUtil;

	@Reference
	private LayoutService layoutService;

	@Reference
	private LayoutWithPortletsRetrievalHelperUtil layoutWithPortletsRetrievalHelperUtil;

	@Reference
	private RoleLocalService roleLocalService;

	@Reference
	private RolePermissionService rolePermissionService;

	@Override
	public List<Portlet> getAllPortletsOnLayout(Layout layout) {
		return layoutWithPortletsRetrievalHelperUtil.getAllPortletsOnLayout(layout);
	}

	@Override
	public List<Layout> getLayoutsWithPortletId(long groupId, String portletId, boolean privateLayout) {
		return layoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, privateLayout);
	}

	@Override
	public Optional<Layout> getLayoutWithPortletId(long groupId, String portletId, boolean privateLayout) {
		return layoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, privateLayout).stream().findFirst();
	}

	@Override
	public NavItem getNavItemForLayout(HttpServletRequest httpServletRequest, long groupId, String friendlyURL, boolean privateLayout) throws PortalException {
		Layout layout = layoutLocalService.getFriendlyURLLayout(groupId, privateLayout, friendlyURL);
		return new NavItem(httpServletRequest, layout);
	}

	@Override
	public LinkedList<NavItem> getNavItemsForLayouts(HttpServletRequest httpServletRequest, List<Layout> layouts) {
		return layouts.stream().map(layout -> new NavItem(httpServletRequest, layout)).collect(Collectors.toCollection(LinkedList::new));
	}

	@Override
	public Optional<NavItem> getOptionalNavItemForLayout(HttpServletRequest httpServletRequest, long groupId, String friendlyURL, boolean privateLayout) {
		Layout layout = layoutLocalService.fetchLayoutByFriendlyURL(groupId, privateLayout, friendlyURL);
		if (Validator.isNotNull(layout)) {
			return Optional.of(new NavItem(httpServletRequest, layout));
		} else {
			LOG.warn("No layout found for groupId: " + groupId + ", privateLayout: " + privateLayout + " and url: " + friendlyURL);
			return Optional.empty();
		}

	}

	@Override
	public Optional<Layout> getPreferredLayoutWithPortletId(long groupId, String portletId, boolean preferenceOnPrivateLayouts) {
		List<Layout> layouts = layoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, preferenceOnPrivateLayouts);
		Optional<Layout> layout = layoutRetrievalUtil.getFirstNonHiddenLayout(layouts);
		if (layout.isPresent()) {
			return layout;
		} else {
			layouts = layoutRetrievalUtil.getLayoutsWithPortletId(groupId, portletId, !preferenceOnPrivateLayouts);
			return layoutRetrievalUtil.getFirstNonHiddenLayout(layouts);
		}
	}

	@Override
	public Optional<Layout> getPreferredLayoutWithPortletId(long groupId, String portletId, boolean preferenceOnPrivateLayouts, Map<String, String> prefsToMatch) {
		Optional<Layout> layout = layoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, preferenceOnPrivateLayouts, prefsToMatch, portletId);
		if (layout.isPresent()) {
			return layout;
		} else {
			return layoutRetrievalUtil.getFirstNonHiddenLayoutWithCorrectPrefs(groupId, !preferenceOnPrivateLayouts, prefsToMatch, portletId);
		}
	}

	@Override
	public List<NavItem> getPublicAndPrivateNavItems(HttpServletRequest request, List<NavItem> currentNavItems) {
		List<NavItem> results = new LinkedList<>();

		Layout sampleLayout = currentNavItems.get(0).getLayout();
		if (sampleLayout.isPrivateLayout()) {
			addNavItemsForOppositeLayoutPrivacy(request, results, sampleLayout);
			results.addAll(currentNavItems);
		} else {
			results.addAll(currentNavItems);
			addNavItemsForOppositeLayoutPrivacy(request, results, sampleLayout);
		}

		return results;
	}

	@Override
	public void hidePagesFromRoles(long companyId, List<Layout> layouts, Set<String> roleNames) throws PortalException {
		try {
			List<Long> roleIds = getRoleIds(companyId, roleNames);

			for (Layout layout : layouts) {
				LOG.info("Making layout plid: " + layout.getPlid() + " hidden from roles: " + roleNames.toString());

				for (Long roleId : roleIds) {
					rolePermissionService.removeIndividualPermissionForRole(layout.getCompanyId(), roleId, Layout.class.getName(), String.valueOf(layout.getPlid()), ActionKeys.VIEW);
				}

				layout.setHidden(true);
				layoutLocalService.updateLayout(layout);
			}
		} catch (Exception e) {
			throw new PortalException("Exception configuring layouts hidden from roles", e);
		}
	}

	@Override
	public void showPagesToRoles(long companyId, List<Layout> layouts, Set<String> roleNames) throws PortalException {
		try {
			List<Long> roleIds = getRoleIds(companyId, roleNames);
			for (Layout layout : layouts) {
				LOG.info("Making layout plid: " + layout.getPlid() + " visible to roles: " + roleNames.toString());

				for (Long roleId : roleIds) {
					rolePermissionService.addIndividualPermissionForRole(layout.getCompanyId(), roleId, Layout.class.getName(), String.valueOf(layout.getPlid()), ActionKeys.VIEW);
				}

				layout.setHidden(false);
				layoutLocalService.updateLayout(layout);
			}

		} catch (Exception e) {
			throw new PortalException("Exception configuring layouts visible for roles", e);
		}
	}

	private void addNavItemsForOppositeLayoutPrivacy(HttpServletRequest request, List<NavItem> results, Layout sampleLayout) {
		List<Layout> layouts = layoutService.getLayouts(sampleLayout.getGroupId(), !sampleLayout.isPrivateLayout());
		layouts.stream().forEach(entry -> {
			if (!entry.isHidden()) {
				results.add(new NavItem(request, entry));
			}
		});
	}

	private List<Long> getRoleIds(long companyId, Set<String> roleNames) {
		List<Long> results = new ArrayList<>();

		for (String roleName : roleNames) {
			try {
				results.add(roleLocalService.getRole(companyId, roleName).getRoleId());
			} catch (PortalException e) {
				LOG.warn(e);
			}
		}
		return results;
	}

}
