/**
 * LgPnpDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.capita_software_services.www.scp.base;

public class LgPnpDetails  implements java.io.Serializable {
    private java.lang.String fundCode;

    private java.lang.String pnpCode;

    private java.lang.String pnpOptionCode;

    private java.lang.String pnpOptionDescription;

    public LgPnpDetails() {
    }

    public LgPnpDetails(
           java.lang.String fundCode,
           java.lang.String pnpCode,
           java.lang.String pnpOptionCode,
           java.lang.String pnpOptionDescription) {
           this.fundCode = fundCode;
           this.pnpCode = pnpCode;
           this.pnpOptionCode = pnpOptionCode;
           this.pnpOptionDescription = pnpOptionDescription;
    }


    /**
     * Gets the fundCode value for this LgPnpDetails.
     *
     * @return fundCode
     */
    public java.lang.String getFundCode() {
        return fundCode;
    }


    /**
     * Sets the fundCode value for this LgPnpDetails.
     *
     * @param fundCode
     */
    public void setFundCode(java.lang.String fundCode) {
        this.fundCode = fundCode;
    }


    /**
     * Gets the pnpCode value for this LgPnpDetails.
     *
     * @return pnpCode
     */
    public java.lang.String getPnpCode() {
        return pnpCode;
    }


    /**
     * Sets the pnpCode value for this LgPnpDetails.
     *
     * @param pnpCode
     */
    public void setPnpCode(java.lang.String pnpCode) {
        this.pnpCode = pnpCode;
    }


    /**
     * Gets the pnpOptionCode value for this LgPnpDetails.
     *
     * @return pnpOptionCode
     */
    public java.lang.String getPnpOptionCode() {
        return pnpOptionCode;
    }


    /**
     * Sets the pnpOptionCode value for this LgPnpDetails.
     *
     * @param pnpOptionCode
     */
    public void setPnpOptionCode(java.lang.String pnpOptionCode) {
        this.pnpOptionCode = pnpOptionCode;
    }


    /**
     * Gets the pnpOptionDescription value for this LgPnpDetails.
     *
     * @return pnpOptionDescription
     */
    public java.lang.String getPnpOptionDescription() {
        return pnpOptionDescription;
    }


    /**
     * Sets the pnpOptionDescription value for this LgPnpDetails.
     *
     * @param pnpOptionDescription
     */
    public void setPnpOptionDescription(java.lang.String pnpOptionDescription) {
        this.pnpOptionDescription = pnpOptionDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LgPnpDetails)) return false;
        LgPnpDetails other = (LgPnpDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.fundCode==null && other.getFundCode()==null) ||
             (this.fundCode!=null &&
              this.fundCode.equals(other.getFundCode()))) &&
            ((this.pnpCode==null && other.getPnpCode()==null) ||
             (this.pnpCode!=null &&
              this.pnpCode.equals(other.getPnpCode()))) &&
            ((this.pnpOptionCode==null && other.getPnpOptionCode()==null) ||
             (this.pnpOptionCode!=null &&
              this.pnpOptionCode.equals(other.getPnpOptionCode()))) &&
            ((this.pnpOptionDescription==null && other.getPnpOptionDescription()==null) ||
             (this.pnpOptionDescription!=null &&
              this.pnpOptionDescription.equals(other.getPnpOptionDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFundCode() != null) {
            _hashCode += getFundCode().hashCode();
        }
        if (getPnpCode() != null) {
            _hashCode += getPnpCode().hashCode();
        }
        if (getPnpOptionCode() != null) {
            _hashCode += getPnpOptionCode().hashCode();
        }
        if (getPnpOptionDescription() != null) {
            _hashCode += getPnpOptionDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LgPnpDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "lgPnpDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "fundCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pnpCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "pnpCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pnpOptionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "pnpOptionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pnpOptionDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.capita-software-services.com/scp/base", "pnpOptionDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
