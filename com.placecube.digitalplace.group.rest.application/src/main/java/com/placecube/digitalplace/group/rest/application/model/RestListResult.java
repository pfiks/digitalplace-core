package com.placecube.digitalplace.group.rest.application.model;

import java.util.LinkedHashSet;
import java.util.Set;

public class RestListResult {

	private final Set<Object> data;

	RestListResult() {
		data = new LinkedHashSet<>();
	}

	public static RestListResult init() {
		return new RestListResult();
	}

	public void addResult(Object resultValue) {
		data.add(resultValue);
	}

	public Set<Object> getData() {
		return data;
	}

	public Integer getItemCount() {
		return data.size();
	}

}
