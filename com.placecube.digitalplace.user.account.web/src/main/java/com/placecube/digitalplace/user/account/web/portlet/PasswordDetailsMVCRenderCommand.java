package com.placecube.digitalplace.user.account.web.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.placecube.digitalplace.user.account.exception.UserAccountException;
import com.placecube.digitalplace.user.account.model.AccountContext;
import com.placecube.digitalplace.user.account.service.AccountConfigurationService;
import com.placecube.digitalplace.user.account.service.AccountContextService;
import com.placecube.digitalplace.user.account.service.UserAuthenticationService;
import com.placecube.digitalplace.user.account.web.constants.AccountPortletKeys;
import com.placecube.digitalplace.user.account.web.constants.MVCCommandKeys;
import com.placecube.digitalplace.user.account.web.internal.constants.RequestKeys;
import com.placecube.digitalplace.userprofile.configuration.UserProfileFieldsConfiguration;

@Component(immediate = true, property = { "javax.portlet.name=" + AccountPortletKeys.CREATE_ACCOUNT, "mvc.command.name=" + MVCCommandKeys.PASSWORD_DETAILS }, service = MVCRenderCommand.class)
public class PasswordDetailsMVCRenderCommand implements MVCRenderCommand {

	@Reference
	private AccountConfigurationService accountConfigurationService;

	@Reference
	private AccountContextService accountContextService;

	@Reference
	private UserAuthenticationService userAuthenticationService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {
			AccountContext accountContext = accountContextService.getOrCreateContext(renderRequest);
			UserProfileFieldsConfiguration userProfileFieldsConfiguration = accountConfigurationService.getCompanyUserProfileFieldsConfiguration(accountContext.getCompanyId());

			String passwordPolicyDescription = userAuthenticationService.getUserAccountPasswordPolicyHtml(renderRequest);

			renderRequest.setAttribute("passwordPolicyDescription", passwordPolicyDescription);
			renderRequest.setAttribute("passwordPreviousStep", accountConfigurationService.getPasswordPreviousStep(accountContext, userProfileFieldsConfiguration));
			renderRequest.setAttribute(RequestKeys.ACCOUNT_CONTEXT, accountContext);
			renderRequest.setAttribute(RequestKeys.ACCOUNT_SECURITY_STEP_ENABLED, accountConfigurationService.isAccountSecurityStepEnabled(accountContext));
			renderRequest.setAttribute(RequestKeys.CAPTCHA_ENABLED, accountContextService.isShowCaptchaOnPasswordStep(accountContext, renderRequest));
			renderRequest.setAttribute(RequestKeys.USER_PROFILE_CONFIG, userProfileFieldsConfiguration);
			accountContextService.updateContextInSession(accountContext, renderRequest);
		} catch (UserAccountException e) {
			throw new PortletException(e);
		}

		return "/password-details.jsp";
	}

}
