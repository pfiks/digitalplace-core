package com.placecube.digitalplace.calendar.web.registry;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.placecube.digitalplace.calendar.model.DigitalPlaceCalendarConnector;

public class DigitalPlaceCalendarConnectorRegistryTest extends PowerMockito {

	@InjectMocks
	private DigitalPlaceCalendarConnectorRegistry digitalPlaceCalendarConnectorRegistry;

	@Mock
	private DigitalPlaceCalendarConnector mockDigitalPlaceCalendarConnector;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void setCalendarConnector_WhenNoError_ThenCalendarConnectorIsAddedToConnectorList() {

		List<DigitalPlaceCalendarConnector> calendarConnectors = digitalPlaceCalendarConnectorRegistry.getCalendarConnectors();

		assertTrue(calendarConnectors.isEmpty());

		digitalPlaceCalendarConnectorRegistry.setCalendarConnector(mockDigitalPlaceCalendarConnector);

		assertFalse(calendarConnectors.isEmpty());

		assertThat(calendarConnectors.get(0), sameInstance(mockDigitalPlaceCalendarConnector));
	}

	@Test
	public void unsetCalendarConnector_WhenNoError_ThenCalendarConnectorIsRemovedFromConnectorList() {

		digitalPlaceCalendarConnectorRegistry.setCalendarConnector(mockDigitalPlaceCalendarConnector);

		List<DigitalPlaceCalendarConnector> calendarConnectors = digitalPlaceCalendarConnectorRegistry.getCalendarConnectors();

		assertFalse(calendarConnectors.isEmpty());

		digitalPlaceCalendarConnectorRegistry.unsetCalendarConnector(mockDigitalPlaceCalendarConnector);

		assertTrue(calendarConnectors.isEmpty());
	}

}
