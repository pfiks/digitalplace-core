package com.placecube.digitalplace.address.constants;

import java.util.Arrays;
import java.util.Optional;

public enum AddressType {
	PRIMARY("primary"), PERSONAL("personal"), BUSINESS("business"), OTHER("other");

	private final String name;

	AddressType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static Optional<AddressType> getAddressTypeByName(String name) {
		return Arrays.stream(AddressType.values()).filter(a -> a.getName().equals(name)).findFirst();
	}
}