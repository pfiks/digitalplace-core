package com.placecube.digitalplace.user.account.web.configuration;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.user.account.configuration.AccountCompanyConfiguration;
import com.placecube.digitalplace.user.account.configuration.AccountGroupConfiguration;
import com.placecube.digitalplace.user.account.configuration.AccountPortletInstanceConfiguration;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfiguration;
import com.placecube.digitalplace.user.account.configuration.CreateAccountConfigurationFactory;

@Component(service = CreateAccountConfigurationFactory.class)
public class CreateAccountConfigurationFactoryImpl implements CreateAccountConfigurationFactory {

	private static final Log LOG = LogFactoryUtil.getLog(CreateAccountConfigurationFactoryImpl.class);

	@Reference
	private ConfigurationProvider configurationProvider;

	@Override
	public CreateAccountConfiguration getCreateAccountConfiguration(Company company) throws ConfigurationException {
		try {
			return new AccountCompanyConfigurationImpl(company.getCompanyId());
		} catch (ConfigurationException e) {
			LOG.error(e);
			throw new ConfigurationException(e);
		}
	}

	@Override
	public CreateAccountConfiguration getCreateAccountConfiguration(Group group) throws ConfigurationException {
		try {
			return new AccountGroupConfigurationImpl(group);
		} catch (PortalException e) {
			LOG.error(e);
			throw new ConfigurationException(e);
		}
	}

	@Override
	public CreateAccountConfiguration getCreateAccountConfiguration(PortletRequest portletRequest) throws ConfigurationException {

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			return new AccountPortletInstanceConfigurationImpl(themeDisplay.getCompanyId(), themeDisplay.getPortletDisplay());
		} catch (PortalException e) {
			LOG.error(e);
			throw new ConfigurationException(e);
		}
	}

	private class AccountCompanyConfigurationImpl implements CreateAccountConfiguration {

		private final AccountCompanyConfiguration accountCompanyConfiguration;

		private AccountCompanyConfigurationImpl(long companyId) throws ConfigurationException {
			accountCompanyConfiguration = configurationProvider.getCompanyConfiguration(AccountCompanyConfiguration.class, companyId);
		}

		@Override
		public boolean addressOutOfLocalArea() {
			return accountCompanyConfiguration.addressOutOfLocalArea();
		}

		@Override
		public String createAccountURL() {
			return accountCompanyConfiguration.createAccountURL();
		}

		@Override
		public String emailAddressPageLocation() {
			return accountCompanyConfiguration.emailAddressPageLocation();
		}

		@Override
		public boolean emailAddressRequired() {
			return accountCompanyConfiguration.emailAddressRequired();
		}

		@Override
		public String emailAddressVerificationURL() {
			return accountCompanyConfiguration.emailAddressVerificationURL();
		}

		@Override
		public boolean generatePasswordWhenLoggedInUserCreateAccount() {
			return accountCompanyConfiguration.generatePasswordWhenLoggedInUserCreateAccount();
		}

		@Override
		public String registrationRefererGroupIdUserExpandoFieldName() {
			return accountCompanyConfiguration.registrationRefererGroupIdUserExpandoFieldName();
		}

		@Override
		public boolean requireCaptchaBeforeEmailAddressEntry() {
			return accountCompanyConfiguration.requireCaptchaBeforeEmailAddressEntry();
		}

		@Override
		public String resetPasswordArticleId() {
			return accountCompanyConfiguration.resetPasswordArticleId();
		}

		@Override
		public boolean resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount() {
			return accountCompanyConfiguration.resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount();
		}

	}

	private class AccountGroupConfigurationImpl implements CreateAccountConfiguration {

		private final CreateAccountConfiguration accountCompanyConfiguration;

		private final AccountGroupConfiguration accountGroupConfiguration;

		private AccountGroupConfigurationImpl(Group group) throws PortalException {
			accountCompanyConfiguration = new AccountCompanyConfigurationImpl(group.getCompanyId());
			accountGroupConfiguration = configurationProvider.getGroupConfiguration(AccountGroupConfiguration.class, group.getGroupId());
		}

		@Override
		public boolean addressOutOfLocalArea() {
			return accountCompanyConfiguration.addressOutOfLocalArea();
		}

		@Override
		public String createAccountURL() {
			return accountCompanyConfiguration.createAccountURL();
		}

		@Override
		public String emailAddressPageLocation() {
			return accountCompanyConfiguration.emailAddressPageLocation();
		}

		@Override
		public boolean emailAddressRequired() {
			return accountGroupConfiguration.emailAddressRequired();
		}

		@Override
		public String emailAddressVerificationURL() {
			return accountCompanyConfiguration.emailAddressVerificationURL();
		}

		@Override
		public boolean generatePasswordWhenLoggedInUserCreateAccount() {
			return accountCompanyConfiguration.generatePasswordWhenLoggedInUserCreateAccount();
		}

		@Override
		public String registrationRefererGroupIdUserExpandoFieldName() {
			return accountCompanyConfiguration.registrationRefererGroupIdUserExpandoFieldName();
		}

		@Override
		public boolean requireCaptchaBeforeEmailAddressEntry() {
			return accountCompanyConfiguration.requireCaptchaBeforeEmailAddressEntry();
		}

		@Override
		public String resetPasswordArticleId() {
			return accountCompanyConfiguration.resetPasswordArticleId();
		}

		@Override
		public boolean resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount() {
			return accountCompanyConfiguration.resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount();
		}
	}

	private class AccountPortletInstanceConfigurationImpl implements CreateAccountConfiguration {

		private final CreateAccountConfiguration accountCompanyConfiguration;

		private final AccountPortletInstanceConfiguration accountPortletInstanceConfiguration;

		private AccountPortletInstanceConfigurationImpl(long companyId, PortletDisplay portletDisplay) throws PortalException {
			accountCompanyConfiguration = new AccountCompanyConfigurationImpl(companyId);
			accountPortletInstanceConfiguration = configurationProvider.getPortletInstanceConfiguration(AccountPortletInstanceConfiguration.class, portletDisplay.getThemeDisplay());
		}

		@Override
		public boolean addressOutOfLocalArea() {
			return accountCompanyConfiguration.addressOutOfLocalArea();
		}

		@Override
		public String createAccountURL() {
			return accountCompanyConfiguration.createAccountURL();
		}

		@Override
		public String emailAddressPageLocation() {
			return accountCompanyConfiguration.emailAddressPageLocation();
		}

		@Override
		public boolean emailAddressRequired() {
			return accountPortletInstanceConfiguration.emailAddressRequired();
		}

		@Override
		public String emailAddressVerificationURL() {
			return accountCompanyConfiguration.emailAddressVerificationURL();
		}

		@Override
		public boolean generatePasswordWhenLoggedInUserCreateAccount() {
			return accountCompanyConfiguration.generatePasswordWhenLoggedInUserCreateAccount();
		}

		@Override
		public String registrationRefererGroupIdUserExpandoFieldName() {
			return accountCompanyConfiguration.registrationRefererGroupIdUserExpandoFieldName();
		}

		@Override
		public boolean requireCaptchaBeforeEmailAddressEntry() {
			return accountCompanyConfiguration.requireCaptchaBeforeEmailAddressEntry();
		}

		@Override
		public String resetPasswordArticleId() {
			return accountCompanyConfiguration.resetPasswordArticleId();
		}

		@Override
		public boolean resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount() {
			return accountCompanyConfiguration.resetPasswordButtonInConfirmationPageWhenLoggedInUserCreateAccount();
		}
	}
}
