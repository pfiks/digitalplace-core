package com.placecube.digitalplace.twitterfeed.portlet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;
import java.util.Optional;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.twitterfeed.configuration.TwitterFeedConfigurationUtil;
import com.placecube.digitalplace.twitterfeed.configuration.TwitterFeedPortletInstanceConfiguration;
import com.placecube.digitalplace.twitterfeed.constants.PortletRequestKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class TwitterFeedPortletTest extends PowerMockito {

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private TwitterFeedConfigurationUtil mockTwitterFeedConfigurationUtil;

	@Mock
	private TwitterFeedPortletInstanceConfiguration mockTwitterFeedPortletInstanceConfiguration;

	@InjectMocks
	private TwitterFeedPortlet twitterFeedPortlet;

	@Test
	public void render_WhenNoValidConfigurationIsFound_ThenDoesNotSetConfigurationAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockTwitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.empty());

		twitterFeedPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(PortletRequestKeys.CONFIGURATION), any());
	}

	@Test
	public void render_WhenNoValidConfigurationIsFound_ThenSetsPortletConfiguratorVisibilityTrueAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockTwitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.empty());

		twitterFeedPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY, true);
	}

	@Test
	public void render_WhenValidConfigurationIsFound_ThenDoesNotSetPortletConfiguratorVisibilityAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockTwitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockTwitterFeedPortletInstanceConfiguration));

		twitterFeedPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, never()).setAttribute(eq(WebKeys.PORTLET_CONFIGURATOR_VISIBILITY), any());
	}

	@Test
	public void render_WhenValidConfigurationIsFound_ThenSetsConfigurationAsRequestAttribute() throws Exception {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockTwitterFeedConfigurationUtil.getValidConfiguration(mockThemeDisplay)).thenReturn(Optional.of(mockTwitterFeedPortletInstanceConfiguration));

		twitterFeedPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute(PortletRequestKeys.CONFIGURATION, mockTwitterFeedPortletInstanceConfiguration);
	}

	@Before
	public void setUp() throws NoSuchMethodException {
		mockCallToSuper();
	}

	@SuppressWarnings("rawtypes")
	private void mockCallToSuper() throws NoSuchMethodException {
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		suppress(superRenderMethod);
	}
}
