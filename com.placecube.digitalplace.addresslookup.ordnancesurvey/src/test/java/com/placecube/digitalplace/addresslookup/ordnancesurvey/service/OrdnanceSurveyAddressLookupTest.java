package com.placecube.digitalplace.addresslookup.ordnancesurvey.service;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.configuration.module.configuration.ConfigurationProvider;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.placecube.digitalplace.address.model.AddressContext;
import com.placecube.digitalplace.addresslookup.ordnancesurvey.configuration.OrdnanceSurveyCompanyConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class OrdnanceSurveyAddressLookupTest extends PowerMockito {

	private static final String APP_KEY = "myAppKey";

	private static final long COMPANY_ID = 10;

	private static final String ENDPOINT = "myEndpointUrl";

	private static final String[] PARAMS_AND_VALUES = new String[] { "one", "two" };

	private static final String URL = "myFinalUrl";

	private static final String VALUE = "myMethodValue";

	@Mock
	private AddressContext mockAddressContext;

	@Mock
	private ConfigurationProvider mockConfigurationProvider;

	@Mock
	private OrdnanceSurveyCompanyConfiguration mockOrdnanceSurveyCompanyConfiguration;

	@Mock
	private OrdnanceSurveyLookupService mockOrdnanceSurveyLookupService;

	@Mock
	private Map<String, String> mockParamsAndValues;

	@Mock
	private Set<AddressContext> mockResults;

	@InjectMocks
	private OrdnanceSurveyAddressLookup ordnanceSurveyAddressLookup;

	@Test
	public void enabled_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = ordnanceSurveyAddressLookup.enabled(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void enabled_WhenNoError_ThenReturnsIfTheConfigurationIsEnabled(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockOrdnanceSurveyCompanyConfiguration);
		when(mockOrdnanceSurveyCompanyConfiguration.enabled()).thenReturn(expected);

		boolean result = ordnanceSurveyAddressLookup.enabled(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void findByKeyword_WhenException_ThenReturnsEmptySet() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		Set<AddressContext> results = ordnanceSurveyAddressLookup.findByKeyword(COMPANY_ID, VALUE, PARAMS_AND_VALUES);

		assertThat(results, empty());
	}

	@Test
	public void findByKeyword_WhenNoError_ThenReturnsSetOfResults() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockOrdnanceSurveyCompanyConfiguration);
		when(mockOrdnanceSurveyCompanyConfiguration.findEndpointURL()).thenReturn(ENDPOINT);
		when(mockOrdnanceSurveyCompanyConfiguration.applicationKey()).thenReturn(APP_KEY);
		when(mockOrdnanceSurveyLookupService.getAdditionalParametersAndValuesMap(PARAMS_AND_VALUES)).thenReturn(mockParamsAndValues);
		when(mockOrdnanceSurveyLookupService.getURL(ENDPOINT, "query", VALUE, mockParamsAndValues, APP_KEY)).thenReturn(URL);
		when(mockOrdnanceSurveyLookupService.getResults(URL)).thenReturn(mockResults);

		Set<AddressContext> results = ordnanceSurveyAddressLookup.findByKeyword(COMPANY_ID, VALUE, PARAMS_AND_VALUES);

		assertThat(results, sameInstance(mockResults));
	}

	@Test
	public void findByPostcode_WhenException_ThenReturnsEmptySet() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		Set<AddressContext> results = ordnanceSurveyAddressLookup.findByPostcode(COMPANY_ID, VALUE, PARAMS_AND_VALUES);

		assertThat(results, empty());
	}

	@Test
	public void findByPostcode_WhenNoError_ThenReturnsSetOfResults() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockOrdnanceSurveyCompanyConfiguration);
		when(mockOrdnanceSurveyCompanyConfiguration.postcodeEndpointURL()).thenReturn(ENDPOINT);
		when(mockOrdnanceSurveyCompanyConfiguration.applicationKey()).thenReturn(APP_KEY);
		when(mockOrdnanceSurveyLookupService.getAdditionalParametersAndValuesMap(PARAMS_AND_VALUES)).thenReturn(mockParamsAndValues);
		when(mockOrdnanceSurveyLookupService.getURL(ENDPOINT, "postcode", VALUE, mockParamsAndValues, APP_KEY)).thenReturn(URL);
		when(mockOrdnanceSurveyLookupService.getResults(URL)).thenReturn(mockResults);

		Set<AddressContext> results = ordnanceSurveyAddressLookup.findByPostcode(COMPANY_ID, VALUE, PARAMS_AND_VALUES);

		assertThat(results, sameInstance(mockResults));
	}

	@Test
	public void findByUprn_WhenException_ThenReturnsEmptyOptional() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		Optional<AddressContext> result = ordnanceSurveyAddressLookup.findByUprn(COMPANY_ID, VALUE, PARAMS_AND_VALUES);

		assertFalse(result.isPresent());
	}

	@Test
	public void findByUprn_WhenNoErrorAndNoResultsFound_ThenReturnsEmptyOptional() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockOrdnanceSurveyCompanyConfiguration);
		when(mockOrdnanceSurveyCompanyConfiguration.uprnEndpointURL()).thenReturn(ENDPOINT);
		when(mockOrdnanceSurveyCompanyConfiguration.applicationKey()).thenReturn(APP_KEY);
		when(mockOrdnanceSurveyLookupService.getAdditionalParametersAndValuesMap(PARAMS_AND_VALUES)).thenReturn(mockParamsAndValues);
		when(mockOrdnanceSurveyLookupService.getURL(ENDPOINT, "uprn", VALUE, mockParamsAndValues, APP_KEY)).thenReturn(URL);
		when(mockOrdnanceSurveyLookupService.getResults(URL)).thenReturn(Collections.emptySet());

		Optional<AddressContext> result = ordnanceSurveyAddressLookup.findByUprn(COMPANY_ID, VALUE, PARAMS_AND_VALUES);

		assertFalse(result.isPresent());
	}

	@Test
	public void findByUprn_WhenNoErrorAndResultFound_ThenReturnsOptionalWithTheFirstResult() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockOrdnanceSurveyCompanyConfiguration);
		when(mockOrdnanceSurveyCompanyConfiguration.uprnEndpointURL()).thenReturn(ENDPOINT);
		when(mockOrdnanceSurveyCompanyConfiguration.applicationKey()).thenReturn(APP_KEY);
		when(mockOrdnanceSurveyLookupService.getAdditionalParametersAndValuesMap(PARAMS_AND_VALUES)).thenReturn(mockParamsAndValues);
		when(mockOrdnanceSurveyLookupService.getURL(ENDPOINT, "uprn", VALUE, mockParamsAndValues, APP_KEY)).thenReturn(URL);
		Set<AddressContext> resultsFound = new HashSet<>();
		resultsFound.add(mockAddressContext);
		when(mockOrdnanceSurveyLookupService.getResults(URL)).thenReturn(resultsFound);

		Optional<AddressContext> result = ordnanceSurveyAddressLookup.findByUprn(COMPANY_ID, VALUE, PARAMS_AND_VALUES);

		assertThat(result.get(), sameInstance(mockAddressContext));
	}

	@Test
	public void getWeight_WhenException_ThenReturnsZero() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		int result = ordnanceSurveyAddressLookup.getWeight(COMPANY_ID);

		assertThat(result, equalTo(0));
	}

	@Test
	public void getWeight_WhenNoError_ThenReturnsIfTheConfigurationIsNational() throws ConfigurationException {
		int expected = 123;
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockOrdnanceSurveyCompanyConfiguration);
		when(mockOrdnanceSurveyCompanyConfiguration.weight()).thenReturn(expected);

		int result = ordnanceSurveyAddressLookup.getWeight(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void national_WhenException_ThenReturnsFalse() throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenThrow(new ConfigurationException());

		boolean result = ordnanceSurveyAddressLookup.national(COMPANY_ID);

		assertFalse(result);
	}

	@Test
	@Parameters({ "true", "false" })
	public void national_WhenNoError_ThenReturnsIfTheConfigurationIsNational(boolean expected) throws ConfigurationException {
		when(mockConfigurationProvider.getCompanyConfiguration(OrdnanceSurveyCompanyConfiguration.class, COMPANY_ID)).thenReturn(mockOrdnanceSurveyCompanyConfiguration);
		when(mockOrdnanceSurveyCompanyConfiguration.national()).thenReturn(expected);

		boolean result = ordnanceSurveyAddressLookup.national(COMPANY_ID);

		assertThat(result, equalTo(expected));
	}
}
