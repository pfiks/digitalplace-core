package com.placecube.digitalplace.externalpage.web.cleaner;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import org.htmlcleaner.ContentNode;
import org.htmlcleaner.HtmlNode;
import org.htmlcleaner.TagNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ HtmlCleanerFactory.class })
public class LeadingSpaceTagNodeVisitorTest {

	private static final String NODE_CONTENT = "<p>content</p>";

	private LeadingSpaceTagNodeVisitor leadingSpaceTagNodeVisitor;

	@Mock
	private TagNode mockTagNode;

	@Mock
	private HtmlNode mockHtmlNode;

	@Mock
	private ContentNode mockHtmlContentNode;

	@Mock
	private ContentNode mockContentNode;

	@Before
	public void setUp() {
		leadingSpaceTagNodeVisitor = new LeadingSpaceTagNodeVisitor();

		mockStatic(HtmlCleanerFactory.class);
	}

	@Test
	public void visit_WhenHtmlNodeIsContentNode_ThenInsertsNewContentNodeWithLeadingSpace_AndRemovesHtmlNodeInOrder() {
		when(mockHtmlContentNode.getContent()).thenReturn(NODE_CONTENT);
		when(HtmlCleanerFactory.createContentNode(StringPool.SPACE + NODE_CONTENT)).thenReturn(mockContentNode);

		leadingSpaceTagNodeVisitor.visit(mockTagNode, mockHtmlContentNode);

		InOrder inOrder = inOrder(mockTagNode);

		inOrder.verify(mockTagNode, times(1)).insertChildAfter(mockHtmlContentNode, mockContentNode);
		inOrder.verify(mockTagNode, times(1)).removeChild(mockHtmlContentNode);
	}

	@Test
	public void visit_WhenHtmlNodeIsNotContentNode_ThenDoesNotRemovesHtmlNode_NorInsertsNewContentNode() {
		leadingSpaceTagNodeVisitor.visit(mockTagNode, mockHtmlNode);

		verifyZeroInteractions(mockTagNode);
	}
}
