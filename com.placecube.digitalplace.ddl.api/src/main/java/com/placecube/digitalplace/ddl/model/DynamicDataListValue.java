package com.placecube.digitalplace.ddl.model;

public interface DynamicDataListValue {

	/**
	 * This will return additional details for the field, e.g. if the field
	 * is a document, it'll include the number of downloads
	 * 
	 * @return the field additional details
	 */
	String getFieldAdditionalDetails();

	String getFieldName();

	String getFieldValue();

}
