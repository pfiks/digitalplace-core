package com.placecube.digitalplace.pageimportadmin.portlet;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.placecube.digitalplace.pageimportadmin.model.RowImportModel;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MVCPortlet.class })
public class PageImportPortletTest extends PowerMockito {
	private static final long globalGroupId = 777l;

	private static final long groupId = 123l;

	@Mock
	private List<AssetVocabulary> mockAssetVocabularies;

	@Mock
	private AssetVocabularyLocalService mockAssetVocabularyLocalService;

	@Mock
	private Company mockCompany;

	@Mock
	private List<AssetVocabulary> mockGlobalAssetVocabularies;

	@Mock
	private Group mockGroup;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private List<RowImportModel> mockRowImportModels;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@InjectMocks
	private PageImportPortlet pageImportPortlet;

	@SuppressWarnings("rawtypes")
	private void mockCallToSuper() throws Exception {
		Class[] cArg = new Class[2];
		cArg[0] = RenderRequest.class;
		cArg[1] = RenderResponse.class;
		Method superRenderMethod = MVCPortlet.class.getMethod("render", cArg);
		MemberModifier.suppress(superRenderMethod);
	}

	@Test(expected = PortletException.class)
	public void render_WhenExceptionRetrievingVocabularies_ThenThrowsPortletException() throws Exception {

		mockCallToSuper();
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockAssetVocabularyLocalService.getGroupVocabularies(groupId)).thenThrow(new PortalException());
		pageImportPortlet.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenSetsRowImportModelsAsRequestAttribute() throws Exception {

		mockCallToSuper();
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockAssetVocabularyLocalService.getGroupVocabularies(groupId)).thenReturn(mockAssetVocabularies);
		when(mockRenderRequest.getAttribute("rowImportModels")).thenReturn(mockRowImportModels);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(globalGroupId);

		when(mockAssetVocabularyLocalService.getGroupVocabularies(globalGroupId)).thenReturn(mockGlobalAssetVocabularies);

		pageImportPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("rowImportModels", mockRowImportModels);
	}

	@Test
	public void render_WhenNoError_ThenSetsVocabulariesAsRequestAttribute() throws Exception {

		mockCallToSuper();
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockAssetVocabularyLocalService.getGroupVocabularies(groupId)).thenReturn(mockAssetVocabularies);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(globalGroupId);

		when(mockAssetVocabularyLocalService.getGroupVocabularies(globalGroupId)).thenReturn(mockGlobalAssetVocabularies);
		List<AssetVocabulary> allVocabularies = new ArrayList<>();

		pageImportPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("vocabularies", allVocabularies);
	}

	@Test
	public void render_WhenStagingEnabledAndIsStagingGroup_ThenDoesntSetDisabledFlagAsRequestAttribute() throws Exception {
		mockCallToSuper();
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.isStaged()).thenReturn(true);
		when(mockGroup.isStagingGroup()).thenReturn(true);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(globalGroupId);

		pageImportPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(0)).setAttribute("disableImportStaging", Boolean.TRUE);
	}

	@Test
	public void render_WhenStagingEnabledAndNotStagingGroup_ThenSetsDisabledFlagAsRequestAttribute() throws Exception {
		mockCallToSuper();
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.isStaged()).thenReturn(true);
		when(mockGroup.isStagingGroup()).thenReturn(false);

		pageImportPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("disableImportStaging", Boolean.TRUE);
	}

	@Test
	public void render_WhenStagingNotEnabled_ThenDoesntSetDisabledFlagAsRequestAttribute() throws Exception {
		mockCallToSuper();
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getScopeGroup()).thenReturn(mockGroup);
		when(mockGroup.isStaged()).thenReturn(false);
		when(mockGroup.isStagingGroup()).thenReturn(false);
		when(mockThemeDisplay.getCompany()).thenReturn(mockCompany);
		when(mockCompany.getGroupId()).thenReturn(globalGroupId);

		pageImportPortlet.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(0)).setAttribute("disableImportStaging", Boolean.TRUE);
	}
}
