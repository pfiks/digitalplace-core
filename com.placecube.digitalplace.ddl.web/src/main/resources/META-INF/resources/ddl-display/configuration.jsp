<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@ include file="init.jsp" %>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="true" var="configurationRenderURL" />

<liferay-frontend:edit-form action="${configurationActionURL}" method="post" name="ddlEditorForm" >

	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="${configurationRenderURL}" />

	<liferay-frontend:edit-form-body>
		<dp-frontend:fieldset-group>

			<liferay-frontend:fieldset>
				<%@ include file ="../configuration-structure-select.jspf" %>
            </liferay-frontend:fieldset>

            <c:if test="${not empty availableVocabularies}">
				<liferay-frontend:fieldset label="ddl-web-display-mandatory-vocabulary-ids">
					<c:forEach items="${availableVocabularies}" var="availableVocabulary">
						<aui:input type="checkbox" name="mandatoryVocabularyIds" label="${availableVocabulary.getTitle(locale)}" value="${availableVocabulary.getVocabularyId()}"
							checked="${mandatoryVocabularyIds.contains(availableVocabulary.getVocabularyId())}"/>
					</c:forEach>
				</liferay-frontend:fieldset>
			</c:if>

			<c:if test="${not empty availableCategories}">
				<liferay-frontend:fieldset label="ddl-web-display-mandatory-category-ids">
					<c:forEach items="${availableCategories}" var="availableCategory">
						<aui:input type="checkbox" name="mandatoryCategoryIds" label="${availableCategory.getTitle(locale)}" value="${availableCategory.getCategoryId()}"
							checked="${mandatoryCategoryIds.contains(availableCategory.getCategoryId())}"/>
					</c:forEach>
				</liferay-frontend:fieldset>
			</c:if>

		    <liferay-frontend:fieldset>
		    	<aui:input name="enableTranslations" label="ddl-web-display-enableTranslations" type="toggle-switch" value="${configuration.enableTranslations()}" />
		    </liferay-frontend:fieldset>

			<liferay-frontend:fieldset>
		    	<aui:input name="showLoginOption" label="ddl-web-display-show-login-option" type="toggle-switch" value="${configuration.showLoginOption()}" />
		    </liferay-frontend:fieldset>

		</dp-frontend:fieldset-group>
	</liferay-frontend:edit-form-body>

	<liferay-frontend:edit-form-footer>
		<aui:button type="submit" />

		<aui:button type="cancel" />
	</liferay-frontend:edit-form-footer>

</liferay-frontend:edit-form>

