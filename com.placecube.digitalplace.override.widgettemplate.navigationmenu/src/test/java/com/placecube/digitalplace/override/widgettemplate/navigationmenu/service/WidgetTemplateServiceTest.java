package com.placecube.digitalplace.override.widgettemplate.navigationmenu.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.theme.NavItem;
import com.liferay.portal.kernel.util.Portal;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RestrictionsFactoryUtil.class)
public class WidgetTemplateServiceTest extends PowerMockito {

	@InjectMocks
	private WidgetTemplateService widgetTemplateUpdateService;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private Criterion mockCriterionGroupId;

	@Mock
	private Criterion mockCriterionClassNameId;

	@Before
	public void setUp() {
		mockStatic(RestrictionsFactoryUtil.class);
	}

	@Test
	public void getNavigationMenuTemplates_WhenNoError_ThenReturnsTheTemplatesFromTheDynamicQuery() {
		long groupId = 123;
		when(mockDDMTemplateLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		List<Object> expected = new ArrayList<>();
		expected.add(mockDDMTemplate);
		when(mockDDMTemplateLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(expected);

		List<DDMTemplate> results = widgetTemplateUpdateService.getNavigationMenuTemplates(groupId);

		assertThat(results, sameInstance(expected));
	}

	@Test
	public void getNavigationMenuTemplates_WhenNoError_ThenConfiuguresTheDynamicQueryBeforeReturningTheResults() {
		long groupId = 123;
		long classNameId = 456;
		when(mockPortal.getClassNameId(NavItem.class)).thenReturn(classNameId);
		when(mockDDMTemplateLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(RestrictionsFactoryUtil.eq("groupId", groupId)).thenReturn(mockCriterionGroupId);
		when(RestrictionsFactoryUtil.eq("classNameId", classNameId)).thenReturn(mockCriterionClassNameId);

		widgetTemplateUpdateService.getNavigationMenuTemplates(groupId);

		InOrder inOrder = Mockito.inOrder(mockDynamicQuery, mockDDMTemplateLocalService);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionGroupId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionClassNameId);
		inOrder.verify(mockDDMTemplateLocalService, times(1)).dynamicQuery(mockDynamicQuery);
	}

	@Test
	public void removeNoItemsFoundAlert_WhenNoError_ThenUpdatesTheTemplateScript() {
		String script = "Hello template script <div class=\"alert alert-info\">  <@liferay.language key=\"there-are-no-menu-items-to-display\" />  </div> with some more text";
		String expectedScript = "Hello template script  with some more text";
		when(mockDDMTemplate.getScript()).thenReturn(script);

		widgetTemplateUpdateService.removeNoItemsFoundAlert(mockDDMTemplate);

		InOrder inOrder = Mockito.inOrder(mockDDMTemplate, mockDDMTemplateLocalService);
		inOrder.verify(mockDDMTemplate, times(1)).setScript(expectedScript);
		inOrder.verify(mockDDMTemplateLocalService, times(1)).updateDDMTemplate(mockDDMTemplate);
	}
}
