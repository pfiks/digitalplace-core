/**
 * RequestIdentification.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uk.co.capita_software.support.selfservice;

public class RequestIdentification  implements java.io.Serializable {
    private java.lang.String uniqueReference;

    private java.lang.String timeStamp;

    public RequestIdentification() {
    }

    public RequestIdentification(
           java.lang.String uniqueReference,
           java.lang.String timeStamp) {
           this.uniqueReference = uniqueReference;
           this.timeStamp = timeStamp;
    }


    /**
     * Gets the uniqueReference value for this RequestIdentification.
     *
     * @return uniqueReference
     */
    public java.lang.String getUniqueReference() {
        return uniqueReference;
    }


    /**
     * Sets the uniqueReference value for this RequestIdentification.
     *
     * @param uniqueReference
     */
    public void setUniqueReference(java.lang.String uniqueReference) {
        this.uniqueReference = uniqueReference;
    }


    /**
     * Gets the timeStamp value for this RequestIdentification.
     *
     * @return timeStamp
     */
    public java.lang.String getTimeStamp() {
        return timeStamp;
    }


    /**
     * Sets the timeStamp value for this RequestIdentification.
     *
     * @param timeStamp
     */
    public void setTimeStamp(java.lang.String timeStamp) {
        this.timeStamp = timeStamp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestIdentification)) return false;
        RequestIdentification other = (RequestIdentification) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.uniqueReference==null && other.getUniqueReference()==null) ||
             (this.uniqueReference!=null &&
              this.uniqueReference.equals(other.getUniqueReference()))) &&
            ((this.timeStamp==null && other.getTimeStamp()==null) ||
             (this.timeStamp!=null &&
              this.timeStamp.equals(other.getTimeStamp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUniqueReference() != null) {
            _hashCode += getUniqueReference().hashCode();
        }
        if (getTimeStamp() != null) {
            _hashCode += getTimeStamp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestIdentification.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "requestIdentification"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uniqueReference");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "uniqueReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("https://support.capita-software.co.uk/selfservice/?commonFoundation", "timeStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType,
           java.lang.Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

    @Override
    public String toString() {
        return "RequestIdentification [uniqueReference=" + uniqueReference
                + ", timeStamp=" + timeStamp + "]";
    }



}
