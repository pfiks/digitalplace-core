package com.placecube.digitalplace.webcontent.email.internal.upgrade;

import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.placecube.digitalplace.webcontent.email.internal.upgrade_1_0_0.EmailDefaultTemplateUpgradeProcess;
import com.placecube.digitalplace.webcontent.email.internal.upgrade_1_0_1.UpdateArticleTemplateIdUpgradeProcess;
import com.placecube.digitalplace.webcontent.email.internal.upgrade_1_0_2.EmailWebContentTemplateUpgrade;
import com.placecube.digitalplace.webcontent.email.internal.upgrade_1_0_3.EmailWebContentTemplateStaticUtilUpgrade;
import com.placecube.digitalplace.webcontent.email.service.EmailWebContentService;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class EmailWebContentUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private EmailWebContentService emailWebContentService;

	@Reference
	private Portal portal;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new EmailDefaultTemplateUpgradeProcess(emailWebContentService, ddmStructureLocalService, portal, userLocalService));
		registry.register("1.0.0", "1.0.1", new UpdateArticleTemplateIdUpgradeProcess(ddmStructureLocalService));
		registry.register("1.0.1", "1.0.2", new EmailWebContentTemplateUpgrade(ddmTemplateLocalService));
		registry.register("1.0.2", "1.0.3", new EmailWebContentTemplateStaticUtilUpgrade(ddmTemplateLocalService));
	}
}
