package com.placecube.digitalplace.appointment.booking.digitalplaceconnector;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.placecube.digitalplace.appointment.booking.model.AppointmentBookingSlot;

@RunWith(MockitoJUnitRunner.class)
public class DigitalPlaceAppointmentConnectorQueryUtilTest {

	@InjectMocks
	private DigitalPlaceAppointmentConnectorQueryUtil digitalPlaceAppointmentConnectorQueryUtil;

	@Mock
	private AppointmentBookingSlot mockAppointmentBookingSlotOther;

	@Mock
	private AppointmentBookingSlot mockAppointmentBookingSlotToCheck;

	@Mock
	private LocalDateTime mockLocalDateTimeOtherEnd;

	@Mock
	private LocalDateTime mockLocalDateTimeOtherStart;

	@Mock
	private LocalDateTime mockLocalDateTimeToCheckEnd;

	@Mock
	private LocalDateTime mockLocalDateTimeToCheckStart;

	@Test
	public void doesSlotOverlap_WhenToCheckEndDateIsAfterOtherStartDateAndToCheckStartDateIsBeforeOtherEndDate_ThenReturnsTrue() {
		when(mockAppointmentBookingSlotToCheck.getEndDateTime()).thenReturn(mockLocalDateTimeToCheckEnd);
		when(mockAppointmentBookingSlotToCheck.getStartDateTime()).thenReturn(mockLocalDateTimeToCheckStart);

		when(mockAppointmentBookingSlotOther.getEndDateTime()).thenReturn(mockLocalDateTimeOtherEnd);
		when(mockAppointmentBookingSlotOther.getStartDateTime()).thenReturn(mockLocalDateTimeOtherStart);

		when(mockLocalDateTimeToCheckEnd.isAfter(mockLocalDateTimeOtherStart)).thenReturn(true);
		when(mockLocalDateTimeToCheckStart.isBefore(mockLocalDateTimeOtherEnd)).thenReturn(true);

		boolean result = digitalPlaceAppointmentConnectorQueryUtil.doesSlotOverlap(mockAppointmentBookingSlotToCheck, mockAppointmentBookingSlotOther);

		assertTrue(result);
	}

	@Test
	public void doesSlotOverlap_WhenToCheckEndDateIsAfterOtherStartDateAndToCheckStartDateIsNotBeforeOtherEndDate_ThenReturnsFalse() {
		when(mockAppointmentBookingSlotToCheck.getEndDateTime()).thenReturn(mockLocalDateTimeToCheckEnd);
		when(mockAppointmentBookingSlotToCheck.getStartDateTime()).thenReturn(mockLocalDateTimeToCheckStart);

		when(mockAppointmentBookingSlotOther.getEndDateTime()).thenReturn(mockLocalDateTimeOtherEnd);
		when(mockAppointmentBookingSlotOther.getStartDateTime()).thenReturn(mockLocalDateTimeOtherStart);

		when(mockLocalDateTimeToCheckEnd.isAfter(mockLocalDateTimeOtherStart)).thenReturn(true);
		when(mockLocalDateTimeToCheckStart.isBefore(mockLocalDateTimeOtherEnd)).thenReturn(false);

		boolean result = digitalPlaceAppointmentConnectorQueryUtil.doesSlotOverlap(mockAppointmentBookingSlotToCheck, mockAppointmentBookingSlotOther);

		assertFalse(result);
	}

	@Test
	public void doesSlotOverlap_WhenToCheckEndDateIsNotAfterOtherStartDate_ThenReturnsFalse() {
		when(mockAppointmentBookingSlotToCheck.getEndDateTime()).thenReturn(mockLocalDateTimeToCheckEnd);

		when(mockAppointmentBookingSlotOther.getStartDateTime()).thenReturn(mockLocalDateTimeOtherStart);

		when(mockLocalDateTimeToCheckEnd.isAfter(mockLocalDateTimeOtherStart)).thenReturn(false);

		boolean result = digitalPlaceAppointmentConnectorQueryUtil.doesSlotOverlap(mockAppointmentBookingSlotToCheck, mockAppointmentBookingSlotOther);

		assertFalse(result);
	}

}
