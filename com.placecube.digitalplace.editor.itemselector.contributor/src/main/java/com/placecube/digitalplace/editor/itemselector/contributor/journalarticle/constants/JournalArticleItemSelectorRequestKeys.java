package com.placecube.digitalplace.editor.itemselector.contributor.journalarticle.constants;

public class JournalArticleItemSelectorRequestKeys {

	public static final String JOURNAL_ARTICLE_ITEM_SELECTOR_VIEW_DISPLAY_CONTEXT = "journalArticleItemSelectorViewDisplayContext";
	public static final String JOURNAL_ARTICLE_ITEM_SELECTOR_VIEW_HELPER = "journalArticleItemSelectorViewHelper";
	public static final String JOURNAL_ARTICLE_ITEM_SELECTOR_VIEW_MANAGEMENT_TOOLBAR_DISPLAY_CONTEXT = "journalArticleItemSelectorViewManagementToolbarDisplayContext";
	public static final String HTML = "html";

	private JournalArticleItemSelectorRequestKeys() {

	}
}
