package com.placecube.digitalplace.user.serviceoverride.internal.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.placecube.digitalplace.user.serviceoverride.model.AuthenticationActionContext;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.digitalplace.user.serviceoverride.service.UserServiceOverrideAction;

@Component(immediate = true, service = UserServiceOverrideActionUtil.class)
public class UserServiceOverrideActionUtil {

	@Reference
	private UserServiceOverrideActionRegistry userServiceOverrideActionRegistry;

	public void executeActionsAfterUserAuthentication(AuthenticationActionContext authenticationActionContext) throws PortalException {

		List<UserServiceOverrideAction> userServiceOverrideActions = userServiceOverrideActionRegistry.getUserServiceOverrideActions();

		for (UserServiceOverrideAction userServiceOverrideAction : userServiceOverrideActions) {
			userServiceOverrideAction.executeAfterUserAuthentication(authenticationActionContext);
		}

	}

	public void executeActionsAfterVerifyEmailAddress(long userId) {
		if (userId > 0) {
			List<UserServiceOverrideAction> userActions = userServiceOverrideActionRegistry.getUserServiceOverrideActions();

			for (UserServiceOverrideAction userListenerAction : userActions) {
				userListenerAction.executeOnAfterVerifyEmailAddress(userId);
			}
		}
	}

	public void executeActionsForUserAddedToGroup(long userId, long groupId) {
		List<UserServiceOverrideAction> userActions = userServiceOverrideActionRegistry.getUserServiceOverrideActions();

		for (UserServiceOverrideAction userListenerAction : userActions) {
			userListenerAction.executeOnAfterAddUserToGroupActions(userId, groupId);
		}
	}

	public User executeActionsForUserCreation(User user, Optional<ServiceContext> serviceContext) {

		List<UserServiceOverrideAction> userListenerActions = userServiceOverrideActionRegistry.getUserServiceOverrideActions();

		for (UserServiceOverrideAction userListenerAction : userListenerActions) {
			try {
				user = userListenerAction.executeOnAfterCreateActions(user, serviceContext);
			} catch (Exception e) {
				throw new SystemException(e);
			}
		}
		return user;
	}

	public User executeActionsForUpdateUser(User user, Optional<ServiceContext> serviceContext) throws PortalException {

		List<UserServiceOverrideAction> userListenerActions = userServiceOverrideActionRegistry.getUserServiceOverrideActions();

		for (UserServiceOverrideAction userListenerAction : userListenerActions) {
		
			user = userListenerAction.executeOnAfterUpdateActions(user, serviceContext);
			
		}
		return user;
	}

	public void executeActionsForUserDeletion(User user) {
		List<UserServiceOverrideAction> userActions = userServiceOverrideActionRegistry.getUserServiceOverrideActions();

		for (UserServiceOverrideAction userListenerAction : userActions) {
			userListenerAction.executeOnAfterDeleteActions(user);
		}
	}

	public void executeActionsForUserRemovedFromGroup(long userId, long groupId, Optional<ServiceContext> serviceContext) {
		List<UserServiceOverrideAction> userActions = userServiceOverrideActionRegistry.getUserServiceOverrideActions();

		for (UserServiceOverrideAction userListenerAction : userActions) {
			userListenerAction.executeOnAfterRemoveUserFromGroupActions(userId, groupId, serviceContext);
		}
	}

	public List<Long> getGroupIds(User user) {
		if (Validator.isNotNull(user)) {
			return Arrays.asList(ArrayUtil.toArray(user.getGroupIds()));
		}
		return new ArrayList<>();
	}

	public List<Long> getGroupIdsAdded(List<Long> originalGroupIds, List<Long> updatedGroupIds) {
		List<Long> groupIdsToUpdate = new ArrayList<>(updatedGroupIds);
		groupIdsToUpdate.removeAll(originalGroupIds);
		return groupIdsToUpdate;
	}

	public List<Long> getGroupIdsRemoved(List<Long> originalGroupIds, List<Long> updatedGroupIds) {
		List<Long> groupIdsToUpdate = new ArrayList<>(originalGroupIds);
		groupIdsToUpdate.removeAll(updatedGroupIds);
		return groupIdsToUpdate;
	}
}
